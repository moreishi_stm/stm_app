#!/bin/bash
# By Chris Willard <chriswillard.dev@gmail.com>
# Will sync stm repositories to the nginx environment

divider="=============================================="

# Determines if the sync process should only update and do a dry sync
declare -a rsyncOptions=(azv azvn)
rsyncOption=${rsyncOptions[0]};
dryText="*Live*"
if [ "$1" == 'dry' ]; then
    rsyncOption=${rsyncOptions[1]}
    dryText="*Dry Run*"
fi

echo $divider
echo "Seize the Market Production Sync Tool $dryText v1.0"
echo $divider
declare -a repositories=('stm_app' 'christineleeteam.com' 'seizethemarket.com', 'wisconsinpropertiesonline.com')
repoCount=0
for repository in ${repositories[@]}
do
    repoCount=$((repoCount+1))
    echo "$repoCount) $repository"
done

echo "Select a repository to sync(1,2,3,etc...):"
read repository
repository=$((repository-1))
if [ ! ${repositories[$repository]+exists} ]; then
    echo "Repository does not exist, exiting..."
   exit 1;
fi

chosenRepository=${repositories[$repository]}
if [ ! -e /var/git/$chosenRepository/.git/info ]; then
    echo "Repository does not exist, exiting..."
    exit 1;
fi

echo $divider
echo "$chosenRepository chosen, pulling changes"
echo $divider
cd "/var/git/$chosenRepository" && git pull origin production
echo $divider

echo "Rsync executing $dryText"
echo $divider
if [ $chosenRepository = 'stm_app' ]; then
    destinationDirPath="/var/www/$chosenRepository"
else
    destinationDirPath="/var/www/stm_domains/$chosenRepository"
fi

rsync -$rsyncOption --omit-dir-times --checksum --exclude '*.git*' --exclude 'Vagrant*' --exclude '*.htaccess*' --progress /var/git/$chosenRepository/* $destinationDirPath

echo $divider
echo "Sync completed @ $(date +%Y-%m-%d@%H:%M:%S)"
echo $divider
exit 1

