$(function () {
    /**
     * Closings maintenance (i.e. regarding addition or removal of
     * Title Companies, Lenders, Other Agents, or Inspectors, etc.) related.
     */

    //For each of these we'll filter out any placeholders by only counting
    //those option values which actually have a value greater than zero.
    var numAvailableInspectors = $('#CompanyAssignmentLu_inspectors_0_company_id option:hidden').filter(function (index) {
        return this.value > 0;
    }).length;

    var inspectorContainer = $('#addlInspectors');

    var reIndexRows = function (selector) {
        var reindexingConfig = {

            inspector : {
                rowSelector         : '.inspectorRow:visible',
                elementNameTemplate : 'Assignments[inspectors][<INDEX>]company_id',
                elementIdTemplate   : 'Assignments_inspectors_<INDEX>_company_id'
            },
        };

        var replacementString = '<INDEX>';

        //Get each element identified by the selector and update their element name and label ID based
        //on their individual indicies. This is done to ensure that after several additions/removals
        //the element IDs are still properly unique and orderly.

        var configuration = reindexingConfig[selector];

        $(configuration.rowSelector).each(function (index) {
            //Each div consists of a Label, Selector/Combo Box, and a Remove Button as its children
            var comboBox = $(this).find('select');

            ++index;
            var newSelectorName = configuration.elementNameTemplate.replace(replacementString, index.toString());
            var newSelectorId = configuration.elementIdTemplate.replace(replacementString, index.toString());

            comboBox.attr('name', newSelectorName);
            comboBox.attr('id', newSelectorId);
        });
    };

    $('#add-inspector').live('click', function () {
        var inspectorRows = $('.inspectorRow:visible');

        if (inspectorRows.length < numAvailableInspectors) {
            var newInspectorRow = $('#addlInspectorTemplate').clone()
                .removeAttr('style')
                .appendTo(inspectorContainer);

            reIndexRows('inspector');

            // Clear any disables
            newInspectorRow.find('select').attr('disabled', false);
        }
    });

    $('.remove-inspector').live('click', function () {
        var inspectorRow = $(this).parents('div.inspectorRow');

        inspectorRow.hide('slow', function () {
            this.remove();
            reIndexRows('inspector');
        });
    });
});
