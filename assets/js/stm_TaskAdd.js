$(document).ready(function(){
	/* ================================================================================================ */
	$('#add-task-button').click(function(){	
		$( '#add-task-dialog' ).dialog( 'open' );
	});

	/* ================================================================================================ */
	$( '#add-task-dialog' ).dialog({
		autoOpen: false,
		height: 'auto', /*570*/
		width: 680,
		modal: true,
		closeOnEscape: true,
		closeText: 'X',
		dialogClass: "stm-dialog",
		drag: function() {
			$('.tipsy').remove();
		},
		close: function() {
			$('.form').wl_Form('reset');
			$('.tipsy').remove();
            $('.email-alert').hide();
		},
		buttons: {
			"Add Task":function(){
				$('.form').wl_Form('submit');
			}
		},
		open: function(){
			$('#task-due-date').wl_Date();
			$('.ui-dialog:visible').find('.ui-dialog-titlebar-close').attr('href','javascipt:void(0)');
		},
	});
	
	/* ================================================================================================ */
//	$('#add-task-dialog').find('input.date, div.date').wl_Date();

	$('form#add-task-form').wl_Form({
		ajax:true,
		onBeforeSubmit: function(){
			//$.wl_Alert('About to submit to URL:','info','#dialog-form');
		},
		onSuccess: function(data, textStatus) {
			if(data == 0){
				$.wl_Alert('ERROR: Contact was NOT added!','warning','#dialog-form');	
			}else{
				//$.wl_Alert('Contact ID #'+ data +' has been added!','success','#dialog-form');	
				window.location = '/admin/contacts/'+data;
			}
		},
		onError: function (textStatus, error, jqXHR) {
			$.wl_Alert('ERROR: Add processing error!','warning','#dialog-form');	
		},		
	});

	/* ================================================================================================ */
	$("select[multiple]").wl_Multiselect({
		searchfield: false,
		height: 110
	});
		
	/* ================================================================================================ */
});
