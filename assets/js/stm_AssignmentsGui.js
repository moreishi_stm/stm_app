$(function () {
    /**
     * Assignments Template processing
     */
    assignmentTemplateRow = $('div#addlAssignmentTemplate:hidden');
    addlAssignmentRowCount = 0 + $('div#addlAssignmentRows div.assignmentRow').length; // prevent index conflicts
    $('#add-assignment').live('click', function () {

        // Clone the template row and update the row id
        var newAssignmentRow = assignmentTemplateRow.clone().appendTo('div#addlAssignmentRows');
        var newAssignmentRowInputContactId = newAssignmentRow.find('select#Assignments_0_assignee_contact_id');
        var newAssignmentRowInputAssignmentTypeId = newAssignmentRow.find('select#Assignments_0_assignment_type_id');
        var newAssignmentRowHiddenInputId = newAssignmentRow.find('input#Assignments_0_id');
        var newAssignmentRowHiddenInputRemove = newAssignmentRow.find('input#Assignments_0_remove');
        var newAssignmentRowErrorDivContactId = newAssignmentRow.find('div#Assignments_0_assignee_contact_id_em_');
        var newAssignmentRowErrorDivAssignmentTypeId = newAssignmentRow.find('div#Assignments_0_assignment_type_id_em_');
        newAssignmentRow.attr('id', 'addlAssignment-' + addlAssignmentRowCount);

        // Configure name and id for batch mode
        newAssignmentRowInputContactId.attr('name', 'Assignments[' + addlAssignmentRowCount + '][assignee_contact_id]');
        newAssignmentRowInputContactId.attr('id', 'Assignments_' + addlAssignmentRowCount + '_assignee_contact_id');
        newAssignmentRowInputContactId.attr('disabled', false);

        newAssignmentRowInputAssignmentTypeId.attr('name', 'Assignments[' + addlAssignmentRowCount + '][assignment_type_id]');
        newAssignmentRowInputAssignmentTypeId.attr('id', 'Assignments_' + addlAssignmentRowCount + '_assignment_type_id');
        newAssignmentRowInputAssignmentTypeId.attr('disabled', false);

        newAssignmentRowHiddenInputId.attr('name', 'Assignments[' + addlAssignmentRowCount + '][id]');
        newAssignmentRowHiddenInputId.attr('id', 'Assignments_' + addlAssignmentRowCount + '_id');
        newAssignmentRowHiddenInputId.attr('disabled', false);

        newAssignmentRowHiddenInputRemove.attr('name', 'Assignments[' + addlAssignmentRowCount + '][remove]');
        newAssignmentRowHiddenInputRemove.attr('id', 'Assignments_' + addlAssignmentRowCount + '_remove');
        newAssignmentRowHiddenInputRemove.attr('disabled', false);

        newAssignmentRowErrorDivContactId.attr('id', 'Assignments_' + addlAssignmentRowCount + '_assignee_contact_id_em_');
        newAssignmentRowErrorDivAssignmentTypeId.attr('id', 'Assignments_' + addlAssignmentRowCount + '_assignment_type_id_em_');

        // Show the new row
        newAssignmentRow.hide().show('slow');

        ++addlAssignmentRowCount;
    });

    $('.remove-assignment').live('click', function () {
        $(this).parents('div.assignmentRow:first').find('input[name*="remove"]').val(1);
        $(this).parents('div.assignmentRow:first').hide('slow', function () {
            if ($(this).find('input[name*="id"]').val()) {
                $(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
            }
            else {
                $(this).remove();
            }
        });
    });
});
