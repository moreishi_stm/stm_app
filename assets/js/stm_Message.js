$(function() {

	/**
	 * Create popup application messages
	 * Change message style on the fly: Message.messageStyle = Message.DEFAULT_MESSAGE_STYLE;
	 * Create popup message: Message.create('success', 'My success message.'); // success, error, warning, notice
     * Sticky only works for toast messages
	 * @author Chris Willard
	 * @type Message
	 */
	Message = {

		// Constants for message styles
		TOAST_MESSAGE_STYLE   : 'toast',
		DEFAULT_MESSAGE_STYLE : 'default',

		// Set the default message style
		messageStyle : 'toast',

		// Set the position of the messages
		messagePosition : 'top-right',

		// Message status types
		messageStatusTypes : ['success', 'error', 'warning', 'notice'],

		// Create map for toast style messages
		toastMessageTypes 		: ['showSuccessToast', 'showErrorToast', 'showWarningToast', 'showNoticeToast'],
		toastMessageStatusMap 	: {
			'success'	: 'showSuccessToast',
			'error'		: 'showErrorToast',
			'warning'	: 'showWarningToast',
			'notice'	: 'showNoticeToast',
		},

		create: function(messageStatus, message, sticky) {

			// Ensure that the message style we are intending to use is valid
			if(!this._validateMessageStyle(this.messageStyle))
				return false;

            if (!sticky)
                sticky = false;

			// Generate the message
			var message;
			if(this.messageStyle == this.TOAST_MESSAGE_STYLE)
				message = this._createToastMessage(messageStatus, message, sticky);
			else
				message = this._createDefaultMessage(messageStatus, message);

			return message;
		},

		_createToastMessage: function(messageStatus, message, sticky) {

			if(!this.toastMessageStatusMap[messageStatus]) {
				this._log('This type of message is not supported.');
				return false;
			}

			return $().toastmessage('showToast', {
				text : message,
				sticky : sticky,
				position : this.messagePosition,
				type : messageStatus
			});
		},

		_createDefaultMessage: function(messageStatus, message) {
			if($.inArray(messageStatus, this.messageStatusTypes) == -1) {
				this._log('This type of message is not supported');
				return false;
			}
			title = messageStatus[0].toUpperCase() + messageStatus.slice(1);
			return $.msg(message, { header: title});
		},

		// Validate the message style being used
		_validateMessageStyle: function(messageStyle) {
			if(messageStyle == this.TOAST_MESSAGE_STYLE || messageStyle == this.DEFAULT_MESSAGE_STYLE)
				return true;

			this._log('Could not load message style: '+messageStyle);
			return false;
		},

		_log: function(message) {
			if(console && console.log)
				console.log('Message Component: '+message);
		}
	}

});