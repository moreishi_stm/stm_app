$(function () {
    /**
     * Expenses Template processing
     */
    expenseTemplateRow = $('div#addlClosingExpenseTemplate:hidden');
    addlExpenseRowCount = 0 + $('div#addlExpenseRows div.expenseRow').length; // prevent index conflicts
    $('#add-expense').live('click', function () {

        // Clone the template row and update the row id
        var newExpenseRow = expenseTemplateRow.clone().appendTo('div#addlExpenseRows');
        var newExpenseRowAccountId = newExpenseRow.find('select#AccountingTransactions_0_accounting_account_id');
        var newExpenseRowInputAgentId = newExpenseRow.find('select#AccountingTransactions_0_agent_id');
        var newExpenseRowInputAmount = newExpenseRow.find('input#AccountingTransactions_0_amount');
        var newExpenseRowHiddenInputId = newExpenseRow.find('input#AccountingTransactions_0_id');
        var newExpenseRowHiddenInputRemove = newExpenseRow.find('input#AccountingTransactions_0_remove');
        var newExpenseRowErrorDivAgentId = newExpenseRow.find('div#AccountingTransactions_0_agent_id_em_');
        var newExpenseRowErrorDivAmount = newExpenseRow.find('div#AccountingTransactions_0_amount_em_');
        var newExpenseRowErrorDivAccountId = newExpenseRow.find('div#AccountingTransactions_0_accounting_account_id_em_');
        newExpenseRow.attr('id', 'addlExpense-' + addlExpenseRowCount);

        // Configure name and id for batch mode
        newExpenseRowAccountId.attr('name', 'AccountingTransactions[' + addlExpenseRowCount + '][accounting_account_id]');
        newExpenseRowAccountId.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_accounting_account_id');
        newExpenseRowAccountId.attr('disabled', false);

        newExpenseRowInputAgentId.attr('name', 'AccountingTransactions[' + addlExpenseRowCount + '][agent_id]');
        newExpenseRowInputAgentId.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_agent_id');
        newExpenseRowInputAgentId.attr('disabled', false);

        newExpenseRowInputAmount.attr('name', 'AccountingTransactions[' + addlExpenseRowCount + '][amount]');
        newExpenseRowInputAmount.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_amount');
        newExpenseRowInputAmount.attr('disabled', false);

        newExpenseRowHiddenInputId.attr('name', 'AccountingTransactions[' + addlExpenseRowCount + '][id]');
        newExpenseRowHiddenInputId.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_id');
        newExpenseRowHiddenInputId.attr('disabled', false);

        newExpenseRowHiddenInputRemove.attr('name', 'AccountingTransactions[' + addlExpenseRowCount + '][remove]');
        newExpenseRowHiddenInputRemove.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_remove');
        newExpenseRowHiddenInputRemove.attr('disabled', false);

        newExpenseRowErrorDivAgentId.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_agent_id_em_');
        newExpenseRowErrorDivAmount.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_amount_em_');
        newExpenseRowErrorDivAccountId.attr('id', 'AccountingTransactions_' + addlExpenseRowCount + '_accounting_account_id_em_');
        // Show the new row
        newExpenseRow.hide().show('slow');

        newExpenseRowInputAmount.maskMoney({'precision': 2, 'symbol' : '$', 'showSymbol' : true, 'symbolStay' : false});

        ++addlExpenseRowCount;
    });

    $('.remove-expense').live('click', function () {
        $(this).parents('div.expenseRow:first').find('input[name*="remove"]').val(1);
        $(this).parents('div.expenseRow:first').hide('slow', function () {
            if ($(this).find('input[name*="id"]').val()) {
                $(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
            }
            else {
                $(this).remove();
            }
        });
    });

    // money mask for all amount fields
    $('.expenseAmount').maskMoney({'precision': 2, 'symbol' : '$', 'showSymbol' : true, 'symbolStay' : false});
});
