$(function() {
    // The src for the placeholder image
    var placeholderImageSrc = 'http://cdn.seizethemarket.com/assets/images/default-placeholder.png';

    // Fix broken images on an ajax request completing and when then page first loads
    $('img').error(processBrokenImages);
    $(document).ajaxComplete(function() {
        $('img').error(processBrokenImages);
    });

    function processBrokenImages() {
        if ($(this).attr('src') == placeholderImageSrc) {
            return;
        }

        $(this).attr('src', 'http://cdn.seizethemarket.com/assets/images/default-placeholder.png');
        $(this).css({
            'border' : '1px solid #AAA',
            'background-color' : '#FFF'
        });
    }
});
