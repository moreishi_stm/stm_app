$(function () {
    //to prevent autocomplete on all elements
    $(':input').live('focus',function(){
        $(this).attr('autocomplete', 'off');
    });

    /**
     * Email Template related
     */
    emailTemplateRow = $('div#addlEmailTemplate:hidden');
    addlEmailRowCount = 0 + $('div#addlEmailRows div.emailRow').length; // prevent index conflicts
    $('#add-email').live('click', function () {

        // Clone the template row and update the row id
        var newEmailRow = emailTemplateRow.clone().appendTo('div#addlEmailRows');
        var newEmailRowInput = newEmailRow.find('input#Emails_placeholder_email');
        var newEmailRowHiddenInputPrimary = newEmailRow.find('input#Emails_placeholder_is_primary');
        var newEmailRowHiddenInputId = newEmailRow.find('input#Emails_placeholder_id');
        var newEmailRowHiddenInputRemove = newEmailRow.find('input#Emails_placeholder_remove');
        var newEmailRowErrorDiv = newEmailRow.find('div.errorMessage');
        newEmailRow.attr('id', 'addlEmail-' + addlEmailRowCount);

        // Configure name and id for batch mode
        newEmailRowInput.attr('name', 'Emails[' + addlEmailRowCount + '][email]');
        newEmailRowInput.attr('id', 'Emails_' + addlEmailRowCount + '_email');
        newEmailRowInput.attr('disabled', false);

        newEmailRowHiddenInputPrimary.attr('name', 'Emails[' + addlEmailRowCount + '][is_primary]');
        newEmailRowHiddenInputPrimary.attr('id', 'Emails_' + addlEmailRowCount + 'is_primary');
        newEmailRowHiddenInputPrimary.attr('disabled', false);

        newEmailRowHiddenInputId.attr('name', 'Emails[' + addlEmailRowCount + '][id]');
        newEmailRowHiddenInputId.attr('id', 'Emails_' + addlEmailRowCount + '_id');
        newEmailRowHiddenInputId.attr('disabled', false);

        newEmailRowHiddenInputRemove.attr('name', 'Emails[' + addlEmailRowCount + '][remove]');
        newEmailRowHiddenInputRemove.attr('id', 'Emails_' + addlEmailRowCount + '_remove');
        newEmailRowHiddenInputRemove.attr('disabled', false);

        newEmailRowErrorDiv.attr('id', 'Emails_' + addlEmailRowCount + '_email_em_');

        // Show the new row
        newEmailRow.hide().show('slow');

        ++addlEmailRowCount;
    });

    $('.remove-email').live('click', function () {
        $(this).parents('div.emailRow:first').find('input[name*="remove"]').val(1);
        $(this).parents('div.emailRow:first').hide('slow', function () {
            if ($(this).find('input[name*="id"]').val()) {
                $(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
            }
            else {
                $(this).remove();
            }
        });
    });


    // Unset a contact's e-mail as the primary e-mail
    var removePrimaryEmail = function () {
        $(this).html($(this).html().replace('Remove', 'Make'));
        $(this).find('em').removeClass('i_stm_star').addClass('i_stm_star_4');
        $(this).removeClass('remove-primary-email').addClass('make-primary-email');
        $('div#addlEmailRows input[name*="primary"]').val(0); // reset all primary hidden fields
    };

    // Set a contact's e-mail as the primary e-mail address
    var makePrimaryEmail = function () {
        $('#addlEmailRows').find('.remove-primary-email').each(removePrimaryEmail);
        $(this).html($(this).html().replace('Make', 'Remove'));
        $(this).find('em').removeClass('i_stm_star_4').addClass('i_stm_star');
        $(this).removeClass('make-primary-email').addClass('remove-primary-email');
        $(this).parents('div.emailRow').find('input[name*="primary"]').val(1); // mark this row as a primary e-mail
    };

    /**
     * Process primary email interactions
     */
    $('.make-primary-email').live('click', makePrimaryEmail);
    $('.remove-primary-email').live('click', removePrimaryEmail);

    /**
     * Phone Template related
     */
    phoneTemplateRow = $('div#addlPhoneTemplate:hidden');
    addlPhoneRowCount = 0 + $('div#addlPhoneRows div.phoneRow').length; // prevent index conflicts
    $('#add-phone').live('click', function () {
        // Clone the template row and update the row id
        var newPhoneRow = phoneTemplateRow.clone().appendTo('div#addlPhoneRows');
        var newPhoneRowInput = newPhoneRow.find('input#Phones_0_phone');
        var newPhoneRowHiddenInputPhoneType = newPhoneRow.find('select#Phones_0_phone_type_ma');
        var newPhoneRowHiddenInputOwner = newPhoneRow.find('select#Phones_0_owner_ma');
        var newPhoneRowHiddenInputPrimary = newPhoneRow.find('input#Phones_0_is_primary');
        var newPhoneRowHiddenInputExtension = newPhoneRow.find('input#Phones_0_extension');
        var newPhoneRowHiddenInputId = newPhoneRow.find('input#Phones_0_id');
        var newPhoneRowHiddenInputRemove = newPhoneRow.find('input#Phones_0_remove');
        var newPhoneRowErrorDiv = newPhoneRow.find('div.errorMessage');
        newPhoneRow.attr('id', 'addlPhone-' + addlPhoneRowCount);

        // Configure name and id for batch mode
        newPhoneRowInput.attr('name', 'Phones[' + addlPhoneRowCount + '][phone]');
        newPhoneRowInput.attr('id', 'Phones_' + addlPhoneRowCount + '_phone');
        newPhoneRowInput.removeAttr('disabled');
        newPhoneRowInput.inputmask('(999) 999-9999');

        newPhoneRowHiddenInputPhoneType.attr('name', 'Phones[' + addlPhoneRowCount + '][phone_type_ma]');
        newPhoneRowHiddenInputPhoneType.attr('id', 'Phones_' + addlPhoneRowCount + '_phone_type_ma');
        newPhoneRowHiddenInputPhoneType.removeAttr('disabled', false);

        newPhoneRowHiddenInputOwner.attr('name', 'Phones[' + addlPhoneRowCount + '][owner_ma]');
        newPhoneRowHiddenInputOwner.attr('id', 'Phones_' + addlPhoneRowCount + '_owner_ma');
        newPhoneRowHiddenInputOwner.removeAttr('disabled', false);

        newPhoneRowHiddenInputPrimary.attr('name', 'Phones[' + addlPhoneRowCount + '][is_primary]');
        newPhoneRowHiddenInputPrimary.attr('id', 'Phones_' + addlPhoneRowCount + '_is_primary');
        newPhoneRowHiddenInputPrimary.removeAttr('disabled');

        newPhoneRowHiddenInputExtension.attr('name', 'Phones[' + addlPhoneRowCount + '][extension]');
        newPhoneRowHiddenInputExtension.attr('id', 'Phones_' + addlPhoneRowCount + 'extension');
        newPhoneRowHiddenInputExtension.removeAttr('disabled',false);

        newPhoneRowHiddenInputId.attr('name', 'Phones[' + addlPhoneRowCount + '][id]');
        newPhoneRowHiddenInputId.attr('id', 'Phones_' + addlPhoneRowCount + '_id');
        newPhoneRowHiddenInputId.removeAttr('disabled', false);

        newPhoneRowHiddenInputRemove.attr('name', 'Phones[' + addlPhoneRowCount + '][remove]');
        newPhoneRowHiddenInputRemove.attr('id', 'Phones_' + addlPhoneRowCount + '_remove');
        newPhoneRowHiddenInputRemove.removeAttr('disabled', false);

        newPhoneRowErrorDiv.attr('id', 'Phones_' + addlPhoneRowCount + '_phone_em_');

        // Show the new row
        newPhoneRow.hide().show('slow');

        ++addlPhoneRowCount;
    });

    $('.remove-phone').live('click', function () {
        $(this).parents('div.phoneRow:first').find('input[name*="remove"]').val(1);
        $(this).parents('div.phoneRow:first').hide('slow', function () {
            if ($(this).find('input[name*="id"]').val()) {
                $(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
            }
            else {
                $(this).remove();
            }
        });
    });

    // Unset a contact's e-mail as the primary e-mail
    var removePrimaryPhone = function () {
        $(this).html($(this).html().replace('Remove', 'Make'));
        $(this).find('em').removeClass('i_stm_star').addClass('i_stm_star_4');
        $(this).removeClass('remove-primary-phone').addClass('make-primary-phone');
        $('div#addlPhoneRows input[name*="primary"]').val(0); // reset all primary hidden fields
    };

    // Set a contact's e-mail as the primary e-mail address
    var makePrimaryPhone = function () {
        $('#addlPhoneRows').find('.remove-primary-phone').each(removePrimaryPhone);
        $(this).html($(this).html().replace('Make', 'Remove'));
        $(this).find('em').removeClass('i_stm_star_4').addClass('i_stm_star');
        $(this).removeClass('make-primary-phone').addClass('remove-primary-phone');
        $(this).parents('div.phoneRow').find('input[name*="primary"]').val(1); // mark this row as a primary e-mail
    };

    /**
     * Process primary phone interactions
     */
    $('.make-primary-phone').live('click', makePrimaryPhone);
    $('.remove-primary-phone').live('click', removePrimaryPhone);

    /**
     * Address Template related
     */
    addressTemplateRow = $('div#addlAddressTemplate:hidden');
    addressTemplateRow.find('input, select').attr('disabled', 'disabled');
    addlAddressRowCount = 0 + $('div#addlAddressRows div.addressRow').length; // prevent index conflicts
    $('#add-address').click(function () {
        var newAddressRow = addressTemplateRow.clone().appendTo('div#addlAddressRows');
        newAddressRow.attr('id', 'addlAddress-' + addlAddressRowCount);

        // Template for the row count added to each name field
        var addressRowCountString = '[' + addlAddressRowCount + ']';

        // Configure name and id for batch mode
        var newContactAddressInput = newAddressRow.find('input#Addresses_0_address');
        newContactAddressInput.attr('name', newContactAddressInput.attr('name').replace('0', addlAddressRowCount));
        newContactAddressInput.attr('id', newContactAddressInput.attr('id').replace('0', addlAddressRowCount));

        var newContactCityInput = newAddressRow.find('input#Addresses_0_city');
        newContactCityInput.attr('name', newContactCityInput.attr('name').replace('0', addlAddressRowCount));
        newContactCityInput.attr('id', newContactCityInput.attr('id').replace('0', addlAddressRowCount));

        var newContactZipInput = newAddressRow.find('input#Addresses_0_zip');
        newContactZipInput.attr('name', newContactZipInput.attr('name').replace('0', addlAddressRowCount));
        newContactZipInput.attr('id', newContactZipInput.attr('id').replace('0', addlAddressRowCount));
        newContactZipInput.inputmask('99999');

        var newContactStateInput = newAddressRow.find('select#Addresses_0_state_id');
        newContactStateInput.attr('name', newContactStateInput.attr('name').replace('0', addlAddressRowCount));
        newContactStateInput.attr('id', newContactStateInput.attr('id').replace('0', addlAddressRowCount));

        var newAddressRowHiddenInputIsPrimary = newAddressRow.find('input#Addresses_0_isPrimary');
        newAddressRowHiddenInputIsPrimary.attr('name', newAddressRowHiddenInputIsPrimary.attr('name').replace('0', addlAddressRowCount));
        newAddressRowHiddenInputIsPrimary.attr('id', newAddressRowHiddenInputIsPrimary.attr('id').replace('0', addlAddressRowCount));

        var newAddressRowHiddenInputId = newAddressRow.find('input#Addresses_0_id');
        newAddressRowHiddenInputId.attr('name', newAddressRowHiddenInputId.attr('name').replace('0', addlAddressRowCount));
        newAddressRowHiddenInputId.attr('id', newAddressRowHiddenInputId.attr('id').replace('0', addlAddressRowCount));

        var newAddressRowHiddenInputRemove = newAddressRow.find('input#Addresses_0_remove');
        newAddressRowHiddenInputRemove.attr('name', newAddressRowHiddenInputRemove.attr('name').replace('0', addlAddressRowCount));
        newAddressRowHiddenInputRemove.attr('id', newAddressRowHiddenInputRemove.attr('id').replace('0', addlAddressRowCount));

        newAddressRow.find('input, select').attr('disabled', false);
        newAddressRow.hide().show('slow');

        ++addlAddressRowCount;
    });

    $('.remove-address').live('click', function () {
        var removeRowAddress = $(this).parents('div.addressRow:first');
        removeRowAddress.find('input[name*="remove"]').val(1);
        removeRowAddress.hide('slow', function () {
            if ($(this).find('input[name*="id"]').val()) {
                $(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
            }
            else {
                $(this).remove();
            }
        });
    });

    // Unset a contact's e-mail as the primary e-mail
    var removePrimaryAddress = function () {
        $(this).html($(this).html().replace('Remove', 'Make'));
        $(this).find('em').removeClass('i_stm_star').addClass('i_stm_star_4');
        $(this).removeClass('remove-primary-address').addClass('make-primary-address');
        $('div#addlAddressRows input[name*="Primary"]').val(0); // reset all primary hidden fields
    };

    // Set a contact's e-mail as the primary e-mail address
    var makePrimaryAddress = function () {
        $('#addlAddressRows').find('.remove-primary-address').each(removePrimaryAddress);
        $(this).html($(this).html().replace('Make', 'Remove'));
        $(this).find('em').removeClass('i_stm_star_4').addClass('i_stm_star');
        $(this).removeClass('make-primary-address').addClass('remove-primary-address');
        $(this).parents('div.addressRow').find('input[name*="Primary"]').val(1); // mark this row as a primary e-mail
    };

    /**
     * Process primary address interactions
     */
    $('.make-primary-address').live('click', makePrimaryAddress);
    $('.remove-primary-address').live('click', removePrimaryAddress);

    /**
     * Profile Template related
     */
    profileTemplateRow = $('div#addlProfileTemplate:hidden');
    addlProfileRowCount = 0;
    $('#add-profile').click(function () {
        // Clone the template row and update the row id
        ++addlProfileRowCount;
        var newProfileRow = profileTemplateRow.clone().appendTo('div#addlProfileRows');
        var newProfileRowInput = newProfileRow.find('input');
        newProfileRow.attr('id', 'addlProfile-' + addlProfileRowCount);

        // Configure name and id for batch mode
        newProfileRowInput.attr('name', newProfileRowInput.attr('name') + '[' + addlProfileRowCount + ']');
        newProfileRowInput.attr('id', newProfileRowInput.attr('id') + '_' + addlProfileRowCount);
        newProfileRowInput.attr('class', newProfileRowInput.attr('class'));

        // Show the new row
        newProfileRow.hide().show('slow');
    });

    $('.remove-profile').live('click', function () {
        $(this).parents('div.profileRow:first').hide('slow', function () {
            $(this).remove();
        });
    });
});
