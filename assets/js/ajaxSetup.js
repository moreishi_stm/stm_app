// Setup global AJAX error handling
$.ajaxSetup({
    error: function (x, status, error) {
        if (x.status == 403) {
            alert("Sorry, your session has expired. Please login again to continue.");
            window.location.href ="/admin";
        }
    }
});

// Enable mobile menu expand
$('#mobile-menu-expand').click(function() {
    if ($("#mobile ul").is(":hidden") ) {
        $("#mobile ul").slideDown("slow");
    } else {
        $("#mobile ul").slideUp();
    }
});

// Top nav search
(function(){

    // Helper function to execute search
    var search = function()
    {
        // Make AJAX call for search results
        $.post('/admin/search/results', {
            query: $('#query').val()
        }, function(results) {

            // Parse template with results
            var template = Handlebars.compile($('#template-result').html());
            var html = template({
                results: results
            });

            // Populate results and slide down
            $('#search-results').html(html);
            $('#search-results').show();
        });
    }


    // Bind keyup eevnt
    $('#search-form input[type="text"]').keyup(function() {
        clearTimeout($.data(this, 'timer'));
        var wait = setTimeout(search, 300);
        $(this).data('timer', wait);
    });

    // Bind search button functionality
    $('#search-form form').submit(function(e) {

        // Prevent default event facade
        e.preventDefault();

        // Run the search
        search();
    });

    // Hide the search container when we click elsewhere
    $('html').click(function() {
        $('#search-results').hide();
    });

    $('#autocomplete-search').click(function(e) {
        e.stopPropagation();
    });

    $('#query').focus(function(e){
        $('#search-results').show();
    });
}());

var stmLoadingGif = {
    show: function() {
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
    },
    hide: function() {
        $("div.loading-container.loading").remove();
    }
};

/**
 * STM modal
 *
 * @type {{width: number, height: number, windowResizeCallback: Function, create: Function, destroy: Function}}
 */
var stmModal = {

    // Width and height for modal
    width: 600,
    height: 550,

    // Modal template
    template: '' +
        '<div class="stm-modal-overlay"></div>' +
        '<div class="stm-modal" style="width: {{width}}; height: {{height}}; top: {{top}}; left: {{left}};"{{#if modalId}} id="{{modalId}}"{{/if}}>' +
            '<div class="stm-modal-header">' +
                '<a href="javascript:void(0)" class="stm-modal-close-x fa fa-times"></a>' +
                '<div class="stm-modal-title">{{title}}</div>' +
            '</div>' +
            '<div class="stm-modal-content">{{{content}}}</div>' +
        '</div>' +
    '',

    // Handler to be registered on modal creation and de-registered when destroyed
    windowResizeCallback: function(e) {

        // Calculate new left and top position
        var left = parseInt(($('body').innerWidth() / 2) - (stmModal.width / 2));
        var top = (($(window).height() - $('.stm-modal').height()) / 2) + document.body.scrollTop;

        if($('.stm-modal').height() > $(window).height()) {
            top = document.body.scrollTop;
        }
        // Update top and left positions
        $('.stm-modal').css({
            left: left,
            top: top
        });
    },

    // Creates a new modal
    create: function(options) {

        // We have to destroy, before we can create
        stmModal.destroy();

        // Add in custom width / height options
        stmModal.width = options.width ? options.width : stmModal.width;
        stmModal.height = options.height ? options.height : stmModal.height;

        // Top and left positioning
        var left = parseInt((document.documentElement.clientWidth / 2) - (stmModal.width / 2));

        // Compile content
        var contentTemplate = Handlebars.compile(options.content ? options.content : '');
        var contentHtml = contentTemplate(options.contentVariables ? options.contentVariables : {});

        // Variables being passed in to render w/ handlebars
        var handlebarsVariables = {
            title: options.title ? options.title : '',
            content: contentHtml,
            width: stmModal.width + 'px',
            //height: stmModal.height + 'px',
            height: 'auto',
            left: left + 'px',
            top: '20px'
        };

        // Add in a modal ID if we had that
        if(options.modalId) {
            handlebarsVariables.modalId = options.modalId;
        }

        // Add in misc vars if we had them
        if(options.handlebarsVariables) {
            for(var i in options.handlebarsVariables) {
                handlebarsVariables[i] = options.handlebarsVariables[i];
            }
        }

        // Compile template
        //var template = Handlebars.compile($('#template-modal').html());
        var template = Handlebars.compile(stmModal.template);
        var html = template(handlebarsVariables);

        // Create element
        var ele = document.createElement('div');
        ele.innerHTML = html;

        // Ensure body has 100% height
        document.body.style.height = '100%';

        // Append elements to body
        while(ele.children.length > 0) {
            document.body.appendChild(ele.children[0]);
        }

        // Bind window resize event
        $(window).on('resize orientationChange', stmModal.windowResizeCallback);

        // Initialize chosen plugin on all modal things
        // checks to see if chosen is a valid function
        if (jQuery().chosen != undefined) {
            // safe to use the function
            $(".stm-modal .chzn-select").chosen();
        }

        // update height of overlay
        $(".stm-modal-overlay").css('height', $(document).height());

        // position modal vertical middle
        var top = (($(window).height() - $('.stm-modal').height()) / 2) + document.body.scrollTop;

        // if modal is larger than window, position to top of window
        if($('.stm-modal').height() > $(window).height()) {
            top = document.body.scrollTop;
        }

        $('.stm-modal').css({ top: top });

        // Bind ajax form callback
        $('.stm-modal form').ajaxForm({

            // If we have a file upload field in our form, then we need to enable iframe support
            iframe: $('.stm-modal form input[type="file"]').length ? true : false,

            // Stuff to do right before we submit
            beforeSubmit: function(data, form) {

                // Execute before submit method if we have it, if it returns false then stop
                if(options.beforeSubmit) {
                    if(options.beforeSubmit(data, form) === false) {
                        return false;
                    }
                }

                // Remove errors
                $(form).find('.error').removeClass('error');
                $(form).find('.errorMessage').remove();
                //$(form).find('.errorMessage').removeClass('errorMessage');

                // Show loader
                stmLoadingGif.show();
            },

            // Complete event, fires when ajax call is over with regardless of succession
            complete: function(e) {

                // Remove loading container
                stmLoadingGif.hide();
            },

            // Success handler, when an HTTP 200 is returned
            success: function(r) {

                // Fish for errors
                try {
                    var response = jQuery.parseJSON(r);
                }
                catch(e) {
                    console.log('No response detected in response!');
                    console.log(e);
                    console.log(r);
                    stmModal.destroy();

                    if(options.successCallback) {
                        options.successCallback();
                    }

                    return;
                }

                if(!response) {
                    console.log('No errors detected in response!');

                    stmModal.destroy();
                    if(options.successCallback) {
                        options.successCallback();
                    }

                    return;
                }

                // Make sure we actually have errors before continuing
                if((response.status && response.status == 'success') || jQuery.isEmptyObject(response)) {

                    stmModal.destroy();
                    if(options.successCallback) {
                        options.successCallback(response);
                    }

                    return;
                }

                // Iterate elements with errors
                var i, j;
                for (i in response) {

                    if (response[i]) {

                        // Mark the element as problematic and add each relative error for below it
                        $('#' + i).parent().addClass('error');
                        for (j = 0; j < response[i].length; j++) {
                            $('<div class="errorMessage">' + response[i][j] + '</div>').insertAfter('#' + i);
                        }
                    }
                }
            },

            // Error handler, when a non HTTP 2xx is returned
            error: function(e) {
                console.log('Error!');
                console.log(e);
            }
        });

        // Load data if URL is present
        if(options.loadFormDataUrl) {

            // Load data via AJAX
            $.ajax(options.loadFormDataUrl, {

                // Use HTTP Post instead of GET, also hint that we have a JSON response (the guess always gets it wrong...)
                method: 'POST',
                dataType: 'json',

                // Show loader before we make the call
                beforeSend: function() {
                    stmLoadingGif.show();
                },

                // Hide loader before we make the call
                complete: function() {
                    stmLoadingGif.hide();
                },

                // Auto-populate the form once we've had a successful result
                success: function(data) {

                    // If we have a custom data mapping, use it instead of the default auto-mapping
                    var formData = data;
                    if(options.loadFormDataMappings) {

                        // Clear existing data and populate with the mapping
                        formData = {};
                        for(var i in options.loadFormDataMappings) {
                            formData[i] = data[options.loadFormDataMappings[i]];
                        }
                    }

                    // Populate the form
                    $('.stm-modal form').populate(formData);
                },

                // Handle unknown errors that may have occurred
                error: function() {
                    alert('A server error has occurred, please try again later or contact support!');
                }
            });
        }

        // Overwrite action URL for form
        if(options.saveFormDataUrl) {
            $('.stm-modal form').attr('action', options.saveFormDataUrl);
        }
    },

    // Destroys modal
    destroy: function() {

        // Destroy TinyMCE
        if(typeof tinymce != 'undefined') {
            tinymce.remove(".stm-modal textarea");
        }

        // Remove the elements
        $('.stm-modal-overlay').remove();
        $('.stm-modal').remove();

        // Un-Bind window resize event
        $(window).off('resize orientationChange', stmModal.windowResizeCallback);
    }
};

// Bind close delegate for modal
$('body').on('click', '.stm-modal-btn-close, .stm-modal-close-x', function() {
    stmModal.destroy();
});

// variable for ipad/iphone for click events to utilize
var ua = navigator.userAgent,
    clickTouchEvent = (ua.match(/iPad/i) || ua.match(/iPhone/i)) ? "touchstart" : "click";