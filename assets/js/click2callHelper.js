// Bind click2call helper
$('body').delegate('.click2callHelper', 'click', function(e) {

    // Prevent default behavior
    e.preventDefault();

    // Get phone number without extra junk
    if($(this).attr("data-phone") != undefined) {
        var phone = $(this).attr("data-phone").replace(/\D/g,'');
    }

    // Popup new window
    window.open('http://' + window.location.host + '/admin/phone?to=' + phone + '&autoDial=1','1446499057043','width=700,height=550,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
});