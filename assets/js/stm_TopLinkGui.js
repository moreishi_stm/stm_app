$(function() {
	/**
	 * Settings - Top Link Template related
	 */
	topLinkTemplateRow = $('div#addlTopLinkTemplate:hidden');
	addlTopLinkRowCount = 0 + $('div#addlTopLinkRows div.topLinkRow').length; // prevent index conflicts
	$('#add-top-link').live('click', function() {

		var newTopLinkRow                 = topLinkTemplateRow.clone().appendTo('div#addlTopLinkRows');
		// Clone the template row and update the row id
		var newTopLinkRowTextInput        = newTopLinkRow.find('input#topLink_0_label');
		var newTopLinkRowLinkInput        = newTopLinkRow.find('input#topLink_0_url');
		var newTopLinkRowSortOrderInput   = newTopLinkRow.find('input#topLink_0_sort_order');
        var newTopLinkRowWindowInput      = newTopLinkRow.find('select#topLink_0_window');
		newTopLinkRow.attr('id', 'addlTopLink-' + addlTopLinkRowCount);

		// Configure name and id for batch mode
		newTopLinkRowTextInput.attr('name', 'topLink['+addlTopLinkRowCount+'][label]');
		newTopLinkRowTextInput.attr('id', 'topLink_'+addlTopLinkRowCount+'_label');
		newTopLinkRowTextInput.attr('disabled', false);

		newTopLinkRowLinkInput.attr('name', 'topLink['+addlTopLinkRowCount+'][url]');
		newTopLinkRowLinkInput.attr('id', 'topLink_'+addlTopLinkRowCount+'_url');
		newTopLinkRowLinkInput.attr('disabled', false);

		newTopLinkRowSortOrderInput.attr('name', 'topLink['+addlTopLinkRowCount+'][sort_order]');
		newTopLinkRowSortOrderInput.attr('id', 'topLink_'+addlTopLinkRowCount+'_sort_order');
		newTopLinkRowSortOrderInput.attr('disabled', false);

        newTopLinkRowWindowInput.attr('name', 'topLink['+addlTopLinkRowCount+'][window]');
        newTopLinkRowWindowInput.attr('id', 'topLink_'+addlTopLinkRowCount+'_window');
        newTopLinkRowWindowInput.attr('disabled', false);

        // Show the new row
		newTopLinkRow.hide().show('slow');

		++addlTopLinkRowCount;
	});

	// Handles removing a Top Link entry on the form
	$('.remove-top-link').live('click', function() {
		$(this).parents('div.topLinkRow:first').find('input[name*="remove"]').val(1);
		$(this).parents('div.topLinkRow:first').hide('slow', function() {
			$(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
		});
	});

	// // Handles removing a Top Link entry on the form
	// $('.remove-top-link').live('click', function() {
	// 	$(this).parents('div.topLinkRow:first').find('input[name*="remove"]').val(1);
	// 	$(this).parents('div.topLinkRow:first').hide('slow', function() {
	// 		$(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
	// 	});
	// });
});
