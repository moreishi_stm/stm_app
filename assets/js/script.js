$(document).ready(function() {

	//for Caching
	var $content = $('#content');

		/*----------------------------------------------------------------------*/
		/* preload images
		/*----------------------------------------------------------------------*/

		//$.preload();

		/*----------------------------------------------------------------------*/
		/* Widgets
		/*----------------------------------------------------------------------*/

		$content.find('div.widgets').wl_Widget();
		/*----------------------------------------------------------------------*/
		/* All Form Plugins
		/*----------------------------------------------------------------------*/

		//Integers and decimals
		$content.find('input[type=number].integer').wl_Number();
		$content.find('input[type=number].decimal').wl_Number({decimals:2,step:0.5});

		//Date and Time fields
		$content.find('input.date, div.date').wl_Date();
		$content.find('input.time').wl_Time();

		/*Autocompletes (source is required)
		$content.find('input.autocomplete').wl_Autocomplete({
			source: ["ActionScript","AppleScript","Asp","BASIC","C","C++","Clojure","COBOL","ColdFusion","Erlang","Fortran","Groovy","Haskell","Java","JavaScript","Lisp","Perl","PHP","Python","Ruby","Scala","Scheme"]
		});
		*/
		//Elastic textareas (autogrow)
		$content.find('textarea[data-autogrow]').elastic();
		//WYSIWYG Editor
		//$content.find('textarea.html').wl_Editor();

		//Validation
		//$content.find('input[data-regex]').wl_Valid();
		//$content.find('input[type=email]').wl_Mail();
		//$content.find('input[type=url]').wl_URL();

		//File Upload
		//$content.find('input[type=file]').wl_File();

		//Password and Color
		//$content.find('input[type=password]').wl_Password();
		//$content.find('input.color').wl_Color();

		//Sliders
		//$content.find('div.slider').wl_Slider();

		//Multiselects
		//$content.find('select[multiple]').wl_Multiselect();

		//The Form is called for the demo with options on line 497
		//$content.find('form').wl_Form();

		/*----------------------------------------------------------------------*/
		/* Alert boxes
		/*----------------------------------------------------------------------*/

		$content.find('div.alert').wl_Alert();

		/*----------------------------------------------------------------------*/
		/* Breadcrumb
		/*----------------------------------------------------------------------*/

		//$content.find('ul.breadcrumb').wl_Breadcrumb();

		/*----------------------------------------------------------------------*/
		/* datatable plugin
		/*----------------------------------------------------------------------*/

		$content.find("table.datatable").dataTable({
			"sPaginationType": "full_numbers"
		});

		/*----------------------------------------------------------------------*/
		/* uniform plugin && checkbox plugin (since 1.3.2)
		/* uniform plugin causes some issues on checkboxes and radios
		/*----------------------------------------------------------------------*/

		$("select, input[type=file]").not('select[multiple]').uniform();
		$('input:checkbox, input:radio').checkbox();

		/*----------------------------------------------------------------------*/
		/* Charts
		/*----------------------------------------------------------------------

		$content.find('table.chart').wl_Chart({
			onClick: function(value, legend, label, id){
				$.msg("value is "+value+" from "+legend+" at "+label+" ("+id+")",{header:'Custom Callback'});
			}
		});

		*/

		/*----------------------------------------------------------------------*/
		/* Calendar (read http://arshaw.com/fullcalendar/docs/ for more info!)
		/*----------------------------------------------------------------------*/
		/*
		$content.find('div.calendar').wl_Calendar({
			eventSources: [
					{
						url: 'http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic'
					},{
						events: [ // put the array in the `events` property
							{
								title  : 'Fixed Event',
								start  : '2012-02-01'
							},
							{
								title  : 'long fixed Event',
								start  : '2012-02-06',
								end    : '2012-02-14'
							}
						],
						color: '#f0a8a8',     // an option!
						textColor: '#ffffff' // an option!
					},{
						events: [ // put the array in the `events` property
							{
								title  : 'Editable',
								start  : '2012-02-09 12:30:00'
							}
						],
						editable:true,
						color: '#a2e8a2',     // an option!
						textColor: '#ffffff' // an option!
					}
					// any other event sources...

				]
			});
		 */

		/*----------------------------------------------------------------------*/
		/* Tipsy Tooltip
		/*----------------------------------------------------------------------*/
		$content.find('input[title]').tipsy({
			gravity: function(){return ($(this).data('tooltip-gravity') || config.tooltip.gravity); },
			fade: config.tooltip.fade,
			opacity: config.tooltip.opacity,
			color: config.tooltip.color,
			offset: config.tooltip.offset
		});

		/*----------------------------------------------------------------------*/
		/* Accordions
		/*----------------------------------------------------------------------*/

		$content.find('div.accordion').accordion({
				collapsible:true,
				autoHeight:false
		});

		/*----------------------------------------------------------------------*/
		/* Tabs
		/*----------------------------------------------------------------------*/

		$content.find('div.tab').tabs({
				fx: {
					opacity: 'toggle',
					duration: 'fast'
				}
		});

		/*----------------------------------------------------------------------*/
		/* Navigation Stuff
		/*----------------------------------------------------------------------*/


		//Top Pageoptions - - CLee copied to functions
		$('#wl_config').click(function(){
			var $pageoptions = $('#pageoptions');
			if($pageoptions.height() < 200){
				$pageoptions.animate({'height':200});
				$(this).addClass('active');
			}else{
				$pageoptions.animate({'height':20});
				$(this).removeClass('active');
			}
			return false;
		});


		//Header navigation for smaller screens
		var $headernav = $('ul#headernav');

		$headernav.bind('click',function(){
			//if(window.innerWidth > 800) return false;
			var ul = $headernav.find('ul').eq(0);
			(ul.is(':hidden')) ? ul.addClass('shown') : ul.removeClass('shown');
		});

		$headernav.find('ul > li').bind('click',function(event){
			event.stopPropagation();
			var children = $(this).children('ul');

			if(children.length){
				(children.is(':hidden')) ? children.addClass('shown') : children.removeClass('shown');
				return false;
			}
		});

		//Search Field Stuff
		var $searchform = $('#searchform'),
			$searchfield = $('#search'),
			livesearch = true;

		$searchfield
			.bind({
				'focus.wl': function(){
		   			$searchfield.select().parent().animate({width: '150px'},100);
				},
				'blur.wl': function(){
	   				$searchfield.parent().animate({width: '90px'},100);
					if(livesearch)$searchboxresult.fadeOut();
				}
		});

		//livesearch is active
		if(livesearch){

			$searchfield.attr('placeholder','Live Search');

			var $searchboxresult = $('#searchboxresult'),
				searchdelay = 800,  //delay of search in milliseconds (prevent flooding)
				searchminimum = 3, //minimum of letter when search should start
				searchtimeout, searchterm, resulttitle;

			//insert the container if missing
			if(!$searchboxresult.length) $searchboxresult = $('<div id="searchboxresult"></div>').insertAfter('#searchbox');

			//bind the key event
			$searchfield
				.bind({
					'keyup.wl': function(event){

						//do nothing if the term hasn't change
						if(searchterm == $searchfield.val()) return false;

						//the current search value
						searchterm = $searchfield.val();

						//clear the old timeout and start a new one
						clearTimeout(searchtimeout);

						//stop if term is too short
						if(searchterm.length < searchminimum){
							$searchboxresult.fadeOut();
							$searchfield.removeClass('load');
							return false;
						}


						searchtimeout = setTimeout(function(){
							$searchfield.addClass('load');

							//get results with ajax
							$.post("search.php", { term: searchterm },
							 function(data){
								$searchfield.removeClass('load');
								//search value don't has to be too short
								if(searchterm.length < searchminimum){
									$searchboxresult.fadeOut();
									return false;
								}

								var count = data.length, html = '';

								//we have results
								if(count){
									for(var i = 0; i< count; i++){
										resulttitle = '';
										if(data[i].text.length > 105){
											resulttitle = 'title="'+data[i].text+'"';
											data[i].text = $.trim(data[i].text.substr(0,100))+'&hellip;';
										}
										html += '<li><a href="'+data[i].href+'" '+resulttitle+'>';
										if(data[i].img) html += '<img src="'+data[i].img+'" width="50">';
										html += ''+data[i].text+'</a></li>';
									}
								//no result to this search term
								}else{
									html += '<li><a class="noresult">Nothing found for<br>"'+searchterm+'"</a></li>';
								}

								//insert it and show
								$searchboxresult.html(html).fadeIn();

							}, "json");

						},searchdelay);
					}
				});
		}

		$searchform
			.bind('submit.wl',function(){
				//do something on submit
				var query = $searchfield.val();
			});




		//Main Navigation
		var $nav = $('#nav');

		$nav.delegate('li','click.wl', function(event){
			var _this = $(this),
				_parent = _this.parent(),
				a = _parent.find('a');
			_parent.find('ul').slideUp('fast');
			a.removeClass('active');
			_this.find('ul:hidden').slideDown('fast');
			_this.find('a').eq(0).addClass('active');
			event.stopPropagation();
		});

		/*----------------------------------------------------------------------*/
		/* Helpers
		/*----------------------------------------------------------------------*/
		//placeholder_init();

		//helper for links???
		var loc = location.pathname.replace(/\/([^.]+)\//g,'');
		var current = $nav.find('a[href="'+loc+'"]');

		if(current.parent().parent().is('#nav')){
			current.addClass('active');
		}else{
			current.parent().parent().parent().find('a').eq(0).addClass('active').next().show();
			current.addClass('active');
		}
});