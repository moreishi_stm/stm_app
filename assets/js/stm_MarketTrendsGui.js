$(function() {
	/**
	 * Market Trends
	 */

	$('input[name*="date"]').datepicker();

	marketDataTemplateRow  = $('div#addlMarketDataTemplate:hidden');
	addlMarketDataRowCount = 0 + $('div#addlMarketDataRows div.marketDataRow').length; // prevent index conflicts
	$('div#addlMarketDataRows input[name*="date"]:visible').removeClass('hasDatepicker').datepicker();

	$('#add-market-data').live('click', function() {

		var newMarketDataRow                 = marketDataTemplateRow.clone().appendTo('div#addlMarketDataRows');

		// Clone the template row and update the row id
		var newMarketDataRowDateInput        = newMarketDataRow.find('#MarketTrendData_0_date');
		var newMarketDataRowValueInput        = newMarketDataRow.find('input#MarketTrendData_0_value');
		newMarketDataRow.attr('id', 'addlMarketData-' + addlMarketDataRowCount);

		// Configure name and id for batch mode
		newMarketDataRowDateInput.attr('name', 'MarketTrendData['+addlMarketDataRowCount+'][date]');
		newMarketDataRowDateInput.attr('id', 'MarketTrendData_'+addlMarketDataRowCount+'date');
		newMarketDataRowDateInput.attr('disabled', false);

		// Need to remove date picker css class and re-init the picker
		newMarketDataRowDateInput.removeClass('hasDatepicker').datepicker();

		newMarketDataRowValueInput.attr('name', 'MarketTrendData['+addlMarketDataRowCount+'][value]');
		newMarketDataRowValueInput.attr('id', 'MarketTrendData'+addlMarketDataRowCount+'value');
		newMarketDataRowValueInput.attr('disabled', false);

		// Remove unused id row
		newMarketDataRow.find('#MarketTrendData_0_id').remove();

		// Show the new row
		newMarketDataRow.hide().show('slow');

		++addlMarketDataRowCount;
	});

	// Handles removing a new row entry on the form
	$('.remove-market-data').live('click', function() {
		$(this).parents('div.marketDataRow:first').find('input[name*="remove"]').val(1);
		$(this).parents('div.marketDataRow:first').hide('slow', function() {
			$(this).find('input:not(input[name*="remove"], input[name*="id"])').attr('disabled', 'disabled');
		});
	});
});
