<?php
class FeaturedHomesForSaleWidget extends BaseWidget
{
	public function init()
    {
		parent::init();

		//Yii::app()->clientScript->registerCssFile($this->cssAssetsUrl.DS.'featured_home_for_sale.css');
        Yii::app()->clientScript->registerCss('featuredHomesForSaleWidgetCss',<<<CSS

            @media (max-width: 1920px) {
                .featuredPostContainer .home-list-photo {
                    height: 430px;
                }
            }
            @media (max-width: 1280px) {
                .featuredPostContainer .home-list-photo {
                    height: 350px;
                }
            }
            @media (max-width: 980px) {
                .featuredPostContainer .home-list-photo {
                    height: 300px;
                }
            }
            @media (max-width: 800px) {
                .featuredPostContainer .home-list-photo {
                    height: 255px;
                }
            }
            @media (max-width: 786px) {
                .featuredPostContainer .home-list-photo {
                    height: 240px;
                }
            }
            @media (max-width: 360px) {
                .featuredPostContainer .home-list-photo {
                    height: auto;
                }
            }
CSS
        );
	}
}