<?php
$data = $this->data;
?>
<div class="product">
    <div class="description">
        <div class="add-fav-container">
            <a data-placement="right" class="add-fav<?=($data->isSaved) ? " active" : "";?>" data-listingid="<?=$data->listing_id?>" data-mbid="<?=$data->getMlsBoardId()?>" data-contact-id="<?=Yii::app()->user->id; ?>">
                <i class="glyphicon glyphicon-heart"></i>
            </a>
        </div>
        <div class="grid-description">
            <div class="image">
                <div class="quickview">
                    <a href="<?php echo $data->getUrl($relative=true); ?>" title="Quick View" class="btn btn-xs  btn-quickview"<?php /*data-target="#product-details-modal" data-toggle="modal"*/ ?>> View Photos & Details</a>
                </div>
                <a href="<?php echo $data->getUrl($relative=true); ?>">
                    <img class="img-responsive home-list-photo" alt="img" src="<?php echo (!empty($data->firstPhotoUrl)) ? $data->firstPhotoUrl : $data->getPhotoUrl($photoNumber = 1); ?>" />
                </a>

                <?php /* <div class="promotion"><span class="new-product"> NEW</span> <span class="discount">15% OFF</span></div> */ ?>
            </div>
            <div class="address">
                <?=$data->formatMlsAddress($data, array('streetOnly'=>true))?>
            </div>
            <div class="neighborhood">
                <?=ucwords(strtolower($data->common_subdivision))?>
            </div>
            <div class="specs-container">
                <?php if ($data->mls_property_type_id == MlsPropertyTypes::LAND_ID): ?>
                    <div class="specs beds"><span>Vacant Land</span></div>
                    <div class="specs price"><span><?=Yii::app()->format->formatDollars($data->price)?></span></div>
                <? else: ?>
                    <div class="specs beds"><span><?=($data->bedrooms) ? $data->bedrooms." BD": ""?></span></div>
                    <div class="specs bath"><span><?=($data->baths_total) ? $data->baths_total." BA": ""?></span></div>
                    <div class="specs price"><span><?=Yii::app()->format->formatDollars($data->price)?></span></div>
                <? endif; ?>
            </div>
        </div>
        <div class="disclosure">
            <?/*if(strpos($_SERVER['SERVER_NAME'], 'myanchorageproperties') !== false): ?>
                <p style="font-size: 12px;">
                    <img src="http://mlsimages.seizethemarket.com/ak_ak/ak_ak_logo.png" align="right" alt="" style="width: 35px; margin-right: 13px;"/>
                    <?=ucwords(strtolower($data->getListingOfficeName()))?>
                </p>
            <? elseif(strpos($_SERVER['SERVER_NAME'], 'findhomesinpdx') !== false):?>
                <p style="font-size: 12px;">
                    <img src="http://mlsimages.seizethemarket.com/or_rmls/or_rmls_logo.png" align="right" alt="" style="width: 35px; margin-right: 13px; margin-top: 4px;"/>
                    <?=ucwords(strtolower($data->getListingOfficeName()))?>
                </p>
            <? endif;*/?>
            <?
            $mlsDisclosure = Yii::app()->user->board->website_disclosure_thumbnail;
            if($mlsDisclosure): ?>
            <p style="font-size: 12px;">

                <? eval('echo '.$mlsDisclosure);
            endif;
            ?>
            </p>
        </div>
    </div>
</div>