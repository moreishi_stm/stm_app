<div class="row featuredPostContainer globalPadding">

	<h3 class="section-title style2 text-center"><span> TOP AREAS & NEIGHBORHOODS </span></h3>

	<div class="featuredImageLook3">
		<div class="col-md-4 col-sm-6 col-xs-6 col-xs-min-12">
			<div class="inner">
				<div class="box-content-overly box-content-overly-white">
					<div class="box-text-table">
						<div class="box-text-cell ">
							<div class="box-text-cell-inner dark">
								<h1 class="uppercase">Handbags</h1>

								<p>Veggies es bonus vobis, proinde vos postulo essum magis
									kohlrabi welsh onion daikon amaranth.</p>
								<hr class="submini">
								<a class="btn btn-inverse"> SHOP NOW</a></div>
						</div>
					</div>
				</div>
				<!--/.box-content-overly -->
				<div class="img-title"> Handbags</div>
				<a class="img-block" href="#"> <img class="img-responsive" src="/images/tempImages/images/look3/2.jpg" alt="img"></a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 col-xs-min-12">
			<div class="inner">
				<div class="box-content-overly box-content-overly-white">
					<div class="box-text-table">
						<div class="box-text-cell ">
							<div class="box-text-cell-inner dark">
								<h1 class="uppercase"> Designers</h1>

								<p>Veggies es bonus vobis, proinde vos postulo essum magis
									kohlrabi welsh onion daikon amaranth.</p>
								<hr class="submini">
								<a class="btn btn-inverse"> SHOP NOW</a></div>
						</div>
					</div>
				</div>
				<!--/.box-content-overly -->
				<div class="img-title"> Designers</div>
				<a class="img-block" href="#"> <img class="img-responsive" src="/images/tempImages/images/look3/1.jpg" alt="img"></a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 col-xs-min-12">
			<div class="inner">
				<div class="box-content-overly box-content-overly-white">
					<div class="box-text-table">
						<div class="box-text-cell ">
							<div class="box-text-cell-inner dark">
								<h1 class="uppercase">Sale Offers</h1>

								<p>Veggies es bonus vobis, proinde vos postulo essum magis
									kohlrabi welsh onion daikon amaranth.</p>
								<hr class="submini">
								<a class="btn btn-inverse"> SHOP NOW</a></div>
						</div>
					</div>
				</div>
				<!--/.box-content-overly -->
				<div class="img-title"> Sale Offers</div>
				<a class="img-block" href="#"> <img class="img-responsive" src="/images/tempImages/images/look3/3.jpg" alt="img"></a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 col-xs-min-12">
			<div class="inner">
				<div class="box-content-overly box-content-overly-white">
					<div class="box-text-table">
						<div class="box-text-cell ">
							<div class="box-text-cell-inner dark">
								<h1 class="uppercase">T Collections</h1>

								<p>Veggies es bonus vobis, proinde vos postulo essum magis
									kohlrabi welsh onion daikon amaranth.</p>
								<hr class="submini">
								<a class="btn btn-inverse"> SHOP NOW</a></div>
						</div>
					</div>
				</div>
				<!--/.box-content-overly -->
				<div class="img-title">T Collections</div>
				<a class="img-block" href="#"> <img class="img-responsive" src="/images/tempImages/images/look3/4.jpg" alt="img"></a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 col-xs-min-12">
			<div class="inner">
				<div class="box-content-overly box-content-overly-white">
					<div class="box-text-table">
						<div class="box-text-cell ">
							<div class="box-text-cell-inner dark">
								<h1 class="uppercase">Latest Trends </h1>

								<p>Veggies es bonus vobis, proinde vos postulo essum magis
									kohlrabi welsh onion daikon amaranth.</p>
								<hr class="submini">
								<a class="btn btn-inverse"> SHOP NOW</a></div>
						</div>
					</div>
				</div>
				<!--/.box-content-overly -->
				<div class="img-title"> Latest Trends</div>
				<a class="img-block" href="#"> <img class="img-responsive" src="/images/tempImages/images/look3/5.jpg" alt="img"></a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 col-xs-min-12">
			<div class="inner">
				<div class="box-content-overly box-content-overly-white">
					<div class="box-text-table">
						<div class="box-text-cell ">
							<div class="box-text-cell-inner dark">
								<h1 class="uppercase">Dresses</h1>

								<p>Veggies es bonus vobis, proinde vos postulo essum magis
									kohlrabi welsh onion daikon amaranth.</p>
								<hr class="submini">
								<a class="btn btn-inverse"> SHOP NOW</a></div>
						</div>
					</div>
				</div>
				<!--/.box-content-overly -->
				<div class="img-title"> Dresses</div>
				<a class="img-block" href="#"> <img class="img-responsive" src="/images/tempImages/images/look3/6.jpg" alt="img"></a>
			</div>
		</div>
	</div>
	<!--/.featuredImageLook3-->
</div>