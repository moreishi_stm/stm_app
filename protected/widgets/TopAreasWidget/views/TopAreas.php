<div class="inner<?=" ".$this->jsHandler;?>" data-href="<?=$this->baseUrl.$data->url; ?>">
	<div class="box-content-overly box-content-overly-white">
		<div class="box-text-table">
			<div class="box-text-cell ">
				<div class="box-text-cell-inner dark">
					<h1 class="uppercase"><?=$data->name;?></h1>
					<p><?=$data->title;?></p>
					<hr class="submini">
					<a class="btn btn-inverse" href="<?=$this->baseUrl.$data->url; ?>">View Details</a>
				</div>
			</div>
		</div>
	</div>
	<!--/.box-content-overly -->
	<div class="img-title"><?=$data->name;?></div>
	<a class="img-block" href="#"> <img class="img-responsive" src="<?=$this->imageUrl;?>" alt="img"></a>
</div>