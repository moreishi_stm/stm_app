<?php
class TopAreasWidget extends BaseWidget {
	
	public $baseUrl = NULL;
	public $imageUrl = NULL;
	public $baseImageUrl = NULL;

    public $jsHandler = '';

	public function init() {
		parent::init();
		
		if(empty($this->baseImageUrl)) {
			$this->baseImageUrl = ((YII_DEBUG) ? "http://www.seizethemarket.local/images/TopAreasWidget/" : "http://cdn.seizethemarket.com/assets/standard2/images/TopAreasWidget/");
		}
		
		if(isset($this->data->image) && !empty($this->data->image)) {
			$this->imageUrl = $this->data->image->image_location;
		}
		
		if(empty($this->imageUrl)) {
			$this->imageUrl = $this->baseImageUrl."ta".$this->index.".jpg";
		}
		
		//Yii::app()->clientScript->registerCssFile($this->cssAssetsUrl.DS.'top_areas.css');
	}
	
	public function run() {
		$this->render($this->view_file, array('data' => $this->data, 'jsHandler' => $this->jsHandler));
	}
}