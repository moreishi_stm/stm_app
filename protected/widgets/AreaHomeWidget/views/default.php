<div class="product">
	<a data-placement="left" data-original-title="Add to Wishlist" data-toggle="tooltip"
	   class="add-fav tooltipHere">
		<i class="glyphicon glyphicon-heart"></i>
	</a>

	<div class="image">
		<div class="quickview">
			<a title="Quick View" class="btn btn-xs  btn-quickview"
			   data-target="#product-details-modal" data-toggle="modal"> Quick View </a>
		</div>
		<a href="#"><img class="img-responsive" alt="img" src="<?php echo (!empty($this->data->firstPhotoUrl))? $this->data->firstPhotoUrl : $this->data->getPhotoUrl($photoNumber=1); ?>"></a>

		<?php /*<div class="promotion"><span class="new-product"> NEW</span> <span class="discount">15% OFF</span></div>*/ ?>
	</div>
	<div class="description">
		<h4><a href="#"><?php if (!empty($this->data->common_subdivision)) : ?>
        <?php echo Yii::app()->format->formatProperCase($this->data->common_subdivision)?><br />
        <?php endif; ?>
        <?php echo Yii::app()->format->formatProperCase($this->data->city)?>, <?php echo $this->data->getStateShortName($lowercase=false).' '.substr($this->data->zip,0,5)?></a></h4>

		<div class="grid-description">
			<p><?php if($this->data->year_built):
						echo $this->data->year_built?> Yr Built<br />
				<?php endif; ?> 
					<?php if (!empty($this->data->bedrooms) and !empty($this->data->baths_total)) : ?>
					<?php echo $this->data->bedrooms?> BR / <?php echo $this->data->baths_total?> Baths<br />
				<?php endif; ?>
				<?php if(!empty($this->data->sq_feet)):?>
					<?php echo Yii::app()->format->formatNumber($this->data->sq_feet)?> SF<br />
				<?php endif; ?>
					<br />
				<?php if(!empty($this->data->sq_feet)):?>
					<?php echo '($'.$this->data->pricePerSf.'/SF)' ?><br />
				<?php endif; ?>
				<?php if($this->data->mls_property_type_id == MlsPropertyTypes::LAND_ID) {
					echo 'Vacant Land<br />';
				}?>
				<?php echo ($this->data->pool_yn)? '<span class="water">Pool</span><br />': '';?>
				<?php if(!empty($this->data->photo_count)): ?>
					<a href="<?php echo $this->data->getUrl($relative=true); ?>" class="photos"><?php echo $this->data->photo_count?> Photos</a>
				<?php endif; ?>

					<?php if($this->data->status_change_date>0 && Yii::app()->user->boardId != 2): //@todo: need to change the hard coding for ferraro ?>
					<?php echo Yii::app()->format->formatDays($this->data->status_change_date)?>
				<?php endif; ?></p>
		</div>
		<div class="list-description">
			<p>
				<?php if($this->data->year_built):
						echo $this->data->year_built?> Yr Built<br />
				<?php endif; ?> 
					<?php if (!empty($this->data->bedrooms) and !empty($this->data->baths_total)) : ?>
					<?php echo $this->data->bedrooms?> BR / <?php echo $this->data->baths_total?> Baths<br />
				<?php endif; ?>
				<?php if(!empty($this->data->sq_feet)):?>
					<?php echo Yii::app()->format->formatNumber($this->data->sq_feet)?> SF<br />
				<?php endif; ?>
					<br />
				<?php if(!empty($this->data->sq_feet)):?>
					<?php echo '($'.$this->data->pricePerSf.'/SF)' ?><br />
				<?php endif; ?>
				<?php if($this->data->mls_property_type_id == MlsPropertyTypes::LAND_ID) {
					echo 'Vacant Land<br />';
				}?>
				<?php echo ($this->data->pool_yn)? '<span class="water">Pool</span><br />': '';?>
				<?php if(!empty($this->data->photo_count)): ?>
					<a href="<?php echo $this->data->getUrl($relative=true); ?>" class="photos"><?php echo $this->data->photo_count?> Photos</a>
				<?php endif; ?>

					<?php if($this->data->status_change_date>0 && Yii::app()->user->boardId != 2): //@todo: need to change the hard coding for ferraro ?>
					<?php echo Yii::app()->format->formatDays($this->data->status_change_date)?>
				<?php endif; ?>
			</p>
		</div>
		<?php /*<span class="size">XL / XXL / S </span>*/ ?>
	</div>
	<div class="price"><span><?php echo Yii::app()->format->formatDollars($this->data->price)?></span></div>
	<div class="action-control"><a class="btn btn-primary"> <span class="add2cart"><i class="glyphicon glyphicon-shopping-cart"> </i> View Details </span> </a></div>
</div>