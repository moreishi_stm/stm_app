<div class="parallax-section parallax-image-1">
    <div class="container">
        <div class="row ">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="parallax-content clearfix">
                    <h1 class="parallaxPrce"> $200 </h1>

                    <h2 class="uppercase">FREE Private Home Selling & Marketing Consultation</h2>

                    <h3> Discover how to get top dollar for your home for the least amount of hassle. </h3>

                    <div style="clear:both"></div>
                    <a class="btn btn-discover "> <i class="fa fa-shopping-cart"></i> BOOK NOW </a></div>
            </div>
        </div>
    </div>
</div>