<style>
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus, .nav-tabs > li:hover > a {
  background: #444;
}
.fadedBg {
	background-color: #FFF;
	border: 5px solid #fff;
	width: 60%;
	max-width: 970px;
	margin: 0 auto;
	/*min-height: 300px*/
}
.bannerContentContainer {
	position: absolute;
	top: 55%;
	left: 50%;
	transform: translate(-50%, -50%);
	z-index: 2;
	/*background-color:rgba(255,255,255,0.5);*/
	padding:0;
}
.bannerContentContainer .nav.nav-tabs {
	margin-bottom: 0;
	border-bottom: none;
}
.bannerContentContainer .nav > li.active,
.bannerContentContainer .nav-tabs > li.active > a,
.bannerContentContainer .nav > li > a:hover,
.bannerContentContainer .nav > li > a:focus,
.bannerContentContainer .nav > li:hover > a,
.bannerContentContainer .nav-tabs > li.active > a:hover,
.bannerContentContainer .nav-tabs > li.active > a:focus,
.bannerContentContainer .nav-tabs > li:hover > a{
	background-color: #444;
}
.bannerContentContainer .nav-tabs li {
	width: 50%;
}
.bannerContentContainer .nav-tabs li a {
	width: 100%;
	font-size: 150%;
	text-align: center;
}

.bannerContentContainer .tab-content {
	background-color: #FFF;
}

.bannerContentContainer .tab-content .tab-pane {
	/*min-height: 300px;*/
	background-color: #444;
}

.bannerContentContainer .form {
	margin: 10px 0;
}

.bannerContentContainer .form input[type="text"] {
	height: 50px;
}
.bannerContentContainer .form-control {
	height: 50px;
}
#homsearch {
	position: relative;
}
.homsearchContents {
	/*position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);*/
}

#homePageSearchForHomesKeywordsBtn,
#homePageHouseValueBtn {
	height: 50px;
	width: 100%;
}
.homePageSearchForHomesKeywordsContainer {
	float: right;
}
.bannerContentContainer h1 {
    color: white;
    text-shadow: 3px 3px 0 black;
    font-weight: bold;
    text-align: center;
    line-height: 1;
    margin-bottom: 60px;
}
@media (min-width: 1200px) {
	.form-group.col-lg-2 {
		/*width: 20%;*/
	}
}
@media (min-width: 1024px) {
    .bannerContentContainer h1 {
        font-size: 70px;
    }
}
@media (max-width: 1024px) {
    .bannerContentContainer h1 {
        font-size: 60px;
    }
}
@media (max-width: 1280px) {
    .nav.navbar-nav > li > a {
	    font-size: 16px;
        padding: 15px 25px;
	}
}
@media (max-width: 991px)  {
	#homePageSearchForHomesKeywordsBtn,
	#homePageHouseValueBtn {
		height: auto;
		margin: 0;
	}
	.bannerContentContainer .form input[type="text"] {
		/*height: auto;*/
		margin: 0;
	}
	.bannerContentContainer .form-control {
		/*height: auto;*/
	}
	.form-group {
		margin-bottom: 5px;
	}
	.bannerContentContainer .fadedBg {
		min-width: 347px;
	}
    .bannerContentContainer h1 {
        font-size: 50px;
        margin-bottom: 20px;
    }
    .bannerContentContainer .nav-tabs li a {
        font-size: 18px;
    }
}
@media (max-width: 800px)  {
	.fadedBg {
		min-height: 1px;
	}
    .bannerContentContainer h1 {
        font-size: 40px;
        text-shadow: 2px 2px 0 black;
    }
    .nav-tabs li a.active.agent-calculator {
        font-size: 20px;
    }
    .bannerContentContainer .nav-tabs li a {
        font-size: 15px;
    }
    .header-version-2 .banner {
        margin-top: 90px !important;
    }
}
@media (max-width: 807px) {
	.bannerContentContainer .fadedBg{
		/*max-width: 50%;*/
	}
}
@media (max-width: 791px) {
	/*.header-version-2 .banner, .full-container, .slider-content, .slider-v2, .slider-v2 img {*/
		/*min-height: 352px;*/
	/*}*/
	.header-version-2 .banner {
		overflow: visible;
	}
	.bannerContentContainer {
		position: absolute;
		top: 35px;
		left: 0;
		transform: none;
	}
}
@media (max-width: 767px) {
	.navbar-top .navbar-brand {
		/*display: none;*/
	}
	.bannerContentContainer .nav-tabs li a {
		font-size: 100%;
	}
    .bannerContentContainer .nav-tabs li.active.agent-calculator a {
        font-size: 20px;
    }
}
@media (max-width: 650px) {
	.bannerContentContainer .fadedBg {
		max-width: 100%;
	}
}
@media (max-width: 360px) {
    .bannerContentContainer .nav-tabs li a {
        font-size: 17px;
    }
    .bannerContentContainer h1 {
        font-size: 35px;
        margin-bottom: 0;
    }
}
@media only screen and (device-width: 768px) {
    /* For general iPad layouts */
    .bannerContentContainer {
        top: 10%;
        left: auto;
    }
}
</style>
<div class="banner">
	<div class="full-container">
		<div class="slider-content">
			<div class="slider slider-v2">
				<img alt="img" src="<?=$this->imageLocation;?>" class="img-responsive parallaximg sliderImg">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12 bannerContentContainer">
<!--                <div style="width: 100%; height: 800px; z-index: 2; opacity: .8; background: black;"></div>-->
                <? if(/*strpos($_SERVER['SERVER_NAME'], 'kwadvantage2') !== false || strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') !== false || strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') !== false || strpos($_SERVER['SERVER_NAME'], 'kwclassicrealty') !== false || strpos($_SERVER['SERVER_NAME'], 'kwrealtyboise') !== false || strpos($_SERVER['SERVER_NAME'], 'kwsarasotasiestakey') !== false || strpos($_SERVER['SERVER_NAME'], 'growwithkwrc') !== false || strpos($_SERVER['SERVER_NAME'], 'demotl.') !== false*/Yii::app()->user->client->type == 'Brokerage'):?>
                    <h1>
                        Welcome to<br>
                        <?=Yii::app()->user->settings->office_name?>
                    </h1>
                    <div class="fadedBg">
                        <ul class="nav nav-tabs" style="">
                            <li class="active agent-calculator" style="width: 100%;"><a href="javascript:void(0)" data-toggle="tab" aria-expanded="true">Proven Business Models for Success</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active clearfix" id="homsearch">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12 homsearchContents">
                                    <div class="form clearfix">
                                        <?php
                                        $formId           = Forms::FORM_RECRUIT_FREE_CONSULT;
                                        $SubmissionValues = new FormSubmissionValues($formId);
                                        $FormFields       = new FormFields;
                                        $form=$this->beginWidget('CActiveForm', array(
                                                'action' => array('/front/forms/recruiting/formId/'.$formId),
                                                'id'=>'recruiting-form',
                                                'enableAjaxValidation' => true,
                                                'enableClientValidation' => false,
                                                'clientOptions' => array(
                                                    'validateOnChange' => false,
                                                    'validateOnSubmit' => true,
                                                    'beforeValidate' => 'js:function(form, attribute) {
                                                        if($("#free-consult-form-container").hasClass("hidden")) {
                                                            $("#free-consult-form-container").hide().removeClass("hidden").show("normal");
                                                            return false;
                                                        }
                                                        else {
                                                            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                                            return true;
                                                        }
                                                    }',
                                                    'afterValidate' => 'js:function(form, data, hasErrors) {
                                                        if (!hasErrors) {
                                                            $("#recruiting-form").addClass("hidden");
                                                            $("#thank-you-message").removeClass("hidden");
                                                        } else {
                                                        }
                                                        $("div.loading-container.loading").remove();
                                                        return false;
                                                    }',
                                                ),
                                            ));
                                        ?>
                                        <?php $SubmissionValues->data[FormFields::SOURCE_DESCRIPTION_ID] = str_replace('www.', '', $_SERVER['SERVER_NAME']).' - FREE Consultation (Home Page)'; ?>
                                        <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_DESCRIPTION_ID.']'); ?>

                                        <div id="free-consult-form-container" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12 hidden">
                                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 col-xs-min-12">
                                                <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array(
                                                        'placeholder'=>'First Name',
                                                        'class'=>'form-control'
                                                    ));
                                                ?>
                                                <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 col-xs-min-12">
                                                <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array(
                                                        'placeholder'=>'Last Name',
                                                        'class'=>'form-control'
                                                    ));
                                                ?>
                                                <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 col-xs-min-12">
                                                <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array(
                                                        'placeholder'=>'Email',
                                                        'class'=>'form-control'
                                                    ));
                                                ?>
                                                <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 col-xs-min-12">
                                                <?php $this->widget('StmMaskedTextField', array(
                                                        'model' => $SubmissionValues,
                                                        'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                                                        'mask' => '(999) 999-9999',
                                                        'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                                                        'htmlOptions' => array('class' => 'form-control', 'placeholder' => 'Phone'),
                                                    )); ?>

                                                <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
                                            </div>
                                        </div>

                                            <?php //echo $form->hiddenField($SubmissionValues, 'data[' . $FormFields->getField('source_description')->id . ']', array('value'=>'Agent Income Calculator')); ?>
                                        <div class="form-group col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 col-xs-min-12 no-padding">
                                            <?php echo CHtml::submitButton('Schedule FREE Consultation Now', array('id' => 'homePageHouseValueBtn', 'class'=>'btn btn-primary btn-lg')); ?>
                                        </div>
                                        <?php $this->endWidget(); ?>
                                        <div id="thank-you-message" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12 hidden" style="background-color: white; padding: 30px;">
                                            <h2>Thank you!</h2>
                                            <h3>Your request was successfully sent! We will touch base to confirm a date and time for the FREE Consultation.  Freel free to call <?=Yii::app()->user->settings->office_phone?> for immediate assistance.</h3>
                                            <h3>Have a great day and we look forward to meeting you.</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?else:?>
				<div class="fadedBg">
					<ul class="nav nav-tabs" style="">
						<li class="active"><a href="#homsearch" data-toggle="tab" aria-expanded="true">Home Search</a></li>
						<li><a href="#freeHouseValue" data-toggle="tab" aria-expanded="false">Free House Value</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active clearfix" id="homsearch">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12 homsearchContents">
								<div class="form clearfix">
									<?php
									$propertyTypeIds = MlsPropertyTypeBoardLu::getDefaultPropertyTypeIds(Yii::app()->user->board->id);

									$addHtml = "";
									if(!empty($propertyTypeIds)) {
										$addHtml = "/property_types/".implode(",",$propertyTypeIds)."/";
									}

									$form=$this->beginWidget('CActiveForm', array(
										'action'=>'/homes'.$addHtml,
										'id'=>'search-form',
                                        'method'=>'GET',
									)); ?>
									<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12">
										<?php

										echo CHtml::textField('keywords', null, $htmlOptions = array(
											'id' => 'homePageSearchForHomesKeywords',
											'placeholder'=>'Type a Keyword, Neighborhood, City, Street Name, Zip',
											'class'=>'form-control'
										)); ?>
									</div>
									<div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-6 col-xs-min-6">
										<?php echo CHtml::dropDownList('price_min', null, StmFormHelper::getPriceList(),$htmlOptions=array('empty'=>'Price Min', 'class'=>'form-control priceMin'));?>
									</div>
									<div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-6 col-xs-min-6">
										<?php echo CHtml::dropDownList('price_max', null, StmFormHelper::getPriceList(),$htmlOptions=array('empty'=>'Price Max', 'class'=>'form-control priceMax'));?>
									</div>
									<div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-6 col-xs-min-6">
										<?php echo CHtml::dropDownList('bedrooms', null, StmFormHelper::getBedroomList(),$htmlOptions=array('empty'=>'Bedrooms', 'class'=>'form-control bedrooms'));?>
									</div>
									<div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-6 col-xs-min-6">
										<?php echo CHtml::dropDownList('baths_total', null, StmFormHelper::getBathList(),$htmlOptions=array('empty'=>'Baths', 'class'=>'form-control bathsTotal'));?>
									</div>
									<div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-6 col-xs-min-6">
										<?php echo CHtml::dropDownList('sq_feet', null, StmFormHelper::getSqFeetList(),$htmlOptions=array('empty'=>'Sq. Feet', 'class'=>'form-control sqFeet'));?>
									</div>
									<div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-6 col-xs-min-6 homePageSearchForHomesKeywordsContainer">
										<?php echo CHtml::submitButton('Search', array('id' => 'homePageSearchForHomesKeywordsBtn', 'name'=>'', 'class'=>'btn btn-primary btn-lg')); ?>
									</div>

								<?php $this->endWidget(); ?>
								</div>
							</div>
						</div>
						<div class="tab-pane clearfix" id="freeHouseValue">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12 homsearchContents">
								<div class="form clearfix">
									<?php
									$formId           = Forms::FORM_HOUSE_VALUES;
                                    $formPart = 1;
                                    $formName = $formId.'_Part'.$formPart;
									$SubmissionValues = new FormSubmissionValues($formId);
                                    $SubmissionValues->formPart = $formPart;
									$FormFields       = new FormFields;

									$form=$this->beginWidget('CActiveForm', array(
                                        'action'=>array('/front/forms/houseValues/formId/'.$formId),
										'id'=>'houseValues-form',
                                        'enableAjaxValidation' => true,
                                        'enableClientValidation' => false,
                                        'clientOptions' => array(
                                            'validateOnChange' => false,
                                            'validateOnSubmit' => true,
                                            'beforeValidate' => 'js:function(form, attribute) {
                                                $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                                return true;
                                            }',
                                            'afterValidate' => 'js:function(form, data, hasErrors) {
                                                if (!hasErrors && data.status =="successPart1") {
                                                    window.location = "http://'.$_SERVER["SERVER_NAME"].'/values/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/2";
                                                } else if (!hasErrors) {
                                                    window.location = "http://'.$_SERVER["SERVER_NAME"].'/values/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/success";
                                                } else {
                                                    $("div.loading-container.loading").remove();
                                                }
                                                return false;
                                            }',
                                            ),
									)); ?>
									<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-min-12">
										<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'form-control', 'style'=>'','placeholder'=>'Address', 'data-prompt-position'=>'topLeft'));?>
                                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('address') . ']'); ?>
									</div>
									<div class="form-group col-lg-5 col-md-4 col-sm-12 col-xs-12 col-xs-min-12">
										<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('city')->id.']', $htmlOptions=array('class'=>'form-control', 'style'=>'','placeholder'=>'City','data-prompt-position'=>'bottomLeft'));?><?php //$SubmissionValues->data[$FormFields->getField('state')->id] = 'FL'; ?>
                                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('city') . ']'); ?>
									</div>
									<div class="form-group col-lg-3 col-md-4 col-sm-12 col-xs-12 col-xs-min-12">
										<?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getField('state')->id.']', CHtml::listData(AddressStates::model()->findAll(), 'short_name', 'short_name'),$htmlOptions=array('empty'=>'State', 'class'=>'g2', 'class'=>'form-control'));?>
                                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('state') . ']'); ?>
									</div>
									<div class="form-group col-lg-2 col-md-4 col-sm-12 col-xs-12 col-xs-min-12">
										<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']', $htmlOptions=array('class'=>'form-control', 'style'=>'','placeholder'=>'Zip','data-prompt-position'=>'bottomLeft'));?>
                                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('zip') . ']'); ?>
									</div>
									<div class="form-group col-lg-2 col-md-12 col-sm-12 col-xs-12 col-xs-min-12">
                                        <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
										<?php echo CHtml::submitButton('Search', array('id' => 'homePageHouseValueBtn', 'class'=>'btn btn-primary btn-lg')); ?>
									</div>
								<?php $this->endWidget(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
                <?endif;?>
			</div>
		</div>
	</div>
</div>