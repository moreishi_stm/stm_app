<?php
class BannerImageWidget extends BaseWidget {
	protected $defaultimage = "http://cdn.seizethemarket.com/assets/standard2/assets/slider/slider2.jpg";
	public $imageLocation = "";

	public function init() {
		parent::init();

		if(empty($this->imageLocation)) {
            // home page main photo

            //@todo: this is TEMPORARY until an interface is created so user can customize
            switch(1) {
                case(strpos($_SERVER['SERVER_NAME'], 'myrealtydfw') !== false || strpos($_SERVER['SERVER_NAME'], 'themcelroyteam') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/17/1/themes/standard2/homepage/banner.png';
                    break;

                case(strpos($_SERVER['SERVER_NAME'], 'myashevillerealestate') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/16/1/themes/standard2/homepage/banner.jpg';
                    break;

                case(strpos($_SERVER['SERVER_NAME'], 'shreveportbossierliving') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/23/1/themes/standard2/homepage/banner.jpg';
                    break;

                case(strpos($_SERVER['SERVER_NAME'], 'hamiltonandcompany') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/3/1/themes/standard2/homepage/banner.jpg';
                    break;

                case(strpos($_SERVER['SERVER_NAME'], 'greaterbirminghamhomesforsale') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/34/1/themes/standard2/homepage/banner.jpg';
                    break;

                case(strpos($_SERVER['SERVER_NAME'], 'utahhomes.today') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/41/1/themes/standard2/homepage/banner.jpg';
                    break;

                case(strpos($_SERVER['SERVER_NAME'], 'mypinehursthomesearch') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/25/1/themes/standard2/homepage/banner.jpg';
                    break;

                case strpos($_SERVER['SERVER_NAME'], 'kwadvantage2') !== false:
                case(strpos($_SERVER['SERVER_NAME'], 'demotl.') !== false):
                case(strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/45/1/themes/standard2/homepage/banner5b.jpg';
                    break;

                case(strpos($_SERVER['SERVER_NAME'], 'mywestchesterhomesearch') !== false):
                    $this->defaultimage = 'http://sites.seizethemarket.com/site-files/26/1/themes/standard2/homepage/banner.jpg';
                    break;
            }
			$this->imageLocation = $this->defaultimage;
		}

		//Yii::app()->clientScript->registerCssFile($this->cssAssetsUrl.DS.'banner_image.css');
	}
}