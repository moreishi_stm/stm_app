<?
Yii::app()->clientScript->registerCss('homesByPriceSidebar', <<<CSS
    #homes-by-price-container {
        background-color: #e8e8e8;
        border: 1px solid #DDD;
        margin: 10px 0;
    }
    #homes-by-price-container h2 {
        text-align: center;
        font-weight: bold;
        padding: 5%;
    }
    #homes-by-price-container ul {
        text-align: center;
    }
    #homes-by-price-container ul li {
    }
    #homes-by-price-container ul li:nth-child(odd) {
        background-color: white;
    }
    #homes-by-price-container ul li:nth-child(even) {
        background-color: #f5f5f5;
    }
    #homes-by-price-container ul li:hover {
        font-weight: bold;
        border-color: #e8e8e8;
        background-color: #E6E9FF;  /*#dcfdd9*/
    }
    #homes-by-price-container ul li a {
        padding: 6px 6px 6px 15px;
        display: block;
    }
    @media (max-width: 768px) {
        #homes-by-price-container{
            width: 70%;
            margin-left: 15%;
        }
    }
CSS
);
?>

<div id="homes-by-price-container" class="col-lg-12 no-padding">
    <h2>Homes by Price</h2>
    <ul>
        <li><a href="/homes/price_min/1/price_max/99999">Up to $100,000</a></li>
        <?for($i=1; $i<10; $i++) { ?>
        <li><a href="/homes/price_min/<?=(($i)*100000)?>/price_max/<?=((($i+1)*100000))?>"><?=Yii::app()->format->formatDollars(($i*100000))?> - <?=Yii::app()->format->formatDollars((($i+1)*100000))?></a></li>
        <? } ?>
        <li><a href="/homes/price_min/1000000/price_max/1999999">$1,000,000 - 2,000,000</a></li>
        <li><a href="/homes/price_min/1000000/price_max/1999999">$2,000,000 - 3,000,000</a></li>
        <li><a href="/homes/price_min/1000000/price_max/1999999">$3,000,000 - 4,000,000</a></li>
        <li><a href="/homes/price_min/1000000/price_max/1999999">$4,000,000 - 5,000,000</a></li>
        <li><a href="/homes/price_min/5000000">Over $5,000,000</a></li>
    </ul>
</div>
