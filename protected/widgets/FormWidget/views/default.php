<div class="form-container col-lg-12">
    <h2 class="ask-question">Ask a Question</h2>
    <h3 class="ask-question">Get FREE Expert Advice</h3>
    <? $form = $this->beginWidget('CActiveForm', array(
            'id'=>'market-snapshot-form',
            'action' => array('/front/forms/submit/formId/'.$formId),
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnChange' => false,
                'validateOnSubmit' => true,
                'beforeValidate'    => 'js:function(form) {
									$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
									return true;
								}',
                'afterValidate' => 'js:function(form, data, hasErrors) {
								$("div.loading-container.loading").remove();
								if (!hasErrors) {
									Message.create("success", "Your request was sent successfully!");
									$("div#form").addClass("hidden");
									$("div#thank-you-message").hide().removeClass("hidden").show();
								}

								return false;
							}',
            )
        ));
    ?>

    <div class="col-sm-6 col-xs-12 no-padding">

        <?php echo $form->textArea($SubmissionValues, 'data['.$FormFields->getField('question')->id.']', $htmlOptions=array('placeholder'=>"Ask a Question now... \nThinking about selling?... Schedule a FREE Consultation. \nRefer a friend or family.",'class'=>'col-xs-12','rows'=>4));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('question')->id.']'); ?>
    </div>
    <div class="col-sm-6 col-xs-12 no-padding">
        <div class="col-xs-12 no-padding">
            <div class="col-sm-6 col-xs-6 no-padding">
                <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array('placeholder'=>'First Name','class'=>'col-xs-12',));?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']'); ?>
            </div>
            <div class="col-sm-6 col-xs-6 no-padding">
                <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array('placeholder'=>'Last Name','class'=>'col-xs-12',));?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']'); ?>
            </div>
        </div>

        <div class="col-xs-12 no-padding">
            <div class="col-sm-6 col-xs-6 no-padding">
                <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array('placeholder'=>'E-mail','class'=>'col-xs-12',));?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('email')->id.']'); ?>
            </div>
            <div class="col-sm-6 col-xs-6 no-padding">
                <?php $this->widget('StmMaskedTextField', array(
                        'model' => $SubmissionValues,
                        'attribute' => 'data['.$FormFields->getField('phone')->id.']',
                        'mask' => '(999) 999-9999',
                        'id' => 'FormSubmissionValues_data_4',
                        'htmlOptions' => array('class'=>'col-xs-12','placeholder' => 'Phone',),
                    )); ?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('phone')->id.']'); ?>
            </div>
            <?php echo CHtml::hiddenField('url',$_SERVER['REQUEST_URI']); ?>
        </div>
    </div>
    <input id="submit-button" type="submit" class="submit btn btn-lg btn-primary" value="Request More Information">
    <?$this->endWidget();?>
    <div class="full-width-bg"></div>
</div>