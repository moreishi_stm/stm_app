<?
Yii::app()->clientScript->registerCss('communitiesSidebar', <<<CSS
    #featured-areas-container {
        background-color: #e8e8e8;
        border: 1px solid #DDD;
        -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 0 8px rgba(0,0,0,0.2);
        box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
        margin: 10px 0;
    }
    #featured-areas-container h2 {
        text-align: center;
        font-weight: bold;
        padding: 5%;
    }
    #featured-areas-container ul li {
        text-align: center;
    }
    #featured-areas-container ul li:nth-child(odd) {
        background-color: white;
    }
    #featured-areas-container ul li:nth-child(even) {
        background-color: #f5f5f5;
    }
    #featured-areas-container ul li:hover {
        font-weight: bold;
        border-color: #e8e8e8;
        background-color: #E6E9FF;  /*#dcfdd9*/
    }
    #featured-areas-container ul li a {
        padding: 6px 6px 6px 15px;
        display: block;
    }
CSS
);
if(!empty($this->data)):
?>
<div id="featured-areas-container" class="col-lg-12 no-padding">
    <?foreach($this->data as $name => $group) { ?>
        <h2><?=$name?></h2>
        <ul>
            <?foreach($group as $featuredArea) { ?>
            <li><a href="/area/<?=$featuredArea->url?>/"><?=$featuredArea->name?></a></li>
            <? } ?>
        </ul>
    <? } ?>
</div>
<?endif;?>