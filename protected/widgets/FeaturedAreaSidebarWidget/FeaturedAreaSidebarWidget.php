<?php
class FeaturedAreaSidebarWidget extends BaseWidget
{
    public $areas = array();//@todo This is temp property until we build this out more. This allows me to hard code a couple types for sidebar before making it settings based.

	public function init() {
		parent::init();

        if(empty($this->areas) || !is_array($this->areas)) {
            return;
        }

        foreach($this->areas as $i => $area) {

            $criteria = new CDbCriteria();
            $criteria->with = array('types');
            $criteria->compare('types.name', $area['tag']);
            $criteria->order = 'types.name ASC';

            if(isset($area['tag']) && !empty($area['tag']) && $result = FeaturedAreas::model()->findAll($criteria)) {
                $this->data[$area['name']] = $result;
            }
        }

		//Yii::app()->clientScript->registerCssFile($this->cssAssetsUrl.DS.'featured_area_sidebar.css');
//        Yii::app()->clientScript->registerCss('featuredHomesForSaleWidget',<<<CSS
//            .featuredPostContainer .item {
//                margin: 0 1% 15px 0;
//                height: auto;
//            }
//            @media (min-width: 768px) {
//                .featuredPostContainer .item.col-sm-4, .featuredPostContainer .item.col-md-4 {
//                    width: 32.33333333%;
//                }
//            }
//            @media (max-width: 767px) {
//                .featuredPostContainer .item.col-sm-4, .featuredPostContainer .item.col-md-4 {
//                    width: 49%;
//                }
//            }
//CSS
//        );
	}
}