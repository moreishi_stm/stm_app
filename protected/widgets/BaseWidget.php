<?php

abstract class BaseWidget extends CWidget {
	public $assetsPath = NULL;
	public $cssAssetsPath = NULL;
	public $jsAssetsPath = NULL;
	public $cssAssetsUrl = NULL;
	public $jsAssetsUrl = NULL;
	public $title = NULL;
	public $view_file = NULL;
	public $type = NULL;
	public $widgets = array();
	public $DataProvider = array();
	public $data = array();
	public $class = '';
	public $id;
	public $index;

	public function init() {

		//Yii::app()->getWidgetFactory()->createWidget($this,$className,$properties);
		if(empty($this->assetsPath)) {
			$this->assetsPath = 'stm_app.widgets'.get_called_class().'.assets';
		}

		if(empty($this->cssAssetsPath)) {
			$this->cssAssetsPath = Yii::getPathOfAlias($this->assetsPath.'.css');
		}

		if(empty($this->jsAssetsPath)) {
			$this->jsAssetsPath = Yii::getPathOfAlias($this->assetsPath.'.js');
		}

		$this->cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias($this->cssAssetsPath), false, -1, false);
		$this->jsAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias($this->jsAssetsPath), false, -1, false);

		if(empty($this->view_file)) {
			$this->view_file = "default";
		}

		if(empty($this->type)) {

		}
	}

	public function run() {
		$this->render($this->view_file);
	}
}
