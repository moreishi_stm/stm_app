<?php Yii::import($this->displayWidget);

if(count($data) > 0):

foreach ($data as $k => $item):
	
	if(get_class($item) == "MlsProperties") {
		#die("<pre>".print_r($item->attributes, 1));
	}
	
	if(!$item->hasProperty('id'))  {
		$id = $item->getPrimaryKey();
	} else {
		$id = $item->id;
	}
	
	?>
<div class="<?=implode(' ', $this->additionalRowCssClasses).' '.$this->getRowClass(); ?>">
	<?php $this->widget($this->displayWidget, array_merge($this->displayWidgetOptions, array('id' => $id,'index' => $k, 'data' => $item))); ?>
</div>
<?php 

endforeach;

else: ?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <?=$this->noResultsMessage;?>
</div>
<?php endif; ?>
