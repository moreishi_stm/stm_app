<?php

class RowWidget extends BaseWidget {
	protected $_maxPerRow = 6;
	protected $_perRow = 2;
	protected $_data = NULL;

	protected static $rowClassMap = array(
		'md' => array(
			1 => 'col-md-12 col-sm-6 col-xs-6 col-xs-min-12 no-margin no-padding',
			2 => 'col-md-6 col-sm-6 col-xs-6 col-xs-min-12 no-margin no-padding',
			3 => 'col-md-4 col-sm-6 col-xs-6 col-xs-min-12 no-margin no-padding',
			4 => 'col-md-3 col-sm-6 col-xs-6 col-xs-min-12 no-margin no-padding',
			5 => 'col-md-2 col-sm-6 col-xs-6 col-xs-min-12 no-margin no-padding',
			6 => 'col-md-1 col-sm-6 col-xs-6 col-xs-min-12 no-margin no-padding'
		),
		'lg' => array(
			//<div class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
			1 => 'col-lg-12 col-md-3 col-sm-4 col-xs-12 no-margin no-padding',
			2 => 'col-lg-6 col-md-6 col-sm-6 col-xs-12 no-margin no-padding',
			3 => 'col-lg-4 col-md-4 col-sm-6 col-xs-12 no-margin no-padding',
			4 => 'col-lg-3 col-md-3 col-sm-4 col-xs-12 no-margin no-padding',
			5 => 'col-lg-2 col-md-3 col-sm-4 col-xs-12 no-margin no-padding',
			6 => 'col-lg-1 col-md-3 col-sm-4 col-xs-12 no-margin no-padding'
		)
	);

	public $total = 0;
    public $perRow;
	public $displayWidget = NULL;
	public $displayWidgetOptions = array();
	public $bootstrapRowSize = NULL;
	public $additionalRowCssClasses = array();
	public $noResultsMessage = "No results found.";
	public $useCustomRowClass = false;
	public $customRowClass = NULL;

	public function getMaxPerRow() {
		return $this->_maxPerRow;
	}

	public function getPerRow() {
		return $this->_perRow;
	}

	public function setPerRow($perRow) {
		$perRow = (int)$perRow;

		if($perRow > $this->getMaxPerRow()) {
			throw new CException(Yii::t('stm','{widget} cannot have more than {maxPerRow}".',
				array('{widget}'=>get_class($this), '{maxPerRow}'=>  $this->getMaxPerRow())));
		}

		$this->_perRow = $perRow;

		return $this;
	}

	public function getRowClass() {

		if($this->useCustomRowClass && !empty($this->customRowClass)) {
			return $this->customRowClass;
		}

		return self::$rowClassMap[$this->bootstrapRowSize][$this->getPerRow()];
	}

	public function init() {
		if(!empty($this->DataProvider)) {
			$this->_data = $this->DataProvider->getData();
		}

		if(empty($this->bootstrapRowSize)) {
			$this->bootstrapRowSize = "lg";
		}

        if($this->perRow) {
            $this->setPerRow($this->perRow);
        }

		parent::init();
	}

	public function run() {
		$this->render($this->view_file, array(
			'data' => $this->_data
		));
	}
}

