<div class="<?=$this->containerClass; ?>">
	<?php if($this->enableTitle):?>
	<h3 class="<?=$this->titleClass; ?>"><span> <?= $this->title; ?> </span></h3>
	<?php
	endif;
	
	echo $content;
	?>
</div>