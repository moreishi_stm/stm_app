<?php

class ContainerWidget extends BaseWidget {
	
	public $wrapper = NULL;
	public $titleClass = NULL;
	public $containerClass = NULL;
	public $enableTitle = true;
	public $data = array();
	
	public function init() {
		if(empty($this->containerClass)) {
			$this->class = 'row globalPadding';
		}
		
		if(empty($this->titleClass)) {
			$this->titleClass = 'section-title style2 text-center';
		}
		
		parent::init();
	}
	
	public function run() {
		
		$content = "";
		foreach ($this->widgets as $widget => $options) {
			Yii::import($widget);			
			$content .= $this->widget($widget, $options, true);
		}

		if(!empty($this->wrapper)){
			$content = sprintf($this->wrapper, $content);
		}
		
		$this->render($this->view_file, array('content' => $content, 'data' => $this->data));
	}
	
}