<div class="w100 sectionCategory">
    <div class="container">
        <div class="sectionCategoryIntro text-center">
            <h1>Featured Categories</h1>

            <p> litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc
                nobis videntur parum clari, fiant sollemnes in futurum.
            </p>
        </div>
        <div class="row subCategoryList clearfix">
            <div class="col-md-2 col-sm-3 col-xs-4  col-xs-mini-6  text-center ">
                <div class="thumbnail equalheight">
                    <a class="subCategoryThumb" href="sub-category.html"><img src="/images/tempImages/images/product/3.jpg" class="img-rounded " alt="img"> </a>
                    <a class="subCategoryTitle"><span> T shirt </span></a></div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-4 col-xs-mini-6 text-center">
                <div class="thumbnail equalheight">
                    <a class="subCategoryThumb" href="sub-category.html"><img src="/images/tempImages/images/site/casual.jpg" class="img-rounded " alt="img"> </a>
                    <a class="subCategoryTitle"><span> Shirt </span></a></div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-4 col-xs-mini-6 text-center">
                <div class="thumbnail equalheight">
                    <a class="subCategoryThumb" href="sub-category.html"><img src="/images/tempImages/images/site/shoe.jpg" class="img-rounded " alt="img"> </a>
                    <a class="subCategoryTitle"><span> shoes </span></a></div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-4 col-xs-mini-6 text-center">
                <div class="thumbnail equalheight">
                    <a class="subCategoryThumb" href="sub-category.html"><img src="/images/tempImages/images/site/jewelry.jpg" class="img-rounded " alt="img"> </a>
                    <a class="subCategoryTitle"><span> Accessories </span></a></div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-4 col-xs-mini-6 text-center">
                <div class="thumbnail equalheight">
                    <a class="subCategoryThumb" href="sub-category.html"><img src="/images/tempImages/images/site/winter.jpg" class="img-rounded  " alt="img"> </a>
                    <a class="subCategoryTitle"><span> Winter Collection </span></a></div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-4 col-xs-mini-6 text-center">
                <div class="thumbnail equalheight">
                    <a class="subCategoryThumb" href="sub-category.html"><img src="/images/tempImages/images/site/Male-Fragrances.jpg" class="img-rounded " alt="img"> </a>
                    <a class="subCategoryTitle"><span> Fragrances </span></a></div>
            </div>
        </div>
        <!--/.row-->

    </div>
    <!--/.container-->
</div>