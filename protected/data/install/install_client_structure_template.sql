/*

SQLyog Ultimate v8.55
MySQL - 5.5.34-0ubuntu0.12.04.1 : Database - wisconsinpropertiesonline

*********************************************************************

*/



/*!40101 SET NAMES utf8 */;



/*!40101 SET SQL_MODE=''*/;



/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;



/*Table structure for table `YiiSession` */
SET FOREIGN_KEY_CHECKS=0;


DROP TABLE IF EXISTS `YiiSession`;



CREATE TABLE `YiiSession` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `YiiSession` */



/*Table structure for table `account_contact_lu` */



DROP TABLE IF EXISTS `account_contact_lu`;



CREATE TABLE `account_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) NOT NULL DEFAULT '0',
  `contact_type_ma` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `account_contact_lu_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `account_contact_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `account_contact_lu` */



/*Table structure for table `accounting_accounts` */



DROP TABLE IF EXISTS `accounting_accounts`;



CREATE TABLE `accounting_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accounting_account_type_ma` int(11) NOT NULL,
  `status_ma` tinyint(1) NOT NULL DEFAULT '1',
  `code` varchar(15) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(127) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounting_accounts_ibfk_1` (`updated_by`),
  KEY `accounting_accounts_ibfk_2` (`added_by`),
  KEY `accounting_accounts_ibfk_3` (`parent_id`),
  CONSTRAINT `accounting_accounts_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `accounting_accounts_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `accounting_accounts_ibfk_3` FOREIGN KEY (`parent_id`) REFERENCES `accounting_accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `accounting_accounts` */



insert  into `accounting_accounts`(`id`,`accounting_account_type_ma`,`status_ma`,`code`,`parent_id`,`name`,`notes`,`updated_by`,`updated`,`added_by`,`added`) values (1,2,1,NULL,NULL,'Marketing/Lead Gen',NULL,1,'2013-11-11 22:54:01',1,'2013-11-11 22:54:01');



/*Table structure for table `accounting_transactions` */



DROP TABLE IF EXISTS `accounting_transactions`;



CREATE TABLE `accounting_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accounting_account_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference_number` varchar(15) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `amount` decimal(11,2) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounting_transactions_ibfk_1` (`updated_by`),
  KEY `accounting_transactions_ibfk_2` (`added_by`),
  KEY `accounting_transactions_ibfk_3` (`source_id`),
  CONSTRAINT `accounting_transactions_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `accounting_transactions_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `accounting_transactions_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `accounting_transactions` */



/*Table structure for table `accounts` */



DROP TABLE IF EXISTS `accounts`;



CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11) DEFAULT NULL,
  `account_status_ma` tinyint(1) DEFAULT NULL,
  `timezone_utc` varchar(6) DEFAULT NULL,
  `agent_board_num` varchar(50) DEFAULT NULL,
  `feed_username` varchar(50) DEFAULT NULL,
  `feed_password` varchar(50) DEFAULT NULL,
  `contract_date` date DEFAULT NULL,
  `go_live_date` date DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `api_key` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`),
  -- KEY `mls_board_id` (`mls_board_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB;



/*Data for the table `accounts` */



insert  into `accounts`(`id`,`mls_board_id`,`account_status_ma`,`agent_board_num`,`feed_username`,`feed_password`,`contract_date`,`go_live_date`,`notes`,`api_key`,`is_deleted`) values (1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);



/*Table structure for table `action_plan_applied_lu` */



DROP TABLE IF EXISTS `action_plan_applied_lu`;



CREATE TABLE `action_plan_applied_lu` (
  `action_plan_item_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`action_plan_item_id`,`task_id`),
  KEY `task_id` (`task_id`),
  KEY `action_plan_item_id` (`action_plan_item_id`),
  CONSTRAINT `action_plan_applied_lu_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `action_plan_applied_lu_ibfk_2` FOREIGN KEY (`action_plan_item_id`) REFERENCES `action_plan_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `action_plan_applied_lu` */



/*Table structure for table `action_plan_items` */



DROP TABLE IF EXISTS `action_plan_items`;



CREATE TABLE `action_plan_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_plan_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '1',
  `task_type_id` int(11) DEFAULT NULL COMMENT 'email, phone, etc.',
  `email_template_id` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `due_days` int(11) DEFAULT NULL,
  `due_days_type_ma` tinyint(1) DEFAULT NULL,
  `due_reference_ma` tinyint(1) DEFAULT NULL,
  `assign_to_type_ma` int(11) DEFAULT NULL COMMENT 'owner, user, closing, specific',
  `if_weekend` tinyint(1) DEFAULT NULL COMMENT 'move before, after, keep',
  `specific_user_id` int(11) DEFAULT NULL,
  `depends_on_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `action_plan_id` (`action_plan_id`),
  KEY `email_template_id` (`email_template_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `specific_user_id` (`specific_user_id`),
  CONSTRAINT `action_plan_items_ibfk_1` FOREIGN KEY (`action_plan_id`) REFERENCES `action_plans` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `action_plan_items_ibfk_2` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `action_plan_items_ibfk_3` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `action_plan_items_ibfk_4` FOREIGN KEY (`specific_user_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `action_plan_items` */



insert  into `action_plan_items`(`id`,`action_plan_id`,`status_ma`,`task_type_id`,`email_template_id`,`description`,`due_days`,`due_days_type_ma`,`due_reference_ma`,`assign_to_type_ma`,`if_weekend`,`specific_user_id`,`depends_on_id`,`sort_order`,`is_deleted`) values (1,1,1,2,NULL,'Sphere Monthly Touch',0,2,1,13,2,NULL,NULL,1,1),(2,12,1,1,NULL,'Final phone call (Expired Follow-up)',0,1,1,1,1,NULL,NULL,1,0),(3,2,1,2,NULL,'Initial phone call- within 5 minutes, no voice mail',0,2,1,2,1,NULL,NULL,1,0),(4,2,1,2,NULL,'follow up phone call with nugget',3,2,1,2,1,NULL,NULL,1,0),(5,3,1,2,NULL,'Initial phone call',2,1,1,2,1,NULL,NULL,1,0),(6,3,1,4,NULL,'Friendly Text Message Follow-Up',1,1,1,2,1,NULL,NULL,1,0),(7,4,1,2,NULL,'Initial phone call',2,1,1,4,1,NULL,NULL,1,0),(8,4,1,6,NULL,'Personal Touch Correspondence',3,1,1,5,1,NULL,NULL,1,0),(9,5,1,2,NULL,'Initial phone call',2,1,1,12,1,3,NULL,1,0),(10,5,1,4,NULL,'Friendly Text Message Follow-Up',1,1,1,13,1,NULL,NULL,1,0),(11,6,1,1,NULL,'Assign IVR # Record outgoing coming soon message',0,2,1,6,3,NULL,NULL,NULL,1),(12,6,1,1,NULL,'Implement Coming Soon Program Install sign with IVR #, e-mail agents details',0,2,1,6,3,NULL,NULL,NULL,1),(13,6,1,1,NULL,'Handle Upfront Fee *',0,2,1,6,3,NULL,NULL,NULL,0),(14,6,1,1,NULL,'Obtain 2 sets of keys',0,2,1,6,3,NULL,NULL,NULL,0),(15,6,1,1,NULL,'Receive and scan all documents',0,2,1,6,3,NULL,NULL,NULL,0),(16,6,1,1,NULL,'Make Paper and Electronic File/Label',0,2,1,6,3,NULL,NULL,NULL,0),(17,6,1,1,NULL,'Make Electronic File w/Subfolders',0,2,1,6,3,NULL,NULL,NULL,1),(18,6,1,1,NULL,'Schedule Staging *',0,2,1,6,3,NULL,NULL,NULL,0),(19,6,1,1,NULL,'Schedule Photography *',0,2,6,6,3,NULL,18,NULL,0),(20,6,1,1,NULL,'Install sign w/ rider, flyers and lockbox',0,2,6,6,3,NULL,11,NULL,0),(21,6,1,1,NULL,'Confirm ALL Seller (+ spouse) contacts',1,2,1,6,3,NULL,NULL,NULL,0),(22,6,1,1,NULL,'Update Admin',1,2,1,6,3,NULL,NULL,NULL,0),(23,6,1,1,NULL,'Download Photos and Resize',0,2,6,6,3,NULL,19,NULL,0),(24,6,1,1,NULL,'Review photos for brightness & color richness',0,2,6,6,3,NULL,23,NULL,0),(25,6,1,1,NULL,'Add Listing,Photos and documents to MLS',0,2,6,4,1,NULL,24,NULL,0),(26,6,1,1,NULL,'Assign lockbox in MLS',0,2,6,6,3,NULL,25,NULL,0),(27,6,1,1,NULL,'Set up Showing Suite & Email Seller',0,2,6,6,3,NULL,25,NULL,0),(28,6,1,1,NULL,'Update Showing Desk',0,2,6,6,3,NULL,25,NULL,0),(29,6,1,1,NULL,'Verify HOA contact and fees, CDD, MISC Fees w/ Seller',2,2,1,6,3,NULL,NULL,NULL,0),(30,6,1,3,NULL,'Email Listing to Seller for Approval',0,2,6,6,3,NULL,25,NULL,0),(31,6,1,3,NULL,'Blast Email to Office ',0,2,6,6,3,NULL,25,NULL,0),(32,6,1,1,NULL,'Add video to MLS and Realtor.com',0,2,6,6,3,NULL,25,NULL,0),(33,6,1,1,NULL,'Check christineleeteam.com for listing',0,2,6,6,3,NULL,25,NULL,0),(34,6,1,1,NULL,'Do KW paperless and upload docs',0,2,6,6,3,NULL,25,NULL,0),(35,6,1,1,NULL,'Order HWA *',3,2,1,6,3,NULL,NULL,NULL,0),(36,6,1,1,NULL,'Schedule Home Inspection *',2,2,1,6,3,NULL,NULL,NULL,0),(37,6,1,3,NULL,'Email Listing Documents to Seller',0,2,6,6,3,NULL,15,NULL,0),(38,6,1,1,NULL,'Announce New Listing at Team Meeting',0,2,6,6,3,NULL,25,NULL,0),(39,6,1,1,NULL,'Deliver & Maintain Flyers',3,2,1,6,3,NULL,NULL,NULL,0),(40,6,1,1,NULL,'Showings Follow-Up',14,2,1,6,3,NULL,NULL,NULL,1),(41,2,1,1,NULL,'Verify Social Networking Facebook, Twitter, Linkedin',0,2,1,2,1,NULL,NULL,NULL,0),(42,2,1,2,NULL,'Call Client Afternoon ',0,2,1,2,1,NULL,NULL,NULL,0),(43,2,1,2,NULL,'Call Client Evening ',0,2,1,2,1,NULL,NULL,NULL,0),(44,2,1,2,NULL,'follow up call with nugget',4,2,1,2,1,NULL,NULL,NULL,0),(45,2,1,2,NULL,'follow up phone call with nugget',14,2,1,2,1,NULL,NULL,NULL,0),(46,2,1,2,NULL,'follow up phone call',28,2,1,2,1,NULL,NULL,NULL,0),(47,2,1,2,NULL,'Call Client (every 2 wks) (Day 60 - Day 90)',60,2,1,2,1,NULL,NULL,NULL,0),(48,2,1,4,NULL,'\"\"\"Hey do you have a sec?\"\", \"\"R u busy?\"\" \"-morning text (Day 0- Day 7)',0,2,1,2,1,NULL,NULL,NULL,1),(49,2,1,4,NULL,'\"\"\"Hey do you have a sec?\"\", \"\"R u busy?\"\" \"-evening text (Day 0- Day 7)',0,2,1,2,1,NULL,NULL,NULL,1),(50,2,1,4,NULL,'\"Do you have a sec- have great info on ________\" ',4,2,1,2,1,NULL,NULL,NULL,0),(51,2,1,2,NULL,'follow up phone call',21,2,1,2,1,NULL,NULL,NULL,0),(52,2,1,4,NULL,'\"Call me ASAP\"',29,2,1,2,1,NULL,NULL,NULL,1),(53,2,1,4,NULL,'\"Name, is this your number?\" (every 3 days) (Day 14 - Day 28)',14,2,1,2,1,NULL,NULL,NULL,1),(54,2,1,4,NULL,'\"Call me ASAP\"',29,2,1,2,1,NULL,NULL,NULL,0),(55,2,1,4,NULL,'\"Are you at work?\" (every 2wks) (Day 60 - Day 90)',60,2,1,2,1,NULL,NULL,NULL,0),(56,2,1,1,NULL,'Change Lead Type Immediately',0,2,1,2,1,NULL,NULL,NULL,0),(57,4,1,1,NULL,'Verify Social Networking Facebook, Twitter, Linkedin',0,2,1,4,1,NULL,NULL,NULL,0),(58,4,1,1,NULL,'look up Property in tax record/MLS',0,2,1,4,1,NULL,NULL,NULL,0),(59,4,1,1,NULL,'Call Client Morning (days 0-7)',0,2,1,4,1,NULL,NULL,NULL,0),(60,4,1,2,NULL,'Call Client Evening (days 0-7)',0,2,1,4,1,NULL,NULL,NULL,0),(61,4,1,2,NULL,'Call Client (once/day) (days 8-14)',7,2,1,4,1,NULL,NULL,NULL,0),(62,4,1,2,NULL,'Call Client (every several days) (Day 14 - Day 28)',14,2,1,4,1,NULL,NULL,NULL,0),(63,4,1,2,NULL,'Call Client (once/wk) (Days 29 - Day 59)',29,2,1,4,1,NULL,NULL,NULL,0),(64,4,1,2,NULL,'Call Client (every 2 wks) (Day 60 - Day 90)',60,2,1,4,1,NULL,NULL,NULL,0),(65,4,1,4,NULL,'\"\"\"Hey do you have a sec?\"\", \"\"R u busy?\"\" \"-evening text (Day 0- Day 7)',0,2,1,4,1,NULL,NULL,NULL,0),(66,4,1,4,NULL,'\"\"\"Hey do you have a sec?\"\", \"\"R u busy?\"\" \"-morning text (Day 0- Day 7)',0,2,1,4,1,NULL,NULL,NULL,0),(67,4,1,4,NULL,'\"Do you have a sec?\" (once/day) (days 8-14)',8,2,1,4,1,NULL,NULL,NULL,0),(68,4,1,4,NULL,'\"Name, is this your number?\" (every 3 days) (Day 14 - Day 28)',14,2,1,4,1,NULL,NULL,NULL,0),(69,4,1,2,NULL,'\"Call me ASAP\" (once/wk) (Day 29 - Day 59)',29,2,1,4,1,NULL,NULL,NULL,0),(70,4,1,4,NULL,'\"Are you at work?\" (every 2wks) (Day 60 - Day 90)',60,2,1,4,1,NULL,NULL,NULL,0),(71,4,1,1,NULL,'Change Lead Type Immediately',0,2,1,4,1,NULL,NULL,NULL,0),(72,9,1,1,NULL,'Send Congrats Email and  Executed Contract to Client',1,2,4,11,3,NULL,NULL,NULL,0),(73,9,1,1,NULL,'Copy of Binder and Verification of Deposit by Escrow',3,2,6,11,1,NULL,80,NULL,0),(74,9,1,1,NULL,'Verify HOA & CDD Discosure',0,2,1,11,3,NULL,NULL,NULL,1),(75,9,1,1,NULL,'Verify ALL docs included with contract (Sellers Disclosure, HOA, CDD, Amendments, Binder)',1,2,4,11,3,NULL,NULL,NULL,0),(76,9,1,1,NULL,'Review Contract - signatures, initials, terms',1,2,4,11,3,NULL,NULL,NULL,0),(77,9,1,1,NULL,'Congrats Email to Team! =)',1,2,4,11,3,NULL,NULL,NULL,0),(78,9,1,1,NULL,'Complete Contract Contact Sheet',2,2,4,11,3,NULL,NULL,NULL,0),(79,9,1,1,NULL,'Enter into KW Paperless',5,2,6,11,3,NULL,73,NULL,0),(80,9,1,1,NULL,'Send Fully Executed Contract to Client, Co-Op Agent, Title, Lender',2,2,4,11,3,NULL,NULL,NULL,0),(81,9,1,1,NULL,'Verify receipt of binder to title company',3,2,4,11,1,NULL,NULL,NULL,0),(82,9,1,1,NULL,'Send Intro Email to Lender (Contract, Contact Info, Title Company)',2,2,4,11,3,NULL,NULL,NULL,0),(83,9,1,1,NULL,'Call/Email Intro to Buyer\'s Agent with copy of contract',1,2,6,11,3,NULL,76,NULL,0),(84,9,1,1,NULL,'Is Seller a Buyer also?  (Assign to agent)',1,2,4,11,3,NULL,NULL,NULL,0),(85,9,1,1,NULL,'Intro to Title Company',1,2,4,11,1,NULL,NULL,NULL,0),(86,9,1,1,NULL,'Intro to Co-Op Agent',1,2,1,11,1,NULL,NULL,NULL,0),(87,9,1,1,NULL,'Intro to Lender (Send Copy of Executed Contract)',1,2,4,11,1,NULL,NULL,NULL,0),(88,9,1,1,NULL,'Send buyer termite bond transfer info if applicable',15,2,4,11,1,NULL,NULL,NULL,0),(89,9,1,1,NULL,'Order Home Inspection and WDO Inspection',1,2,4,11,1,NULL,NULL,NULL,0),(90,9,1,1,NULL,'WDO Ordered',1,2,1,11,1,NULL,NULL,NULL,1),(91,9,1,1,NULL,'Draft Request for Repairs',5,2,6,11,1,NULL,89,NULL,0),(92,9,1,1,NULL,'Request for Repairs Submitted',1,2,6,11,3,NULL,91,NULL,0),(93,9,1,1,NULL,'Received Fully Signed Repair Agreement',5,2,6,11,1,NULL,92,NULL,0),(94,9,1,1,NULL,'Schedule Closing Time w/Buyer & Seller',5,1,5,11,1,NULL,NULL,NULL,0),(95,9,1,1,NULL,'Confirm Seller Repairs Complete',10,2,6,11,3,NULL,93,NULL,0),(96,9,1,1,NULL,'WDO Re-inspect',1,2,6,11,3,NULL,95,NULL,0),(97,9,1,1,NULL,'Schedule Walk-thru',3,1,5,11,3,NULL,NULL,NULL,0),(98,9,1,1,NULL,'Perform Final walk-thru',2,1,5,11,3,NULL,97,NULL,0),(99,9,1,1,NULL,'Lender has all req buyer docs (Day 3) After call & email intro to lender',3,2,1,11,1,NULL,87,NULL,0),(100,9,1,1,NULL,'Verify loan has been submitted to UW after lender has all required docs',7,2,4,11,1,NULL,NULL,NULL,0),(101,9,1,1,NULL,'Order Appraisal',0,2,6,11,3,NULL,98,NULL,1),(102,9,1,1,NULL,'Loan out of U/W, verify conditions (Day 7)',15,2,4,11,1,NULL,NULL,NULL,0),(103,9,1,3,NULL,'Email \"Are we on track?\" w/closing date',15,2,4,11,1,NULL,NULL,NULL,0),(104,9,1,1,NULL,'Confirm Conditions Cleared on Loan',30,2,4,11,1,NULL,NULL,NULL,0),(105,9,1,1,NULL,'Send Utilities Letter',20,2,1,11,1,NULL,NULL,NULL,1),(106,9,1,1,NULL,'Schedule Testimonial at Closing',2,1,5,11,1,NULL,NULL,NULL,0),(107,9,1,1,NULL,'Review HUD',1,1,5,11,1,NULL,104,NULL,0),(108,9,1,1,NULL,'HUD, Survey, MLS, Closing Docs in File',3,2,5,11,1,NULL,0,NULL,0),(109,9,1,1,NULL,'Print HUD Letter & Envelope, Add to stack',4,2,5,11,1,NULL,108,NULL,0),(110,9,1,1,NULL,'KW Paperless (HUD, Survey, WDO, Check)',3,2,5,11,1,NULL,NULL,NULL,0),(111,9,1,1,NULL,'Enter Dates in Admin, Closing Excel Docs, HUD Letter',5,2,6,11,1,NULL,94,NULL,0),(112,9,1,1,NULL,'Initiate Closing Celebration & F/U Plan',0,2,6,11,1,NULL,104,NULL,1),(113,9,1,1,NULL,'Send Just Sold Postcards',0,2,6,11,1,NULL,104,NULL,1),(114,9,1,1,NULL,'Change Client\'s Status to PAST CLIENT in Admin',3,2,5,11,3,NULL,111,NULL,0),(115,9,1,1,NULL,'Just Sold Sign in  \"Another House Sold…',0,2,6,11,3,NULL,111,NULL,1),(116,9,1,1,NULL,'If Referral, send Referrer Note & Gift',3,2,4,11,3,NULL,111,NULL,0),(117,1,1,2,NULL,'Sphere Monthly Touch',30,2,1,13,1,NULL,NULL,NULL,0),(118,1,1,2,NULL,'Sphere Monthly Touch',60,2,1,13,1,NULL,NULL,NULL,1),(119,1,1,2,NULL,'Sphere Monthly Touch',90,2,1,13,1,NULL,NULL,NULL,1),(120,1,1,2,NULL,'Sphere Monthly Touch',120,2,1,13,1,NULL,NULL,NULL,1),(121,1,1,2,NULL,'Sphere Monthly Touch',150,2,1,13,1,NULL,NULL,NULL,1),(122,1,1,2,NULL,'Sphere Monthly Touch',180,2,1,13,1,NULL,NULL,NULL,1),(123,1,1,2,NULL,'Sphere Monthly Touch',210,2,1,13,1,NULL,NULL,NULL,1),(124,1,1,2,NULL,'Sphere Monthly Touch',240,2,1,13,1,NULL,NULL,NULL,1),(125,1,1,2,NULL,'Sphere Monthly Touch',270,2,1,13,1,NULL,NULL,NULL,1),(126,1,1,2,NULL,'Sphere Monthly Touch',300,2,1,13,1,NULL,NULL,NULL,1),(127,1,1,2,NULL,'Sphere Monthly Touch',330,2,1,13,1,NULL,NULL,NULL,1),(128,1,1,2,NULL,'Sphere Monthly Touch',360,2,1,13,1,NULL,NULL,NULL,1),(129,11,1,2,NULL,'Closing Day Phone Call- remind and ask customer to do closing survey',0,2,1,13,3,NULL,NULL,NULL,0),(130,11,1,2,NULL,'Closing 1 Week Follow-up- how is everything, any questions, follow up on survey (if necessary)',7,2,1,13,3,NULL,NULL,NULL,0),(131,11,1,2,NULL,'Closing 2 Week Follow-up- how is everything, any questions, follow up on survey (if necessary)',14,2,1,13,3,NULL,NULL,NULL,0),(132,11,1,2,NULL,'Closing 1 Month Follow-up- how is everything, any questions, follow up on survey (if necessary)',30,2,1,13,3,NULL,NULL,NULL,0),(133,11,1,2,NULL,'Closing 45 Day closing touch- answer any questions, thank for business, ask for referrals',45,2,1,13,3,NULL,NULL,NULL,0),(134,2,1,3,NULL,'New Lead E-mail',0,2,1,2,1,NULL,NULL,NULL,1),(135,2,1,3,NULL,'Homes I sent You Email',3,2,1,2,1,NULL,NULL,NULL,1),(136,2,1,3,6,'School Districts E-mail',0,2,1,2,1,NULL,NULL,NULL,0),(137,2,1,3,NULL,'Email top list of neighborhoods',5,2,1,2,1,NULL,NULL,NULL,0),(138,2,1,3,8,'What are CDD Fees?',11,2,1,2,1,NULL,NULL,NULL,0),(139,2,1,3,NULL,'List of Top Neighborhoods',20,2,1,2,1,NULL,NULL,NULL,1),(140,2,1,3,NULL,'Time Flies...Checking In E-mail',30,2,1,2,1,NULL,NULL,NULL,0),(141,2,1,3,NULL,'Saving On Your Home...',40,2,1,2,1,NULL,NULL,NULL,0),(142,2,1,3,NULL,'A Home I Saw...',50,2,1,2,1,NULL,NULL,NULL,0),(143,2,1,3,NULL,'Ready to see homes?',60,2,1,2,1,NULL,NULL,NULL,0),(144,2,1,3,NULL,'Ready for a Great Deal?...',70,2,1,2,1,NULL,NULL,NULL,0),(145,2,1,3,NULL,'Time the Market...',80,2,1,2,1,NULL,NULL,NULL,0),(146,2,1,3,NULL,'Financing Oops!...',90,2,1,2,1,NULL,NULL,NULL,0),(147,2,1,3,NULL,'Quick Review...',100,2,1,2,1,NULL,NULL,NULL,0),(148,2,1,3,NULL,'Top Schools & Golf Communities',110,2,1,2,1,NULL,NULL,NULL,0),(149,2,1,3,NULL,'The Truth about Zillow...',120,2,1,2,1,NULL,NULL,NULL,0),(150,2,1,3,NULL,'Fixing up a Home...',130,2,1,2,1,NULL,NULL,NULL,0),(151,2,1,3,NULL,'The \"F\" Word...Foreclosure!',140,2,1,2,1,NULL,NULL,NULL,0),(152,2,1,3,NULL,'Checking In... Market Update',150,2,1,2,1,NULL,NULL,NULL,0),(153,2,1,3,NULL,'Homeowner\'s Insurance',160,2,1,2,1,NULL,NULL,NULL,0),(154,2,1,3,NULL,'My Top pros & contractors',170,2,1,2,1,NULL,NULL,NULL,0),(155,2,1,3,NULL,'New Construction Pitfalls',180,2,1,2,1,NULL,NULL,NULL,0),(156,2,1,3,NULL,'Ready to See Homes?',190,2,1,2,1,NULL,NULL,NULL,0),(157,2,1,3,NULL,'Lending Myths',200,2,1,2,1,NULL,NULL,NULL,0),(158,2,1,3,NULL,'Staging Your Home to Sell',210,2,1,2,1,NULL,NULL,NULL,0),(159,2,1,3,NULL,'Foreclosure vs. Short Sale?...',220,2,1,2,1,NULL,NULL,NULL,0),(160,2,1,3,NULL,'Touching Base...Time Flies',230,2,1,2,1,NULL,NULL,NULL,0),(161,2,1,3,NULL,'Closing Costs',240,2,1,2,1,NULL,NULL,NULL,0),(162,2,1,3,NULL,'This could be you...',250,2,1,2,1,NULL,NULL,NULL,0),(163,2,1,3,NULL,'A Home I Saw...',260,2,1,2,1,NULL,NULL,NULL,0),(164,2,1,3,NULL,'Quick Review...',270,2,1,2,1,NULL,NULL,NULL,0),(165,2,1,3,NULL,'Home Inspection - Saving or costing you?',280,2,1,2,1,NULL,NULL,NULL,0),(166,2,1,3,NULL,'Homebuyer Survival Kit',290,2,1,2,1,NULL,NULL,NULL,0),(167,2,1,3,NULL,'Knowing House Values',300,2,1,2,1,NULL,NULL,NULL,0),(168,2,1,3,NULL,'Ready to see homes?',310,2,1,2,1,NULL,NULL,NULL,0),(169,2,1,3,NULL,'Financing Oops...',320,2,1,2,1,NULL,NULL,NULL,0),(170,2,1,3,NULL,'Foreclosure vs. Short Sales',330,2,1,2,1,NULL,NULL,NULL,0),(171,2,1,3,NULL,'Reducing Property Taxes....',340,2,1,2,1,NULL,NULL,NULL,0),(172,2,1,3,NULL,'Quick Question...',350,2,1,2,1,NULL,NULL,NULL,0),(173,2,1,3,NULL,'Home Updates...Time Flies',360,2,1,2,1,NULL,NULL,NULL,0),(174,2,1,3,NULL,'Fixing up a Home...',375,2,1,2,1,NULL,NULL,NULL,0),(175,2,1,3,NULL,'What are CDD Fees?',390,2,1,2,1,NULL,NULL,NULL,0),(176,2,1,3,NULL,'Top Schools & Golf Communities',405,2,1,2,1,NULL,NULL,NULL,0),(177,2,1,3,NULL,'Time Flies...Checking In (version Yr 2-5)',420,2,1,2,1,NULL,NULL,NULL,0),(178,2,1,3,NULL,'A Home I Saw...',435,2,1,2,1,NULL,NULL,NULL,0),(179,2,1,3,NULL,'Ready for a Great Deal?...',450,2,1,2,1,NULL,NULL,NULL,0),(180,2,1,3,NULL,'The Truth about Zillow...',465,2,1,2,1,NULL,NULL,NULL,0),(181,2,1,3,NULL,'Ready to see homes?',480,2,1,2,1,NULL,NULL,NULL,0),(182,2,1,3,NULL,'Timing the Market...',495,2,1,2,1,NULL,NULL,NULL,0),(183,2,1,3,NULL,'Financing Oops... ?',510,2,1,2,1,NULL,NULL,NULL,0),(184,2,1,3,NULL,'Relocation Package',525,2,1,2,1,NULL,NULL,NULL,0),(185,2,1,3,NULL,'Quick Review...',540,2,1,2,1,NULL,NULL,NULL,0),(186,2,1,3,NULL,'Top Schools & Golf Communities',555,2,1,2,1,NULL,NULL,NULL,0),(187,2,1,3,NULL,'New Construction Pitfalls',570,2,1,2,1,NULL,NULL,NULL,0),(188,2,1,3,NULL,'Staging Your Home to Sell',585,2,1,2,1,NULL,NULL,NULL,0),(189,2,1,3,NULL,'Checking In... Market Update',600,2,1,2,1,NULL,NULL,NULL,0),(190,2,1,3,NULL,'Homeowner\'s Insurance',615,2,1,2,1,NULL,NULL,NULL,0),(191,2,1,3,NULL,'Foreclosure vs. Short Sale',630,2,1,2,1,NULL,NULL,NULL,0),(192,2,1,3,NULL,'My Top pros & contractors',645,2,1,2,1,NULL,NULL,NULL,0),(193,2,1,3,NULL,'Ready to see homes?',660,2,1,2,1,NULL,NULL,NULL,0),(194,2,1,3,NULL,'Knowing House Values',675,2,1,2,1,NULL,NULL,NULL,0),(195,2,1,3,NULL,'Lending Myths',690,2,1,2,1,NULL,NULL,NULL,0),(196,2,1,3,NULL,'This could be you...',705,2,1,2,1,NULL,NULL,NULL,0),(197,2,1,3,NULL,'Foreclosure vs. Short Sale',720,2,1,2,1,NULL,NULL,NULL,0),(198,2,1,3,NULL,'Quick Review...',735,2,1,2,1,NULL,NULL,NULL,0),(199,14,1,1,NULL,'Internet Research - Google, Facebook, LinkedIn, Tax Records',0,2,1,12,1,27905,NULL,NULL,0),(200,14,1,1,NULL,'Setup Market Update',0,2,1,12,1,27905,NULL,NULL,0),(201,14,1,1,NULL,'Apply Action Plan for Buyer Specialist',0,2,1,12,1,27905,NULL,NULL,0),(202,15,1,1,NULL,'Review Contract Package - Ensure Completion',1,2,4,11,1,NULL,NULL,NULL,1),(203,13,1,2,NULL,'FSBO Week 1 Follow-up',7,2,1,13,1,NULL,NULL,NULL,0),(204,13,1,2,NULL,'FSBO Week 2 Follow-up',14,2,1,13,1,NULL,NULL,NULL,0),(205,13,1,2,NULL,'FSBO Week 3 Follow-up',21,2,1,13,1,NULL,NULL,NULL,0),(206,13,1,2,NULL,'FSBO Week 4 Follow-up',28,2,1,13,1,NULL,NULL,NULL,0),(207,13,1,2,NULL,'FSBO Week 5 Follow-up',35,2,1,13,1,NULL,NULL,NULL,0),(208,13,1,2,NULL,'FSBO Week 6 Follow-up',42,2,1,13,1,NULL,NULL,NULL,0),(209,13,1,2,NULL,'FSBO Week 7 Follow-up',49,2,1,13,1,NULL,NULL,NULL,0),(210,13,1,2,NULL,'FSBO Week 8 Follow-up',56,2,1,13,1,NULL,NULL,NULL,0),(211,13,1,2,NULL,'FSBO Week 9 Follow-up',63,2,1,13,1,NULL,NULL,NULL,0),(212,13,1,2,NULL,'FSBO Week 10 Follow-up',70,2,1,13,1,NULL,NULL,NULL,0),(213,13,1,2,NULL,'FSBO Week 11 Follow-up',77,2,1,13,1,NULL,NULL,NULL,0),(214,13,1,2,NULL,'FSBO Week 12 Follow-up',84,2,1,13,1,NULL,NULL,NULL,0),(215,13,1,2,NULL,'FSBO Week 13 Follow-up',91,2,1,13,1,NULL,NULL,NULL,0),(216,13,1,2,NULL,'FSBO Week 14 Follow-up',98,2,1,13,1,NULL,NULL,NULL,0),(217,13,1,2,NULL,'FSBO Week 15 Follow-up',105,2,1,13,1,NULL,NULL,NULL,0),(218,13,1,2,NULL,'FSBO Week 16 Follow-up',112,2,1,13,1,NULL,NULL,NULL,0),(219,13,1,2,NULL,'FSBO Week 17 Follow-up',119,2,1,13,1,NULL,NULL,NULL,0),(220,13,1,2,NULL,'FSBO Week 18 Follow-up',126,2,1,13,1,NULL,NULL,NULL,0),(221,13,1,2,NULL,'FSBO Week 19 Follow-up',133,2,1,13,1,NULL,NULL,NULL,0),(222,13,1,2,NULL,'FSBO Week 20 Follow-up',140,2,1,13,1,NULL,NULL,NULL,0),(223,16,1,2,NULL,'Weekly update call',7,2,1,6,3,27554,NULL,NULL,0),(224,16,1,2,NULL,'Weekly update call',14,2,1,6,3,NULL,NULL,NULL,0),(225,16,1,2,NULL,'Weekly update call ',21,2,1,6,3,NULL,NULL,NULL,0),(226,16,1,2,NULL,'Weekly update call- price adjustment OK from seller',28,2,1,6,3,NULL,NULL,NULL,0),(227,16,1,3,NULL,'Prepare and send market status update- price adjustment prep talk',28,2,1,6,3,NULL,NULL,NULL,0),(228,16,1,2,NULL,'Weekly update call',35,2,1,6,3,NULL,NULL,NULL,0),(229,16,1,2,NULL,'Weekly update call',42,2,1,6,3,NULL,NULL,NULL,0),(230,16,1,2,NULL,'Weekly update call ',49,2,1,6,3,NULL,NULL,NULL,0),(231,16,1,2,NULL,'Weekly update call- price adjustment OK from seller',56,2,1,6,3,NULL,NULL,NULL,0),(232,16,1,3,NULL,'Prepare and send market status update- price adjustment OK from seller',56,2,1,6,3,NULL,NULL,NULL,0),(233,16,1,2,NULL,'Weekly update call',63,2,1,6,3,NULL,NULL,NULL,1),(234,16,1,2,NULL,'Weekly update call',63,2,1,6,3,NULL,NULL,NULL,0),(235,16,1,2,NULL,'Weekly update call',70,2,1,6,3,NULL,NULL,NULL,0),(236,16,1,2,NULL,'Weekly update call',77,2,1,6,3,NULL,NULL,NULL,0),(237,16,1,2,NULL,'Weekly update call- price adjustment OK from seller',84,2,1,6,3,NULL,NULL,NULL,0),(238,16,1,3,NULL,'Prepare and send market status update- price adjustment prep talk',84,2,1,6,3,NULL,NULL,NULL,0),(239,16,1,1,NULL,'Re-launch \"Listing Market Update\" action plan',90,2,1,6,3,NULL,NULL,NULL,0),(240,16,1,2,NULL,'Weekly update call - price adjustment prep talk',52,2,1,6,3,NULL,NULL,NULL,1),(241,16,1,3,NULL,'Prepare and send market status update',14,2,1,6,3,NULL,NULL,NULL,0),(242,16,1,3,NULL,'Prepare and send market status update',28,2,1,6,3,NULL,NULL,NULL,1),(243,16,1,3,NULL,'Prepare and send market status update',42,2,1,6,3,NULL,NULL,NULL,0),(244,16,1,3,NULL,'Prepare and send market status update',70,2,1,6,3,NULL,NULL,NULL,0),(245,16,1,2,NULL,'Weekly update call',90,2,1,6,3,NULL,NULL,NULL,0),(246,17,1,1,NULL,'Implement coming soon program',0,2,1,6,3,NULL,NULL,NULL,1),(247,17,1,1,NULL,'Put together paper and electronic folders',0,2,1,6,3,NULL,NULL,NULL,0),(248,17,1,1,NULL,'Give up front fees to Sherri',0,2,1,6,3,NULL,NULL,NULL,0),(249,17,1,1,NULL,'Verify all paperwork is execuated properly and received- stamp contracts',0,2,1,6,3,NULL,NULL,NULL,0),(250,17,1,1,NULL,'Notify investors',0,2,1,6,3,NULL,NULL,NULL,0),(251,17,1,1,NULL,'Send runner to place coming soon sign and lock box with 1 key (extra keys go in file)',0,2,1,6,3,NULL,NULL,NULL,0),(252,17,1,2,NULL,'Schedule staging and photography',0,2,1,6,3,NULL,NULL,NULL,0),(253,17,1,1,NULL,'Enter property into MLS and save as incomplete- send link to listing agent for approval',0,2,1,6,3,NULL,NULL,NULL,0),(254,17,1,1,NULL,'Assign IVR#',0,2,1,6,3,NULL,NULL,NULL,1),(255,17,1,1,NULL,'Verify listing is in Admin and all information is filled out correctly',0,2,1,6,3,NULL,NULL,NULL,0),(256,17,1,2,NULL,'Intro call to seller- confirm all contact and HOA/CDD info',0,2,1,6,3,NULL,NULL,NULL,0),(257,17,1,3,NULL,'Send email to team to inform of new coming soon property',0,2,1,6,3,NULL,NULL,NULL,0),(258,17,1,1,NULL,'Add listing to whiteboard',0,2,1,6,3,NULL,NULL,NULL,0),(259,17,1,1,NULL,'Prepare coming soon cards and give to listing agent for approval',0,2,1,6,3,NULL,NULL,NULL,0),(260,17,1,1,NULL,'Send coming soon cards',1,2,1,6,3,NULL,NULL,NULL,0),(261,17,1,1,NULL,'Prepare Internet copy/flyer and give to listing agent for approval',0,2,1,6,3,NULL,NULL,NULL,0),(262,17,1,1,NULL,'Post property to Facebook, YouTube, Pinterest, Twitter, etc',1,2,1,6,3,NULL,NULL,NULL,0),(263,17,1,1,NULL,'Scan all documents',0,2,1,6,3,NULL,NULL,NULL,0),(264,17,1,1,NULL,'Assign and record message for IVR#',0,2,1,6,3,NULL,NULL,NULL,0),(265,17,1,6,NULL,'Send thank you to seller',1,2,1,4,3,NULL,NULL,NULL,0),(266,6,1,2,NULL,'Introduction call to sellers',0,2,1,6,3,NULL,NULL,NULL,0),(267,6,1,1,NULL,'Notify investors',0,2,1,6,3,NULL,NULL,NULL,0),(268,9,1,1,NULL,'Send surveys to client, title company and lender',30,2,1,11,3,NULL,NULL,NULL,0),(269,18,1,3,2,'Email confirmation of appointment to all parties',0,2,1,2,1,NULL,NULL,NULL,0),(270,18,1,2,NULL,'Call all parties to confirm receipt of email- give nugget, dialog about coming soon properties',1,2,1,2,1,NULL,NULL,NULL,0),(271,18,1,1,NULL,'set reminder to call 1 day before appointment to confirm',1,2,1,2,1,NULL,NULL,NULL,0),(272,20,1,6,NULL,'write thank you note and mail',0,2,1,2,1,NULL,NULL,NULL,0),(273,20,1,2,NULL,'Weekly call with nugget',7,2,1,2,1,NULL,NULL,NULL,0),(274,20,1,2,NULL,'Weekly call with nugget',14,2,1,2,1,NULL,NULL,NULL,0),(275,20,1,2,NULL,'Weekly call with nugget',21,2,1,2,1,NULL,NULL,NULL,0),(276,20,1,2,NULL,'Weekly call with nugget',28,2,1,2,1,NULL,NULL,NULL,0),(277,20,1,2,NULL,'Weekly call with nugget',35,2,1,2,1,NULL,NULL,NULL,0),(278,20,1,2,NULL,'Weekly call with nugget',42,2,1,2,1,NULL,NULL,NULL,0),(279,20,1,2,NULL,'Weekly call with nugget',49,2,1,2,1,NULL,NULL,NULL,0),(280,20,1,2,NULL,'Weekly call with nugget',64,2,1,2,1,NULL,NULL,NULL,0),(281,20,1,2,NULL,'Weekly call with nugget',71,2,1,2,1,NULL,NULL,NULL,0),(282,20,1,2,NULL,'Weekly call with nugget',78,2,1,2,1,NULL,NULL,NULL,0),(283,20,1,2,NULL,'Weekly call with nugget',90,2,1,2,1,NULL,NULL,NULL,0),(284,20,1,2,NULL,'Weekly call with nugget',85,2,1,2,1,NULL,NULL,NULL,0),(285,20,1,1,NULL,'buyer questionnaire',0,2,1,2,1,NULL,NULL,NULL,0),(286,20,1,1,NULL,'send/deliver homebuyer guide',0,2,1,2,1,NULL,NULL,NULL,0),(287,2,1,2,NULL,'follow up call and leave interest rate nugget',2,2,1,2,1,NULL,NULL,NULL,0),(288,2,1,3,5,'Email homes I sent you template',2,2,1,2,1,NULL,NULL,NULL,0),(289,2,1,4,NULL,'Text \"did you get the home I sent you?\"',2,2,1,2,1,NULL,NULL,NULL,0),(290,2,1,3,NULL,'email KW app info ',3,2,1,2,1,NULL,NULL,NULL,0),(291,2,1,2,NULL,'follow up call with nugget',5,2,1,2,1,NULL,NULL,NULL,0),(292,2,1,4,NULL,'text \"I found a home for you in_____\"',7,2,1,2,1,NULL,NULL,NULL,0),(293,2,1,2,NULL,'follow up phone call with nugget',9,2,1,2,1,NULL,NULL,NULL,0),(294,21,1,2,NULL,'Initial Phone Call- within 5 minutes, No voice mail',0,2,1,2,1,NULL,NULL,NULL,0),(295,21,1,3,6,'School District E-mail',0,2,1,2,1,NULL,NULL,NULL,0),(296,21,1,2,NULL,'Call Client afternoon- No voice mail',0,2,1,2,1,NULL,NULL,NULL,0),(297,21,1,2,NULL,'Call Client evening- leave voice mail',0,2,1,2,1,NULL,NULL,NULL,0),(298,21,1,2,NULL,'Follow up- leave nugget',2,2,1,2,1,NULL,NULL,NULL,0),(299,21,1,3,5,'Email Homes I sent you templet',2,2,1,2,1,NULL,NULL,NULL,0),(300,21,1,4,NULL,'Text: \"Got a coming soon.. sneak peak?\"',2,2,1,2,1,NULL,NULL,NULL,0),(301,21,1,2,NULL,'Follow up- leave nugget',3,2,1,2,1,NULL,NULL,NULL,0),(302,21,1,3,NULL,'Email KW app info',3,2,1,2,1,NULL,NULL,NULL,0),(303,21,1,2,NULL,'Follow up- leave nugget',4,2,1,2,1,NULL,NULL,NULL,0),(304,21,1,4,NULL,'Text: \"I found a home for you in__________\"',4,2,1,2,1,NULL,NULL,NULL,0),(305,21,1,2,NULL,'Follow up- leave nugget',5,2,1,2,1,NULL,NULL,NULL,0),(306,21,1,3,9,'Email top list of neighborhoods',5,2,1,2,1,NULL,NULL,NULL,0),(307,21,1,2,NULL,'Follow up- leave nugget',8,2,1,2,1,NULL,NULL,NULL,0),(308,21,1,3,7,'Email: Market Updates',10,2,1,2,1,NULL,NULL,NULL,0),(309,21,1,4,NULL,'Text: \"I have great info on_______\"',12,2,1,2,1,NULL,NULL,NULL,0),(310,21,1,2,NULL,'Follow up- leave nugget',17,2,1,2,1,NULL,NULL,NULL,0),(311,21,1,2,NULL,'Follow up- leave nugget',24,2,1,2,1,NULL,NULL,NULL,0),(312,21,1,2,NULL,'Follow up- leave nugget',60,2,2,2,1,NULL,NULL,NULL,0),(313,21,1,2,NULL,'Follow up- leave nugget',90,2,1,2,1,NULL,NULL,NULL,0),(314,21,1,2,NULL,'Follow up- leave nugget',120,2,1,2,1,NULL,NULL,NULL,0),(315,21,1,2,NULL,'Follow up- leave nugget',150,2,1,2,1,NULL,NULL,NULL,0),(316,21,1,2,NULL,'Follow up- leave nugget',180,2,1,2,1,NULL,NULL,NULL,0),(317,21,1,2,NULL,'Follow up- leave nugget',210,2,1,2,1,NULL,NULL,NULL,0),(318,21,1,2,NULL,'Follow up- leave nugget',240,2,1,2,1,NULL,NULL,NULL,0),(319,21,1,2,NULL,'Follow up- leave nugget',270,2,1,2,1,NULL,NULL,NULL,0),(320,21,1,2,NULL,'Follow up- leave nugget',300,2,1,2,1,NULL,NULL,NULL,0),(321,21,1,2,NULL,'Follow up- leave nugget',330,2,2,2,1,NULL,NULL,NULL,0),(322,21,1,2,NULL,'Follow up- leave nugget',360,2,1,2,1,NULL,NULL,NULL,0),(323,22,1,3,NULL,'Email a confirmation with the addresses to the office and time of appointment',0,2,1,2,1,NULL,NULL,NULL,0),(324,22,1,2,NULL,'Ask about the homes I sent.....',14,2,1,2,1,NULL,NULL,NULL,0),(325,22,1,3,5,'Ask about the homes I sent you...',14,2,1,2,1,NULL,NULL,NULL,0),(326,22,1,1,NULL,'Send a relocation packet',0,2,1,2,1,NULL,NULL,NULL,0),(327,22,1,2,NULL,'Homes I sent you.......',21,2,1,2,1,NULL,NULL,NULL,0),(328,22,1,3,5,'Homes I sent you.......',21,2,1,2,1,NULL,NULL,NULL,0),(329,22,1,2,NULL,'Homes I sent you.......',28,2,1,2,1,NULL,NULL,NULL,0),(330,22,1,3,5,'Homes I sent you.......',28,2,2,2,1,NULL,NULL,NULL,0),(331,22,1,2,NULL,'Confirm location and time of meeting',30,2,1,2,1,NULL,NULL,NULL,0),(332,22,1,3,NULL,'Confirm location and time of meeting',30,2,1,2,1,NULL,NULL,NULL,0),(333,22,1,9,NULL,'Get paper work ready',30,2,1,2,1,NULL,NULL,NULL,0),(334,22,1,10,NULL,'Get loyalty agreement signed',30,2,1,2,1,NULL,NULL,NULL,0),(335,22,1,1,NULL,'Get showings appointments scheduled ',28,2,1,2,1,NULL,NULL,NULL,0),(336,23,1,3,NULL,'Email a confirmation with the addresses to the office and time of appointment',0,2,1,2,1,NULL,NULL,NULL,0),(337,23,1,1,NULL,'Send a relocation packet',0,2,1,2,1,NULL,NULL,NULL,0),(338,23,1,2,NULL,'Ask about the homes I sent.....',14,2,1,2,1,NULL,NULL,NULL,0),(339,23,1,3,5,'Ask about the homes I sent.....',14,2,1,2,1,NULL,NULL,NULL,0),(340,23,1,2,NULL,'Confirm location and time of meeting',21,2,1,2,1,NULL,NULL,NULL,0),(341,23,1,3,NULL,'Confirm location and time of meeting',21,2,1,2,1,NULL,NULL,NULL,0),(342,23,1,9,NULL,'Get paper work raedy',21,2,1,2,1,NULL,NULL,NULL,0),(343,23,1,10,NULL,'Get loyalty agreement signed',21,2,1,2,1,NULL,NULL,NULL,0),(344,23,1,1,NULL,'Get showings appointments scheduled ',19,2,1,2,1,NULL,NULL,NULL,0),(345,22,1,1,NULL,'Mark met or not met in admin',31,2,1,2,1,NULL,NULL,NULL,0),(346,22,1,10,NULL,'Upload loyalty agreement',31,2,1,2,1,NULL,NULL,NULL,0),(347,23,1,1,NULL,'Mark met or not met in admin',22,2,1,2,1,NULL,NULL,NULL,0),(348,23,1,1,NULL,'Upload loyalty agreement',22,2,1,2,1,NULL,NULL,NULL,0),(349,24,1,3,NULL,'Email a confirmation with the addresses to the office and time of appointment',0,2,1,2,1,NULL,NULL,NULL,0),(350,24,1,1,NULL,'Send a relocation packet',0,2,1,2,1,NULL,NULL,NULL,0),(351,24,1,2,NULL,'Ask about the homes I sent.....',7,2,1,2,1,NULL,NULL,NULL,0),(352,24,1,3,5,'Ask about the homes I sent.....',7,2,1,2,1,NULL,NULL,NULL,0),(353,24,1,2,NULL,'Ask about the homes I sent.....',12,2,1,2,1,NULL,NULL,NULL,0),(354,24,1,3,5,'Ask about the homes I sent.....',12,2,1,2,1,NULL,NULL,NULL,0),(355,24,1,1,NULL,'Get showings appointments scheduled ',12,2,1,2,1,NULL,NULL,NULL,0),(356,24,1,2,NULL,'Confirm location and time of meeting',14,2,1,2,1,NULL,NULL,NULL,0),(357,24,1,3,NULL,'Confirm location and time of meeting',14,2,1,2,1,NULL,NULL,NULL,0),(358,24,1,9,NULL,'Get paper work raedy',14,2,1,2,1,NULL,NULL,NULL,0),(359,24,1,10,NULL,'Get loyalty agreement signed',14,2,1,2,1,NULL,NULL,NULL,0),(360,24,1,1,NULL,'Mark met or not met in admin',15,2,1,2,1,NULL,NULL,NULL,0),(361,24,1,1,NULL,'Upload loyalty agreement',15,2,1,2,1,NULL,NULL,NULL,0),(362,25,1,3,NULL,'Email a confirmation with the addresses to the office and time of appointment',0,2,1,2,1,NULL,NULL,NULL,0),(363,25,1,1,NULL,'Send a relocation packet',0,2,1,2,1,NULL,NULL,NULL,0),(364,25,1,2,NULL,'Ask about the homes I sent.....',5,2,1,2,1,NULL,NULL,NULL,0),(365,25,1,3,5,'Ask about the homes I sent.....',5,2,1,2,1,NULL,NULL,NULL,0),(366,25,1,1,NULL,'Get showings appointments scheduled',5,2,1,2,1,NULL,NULL,NULL,0),(367,25,1,2,NULL,'Confirm location and time of meeting',7,2,1,2,1,NULL,NULL,NULL,0),(368,25,1,3,NULL,'Confirm location and time of meeting',7,2,1,2,1,NULL,NULL,NULL,0),(369,25,1,9,NULL,'Get paper work raedy',7,2,1,2,1,NULL,NULL,NULL,0),(370,25,1,10,NULL,'Get loyalty agreement signed',7,2,1,2,1,NULL,NULL,NULL,0),(371,25,1,1,NULL,'Mark met or not met in admin',15,2,1,2,1,NULL,NULL,NULL,0),(372,25,1,1,NULL,'Upload loyalty agreement',15,2,1,2,1,NULL,NULL,NULL,0),(373,11,1,2,NULL,'Call and follow up on survey (read and answer over the phone if necessary)',3,2,1,13,3,NULL,NULL,NULL,0),(374,11,1,3,NULL,'Follow up on survey (if applicable)',10,2,1,13,3,NULL,NULL,NULL,0),(375,11,1,3,NULL,'Follow up on survey (if applicable)',21,2,1,13,3,NULL,NULL,NULL,0),(376,11,1,1,NULL,'Prepare tax letter and statement- place in box for mailing in January',0,2,1,13,3,NULL,NULL,NULL,0),(377,11,1,6,NULL,'Write a thank you note and mail immediately (agent)',0,2,1,12,3,NULL,NULL,NULL,0),(378,11,1,1,NULL,'Give closed home info to Marketing to create just closed post card',0,2,1,13,3,NULL,NULL,NULL,0),(379,11,1,1,NULL,'Create just closed post card and send for print',1,2,1,12,3,27554,NULL,NULL,0);



/*Table structure for table `action_plans` */



DROP TABLE IF EXISTS `action_plans`;



CREATE TABLE `action_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `name` varchar(75) DEFAULT '',
  `on_complete_plan_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  CONSTRAINT `action_plans_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `action_plans_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `action_plans` */



insert  into `action_plans`(`id`,`account_id`,`component_type_id`,`status_ma`,`name`,`on_complete_plan_id`,`is_deleted`) values (1,2,1,1,'Sphere Monthly Touch (30 days Forever)',1,0),(2,2,2,1,'New Buyer Lead Follow-up',NULL,0),(3,2,2,1,'Met Buyer Lead Follow-up',NULL,0),(4,2,3,1,'New Seller Lead Follow-up',NULL,0),(5,2,3,1,'Met Seller Lead Follow-up',NULL,0),(6,2,3,1,'Traditional Listing Plan',NULL,0),(7,2,3,1,'Short Sale Listing Plan',NULL,0),(8,2,3,1,'REO Listing Plan',NULL,0),(9,2,5,1,'Buyer and Seller Closing Plan',NULL,0),(10,2,5,0,'Short Sale Closing Plan',NULL,0),(11,2,1,1,'Post Closing Plan',NULL,0),(12,2,1,1,'Expired Follow-up Plan',NULL,0),(13,2,1,1,'FSBO Follow-up Plan',NULL,0),(14,2,2,1,'Concierge New Buyer Lead Initial Tasks',NULL,0),(15,2,5,0,'Under Contract Action Plan',NULL,0),(16,2,3,1,'Listing Market Update (only)',NULL,0),(17,2,3,1,'Coming soon program',NULL,0),(18,2,2,1,'Buyer appointment confirmation',NULL,0),(19,2,5,0,'Prepare for closing ',NULL,0),(20,2,2,1,'Post appointment touch',NULL,0),(21,2,2,1,'New Lead Follow-Up (concierge)',NULL,0),(22,2,2,1,'Post Set Appointment Action Plan- Relocate- 4 weeks  out',NULL,0),(23,2,2,1,'Post Set Appointment Action Plan- Relocate- 3 weeks out',NULL,0),(24,2,2,1,'Post Set Appointment Action Plan- Relocate- 2 weeks out',NULL,0),(25,2,2,1,'Post Set Appointment Action Plan- Relocate- 1 week out',NULL,0),(26,2,1,1,'Sphere Monthly Touch (45 days Forever)',26,0);



/*Table structure for table `active_guests` */



DROP TABLE IF EXISTS `active_guests`;



CREATE TABLE `active_guests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`),
  CONSTRAINT `active_guests_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='User Groups';



/*Data for the table `active_guests` */



/*Table structure for table `active_users` */



DROP TABLE IF EXISTS `active_users`;



CREATE TABLE `active_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `active_users_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='User Groups';



/*Data for the table `active_users` */



/*Table structure for table `activity_log` */



DROP TABLE IF EXISTS `activity_log`;



CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL COMMENT 'seller, buyer, contact',
  `component_id` int(11) DEFAULT NULL,
  `origin_type` int(11) DEFAULT NULL COMMENT 'manual, taks, website',
  `task_type_id` int(11) DEFAULT NULL,
  `activity_date` timestamp NULL DEFAULT NULL,
  `note` blob,
  `lead_gen_type_ma` tinyint(1) DEFAULT '0' COMMENT 'new or f/u',
  `is_spoke_to` tinyint(1) DEFAULT '0',
  `asked_for_referral` tinyint(1) NOT NULL DEFAULT '0',
  `is_public` tinyint(1) DEFAULT '0',
  `is_action_plan` tinyint(1) DEFAULT '0',
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `added_by` (`added_by`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `activity_date` (`activity_date`) USING BTREE,
  KEY `lead_gen_type_ma` (`lead_gen_type_ma`) USING BTREE,
  KEY `is_contact` (`is_spoke_to`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  CONSTRAINT `activity_log_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `activity_log_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `activity_log_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `activity_log_ibfk_4` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `activity_log` */



/*Table structure for table `activity_log_email_message_lu` */



DROP TABLE IF EXISTS `activity_log_email_message_lu`;



CREATE TABLE `activity_log_email_message_lu` (
  `activity_log_id` int(11) NOT NULL,
  `email_message_id` int(11) NOT NULL,
  PRIMARY KEY (`activity_log_id`,`email_message_id`)
) ENGINE=InnoDB;



/*Data for the table `activity_log_email_message_lu` */



/*Table structure for table `address_company_lu` */



DROP TABLE IF EXISTS `address_company_lu`;



CREATE TABLE `address_company_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `address_company_lu_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `address_company_lu_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `address_company_lu` */



/*Table structure for table `address_contact_lu` */



DROP TABLE IF EXISTS `address_contact_lu`;



CREATE TABLE `address_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `address_contact_lu_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `address_contact_lu_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `address_contact_lu` */


/*Table structure for table `addresses` */



DROP TABLE IF EXISTS `addresses`;



CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `address_type_ma` int(11) DEFAULT NULL,
  `address` varchar(127) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `state_id` (`state_id`),
  KEY `account_id` (`account_id`),
  KEY `address` (`address`) USING BTREE,
  -- CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `address_states` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `addresses_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `addresses` */



/*Table structure for table `api_access` */



DROP TABLE IF EXISTS `api_access`;



CREATE TABLE `api_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `request_limit` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `api_access` */



/*Table structure for table `api_requests` */



DROP TABLE IF EXISTS `api_requests`;



CREATE TABLE `api_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_access_id` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `api_requests` */



/*Table structure for table `appointments` */



DROP TABLE IF EXISTS `appointments`;



CREATE TABLE `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `set_by_id` int(11) NOT NULL,
  `met_by_id` int(11) NOT NULL,
  `met_status_ma` tinyint(1) NOT NULL DEFAULT '0',
  `is_signed` tinyint(1) DEFAULT NULL,
  `not_signed_reason` tinyint(1) DEFAULT NULL,
  `not_signed_reason_other` varchar(63) DEFAULT NULL,
  `set_activity_type_id` int(11) DEFAULT NULL,
  `set_on_datetime` timestamp NULL DEFAULT NULL,
  `set_for_datetime` timestamp NULL DEFAULT NULL,
  `location_ma` tinyint(1) DEFAULT NULL,
  `location_other` varchar(63) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `met_by_id` (`met_by_id`),
  KEY `set_by_id` (`set_by_id`),
  KEY `added_by` (`added_by`),
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `component_type_id` (`component_type_id`) USING BTREE,
  KEY `set_activity_type_id` (`set_activity_type_id`) USING BTREE,
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `added_by` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `component_type_id` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `met_by` FOREIGN KEY (`met_by_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `set_activity_type_id` FOREIGN KEY (`set_activity_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `set_by` FOREIGN KEY (`set_by_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `updated_by` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;


/*Table structure for table `assignment_logs` */



DROP TABLE IF EXISTS `assignment_logs`;



CREATE TABLE `assignment_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `old_assignment_type_id` int(11) NOT NULL,
  `new_assignment_type_id` int(11) NOT NULL,
  `old_assignee_contact_id` int(11) NOT NULL,
  `new_assignee_contact_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assignment_logs_ibfk_1` (`component_type_id`),
  KEY `assignment_logs_ibfk_2` (`old_assignment_type_id`),
  KEY `assignment_logs_ibfk_3` (`new_assignment_type_id`),
  KEY `assignment_logs_ibfk_4` (`old_assignee_contact_id`),
  KEY `assignment_logs_ibfk_5` (`new_assignee_contact_id`),
  KEY `assignment_logs_ibfk_6` (`added_by`),
  CONSTRAINT `assignment_logs_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignment_logs_ibfk_2` FOREIGN KEY (`old_assignment_type_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignment_logs_ibfk_3` FOREIGN KEY (`new_assignment_type_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignment_logs_ibfk_4` FOREIGN KEY (`old_assignee_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignment_logs_ibfk_5` FOREIGN KEY (`new_assignee_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignment_logs_ibfk_6` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `assignment_logs` */



/*Table structure for table `assignment_type_component_lu` */



DROP TABLE IF EXISTS `assignment_type_component_lu`;



CREATE TABLE `assignment_type_component_lu` (
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `assignment_type_id` int(11) DEFAULT NULL,
  KEY `assignment_type_id` (`assignment_type_id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  CONSTRAINT `assignment_type_component_lu_ibfk_1` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignment_type_component_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignment_type_component_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `assignment_type_component_lu` */



insert  into `assignment_type_component_lu`(`account_id`,`component_type_id`,`assignment_type_id`) values (1,1,1),(1,2,2),(1,3,4),(1,3,10),(1,3,6),(1,4,11),(1,2,8),(1,5,11);



/*Table structure for table `assignment_types` */



DROP TABLE IF EXISTS `assignment_types`;



CREATE TABLE `assignment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `display_name` varchar(63) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `assignment_types` */



insert  into `assignment_types`(`id`,`name`,`display_name`,`sort_order`,`is_deleted`) values (1,'assigned_to','Assigned to',1,0),(2,'buyer_agent','Buyer Agent',2,0),(3,'co_buyer_agent','Co-Buyer Agent',3,0),(4,'listing_agent','Listing Agent',4,0),(5,'co_listing_agent','Co-listing Agent',5,0),(6,'listing_manager','Listing Manager',6,0),(7,'showing_partner','Showing Partner',7,0),(8,'isa','ISA',8,0),(9,'isa_buyer','Buyer ISA',9,0),(10,'isa_seller','Seller ISA',10,0),(11,'closing_manager','Closing Manager',11,0),(12,'specific_user','Specific User',12,0),(13,'current_user','Current User',13,0),(14,'co_broker_agent','Co Broker Agent',14,0),(15,'title_agent','Title Agent',15,0),(16,'title_processor','Title Processor',16,0),(17,'loan_officer','Loan Officer',17,0),(18,'loan_processor','Loan Processor',18,0),(19,'inspector','Inspector',19,0);



/*Table structure for table `assignments` */



DROP TABLE IF EXISTS `assignments`;



CREATE TABLE `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `assignment_type_id` int(11) DEFAULT NULL,
  `assignee_company_id` int(11) DEFAULT NULL,
  `assignee_contact_id` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `assignment_type_id` (`assignment_type_id`),
  KEY `assignee_company_id` (`assignee_company_id`),
  KEY `assignee_contact_id` (`assignee_contact_id`),
  KEY `component_id` (`component_id`) USING BTREE,
  CONSTRAINT `assignments_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignments_ibfk_2` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignments_ibfk_3` FOREIGN KEY (`assignee_company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignments_ibfk_4` FOREIGN KEY (`assignee_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `assignments` */



/*Table structure for table `auth_assignment` */



DROP TABLE IF EXISTS `auth_assignment`;



CREATE TABLE `auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `userid` (`userid`),
  KEY `auth_assignment_ibfk_3` (`added_by`),
  KEY `auth_assignment_ibfk_4` (`updated_by`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `auth_assignment_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `auth_assignment_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `auth_assignment` */

insert  into `auth_assignment`(`itemname`,`userid`,`added_by`,`added`,`updated_by`,`updated`) values ('owner',1,1,now(),1,now());

/*Table structure for table `auth_assignment_logs` */



DROP TABLE IF EXISTS `auth_assignment_logs`;



CREATE TABLE `auth_assignment_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_item_name_old` varchar(64) DEFAULT NULL,
  `auth_item_name_new` varchar(64) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_assignment_logs_ibfk_3` (`added_by`),
  KEY `auth_assignment_logs_ibfk_1` (`auth_item_name_old`),
  KEY `auth_assignment_logs_ibfk_4` (`auth_item_name_new`),
  KEY `auth_assignment_logs_ibfk_2` (`userid`),
  CONSTRAINT `auth_assignment_logs_ibfk_1` FOREIGN KEY (`auth_item_name_old`) REFERENCES `auth_item` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `auth_assignment_logs_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `auth_assignment_logs_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `auth_assignment_logs_ibfk_4` FOREIGN KEY (`auth_item_name_new`) REFERENCES `auth_item` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `auth_assignment_logs` */



/*Table structure for table `auth_item` */



DROP TABLE IF EXISTS `auth_item`;



CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB;



/*Data for the table `auth_item` */



insert  into `auth_item`(`name`,`label`,`type`,`description`,`bizrule`,`data`) values ('accounting','Accounting',1,NULL,NULL,NULL),('accountingAddAction','Accounting Add Action',0,NULL,NULL,NULL),('accountingEditAction','Accounting Edit Action',0,NULL,NULL,NULL),('accountingIndexAction','Accounting Index Action',0,NULL,NULL,NULL),('accountingRoiAction','Accounting Roi Action',0,NULL,NULL,NULL),('actionPlans','Action Plans',1,NULL,NULL,NULL),('actionPlansAddAction','Action Plans Add Action',0,NULL,NULL,NULL),('actionPlansAddItemAction','Action Plans Add Item Action',0,NULL,NULL,NULL),('actionPlansApplyAction','Action Plans Apply Action',0,NULL,NULL,NULL),('actionPlansEditAction','Action Plans Edit Action',0,NULL,NULL,NULL),('actionPlansGetAllAction','Action Plans Get All Action',0,NULL,NULL,NULL),('actionPlansIndexAction','Action Plans Index Action',0,NULL,NULL,NULL),('actionPlansItemsAction','Action Plans Items Action',0,NULL,NULL,NULL),('actionPlansItemsDeleteAction','Action Plans Items Delete Action',0,NULL,NULL,NULL),('actionPlansListAction','Action Plans List Action',0,NULL,NULL,NULL),('actionPlansListMiniAppliedPlansAction','Action Plans List Mini Applied Plans Action',0,NULL,NULL,NULL),('actionPlansUnapplyAction','Action Plans Unapply Action',0,NULL,NULL,NULL),('actionPlansViewAction','Action Plans View Action',0,NULL,NULL,NULL),('activitylog','Activitylog',1,NULL,NULL,NULL),('activitylogAddAction','Activitylog Add Action',0,NULL,NULL,NULL),('activitylogEditAction','Activitylog Edit Action',0,NULL,NULL,NULL),('activitylogEmailAction','Activitylog Email Action',0,NULL,NULL,NULL),('activitylogValidateAction','Activitylog Validate Action',0,NULL,NULL,NULL),('activitylogViewAction','Activitylog View Action',0,NULL,NULL,NULL),('admin','Seize the Market Administrator',2,'STM Admin',NULL,'N;'),('agent','Agent',2,'Traditional agent working Buyers & Sellers',NULL,'N;'),('badges','Badges',1,NULL,NULL,NULL),('badgesAddPointAction','Badges Add Point Action',0,NULL,NULL,NULL),('badgesIndexAction','Badges Index Action',0,NULL,NULL,NULL),('badgesListAction','Badges List Action',0,NULL,NULL,NULL),('badgesMyBadgesAction','Badges My Badges Action',0,NULL,NULL,NULL),('badgesRequirementsAction','Badges Requirements Action',0,NULL,NULL,NULL),('badgesTypesAction','Badges Types Action',0,NULL,NULL,NULL),('bombBomb','Bomb Bomb',1,NULL,NULL,NULL),('bombBombIndexAction','Bomb Bomb Index Action',0,NULL,NULL,NULL),('buyerAgent','Buyer Agent',2,'Buyer Specialist Role',NULL,'N;'),('buyerIsa','Buyer ISA',2,'Buyer Inside Sales',NULL,'N;'),('clientCare','Client Care',2,'Client Care',NULL,'N;'),('closingManager','Closing Manager',2,'Closing Manager',NULL,'N;'),('closings','Closings',1,NULL,NULL,NULL),('closingsAddAction','Closings Add Action',0,NULL,NULL,NULL),('closingsAgentsAction','Closings Agents Action',0,NULL,NULL,NULL),('closingsClosedAction','Closings Closed Action',0,NULL,NULL,NULL),('closingsCompaniesAction','Closings Companies Action',0,NULL,NULL,NULL),('closingsCompanyContactsAction','Closings Company Contacts Action',0,NULL,NULL,NULL),('closingsDeleteAction','Closings Delete Action',0,NULL,NULL,NULL),('closingsDocumentsAction','Closings Documents Action',0,NULL,NULL,NULL),('closingsEditAction','Closings Edit Action',0,NULL,NULL,NULL),('closingsIndexAction','Closings Index Action',0,NULL,NULL,NULL),('closingsInfoAction','Closings Info Action',0,NULL,NULL,NULL),('closingsInspectorsAction','Closings Inspectors Action',0,NULL,NULL,NULL),('closingsListAction','Closings List Action',0,NULL,NULL,NULL),('closingsPendingAction','Closings Pending Action',0,NULL,NULL,NULL),('closingsViewAction','Closings View Action',0,NULL,NULL,NULL),('cms','Cms',1,NULL,NULL,NULL),('cmsAddAction','Cms Add Action',0,NULL,NULL,NULL),('cmsAddBlogAction','Cms Add Blog Action',0,NULL,NULL,NULL),('cmsAddBuyerStoryboardAction','Cms Add Buyer Storyboard Action',0,'','',NULL),('cmsAddCommentAction','Cms Add Comment Action',0,NULL,NULL,NULL),('cmsAddListingStoryboardAction','Cms Add Listing Storyboard Action',0,NULL,NULL,NULL),('cmsAddPageAction','Cms Add Page Action',0,NULL,NULL,NULL),('cmsContentAction','Cms Content Action',0,NULL,NULL,NULL),('cmsDomainAction','Cms Domain Action',0,NULL,NULL,NULL),('cmsDomainListAction','Cms Domain List Action',0,NULL,NULL,NULL),('cmsDomainPagesAction','Cms Domain Pages Action',0,NULL,NULL,NULL),('cmsEditAction','Cms Edit Action',0,NULL,NULL,NULL),('cmsEditBlogAction','Cms Edit Blog Action',0,NULL,NULL,NULL),('cmsEditBuyerStoryboardAction','Cms Edit Buyer Storyboard Action',0,'','',NULL),('cmsEditListingStoryboardAction','Cms Edit Listing Storyboard Action',0,NULL,NULL,NULL),('cmsEditPageAction','Cms Edit Page Action',0,NULL,NULL,NULL),('cmsIndexAction','Cms Index Action',0,NULL,NULL,NULL),('cmsTagsAction','Cms Tags Action',0,NULL,NULL,NULL),('cmsTagsAddAction','Cms Tags Add Action',0,NULL,NULL,NULL),('cmsTagsEditAction','Cms Tags Edit Action',0,NULL,NULL,NULL),('cmsTagsListAction','Cms Tags List Action',0,NULL,NULL,NULL),('companies','Companies',1,NULL,NULL,NULL),('companiesAddAction','Companies Add Action',0,NULL,NULL,NULL),('companiesAddContactAction','Companies Add Contact Action',0,NULL,NULL,NULL),('companiesDeleteAction','Companies Delete Action',0,NULL,NULL,NULL),('companiesEditAction','Companies Edit Action',0,NULL,NULL,NULL),('companiesIndexAction','Companies Index Action',0,NULL,NULL,NULL),('companiesListAction','Companies List Action',0,NULL,NULL,NULL),('companiesRemoveContactAction','Companies Remove Contact Action',0,NULL,NULL,NULL),('companiesViewAction','Companies View Action',0,NULL,NULL,NULL),('companyTypes','Company Types',1,NULL,NULL,NULL),('companyTypesAddAction','Company Types Add Action',0,NULL,NULL,NULL),('companyTypesEditAction','Company Types Edit Action',0,NULL,NULL,NULL),('companyTypesIndexAction','Company Types Index Action',0,NULL,NULL,NULL),('companyTypesListAction','Company Types List Action',0,NULL,NULL,NULL),('companyTypesViewAction','Company Types View Action',0,NULL,NULL,NULL),('contacts','Contacts',1,NULL,NULL,NULL),('contactsAddAction','Contacts Add Action',0,NULL,NULL,NULL),('contactsAutocompleteAction','Contacts Autocomplete Action',0,NULL,NULL,NULL),('contactsDeleteAction','Contacts Delete Action',0,NULL,NULL,NULL),('contactsDocumentsAction','Contacts Documents Action',0,NULL,NULL,NULL),('contactsEditAction','Contacts Edit Action',0,NULL,NULL,NULL),('contactsEmailCheckAction','Contacts Email Check Action',0,NULL,NULL,NULL),('contactsHomesSavedAction','Contacts Homes Saved Action',0,NULL,NULL,NULL),('contactsHomesViewedAction','Contacts Homes Viewed Action',0,NULL,NULL,NULL),('contactsIndexAction','Contacts Index Action',0,NULL,NULL,NULL),('contactsListAction','Contacts List Action',0,NULL,NULL,NULL),('contactsSmsAction','Contacts Sms Action',0,NULL,NULL,NULL),('contactsTransactionsAction','Contacts Transactions Action',0,NULL,NULL,NULL),('contactsUsersAction','Contacts Users Action',0,NULL,NULL,NULL),('contactsViewAction','Contacts View Action',0,NULL,NULL,NULL),('contactsViewedPagesAction','Contacts Viewed Pages Action',0,NULL,NULL,NULL),('contactTypes','Contact Types',1,NULL,NULL,NULL),('contactTypesAddAction','Contact Types Add Action',0,NULL,NULL,NULL),('contactTypesEditAction','Contact Types Edit Action',0,NULL,NULL,NULL),('contactTypesIndexAction','Contact Types Index Action',0,NULL,NULL,NULL),('contactTypesListAction','Contact Types List Action',0,NULL,NULL,NULL),('contactTypesViewAction','Contact Types View Action',0,NULL,NULL,NULL),('craigslist','Craigslist',1,NULL,NULL,NULL),('craigslistIndexAction','Craigslist Index Action',0,NULL,NULL,NULL),('craigslistListAction','Craigslist List Action',0,NULL,NULL,NULL),('craigslistPostAction','Craigslist Post Action',0,NULL,NULL,NULL),('craigslistViewAction','Craigslist View Action',0,NULL,NULL,NULL),('default','Default',2,'Default Permissions','',NULL),('documents','Documents',1,NULL,NULL,NULL),('documentsAddAction','Documents Add Action',0,NULL,NULL,NULL),('documentsEditAction','Documents Edit Action',0,NULL,NULL,NULL),('documentsIndexAction','Documents Index Action',0,NULL,NULL,NULL),('documentsListAction','Documents List Action',0,NULL,NULL,NULL),('documentsListMiniAction','Documents List Mini Action',0,NULL,NULL,NULL),('documentsUploadAction','Documents Upload Action',0,NULL,NULL,NULL),('documentsViewAction','Documents View Action',0,NULL,NULL,NULL),('emailTemplates','Email Templates',1,NULL,NULL,NULL),('emailTemplatesAddAction','Email Templates Add Action',0,NULL,NULL,NULL),('emailTemplatesEditAction','Email Templates Edit Action',0,NULL,NULL,NULL),('emailTemplatesIndexAction','Email Templates Index Action',0,NULL,NULL,NULL),('emailTemplatesListAction','Email Templates List Action',0,NULL,NULL,NULL),('emailTemplatesViewAction','Email Templates View Action',0,NULL,NULL,NULL),('featuredAreas','Featured Areas',1,NULL,NULL,NULL),('featuredAreasEditAction','Featured Areas Edit Action',0,NULL,NULL,NULL),('featuredAreasIndexAction','Featured Areas Index Action',0,NULL,NULL,NULL),('featuredAreasListAction','Featured Areas List Action',0,NULL,NULL,NULL),('goals','Goals',1,NULL,NULL,NULL),('goalsAddAction','Goals Add Action',0,NULL,NULL,NULL),('goalsDashboardAction','Goals Dashboard Action',0,NULL,NULL,NULL),('goalsEditAction','Goals Edit Action',0,NULL,NULL,NULL),('goalsIndexAction','Goals Index Action',0,NULL,NULL,NULL),('goalsListAction','Goals List Action',0,NULL,NULL,NULL),('inactive','Inactive',2,NULL,NULL,NULL),('leadBuyeragent','Lead Buyer Agent',2,'Lead Buyer Specialist',NULL,'N;'),('leadListingagent','Lead Listing Agent',2,'Lead Listing Specialist',NULL,'N;'),('leadRouting','Lead Routing',1,NULL,NULL,NULL),('leadRoutingIndexAction','Lead Routing Index Action',0,NULL,NULL,NULL),('leadRoutingListAction','Lead Routing List Action',0,NULL,NULL,NULL),('leadRoutingRoutesAction','Lead Routing Routes Action',0,NULL,NULL,NULL),('leadRoutingRulesAction','Lead Routing Rules Action',0,NULL,NULL,NULL),('lender','Lender',2,'Lender Level Access',NULL,'N;'),('lifestyle','Lifestyle',1,NULL,NULL,NULL),('lifestyleAddAction','Lifestyle Add Action',0,NULL,NULL,NULL),('lifestyleEditAction','Lifestyle Edit Action',0,NULL,NULL,NULL),('lifestyleIndexAction','Lifestyle Index Action',0,NULL,NULL,NULL),('lifestyleListAction','Lifestyle List Action',0,NULL,NULL,NULL),('listingAgent','Listing Agent',2,'Listing Agent Specialist Role',NULL,'N;'),('listingManager','Listing Manager',2,'Lisint Manager',NULL,'N;'),('main','Main',1,NULL,NULL,NULL),('mainCaptchaAction','Main Captcha Action',0,NULL,NULL,NULL),('mainDashboardAction','Main Dashboard Action',0,NULL,NULL,NULL),('mainErrorAction','Main Error Action',0,NULL,NULL,NULL),('mainIndexAction','Main Index Action',0,NULL,NULL,NULL),('mainLoginAction','Main Login Action',0,NULL,NULL,NULL),('mainLogoutAction','Main Logout Action',0,NULL,NULL,NULL),('mainPageAction','Main Page Action',0,NULL,NULL,NULL),('marketTrends','Market Trends',1,NULL,NULL,NULL),('marketTrendsAddAction','Market Trends Add Action',0,NULL,NULL,NULL),('marketTrendsDataAction','Market Trends Data Action',0,NULL,NULL,NULL),('marketTrendsEditAction','Market Trends Edit Action',0,NULL,NULL,NULL),('marketTrendsIndexAction','Market Trends Index Action',0,NULL,NULL,NULL),('marketTrendsListAction','Market Trends List Action',0,NULL,NULL,NULL),('marketTrendsViewAction','Market Trends View Action',0,NULL,NULL,NULL),('operationManuals','Operation Manuals',1,NULL,NULL,NULL),('operationManualsAddAction','Operation Manuals Add Action',0,NULL,NULL,NULL),('operationManualsAddTagAction','Operation Manuals Add Tag Action',0,NULL,NULL,NULL),('operationManualsEditAction','Operation Manuals Edit Action',0,NULL,NULL,NULL),('operationManualsEditTagAction','Operation Manuals Edit Tag Action',0,NULL,NULL,NULL),('operationManualsIndexAction','Operation Manuals Index Action',0,NULL,NULL,NULL),('operationManualsListAction','Operation Manuals List Action',0,NULL,NULL,NULL),('operationManualsTagsAction','Operation Manuals Tags Action',0,NULL,NULL,NULL),('operationManualsViewAction','Operation Manuals View Action',0,NULL,NULL,NULL),('owner','Owner',2,'Owner of Account - Full Access to Account',NULL,'N;'),('permissions','Permissions',1,'Permission Controller Task',NULL,NULL),('permissionsAddAction','Permissions Add Action',0,NULL,NULL,NULL),('permissionsAssignAction','Permissions Assign Action',0,NULL,NULL,NULL),('permissionsDeleteAction','Permissions Delete Action',0,NULL,NULL,NULL),('permissionsEditAction','Permissions Edit Action',0,NULL,NULL,NULL),('permissionsGenerateAction','Permissions Generate Action',0,NULL,NULL,NULL),('permissionsGenerateActionOperations','Permissions Generate Action Operations',0,NULL,NULL,NULL),('permissionsListAction','Permissions List Action',0,NULL,NULL,NULL),('projects','Projects',1,NULL,NULL,NULL),('projectsAddAction','Projects Add Action',0,NULL,NULL,NULL),('projectsAddItemAction','Projects Add Item Action',0,NULL,NULL,NULL),('projectsEditAction','Projects Edit Action',0,NULL,NULL,NULL),('projectsIndexAction','Projects Index Action',0,NULL,NULL,NULL),('projectsItemsAction','Projects Items Action',0,NULL,NULL,NULL),('projectsListAction','Projects List Action',0,NULL,NULL,NULL),('projectsViewAction','Projects View Action',0,NULL,NULL,NULL),('recruits','Recruits',1,NULL,NULL,NULL),('recruitsAddAction','Recruits Add Action',0,NULL,NULL,NULL),('recruitsAddFindAction','Recruits Add Find Action',0,NULL,NULL,NULL),('recruitsEditAction','Recruits Edit Action',0,NULL,NULL,NULL),('recruitsIndexAction','Recruits Index Action',0,NULL,NULL,NULL),('recruitsListAction','Recruits List Action',0,NULL,NULL,NULL),('recruitsViewAction','Recruits View Action',0,NULL,NULL,NULL),('referrals','Referrals',1,NULL,NULL,NULL),('referralsAddAction','Referrals Add Action',0,NULL,NULL,NULL),('referralsEditAction','Referrals Edit Action',0,NULL,NULL,NULL),('referralsIndexAction','Referrals Index Action',0,NULL,NULL,NULL),('referralsListMiniAction','Referrals List Mini Action',0,NULL,NULL,NULL),('referralsViewAction','Referrals View Action',0,NULL,NULL,NULL),('roi','Roi',1,NULL,NULL,NULL),('roiIndexAction','Roi Index Action',0,NULL,NULL,NULL),('roiListAction','Roi List Action',0,NULL,NULL,NULL),('salesManager','Sales Manager',2,'Sales Manager',NULL,'N;'),('savedHomeSearch','Saved Home Search',1,NULL,NULL,NULL),('savedHomeSearchAddAction','Saved Home Search Add Action',0,NULL,NULL,NULL),('savedHomeSearchAutocompleteAction','Saved Home Search Autocomplete Action',0,NULL,NULL,NULL),('savedHomeSearchDeleteAction','Saved Home Search Delete Action',0,NULL,NULL,NULL),('savedHomeSearchEditAction','Saved Home Search Edit Action',0,NULL,NULL,NULL),('savedHomeSearchEmailAction','Saved Home Search Email Action',0,NULL,NULL,NULL),('savedHomeSearchIndexAction','Saved Home Search Index Action',0,NULL,NULL,NULL),('savedHomeSearchListAction','Saved Home Search List Action',0,NULL,NULL,NULL),('savedHomeSearchListMiniAction','Saved Home Search List Mini Action',0,NULL,NULL,NULL),('savedHomeSearchPreviewAction','Saved Home Search Preview Action',0,NULL,NULL,NULL),('savedHomeSearchSentAction','Saved Home Search Sent Action',0,NULL,NULL,NULL),('savedHomeSearchSentByAction','Saved Home Search Sent By Action',0,NULL,NULL,NULL),('savedHomeSearchViewAction','Saved Home Search View Action',0,NULL,NULL,NULL),('sellerIsa','Seller ISA',2,'Seller Inside Sales',NULL,'N;'),('settings','Settings',1,NULL,NULL,NULL),('settingsAccountAction','Settings Account Action',0,NULL,NULL,NULL),('settingsEditAction','Settings Edit Action',0,NULL,NULL,NULL),('settingsIndexAction','Settings Index Action',0,NULL,NULL,NULL),('settingsStickyNotesAction','Settings Sticky Notes Action',0,NULL,NULL,NULL),('settingsUsersAction','Settings Users Action',0,NULL,NULL,NULL),('settingsUsersAddAction','Settings Users Add Action',0,NULL,NULL,NULL),('settingsViewAction','Settings View Action',0,NULL,NULL,NULL),('shift','Shift',1,NULL,NULL,NULL),('shiftAddAction','Shift Add Action',0,NULL,NULL,NULL),('shiftDeleteAction','Shift Delete Action',0,NULL,NULL,NULL),('shiftEditAction','Shift Edit Action',0,NULL,NULL,NULL),('shiftGetAction','Shift Get Action',0,NULL,NULL,NULL),('shiftGetShiftAction','Shift Get Shift Action',0,NULL,NULL,NULL),('shiftViewAction','Shift View Action',0,NULL,NULL,NULL),('sources','Sources',1,NULL,NULL,NULL),('sourcesAddAction','Sources Add Action',0,NULL,NULL,NULL),('sourcesAddCampaignsAction','Sources Add Campaigns Action',0,NULL,NULL,NULL),('sourcesCampaignsAction','Sources Campaigns Action',0,NULL,NULL,NULL),('sourcesEditAction','Sources Edit Action',0,NULL,NULL,NULL),('sourcesIndexAction','Sources Index Action',0,NULL,NULL,NULL),('sourcesListAction','Sources List Action',0,NULL,NULL,NULL),('sourcesViewAction','Sources View Action',0,NULL,NULL,NULL),('staff','Staff',2,'Staff Member of Owner',NULL,'N;'),('tasks','Tasks',1,NULL,NULL,NULL),('tasksAddAction','Tasks Add Action',0,NULL,NULL,NULL),('tasksCompleteAction','Tasks Complete Action',0,NULL,NULL,NULL),('tasksDashboardAction','Tasks Dashboard Action',0,NULL,NULL,NULL),('tasksDeleteAction','Tasks Delete Action',0,NULL,NULL,NULL),('tasksEditAction','Tasks Edit Action',0,NULL,NULL,NULL),('tasksIndexAction','Tasks Index Action',0,NULL,NULL,NULL),('tasksListAction','Tasks List Action',0,NULL,NULL,NULL),('tasksListMiniAction','Tasks List Mini Action',0,NULL,NULL,NULL),('tasksValidateAction','Tasks Validate Action',0,NULL,NULL,NULL),('tasksViewAction','Tasks View Action',0,NULL,NULL,NULL),('telephony','Telephony',1,NULL,NULL,NULL),('telephonyAddAction','Telephony Add Action',0,NULL,NULL,NULL),('telephonyDeleteAction','Telephony Delete Action',0,NULL,NULL,NULL),('telephonyEditAction','Telephony Edit Action',0,NULL,NULL,NULL),('telephonyIndexAction','Telephony Index Action',0,NULL,NULL,NULL),('telephonyViewAction','Telephony View Action',0,NULL,NULL,NULL),('timeblockUpdateOthers','Timeblock Update Others',1,NULL,NULL,NULL),('tracking','Tracking',1,NULL,NULL,NULL),('trackingActivitiesAction','Tracking Activities Action',0,NULL,NULL,NULL),('trackingAddTimeblockingAction','Tracking Add Timeblocking Action',0,NULL,NULL,NULL),('trackingAppointmentsAction','Tracking Appointments Action',0,NULL,NULL,NULL),('trackingClosingsAction','Tracking Closings Action',0,NULL,NULL,NULL),('trackingConversionAction','Tracking Conversion Action',0,NULL,NULL,NULL),('trackingDeleteTimeblockAction','Tracking Delete Timeblock Action',0,NULL,NULL,NULL),('trackingEditTimeblockingAction','Tracking Edit Timeblocking Action',0,NULL,NULL,NULL),('trackingFormViewsAction','Tracking Form Views Action',0,NULL,NULL,NULL),('trackingGoalsAction','Tracking Goals Action',0,NULL,NULL,NULL),('trackingLeadsAction','Tracking Leads Action',0,NULL,NULL,NULL),('trackingListingsAction','Tracking Listings Action',0,NULL,NULL,NULL),('trackingMarketUpdatesAction','Tracking Market Updates Action',0,NULL,NULL,NULL),('trackingSessionAction','Tracking Session Action',0,NULL,NULL,NULL),('trackingSourcesAction','Tracking Sources Action',0,NULL,NULL,NULL),('trackingTasksAction','Tracking Tasks Action',0,NULL,NULL,NULL),('trackingTimeblockingAction','Tracking Timeblocking Action',0,NULL,NULL,NULL),('trackingTrafficAction','Tracking Traffic Action',0,NULL,NULL,NULL),('trackingTrafficDetailsAction','Tracking Traffic Details Action',0,NULL,NULL,NULL),('trackingViewTimeblockAction','Tracking View Timeblock Action',0,NULL,NULL,NULL),('transactions','Transactions',1,NULL,NULL,NULL),('transactionsAbcAction','Transactions Abc Action',0,NULL,NULL,NULL),('transactionsABCListAction','Transactions ABC List Action',0,NULL,NULL,NULL),('transactionsAddAction','Transactions Add Action',0,NULL,NULL,NULL),('transactionsAddAddressAction','Transactions Add Address Action',0,NULL,NULL,NULL),('transactionsAddFindAction','Transactions Add Find Action',0,NULL,NULL,NULL),('transactionsComingSoonAction','Transactions Coming Soon Action',0,NULL,NULL,NULL),('transactionsDashboardAction','Transactions Dashboard Action',0,NULL,NULL,NULL),('transactionsEditAction','Transactions Edit Action',0,NULL,NULL,NULL),('transactionsExpiredCancelAction','Transactions Expired Cancel Action',0,NULL,NULL,NULL),('transactionsIndexAction','Transactions Index Action',0,NULL,NULL,NULL),('transactionsListAction','Transactions List Action',0,NULL,NULL,NULL),('transactionsListingsAction','Transactions Listings Action',0,NULL,NULL,NULL),('transactionsMergeAction','Transactions Merge Action',0,NULL,NULL,NULL),('transactionsMyAction','Transactions My Action',0,NULL,NULL,NULL),('transactionsPipelineAction','Transactions Pipeline Action',0,NULL,NULL,NULL),('transactionsReverseProspectAction','Transactions Reverse Prospect Action',0,NULL,NULL,NULL),('transactionsViewAction','Transactions View Action',0,NULL,NULL,NULL),('virtualAsst','Virtual Assistant',2,'Virtual Assistant',NULL,'N;'),('widgets','Widgets',1,NULL,NULL,NULL),('widgetsAddAction','Widgets Add Action',0,NULL,NULL,NULL),('widgetsGroupsAction','Widgets Groups Action',0,NULL,NULL,NULL),('widgetsIndexAction','Widgets Index Action',0,NULL,NULL,NULL),('widgetsListAction','Widgets List Action',0,NULL,NULL,NULL),('exportContacts','Export Contacts',0,NULL,NULL,NULL),('transactionTags', 'Transaction Tags', 1, NULL, NULL, NULL),
('supportAgentLevel2', 'Support: Agent Quick Start Training Level 2', 1, '', NULL, 'N;'),
('supportAgentLevel3', 'Support: Agent Quick Start Training Level 3', 1, '', NULL, 'N;'),
('supportAgentLevel4', 'Support: Agent Quick Start Training Level 4', 1, '', NULL, 'N;'),
('supportAgentLevel5', 'Support: Agent Quick Start Training Level 5', 1, '', NULL, 'N;'),
('supportAgentLevel6', 'Support: Agent Quick Start Training Level 6', 1, '', NULL, 'N;'),
('supportLevel2', 'Support: Comprehensive Training Level 2', 1, '', NULL, 'N;'),
('supportLevel3', 'Support: Comprehensive Training Level 3', 1, '', NULL, 'N;'),
('supportLevel4', 'Support: Comprehensive Training Level 4', 1, '', NULL, 'N;'),
('supportLevel5', 'Support: Comprehensive Training Level 5', 1, '', NULL, 'N;'),
('supportLevel6', 'Support: Comprehensive Training Level 6', 1, '', NULL, 'N;'),
('supportLevel7', 'Support: Comprehensive Training Level 7', 1, '', NULL, 'N;'),
('supportLevel8', 'Support: Comprehensive Training Level 8', 1, '', NULL, 'N;');



/*Table structure for table `auth_item_child` */



DROP TABLE IF EXISTS `auth_item_child`;



CREATE TABLE `auth_item_child` (
  `parent` varchar(50) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;



/*Data for the table `auth_item_child` */



insert  into `auth_item_child`(`parent`,`child`) values ('accounting','accountingAddAction'),('accounting','accountingEditAction'),('accounting','accountingIndexAction'),('accounting','accountingRoiAction'),('agent','actionPlans'),('buyerAgent','actionPlans'),('buyerIsa','actionPlans'),('closingManager','actionPlans'),('owner','actionPlans'),('salesManager','actionPlans'),('actionPlans','actionPlansAddAction'),('actionPlans','actionPlansAddItemAction'),('actionPlans','actionPlansApplyAction'),('actionPlans','actionPlansEditAction'),('actionPlans','actionPlansGetAllAction'),('actionPlans','actionPlansIndexAction'),('actionPlans','actionPlansItemsAction'),('actionPlans','actionPlansItemsDeleteAction'),('actionPlans','actionPlansListAction'),('actionPlans','actionPlansListMiniAppliedPlansAction'),('actionPlans','actionPlansUnapplyAction'),('actionPlans','actionPlansViewAction'),('agent','activitylog'),('buyerAgent','activitylog'),('buyerIsa','activitylog'),('closingManager','activitylog'),('owner','activitylog'),('salesManager','activitylog'),('activitylog','activitylogAddAction'),('activitylog','activitylogEditAction'),('activitylog','activitylogEmailAction'),('activitylog','activitylogValidateAction'),('activitylog','activitylogViewAction'),('agent','badges'),('buyerAgent','badges'),('buyerIsa','badges'),('closingManager','badges'),('owner','badges'),('salesManager','badges'),('badges','badgesAddPointAction'),('badges','badgesIndexAction'),('badges','badgesListAction'),('badges','badgesMyBadgesAction'),('badges','badgesRequirementsAction'),('badges','badgesTypesAction'),('buyerAgent','bombBomb'),('buyerIsa','bombBomb'),('owner','bombBomb'),('salesManager','bombBomb'),('bombBomb','bombBombIndexAction'),('agent','buyerAgent'),('staff','clientCare'),('staff','closingManager'),('agent','closings'),('buyerIsa','closings'),('closingManager','closings'),('owner','closings'),('salesManager','closings'),('closings','closingsAddAction'),('closings','closingsAgentsAction'),('closings','closingsClosedAction'),('closings','closingsCompaniesAction'),('closings','closingsCompanyContactsAction'),('closings','closingsDeleteAction'),('closings','closingsDocumentsAction'),('closings','closingsEditAction'),('closings','closingsIndexAction'),('closings','closingsInfoAction'),('closings','closingsInspectorsAction'),('closings','closingsListAction'),('closings','closingsPendingAction'),('closings','closingsViewAction'),('agent','cms'),('buyerAgent','cms'),('buyerIsa','cms'),('closingManager','cms'),('owner','cms'),('salesManager','cms'),('cms','cmsAddAction'),('cms','cmsAddBlogAction'),('cms','cmsAddCommentAction'),('cms','cmsAddListingStoryboardAction'),('cms','cmsAddPageAction'),('cms','cmsContentAction'),('cms','cmsDomainAction'),('cms','cmsDomainListAction'),('cms','cmsDomainPagesAction'),('cms','cmsEditAction'),('cms','cmsEditBlogAction'),('agent','cmsEditBuyerStoryboardAction'),('buyerAgent','cmsEditBuyerStoryboardAction'),('cms','cmsEditListingStoryboardAction'),('cms','cmsEditPageAction'),('cms','cmsIndexAction'),('cms','cmsTagsAction'),('cms','cmsTagsAddAction'),('cms','cmsTagsEditAction'),('cms','cmsTagsListAction'),('agent','companies'),('buyerAgent','companies'),('buyerIsa','companies'),('closingManager','companies'),('owner','companies'),('salesManager','companies'),('companies','companiesAddAction'),('companies','companiesAddContactAction'),('companies','companiesDeleteAction'),('companies','companiesEditAction'),('companies','companiesIndexAction'),('companies','companiesListAction'),('companies','companiesRemoveContactAction'),('companies','companiesViewAction'),('agent','companyTypes'),('buyerAgent','companyTypes'),('buyerIsa','companyTypes'),('closingManager','companyTypes'),('owner','companyTypes'),('salesManager','companyTypes'),('companyTypes','companyTypesAddAction'),('companyTypes','companyTypesEditAction'),('companyTypes','companyTypesIndexAction'),('companyTypes','companyTypesListAction'),('companyTypes','companyTypesViewAction'),('agent','contacts'),('buyerAgent','contacts'),('buyerIsa','contacts'),('closingManager','contacts'),('owner','contacts'),('salesManager','contacts'),('contacts','contactsAddAction'),('contacts','contactsAutocompleteAction'),('contacts','contactsDeleteAction'),('contacts','contactsDocumentsAction'),('contacts','contactsEditAction'),('contacts','contactsEmailCheckAction'),('contacts','contactsHomesSavedAction'),('contacts','contactsHomesViewedAction'),('contacts','contactsIndexAction'),('contacts','contactsListAction'),('contacts','contactsSmsAction'),('contacts','contactsTransactionsAction'),('badges','contactsUsersAction'),('contacts','contactsViewAction'),('contacts','contactsViewedPagesAction'),('agent','contactTypes'),('buyerAgent','contactTypes'),('buyerIsa','contactTypes'),('closingManager','contactTypes'),('owner','contactTypes'),('salesManager','contactTypes'),('contactTypes','contactTypesAddAction'),('contactTypes','contactTypesEditAction'),('contactTypes','contactTypesIndexAction'),('contactTypes','contactTypesListAction'),('contactTypes','contactTypesViewAction'),('agent','craigslist'),('buyerAgent','craigslist'),('buyerIsa','craigslist'),('closingManager','craigslist'),('owner','craigslist'),('salesManager','craigslist'),('craigslist','craigslistIndexAction'),('craigslist','craigslistListAction'),('craigslist','craigslistPostAction'),('badges','craigslistViewAction'),('agent','documents'),('buyerAgent','documents'),('buyerIsa','documents'),('closingManager','documents'),('owner','documents'),('salesManager','documents'),('documents','documentsAddAction'),('documents','documentsEditAction'),('documents','documentsIndexAction'),('documents','documentsListAction'),('documents','documentsListMiniAction'),('documents','documentsUploadAction'),('documents','documentsViewAction'),('agent','emailTemplates'),('buyerAgent','emailTemplates'),('buyerIsa','emailTemplates'),('closingManager','emailTemplates'),('owner','emailTemplates'),('salesManager','emailTemplates'),('emailTemplates','emailTemplatesAddAction'),('emailTemplates','emailTemplatesEditAction'),('emailTemplates','emailTemplatesIndexAction'),('emailTemplates','emailTemplatesListAction'),('emailTemplates','emailTemplatesViewAction'),('agent','featuredAreas'),('buyerAgent','featuredAreas'),('buyerIsa','featuredAreas'),('closingManager','featuredAreas'),('owner','featuredAreas'),('salesManager','featuredAreas'),('featuredAreas','featuredAreasEditAction'),('featuredAreas','featuredAreasIndexAction'),('featuredAreas','featuredAreasListAction'),('agent','goals'),('buyerAgent','goals'),('buyerIsa','goals'),('closingManager','goals'),('owner','goals'),('salesManager','goals'),('goals','goalsAddAction'),('goals','goalsDashboardAction'),('goals','goalsEditAction'),('goals','goalsIndexAction'),('goals','goalsListAction'),('agent','leadBuyerAgent'),('agent','leadListingAgent'),('agent','leadRouting'),('buyerAgent','leadRouting'),('buyerIsa','leadRouting'),('closingManager','leadRouting'),('owner','leadRouting'),('salesManager','leadRouting'),('leadRouting','leadRoutingIndexAction'),('leadRouting','leadRoutingListAction'),('leadRouting','leadRoutingRoutesAction'),('leadRouting','leadRoutingRulesAction'),('agent','lifestyle'),('buyerAgent','lifestyle'),('buyerIsa','lifestyle'),('owner','lifestyle'),('salesManager','lifestyle'),('lifestyle','lifestyleAddAction'),('lifestyle','lifestyleEditAction'),('lifestyle','lifestyleIndexAction'),('lifestyle','lifestyleListAction'),('agent','listingAgent'),('staff','listingManager'),('agent','main'),('buyerAgent','main'),('buyerIsa','main'),('closingManager','main'),('owner','main'),('salesManager','main'),('main','mainCaptchaAction'),('main','mainDashboardAction'),('main','mainErrorAction'),('main','mainIndexAction'),('main','mainLoginAction'),('main','mainLogoutAction'),('main','mainPageAction'),('buyerAgent','marketTrends'),('buyerIsa','marketTrends'),('closingManager','marketTrends'),('owner','marketTrends'),('salesManager','marketTrends'),('marketTrends','marketTrendsAddAction'),('marketTrends','marketTrendsDataAction'),('marketTrends','marketTrendsEditAction'),('marketTrends','marketTrendsIndexAction'),('marketTrends','marketTrendsListAction'),('marketTrends','marketTrendsViewAction'),('buyerAgent','operationManuals'),('buyerIsa','operationManuals'),('owner','operationManuals'),('salesManager','operationManuals'),('operationManuals','operationManualsAddAction'),('operationManuals','operationManualsAddTagAction'),('operationManuals','operationManualsEditAction'),('operationManuals','operationManualsEditTagAction'),('operationManuals','operationManualsIndexAction'),('operationManuals','operationManualsListAction'),('operationManuals','operationManualsTagsAction'),('operationManuals','operationManualsViewAction'),('owner','permissions'),('marketTrends','permissionsAddAction'),('marketTrends','permissionsAssignAction'),('marketTrends','permissionsDeleteAction'),('marketTrends','permissionsEditAction'),('permissions','permissionsGenerateAction'),('marketTrends','permissionsGenerateActionOperations'),('marketTrends','permissionsListAction'),('buyerAgent','projects'),('buyerIsa','projects'),('owner','projects'),('salesManager','projects'),('projects','projectsAddAction'),('projects','projectsAddItemAction'),('projects','projectsEditAction'),('projects','projectsIndexAction'),('projects','projectsItemsAction'),('projects','projectsListAction'),('projects','projectsViewAction'),('agent','recruits'),('buyerAgent','recruits'),('buyerIsa','recruits'),('closingManager','recruits'),('owner','recruits'),('salesManager','recruits'),('recruits','recruitsAddAction'),('recruits','recruitsAddFindAction'),('recruits','recruitsEditAction'),('recruits','recruitsIndexAction'),('recruits','recruitsListAction'),('recruits','recruitsViewAction'),('agent','referrals'),('buyerAgent','referrals'),('buyerIsa','referrals'),('closingManager','referrals'),('owner','referrals'),('salesManager','referrals'),('referrals','referralsAddAction'),('referrals','referralsEditAction'),('referrals','referralsIndexAction'),('referrals','referralsListMiniAction'),('referrals','referralsViewAction'),('buyerAgent','roi'),('buyerIsa','roi'),('closingManager','roi'),('owner','roi'),('salesManager','roi'),('roi','roiIndexAction'),('roi','roiListAction'),('staff','salesManager'),('agent','savedHomeSearch'),('buyerAgent','savedHomeSearch'),('buyerIsa','savedHomeSearch'),('closingManager','savedHomeSearch'),('owner','savedHomeSearch'),('salesManager','savedHomeSearch'),('savedHomeSearch','savedHomeSearchAddAction'),('savedHomeSearch','savedHomeSearchAutocompleteAction'),('savedHomeSearch','savedHomeSearchDeleteAction'),('savedHomeSearch','savedHomeSearchEditAction'),('badges','savedHomeSearchEmailAction'),('savedHomeSearch','savedHomeSearchIndexAction'),('badges','savedHomeSearchListAction'),('savedHomeSearch','savedHomeSearchListMiniAction'),('savedHomeSearch','savedHomeSearchPreviewAction'),('badges','savedHomeSearchSentAction'),('savedHomeSearch','savedHomeSearchSentByAction'),('savedHomeSearch','savedHomeSearchViewAction'),('agent','settings'),('buyerAgent','settings'),('buyerIsa','settings'),('closingManager','settings'),('owner','settings'),('salesManager','settings'),('settings','settingsAccountAction'),('settings','settingsEditAction'),('settings','settingsIndexAction'),('settings','settingsStickyNotesAction'),('settings','settingsUsersAction'),('settings','settingsUsersAddAction'),('settings','settingsViewAction'),('agent','shift'),('buyerAgent','shift'),('buyerIsa','shift'),('closingManager','shift'),('owner','shift'),('salesManager','shift'),('shift','shiftAddAction'),('shift','shiftDeleteAction'),('shift','shiftEditAction'),('shift','shiftGetAction'),('shift','shiftGetShiftAction'),('shift','shiftViewAction'),('agent','sources'),('buyerAgent','sources'),('buyerIsa','sources'),('closingManager','sources'),('owner','sources'),('salesManager','sources'),('sources','sourcesAddAction'),('sources','sourcesAddCampaignsAction'),('sources','sourcesCampaignsAction'),('sources','sourcesEditAction'),('sources','sourcesIndexAction'),('sources','sourcesListAction'),('sources','sourcesViewAction'),('agent','tasks'),('buyerAgent','tasks'),('buyerIsa','tasks'),('closingManager','tasks'),('owner','tasks'),('salesManager','tasks'),('tasks','tasksAddAction'),('tasks','tasksCompleteAction'),('tasks','tasksDashboardAction'),('tasks','tasksDeleteAction'),('tasks','tasksEditAction'),('tasks','tasksIndexAction'),('tasks','tasksListAction'),('tasks','tasksListMiniAction'),('tasks','tasksValidateAction'),('tasks','tasksViewAction'),('buyerAgent','telephony'),('telephony','telephonyAddAction'),('telephony','telephonyDeleteAction'),('telephony','telephonyEditAction'),('telephony','telephonyIndexAction'),('telephony','telephonyViewAction'),('buyerAgent','timeblockUpdateOthers'),('buyerIsa','timeblockUpdateOthers'),('owner','timeblockUpdateOthers'),('salesManager','timeblockUpdateOthers'),('agent','tracking'),('buyerAgent','tracking'),('buyerIsa','tracking'),('closingManager','tracking'),('owner','tracking'),('salesManager','tracking'),('tracking','trackingAddTimeblockingAction'),('tracking','trackingAppointmentsAction'),('tracking','trackingClosingsAction'),('tracking','trackingConversionAction'),('tracking','trackingDeleteTimeblockAction'),('tracking','trackingFormViewsAction'),('tracking','trackingGoalsAction'),('tracking','trackingLeadsAction'),('badges','trackingListingsAction'),('tracking','trackingMarketUpdatesAction'),('tracking','trackingSessionAction'),('tracking','trackingTasksAction'),('tracking','trackingTimeblockingAction'),('tracking','trackingTrafficAction'),('tracking','trackingTrafficDetailsAction'),('agent','transactions'),('buyerAgent','transactions'),('buyerIsa','transactions'),('closingManager','transactions'),('owner','transactions'),('salesManager','transactions'),('transactions','transactionsAbcAction'),('transactions','transactionsABCListAction'),('transactions','transactionsAddAction'),('transactions','transactionsAddAddressAction'),('transactions','transactionsAddFindAction'),('transactions','transactionsComingSoonAction'),('transactions','transactionsDashboardAction'),('transactions','transactionsEditAction'),('transactions','transactionsExpiredCancelAction'),('transactions','transactionsIndexAction'),('transactions','transactionsListAction'),('transactions','transactionsListingsAction'),('transactions','transactionsMergeAction'),('transactions','transactionsMyAction'),('transactions','transactionsPipelineAction'),('transactions','transactionsReverseProspectAction'),('transactions','transactionsViewAction'),('agent','virtualAsst'),('buyerAgent','widgets'),('closingManager','widgets'),('owner','widgets'),('widgets','widgetsAddAction'),('widgets','widgetsGroupsAction'),('widgets','widgetsIndexAction'),('widgets','widgetsListAction');



/*Table structure for table `badge_awarded` */



DROP TABLE IF EXISTS `badge_awarded`;



CREATE TABLE `badge_awarded` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `points_redeemed` int(11) DEFAULT NULL,
  `note` varchar(127) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `badge_contact_lu_ibfk_1` (`updated_by`),
  KEY `badge_contact_lu_ibfk_2` (`added_by`),
  KEY `badge_id` (`badge_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `badge_awarded_ibfk_1` FOREIGN KEY (`badge_id`) REFERENCES `badges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_awarded_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_awarded_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_awarded_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `badge_awarded` */



/*Table structure for table `badge_contact_lu` */



DROP TABLE IF EXISTS `badge_contact_lu`;



CREATE TABLE `badge_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `points_redeemed` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `badge_contact_lu_ibfk_1` (`updated_by`),
  KEY `badge_contact_lu_ibfk_2` (`added_by`),
  CONSTRAINT `badge_contact_lu_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_contact_lu_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `badge_contact_lu` */



/*Table structure for table `badge_point_types` */



DROP TABLE IF EXISTS `badge_point_types`;



CREATE TABLE `badge_point_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `badge_point_types_ibfk_1` (`updated_by`),
  KEY `badge_point_types_ibfk_2` (`added_by`),
  CONSTRAINT `badge_point_types_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_point_types_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `badge_point_types` */



insert  into `badge_point_types`(`id`,`name`,`display_name`,`updated_by`,`updated`,`added_by`,`added`) values (1,'thank_you','Thank You',1,'2013-08-13 14:10:34',1,'2013-08-13 14:10:34');



/*Table structure for table `badge_points_earned` */



DROP TABLE IF EXISTS `badge_points_earned`;



CREATE TABLE `badge_points_earned` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `badge_point_type_id` int(11) DEFAULT NULL,
  `points_earned` int(11) DEFAULT NULL,
  `note` varchar(750) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `badge_points_earned_ibfk_1` (`contact_id`),
  KEY `badge_points_earned_ibfk_2` (`badge_point_type_id`),
  KEY `badge_points_earned_ibfk_3` (`updated_by`),
  KEY `badge_points_earned_ibfk_4` (`added_by`),
  CONSTRAINT `badge_points_earned_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_points_earned_ibfk_2` FOREIGN KEY (`badge_point_type_id`) REFERENCES `badge_point_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_points_earned_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_points_earned_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `badge_points_earned` */



/*Table structure for table `badge_requirements` */



DROP TABLE IF EXISTS `badge_requirements`;



CREATE TABLE `badge_requirements` (
  `badge_id` int(11) DEFAULT NULL,
  `badge_point_type_id` int(11) DEFAULT NULL,
  `points_required` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  KEY `badge_requirements_ibfk_1` (`updated_by`),
  KEY `badge_requirements_ibfk_2` (`added_by`),
  CONSTRAINT `badge_requirements_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badge_requirements_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `badge_requirements` */



insert  into `badge_requirements`(`badge_id`,`badge_point_type_id`,`points_required`,`updated_by`,`updated`,`added_by`,`added`) values (1,1,1,1,'2013-08-11 14:36:59',1,'2013-08-11 14:36:59');



/*Table structure for table `badges` */



DROP TABLE IF EXISTS `badges`;



CREATE TABLE `badges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_filename` varchar(255) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `badges_ibfk_1` (`updated_by`),
  KEY `badges_ibfk_2` (`added_by`),
  CONSTRAINT `badges_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `badges_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `badges` */



insert  into `badges`(`id`,`name`,`description`,`image_filename`,`updated_by`,`updated`,`added_by`,`added`) values (1,'Thank You','Giving thanks to fellow peeps!','thank_you.png',1,'2013-08-11 14:36:27',1,'2013-08-11 14:36:27');



/*Table structure for table `bucket_rules` */



DROP TABLE IF EXISTS `bucket_rules`;



CREATE TABLE `bucket_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bucket_rules_added_by_fk` (`added_by`),
  CONSTRAINT `bucket_rules_added_by_fk` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
) ENGINE=InnoDB;



/*Data for the table `bucket_rules` */



/*Table structure for table `bucket_rules_saved` */



DROP TABLE IF EXISTS `bucket_rules_saved`;



CREATE TABLE `bucket_rules_saved` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket_rule_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `criteria` blob NOT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bucket_rules_saved_bucket_rule_fk` (`bucket_rule_id`),
  CONSTRAINT `bucket_rules_saved_bucket_rule_fk` FOREIGN KEY (`bucket_rule_id`) REFERENCES `bucket_rules` (`id`)
) ENGINE=InnoDB;



/*Data for the table `bucket_rules_saved` */



/*Table structure for table `buckets` */



DROP TABLE IF EXISTS `buckets`;



CREATE TABLE `buckets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `buckets_added_by_fk` (`added_by`),
  KEY `buckets_updated_by_fk` (`updated_by`),
  CONSTRAINT `buckets_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`),
  CONSTRAINT `buckets_added_by_fk` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
) ENGINE=InnoDB;



/*Data for the table `buckets` */



/*Table structure for table `call_session_queues` */



DROP TABLE IF EXISTS `call_session_queues`;



CREATE TABLE `call_session_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_session_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `phone_id` int(11) NOT NULL,
  `spoke_to` tinyint(1) DEFAULT '0',
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `call_session_queues_call_session_fk` (`call_session_id`),
  KEY `call_session_queues_agent_id_fk` (`agent_id`),
  KEY `call_session_queues_phone_id_fk` (`phone_id`),
  CONSTRAINT `call_session_queues_phone_id_fk` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`),
  CONSTRAINT `call_session_queues_agent_id_fk` FOREIGN KEY (`agent_id`) REFERENCES `contacts` (`id`),
  CONSTRAINT `call_session_queues_call_session_fk` FOREIGN KEY (`call_session_id`) REFERENCES `call_sessions` (`id`)
) ENGINE=InnoDB;



/*Data for the table `call_session_queues` */



/*Table structure for table `call_sessions` */



DROP TABLE IF EXISTS `call_sessions`;



CREATE TABLE `call_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `call_sessions_added_by_fk` (`added_by`),
  KEY `call_sessions_updated_by_fk` (`updated_by`),
  CONSTRAINT `call_sessions_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`),
  CONSTRAINT `call_sessions_added_by_fk` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
) ENGINE=InnoDB;



/*Data for the table `call_sessions` */



/*Table structure for table `closing_contacts` */



DROP TABLE IF EXISTS `closing_contacts`;



CREATE TABLE `closing_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `closing_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `closing_id` (`closing_id`),
  KEY `company_id` (`company_id`) USING BTREE,
  KEY `contact_id` (`contact_id`) USING BTREE,
  CONSTRAINT `closing_contacts_ibfk_1` FOREIGN KEY (`closing_id`) REFERENCES `closings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `company_id` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_id` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `closing_contacts` */



/*Table structure for table `closings` */



DROP TABLE IF EXISTS `closings`;



CREATE TABLE `closings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `closing_status_ma` tinyint(1) NOT NULL DEFAULT '1',
  `post_close_status_ma` tinyint(1) DEFAULT NULL,
  `fallout_date` date DEFAULT NULL,
  `fallout_reason_ma` tinyint(1) DEFAULT NULL,
  `fallout_reason_other` varchar(63) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `sale_type_ma` tinyint(1) DEFAULT '0',
  `financing_type_ma` tinyint(4) DEFAULT NULL,
  `loan_amount` int(11) DEFAULT NULL,
  `is_new_construction` tinyint(1) DEFAULT '0',
  `mls_id` varchar(25) DEFAULT '0',
  `is_our_listing` tinyint(1) DEFAULT '0', -- can be a link to other closing...
  `ernest_money_deposit` decimal(10,2) DEFAULT NULL,
  `inspection_contingency_date` date DEFAULT NULL,
  `appraisal_contingency_date` date DEFAULT NULL,
  `financing_contingency_date` date DEFAULT NULL,
  `contingencies` varchar(255) DEFAULT NULL,
  `notes` varchar(750) DEFAULT NULL,
  `is_referral` tinyint(1) DEFAULT NULL,
  `referral_commission` decimal(6,2) DEFAULT NULL,
  `commission_amount` decimal(10,2) DEFAULT NULL,
  `contract_execute_date` date DEFAULT NULL,
  `contract_closing_date` date DEFAULT NULL,
  `actual_closing_datetime` timestamp NULL DEFAULT NULL,
  `is_mail_away` tinyint(1) DEFAULT '0',
  `closing_location` varchar(255) DEFAULT NULL,
  `closing_gift` varchar(127) DEFAULT NULL,
  `title_company_id` int(11) DEFAULT NULL,
  `other_agent_id` int(11) DEFAULT NULL,
  `lender_id` int(11) DEFAULT NULL,
  `home_warranty_company_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `closing_status_ma` (`closing_status_ma`) USING BTREE,
  CONSTRAINT `closings_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `closings` */



/*Table structure for table `cms_comments` */



DROP TABLE IF EXISTS `cms_comments`;



CREATE TABLE `cms_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_content_id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `first_name` varchar(85) NOT NULL,
  `last_name` varchar(85) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `cms_comments` */



/*Table structure for table `cms_content_contact_lu` */



DROP TABLE IF EXISTS `cms_content_contact_lu`;



CREATE TABLE `cms_content_contact_lu` (
  `cms_content_id` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cms_content_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `cms_content_contact_lu_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_content_contact_lu_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `cms_content_contact_lu` */



/*Table structure for table `cms_content_tag_lu` */



DROP TABLE IF EXISTS `cms_content_tag_lu`;



CREATE TABLE `cms_content_tag_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_content_id` int(11) DEFAULT NULL,
  `cms_tag_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_content_id` (`cms_content_id`),
  KEY `cms_tag_id` (`cms_tag_id`),
  CONSTRAINT `cms_content_tag_lu_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_content_tag_lu_ibfk_2` FOREIGN KEY (`cms_tag_id`) REFERENCES `cms_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `cms_content_tag_lu` */



/*Table structure for table `cms_content_widget_lu` */



DROP TABLE IF EXISTS `cms_content_widget_lu`;



CREATE TABLE `cms_content_widget_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_content_id` int(11) DEFAULT NULL,
  `cms_widget_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_content_id` (`cms_content_id`),
  KEY `cms_widget_id` (`cms_widget_id`),
  CONSTRAINT `cms_content_widget_lu_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_content_widget_lu_ibfk_2` FOREIGN KEY (`cms_widget_id`) REFERENCES `cms_widgets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `cms_content_widget_lu` */



/*Table structure for table `cms_contents` */



DROP TABLE IF EXISTS `cms_contents`;



CREATE TABLE `cms_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `type_ma` tinyint(1) DEFAULT '1',
  `template_name` varchar(40) NOT NULL,
  `url` varchar(127) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `template_data` longblob,
  `cms_widget_group_id` int(11) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `published` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`domain_id`,`url`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`),
  KEY `cms_widget_group_id` (`cms_widget_group_id`),
  CONSTRAINT `cms_contents_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_contents_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_contents_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_contents_ibfk_4` FOREIGN KEY (`cms_widget_group_id`) REFERENCES `cms_widget_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;


/*Table structure for table `cms_tags` */



DROP TABLE IF EXISTS `cms_tags`;



CREATE TABLE `cms_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `url` varchar(63) DEFAULT NULL,
  `is_popular_topic_link` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`account_id`),
  CONSTRAINT `cms_tags_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_tags_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `cms_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

/*Data for the table `cms_tags` */



insert  into `cms_tags`(`id`,`account_id`,`name`,`url`,`is_deleted`) values (1,1,'Buyers','buyers',0),(2,1,'Sellers','sellers',0),(3,1,'Market','market',0),(4,1,'Financing','financing',0),(5,1,'Team','team',0),(6,1,'Reviews','reviews',0),(7,1,'Hiring','hiring',0);



/*Table structure for table `cms_videos` */



DROP TABLE IF EXISTS `cms_videos`;



CREATE TABLE `cms_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT '0',
  `description` varchar(63) DEFAULT NULL,
  `video_source_ma` tinyint(1) DEFAULT NULL,
  `video_code` varchar(127) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `cms_videos` */



/*Table structure for table `cms_widget_group_lu` */



DROP TABLE IF EXISTS `cms_widget_group_lu`;



CREATE TABLE `cms_widget_group_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_widget_group_id` int(11) DEFAULT NULL,
  `cms_widget_id` int(11) DEFAULT NULL,
  `sort_order` tinyint(2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_widget_id` (`cms_widget_id`),
  KEY `cms_widget_group_id` (`cms_widget_group_id`),
  CONSTRAINT `cms_widget_group_lu_ibfk_1` FOREIGN KEY (`cms_widget_id`) REFERENCES `cms_widgets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cms_widget_group_lu_ibfk_2` FOREIGN KEY (`cms_widget_group_id`) REFERENCES `cms_widget_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `cms_widget_group_lu` */



/*Table structure for table `cms_widget_groups` */



DROP TABLE IF EXISTS `cms_widget_groups`;



CREATE TABLE `cms_widget_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `cms_widget_groups_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `cms_widget_groups` */



insert  into `cms_widget_groups`(`id`,`account_id`,`status_ma`,`name`,`description`,`is_default`,`is_deleted`) values (1,2,1,'default','Default if none is specified',1,0),(2,2,1,'Buyers','widgets displayed for buyer pages',0,0),(3,2,1,'Sellers','widgets displayed for seller pages',0,0);



/*Table structure for table `cms_widgets` */



DROP TABLE IF EXISTS `cms_widgets`;



CREATE TABLE `cms_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `folder_name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `cms_widgets_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `cms_widgets` */



insert  into `cms_widgets`(`id`,`account_id`,`name`,`folder_name`,`description`,`is_active`,`is_deleted`) values (1,2,'Market Stats','MarketStatsWidget','Detailed stats on the market or neighborhood',1,0),(2,2,'House Values','HouseValuesWidget','Find Out What Your Home is Worth',0,0),(3,2,'Recently Viewed Homes','RecentlyViewedHomesWidget','Lists recently viewed homes by visitor',0,0),(4,2,'Foreclosure List','ForeclosureListWidget','Free Foreclosure List Form',0,0),(5,2,'Now Hiring','NowHiringWidget','Recruiting page for admin and agents',0,0);



/*Table structure for table `command_processed_log` */



DROP TABLE IF EXISTS `command_processed_log`;



CREATE TABLE `command_processed_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `command_id` int(11) NOT NULL,
  `message` text,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `command_processed_log` */



/*Table structure for table `companies` */



DROP TABLE IF EXISTS `companies`;



CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `primary_contact_id` int(11) DEFAULT NULL,
  `website` varchar(127) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `primary_contact_id` (`primary_contact_id`),
  CONSTRAINT `companies_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `companies_ibfk_2` FOREIGN KEY (`primary_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `companies` */



/*Table structure for table `company_assignment_lu` */



DROP TABLE IF EXISTS `company_assignment_lu`;



CREATE TABLE `company_assignment_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL COMMENT 'The component_id of the assignee to which this component has been assigned.',
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `company_id` (`company_id`) USING BTREE,
  CONSTRAINT `company_assignment_lu_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `company_assignment_lu` */



/*Table structure for table `company_contact_lu` */



DROP TABLE IF EXISTS `company_contact_lu`;



CREATE TABLE `company_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB;



/*Data for the table `company_contact_lu` */



/*Table structure for table `company_type_lu` */



DROP TABLE IF EXISTS `company_type_lu`;



CREATE TABLE `company_type_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `company_type_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `company_type_id` (`company_type_id`),
  CONSTRAINT `company_type_lu_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `company_type_lu_ibfk_2` FOREIGN KEY (`company_type_id`) REFERENCES `company_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `company_type_lu` */



/*Table structure for table `company_types` */



DROP TABLE IF EXISTS `company_types`;



CREATE TABLE `company_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `company_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `company_types` */



insert  into `company_types`(`id`,`account_id`,`name`,`is_deleted`) values (1,0,'Coach',0),(2,0,'Recruit',0),(3,0,'Sphere',0),(4,0,'Client',0),(5,0,'Prospect',0),(6,0,'Lender',0),(7,0,'Vendor',0),(8,0,'Title Company',0),(9,0,'Home Inspector',0),(10,0,'WDO Inspector',0),(11,0,'General Contractor',0),(12,0,'Insurance',0),(13,0,'Pool Inspector',0),(14,0,'Electrician',0),(15,0,'Plumber',0),(16,0,'Painter',0),(17,0,'Roofer',0),(18,0,'Mover',0),(19,0,'Real Estate Brokerage',0);



/*Table structure for table `component_types` */



DROP TABLE IF EXISTS `component_types`;



CREATE TABLE `component_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `display_name` varchar(63) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `component_types` */



insert  into `component_types`(`id`,`name`,`display_name`) values (1,'contacts','Contacts'),(2,'buyers','Buyers'),(3,'sellers','Sellers'),(4,'listings','Listings'),(5,'closings','Closings'),(6,'actionPlans','Action Plans'),(7,'actionPlanItem','Action Plan Items'),(8,'task','Task'),(9,'email','Email'),(10,'projects','Projects'),(11,'projectItems','Project Items'),(12,'assignment','Assignment'),(13,'companies','Companies'),(14,'recruits','Recruiting'),(15,'appointments','Appointments'),(16,'featuredAreas','Featured Areas'),(17,'savedSearches','Saved Searches'),(18,'consoleCommand','Console Command'),(19,'referrals','Referrals');



/*Table structure for table `consultations` */



DROP TABLE IF EXISTS `consultations`;



CREATE TABLE `consultations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `set_on_datetime` timestamp NULL DEFAULT NULL,
  `set_by` int(11) DEFAULT NULL,
  `set_for_datetime` timestamp NULL DEFAULT NULL,
  `met_datetime` timestamp NULL DEFAULT NULL,
  `met_by` int(11) DEFAULT NULL,
  `met_result_ma` tinyint(1) DEFAULT '0',
  `met_location` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `met_by` (`met_by`),
  KEY `set_by` (`set_by`),
  CONSTRAINT `consultations_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `consultations_ibfk_2` FOREIGN KEY (`met_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `consultations_ibfk_3` FOREIGN KEY (`set_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `consultations` */



/*Table structure for table `contact_assignment_lu` */



DROP TABLE IF EXISTS `contact_assignment_lu`;



CREATE TABLE `contact_assignment_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `assignment_type_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `contact_assignment_lu` */



/*Table structure for table `contact_attribute_account_lu` */



DROP TABLE IF EXISTS `contact_attribute_account_lu`;



CREATE TABLE `contact_attribute_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_attribute_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_attribute_id` (`contact_attribute_id`),
  CONSTRAINT `contact_attribute_account_lu_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_attribute_account_lu_ibfk_2` FOREIGN KEY (`contact_attribute_id`) REFERENCES `contact_attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `contact_attribute_account_lu` */



insert  into `contact_attribute_account_lu`(`id`,`account_id`,`contact_attribute_id`,`is_deleted`) values (1,1,1,0),(2,1,2,0),(3,1,3,0),(4,1,4,0),(5,1,5,0),(6,1,6,0),(7,1,7,0),(8,1,8,0),(9,1,9,0),(10,1,10,0),(11,1,11,0),(12,1,12,0);



/*Table structure for table `contact_attribute_values` */



DROP TABLE IF EXISTS `contact_attribute_values`;



CREATE TABLE `contact_attribute_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `contact_attribute_id` int(11) DEFAULT NULL,
  `value` varchar(750) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `contact_attribute_id` (`contact_attribute_id`),
  CONSTRAINT `contact_attribute_values_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_attribute_values_ibfk_2` FOREIGN KEY (`contact_attribute_id`) REFERENCES `contact_attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `contact_attribute_values` */



/*Table structure for table `contact_attributes` */



DROP TABLE IF EXISTS `contact_attributes`;



CREATE TABLE `contact_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `label` varchar(63) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT 999999,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `data_type_id` (`data_type_id`),
  KEY `name` (`name`) USING BTREE,
  CONSTRAINT `contact_attributes_ibfk_1` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `contact_attributes` */

INSERT INTO `contact_attributes` (`id`, `name`, `label`, `data_type_id`, `sort_order`, `is_deleted`) VALUES
(1, 'birthday', 'Birthday', 2, 1, 0),
(2, 'closing_anniversary', 'Closing Anniversary', 2, 3, 0),
(3, 'family', 'Family', 1, 5, 0),
(4, 'hobbies', 'Hobbies', 1, 6, 0),
(5, 'books', 'Books', 1, 7, 0),
(6, 'movies', 'Movies', 1, 8, 0),
(7, 'restaurants', 'Restaurants', 1, 9, 0),
(8, 'career', 'Career', 1, 10, 0),
(9, 'facebook', 'Facebook', 1, 11, 0),
(10, 'linkedIn', 'LinkedIn', 1, 13, 0),
(11, 'wedding_anniversary', 'Wedding Anniversary', 2, 4, 0),
(12, 'spouse_birthday', 'Spouse Birthday', 2, 3, 0);

--
-- Table structure for table `contact_relationship_types`
--

CREATE TABLE IF NOT EXISTS `contact_relationship_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(31) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB;

--
-- Dumping data for table `contact_relationship_types`
--

INSERT INTO `contact_relationship_types` (`id`, `name`, `updated_by`, `updated`, `added_by`, `added`, `is_deleted`) VALUES
(1, 'Friend', NULL, '2014-03-03 06:20:51', NULL, '2014-03-03 00:57:02', 0),
(2, 'Family', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(3, 'Mother', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(4, 'Father', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(5, 'Brother', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(6, 'Sister', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(7, 'Aunt', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(8, 'Uncle', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(9, 'Grandmother', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(10, 'Grandfather', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(11, 'Neice', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(12, 'Nephew', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(13, 'Mother-in-law', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(14, 'Father-in-law', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(15, 'Sister-in-law', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(16, 'Brother-in-law', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(17, 'Co-worker', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:02', 0),
(18, 'Church Member', NULL, '2014-03-03 01:03:04', NULL, '2014-03-03 00:57:02', 0),
(19, 'Son', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:36', 0),
(20, 'Daughter', NULL, '2014-03-03 01:02:12', NULL, '2014-03-03 00:57:36', 0),
(21, 'Cousin', NULL, '2014-03-03 07:21:12', NULL, '2014-03-03 01:01:20', 0),
(22, 'Grandson', NULL, '2014-03-03 07:21:30', NULL, NULL, 0),
(23, 'Granddaughter', NULL, '2014-03-03 07:21:30', NULL, NULL, 0),
(24, 'Husband', NULL, '2014-03-03 07:21:51', NULL, NULL, 0),
(25, 'Wife', NULL, '2014-03-03 07:21:51', NULL, NULL, 0),
(26, 'Other', NULL, '2014-03-03 07:22:39', NULL, NULL, 0),
(27, 'Business Partners', NULL, '2014-03-03 07:22:39', NULL, NULL, 0);





/*Table structure for table `contact_relationship_lu` */



DROP TABLE IF EXISTS `contact_relationship_lu`;



CREATE TABLE `contact_relationship_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_contact_id` int(11) DEFAULT NULL,
  `origin_relationship_type_id` int(11) DEFAULT NULL,
  `related_to_contact_id` int(11) DEFAULT NULL,
  `related_to_relationship_type_id` int(11) DEFAULT NULL,
  `is_referral` tinyint(1) DEFAULT NULL,
  `is_past_referral` tinyint(1) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,cona
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_relationship_lu_ibfk_1` (`updated_by`),
  KEY `contact_relationship_lu_ibfk_2` (`added_by`),
  KEY `origin_contact_id` (`origin_contact_id`),
  KEY `related_to_contact_id` (`related_to_contact_id`),
  KEY `origin_relationship_type_id` (`origin_relationship_type_id`),
  KEY `related_to_relationship_type_id` (`related_to_relationship_type_id`)
  CONSTRAINT `contact_relationship_lu_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_relationship_lu_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_relationship_lu_ibfk_3` FOREIGN KEY (`origin_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_relationship_lu_ibfk_4` FOREIGN KEY (`origin_relationship_type_id`) REFERENCES `contact_relationship_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_relationship_lu_ibfk_5` FOREIGN KEY (`related_to_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_relationship_lu_ibfk_6` FOREIGN KEY (`related_to_relationship_type_id`) REFERENCES `contact_relationship_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `contact_relationship_lu` */



/*Table structure for table `contact_type_lu` */



DROP TABLE IF EXISTS `contact_type_lu`;



CREATE TABLE `contact_type_lu` (
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `contact_type_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_id`,`contact_type_id`),
  KEY `contact_type_id` (`contact_type_id`),
  CONSTRAINT `contact_type_lu_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contact_type_lu_ibfk_2` FOREIGN KEY (`contact_type_id`) REFERENCES `contact_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `contact_type_lu` */



/*Table structure for table `contact_types` */



DROP TABLE IF EXISTS `contact_types`;



CREATE TABLE `contact_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `contact_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `contact_types` */



insert  into `contact_types`(`id`,`account_id`,`name`,`is_deleted`) values (1,0,'Coach',0),(2,0,'Agent',0),(3,0,'Recruit',0),(4,0,'Sphere',0),(5,0,'Client',0),(6,0,'Prospect',0),(7,0,'Lender',0),(8,0,'Vendor',0),(9,0,'Title Company',0),(10,0,'Home Inspector',0),(11,0,'WDO Inspector',0),(12,0,'General Contractor',0),(13,0,'Insurance',0),(14,0,'Pool Inspector',0),(15,0,'Electrician',0),(16,0,'Plumber',0),(17,0,'Painter',0),(18,0,'Roofer',0),(19,0,'Mover',0),(20,2,'Past Clients',0),(21,2,'Investor',0),(22,2,'Investor- Invitation Homes',0),(23,2,'Investor  - Inheritance Investments',0);



/*Table structure for table `contacts` */



DROP TABLE IF EXISTS `contacts`;



CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `contact_status_ma` int(11) DEFAULT NULL,
  `lead_type_ma` tinyint(1) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `spouse_first_name` varchar(50) DEFAULT NULL,
  `spouse_last_name` varchar(50) DEFAULT NULL,
  `password` varchar(127) DEFAULT NULL,
  `notes` varchar(750) DEFAULT NULL,
  `session_id` varchar(127) DEFAULT NULL,
  `hash` varchar(50) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(50) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `source_id` (`source_id`),
  KEY `first_name` (`first_name`) USING BTREE,
  KEY `last_name` (`last_name`) USING BTREE,
  KEY `last_login` (`last_login`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `contacts` */

insert  into `contacts`(`id`,`account_id`,`first_name`,`last_name`,`password`) values (1,1,'STM','QA','stmqa2014');


/*Table structure for table `craigslist_template_groups` */



DROP TABLE IF EXISTS `craigslist_template_groups`;



CREATE TABLE `craigslist_template_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `content` varchar(800) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `craigslist_template_groups_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `craigslist_template_groups` */



/*Table structure for table `craigslist_templates` */



DROP TABLE IF EXISTS `craigslist_templates`;



CREATE TABLE `craigslist_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `craigslist_template_group_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `content` varchar(800) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craigslist_template_group_id` (`craigslist_template_group_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `craigslist_templates_ibfk_1` FOREIGN KEY (`craigslist_template_group_id`) REFERENCES `craigslist_template_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `craigslist_templates_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `craigslist_templates` */



/*Table structure for table `custom_field_groups` */



DROP TABLE IF EXISTS `custom_field_groups`;



CREATE TABLE `custom_field_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` varchar(50) DEFAULT NULL,
  `table_name` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `custom_field_groups` */



/*Table structure for table `custom_field_values` */



DROP TABLE IF EXISTS `custom_field_values`;



CREATE TABLE `custom_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `custom_field_id` (`custom_field_id`),
  CONSTRAINT `custom_field_values_ibfk_1` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `custom_field_values` */



/*Table structure for table `custom_fields` */



DROP TABLE IF EXISTS `custom_fields`;



CREATE TABLE `custom_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_group_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `sort_order` tinyint(4) DEFAULT NULL COMMENT 'may not be needed if views are custom',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `custom_field_group_id` (`custom_field_group_id`),
  CONSTRAINT `custom_fields_ibfk_1` FOREIGN KEY (`custom_field_group_id`) REFERENCES `custom_field_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `custom_fields` */



/*Table structure for table `data_types` */



DROP TABLE IF EXISTS `data_types`;



CREATE TABLE `data_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `data_types` */



insert  into `data_types`(`id`,`name`) values (1,'text'),(2,'date'),(3,'datetime'),(4,'dollars'),(5,'integer'),(6,'decimal'),(7,'boolean'),(8,'textarea');



/*Table structure for table `document_permissions` */



DROP TABLE IF EXISTS `document_permissions`;



CREATE TABLE `document_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `component_type_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `permission_level_ma` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `document_permissions` */



/*Table structure for table `document_types` */



DROP TABLE IF EXISTS `document_types`;



CREATE TABLE `document_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_core` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  CONSTRAINT `document_types_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `document_types` */



insert  into `document_types`(`id`,`component_type_id`,`name`,`is_core`,`is_deleted`) values (1,1,'Misc',1,0),(2,2,'Pre-qual Letter',1,0),(3,2,'Proof of Funds',1,0),(4,2,'Loyalty Agreement',1,0),(5,3,'Listing Agreement',1,0),(6,3,'Seller\'s Disclosure',1,0),(7,3,'Homeowners Association Docs',1,0),(8,3,'Contract Offer',1,0),(9,5,'Executed Contract',1,0),(10,5,'Inspections',1,0),(11,5,'Repair Agreement',1,0),(12,5,'Contract Addendum',1,0),(13,5,'Prelim HUD',1,0),(14,5,'Executed HUD',1,0),(15,10,'Office Doc',1,0),(16,14,'Application',1,0),(17,14,'DISC',1,0),(18,14,'AVA',0,0),(19,14,'References',1,0),(20,14,'Phone Interview Notes',0,0),(21,14,'Screening Interview Notes',0,0),(22,14,'Comprehensive Interview',0,0),(23,14,'Other',0,0),(24,19,'Referral Agreement',1,0);



/*Table structure for table `documents` */



DROP TABLE IF EXISTS `documents`;



CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `visibility_ma` tinyint(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file_name` varchar(63) NOT NULL,
  `file_type` varchar(7) NOT NULL,
  `file_size` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `document_type_id` (`document_type_id`),
  CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `documents` */



/*Table structure for table `domains` */



DROP TABLE IF EXISTS `domains`;



CREATE TABLE `domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(63) DEFAULT NULL,
  `directory` varchar(63) DEFAULT NULL,
  `theme` varchar(63) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`name`) USING BTREE,
  KEY `account_id` (`account_id`),
  CONSTRAINT `domains_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `domains` */



/*Table structure for table `email_click_tracking` */



DROP TABLE IF EXISTS `email_click_tracking`;



CREATE TABLE `email_click_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_message_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `email_link_id` int(11) NULL DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email_message_fk` (`email_message_id`),
  KEY `contact_fk` (`contact_id`),
  KEY `email_link_fk` (`email_link_id`),
  CONSTRAINT `contact_fk` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `email_link_fk` FOREIGN KEY (`email_link_id`) REFERENCES `email_links` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `email_message_fk` FOREIGN KEY (`email_message_id`) REFERENCES `email_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;



/*Data for the table `email_click_tracking` */



/*Table structure for table `email_links` */



DROP TABLE IF EXISTS `email_links`;



CREATE TABLE `email_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `email_links` */



/*Table structure for table `email_messages` */



DROP TABLE IF EXISTS `email_messages`;



CREATE TABLE `email_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_type_ma` tinyint(1) DEFAULT '0',
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `to_email_id` int(11) DEFAULT NULL,
  `to_email_cc` text,
  `to_email_bcc` text,
  `from_email_id` int(11) DEFAULT NULL,
  `processed_datetime` timestamp NULL DEFAULT NULL,
  `delivery_status_ma` tinyint(1) DEFAULT '0',
  `needs_response` tinyint(1) DEFAULT '0',
  `imap_message_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`) USING BTREE,
  KEY `to_email_id` (`to_email_id`) USING BTREE,
  CONSTRAINT `email_messages_component_type_id` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `email_messages` */



/*Table structure for table `email_opt_status` */



DROP TABLE IF EXISTS `email_opt_status`;



CREATE TABLE `email_opt_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `email_opt_status` */



insert  into `email_opt_status`(`id`,`name`) values (1,'0'),(2,'0'),(3,'0'),(4,'0'),(5,'0'),(6,'0'),(7,'0');



/*Table structure for table `email_status_log` */



DROP TABLE IF EXISTS `email_status_log`;



CREATE TABLE `email_status_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_id` int(11) NOT NULL,
  `email_status_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `added_by_component_type_id` int(11) NOT NULL,
  `added_by_component_id` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email_ref` (`email_id`),
  KEY `email_status_ref` (`email_status_id`),
  KEY `component_type_id_ref` (`added_by_component_type_id`),
  CONSTRAINT `component_type_id_ref` FOREIGN KEY (`added_by_component_type_id`) REFERENCES `component_types` (`id`),
  CONSTRAINT `email_ref` FOREIGN KEY (`email_id`) REFERENCES `emails` (`id`),
  CONSTRAINT `email_status_ref` FOREIGN KEY (`email_status_id`) REFERENCES `email_statuses` (`id`)
) ENGINE=InnoDB;



/*Data for the table `email_status_log` */



/*Table structure for table `email_statuses` */



DROP TABLE IF EXISTS `email_statuses`;



CREATE TABLE `email_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `email_statuses` */



insert  into `email_statuses`(`id`,`name`) values (1,'Single Opt-in'),(2,'Double Opt-in'),(3,'Manual Entry'),(4,'Verified'),(5,'Opt-out'),(6,'Soft Bounce'),(7,'Hard Bounce');



/*Table structure for table `email_templates` */



DROP TABLE IF EXISTS `email_templates`;



CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT NULL,
  `description` varchar(63) DEFAULT NULL,
  `subject` varchar(128) DEFAULT NULL,
  `body` text,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `component_typd_id` (`component_type_id`) USING BTREE,
  CONSTRAINT `email_templates_component_type_id` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `email_templates` */



insert  into `email_templates`(`id`,`component_type_id`,`status_ma`,`description`,`subject`,`body`,`is_deleted`) values (2,2,1,'','Dropping the ball...?','<p style=\"color: #222222; font-family: arial, sans-serif; font-size: 13px; margin: 0in 0in 0pt;\">Hi {{first_name}},<br /><br />You\'ve been receiving homes on the market for a while but I haven\'t been able to reach you...<br /><br />Please reply to this email and let me know if you like what you are receiving or if you would like me to stop sending the homes.&nbsp; <br /><br />BTW, Do you prefer to communicate by email, text or cell?<br /><br />Just let me know. &nbsp;<br /><br />Thanks!</p><div>&nbsp;</div>',NULL),(3,2,1,'New Lead Email','Home Search...','<span style=\"font-size: 13px; font-family: arial, helvetica, sans-serif;\">Hello,</span><br /><br /><span style=\"font-size: 13px; font-family: arial, helvetica, sans-serif;\">Just had a quick question for you about your home search.<br /><br />Are schools important to you?<br /><br />Just reply and let me know. &nbsp;<br /><br />Thanks!</span>',NULL),(4,2,1,'','Wrong phone #...','<span style=\"font-family: arial, helvetica, sans-serif; font-size: 14px;\">Hello,<br /><br />I just called you but I must have it down wrong, probably off by a digit :) ... can you pls confirm what the best # and time is to reach you?<br /><br />Thanks!</span>',NULL),(5,2,0,'Day 3: Homes I Sent You...','Homes I Sent to You...','<!--[if gte mso 9]><xml>\r\n <o:OfficeDocumentSettings>\r\n  <o:PixelsPerInch>96</o:PixelsPerInch>\r\n  <o:TargetScreenSize>800x600</o:TargetScreenSize>\r\n </o:OfficeDocumentSettings>\r\n</xml><![endif]--> <!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>JA</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:EnableOpenTypeKerning/>\r\n   <w:DontFlipMirrorIndents/>\r\n   <w:OverrideTableStyleHps/>\r\n  </w:Compatibility>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"/>\r\n   <m:brkBin m:val=\"before\"/>\r\n   <m:brkBinSub m:val=\"&#45;-\"/>\r\n   <m:smallFrac m:val=\"off\"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val=\"0\"/>\r\n   <m:rMargin m:val=\"0\"/>\r\n   <m:defJc m:val=\"centerGroup\"/>\r\n   <m:wrapIndent m:val=\"1440\"/>\r\n   <m:intLim m:val=\"subSup\"/>\r\n   <m:naryLim m:val=\"undOvr\"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"\r\n  DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"276\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" Name=\"Default Paragraph Font\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Table Grid\"/>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--> <!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\ntable.MsoNormalTable\r\n	{mso-style-name:\"Table Normal\";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-parent:\"\";\r\n	mso-padding-alt:0in 5.4pt 0in 5.4pt;\r\n	mso-para-margin:0in;\r\n	mso-para-margin-bottom:.0001pt;\r\n	mso-pagination:widow-orphan;\r\n	font-size:10.0pt;\r\n	font-family:\"Times New Roman\";}\r\n</style>\r\n<![endif]--> <!--StartFragment--><p class=\"MsoNormal\"><span style=\"font-size: 7.5pt; font-family: Arial;\"><span style=\"color: #222222; font-family: arial, helvetica, sans-serif;\">Hi {{first_name}},<br /></span><br />Great to hear from you! I just wanted to make sure that you received the list of homes.<br /> <br /> A couple quick questions... <br /> <br /> 1)&nbsp;&nbsp; What&rsquo;s your ideal timeframe for buying a home? <br /> 2)&nbsp;&nbsp; Can you tell me a little more about your situation?<br /> <br /> Just reply and let me know. Have a wonderful day!<br /> <!--[if !supportLineBreakNewLine]--><br /> <!--[endif]--></span></p><p class=\"MsoNormal\"><strong><span style=\"font-size: 7.5pt; font-family: Arial;\">P.S.</span></strong><strong><span style=\"text-decoration: underline;\"><span style=\"font-size: 7.5pt; font-family: Arial; color: red;\"><a href=\"http://www.beonhousehunters.com/be-on-hgtv-house-hunters\">Find out how you can be on HGTV House Hunters. CLICK HERE.</a></span></span></strong></p><div style=\"color: #222222; font-family: arial, sans-serif; font-size: 13px;\">&nbsp;</div><!--EndFragment-->',NULL),(6,2,1,'Day 0: Are School Districts Important? ','Are School Districts Important to You? ','<div style=\"color: #222222; font-family: arial, sans-serif; font-size: 13px;\"><span style=\"font-family: arial, helvetica, sans-serif;\">Hi {{first_name}},</span></div><div style=\"color: #222222; font-family: arial, sans-serif; font-size: 13px;\"><span style=\"font-family: arial, helvetica, sans-serif;\">&nbsp;</span></div><div style=\"color: #222222; font-family: arial, sans-serif; font-size: 13px;\"><span style=\"font-family: arial, helvetica, sans-serif;\">Quick question on your home search... are school districts important to you?</span></div>',NULL),(7,2,0,'Day 7: Market Watch Updates','Market Watch Updates & Subdivision Search','Hey {{first_name}},<br /><br />FYI, I\'ll keep you up-to-date on the newest homes and changes in the market. I call it \"Market Watch Updates\". &nbsp;:)<br /><br />Also, as you begin your house hunting, I just wanted to let you know that you can now search by Subdivision name or&nbsp;<span style=\"text-decoration: underline;\"><a href=\"http://www.christineleeteam.com/search/neighborhood\"><strong>CLICK HERE to search home in a specific subdivision.</strong></a><br /></span><br />P.S. Can you tell me anything else about what\'s important to you in your next home?&nbsp;<br /><br />Just reply and let me know and I\'ll send you over a few subdivisions I would recommend.',NULL),(8,2,0,'Day 13: What are CDD Fees?','What are CDD Fees?','Hi {{first_name}},<br /><br />I have some great info on CDD fees... what they truly are, who has to pay them, if it should influence your home selection process, etc.&nbsp;<br /><br />If you\'re interested, just reply and I\'ll send you a clear and simple answer to those questions... in Layman\'s terms! :)<br /><br />By the way, what\'s prompting your move?&nbsp;<br /><br />P.S. Do you know anyone who would be interested in being on HGTV\'s House Hunters?&nbsp;<a href=\"http://www.beonhousehunters.com/be-on-hgtv-house-hunters\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to apply for HGTV\'s House Hunters.</strong></span></a>',NULL),(9,2,0,'Day 20: List of Top Neighborhoods','List of Top Neighborhoods','Hi {{first_name}},<br />There are so many great neighborhoods in Jacksonville... sometimes it\'s hard to narrow down the search! Can you tell me anything else about what you\'re looking for and I\'ll recommend some that fit you best.&nbsp;<br />Just reply and let me know if this might help in the search!<br /><a href=\"http://www.christineleeteam.com/search/neighborhood\"><span style=\"text-decoration: underline;\"><strong>P.S. Click here to search top communities by area!</strong></span></a>',NULL),(10,2,0,'Day 30: Time Flies... Checking In','Time Flies... Checking In','Hi {{first_name}},<br /><br />Hope everything is going well... Wow, I can\'t believe it\'s already been a month since I started sending you homes for sale. Time really flies!<br /><br />I was just making sure that you\'re happy with the homes I\'ve been sending you. I can fine-tune my search some more if you\'d like... any new areas, prices, or changes in your situation?<br /><br />Happy house hunting! :)<br /><br />P.S. I\'d like to touch base with you... What\'s the best time and number to reach you?&nbsp;<br /><br />Look forward to hearing from you!',NULL),(11,2,0,'Day 40: Saving on Your Home...','Saving on Your Home...','Hi {{first_name}},<br /><br />Are you interested in saving money?<br /><br />In town or moving to Jacksonville area -- you can save money with our Vendor Savings Program, including offers from:<br /><ul><li>Movers</li><li>Painters</li><li>Roofers</li><li>Pressure Washers</li><li>And Much More!</li></ul>Just reply and let me know!<span style=\"text-decoration: underline;\"><br /><br /></span><br />P.S. We also have a \"Complete Relocation Package\" for homebuyers that includes info on local neighborhoods, restaurants, schools, taxes, insurance, important contacts such as utilities, hospitals, etc.&nbsp;<a href=\"http://www.christineleeteam.com/relocation-guide\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE for your FREE relocation guide.</strong></span></a>',NULL),(12,2,0,'Day 50: A Home I Saw...','A Home I Saw...','Hi {{first_name}},<br /><br />As I looked over the new listings of homes today, I noticed a great home and thought you may want to see more photos. Just reply back and I will send them...<br /><br />P.S. To schedule a showing, please give me 1-2 days notice... Although I\'m available,&nbsp;<strong>the seller may require more time. (If you\'re coming from out of town let me know a week or so prior so I can plan a more informative showing).<br /></strong><br />Thanks in advance!<br /><br /><a href=\"http://www.christineleeteam.com/search/full\"><span style=\"text-decoration: underline;\"><strong>P.S. CLICK HERE to start searching for your dream home!</strong></span></a><br /><br />',NULL),(13,2,0,'Day 60: Ready to see homes? ','Ready to see homes?','{{first_name}}, <br /><br />I was just going over my schedule today...<br /><br />Would you like to look at any homes in the next few weeks? <br /><br />Just reply to this message and let me know. <br /><br /><a href=\"http://www.beonhousehunters.com/be-on-hgtv-house-hunters\"><strong>P.S. <span style=\"text-decoration: underline;\">Find out how you can be on HGTV House Hunters. CLICK HERE. </span></strong></a>',NULL),(14,2,0,'Day 70: Ready for a Great Deal?...','Ready for a Great Deal?...','Hi {{first_name}},<br /><br />There are a couple things that you can do now that will give you an advantage by being ready for great deal&hellip;<br /><br />Can we touch base real quick? What&rsquo;s the best time and number to reach you???Just reply and let me know&hellip;<br /><br /><a href=\"http://www.christineleeteam.com/free-money-for-buyers\"><strong>P.S. <span style=\"text-decoration: underline;\">Find out how you can save money when buying a home. CLICK HERE.</span></strong></a>',NULL),(15,2,0,'Day 80: Timing the Market…','Timing the Market…','Hi {{first_name}},<br /><br />I have some great information that has helped my other clients benefit from today&rsquo;s real estate market.&nbsp; I think it could help you as well&hellip;<br /><br />Just reply and let me know if you&rsquo;re interested.<br /><br />Have a great week!<br /><br /><a href=\"http://www.christineleeteam.com/this-month-in-real-estate\"><strong>P.S. <span style=\"text-decoration: underline;\">CLICK HERE to see whats going on This Month in Real Estate.</span></strong></a>',NULL),(16,2,0,'Day 90: Financing Oops!...','Financing Oops!...','Hi {{first_name}},<br /><br />Usually, a little &ldquo;Oops&rdquo; in financing means thousands of dollars to you&hellip; Avoid the pitfalls of bad rates or advice that can cost you a lot of money and aggravation.??<br /><br />Here&rsquo;s a quick video for you&hellip; <a href=\"http://www.christineleeteam.com/myth-buster-an-agent-doesnt-matter\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to watch the video</strong></span>.</a><br /><br />??Would it be ok if I connect you with my best lender? Just reply and let me!',NULL),(17,2,0,'Day 100: Quick Review…','Quick Review…','Hi {{first_name}},??<br /><br />I was just making sure that I&rsquo;m sending you homes that fit what you&rsquo;re looking for? Feel free to update me on your latest wants and needs.?<br /><br />I need your help to improve&hellip; can you take a second to answer a few quick questions for me???<br /><br />1) How would you rate my service overall? Rate on a scale of 1(bad) &ndash; 10(excellent).<br /><br />??2) What did you like best? (ex. Website, email, information, communication, etc.)??<br /><br />3) How can I improve? (ex. Website, email, information, communication, etc.)??<br /><br />4) Can you introduce me to just 1 person you know that may be thinking of buying or selling???<br /><br />This will help more than you&rsquo;ll know&hellip; Thank you for your time!:)??<br /><br />Happy house-hunting! :)??<br /><br /><a href=\"http://www.beonhousehunters.com/be-on-hgtv-house-hunters\"><strong>P.S.</strong><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></a>',NULL),(18,2,0,'Day 110: Top Schools & Golf Communities','Top Schools & Golf Communities','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br /> I have a list of neighborhoods with the top rated schools in the area, as well as a list of <em>great</em> golf communities.<br /> <br /> Some things to consider as it may improve the re-sale value of your home.<br /> <br /> Just reply with what you&rsquo;re looking for and I&rsquo;ll send a list over to you!</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong><br />P.S. </strong></span><a href=\"http://www.christineleeteam.com/top-schools-report\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to view the top school district communities in your area.&nbsp;</strong></span></a></p>',NULL),(19,2,0,' Day 120: The Truth about Zillow…','The Truth about Zillow...','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong><br /> </strong>Here&rsquo;s a video on the truth about Zillow that sheds some light on your home value&hellip;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\"><a href=\"http://www.christineleeteam.com/the-truth-about-zillow-and-other-avms\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE to view The Truth about Zillow.</strong></span></a><em><br /> <br /> </em>Hope this helps!<br /> <br /> <strong>P.S.</strong> Do you have a home to sell or know someone who needs to sell? <a href=\"http://www.jaxvalues.com/houseValues\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE to find out what your home is worth.</strong></span></a></p>',NULL),(20,2,0,'Day 130: Fixing up a Home…','Fixing up a Home…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><br /></p><p style=\"margin: 0px; font-family: Arial;\">There&rsquo;s a great way for you to buy and fix up a home&hellip;with no out-of-pocket expense!</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Just let me know if you are interested in the details.<br /><strong><br /> </strong></p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong>P.S. </strong></span><span style=\"text-decoration: underline;\"><strong>Feel free to contact my favorite contractors. CLICK HERE to search for a contractor you need.&nbsp;</strong></span></p>',NULL),(21,2,0,'Day 140: The “F” word….Foreclosure! ','The “F” word….Foreclosure! ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br />As most people know&hellip;foreclosure is a dirty word&hellip;but it may also be a great chance for you to buy a great deal! Are you interested in taking advantage of today&rsquo;s market?<br /> <br /> Just reply and let me know.<br /> <br /> <strong>P.S.</strong> Do you have any friends that own a home who are distressed and may need some good advice? <br /><br /><a href=\"http://www.christineleeteam.com/stop-foreclosures-with-a-short-sale\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE to learn how to STOP a Foreclosure!</strong></span></a></p>',NULL),(22,2,0,'Day 150: Checking In…Market Update','Checking In…Market Update','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br /> I hope this email finds you doing well! I was just checking in to say \"hello\" and see what was new with your situation. <br /> <br /> Let&rsquo;s touch base real quick&hellip; What&rsquo;s the best time and number where I can reach you? Just reply and let me know. <br /> <br /> I\'ll keep the Market Updates coming:)</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong>P.S.</strong> </span><a href=\"http://www.beonhousehunters.com/be-on-hgtv-house-hunters\"><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></a></p>',NULL),(23,2,0,' Day 160: Homeowner’s Insurance','Homeowner’s Insurance','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<strong><br /> <br /> </strong>I have some great info on homeowners &amp; flood insurance.&nbsp; It gives you an estimate of what to expect on costs for the range of home you&rsquo;re looking for.<br /> <br /> Just reply and let me know if you&rsquo;re interested.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Happy house-hunting!</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong><br />P.S.</strong> </span><a href=\"http://www.christineleeteam.com/first-time-homebuyer\"><span style=\"text-decoration: underline;\"><strong>Is it your first time buying a home? CLICK HERE for a quick video on tips for First Time Home Buyers.&nbsp;</strong></span></a></p>',NULL),(24,2,0,' Day 170: My top pros & contractors','My top pros & contractors','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong><br /> </strong>I&rsquo;ve put together a list of my top professional services &amp; contractors in the area&hellip; everyone from handyman to home lenders. <span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE</strong></span> if you could use a few recommendations.<br /> <br /> P.S. Do you know of anyone else who might be looking to buy or sell a home in the area? Just let me know!</p>',NULL),(25,2,0,' Day 180: New Construction Pitfalls','New Construction Pitfalls','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><br />Building a new home can be exciting &ndash; and exhausting! Let me know if this is something you are considering&hellip;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\"><a href=\"http://www.christineleeteam.com/myth-buster-on-new-construction-homes\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE to view a quick video on Mythbusters of New Construction Homes.&nbsp;</strong></span></a> <em><br /> </em><br /> <strong>P.S.</strong> Who do you know that\'s thinking of buying or selling in today\'s market?</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Have a great week!&nbsp;</p>',NULL),(26,2,0,' Day 190: Ready to See Homes?','Ready to See Homes?','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong><br /> </strong>I was just going over my schedule today... <br /> <br /> Would you like to look at any homes in the next few weeks? <br /> <br /> Just reply to this message and let me know.</p>',NULL),(27,2,0,' Day 200: Lending Myths','Lending Myths','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><br />Inaccurate information has cost buyers a lot of money, time and aggravation --I have some great info to help you avoid the hassle!</p><p style=\"margin: 0px; font-family: Arial;\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong><br /><a href=\"http://www.christineleeteam.com/myths-about-loans-and-lending\">CLICK HERE to watch a video on Mythbuster of Loans and Lenders.</a></strong></span> <br /> <br /> Just reply and let me know what you think...<br /> <br /> P.S. Would it be ok to connect you with my best lender?</p><div>&nbsp;</div>',NULL),(28,2,0,' Day 210: Staging Your Home to Sell','Staging Your Home to Sell','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><br />Staging a home properly helps you get top dollar for your home and it doesn&rsquo;t require a lot of money, just a little time and effort! :)<br /> <br /> <a href=\"http://www.christineleeteam.com/hot-staging-tips-help-your-home-sell-itself\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong>Here&rsquo;s a quick video on Home Staging&nbsp;</strong></span><span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE</strong></span></a><br /> <br /> P.S. Who do you know that\'s thinking of buying or selling in today\'s market?&nbsp;</p>',NULL),(29,2,0,' Day 220: Foreclosure vs. Short Sale?... ','Foreclosure vs. Short Sale?... ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<strong><br /><br /> </strong>Here&rsquo;s some info on the difference between a foreclosure vs. a short sale.<br /> <br /> <span style=\"text-decoration: underline;\">Short sales</span> take between 3-6 months to negotiate and close. Seller&rsquo;s usually live in the home during this period and property conditions <span style=\"text-decoration: underline;\">VARY!</span> &ndash; from pristine to disaster zone.<br /> <br /> <span style=\"text-decoration: underline;\">Foreclosures</span> have a much quicker response &ndash; around 2 weeks and can close in 30-45 days. These homes are usually vacant and the condition ranges from needing major repairs to being move-in ready.</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><br />CLICK HERE for a FREE List of foreclosures in Jacksonville!</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> If you&rsquo;re interested in a great deal, let me know &hellip; what the best time and number to reach you?<span style=\"text-decoration: underline; color: #ff0000;\"><strong>&nbsp;</strong></span></p>',NULL),(30,2,0,' Day 230: Touching Base…Time Flies','Touching Base…Time Flies','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br />Hope the home search is going well... Time really flies! <br /> <br /> I was just checking to see if the homes I\'m sending you still meet what you&rsquo;re looking for. I can fine tune my search some more if you\'d like&hellip; any other areas you\'ve seen or thought of? <br /> <br /> Just reply and let me know about any changes in your latest situation. The house-hunting continues! :)</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong>P.S.</strong> </span><a href=\"http://www.beonhousehunters.com/be-on-hgtv-house-hunters\"><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></a></p>',NULL),(31,2,0,' Day 240: Closing Costs','Closing Costs','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<strong><br /></strong><strong> <br /> </strong>In the spirit of budgeting and planning ahead, I have some great info on closing costs and what to expect. Planning right now can save you a pretty penny! :)<br /> <br /> Let me know if you are interested&hellip;Happy house-hunting!</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><br /><a href=\"http://www.christineleeteam.com/mortgagecalculator\">CLIKC HERE to estimate the monthly mortgage payments for your dream home!</a></strong></span></p>',NULL),(32,2,0,' Day 250: This could be you…','This could be you…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong> <br /> </strong>This could be you! :)<br /> <br /> <span style=\"text-decoration: underline;\"><em><strong>Click here to see&hellip;.!</strong></em></span></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Hope you&rsquo;re having a great day!<strong><em><br /></em></strong></p>',NULL),(33,2,0,' Day 260: A Home I Saw…','A Home I Saw…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<strong><em><br /></em> <br /> </strong>As I looked over the new listings of homes today, I noticed a great home and thought you may want to see more photos. Just reply back and I will send them...&nbsp;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to start searching for your dream home!</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> <strong>P.S.</strong><em> To schedule a showing, pls give me 1-2 days notice... Although I\'m available, </em><strong><em>the seller may need some time to prepare the home</em></strong><em>. The more notice the better :) (</em><strong><em>If you\'re coming from out of town</em></strong><em>, let me know a week or so prior so I can plan a more informative showing) <br /><br /></em>Thanks in advance!&nbsp;</p>',NULL),(34,2,0,'  Day 270: Quick Review…','Quick Review…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br /> I was just making sure that I&rsquo;m sending you homes that fit what you&rsquo;re looking for? Feel free to update me on your latest wants and needs.<br /> <br /> Let&rsquo;s touch base to catch up real quick&hellip; What&rsquo;s the best time and number to reach you? <br /> <br /> I\'ll keep the Market Updates coming! :) <br /> <br /> P.S. I need your help to improve&hellip; can you take a second to answer a few quick questions for me?<br /> <br /> 1) How would you rate my service overall? Rate on a scale of 1(bad) &ndash; 10(excellent).<br /> <br /> 2) What did you like best? (ex. Website, email, communication, etc.)<br /> <br /> 3) How can I improve? (ex. Website, email, communication, etc.)<br /> <br /> 4) Can you name just 1 person you know that <em>may</em> be thinking of buying or selling?<br /> <br /> This will help more than you&rsquo;ll know&hellip;Thank you for your time!:)</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong>P.S.</strong> </span><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></p>',NULL),(35,2,0,'  Day 280: Home Inspection – Saving or costing you?','Home Inspection – Saving or costing you?','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<strong><br /> <br /> </strong>A home inspector can either save you money or cost you!&nbsp; The key is to have a good one&hellip; which I do. :)<br /> <br /> Just reply and let me know if you&rsquo;re interested and I&rsquo;ll send you the name of my best inspector.&nbsp;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to view ALL my favorite contractors!</strong></span><span style=\"color: #000000;\"><br /> <br /> Let the savings continue! :)</span></p>',NULL),(36,2,0,' Day 290: Homebuyer Survival Kit','Homebuyer Survival Kit','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong> <br /> </strong>I have a Homebuyer Survival Kit that helps you plan on buying your next home the right way&hellip;saving you time, effort and money!<br /><br /></p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to get your FREE Homebuyer Survival Kit now!</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> Just reply to let me know if this might help you. Hope you are having a great week!</p><div>&nbsp;</div>',NULL),(37,2,0,'Day 300: Knowing House Values','Knowing House Values','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong> <br /> </strong>Here is a great website that will give you a report of what your home is currently worth in today&rsquo;s market.<br /> <br /> Visit&nbsp; <a href=\"http://www.JaxValues.com\"><span style=\"color: #3a00ff;\">www.JaxValues.com</span></a>. Hope this helps!</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>OR CLICK HERE to request your home value NOW!</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://christineleeteam.com/sellers_housevalue.php</em>)<br /> <br /> P.S. Can you introduce me to 1 person you know that could use some expert advice in today&rsquo;s market to sell or buy a home?</p>',NULL),(38,2,0,'Day 310: Ready to see homes?','Ready to see homes?','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br />I was just going over my schedule today...&nbsp;<br /> <br /> Would you like to look at any homes in the next few weeks? <br /> <br /> Just reply to this message and let me know.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong>P.S.</strong> </span><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://www.beonhousehunters.com/)</em></p>',NULL),(39,2,0,'Day 320: Financing Oops…','Financing Oops…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong> <br /> </strong>Usually, a little &ldquo;Oops&rdquo; in financing means thousands of dollars to you&hellip; Avoid the pitfalls of bad rates or advice that can cost you a lot of money and aggravation.<br /> <br /> Here&rsquo;s a quick video for you&hellip; Click here to <a href=\"http://www.christineleeteam.com/buyers_loanmyths.php\"><span style=\"color: #3a00ff;\">watch the video</span></a><br /> <br /> Would it be ok if I connect you with my best lender? Just reply and let me know.</p>',NULL),(40,2,0,'Day 330: Foreclosure vs. Short Sales','Foreclosure vs. Short Sales','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong> <br /> </strong>Here&rsquo;s some info on the difference between a foreclosure vs. a short sale.<br /> <br /> 1) &nbsp; <span style=\"text-decoration: underline;\">Short sales</span> take between 3-6 months to negotiate and close. Seller&rsquo;s usually live in the home during this period and property conditions <span style=\"text-decoration: underline;\">VARY!</span> &ndash; from pristine to disaster zone.<br /> <br /> 2) &nbsp; Foreclosures have a much quicker response &ndash; around 2 weeks and can close in 30-45 days. These homes are usually vacant and the condition ranges from needing major repairs to being move-in ready.</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to stop foreclosure!&nbsp;</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://christineleeteam.com/sellers_shortsale.php)</em><br /> <br /> If you&rsquo;re interested in a great deal, let me know &hellip; what&rsquo;s the best time and number to reach you?<br /> <br /> Thanks!</p>',NULL),(41,2,0,'Day 340: Reducing Property Taxes…. ','Reducing Property Taxes…. ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Here are a couple property tax money saving facts&hellip; <br /> <br /> 1) &nbsp;&nbsp;Once you buy a home, don&rsquo;t forget to apply for your homestead which will give you a $50,000 homestead exemption where that amount is exempt from the taxable value. <br /> <br /> 2) &nbsp;&nbsp;If you homestead a property in Florida, your property taxes increase is capped at 3%. This can be a great advantage when you get a good deal in a down market &ndash; potentially a HUGE discount.</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to calculate property taxes on the home of your dreams!&nbsp;</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://christineleeteam.com/buyers_taxcalc.php)</em><br /> <br /> P.S. Who do you know that&rsquo;s thinking of buying or selling in today&rsquo;s market.</p>',NULL),(42,2,0,'Day 350: Quick Question…','Quick Question…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br />I hope this email finds you doing well! I was just checking in to say \"hello.\" I hope your moving plans are coming along well. Any changes or updates you can share with me?<br /> <br /> I\'ll keep the Market Updates coming as usual:)&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to start searching for your dream home!</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(</em><a href=\"http://christineleeteam.com/\"><em>http://christineleeteam.com/</em></a><em>)</em></p><p style=\"margin: 0px; font-family: Arial;\"><br /> Thanks!</p>',NULL),(43,2,0,'Day 360: Home Updates…Time Flies ','Home Updates…Time Flies ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br />Guess what?... I can\'t believe it, but it\'s been a whole year already since I started sending you updates on homes on the market. Time really flies! Take your time... I\'m here to help you whenever you\'re ready. :) <br /> <br /> I was just making sure that you\'re happy with the updates I\'ve been sending you. I can fine tune my search some more if you\'d like&hellip; any new thoughts, plans, changes? <br /> <br /> Just reply and keep me posted! The house-hunting continues! :)&nbsp;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Look forward to hearing from you!</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong>P.S.</strong> </span><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://www.beonhousehunters.com/)</em></p>',NULL),(44,2,0,'Day 375: Fixing up a home…','Fixing up a home…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br />There&rsquo;s a great way for you to buy and fix up a home&hellip;with no out-of pocket expense!</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Just let me know if you are interested in the details</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\"><span style=\"text-decoration: underline;\"><strong>P.S.</strong></span><span style=\"text-decoration: underline; color: #ff0000;\"><strong> CLICK HERE to view ALL my favorite contractors!</strong></span><br /> <em>(http://christineleeteam.com/christinesfavorites.php)</em></p><p style=\"margin: 0px; font-family: Arial;\"><br />Let the house-hunting continue!</p>',NULL),(45,2,0,'Day 390: What are CDD Fees?','What are CDD Fees?','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br />I have some great info on CDD fees&hellip; what they truly are, who has to pay them, if it should influence your home selection process, etc.&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><br />CLICK HERE to view my video that explains it ALL</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(</em><a href=\"http://christineleeteam.com/buyers_cdd.php\"><em>http://christineleeteam.com/buyers_cdd.php</em></a><em>)</em></p><p style=\"margin: 0px; font-family: Arial;\"><br />If you\'re interested, just reply and I\'ll send you a clear and simple answer to those questions - - in Layman\'s terms! :)</p>',NULL),(46,2,0,'Day 405: Top Schools & Golf Communities ','Top Schools & Golf Communities ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">I have a list of neighborhoods with the top rated schools in the area, as well as a list of <em>great</em> golf communities.<br /> <br /> Some things to consider as it may improve the re-sale value of your home.<br /> <br /> Just reply with what you&rsquo;re looking for and I&rsquo;ll send a list over to you. Hope you&rsquo;re having a great day!</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE for Top Schools by Community</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://christineleeteam.com/communities_top_schools.php)</em></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!</p><div>&nbsp;</div>',NULL),(47,2,0,'Day 420: Time Flies…Checking In (version Yr 2-5)','Time Flies…Checking In','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Hope everything is going well.. I was just making sure that you\'re happy with the homes I\'ve been sending you. I can fine tune my search some more if you\'d like&hellip; any new areas, prices, or changes in your situation? <br /> <br /> Happy house-hunting! :)</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. <a title=\"Be On House Hunters\" href=\"http://www.beonhousehunters.com/be-on-hgtv-house-hunters\" target=\"_blank\">CLICK HERE.</a></strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> P.S. Can we touch base real quick&hellip; What&rsquo;s the best time and number to reach you?</p>',NULL),(48,2,0,'Day 435: A Home I Saw….','A Home I Saw…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br /> As I looked over the new listings of homes today, I noticed a great home and thought you may want to see more photos. Just reply back and I will send them...&nbsp;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a title=\"Christine Lee Team\" href=\"http://www.christineleeteam.com\" target=\"_blank\">CLICK HERE to start searching for your dream home!</a></strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> <strong>P.S.</strong><em> To schedule a showing, please give me 1-2 days notice... Although I\'m available, </em><strong><em>the seller may require more time.</em></strong><em> (</em><strong><em>If you\'re coming from out of town</em></strong><em>, let me know a week or so prior so I can plan a more informative showing) Thanks in advance!</em></p>',NULL),(49,2,0,'Day 450: Ready for a Great Deal?...','Ready for a Great Deal?...','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br />There&rsquo;s a couple things that you can do now that will give you an advantage by being ready for great deal&hellip;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Can we touch base real quick? What&rsquo;s the best time and number to reach you?<br /> <br /> Just reply and let me know&hellip;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to get Pre-Qualified NOW!</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(</em><a href=\"http://christineleeteam.com/buyers_prequalify.php\"><em>http://christineleeteam.com/buyers_prequalify.php</em></a><em>)</em></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!</p>',NULL),(50,2,0,'Day 465: The Truth about Zillow …','The Truth about Zillow …','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><strong> <br /> </strong>Here&rsquo;s a video on the truth about Zillow that sheds some light on your home value&hellip;<br /> <br /> <em>[images + hyperlink]&nbsp;</em></p><p style=\"margin: 0px; font-family: Arial;\"><em>(</em><a href=\"http://christineleeteam.com/sellers_zillow.php\"><em>http://christineleeteam.com/sellers_zillow.php</em></a><em>)<br /> <br /> </em>Hope this helps!<br /> <br /> P.S. Do you have a home to sell?</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE for a step by step guide to selling your home</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://christineleeteam.com/sellers_stepbystep.php)</em></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Warmly,</p>',NULL),(51,2,0,'Day 480: Ready to see homes?','Ready to see homes?','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><strong><br /> </strong>I was just going over my schedule today... <br /> <br /> Would you like to look at any homes in the next few weeks? <br /> <br /> Just reply to this message and let me know.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"color: #000000;\"><strong>P.S.</strong> </span><a title=\"Be on House Hunters\" href=\"http://www.beonhousehunters.com/\" target=\"_blank\"><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></a></p>',NULL),(52,2,0,'Day 495: Timing the Market…','Timing the Market…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br /> I have some great information that has helped my other clients benefit from today&rsquo;s real estate market.&nbsp; I think it could help you as well&hellip;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Just reply and let me know if you&rsquo;re interested.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE for some tips on buying &amp; selling.</strong></span><em> (</em><a href=\"http://christineleeteam.com/buyers_buywhileselling.php\"><em>http://christineleeteam.com/buyers_buywhileselling.php</em></a><em>)</em></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Have a great week!</p>',NULL),(53,2,0,'Day 510: Financing Oops… ?','Financing Oops… ?','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> </p><p style=\"margin: 0px; font-family: Arial;\">Usually, a little &ldquo;Oops&rdquo; in financing means thousands of dollars to you&hellip; Avoid the pitfalls of bad rates or advice that can cost you a lot of money and aggravation.<br /> <br /> Here&rsquo;s a quick video for you&hellip; Click here to <a href=\"http://www.christineleeteam.com/myths-about-loans-and-lending\" target=\"_blank\"><span style=\"color: #3a00ff;\">watch the video</span></a><br /> <br /> Would it be ok if I connect you with my <em>best</em> lender? Just reply and let me know.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to contact my BEST lender!</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://christineleeteam.com/christinesfavorites.php#lender)</em></p>',NULL),(54,2,0,'Day 525: Relocation Package ','Relocation Package ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> </p><p style=\"margin: 0px; font-family: Arial;\">There&rsquo;s a &ldquo;Complete Relocation Package&rdquo; for home buyers moving to the Jacksonville area&hellip;it includes a lot of useful info on neighborhoods, restaurants, schools, taxes, insurance, important contacts such as utilities, hospitals, etc.<br /> <br /> Just reply and let me know if you&rsquo;re interested and I&rsquo;ll see if I can get one out to you&hellip;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a href=\"http://www.christineleeteam.com/relocation-guide\" target=\"_blank\">CLICK HERE to get your relocation guide now!</a></strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> P.S. Do you know anyone who needs to buy or sell?</p>',NULL),(55,2,0,' Day 540: Quick review…','Quick review…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br /> I was just making sure that I&rsquo;m sending you homes that fit what you&rsquo;re looking for? Feel free to update me on your latest wants and needs.<br /> <br /> I&rsquo;d like to touch base with you. What&rsquo;s the best time and number to reach you? <br /> <br /> Happy house-hunting! :)</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><a href=\"http://www.christineleeteam.com/admin/emailTemplates/Hi%20{{first_name}},??Day%20540:%20Quick%20review&hellip;??I%20was%20just%20making%20sure%20that%20I&rsquo;m%20sending%20you%20homes%20that%20fit%20what%20you&rsquo;re%20looking%20for?%20Feel%20free%20to%20update%20me%20on%20your%20latest%20wants%20and%20needs.??I&rsquo;d%20like%20to%20touch%20base%20with%20you.%20What&rsquo;s%20the%20best%20time%20and%20number%20to%20reach%20you?%20??Happy%20house-hunting!%20:)%20%20Find%20out%20how%20you%20can%20be%20on%20HGTV%20House%20Hunters.%20CLICK%20HERE.%20(http:/www.beonhousehunters.com/)%20??P.S.%20I%20need%20your%20help%20to%20improve&hellip;%20can%20you%20take%20a%20second%20to%20answer%20a%20few%20quick%20questions%20for%20me???1)%20How%20would%20you%20rate%20my%20service%20overall?%20Rate%20on%20a%20scale%20of%201(bad)%20&ndash;%2010(excellent).??2)%20What%20did%20you%20like%20best?%20(ex.%20Website,%20email,%20information,%20communication,%20etc.)??3)%20How%20can%20I%20improve?%20(ex.%20Website,%20email,%20information,%20communication,%20etc.)??4)%20Can%20you%20introduce%20me%20to%20just%201%20person%20you%20know%20that%20may%20be%20thinking%20of%20buying%20or%20selling???This%20will%20help%20more%20than%20you&rsquo;ll%20know&hellip;Thank%20you%20for%20your%20time!:)\" target=\"_blank\"><span style=\"text-decoration: underline;\"><strong>Find out how you can be on HGTV House Hunters. CLICK HERE.</strong></span></a></p><p style=\"margin: 0px; font-family: Arial;\"><br /> <br /> P.S. I need your help to improve&hellip; can you take a second to answer a few quick questions for me?<br /> <br /> 1) How would you rate my service overall? Rate on a scale of 1(bad) &ndash; 10(excellent).<br /> <br /> 2) What did you like best? (ex. Website, email, information, communication, etc.)<br /> <br /> 3) How can I improve? (ex. Website, email, information, communication, etc.)<br /> <br /> 4) Can you introduce me to just 1 person you know that <em>may</em> be thinking of buying or selling?<br /> <br /> This will help more than you&rsquo;ll know&hellip;Thank you for your time!:)</p>',NULL),(56,2,0,' Day 555: Top Schools & Golf Communities','Top Schools & Golf Communities ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><br /> I have a list of neighborhoods with the top rated schools in the area, as well as a list of <em>great</em> golf communities.<br /> <br /> Some things to consider as it may improve the re-sale value of your home.<br /> <br /> Just reply with what you&rsquo;re looking for and I&rsquo;ll send a list over to you. Hope you&rsquo;re having a great day!</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE for Golf Communities</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(</em><a href=\"http://christineleeteam.com/communities_golf_homes.php\"><em>http://christineleeteam.com/communities_golf_homes.php</em></a><em>)</em></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!</p>',NULL),(57,2,0,' Day 570: New Construction Pitfalls ','New Construction Pitfalls ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Building a new home can be exciting &ndash; and exhausting! Let me know if this is something you are considering&hellip;</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\"><em>[Video link] (http://christineleeteam.com/buyers_newhomemyth.php)</em><br /> <br /> P.S. Do you know of anyone else who is thinking about buying or building a home?</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a href=\"http://www.christineleeteam.com/search/full\" target=\"_blank\">CLICK HERE to find your perfect home wih a power search!</a></strong></span></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Have a great week!</p>',NULL),(58,2,0,'Day 585: Staging Your Home to Sell','Staging Your Home to Sell','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><strong> <br /> </strong>Staging a home properly helps you get top dollar for your home and it doesn&rsquo;t require a lot of money, just a little time and effort! :)<br /> <br /> Here&rsquo;s a quick video on Home Staging <em>[video]</em></p><p style=\"margin: 0px; font-family: Arial;\"><em>(</em><a href=\"http://christineleeteam.com/sellers_staging.php\"><em>http://christineleeteam.com/sellers_staging.php</em></a>)<br /> <br /> P.S. Who do you know that\'s thinking of buying or selling in today\'s market? <br /> </p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\"><strong><a href=\"http://www.christineleeteam.com/mortgagecalculator\" target=\"_blank\">CLICK HERE for a free mortgage calculator!</a></strong></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!<strong><br /></strong></p>',NULL),(59,2,0,'Day 600: Checking In…Market Update','Checking In…Market Update','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /><br /> I hope this email finds you doing well! I was just checking in to say \"hello\" and see what was new with your situation. <br /> <br /> Have you seen any new homes or neighborhoods that you like lately?</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a href=\"http://christineleeteam.com/\" target=\"_blank\">CLICK HERE to start searching for your dream home!</a></strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> I\'ll keep the Market Updates coming:)</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!</p>',NULL),(60,2,0,' Day 615: Homeowner’s Insurance','Homeowner’s Insurance','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<strong><br /></strong><strong> <br /> </strong>I have some great info on homeowners &amp; flood insurance.&nbsp; It gives you an estimate of what to expect on costs for the range of home you&rsquo;re looking for.<br /> <br /> Just reply and let me know if you&rsquo;re interested.</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE for more info on Homeowner&rsquo;s Insurance</strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><em>(http://christineleeteam.com/buyers_insurance.php)</em></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Happy house-hunting!</p>',NULL),(61,2,0,'Day 630: Foreclosure vs. Short Sale','Foreclosure vs. Short Sale?... ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br /> Here&rsquo;s some info on the difference between a foreclosure vs. a short sale.<br /> <br /> <span style=\"text-decoration: underline;\">Short sales</span> take between 3-6 months to negotiate and close. Seller&rsquo;s usually live in the home during this period and property conditions <span style=\"text-decoration: underline;\">VARY!</span> &ndash; from pristine to disaster zone.<br /> <br /> <span style=\"text-decoration: underline;\">Foreclosures</span> have a much quicker response &ndash; around 2 weeks and can close in 30-45 days. These homes are usually vacant and the condition ranges from needing major repairs to being move-in ready.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a href=\"http://www.christineleeteam.com/stop-foreclosures-with-a-short-sale\" target=\"_blank\">CLICK HERE to stop foreclosure!&nbsp;</a></strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> If you&rsquo;re interested in a great deal, let me know &hellip; what the best time and number to reach you?</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!</p>',NULL),(62,2,0,'Day 645: My top pros & contractors…','My top pros & contractors…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br />I&rsquo;ve put together a list of my top professional services &amp; contractors in the area&hellip; everyone from handyman to home lenders. <span style=\"text-decoration: underline;\">Click here</span> if you could use a few recommendations.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\"><span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE to see ALL my favorite contractors!</strong></span><br /> <em>(http://christineleeteam.com/christinesfavorites.php)</em><br /> <br /> P.S. Do you know of anyone else who might be looking to buy or sell a home in the area? Just let me know.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Warmly,</p>',NULL),(63,2,0,'Day 660: Ready to see homes?','Ready to see homes?','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <strong> <br /> </strong>I was just going over my schedule today... <br /> <br /> Would you like to look at any homes in the next few weeks?&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a href=\"http://christineleeteam.com/\" target=\"_blank\">CLICK HERE to start searching for your dream home!</a></strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br />Just reply to this message and let me know.&nbsp;</p>',NULL),(64,2,0,'Day 675: Knowing House Values','Knowing House Values','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br />Here is a great website that will give you a report of what your home is currently worth in today&rsquo;s market.<br /> <br /> Visit&nbsp; <a href=\"http://www.jaxvalues.com/houseValues\" target=\"_blank\"><span style=\"color: #3a00ff;\">www.JaxValues.com</span></a>. Hope this helps!<br /> <br /> P.S. Can you introduce me to 1 person you know that could use some expert advice in today&rsquo;s market to sell or buy a home?</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a href=\"http://christineleeteam.com/values\" target=\"_blank\">OR CLICK HERE to request your home value NOW!</a></strong></span></p>',NULL),(65,2,0,'Day 690: Lending Myths','Lending Myths','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},<br /> <br />Inaccurate information has cost buyers a lot of money, time and aggravation --I have some great info on Lending Mythbusters to help you avoid the hassle!<br /> <br /> Here&rsquo;s a quick video for you&hellip; Click here to <a href=\"http://christineleeteam.com/myths-about-loans-and-lending\" target=\"_blank\"><span style=\"color: #3a00ff;\">watch the video</span></a><br /> <br /> Just reply and let me know what you think...<br /> <br /> P.S. Would it be ok to connect you with my best lender? Just reply and let me know or <span style=\"text-decoration: underline; color: #ff0000;\"><strong>CLICK HERE to view my favorite contractors.</strong></span></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Warmly,</p>',NULL),(66,2,0,' Day 705: This could be you…','This could be you…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br /> This could be you!<br /> <br /> <a href=\"http://christineleeteam.com/homes-that-sell-vs.-homes-that-sit\" target=\"_blank\"><em>Click here to see&hellip;.!</em></a></p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Hope you&rsquo;re having a great day!</p>',NULL),(67,2,0,' Day 720: Foreclosure vs. Short Sale ','Foreclosure vs. Short Sale ','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Foreclosure vs. Short Sale<br /> <br /> Here&rsquo;s some info on the difference between a foreclosure vs. a short sale.<br /> <br /> <span style=\"text-decoration: underline;\">Short sales</span> take between 3-6 months to negotiate and close. Seller&rsquo;s usually live in the home during this period and property conditions <span style=\"text-decoration: underline;\">VARY!</span> &ndash; from</p><p style=\"margin: 0px; font-family: Arial;\">pristine to disaster zone.<br /> <br /> With foreclosures turnaround time is typically much quicker &ndash; around 2 weeks and can close in 30-45 days. These homes are usually vacant and the condition ranges from needing major repairs to being move-in ready.</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><span style=\"text-decoration: underline;\"><strong><a href=\"http://christineleeteam.com/stop-foreclosures-with-a-short-sale\" target=\"_blank\">CLICK HERE to stop foreclosure!&nbsp;</a></strong></span></p><p style=\"margin: 0px; font-family: Arial;\"><br /> If you&rsquo;re interested in a great deal, let me know &hellip; what the best time and number to reach you?</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial;\">Thanks!</p>',NULL),(68,2,0,'735: Quick review…','Quick review…','<p style=\"margin: 0px; font-family: Arial;\">Hi {{first_name}},</p><p style=\"margin: 0px; font-family: Arial;\"><br /> I was just making sure that I&rsquo;m sending you homes that fit what you&rsquo;re looking for? Feel free to update me on your latest wants and needs.<br /> <br /> I&rsquo;d like to touch base with you. What&rsquo;s the best time and number to reach you? <br /> <br /> Happy house-hunting! :)<br /> <br /> P.S. I need your help to improve&hellip; can you take a second to answer a few quick questions for me?<br /> <br /> 1) How would you rate my service overall? Rate on a scale of 1(bad) &ndash; 10(excellent).<br /> <br /> 2) What did you like best? (ex. Website, email, information, communication, etc.)<br /> <br /> 3) How can I improve? (ex. Website, email, information, communication, etc.)<br /> <br /> 4) Can you introduce me to just 1 person you know that <em>may</em> be thinking of buying or selling?<br /> <br /> This will help more than you&rsquo;ll know&hellip;Thank you for your time!:)</p><p style=\"margin: 0px; font-family: Arial; min-height: 14px;\">&nbsp;</p><p style=\"margin: 0px; font-family: Arial; color: #ff0000;\"><a href=\"http://christineleeteam.com/\" target=\"_blank\"><span style=\"text-decoration: underline;\"><strong>CLICK HERE to start searching for your dream home!</strong></span></a></p><p style=\"margin: 0px; font-family: Arial;\">&nbsp;</p>',NULL),(69,2,1,'Z-BUYER','Noticed you on Z-Buyer','Hello ,&nbsp;<br /><br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; I saw you on Z-Buyer.com and noticed you were looking to buy a home. What area did you want to live in?<br /><br />',NULL),(70,2,1,'Serg-kwa','Keller Williams App-Sergio','<p><span style=\"font-family: arial; font-size: small;\">Hello ,&nbsp;</span></p><p><span style=\"font-family: arial; font-size: small;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;For the Keller Williams Realty app just go to your google play store on your phone or the app store on your i-phone and type into your search bar Keller Williams Realty and this logo will appear in the red box and white letters of kw.&nbsp;</span><img class=\"cover-image\" style=\"color: #333333; font-family: Roboto, Arial, sans-serif; font-size: 13px; line-height: 18px; vertical-align: middle; margin: 0px; padding: 0px; -webkit-box-shadow: none; box-shadow: none; width: 200px;\" src=\"https://lh6.ggpht.com/dgyRuBtTFL9uFqvigugwevkm48FqkabZqZS-NSLknQ_wQeR1WwWFAmP6zxaqTWEFrJs=w300-rw\" alt=\"Cover art\" /></p><p>After you download it and it will ask for a agent code and here is mine:KWA9WSUY</p><p>Then you will be able to browse on your own and if you have a question hit the contact button at the bottom and you can call me or email me for information. Happy hunting and let me know if you have any questions on how to use it.<br /><br />This is how it will look if you got the correct app!!!!<br /><br /><img class=\"screenshot clickable\" style=\"vertical-align: top; margin: 0px 10px 40px 0px; padding: 0px; cursor: pointer; -webkit-box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; display: inline-block; height: 285px; color: #333333; font-family: Roboto, Arial, sans-serif; font-size: 13px; line-height: 18px; white-space: nowrap; background-color: #f5f5f5;\" src=\"https://lh5.ggpht.com/tg-tMTrtsoZ_UXVqIQNvH0ANMweVNresj--WnPcBag4Ad1WS23dkD7ItiGcCXrnyiTY=h310-rw\" alt=\"Keller Williams Realty Search - screenshot thumbnail\" /><img class=\"screenshot clickable\" style=\"vertical-align: top; margin: 0px 10px 40px 0px; padding: 0px; cursor: pointer; -webkit-box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; display: inline-block; height: 285px; color: #333333; font-family: Roboto, Arial, sans-serif; font-size: 13px; line-height: 18px; white-space: nowrap; background-color: #f5f5f5;\" src=\"https://lh4.ggpht.com/rXnUQffA4XJGt-r5UzWL3t-VB_BsJEay3eiBufrg92u8yJw7Ih13CxKaJCCnaW2QRfE=h310-rw\" alt=\"Keller Williams Realty Search - screenshot thumbnail\" /><img class=\"screenshot clickable\" style=\"vertical-align: top; margin: 0px 10px 40px 0px; padding: 0px; cursor: pointer; -webkit-box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; display: inline-block; height: 285px; color: #333333; font-family: Roboto, Arial, sans-serif; font-size: 13px; line-height: 18px; white-space: nowrap; background-color: #f5f5f5;\" src=\"https://lh6.ggpht.com/95ZgMYMd133mqPmg3uhyQM4djLUWLe7yME0JPhlx5QOUgBTfAfTWMCed3DUSPRqSWLWx=h310-rw\" alt=\"Keller Williams Realty Search - screenshot thumbnail\" /><img class=\"screenshot clickable\" style=\"vertical-align: top; margin: 0px 10px 40px 0px; padding: 0px; cursor: pointer; -webkit-box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; box-shadow: rgba(0, 0, 0, 0.0980392) 0px 0px 4px; display: inline-block; height: 285px; color: #333333; font-family: Roboto, Arial, sans-serif; font-size: 13px; line-height: 18px; white-space: nowrap; background-color: #f5f5f5;\" src=\"https://lh5.ggpht.com/wiuajaxY0SQTXhB-GaJ0dVEI_x_nuYMIWxiIcekxhbhs4npZ5HI7rPg-jO1U4-8o4Gra=h310-rw\" alt=\"Keller Williams Realty Search - screenshot thumbnail\" /></p><div><br /><p style=\"color: #500050; font-family: arial, sans-serif; font-size: 13px;\"><span style=\"font-size: xx-large; font-family: Georgia, serif;\">&nbsp;</span></p></div>',NULL),(71,3,1,'2014 home value report','2014 home value report for your Jacksonville area property','<p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">Dear Friends, Clients, and Colleagues,</span></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">I&rsquo;m writing you this important email just as our stock market has reached new heights, and you may have seen the MetroStudy report headlines about housing returning to &lsquo;normal&rsquo; levels.</span></p><hr /><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><img src=\"https://lh6.googleusercontent.com/BLIi9jEEFCJx8ZJkg4So4VNwdx2k_djVQbHTHm7YoYtumg5NxH_bADsRijeIDD0F88AbC_XPRYTXkWTuUjdP_BBc6R1yTneL27OjPYEP8vxaeW4YH4lALPwXfw\" alt=\"\" width=\"720px;\" height=\"432px;\" /></strong></strong><hr /><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">As I&rsquo;m sure you are aware, our Jacksonville area</span><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">real estate market has seen some considerable changes over the past year.</span><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">If you&rsquo;re thinking about making renovations to your home, or possibly selling it in 2014, the first thing you&rsquo;ll want to know is the current market value. You&rsquo;re invited to use a free home value report calculator on our website that cross-references your home to comparable sales in your neighborhood to estimate a price your home would sell for it were to go on the MLS today.</span></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">Here&rsquo;s the link to use the free home value report tool </span><a style=\"text-decoration: none;\" href=\"http://www.christineleeteam.com/values\"><span style=\"font-size: 16px; font-family: Arial; color: #1155cc; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: underline; vertical-align: baseline; white-space: pre-wrap;\">now:</span></a></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><a style=\"text-decoration: none;\" href=\"http://www.christineleeteam.com/values\"><span style=\"font-size: 24px; font-family: Arial; color: #1155cc; background-color: #ffffff; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: underline; vertical-align: baseline; white-space: pre-wrap;\">Jacksonville House Value Calculator</span></a></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">Enter your home address and you&rsquo;ll get a suggested selling price specific to our market.</span></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">If you have any questions about it, or if you do actually want to sell your home next year, just reply back to this email as I know exactly what homes buyers are looking for (and what they are paying for) in this area.</span></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">Enjoy </span><a style=\"text-decoration: none;\" href=\"http://www.christineleeteam.com/values\"><span style=\"font-size: 16px; font-family: Arial; color: #1155cc; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: underline; vertical-align: baseline; white-space: pre-wrap;\">the calculator</span></a><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\"> &ndash; you may be surprised what your home is really worth!</span></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">The Christine Lee team sells 3 homes per week - make yours the next.</span></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Arial; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">Christine Lee</span></p><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><a style=\"text-decoration: none;\" href=\"http://www.christineleeteam.com/\"><span style=\"font-size: 16px; font-family: Arial; color: #1155cc; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: underline; vertical-align: baseline; white-space: pre-wrap;\">http://www.christineleeteam.com/</span></a></p><strong style=\"font-weight: normal;\"><strong style=\"font-weight: normal;\"><br /></strong></strong><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 15px; font-family: Arial; color: #333333; background-color: #ffffff; font-weight: bold; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">904-280-3000</span></p><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Verdana; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">Hear Christine every Saturday at 10am on </span><span style=\"font-size: 16px; font-family: Verdana; color: #222222; background-color: #ffffff; font-weight: normal; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">\"The Plain English Real Estate Show\"</span><img src=\"https://lh6.googleusercontent.com/s9RYm1EBttyBa-fVAFW9P_tpLK2imDdR6cY-p1-qw9SnIrdTtcUyvsMCUCGIWNla-wVE_wWusP0Ams2U-ysecsq_kFZyx42iuBkVx-CGwgHIoEdilJsiiTAeBA\" alt=\"\" width=\"102px;\" height=\"138px;\" /></p><p style=\"line-height: 1.15; margin-top: 0pt; margin-bottom: 0pt;\" dir=\"ltr\"><span style=\"font-size: 16px; font-family: Verdana; color: #222222; background-color: #ffffff; font-weight: normal; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;\">Fox, Sat 10-11am</span></p>',NULL);



/*Table structure for table `emails` */



DROP TABLE IF EXISTS `emails`;



CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `email_status_id` int(11) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  `email_type_ma` tinyint(1) DEFAULT NULL,
  `owner_ma` tinyint(1) DEFAULT '0',
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_account_id` (`email`,`account_id`),
  KEY `email_opt_status_id` (`email_status_id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`),
  KEY `email` (`email`) USING BTREE,
  KEY `is_primary` (`is_primary`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  CONSTRAINT `emails_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `emails_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `emails_ibfk_4` FOREIGN KEY (`email_status_id`) REFERENCES `email_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;



/*Data for the table `emails` */

insert  into `emails`(`id`,`account_id`,`contact_id`,`email_status_id`,`email`) values (1,1,1,1,'qa@seizethemarket.com');

/*Table structure for table `featured_area_type_lu` */



DROP TABLE IF EXISTS `featured_area_type_lu`;



CREATE TABLE `featured_area_type_lu` (
  `featured_area_type_id` int(11) NOT NULL DEFAULT '0',
  `featured_area_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`featured_area_type_id`,`featured_area_id`),
  KEY `featured_area_id` (`featured_area_id`),
  KEY `featured_area_type_id` (`featured_area_type_id`),
  CONSTRAINT `featured_area_type_lu_ibfk_1` FOREIGN KEY (`featured_area_id`) REFERENCES `featured_areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `featured_area_type_lu_ibfk_2` FOREIGN KEY (`featured_area_type_id`) REFERENCES `featured_area_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `featured_area_type_lu` */



/*Table structure for table `featured_area_types` */



DROP TABLE IF EXISTS `featured_area_types`;



CREATE TABLE `featured_area_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `featured_area_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `featured_area_types` */



/*Table structure for table `featured_areas` */



DROP TABLE IF EXISTS `featured_areas`;



CREATE TABLE `featured_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `name` varchar(63) DEFAULT NULL,
  `title` varchar(63) DEFAULT NULL,
  `description` text,
  `url` varchar(63) DEFAULT NULL,
  `count_homes` int(11) DEFAULT NULL,
  `count_photo` int(11) DEFAULT NULL,
  `min_price` int(11) DEFAULT NULL,
  `max_price` int(11) DEFAULT NULL,
  `avg_price` int(11) DEFAULT NULL,
  `avg_bedrooms` decimal(3,1) DEFAULT NULL,
  `avg_baths` decimal(3,1) DEFAULT NULL,
  `avg_sf` int(11) DEFAULT NULL,
  `avg_price_sf` int(11) DEFAULT NULL,
  `avg_br1` int(11) DEFAULT NULL,
  `avg_br2` int(11) DEFAULT NULL,
  `avg_br3` int(11) DEFAULT NULL,
  `avg_br4` int(11) DEFAULT NULL,
  `avg_br5` int(11) DEFAULT NULL,
  `avg_dom` int(11) DEFAULT NULL,
  `query` text,
  `stats_last_updated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `status_ma` (`status_ma`) USING BTREE,
  KEY `url` (`url`) USING BTREE,
  CONSTRAINT `featured_areas_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `featured_areas` */



insert  into `featured_areas`(`id`,`account_id`,`status_ma`,`name`,`title`,`description`,`url`,`count_homes`,`count_photo`,`min_price`,`max_price`,`avg_price`,`avg_bedrooms`,`avg_baths`,`avg_sf`,`avg_price_sf`,`avg_br1`,`avg_br2`,`avg_br3`,`avg_br4`,`avg_br5`,`avg_dom`,`query`,`updated`,`is_default`,`is_deleted`) values (1,1,1,'Albany',NULL,NULL,'albany',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(2,1,1,'Albion',NULL,NULL,'albion',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(3,1,1,'Belleville',NULL,NULL,'belleville',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(4,1,1,'Brooklyn',NULL,NULL,'brooklyn',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(5,1,1,'Busseyville',NULL,NULL,'busseyville',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(6,1,1,'Cambridge',NULL,NULL,'cambridge',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(7,1,1,'Cooksville',NULL,NULL,'cooksville',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(8,1,1,'Dayton',NULL,NULL,'dayton',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(9,1,1,'Edgerton',NULL,NULL,'edgerton',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(10,1,1,'Evansville',NULL,NULL,'evansville',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(11,1,1,'Fitchburg',NULL,NULL,'fitchburg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(12,1,1,'Footville',NULL,NULL,'footville',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(13,1,1,'Fulton',NULL,NULL,'fulton',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(14,1,1,'Indianford',NULL,NULL,'indianford',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(15,1,1,'Lake Kegonsa',NULL,NULL,'lakekegonsa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(16,1,1,'Lake Koshkonong',NULL,NULL,'lakekoshkonong',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(17,1,1,'Madison',NULL,NULL,'madison',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(18,1,1,'Magnolia',NULL,NULL,'magnolia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(19,1,1,'Mcfarland',NULL,NULL,'mcfarland',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(20,1,1,'Monona',NULL,NULL,'monona',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(21,1,1,'Newville',NULL,NULL,'newville',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(22,1,1,'Oregon',NULL,NULL,'oregon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(23,1,1,'Rockdale',NULL,NULL,'rockdale',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(24,1,1,'Stoughton',NULL,NULL,'stoughton',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(25,1,1,'Union',NULL,NULL,'union',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(26,1,1,'Utica',NULL,NULL,'utica',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(27,1,1,'Verona',NULL,NULL,'verona',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(28,1,1,'Sun Prairie',NULL,NULL,'sunprairie',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(29,1,1,'Cottage Grove',NULL,NULL,'cottagegrove',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(30,1,1,'Marshall',NULL,NULL,'marshall',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(31,1,1,'Waterloo',NULL,NULL,'waterloo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(32,1,1,'Columbus',NULL,NULL,'columbus',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(33,1,1,'Deerfield',NULL,NULL,'deerfield',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(34,1,1,'Arlington',NULL,NULL,'arlington',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(35,1,1,'Poynette',NULL,NULL,'poynette',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(36,1,1,'Deforest',NULL,NULL,'deforest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(37,1,1,'Dane',NULL,NULL,'dane',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(38,1,1,'Windsor',NULL,NULL,'windsor',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(39,1,1,'Waunakee',NULL,NULL,'waunakee',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(40,1,1,'Middleton',NULL,NULL,'middleton',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),(41,1,1,'All',NULL,NULL,'all',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);



/*Table structure for table `form_account_lu` */



DROP TABLE IF EXISTS `form_account_lu`;



CREATE TABLE `form_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`) USING BTREE,
  CONSTRAINT `form_account_lu_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `form_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `form_account_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `form_account_lu` */



insert  into `form_account_lu`(`id`,`account_id`,`form_id`,`component_type_id`,`is_deleted`) values (1,1,1,2,0),(2,1,2,2,0),(3,1,3,2,0),(4,1,4,2,0),(5,1,5,2,0),(6,1,10,2,0),(7,1,7,2,0),(8,1,6,3,0),(9,1,11,2,0),(10,1,12,2,0),(11,1,13,3,0),(12,1,14,3,0),(13,1,15,3,0),(14,1,16,3,0),(15,1,17,3,0),(16,1,18,2,0),(17, '2', '19', '2', '0');



/*Table structure for table `form_field_lu` */



DROP TABLE IF EXISTS `form_field_lu`;



CREATE TABLE `form_field_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_account_lu_id` int(11) DEFAULT NULL,
  `form_field_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form_field_id` (`form_field_id`),
  KEY `form_account_lu_id` (`form_account_lu_id`),
  CONSTRAINT `form_field_lu_ibfk_1` FOREIGN KEY (`form_field_id`) REFERENCES `form_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `form_field_lu_ibfk_2` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `form_field_lu` */



insert  into `form_field_lu`(`id`,`form_account_lu_id`,`form_field_id`,`is_deleted`) values (1,1,1,0),(2,1,2,0),(3,1,3,0),(4,1,4,0),(5,1,5,0),(6,1,6,0),(7,1,7,0),(8,2,1,0),(9,2,2,0),(10,2,3,0),(11,2,4,0),(12,2,5,0),(13,2,6,0),(14,2,7,0),(15,3,1,0),(16,3,2,0),(17,3,3,0),(18,3,9,0),(19,3,10,0),(20,3,11,0),(21,3,7,0),(22,4,1,0),(23,4,2,0),(24,4,3,0),(25,4,4,0),(26,4,8,0),(27,5,1,0),(28,5,2,0),(29,5,3,0),(30,5,4,0),(31,5,7,0),(32,6,1,0),(33,6,2,0),(34,6,3,0),(35,6,4,0),(38,7,1,0),(39,7,2,0),(40,7,3,0),(41,7,4,0),(42,7,13,0),(43,8,14,0),(44,8,15,0),(45,8,16,0),(46,8,17,0),(47,8,18,0),(48,8,3,0),(49,8,1,0),(50,8,2,0),(51,8,4,0),(52,9,1,0),(53,9,2,0),(54,9,3,0),(55,9,4,0),(56,9,7,0),(57,9,12,0),(59,9,20,0),(60,10,1,0),(61,10,2,0),(62,10,3,0),(63,10,4,0),(64,11,1,0),(65,11,2,0),(66,11,3,0),(67,11,4,0),(68,11,14,0),(69,11,16,0),(70,11,17,0),(71,11,18,0),(72,12,1,0),(73,12,2,0),(74,12,3,0),(75,12,4,0),(76,12,14,0),(77,12,16,0),(78,12,17,0),(79,12,18,0),(80,13,1,0),(81,13,2,0),(82,13,3,0),(83,13,4,0),(84,13,14,0),(85,13,16,0),(86,13,17,0),(87,13,18,0),(88,14,1,0),(89,14,2,0),(90,14,3,0),(91,14,4,0),(92,14,14,0),(93,14,16,0),(94,14,17,0),(95,14,18,0),(96,15,1,0),(97,15,2,0),(98,15,3,0),(99,15,4,0),(100,15,14,0),(101,15,16,0),(102,15,17,0),(103,15,18,0),(104,3,4,0),(105, '16', '1', '0'),(106, '16', '2', '0'),(107, '16', '3', '0'),(108, '16', '4', '0'),(109, '16', '5', '0'),(110, '16', '6', '0'),(111, '17', '1', '0'),(112, '17', '2', '0'),(113, '17', '3', '0'),(114, '17', '4', '0'),(115, '17', '7', '0');



/*Table structure for table `form_fields` */



DROP TABLE IF EXISTS `form_fields`;



CREATE TABLE `form_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `label` varchar(31) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB;



/*Data for the table `form_fields` */



insert  into `form_fields`(`id`,`name`,`label`,`is_deleted`) values (1,'first_name','Your First Name',0),(2,'last_name','Your Last Name',0),(3,'email','Email',0),(4,'phone','Phone',0),(5,'contact_date','Contact Date',0),(6,'contact_time','Contact Time',0),(7,'comments','Comments',0),(8,'question','Question',0),(9,'to_first_name','To First Name',0),(10,'to_last_name','To Last Name',0),(11,'to_email','To Email',0),(12,'form_title','Form Title',0),(13,'password','Pick a Password',0),(14,'address','Address',0),(15,'unit_num','Unit #',0),(16,'city','City',0),(17,'state','State',0),(18,'zip','Zip',0),(20,'reason','Reason',0),(21,'listing_id','Listing ID',0),(22,'cms_content_id','CMS ID',0),(23,'price', 'Price',0),(24,'source_id', NULL,0),(25,'source_description', NULL,0), (26, 'bedrooms', 'Beds', '0'), (27, 'baths', 'Baths', '0'), (28, 'sq_feet', 'Sq. Feet', '0'), (29, 'condition', 'Condition', '0');



/*Table structure for table `form_submission_raw_data` */



DROP TABLE IF EXISTS `form_submission_raw_data`;



CREATE TABLE `form_submission_raw_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_spam` tinyint(1) NOT NULL DEFAULT '0',
  `first_name__1` varchar(255) DEFAULT NULL COMMENT '1',
  `last_name__2` varchar(255) DEFAULT NULL COMMENT '2',
  `email__3` varchar(255) DEFAULT NULL COMMENT '3',
  `phone__4` varchar(255) DEFAULT NULL COMMENT '4',
  `contact_date__5` varchar(255) DEFAULT NULL COMMENT '5',
  `contact_time__6` varchar(255) DEFAULT NULL COMMENT '6',
  `comments__7` varchar(255) DEFAULT NULL COMMENT '7',
  `question__8` varchar(255) DEFAULT NULL COMMENT '8',
  `to_first_name__9` varchar(255) DEFAULT NULL COMMENT '9',
  `to_last_name__10` varchar(255) DEFAULT NULL COMMENT '10',
  `to_email__11` varchar(255) DEFAULT NULL COMMENT '11',
  `form_title__12` varchar(255) DEFAULT NULL COMMENT '12',
  `password__13` varchar(255) DEFAULT NULL COMMENT '13',
  `address__14` varchar(255) DEFAULT NULL COMMENT '14',
  `unit_num__15` varchar(255) DEFAULT NULL COMMENT '15',
  `city__16` varchar(255) DEFAULT NULL COMMENT '16',
  `state__17` varchar(255) DEFAULT NULL COMMENT '17',
  `zip__18` varchar(255) DEFAULT NULL COMMENT '18',
  `reason__20` varchar(255) DEFAULT NULL COMMENT '20',
  `misc` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `ip` varchar(35) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `form_submission_raw_data` */



/*Table structure for table `form_submission_values` */



DROP TABLE IF EXISTS `form_submission_values`;



CREATE TABLE `form_submission_values` (
  `form_account_lu_id` int(11) DEFAULT NULL,
  `form_submission_id` int(11) DEFAULT NULL,
  `form_field_id` int(11) DEFAULT NULL,
  `value` varchar(500) DEFAULT NULL,
  KEY `form_submission_id` (`form_submission_id`),
  KEY `form_account_lu_id` (`form_account_lu_id`),
  KEY `form_field_id` (`form_field_id`),
  CONSTRAINT `form_submission_values_ibfk_1` FOREIGN KEY (`form_submission_id`) REFERENCES `form_submissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `form_submission_values_ibfk_2` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `form_submission_values_ibfk_3` FOREIGN KEY (`form_field_id`) REFERENCES `form_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `form_submission_values` */



/*Table structure for table `form_submissions` */



DROP TABLE IF EXISTS `form_submissions`;



CREATE TABLE `form_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_account_lu_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL COMMENT '(utm_source)',
  `url` varchar(500) DEFAULT NULL COMMENT 'entry url',
  `domain` varchar(255) DEFAULT NULL COMMENT 'entry domain',
  `entry_page` varchar(255) DEFAULT NULL COMMENT 'entry page',
  `register_page` int(11) DEFAULT NULL,
  `campaign` varchar(255) DEFAULT NULL COMMENT 'product, promo code, slogan (utm_campaign)',
  `source` varchar(255) DEFAULT NULL COMMENT 'utm_source',
  `keywords` varchar(255) DEFAULT NULL COMMENT '(utm_term)',
  `medium` varchar(127) DEFAULT NULL COMMENT 'ppc, email, newsletter (utm_medium)',
  `added` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_account_lu_id` (`form_account_lu_id`),
  KEY `contact_id` (`contact_id`),
  KEY `source_id` (`source_id`),
  CONSTRAINT `form_submissions_ibfk_1` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `form_submissions_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `form_submissions_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `form_submissions` */



/*Table structure for table `form_views` */



DROP TABLE IF EXISTS `form_views`;



CREATE TABLE `form_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `home_detail_view_count` int(11) DEFAULT NULL,
  `domain` varchar(255) NOT NULL,
  `page` text NOT NULL,
  `url` text,
  `referrer` text NOT NULL,
  `utmz` text,
  `utma` text,
  `utmb` text,
  `utmc` text,
  `browser` text,
  `ip` varchar(40) NOT NULL,
  `session_id` varchar(63) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `form_views` */



/*Table structure for table `forms` */



DROP TABLE IF EXISTS `forms`;



CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_type_ma` tinyint(1) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `alert_label` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `forms` */



INSERT INTO `forms` (`id`, `form_type_ma`, `name`, `alert_label`, `is_deleted`) VALUES
(1, NULL, 'Contact Us', 'Contact Us', 0),
(2, NULL, 'Showing Request', 'Showing Request', 0),
(3, NULL, 'Share Home to Friend', 'Share Home to Friend', 0),
(4, NULL, 'Ask A Question', 'Ask A Property Question', 0),
(5, NULL, 'Property Tax', 'Property Tax', 0),
(6, NULL, 'House Values', 'House Values', 0),
(7, NULL, 'Home Details Forced Registration', 'Home Detail View', 0),
(8, NULL, 'Foreclosure List', 'Foreclosure List', 0),
(9, NULL, 'Free Reports', 'Free Reports', 0),
(10, NULL, 'Video Form', 'Video Form', 0),
(11, NULL, 'Home Details Multi-Purpose Form', 'Property Question', 0),
(12, NULL, 'Login Widget Registration', 'Login Widget', 0),
(13, NULL, 'We Buy Houses', 'We Buy Houses', 0),
(14, NULL, 'Move Once Program', 'Move Once Program', 0),
(15, NULL, 'Guaranteed Sale', 'Guaranteed Sale', 0),
(16, NULL, 'Triple Guarantee', 'Triple Guarantee', 0),
(17, NULL, 'Free Seller Report', 'Free Seller Report', 0),
(18, NULL, 'Sneak Preview Listing Storyboard', 'Sneak Preview Listing Storyboard', 0),
(19, NULL, 'Pre-Qualify', 'Pre-Qualify', 0),
(20, NULL, 'Sneak Preview Listing Storyboard', 'Sneak Preview Listing Storyboard', 0),
(21, NULL, 'Make an Offer (Listing Storyboard)', 'Make an Offer (LSB)', 0),
(22, NULL, 'Make an Offer (Home Details)', 'Make an Offer', 0);


/*Table structure for table `goals` */



DROP TABLE IF EXISTS `goals`;



CREATE TABLE `goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '1',
  `name` varchar(63) DEFAULT NULL,
  `year` smallint(4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `average_sales_price` int(11) NOT NULL,
  `average_commission_percent` decimal(2,2) NOT NULL,
  `commission_split` decimal(2,2) DEFAULT '0.50',
  `weeks_time_off` tinyint(2) NOT NULL DEFAULT '6',
  `lead_gen_minutes_per_day` tinyint(4) DEFAULT NULL,
  `income_per_year` int(11) NOT NULL,
  `contracts_closing_conversion` decimal(2,2) DEFAULT NULL,
  `agreements_contract_conversion` decimal(2,2) DEFAULT NULL,
  `appointments_agreement_conversion` decimal(2,2) DEFAULT NULL,
  `contacts_per_appointment` tinyint(4) DEFAULT NULL,
  `contacts_per_day` tinyint(2) DEFAULT NULL,
  `referral_percentage` decimal(2,2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `goals_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `goals` */



/*Table structure for table `history_log` */



DROP TABLE IF EXISTS `history_log`;



CREATE TABLE `history_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(127) DEFAULT NULL,
  `primary_key` int(11) NOT NULL,
  `action_type_ma` tinyint(1) DEFAULT NULL,
  `old_value` blob,
  `new_value` blob,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_log_ibfk_1` (`added_by`),
  CONSTRAINT `history_log_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `history_log` */



/*Table structure for table `homes_shown` */



DROP TABLE IF EXISTS `homes_shown`;



CREATE TABLE `homes_shown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `shown_by` int(11) DEFAULT '0',
  `homes_saved_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT NULL COMMENT 'scheduled or shown',
  `datetime` timestamp NULL DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `wrote_offer` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `shown_by` (`shown_by`),
  KEY `homes_saved_id` (`homes_saved_id`),
  CONSTRAINT `homes_shown_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `homes_shown_ibfk_2` FOREIGN KEY (`shown_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `homes_shown_ibfk_3` FOREIGN KEY (`homes_saved_id`) REFERENCES `saved_homes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `homes_shown` */



/*Table structure for table `lead_round_robbin_contact_lu` */



DROP TABLE IF EXISTS `lead_round_robbin_contact_lu`;



CREATE TABLE `lead_round_robbin_contact_lu` (
  `lead_route_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `leads_per_round` tinyint(1) DEFAULT '1',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  KEY `lead_route_id` (`lead_route_id`),
  KEY `updated_by` (`updated_by`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `lead_round_robbin_contact_lu_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lead_round_robbin_contact_lu_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lead_round_robbin_contact_lu_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `lead_round_robbin_contact_lu` */



/*Table structure for table `lead_route_autoresponders` */



DROP TABLE IF EXISTS `lead_route_autoresponders`;



CREATE TABLE `lead_route_autoresponders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` int(11) NOT NULL,
  `type_ma` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lead_route_ref` (`lead_route_id`),
  KEY `added_by_ref` (`added_by`),
  KEY `updated_by_ref` (`updated_by`),
  CONSTRAINT `added_by_ref` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`),
  CONSTRAINT `lead_route_ref` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`),
  CONSTRAINT `updated_by_ref` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
) ENGINE=InnoDB;



/*Data for the table `lead_route_autoresponders` */



/*Table structure for table `lead_route_rules` */



DROP TABLE IF EXISTS `lead_route_rules`;



CREATE TABLE `lead_route_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `field_ma` tinyint(4) DEFAULT NULL COMMENT 'group, source, subsource, time',
  `operator_ma` tinyint(1) DEFAULT '0',
  `value` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lead_route_id` (`lead_route_id`),
  CONSTRAINT `lead_route_rules_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='value, is_greater_than, is_less_than, is_equal, is_or_equal_to';



/*Data for the table `lead_route_rules` */



insert  into `lead_route_rules`(`id`,`lead_route_id`,`name`,`field_ma`,`operator_ma`,`value`,`is_deleted`) values (1,1,'Buyer Leads',1,1,'Buyer',0);



/*Table structure for table `lead_routes` */



DROP TABLE IF EXISTS `lead_routes`;



CREATE TABLE `lead_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `context_ma` tinyint(1) DEFAULT NULL,
  `type_ma` tinyint(1) DEFAULT NULL COMMENT 'shift or round robbin',
  `component_type_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `default_contact_id` int(11) DEFAULT NULL,
  `overflow_action_ma` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `sort_order` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `default_contact_id` (`default_contact_id`),
  KEY `component_type_id` (`component_type_id`),
  CONSTRAINT `lead_routes_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lead_routes_ibfk_2` FOREIGN KEY (`default_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lead_routes_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='User Groups';



/*Data for the table `lead_routes` */



insert  into `lead_routes`(`id`,`account_id`,`type_ma`,`component_type_id`,`name`,`description`,`default_contact_id`,`overflow_action_ma`,`is_deleted`,`sort_order`) values (1,1,1,2,'Buyer Leads','Handles Buyer type leads',28127,1,0,NULL),(2,1,1,3,'Seller Leads','Handles Seller type leads',27354,1,0,NULL),(3,1,1,NULL,'Website Contact','General web contact form',NULL,NULL,0,NULL),(4,1,1,NULL,'General Recruiting','Team opportunity inquiries',NULL,NULL,0,NULL),(5,1,1,NULL,'Agent Recruiting','Agent opportunity inquiries',NULL,NULL,0,NULL);


--
-- Table structure for table `lead_routes_triggered`
--

CREATE TABLE IF NOT EXISTS `lead_routes_triggered` (
  `parent_route_id` int(11) NOT NULL DEFAULT '0',
  `triggered_route_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`parent_route_id`,`triggered_route_id`),
  KEY `triggered_route_id` (`triggered_route_id`),
  CONSTRAINT `lead_routes_triggered_ibfk_2` FOREIGN KEY (`triggered_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lead_routes_triggered_ibfk_1` FOREIGN KEY (`parent_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO `lead_routes_triggered` (`parent_route_id`, `triggered_route_id`) VALUES
(1, 8),
(2, 8),
(3, 8);

/*Table structure for table `lead_shifts` */



DROP TABLE IF EXISTS `lead_shifts`;



CREATE TABLE `lead_shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `updated_by` (`updated_by`),
  KEY `lead_route_id` (`lead_route_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `lead_shifts_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lead_shifts_ibfk_2` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lead_shifts_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `lead_shifts` */



/*Table structure for table `leads` */



DROP TABLE IF EXISTS `leads`;



CREATE TABLE `leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` int(11) DEFAULT NULL COMMENT 'shift or round robbin',
  `assigned_to_id` int(11) DEFAULT NULL,
  `origin_component_type_id` int(11) DEFAULT NULL,
  `origin_component_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lead_route_id` (`lead_route_id`),
  KEY `assigned_to_id` (`assigned_to_id`),
  KEY `component_type_id` (`component_type_id`) USING BTREE,
  KEY `origin_component_type_id` (`origin_component_type_id`) USING BTREE,
  CONSTRAINT `leads_component_type_id` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `leads_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `leads_ibfk_2` FOREIGN KEY (`assigned_to_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `leads_origin_component_type_id` FOREIGN KEY (`origin_component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB COMMENT='User Groups';



/*Data for the table `leads` */



/*Table structure for table `login_log` */



DROP TABLE IF EXISTS `login_log`;



CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `login_log` */



/*Table structure for table `market_trend_data` */



DROP TABLE IF EXISTS `market_trend_data`;



CREATE TABLE `market_trend_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `market_trend_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `market_trend_id` (`market_trend_id`),
  CONSTRAINT `market_trend_data_ibfk_1` FOREIGN KEY (`market_trend_id`) REFERENCES `market_trends` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `market_trend_data` */



/*Table structure for table `market_trends` */



DROP TABLE IF EXISTS `market_trends`;



CREATE TABLE `market_trends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `criteria` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `market_trends_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `market_trends` */



/*Table structure for table `market_update_log` */



DROP TABLE IF EXISTS `market_update_log`;



CREATE TABLE `market_update_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saved_home_search_id` int(11) DEFAULT NULL,
  `mls_property_id` int(11) DEFAULT NULL,
  `send_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mls_property_id` (`mls_property_id`),
  KEY `market_update_log_ibfk_1` (`saved_home_search_id`),
  CONSTRAINT `market_update_log_ibfk_1` FOREIGN KEY (`saved_home_search_id`) REFERENCES `saved_home_searches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `market_update_log` */



/*Table structure for table `mobile_gateway_phone_lu` */



DROP TABLE IF EXISTS `mobile_gateway_phone_lu`;



CREATE TABLE `mobile_gateway_phone_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_id` int(11) NOT NULL DEFAULT '0',
  `mobile_gateway_id` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mobile_gateway_id` (`mobile_gateway_id`),
  KEY `phone_id` (`phone_id`),
  CONSTRAINT `mobile_gateway_phone_lu_ibfk_1` FOREIGN KEY (`mobile_gateway_id`) REFERENCES `mobile_gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `mobile_gateway_phone_lu_ibfk_2` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `mobile_gateway_phone_lu` */



/*Table structure for table `mobile_gateways` */



DROP TABLE IF EXISTS `mobile_gateways`;



CREATE TABLE `mobile_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_name` varchar(63) DEFAULT NULL,
  `sms_gateway` varchar(63) DEFAULT NULL,
  `mms_gateway` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `mobile_gateways` */



insert  into `mobile_gateways`(`id`,`carrier_name`,`sms_gateway`,`mms_gateway`,`is_deleted`) values (1,'AT&T','@txt.att.net',NULL,0),(2,'Sprint','@messaging.sprintpcs.com',NULL,0),(3,'T-mobile','@tmomail.net',NULL,0),(4,'Verizon','@vtext.com',NULL,0),(5, 'US Cellular', '@email.uscc.net', 'mms.uscc.net ', 0),(6, 'Virgin Mobile', '@vmobl.com', NULL, 0);;



/*Table structure for table `operation_manual_account_lu` */



DROP TABLE IF EXISTS `operation_manual_account_lu`;



CREATE TABLE `operation_manual_account_lu` (
  `operation_manual_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`operation_manual_id`,`account_id`),
  KEY `operation_manual_account_lu_ibfk_2` (`account_id`),
  CONSTRAINT `operation_manual_account_lu_ibfk_1` FOREIGN KEY (`operation_manual_id`) REFERENCES `operation_manuals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `operation_manual_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `operation_manual_account_lu` */



/*Table structure for table `operation_manual_role_permissions` */



DROP TABLE IF EXISTS `operation_manual_role_permissions`;



CREATE TABLE `operation_manual_role_permissions` (
  `operation_manual_tag_id` int(11) NOT NULL DEFAULT '0',
  `auth_item_name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`operation_manual_tag_id`,`auth_item_name`),
  KEY `operation_manual_role_permissions_ibfk_2` (`auth_item_name`),
  CONSTRAINT `operation_manual_role_permissions_ibfk_1` FOREIGN KEY (`operation_manual_tag_id`) REFERENCES `operation_manual_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `operation_manual_role_permissions_ibfk_2` FOREIGN KEY (`auth_item_name`) REFERENCES `auth_item` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `operation_manual_role_permissions` */



/*Table structure for table `operation_manual_tag_lu` */



DROP TABLE IF EXISTS `operation_manual_tag_lu`;



CREATE TABLE `operation_manual_tag_lu` (
  `operation_manual_id` int(11) NOT NULL,
  `operation_manual_tag_id` int(11) NOT NULL,
  PRIMARY KEY (`operation_manual_id`,`operation_manual_tag_id`),
  KEY `operation_manual_tag_lu_ibfk_2` (`operation_manual_tag_id`),
  CONSTRAINT `operation_manual_tag_lu_ibfk_1` FOREIGN KEY (`operation_manual_id`) REFERENCES `operation_manuals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `operation_manual_tag_lu_ibfk_2` FOREIGN KEY (`operation_manual_tag_id`) REFERENCES `operation_manual_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `operation_manual_tag_lu` */



/*Table structure for table `operation_manual_tag_permission_lu` */



DROP TABLE IF EXISTS `operation_manual_tag_permission_lu`;



CREATE TABLE `operation_manual_tag_permission_lu` (
  `operation_manual_tag_id` int(11) NOT NULL,
  `auth_item_name` varchar(64) NOT NULL,
  PRIMARY KEY (`operation_manual_tag_id`,`auth_item_name`),
  KEY `operation_manual_tag_permission_lu_ibfk_2` (`auth_item_name`),
  CONSTRAINT `operation_manual_tag_permission_lu_ibfk_1` FOREIGN KEY (`operation_manual_tag_id`) REFERENCES `operation_manual_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `operation_manual_tag_permission_lu_ibfk_2` FOREIGN KEY (`auth_item_name`) REFERENCES `auth_item` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `operation_manual_tag_permission_lu` */



/*Table structure for table `operation_manual_tags` */



DROP TABLE IF EXISTS `operation_manual_tags`;



CREATE TABLE `operation_manual_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `PRIMARY KEY` (`account_id`),
  KEY `operation_manual_tags_ibfk_1` (`parent_id`),
  CONSTRAINT `operation_manual_tags_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `operation_manual_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `PRIMARY KEY` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `operation_manual_tags` */



insert  into `operation_manual_tags`(`id`,`account_id`,`name`,`parent_id`) values (1,2,'Buyer Agents',NULL),(2,2,'Script',NULL);



/*Table structure for table `operation_manuals` */



DROP TABLE IF EXISTS `operation_manuals`;



CREATE TABLE `operation_manuals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `content` blob NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `operation_manuals_ibfk_1` (`account_id`),
  CONSTRAINT `operation_manuals_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `operation_manuals` */



/*Table structure for table `operators` */



DROP TABLE IF EXISTS `operators`;



CREATE TABLE `operators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `operators` */



insert  into `operators`(`id`,`name`,`symbol`) values (1,'equals','='),(2,'not_equals','!='),(3,'between',''),(4,'greater_than','>'),(5,'less_than','<'),(6,'greater_than_equals','>='),(7,'less_than_equals','<='),(8,'like','like'),(9,'like_wildcard','like%%'),(10,'and','AND'),(11,'or','OR');



/*Table structure for table `phones` */



DROP TABLE IF EXISTS `phones`;



CREATE TABLE `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `phone_type_ma` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `extension` varchar(50) DEFAULT NULL,
  `owner_ma` tinyint(1) DEFAULT '0',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`),
  KEY `company_id` (`company_id`),
  KEY `phone` (`phone`) USING BTREE,
  KEY `phone_type_ma` (`phone_type_ma`) USING BTREE,
  KEY `owner_ma` (`owner_ma`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  CONSTRAINT `phones_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `phones_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `phones_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `phones` */



/*Table structure for table `processed_contact_responses` */



DROP TABLE IF EXISTS `processed_contact_responses`;



CREATE TABLE `processed_contact_responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imap_message_id` int(11) NOT NULL,
  `email_message_id` int(11) NOT NULL,
  `processed` tinyint(1) DEFAULT '0',
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `processed_contact_responses` */



/*Table structure for table `project_items` */



DROP TABLE IF EXISTS `project_items`;



CREATE TABLE `project_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `description` blob,
  `is_priority` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `project_items_ibfk_2` (`updated_by`),
  KEY `project_items_ibfk_3` (`added_by`),
  CONSTRAINT `project_items_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_items_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_items_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `project_items` */



/*Table structure for table `projects` */



DROP TABLE IF EXISTS `projects`;



CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) NOT NULL DEFAULT '1',
  `contact_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(127) DEFAULT NULL,
  `is_priority` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `due_date` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`),
  KEY `projects_ibfk_3` (`updated_by`),
  KEY `projects_ibfk_4` (`added_by`),
  KEY `projects_ibfk_5` (`parent_id`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `projects_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `projects_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `projects_ibfk_5` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `projects` */



/*Table structure for table `recruit_type_lu` */



DROP TABLE IF EXISTS `recruit_type_lu`;



CREATE TABLE `recruit_type_lu` (
  `recruit_id` int(11) NOT NULL,
  `recruit_type_id` int(11) NOT NULL,
  PRIMARY KEY (`recruit_id`,`recruit_type_id`),
  KEY `recruit_id` (`recruit_id`),
  KEY `recruit_type_id` (`recruit_type_id`),
  CONSTRAINT `recruit_type_lu_ibfk_1` FOREIGN KEY (`recruit_id`) REFERENCES `recruits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `recruit_type_lu_ibfk_2` FOREIGN KEY (`recruit_type_id`) REFERENCES `recruit_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `recruit_type_lu` */



/*Table structure for table `recruit_types` */



DROP TABLE IF EXISTS `recruit_types`;



CREATE TABLE `recruit_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `recruit_types` */



insert  into `recruit_types`(`id`,`name`) values (1,'Unknown'),(2,'Allied Resource'),(3,'Admin'),(4,'Agent'),(5,'Buyer Agent'),(6,'Listing Agent'),(7,'ISA'),(8,'Listing Manager'),(9,'Closing Manager'),(10,'Executive Assistant'),(11,'JAR Tracking');



/*Table structure for table `recruits` */



DROP TABLE IF EXISTS `recruits`;



CREATE TABLE `recruits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `referred_by_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) NOT NULL,
  `is_licensed` tinyint(1) DEFAULT NULL,
  `behavioral_profile` varchar(127) DEFAULT NULL,
  `disc_D` tinyint(2) DEFAULT NULL,
  `disc_I` tinyint(2) DEFAULT NULL,
  `disc_S` tinyint(2) DEFAULT NULL,
  `disc_C` tinyint(2) DEFAULT NULL,
  `disc_date` date DEFAULT NULL,
  `ava_vector_1` tinyint(2) DEFAULT NULL,
  `ava_vector_1_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_2` tinyint(2) DEFAULT NULL,
  `ava_vector_2_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_3` tinyint(2) DEFAULT NULL,
  `ava_vector_3_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_4` tinyint(2) DEFAULT NULL,
  `ava_vector_4_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_5` tinyint(2) DEFAULT NULL,
  `ava_vector_5_asterisk` tinyint(1) DEFAULT NULL,
  `ava_date` date DEFAULT NULL,
  `current_production` varchar(255) DEFAULT NULL,
  `goal` varchar(127) DEFAULT NULL,
  `dream_goal` varchar(127) DEFAULT NULL,
  `motivation` varchar(127) DEFAULT NULL,
  `application_status_ma` tinyint(1) NOT NULL DEFAULT '0',
  `met_status_ma` tinyint(1) NOT NULL DEFAULT '0',
  `rating` decimal(3,1) DEFAULT NULL,
  `strengths` varchar(255) DEFAULT NULL,
  `weaknesses` varchar(255) DEFAULT NULL,
  `communication_skills` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `current_employer` varchar(63) DEFAULT NULL,
  `years_current_job` varchar(127) DEFAULT NULL,
  `years_previous_job` varchar(127) DEFAULT NULL,
  `average_years_job` varchar(127) DEFAULT NULL,
  `notes` text,
  `updated_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`),
  KEY `source_id` (`source_id`),
  CONSTRAINT `recruits_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `recruits_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `recruits_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `recruits_ibfk_4` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `recruits` */



/*Table structure for table `referrals` */



DROP TABLE IF EXISTS `referrals`;



CREATE TABLE `referrals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_type_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `referral_direction_ma` tinyint(1) DEFAULT NULL,
  `referral_agreement_signed` tinyint(1) NOT NULL DEFAULT '0',
  `status_ma` tinyint(1) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `outside_referral_agent_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `referral_date` timestamp NULL DEFAULT NULL,
  `referral_percent` decimal(4,2) DEFAULT NULL,
  `referral_paid` decimal(11,2) DEFAULT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `closed_date` timestamp NULL DEFAULT NULL,
  `commission_gross` decimal(11,2) DEFAULT NULL,
  `commission_net` decimal(11,2) DEFAULT NULL,
  `notes` varchar(750) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `referred_by` (`assigned_to_id`),
  KEY `referred_to` (`outside_referral_agent_id`),
  KEY `source_id` (`source_id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`),
  KEY `referrals_ibfk_7` (`transaction_type_id`),
  CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`assigned_to_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referrals_ibfk_2` FOREIGN KEY (`outside_referral_agent_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referrals_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referrals_ibfk_4` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referrals_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referrals_ibfk_6` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referrals_ibfk_7` FOREIGN KEY (`transaction_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `referrals` */



/*Table structure for table `saved_home_searches` */



DROP TABLE IF EXISTS `saved_home_searches`;



CREATE TABLE `saved_home_searches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `criteria` text,
  `frequency` text,
  `property_count` int(11) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`),
  KEY `saved_home_searches_ibfk_4` (`agent_id`),
  CONSTRAINT `saved_home_searches_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `saved_home_searches_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `saved_home_searches_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `saved_home_searches_ibfk_4` FOREIGN KEY (`agent_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `saved_home_searches` */



/*Table structure for table `saved_homes` */



DROP TABLE IF EXISTS `saved_homes`;



CREATE TABLE `saved_homes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `listing_id` varchar(50) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT NULL COMMENT 'like, dislike, seen',
  `notes` varchar(255) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `saved_homes_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `saved_homes` */



/*Table structure for table `setting_account_values` */



DROP TABLE IF EXISTS `setting_account_values`;



CREATE TABLE `setting_account_values` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `setting_id` int(11) DEFAULT NULL,
  `value` text,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `account_id` (`account_id`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `setting_account_values_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `setting_account_values_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `setting_account_values_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Table structure for table `setting_contact_values` */



DROP TABLE IF EXISTS `setting_contact_values`;



CREATE TABLE `setting_contact_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `setting_id` int(11) DEFAULT NULL,
  `value` text,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `setting_contact_values_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `setting_contact_values_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `setting_contact_values_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `setting_contact_values` */



/*Table structure for table `settings` */



DROP TABLE IF EXISTS `settings`;



CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_type_ma` tinyint(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `label` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB COMMENT='User Groups';



/*Data for the table `settings` */



insert  into `settings`(`id`,`setting_type_ma`,`name`,`label`,`description`,`is_deleted`) values (1,1,'office_name','Name',NULL,0),(2,1,'office_phone','Phone',NULL,0),(3,1,'office_fax','Fax',NULL,0),(4,1,'office_email','Email',NULL,0),(5,1,'office_address','Address',NULL,0),(6,1,'office_city','City',NULL,0),(7,1,'office_state','State',NULL,0),(8,1,'office_zip','Zip',NULL,0),(9,1,'anniversary_date','Anniversary',NULL,0),(10,1,'end_of_day','End of Day',NULL,0),(11,1,'views_max','Home Views Max',NULL,0),(12,1,'primary_market_trend_id','Primary Market Trend',NULL,0),(13,1,'reponse_time_active','Active Propsect',NULL,0),(14,1,'reponse_time_new_lead','New Lead Response Time',NULL,0),(15,1,'reponse_time_A','A Lead Response Time',NULL,0),(16,1,'reponse_time_B','B Lead Response Time',NULL,0),(17,1,'reponse_time_C','C Lead Response Time',NULL,0),(18,1,'sphere_touch_frequency','Sphere',NULL,0),(19,2,'agent_mls_num','Agent MLS #',NULL,0),(20,2,'dashboard','Dashboard Layout',NULL,0),(21,2,'agent_website','Agent Website',NULL,0),(22,2,'profile_photo','Photo',NULL,0),(23,2,'gender','Gender',NULL,0),(24,2,'title','Title',NULL,0),(25,2,'subtitle','Subtitle',NULL,0),(26,2,'designations','Designations',NULL,0),(27,3,'email_signature','Email Signature',NULL,0),(28,2,'cell_number','Cell #',NULL,0),(29,2,'cell_carrier_id','Cell Carrier',NULL,0),(30,2,'bio','Personal Bio',NULL,0),(31,2,'social_facebook','Facebook',NULL,0),(32,2,'social_linkedin','LinkedIn',NULL,0),(33,2,'social_youtube','You Tube',NULL,0),(34,2,'social_twitter','Twitter',NULL,0),(35,2,'social_activerain','Active Rain',NULL,0),(36,2,'social_google','Google+',NULL,0),(37,3,'top_links','Top Links',NULL,0),(38,2,'sticky_notes','Sticky Notes',NULL,0),(39,2,'primary_goal_id','Goal',NULL,0),(40,2,'memorable_moments','Memorable Moments',NULL,0),(41,2,'favorite_quotes','Favorite Quotes',NULL,0),(42,2,'words_describe_self','Words to Describe Self',NULL,0),(43,2,'profile_on_website','Profile on Website',NULL,0),(44,1,'website_footer_contact','Website Footer Contact',NULL,0),(45,1,'email_all','Email All',NULL,0),(46,2,'default_assignment_type','Assignment Type',NULL,0),(47,1,'primary_domain','Primary Domain',NULL,0),(48,1,'website_flyer_contact','Website Flyer Contact',NULL,0),(49,2,'team_direct_phone','Team Direct Phone',NULL,0),(50,1,'lead_pool_contact_id','Lead Pool Contact',NULL,0),(51,1,'max_admin_users','Max Users',NULL,0),(52,1,'bombbomb_api_key','Bomb Bomb API Key',NULL,0),(53,1,'google_tracking_code','Google Tracking Code',NULL,0),(54, '2', 'lender_company_name', 'Lender Company', NULL, '0'),(55, '2', 'lender_address', 'Lender Address', NULL, '0'),(56, '2', 'lender_city', 'City', NULL, '0'),(57, '2', 'lender_state_id', 'State', NULL, '0'),(58, '2', 'lender_zip', 'Zip', NULL, '0'),(59, '2', 'lender_office_phone', 'Office Phone', NULL, '0'),(60, '2', 'lender_direct_phone', 'Direct Phone', NULL, '0'),(61, '2', 'lender_fax', 'Fax', NULL, '0'),(62, '2', 'lender_website', 'Website', NULL, '0'),(63, '2', 'lender_license_number', 'License #', NULL, '0'),(64, '2', 'lender_logo', 'Lender Logo', NULL, '0'),(65, '1', 'blog_comment_moderator_email', 'Blog Comment Moderator Email', NULL, '0'),(66, 1, 'daily_task_executive_summary_recipients', 'Daily Task Executive Summary Recipients', NULL, '0'), ('67', '2', 'send_daily_task_email', 'Send Daily Task Email', NULL, '0'), ('68', '1', 'closing_pipeline_executive_summary_recipients', 'Closing Pipeline Executive Summary Recipients', NULL, '0'),('69', '2', 'send_closing_report_email', 'Send Closing Report Email', NULL, '0'),('70', '1', 'appointments_executive_summary_recipients', 'Appointments Executive Summary Recipients', NULL, '0'),('71', '2', 'send_appointments_report_email', 'Send Appointments Report Email', NULL, '0'),('72', '1', 'goals_executive_summary_recipients', 'Goals Executive Summary Recipients', NULL, '0'),('73', '2', 'send_goals_report_email', 'Send Goals Report Email', NULL, '0'),('74', '1', 'appointments_executive_summary_agents', 'Appointments Executive Summary Agents', NULL, '0');



/*Table structure for table `sources` */



DROP TABLE IF EXISTS `sources`;



CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `sources_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `sources` */


INSERT INTO `sources` (`id`, `account_id`, `parent_id`, `name`, `description`, `added`, `is_deleted`) VALUES
(1, 1, NULL, 'Unknown', NULL, NULL, 0),
(2, 1, NULL, 'Sphere & Referrals', NULL, NULL, 0),
(3, 1, 2, 'Past Client', NULL, NULL, 0),
(4, 1, 2, 'Sphere (Personally Known)', '', NULL, 0),
(5, 1, 2, 'Agent Referral', '', NULL, 0),
(6, 1, NULL, 'Craigslist', '', NULL, 0),
(7, 1, NULL, 'Dave Ramsey', NULL, NULL, 0),
(8, 1, NULL, 'Farming', NULL, NULL, 0),
(9, 1, NULL, 'Expired', NULL, NULL, 0),
(10, 1, NULL, 'FSBO', NULL, NULL, 0),
(11, 1, NULL, 'IVR', NULL, NULL, 0),
(12, 1, NULL, 'Newsletter', NULL, NULL, 0),
(13, 1, NULL, 'Open House', NULL, NULL, 0),
(14, 1, NULL, 'Radio', NULL, NULL, 0),
(15, 1, NULL, 'Sign Call', NULL, NULL, 0),
(16, 1, NULL, 'TV', NULL, NULL, 0),
(17, 1, NULL, 'Website', NULL, NULL, 0),
(18, 1, 9, 'Expired Calls', NULL, NULL, 0),
(19, 1, 9, 'Expired Mailing', NULL, NULL, 0),
(20, 1, 8, 'Just Sold', NULL, NULL, 0),
(21, 1, 8, 'Short Sale Letter', NULL, NULL, 0),
(22, 1, NULL, 'Transaction / Cross Sale', '', NULL, 0),
(23, 1, NULL, 'Door Knocking', '', NULL, 0),
(24, 1, NULL, 'Investor', '', NULL, 0),
(25, 1, 2, 'Referral from Sphere (New Contact)', NULL, NULL, 0),
(26, 1, NULL, 'Zillow', 'Web Lead', NULL, 0);


/*Table structure for table `splash_screens` */



DROP TABLE IF EXISTS `splash_screens`;



CREATE TABLE `splash_screens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `content` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `splash_screens_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `splash_screens` */



/*Table structure for table `task_email_template_lu` */



DROP TABLE IF EXISTS `task_email_template_lu`;



CREATE TABLE `task_email_template_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT '0',
  `email_template_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  KEY `email_template_id` (`email_template_id`),
  CONSTRAINT `task_email_template_lu_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `task_email_template_lu_ibfk_2` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `task_email_template_lu` */



/*Table structure for table `task_type_component_lu` */



DROP TABLE IF EXISTS `task_type_component_lu`;



CREATE TABLE `task_type_component_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `task_type_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `component_type_id` (`component_type_id`),
  CONSTRAINT `task_type_component_lu_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `task_type_component_lu_ibfk_2` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `task_type_component_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `task_type_component_lu` */



insert  into `task_type_component_lu`(`id`,`account_id`,`task_type_id`,`component_type_id`,`is_deleted`) values (1,1,1,1,0),(2,1,2,1,0),(3,1,3,1,0),(4,1,4,1,0),(5,1,5,1,0),(6,1,6,1,0),(7,1,7,1,0),(8,1,8,1,0),(9,1,25,1,0),(10,1,1,2,0),(11,1,2,2,0),(12,1,3,2,0),(13,1,4,2,0),(14,1,5,2,0),(15,1,6,2,0),(16,1,7,2,0),(17,1,8,2,0),(18,1,9,2,0),(19,1,10,2,0),(20,1,11,2,0),(21,1,12,2,0),(22,1,13,2,0),(23,1,14,2,0),(24,1,25,2,0),(25,1,1,3,0),(26,1,2,3,0),(27,1,3,3,0),(28,1,4,3,0),(29,1,5,3,0),(30,1,6,3,0),(31,1,7,3,0),(32,1,8,3,0),(33,1,9,3,0),(34,1,10,3,0),(35,1,11,3,0),(36,1,12,3,0),(37,1,13,3,0),(38,1,14,3,0),(39,1,15,3,0),(40,1,16,3,0),(41,1,17,3,0),(42,1,25,3,0),(43,1,27,3,0),(44,1,1,5,0),(45,1,2,5,0),(46,1,3,5,0),(47,1,4,5,0),(48,1,5,5,0),(49,1,6,5,0),(50,1,7,5,0),(51,1,18,5,0),(52,1,19,5,0),(53,1,20,5,0),(54,1,21,5,0),(55,1,22,5,0),(56,1,23,5,0),(57,1,1,6,0),(58,1,2,6,0),(59,1,3,6,0),(60,1,4,6,0),(61,1,5,6,0),(62,1,6,6,0),(63,1,7,6,0),(64,1,1,14,0),(65,1,2,14,0),(66,1,3,14,0),(67,1,4,14,0),(68,1,5,14,0),(69,1,6,14,0),(70,1,7,14,0),(71,1,8,14,0),(72,1,2,15,0),(73,1,3,15,0),(74,1,25,15,0),(75,1,26,15,0),(76,1,1,10,0),(77,1,2,10,0),(78,1,3,10,0),(79,1,4,10,0),(80,1,5,10,0),(81,1,6,10,0),(82,1,7,10,0),(83,1,8,10,0),(84,1,1,19,0),(85,1,2,19,0),(86,1,3,19,0),(87,1,4,19,0),(88,1,5,19,0),(89,1,6,19,0),(90,1,7,19,0),(91,1,8,19,0),(92,1,26,19,0),(93, '1', '32', '1', '0'), (94, '1', '32', '2', '0'), (95, '1', '32', '3', '0'), (96, '1', '32', '5', '0'), (97, '1', '32', '10', '0');



/*Table structure for table `task_types` */



DROP TABLE IF EXISTS `task_types`;



CREATE TABLE `task_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `task_types` */



insert  into `task_types`(`id`,`name`,`is_deleted`) 
  values (1,'To Do',0),(2,'Phone',0),(3,'Email (Manual)',0),(4,'Text Message',0),(5,'Video Mail',0),
  (6,'Handwritten Note',0),(7,'Mail',0),(8,'Log Note',0),(9,'Initial Consultation',0),(10,'Signed Agreement',0),
  (11,'Showing',0),(12,'Contract Offer',0),(13,'Contract Counter-Offer',0),(14,'CMA',0),(15,'Showing Feedback',0),
  (16,'Price Reduction',0),(17,'Seller Market Update',0),(18,'Contract Executed',0),(19,'Home Inspection',0),
  (20,'Repair Agreement',0),(21,'Appraisal',0),(22,'HUD',0),(23,'Closing',0),(24,'Lead Transfer',0),(25,'Web Chat',0),
  (26,'Met in Person',0),(27,'Sent Pre-list Package',0),(28,'Password Reset',0),(29,'Re-submit',0),(30,'Email Drip (Auto)',0),
  (31,'Action Plan',0),(32,'Waiting',0),(33,'Bad Phone #',0),(34,'Welcome Email',0),(35,'Blog Comment',0),(36,'Email Drip (Auto)',0),(37,'System Note',0);



/*Table structure for table `tasks` */



DROP TABLE IF EXISTS `tasks`;



CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL COMMENT 'value for seller_id, contact_id',
  `is_priority` tinyint(1) NOT NULL DEFAULT '0',
  `task_type_id` int(11) DEFAULT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `assigned_by_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `complete_date` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `depends_on_id` int(11) DEFAULT NULL,
  `task_recursion_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_to_id` (`assigned_to_id`),
  KEY `assigned_by_id` (`assigned_by_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `account_id` (`account_id`),
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `component_type_component_id` (`component_type_id`,`component_id`) USING BTREE,
  KEY `complete_date` (`complete_date`) USING BTREE,
  KEY `due_date` (`due_date`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  KEY `task_recursion_id` (`task_recursion_id`),
  CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`assigned_to_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`assigned_by_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tasks_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tasks_ibfk_4` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tasks_ibfk_5` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tasks_ibfk_6` FOREIGN KEY (`task_recursion_id`) REFERENCES `task_recursions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;


--
-- Table structure for table `task_recursions`
--

CREATE TABLE IF NOT EXISTS `task_recursions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `recur_type` enum('One time only','Daily','Every weekday (Monday to Friday)','Weekly','Monthly','Yearly') NOT NULL DEFAULT 'One time only',
  `recur_repeat_every` tinyint(1) DEFAULT '1',
  `recur_end_after_occurrences` int(11) DEFAULT NULL,
  `recur_start_date` datetime DEFAULT NULL,
  `recur_end_date` datetime DEFAULT NULL,
  `recur_added` datetime DEFAULT NULL,
  `recur_added_by` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

/*Data for the table `tasks` */



/*Table structure for table `tbl_migration` */



DROP TABLE IF EXISTS `tbl_migration`;



CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB;



/*Data for the table `tbl_migration` */



/*Table structure for table `term_component_lu` */



DROP TABLE IF EXISTS `term_component_lu`;



CREATE TABLE `term_component_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `term_conjunctor` varchar(5) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `term_id` (`term_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `component_id` (`component_id`),
  CONSTRAINT `term_component_lu_ibfk_2` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `term_component_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `term_component_lu` */



insert  into `term_component_lu`(`id`,`component_type_id`,`component_id`,`term_id`,`value`,`term_conjunctor`,`group_id`) values (1,16,1,13,'%Albany%',NULL,NULL),(2,16,2,13,'%Albion%',NULL,NULL),(3,16,3,13,'%Belleville%',NULL,NULL),(4,16,4,13,'%Brooklyn%',NULL,NULL),(5,16,5,13,'%Busseyville%',NULL,NULL),(6,16,6,13,'%Cambridge%',NULL,NULL),(7,16,7,13,'%Cooksville%',NULL,NULL),(8,16,8,13,'%Dayton%',NULL,NULL),(9,16,9,13,'%Edgerton%',NULL,NULL),(10,16,10,13,'%Evansville%',NULL,NULL),(11,16,11,13,'%Fitchburg%',NULL,NULL),(12,16,12,13,'%Footville%',NULL,NULL),(13,16,13,13,'%Fulton%',NULL,NULL),(14,16,14,13,'%Indianford%',NULL,NULL),(15,16,15,13,'%Lake Kegonsa%',NULL,NULL),(16,16,16,13,'%Lake Koshkonong%',NULL,NULL),(17,16,17,13,'%Madison%',NULL,NULL),(18,16,18,13,'%Magnolia%',NULL,NULL),(19,16,19,13,'%McFarland%',NULL,NULL),(20,16,20,13,'%Monona%',NULL,NULL),(21,16,21,13,'%Newville%',NULL,NULL),(22,16,22,13,'%Oregon%',NULL,NULL),(23,16,23,13,'%Rockdale%',NULL,NULL),(24,16,24,13,'%Stoughton%',NULL,NULL),(25,16,25,13,'%Union%',NULL,NULL),(26,16,26,13,'%Utica%',NULL,NULL),(27,16,27,13,'%Verona%',NULL,NULL),(28,16,28,13,'%Sun Prairie%',NULL,NULL),(29,16,29,13,'%Cottage Grove%',NULL,NULL),(30,16,30,13,'%Marshall%',NULL,NULL),(31,16,31,13,'%Waterloo%',NULL,NULL),(32,16,32,13,'%Columbus%',NULL,NULL),(33,16,33,13,'%Deerfield%',NULL,NULL),(34,16,34,13,'%Arlington%',NULL,NULL),(35,16,35,13,'%Poynette%',NULL,NULL),(36,16,36,13,'%Deforest%',NULL,NULL),(37,16,37,13,'%Dane%',NULL,NULL),(38,16,38,13,'%Windsor%',NULL,NULL),(39,16,39,13,'%Waunakee%',NULL,NULL),(40,16,40,13,'%Middleton%',NULL,NULL),(41,16,41,13,'%%',NULL,NULL);



/*Table structure for table `term_criteria` */



DROP TABLE IF EXISTS `term_criteria`;



CREATE TABLE `term_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `term_id` int(11) NOT NULL,
  `type_ma` tinyint(4) NOT NULL,
  `column` varchar(127) NOT NULL,
  `operator_id` int(11) NOT NULL,
  `constant_value` text NOT NULL,
  `default_conjunctor` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `term_id` (`term_id`),
  KEY `operator_id` (`operator_id`),
  CONSTRAINT `term_criteria_ibfk_1` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `term_criteria_ibfk_2` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `term_criteria` */



insert  into `term_criteria`(`id`,`term_id`,`type_ma`,`column`,`operator_id`,`constant_value`,`default_conjunctor`) values (1,1,1,'status',1,'',''),(2,2,1,'price',6,'',''),(3,3,1,'price',7,'',''),(4,4,1,'bedrooms',6,'',''),(5,5,1,'baths_total',6,'',''),(6,6,1,'sq_feet',6,'',''),(7,7,1,'gated_community_yn',1,'',''),(8,8,1,'year_built',6,'',''),(9,9,1,'area',8,'',''),(10,10,1,'street_number',9,'',''),(11,11,1,'street_name',9,'',''),(12,12,1,'street_suffix',9,'',''),(13,13,1,'city',9,'',''),(14,14,1,'zip',9,'',''),(15,15,1,'county',9,'',''),(16,16,1,'legal_subdivision',9,'',''),(17,16,1,'common_subdivision',9,'','OR'),(18,17,1,'waterfront_yn',1,'',''),(19,18,1,'navig_waterfront',1,'',''),(20,19,2,'waterfront_description',8,'%Oceanfront%',''),(21,20,1,'foreclosure',1,'',''),(22,21,1,'short_sale',1,'',''),(23,23,1,'garage',1,'',''),(24,24,1,'golf_community',8,'',''),(25,18,2,'waterfront_description',8,'%6%',''),(26,25,2,'pool',8,'%In Ground%',''),(27,26,1,'new_construction_yn',1,'',''),(28,22,1,'elementary_school',9,'',''),(29,22,1,'middle_school',9,'','OR'),(30,22,1,'high_school',9,'','OR'),(31,27,1,'mls_property_type_id',1,'','');



/*Table structure for table `terms` */



DROP TABLE IF EXISTS `terms`;



CREATE TABLE `terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_ma` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(63) NOT NULL,
  `display_name` varchar(63) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `terms` */



insert  into `terms`(`id`,`status_ma`,`name`,`display_name`) values (1,1,'status','Status'),(2,1,'price_min','Price Min'),(3,1,'price_max','Price Max'),(4,1,'bedrooms','Bedrooms'),(5,1,'baths_total','Baths'),(6,1,'sq_feet','Sq. Feet'),(7,1,'gated_community','Gated'),(8,1,'year_built','Yr Built'),(9,1,'area','Area'),(10,1,'street_number','Street Number'),(11,1,'street_name','Street Name'),(12,1,'street_suffix','Street Suffix'),(13,1,'city','City'),(14,1,'zip','Zip'),(15,1,'county','County'),(16,1,'neighborhood','Neighborhood'),(17,1,'waterfront','Waterfront'),(18,1,'navig_waterfront','Navig Waterfront'),(19,1,'oceanfront','Oceanfront'),(20,1,'foreclosure','Foreclosure'),(21,1,'short_sale','Short Sale'),(22,1,'school','School'),(23,1,'garage','Garage'),(24,1,'golf_community','Golf Community'),(25,1,'pool','Pool'),(26,1,'new_construction','New Construction'),(27,1,'property_type','Property Type');



/*Table structure for table `timeblocking` */



DROP TABLE IF EXISTS `timeblocking`;



CREATE TABLE `timeblocking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_type_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `timeblock_type_ma` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `assignment_type_id` (`assignment_type_id`) USING BTREE,
  CONSTRAINT `timeblocking_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `timeblocking_ibfk_2` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `timeblocking` */



/*Table structure for table `traffic_tracker` */



DROP TABLE IF EXISTS `traffic_tracker`;



CREATE TABLE `traffic_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `user_agent_type` varchar(63) DEFAULT NULL,
  `user_agent_name` varchar(63) DEFAULT NULL,
  `user_agent_os_name` varchar(63) DEFAULT NULL,
  `home_detail_view_count` int(11) DEFAULT NULL,
  `pageview_cookie` text,
  `domain` varchar(255) DEFAULT NULL,
  `page` text,
  `referrer` text,
  `campaign_source` varchar(255) DEFAULT NULL,
  `campaign_name` varchar(255) DEFAULT NULL,
  `campaign_medium` varchar(255) DEFAULT NULL,
  `campaign_content` varchar(255) DEFAULT NULL,
  `campaign_term` varchar(255) DEFAULT NULL,
  `first_visit` timestamp NULL DEFAULT NULL,
  `previous_visit` timestamp NULL DEFAULT NULL,
  `current_visit_started` timestamp NULL DEFAULT NULL,
  `times_visited` varchar(255) DEFAULT NULL,
  `pages_viewed` varchar(255) DEFAULT NULL,
  `utmz` text,
  `utma` text,
  `utmb` text,
  `utmc` text,
  `ip_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`) USING BTREE,
  KEY `traffic_tracker_ibfk_1` (`contact_id`),
  KEY `traffic_tracker_ibfk_3` (`ip_id`),
  CONSTRAINT `traffic_tracker_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `traffic_tracker_ibfk_2` FOREIGN KEY (`session_id`) REFERENCES `traffic_tracker_sessions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `traffic_tracker_ibfk_3` FOREIGN KEY (`ip_id`) REFERENCES `traffic_tracker_ips` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `traffic_tracker` */



/*Table structure for table `traffic_tracker_ips` */



DROP TABLE IF EXISTS `traffic_tracker_ips`;



CREATE TABLE `traffic_tracker_ips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `traffic_tracker_ips` */



/*Table structure for table `traffic_tracker_sessions` */



DROP TABLE IF EXISTS `traffic_tracker_sessions`;



CREATE TABLE `traffic_tracker_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `traffic_tracker_sessions` */



/*Table structure for table `transaction_closing_cost_items` */



DROP TABLE IF EXISTS `transaction_closing_cost_items`;



CREATE TABLE `transaction_closing_cost_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `name` int(11) DEFAULT NULL,
  `formula` varchar(150) DEFAULT NULL,
  `sort_order` tinyint(2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  CONSTRAINT `transaction_closing_cost_items_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_closing_cost_items_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transaction_closing_cost_items` */



/*Table structure for table `transaction_closing_costs` */



DROP TABLE IF EXISTS `transaction_closing_costs`;



CREATE TABLE `transaction_closing_costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `sales_price` decimal(11,2) DEFAULT NULL,
  `loan_amount` decimal(11,2) DEFAULT NULL,
  `closing_date` date DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `total` decimal(11,2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  CONSTRAINT `transaction_closing_costs_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transaction_closing_costs` */



/*Table structure for table `transaction_cms_lu` */



DROP TABLE IF EXISTS `transaction_cms_lu`;



CREATE TABLE `transaction_cms_lu` (
  `transaction_id` int(11) DEFAULT NULL,
  `cms_id` int(11) DEFAULT NULL,
  UNIQUE KEY `UNIQUE` (`transaction_id`,`cms_id`),
  KEY `cms_id` (`cms_id`),
  CONSTRAINT `cms_id` FOREIGN KEY (`cms_id`) REFERENCES `cms_contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_cms_lu_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transaction_cms_lu` */



/*Table structure for table `transaction_field_account_lu` */



DROP TABLE IF EXISTS `transaction_field_account_lu`;



CREATE TABLE `transaction_field_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `transaction_field_id` int(11) DEFAULT NULL,
  `name_override` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_field_id` (`transaction_field_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `transaction_field_account_lu_ibfk_1` FOREIGN KEY (`transaction_field_id`) REFERENCES `transaction_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_field_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transaction_field_account_lu` */



/*Table structure for table `transaction_field_values` */



DROP TABLE IF EXISTS `transaction_field_values`;



CREATE TABLE `transaction_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_field_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `value` varchar(750) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_field_id` (`transaction_field_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  CONSTRAINT `transaction_field_values_ibfk_1` FOREIGN KEY (`transaction_field_id`) REFERENCES `transaction_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_field_values_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transaction_field_values` */



/*Table structure for table `transaction_fields` */



DROP TABLE IF EXISTS `transaction_fields`;



CREATE TABLE `transaction_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) DEFAULT NULL COMMENT 'Buyer Seller only',
  `name` varchar(100) DEFAULT NULL,
  `label` varchar(150) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `sort_order` tinyint(4) DEFAULT NULL COMMENT 'may not be needed if views are custom',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `data_type_id` (`data_type_id`),
  KEY `name` (`name`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  CONSTRAINT `transaction_fields_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_fields_ibfk_2` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transaction_fields` */



insert  into `transaction_fields`(`id`,`component_type_id`,`name`,`label`,`data_type_id`,`sort_order`,`is_deleted`) values (1,2,'price_min','Price',4,2,0),(2,2,'price_max','',4,2,0),(3,2,'motivation','Motivation',1,3,0),(4,2,'if_not_buy','If Not Buy',1,3,0),(5,2,'other_agent','Agent',7,4,0),(6,2,'other_agent_signed','',7,5,0),(7,2,'trial_close','Trial Close',1,10,0),(8,2,'features','Features',1,11,0),(9,2,'location','Location',1,11,0),(10,2,'bedrooms','Bedrooms',1,11,0),(11,2,'baths','Baths',1,12,0),(12,2,'sq_feet','SF',1,13,0),(13,2,'target_date','Target Date',1,14,0),(14,2,'best_time_to_look','Best time to look',1,15,0),(15,2,'own_rent','Own/Rent',1,16,0),(16,2,'home_to_sell','Home to sell?',7,17,0),(17,2,'home_to_sell_addr','',1,18,0),(18,2,'prequal_date','Prequal',2,19,0),(19,2,'prequal_amount','',4,20,0),(20,2,'lender','Lender',1,22,0),(21,2,'sale_type_ma','Sale Type',4,39,0),(22,3,'motivation','What\'s prompting your move? What\'s your main reason for selling?',1,1,0),(23,3,'where_moving_to','Where are you moving to? Why?',1,2,0),(24,3,'target_date','How soon do you need to be there?',1,3,0),(25,3,'if_not_sell','What\'ll happen if your house doesn\'t sell?',1,4,0),(26,3,'idea_of_sales','Do you have any idea of what homes are selling for in your area?',7,5,0),(27,3,'idea_of_value','Do you have your own thoughts on what your home is worth?',1,6,0),(28,3,'must_get_price','Do you have a price you MUST get for the property? Why?',1,8,0),(29,3,'mortgage_amount_1','How much do you owe on the house? 1st Mtg:',1,10,0),(30,3,'mortgage_amount_2','2nd Mtg:',1,11,0),(31,3,'payments_current','Are you current on your payments?',1,12,0),(32,3,'is_sole_owner','Are you the sole owner of the house?',1,13,0),(33,3,'ownership_comment','',1,14,0),(34,3,'relocation_pkg','Relocation Pkg?',7,16,0),(35,3,'relocation_company','',1,17,0),(36,3,'other_agent','Are you working with an agent?',7,18,0),(37,3,'other_agent_signed','Signed an agreement with them?',1,19,0),(38,3,'agent_quality_1','What are the top 3 things you\'re looking for in a real estate agent?',1,20,0),(39,3,'agent_quality_2','',1,21,0),(40,3,'agent_quality_3','',1,22,0),(41,3,'interview_others','Are you interviewing other agents?',7,23,0),(42,3,'interview_who','When are the other interviews? With who? Company?',7,24,0),(43,3,'interview_last','Can we be last?',1,25,0),(44,3,'concerns','Do you have any other major concerns about selling your home?',1,26,0),(45,3,'features','Features',1,41,0),(46,3,'area','Area/Subdivision',1,40,0),(47,3,'bedrooms','Bedrooms',5,42,0),(48,3,'baths','Baths',6,43,0),(49,3,'half_baths','Half Baths',6,43,0),(50,3,'sq_feet','Sq Feet',5,44,0),(51,3,'pool','Pool',7,45,0),(52,3,'waterfront','Waterfront',7,46,0),(53,3,'expired_mls_number','Expired MLS #',1,33,0),(54,3,'expired_date','Expired Date',2,33,0),(55,3,'expired_price','Expired Price',4,33,0),(56,3,'price_original','Original Price',4,38,0),(57,3,'price_current','Current Price',4,39,0),(58,3,'sale_type_ma','Sale Type',4,39,0),(59,3,'mls_number','MLS #',1,33,0),(60,3,'ivr_number','IVR #',1,34,0),(61,3,'ivr_extension','Ext.',1,35,0),(62,3,'lockbox_number','Lockbox #',1,35,0),(63,3,'lockbox_status_ma','Lockbox Status',5,35,0),(64,3,'upfront_fee','Upfront Fee',4,41,0),(65,3,'showing_instructions','Showing Instructions',1,41,0),(66,3,'listing_expire_date','Listing Expire Date',2,41,0),(67,3,'listing_date','Listing Date',2,41,0),(68,3,'trial_close','Trial Close',1,10,0),(69,3,'cancel_date','Cancel Date',2,47,0),(70,3,'price_recommend','Recommend Price',4,48,0),(71,3,'appraised_value','Appraised Value',4,49,0),(72,3,'appraised_date','Appraised Date',2,50,0);



/*Table structure for table `transaction_status` */



DROP TABLE IF EXISTS `transaction_status`;



CREATE TABLE `transaction_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `is_core` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



/*Data for the table `transaction_status` */



insert  into `transaction_status`(`id`,`name`,`is_core`,`is_deleted`) values (1,'Lead Pool',1,0),(2,'Hot \"A\" Lead',1,0),(3,'\"B\" Lead',1,0),(4,'\"C\" Lead',1,0),(5,'Nurturing',1,0),(6,'STM Not Used...',1,0),(7,'Coming Soon',1,0),(8,'Listed',1,0),(9,'Under Contract',1,0),(10,'Closed',1,0),(11,'Expired',1,0),(12,'Cancelled',1,0),(13,'Trash',1,0);



/*Table structure for table `transaction_status_account_lu` */



DROP TABLE IF EXISTS `transaction_status_account_lu`;



CREATE TABLE `transaction_status_account_lu` (
  `account_id` int(11) NOT NULL DEFAULT '0',
  `component_type_id` int(11) NOT NULL DEFAULT '0',
  `transaction_status_id` int(11) NOT NULL DEFAULT '0',
  `override_name` varchar(63) DEFAULT NULL,
  PRIMARY KEY (`account_id`,`component_type_id`,`transaction_status_id`),
  KEY `transaction_status_id` (`transaction_status_id`),
  KEY `component_type_id` (`component_type_id`),
  CONSTRAINT `transaction_status_account_lu_ibfk_1` FOREIGN KEY (`transaction_status_id`) REFERENCES `transaction_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_status_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_status_account_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transaction_status_account_lu` */



insert  into `transaction_status_account_lu`(`account_id`,`component_type_id`,`transaction_status_id`,`override_name`) values (1,2,1,''),(1,2,2,''),(1,2,3,''),(1,2,4,''),(1,2,5,''),(1,2,9,''),(1,2,10,''),(1,2,12,''),(1,2,13,NULL),(1,3,1,''),(1,3,2,''),(1,3,3,''),(1,3,4,''),(1,3,5,NULL),(1,3,7,NULL),(1,3,8,NULL),(1,3,9,NULL),(1,3,10,NULL),(1,3,11,NULL),(1,3,12,NULL),(1,3,13,NULL),(1,4,8,''),(1,4,9,''),(1,4,10,''),(1,4,11,''),(1,4,12,'');



/*Table structure for table `transactions` */



DROP TABLE IF EXISTS `transactions`;



CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `component_type_id` int(11) NOT NULL COMMENT 'Seller or Buyer',
  `transaction_status_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `internal_referrer_id` int(11) DEFAULT NULL,
  `internal_referrer_assignment_type_id` int(11) DEFAULT NULL,
  `mls_property_type_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `notes` varchar(750) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `source_id` (`source_id`),
  KEY `transaction_status_id` (`transaction_status_id`),
  KEY `added_by` (`added_by`),
  KEY `address_id` (`address_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `is_deleted` (`is_deleted`) USING BTREE,
  KEY `internal_referrer_id` (`internal_referrer_id`),
  KEY `internal_referrer_assignment_type_id` (`internal_referrer_assignment_type_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_10` FOREIGN KEY (`internal_referrer_assignment_type_id`) REFERENCES `assignment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`transaction_status_id`) REFERENCES `transaction_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_5` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_7` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_8` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_9` FOREIGN KEY (`internal_referrer_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transactions_ibfk_11` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `transactions` */



--
-- Table structure for table `transaction_tags`
--

CREATE TABLE IF NOT EXISTS `transaction_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT '',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`account_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB;

--
-- Constraints for table `transaction_tags`
--
ALTER TABLE `transaction_tags`
    ADD CONSTRAINT `transaction_tags_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `transaction_tags_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `transaction_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Table structure for table `transaction_tag_lu`
--

CREATE TABLE IF NOT EXISTS `transaction_tag_lu` (
  `transaction_id` int(11) NOT NULL DEFAULT '0',
  `transaction_tag_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_id`,`transaction_tag_id`),
  KEY `transaction_tag_id` (`transaction_tag_id`)
) ENGINE=InnoDB;
--
-- Constraints for table `transaction_tag_lu`
--
ALTER TABLE `transaction_tag_lu`
  ADD CONSTRAINT `transaction_tag_lu_ibfk_2` FOREIGN KEY (`transaction_tag_id`) REFERENCES `transaction_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_tag_lu_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


/*Table structure for table `viewed_pages` */



DROP TABLE IF EXISTS `viewed_pages`;



CREATE TABLE `viewed_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `page_type_ma` tinyint(1) DEFAULT NULL,
  `listing_id` varchar(50) DEFAULT NULL,
  `url` varchar(127) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `page_type_ma` (`page_type_ma`) USING BTREE,
  KEY `listing_id` (`listing_id`) USING BTREE,
  CONSTRAINT `viewed_pages_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;



/*Data for the table `viewed_pages` */



/*Table structure for table `contacts_info` */



DROP TABLE IF EXISTS `contacts_info`;



/*!50001 DROP VIEW IF EXISTS `contacts_info` */;

/*!50001 DROP TABLE IF EXISTS `contacts_info` */;


/*!50001 CREATE TABLE  `contacts_info`(
 `contact_id` int(11) ,
 `first_name` varchar(50) ,
 `last_name` varchar(50) ,
 `email` varchar(127) ,
 `phone_1` varchar(50) ,
 `phone_2` varchar(50) ,
 `phone_3` varchar(50) ,
 `added` timestamp ,
 `last_login` datetime
)*/;


/*Table structure for table `contacts_primary_emails` */



DROP TABLE IF EXISTS `contacts_primary_emails`;



/*!50001 DROP VIEW IF EXISTS `contacts_primary_emails` */;

/*!50001 DROP TABLE IF EXISTS `contacts_primary_emails` */;


/*!50001 CREATE TABLE  `contacts_primary_emails`(
 `contact_id` int(11) ,
 `first_name` varchar(50) ,
 `last_name` varchar(50) ,
 `added` timestamp ,
 `email` varchar(127)
)*/;


/*Table structure for table `contacts_primary_phones` */



DROP TABLE IF EXISTS `contacts_primary_phones`;



/*!50001 DROP VIEW IF EXISTS `contacts_primary_phones` */;

/*!50001 DROP TABLE IF EXISTS `contacts_primary_phones` */;


/*!50001 CREATE TABLE  `contacts_primary_phones`(
 `contact_id` int(11) ,
 `phone` varchar(50)
)*/;


/*Table structure for table `sendable_emails` */



DROP TABLE IF EXISTS `sendable_emails`;



/*!50001 DROP VIEW IF EXISTS `sendable_emails` */;

/*!50001 DROP TABLE IF EXISTS `sendable_emails` */;


/*!50001 CREATE TABLE  `sendable_emails`(
 `id` int(11) ,
 `first_name` varchar(50) ,
 `last_name` varchar(50) ,
 `added` timestamp ,
 `email` varchar(127)
)*/;


/*Table structure for table `transaction_contacts_buyers` */



DROP TABLE IF EXISTS `transaction_contacts_buyers`;



/*!50001 DROP VIEW IF EXISTS `transaction_contacts_buyers` */;

/*!50001 DROP TABLE IF EXISTS `transaction_contacts_buyers` */;


/*!50001 CREATE TABLE  `transaction_contacts_buyers`(
 `tranasaction_id` int(11) ,
 `component_type_id` int(11) ,
 `transaction_status_id` int(11) ,
 `contact_id` int(11) ,
 `first_name` varchar(50) ,
 `last_name` varchar(50) ,
 `address` varchar(127) ,
 `city` varchar(50) ,
 `state` varchar(2) ,
 `zip` varchar(5) ,
 `notes` varchar(750) ,
 `phone1` varchar(50) ,
 `phone2` varchar(50) ,
 `phone3` varchar(50) ,
 `updated` timestamp ,
 `added` datetime
)*/;


/*Table structure for table `transaction_contacts_sellers` */



DROP TABLE IF EXISTS `transaction_contacts_sellers`;



/*!50001 DROP VIEW IF EXISTS `transaction_contacts_sellers` */;

/*!50001 DROP TABLE IF EXISTS `transaction_contacts_sellers` */;


/*!50001 CREATE TABLE  `transaction_contacts_sellers`(
 `tranasaction_id` int(11) ,
 `component_type_id` int(11) ,
 `transaction_status_id` int(11) ,
 `contact_id` int(11) ,
 `first_name` varchar(50) ,
 `last_name` varchar(50) ,
 `address` varchar(127) ,
 `city` varchar(50) ,
 `state` varchar(2) ,
 `zip` varchar(5) ,
 `notes` varchar(750) ,
 `phone1` varchar(50) ,
 `phone2` varchar(50) ,
 `phone3` varchar(50) ,
 `updated` timestamp ,
 `added` datetime
)*/;


/*Table structure for table `transactions_status_a_b_c_nurturing` */



DROP TABLE IF EXISTS `transactions_status_a_b_c_nurturing`;



/*!50001 DROP VIEW IF EXISTS `transactions_status_a_b_c_nurturing` */;

/*!50001 DROP TABLE IF EXISTS `transactions_status_a_b_c_nurturing` */;


/*!50001 CREATE TABLE  `transactions_status_a_b_c_nurturing`(
 `transaction_id` int(11) ,
 `contact_id` int(11) ,
 `transaction_type` varchar(6) ,
 `email` varchar(127) ,
 `added` timestamp ,
 `last_login` timestamp
)*/;


/*View structure for view contacts_info */



/*!50001 DROP TABLE IF EXISTS `contacts_info` */;

/*!50001 DROP VIEW IF EXISTS `contacts_info` */;



/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `contacts_info` AS (select `contact`.`id` AS `contact_id`,`contact`.`first_name` AS `first_name`,`contact`.`last_name` AS `last_name`,`primary_email`.`email` AS `email`,`primary_phone`.`phone` AS `phone_1`,(select `phone2`.`phone` from `phones` `phone2` where (`phone2`.`contact_id` = `contact`.`id`) limit 2,1) AS `phone_2`,(select `phone3`.`phone` from `phones` `phone3` where (`phone3`.`contact_id` = `contact`.`id`) limit 3,1) AS `phone_3`,`contact`.`added` AS `added`,`contact`.`last_login` AS `last_login` from ((`contacts` `contact` join `contacts_primary_emails` `primary_email` on((`primary_email`.`contact_id` = `contact`.`id`))) join `contacts_primary_phones` `primary_phone` on((`primary_phone`.`contact_id` = `contact`.`id`))) where (`contact`.`is_deleted` <> 1)) */;



/*View structure for view contacts_primary_emails */



/*!50001 DROP TABLE IF EXISTS `contacts_primary_emails` */;

/*!50001 DROP VIEW IF EXISTS `contacts_primary_emails` */;



/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `contacts_primary_emails` AS (select `contact`.`id` AS `contact_id`,`contact`.`first_name` AS `first_name`,`contact`.`last_name` AS `last_name`,`contact`.`added` AS `added`,if((`primary_email`.`email` is not null),`primary_email`.`email`,`email`.`email`) AS `email` from ((`contacts` `contact` left join `emails` `email` on((`email`.`id` = (select `email`.`id` from `emails` `email` where ((`email`.`contact_id` = `contact`.`id`) and (`email`.`is_deleted` <> 1) and (`email`.`is_primary` <> 1)) limit 1)))) left join `emails` `primary_email` on(((`primary_email`.`contact_id` = `contact`.`id`) and (`primary_email`.`is_primary` = 1) and (`primary_email`.`is_deleted` <> 1)))) where ((`primary_email`.`email` is not null) or (`email`.`email` is not null)) group by `contact`.`id`) */;



/*View structure for view contacts_primary_phones */



/*!50001 DROP TABLE IF EXISTS `contacts_primary_phones` */;

/*!50001 DROP VIEW IF EXISTS `contacts_primary_phones` */;



/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `contacts_primary_phones` AS (select `contact`.`id` AS `contact_id`,if((`primary_phone`.`phone` is not null),`primary_phone`.`phone`,`phone`.`phone`) AS `phone` from ((`contacts` `contact` left join `phones` `phone` on((`phone`.`id` = (select `phone`.`id` from `phones` `phone` where ((`phone`.`contact_id` = `contact`.`id`) and (`phone`.`is_deleted` <> 1) and (`phone`.`is_primary` <> 1)) limit 1)))) left join `phones` `primary_phone` on(((`primary_phone`.`contact_id` = `contact`.`id`) and (`primary_phone`.`is_primary` = 1) and (`primary_phone`.`is_deleted` <> 1)))) where ((`primary_phone`.`phone` is not null) or (`phone`.`phone` is not null)) group by `contact`.`id`) */;



/*View structure for view sendable_emails */



/*!50001 DROP TABLE IF EXISTS `sendable_emails` */;

/*!50001 DROP VIEW IF EXISTS `sendable_emails` */;



/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `sendable_emails` AS (select `contact`.`id` AS `id`,`contact`.`first_name` AS `first_name`,`contact`.`last_name` AS `last_name`,`contact`.`added` AS `added`,`email`.`email` AS `email` from (`emails` `email` join `contacts` `contact` on((`contact`.`id` = `email`.`contact_id`))) where (`email`.`email_status_id` not in (5,6,7))) */;



/*View structure for view transaction_contacts_buyers */



/*!50001 DROP TABLE IF EXISTS `transaction_contacts_buyers` */;

/*!50001 DROP VIEW IF EXISTS `transaction_contacts_buyers` */;



/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `transaction_contacts_buyers` AS select `transactions`.`id` AS `tranasaction_id`,`transactions`.`component_type_id` AS `component_type_id`,`transactions`.`transaction_status_id` AS `transaction_status_id`,`transactions`.`contact_id` AS `contact_id`,`contacts`.`first_name` AS `first_name`,`contacts`.`last_name` AS `last_name`,`addresses`.`address` AS `address`,`addresses`.`city` AS `city`,stm_hq.`address_states`.`short_name` AS `state`,`addresses`.`zip` AS `zip`,`transactions`.`notes` AS `notes`,(select `phones`.`phone` from `phones` where (`phones`.`contact_id` = `transactions`.`contact_id`) limit 1) AS `phone1`,(select `phones`.`phone` from `phones` where (`phones`.`contact_id` = `transactions`.`contact_id`) limit 2,1) AS `phone2`,(select `phones`.`phone` from `phones` where (`phones`.`contact_id` = `transactions`.`contact_id`) limit 3,1) AS `phone3`,`transactions`.`updated` AS `updated`,`transactions`.`added` AS `added` from ((((`transactions` left join `contacts` on((`transactions`.`contact_id` = `contacts`.`id`))) left join `addresses` on((`transactions`.`address_id` = `addresses`.`id`))) left join stm_hq.`address_states` on((`addresses`.`state_id` = stm_hq.`address_states`.`id`))) left join `phones` on((`contacts`.`id` = `phones`.`contact_id`))) where ((`transactions`.`transaction_status_id` in (1,2,3,4,5)) and (`transactions`.`component_type_id` = 2)) group by `transactions`.`id` desc limit 2500 */;



/*View structure for view transaction_contacts_sellers */



/*!50001 DROP TABLE IF EXISTS `transaction_contacts_sellers` */;

/*!50001 DROP VIEW IF EXISTS `transaction_contacts_sellers` */;



/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `transaction_contacts_sellers` AS select `transactions`.`id` AS `tranasaction_id`,`transactions`.`component_type_id` AS `component_type_id`,`transactions`.`transaction_status_id` AS `transaction_status_id`,`transactions`.`contact_id` AS `contact_id`,`contacts`.`first_name` AS `first_name`,`contacts`.`last_name` AS `last_name`,`addresses`.`address` AS `address`,`addresses`.`city` AS `city`,stm_hq.`address_states`.`short_name` AS `state`,`addresses`.`zip` AS `zip`,`transactions`.`notes` AS `notes`,(select `phones`.`phone` from `phones` where (`phones`.`contact_id` = `transactions`.`contact_id`) limit 1) AS `phone1`,(select `phones`.`phone` from `phones` where (`phones`.`contact_id` = `transactions`.`contact_id`) limit 2,1) AS `phone2`,(select `phones`.`phone` from `phones` where (`phones`.`contact_id` = `transactions`.`contact_id`) limit 3,1) AS `phone3`,`transactions`.`updated` AS `updated`,`transactions`.`added` AS `added` from ((((`transactions` left join `contacts` on((`transactions`.`contact_id` = `contacts`.`id`))) left join `addresses` on((`transactions`.`address_id` = `addresses`.`id`))) left join stm_hq.`address_states` on((`addresses`.`state_id` = stm_hq.`address_states`.`id`))) left join `phones` on((`contacts`.`id` = `phones`.`contact_id`))) where ((`transactions`.`transaction_status_id` in (1,2,3,4,5)) and (`transactions`.`component_type_id` = 3)) group by `transactions`.`id` desc limit 2500 */;



/*View structure for view transactions_status_a_b_c_nurturing */



/*!50001 DROP TABLE IF EXISTS `transactions_status_a_b_c_nurturing` */;

/*!50001 DROP VIEW IF EXISTS `transactions_status_a_b_c_nurturing` */;



/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `transactions_status_a_b_c_nurturing` AS (select `transaction`.`id` AS `transaction_id`,`contact`.`id` AS `contact_id`,if((`transaction`.`component_type_id` = 2),'buyer','seller') AS `transaction_type`,`email`.`email` AS `email`,`contact`.`added` AS `added`,`contact`.`last_login` AS `last_login` from ((`transactions` `transaction` join `contacts` `contact` on((`transaction`.`contact_id` = `contact`.`id`))) join `emails` `email` on((`contact`.`id` = `email`.`contact_id`))) where ((`transaction`.`transaction_status_id` in (2,3,4,5)) and (`transaction`.`component_type_id` in (2,3,4)))) */;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;

/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
SET FOREIGN_KEY_CHECKS=1;
