/*
SQLyog Community v10.2 
MySQL - 5.5.24-0ubuntu0.12.04.1-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('admin','Seize the Market Administrator','2','STM Admin',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('agent','Agent','2','Traditional agent working Buyers & Sellers',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('buyerAgent','Buyer Agent','2','Buyer Specialist Role',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('buyerIsa','Buyer ISA','2','Buyer Inside Sales',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('clientCare','Client Care','2','Client Care',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('closingManager','Closing Manager','2','Closing Manager',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('default','Default','2','Default Permissions','',NULL);
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('inactive','Inactive','2',NULL,NULL,NULL);
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('leadBuyeragent','Lead Buyer Agent','2','Lead Buyer Specialist',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('leadListingagent','Lead Listing Agent','2','Lead Listing Specialist',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('lender','Lender','2','Lender Level Access',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('listingAgent','Listing Agent','2','Listing Agent Specialist Role',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('listingManager','Listing Manager','2','Lisint Manager',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('owner','Owner','2','Owner of Account - Full Access to Account',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('salesManager','Sales Manager','2','Sales Manager',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('sellerIsa','Seller ISA','2','Seller Inside Sales',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('staff','Staff','2','Staff Member of Owner',NULL,'N;');
insert into `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) values('virtualAsst','Virtual Assistant','2','Virtual Assistant',NULL,'N;');
