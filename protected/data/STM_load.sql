-- DROP DATABASE `stm_app`;
-- CREATE DATABASE `stm_app`;

SET FOREIGN_KEY_CHECKS = 0;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Empty out all tables in db
--
TRUNCATE `accounts`;
TRUNCATE `account_contact_lu`;
TRUNCATE `action_plans`;
TRUNCATE `action_plan_applied_lu`;
TRUNCATE `action_plan_items`;
TRUNCATE `active_guests`;
TRUNCATE `active_users`;
TRUNCATE `activity_log`;
TRUNCATE `addresses`;
TRUNCATE `address_company_lu`;
TRUNCATE `address_contact_lu`;
TRUNCATE `address_states`;
TRUNCATE `assignments`;
TRUNCATE `assignment_types`;
TRUNCATE `assignment_type_account_lu`;
TRUNCATE `auth_assignment`;
TRUNCATE `auth_item`;
TRUNCATE `auth_item_child`;
TRUNCATE `closings`;
TRUNCATE `cms_contents`;
TRUNCATE `cms_content_tag_lu`;
TRUNCATE `cms_content_widget_lu`;
TRUNCATE `cms_tags`;
TRUNCATE `cms_videos`;
TRUNCATE `cms_widgets`;
TRUNCATE `cms_widget_groups`;
TRUNCATE `cms_widget_group_lu`;
TRUNCATE `companies`;
TRUNCATE `company_contact_lu`;
TRUNCATE `company_types`;
TRUNCATE `company_type_lu`;
TRUNCATE `component_types`;
TRUNCATE `consultations`;
TRUNCATE `contacts`;
TRUNCATE `contact_attributes`;
TRUNCATE `contact_attribute_account_lu`;
TRUNCATE `contact_attribute_values`;
TRUNCATE `contact_types`;
TRUNCATE `contact_type_lu`;
TRUNCATE `craigslist_templates`;
TRUNCATE `craigslist_template_groups`;
TRUNCATE `custom_fields`;
TRUNCATE `custom_field_groups`;
TRUNCATE `custom_field_values`;
TRUNCATE `data_types`;
TRUNCATE `documents`;
TRUNCATE `document_types`;
TRUNCATE `domains`;
TRUNCATE `emails`;
TRUNCATE `email_messages`;
TRUNCATE `email_opt_status`;
TRUNCATE `email_templates`;
TRUNCATE `featured_areas`;
TRUNCATE `featured_area_types`;
TRUNCATE `featured_area_type_lu`;
TRUNCATE `forms`;
TRUNCATE `form_account_lu`;
TRUNCATE `form_fields`;
TRUNCATE `form_field_lu`;
TRUNCATE `form_submissions`;
TRUNCATE `form_submission_values`;
TRUNCATE `form_views`;
TRUNCATE `goals`;
TRUNCATE `history_log`;
TRUNCATE `homes_shown`;
TRUNCATE `leads`;
TRUNCATE `lead_round_robbin_contact_lu`;
TRUNCATE `lead_routes`;
TRUNCATE `lead_route_rules`;
TRUNCATE `lead_shifts`;
TRUNCATE `login_log`;
TRUNCATE `market_trends`;
TRUNCATE `market_trend_data`;
TRUNCATE `mls_boards`;
TRUNCATE `mls_board_address_lu`;
TRUNCATE `mls_board_contact_lu`;
TRUNCATE `mls_error_log`;
TRUNCATE `mls_features`;
TRUNCATE `mls_feature_groups`;
TRUNCATE `mls_feed_log`;
TRUNCATE `mls_field_maps`;
TRUNCATE `mls_property_types`;
TRUNCATE `mls_property_type_board_lu`;
TRUNCATE `mobile_gateways`;
TRUNCATE `mobile_gateway_phone_lu`;
TRUNCATE `phones`;
TRUNCATE `projects`;
TRUNCATE `project_items`;
TRUNCATE `referrals`;
TRUNCATE `saved_homes`;
TRUNCATE `saved_home_searches`;
TRUNCATE `settings`;
TRUNCATE `setting_account_values`;
TRUNCATE `setting_configs`;
TRUNCATE `setting_contact_values`;
TRUNCATE `setting_options`;
TRUNCATE `sources`;
TRUNCATE `splash_screens`;
TRUNCATE `stm_addresses`;
TRUNCATE `stm_contacts`;
TRUNCATE `stm_contact_phone_lu`;
TRUNCATE `stm_emails`;
TRUNCATE `stm_history_log`;
TRUNCATE `stm_phones`;
TRUNCATE `tasks`;
TRUNCATE `task_email_template_lu`;
TRUNCATE `task_types`;
TRUNCATE `task_type_component_lu`;
TRUNCATE `timeblocking`;
TRUNCATE `transactions`;
TRUNCATE `transaction_address_lu`;
TRUNCATE `transaction_closing_costs`;
TRUNCATE `transaction_closing_cost_items`;
TRUNCATE `transaction_fields`;
TRUNCATE `transaction_field_account_lu`;
TRUNCATE `transaction_field_values`;
TRUNCATE `transaction_status`;
TRUNCATE `transaction_status_account_lu`;
TRUNCATE `viewed_pages`;

-- SET FOREIGN_KEY_CHECKS=1;

--
-- Dumping data for table `AuthItem`
--
INSERT INTO `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 'Seize the Market Administrator', 2, 'STM Admin', NULL, 'N;'),
('owner', 'Owner', 2, 'Owner of Account - Full Access to Account', NULL, 'N;'),
('salesManager', 'Sales Manager', 2, 'Sales Manager', NULL, 'N;'),
('listingManager', 'Listing Manager', 2, 'Lisint Manager', NULL, 'N;'),
('closingManager', 'Closing Manager', 2, 'Closing Manager', NULL, 'N;'),
('clientCare', 'Client Care', 2, 'Client Care', NULL, 'N;'),
('leadListingagent', 'Lead Listing Agent', 2, 'Lead Listing Specialist', NULL, 'N;'),
('leadBuyeragent', 'Lead Buyer Agent', 2, 'Lead Buyer Specialist', NULL, 'N;'),
('staff', 'Staff', 2, 'Staff Member of Owner', NULL, 'N;'),
('agent', 'Agent', 2, 'Traditional agent working Buyers & Sellers', NULL, 'N;'),
('lender', 'Lender', 2, 'Lender Level Access', NULL, 'N;'),
('virtualAsst', 'Virtual Assistant', 2, 'Virtual Assistant', NULL, 'N;'),
('buyerAgent', 'Buyer Agent', 2, 'Buyer Specialist Role', NULL, 'N;'),
('listingAgent', 'Listing Agent', 2, 'Listing Agent Specialist Role', NULL, 'N;'),
('buyerIsa', 'Buyer ISA', 2, 'Buyer Inside Sales', NULL, 'N;'),
('sellerIsa', 'Seller ISA', 2, 'Seller Inside Sales', NULL, 'N;');

--
-- Dumping data for table `AuthItemChild`
--
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('staff', 'salesManager'),
('staff', 'listingManager'),
('staff', 'closingManager'),
('staff', 'clientCare'),
('agent', 'buyerAgent'),
('agent', 'listingAgent'),
('agent', 'leadBuyerAgent'),
('agent', 'leadListingAgent'),
('agent', 'virtualAsst');

--
-- Dumping data for table `AuthAssignment`
--

INSERT INTO `auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('owner', '1', NULL, 'N;'),
('owner', '3', NULL, 'N;'),
('owner', '5', NULL, 'N;'),
('owner', '7', NULL, 'N;'),
('owner', '14', NULL, 'N;'),
('owner', '20', NULL, 'N;'),
('owner', '21', NULL, 'N;'),
('owner', '22', NULL, 'N;'),
('owner', '31', NULL, 'N;'),
('owner', '35', NULL, 'N;'),
('owner', '36', NULL, 'N;'),
('owner', '37', NULL, 'N;'),
('owner', '38', NULL, 'N;');

--
-- Dumping data for table `account_contact_lu`
--
INSERT INTO `account_contact_lu` (`contact_id`, `account_id`, `contact_type_ma`) VALUES
('1', '2', '1');
-- ('2', '?', '1'),
-- ('9', '?', '1');

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `mls_board_id`, `account_status_ma`, `agent_board_num`, `feed_username`, `feed_password`, `contract_date`, `go_live_date`, `notes`, `api_key`, `is_deleted`) VALUES
(0, NULL, 1, 'Global Account', NULL, NULL, NULL, NULL, 'Global Account', 'globalkey', 0),
(1, 1, 1, '15245', NULL, NULL, '2012-04-23', '2012-08-01', 'Seize the Market', 'stmkey', 0),
(2, 2, 1, '15245', 'nef.rets.lee-15245', 'risus-oidea80', '2012-04-23', '2012-08-01', 'CLee', 'cleekey', 0),
(3, 3, 1, 'TBD', NULL, NULL, '2012-08-15', '2012-09-01', 'Gene', 'genekey', 0);


--
-- Dumping data for table `settings`
--
INSERT INTO `settings` (`setting_type_ma`, `name`, `label`) VALUES
('1', 'office_name', 'Name'),
('1', 'office_phone', 'Phone'),
('1', 'office_fax', 'Fax'),
('1', 'office_email', 'Email'),
('1', 'office_address', 'Address'),
('1', 'office_city', 'City'),
('1', 'office_state', 'State'),
('1', 'office_zip', 'Zip'),
('1', 'anniversary_date', 'Anniversary'),
('1', 'end_of_day', 'End of Day'),
('1', 'views_max', 'Home Views Max'),
('1', 'primary_market_trend_id', 'Primary Market Trend'),
('1', 'reponse_time_active', 'Active Propsect'),
('1', 'reponse_time_new_lead', 'New Lead Response Time'), -- 15 min from submit, 1 day from last attempt in 1st 10 days, outstanding tasks
('1', 'reponse_time_A', 'A Lead Response Time'), -- once every 1 week, or contact w/in 1 week or set for future
('1', 'reponse_time_B', 'B Lead Response Time'), -- once every 2 weeks, or contact w/in 2 weeks or future task
('1', 'reponse_time_C', 'C Lead Response Time'), -- once every 1 month or contact w/in 1 month or future task
('1', 'sphere_touch_frequency', 'Sphere'), -- once every 1 month or contact w/in 1 month or future task
('2', 'agent_mls_num', 'Agent MLS #'),
('2', 'dashboard', 'Dashboard Layout'),
('2', 'agent_website', 'Agent Website'),
('2', 'profile_photo', 'Photo'),
('2', 'gender', 'Gender'),
('2', 'title', 'Title'),
('2', 'subtitle', 'Subtitle'),
('2', 'designations', 'Designations'),
('2', 'email_signature', 'Email Signature'),
('2', 'cell_number', 'Cell #'),
('2', 'cell_carrier_id', 'Cell Carrier'),
('2', 'bio', 'Personal Bio'),
('2', 'social_facebook', 'Facebook'),
('2', 'social_linkedin', 'LinkedIn'),
('2', 'social_youtube', 'You Tube'),
('2', 'social_twitter', 'Twitter'),
('2', 'social_activerain', 'Active Rain'),
('2', 'social_google', 'Google+'),
('3', 'top_links', 'Top Links'),
('2', 'sticky_notes', 'Sticky Notes'),
('2', 'primary_goal_id', 'Goal'),
('2', 'memorable_moments', 'Memorable Moments'),
('2', 'favorite_quotes', 'Favorite Quotes'),
('2', 'words_describe_self', 'Words to Describe Self'),
('2', 'profile_on_website', 'Profile on Website'),
('1', 'website_footer_contact', 'Website Footer Contact');

--
-- Dumping data for table `setting_configs`
--
INSERT INTO `setting_configs` (`account_id`, `setting_id`) VALUES
('2', '1'),
('2', '2'),
('2', '3'),
('2', '4'),
('2', '5'),
('2', '6'),
('2', '7'),
('2', '8'),
('2', '9'),
('2', '10'),
('2', '11'),
('2', '12'),
('2', '13'),
('2', '29');

--
-- Dumping data for table `setting_accounts`
--
INSERT INTO `setting_account_values`
(`account_id`, `setting_id`, `value`) VALUES
('2', '1', 'Christine Lee Team, Keller Williams Jacksonville Realty'),
('2', '2', '(904) 262-9505'),
('2', '3', '(904) 262-0566'),
('2', '4', 'info@christineleeteam.com'),
('2', '5', '2950 Halcyon Ln #102'),
('2', '6', 'Jacksonville'),
('2', '7', '9'),
('2', '8', '32223'),
('2', '9', '9'),
('2', '10', '21'),
('2', '11', '2'),
('2', '37', '{"0":{"label":"My Tasks","url":"/admin/tasks","sort_order":"1"},"1":{"label":"Calendar","url":"http://calendar.google.com","sort_order":"1"},"2":{"label":"MLS","url":"https:\/\/nefmls.com","sort_order":"2"},"3":{"label":"Proquest","url":"http:\/\/beta.proquest-tech.com","sort_order":"3"},"4":{"label":"Telepro","url":"http:\/\/corcorancoaching.com\/private","sort_order":"4"},"5":{"label":"Zopim","url":"http:\/\/dashboard.zopim.com","sort_order":"5"}}'),
('2', '44', 'Christine Lee Team | (Office) 2950 Halcyon Ln #102, Jacksonville, FL 32223 | (Mailing) 2974 Hartley Road Floor 2, Jacksonville, FL 32257');

--
-- Dumping data for table `setting_contact_values`
--

INSERT INTO `setting_contact_values` (`id`, `contact_id`, `setting_id`, `value`, `updated`, `updated_by`, `is_deleted`) VALUES
(1, 1, 19, '15245', NULL, 1, 0),
(2, 1, 20, 'buyerAgent', NULL, 1, 0),
(3, 1, 21, 'christinelee', NULL, 1, 0),
(4, 1, 22, 'profile_photo.jpg', NULL, 1, 0),
(5, 1, 23, '0', NULL, 1, 0),
(6, 1, 24, 'CEO / Team Leader', NULL, 1, 0),
(7, 1, 25, 'REALTOR?', NULL, 1, 0),
(8, 1, 26, 'Licensed Realtor', NULL, 1, 0),
(9, 1, 27, 'Christine Lee\r\rReal Estate Expert\r\r(904) 262-9505\r\rwww.ChristineLeeTeam.com', NULL, 1, 0),
(10, 1, 28, '9042803315', NULL, 1, 0),
(11, 1, 29, '2', NULL, 1, 0),
(12, 1, 30, 'With a Bachelor degree in Computer Information Systems, Christine''s experience in real estate started as a real estate appraiser and continued to grow as she developed into a mortgage broker, real estate broker and general contractor. She showed her great', NULL, 1, 0),
(13, 1, 38, 'John Smith coming into town Nov 18, job relo, has 2 kids 7, 10<br /><br />testing hopefully this works!<br /><br />', NULL, 1, 0),
(14, 1, 39, '1', NULL, 1, 0),
(15, 1, 40, 'One day I got a call saying that someone was ripping the roof off of a house under contract and the agent thought we had done that. In fact, it was a roofer who started working on the wrong house! The buyer ended up getting a partial new roof.', NULL, 1, 0),
(16, 1, 41, '"Feel the Fear and Do It Anyways!", "Work Smarter Not Harder"', NULL, 1, 0),
(17, 1, 42, 'Creative, Motivated, Aggressive, Resourceful', NULL, 1, 0),
(18, 1, 43, '1', NULL, 1, 0),
(19, 3, 20, 'generalAdmin', NULL, 3, 0),
(20, 3, 24, 'Licensed Closing Manager', NULL, 3, 0),
(21, 3, 25, 'Short Sale Listing Manager', NULL, 3, 0),
(22, 3, 27, 'Thanks,\r\n\r\n\r\n\r\n\r\n\r\nSherri Bankston\r\n\r\nLicensed Closing Manager &\r\n\r\n\r\n\r\nShort Sale Listing Manager\r\n\r\n\r\n\r\n904-262-9393  direct\r\n\r\n904- 262-0566 fax\r\n\r\n\r\n\r\nKeller Williams Jacksonville Realty ', NULL, 3, 0),
(23, 3, 28, '9046518899', NULL, 3, 0),
(24, 3, 29, '4', NULL, 3, 0),
(25, 3, 30, 'Sherri has an amazing eight years experience in real estate as a Closing Manager and is an expert at her role!  With the ever changing real estate market, she has become very knowledgeable with short sales and has taken over the position of Short Sale Lis', NULL, 3, 0),
(26, 3, 40, 'Currently working on my Masters in Business Administration', NULL, 3, 0),
(27, 3, 41, 'I shall pass through this world but once. Any good therefore that I can do, or any kindness that I can show to any human being, let me do it now. Let me not defer not neglect it, for I shall not pass this way again.?   Unknown                           ', NULL, 3, 0),
(28, 3, 42, 'Hard Working, Dependable, and Always willing to help others', NULL, 3, 0),
(29, 3, 43, '1', NULL, 3, 0),
(30, 5, 20, 'generalAdmin', NULL, 5, 0),
(31, 5, 24, 'Client Care Coordinator', NULL, 5, 0),
(32, 5, 26, 'Assistant, Accounting & Payroll', NULL, 5, 0),
(33, 5, 27, 'Grace Harrison\r\rClient Care Coordinator\r\rThe Christine Lee Team\r\r(904) 280-3312', NULL, 5, 0),
(34, 5, 28, '9049458926', NULL, 5, 0),
(35, 5, 29, '1', NULL, 5, 0),
(36, 5, 30, 'Grace is the team''s recently graduated college student. She is currently aspiring to be a commercial airline pilot. Committed to hard work, she gladly manages all of the "behind the scenes" tasks and assists wherever she is needed most.', NULL, 5, 0),
(37, 5, 41, '"Whatever you want to do, do it. There are only so many tomorrows."', NULL, 5, 0),
(38, 5, 42, 'Trustworthy, Caring, and Dependable', NULL, 5, 0),
(39, 5, 43, '1', NULL, 5, 0),
(40, 7, 19, '32499', NULL, 7, 0),
(41, 7, 20, 'buyerAgent', NULL, 7, 0),
(42, 7, 23, '0', NULL, 7, 0),
(43, 7, 24, 'Buyer Specialist', NULL, 7, 0),
(44, 7, 26, 'Realtor', NULL, 7, 0),
(45, 7, 27, 'Thanks,\r\n\r\n\r\n\r\nEric Landry\r\n\r\nSenior Buyer Specialist\r\n\r\n904-536-8702 (cell)\r\n\r\n\r\n\r\nFind out What Your Home is Worth! ? JaxValues.com\r\n\r\nSearch 15,000+ Homes Online! ? Jax-HomeFinder.com\r\n\r\nBuy ~ Sell ~  Invest ~ Build ? ChristineLeeTeam.com\r\n\r\n\r\n\r\nForeclosure List! ? Jackso', NULL, 7, 0),
(46, 7, 28, '9045368702', NULL, 7, 0),
(47, 7, 29, '1', NULL, 7, 0),
(48, 7, 41, 'The truth is incontrovertible. Malice may attack it, ignorance may deride it, but in the end, there it is. -Winston Churchill', NULL, 7, 0),
(49, 7, 42, 'Guniune, Honest, Funny, Hard Working, Handsome! :)', NULL, 7, 0),
(50, 7, 43, '1', NULL, 7, 0),
(51, 14, 20, 'buyerAgent', NULL, 14, 0),
(52, 20, 20, '', NULL, 20, 0),
(53, 20, 24, 'Listing Manager', NULL, 20, 0),
(54, 20, 30, 'With a degree in Business Management combined with her years of experience in Real Estate, Ashley is a integral part of our listing team. She is the listing manager for the team, which means that she has the day to day pulse on the activity for our seller', NULL, 20, 0),
(55, 20, 43, '1', NULL, 20, 0),
(56, 22, 19, '42289', NULL, 22, 0),
(57, 22, 20, 'buyerAgent', NULL, 22, 0),
(58, 22, 21, 'katrina', NULL, 22, 0),
(59, 22, 24, 'Buyer Specialist ', NULL, 22, 0),
(60, 22, 25, 'REALTOR?', NULL, 22, 0),
(61, 22, 26, 'Licensed Realtor', NULL, 22, 0),
(62, 22, 27, 'Katrina Abdullah \r\rReal Estate Expert / Buyer Specialist\r\rDirect: (904) 568-1752\r\rKatrina@ChristineLeeTeam.com\r\r\r\rFind me on Skype - KatrinaSellsJax\r\r\r\rKeller Williams Jacksonville Realty\r\r\r\r"Let''s get you packing!"\r\r\r\r\r\rSELLING?\r\rWhat''s Your Home Really ', NULL, 22, 0),
(63, 22, 28, '9045681752', NULL, 22, 0),
(64, 22, 30, 'Katrina''s strength lies in strong relationships and negotiations. Her knowledge of new construction homes in the Jacksonville and St Johns county area has saved her clients tens of thousands of dollars. Her commitment and focus is finding great homes for ', NULL, 22, 0),
(65, 22, 40, 'Negotiating the price of a newly constructed home for a family saving them over $30,000.  It was so satisfying seeing this young couple, elated with joy that there dream home was now in there budget.', NULL, 22, 0),
(66, 22, 41, '"It''s all about your perspective." ', NULL, 22, 0),
(67, 22, 42, 'Tenacious, Up-Front, Honest, Direct, Passoinate, Considerate', NULL, 22, 0),
(68, 22, 43, '1', NULL, 22, 0),
(69, 31, 23, '0', NULL, 31, 0),
(70, 31, 24, 'Listing Specialist', NULL, 31, 0),
(71, 31, 25, '', NULL, 31, 0),
(72, 31, 28, '9045021565', NULL, 31, 0),
(73, 31, 29, '4', NULL, 31, 0),
(74, 31, 41, 'If you have never failed at anything .... Then you have never done anything worth while- Woody Harrelson', NULL, 31, 0),
(75, 31, 42, 'Sincere, Social ', NULL, 31, 0),
(76, 35, 19, '12663', NULL, 35, 0),
(77, 35, 20, 'listingAgent', NULL, 35, 0),
(78, 35, 24, 'Listing Specialist', NULL, 35, 0),
(79, 35, 27, 'Jackie Behr', NULL, 35, 0),
(80, 35, 28, '9046513116', NULL, 35, 0),
(81, 35, 29, '3', NULL, 35, 0),
(82, 35, 38, '', NULL, 35, 0),
(83, 37, 19, '16936', NULL, 37, 0),
(84, 37, 20, 'buyerAgent', NULL, 37, 0),
(85, 37, 24, 'Buyers Specialist', NULL, 37, 0),
(86, 37, 25, 'Member of FAR ', NULL, 37, 0),
(87, 37, 26, 'REALTOR; CDPA', NULL, 37, 0),
(88, 37, 27, 'Marie Cruz\r\rChristineLeeTeam.com\r\rBuyers Specialist\r\rKeller Williams Jacksonville Realty\r\r\r\r', NULL, 37, 0),
(89, 37, 28, '9044650750', NULL, 37, 0),
(90, 37, 30, 'Marie brings a wealth of real estate knowledge and synergy to her clients, with a Bachelor degree in Finance and Associate of Arts degree in Real Estate, alone with 20 years of work experience in different real estate roles. Marie''s strength lies in build', NULL, 37, 0),
(91, 37, 40, 'I represented a home buyer on a house that was REO and was vacant with a lot of city violations that added to more than the value of the home!....Other challenges, included getting different safety inspectors, JEA inspectors and City of Jacksonville engin', NULL, 37, 0),
(92, 37, 41, '"Actions Speak Louder Than Words"', NULL, 37, 0),
(93, 37, 42, 'Diciplined;dedicated to my clients; problem solver;highly ethical', NULL, 37, 0),
(94, 37, 43, '1', NULL, 37, 0),
(95, 38, 19, '48146', NULL, 38, 0),
(96, 38, 20, 'buyerAgent', NULL, 38, 0),
(97, 38, 24, 'Realtor', NULL, 38, 0),
(98, 38, 25, 'Buyer Agent', NULL, 38, 0),
(99, 38, 27, 'Warmly, \r\r\r\rDebbie Dennis\r\rREALTOR? ~ Buyer Specialist\r\r\r\rCell ~ (904) 923-9472\r\r\r\r\r\r Find out What Your Home is Worth! ? JaxValues.com\r\rSearch 15,000+ Homes Online! ? Jax-HomeFinder.com \r\rBuy ~ Sell ~  Invest ~ Build ? ChristineLeeTeam.com\r\r \r\rForeclosur', NULL, 38, 0),
(100, 38, 28, '9049239472', NULL, 38, 0),
(101, 38, 29, '1', NULL, 38, 0),
(102, 31, 43, '1', NULL, 31, 0),
(103, 7, 30, 'Spent 20 years in retail customer service so I understand how to listen to people and give them what they want.\r\n\r\nI have been a Buyer Specialist for 6 years and truly LOVE helping people find their dream home. This is the best, most fulfilling job I have ever had. I work for the BEST real estate team in the world. We are supportive, honest, hard working and simply have a fantastic environment to excel in. There is no where I would rather be than here with my work family helping people to fulfill their home ownership goals. ', NULL, 7, 0),
(104, 35, 26, 'Realtor Associate', NULL, 35, 0),
(105, 35, 43, '1', NULL, 35, 0),
(106, 35, 41, 'To Be Old and Wise You Once had to be Young and Dumb', NULL, 35, 0),
(107, 35, 42, 'Determined Out Going Motivated ', NULL, 35, 0),
(108, 35, 40, 'Raising Three Children (memories are priceless) ', NULL, 35, 0),
(109, 35, 30, 'Have lived in Jacksonville/Orange Park mostly my whole life. Active Real Estate Licensee for 13 years serving Northeast Florida. ', NULL, 35, 0),
(110, 7, 39, '2', NULL, 7, 0),
(111, 3, 22, 'profile_photo.jpg', NULL, 3, 0),
(112, 20, 22, 'profile_photo.jpg', NULL, 20, 0),
(113, 14, 22, 'profile_photo.jpg', NULL, 14, 0),
(114, 14, 43, '1', NULL, 14, 0),
(115, 22, 22, 'profile_photo.jpg', NULL, 22, 0),
(116, 22, 39, '1', NULL, 22, 0),
(117, 7, 22, 'profile_photo.jpg', NULL, 7, 0),
(118, 31, 22, 'profile_photo.jpg', NULL, 31, 0),
(119, 37, 22, 'profile_photo.jpg', NULL, 37, 0),
(120, 38, 22, 'profile_photo.jpg', NULL, 38, 0),
(121, 38, 43, '1', NULL, 38, 0),
(122, 35, 22, 'profile_photo.jpg', NULL, 35, 0),
(123, 5, 22, 'profile_photo.jpg', NULL, 5, 0),
(124, 5, 38, '', NULL, 5, 0),
(125, 31, 38, '', NULL, 31, 0);


INSERT INTO `contacts` (`id`, `account_id`, `source_id`, `contact_status_ma`, `lead_type_ma`,
                        `first_name`, `last_name`, `password`, `notes`, `session_id`, `last_login`, `added`, `is_deleted`) VALUES
(1, 2, 1, 1, NULL, 'Christine', 'Lee', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(2, 2, 1, 0, NULL, 'Therese', 'Cannizzaro', '', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(3, 2, 1, 1, NULL, 'Sherri', 'Wilson', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(4, 2, 1, 0, NULL, 'Tasha', 'Bellanger', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(5, 2, 1, 1, NULL, 'Grace', 'Harrison', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(6, 2, 1, 0, NULL, 'Stacy', 'Hale', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(7, 2, 1, 1, NULL, 'Eric', 'Landry', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(8, 2, 1, 0, NULL, 'Susan', 'Severson', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(9, 2, 1, 0, NULL, 'Tracy', 'Cain', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(10, 2, 1, 0, NULL, 'Betsy', 'Sorrells', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(11, 2, 1, 0, NULL, 'Chelsey', '', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(12, 2, 1, 0, NULL, 'Cristie', 'Karsten', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(13, 2, 1, 0, NULL, 'Danielle', 'Sak', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(14, 2, 1, 1, NULL, 'Jennifer', 'Graham', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(15, 2, 1, 0, NULL, 'Connie', 'Laudel', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(16, 2, 1, 0, NULL, 'Heather', 'Sherrod', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(17, 2, 1, 0, NULL, 'Courtney', 'Boyd', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(18, 2, 1, 0, NULL, 'Al', 'Andrzejewski', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(19, 2, 1, 0, NULL, 'Mike', 'Schuler', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(20, 2, 1, 1, NULL, 'Ashley', 'Schwartz', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(21, 2, 1, 1, NULL, 'Soumen', 'Saha', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(22, 2, 1, 1, NULL, 'Katrina', 'Abdullah', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(23, 2, 1, 0, NULL, 'Bob', 'Corcoran', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(24, 2, 1, 0, NULL, 'Vivian', 'Peters', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(25, 2, 1, 0, NULL, 'Don', 'Bevis', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(26, 2, 1, 0, NULL, 'AppSoft', 'Development', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(27, 2, 1, 0, NULL, 'Liza', 'Rodriguez', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(28, 2, 1, 0, NULL, 'Allan', 'Palmer', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(29, 2, 1, 0, NULL, 'Jocylyne', 'Corlew', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(30, 2, 1, 0, NULL, 'Alex', 'Sanford', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(31, 2, 1, 1, NULL, 'Sergio', 'Barahona', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(32, 2, 1, 0, NULL, 'Christie', 'Starratt', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(33, 2, 1, 0, NULL, 'Andrew', 'Jajack', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(34, 2, 1, 0, NULL, 'Chris', 'Willard', NULL, NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(35, 2, 1, 1, NULL, 'Jackie', 'Behr', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(36, 2, 1, 1, NULL, 'Donna', 'McColgin', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(37, 2, 1, 1, NULL, 'Marie', 'Cruz', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0),
(38, 2, 1, 1, NULL, 'Debra', 'Dennis', 'admin', NULL, NULL, NULL, '2012-01-06 12:29:30', 0);


--
-- Dumping data for table `emails`
--
INSERT INTO `emails` (`account_id`, `contact_id`, `email_type_ma`, `email`, `email_opt_status_id`, `is_primary`) VALUES
(2, 1, 1, 'lee@christineleeteam.com', 1, 1),
(2, 3, 1, 'sherri@christineleeteam.com', 1, 1),
(2, 5, 1, 'grace@christineleeteam.com', 1, 1),
(2, 7, 1, 'eric@christineleeteam.com', 1, 1),
(2, 22, 1, 'katrina@christineleeteam.com', 1, 1),
(2, 14, 1, 'jennifer@christineleeteam.com', 1, 1),
(2, 20, 1, 'ashley@christineleeteam.com', 1, 1),
(2, 21, 1, 'soumen@christineleeteam.com', 1, 1),
(2, 31, 1, 'sergio@christineleeteam.com', 1, 1),
(2, 35, 1, 'jackie@christineleeteam.com', 1, 1),
(2, 36, 1, 'donna@christineleeteam.com', 1, 1),
(2, 37, 1, 'marie@christineleeteam.com', 1, 1),
(2, 38, 1, 'debra@christineleeteam.com', 1, 1);


--
-- Dumping data for table `contact_types`
--
INSERT INTO `contact_types` (`account_id`, `name`) VALUES
-- 0 is Global
('0', 'Coach'),
('0', 'Agent'),
('0', 'Recruit'),
('0', 'Sphere'),
('0', 'Client'),
('0', 'Prospect'),
('0', 'Lender'),
('0', 'Vendor'),
('0', 'Title Company'),
('0', 'Home Inspector'),
('0', 'WDO Inspector'),
('0', 'General Contractor'),
('0', 'Insurance'),
('0', 'Pool Inspector'),
('0', 'Electrician'),
('0', 'Plumber'),
('0', 'Painter'),
('0', 'Roofer'),
('0', 'Mover');

--
-- Dumping data for table `company_types`
--
INSERT INTO `company_types` (`account_id`, `name`) VALUES
-- 0 is Global
('0', 'Coach'),
('0', 'Recruit'),
('0', 'Sphere'),
('0', 'Client'),
('0', 'Prospect'),
('0', 'Lender'),
('0', 'Vendor'),
('0', 'Title Company'),
('0', 'Home Inspector'),
('0', 'WDO Inspector'),
('0', 'General Contractor'),
('0', 'Insurance'),
('0', 'Pool Inspector'),
('0', 'Electrician'),
('0', 'Plumber'),
('0', 'Painter'),
('0', 'Roofer'),
('0', 'Mover');

--
-- Dumping data for table `company_types`
--
-- INSERT INTO `company_types` (`account_id`, `type`) VALUES
-- ('2', 'Lender'), 				-- Contact REPEAT
-- ('2', 'Title Company'),			-- Contact REPEAT
-- ('2', 'Home Insepctor'),		-- Contact REPEAT
-- ('2', 'WDO Inspector'),			-- Contact REPEAT
-- ('2', 'Handyman'),				-- Contact REPEAT
-- ('2', 'General Contractor'),
-- ('2', 'Insurance'),
-- ('2', 'Pool Inspector'),
-- ('2', 'Electrician'),
-- ('2', 'Plumber'),
-- ('2', 'Painter'),
-- ('2', 'Roofer'),
-- ('2', 'Mover');

--
-- Dumping data for table `contact_type_lu`
--
-- INSERT INTO `contact_type_lu` (`contact_id`,`contact_type_id`) VALUES
-- (1, 1),
-- (2, 6),
-- (3, 6),
-- (4, 6),
-- (5, 6),
-- (6, 6),
-- (7, 6),
-- (8, 6),
-- (9, 6),
-- (10, 6),
-- (11, 6);

--
-- Dumping data for table `action_plans`
--
INSERT INTO `action_plans` (`id`, `account_id`, `status_ma`, `component_type_id`, `name`) VALUES
(1, '2', '1', '1', 'Sphere Monthly Touch'),
(2, '2', '1', '2', 'New Buyer Lead Follow-up'),
(3, '2', '1', '2', 'Met Buyer Lead Follow-up'),
(4, '2', '1', '3', 'New Seller Lead Follow-up'),
(5, '2', '1', '3', 'Met Seller Lead Follow-up'),
(6, '2', '1', '3', 'Traditional Listing Plan'),
(7, '2', '1', '3', 'Short Sale Listing Plan'),
(8, '2', '1', '3', 'REO Listing Plan'),
(9, '2', '1', '5', 'Equity Seller Closing Plan'),
(10, '2', '1', '5', 'Short Sale Closing Plan'),
(11, '2', '1', '5', 'Post Closing Plan'),
(12, '2', '1', '1', 'Expired Follow-up Plan'),
(13, '2', '1', '1', 'FSBO Follow-up Plan');

--
-- Dumping data for table `action_plan_items`
--
INSERT INTO `action_plan_items` (`id`, `action_plan_id`, `task_type_id`, `description`, `due_days`, `due_days_type_ma`,
                                 `due_reference_ma`, `assign_to_type_ma`, `specific_user_id`, `if_weekend`, `sort_order`,
                                 `is_deleted`) VALUES
('1', '1', '1', 'Initial phone call', '0', '1', '1', '1', NULL, '1', '1', '0'),
('2', '12', '1', 'Final phone call (Expired Follow-up)', '0', '1', '1', '1', NULL, '1', '1', '0'),
('3', '2', '2', 'Initial phone call', '2', '1', '1', '2', NULL, '1', '1', '0'),
('4', '2', '6', 'Personal Touch Correspondence', '3', '1', '1', '2', NULL, '1', '1', '0'),
('5', '3', '2', 'Initial phone call', '2', '1', '1', '2', NULL, '1', '1', '0'),
('6', '3', '4', 'Friendly Text Message Follow-Up', '1', '1', '1', '2', NULL, '1', '1', '0'),
('7', '4', '2', 'Initial phone call', '2', '1', '1', '4', NULL, '1', '1', '0'),
('8', '4', '6', 'Personal Touch Correspondence', '3', '1', '1', '5', NULL, '1', '1', '0'),
('9', '5', '2', 'Initial phone call', '2', '1', '1', '12', '3', '1', '1', '0'),
('10', '5', '4', 'Friendly Text Message Follow-Up', '1', '1', '1', '13', NULL, '1', '1', '0');


--
-- Dumping data for table `homes_shown`
--
-- INSERT INTO `homes_shown` (`id`, `contact_id`, `shown_by`, `homes_saved_id`, `status_ma`, `datetime`, `notes`, `wrote_offer`, `is_deleted`) VALUES (NULL, '2', '2', '1', '1', '2012-08-22 15:27:10', 'top of list', '0', NULL);

--
-- Dumping data for table `saved_homes`
--
-- INSERT INTO `saved_homes` (`id`, `contact_id`, `listing_id`, `status_ma`, `notes`, `added`, `added_by`, `is_deleted`) VALUES
-- (NULL, '2', '633207', NULL, 'want to see this one', '2012-08-08 06:15:26', '1', '0');

--
-- Dumping data for table `viewed_pages`
--
-- INSERT INTO `viewed_pages` (`id`, `contact_id`, `page_type_ma`, `listing_id`, `url`, `added`, `ip`) VALUES
-- (NULL, '2', '1', '57294', NULL, '2012-08-22 08:24:16', NULL);

--
-- Dumping data for table `assignments`
--
-- INSERT INTO `assignments` (`id`, `contact_id`, `component_type_id`, `assignment_type_id`, `record_id`, `is_primary`) VALUES
-- (NULL, '2', '3', '4', '1', '1'),
-- (NULL, '2', '2', '2', '2', '1'),
-- (NULL, '3', '4', '2', '3', '1');

--
-- Dumping data for table `contact_attribute_types`
--
INSERT INTO `contact_attributes` (`id`, `data_type_id`, `name`, `label`) VALUES
(1, 2, 'birthday', 'Birthday'),
(2, 2, 'closing_anniversary', 'Closing Anniversary'),
(3, 1, 'family', 'Family'),
(4, 1, 'hobbies', 'Hobbies'),
(5, 1, 'books', 'Books'),
(6, 1, 'movies', 'Movies'),
(7, 1, 'restaurants', 'Restaurants'),
(8, 1, 'career', 'Career'),
(9, 1, 'social_media', 'Social Media');

--
-- Dumping data for table `contact_attribute_account_lu`
--
INSERT INTO `contact_attribute_account_lu` (`account_id`, `contact_attribute_id`) VALUES
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9);


INSERT INTO `contact_attribute_values` (`contact_id`, `contact_attribute_id`, `value`) VALUES
(1, 1, '1981-05-31'),
(1, 2, '2012-02-15'),
(1, 3, 'mother, father, 1 younger brother in CA'),
(1, 4, 'Golf Skiing, Reading'),
(1, 5, 'Atlas Shrugged by Ayn Rand, Think & Grow Rich'),
(1, 6, 'Taken, Forest Gump, The Notebook'),
(1, 7, 'Melting Pot, Smokey Bones'),
(1, 8, 'Residential Home Builder'),
(3, 4, 'Reading, College Sports, Marksmanship, Literacy Tutor'),
(3, 5, 'True Crime, Mystery'),
(3, 6, 'Action, Historical'),
(3, 7, 'Mellow Mushroom'),
(5, 4, 'Flying airplanes, riding four wheelers, enjoying life :)'),
(5, 7, 'Carrabbas, Kickbacks, Moon River Pizza'),
(7, 4, 'Target Shooting, Teaching Martial Arts, Hiking, Playing with my dog, Clubber, Cooking and Spending time with my family and friends'),
(7, 5, 'The Noticer, Secrets of the Millionaire Mind, The Secret, The 5 Dysfunctions of a Team, Awaken the Giant Within, Dragon Tears, Twilight Eyes'),
(7, 6, 'Star Wars, Raiders of the Lost Ark, Fight Club, The Pricess Bride, The Pursuit of Happyness'),
(7, 7, 'Taco Lu, Mellow Mushroom, Mojo''s BBQ, Borillos Pizza'),
(20, 5, 'The Noticer, The Unbroken, and '),
(22, 4, 'Reading, bike riding, family time, interior decorating.'),
(22, 5, 'The Noticer, The Travelers Gift, The Great Gatsby'),
(22, 6, 'Armageddon, The Pianist, Wall-E, The Shawshank Redemption, The Wizard of Oz, ET'),
(22, 7, 'Picasso''s, Kanki, Sandwhich Depot, Seasons 52....'),
(37, 4, 'zumba dancing'),
(37, 6, 'Ghost story'),
(37, 7, 'Ruth Chris'),
(31, 4, 'Running , Soccer, weight training'),
(31, 5, 'Game of Thrones'),
(31, 6, 'Transformers ,300, Avengers'),
(31, 7, 'Taco Lu');

--
-- Dumping data for table `market_trends`
--
INSERT INTO `market_trends` (`account_id`, `name`, `description`) VALUES
('2', 'Jacksonville', 'General Market');

--
-- Dumping data for table `market_trend_data`
--
INSERT INTO `market_trend_data` (`market_trend_id`, `date`, `value`) VALUES
('1', '2012-11-01', '528');

--
-- Dumping data for table `lead_routes`
--
INSERT INTO `lead_routes` (`account_id`, `type_ma`, `name`, `description`) VALUES
('2', '1', 'Buyer Leads', 'Handles Buyer type leads'),
('2', '1', 'Seller Leads', 'Handles Seller type leads'),
('2', '1', 'Website Contact', 'General web contact form'),
('2', '1', 'General Recruiting', 'Team opportunity inquiries'),
('2', '1', 'Agent Recruiting', 'Agent opportunity inquiries');


--
-- Dumping data for table `lead_route_rules`
--
INSERT INTO `lead_route_rules` (`lead_route_id`, `name`, `field_ma`, `operator_ma`, `value`, `is_deleted`) VALUES
('1', 'Buyer Leads', '1', '1', 'Buyer', '0');

--
-- Dumping data for table `lead_shifts`
--
INSERT INTO `lead_shifts` (`id`, `lead_route_id`, `contact_id`, `date`, `start_time`, `end_time`, `updated`, `updated_by`, `is_deleted`) VALUES
(1, 1, 14, '2013-02-04', '17:00:00', '23:59:00', '2013-02-04 09:39:21', 14, 0),
(2, 1, 14, '2013-02-07', '13:00:00', '17:00:00', '2013-02-04 09:41:35', 14, 0),
(3, 1, 14, '2013-02-09', '09:00:00', '13:00:00', '2013-02-04 09:42:01', 14, 0),
(4, 1, 14, '2013-02-10', '09:00:00', '13:00:00', '2013-02-04 09:42:27', 14, 0),
(5, 1, 14, '2013-02-11', '13:00:00', '17:00:00', '2013-02-04 09:43:38', 14, 0),
(6, 1, 14, '2013-02-13', '13:00:00', '17:00:00', '2013-02-04 09:44:02', 14, 0),
(7, 1, 14, '2013-02-14', '17:00:00', '00:00:00', '2013-02-04 09:44:22', 14, 0),
(8, 1, 14, '2013-02-16', '21:00:00', '13:00:00', '2013-02-04 09:44:45', 14, 0),
(9, 1, 14, '2013-02-17', '17:00:00', '00:00:00', '2013-02-04 09:45:06', 14, 0),
(10, 1, 14, '2013-02-19', '13:00:00', '17:00:00', '2013-02-04 09:45:29', 14, 0),
(11, 1, 14, '2013-02-21', '00:00:00', '13:00:00', '2013-02-04 09:45:55', 14, 0),
(12, 1, 14, '2013-02-23', '17:00:00', '00:00:00', '2013-02-04 09:46:17', 14, 0),
(13, 1, 14, '2013-02-25', '13:00:00', '17:00:00', '2013-02-04 09:46:35', 14, 0),
(14, 1, 14, '2013-02-27', '00:00:00', '13:00:00', '2013-02-04 09:46:55', 14, 0),
(15, 1, 14, '2013-02-28', '13:00:00', '17:00:00', '2013-02-04 09:47:15', 14, 0),
(16, 1, 7, '2013-02-04', '13:00:00', '17:00:00', '2013-02-04 13:06:31', 7, 0),
(17, 1, 7, '2013-02-07', '00:00:00', '13:00:00', '2013-02-04 13:07:12', 7, 0),
(18, 1, 37, '2013-02-07', '00:00:00', '13:00:00', '2013-02-04 13:08:30', 7, 0),
(19, 1, 37, '2013-02-04', '09:00:00', '13:00:00', '2013-02-04 13:09:22', 37, 0),
(20, 1, 37, '2013-02-05', '17:00:00', '21:00:00', '2013-02-04 13:10:10', 37, 0),
(21, 1, 22, '2013-02-05', '00:01:00', '13:00:00', '2013-02-04 13:12:09', 22, 0),
(22, 1, 22, '2013-02-08', '17:00:00', '23:59:00', '2013-02-04 13:13:00', 22, 0),
(23, 1, 22, '2013-02-09', '13:00:00', '17:00:00', '2013-02-04 13:13:31', 22, 0),
(24, 1, 22, '2013-02-10', '13:00:00', '17:00:00', '2013-02-04 13:13:53', 22, 0),
(25, 1, 7, '2013-02-08', '00:00:00', '13:00:00', '2013-02-04 13:14:29', 7, 0),
(26, 1, 22, '2013-02-12', '00:00:00', '13:00:00', '2013-02-04 13:14:35', 22, 0),
(27, 1, 7, '2013-02-06', '00:00:00', '13:00:00', '2013-02-04 13:14:57', 7, 0),
(28, 1, 22, '2013-02-15', '00:00:00', '13:00:00', '2013-02-04 13:15:23', 22, 0),
(29, 1, 7, '2013-02-11', '17:00:00', '23:59:00', '2013-02-04 13:15:44', 7, 0),
(30, 1, 22, '2013-02-16', '13:00:00', '17:00:00', '2013-02-04 13:15:49', 22, 0),
(31, 1, 7, '2013-02-13', '00:00:00', '13:00:00', '2013-02-04 13:16:18', 7, 0),
(32, 1, 22, '2013-02-18', '00:00:00', '13:00:00', '2013-02-04 13:16:36', 22, 0),
(33, 1, 7, '2013-02-14', '13:00:00', '17:00:00', '2013-02-04 13:16:51', 7, 0),
(34, 1, 22, '2013-02-19', '17:00:00', '23:59:00', '2013-02-04 13:17:05', 22, 0),
(35, 1, 7, '2013-02-15', '17:00:00', '23:59:00', '2013-02-04 13:17:16', 7, 0),
(36, 1, 22, '2013-02-21', '13:00:00', '17:00:00', '2013-02-04 13:17:35', 22, 0),
(37, 1, 7, '2013-02-17', '13:00:00', '17:00:00', '2013-02-04 13:17:50', 7, 0),
(38, 1, 22, '2013-02-22', '13:00:00', '17:00:00', '2013-02-04 13:18:01', 22, 0),
(39, 1, 7, '2013-02-19', '00:00:00', '13:00:00', '2013-02-04 13:18:19', 7, 0),
(40, 1, 22, '2013-02-24', '00:01:00', '13:00:00', '2013-02-04 13:18:28', 22, 0),
(41, 1, 7, '2013-02-20', '17:00:00', '23:59:00', '2013-02-04 13:18:51', 7, 0),
(42, 1, 22, '2013-02-25', '17:00:00', '23:59:00', '2013-02-04 13:19:05', 22, 0),
(43, 1, 7, '2013-02-22', '00:00:00', '13:00:00', '2013-02-04 13:19:28', 7, 0),
(44, 1, 22, '2013-02-28', '17:00:00', '00:01:00', '2013-02-04 13:19:35', 22, 0),
(45, 1, 7, '2013-02-23', '13:00:00', '17:00:00', '2013-02-04 13:19:50', 7, 0),
(46, 1, 7, '2013-02-25', '00:00:00', '13:00:00', '2013-02-04 13:20:19', 7, 0),
(47, 1, 7, '2013-02-26', '17:00:00', '23:59:00', '2013-02-04 13:20:43', 7, 0),
(48, 1, 37, '2013-02-07', '09:00:00', '13:00:00', '2013-02-04 13:20:50', 37, 0),
(49, 1, 7, '2013-02-28', '00:00:00', '13:00:00', '2013-02-04 13:21:09', 7, 0),
(50, 1, 37, '2013-02-09', '17:00:00', '21:00:00', '2013-02-04 13:22:41', 37, 0),
(51, 1, 37, '2013-02-11', '09:00:00', '12:00:00', '2013-02-04 13:23:36', 37, 0),
(52, 1, 37, '2013-02-12', '17:00:00', '21:00:00', '2013-02-04 13:24:13', 37, 0),
(53, 1, 37, '2013-02-14', '09:00:00', '13:00:00', '2013-02-04 13:24:52', 37, 0),
(54, 1, 37, '2013-02-16', '17:00:00', '21:00:00', '2013-02-04 13:25:51', 37, 0),
(55, 1, 37, '2013-02-18', '17:00:00', '21:00:00', '2013-02-04 13:26:37', 37, 0),
(56, 1, 37, '2013-02-20', '13:00:00', '17:00:00', '2013-02-04 13:27:09', 37, 0),
(57, 1, 37, '2013-02-23', '09:00:00', '13:00:00', '2013-02-04 13:28:52', 37, 0),
(58, 1, 37, '2013-02-24', '17:00:00', '21:00:00', '2013-02-04 13:29:23', 37, 0),
(59, 1, 37, '2013-02-26', '13:00:00', '17:00:00', '2013-02-04 13:30:10', 37, 0),
(60, 1, 37, '2013-02-27', '17:00:00', '21:00:00', '2013-02-04 13:30:44', 37, 0),
(61, 2, 31, '2013-02-04', '08:00:00', '00:00:00', '2013-02-04 16:03:35', 31, 0),
(62, 2, 31, '2013-02-06', '08:00:00', '00:00:00', '2013-02-04 16:04:35', 31, 0),
(63, 2, 31, '2013-02-08', '08:00:00', '00:00:00', '2013-02-04 16:04:57', 31, 0),
(64, 2, 31, '2013-02-10', '08:00:00', '21:00:00', '2013-02-04 16:05:36', 31, 0),
(65, 2, 31, '2013-02-12', '08:00:00', '21:00:00', '2013-02-04 16:06:13', 31, 0),
(66, 2, 31, '2013-02-14', '08:00:00', '21:00:00', '2013-02-04 16:06:59', 31, 0),
(67, 2, 31, '2013-02-16', '08:00:00', '21:00:00', '2013-02-04 16:07:19', 31, 0),
(68, 2, 31, '2013-02-18', '08:00:00', '21:00:00', '2013-02-04 16:08:09', 31, 0),
(69, 2, 31, '2013-02-20', '08:00:00', '21:00:00', '2013-02-04 16:08:29', 31, 0),
(70, 2, 31, '2013-02-22', '08:00:00', '21:00:00', '2013-02-04 16:08:45', 31, 0),
(71, 2, 31, '2013-02-24', '08:00:00', '21:00:00', '2013-02-04 16:09:08', 31, 0),
(72, 2, 31, '2013-02-26', '08:00:00', '21:00:00', '2013-02-04 16:09:28', 31, 0),
(73, 2, 31, '2013-02-28', '08:00:00', '21:00:00', '2013-02-04 16:09:46', 31, 0);

--
-- Dumping data for table `email_opt_status`
--
INSERT INTO `email_opt_status` (`id`, `name`) VALUES
(1, 'Single Opt-in'),
(2, 'Double Opt-in'),
(3, 'Manual Entry'),
(4, 'Verified'),
(5, 'Opt-out'),
(6, 'Soft-bound'),
(7, 'Hard-bound');

--
-- Dumping data for table `addresses`
--
-- INSERT INTO  `addresses` (`id` ,`account_id` ,`address_type_ma` ,`address` ,`city` ,`state_id` ,`zip` ,`is_deleted`) VALUES
-- ( '1' ,  '2',  '1', '6839 Holly Lane',  'Jacksonville',  '9',  '32259',  '0'),
-- ( '2' ,  '2',  '1', '5938 Bloomingdale Court', 'Jacksonville',  '9',  '32258',  '0'),
-- ( '3' ,  '2',  '1', '4827 Blueberry Street',  'Jacksonville',  '9',  '32257',  '0');

--
-- Dumping data for table `address_contact_lu`
--
-- INSERT INTO `address_contact_lu` (`id` ,`address_id` ,`contact_id` ,`is_primary`, `is_deleted`) VALUES
-- ('1',  '1',  '1', '1', '0'),
-- ('2',  '2',  '2', '1', '0'),
-- ('3',  '3',  '3', '1', '0');

--
-- Dumping data for table `transaction_address_lu`
--
-- INSERT INTO `transaction_address_lu` (`id`, `transaction_id`, `address_id`, `is_deleted`) VALUES
-- (NULL, '1', '1', '0'),
-- (NULL, '2', '2', '0'),
-- (NULL, '3', '3', '0');

--
-- Dumping data for table `component_types`
--
INSERT INTO `component_types` (`id`, `name`, `display_name`) VALUES
(1, 'contacts', 'Contacts'),
(2, 'buyers', 'Buyers'),
(3, 'sellers', 'Sellers'),
(4, 'listings', 'Listings'),
(5, 'closings', 'Closings'),
(6, 'actionPlans', 'Action Plans'),
(7, 'actionPlanItem', 'Action Plan Items'),
(8, 'task', 'Task'),
(9, 'email', 'Email'),
(10, 'projects', 'Projects'),
(11, 'projectItems', 'Project Items'),
(12, 'assignment', 'Assignment'),
(13, 'companies', 'Companies');
-- (12, 'survey', 'Survey'),
-- (13, 'team', 'Team'),

--
-- Dumping data for table `data_types`
--
INSERT INTO `data_types` (`id`, `name`) VALUES
(1, 'text'),
(2, 'date'),
(3, 'datetime'),
(4, 'dollars'),
(5, 'integer'),
(6, 'decimal'),
(7, 'boolean'),
(8, 'textarea');

--
-- Dumping data for table `task_types`
--
INSERT INTO `task_types` (`id`, `name`) VALUES
-- Applies to All Transaction Types
(1, 'To Do'),
(2, 'Phone'),
(3, 'Email'),
(4, 'Text Message'),
(5, 'Video Mail'),
(6, 'Handwritten Note'),
(7, 'Mail'),

-- Applies to Contacts/Buyer/Sellers
(8, 'Log Note'),

-- Both: Seller/Buyer
(9, 'Initial Consultation'),
(10, 'Signed Agreement'),
(11, 'Showing'),
(12, 'Contract Offer'),
(13, 'Contract Counter-Offer'),
(14, 'CMA'),

-- Seller Task Types (transaction_id# 2)
(15, 'Showing Feedback'),
(16, 'Price Reduction'),
(17, 'Seller Market Update'),

-- Closing Task Types (transaction_id# 4)
(18, 'Contract Executed'),
(19, 'Home Inspection'),
(20, 'Repair Agreement'),
(21, 'Appraisal'),
(22, 'HUD'),
(23, 'Closing'),

-- System Types or Entries
(24, 'Lead Transfer'),
(25, 'Web Chat'),

-- Extras, after the fact - Contacts/Buyer/Sellers
(26, 'Met in Person'),

-- After the fact - Sellers
(27, 'Sent Pre-list Package'),

(28, 'Password Reset'),

(29, 'Resubmission'),

(30, 'Action Plan');

--
-- Dumping data for table `task_type_component_lu`
--
INSERT INTO `task_type_component_lu` (`account_id`, `task_type_id`, `component_type_id`) VALUES
-- Contact Types
(2, 1, 1),
(2, 2, 1),
(2, 3, 1),
(2, 4, 1),
(2, 5, 1),
(2, 6, 1),
(2, 7, 1),
(2, 8, 1),
(2, 25, 1),
(2, 28, 1),

-- Buyers
(2, 1, 2),
(2, 2, 2),
(2, 3, 2),
(2, 4, 2),
(2, 5, 2),
(2, 6, 2),
(2, 7, 2),
(2, 8, 2),
(2, 9, 2),
(2, 10, 2),
(2, 11, 2),
(2, 12, 2),
(2, 13, 2),
(2, 14, 2),
(2, 25, 2),
(2, 28, 2),

-- Sellers
(2, 1, 3),
(2, 2, 3),
(2, 3, 3),
(2, 4, 3),
(2, 5, 3),
(2, 6, 3),
(2, 7, 3),
(2, 8, 3),
(2, 9, 3),
(2, 10, 3),
(2, 11, 3),
(2, 12, 3),
(2, 13, 3),
(2, 14, 3),
(2, 15, 3),
(2, 16, 3),
(2, 17, 3),
(2, 25, 3),
(2, 27, 3),
(2, 28, 3),

-- Closings
(2, 1, 5),
(2, 2, 5),
(2, 3, 5),
(2, 4, 5),
(2, 5, 5),
(2, 6, 5),
(2, 7, 5),
(2, 18, 5),
(2, 19, 5),
(2, 20, 5),
(2, 21, 5),
(2, 22, 5),
(2, 23, 5),
(2, 28, 5),

-- Action Plans
(2, 1, 6),
(2, 2, 6),
(2, 3, 6),
(2, 4, 6),
(2, 5, 6),
(2, 6, 6),
(2, 7, 6);

--
-- Dumping data for table `closings`
--
-- INSERT INTO `closings` (`id`, `transaction_id`, `closing_status_ma`, `price`, `sale_type_ma`, `mls_id`, `is_our_listing`, `notes`, `is_referral`, `referral_commission`, `commission_percent`, `commission_amount`, `contract_execute_date`, `contract_closing_date`, `actual_closing_datetime`, `is_mail_away`, `closing_location`, `is_deleted`) VALUES
-- (NULL, '3', '1', '725000', '1', '573826', NULL, 'Going out of town Sept 4-10', '0', NULL, '3', '21750', '2012-08-14', '2012-09-27', '2012-09-17 08:11:40', '1', 'KW office', '0');

--
-- Dumping data for table `assignment_types`
--
INSERT INTO `assignment_types` (`id`, `name`, `sort_order`, `is_deleted`) VALUES
-- Contacts
(1, 'Assigned to', '1', '0'),

-- Buyers
(2, 'Buyer Agent', '2', '0'),
(3, 'Co-Buyer Agent', '3', '0'),

-- Sellers
(4, 'Listing Agent', '4', '0'),
(5, 'Co-listing Agent', '5', '0'),
(6, 'Listing Manager', '6', '0'),

-- Showing Partners
(7, 'Showing Partner', '7', '0'),

-- ISAs
(8, 'ISA', '8', '0'),
(9, 'Buyer ISA', '9', '0'),
(10, 'Seller ISA', '10', '0'),

-- Closing
(11, 'Closing Manager', '11', '0'),

-- Global
(12, 'Specific User', '12', '0'),
(13, 'Current User', '13', '0');


--
-- Dumping data for table `activity_log`
--
-- INSERT INTO `activity_log` (`id`, `account_id`, `component_type_id`, `component_id`, `task_type_id`, `activity_date`, `note`, `is_contact`, `added_by`, `added`, `is_action_plan`, `is_deleted`) VALUES
-- (NULL, '2', '1', '1', '2', '2012-07-10 16:37:45', 'Spoke to wife, they''re coming into town October 15th.', '1', '2', '2012-07-10 16:37:45', '0', '0');

--
-- Dumping data for table `tasks`
--
-- INSERT INTO `tasks` (`id`, `account_id`, `component_type_id`, `task_type_id`, `assigned_to_id`, `assigned_by_id`, `component_id`, `status_ma`, `due_date`, `complete_date`, `description`, `is_deleted`) VALUES
-- (NULL, '2', '1', '1', '2', '2', '2', '0', '2012-08-15 08:30:20', NULL, 'follow-up', '0'),
-- ('8', '2', '1', '1', '2', '2', '2', '0', '2012-11-15 08:30:20', NULL, 'final follow-up', '0');


-- INSERT INTO `saved_home_searches` (`contact_id`, `name`, `description`, `criteria`, `frequency`, `added_by`, `added`, `updated_by`, `updated`) VALUES
-- ('2', 'Mandarin Homes', NULL, '{"property_type_id":"1","price_min":"500000","price_max":"700000","bedrooms":"3","baths_total":"2","sq_feet":"4000"}', '0', '2', '2013-01-01 05:24:14', '2', '2013-01-01 05:24:14');

--
-- Dumping data for table `login_log`
--
-- INSERT INTO `login_log` (`id`, `contact_id`, `ip`, `datetime`) VALUES
-- (NULL, '2', '182.924.395.65', '2012-08-09 11:15:43');

--
-- Dumping data for table `document_types`
--
INSERT INTO `document_types` (`id`, `component_type_id`, `name`, `is_core`, `is_deleted`) VALUES

(NULL, '1', 'Misc', '1', '0'),

(NULL, '2', 'Pre-qual Letter', '1', '0'),
(NULL, '2', 'Proof of Funds', '1', '0'),
(NULL, '2', 'Loyalty Agreement', '1', '0'),

(NULL, '4', 'Listing Agreement', '1', '0'),
(NULL, '4', 'Seller\'s Disclosure', '1', '0'),
(NULL, '4', 'Homeowners Association Docs', '1', '0'),
(NULL, '4', 'Contract Offer', '1', '0'),

(NULL, '5', 'Executed Contract', '1', '0'),
(NULL, '5', 'Inspections', '1', '0'),
(NULL, '5', 'Repair Agreement', '1', '0'),
(NULL, '5', 'Contract Addendum', '1', '0'),
(NULL, '5', 'Prelim HUD', '1', '0'),
(NULL, '5', 'Executed HUD', '1', '0'),
(NULL, '10', 'Office Doc', '1', '0');

--
-- Dumping data for table `assignment_type_account_lu`
--
INSERT INTO `assignment_type_account_lu` (`id`, `account_id`, `component_type_id`, `assignment_type_id`, `is_deleted`) VALUES
(NULL, '2', '1', '1', '0'),
(NULL, '2', '2', '2', '0'),
(NULL, '2', '3', '4', '0'),
(NULL, '2', '3', '10', '0'),
(NULL, '2', '3', '6', '0'),
(NULL, '2', '4', '11', '0');

--
-- Dumping data for table `goals`
--
-- INSERT INTO `goals` (`id`, `contact_id`, `status_ma`, `name`, `year`, `start_date`, `end_date`, `avg_sales_price`, `lead_gen_min_per_day`, `lead_gen_min_per_week`, `lead_gen_hrs_per_month`, `contacts_per_day`, `contacts_per_week`, `contacts_per_month`, `contacts_appointment_conversion`, `appointments_per_month`, `appointments_agreement_conversion`, `agreements_per_month`, `agreements_contract_conversion`, `contracts_per_month`, `contracts_closing_conversion`, `closings_per_month`, `referral_percentage`, `income_per_month`, `income_per_year`, `is_deleted`) VALUES
-- (1, 2, 1, 'Christine''s Goals', 2012, '2012-01-01', '2012-12-31', 200000, 90, 450, 30, 10, 50, 200, 0.15, 8, 0.90, 7, 0.75, 4, 0.75, 3, 0.25, 9500, 100000, 0);

-- INSERT INTO `stm_app`.`goals` (`id`, `contact_id`, `status_ma`, `name`, `year`, `start_date`, `end_date`, `average_sales_price`, `lead_gen_minutes_per_day`, `income_per_year`, `average_commission_percent`, `contracts_closing_conversion`, `agreements_contract_conversion`, `appointments_agreement_conversion`, `contacts_per_appointment`, `contacts_per_day`, `referral_percentage`, `is_deleted`) VALUES
-- (NULL, '1', '1', 'Christine''s Goal', '2012', '2012-12-01', '2012-12-31', '200000', '90', '100000', '.03', '.85', '.85', '.9', '15', '10', '.2', '0');

--
-- Dumping data for table `mobile_gateways`
--
INSERT INTO `mobile_gateways` (`sms_gateway`, `carrier_name`) VALUES
('@txt.att.net', 'AT&T'),
('@messaging.sprintpcs.com', 'Sprint'),
('@tmomail.net', 'T-mobile'),
('@vtext.com', 'Verizon');

--
-- Dumping data for table `stm_contacts`
--

INSERT INTO `stm_contacts` (`id`, `stm_user_group_id`, `stm_contact_status_ma`, `first_name`, `last_name`, `username`, `password`, `is_deleted`) VALUES
(1, 1, 1, 'Christine', 'Lee', 'admin', 'admin', 0),
(2, 1, 1, 'Wayne', 'Wetherington', 'wayne', 'pw', 0),
(3, 1, 1, 'Paul', 'Galloway', 'paul', 'pw', 0);

--
-- Dumping data for table `stm_emails`
--
INSERT INTO `stm_emails` (`id`, `stm_contact_id`, `email`, `is_primary`, `is_deleted`) VALUES
(1, 1, 'admin@seizethemarket.com', 1, 0),
(2, 2, 'wayne@nefmls.com', 1, 0),
(3, 3, 'paul@tbrnet.org', 1, 0);

--
-- Dumping data for table `stm_addresses`
--

INSERT INTO `stm_addresses` (`id`, `address`, `city`, `state`, `zip`, `is_deleted`) VALUES
(1, '2974 Hartley Rd', 'Jacksonville', 'FL', '32257', 0),
(2, '7801 Deercreek Club Road', 'Jacksonville', 'FL', '32257', 0),
(3, '1029 Thomasville Road', 'Tallahassee', 'FL', '32303', 0);

--
-- Dumping data for table `stm_phones`
--

INSERT INTO `stm_phones` (`id`, `phone`, `extension`, `is_primary`, `is_deleted`) VALUES
(1, '9042803800', NULL, 1, 0),
(2, '9043949494', NULL, 1, 0),
(3, '8502247713', NULL, 1, 0);

--
-- Dumping data for table `mls_boards`
--

INSERT INTO `mls_boards` (`id`, `name`, `short_name`, `state_id`, `time_zone_ma`, `feed_type`, `feed_method`, `feed_host`, `feed_update_frequency`, `feed_cron_frequency`, `feed_cron_time`, `feed_delimeter`, `feed_last_updated`, `stm_phone_id`, `notes`, `is_deleted`) VALUES
(1, 'Seize the Market', 'STM', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0),
(2, 'Northeast Florida Association of Realtors', 'NEFAR', 9, NULL, 'IDX', 'RETS', 'http://retsgw.flexmls.com:80/rets2_0/Login', 'ASAP', 'Daily', '3', NULL, NULL, 2, NULL, 0),
(3, 'Tallahassee Board of Realtors', 'TBR', 9, NULL, 'IDX', 'RETS', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, 0);

--
-- Dumping data for table `mls_board_address_lu`
--

INSERT INTO `mls_board_address_lu` (`id`, `mls_board_id`, `stm_address_id`, `is_deleted`) VALUES
(1, 1, 1, 0),
(2, 2, 2, 0),
(3, 3, 3, 0);

--
-- Dumping data for table `mls_board_contact_lu`
--

INSERT INTO `mls_board_contact_lu` (`id`, `mls_board_id`, `stm_contact_id`, `mls_board_contact_type_ma`, `is_deleted`) VALUES
(1, 1, 1, NULL, 0),
(2, 2, 2, NULL, 0);

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`id`, `account_id`, `name`, `theme`, `is_active`, `is_deleted`) VALUES
(1, 1, 'seizethemarket.local', NULL, 1, 0),
(2, 2, 'cleeteam.local', NULL, 1, 0),
(3, 3, 'tallahassee.top-neighborhoods.com', NULL, 1, 0);

--
-- Dumping data for table `sources`
--
INSERT INTO `sources` (`id`, `account_id`, `name`) VALUES
(1, 2, 'Unknown'),
(2, 2, 'Sphere'),
(3, 2, 'Past Client'),
(4, 2, 'Referral'),
(5, 2, 'Agent Referral'),
(6, 2, 'Craigslist'),
(7, 2, 'Dave Ramsey'),
(8, 2, 'Farming'),
(9, 2, 'Expired'),
(10, 2, 'FSBO'),
(11, 2, 'IVR'),
(12, 2, 'Newsletter'),
(13, 2, 'Open House'),
(14, 2, 'Radio'),
(15, 2, 'Sign Call'),
(16, 2, 'TV'),
(17, 2, 'Website');

--
-- Dumping data for table `source_campaigns` ... modified to sources with parent_id
--
INSERT INTO `sources` (`account_id`, `parent_id`, `name`) VALUES
(2, 19, 'Aberdeen'),
(2, 19, 'Austin Park'),
(2, 19, 'Bartram Plantation'),
(2, 19, 'Bartram Springs'),
(2, 19, 'Cimarrone'),
(2, 19, 'Coastal Oaks'),
(2, 19, 'Deercreek'),
(2, 19, 'Deerwood'),
(2, 19, 'Doctors Inlet Reserve'),
(2, 19, 'Durbin Crossing'),
(2, 19, 'Eagle Harbor'),
(2, 19, 'Eagle Landing'),
(2, 19, 'Epping Forest'),
(2, 19, 'Equestrian'),
(2, 19, 'Fleming Island'),
(2, 19, 'Glen Kernan'),
(2, 19, 'Hampton Park'),
(2, 19, 'Harbour Island'),
(2, 19, 'Jacksonville'),
(2, 19, 'Jacksonville Foreclosure'),
(2, 19, 'James Island'),
(2, 19, 'Jax Golf & CC'),
(2, 19, 'Julington Creek Plantation'),
(2, 19, 'Kensington'),
(2, 19, 'King & Bear'),
(2, 19, 'Luxury'),
(2, 19, 'Mandarin'),
(2, 19, 'Marsh Landing'),
(2, 19, 'Murabella'),
(2, 19, 'Nocatee'),
(2, 19, 'Oakleaf Plantation'),
(2, 19, 'Oceanfront'),
(2, 19, 'Orange Park CC'),
(2, 19, 'Ortega'),
(2, 19, 'Pablo Bay'),
(2, 19, 'Pablo Creek Reserve'),
(2, 19, 'Palencia'),
(2, 19, 'Payasada Estates'),
(2, 19, 'Plantation at Ponte Vedra'),
(2, 19, 'Plantation Oaks'),
(2, 19, 'Ponte Vedra'),
(2, 19, 'Ponte Vedra Beach Oceanfront'),
(2, 19, 'Ponte Vedra Blvd'),
(2, 19, 'Ponte Vedra Oceanfront'),
(2, 19, 'Queens Harbour'),
(2, 19, 'Realtor.com'),
(2, 19, 'San Marco'),
(2, 19, 'Sawgrass'),
(2, 19, 'Sawmill Lakes'),
(2, 19, 'Search Engine (Organic)'),
(2, 19, 'South Hampton'),
(2, 19, 'St Johns Forest'),
(2, 19, 'St Johns Golf & CC'),
(2, 19, 'Walden Chase'),
(2, 19, 'Waterfront'),
(2, 19, 'World Golf Village'),
(2, 19, 'Worthington Park'),

(2, 4, 'Shands'),

(2, 9, 'Expired Calls'),
(2, 9, 'Expired Mailing'),

(2, 14, 'Rush Limbaugh'),
(2, 14, 'Sean Hannity'),

(2, 8, 'Just Sold'),
(2, 8, 'Jax Values'),
(2, 8, 'Queens Harbour Values'),
(2, 8, 'South Hampton Values'),
(2, 8, 'Eagle Harbor Values'),
(2, 8, 'JCP Values'),
(2, 8, 'Short Sale Letter'),
(2, 8, 'St Johns Golf & CC Values'),
(2, 8, 'Cimarrone Values'),
(2, 17, 'House Values Widget'),
(2, 17, 'Relocation Guide'),
(2, 17, 'Top School Report'),
(2, 17, 'Military Relocation Guide'),
(2, 17, 'Staging Report'),
(2, 17, 'Be on House Hunters'),
(2, 17, 'First-time Homebuyer Guide'),
(2, 17, 'Loan Myth Report'),
(2, 17, 'Hot Staging Tips');

--
-- Dumping data for table `phones`
--
INSERT INTO `phones` (`id`, `account_id`, `contact_id`, `phone_type_ma`, `phone`, `owner_ma`) VALUES
(1, 2, 1, 1, '9042803315', 0), -- CLee
(2, 2, 3, 1, '9046518899', 0), -- Sherri
(3, 2, 5, 1, '9049458926', 0), -- Grace
(4, 2, 7, 1, '9045368702', 0), -- Eric
(5, 2, 14, 1, '9047081839', 0), -- Jennifer
(6, 2, 20, 1, '3053933803', 0), -- Ashley
(7, 2, 22, 1, '9045681752', 0), -- Katrina
(8, 2, 31, 1, '9045021565', 0), -- Sergio
(9, 2, 35, 1, '9046513116', 0), -- Jackie
(10, 2, 36, 1, '9045400625', 0), -- Donna
(11, 2, 37, 1, '9044650750', 0), -- Marie
(12, 2, 38, 1, '9049239472', 0);
-- Debra

--
-- Dumping data for table `cms_content_type`
--
INSERT INTO `cms_tags` (`domain_id`, `name`) VALUES
(2, 'Buyer'),
(2, 'Seller'),
(2, 'Market'),
(2, 'Financing'),
(2, 'Team'),
(2, 'Testimonials'),
(2, 'Hiring');

--
-- Dumping data for table `cms_content`
--
INSERT INTO `cms_contents` VALUES ('1', '2', '1', '1', 'video', 'meet-the-team', 'Meet the Team!', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/OCWj5xgu5Ng\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\"></h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Insert body paragraph text.</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"#\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/OCWj5xgu5Ng/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\"></h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"#\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/OCWj5xgu5Ng/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\"></h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"#\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/OCWj5xgu5Ng/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\"></h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"#\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/OCWj5xgu5Ng/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\"></h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\"></h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Insert body paragraph text.</p></div> ', '', '1', '2013-01-31 05:55:16', '1', null, null, '0'), ('2', '2', '1', '1', 'video', 'the-truth-about-zillow-and-other-avms', 'The Truth About Zillow and other AVM\'s', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/ZEjOzPAIjwA\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">The Truth About Zillow and other AVM\'s</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p>Zillow and other Automatic Valuation Models (AVM\'s)...how accurate are they?</p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/stop-foreclosures-with-a-short-sale\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/yiW5Us6iyX4/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Stop Foreclosures with a Short Sale</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/kids-on-real-estate\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PcoWB-Vsl8g/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Kids and Real Estate</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/selling-and-buying-with-christine\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/s0GXwdORrlY/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Testimonial - Buying and Selling with Christine</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/this-month-in-real-estate\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/gX6vsfwgJgg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">This Month In Real Estate</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Are Zillow and other AVM\'s accurate?</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Zillow and many other automated valuation models claim to be able to give a current value of your home.&nbsp; But are the values really accurate?</p></div> ', null, '1', '2013-01-31 13:06:48', '20', null, null, '0'), ('3', '2', '1', '1', 'video', 'relocation-guide', 'Relocation Guide', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/0SSEJ_Rf6qE\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Relocation Guide</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p><span style=\"font-size: 8.0pt; line-height: 115%; font-family: \'Arial\',\'sans-serif\'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Get your FREE Relocation Guide now!</span></p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/free-money-for-buyers\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Free Money for Buyers</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">An Agent Doesn\'t Matter... What?</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homeowners-insurance\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/41Yu5F9aZU4/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Free Report on Homeowner\'s Insurance</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/find-out-what-your-home-is-worth\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/9DsLoEAncuw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Find Out What Your Homes is Worth!</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Relocation Guide</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><span style=\"font-family: Arial, sans-serif; font-size: 11px; line-height: 12px;\">Everything you need for a relocation to Northeast Florida.</span></p></div> ', null, '1', '2013-01-31 11:47:17', '5', null, null, '0'), ('4', '2', '1', '1', 'video', 'find-out-what-your-home-is-worth', 'Find Out What Your Home is Worth', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/9DsLoEAncuw\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Find Out What Your Home is Worth</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p>Taking a realistic approach in determing your home\'s current value.</p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/this-month-in-real-estate\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/gX6vsfwgJgg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">This Month In Real Estate</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">The Truth About Zillow and Other AVM\'s</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/stop-foreclosures-with-a-short-sale\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/yiW5Us6iyX4/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Stop Foreclosure with a Short Sale</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">An Agent Doesn’t Matter…..WHAT??!!!</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">What is your home worth?</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Let The Christine Lee Team help you determine the accurate value of your home today.</p></div> ', null, '1', '2013-01-31 11:49:00', '3', null, null, '0'), ('5', '2', '1', '1', 'video', 'test', 'test', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/nKU_rfQYpqY\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Test</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p>Insert body paragraph text.</p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"meet-the-team\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/nKU_rfQYpqY/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Meet the Team</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"test1\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Test2</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"test2\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Test3</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"test3\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Test4</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\"></h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Insert body paragraph text.</p></div> ', null, '1', '2013-01-30 12:59:55', '1', null, null, '0'), ('6', '2', '1', '1', 'video', 'this-month-in-real-estate', 'This Month In Real Estate', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/gX6vsfwgJgg\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">This Month in Real Estate</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p><!--[if gte mso 9]><xml>   <o:OfficeDocumentSettings>    <o:AllowPNG/>   </o:OfficeDocumentSettings>  </xml><![endif]--> <!--[if gte mso 9]><xml>   <w:WordDocument>    <w:View>Normal</w:View>    <w:Zoom>0</w:Zoom>    <w:TrackMoves/>    <w:TrackFormatting/>    <w:PunctuationKerning/>    <w:ValidateAgainstSchemas/>    <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>    <w:IgnoreMixedContent>false</w:IgnoreMixedContent>    <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>    <w:DoNotPromoteQF/>    <w:LidThemeOther>EN-US</w:LidThemeOther>    <w:LidThemeAsian>JA</w:LidThemeAsian>    <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>    <w:Compatibility>     <w:BreakWrappedTables/>     <w:SnapToGridInCell/>     <w:WrapTextWithPunct/>     <w:UseAsianBreakRules/>     <w:DontGrowAutofit/>     <w:SplitPgBreakAndParaMark/>     <w:EnableOpenTypeKerning/>     <w:DontFlipMirrorIndents/>     <w:OverrideTableStyleHps/>    </w:Compatibility>    <m:mathPr>     <m:mathFont m:val=\"Cambria Math\"/>     <m:brkBin m:val=\"before\"/>     <m:brkBinSub m:val=\"&#45;-\"/>     <m:smallFrac m:val=\"off\"/>     <m:dispDef/>     <m:lMargin m:val=\"0\"/>     <m:rMargin m:val=\"0\"/>     <m:defJc m:val=\"centerGroup\"/>     <m:wrapIndent m:val=\"1440\"/>     <m:intLim m:val=\"subSup\"/>     <m:naryLim m:val=\"undOvr\"/>    </m:mathPr></w:WordDocument>  </xml><![endif]--><!--[if gte mso 9]><xml>   <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"    DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"    LatentStyleCount=\"276\">    <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"/>    <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"/>    <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"/>    <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>    <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"/>    <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Table Grid\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"/>    <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"/>    <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"/>    <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"/>   </w:LatentStyles>  </xml><![endif]--> <!--[if gte mso 10]>  <style>   /* Style Definitions */  table.MsoNormalTable  	{mso-style-name:\"Table Normal\";  	mso-tstyle-rowband-size:0;  	mso-tstyle-colband-size:0;  	mso-style-noshow:yes;  	mso-style-priority:99;  	mso-style-parent:\"\";  	mso-padding-alt:0in 5.4pt 0in 5.4pt;  	mso-para-margin-top:0in;  	mso-para-margin-right:0in;  	mso-para-margin-bottom:10.0pt;  	mso-para-margin-left:0in;  	line-height:115%;  	mso-pagination:widow-orphan;  	font-size:11.0pt;  	font-family:Calibri;  	mso-ascii-font-family:Calibri;  	mso-ascii-theme-font:minor-latin;  	mso-hansi-font-family:Calibri;  	mso-hansi-theme-font:minor-latin;}  </style>  <![endif]--> <!--StartFragment--><span style=\"font-size: 8.0pt; line-height: 115%; font-family: Arial; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Find out what is going on this month in real estate.&nbsp;</span><!--EndFragment--></p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/this-month-in-real-estate\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/9DsLoEAncuw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Find Out What Your Home is Worth</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/your-home-sold-guaranteed\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/_2dAjbfT0jA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Your Home Sold... Guaranteed</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/hot-staging-tips--help-your-home-sell-itself\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/fy92KoqiXlw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Hot Staging Tips</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homes-that-sell-vs.-homes-that-sit\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PvWTW65rF4c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Homes That Sit Vs. Homes That Sell</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">This Month In Real Estate</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><!--[if gte mso 9]><xml>   <o:OfficeDocumentSettings>    <o:AllowPNG/>   </o:OfficeDocumentSettings>  </xml><![endif]--> <!--[if gte mso 9]><xml>   <w:WordDocument>    <w:View>Normal</w:View>    <w:Zoom>0</w:Zoom>    <w:TrackMoves/>    <w:TrackFormatting/>    <w:PunctuationKerning/>    <w:ValidateAgainstSchemas/>    <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>    <w:IgnoreMixedContent>false</w:IgnoreMixedContent>    <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>    <w:DoNotPromoteQF/>    <w:LidThemeOther>EN-US</w:LidThemeOther>    <w:LidThemeAsian>JA</w:LidThemeAsian>    <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>    <w:Compatibility>     <w:BreakWrappedTables/>     <w:SnapToGridInCell/>     <w:WrapTextWithPunct/>     <w:UseAsianBreakRules/>     <w:DontGrowAutofit/>     <w:SplitPgBreakAndParaMark/>     <w:EnableOpenTypeKerning/>     <w:DontFlipMirrorIndents/>     <w:OverrideTableStyleHps/>    </w:Compatibility>    <m:mathPr>     <m:mathFont m:val=\"Cambria Math\"/>     <m:brkBin m:val=\"before\"/>     <m:brkBinSub m:val=\"&#45;-\"/>     <m:smallFrac m:val=\"off\"/>     <m:dispDef/>     <m:lMargin m:val=\"0\"/>     <m:rMargin m:val=\"0\"/>     <m:defJc m:val=\"centerGroup\"/>     <m:wrapIndent m:val=\"1440\"/>     <m:intLim m:val=\"subSup\"/>     <m:naryLim m:val=\"undOvr\"/>    </m:mathPr></w:WordDocument>  </xml><![endif]--><!--[if gte mso 9]><xml>   <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"    DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"    LatentStyleCount=\"276\">    <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"/>    <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"/>    <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"/>    <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>    <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"/>    <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Table Grid\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"/>    <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"/>    <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"/>    <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"/>   </w:LatentStyles>  </xml><![endif]--> <!--[if gte mso 10]>  <style>   /* Style Definitions */  table.MsoNormalTable  	{mso-style-name:\"Table Normal\";  	mso-tstyle-rowband-size:0;  	mso-tstyle-colband-size:0;  	mso-style-noshow:yes;  	mso-style-priority:99;  	mso-style-parent:\"\";  	mso-padding-alt:0in 5.4pt 0in 5.4pt;  	mso-para-margin-top:0in;  	mso-para-margin-right:0in;  	mso-para-margin-bottom:10.0pt;  	mso-para-margin-left:0in;  	line-height:115%;  	mso-pagination:widow-orphan;  	font-size:11.0pt;  	font-family:Calibri;  	mso-ascii-font-family:Calibri;  	mso-ascii-theme-font:minor-latin;  	mso-hansi-font-family:Calibri;  	mso-hansi-theme-font:minor-latin;}  </style>  <![endif]--> <!--StartFragment--><span style=\"font-size: 8.0pt; line-height: 115%; font-family: Arial; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">The annual projected home sales are staying steady at 5 million with an average home sale price of $181,000 (up 2.1% from last month). Mortgage rates are up to 3.37%. Over 90% of buyers start their search on the Internet. List now to get a head start on your competition. Watch the video to find out more!</span><!--EndFragment--></p></div> ', null, '1', '2013-01-31 11:45:26', '20', null, null, '0'), ('7', '2', '0', '1', 'video', 'cdd-fees', 'CDD Fees', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/Uv0m5-NeX8Y\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">CDD Fees</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Christine explains CDD fees.&nbsp;</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/this-month-in-real-estate\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/gX6vsfwgJgg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">This Month in Real Estate</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/top-schools-report\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/HTJFT2baggs/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Top Schools Report</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">The Truth About Zillow</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">First Time Home Buyers</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">CDD Fees</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Everything you need to know about CDD fees.&nbsp;</p></div> ', null, '1', '2013-01-31 13:11:01', '20', null, null, '0');
INSERT INTO `cms_contents` VALUES ('8', '2', '1', '1', 'video', 'first-time-homebuyer', 'First Time Homebuyer', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/jLoBrsxUG3E\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">First Time Homebuyer</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Get a FREE Hombuyer Guide now!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myths-about-loans-and-lending\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/QXVNERa_yts/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Myths About Loans & Lending</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homeowners-insurance\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/41Yu5F9aZU4/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Free Report on Homeowner\'s Insurance</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Agent Doesn\'t Matter... What?</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster-on-new-construction-homes\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/VhjR3lkyaLU/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Mythbuster on New Construction Homes</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">First Time Homebuyer</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><span style=\"font-size: 8.0pt; line-height: 115%; font-family: \'Arial\',\'sans-serif\'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Rest assured our agents have answers to all your questions. We will be there every step of the way</span></p></div> ', null, '1', '2013-01-31 11:55:31', '5', null, null, '0'), ('9', '2', '1', '1', 'video', 'free-money-for-buyers', 'Free Money for Buyers', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/kejm_pPBtTM\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Free Money & Great Loans!</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Get a FREE Money Report &amp; Information on Great Loans!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">First Time Homebuyer</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/client-testimonial-sonya-jim\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/qPway50bf-c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Client Testimonial - Sonya & Jim</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/relocation-guide\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/0SSEJ_Rf6qE/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Relocation Guide</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">The Truth About Zillow</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Free Money for Buyers</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><span style=\"font-size: 8.0pt; line-height: 115%; font-family: \'Arial\',\'sans-serif\'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">There are many great loan options available! Our guide goes over many of those options available to you.&nbsp;</span></p></div> ', null, '1', '2013-01-31 13:18:59', '20', null, null, '0'), ('10', '2', '1', '1', 'video', 'customer-review-sonya-jim', 'Customer Review - Sonya & Jim', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/qPway50bf-c\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">We Need Advice – Help us Buy or Sell!</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Find your agent today!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/relocation-guide\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/0SSEJ_Rf6qE/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Relocation Guide</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/free-money-for-buyers\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Free Money for Buyers</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">An Agent Doesn\'t Matter.. What?</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/your-home-sold-guaranteed\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/_2dAjbfT0jA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Your Home Sold GUARANTEED!</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Customer Review - Sonya & Jim</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><span style=\"font-size: 8.0pt; line-height: 115%; font-family: \'Arial\',\'sans-serif\'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Eric helped Jim &amp; Sonya every step of the way! &nbsp;</span></p></div> ', null, '1', '2013-01-31 12:05:46', '5', null, null, '0'), ('11', '2', '1', '1', 'video', 'customer-review-rick', 'Customer Review - Rick', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/Y3k5I6thpKo\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">We Need Advice – Help us Buy or Sell!</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Find your agent today!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/client-testimonial-sonya-jim\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/qPway50bf-c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Customer Review - Jim & Sonya</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/your-home-sold-guaranteed\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/_2dAjbfT0jA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Your Home Sold GUARANTEED!</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/free-money-for-buyers\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Free Money for Buyer</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/relocation-guide\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/0SSEJ_Rf6qE/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Relocation Guide</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Customer Review - Rick</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><span style=\"font-size: 8.0pt; line-height: 115%; font-family: \'Arial\',\'sans-serif\'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Rick recommends Christine 100%!&nbsp;</span></p></div> ', null, '1', '2013-01-31 12:09:00', '5', null, null, '0'), ('12', '2', '1', '1', 'video', 'customer-review-bernie-tracey', 'Customer Review - Bernie & Tracey', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/s0GXwdORrlY\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\"> We Need Advice – Help us Buy or Sell!</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Find your agent today!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/free-money-for-buyers\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Free Money for Buyers</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/be-on-hgtvs-house-hunters\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/IXKK3sjLUgk/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Be on HGTV\'s House Hunter\'s!</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">First Time Homebuyer</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homes-that-sell-vs.-homes-that-sit\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PvWTW65rF4c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Homes That Sell vs. Sit</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Customer Review - Bernie & Tracey</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><span style=\"font-size: 8.0pt; line-height: 115%; font-family: \'Arial\',\'sans-serif\'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Christine helped us find the right home for us!&nbsp;</span></p></div> ', null, '1', '2013-01-31 12:12:30', '5', null, null, '0'), ('13', '2', '1', '1', 'video', 'military-relocation-guide', 'Military Relocation Guide', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/g3dOnAArnxw\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Military Relocation Guide</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Get your FREE Military Relocation Guide!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">First Time Homebuyer</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/free-money-for-buyers\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Free Money for Buyers</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">An Agent Doesn\'t Matter.. What?</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/your-home-sold-guaranteed\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/_2dAjbfT0jA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Your Home Sold GUARANTEED!</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Military Relocation Guide</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><span style=\"font-size: 8.0pt; line-height: 115%; font-family: \'Arial\',\'sans-serif\'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">A relocation guide custom-tailored for military families.</span></p></div> ', null, '1', '2013-01-31 12:15:35', '5', null, null, '0'), ('14', '2', '1', '1', 'video', 'hot-staging-tips-help-your-home-sell-itself', 'Hot Staging Tips:  Help Your Home Sell Itself', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/fy92KoqiXlw\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Hot Staging Tips:  Help Your Home Sell Itself</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p>Learn what you need to know to help your home sell itself.</p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">The Truth About Zillow and other AVM\'s</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/stop-foreclosures-with-a-short-sale\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/yiW5Us6iyX4/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Stop a Foreclosure with a Short Sale</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">An Agent Doesn\'t Matter....WHAT?!?!</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\" /this-month-in-real-estate\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/gX6vsfwgJgg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">This Month in Real Estate</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Staging leads to selling</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Learn what you can do to help your home sell itself.</p></div> ', null, '1', '2013-01-31 13:21:02', '20', null, null, '0'), ('15', '2', '1', '1', 'video', 'top-schools-report', 'Top Schools Report', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/HTJFT2baggs\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Top Schools Report</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p>Find out the top schools in the tri-county area!</p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/relocation-guide\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/0SSEJ_Rf6qE/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Relocation Guide</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myths-about-loans-and-lending\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/QXVNERa_yts/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Myths About Loans and Lenders</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"First Time Homebuyer\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">/first-time-homebuyer</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/your-home-sold-guaranteed\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/_2dAjbfT0jA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Your Home Sold... Guaranteed!</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Top Schools in Northeast Florida</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><!--[if gte mso 9]><xml>   <o:OfficeDocumentSettings>    <o:AllowPNG/>   </o:OfficeDocumentSettings>  </xml><![endif]--> <!--[if gte mso 9]><xml>   <w:WordDocument>    <w:View>Normal</w:View>    <w:Zoom>0</w:Zoom>    <w:TrackMoves/>    <w:TrackFormatting/>    <w:PunctuationKerning/>    <w:ValidateAgainstSchemas/>    <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>    <w:IgnoreMixedContent>false</w:IgnoreMixedContent>    <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>    <w:DoNotPromoteQF/>    <w:LidThemeOther>EN-US</w:LidThemeOther>    <w:LidThemeAsian>JA</w:LidThemeAsian>    <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>    <w:Compatibility>     <w:BreakWrappedTables/>     <w:SnapToGridInCell/>     <w:WrapTextWithPunct/>     <w:UseAsianBreakRules/>     <w:DontGrowAutofit/>     <w:SplitPgBreakAndParaMark/>     <w:EnableOpenTypeKerning/>     <w:DontFlipMirrorIndents/>     <w:OverrideTableStyleHps/>    </w:Compatibility>    <m:mathPr>     <m:mathFont m:val=\"Cambria Math\"/>     <m:brkBin m:val=\"before\"/>     <m:brkBinSub m:val=\"&#45;-\"/>     <m:smallFrac m:val=\"off\"/>     <m:dispDef/>     <m:lMargin m:val=\"0\"/>     <m:rMargin m:val=\"0\"/>     <m:defJc m:val=\"centerGroup\"/>     <m:wrapIndent m:val=\"1440\"/>     <m:intLim m:val=\"subSup\"/>     <m:naryLim m:val=\"undOvr\"/>    </m:mathPr></w:WordDocument>  </xml><![endif]--><!--[if gte mso 9]><xml>   <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"    DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"    LatentStyleCount=\"276\">    <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"/>    <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"/>    <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"/>    <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>    <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"/>    <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Table Grid\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"/>    <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"/>    <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"/>    <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"/>   </w:LatentStyles>  </xml><![endif]--> <!--[if gte mso 10]>  <style>   /* Style Definitions */  table.MsoNormalTable  	{mso-style-name:\"Table Normal\";  	mso-tstyle-rowband-size:0;  	mso-tstyle-colband-size:0;  	mso-style-noshow:yes;  	mso-style-priority:99;  	mso-style-parent:\"\";  	mso-padding-alt:0in 5.4pt 0in 5.4pt;  	mso-para-margin-top:0in;  	mso-para-margin-right:0in;  	mso-para-margin-bottom:10.0pt;  	mso-para-margin-left:0in;  	line-height:115%;  	mso-pagination:widow-orphan;  	font-size:11.0pt;  	font-family:Calibri;  	mso-ascii-font-family:Calibri;  	mso-ascii-theme-font:minor-latin;  	mso-hansi-font-family:Calibri;  	mso-hansi-theme-font:minor-latin;}  </style>  <![endif]--> <!--StartFragment--><span style=\"font-size: 8.0pt; line-height: 115%; font-family: Arial; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Free School report with a list of all the top schools ranked by county.&nbsp;</span><!--EndFragment--></p></div> ', null, '1', '2013-01-31 11:49:50', '20', null, null, '0'), ('16', '2', '1', '1', 'video', 'myths-about-loans-and-lending', 'Myths About Loans and Lending', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/QXVNERa_yts\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Myths About Loans and Lending</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p>There is a program out there for you! Get the lowest interest rates possible.&nbsp;</p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/relocation-guide\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/0SSEJ_Rf6qE/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Relocation Guide</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/be-on-hgtvs-house-hunters\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/IXKK3sjLUgk/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Be on HGTV\'s House Hunters</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">An Agent Doesn\'t Matter... What?</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/find-out-what-your-home-is-worth\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/9DsLoEAncuw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Find Out What Your Home is Worth</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Myths About Loans and Lending</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p><!--[if gte mso 9]><xml>   <o:OfficeDocumentSettings>    <o:AllowPNG/>   </o:OfficeDocumentSettings>  </xml><![endif]--> <!--[if gte mso 9]><xml>   <w:WordDocument>    <w:View>Normal</w:View>    <w:Zoom>0</w:Zoom>    <w:TrackMoves/>    <w:TrackFormatting/>    <w:PunctuationKerning/>    <w:ValidateAgainstSchemas/>    <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>    <w:IgnoreMixedContent>false</w:IgnoreMixedContent>    <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>    <w:DoNotPromoteQF/>    <w:LidThemeOther>EN-US</w:LidThemeOther>    <w:LidThemeAsian>JA</w:LidThemeAsian>    <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>    <w:Compatibility>     <w:BreakWrappedTables/>     <w:SnapToGridInCell/>     <w:WrapTextWithPunct/>     <w:UseAsianBreakRules/>     <w:DontGrowAutofit/>     <w:SplitPgBreakAndParaMark/>     <w:EnableOpenTypeKerning/>     <w:DontFlipMirrorIndents/>     <w:OverrideTableStyleHps/>    </w:Compatibility>    <m:mathPr>     <m:mathFont m:val=\"Cambria Math\"/>     <m:brkBin m:val=\"before\"/>     <m:brkBinSub m:val=\"&#45;-\"/>     <m:smallFrac m:val=\"off\"/>     <m:dispDef/>     <m:lMargin m:val=\"0\"/>     <m:rMargin m:val=\"0\"/>     <m:defJc m:val=\"centerGroup\"/>     <m:wrapIndent m:val=\"1440\"/>     <m:intLim m:val=\"subSup\"/>     <m:naryLim m:val=\"undOvr\"/>    </m:mathPr></w:WordDocument>  </xml><![endif]--><!--[if gte mso 9]><xml>   <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"    DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"    LatentStyleCount=\"276\">    <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"/>    <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"/>    <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"/>    <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"/>    <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>    <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"/>    <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Table Grid\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"/>    <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"/>    <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"/>    <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"/>    <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"/>    <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"/>    <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"/>    <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"/>    <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"     UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"/>    <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"/>    <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"/>   </w:LatentStyles>  </xml><![endif]--> <!--[if gte mso 10]>  <style>   /* Style Definitions */  table.MsoNormalTable  	{mso-style-name:\"Table Normal\";  	mso-tstyle-rowband-size:0;  	mso-tstyle-colband-size:0;  	mso-style-noshow:yes;  	mso-style-priority:99;  	mso-style-parent:\"\";  	mso-padding-alt:0in 5.4pt 0in 5.4pt;  	mso-para-margin-top:0in;  	mso-para-margin-right:0in;  	mso-para-margin-bottom:10.0pt;  	mso-para-margin-left:0in;  	line-height:115%;  	mso-pagination:widow-orphan;  	font-size:11.0pt;  	font-family:Calibri;  	mso-ascii-font-family:Calibri;  	mso-ascii-theme-font:minor-latin;  	mso-hansi-font-family:Calibri;  	mso-hansi-theme-font:minor-latin;}  </style>  <![endif]--> <!--StartFragment--><span style=\"font-size: 8.0pt; line-height: 115%; font-family: Arial; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Take advantage of the market! Listen to Christine bust the Myths about Loans and Lending.</span><!--EndFragment--></p></div> ', null, '1', '2013-01-31 11:54:42', '20', null, null, '0');
INSERT INTO `cms_contents` VALUES ('17', '2', '1', '1', 'video', 'homes-that-sell-vs.-homes-that-sit', 'Homes That Sell vs. Homes That Sit', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/PvWTW65rF4c\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<form id=\"video-form\" action=\"/front/forms/videoForm/formId/6\" method=\"post\">	<div class=\"form-container\"> 		<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Homes That Sell vs Homes That Sit</h3> 		<span class=\"stmcms form-description\" title=\"Form Description\"><p>Free report on the difference between homes that sell vs homes that sit.&nbsp;</p></span> 		<div id=\"name-inputs\"> 		    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />		    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />		</div> 		<div id=\"email-input\"> 		    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />		</div> 		<div id=\"phone-input\"> 		    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />		</div> 		<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 	</div> 	</form></div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/your-home-sold-guaranteed\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/_2dAjbfT0jA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Your Home Sold... Guaranteed!</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/find-out-what-your-home-is-worth\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/9DsLoEAncuw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Find Out What Your Home is Worth</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/hot-staging-tips--help-your-home-sell-itself\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/fy92KoqiXlw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Hot Staging Tips</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">First Time Home Buyers</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Homes That Sell vs Homes That Sit</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Find out what you need to do to make sure your home sells!</p></div> ', null, '1', '2013-01-31 13:25:14', '20', null, null, '0'), ('18', '2', '1', '1', 'video', 'myth-buster-on-new-construction-homes', 'Myth Buster on New Construction Homes', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/VhjR3lkyaLU\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Myth Buster on New Construction Homes</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Find out the muths of buying a new construction home.</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/top-schools-report\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/HTJFT2baggs/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Top Schools Report</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homeowners-insurance\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/41Yu5F9aZU4/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Free Report on Homeowner\'s Insurance</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/free-money-for-buyers\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Free Month & Great Loans!</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">The Truth About Zillow</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Myths About New Construction Homes</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Using a real estate agent while buying a new construction home BENEFITS the buyers!! It does not cost you anything to use a real estate agent for a new construction transaction.&nbsp;</p></div> ', null, '1', '2013-01-31 11:57:42', '20', null, null, '0'), ('19', '2', '1', '1', 'video', 'stop-foreclosures-with-a-short-sale', 'Stop Foreclosures with a Short Sale', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/PvWTW65rF4c\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Alternatives to Foreclosure</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>There are alternatives to a foreclosure if you a underwater with your home.</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">The Truth About Zillow and other AVM\'s</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/kids-on-real-estate\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PcoWB-Vsl8g/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Kids and Real Estate</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/selling-and-buying-with-christine\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/s0GXwdORrlY/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">\'Nuff Said</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/this-month-in-real-estate\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/gX6vsfwgJgg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">This Month in Real Estate</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Avoiding Foreclosure</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>&nbsp;Learn everything you need to know about short sales and get out from under your mortgage without a foreclosure</p></div> ', null, '1', '2013-01-31 13:28:49', '20', null, null, '0'), ('20', '2', '1', '1', 'video', 'myth-buster-an-agent-doesnt-matter', 'Myth Buster - An Agent Doesn\'t Matter... What?', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/sqs5DWEbrTg\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Myth Buster - An Agent Doesn\'t Matter... What? </h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Find out the myths on using an agent to help you buy your next home.&nbsp;</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">First Time Home Buyer</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homeowners-insurance\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/41Yu5F9aZU4/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Free Homeowner\'s Insurance Report</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">The Truth About Zillow</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homes-that-sell-vs.-homes-that-sit\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PvWTW65rF4c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Homes That Sit Vs. Homes That Sell</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Myth Buster - An Agent Doesn\'t Matter... What? </h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Find out the benefits of using an experienced, prepared agent.&nbsp;</p></div> ', null, '1', '2013-01-31 12:01:38', '20', null, null, '0'), ('21', '2', '1', '1', 'video', 'kids-on-real-estate', 'Kids on Real Estate', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/PcoWB-Vsl8g\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Kids on Real Estate</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>What children know about real estate.&nbsp;</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/find-out-what-your-home-is-worth\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/9DsLoEAncuw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Find Out What Your Home is Worth</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/This-month-in-real-estate\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/gX6vsfwgJgg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">This Month In Real Estate</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/selling-and-buying-with-christine\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/s0GXwdORrlY/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Selling and Buying with Christine</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">First Time Home Buyer</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Kids on Real Estate</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Choose the best real estate agent in Jacksonville... It\'s Christine Lee!</p></div> ', null, '1', '2013-01-31 13:06:01', '20', null, null, '0'), ('22', '2', '1', '1', 'video', 'selling-and-buying-with-christine', 'Selling and Buying with Christine', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/s0GXwdORrlY\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Selling and Buying with Christine</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Testimonial from one of Christine\'s sellers/buyers.&nbsp;</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/your-home-sold-guaranteed\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/_2dAjbfT0jA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Your Home Sold... Guaranteed!</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">First Time Home Buyers</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/top-schools-report\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/HTJFT2baggs/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Top Schools Report</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">The Truth About Zillow</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Selling and Buying with Christine</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Christine does it all for these sellers. She sold their home AND helped them purchase a new one!</p></div> ', null, '1', '2013-01-31 12:07:29', '20', null, null, '0'), ('23', '2', '1', '1', 'video', 'hgtv-jamie-durie-says', 'HGTV Jamie Durie Says...', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/pzWrP8hfe9s\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">HGTV Jamie Durie Says...</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>HGTV\'s Jamie Durie on \"Bang for Your Buck\".&nbsp;</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/find-out-what-your-home-is-worth\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/9DsLoEAncuw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Find Out What Your Home Is Worth</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homes-that-sell-vs.-homes-that-sit\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PvWTW65rF4c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Homes That Sit vs Homes That Sell</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">First Time Home Buyers</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/top-schools-report\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/HTJFT2baggs/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Top Schools Report</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">HGTV\'s Jamie Durie Says...</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Christine Lee is&nbsp;Jamie Durie\'s FAVORITE realtor!!</p></div> ', null, '1', '2013-01-31 12:18:28', '20', null, null, '0'), ('24', '2', '1', '1', 'video', 'your-home-sold-guaranteed', 'Your Home Sold Guaranteed', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/_2dAjbfT0jA\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Your Home Sold Guaranteed</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Christine can guaranteed that your home sells!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homes-that-sell-vs.-homes-that-sit\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PvWTW65rF4c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Homes That Sell vs Homes That Sit</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"hot-staging-tips--help-your-home-sell-itself\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/fy92KoqiXlw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Hot Staging Tips</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">First Time Home Buyers</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/find-out-what-your-home-is-worth\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/9DsLoEAncuw/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Find Out What Your Home Is Worth</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Your Home Sold Guaranteed</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Christine can guarantee to sell your home at a price and deadline you agree to or she will have it bought for cash.&nbsp;</p></div> ', null, '1', '2013-01-31 12:16:25', '20', null, null, '0'), ('25', '2', '1', '1', 'video', 'be-on-hgtv-house-hunters', 'Be on HGTV\'s House Hunters', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/IXKK3sjLUgk\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Be on HGTV\'s House Hunters</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Contact us to be on HGTV\'s House Hunters</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/hgtv-jamie-durie-says…\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/pzWrP8hfe9s/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">HGTV\'s Jamie Durie Says...</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/top-schools-report\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/HTJFT2baggs/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">Top Schools Report</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/the-truth-about-zillow-and-other-avms\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/ZEjOzPAIjwA/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">The Truth About Zillow</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/first-time-homebuyer\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/jLoBrsxUG3E/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">First Time Home Buyers</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Be on HGTV\'s House Hunters</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Think you got what it takes to be on House Hunters? Send over you information and we will get the process started!</p></div> ', null, '1', '2013-01-31 12:14:07', '20', null, null, '0'), ('26', '2', '1', '1', 'video', 'homeowners-insurance', 'Homeowner\'s Insurance', '<div class=\"top-container\"> 	<div class=\"video-container\"> 		<iframe class=\"stmcms\" title=\"Video URL\" data-thumbnail=\"\" data-type=\"youtube\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/41Yu5F9aZU4\" frameborder=\"0\" allowfullscreen></iframe> 	</div> 	<div class=\"form-container\"> 		<div id=\"form\"> 			<form id=\"video-form\" action=\"/front/forms/videoForm/formId/10\" method=\"post\">			<h3 class=\"stmcms\" title=\"Form Title\" placeholder=\"Add Your Title\">Homeowner\'s Insurance</h3> 			<span class=\"stmcms form-description\" title=\"Form Description\"><p>Get a free homeowner\'s report!</p></span> 			<div id=\"name-inputs\"> 			    <input placeholder=\"First Name\" name=\"FormSubmissionValues[data][1]\" id=\"FormSubmissionValues_data_1\" type=\"text\" />			    <input placeholder=\"Last Name\" name=\"FormSubmissionValues[data][2]\" id=\"FormSubmissionValues_data_2\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"E-mail\" name=\"FormSubmissionValues[data][3]\" id=\"FormSubmissionValues_data_3\" type=\"text\" />			</div> 			<div class=\"row\"> 			    <input placeholder=\"Phone\" name=\"FormSubmissionValues[data][4]\" id=\"FormSubmissionValues_data_4\" type=\"text\" />			</div> 			<button type=\"submit\" class=\"submit\">Get Free Report Now!</button> 			</form>		</div> 		<div id=\"thank-you-message\"> 			<span class=\"stmcms form-description\" title=\"Form Success Message\" style=\"display: none\"><p>Add text to display when a user successfully fills out a form.</p></span> 		</div> 	</div> </div>  <div class=\"video-thumbnails\"> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/top-schools-report\" class=\"stmcms\" title=\"Video #1 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #1\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/HTJFT2baggs/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #1\" placeholder=\"Video Title\">Top Schools Report</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/myth-buster---an-agent-doesnt-matter...-what\" class=\"stmcms\" title=\"Video #2 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #2\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/sqs5DWEbrTg/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #2\" placeholder=\"Video Title\">An Agent Doesn\'t Matter... What?</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/free-money-for-buyers\" class=\"stmcms\" title=\"Video #3 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #3\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/kejm_pPBtTM/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #3\" placeholder=\"Video Title\">Free Money & Great Loans</h5> 		</a> 	</div> 	<div class=\"video-thumbnails-item\"> 		<a href=\"/homes-that-sell-vs.-homes-that-sit\" class=\"stmcms\" title=\"Video #4 Link\"> 			<img class=\"stmcms video-thumbnail\" title=\"Suggested Video #4\" data-type=\"youtube\" data-thumbnail=\"true\" src=\"http://img.youtube.com/vi/PvWTW65rF4c/2.jpg\" width=\"200\" height=\"123\" /> 			<h5 class=\"stmcms\" title=\"Suggested Video Title #4\" placeholder=\"Video Title\">Homes That Sit Vs Homes That Sell</h5> 		</a> 	</div> </div>  <hr />  <h1 class=\"stmcms\" title=\"Content Title\" placeholder=\"Add Content Title\">Homeowner\'s Report</h1> <div class=\"stmcms\" id=\"content\" title=\"Content\"><p>Find out what your homeowner\'s insurance will cost. Get this free report.&nbsp;</p></div> ', null, '1', '2013-01-31 12:09:59', '20', null, null, '0');

--
-- Dumping data for table `cms_widgets`
--
INSERT INTO `cms_widgets` (`id`, `account_id`, `name`, `folder_name`, `description`, `is_active`, `is_deleted`) VALUES
(NULL, '2', 'Market Stats', 'MarketStatsWidget', 'Detailed stats on the market or neighborhood', '1', '0'),
(NULL, '2', 'House Values', 'HouseValuesWidget', 'Find Out What Your Home is Worth', '0', '0'),
(NULL, '2', 'Recently Viewed Homes', 'RecentlyViewedHomesWidget', 'Lists recently viewed homes by visitor', '0', '0'),
(NULL, '2', 'Foreclosure List', 'ForeclosureListWidget', 'Free Foreclosure List Form', '0', '0'),
(NULL, '2', 'Now Hiring', 'NowHiringWidget', 'Recruiting page for admin and agents', '0', '0');

--
-- Dumping data for table `cms_widget_groups`
--
INSERT INTO `cms_widget_groups` (`id`, `account_id`, `status_ma`, `name`, `description`, `is_default`) VALUES
(1, '2', '1', 'default', 'Default if none is specified', '1'),
(2, '2', '1', 'Buyers', 'widgets displayed for buyer pages', '0'),
(3, '2', '1', 'Sellers', 'widgets displayed for seller pages', '0');

--
-- Dumping data for table `transaction_status`
--

INSERT INTO `transaction_status` (`id`, `is_core`, `name`, `is_deleted`) VALUES
(1, 1, 'New Lead', 0),
(2, 1, 'Hot "A" Lead', 0),
(3, 1, '"B" Lead', 0),
(4, 1, '"C" Lead', 0),
(5, 1, 'Nurturing', 0),
(6, 1, 'Appt Set', 0),
(7, 1, 'Met', 0),
(8, 1, 'Listed', 0),
(9, 1, 'Under Contract', 0),
(10, 1, 'Closed', 0),
(11, 1, 'Expired', 0),
(12, 1, 'Cancelled', 0),
(13, 1, 'Inactive', 0);

--
-- Dumping data for table `transaction_status_account_lu`
--

INSERT INTO `transaction_status_account_lu` (`account_id`, `component_type_id`, `transaction_status_id`, `override_name`, `is_deleted`) VALUES
(2, 2, 1, '', 0),
(2, 2, 2, '', 0),
(2, 2, 3, '', 0),
(2, 2, 4, '', 0),
(2, 2, 5, '', 0),
(2, 2, 9, '', 0),
(2, 2, 10, '', 0),
(2, 2, 12, '', 0),
(2, 3, 1, '', 0),
(2, 3, 5, '', 0),
(2, 3, 6, '', 0),
(2, 3, 7, '', 0),
(2, 4, 8, '', 0),
(2, 4, 9, '', 0),
(2, 4, 10, '', 0),
(2, 4, 11, '', 0),
(2, 4, 12, '', 0);

--
-- Dumping data for table `transaction_fields`
--
INSERT INTO `transaction_fields` (`component_type_id`, `name`, `label`, `data_type_id`, `sort_order`, `is_deleted`) VALUES
-- (2, 'location', 'Location', 1, 1, 0),
(2, 'price_min', 'Price', 4, 2, 0),
(2, 'price_max', '', 4, 2, 0),
(2, 'motivation', 'Motivation', 1, 3, 0),
(2, 'if_not_buy', 'If Not Buy', 1, 3, 0),
(2, 'other_agent', 'Agent', 7, 4, 0),
(2, 'other_agent_signed', '', 7, 5, 0),
-- (8, 2, 'appt_met', 'Appt Met', 7, 7, 0),
-- (9, 2, 'appt_timedate', 'Appt Set', 3, 8, 0),
-- (10, 2, 'appt_location', '', 1, 9, 0),
(2, 'trial_close', 'Trial Close', 1, 10, 0),
(2, 'features', 'Features', 1, 11, 0),
(2, 'location', 'Location', 1, 11, 0),
(2, 'bedrooms', 'Bedrooms', 1, 11, 0),
(2, 'baths', 'Baths', 1, 12, 0),
(2, 'sq_feet', 'SF', 1, 13, 0),
(2, 'target_date', 'Target Date', 1, 14, 0),
(2, 'best_time_to_look', 'Best time to look', 1, 15, 0),
(2, 'own_rent', 'Own/Rent', 1, 16, 0),
(2, 'home_to_sell', 'Home to sell?', 7, 17, 0),
(2, 'home_to_sell_addr', '', 1, 18, 0),
(2, 'prequal_date', 'Prequal', 2, 19, 0),
(2, 'prequal_amount', '', 1, 20, 0),
-- (2, 'agreement_signed', 'Loyalty Agreement', 7, 21, 0),
(2, 'lender', 'Lender', 1, 22, 0),
(2, 'sale_type_ma', 'Sale Type', 4, 39, 0),
-- ============================= Listing Fields ====================================================



(3, 'motivation', 'What''s prompting your move? What''s your main reason for selling?', 1, 1, 0),
(3, 'where_moving_to', 'Where are you moving to? Why?', 1, 2, 0),
(3, 'target_date', 'How soon do you need to be there?', 1, 3, 0),
(3, 'if_not_sell', 'What''ll happen if your house doesn''t sell?', 1, 4, 0),
(3, 'idea_of_sales', 'Do you have any idea of what homes are selling for in your area?', 7, 5, 0),
(3, 'idea_of_value', 'Do you have your own thoughts on what your home is worth?', 1, 6, 0),
-- (3, 'idea_of_value_comments', '', 1, 7, 0),
(3, 'must_get_price', 'Do you have a price you MUST get for the property? Why?', 1, 8, 0),
(3, 'mortgage_amount_1', 'How much do you owe on the house? 1st Mtg:', 1, 10, 0),
(3, 'mortgage_amount_2', '2nd Mtg:', 1, 11, 0),
(3, 'payments_current', 'Are you current on your payments?', 1, 12, 0),
(3, 'is_sole_owner', 'Are you the sole owner of the house?', 1, 13, 0),
(3, 'ownership_comment', '', 1, 14, 0),
-- (3, 'home_description', 'Tell me a little bit about your house? BR, Bath, Sq Feet, Yr Built, # Garage, Pool-Screened, View (Woods, Water) Upgrades/Features', 1, 15, 0),
(3, 'relocation_pkg', 'Relocation Pkg?', 7, 16, 0),
(3, 'relocation_company', '', 1, 17, 0),
(3, 'other_agent', 'Are you working with an agent?', 7, 18, 0),
(3, 'other_agent_signed', 'Signed an agreement with them?', 1, 19, 0),
(3, 'agent_quality_1', 'What are the top 3 things you''re looking for in a real estate agent?', 1, 20, 0),
(3, 'agent_quality_2', '', 1, 21, 0),
(3, 'agent_quality_3', '', 1, 22, 0),
(3, 'interview_others', 'Are you interviewing other agents?', 7, 23, 0),
(3, 'interview_who', 'When are the other interviews? With who? Company?', 7, 24, 0),
(3, 'interview_last', 'Can we be last?', 1, 25, 0),
(3, 'concerns', 'Do you have any other major concerns about selling your home?', 1, 26, 0),
-- (3, 'trial_close', 'TRIAL CLOSE: Great, if we can address your concerns with a, b, c, are you ready to hire us to get your home SOLD so you can move on to x, y, z?', 7, 27, 0),
-- (53, 3, 'appt_timedate', 'Set Appt!', 1, 28, 0),
-- (54, 3, 'appt_location', 'Location', 1, 29, 0),
-- (55, 3, 'appt_met', 'Appt Met', 7, 30, 0),
-- (56, 3, 'agreement_signed', 'Listing Signed', 7, 31, 0),
-- (3, 'coming_soon', 'Coming Soon', 7, 32, 0),
-- (3, 'prop_type', 'Property Type', 1, 37, 0),
-- (3, 'subdivision', 'Subdivision', 1, 41, 0),
(3, 'features', 'Features', 1, 41, 0),
(3, 'area', 'Area/Subdivision', 1, 40, 0),
(3, 'bedrooms', 'Bedrooms', 5, 42, 0),
(3, 'baths', 'Baths', 6, 43, 0),
(3, 'half_baths', 'Half Baths', 6, 43, 0),
(3, 'sq_feet', 'Sq Feet', 5, 44, 0),
(3, 'pool', 'Pool', 7, 45, 0),
(3, 'waterfront', 'Waterfront', 7, 46, 0),
(3, 'expired_mls_number', 'Expired MLS #', 1, 33, 0),
(3, 'expired_date', 'Expired Date', 1, 33, 0),
(3, 'expired_price', 'Expired Price', 1, 33, 0),
-- (3, 'description', 'Description', 1, 47, 0),

-- =============== Lisitng Fields =========================================
(3, 'price_original', 'Original Price', 4, 38, 0),
(3, 'price_current', 'Current Price', 4, 39, 0),
(3, 'sale_type_ma', 'Sale Type', 4, 39, 0),
(3, 'mls_number', 'MLS #', 1, 33, 0),
(3, 'ivr_number', 'IVR #', 1, 34, 0),
(3, 'ivr_extension', 'Ext.', 1, 35, 0),


-- ===============Ex-Lisitng Fields=========================================

(3, 'lockbox_number', 'Lockbox #', 5, 35, 0),
(3, 'lockbox_status_ma', 'Lockbox Status', 5, 35, 0), -- Installed, Not Installed
(3, 'upfront_fee', 'Upfront Fee', 1, 41, 0),
(3, 'showing_instructions', 'Showing Instructions', 1, 41, 0),
(3, 'listing_expire_date', 'Listing Expire Date', 1, 41, 0),
(3, 'listing_date', 'Listing Date', 1, 41, 0),
(3, 'trial_close', 'Trial Close', 1, 10, 0);

--
-- Dumping data for table `address_states`
--

INSERT INTO `address_states` (`id`, `short_name`, `name`) VALUES
(1, 'AL', 'Alabama'),
(2, 'AK', 'Alaska'),
(3, 'AZ', 'Arizona'),
(4, 'AR', 'Arkansas'),
(5, 'CA', 'California'),
(6, 'CO', 'Colorado'),
(7, 'CT', 'Connecticut'),
(8, 'DE', 'Delaware'),
(9, 'FL', 'Florida'),
(10, 'GA', 'Georgia'),
(11, 'HI', 'Hawaii'),
(12, 'ID', 'Idaho'),
(13, 'IL', 'Illinois'),
(14, 'IN', 'Indiana'),
(15, 'IA', 'Iowa'),
(16, 'KS', 'Kansas'),
(17, 'KY', 'Kentucky'),
(18, 'LA', 'Louisiana'),
(19, 'ME', 'Maine'),
(20, 'MD', 'Maryland'),
(21, 'MA', 'Massachusetts'),
(22, 'MI', 'Michigan'),
(23, 'MN', 'Minnesota'),
(24, 'MS', 'Mississippi'),
(25, 'MO', 'Missouri'),
(26, 'MT', 'Montana'),
(27, 'NE', 'Nebraska'),
(28, 'NV', 'Nevada'),
(29, 'NH', 'New Hampshire'),
(30, 'NJ', 'New Jersey'),
(31, 'NM', 'New Mexico'),
(32, 'NY', 'New York'),
(33, 'NC', 'North Carolina'),
(34, 'ND', 'North Dakota'),
(35, 'OH', 'Ohio'),
(36, 'OK', 'Oklahoma'),
(37, 'OR', 'Oregon'),
(38, 'PA', 'Pennsylvania'),
(39, 'RI', 'Rhode Island'),
(40, 'SC', 'South Carolina'),
(41, 'SD', 'South Dakota'),
(42, 'TN', 'Tennessee'),
(43, 'TX', 'Texas'),
(44, 'UT', 'Utah'),
(45, 'VT', 'Vermont'),
(46, 'VA', 'Virginia'),
(47, 'WA', 'Washington'),
(48, 'WV', 'West Virginia'),
(49, 'WI', 'Wisconsin'),
(50, 'WY', 'Wyoming');

--
-- Dumping data for table `featured_areas_types`
--
INSERT INTO `featured_area_types` (`account_id`, `name`) VALUES
('2', 'Top Communities'),
('2', 'Top Schools'),
('2', 'Golf Communities'),
('2', 'Gated Communities'),
('2', 'New Construction'),
('2', 'Waterfront'),
('2', 'Oceanfront'),
('2', 'Ponte Vedra'),
('2', 'St Johns'),
('2', 'Mandarin'),
('2', 'Southside'),
('2', 'Intracoastal'),
('2', 'St Augustine');



--
-- Dumping data for table `featured_areas`
--
INSERT INTO `featured_areas` (`id`, `account_id`, `status_ma`, `name`, `url`, `count_homes`, `count_photo`, `min_price`, `max_price`, `avg_price`, `avg_bedrooms`, `avg_baths`, `avg_sf`, `avg_price_sf`, `avg_br1`, `avg_br2`, `avg_br3`, `avg_br4`, `avg_br5`, `avg_dom`, `query`, `is_default`, `is_deleted`) VALUES
(1, 2, 1, 'Mandarin', 'mandarin', 324, 6406, 47000, 6900000, 349580, 3.7, 2.8, 2517, 138, NULL, 135684, 199199, 339272, 802659, 143, '[{"area":[{"value":"%014-%","operator":"like","conjunctor":"or"},{"value":"%013-%","operator":"like"}]}]', 0, 0),
(2, 2, 1, 'Julington Creek Plantation', 'julingtoncreekplantation', 41, 1234, 109900, 950000, 333175, 4.2, 3.2, 2807, 118, NULL, NULL, 202320, 305095, 433184, 126, '[{"common_subdivision":[{"value":"%Julington Creek Plan%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Julington Creek Plan%","operator":"like","conjunctor":"or"}]}]\r\n', 0, 0),
(3, 2, 1, 'Ponte Vedra', 'pontevedra', 388, 9078, 89500, 7950000, 841682, 3.9, 3.7, 3264, 257, NULL, 266377, 533055, 832629, 1200254, 148, '[{"area":[{"value":"250-","operator":">","conjunctor":"and"},{"value":"270-","operator":"<"}]}]', 0, 0),
(4, 2, 0, 'Waterfront', 'waterfront', 944, 14238, 39000, 12000000, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '(`Navg. Wtr Frnt Y/N` = ''Y'' || `Navigable: Navigab', 0, 0),
(5, 2, 0, 'Luxury', 'luxury', 598, 10988, 1025000, 12000000, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '[{"area":[{"price":"1000000","operator":">"}]', 0, 0),
(6, 2, 1, 'Oakleaf Plantation', 'oakleafplantation', 28, 439, 85900, 378170, 184067, 3.9, 2.8, 2285, 80, NULL, NULL, 138900, 188625, 259900, 27, '[{"common_subdivision":[{"value":"%Oakleaf%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Oakleaf%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(7, 2, 1, 'Fleming Island', 'flemingisland', 18, 516, 90000, 699000, 261944, 3.8, 2.9, 2376, 110, NULL, NULL, 207087, 287466, 333375, 73, '[{"common_subdivision":[{"value":"%Fleming Island%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Fleming Island%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(8, 2, 1, 'Intracoastal', 'intracoastal', 6005, 105780, 0, 13500000, 285076, 3.4, 2.6, 2136, 133, 166277, 118671, 183171, 358801, 642649, 130, '[{"area":[{"value":"025-","operator":">="}]}]', 0, 0),
(9, 2, 0, 'World Golf Village', 'worldgolfvillage', 82, 1739, 109900, 1400000, 335565, 3.6, 2.8, 2601, 125, 0, 245037, 252870, 348668, 582780, NULL, '[{"common_subdivision":[{"value":"%World Golf Vill%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%World Golf Vill%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(10, 2, 0, 'Vacant Land', 'vacantland', 0, 0, 0, 0, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '[{"mls_property_type_id":[{"value":"C"}]}]', 0, 0),
(11, 2, 1, 'Eagle Harbor', 'eagleharbor', 50, 1675, 124900, 1150000, 347901, 4.3, 3.6, 2928, 118, NULL, 124900, 179650, 339160, 374168, 127, '[{"common_subdivision":[{"value":"%Eagle Harb%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Eagle Harb%","operator":"like","conjunctor":"or"}]}]\r\n', 0, 0),
(12, 2, 0, 'Marsh Landing', 'marshlanding', 72, 2101, 444900, 7850000, 1232425, 4.6, 4.0, 4667, 241, 0, 0, 557000, 1022423, 1136104, NULL, '[{"common_subdivision":[{"value":"%Marsh Landing%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Marsh Landing%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(13, 2, 1, 'Cimarrone', 'cimarrone', 9, 242, 255000, 579900, 399777, 4.6, 3.3, 3101, 128, NULL, NULL, NULL, 346960, 427766, 126, '[{"common_subdivision":[{"value":"%Cimarrone%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Cimarrone%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(14, 2, 1, 'Deercreek', 'deercreek', 15, 400, 280000, 599000, 417600, 4.5, 4.1, 3430, 121, NULL, NULL, 532700, 357257, 414500, 70, '[{"common_subdivision":[{"value":"%Deercreek%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Deercreek%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(15, 2, 1, 'Deerwood', 'deerwood', 27, 803, 189000, 1300000, 548885, 4.1, 4.1, 3678, 149, NULL, NULL, 395700, 450700, 707100, 82, '[{"common_subdivision":[{"value":"%Deerwood%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Deerwood%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(16, 2, 1, 'Eagle Landing', 'eaglelanding', 47, 631, 220940, 565000, 328302, 4.4, 3.8, 3251, 100, NULL, NULL, 265138, 308975, 353952, 91, '[{"common_subdivision":[{"value":"%Eagle Landing%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Eagle Landing%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(17, 2, 1, 'Glen Kernan', 'glenkernan', 15, 493, 550000, 1750000, 1008920, 4.5, 4.9, 4545, 221, NULL, NULL, 675000, 834983, 1181112, 119, '[{"common_subdivision":[{"value":"%Glen Kernan%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Glen Kernan%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(18, 2, 1, 'James Island', 'jamesisland', 9, 206, 245000, 495000, 350200, 4.9, 3.6, 2969, 117, NULL, NULL, NULL, 259950, 372816, 61, '[{"common_subdivision":[{"value":"%James Island%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%James Island%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(19, 2, 0, 'Oceanfront', 'oceanfront', 93, 2028, 350000, 8490000, 2181568, 4.0, 3.5, 3501, 582, 350000, 723600, 1126738, 2285122, 3265524, NULL, '(`Waterfront: Oceanfront` = ''1'')', 0, 0),
(20, 2, 1, 'Palencia', 'palencia', 32, 749, 249000, 1795000, 603615, 4.4, 4.3, 3712, 162, NULL, NULL, 297478, 506306, 784618, 259, '[{"common_subdivision":[{"value":"%Palencia%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Palencia%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(21, 2, 1, 'Queens Harbour', 'queensharbour', 46, 1198, 324900, 1900000, 754508, 4.3, 4.1, 3766, 200, NULL, NULL, 473133, 703721, 865189, 109, '[{"common_subdivision":[{"value":"%Queens Harb%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Queens Harb%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(22, 2, 0, 'San Marco', 'sanmarco', 116, 2281, 43900, 6000000, 636041, 3.4, 2.4, 2636, 183, 0, 171588, 331352, 875130, 2289625, NULL, '[{"area":[{"value":"%011-%","operator":"like"}]}]', 0, 0),
(23, 2, 1, 'Sawgrass', 'sawgrass', 32, 962, 199000, 1499000, 706728, 3.9, 3.8, 3455, 204, NULL, 219900, 498090, 813966, 907990, 129, '[{"common_subdivision":[{"value":"%Sawgrass%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Sawgrass%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(24, 2, 1, 'Epping Forest', 'eppingforest', 8, 249, 599000, 3500000, 1443500, 4.0, 4.6, 4408, 327, NULL, NULL, 626333, 1635000, 2133000, 269, '[{"common_subdivision":[{"value":"%Epping Forest%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Epping Forest%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(25, 2, 0, 'St Johns Golf & CC', 'stjohnsgolfcc', 42, 1068, 173000, 554499, 316695, 4.3, 3.0, 3010, 104, 0, 0, 250483, 292500, 354217, NULL, '[{"common_subdivision":[{"value":"%St Johns Golf%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%St Johns Golf%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(26, 2, 1, 'Jax Golf & CC', 'jaxgolfcc', 20, 622, 215000, 659000, 449325, 4.4, 3.6, 2901, 154, NULL, NULL, 327000, 419080, 505385, 82, '[{"common_subdivision":[{"value":"%Jax Golf%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Jax Golf%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(27, 2, 1, 'Orange Park CC', 'orangeparkcc', 13, 345, 200000, 430000, 300561, 4.1, 3.3, 3033, 99, NULL, NULL, 349900, 277750, 389950, 67, '[{"common_subdivision":[{"value":"%Orange Park CC%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Orange Park CC%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(28, 2, 0, 'Ortega', 'ortega', 76, 760, 16200, 299900, 81830, 3.1, 1.7, 1492, 53, 0, 75308, 80637, 89229, 83000, NULL, '{"area":{"value":"%053-%","operator":"like","conjunctor":"AND"}}', 0, 0),
(29, 2, 0, 'Bartram Springs', 'bartramsprings', 56, 1021, 89000, 325000, 207814, 4.0, 2.7, 2468, 84, 0, 100000, 174608, 213530, 240014, NULL, '[{"common_subdivision":[{"value":"%Bartram Springs%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Bartram Springs%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(30, 2, 1, 'Worthington Park', 'worthingtonpark', 8, 177, 299900, 569000, 381898, 4.4, 3.5, 3132, 121, NULL, NULL, NULL, 340976, 450101, 199, '[{"common_subdivision":[{"value":"%Worthington Park%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Worthington Park%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(31, 2, 0, 'Kensington', 'kensington', 21, 262, 239900, 489900, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '{"subdivision":[{"value":"%Kensington%","operator":"like"}],"area":{"value":"%312-%","operator":"like","conjunctor":"AND"}}', 0, 0),
(32, 2, 0, 'Murabella', 'murabella', 46, 701, 159900, 288990, 213742, 4.1, 2.7, 2584, 84, 0, 0, 183493, 208010, 244287, NULL, '[{"common_subdivision":[{"value":"%Murabella%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Murabella%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(33, 2, 1, 'Nocatee', 'nocatee', 57, 590, 207387, 750000, 367318, 3.8, 3.5, 2897, 126, NULL, NULL, 331772, 382925, 372757, 56, '[{"common_subdivision":[{"value":"%Nocatee%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Nocatee%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(34, 2, 0, 'Austin Park', 'austinpark', 11, 80, 229990, 409900, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Austin Park%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Austin Park%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(35, 2, 0, 'World Golf New Homes', 'worldgolfnewhomes', 0, 0, 0, 0, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '((`Legal Name of Subdiv` like ''%World Golf Vill%'' ', 0, 0),
(36, 2, 0, 'Eagle Landing New Homes', 'eaglelandingnewhomes', 30, 419, 259900, 745900, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '', 0, 0),
(37, 2, 0, 'King & Bear', 'kingandbear', 11, 230, 205000, 895000, 410609, 4.3, 3.4, 3174, 124, 0, 0, 339000, 375800, 480725, NULL, ' (\r\r							(\r\r							((`Legal Name of Subdiv` like', 0, 0),
(38, 2, 0, 'King & Bear New Homes', 'kingandbearnewhomes', 0, 0, 0, 0, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, ' (\r\r							(\r\r							((`Legal Name of Subdiv` like', 0, 0),
(39, 2, 0, 'Coastal Oaks', 'coastaloaks', 1, 12, 467900, 467900, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Coastal Oaks%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Coastal Oaks%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(40, 2, 0, 'Doctors Inlet Reserve', 'doctorsinletreserve', 17, 185, 144900, 525000, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Doctors Inlet%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Doctors Inlet%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(41, 2, 0, 'Eagle Harbor New Homes', 'eagleharbornewhomes', 7, 58, 289900, 1073000, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '', 0, 0),
(42, 2, 0, 'World Golf Custom Homes', 'worldgolfcustomhomes', 0, 0, 0, 0, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '((`Legal Name of Subdiv` like ''%World Golf Vill%'' ', 0, 0),
(43, 2, 1, 'South Hampton', 'southhampton', 13, 395, 227900, 525000, 354761, 4.7, 3.7, 3245, 109, NULL, NULL, NULL, 303700, 377455, 115, '[{"common_subdivision":[{"value":"%South Hampton%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%South Hampton%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(44, 2, 1, 'St Johns Forest', 'stjohnsforest', 14, 340, 214900, 899900, 371260, 4.3, 3.4, 3123, 118, NULL, NULL, 268000, 303104, 500963, 163, '[{"common_subdivision":[{"value":"%St Johns Forest%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%St Johns Forest%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(45, 2, 1, 'Plantation at Ponte Vedra', 'plantationatpontevedra', 32, 1042, 349900, 2595000, 924118, 4.0, 4.8, 4325, 213, NULL, NULL, 759057, 847166, 1349000, 34, '[{"common_subdivision":[{"value":"%The Plantation%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%The Plantation%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(46, 2, 0, 'Azalea Point', 'azaleapoint', 12, 253, 309000, 525000, 377892, 3.7, 2.4, 2350, 163, 0, 0, 337817, 390950, 472000, NULL, '[{"common_subdivision":[{"value":"%Azalea Point%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Azalea Point%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(47, 2, 0, 'Pace Island', 'paceisland', 17, 457, 149900, 1375000, 360088, 4.0, 2.8, 2890, 113, 0, 0, 294950, 252431, 1125000, NULL, '[{"common_subdivision":[{"value":"%Pace Island%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Pace Island%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(48, 2, 0, 'Paradise Moorings', 'paradisemoorings', 15, 420, 314900, 1495000, 620027, 4.7, 3.8, 3768, 156, 0, 0, 314900, 434360, 589117, NULL, '[{"common_subdivision":[{"value":"%Paradise Moorings%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Paradise Moorings%","operator":"like"}]}]', 0, 0),
(49, 2, 0, 'Dolphin Cove', 'dolphincove', 11, 117, 90000, 269000, 175782, 3.1, 2.0, 1610, 109, 0, 90000, 164450, 264000, 0, NULL, '[{"common_subdivision":[{"value":"%Dolphin Cove%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Dolphin Cove%","operator":"like"}]}]', 0, 0),
(50, 2, 0, 'Ponte Vedra Blvd', 'pontevedrablvd', 65, 1677, 249000, 8490000, 2431426, 4.1, 3.7, 3957, 585, 0, 798333, 1381069, 2225961, 3038856, NULL, '{"street_name":[{"value":"%Ponte Vedra%","operator":"like"}],"street_suffix":[{"value":"%Blvd%","operator":"like"}]}', 0, 0),
(51, 2, 0, 'Hidden Hills', 'hiddenhills', 30, 687, 130000, 595000, 324323, 4.0, 2.7, 3020, 107, 0, 130000, 228940, 319165, 432743, NULL, '[{"common_subdivision":[{"value":"%Hidden Hills%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Hidden Hills%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(52, 2, 0, 'Windsor Parke', 'windsorparke', 8, 120, 249500, 339200, 292925, 3.8, 2.4, 2380, 123, 0, 0, 256075, 332100, 327450, NULL, '[{"common_subdivision":[{"value":"%Windsor Park%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Windsor Park%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(53, 2, 0, 'Estates of Ponte Vedra', 'estatesofpontevedra', 0, 0, 0, 0, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Estates of Ponte Vedra%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Estates of Ponte Vedra%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(54, 2, 0, 'Fairfield', 'fairfield', 8, 192, 214900, 490000, 293775, 3.0, 2.0, 1878, 161, 0, 214900, 297550, 350000, 0, NULL, '[{"common_subdivision":[{"value":"%Fairfield%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Fairfield%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(55, 2, 0, 'Fiddlers Marsh', 'fiddlersmarsh', 1, 14, 199900, 199900, 199900, 3.0, 2.0, 1684, 119, 0, 0, 199900, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Fiddlers Marsh%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Fiddlers Marsh%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(56, 2, 0, 'Florentine', 'florentine', 3, 43, 139900, 170000, 151600, 3.3, 2.0, 2114, 77, 0, 0, 142400, 170000, 0, NULL, '[{"common_subdivision":[{"value":"%Florentine%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Florentine%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(57, 2, 0, 'Las Palmas', 'laspalmas', 1, 22, 535000, 535000, 535000, 4.0, 4.0, 3188, 168, 0, 0, 0, 535000, 0, NULL, '[{"common_subdivision":[{"value":"%Las Palmas%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Las Palmas%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(58, 2, 0, 'L Atrium', 'latrium', 10, 114, 165000, 259000, 200210, 2.6, 2.1, 1519, 133, 0, 182475, 212033, 0, 0, NULL, '[{"common_subdivision":[{"value":"%L Atrium%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%L Atrium%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(59, 2, 0, 'Marsh Harbor', 'marshharbor', 7, 151, 360000, 1335000, 676143, 4.4, 3.7, 3627, 183, 0, 0, 0, 759750, 564667, NULL, '[{"common_subdivision":[{"value":"%Marsh Harbor%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Marsh Harbor%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(60, 2, 0, 'Old Palm Valley', 'oldpalmvalley', 4, 92, 312000, 443900, 392700, 4.0, 3.0, 2424, 162, 0, 0, 0, 392700, 0, NULL, '[{"common_subdivision":[{"value":"%Old Palm Valley%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Old Palm Valley%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(61, 2, 1, 'Aberdeen', 'aberdeen', 30, 1044, 124000, 339997, 264306, 4.1, 3.3, 2731, 96, NULL, 124000, 239630, 257831, 305667, 93, '[{"common_subdivision":[{"value":"%Aberdeen%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Aberdeen%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(62, 2, 0, 'Bartram Plantation', 'bartramplantation', 9, 256, 429900, 649000, 529289, 4.4, 4.1, 3856, 137, 0, 0, 0, 528760, 529950, NULL, '[{"common_subdivision":[{"value":"%Bartram Plantation%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Bartram Plantation%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(63, 2, 0, 'Colony at Greenbriar', 'colonyatgreenbriar', 8, 146, 315000, 525000, 365233, 4.4, 3.9, 3327, 109, 0, 0, 0, 335392, 414967, NULL, '[{"common_subdivision":[{"value":"%Colony at Greenbriar%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Colony at Greenbriar%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(64, 2, 1, 'Durbin Crossing', 'durbincrossing', 55, 690, 174900, 469000, 305697, 4.2, 3.4, 2844, 107, NULL, NULL, 226978, 297445, 344800, 132, '[{"common_subdivision":[{"value":"%Durbin Crossing%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Durbin Crossing%","operator":"like"}]}]', 0, 0),
(65, 2, 0, 'Greenbriar Plantation', 'greenbriarplantation', 0, 0, 0, 0, 0, 0.0, 0.0, 0, 0, 0, 0, 0, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Greenbriar Plantation%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Greenbriar Plantation%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(66, 2, 0, 'Heritage Landing', 'heritagelanding', 37, 876, 109900, 287400, 186291, 4.2, 2.7, 2533, 75, 0, 150000, 130140, 178004, 218999, NULL, '[{"common_subdivision":[{"value":"%Heritage Landing%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Heritage Landing%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(67, 2, 0, 'Mallard Landing', 'mallardlanding', 5, 111, 249900, 400000, 297860, 4.2, 2.4, 2369, 126, 0, 0, 274500, 262450, 344950, NULL, '[{"common_subdivision":[{"value":"%Mallard Landing%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Mallard Landing%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(68, 2, 0, 'Odoms Mill', 'odomsmill', 9, 178, 341910, 479900, 409401, 4.3, 3.4, 3077, 133, 0, 0, 364900, 430975, 398953, NULL, '[{"common_subdivision":[{"value":"%Odoms Mill%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Odoms Mill%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(69, 2, 0, 'Payasada Estates', 'payasadaestates', 5, 36, 624900, 698000, 654540, 4.4, 3.4, 3781, 174, 0, 0, 0, 641567, 674000, NULL, '[{"common_subdivision":[{"value":"%Payasada Estates%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Payasada Estates%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(70, 2, 1, 'Plantation Oaks', 'plantationoaks', 10, 330, 175000, 1099000, 684290, 4.8, 4.6, 3975, 172, NULL, NULL, 659000, 387000, 924333, 109, '[{"common_subdivision":[{"value":"%Plantation Oaks%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Plantation Oaks%","operator":"like","conjunctor":"or"}]}]', 0, 0),
(71, 2, 1, 'Sawmill Lakes', 'sawmilllakes', 4, 84, 287900, 399900, 359300, 4.0, 3.0, 2351, 152, NULL, NULL, NULL, 359300, NULL, NULL, '[{"common_subdivision":[{"value":"%Sawmill Lakes%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Sawmill Lakes%","operator":"like"}]}]', 0, 0),
(72, 2, 1, 'Walden Chase', 'waldenchase', 7, 126, 189500, 325000, 261885, 4.0, 3.0, 2497, 104, NULL, NULL, 240000, 253640, 325000, 22, '[{"common_subdivision":[{"value":"%Walden Chase%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Walden Chase%","operator":"like"}]}]', 0, 0),
(73, 2, 0, 'Woodlands', 'woodlands', 4, 108, 485000, 659000, 586000, 4.0, 3.5, 3024, 193, 0, 0, 0, 586000, 0, NULL, '[{"common_subdivision":[{"value":"%Woodlands%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Woodlands%","operator":"like"}]}]', 0, 0),
(74, 2, 0, 'Ponte Vedra by Sea', 'pontevedrabysea', 5, 116, 525000, 1260000, 896200, 4.8, 4.0, 4134, 214, 0, 0, 0, 525000, 989000, NULL, '[{"common_subdivision":[{"value":"%Ponte Vedra by Sea%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Ponte Vedra by Sea%","operator":"like"}]}]', 0, 0),
(75, 2, 0, 'Port St John', 'portstjohn', 1, 13, 600000, 600000, 600000, 5.0, 3.0, 3930, 153, 0, 0, 0, 0, 600000, NULL, '[{"common_subdivision":[{"value":"%Port St Johns%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Port St Johns%","operator":"like"}]}]', 0, 0),
(76, 2, 0, 'River Oaks Plantation', 'riveroaksplantation', 12, 318, 199000, 825000, 421950, 4.3, 3.3, 2921, 142, 0, 0, 210500, 516200, 429600, NULL, '[{"common_subdivision":[{"value":"%River Oaks Plan%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%River Oaks Plan%","operator":"like"}]}]', 0, 0),
(77, 2, 0, 'Sandy Creek', 'sandycreek', 9, 248, 211900, 294900, 240322, 3.9, 2.6, 2282, 106, 0, 0, 241400, 230866, 294900, NULL, '[{"common_subdivision":[{"value":"%Sandy Creek%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Sandy Creek%","operator":"like"}]}]', 0, 0),
(78, 2, 0, 'Stonehurst Plantation', 'stonehurstplantation', 10, 185, 169900, 229000, 189423, 3.7, 2.2, 1846, 103, 0, 0, 189575, 189357, 0, NULL, '[{"common_subdivision":[{"value":"%Stonehurst Plan%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Stonehurst Plan%","operator":"like"}]}]', 0, 0),
(79, 2, 0, 'Admirals Inlet', 'admiralsinlet', 3, 98, 424900, 660000, 519967, 4.3, 3.7, 3373, 158, 0, 0, 0, 449950, 660000, NULL, '[{"common_subdivision":[{"value":"%Admirals Inlet%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Admirals Inlet%","operator":"like"}]}]', 0, 0),
(80, 2, 0, 'Arbor Glade', 'arborglade', 3, 27, 232800, 240000, 237567, 4.3, 3.0, 3157, 75, 0, 0, 0, 236400, 239900, NULL, '(`Area` like ''%041-%'') AND (`Navg. Wtr Frnt Y/N` =', 0, 0),
(81, 2, 0, 'Cormorant Landing', 'cormorantlanding', 9, 190, 235000, 426700, 331389, 3.8, 2.4, 3045, 111, 0, 0, 308000, 338071, 0, NULL, '[{"common_subdivision":[{"value":"%Cormorant Landing%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Cormorant Landing%","operator":"like"}]}]', 0, 0),
(82, 2, 0, 'Creekside', 'creekside', 1, 18, 219900, 219900, 219900, 4.0, 3.0, 2874, 77, 0, 0, 0, 219900, 0, NULL, '[{"common_subdivision":[{"value":"%Creekside%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Creekside%","operator":"like"}]}]', 0, 0),
(83, 2, 0, 'Crusselle Cove', 'crussellecove', 2, 69, 399000, 525000, 462000, 5.0, 4.0, 3497, 133, 0, 0, 0, 0, 462000, NULL, '[{"common_subdivision":[{"value":"%Crusselle Cove%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Crusselle Cove%","operator":"like"}]}]', 0, 0),
(84, 2, 0, 'Cypress Bay', 'cypressbay', 5, 130, 225000, 349900, 305960, 4.2, 2.8, 2907, 105, 0, 0, 225000, 325000, 327400, NULL, '[{"common_subdivision":[{"value":"%Cypress Bay%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Cypress Bay%","operator":"like"}]}]', 0, 0),
(85, 2, 0, 'Greenland Chase', 'greenlandchase', 12, 205, 165000, 325000, 223775, 4.0, 2.7, 2491, 90, 0, 0, 165000, 231130, 209000, NULL, '[{"common_subdivision":[{"value":"%Greenland Chase%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Greenland Chase%","operator":"like"}]}]', 0, 0),
(86, 2, 0, 'Oldfield Point', 'oldfieldpoint', 2, 44, 199900, 254900, 227400, 4.0, 2.5, 2506, 91, 0, 0, 0, 227400, 0, NULL, '[{"common_subdivision":[{"value":"%Oldfield Point%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Oldfield Point%","operator":"like"}]}]', 0, 0),
(87, 2, 0, 'Plantation South', 'plantationsouth', 2, 67, 254900, 400000, 327450, 4.0, 2.0, 2681, 122, 0, 0, 0, 327450, 0, NULL, '[{"common_subdivision":[{"value":"%Plantation South%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Plantation South%","operator":"like"}]}]', 0, 0),
(88, 2, 0, 'River Bay Plantation', 'riverbayplantation', 1, 44, 549900, 549900, 549900, 5.0, 5.0, 4785, 115, 0, 0, 0, 0, 549900, NULL, '[{"common_subdivision":[{"value":"%River Bay Plan%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%River Bay Plan%","operator":"like"}]}]', 0, 0),
(89, 2, 0, 'River Oaks', 'riveroaks', 2, 70, 264900, 309000, 286950, 4.5, 3.0, 2615, 110, 0, 0, 0, 264900, 309000, NULL, '[{"common_subdivision":[{"value":"%River Oaks%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%River Oaks%","operator":"like"}]}]', 0, 0),
(90, 2, 0, 'Tuscan Oaks', 'tuscanoaks', 1, 98, 379900, 379900, 379900, 5.0, 3.0, 4418, 86, 0, 0, 0, 0, 379900, NULL, '[{"common_subdivision":[{"value":"%Tuscan Oaks%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Tuscan Oaks%","operator":"like"}]}]', 0, 0),
(91, 2, 0, 'Waterford Estates', 'waterfordestates', 17, 322, 159000, 295000, 239953, 4.0, 2.9, 2601, 93, 0, 0, 191475, 247044, 272475, NULL, '[{"common_subdivision":[{"value":"%Waterford Estates%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Waterford Estates%","operator":"like"}]}]', 0, 0),
(92, 2, 0, 'Broadwater', 'broadwater', 2, 45, 374999, 424900, 399950, 4.0, 2.5, 2805, 143, 0, 0, 0, 399950, 0, NULL, '[{"common_subdivision":[{"value":"%Broadwater%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Broadwater%","operator":"like"}]}]', 0, 0),
(93, 2, 1, 'East Hampton', 'easthampton', 7, 185, 229900, 395000, 305528, 3.9, 3.1, 2886, 105, NULL, NULL, 279700, 296075, 395000, 8, '[{"common_subdivision":[{"value":"%East Hampton%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%East Hampton%","operator":"like"}]}]', 0, 0),
(94, 2, 1, 'Hampton Glen', 'hamptonglen', 2, 46, 225000, 631900, 428450, 4.0, 3.0, 2997, 142, NULL, NULL, NULL, 428450, NULL, NULL, '[{"common_subdivision":[{"value":"%Hampton Glen%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Hampton Glen%","operator":"like"}]}]', 0, 0),
(95, 2, 1, 'Hampton Park', 'hamptonpark', 7, 163, 269000, 649900, 410228, 3.9, 3.9, 3105, 132, NULL, NULL, 432000, 339425, 649900, 49, '[{"common_subdivision":[{"value":"%Hampton Park%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Hampton Park%","operator":"like"}]}]', 0, 0),
(96, 2, 1, 'Highland Glen', 'highlandglen', 4, 155, 397800, 559900, 481650, 4.5, 4.3, 3450, 139, NULL, NULL, NULL, 459200, NULL, NULL, '[{"common_subdivision":[{"value":"%Highland Glen%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Highland Glen%","operator":"like"}]}]', 0, 0),
(97, 2, 0, 'Isle of Palms', 'isleofpalms', 29, 574, 149900, 1100000, 375005, 3.5, 2.5, 2150, 171, 0, 378000, 299671, 500192, 525475, NULL, '[{"common_subdivision":[{"value":"%Isle of Palms%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Isle of Palms%","operator":"like"}]}]', 0, 0),
(98, 2, 0, 'Johns Creek (Southside)', 'johnscreek(southside)', 17, 291, 165000, 285900, 231929, 3.5, 2.3, 2044, 114, 0, 0, 217633, 242600, 285900, NULL, '[{"common_subdivision":[{"value":"%Johns Creek%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Johns Creek%","operator":"like"}]}]', 0, 0),
(99, 2, 0, 'Pablo Bay', 'pablobay', 18, 429, 260000, 667500, 425211, 4.1, 2.9, 2864, 150, 0, 0, 339950, 428500, 457975, NULL, '[{"common_subdivision":[{"value":"%Pablo Bay%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Pablo Bay%","operator":"like"}]}]', 0, 0),
(100, 2, 1, 'Pablo Creek Reserve', 'pablocreekreserve', 5, 115, 799000, 3800000, 1487780, 4.6, 5.4, 5339, 278, NULL, NULL, NULL, 812966, 3800000, 25, '[{"common_subdivision":[{"value":"%Pablo Creek Reserve%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Pablo Creek Reserve%","operator":"like"}]}]', 0, 0),
(101, 2, 1, 'Old Mill Branch', 'oldmillbranch', 2, 45, 305000, 319000, 312000, 4.0, 3.0, 2468, 126, NULL, NULL, NULL, 312000, NULL, NULL, '[{"common_subdivision":[{"value":"%Old Mill Branch%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Old Mill Branch%","operator":"like"}]}]', 0, 0),
(102, 2, 0, 'Wyngate Forest', 'wyngateforest', 2, 48, 224900, 319000, 271950, 5.0, 3.0, 2887, 96, 0, 0, 0, 224900, 0, NULL, '[{"common_subdivision":[{"value":"%Wyngate Forest%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Wyngate Forest%","operator":"like"}]}]', 0, 0),
(103, 2, 1, 'Timberlin Parc', 'timberlinparc', 6, 89, 217000, 342000, 299148, 4.0, 3.2, 2584, 115, NULL, NULL, 217000, 315722, 315000, 4, '[{"common_subdivision":[{"value":"%Timberlin Parc%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Timberlin Parc%","operator":"like"}]}]', 0, 0),
(104, 2, 0, 'Carriage Crossing', 'carriagecrossing', 6, 103, 149900, 179900, 159533, 3.0, 2.0, 1508, 106, 0, 0, 159533, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Carriage Crossing%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Carriage Crossing%","operator":"like"}]}]', 0, 0),
(105, 2, 0, 'Waverly', 'waverly', 1, 8, 335900, 335900, 335900, 4.0, 3.0, 3102, 108, 0, 0, 0, 335900, 0, NULL, '[{"common_subdivision":[{"value":"%Waverly%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Waverly%","operator":"like"}]}]', 0, 0),
(106, 2, 0, 'Cormorant Creek', 'cormorantcreek', 1, 18, 219900, 219900, 219900, 4.0, 3.0, 2874, 77, 0, 0, 0, 219900, 0, NULL, '[{"common_subdivision":[{"value":"%Cormorant Creek%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Cormorant Creek%","operator":"like"}]}]', 0, 0),
(107, 2, 0, 'Sweetwater Creek', 'sweetwatercreek', 27, 453, 129900, 295000, 213170, 3.7, 2.5, 2314, 93, 0, 0, 180200, 225863, 260000, NULL, '[{"common_subdivision":[{"value":"%Sweetwater Creek%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Sweetwater Creek%","operator":"like"}]}]', 0, 0),
(108, 2, 0, 'Tala Cay', 'talacay', 2, 54, 675000, 2499900, 1587450, 6.5, 4.0, 5668, 255, 0, 0, 0, 0, 0, NULL, '[{"common_subdivision":[{"value":"%Tala Cay%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Tala Cay%","operator":"like"}]}]', 0, 0),
(109, 2, 0, 'Ponte Vedra Beach Oceanfront', 'pontevedrabeachoceanfront', 52, 1273, 599000, 8490000, 2605463, 4.2, 3.8, 3999, 606, 0, 822500, 1257990, 2400115, 3867400, NULL, '((`Waterfront: Oceanfront` = ''1'') AND (`Area` > ''2', 0, 0),
(110, 2, 0, 'Jacksonville Beach Oceanfront', 'jacksonvillebeachoceanfront', 5, 89, 499000, 3650000, 1879600, 3.2, 2.8, 2982, 636, 0, 499000, 1525000, 2924500, 0, NULL, '(`Waterfront: Oceanfront` = ''1'') AND (`Region` = ''', 0, 0),
(111, 2, 0, 'Atlantic Beach Oceanfront', 'atlanticbeachoceanfront', 8, 182, 1000000, 6500000, 2971125, 4.0, 3.8, 3513, 848, 0, 1000000, 2499000, 3000000, 3756667, NULL, '', 0, 0),
(112, 2, 0, 'Neptune Beach Oceanfront', 'neptunebeachoceanfront', 9, 188, 899000, 2490000, 1394778, 4.3, 3.4, 3104, 446, 0, 0, 1049500, 1277800, 1575000, NULL, '(`Waterfront: Oceanfront` = ''1'') AND (`Region` = ''', 0, 0),
(113, 2, 0, 'Oceanfront Condos', 'oceanfrontcondos', 106, 1778, 235000, 4000000, 814526, 2.8, 2.5, 2054, 366, 560000, 475443, 803241, 1664067, 0, NULL, '((`Waterfront: Oceanfront` = ''1'') AND (`Area` > ''2', 0, 0),
(114, 2, 0, 'Mandarin Waterfront', 'mandarinwaterfront', 27, 833, 150000, 6900000, 1543656, 4.1, 3.3, 4042, 341, 0, 0, 575128, 1329915, 3222800, NULL, '(`Area` like ''%014-%'' || `Area` like ''%013-%'') AND', 0, 0),
(115, 2, 0, 'San Marco Waterfront', 'sanmarcowaterfront', 33, 957, 199500, 6000000, 1905509, 4.0, 3.9, 5003, 341, 0, 461167, 987057, 1649300, 2706000, NULL, '(`Area` like ''%011-%'' || `Area` like ''%012-%'') AND', 0, 0),
(116, 2, 0, 'St Johns County Waterfront', 'stjohnscountywaterfront', 42, 1483, 280000, 5800000, 1189393, 4.2, 3.5, 3830, 290, 0, 836667, 635222, 1109621, 1451718, NULL, '(`Area` like ''%301-%'' || `Area` like ''%302-%'') AND', 0, 0),
(117, 2, 0, 'Doctors Lake & Orange Park Waterfront', 'doctorslakeopwaterfront', 63, 1666, 242900, 1589000, 703249, 3.9, 3.0, 3208, 229, 899000, 0, 596171, 719307, 810464, NULL, '{"area":[{"value":"250-","operator":">"},{"value":"270-","operator":"<","conjunctor":"AND"}]}', 0, 0),
(118, 2, 0, 'Queens Harbour Waterfront', 'queensharbourwaterfront', 24, 695, 475000, 2799000, 1316538, 4.8, 4.5, 5060, 252, 0, 0, 475000, 1224271, 1546200, NULL, '[{"common_subdivision":[{"value":"%Queens Harb%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Queens Harb%","operator":"like"}]}]', 0, 0),
(119, 2, 0, 'Ortega Waterfront', 'ortegawaterfront', 4, 147, 74000, 395000, 224475, 3.8, 2.3, 2154, 126, 0, 0, 136950, 395000, 229000, NULL, '(`Area` like ''%053-%'' || `Area` like ''%063-%'') AND', 0, 0),
(120, 2, 0, 'Mariner Point Waterfront', 'marinerpointwaterfront', 2, 47, 548300, 1500000, 1024150, 4.0, 3.0, 4781, 205, 0, 0, 0, 1024150, 0, NULL, '[{"common_subdivision":[{"value":"%Mariner Point%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Mariner Point%","operator":"like"}]}]', 0, 0),
(121, 2, 0, 'Arlington Waterfront', 'arlingtonwaterfront', 26, 542, 177500, 1095000, 525050, 4.0, 3.1, 3332, 163, 0, 595000, 431538, 427911, 721829, NULL, '', 0, 0),
(122, 2, 0, 'Ft Caroline Waterfront', 'ftcarolinewaterfront', 17, 390, 325000, 2775000, 841450, 4.0, 3.2, 3696, 212, 0, 0, 341760, 782428, 1589000, NULL, '(`Area` like ''%042-%'') AND (`Navg. Wtr Frnt Y/N` =', 0, 0),
(123, 2, 0, 'Hecksher Drive area Waterfront', 'hecksherdriveareawaterfront', 37, 980, 155000, 1999999, 599196, 3.5, 2.5, 2871, 213, 0, 421300, 452069, 722883, 515000, NULL, '', 0, 0),
(124, 2, 0, 'Intracoastal (Southside) Waterfront', 'intracoastalss', 48, 1183, 114500, 1900000, 495780, 3.7, 2.6, 2670, 186, 0, 378000, 434229, 474803, 600150, NULL, '(`Region` like ''%02-%'') AND (`Navg. Wtr Frnt Y/N` ', 0, 0),
(125, 2, 0, 'Intracoastal (Ponte Vedra) Waterfront', 'intracoastalpontevedra', 55, 1468, 249000, 7850000, 2452285, 4.4, 4.1, 4933, 480, 0, 250000, 1516488, 2059262, 2854230, NULL, '(`Area` > ''250-'') AND (`Area` < ''264-'') AND (`Navg', 0, 0),
(126, 2, 1, 'Jacksonville', 'jacksonville', 5543, 100206, 5900, 13500000, 316645, 3.5, 2.7, 2254, 140, 168126, 132757, 190167, 371444, 685376, 129, '[{"county":[{"value":"%Duval%","operator":"like","conjunctor":"or"},{"value":"%Clay%","operator":"like","conjunctor":"or"},{"value":"%St. Johns%","operator":"like"}]}]', 1, 0),
(127, 2, 0, 'Jacksonville Foreclosure', 'jacksonvilleforeclosure', 310, 2975, 6500, 1250000, 132595, 3.3, 2.0, 1836, 65, 0, 58957, 101218, 175857, 287523, NULL, '(`OwnerApprPublicMktg: Foreclosed` = ''1'')', 0, 0),
(128, 2, 0, 'Magnolia Point', 'magnoliapoint', 36, 842, 125000, 524700, 292800, 4.1, 2.9, 2676, 109, 0, 164900, 245425, 275198, 426860, NULL, '[{"common_subdivision":[{"value":"%Magnolia Point%","operator":"like","conjunctor":"or"}]},{"legal_subdivision":[{"value":"%Magnolia Point%","operator":"like"}]}]', 0, 0),
(129, 2, 1, 'Jacksonville', 'all', 6896, 121942, 5900, 13500000, 292271, 3.4, 2.6, 2164, 135, 160042, 120051, 183051, 361005, 678927, 132, '[{"price":[{"value":"1","operator":">","conjunctor":"AND"}]}]', 0, 0),
(130, 2, 1, 'Southside', 'southside', 495, 9227, 15900, 3800000, 294960, 3.5, 2.7, 2271, 129, 50166, 114276, 175498, 345013, 677623, 82, '[{"area":[{"value":"020-","operator":">","conjunctor":"and"},{"value":"030-","operator":"<="}]}]', 0, 0),
(131, 2, 1, 'St Johns County', 'stjohnscounty', 1286, 28561, 10000, 7950000, 517122, 3.9, 3.3, 2816, 183, 251175, 273226, 350437, 504227, 716226, 137, '[{"county":[{"value":"St. Johns","operator":"=","conjunctor":"and"}]}]', 0, 0);

--
-- Dumping data for table `featured_area_type_lu`
--
INSERT INTO `featured_area_type_lu` (`featured_area_type_id`, `featured_area_id`) VALUES
(1, 3),
(10, 1),
(1, 1),
(2, 6),
(5, 6),
(1, 7),
(2, 7),
(3, 7),
(11, 8),
(12, 8),
(1, 11),
(2, 11),
(3, 11),
(5, 11),
(1, 13),
(2, 13),
(3, 13),
(4, 13),
(5, 13),
(9, 13),
(1, 14),
(2, 14),
(3, 14),
(4, 14),
(11, 14),
(1, 15),
(2, 15),
(3, 15),
(4, 15),
(1, 16),
(2, 16),
(3, 16),
(5, 16),
(1, 17),
(2, 17),
(3, 17),
(4, 17),
(5, 17),
(11, 17),
(1, 18),
(2, 18),
(4, 18),
(11, 18),
(1, 20),
(2, 20),
(3, 20),
(4, 20),
(5, 20),
(13, 20),
(1, 21),
(3, 21),
(4, 21),
(6, 21),
(11, 21),
(12, 21),
(1, 23),
(2, 23),
(3, 23),
(4, 23),
(7, 23),
(8, 23),
(1, 24),
(4, 24),
(1, 25),
(2, 25),
(3, 25),
(4, 25),
(9, 25),
(13, 25),
(1, 26),
(2, 26),
(3, 26),
(4, 26),
(12, 26),
(11, 26),
(1, 27),
(3, 27),
(4, 27),
(1, 30),
(2, 30),
(4, 30),
(9, 30),
(5, 30),
(1, 131),
(2, 131),
(9, 131),
(8, 131),
(3, 131),
(4, 131),
(5, 131),
(1, 130),
(2, 130),
(3, 130),
(4, 130),
(5, 130),
(11, 130),
(12, 130),
(1, 126),
(2, 126),
(3, 126),
(4, 126),
(5, 126),
(6, 126),
(7, 126),
(8, 126),
(9, 126),
(10, 126),
(11, 126),
(12, 126),
(13, 126),
(1, 95),
(2, 95),
(4, 95),
(11, 95),
(1, 94),
(2, 94),
(11, 94),
(1, 93),
(2, 93),
(11, 93),
(1, 96),
(2, 96),
(11, 96),
(1, 100),
(2, 100),
(4, 100),
(5, 100),
(12, 100),
(11, 100),
(1, 101),
(11, 101),
(1, 103),
(2, 103),
(11, 103),
(1, 33),
(2, 33),
(4, 33),
(5, 33),
(8, 33),
(9, 33),
(1, 45),
(2, 45),
(3, 45),
(4, 45),
(5, 45),
(8, 45),
(1, 44),
(2, 44),
(4, 44),
(5, 44),
(9, 44),
(1, 43),
(2, 43),
(3, 43),
(9, 43),
(1, 64),
(2, 64),
(5, 64),
(9, 64),
(1, 61),
(2, 61),
(5, 61),
(9, 61),
(1, 70),
(2, 70),
(4, 70),
(8, 70),
(2, 71),
(1, 71),
(8, 71),
(1, 72),
(2, 72),
(8, 72),
(9, 72);

--
-- Dumping data for table `mls_feature_groups`
--

INSERT INTO `mls_feature_groups` (`id`, `display_name`) VALUES
(1, 'System Fields'),
(2, 'Core Fields'),
(3, 'Interior Features'),
(4, 'Exterior Features'),
(5, 'Property Features'),
(6, 'School Information'),
(7, 'Listing Agent Fields'),
(8, 'Photo Resources'),
(9, 'Listing Office Fields'),
(10, 'CoListing Agent Fields');

--
-- Dumping data for table `mls_features`
--

INSERT INTO `mls_features` (`id`, `mls_feature_group_id`, `column_name`, `display_name`, `is_deleted`) VALUES
(1, 1, 'mls_identifier', 'MLS Identifier', 0),
(2, 1, 'system_listing_id', 'System Listing ID', 0),
(3, 1, 'mls_approved', 'MLS Approved', 0),
(4, 1, 'system_listing_agent_id', 'System Agent ID', 0),
(5, 1, 'system_listing_agent_co_id', 'System CoList Agent ID', 0),
(6, 1, 'property_type', 'Property Type', 0),
(7, 2, 'property_last_updated', 'Property Last Updated', 0),
(8, 2, 'listing_id', 'Listing ID', 0),
(9, 1, 'system_office_id', 'System Office ID', 0),
(10, 2, 'photo_count', 'Photo Count', 0),
(11, 2, 'photo_last_updated', 'Photo Last Updated', 0),
(12, 1, 'category', 'Category', 0),
(13, 2, 'status', 'Status', 0),
(14, 2, 'status_change_date', 'Status Change Date', 0),
(15, 1, 'contingent', 'CTG/FROR/AUC', 0),
(16, 2, 'price', 'Price', 0),
(17, 5, 'association_fee', 'Association Fee', 0),
(18, 5, 'manufactured_home', 'Mobile/Mfg Home', 0),
(19, 5, 'financing', 'Financing', 0),
(20, 2, 'area', 'Area', 0),
(21, 5, 'association_fee_freq', 'Association Fee Freq', 0),
(22, 2, 'street_number', 'Street Number', 0),
(23, 5, 'legal_unit_number', 'Legal Unit #', 0),
(24, 2, 'street_direction_prefix', 'Street Direction Prefix', 0),
(25, 2, 'street_name', 'Street Name', 0),
(26, 2, 'unit_number', 'Unit #', 0),
(27, 2, 'street_direction_suffix', 'Street Direction Suffix', 0),
(28, 2, 'street_suffix', 'Street Sfx', 0),
(29, 5, 'homestead', 'Homestead', 0),
(30, 2, 'city', 'City', 0),
(31, 2, 'state', 'State', 0),
(32, 2, 'county', 'County', 0),
(33, 5, 'historic_area', 'Historic Area', 0),
(34, 2, 'zip', 'zip', 0),
(35, 2, 'geo_lat', 'Geo Lat', 0),
(36, 2, 'geo_lon', 'Geo Lon', 0),
(37, 2, 'sq_feet', 'Approx. Heated SqFt', 0),
(38, 2, 'year_built', 'Year Built', 0),
(39, 5, 'block', 'Block', 0),
(40, 1, 'realtor.com_type', 'Realtor.com Type', 0),
(41, 5, 'lot_dimensions', 'Appx. Lot Dimensions', 0),
(42, 2, 'bedrooms', 'Bedrooms', 0),
(43, 2, 'baths_total', 'Bathrooms', 0),
(44, 2, 'baths_full', 'Full Baths', 0),
(45, 2, 'baths_half', 'Half Baths', 0),
(46, 1, 'all_bedrooms_conform', 'All Bedrooms Conform', 0),
(47, 5, 'association_fee_yn', 'Association Fee Y/N', 0),
(48, 2, 'common_subdivision', 'Common Name of Sub', 0),
(49, 2, 'public_remarks', 'Public Remarks', 0),
(50, 2, 'real_estate_parcel_number', 'Real Estate Parcel #', 0),
(51, 2, 'directions', 'Directions', 0),
(52, 2, 'legal_subdivision', 'Legal Name of Subdivision', 0),
(53, 5, 'gated_community_yn', 'Gated Community', 0),
(54, 5, 'lot', 'Lot', 0),
(55, 6, 'elementary_school', 'Elementary School', 0),
(56, 6, 'middle_school', 'Middle School', 0),
(57, 6, 'high_school', 'High School', 0),
(58, 5, 'nvgble_to_ocean', 'Navgble to Ocean Y/N', 0),
(59, 2, 'region', 'Region', 0),
(60, 1, 'range_marketing', 'Range Marketing', 0),
(61, 2, 'waterfront_yn', 'Waterfront Y/N', 0),
(62, 2, 'new_construction_yn', 'New Construction Y/N', 0),
(63, 2, 'type', 'Type', 0),
(64, 1, 'publish_to_internet', 'Publish to Internet', 0),
(65, 5, 'cdd_fee_yn', 'CDD Fee Y/N', 0),
(66, 5, 'cdd_fee', 'CDD Fee', 0),
(67, 5, 'addtl_det_heat_sqft', 'Addl Det Heat SQFT', 0),
(68, 5, 'other_required_fees', 'Other Required Fees', 0),
(69, 5, 'builder', 'Builder', 0),
(70, 5, 'hoa_name', 'Homeowner Assoc. Name', 0),
(71, 5, 'hoa_phone', 'Homeowner Assoc. Phone', 0),
(72, 3, 'additional_equipment', 'Additional Equipment', 0),
(73, 3, 'additional_rooms', 'Additional Rooms', 0),
(74, 5, 'approx_parcel_size', 'Parcel Size', 0),
(75, 3, 'bath_tub', 'Bath Tub', 0),
(76, 3, 'bedrooms_desc', 'Bedrooms Desc', 0),
(77, 5, 'common_amentities', 'Common/ClubAmenities', 0),
(78, 3, 'dining', 'Dining', 0),
(79, 5, 'documents_on_file', 'Documents on File', 0),
(80, 3, 'energy_features', 'Energy Features', 0),
(81, 1, 'export_options', 'Export Options', 0),
(82, 4, 'exterior_wall', 'Exterior Wall', 0),
(83, 4, 'fencing', 'Fencing', 0),
(84, 3, 'fireplace', 'Fireplace', 0),
(85, 3, 'interior_amenities', 'Interior Amenities', 0),
(86, 3, 'kitchen', 'Kitchen', 0),
(87, 4, 'lot_description', 'Lot Description', 0),
(88, 4, 'lot_location', 'Lot Location', 0),
(89, 3, 'major_applicances', 'Major Appliances', 0),
(90, 4, 'misc_exterior', 'Misc Exterior', 0),
(91, 4, 'new_construction', 'New Construction', 0),
(92, 5, 'occupancy', 'Occupancy', 0),
(93, 2, 'ownerapprpublicmktg', 'OwnerApprPublicMktg', 0),
(94, 4, 'parking_facilities', 'Parking Facilities', 0),
(95, 4, 'pool', 'Pool', 0),
(96, 1, 'possible_financing', 'Possible Financing', 0),
(97, 5, 'presently_zoned', 'Presently Zoned', 0),
(98, 4, 'road_frontage', 'Road Frontage', 0),
(99, 4, 'road_surface', 'Road Surface', 0),
(100, 4, 'roof', 'Roof', 0),
(101, 5, 'square_foot_source', 'Square Foot Source', 0),
(102, 4, 'structure', 'Structure', 0),
(103, 4, 'style', 'Style', 0),
(104, 3, 'type_of_cooling', 'Type of Cooling', 0),
(105, 4, 'type_of_dwelling', 'Type of Dwelling', 0),
(106, 3, 'type_of_heating', 'Type of Heating', 0),
(107, 5, 'utilities', 'Utilities', 0),
(108, 3, 'water_heater', 'Water Heater', 0),
(109, 5, 'waterfront_description', 'Waterfront Description', 0),
(110, 1, 'room_area', 'Room Area', 0),
(111, 1, 'room_width', 'Room Width', 0),
(112, 1, 'room_remarks', 'Room Remarks', 0),
(113, 1, 'room_level', 'Room Level', 0),
(114, 1, 'room_length', 'Room Length', 0),
(115, 1, 'room_flooring', 'Room Flooring', 0),
(116, 1, 'bedroom_2_area', 'Bedroom 2 Area', 0),
(117, 1, 'bedroom_2_width', 'Bedroom 2 Width', 0),
(118, 1, 'bedroom_2_room_remarks', 'Bedroom 2 Room Remarks', 0),
(119, 1, 'bedroom_2_room_level', 'Bedroom 2 Room Level', 0),
(120, 1, 'bedroom_2_length', 'Bedroom 2 Length', 0),
(121, 1, 'bedroom_2_flooring', 'Bedroom 2 Flooring', 0),
(122, 1, 'bedroom_3_area', 'Bedroom 3 Area', 0),
(123, 1, 'bedroom_3_width', 'Bedroom 3 Width', 0),
(124, 1, 'bedroom_3_room_remarks', 'Bedroom 3 Room Remarks', 0),
(125, 1, 'bedroom_3_room_level', 'Bedroom 3 Room Level', 0),
(126, 1, 'bedroom_3_length', 'Bedroom 3 Length', 0),
(127, 1, 'bedroom_3_flooring', 'Bedroom 3 Flooring', 0),
(128, 1, 'bedroom_4_area', 'Bedroom 4 Area', 0),
(129, 1, 'bedroom_4_width', 'Bedroom 4 Width', 0),
(130, 1, 'bedroom_4_room_remarks', 'Bedroom 4 Room Remarks', 0),
(131, 1, 'bedroom_4_level', 'Bedroom 4 Room Level', 0),
(132, 1, 'bedroom_4_length', 'Bedroom 4 Length', 0),
(133, 1, 'bedroom_4_flooring', 'Bedroom 4 Flooring', 0),
(134, 1, 'bedroom_5_area', 'Bedroom 5 Area', 0),
(135, 1, 'bedroom_5_width', 'Bedroom 5 Width', 0),
(136, 1, 'bedroom_5_remarks', 'Bedroom 5 Room Remarks', 0),
(137, 1, 'bedroom_5_room_level', 'Bedroom 5 Room Level', 0),
(138, 1, 'bedroom_5_length', 'Bedroom 5 Length', 0),
(139, 1, 'bedroom_5_flooring', 'Bedroom 5 Flooring', 0),
(140, 1, 'bedroom_6_area', 'Bedroom 6 Area', 0),
(141, 1, 'bedroom_6_width', 'Bedroom 6 Width', 0),
(142, 1, 'bedroom_6_room_remarks', 'Bedroom 6 Room Remarks', 0),
(143, 1, 'bedroom_6_room_level', 'Bedroom 6 Room Level', 0),
(144, 1, 'bedroom_6_length', 'Bedroom 6 Length', 0),
(145, 1, 'bedroom_6_flooring', 'Bedroom 6 Flooring', 0),
(146, 1, 'bonus_den_office_area', 'Bonus/Den/Office Area', 0),
(147, 1, 'bonus_den_office_width', 'Bonus/Den/Office Width', 0),
(148, 1, 'bonus_den_office_room_remarks', 'Bonus/Den/Office Room Remarks', 0),
(149, 1, 'bonus_den_office_room_level', 'Bonus/Den/Office Room Level', 0),
(150, 1, 'bonus_den_office_length', 'Bonus/Den/Office Length', 0),
(151, 1, 'bonus_den_office_flooring', 'Bonus/Den/Office Flooring', 0),
(152, 1, 'breakfast_rm_area', 'Breakfast Rm Area', 0),
(153, 1, 'breakfast_rm_width', 'Breakfast Rm Width', 0),
(154, 1, 'breakfast_rm_remarks', 'Breakfast Rm Room Remarks', 0),
(155, 1, 'breakfast_rm_level', 'Breakfast Rm Room Level', 0),
(156, 1, 'breakfast_rm_length', 'Breakfast Rm Length', 0),
(157, 1, 'breakfast_rm_flooring', 'Breakfast Rm Flooring', 0),
(158, 1, 'dining_rm_area', 'Dining Rm Area', 0),
(159, 1, 'dining_rm_width', 'Dining Rm Width', 0),
(160, 1, 'dining_rm_room_remarks', 'Dining Rm Room Remarks', 0),
(161, 1, 'dining_rm_room_level', 'Dining Rm Room Level', 0),
(162, 1, 'dining_rm_length', 'Dining Rm Length', 0),
(163, 1, 'dining_rm_fooring', 'Dining Rm Flooring', 0),
(164, 1, 'family_rm_area', 'Family Rm Area', 0),
(165, 1, 'family_rm_width', 'Family Rm Width', 0),
(166, 1, 'family_rm_room_remarks', 'Family Rm Room Remarks', 0),
(167, 1, 'family_rm_room_level', 'Family Rm Room Level', 0),
(168, 1, 'family_rm_length', 'Family Rm Length', 0),
(169, 1, 'family_rm_flooring', 'Family Rm Flooring', 0),
(170, 1, 'great_rm_area', 'Great Rm Area', 0),
(171, 1, 'great_rm_width', 'Great Rm Width', 0),
(172, 1, 'great_rm_room_remarks', 'Great Rm Room Remarks', 0),
(173, 1, 'great_rm_room_level', 'Great Rm Room Level', 0),
(174, 1, 'great_rm_length', 'Great Rm Length', 0),
(175, 1, 'great_rm_flooring', 'Great Rm Flooring', 0),
(176, 1, 'kitchen_area', 'Kitchen Area', 0),
(177, 1, 'kitchen_width', 'Kitchen Width', 0),
(178, 1, 'kitchen_room_remarks', 'Kitchen Room Remarks', 0),
(179, 1, 'kitchen_room_level', 'Kitchen Room Level', 0),
(180, 1, 'kitchen_length', 'Kitchen Length', 0),
(181, 1, 'kitchen_flooring', 'Kitchen Flooring', 0),
(182, 1, 'living_rm_area', 'Living Rm Area', 0),
(183, 1, 'living_rm_width', 'Living Rm Width', 0),
(184, 1, 'living_rm_room_remarks', 'Living Rm Room Remarks', 0),
(185, 1, 'living_rm_room_level', 'Living Rm Room Level', 0),
(186, 1, 'living_rm_length', 'Living Rm Length', 0),
(187, 1, 'living_rm_flooring', 'Living Rm Flooring', 0),
(188, 1, 'masterbr_area', 'MasterBr Area', 0),
(189, 1, 'masterbr_width', 'MasterBr Width', 0),
(190, 1, 'masterbr_room_remarks', 'MasterBr Room Remarks', 0),
(191, 1, 'masterbr_room_level', 'MasterBr Room Level', 0),
(192, 1, 'masterbr_length', 'MasterBr Length', 0),
(193, 1, 'masterbr_flooring', 'MasterBr Flooring', 0),
(194, 1, 'other_area', 'Other Area', 0),
(195, 1, 'other_width', 'Other Width', 0),
(196, 1, 'other_room_remarks', 'Other Room Remarks', 0),
(197, 1, 'other_room_level', 'Other Room Level', 0),
(198, 1, 'other_length', 'Other Length', 0),
(199, 1, 'other_flooring', 'Other Flooring', 0),
(200, 2, 'listing_agent_id', 'Listing Agent ID', 0),
(201, 2, 'colisting_agent_id', 'CoListing Agent ID', 0),
(202, 2, 'listing_office_id', 'Listing Office ID', 0),
(203, 7, 'listing_agent_name', 'Listing Agent Name', 0),
(204, 10, 'colisting_agent_name', 'CoListing Agent Name', 0),
(205, 9, 'listing_office_name', 'Listing Office Name', 0),
(206, 7, 'listing_agent_phone', 'Listing Agent Phone', 0),
(207, 10, 'colisting_agent_phone', 'CoListing Agent Phone', 0),
(208, 9, 'listing_office_phone', 'Listing Office Phone', 0),
(209, 7, 'listing_agent_fax', 'Listing Agent Fax', 0),
(210, 10, 'colisting_agent_fax', 'CoListing Agent Fax', 0),
(211, 9, 'listing_office_fax', 'Listing Office Fax', 0),
(212, 7, 'listing_agent_address', 'Listing Agent Address', 0),
(213, 10, 'colisting_agent_address', 'CoListing Agent Address', 0),
(214, 9, 'listing_office_address', 'Listing Office Address', 0),
(215, 7, 'listing_agent_email', 'Listing Agent Email', 0),
(216, 10, 'colisting_agent_email', 'CoListing Agent Email', 0),
(217, 9, 'listing_office_email', 'Listing Office Email', 0),
(218, 7, 'listing_agent_url', 'Listing Agent Url', 0),
(219, 10, 'colisting_agent_url', 'CoListing Agent Url', 0),
(220, 9, 'listing_office_url', 'Listing Office Url', 0),
(221, 5, 'unbranded_virtual_tour', 'UnBranded Virtual Tour', 0),
(222, 4, 'stories', 'Stories', 0),
(223, 2, 'pool_yn', 'Pool Y/N', 0);


--
-- Dumping data for table `mls_field_maps`
--
INSERT INTO `mls_field_maps` (`id`, `mls_feature_id`, `mls_board_id`, `mls_property_type_id`, `feed_system_field`, `display_name_override`, `is_hidden`, `parse`, `is_standardized`, `is_deleted`) VALUES
(1, 1, 2, 1, 'LIST_0', '', 0, 1, 0, 0),
(2, 2, 2, 1, 'LIST_1', '', 0, 0, 0, 0),
(3, 3, 2, 1, 'LIST_4', '', 0, 0, 0, 0),
(4, 4, 2, 1, 'LIST_5', '', 0, 0, 0, 0),
(5, 5, 2, 1, 'LIST_6', '', 0, 0, 0, 0),
(6, 6, 2, 1, 'LIST_8', '', 0, 0, 0, 0),
(7, 7, 2, 1, 'LIST_87', '', 0, 0, 0, 0),
(8, 8, 2, 1, 'LIST_105', '', 0, 0, 0, 0),
(9, 9, 2, 1, 'LIST_106', '', 0, 0, 0, 0),
(10, 10, 2, 1, 'LIST_133', '', 0, 0, 1, 0),
(11, 11, 2, 1, 'LIST_134', '', 0, 0, 0, 0),
(12, 12, 2, 1, 'LIST_9', '', 0, 0, 0, 0),
(13, 13, 2, 1, 'LIST_15', '', 0, 0, 0, 0),
(14, 14, 2, 1, 'LIST_16', '', 0, 0, 0, 0),
(15, 15, 2, 1, 'LIST_19', '', 0, 0, 0, 0),
(16, 16, 2, 1, 'LIST_22', '', 0, 0, 1, 0),
(17, 17, 2, 1, 'LIST_26', '', 0, 0, 0, 0),
(18, 18, 2, 1, 'LIST_27', '', 0, 0, 0, 0),
(19, 19, 2, 1, 'LIST_28', '', 0, 0, 0, 0),
(20, 20, 2, 1, 'LIST_29', '', 0, 0, 0, 0),
(21, 21, 2, 1, 'LIST_101', '', 0, 0, 0, 0),
(22, 22, 2, 1, 'LIST_31', '', 0, 0, 0, 0),
(23, 23, 2, 1, 'LIST_32', '', 0, 0, 0, 0),
(24, 24, 2, 1, 'LIST_33', '', 0, 0, 0, 0),
(25, 25, 2, 1, 'LIST_34', '', 0, 0, 0, 0),
(26, 26, 2, 1, 'LIST_35', '', 0, 0, 0, 0),
(27, 27, 2, 1, 'LIST_36', '', 0, 0, 0, 0),
(28, 28, 2, 1, 'LIST_37', '', 0, 0, 0, 0),
(29, 29, 2, 1, 'LIST_38', '', 0, 0, 0, 0),
(30, 30, 2, 1, 'LIST_39', '', 0, 0, 0, 0),
(31, 31, 2, 1, 'LIST_40', '', 0, 0, 0, 0),
(32, 32, 2, 1, 'LIST_41', '', 0, 0, 0, 0),
(33, 33, 2, 1, 'LIST_42', '', 0, 0, 0, 0),
(34, 34, 2, 1, 'LIST_43', '', 0, 0, 0, 0),
(35, 35, 2, 1, 'LIST_46', '', 0, 0, 0, 0),
(36, 36, 2, 1, 'LIST_47', '', 0, 0, 0, 0),
(37, 37, 2, 1, 'LIST_48', '', 0, 0, 1, 0),
(38, 38, 2, 1, 'LIST_53', '', 0, 0, 1, 0),
(39, 39, 2, 1, 'LIST_54', '', 0, 0, 0, 0),
(40, 40, 2, 1, 'LIST_55', '', 0, 0, 0, 0),
(41, 41, 2, 1, 'LIST_56', '', 0, 0, 0, 0),
(42, 42, 2, 1, 'LIST_66', '', 0, 0, 1, 0),
(43, 43, 2, 1, 'LIST_67', '', 0, 0, 1, 0),
(44, 44, 2, 1, 'LIST_68', '', 0, 0, 1, 0),
(45, 45, 2, 1, 'LIST_69', '', 0, 0, 1, 0),
(46, 46, 2, 1, 'LIST_73', '', 0, 0, 0, 0),
(47, 47, 2, 1, 'LIST_74', '', 0, 0, 0, 0),
(48, 48, 2, 1, 'LIST_77', '', 0, 0, 0, 0),
(49, 49, 2, 1, 'LIST_78', '', 0, 0, 0, 0),
(50, 50, 2, 1, 'LIST_80', '', 0, 0, 0, 0),
(51, 51, 2, 1, 'LIST_82', '', 0, 0, 0, 0),
(52, 52, 2, 1, 'LIST_83', '', 0, 0, 0, 0),
(53, 53, 2, 1, 'LIST_84', '', 0, 0, 0, 0),
(54, 54, 2, 1, 'LIST_86', '', 0, 0, 0, 0),
(55, 55, 2, 1, 'LIST_88', '', 0, 0, 0, 0),
(56, 56, 2, 1, 'LIST_89', '', 0, 0, 0, 0),
(57, 57, 2, 1, 'LIST_90', '', 0, 0, 0, 0),
(58, 58, 2, 1, 'LIST_91', '', 0, 0, 0, 0),
(59, 59, 2, 1, 'LIST_93', '', 0, 0, 0, 0),
(60, 60, 2, 1, 'LIST_94', '', 0, 0, 0, 0),
(61, 61, 2, 1, 'LIST_95', '', 0, 0, 1, 0),
(62, 62, 2, 1, 'LIST_96', '', 0, 0, 1, 0),
(63, 63, 2, 1, 'LIST_97', '', 0, 0, 0, 0),
(64, 64, 2, 1, 'LIST_104', '', 0, 0, 0, 0),
(65, 65, 2, 1, 'LIST_115', '', 0, 0, 0, 0),
(66, 66, 2, 1, 'LIST_117', '', 0, 0, 0, 0),
(67, 67, 2, 1, 'LIST_118', '', 0, 0, 0, 0),
(68, 68, 2, 1, 'LIST_119', '', 0, 0, 0, 0),
(69, 69, 2, 1, 'FEAT20030326032241373388000000', '', 0, 0, 0, 0),
(70, 70, 2, 1, 'FEAT20030923163353316224000000', '', 0, 0, 0, 0),
(71, 71, 2, 1, 'FEAT20030923163620661182000000', '', 0, 0, 0, 0),
(72, 72, 2, 1, 'GF20030226223925901561000000', '', 0, 0, 0, 0),
(73, 73, 2, 1, 'GF20030226223926983029000000', '', 0, 0, 0, 0),
(74, 74, 2, 1, 'GF20030305220239790821000000', '', 0, 0, 0, 0),
(75, 75, 2, 1, 'GF20030226223924961175000000', '', 0, 0, 0, 0),
(76, 76, 2, 1, 'GF20030226223924548899000000', '', 0, 0, 0, 0),
(77, 77, 2, 1, 'GF20030226223930354490000000', '', 0, 0, 0, 0),
(78, 78, 2, 1, 'GF20030226223925313972000000', '', 0, 0, 0, 0),
(79, 79, 2, 1, 'GF20030226223942637168000000', '', 0, 0, 0, 0),
(80, 80, 2, 1, 'GF20030226223930665208000000', '', 0, 0, 0, 0),
(81, 81, 2, 1, 'GF20070122133416855055000000', '', 0, 0, 0, 0),
(82, 82, 2, 1, 'GF20061101134252098174000000', '', 0, 0, 0, 0),
(83, 83, 2, 1, 'GF20030305220206873663000000', '', 0, 0, 0, 0),
(84, 84, 2, 1, 'GF20030226223929026171000000', '', 0, 0, 0, 0),
(85, 85, 2, 1, 'GF20030226223927789385000000', '', 0, 1, 0, 0),
(86, 86, 2, 1, 'GF20030226223925644818000000', '', 0, 0, 0, 0),
(87, 87, 2, 1, 'GF20030226223938465518000000', '', 0, 0, 0, 0),
(88, 88, 2, 1, 'GF20030226223941039602000000', '', 0, 0, 0, 0),
(89, 89, 2, 1, 'GF20030228184016963251000000', '', 0, 1, 0, 0),
(90, 90, 2, 1, 'GF20030226223934789160000000', '', 0, 0, 0, 0),
(91, 91, 2, 1, 'GF20030227010650718647000000', '', 0, 0, 0, 0),
(92, 92, 2, 1, 'GF20030306165225382102000000', '', 0, 0, 0, 0),
(93, 93, 2, 1, 'GF20091007174340223056000000', '', 0, 0, 1, 0),
(94, 94, 2, 1, 'GF20030226223936808977000000', '', 0, 1, 0, 0),
(95, 95, 2, 1, 'GF20030226223936396599000000', '', 0, 0, 1, 0),
(96, 96, 2, 1, 'GF20030226223940461070000000', '', 0, 1, 0, 0),
(97, 97, 2, 1, 'GF20030305180243592752000000', '', 0, 0, 0, 0),
(98, 98, 2, 1, 'GF20030305220303003689000000', '', 0, 0, 0, 0),
(99, 99, 2, 1, 'GF20030305220326457642000000', '', 0, 0, 0, 0),
(100, 100, 2, 1, 'GF20030226223934316859000000', '', 0, 0, 0, 0),
(101, 101, 2, 1, 'GF20030226223924178856000000', '', 0, 0, 0, 0),
(102, 102, 2, 1, 'GF20030226223933328883000000', '', 0, 0, 0, 0),
(103, 103, 2, 1, 'GF20030226223932480526000000', '', 0, 0, 0, 0),
(104, 104, 2, 1, 'GF20030226223930163349000000', '', 0, 0, 0, 0),
(105, 105, 2, 1, 'GF20030226223922292850000000', '', 0, 0, 0, 0),
(106, 106, 2, 1, 'GF20030226223929852543000000', '', 0, 0, 0, 0),
(107, 107, 2, 1, 'GF20030226223931411163000000', '', 0, 0, 0, 0),
(108, 108, 2, 1, 'GF20030226223932164213000000', '', 0, 0, 0, 0),
(109, 109, 2, 1, 'GF20020923141755529165000000', '', 0, 0, 0, 0),
(110, 110, 2, 1, 'ROOM_blank_room_area', '', 0, 0, 0, 0),
(111, 111, 2, 1, 'ROOM_blank_room_width', '', 0, 0, 0, 0),
(112, 112, 2, 1, 'ROOM_blank_room_rem', '', 0, 0, 0, 0),
(113, 113, 2, 1, 'ROOM_blank_room_level', '', 0, 0, 0, 0),
(114, 114, 2, 1, 'ROOM_blank_room_length', '', 0, 0, 0, 0),
(115, 115, 2, 1, 'ROOM_blank_room_nbr', '', 0, 0, 0, 0),
(116, 116, 2, 1, 'ROOM_BD2_room_area', '', 0, 0, 0, 0),
(117, 117, 2, 1, 'ROOM_BD2_room_width', '', 0, 0, 0, 0),
(118, 118, 2, 1, 'ROOM_BD2_room_rem', '', 0, 0, 0, 0),
(119, 119, 2, 1, 'ROOM_BD2_room_level', '', 0, 0, 0, 0),
(120, 120, 2, 1, 'ROOM_BD2_room_length', '', 0, 0, 0, 0),
(121, 121, 2, 1, 'ROOM_BD2_room_nbr', '', 0, 0, 0, 0),
(122, 122, 2, 1, 'ROOM_BD3_room_area', '', 0, 0, 0, 0),
(123, 123, 2, 1, 'ROOM_BD3_room_width', '', 0, 0, 0, 0),
(124, 124, 2, 1, 'ROOM_BD3_room_rem', '', 0, 0, 0, 0),
(125, 125, 2, 1, 'ROOM_BD3_room_level', '', 0, 0, 0, 0),
(126, 126, 2, 1, 'ROOM_BD3_room_length', '', 0, 0, 0, 0),
(127, 127, 2, 1, 'ROOM_BD3_room_nbr', '', 0, 0, 0, 0),
(128, 128, 2, 1, 'ROOM_BD4_room_area', '', 0, 0, 0, 0),
(129, 129, 2, 1, 'ROOM_BD4_room_width', '', 0, 0, 0, 0),
(130, 130, 2, 1, 'ROOM_BD4_room_rem', '', 0, 0, 0, 0),
(131, 131, 2, 1, 'ROOM_BD4_room_level', '', 0, 0, 0, 0),
(132, 132, 2, 1, 'ROOM_BD4_room_length', '', 0, 0, 0, 0),
(133, 133, 2, 1, 'ROOM_BD4_room_nbr', '', 0, 0, 0, 0),
(134, 134, 2, 1, 'ROOM_BD5_room_area', '', 0, 0, 0, 0),
(135, 135, 2, 1, 'ROOM_BD5_room_width', '', 0, 0, 0, 0),
(136, 136, 2, 1, 'ROOM_BD5_room_rem', '', 0, 0, 0, 0),
(137, 137, 2, 1, 'ROOM_BD5_room_level', '', 0, 0, 0, 0),
(138, 138, 2, 1, 'ROOM_BD5_room_length', '', 0, 0, 0, 0),
(139, 139, 2, 1, 'ROOM_BD5_room_nbr', '', 0, 0, 0, 0),
(140, 140, 2, 1, 'ROOM_BD6_room_area', '', 0, 0, 0, 0),
(141, 141, 2, 1, 'ROOM_BD6_room_width', '', 0, 0, 0, 0),
(142, 142, 2, 1, 'ROOM_BD6_room_rem', '', 0, 0, 0, 0),
(143, 143, 2, 1, 'ROOM_BD6_room_level', '', 0, 0, 0, 0),
(144, 144, 2, 1, 'ROOM_BD6_room_length', '', 0, 0, 0, 0),
(145, 145, 2, 1, 'ROOM_BD6_room_nbr', '', 0, 0, 0, 0),
(146, 146, 2, 1, 'ROOM_BON_room_area', '', 0, 0, 0, 0),
(147, 147, 2, 1, 'ROOM_BON_room_width', '', 0, 0, 0, 0),
(148, 148, 2, 1, 'ROOM_BON_room_rem', '', 0, 0, 0, 0),
(149, 149, 2, 1, 'ROOM_BON_room_level', '', 0, 0, 0, 0),
(150, 150, 2, 1, 'ROOM_BON_room_length', '', 0, 0, 0, 0),
(151, 151, 2, 1, 'ROOM_BON_room_nbr', '', 0, 0, 0, 0),
(152, 152, 2, 1, 'ROOM_BRK_room_area', '', 0, 0, 0, 0),
(153, 153, 2, 1, 'ROOM_BRK_room_width', '', 0, 0, 0, 0),
(154, 154, 2, 1, 'ROOM_BRK_room_rem', '', 0, 0, 0, 0),
(155, 155, 2, 1, 'ROOM_BRK_room_level', '', 0, 0, 0, 0),
(156, 156, 2, 1, 'ROOM_BRK_room_length', '', 0, 0, 0, 0),
(157, 157, 2, 1, 'ROOM_BRK_room_nbr', '', 0, 0, 0, 0),
(158, 158, 2, 1, 'ROOM_DIN_room_area', '', 0, 0, 0, 0),
(159, 159, 2, 1, 'ROOM_DIN_room_width', '', 0, 0, 0, 0),
(160, 160, 2, 1, 'ROOM_DIN_room_rem', '', 0, 0, 0, 0),
(161, 161, 2, 1, 'ROOM_DIN_room_level', '', 0, 0, 0, 0),
(162, 162, 2, 1, 'ROOM_DIN_room_length', '', 0, 0, 0, 0),
(163, 163, 2, 1, 'ROOM_DIN_room_nbr', '', 0, 0, 0, 0),
(164, 164, 2, 1, 'ROOM_FAM_room_area', '', 0, 0, 0, 0),
(165, 165, 2, 1, 'ROOM_FAM_room_width', '', 0, 0, 0, 0),
(166, 166, 2, 1, 'ROOM_FAM_room_rem', '', 0, 0, 0, 0),
(167, 167, 2, 1, 'ROOM_FAM_room_level', '', 0, 0, 0, 0),
(168, 168, 2, 1, 'ROOM_FAM_room_length', '', 0, 0, 0, 0),
(169, 169, 2, 1, 'ROOM_FAM_room_nbr', '', 0, 0, 0, 0),
(170, 170, 2, 1, 'ROOM_GRT_room_area', '', 0, 0, 0, 0),
(171, 171, 2, 1, 'ROOM_GRT_room_width', '', 0, 0, 0, 0),
(172, 172, 2, 1, 'ROOM_GRT_room_rem', '', 0, 0, 0, 0),
(173, 173, 2, 1, 'ROOM_GRT_room_level', '', 0, 0, 0, 0),
(174, 174, 2, 1, 'ROOM_GRT_room_length', '', 0, 0, 0, 0),
(175, 175, 2, 1, 'ROOM_GRT_room_nbr', '', 0, 0, 0, 0),
(176, 176, 2, 1, 'ROOM_KIT_room_area', '', 0, 0, 0, 0),
(177, 177, 2, 1, 'ROOM_KIT_room_width', '', 0, 0, 0, 0),
(178, 178, 2, 1, 'ROOM_KIT_room_rem', '', 0, 0, 0, 0),
(179, 179, 2, 1, 'ROOM_KIT_room_level', '', 0, 0, 0, 0),
(180, 180, 2, 1, 'ROOM_KIT_room_length', '', 0, 0, 0, 0),
(181, 181, 2, 1, 'ROOM_KIT_room_nbr', '', 0, 0, 0, 0),
(182, 182, 2, 1, 'ROOM_LIV_room_area', '', 0, 0, 0, 0),
(183, 183, 2, 1, 'ROOM_LIV_room_width', '', 0, 0, 0, 0),
(184, 184, 2, 1, 'ROOM_LIV_room_rem', '', 0, 0, 0, 0),
(185, 185, 2, 1, 'ROOM_LIV_room_level', '', 0, 0, 0, 0),
(186, 186, 2, 1, 'ROOM_LIV_room_length', '', 0, 0, 0, 0),
(187, 187, 2, 1, 'ROOM_LIV_room_nbr', '', 0, 0, 0, 0),
(188, 188, 2, 1, 'ROOM_BD1_room_area', '', 0, 0, 0, 0),
(189, 189, 2, 1, 'ROOM_BD1_room_width', '', 0, 0, 0, 0),
(190, 190, 2, 1, 'ROOM_BD1_room_rem', '', 0, 0, 0, 0),
(191, 191, 2, 1, 'ROOM_BD1_room_level', '', 0, 0, 0, 0),
(192, 192, 2, 1, 'ROOM_BD1_room_length', '', 0, 0, 0, 0),
(193, 193, 2, 1, 'ROOM_BD1_room_nbr', '', 0, 0, 0, 0),
(194, 194, 2, 1, 'ROOM_OTH_room_area', '', 0, 0, 0, 0),
(195, 195, 2, 1, 'ROOM_OTH_room_width', '', 0, 0, 0, 0),
(196, 196, 2, 1, 'ROOM_OTH_room_rem', '', 0, 0, 0, 0),
(197, 197, 2, 1, 'ROOM_OTH_room_level', '', 0, 0, 0, 0),
(198, 198, 2, 1, 'ROOM_OTH_room_length', '', 0, 0, 0, 0),
(199, 199, 2, 1, 'ROOM_OTH_room_nbr', '', 0, 0, 0, 0),
(200, 200, 2, 1, 'listing_member_shortid', '', 0, 0, 0, 0),
(201, 201, 2, 1, 'colisting_member_shortid', '', 0, 0, 0, 0),
(202, 202, 2, 1, 'listing_office_shortid', '', 0, 0, 0, 0),
(203, 203, 2, 1, 'listing_member_name', '', 0, 0, 0, 0),
(204, 204, 2, 1, 'colisting_member_name', '', 0, 0, 0, 0),
(205, 205, 2, 1, 'listing_office_name', '', 0, 0, 0, 0),
(206, 206, 2, 1, 'listing_member_phone', '', 0, 0, 0, 0),
(207, 207, 2, 1, 'colisting_member_phone', '', 0, 0, 0, 0),
(208, 208, 2, 1, 'listing_office_phone', '', 0, 0, 0, 0),
(209, 209, 2, 1, 'listing_member_fax', '', 0, 0, 0, 0),
(210, 210, 2, 1, 'colisting_member_fax', '', 0, 0, 0, 0),
(211, 211, 2, 1, 'listing_office_fax', '', 0, 0, 0, 0),
(212, 212, 2, 1, 'listing_member_address', '', 0, 0, 0, 0),
(213, 213, 2, 1, 'colisting_member_address', '', 0, 0, 0, 0),
(214, 214, 2, 1, 'listing_office_address', '', 0, 0, 0, 0),
(215, 215, 2, 1, 'listing_member_email', '', 0, 0, 0, 0),
(216, 216, 2, 1, 'colisting_member_email', '', 0, 0, 0, 0),
(217, 217, 2, 1, 'listing_office_email', '', 0, 0, 0, 0),
(218, 218, 2, 1, 'listing_member_url', '', 0, 0, 0, 0),
(219, 219, 2, 1, 'colisting_member_url', '', 0, 0, 0, 0),
(220, 220, 2, 1, 'listing_office_url', '', 0, 0, 0, 0),
(221, 221, 2, 1, 'UNBRANDEDIDXVIRTUALTOUR', '', 0, 0, 0, 0),
(222, 222, 2, 1, 'GF20120424173840764151000000', '', 0, 0, 0, 0);

--
-- Dumping data for table `mls_property_types`
--

INSERT INTO `mls_property_types` (`id`, `name`, `table_suffix`, `description`) VALUES
(1, 'Residential', 'residential', 'Residential Homes for Sale'),
(2, 'Condo', 'condo', 'Condominiums for Sale'),
(3, 'Land', 'land', 'Vacant Land & Lots'),
(4, 'Rental', 'rental', 'Properties for Rent'),
(5, 'Investment', 'investment', 'Investment Properties'),
(6, 'Commercial', 'commercial', 'Commercial Properties for Sale'),
(7, 'Commerical for Lease', 'commercialLease', 'Commercial Properties for Lease'),
(8, 'Villa/Townhouse', 'villa', 'Commercial Properties for Lease'),
(9, 'Manufactured Home', 'manufactured', 'Manufacture/Mobile Homes for Sale'),
(10, 'Farm', 'farm', 'Farm Land for Sale'),
(11, 'Patio Home', 'patio', 'Patio Homes for Sale');

--
-- Dumping data for table `mls_property_type_board_lu`
--

INSERT INTO `mls_property_type_board_lu` (`id`, `mls_board_id`, `mls_property_type_id`, `status_ma`, `feed_table_name`, `display_name`) VALUES
(1, 2, 1, 1, 'A', 'Residential'),
(2, 2, 2, 1, 'B', 'Condominium'),
(3, 2, 3, 1, 'C', 'Land'),
(4, 2, 4, 0, 'D', 'Rental'),
(5, 2, 5, 0, 'E', 'Investment'),
(6, 2, 6, 0, 'F', 'Commercial'),
(7, 2, 7, 0, 'G', 'Commercial Lease');


INSERT INTO `form_fields` (`id`, `name`, `label`, `is_deleted`) VALUES
(1, 'first_name', 'Your First Name', 0),
(2, 'last_name', 'Your Last Name', 0),
(3, 'email', 'Your Email', 0),
(4, 'phone', 'Phone', 0),
(5, 'contact_date', 'Contact Date', 0),
(6, 'contact_time', 'Contact Time', 0),
(7, 'comments', 'Comments', 0),
(8, 'question', 'Question', 0),
(9, 'to_first_name', 'To First Name', 0),
(10, 'to_last_name', 'To Last Name', 0),
(11, 'to_email', 'To Email', 0),
(12, 'form_title', 'Form Title', 0);

INSERT INTO `form_field_lu` (`id`, `form_account_lu_id`, `form_field_id`, `is_deleted`) VALUES
(1, 1, 1, 0),
(2, 1, 2, 0),
(3, 1, 3, 0),
(4, 1, 4, 0),
(5, 1, 5, 0),
(6, 1, 6, 0),
(7, 1, 7, 0),
(8, 2, 1, 0),
(9, 2, 2, 0),
(10, 2, 3, 0),
(11, 2, 4, 0),
(12, 2, 5, 0),
(13, 2, 6, 0),
(14, 2, 7, 0),
(15, 3, 1, 0),
(16, 3, 2, 0),
(17, 3, 3, 0),
(18, 3, 9, 0),
(19, 3, 10, 0),
(20, 3, 11, 0),
(21, 3, 7, 0),
(22, 4, 1, 0),
(23, 4, 2, 0),
(24, 4, 3, 0),
(25, 4, 4, 0),
(26, 4, 8, 0),
(27, 5, 1, 0),
(28, 5, 2, 0),
(29, 5, 3, 0),
(30, 5, 4, 0),
(31, 6, 1, 0),
(32, 6, 2, 0),
(33, 6, 3, 0),
(34, 6, 4, 0);

INSERT INTO `form_account_lu` (`account_id`, `form_id`, `component_type_id`, `is_deleted`) VALUES
(2, 1, 2, 0),
(2, 2, 2, 0),
(2, 3, 2, 0),
(2, 4, 2, 0),
(2, 5, 2, 0),
(2, 7, 2, 0);


INSERT INTO `forms` (`form_type_ma`, `name`) VALUES
(NULL, 'Contact Us'),
(NULL, 'Showing Request'),
(NULL, 'Share Home to Friend'),
(NULL, 'Ask A Question'),
(NULL, 'Property Tax'),
(NULL, 'House Values'),
(NULL, 'Home Details Forced Registration'),
(NULL, 'Foreclosure List'),
(NULL, 'Free Reports'),
(NULL, 'Video Form');


SET FOREIGN_KEY_CHECKS = 1;
