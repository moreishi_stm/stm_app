-- =============================================================================
-- Diagram Name: STM-MLS
-- Created on: 10/9/2012 11:25:07 PM
-- Diagram Version: 13
-- =============================================================================
SET FOREIGN_KEY_CHECKS=0;

-- Drop table mls_error_log
DROP TABLE IF EXISTS `mls_error_log`;

CREATE TABLE `mls_error_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11),
  `mls_property_type_id` int(11),
  `listing_id` int(11),
  `data_type` tinyint(4) COMMENT 'data or photo',
  `error_type` tinyint(4) COMMENT 'notice or error',
  `message` text,
  `datetime` datetime,
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;

-- Drop table nefar_offices
DROP TABLE IF EXISTS `nefar_offices`;

CREATE TABLE `nefar_offices` (
  `id` int(11) DEFAULT '0',
  `system_id` varchar(63),
  `name` varchar(63),
  `address` varchar(255),
  `email` varchar(127),
  `phone` varchar(15),
  `fax` varchar(15),
  `website` varchar(127),
  `is_deleted` tinyint(1) DEFAULT '0'
)
ENGINE=INNODB;

-- Drop table nefar_agents
DROP TABLE IF EXISTS `nefar_agents`;

CREATE TABLE `nefar_agents` (
  `id` int(11) DEFAULT '0',
  `system_id` varchar(63),
  `name` varchar(63),
  `address` varchar(255),
  `email` varchar(127),
  `phone` varchar(15),
  `fax` varchar(15),
  `website` varchar(127),
  `is_deleted` tinyint(1) DEFAULT '0'
)
ENGINE=INNODB;

-- Drop table nefar_residential
DROP TABLE IF EXISTS `nefar_residential`;

CREATE TABLE `nefar_residential` (
  `listing_id` int(11) NOT NULL,
  `listing_agent_id` varchar(50) NOT NULL,
  `listing_agent_co_id` varchar(50),
  `listing_office_id` varchar(50) NOT NULL,
  `status` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `contingent` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `price` int(11),
  `region` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `area` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `street_num` varchar(15),
  `street_direction_prefix` varchar(50),
  `street_name` varchar(63) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `street_suffix` varchar(31) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `street_direction_suffix` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `unit_num` varchar(15),
  `city` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `state` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `zip` varchar(10),
  `county` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `geo_lat` decimal(10,6),
  `geo_lon` decimal(10,6),
  `sq_feet` mediumint(9),
  `year_built` smallint(6),
  `bedrooms` tinyint(4),
  `baths_total` tinyint(4),
  `baths_full` tinyint(4),
  `baths_half` tinyint(4),
  `legal_subdivision` varchar(63) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `common_subdivision` varchar(63) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `ownerapprpublicmktg` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `directions` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `property_last_updated` datetime NOT NULL,
  `photo_last_updated` datetime NOT NULL,
  `photo_count` tinyint(4) NOT NULL,
  `real_estate_parcel_num` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `public_remarks` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_change_date` datetime NOT NULL,
  PRIMARY KEY(`listing_id`),
  INDEX `listing_office_id`(`listing_office_id`),
  INDEX `listing_agent_id`(`listing_agent_id`),
  INDEX `listing_agent_co_id`(`listing_agent_co_id`)
)
ENGINE=INNODB;

-- Drop table nefar_residential_fields
DROP TABLE IF EXISTS `nefar_residential_fields`;

CREATE TABLE `nefar_residential_fields` (
  `listing_id` int(11) NOT NULL,
  `column_name` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `column_value` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `group_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY(`listing_id`, `column_name`)
)
ENGINE=INNODB;

-- Drop table mls_history_log
DROP TABLE IF EXISTS `mls_history_log`;

CREATE TABLE `mls_history_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11),
  `mls_property_type_id` int(11) COMMENT 'FK in stm_app db',
  `data_type` tinyint(1) COMMENT 'data(1) or photo(2)',
  `start_datetime` datetime,
  `begin_count` int(11),
  `homes_affected_count` int(11) COMMENT 'add and updates',
  `total_count` int(11),
  `new_added_count` int(11) COMMENT 'diff begin, total',
  `delete_start_datetime` datetime,
  `all_active_count` int(11),
  `delete_inactive_count` int(11),
  `active_final_count` int(11),
  `delete_complete_datetime` datetime,
  `error_count` int(11),
  `error_listing_ids` text,
  `complete_datetime` datetime,
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;

SET FOREIGN_KEY_CHECKS=1;
