-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2013 at 08:01 AM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `stm_hq`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11) DEFAULT NULL,
  `account_status_ma` tinyint(1) DEFAULT NULL,
  `agent_board_num` varchar(50) DEFAULT NULL,
  `feed_username` varchar(50) DEFAULT NULL,
  `feed_password` varchar(50) DEFAULT NULL,
  `contract_date` date DEFAULT NULL,
  `go_live_date` date DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `api_key` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `mls_board_id`, `account_status_ma`, `agent_board_num`, `feed_username`, `feed_password`, `contract_date`, `go_live_date`, `notes`, `api_key`, `is_deleted`) VALUES
(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'Seize the Market', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `account_contact_lu`
--

CREATE TABLE IF NOT EXISTS `account_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) NOT NULL DEFAULT '0',
  `contact_type_ma` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `action_plans`
--

CREATE TABLE IF NOT EXISTS `action_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `name` varchar(75) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `action_plan_applied_lu`
--

CREATE TABLE IF NOT EXISTS `action_plan_applied_lu` (
  `action_plan_item_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT NULL,
  KEY `task_id` (`task_id`),
  KEY `action_plan_item_id` (`action_plan_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `action_plan_items`
--

CREATE TABLE IF NOT EXISTS `action_plan_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_plan_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '1',
  `task_type_id` int(11) DEFAULT NULL COMMENT 'email, phone, etc.',
  `email_template_id` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `due_days` int(11) DEFAULT NULL,
  `due_days_type_ma` tinyint(1) DEFAULT NULL,
  `due_reference_ma` tinyint(1) DEFAULT NULL,
  `assign_to_type_ma` int(11) DEFAULT NULL COMMENT 'owner, user, closing, specific',
  `if_weekend` tinyint(1) DEFAULT NULL COMMENT 'move before, after, keep',
  `specific_user_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `action_plan_id` (`action_plan_id`),
  KEY `email_template_id` (`email_template_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `specific_user_id` (`specific_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `active_guests`
--

CREATE TABLE IF NOT EXISTS `active_guests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User Groups' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `active_users`
--

CREATE TABLE IF NOT EXISTS `active_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User Groups' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL COMMENT 'seller, buyer, contact',
  `component_id` int(11) DEFAULT NULL,
  `task_type_id` int(11) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `note` blob,
  `lead_gen_type_ma` tinyint(1) DEFAULT '0' COMMENT 'new or f/u',
  `is_contact` tinyint(1) DEFAULT '0',
  `is_public` tinyint(1) DEFAULT '0',
  `added_by` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `is_action_plan` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `origin_type` int(11) DEFAULT NULL COMMENT 'manual, taks, website',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `added_by` (`added_by`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `activity_date` (`activity_date`) USING BTREE,
  KEY `lead_gen_type_ma` (`lead_gen_type_ma`) USING BTREE,
  KEY `is_contact` (`is_contact`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `account_id`, `component_type_id`, `component_id`, `task_type_id`, `activity_date`, `note`, `lead_gen_type_ma`, `is_contact`, `is_public`, `added_by`, `added`, `is_action_plan`, `is_deleted`, `origin_type`, `updated`) VALUES
(1, 1, 1, 8, 9, '2013-03-01 08:30:00', 0x44656d6f2077656e742077656c6c2e204e65656420746f2072756e2062792053657468206265666f7265206d616b696e6720616e7920657870656e73697665206465636973696f6e732e, 0, 0, 0, 1, '2013-03-12 02:19:53', 0, 0, NULL, NULL),
(2, 1, 1, 8, 2, '2013-03-11 18:04:00', 0x4c4d, 0, 0, 0, 1, '2013-03-12 02:21:29', 0, 0, NULL, NULL),
(3, 1, 1, 9, 9, '2013-03-01 15:00:00', 0x57656e742077656c6c2c206e6565647320746f207265766965772068657220636f73747320616e642074616c6b20746f207465616d2e20576f756c642077616e7420746f2072756e20736964652d62792d73696465207769746820546f702050726f64756365722e, 0, 0, 0, 1, '2013-03-12 02:29:06', 0, 0, NULL, NULL),
(4, 1, 1, 10, 9, '2013-03-04 08:00:00', 0x4469642044656d6f2c2077656e742077656c6c2e204e65656420746f20662f75, 0, 0, 0, 1, '2013-03-12 02:36:41', 0, 0, NULL, NULL),
(5, 1, 1, 13, 9, '2013-03-05 17:30:00', 0x57656e74207265616c6c792077656c6c2e204e65656420746f20636f6e74696e756520636f6e766572736174696f6e20726567617264696e67203520446f6f727320616e6420746865697220726f6c6c2d6f757420706c616e2e, 0, 0, 0, 1, '2013-03-12 02:43:23', 0, 0, NULL, NULL),
(6, 1, 1, 13, 2, '2013-03-11 17:44:00', 0x4c4d2c20662f75, 0, 0, 0, 1, '2013-03-12 02:44:37', 0, 0, NULL, NULL),
(7, 1, 1, 14, 9, '2013-03-05 21:00:00', 0x57656e7420766572792077656c6c2e2057696c6c20726576696577207768617420636f6e7472616374206f626c69676174696f6e732074686579206861766520616e642074616c6b20616761696e2e20456420676f696e6720696e746f2073686f756c646572207375726765727920736f2077696c6c206265206f757420666f7220746865207765656b656e642e, 0, 0, 0, 1, '2013-03-12 02:46:54', 0, 0, NULL, NULL),
(8, 1, 1, 14, 2, '2013-03-11 17:52:00', 0x546f7563686564206261736520776974682045642057616c64656e2e20576173206c6f6f6b696e6720666f722050504320657374696d617465732e2053656e74206d652074686520617265617320746f206c6f6f6b20696e746f2e2057696c6c206c6574206d65206b6e6f77206f6e20746865697220636f6e7472616374206f626c69676174696f6e7320627920746f6461792e, 0, 0, 0, 1, '2013-03-12 02:48:02', 0, 0, NULL, NULL),
(9, 1, 1, 17, 9, '2013-03-06 14:30:00', 0x57656e742077656c6c2e204861732061206c6f74206f66206f7468657220736974657320736f206e65656420746f20636f6d7061726520616e64207365652e204d656469756d206c6576656c206d6f7469766174696f6e2e204f766572616c6c206c696b65642074686520636f6e636570742e, 0, 0, 0, 1, '2013-03-12 02:50:53', 0, 0, NULL, NULL),
(10, 1, 1, 18, 9, '2013-03-06 17:30:00', 0x57656e742077656c6c2e2054686579207265616c6c79206c696b65642069742e204e65656420746f2074616c6b206974206f7665722e20507269636520697320746f6f206869676820616e6420776f72726965642061626f757420696e6372656173696e672070726963652e, 0, 0, 0, 1, '2013-03-12 02:52:30', 0, 0, NULL, NULL),
(11, 1, 1, 19, 9, '2013-03-06 19:30:00', 0x57656e742077656c6c2e20526565642068617320426f6f6d746f776e20736f206e6565647320746f20746f75636820626173652077697468207465616d206f6e20776861742074686579206861766520616e6420646973637573732e, 0, 0, 0, 1, '2013-03-12 02:55:07', 0, 0, NULL, NULL),
(12, 1, 1, 20, 9, '2013-03-12 12:30:00', 0x57656e742077656c6c2e205368652077617320776f7727642e2057616e747320746f207363686564756c6520616e6f74686572206170707420746f2068617665206865722068757362616e642f627573696e65737320706172746e65722073656520697420746f6f2e, 0, 0, 0, 1, '2013-03-12 02:57:18', 0, 0, NULL, NULL),
(13, 1, 1, 21, 9, '2013-03-11 14:00:00', 0x57656e742077656c6c2e204865206a7573742073746172746564204b756e76657273696f6e2e2050726574747920737572652068652063616e20676574206f7574206f66206974206966206e65656465642e204865206973204956522068656176792c2067657473203230302d343030207369676e2063616c6c732f6d6f2e20446f6e2774207265616c6c7920757365204372616967736c697374206f72206f74686572206c6561642067656e2e20446f657320526164696f2e204368616c6c656e67652077697468206d6f76696e67207465616d206f76657220616761696e2e, 0, 0, 0, 1, '2013-03-12 03:00:28', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `address_type_ma` int(11) DEFAULT NULL,
  `address` varchar(127) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `state_id` (`state_id`),
  KEY `account_id` (`account_id`),
  KEY `address` (`address`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `address_company_lu`
--

CREATE TABLE IF NOT EXISTS `address_company_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `address_contact_lu`
--

CREATE TABLE IF NOT EXISTS `address_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `address_states`
--

CREATE TABLE IF NOT EXISTS `address_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `short_name` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `assignment_type_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `contact_id` (`contact_id`),
  KEY `assignment_type_id` (`assignment_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_types`
--

CREATE TABLE IF NOT EXISTS `assignment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_type_account_lu`
--

CREATE TABLE IF NOT EXISTS `assignment_type_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `assignment_type_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assignment_type_id` (`assignment_type_id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 1, NULL, 'N;'),
('owner', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `label`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 'Seize the Market', 2, NULL, NULL, 'N;'),
('owner', 'Owner', 2, NULL, NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(50) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `closings`
--

CREATE TABLE IF NOT EXISTS `closings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `closing_status_ma` tinyint(1) DEFAULT NULL,
  `mls_property_type_id` int(11) DEFAULT NULL,
  `title_company_id` int(11) DEFAULT NULL,
  `lender_id` int(11) DEFAULT NULL,
  `other_agent_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `sale_type_ma` tinyint(1) DEFAULT '0',
  `financing_type_ma` tinyint(4) DEFAULT NULL,
  `loan_amount` int(11) DEFAULT NULL,
  `is_new_construction` tinyint(1) DEFAULT '0',
  `mls_id` varchar(25) DEFAULT '0',
  `is_our_listing` tinyint(1) DEFAULT '0',
  `inspection_end_date` date DEFAULT NULL,
  `contingencies` varchar(255) DEFAULT NULL,
  `notes` varchar(750) DEFAULT NULL,
  `is_referral` tinyint(1) DEFAULT NULL,
  `referral_commission` decimal(6,2) DEFAULT NULL,
  `commission_amount` decimal(10,2) DEFAULT NULL,
  `contract_execute_date` date DEFAULT NULL,
  `contract_closing_date` date DEFAULT NULL,
  `actual_closing_datetime` datetime DEFAULT NULL,
  `is_mail_away` tinyint(1) DEFAULT '0',
  `closing_location` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_property_type_id` (`mls_property_type_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `closing_status_ma` (`closing_status_ma`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `closing_contacts`
--

CREATE TABLE IF NOT EXISTS `closing_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `closing_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `closing_id` (`closing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_contents`
--

CREATE TABLE IF NOT EXISTS `cms_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `type_ma` tinyint(1) DEFAULT '1',
  `template_name` varchar(40) NOT NULL,
  `url` varchar(127) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content_public` mediumtext,
  `content_private` mediumtext,
  `cms_widget_group_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`domain_id`,`url`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`),
  KEY `cms_widget_group_id` (`cms_widget_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_content_tag_lu`
--

CREATE TABLE IF NOT EXISTS `cms_content_tag_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_content_id` int(11) DEFAULT NULL,
  `cms_tag_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_content_id` (`cms_content_id`),
  KEY `cms_tag_id` (`cms_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_content_widget_lu`
--

CREATE TABLE IF NOT EXISTS `cms_content_widget_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_content_id` int(11) DEFAULT NULL,
  `cms_widget_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_content_id` (`cms_content_id`),
  KEY `cms_widget_id` (`cms_widget_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_tags`
--

CREATE TABLE IF NOT EXISTS `cms_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_videos`
--

CREATE TABLE IF NOT EXISTS `cms_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT '0',
  `description` varchar(63) DEFAULT NULL,
  `video_source_ma` tinyint(1) DEFAULT NULL,
  `video_code` varchar(127) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_widgets`
--

CREATE TABLE IF NOT EXISTS `cms_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `folder_name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_widget_groups`
--

CREATE TABLE IF NOT EXISTS `cms_widget_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_widget_group_lu`
--

CREATE TABLE IF NOT EXISTS `cms_widget_group_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_widget_group_id` int(11) DEFAULT NULL,
  `cms_widget_id` int(11) DEFAULT NULL,
  `sort_order` tinyint(2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_widget_id` (`cms_widget_id`),
  KEY `cms_widget_group_id` (`cms_widget_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `primary_contact_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `primary_contact_id` (`primary_contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_assignment_lu`
--

CREATE TABLE IF NOT EXISTS `company_assignment_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL COMMENT 'The component_id of the assignee to which this component has been assigned.',
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_contact_lu`
--

CREATE TABLE IF NOT EXISTS `company_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_types`
--

CREATE TABLE IF NOT EXISTS `company_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_type_lu`
--

CREATE TABLE IF NOT EXISTS `company_type_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `company_type_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `company_type_id` (`company_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `component_types`
--

CREATE TABLE IF NOT EXISTS `component_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `display_name` varchar(63) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `component_types`
--

INSERT INTO `component_types` (`id`, `name`, `display_name`) VALUES
(1, 'contacts', 'Contacts');

-- --------------------------------------------------------

--
-- Table structure for table `consultations`
--

CREATE TABLE IF NOT EXISTS `consultations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `set_on_datetime` datetime DEFAULT NULL COMMENT 'current time it was set',
  `set_by` int(11) DEFAULT NULL,
  `set_for_datetime` datetime DEFAULT NULL COMMENT 'future appt',
  `met_datetime` datetime DEFAULT NULL,
  `met_by` int(11) DEFAULT NULL,
  `met_result_ma` tinyint(1) DEFAULT '0',
  `met_location` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `met_by` (`met_by`),
  KEY `set_by` (`set_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `contact_status_ma` int(11) DEFAULT NULL,
  `lead_type_ma` tinyint(1) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `password` varchar(127) DEFAULT NULL,
  `notes` varchar(750) DEFAULT NULL,
  `session_id` varchar(127) DEFAULT NULL,
  `hash` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `source_id` (`source_id`),
  KEY `first_name` (`first_name`) USING BTREE,
  KEY `last_name` (`last_name`) USING BTREE,
  KEY `last_login` (`last_login`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `account_id`, `source_id`, `contact_status_ma`, `lead_type_ma`, `first_name`, `last_name`, `password`, `notes`, `session_id`, `hash`, `last_login`, `added`, `is_deleted`) VALUES
(1, 1, 1, 1, NULL, 'Christine', 'Lee', 'admin', '', NULL, NULL, NULL, NULL, 0),
(2, 1, 1, 1, NULL, 'Gene', 'Rivers', 'DSn}a!d>', '', NULL, NULL, NULL, '2013-03-12 01:56:34', 0),
(3, 1, 2, 1, NULL, 'Jonathan', 'Ceaser', '(z=9(Fmb', 'Referred by Shawn Mahdavi', NULL, NULL, NULL, '2013-03-12 02:02:51', 0),
(4, 1, 1, 1, NULL, 'Bob', 'Corcoran', '\\d,KI@X{', '', NULL, NULL, NULL, '2013-03-12 02:03:14', 0),
(5, 1, 1, 1, NULL, 'Anthony', 'Dazet', 'lF&Z4}''f', '', NULL, NULL, NULL, '2013-03-12 02:06:24', 0),
(6, 1, 1, 1, NULL, 'Lisa', 'Burley', '80x~"8v"', '', NULL, NULL, NULL, '2013-03-12 02:07:27', 0),
(7, 1, 1, 1, NULL, 'Tim', 'Roh (Nick Shivers)', '/x}h-;<s', '', NULL, NULL, NULL, '2013-03-12 02:08:54', 0),
(8, 1, 1, 1, NULL, 'Chris', 'Hart', 'sW~yYV_V', 'Seth Campbell''s new TL\r\nAppt 8:30am Fri 3/1/2013 - F/U', NULL, NULL, NULL, '2013-03-12 02:12:50', 0),
(9, 1, 1, 1, NULL, 'Wendy', 'Harris', '7O$%w3Jh', '', NULL, NULL, NULL, '2013-03-12 02:24:49', 0),
(10, 1, 3, 1, NULL, 'Josh ', 'Roy', '3M<}%0GF', 'Joy Roy - Kunversion is taken\r\n\r\nUsed Realty Generator - was KW product\r\nHeard about Boomtown afterwards\r\n\r\nIn Kansas', NULL, NULL, NULL, '2013-03-12 02:34:00', 0),
(11, 1, 3, 1, NULL, 'Mark', 'Z', '`5OY@.YN', '', NULL, NULL, NULL, '2013-03-12 02:41:28', 0),
(12, 1, 2, 1, NULL, 'Justin', 'Agent', '6gD?I{_D', 'No show to our appt\r\nReferral from a RATE member', NULL, NULL, NULL, '2013-03-12 02:42:02', 0),
(13, 1, 1, 1, NULL, 'Devin', 'Dougherty', 'KA(/qN?/', '', NULL, NULL, NULL, '2013-03-12 02:42:31', 0),
(14, 1, 2, 1, NULL, 'Ed & Dar', 'Walden', '22n]G.(k', 'Referred by Katherine @ Corcoran Coaching', NULL, NULL, NULL, '2013-03-12 02:45:38', 0),
(15, 1, 1, 1, NULL, 'Sue', 'Adler', 'F,b"Z=bs', '', NULL, NULL, NULL, '2013-03-12 02:48:51', 0),
(16, 1, 1, 1, NULL, 'Andrew', 'Duncan', 'MF^CLKRd', '', NULL, NULL, NULL, '2013-03-12 02:49:10', 0),
(17, 1, 3, 1, NULL, 'Jason', 'Bramblett', 'm_Fs5L4k', '', NULL, NULL, NULL, '2013-03-12 02:49:43', 0),
(18, 1, 1, 1, NULL, 'Kurt & Darla', 'Buehler', 'Bur/dd>(', '', NULL, NULL, NULL, '2013-03-12 02:51:26', 0),
(19, 1, 1, 1, NULL, 'Reed', 'Moore', '&cZNC`|x', '', NULL, NULL, NULL, '2013-03-12 02:54:03', 0),
(20, 1, 2, 1, NULL, 'JoAnne', 'Owens', 'n"]DUS=3', 'Referred by Gene\r\nOP & TL', NULL, NULL, NULL, '2013-03-12 02:56:07', 0),
(21, 1, 3, 1, NULL, 'Josh', 'DeShong', ',YX&n-xH', 'Uses Kunversion, started 1 month ago.', NULL, NULL, NULL, '2013-03-12 02:58:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_attributes`
--

CREATE TABLE IF NOT EXISTS `contact_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `label` varchar(63) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `data_type_id` (`data_type_id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_attribute_account_lu`
--

CREATE TABLE IF NOT EXISTS `contact_attribute_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_attribute_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_attribute_id` (`contact_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_attribute_values`
--

CREATE TABLE IF NOT EXISTS `contact_attribute_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `contact_attribute_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `contact_attribute_id` (`contact_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_types`
--

CREATE TABLE IF NOT EXISTS `contact_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `contact_types`
--

INSERT INTO `contact_types` (`id`, `account_id`, `name`, `is_deleted`) VALUES
(1, 1, 'Sphere', 0),
(2, 1, 'Referral', 0),
(3, 1, 'RATE', 0),
(4, 1, 'KW', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_type_lu`
--

CREATE TABLE IF NOT EXISTS `contact_type_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `contact_type_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `contact_type_id` (`contact_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `contact_type_lu`
--

INSERT INTO `contact_type_lu` (`id`, `contact_id`, `contact_type_id`, `is_deleted`) VALUES
(1, 1, 1, 0),
(2, 2, 2, 1),
(3, 2, 1, 0),
(4, 3, 2, 0),
(5, 4, 1, 0),
(6, 5, 1, 0),
(7, 6, 1, 0),
(8, 7, 1, 0),
(9, 7, 3, 0),
(10, 8, 1, 0),
(11, 9, 1, 0),
(12, 10, 3, 0),
(13, 10, 4, 0),
(14, 11, 3, 0),
(15, 11, 1, 0),
(16, 12, 2, 0),
(17, 13, 1, 0),
(18, 13, 4, 0),
(19, 14, 3, 0),
(20, 14, 2, 0),
(21, 15, 3, 0),
(22, 15, 1, 0),
(23, 16, 3, 0),
(24, 16, 1, 0),
(25, 17, 3, 0),
(26, 18, 4, 0),
(27, 19, 1, 0),
(28, 20, 4, 0),
(29, 20, 2, 0),
(30, 21, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `craigslist_templates`
--

CREATE TABLE IF NOT EXISTS `craigslist_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `craigslist_template_group_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `content` varchar(800) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craigslist_template_group_id` (`craigslist_template_group_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craigslist_template_groups`
--

CREATE TABLE IF NOT EXISTS `craigslist_template_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `content` varchar(800) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE IF NOT EXISTS `custom_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_group_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `sort_order` tinyint(4) DEFAULT NULL COMMENT 'may not be needed if views are custom',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `custom_field_group_id` (`custom_field_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_groups`
--

CREATE TABLE IF NOT EXISTS `custom_field_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` varchar(50) DEFAULT NULL,
  `table_name` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_values`
--

CREATE TABLE IF NOT EXISTS `custom_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `custom_field_id` (`custom_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL COMMENT 'seller, buyer, closing',
  `component_id` int(11) DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file_path` varchar(63) DEFAULT NULL,
  `file_name` varchar(63) DEFAULT NULL,
  `file_type` varchar(7) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `document_type_id` (`document_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE IF NOT EXISTS `document_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_core` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE IF NOT EXISTS `domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(63) DEFAULT NULL,
  `directory` varchar(63) DEFAULT NULL,
  `theme` varchar(63) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`name`) USING BTREE,
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`id`, `account_id`, `name`, `description`, `directory`, `theme`, `is_active`, `is_primary`, `is_deleted`) VALUES
(1, 1, 'seizethemarket.local', NULL, NULL, NULL, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `email_opt_status_id` int(11) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  `email_type_ma` tinyint(1) DEFAULT NULL,
  `owner_ma` tinyint(1) DEFAULT '0',
  `is_primary` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_account_id` (`email`,`account_id`),
  KEY `email_opt_status_id` (`email_opt_status_id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`),
  KEY `email` (`email`) USING BTREE,
  KEY `is_primary` (`is_primary`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `account_id`, `contact_id`, `email_opt_status_id`, `email`, `email_type_ma`, `owner_ma`, `is_primary`, `is_deleted`) VALUES
(1, 1, 1, NULL, 'admin@seizethemarket.com', NULL, 0, 0, 0),
(2, 1, 8, NULL, 'chris@sethcampbell.com', NULL, 0, 0, 0),
(3, 1, 8, NULL, 'cvhart@me.com', NULL, 0, 0, 0),
(4, 1, 10, NULL, 'joshroy@kw.com', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_messages`
--

CREATE TABLE IF NOT EXISTS `email_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_type_ma` tinyint(1) DEFAULT '0',
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `to_email_id` int(11) DEFAULT NULL,
  `from_email_id` int(11) DEFAULT NULL,
  `processed_datetime` datetime DEFAULT NULL,
  `delivery_status_ma` tinyint(1) DEFAULT '0',
  `needs_response` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_opt_status`
--

CREATE TABLE IF NOT EXISTS `email_opt_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) DEFAULT NULL,
  `subject` int(11) DEFAULT NULL,
  `body` text,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `featured_areas`
--

CREATE TABLE IF NOT EXISTS `featured_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `name` varchar(63) DEFAULT NULL,
  `url` varchar(63) DEFAULT NULL,
  `count_homes` int(11) DEFAULT NULL,
  `count_photo` int(11) DEFAULT NULL,
  `min_price` int(11) DEFAULT NULL,
  `max_price` int(11) DEFAULT NULL,
  `avg_price` int(11) DEFAULT NULL,
  `avg_bedrooms` decimal(3,1) DEFAULT NULL,
  `avg_baths` decimal(3,1) DEFAULT NULL,
  `avg_sf` int(11) DEFAULT NULL,
  `avg_price_sf` int(11) DEFAULT NULL,
  `avg_br1` int(11) DEFAULT NULL,
  `avg_br2` int(11) DEFAULT NULL,
  `avg_br3` int(11) DEFAULT NULL,
  `avg_br4` int(11) DEFAULT NULL,
  `avg_br5` int(11) DEFAULT NULL,
  `avg_dom` int(11) DEFAULT NULL,
  `query` text,
  `updated` datetime DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `status_ma` (`status_ma`) USING BTREE,
  KEY `url` (`url`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `featured_area_types`
--

CREATE TABLE IF NOT EXISTS `featured_area_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `featured_area_type_lu`
--

CREATE TABLE IF NOT EXISTS `featured_area_type_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `featured_area_type_id` int(11) DEFAULT NULL,
  `featured_area_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `featured_area_id` (`featured_area_id`),
  KEY `featured_area_type_id` (`featured_area_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE IF NOT EXISTS `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_type_ma` tinyint(1) DEFAULT NULL,
  `name` varchar(50) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_account_lu`
--

CREATE TABLE IF NOT EXISTS `form_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_fields`
--

CREATE TABLE IF NOT EXISTS `form_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `label` varchar(31) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_field_lu`
--

CREATE TABLE IF NOT EXISTS `form_field_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_account_lu_id` int(11) DEFAULT NULL,
  `form_field_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form_field_id` (`form_field_id`),
  KEY `form_account_lu_id` (`form_account_lu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_submissions`
--

CREATE TABLE IF NOT EXISTS `form_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_account_lu_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL COMMENT '(utm_source)',
  `url` varchar(500) DEFAULT NULL COMMENT 'entry url',
  `domain` varchar(255) DEFAULT NULL COMMENT 'entry domain',
  `entry_page` varchar(255) DEFAULT NULL COMMENT 'entry page',
  `register_page` int(11) DEFAULT NULL,
  `campaign` varchar(255) DEFAULT NULL COMMENT 'product, promo code, slogan (utm_campaign)',
  `keywords` varchar(255) DEFAULT NULL COMMENT '(utm_term)',
  `medium` varchar(127) DEFAULT NULL COMMENT 'ppc, email, newsletter (utm_medium)',
  `added` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_account_lu_id` (`form_account_lu_id`),
  KEY `contact_id` (`contact_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_submission_values`
--

CREATE TABLE IF NOT EXISTS `form_submission_values` (
  `form_account_lu_id` int(11) DEFAULT NULL,
  `form_submission_id` int(11) DEFAULT NULL,
  `form_field_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  KEY `form_submission_id` (`form_submission_id`),
  KEY `form_account_lu_id` (`form_account_lu_id`),
  KEY `form_field_id` (`form_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form_views`
--

CREATE TABLE IF NOT EXISTS `form_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_account_lu_id` int(11) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `viewed` datetime DEFAULT NULL,
  `session_id` varchar(63) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_account_lu_id` (`form_account_lu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goals`
--

CREATE TABLE IF NOT EXISTS `goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '1',
  `name` varchar(63) DEFAULT NULL,
  `year` smallint(4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `average_sales_price` int(11) NOT NULL,
  `average_commission_percent` decimal(2,2) NOT NULL,
  `commission_split` decimal(2,2) DEFAULT '0.50',
  `weeks_time_off` tinyint(2) NOT NULL DEFAULT '6',
  `lead_gen_minutes_per_day` tinyint(4) DEFAULT NULL,
  `income_per_year` int(11) NOT NULL,
  `contracts_closing_conversion` decimal(2,2) DEFAULT NULL,
  `agreements_contract_conversion` decimal(2,2) DEFAULT NULL,
  `appointments_agreement_conversion` decimal(2,2) DEFAULT NULL,
  `contacts_per_appointment` tinyint(4) DEFAULT NULL,
  `contacts_per_day` tinyint(2) DEFAULT NULL,
  `referral_percentage` decimal(2,2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `history_log`
--

CREATE TABLE IF NOT EXISTS `history_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `model_name` varchar(50) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `column_name` varchar(50) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `old_value` varchar(50) DEFAULT NULL,
  `new_value` varchar(50) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `homes_shown`
--

CREATE TABLE IF NOT EXISTS `homes_shown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `shown_by` int(11) DEFAULT '0',
  `homes_saved_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT NULL COMMENT 'scheduled or shown',
  `datetime` datetime DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `wrote_offer` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `shown_by` (`shown_by`),
  KEY `homes_saved_id` (`homes_saved_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE IF NOT EXISTS `leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` int(11) DEFAULT NULL COMMENT 'shift or round robbin',
  `assigned_to_id` int(11) DEFAULT NULL,
  `origin_component_type_id` int(11) DEFAULT NULL,
  `origin_component_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lead_route_id` (`lead_route_id`),
  KEY `assigned_to_id` (`assigned_to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User Groups' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_round_robbin_contact_lu`
--

CREATE TABLE IF NOT EXISTS `lead_round_robbin_contact_lu` (
  `lead_route_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `leads_per_round` tinyint(1) DEFAULT '1',
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  KEY `lead_route_id` (`lead_route_id`),
  KEY `updated_by` (`updated_by`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_routes`
--

CREATE TABLE IF NOT EXISTS `lead_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `type_ma` tinyint(1) DEFAULT NULL COMMENT 'shift or round robbin',
  `component_type_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `default_contact_id` int(11) DEFAULT NULL,
  `overflow_action_ma` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `sort_order` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `default_contact_id` (`default_contact_id`),
  KEY `component_type_id` (`component_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User Groups' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_route_rules`
--

CREATE TABLE IF NOT EXISTS `lead_route_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `field_ma` tinyint(4) DEFAULT NULL COMMENT 'group, source, subsource, time',
  `operator_ma` tinyint(1) DEFAULT '0',
  `value` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lead_route_id` (`lead_route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='value, is_greater_than, is_less_than, is_equal, is_or_equal_to' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lead_shifts`
--

CREATE TABLE IF NOT EXISTS `lead_shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `updated_by` (`updated_by`),
  KEY `lead_route_id` (`lead_route_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_log`
--

CREATE TABLE IF NOT EXISTS `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `market_trends`
--

CREATE TABLE IF NOT EXISTS `market_trends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `criteria` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `market_trend_data`
--

CREATE TABLE IF NOT EXISTS `market_trend_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `market_trend_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `market_trend_id` (`market_trend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_boards`
--

CREATE TABLE IF NOT EXISTS `mls_boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `time_zone_ma` int(11) DEFAULT NULL,
  `feed_type` enum('IDX','VOW') DEFAULT NULL,
  `feed_method` enum('RETS','FTP') DEFAULT NULL,
  `feed_host` varchar(255) DEFAULT NULL,
  `feed_update_frequency` varchar(50) DEFAULT NULL,
  `feed_cron_frequency` varchar(50) DEFAULT NULL,
  `feed_cron_time` varchar(50) DEFAULT NULL,
  `feed_delimeter` varchar(7) DEFAULT NULL,
  `feed_last_updated` datetime DEFAULT NULL,
  `stm_phone_id` int(11) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `short_name_unique` (`short_name`),
  KEY `stm_phone_id` (`stm_phone_id`),
  KEY `state_id` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_board_address_lu`
--

CREATE TABLE IF NOT EXISTS `mls_board_address_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11) DEFAULT NULL,
  `stm_address_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `stm_address_id` (`stm_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_board_contact_lu`
--

CREATE TABLE IF NOT EXISTS `mls_board_contact_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11) DEFAULT NULL,
  `stm_contact_id` int(11) DEFAULT NULL,
  `mls_board_contact_type_ma` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `stm_contact_id` (`stm_contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_error_log`
--

CREATE TABLE IF NOT EXISTS `mls_error_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT NULL,
  `error_code` tinyint(1) DEFAULT NULL,
  `error_message` text,
  `added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_features`
--

CREATE TABLE IF NOT EXISTS `mls_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_feature_group_id` int(11) DEFAULT NULL,
  `column_name` varchar(127) DEFAULT NULL,
  `display_name` varchar(127) DEFAULT NULL,
  `data_type_ma` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `column_name` (`column_name`),
  KEY `mls_feature_group_id` (`mls_feature_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_feature_groups`
--

CREATE TABLE IF NOT EXISTS `mls_feature_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `display_name` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_feed_log`
--

CREATE TABLE IF NOT EXISTS `mls_feed_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11) DEFAULT NULL,
  `mls_property_type_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `start_count` int(11) DEFAULT NULL,
  `end_count` int(11) DEFAULT NULL,
  `update_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_field_maps`
--

CREATE TABLE IF NOT EXISTS `mls_field_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_feature_id` int(11) DEFAULT NULL,
  `mls_board_id` int(11) DEFAULT NULL,
  `mls_property_type_id` int(11) DEFAULT NULL,
  `feed_system_field` varchar(127) DEFAULT NULL,
  `display_name_override` varchar(63) DEFAULT NULL,
  `is_hidden` tinyint(1) DEFAULT '0',
  `parse` tinyint(1) DEFAULT '0',
  `is_standardized` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `mls_feature_id` (`mls_feature_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_property_types`
--

CREATE TABLE IF NOT EXISTS `mls_property_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `table_suffix` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_suffix` (`table_suffix`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mls_property_type_board_lu`
--

CREATE TABLE IF NOT EXISTS `mls_property_type_board_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id` int(11) DEFAULT NULL,
  `mls_property_type_id` int(11) DEFAULT NULL,
  `status_ma` int(11) DEFAULT NULL,
  `feed_table_name` varchar(50) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_gateways`
--

CREATE TABLE IF NOT EXISTS `mobile_gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_name` varchar(63) DEFAULT NULL,
  `sms_gateway` varchar(63) DEFAULT NULL,
  `mms_gateway` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_gateway_phone_lu`
--

CREATE TABLE IF NOT EXISTS `mobile_gateway_phone_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_id` int(11) NOT NULL DEFAULT '0',
  `mobile_gateway_id` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mobile_gateway_id` (`mobile_gateway_id`),
  KEY `phone_id` (`phone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE IF NOT EXISTS `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `phone_type_ma` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `extension` varchar(50) DEFAULT NULL,
  `owner_ma` tinyint(1) DEFAULT '0',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`),
  KEY `company_id` (`company_id`),
  KEY `phone` (`phone`) USING BTREE,
  KEY `phone_type_ma` (`phone_type_ma`) USING BTREE,
  KEY `owner_ma` (`owner_ma`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `account_id`, `contact_id`, `company_id`, `phone_type_ma`, `phone`, `extension`, `owner_ma`, `is_primary`, `is_deleted`) VALUES
(1, 1, 8, NULL, 1, '5089485045', NULL, NULL, 0, 0),
(2, 1, 9, NULL, 1, '9102377355', NULL, NULL, 0, 0),
(3, 1, 10, NULL, 1, '3162045088', NULL, NULL, 0, 0),
(4, 1, 10, NULL, 2, '3169254704', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_items`
--

CREATE TABLE IF NOT EXISTS `project_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE IF NOT EXISTS `referrals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `referred_by` int(11) DEFAULT NULL COMMENT 'contact_id',
  `referred_to` int(11) DEFAULT NULL COMMENT 'contact_id',
  `source_id` int(11) DEFAULT NULL,
  `referral_date` datetime DEFAULT NULL,
  `referral_type_ma` int(11) DEFAULT NULL COMMENT 'buyer, seller, both, agent talent',
  `referral_percent` decimal(4,2) DEFAULT NULL,
  `referral_paid` decimal(11,2) DEFAULT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `closed_date` datetime DEFAULT NULL,
  `commission_gross` decimal(11,2) DEFAULT NULL,
  `commission_net` decimal(11,2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `referred_by` (`referred_by`),
  KEY `referred_to` (`referred_to`),
  KEY `source_id` (`source_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `saved_homes`
--

CREATE TABLE IF NOT EXISTS `saved_homes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `listing_id` varchar(50) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT NULL COMMENT 'like, dislike, seen',
  `notes` varchar(255) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `saved_home_searches`
--

CREATE TABLE IF NOT EXISTS `saved_home_searches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `criteria` text,
  `frequency` text,
  `added_by` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_type_ma` tinyint(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `label` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='User Groups' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting_account_values`
--

CREATE TABLE IF NOT EXISTS `setting_account_values` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `setting_id` int(11) DEFAULT NULL,
  `value` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `account_id` (`account_id`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting_configs`
--

CREATE TABLE IF NOT EXISTS `setting_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `setting_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting_contact_values`
--

CREATE TABLE IF NOT EXISTS `setting_contact_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `setting_id` int(11) DEFAULT NULL,
  `value` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting_options`
--

CREATE TABLE IF NOT EXISTS `setting_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_id` int(11) DEFAULT NULL,
  `value` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE IF NOT EXISTS `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(63) DEFAULT NULL,
  `description` varchar(127) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `account_id`, `parent_id`, `name`, `description`, `added`, `is_deleted`) VALUES
(1, 1, NULL, 'Sphere', NULL, NULL, 0),
(2, 1, NULL, 'Referral', NULL, NULL, 0),
(3, 1, NULL, 'RATE', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `splash_screens`
--

CREATE TABLE IF NOT EXISTS `splash_screens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `content` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stm_addresses`
--

CREATE TABLE IF NOT EXISTS `stm_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(127) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stm_contacts`
--

CREATE TABLE IF NOT EXISTS `stm_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stm_user_group_id` int(11) DEFAULT NULL,
  `stm_contact_status_ma` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `username` varchar(127) DEFAULT NULL,
  `password` varchar(127) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(30) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stm_phones`
--

CREATE TABLE IF NOT EXISTS `stm_phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(50) DEFAULT NULL,
  `extension` varchar(50) DEFAULT NULL,
  `is_primary` tinyint(4) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) DEFAULT '0',
  `component_type_id` int(11) DEFAULT NULL,
  `task_type_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL COMMENT 'value for seller_id, contact_id',
  `assigned_to_id` int(11) DEFAULT NULL,
  `assigned_by_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assigned_to_id` (`assigned_to_id`),
  KEY `assigned_by_id` (`assigned_by_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `account_id` (`account_id`),
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `component_type_component_id` (`component_type_id`,`component_id`) USING BTREE,
  KEY `complete_date` (`complete_date`) USING BTREE,
  KEY `due_date` (`due_date`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `account_id`, `status_ma`, `component_type_id`, `task_type_id`, `component_id`, `assigned_to_id`, `assigned_by_id`, `description`, `due_date`, `complete_date`, `is_deleted`) VALUES
(1, 1, 0, 1, 1, 8, 1, 1, 'f/u', '2013-03-12 00:00:00', NULL, 0),
(2, 1, 0, 1, 1, 9, 1, 1, 'f/u', '2013-03-12 00:00:00', NULL, 0),
(3, 1, 0, 1, 2, 10, 1, 1, 'f/u', '2013-03-12 00:00:00', NULL, 0),
(4, 1, 0, 1, 2, 13, 1, 1, 'f/u', '2013-03-12 00:00:00', NULL, 0),
(5, 1, 0, 1, 2, 14, 1, 1, 'Give feedback on PPC cost', '2013-03-12 00:00:00', NULL, 0),
(6, 1, 0, 1, 2, 17, 1, 1, 'f/u', '2013-03-12 00:00:00', NULL, 0),
(7, 1, 0, 1, 2, 18, 1, 1, 'f/u with offer proposal from Kurt. Find out more about Zurple, need to talk to teammates if can work out some figures.', '2013-03-12 00:00:00', NULL, 0),
(8, 1, 0, 1, 2, 19, 1, 1, 'f/u', '2013-03-12 00:00:00', NULL, 0),
(9, 1, 0, 1, 2, 20, 1, 1, 'f/u', '2013-03-13 00:00:00', NULL, 0),
(10, 1, 0, 1, 2, 21, 1, 1, 'f/u', '2013-03-19 00:00:00', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_email_template_lu`
--

CREATE TABLE IF NOT EXISTS `task_email_template_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT '0',
  `email_template_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  KEY `email_template_id` (`email_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_types`
--

CREATE TABLE IF NOT EXISTS `task_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `task_types`
--

INSERT INTO `task_types` (`id`, `name`, `is_deleted`) VALUES
(1, 'To Do', 0),
(2, 'Phone', 0),
(3, 'Email', 0),
(4, 'Text Message', 0),
(5, 'Video Mail', 0),
(6, 'Handwritten Note', 0),
(7, 'Mail', 0),
(8, 'Log Note', 0),
(9, 'Live Demo', 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_type_component_lu`
--

CREATE TABLE IF NOT EXISTS `task_type_component_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `task_type_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `component_type_id` (`component_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `task_type_component_lu`
--

INSERT INTO `task_type_component_lu` (`id`, `account_id`, `task_type_id`, `component_type_id`, `is_deleted`) VALUES
(1, 1, 1, 1, 0),
(2, 1, 2, 1, 0),
(3, 1, 3, 1, 0),
(4, 1, 4, 1, 0),
(5, 1, 5, 1, 0),
(6, 1, 6, 1, 0),
(7, 1, 7, 1, 0),
(8, 1, 8, 1, 0),
(9, 1, 9, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `timeblocking`
--

CREATE TABLE IF NOT EXISTS `timeblocking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `timeblock_type_ma` tinyint(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) NOT NULL COMMENT 'Seller or Buyer',
  `transaction_status_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `mls_property_type_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `internal_referrer_id` int(11) DEFAULT NULL,
  `notes` varchar(750) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `source_id` (`source_id`),
  KEY `transaction_status_id` (`transaction_status_id`),
  KEY `added_by` (`added_by`),
  KEY `address_id` (`address_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `internal_referrer_id` (`internal_referrer_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_address_lu`
--

CREATE TABLE IF NOT EXISTS `transaction_address_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `address_id` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_closing_costs`
--

CREATE TABLE IF NOT EXISTS `transaction_closing_costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `sales_price` decimal(11,2) DEFAULT NULL,
  `loan_amount` decimal(11,2) DEFAULT NULL,
  `closing_date` date DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `total` decimal(11,2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_closing_cost_items`
--

CREATE TABLE IF NOT EXISTS `transaction_closing_cost_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `name` int(11) DEFAULT NULL,
  `formula` varchar(150) DEFAULT NULL,
  `sort_order` tinyint(2) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_fields`
--

CREATE TABLE IF NOT EXISTS `transaction_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` int(11) DEFAULT NULL COMMENT 'Buyer Seller only',
  `name` varchar(100) DEFAULT NULL,
  `label` varchar(150) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `sort_order` tinyint(4) DEFAULT NULL COMMENT 'may not be needed if views are custom',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `data_type_id` (`data_type_id`),
  KEY `name` (`name`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_field_account_lu`
--

CREATE TABLE IF NOT EXISTS `transaction_field_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `transaction_field_id` int(11) DEFAULT NULL,
  `name_override` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_field_id` (`transaction_field_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_field_values`
--

CREATE TABLE IF NOT EXISTS `transaction_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_field_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `value` varchar(750) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_field_id` (`transaction_field_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_status`
--

CREATE TABLE IF NOT EXISTS `transaction_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `is_core` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_status_account_lu`
--

CREATE TABLE IF NOT EXISTS `transaction_status_account_lu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `component_type_id` int(11) DEFAULT NULL,
  `transaction_status_id` int(11) DEFAULT NULL,
  `override_name` varchar(63) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_status_id` (`transaction_status_id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `viewed_pages`
--

CREATE TABLE IF NOT EXISTS `viewed_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `page_type_ma` tinyint(1) DEFAULT NULL,
  `listing_id` varchar(50) DEFAULT NULL,
  `url` varchar(127) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `page_type_ma` (`page_type_ma`) USING BTREE,
  KEY `listing_id` (`listing_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `account_contact_lu`
--
ALTER TABLE `account_contact_lu`
  ADD CONSTRAINT `account_contact_lu_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `account_contact_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `action_plans`
--
ALTER TABLE `action_plans`
  ADD CONSTRAINT `action_plans_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `action_plans_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `action_plan_applied_lu`
--
ALTER TABLE `action_plan_applied_lu`
  ADD CONSTRAINT `action_plan_applied_lu_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `action_plan_applied_lu_ibfk_2` FOREIGN KEY (`action_plan_item_id`) REFERENCES `action_plan_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `action_plan_items`
--
ALTER TABLE `action_plan_items`
  ADD CONSTRAINT `action_plan_items_ibfk_1` FOREIGN KEY (`action_plan_id`) REFERENCES `action_plans` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `action_plan_items_ibfk_2` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `action_plan_items_ibfk_3` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `action_plan_items_ibfk_4` FOREIGN KEY (`specific_user_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `active_guests`
--
ALTER TABLE `active_guests`
  ADD CONSTRAINT `active_guests_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `active_users`
--
ALTER TABLE `active_users`
  ADD CONSTRAINT `active_users_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD CONSTRAINT `activity_log_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `activity_log_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `activity_log_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `activity_log_ibfk_4` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `address_states` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `addresses_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `address_company_lu`
--
ALTER TABLE `address_company_lu`
  ADD CONSTRAINT `address_company_lu_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `address_company_lu_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `address_contact_lu`
--
ALTER TABLE `address_contact_lu`
  ADD CONSTRAINT `address_contact_lu_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `address_contact_lu_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `assignments_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `assignments_ibfk_3` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `assignment_type_account_lu`
--
ALTER TABLE `assignment_type_account_lu`
  ADD CONSTRAINT `assignment_type_account_lu_ibfk_1` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `assignment_type_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `assignment_type_account_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `closings`
--
ALTER TABLE `closings`
  ADD CONSTRAINT `closings_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `closings_ibfk_2` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `closing_contacts`
--
ALTER TABLE `closing_contacts`
  ADD CONSTRAINT `closing_contacts_ibfk_1` FOREIGN KEY (`closing_id`) REFERENCES `closings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_contents`
--
ALTER TABLE `cms_contents`
  ADD CONSTRAINT `cms_contents_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cms_contents_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cms_contents_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cms_contents_ibfk_4` FOREIGN KEY (`cms_widget_group_id`) REFERENCES `cms_widget_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_content_tag_lu`
--
ALTER TABLE `cms_content_tag_lu`
  ADD CONSTRAINT `cms_content_tag_lu_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cms_content_tag_lu_ibfk_2` FOREIGN KEY (`cms_tag_id`) REFERENCES `cms_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_content_widget_lu`
--
ALTER TABLE `cms_content_widget_lu`
  ADD CONSTRAINT `cms_content_widget_lu_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cms_content_widget_lu_ibfk_2` FOREIGN KEY (`cms_widget_id`) REFERENCES `cms_widgets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_tags`
--
ALTER TABLE `cms_tags`
  ADD CONSTRAINT `cms_tags_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_widgets`
--
ALTER TABLE `cms_widgets`
  ADD CONSTRAINT `cms_widgets_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_widget_groups`
--
ALTER TABLE `cms_widget_groups`
  ADD CONSTRAINT `cms_widget_groups_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_widget_group_lu`
--
ALTER TABLE `cms_widget_group_lu`
  ADD CONSTRAINT `cms_widget_group_lu_ibfk_1` FOREIGN KEY (`cms_widget_id`) REFERENCES `cms_widgets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cms_widget_group_lu_ibfk_2` FOREIGN KEY (`cms_widget_group_id`) REFERENCES `cms_widget_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `companies_ibfk_2` FOREIGN KEY (`primary_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_assignment_lu`
--
ALTER TABLE `company_assignment_lu`
  ADD CONSTRAINT `company_assignment_lu_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_contact_lu`
--
ALTER TABLE `company_contact_lu`
  ADD CONSTRAINT `company_contact_lu_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `company_contact_lu_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_types`
--
ALTER TABLE `company_types`
  ADD CONSTRAINT `company_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_type_lu`
--
ALTER TABLE `company_type_lu`
  ADD CONSTRAINT `company_type_lu_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `company_type_lu_ibfk_2` FOREIGN KEY (`company_type_id`) REFERENCES `company_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `consultations`
--
ALTER TABLE `consultations`
  ADD CONSTRAINT `consultations_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `consultations_ibfk_2` FOREIGN KEY (`met_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `consultations_ibfk_3` FOREIGN KEY (`set_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contact_attributes`
--
ALTER TABLE `contact_attributes`
  ADD CONSTRAINT `contact_attributes_ibfk_1` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contact_attribute_account_lu`
--
ALTER TABLE `contact_attribute_account_lu`
  ADD CONSTRAINT `contact_attribute_account_lu_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `contact_attribute_account_lu_ibfk_2` FOREIGN KEY (`contact_attribute_id`) REFERENCES `contact_attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contact_attribute_values`
--
ALTER TABLE `contact_attribute_values`
  ADD CONSTRAINT `contact_attribute_values_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `contact_attribute_values_ibfk_2` FOREIGN KEY (`contact_attribute_id`) REFERENCES `contact_attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contact_types`
--
ALTER TABLE `contact_types`
  ADD CONSTRAINT `contact_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contact_type_lu`
--
ALTER TABLE `contact_type_lu`
  ADD CONSTRAINT `contact_type_lu_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `contact_type_lu_ibfk_2` FOREIGN KEY (`contact_type_id`) REFERENCES `contact_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `craigslist_templates`
--
ALTER TABLE `craigslist_templates`
  ADD CONSTRAINT `craigslist_templates_ibfk_1` FOREIGN KEY (`craigslist_template_group_id`) REFERENCES `craigslist_template_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `craigslist_templates_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `craigslist_template_groups`
--
ALTER TABLE `craigslist_template_groups`
  ADD CONSTRAINT `craigslist_template_groups_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD CONSTRAINT `custom_fields_ibfk_1` FOREIGN KEY (`custom_field_group_id`) REFERENCES `custom_field_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD CONSTRAINT `custom_field_values_ibfk_1` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `documents_ibfk_3` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `document_types`
--
ALTER TABLE `document_types`
  ADD CONSTRAINT `document_types_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `document_types_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `domains`
--
ALTER TABLE `domains`
  ADD CONSTRAINT `domains_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `emails`
--
ALTER TABLE `emails`
  ADD CONSTRAINT `emails_ibfk_1` FOREIGN KEY (`email_opt_status_id`) REFERENCES `email_opt_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `emails_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `emails_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `featured_areas`
--
ALTER TABLE `featured_areas`
  ADD CONSTRAINT `featured_areas_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `featured_area_types`
--
ALTER TABLE `featured_area_types`
  ADD CONSTRAINT `featured_area_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `featured_area_type_lu`
--
ALTER TABLE `featured_area_type_lu`
  ADD CONSTRAINT `featured_area_type_lu_ibfk_1` FOREIGN KEY (`featured_area_id`) REFERENCES `featured_areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `featured_area_type_lu_ibfk_2` FOREIGN KEY (`featured_area_type_id`) REFERENCES `featured_area_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `form_account_lu`
--
ALTER TABLE `form_account_lu`
  ADD CONSTRAINT `form_account_lu_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `form_field_lu`
--
ALTER TABLE `form_field_lu`
  ADD CONSTRAINT `form_field_lu_ibfk_1` FOREIGN KEY (`form_field_id`) REFERENCES `form_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_field_lu_ibfk_2` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `form_submissions`
--
ALTER TABLE `form_submissions`
  ADD CONSTRAINT `form_submissions_ibfk_1` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_submissions_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_submissions_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `form_submission_values`
--
ALTER TABLE `form_submission_values`
  ADD CONSTRAINT `form_submission_values_ibfk_1` FOREIGN KEY (`form_submission_id`) REFERENCES `form_submissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_submission_values_ibfk_2` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_submission_values_ibfk_3` FOREIGN KEY (`form_field_id`) REFERENCES `form_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `form_views`
--
ALTER TABLE `form_views`
  ADD CONSTRAINT `form_views_ibfk_1` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `goals`
--
ALTER TABLE `goals`
  ADD CONSTRAINT `goals_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_log`
--
ALTER TABLE `history_log`
  ADD CONSTRAINT `history_log_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `history_log_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `homes_shown`
--
ALTER TABLE `homes_shown`
  ADD CONSTRAINT `homes_shown_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `homes_shown_ibfk_2` FOREIGN KEY (`shown_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `homes_shown_ibfk_3` FOREIGN KEY (`homes_saved_id`) REFERENCES `saved_homes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `leads_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `leads_ibfk_2` FOREIGN KEY (`assigned_to_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lead_round_robbin_contact_lu`
--
ALTER TABLE `lead_round_robbin_contact_lu`
  ADD CONSTRAINT `lead_round_robbin_contact_lu_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lead_round_robbin_contact_lu_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lead_round_robbin_contact_lu_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lead_routes`
--
ALTER TABLE `lead_routes`
  ADD CONSTRAINT `lead_routes_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lead_routes_ibfk_2` FOREIGN KEY (`default_contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lead_routes_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lead_route_rules`
--
ALTER TABLE `lead_route_rules`
  ADD CONSTRAINT `lead_route_rules_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lead_shifts`
--
ALTER TABLE `lead_shifts`
  ADD CONSTRAINT `lead_shifts_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lead_shifts_ibfk_2` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lead_shifts_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `login_log`
--
ALTER TABLE `login_log`
  ADD CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `market_trends`
--
ALTER TABLE `market_trends`
  ADD CONSTRAINT `market_trends_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `market_trend_data`
--
ALTER TABLE `market_trend_data`
  ADD CONSTRAINT `market_trend_data_ibfk_1` FOREIGN KEY (`market_trend_id`) REFERENCES `market_trends` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_boards`
--
ALTER TABLE `mls_boards`
  ADD CONSTRAINT `mls_boards_ibfk_1` FOREIGN KEY (`stm_phone_id`) REFERENCES `stm_phones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mls_boards_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `address_states` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_board_address_lu`
--
ALTER TABLE `mls_board_address_lu`
  ADD CONSTRAINT `mls_board_address_lu_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mls_board_address_lu_ibfk_2` FOREIGN KEY (`stm_address_id`) REFERENCES `stm_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_board_contact_lu`
--
ALTER TABLE `mls_board_contact_lu`
  ADD CONSTRAINT `mls_board_contact_lu_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mls_board_contact_lu_ibfk_2` FOREIGN KEY (`stm_contact_id`) REFERENCES `stm_contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_error_log`
--
ALTER TABLE `mls_error_log`
  ADD CONSTRAINT `mls_error_log_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_features`
--
ALTER TABLE `mls_features`
  ADD CONSTRAINT `mls_features_ibfk_1` FOREIGN KEY (`mls_feature_group_id`) REFERENCES `mls_feature_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_feed_log`
--
ALTER TABLE `mls_feed_log`
  ADD CONSTRAINT `mls_feed_log_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mls_feed_log_ibfk_2` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_field_maps`
--
ALTER TABLE `mls_field_maps`
  ADD CONSTRAINT `mls_field_maps_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mls_field_maps_ibfk_2` FOREIGN KEY (`mls_feature_id`) REFERENCES `mls_features` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mls_field_maps_ibfk_3` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mls_property_type_board_lu`
--
ALTER TABLE `mls_property_type_board_lu`
  ADD CONSTRAINT `mls_property_type_board_lu_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mls_property_type_board_lu_ibfk_2` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mobile_gateway_phone_lu`
--
ALTER TABLE `mobile_gateway_phone_lu`
  ADD CONSTRAINT `mobile_gateway_phone_lu_ibfk_1` FOREIGN KEY (`mobile_gateway_id`) REFERENCES `mobile_gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mobile_gateway_phone_lu_ibfk_2` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `phones`
--
ALTER TABLE `phones`
  ADD CONSTRAINT `phones_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `phones_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `phones_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project_items`
--
ALTER TABLE `project_items`
  ADD CONSTRAINT `project_items_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `referrals`
--
ALTER TABLE `referrals`
  ADD CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`referred_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `referrals_ibfk_2` FOREIGN KEY (`referred_to`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `referrals_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `referrals_ibfk_4` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `saved_homes`
--
ALTER TABLE `saved_homes`
  ADD CONSTRAINT `saved_homes_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `saved_home_searches`
--
ALTER TABLE `saved_home_searches`
  ADD CONSTRAINT `saved_home_searches_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `saved_home_searches_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `saved_home_searches_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `setting_account_values`
--
ALTER TABLE `setting_account_values`
  ADD CONSTRAINT `setting_account_values_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `setting_account_values_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `setting_account_values_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `setting_configs`
--
ALTER TABLE `setting_configs`
  ADD CONSTRAINT `setting_configs_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `setting_configs_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `setting_contact_values`
--
ALTER TABLE `setting_contact_values`
  ADD CONSTRAINT `setting_contact_values_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `setting_contact_values_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `setting_contact_values_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `setting_options`
--
ALTER TABLE `setting_options`
  ADD CONSTRAINT `setting_options_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sources`
--
ALTER TABLE `sources`
  ADD CONSTRAINT `sources_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `splash_screens`
--
ALTER TABLE `splash_screens`
  ADD CONSTRAINT `splash_screens_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`assigned_to_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`assigned_by_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tasks_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tasks_ibfk_4` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tasks_ibfk_5` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `task_email_template_lu`
--
ALTER TABLE `task_email_template_lu`
  ADD CONSTRAINT `task_email_template_lu_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `task_email_template_lu_ibfk_2` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `task_type_component_lu`
--
ALTER TABLE `task_type_component_lu`
  ADD CONSTRAINT `task_type_component_lu_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `task_type_component_lu_ibfk_2` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `task_type_component_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `timeblocking`
--
ALTER TABLE `timeblocking`
  ADD CONSTRAINT `timeblocking_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`transaction_status_id`) REFERENCES `transaction_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_5` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_6` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_7` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_8` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions_ibfk_9` FOREIGN KEY (`internal_referrer_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_address_lu`
--
ALTER TABLE `transaction_address_lu`
  ADD CONSTRAINT `transaction_address_lu_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_address_lu_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_closing_costs`
--
ALTER TABLE `transaction_closing_costs`
  ADD CONSTRAINT `transaction_closing_costs_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_closing_cost_items`
--
ALTER TABLE `transaction_closing_cost_items`
  ADD CONSTRAINT `transaction_closing_cost_items_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_closing_cost_items_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_fields`
--
ALTER TABLE `transaction_fields`
  ADD CONSTRAINT `transaction_fields_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_fields_ibfk_2` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_field_account_lu`
--
ALTER TABLE `transaction_field_account_lu`
  ADD CONSTRAINT `transaction_field_account_lu_ibfk_1` FOREIGN KEY (`transaction_field_id`) REFERENCES `transaction_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_field_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_field_values`
--
ALTER TABLE `transaction_field_values`
  ADD CONSTRAINT `transaction_field_values_ibfk_1` FOREIGN KEY (`transaction_field_id`) REFERENCES `transaction_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_field_values_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_status_account_lu`
--
ALTER TABLE `transaction_status_account_lu`
  ADD CONSTRAINT `transaction_status_account_lu_ibfk_1` FOREIGN KEY (`transaction_status_id`) REFERENCES `transaction_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_status_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_status_account_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `viewed_pages`
--
ALTER TABLE `viewed_pages`
  ADD CONSTRAINT `viewed_pages_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
