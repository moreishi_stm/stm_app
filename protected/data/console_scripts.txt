/Applications/MAMP/library/bin/mysql -u root -p  christineleeteam < /Applications/MAMP/htdocs/stm_app-2013-02-28@14-41-09.sql

git diff | vim -

Assignments Refactor TODO:
AssignmentTypeComponentLu -  SCHEMA UPDATE! rename model & table from AssignmentTypeAccountLu, drop is_deleted, drop account_id, drop primary_key, CREATE composite primary key
2) Assignments - SCHEMA UPDATE! remove sort_order, remove is_deleted, add display_name  + data update with display_name and new system version of name
3) Timeblock - SCHEMA UPDATE! Add assignment_type_id column + relationship, ADD row in settings for timeblock_assignment_type of type 2
4) Insert "recruiter" as assignment type #19. Update setting via user setting gui.
5) SettingContactValues - SCHEMA UPDATE - create composite key unique with contact_id and setting_id. should that be PK?? remove id as PK? - remove is_deleted
6) SettingAccountValues - SCHEMA UPDATE - create composite key unique with setting_id?? should that be PK?? remove id as PK? - remove is_deleted, remove account_id
7) SettingConfigs - SCHEMA DELETE - delete table, not used currently and no longer needed with accounts having it's own db.


10) BIG PICTURE QUESTION: What would be the design if there were multiple accounts but 1 mother account who had access to everyone's account info?

QUESTION: ClosingContactLu vs. CompanyAssignmentLu vs. ContactAssignmentLu vs. Assignments

FYI:
1) Removed assignee_type_id from transactions add & edit, Leadgen behavior


TODO:
1) TaskTypeComponentLu - remove pk, is_deleted, make pk composite of task_type_id & component_type_id
2) TaskEmailTemplateLu - remove pk, is_deleted, make pk composite of task_type_id & email_template_id - NOW???
3) Log history of who assigns what from where, etc. - create table - assignments_log
4) Review all _lu tables to deleted un-needed columns ... there are lots!

=============================================================================================================================================
Server commands
1) /var/log/mail.log
2) /var/www/stm_domains/christineleeteam.com/protected/runtime/errors.log
3) Block IPs: /etc/nginx/blockips.conf
4) URL Rewrites: /etc/nginx/yii_default_config (whole site)... /etc/nginx/sites-available/christineleeteam.com (domain specific) ... service nginx reload
5) top (shows you server resource / performance data)

PHP Error Log:
/root/php_errors.log
;error_log=php_errors.log
/root/php_errors.log
root@seizethemarket:~# cat /etc/php5/cgi/php.ini | grep error_log
; server-specific log, STDERR, or a location specified by the error_log
; Set maximum length of log_errors. In error_log information about the source is
error_log = /root/php_errors.log
;error_log = syslog


TO DO's:
http://www.yiiframework.com/doc/api/1.1/CHtml#clientChange-detail
http://www.yiiframework.com/forum/index.php/topic/10895-use-clientchange-with-dropdownlist/page__p__53567__hl__clientChange#entry53567
http://www.yiiframework.com/forum/index.php/topic/19440-need-some-examples-of-chtmlclientchange/page__p__95309__hl__clientChange#entry95309
http://www.yiiframework.com/forum/index.php/topic/14826-validating-form-by-handling-keyup-event/


CLOSINGS
- CHange commission_amount to gross_commission


Commissions
- Lookup table
    :: commission_paid_to,
    :: commission_paid_amount,
    :: commission_paid_date,
    :: commission_type_ma, (sales, referral, bonus)

1) Contact assignments relationship, column name to record_id
2) ALTER TABLE  `assignments` ADD  `assignee_id` INT NOT NULL AFTER  `id`

----------------------------------------------------------------------------------------------------------------------------
view all symlinks: ls -l /etc/nginx/sites-enabled | grep '>'

Update symlink:
1) cd /etc/nginx/sites-available
2) ln -sf demo.seizethemarket.com ../sites-available/

../sites-available/christineleeteam.com
----------------------------------------------------------------------------------------------------------------------------

IMPORT from backup
mysql -u root -p \$2404Mjo0\$ DB_NAME_HERE < /root/stm_app_backup/FILE_NAME_HERE.sql
ex. mysql -u root -p stm_demo < /root/stm_app_backup/

----------------------------------------------------------------------------------------------------------------------------


TODO: taskPortlet.php line:70-80
Dialogues have script register by the name of popTrigger-id.... any possibility of dup if there are multiple dialogues or does Yii take care of it?
If external action is an ajax/api call... should the actio names indicate that? Ex. Tasks... what if we want a non-dialog/api view action?
What is the security of the apiActions? Can anyone make a call to it? Do we need an API Key?

SERVER LOGS:
1) Site specific - domain.com/protected/runtime/error.log
2) Nginx - /var/log/nginx/[domain].access.log

-- Git drop stash loop by Chris
for i in {0..0}; do git stash drop stash@{$i}; done

yiic createMlsTables --boardName=nefar
yiic resyncBoard --boardID=2 --propertyType=residential --clearTable=true

MAC: /Applications/MAMP/htdocs/stm_domains/seizethemarket.com/protected/./yiic "$1";
CLee: /Applications/MAMP/htdocs/stm_domains/seizethemarket.com/protected/./yiic resyncBoard --boardID=2 --propertyType=residential --clearTable=true

ZWID: Zillow - mortgage, property details, valuation, postings
X1-ZWz1df9lsuj6dn_7yjzz
http://www.zillow.com/webservice/GetRateSummary.htm?zws-id=<ZWSID>
http://www.zillow.com/webservice/GetRateSummary.htm?zws-id=X1-ZWz1df9lsuj6dn_7yjzz
http://www.zillow.com/webservice/GetRateSummary.htm?zws-id=X1-ZWz1df9lsuj6dn_7yjzz&state=FL&output=json

Vim Trick:
1) vim -p `grep -ril 'primary_assignment_id' ./`
   Note: -p opens up each one a new tab, -ril is recursive case insensitive l
2) gT gt ... cycles through tabs forward / backwards

Bash refresh
source ~/.bashrc

- Cron tab generator
http://www.generateit.net/cron-job/
To see it in NginX => crontab -e
grep CRON /var/log/syslog

Tailing slow log
tail -f /var/log/mysql/mysql-slow.log

Clear Assets
rm -rf assets/*

TO DO:
1) mlscore - street_direction_suffix - DONE
2) Photo/property_timestamp => photo_last_updated - DONE
3) Street_number => street_num - DONE

4) Change Property Type - System Field - DONE
5) Remove tax_id - update real_estate_parcel + num - DONE
6) MLS Identifier - system field - DONE
7) All bedrooms conform - interior - DONE
8) Virtual tour - property feature - DONE
9) Category ?? - check legacy - DONE
10) mls_approved - property/system - DONE
11) interal_listing_id - system - DONE
12) contingent - boolean? check legacy... legacy has CTG
13) remove board id - DONE
14) add state field to mls_board table, pull FL                           <------------------still needs to be done
15) property types table - unique table suffix - DONE


NAMING:
fl/jacksonville/123-main/123456


{"area":[{"value":"250-","operator":">"},{"value":"270-","operator":"<","conjunctor":"AND"}]}

{"subdivision":[{"value":"%Julington Creek Plan%","operator":"like"}]}


((`Legal Name of Subdiv` like ''%Julington Creek%'' )

CLI:
Screen: window manager for terminal, use if u get kicked out: screen -raAD [id]
Screen shortcuts: http://www.math.utah.edu/docs/info/screen_5.html

Command Line Tips:  http://www.commandlinefu.com/commands/browse
                    http://www.commandlinefu.com/commands/browse/sort-by-votes

See what pkgs are installed: dpkg --get-selections | grep dovecot

Login: ssh root@stmhq.com ...$#@!stmapp!@#$


Installing Postfix & Dovecot
http://www.exratione.com/2012/05/a-mailserver-on-ubuntu-1204-postfix-dovecot-mysql/
http://www.davidgrant.ca/setting_up_postfix_to_send_outgoing_mail_on_ubuntu


DB Logins:
stm_app, $stmapp123$

Wiredtree:


GVoice
270-9070 -
300-2405
300-2390 - help@seizethemarket.com (spectrum: 9047010300)
300-2404
300-2380 - support@seizethemarket.com (spectrum: 9047010320)
300-1640
300-2460