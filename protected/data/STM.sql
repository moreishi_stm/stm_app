-- =============================================================================
-- Diagram Name: STM
-- Created on: 2/9/2013 2:20:41 PM
-- Diagram Version: .1062
-- =============================================================================
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE IF NOT EXISTS `accounts` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id`      INT(11) DEFAULT NULL,
  `account_status_ma` TINYINT(1) DEFAULT NULL,
  `agent_board_num`   VARCHAR(50) DEFAULT NULL,
  `feed_username`     VARCHAR(50) DEFAULT NULL,
  `feed_password`     VARCHAR(50) DEFAULT NULL,
  `contract_date`     DATE DEFAULT NULL,
  `go_live_date`      DATE DEFAULT NULL,
  `notes`             VARCHAR(500) DEFAULT NULL,
  `api_key`           VARCHAR(63) DEFAULT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `account_contact_lu` (
  `id`              INT(11)    NOT NULL AUTO_INCREMENT,
  `contact_id`      INT(11)    NOT NULL DEFAULT '0',
  `account_id`      INT(11)    NOT NULL DEFAULT '0',
  `contact_type_ma` TINYINT(1) NOT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `action_plans` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `component_type_id` INT(11) DEFAULT NULL,
  `status_ma`         TINYINT(1) DEFAULT '0',
  `name`              VARCHAR(75) DEFAULT '0',
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `action_plan_applied_lu` (
  `action_plan_item_id` INT(11) DEFAULT NULL,
  `task_id`             INT(11) DEFAULT '0',
  `is_deleted`          TINYINT(1) DEFAULT NULL,
  KEY `task_id` (`task_id`),
  KEY `action_plan_item_id` (`action_plan_item_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `action_plan_items` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `action_plan_id`    INT(11) DEFAULT NULL,
  `status_ma`         TINYINT(1) DEFAULT '1',
  `task_type_id`      INT(11) DEFAULT NULL
  COMMENT 'email, phone, etc.',
  `email_template_id` INT(11) DEFAULT NULL,
  `description`       VARCHAR(100) DEFAULT NULL,
  `due_days`          INT(11) DEFAULT NULL,
  `due_days_type_ma`  TINYINT(1) DEFAULT NULL,
  `due_reference_ma`  TINYINT(1) DEFAULT NULL,
  `assign_to_type_ma` INT(11) DEFAULT NULL
  COMMENT 'owner, user, closing, specific',
  `if_weekend`        TINYINT(1) DEFAULT NULL
  COMMENT 'move before, after, keep',
  `specific_user_id`  INT(11) DEFAULT NULL,
  `sort_order`        INT(11) DEFAULT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `action_plan_id` (`action_plan_id`),
  KEY `email_template_id` (`email_template_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `specific_user_id` (`specific_user_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

ALTER TABLE `action_plan_items` ADD `depends_on_id` INT(11) DEFAULT NULL;


CREATE TABLE IF NOT EXISTS `active_guests` (
  `id`        INT(11)   NOT NULL AUTO_INCREMENT,
  `domain_id` INT(11) DEFAULT NULL,
  `ip`        VARCHAR(25) DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1
  COMMENT ='User Groups';

CREATE TABLE IF NOT EXISTS `active_users` (
  `id`         INT(11)   NOT NULL AUTO_INCREMENT,
  `contact_id` INT(11) DEFAULT NULL,
  `ip`         VARCHAR(25) DEFAULT NULL,
  `timestamp`  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1
  COMMENT ='User Groups';

CREATE TABLE IF NOT EXISTS `activity_log` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `component_type_id` INT(11) DEFAULT NULL
  COMMENT 'seller, buyer, contact',
  `component_id`      INT(11) DEFAULT NULL,
  `task_type_id`      INT(11) DEFAULT NULL,
  `activity_date`     DATETIME DEFAULT NULL,
  `note`              BLOB,
  `lead_gen_type_ma`  TINYINT(1) DEFAULT '0'
  COMMENT 'new or f/u',
  `is_contact`        TINYINT(1) DEFAULT '0',
  `is_public`         TINYINT(1) DEFAULT '0',
  `added_by`          INT(11) DEFAULT NULL,
  `added`             DATETIME DEFAULT NULL,
  `is_action_plan`    TINYINT(1) DEFAULT '0',
  `is_deleted`        TINYINT(1) DEFAULT '0',
  `origin_type`       INT(11) DEFAULT NULL
  COMMENT 'manual, taks, website',
  `updated`           DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `added_by` (`added_by`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `activity_date` (`activity_date`) USING BTREE,
  KEY `lead_gen_type_ma` (`lead_gen_type_ma`) USING BTREE,
  KEY `is_contact` (`is_contact`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `addresses` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`      INT(11) DEFAULT NULL,
  `address_type_ma` INT(11) DEFAULT NULL,
  `address`         VARCHAR(127) DEFAULT NULL,
  `city`            VARCHAR(50) DEFAULT NULL,
  `state_id`        INT(11) DEFAULT NULL,
  `zip`             VARCHAR(5) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `state_id` (`state_id`),
  KEY `account_id` (`account_id`),
  KEY `address` (`address`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `address_company_lu` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `address_id` INT(11) DEFAULT NULL,
  `company_id` INT(11) DEFAULT NULL,
  `is_primary` TINYINT(1) DEFAULT '0',
  `is_deleted` TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `company_id` (`company_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `address_contact_lu` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `address_id` INT(11) DEFAULT NULL,
  `contact_id` INT(11) DEFAULT NULL,
  `is_primary` TINYINT(1) DEFAULT '0',
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `address_states` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(50) DEFAULT NULL,
  `short_name` VARCHAR(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `appointments` (
  `id`                      INT(11)  NOT NULL AUTO_INCREMENT,
  `component_type_id`       INT(11) DEFAULT NULL,
  `component_id`            INT(11)  NOT NULL,
  `set_by_id`               INT(11)  NOT NULL,
  `met_by_id`               INT(11)  NOT NULL,
  `met_status_ma`           TINYINT(1) DEFAULT '0',
  `is_signed`               TINYINT(1) DEFAULT NULL,
  `not_signed_reason`       TINYINT(1) DEFAULT NULL,
  `not_signed_reason_other` VARCHAR(63) DEFAULT NULL,
  `set_activity_type_id`    INT(11)  NOT NULL,
  `set_on_datetime`         DATETIME NOT NULL,
  `set_for_datetime`        DATETIME DEFAULT NULL,
  `location_ma`             TINYINT(1) DEFAULT NULL,
  `location_other`          VARCHAR(63) DEFAULT NULL,
  `added`                   DATETIME NOT NULL,
  `added_by`                INT(11)  NOT NULL,
  PRIMARY KEY (`id`),
  KEY `met_by_id` (`met_by_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `set_by` (`set_by_id`),
  KEY `set_activity_type_by` (`set_activity_type_id`),
  KEY `added_by` (`added_by`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `assignments` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`         INT(11) DEFAULT NULL,
  `component_type_id`  INT(11) DEFAULT NULL,
  `assignment_type_id` INT(11) DEFAULT NULL,
  `component_id`       INT(11) DEFAULT NULL,
  `is_primary`         TINYINT(1) DEFAULT '0',
  `is_deleted`         TINYINT(1) DEFAULT '0',
  `assignee_id`        INT(11) DEFAULT NULL,
  `assignee_type_id`   INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `contact_id` (`contact_id`),
  KEY `assignment_type_id` (`assignment_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `assignment_types` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(63) DEFAULT NULL,
  `sort_order` INT(11) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `assignment_type_account_lu` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`         INT(11) DEFAULT NULL,
  `component_type_id`  INT(11) DEFAULT NULL,
  `assignment_type_id` INT(11) DEFAULT NULL,
  `is_deleted`         TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assignment_type_id` (`assignment_type_id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `itemname` VARCHAR(64) NOT NULL,
  `userid`   INT(11)     NOT NULL,
  `bizrule`  TEXT,
  `data`     TEXT,
  PRIMARY KEY (`itemname`, `userid`),
  KEY `userid` (`userid`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name`        VARCHAR(64) NOT NULL,
  `label`       VARCHAR(64) DEFAULT NULL,
  `type`        INT(11)     NOT NULL,
  `description` TEXT,
  `bizrule`     TEXT,
  `data`        TEXT,
  PRIMARY KEY (`name`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` VARCHAR(50) NOT NULL,
  `child`  VARCHAR(64) NOT NULL,
  PRIMARY KEY (`parent`, `child`),
  KEY `child` (`child`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `closings` (
  `id`                      INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id`          INT(11) DEFAULT NULL,
  `closing_status_ma`       TINYINT(1) DEFAULT NULL,
  `mls_property_type_id`    INT(11) DEFAULT NULL,
  `price`                   INT(11) DEFAULT NULL,
  `sale_type_ma`            TINYINT(1) DEFAULT '0',
  `financing_type_ma`       TINYINT(4) DEFAULT NULL,
  `loan_amount`             INT(11) DEFAULT NULL,
  `is_new_construction`     TINYINT(1) DEFAULT '0',
  `mls_id`                  VARCHAR(25) DEFAULT '0',
  `is_our_listing`          TINYINT(1) DEFAULT '0',
  `inspection_end_date`     DATE DEFAULT NULL,
  `contingencies`           VARCHAR(255) DEFAULT NULL,
  `notes`                   VARCHAR(750) DEFAULT NULL,
  `is_referral`             TINYINT(1) DEFAULT NULL,
  `referral_commission`     DECIMAL(6, 2) DEFAULT NULL,
  `commission_amount`       DECIMAL(10, 2) DEFAULT NULL,
  `contract_execute_date`   DATE DEFAULT NULL,
  `contract_closing_date`   DATE DEFAULT NULL,
  `actual_closing_datetime` DATETIME DEFAULT NULL,
  `is_mail_away`            TINYINT(1) DEFAULT '0',
  `closing_location`        VARCHAR(255) DEFAULT NULL,
  `is_deleted`              TINYINT(1) DEFAULT '0',
  `title_company_id`        INT(11) DEFAULT NULL,
  `other_agent_id`          INT(11) DEFAULT NULL,
  `lender_id`               INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mls_property_type_id` (`mls_property_type_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `closing_status_ma` (`closing_status_ma`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `closing_contacts` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `closing_id` INT(11) DEFAULT NULL,
  `company_id` INT(11) DEFAULT NULL,
  `contact_id` INT(11) DEFAULT NULL,
  `notes`      VARCHAR(500) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `closing_id` (`closing_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_contents` (
  `id`                  INT(11)     NOT NULL AUTO_INCREMENT,
  `domain_id`           INT(11) DEFAULT NULL,
  `status_ma`           TINYINT(1) DEFAULT '0',
  `type_ma`             TINYINT(1) DEFAULT '1',
  `template_name`       VARCHAR(40) NOT NULL,
  `url`                 VARCHAR(127) DEFAULT NULL,
  `title`               VARCHAR(100) DEFAULT NULL,
  `content_public`      MEDIUMTEXT,
  `content_private`     MEDIUMTEXT,
  `cms_widget_group_id` INT(11) DEFAULT NULL,
  `updated`             DATETIME DEFAULT NULL,
  `updated_by`          INT(11) DEFAULT NULL,
  `added`               DATETIME DEFAULT NULL,
  `added_by`            INT(11) DEFAULT NULL,
  `is_deleted`          TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`domain_id`, `url`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`),
  KEY `cms_widget_group_id` (`cms_widget_group_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_content_tag_lu` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `cms_content_id` INT(11) DEFAULT NULL,
  `cms_tag_id`     INT(11) DEFAULT NULL,
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_content_id` (`cms_content_id`),
  KEY `cms_tag_id` (`cms_tag_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_content_widget_lu` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `cms_content_id` INT(11) DEFAULT NULL,
  `cms_widget_id`  INT(11) DEFAULT NULL,
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_content_id` (`cms_content_id`),
  KEY `cms_widget_id` (`cms_widget_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_tags` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `domain_id`  INT(11) DEFAULT NULL,
  `name`       VARCHAR(63) DEFAULT '0',
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_videos` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `name`            VARCHAR(63) DEFAULT '0',
  `description`     VARCHAR(63) DEFAULT NULL,
  `video_source_ma` TINYINT(1) DEFAULT NULL,
  `video_code`      VARCHAR(127) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_widgets` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`  INT(11) DEFAULT NULL,
  `name`        VARCHAR(63) DEFAULT NULL,
  `folder_name` VARCHAR(63) DEFAULT NULL,
  `description` VARCHAR(127) DEFAULT NULL,
  `is_active`   TINYINT(1) DEFAULT '0',
  `is_deleted`  TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_widget_groups` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`  INT(11) DEFAULT NULL,
  `status_ma`   TINYINT(1) DEFAULT '0',
  `name`        VARCHAR(63) DEFAULT NULL,
  `description` VARCHAR(127) DEFAULT NULL,
  `is_default`  TINYINT(1) DEFAULT '0',
  `is_deleted`  TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `cms_widget_group_lu` (
  `id`                  INT(11) NOT NULL AUTO_INCREMENT,
  `cms_widget_group_id` INT(11) DEFAULT NULL,
  `cms_widget_id`       INT(11) DEFAULT NULL,
  `sort_order`          TINYINT(2) DEFAULT NULL,
  `is_deleted`          TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cms_widget_id` (`cms_widget_id`),
  KEY `cms_widget_group_id` (`cms_widget_group_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `companies` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`         INT(11) DEFAULT NULL,
  `name`               VARCHAR(63) DEFAULT NULL,
  `primary_contact_id` INT(11) DEFAULT NULL,
  `is_deleted`         TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `primary_contact_id` (`primary_contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `company_assignment_lu` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `company_id`        INT(11) DEFAULT NULL
  COMMENT 'The component_id of the assignee to which this component has been assigned.',
  `component_type_id` INT(11) DEFAULT NULL,
  `component_id`      INT(11) DEFAULT NULL,
  `is_primary`        TINYINT(1) DEFAULT '0',
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `company_contact_lu` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `company_id` INT(11) NOT NULL DEFAULT '0',
  `contact_id` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_contact_id` (`company_id`, `contact_id`),
  KEY `company_id` (`company_id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `company_types` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) DEFAULT NULL,
  `name`       VARCHAR(63) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `company_type_lu` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `company_id`      INT(11) DEFAULT NULL,
  `company_type_id` INT(11) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `company_type_id` (`company_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `component_types` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(63) DEFAULT NULL,
  `display_name` VARCHAR(63) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `consultations` (
  `id`               INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id`   INT(11) DEFAULT NULL,
  `status_ma`        TINYINT(1) DEFAULT '0',
  `set_on_datetime`  DATETIME DEFAULT NULL
  COMMENT 'current time it was set',
  `set_by`           INT(11) DEFAULT NULL,
  `set_for_datetime` DATETIME DEFAULT NULL
  COMMENT 'future appt',
  `met_datetime`     DATETIME DEFAULT NULL,
  `met_by`           INT(11) DEFAULT NULL,
  `met_result_ma`    TINYINT(1) DEFAULT '0',
  `met_location`     VARCHAR(63) DEFAULT NULL,
  `is_deleted`       TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `met_by` (`met_by`),
  KEY `set_by` (`set_by`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `contacts` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `source_id`         INT(11) DEFAULT NULL,
  `contact_status_ma` INT(11) DEFAULT NULL,
  `lead_type_ma`      TINYINT(1) DEFAULT NULL,
  `first_name`        VARCHAR(50) DEFAULT NULL,
  `last_name`         VARCHAR(50) DEFAULT NULL,
  `password`          VARCHAR(127) DEFAULT NULL,
  `notes`             VARCHAR(750) DEFAULT NULL,
  `session_id`        VARCHAR(127) DEFAULT NULL,
  `last_login`        DATETIME DEFAULT NULL,
  `added`             DATETIME DEFAULT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `source_id` (`source_id`),
  KEY `first_name` (`first_name`) USING BTREE,
  KEY `last_name` (`last_name`) USING BTREE,
  KEY `last_login` (`last_login`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `contact_assignment_lu` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`         INT(11) NOT NULL,
  `assignment_type_id` INT(11) DEFAULT NULL,
  `component_type_id`  INT(11) NOT NULL,
  `component_id`       INT(11) NOT NULL,
  `is_primary`         TINYINT(1) DEFAULT '0',
  `is_deleted`         TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `contact_attributes` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(63) DEFAULT NULL,
  `label`        VARCHAR(63) DEFAULT NULL,
  `data_type_id` INT(11) DEFAULT NULL,
  `is_deleted`   TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `data_type_id` (`data_type_id`),
  KEY `name` (`name`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `contact_attribute_account_lu` (
  `id`                   INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`           INT(11) DEFAULT NULL,
  `contact_attribute_id` INT(11) DEFAULT NULL,
  `is_deleted`           TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_attribute_id` (`contact_attribute_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `contact_attribute_values` (
  `id`                   INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`           INT(11) NOT NULL DEFAULT '0',
  `contact_attribute_id` INT(11) DEFAULT NULL,
  `value`                VARCHAR(255) DEFAULT NULL,
  `is_deleted`           TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `contact_attribute_id` (`contact_attribute_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `contact_types` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) DEFAULT NULL,
  `name`       VARCHAR(63) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `contact_type_lu` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`      INT(11) DEFAULT NULL,
  `contact_type_id` INT(11) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `contact_type_id` (`contact_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `craigslist_templates` (
  `id`                           INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`                   INT(11) DEFAULT NULL,
  `craigslist_template_group_id` INT(11) DEFAULT NULL,
  `name`                         VARCHAR(50) DEFAULT NULL,
  `content`                      VARCHAR(800) DEFAULT NULL,
  `is_deleted`                   TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craigslist_template_group_id` (`craigslist_template_group_id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `craigslist_template_groups` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) DEFAULT NULL,
  `record_id`  INT(11) DEFAULT NULL,
  `name`       VARCHAR(50) DEFAULT NULL,
  `content`    VARCHAR(800) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `custom_fields` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `custom_field_group_id` INT(11) DEFAULT NULL,
  `name`                  VARCHAR(100) DEFAULT NULL,
  `display_name`          VARCHAR(50) DEFAULT NULL,
  `sort_order`            TINYINT(4) DEFAULT NULL
  COMMENT 'may not be needed if views are custom',
  `is_deleted`            TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `custom_field_group_id` (`custom_field_group_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `custom_field_groups` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` VARCHAR(50) DEFAULT NULL,
  `table_name`      VARCHAR(100) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `custom_field_values` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` INT(11) DEFAULT NULL,
  `record_id`       INT(11) DEFAULT NULL,
  `value`           VARCHAR(50) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `custom_field_id` (`custom_field_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `data_types` (
  `id`   INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `documents` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `component_type_id` INT(11) DEFAULT NULL
  COMMENT 'seller, buyer, closing',
  `component_id`      INT(11) DEFAULT NULL,
  `document_type_id`  INT(11) DEFAULT NULL,
  `description`       VARCHAR(255) DEFAULT NULL,
  `file_path`         VARCHAR(63) DEFAULT NULL,
  `file_name`         VARCHAR(63) DEFAULT NULL,
  `file_type`         VARCHAR(7) DEFAULT NULL,
  `file_size`         INT(11) DEFAULT NULL,
  `added`             DATETIME DEFAULT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `document_type_id` (`document_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `document_types` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` INT(11) DEFAULT NULL,
  `name`              VARCHAR(63) DEFAULT NULL,
  `is_core`           TINYINT(1) DEFAULT '0',
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `domains` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`  INT(11) NOT NULL DEFAULT '0',
  `name`        VARCHAR(63) DEFAULT NULL,
  `description` VARCHAR(63) DEFAULT NULL,
  `directory`   VARCHAR(63) DEFAULT NULL,
  `theme`       VARCHAR(63) DEFAULT NULL,
  `is_active`   TINYINT(1) DEFAULT '1',
  `is_primary`  TINYINT(1) DEFAULT '0',
  `is_deleted`  TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`name`) USING BTREE,
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `emails` (
  `id`                  INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`          INT(11) DEFAULT NULL,
  `contact_id`          INT(11) DEFAULT NULL,
  `email_opt_status_id` INT(11) DEFAULT NULL,
  `email`               VARCHAR(127) DEFAULT NULL,
  `email_type_ma`       TINYINT(1) DEFAULT NULL,
  `owner_ma`            TINYINT(1) DEFAULT '0',
  `is_primary`          TINYINT(1) DEFAULT '0',
  `is_deleted`          TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_account_id` (`email`, `account_id`),
  KEY `email_opt_status_id` (`email_opt_status_id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`),
  KEY `email` (`email`) USING BTREE,
  KEY `is_primary` (`is_primary`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `email_messages` (
  `id`                 INT(11)  NOT NULL AUTO_INCREMENT,
  `origin_type_ma`     TINYINT(1) DEFAULT '0',
  `component_type_id`  INT(11) DEFAULT NULL,
  `component_id`       INT(11) DEFAULT NULL,
  `subject`            VARCHAR(255) DEFAULT NULL,
  `content`            BLOB,
  `to_email_id`        INT(11) DEFAULT NULL,
  `to_email_cc`        TEXT,
  `to_email_bcc`       TEXT,
  `from_email_id`      INT(11) DEFAULT NULL,
  `processed_datetime` DATETIME DEFAULT NULL,
  `delivery_status_ma` TINYINT(1) DEFAULT '0',
  `is_read`            TINYINT(1) DEFAULT '0',
  `added_by`           INT(11)  NOT NULL,
  `added`              DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `email_id` (`to_email_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `email_opt_status` (
  `id`   INT(11) NOT NULL AUTO_INCREMENT,
  `name` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id`                INT(11)    NOT NULL AUTO_INCREMENT,
  `component_type_id` INT(11)    NOT NULL,
  `status_ma`         TINYINT(1) NOT NULL DEFAULT '1',
  `description`       VARCHAR(63) DEFAULT NULL,
  `subject`           VARCHAR(63) DEFAULT NULL,
  `body`              TEXT,
  `is_deleted`        TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `featured_areas` (
  `id`           INT(11)     NOT NULL AUTO_INCREMENT,
  `account_id`   INT(11) DEFAULT NULL,
  `status_ma`    TINYINT(1) DEFAULT '0',
  `name`         VARCHAR(63) DEFAULT NULL,
  `title`        VARCHAR(50) NOT NULL,
  `description`  TEXT        NOT NULL,
  `url`          VARCHAR(63) DEFAULT NULL,
  `count_homes`  INT(11) DEFAULT NULL,
  `count_photo`  INT(11) DEFAULT NULL,
  `min_price`    INT(11) DEFAULT NULL,
  `max_price`    INT(11) DEFAULT NULL,
  `avg_price`    INT(11) DEFAULT NULL,
  `avg_bedrooms` DECIMAL(3, 1) DEFAULT NULL,
  `avg_baths`    DECIMAL(3, 1) DEFAULT NULL,
  `avg_sf`       INT(11) DEFAULT NULL,
  `avg_price_sf` INT(11) DEFAULT NULL,
  `avg_br1`      INT(11) DEFAULT NULL,
  `avg_br2`      INT(11) DEFAULT NULL,
  `avg_br3`      INT(11) DEFAULT NULL,
  `avg_br4`      INT(11) DEFAULT NULL,
  `avg_br5`      INT(11) DEFAULT NULL,
  `avg_dom`      INT(11) DEFAULT NULL,
  `query`        TEXT,
  `updated`      DATETIME DEFAULT NULL,
  `is_default`   TINYINT(1) DEFAULT '0',
  `is_deleted`   TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `status_ma` (`status_ma`) USING BTREE,
  KEY `url` (`url`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `featured_areas_2` (
  `id`           INT(11)     NOT NULL AUTO_INCREMENT,
  `status_ma`    TINYINT(1) DEFAULT '0',
  `name`         VARCHAR(63) DEFAULT NULL,
  `title`        VARCHAR(50) NOT NULL,
  `description`  TEXT,
  `url`          VARCHAR(63) DEFAULT NULL,
  `count_homes`  INT(11) DEFAULT NULL,
  `count_photo`  INT(11) DEFAULT NULL,
  `min_price`    INT(11) DEFAULT NULL,
  `max_price`    INT(11) DEFAULT NULL,
  `avg_price`    INT(11) DEFAULT NULL,
  `avg_bedrooms` DECIMAL(3, 1) DEFAULT NULL,
  `avg_baths`    DECIMAL(3, 1) DEFAULT NULL,
  `avg_sf`       INT(11) DEFAULT NULL,
  `avg_price_sf` INT(11) DEFAULT NULL,
  `avg_br1`      INT(11) DEFAULT NULL,
  `avg_br2`      INT(11) DEFAULT NULL,
  `avg_br3`      INT(11) DEFAULT NULL,
  `avg_br4`      INT(11) DEFAULT NULL,
  `avg_br5`      INT(11) DEFAULT NULL,
  `avg_dom`      INT(11) DEFAULT NULL,
  `updated`      DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status_ma` (`status_ma`) USING BTREE,
  KEY `url` (`url`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `featured_area_types` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) DEFAULT NULL,
  `name`       VARCHAR(63) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `featured_area_type_lu` (
  `featured_area_type_id` INT(11) DEFAULT NULL,
  `featured_area_id`      INT(11) DEFAULT NULL,
  UNIQUE KEY `PK` (`featured_area_type_id`, `featured_area_id`),
  KEY `featured_area_id` (`featured_area_id`),
  KEY `featured_area_type_id` (`featured_area_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `forms` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `form_type_ma` TINYINT(1) DEFAULT NULL,
  `name`         VARCHAR(50) DEFAULT '0',
  `is_deleted`   TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `form_account_lu` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `form_id`           INT(11) DEFAULT NULL,
  `component_type_id` INT(11) NOT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `form_fields` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(50) DEFAULT NULL,
  `label`      VARCHAR(31) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `form_field_lu` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `form_account_lu_id` INT(11) DEFAULT NULL,
  `form_field_id`      INT(11) DEFAULT NULL,
  `is_deleted`         TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form_field_id` (`form_field_id`),
  KEY `form_account_lu_id` (`form_account_lu_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `form_submissions` (
  `id`                 INT(11)   NOT NULL AUTO_INCREMENT,
  `form_account_lu_id` INT(11) DEFAULT NULL,
  `contact_id`         INT(11) DEFAULT NULL,
  `source_id`          INT(11) DEFAULT NULL
  COMMENT '(utm_source)',
  `url`                VARCHAR(500) DEFAULT NULL
  COMMENT 'entry url',
  `domain`             VARCHAR(255) DEFAULT NULL
  COMMENT 'entry domain',
  `entry_page`         VARCHAR(255) DEFAULT NULL
  COMMENT 'entry page',
  `register_page`      INT(11) DEFAULT NULL,
  `campaign`           VARCHAR(255) DEFAULT NULL
  COMMENT 'product, promo code, slogan (utm_campaign)',
  `keywords`           VARCHAR(255) DEFAULT NULL
  COMMENT '(utm_term)',
  `medium`             VARCHAR(127) DEFAULT NULL
  COMMENT 'ppc, email, newsletter (utm_medium)',
  `added`              DATETIME DEFAULT NULL,
  `updated`            TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_account_lu_id` (`form_account_lu_id`),
  KEY `contact_id` (`contact_id`),
  KEY `source_id` (`source_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `form_submission_raw_data` (
  `id`               INT(11)      NOT NULL AUTO_INCREMENT,
  `is_spam`          TINYINT(1)   NOT NULL DEFAULT '0',
  `first_name__1`    VARCHAR(255) DEFAULT NULL
  COMMENT '1',
  `last_name__2`     VARCHAR(255) DEFAULT NULL
  COMMENT '2',
  `email__3`         VARCHAR(255) DEFAULT NULL
  COMMENT '3',
  `phone__4`         VARCHAR(255) DEFAULT NULL
  COMMENT '4',
  `contact_date__5`  VARCHAR(255) DEFAULT NULL
  COMMENT '5',
  `contact_time__6`  VARCHAR(255) DEFAULT NULL
  COMMENT '6',
  `comments__7`      VARCHAR(255) DEFAULT NULL
  COMMENT '7',
  `question__8`      VARCHAR(255) DEFAULT NULL
  COMMENT '8',
  `to_first_name__9` VARCHAR(255) DEFAULT NULL
  COMMENT '9',
  `to_last_name__10` VARCHAR(255) DEFAULT NULL
  COMMENT '10',
  `to_email__11`     VARCHAR(255) DEFAULT NULL
  COMMENT '11',
  `form_title__12`   VARCHAR(255) DEFAULT NULL
  COMMENT '12',
  `password__13`     VARCHAR(255) DEFAULT NULL
  COMMENT '13',
  `address__14`      VARCHAR(255) DEFAULT NULL
  COMMENT '14',
  `unit_num__15`     VARCHAR(255) DEFAULT NULL
  COMMENT '15',
  `city__16`         VARCHAR(255) DEFAULT NULL
  COMMENT '16',
  `state__17`        VARCHAR(255) DEFAULT NULL
  COMMENT '17',
  `zip__18`          VARCHAR(255) DEFAULT NULL
  COMMENT '18',
  `reason__20`       VARCHAR(255) DEFAULT NULL
  COMMENT '20',
  `misc`             VARCHAR(255) DEFAULT NULL,
  `url`              VARCHAR(255) NOT NULL,
  `ip`               VARCHAR(35)  NOT NULL,
  `added`            DATETIME     NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `form_submission_values` (
  `form_account_lu_id` INT(11) DEFAULT NULL,
  `form_submission_id` INT(11) DEFAULT NULL,
  `form_field_id`      INT(11) DEFAULT NULL,
  `value`              VARCHAR(255) DEFAULT NULL,
  KEY `form_submission_id` (`form_submission_id`),
  KEY `form_account_lu_id` (`form_account_lu_id`),
  KEY `form_field_id` (`form_field_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `form_views` (
  `id`                     INT(11)      NOT NULL AUTO_INCREMENT,
  `home_detail_view_count` INT(11) DEFAULT NULL,
  `domain`                 VARCHAR(255) NOT NULL,
  `page`                   VARCHAR(255) NOT NULL,
  `url`                    VARCHAR(500) DEFAULT NULL,
  `referrer`               VARCHAR(255) NOT NULL,
  `utmz`                   TEXT,
  `utma`                   TEXT,
  `utmb`                   TEXT,
  `utmc`                   TEXT,
  `ip`                     VARCHAR(40)  NOT NULL,
  `session_id`             VARCHAR(63) DEFAULT NULL,
  `added`                  DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `goals` (
  `id`                                INT(11)       NOT NULL AUTO_INCREMENT,
  `contact_id`                        INT(11) DEFAULT NULL,
  `status_ma`                         TINYINT(1) DEFAULT '1',
  `name`                              VARCHAR(63) DEFAULT NULL,
  `year`                              SMALLINT(4) DEFAULT NULL,
  `start_date`                        DATE DEFAULT NULL,
  `end_date`                          DATE DEFAULT NULL,
  `average_sales_price`               INT(11)       NOT NULL,
  `average_commission_percent`        DECIMAL(2, 2) NOT NULL,
  `commission_split`                  DECIMAL(2, 2) DEFAULT '0.50',
  `weeks_time_off`                    TINYINT(2)    NOT NULL DEFAULT '6',
  `lead_gen_minutes_per_day`          TINYINT(4) DEFAULT NULL,
  `income_per_year`                   INT(11)       NOT NULL,
  `contracts_closing_conversion`      DECIMAL(2, 2) DEFAULT NULL,
  `agreements_contract_conversion`    DECIMAL(2, 2) DEFAULT NULL,
  `appointments_agreement_conversion` DECIMAL(2, 2) DEFAULT NULL,
  `contacts_per_appointment`          TINYINT(4) DEFAULT NULL,
  `contacts_per_day`                  TINYINT(2) DEFAULT NULL,
  `referral_percentage`               DECIMAL(2, 2) DEFAULT NULL,
  `is_deleted`                        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `history_log` (
  `id`          INT(11)   NOT NULL AUTO_INCREMENT,
  `account_id`  INT(11) DEFAULT NULL,
  `contact_id`  INT(11) DEFAULT NULL,
  `model_name`  VARCHAR(50) DEFAULT NULL,
  `record_id`   INT(11) DEFAULT NULL,
  `column_name` VARCHAR(50) DEFAULT NULL,
  `action`      VARCHAR(50) DEFAULT NULL,
  `old_value`   VARCHAR(50) DEFAULT NULL,
  `new_value`   VARCHAR(50) DEFAULT NULL,
  `timestamp`   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `homes_shown` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`     INT(11) DEFAULT NULL,
  `shown_by`       INT(11) DEFAULT '0',
  `homes_saved_id` INT(11) DEFAULT NULL,
  `status_ma`      TINYINT(1) DEFAULT NULL
  COMMENT 'scheduled or shown',
  `datetime`       DATETIME DEFAULT NULL,
  `notes`          VARCHAR(255) DEFAULT NULL,
  `wrote_offer`    TINYINT(1) DEFAULT '0',
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `shown_by` (`shown_by`),
  KEY `homes_saved_id` (`homes_saved_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `leads` (
  `id`                       INT(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id`            INT(11) DEFAULT NULL
  COMMENT 'shift or round robbin',
  `assigned_to_id`           INT(11) DEFAULT NULL,
  `origin_component_type_id` INT(11) DEFAULT NULL,
  `origin_component_id`      INT(11) DEFAULT NULL,
  `component_type_id`        INT(11) DEFAULT NULL,
  `component_id`             INT(11) DEFAULT NULL,
  `added`                    DATETIME DEFAULT NULL,
  `is_deleted`               TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lead_route_id` (`lead_route_id`),
  KEY `assigned_to_id` (`assigned_to_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1
  COMMENT ='User Groups';

CREATE TABLE IF NOT EXISTS `lead_round_robbin_contact_lu` (
  `lead_route_id`   INT(11) DEFAULT NULL,
  `contact_id`      INT(11) DEFAULT NULL,
  `leads_per_round` TINYINT(1) DEFAULT '1',
  `updated`         DATETIME DEFAULT NULL,
  `updated_by`      INT(11) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  KEY `lead_route_id` (`lead_route_id`),
  KEY `updated_by` (`updated_by`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `lead_routes` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`         INT(11) DEFAULT NULL,
  `type_ma`            TINYINT(1) DEFAULT NULL
  COMMENT 'shift or round robbin',
  `component_type_id`  INT(11) DEFAULT NULL,
  `name`               VARCHAR(63) DEFAULT NULL,
  `description`        VARCHAR(127) DEFAULT NULL,
  `default_contact_id` INT(11) DEFAULT NULL,
  `overflow_action_ma` TINYINT(1) DEFAULT NULL,
  `is_deleted`         TINYINT(1) DEFAULT '0',
  `sort_order`         TINYINT(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `default_contact_id` (`default_contact_id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1
  COMMENT ='User Groups';

CREATE TABLE IF NOT EXISTS `lead_route_rules` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` INT(11) DEFAULT NULL,
  `name`          VARCHAR(50) DEFAULT NULL,
  `field_ma`      TINYINT(4) DEFAULT NULL
  COMMENT 'group, source, subsource, time',
  `operator_ma`   TINYINT(1) DEFAULT '0',
  `value`         VARCHAR(63) DEFAULT NULL,
  `is_deleted`    TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lead_route_id` (`lead_route_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1
  COMMENT ='value, is_greater_than, is_less_than, is_equal, is_or_equal_to';

CREATE TABLE IF NOT EXISTS `lead_shifts` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `lead_route_id` INT(11) DEFAULT NULL,
  `contact_id`    INT(11) DEFAULT NULL,
  `date`          DATE DEFAULT NULL,
  `start_time`    TIME DEFAULT NULL,
  `end_time`      TIME DEFAULT NULL,
  `updated`       DATETIME DEFAULT NULL,
  `updated_by`    INT(11) DEFAULT NULL,
  `is_deleted`    TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `updated_by` (`updated_by`),
  KEY `lead_route_id` (`lead_route_id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `login_log` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id` INT(11) DEFAULT NULL,
  `ip`         VARCHAR(25) DEFAULT NULL,
  `datetime`   DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `market_trends` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`  INT(11) DEFAULT NULL,
  `name`        VARCHAR(63) DEFAULT NULL,
  `description` VARCHAR(127) DEFAULT NULL,
  `criteria`    TEXT,
  `is_deleted`  TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `market_update_log` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`      INT(11) DEFAULT NULL,
  `mls_property_id` INT(11) DEFAULT NULL,
  `send_date`       DATE DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `market_trend_data` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `market_trend_id` INT(11) DEFAULT NULL,
  `date`            DATE DEFAULT NULL,
  `value`           INT(11) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `market_trend_id` (`market_trend_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_boards` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `name`                  VARCHAR(100) DEFAULT NULL,
  `short_name`            VARCHAR(50) DEFAULT NULL,
  `state_id`              INT(11) DEFAULT NULL,
  `time_zone_ma`          INT(11) DEFAULT NULL,
  `feed_type`             ENUM('IDX', 'VOW') DEFAULT NULL,
  `feed_method`           ENUM('RETS', 'FTP') DEFAULT NULL,
  `feed_host`             VARCHAR(255) DEFAULT NULL,
  `feed_update_frequency` VARCHAR(50) DEFAULT NULL,
  `feed_cron_frequency`   VARCHAR(50) DEFAULT NULL,
  `feed_cron_time`        VARCHAR(50) DEFAULT NULL,
  `feed_delimeter`        VARCHAR(7) DEFAULT NULL,
  `feed_last_updated`     DATETIME DEFAULT NULL,
  `stm_phone_id`          INT(11) DEFAULT NULL,
  `notes`                 VARCHAR(500) DEFAULT NULL,
  `is_deleted`            TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `short_name_unique` (`short_name`),
  KEY `stm_phone_id` (`stm_phone_id`),
  KEY `state_id` (`state_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_board_address_lu` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id`   INT(11) DEFAULT NULL,
  `stm_address_id` INT(11) DEFAULT NULL,
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `stm_address_id` (`stm_address_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_board_contact_lu` (
  `id`                        INT(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id`              INT(11) DEFAULT NULL,
  `stm_contact_id`            INT(11) DEFAULT NULL,
  `mls_board_contact_type_ma` TINYINT(1) DEFAULT NULL,
  `is_deleted`                TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `stm_contact_id` (`stm_contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_error_log` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id`  INT(11) DEFAULT NULL,
  `status_ma`     TINYINT(1) DEFAULT NULL,
  `error_code`    TINYINT(1) DEFAULT NULL,
  `error_message` TEXT,
  `added`         DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_features` (
  `id`                   INT(11)    NOT NULL AUTO_INCREMENT,
  `mls_feature_group_id` INT(11) DEFAULT NULL,
  `column_name`          VARCHAR(127) DEFAULT NULL,
  `display_name`         VARCHAR(127) DEFAULT NULL,
  `data_type_ma`         TINYINT(1) NOT NULL,
  `is_deleted`           TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `column_name` (`column_name`),
  KEY `mls_feature_group_id` (`mls_feature_group_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_feature_groups` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `display_name` VARCHAR(50) DEFAULT NULL,
  `is_deleted`   TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

-- Drop table mls_feed_log
DROP TABLE IF EXISTS `mls_feed_log`;

CREATE TABLE IF NOT EXISTS `mls_feed_log` (
  `id`                     INT(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id`           INT(11),
  `mls_property_type_id`   INT(11),
  `run_date`               DATETIME,
  `mls_collection_type`    VARCHAR(200),
  `end_time`               DATETIME,
  `beginning_record_count` INT(11),
  `final_record_count`     INT(11),
  `num_active_records`     INT(11),
  `num_inactive_records`   INT(11),
  `num_records_updated`    INT(11),
  `run_details`            VARCHAR(1000),
  `peak_memory_usage_mb`   DECIMAL(6, 2),
  `runtime_in_seconds`     DECIMAL(6, 2),
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`)
)
  ENGINE =INNODB;

CREATE TABLE IF NOT EXISTS `mls_field_maps` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `mls_feature_id`        INT(11) DEFAULT NULL,
  `mls_board_id`          INT(11) DEFAULT NULL,
  `mls_property_type_id`  INT(11) DEFAULT NULL,
  `feed_system_field`     VARCHAR(127) DEFAULT NULL,
  `display_name_override` VARCHAR(63) DEFAULT NULL,
  `is_hidden`             TINYINT(1) DEFAULT '0',
  `parse`                 TINYINT(1) DEFAULT '0',
  `is_standardized`       TINYINT(1) DEFAULT '0',
  `is_deleted`            TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `mls_feature_id` (`mls_feature_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_property_types` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(50) DEFAULT NULL,
  `table_suffix` VARCHAR(50) DEFAULT NULL,
  `description`  VARCHAR(50) DEFAULT NULL,
  `is_deleted`   TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_suffix` (`table_suffix`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mls_property_type_board_lu` (
  `id`                   INT(11) NOT NULL AUTO_INCREMENT,
  `mls_board_id`         INT(11) DEFAULT NULL,
  `mls_property_type_id` INT(11) DEFAULT NULL,
  `status_ma`            INT(11) DEFAULT NULL,
  `feed_table_name`      VARCHAR(50) DEFAULT NULL,
  `display_name`         VARCHAR(50) DEFAULT NULL,
  `is_deleted`           TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mls_board_id` (`mls_board_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mobile_gateways` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `carrier_name` VARCHAR(63) DEFAULT NULL,
  `sms_gateway`  VARCHAR(63) DEFAULT NULL,
  `mms_gateway`  VARCHAR(63) DEFAULT NULL,
  `is_deleted`   TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mobile_gateway_phone_lu` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `phone_id`          INT(11) NOT NULL DEFAULT '0',
  `mobile_gateway_id` INT(11) NOT NULL DEFAULT '0',
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mobile_gateway_id` (`mobile_gateway_id`),
  KEY `phone_id` (`phone_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `mortgage_rates` (
  `id`       INT(11)       NOT NULL,
  `added`    DATETIME      NOT NULL,
  `state_id` INT(11)       NOT NULL,
  `30_year`  DECIMAL(5, 3) NOT NULL,
  `20_year`  DECIMAL(5, 3) NOT NULL,
  `15_year`  DECIMAL(5, 3) NOT NULL
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `operators` (
  `id`     INT(11)     NOT NULL AUTO_INCREMENT,
  `name`   VARCHAR(63) NOT NULL,
  `symbol` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `phones` (
  `id`            INT(11)    NOT NULL AUTO_INCREMENT,
  `account_id`    INT(11) DEFAULT NULL,
  `contact_id`    INT(11) DEFAULT NULL,
  `company_id`    INT(11) DEFAULT NULL,
  `phone_type_ma` INT(11) DEFAULT NULL,
  `phone`         VARCHAR(50) DEFAULT NULL,
  `extension`     VARCHAR(50) DEFAULT NULL,
  `owner_ma`      TINYINT(1) DEFAULT '0',
  `is_primary`    TINYINT(1) NOT NULL DEFAULT '0',
  `is_deleted`    TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`),
  KEY `company_id` (`company_id`),
  KEY `phone` (`phone`) USING BTREE,
  KEY `phone_type_ma` (`phone_type_ma`) USING BTREE,
  KEY `owner_ma` (`owner_ma`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `projects` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) DEFAULT NULL,
  `contact_id` INT(11) DEFAULT NULL,
  `name`       VARCHAR(63) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `project_items` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `project_id` INT(11) DEFAULT NULL,
  `sort_order` INT(11) DEFAULT NULL,
  `note`       VARCHAR(255) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `recruits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `referred_by_id` int(11) DEFAULT NULL,
  `status_ma` tinyint(1) NOT NULL,
  `recruit_type_ma` int(11) DEFAULT NULL,
  `is_licensed` tinyint(1) NOT NULL,
  `disc_D` tinyint(2) DEFAULT NULL,
  `disc_I` tinyint(2) DEFAULT NULL,
  `disc_S` tinyint(2) DEFAULT NULL,
  `disc_C` tinyint(2) DEFAULT NULL,
  `disc_date` date DEFAULT NULL,
  `ava_vector_1` tinyint(2) DEFAULT NULL,
  `ava_vector_1_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_2` tinyint(2) DEFAULT NULL,
  `ava_vector_2_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_3` tinyint(2) DEFAULT NULL,
  `ava_vector_3_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_4` tinyint(2) DEFAULT NULL,
  `ava_vector_4_asterisk` tinyint(1) DEFAULT NULL,
  `ava_vector_5` tinyint(2) DEFAULT NULL,
  `ava_vector_5_asterisk` tinyint(1) DEFAULT NULL,
  `ava_date` date DEFAULT NULL,
  `current_production` varchar(255) DEFAULT NULL,
  `goal` varchar(127) DEFAULT NULL,
  `dream_goal` varchar(127) DEFAULT NULL,
  `motivation` varchar(127) DEFAULT NULL,
  `application_status_ma` tinyint(1) NOT NULL DEFAULT '0',
  `met_status_ma` tinyint(1) NOT NULL DEFAULT '0',
  `rating` tinyint(1) DEFAULT NULL,
  `strengths` varchar(255) DEFAULT NULL,
  `weaknesses` varchar(255) DEFAULT NULL,
  `communication_skills` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `current_employer` varchar(63) DEFAULT NULL,
  `years_current_job` varchar(127) DEFAULT NULL,
  `years_previous_job` varchar(127) DEFAULT NULL,
  `average_years_job` varchar(127) DEFAULT NULL,
  `notes` text,
  `updated_by` int(11) NOT NULL,
  `updated` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`),
  KEY `source_id` (`source_id`),
  KEY `referred_by_id` (`referred_by_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `recruit_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

INSERT INTO `recruit_types` (`id`, `name`) VALUES
(1, 'Unknown'),
(2, 'Allied Resource'),
(3, 'Admin'),
(4, 'Agent'),
(5, 'Buyer Agent'),
(6, 'Listing Agent'),
(7, 'ISA'),
(8, 'Listing Manager'),
(9, 'Closing Manager'),
(10, 'Executive Assistant');

CREATE TABLE IF NOT EXISTS `recruit_type_lu` (
  `recruit_id` int(11) NOT NULL,
  `recruit_type_id` int(11) NOT NULL,
  PRIMARY KEY (`recruit_id`,`recruit_type_id`),
  KEY `recruit_id` (`recruit_id`),
  KEY `recruit_type_id` (`recruit_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `recruit_type_lu`
ADD CONSTRAINT `recruit_type_lu_ibfk_1` FOREIGN KEY (`recruit_id`) REFERENCES `recruits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `recruit_type_lu_ibfk_2` FOREIGN KEY (`recruit_type_id`) REFERENCES `recruit_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE TABLE IF NOT EXISTS `referrals` (
  `id`               INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`       INT(11) DEFAULT NULL,
  `referred_by`      INT(11) DEFAULT NULL
  COMMENT 'contact_id',
  `referred_to`      INT(11) DEFAULT NULL
  COMMENT 'contact_id',
  `source_id`        INT(11) DEFAULT NULL,
  `referral_date`    DATETIME DEFAULT NULL,
  `referral_type_ma` INT(11) DEFAULT NULL
  COMMENT 'buyer, seller, both, agent talent',
  `referral_percent` DECIMAL(4, 2) DEFAULT NULL,
  `referral_paid`    DECIMAL(11, 2) DEFAULT NULL,
  `price`            DECIMAL(11, 2) DEFAULT NULL,
  `closed_date`      DATETIME DEFAULT NULL,
  `commission_gross` DECIMAL(11, 2) DEFAULT NULL,
  `commission_net`   DECIMAL(11, 2) DEFAULT NULL,
  `is_deleted`       TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `referred_by` (`referred_by`),
  KEY `referred_to` (`referred_to`),
  KEY `source_id` (`source_id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `saved_homes` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id` INT(11) DEFAULT NULL,
  `listing_id` VARCHAR(50) DEFAULT NULL,
  `status_ma`  TINYINT(1) DEFAULT NULL
  COMMENT 'like, dislike, seen',
  `notes`      VARCHAR(255) DEFAULT NULL,
  `ip`         VARCHAR(50) DEFAULT NULL,
  `added`      DATETIME DEFAULT NULL,
  `added_by`   INT(11) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `saved_home_searches` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`  INT(11) DEFAULT NULL,
  `name`        VARCHAR(63) DEFAULT NULL,
  `description` VARCHAR(255) DEFAULT NULL,
  `criteria`    TEXT,
  `frequency`   TEXT,
  `added_by`    INT(11) DEFAULT NULL,
  `added`       DATETIME DEFAULT NULL,
  `updated_by`  INT(11) DEFAULT NULL,
  `updated`     DATETIME DEFAULT NULL,
  `is_deleted`  TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `added_by` (`added_by`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `settings` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `setting_type_ma` TINYINT(1) DEFAULT '0',
  `name`            VARCHAR(50) DEFAULT NULL,
  `label`           VARCHAR(63) DEFAULT NULL,
  `description`     VARCHAR(127) DEFAULT NULL,
  `is_deleted`      TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1
  COMMENT ='User Groups';

CREATE TABLE IF NOT EXISTS `setting_account_values` (
  `id`         INT(1) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) DEFAULT NULL,
  `setting_id` INT(11) DEFAULT NULL,
  `value`      TEXT,
  `updated`    DATETIME DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `account_id` (`account_id`),
  KEY `updated_by` (`updated_by`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `setting_configs` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) DEFAULT NULL,
  `setting_id` INT(11) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `setting_contact_values` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id` INT(11) DEFAULT NULL,
  `setting_id` INT(11) DEFAULT NULL,
  `value`      TEXT,
  `updated`    DATETIME DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `setting_options` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `setting_id` INT(11) DEFAULT NULL,
  `value`      VARCHAR(63) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `setting_id` (`setting_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `sources` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`  INT(11) DEFAULT NULL,
  `parent_id`   INT(11) DEFAULT NULL,
  `name`        VARCHAR(63) DEFAULT NULL,
  `description` VARCHAR(127) DEFAULT NULL,
  `added`       DATETIME DEFAULT NULL,
  `is_deleted`  TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `splash_screens` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id` INT(11) DEFAULT NULL,
  `content`    TEXT,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `stm_addresses` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `address`    VARCHAR(127) DEFAULT NULL,
  `city`       VARCHAR(50) DEFAULT NULL,
  `state`      VARCHAR(50) DEFAULT NULL,
  `zip`        VARCHAR(5) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `stm_contacts` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `stm_user_group_id`     INT(11) DEFAULT NULL,
  `stm_contact_status_ma` INT(11) DEFAULT NULL,
  `first_name`            VARCHAR(50) DEFAULT NULL,
  `last_name`             VARCHAR(50) DEFAULT NULL,
  `username`              VARCHAR(127) DEFAULT NULL,
  `password`              VARCHAR(127) DEFAULT NULL,
  `notes`                 VARCHAR(500) DEFAULT NULL,
  `last_login`            DATETIME DEFAULT NULL,
  `last_login_ip`         VARCHAR(30) DEFAULT NULL,
  `is_deleted`            TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `stm_contact_phone_lu` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `stm_contact_id` INT(11) DEFAULT NULL,
  `stm_phone_id`   INT(11) DEFAULT NULL,
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `stm_contact_id` (`stm_contact_id`),
  KEY `stm_phone_id` (`stm_phone_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `stm_emails` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `stm_contact_id` INT(11) NOT NULL,
  `email`          VARCHAR(100) DEFAULT NULL,
  `is_primary`     TINYINT(1) DEFAULT '0',
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `stm_contact_id` (`stm_contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `stm_history_log` (
  `id`             INT(11)   NOT NULL AUTO_INCREMENT,
  `stm_contact_id` INT(11) DEFAULT NULL,
  `table_name`     VARCHAR(50) DEFAULT NULL,
  `record_id`      INT(11) DEFAULT NULL,
  `column_name`    VARCHAR(50) DEFAULT NULL,
  `action`         VARCHAR(50) DEFAULT NULL,
  `old_value`      VARCHAR(50) DEFAULT NULL,
  `new_value`      VARCHAR(50) DEFAULT NULL,
  `timestamp`      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `stm_contact_id` (`stm_contact_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `stm_phones` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `phone`      VARCHAR(50) DEFAULT NULL,
  `extension`  VARCHAR(50) DEFAULT NULL,
  `is_primary` TINYINT(4) DEFAULT NULL,
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `tasks` (
  `id`                INT(11)  NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `status_ma`         TINYINT(1) DEFAULT '0',
  `component_type_id` INT(11) DEFAULT NULL,
  `task_type_id`      INT(11) DEFAULT NULL,
  `component_id`      INT(11) DEFAULT NULL
  COMMENT 'value for seller_id, contact_id',
  `assigned_to_id`    INT(11) DEFAULT NULL,
  `assigned_by_id`    INT(11) DEFAULT NULL,
  `description`       VARCHAR(255) DEFAULT NULL,
  `due_date`          DATETIME DEFAULT NULL,
  `complete_date`     DATETIME DEFAULT NULL,
  `updated_by`        INT(11)  NOT NULL,
  `updated`           DATETIME NOT NULL,
  `added_by`          INT(11)  NOT NULL,
  `added`             DATETIME NOT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assigned_to_id` (`assigned_to_id`),
  KEY `assigned_by_id` (`assigned_by_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `account_id` (`account_id`),
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `component_type_component_id` (`component_type_id`, `component_id`) USING BTREE,
  KEY `complete_date` (`complete_date`) USING BTREE,
  KEY `due_date` (`due_date`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

ALTER TABLE `tasks` ADD `depends_on_id` INT(11) DEFAULT NULL;


CREATE TABLE IF NOT EXISTS `task_email_template_lu` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `task_id`           INT(11) DEFAULT '0',
  `email_template_id` INT(11) DEFAULT NULL,
  `is_deleted`        TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  KEY `email_template_id` (`email_template_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `task_types` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(50) DEFAULT '0',
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `task_type_component_lu` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `task_type_id`      INT(11) DEFAULT NULL,
  `component_type_id` INT(11) DEFAULT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `task_type_id` (`task_type_id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `terms` (
  `id`        INT(11)     NOT NULL AUTO_INCREMENT,
  `status_ma` TINYINT(1)  NOT NULL DEFAULT '1',
  `name`      VARCHAR(63) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `term_component_lu` (
  `id`                INT(11)    NOT NULL AUTO_INCREMENT,
  `component_type_id` INT(11)    NOT NULL,
  `component_id`      INT(11)    NOT NULL,
  `term_id`           INT(11)    NOT NULL,
  `value`             TEXT       NOT NULL,
  `term_conjunctor`   VARCHAR(5) NOT NULL,
  `group_id`          INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `term_id` (`term_id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `component_id` (`component_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `term_criteria` (
  `id`                 INT(11)      NOT NULL AUTO_INCREMENT,
  `term_id`            INT(11)      NOT NULL,
  `type_ma`            TINYINT(4)   NOT NULL,
  `column`             VARCHAR(127) NOT NULL,
  `operator_id`        INT(11)      NOT NULL,
  `constant_value`     TEXT         NOT NULL,
  `default_conjunctor` VARCHAR(5)   NOT NULL,
  PRIMARY KEY (`id`),
  KEY `term_id` (`term_id`),
  KEY `operator_id` (`operator_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `timeblocking` (
  `id`                 INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`         INT(11) DEFAULT NULL,
  `assignment_type_id` INT(11) DEFAULT NULL,
  `timeblock_type_ma`  TINYINT(1) DEFAULT NULL,
  `date`               DATE DEFAULT NULL,
  `start_time`         TIME DEFAULT NULL,
  `end_time`           TIME DEFAULT NULL,
  `is_deleted`         TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `assignment_type_id` (`assignment_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `traffic_tracker` (
  `id`                     INT(11)     NOT NULL AUTO_INCREMENT,
  `contact_id`             INT(11) DEFAULT NULL,
  `home_detail_view_count` INT(11) DEFAULT NULL,
  `pageview_cookie`        TEXT,
  `domain`                 VARCHAR(255) DEFAULT NULL,
  `page`                   VARCHAR(255) DEFAULT NULL,
  `referrer`               VARCHAR(255) DEFAULT NULL,
  `campaign_source`        VARCHAR(255) DEFAULT NULL,
  `campaign_name`          VARCHAR(255) DEFAULT NULL,
  `campaign_medium`        VARCHAR(255) DEFAULT NULL,
  `campaign_content`       VARCHAR(255) DEFAULT NULL,
  `campaign_term`          VARCHAR(255) DEFAULT NULL,
  `first_visit`            DATETIME DEFAULT NULL,
  `previous_visit`         DATETIME DEFAULT NULL,
  `current_visit_started`  DATETIME DEFAULT NULL,
  `times_visited`          VARCHAR(255) DEFAULT NULL,
  `pages_viewed`           VARCHAR(255) DEFAULT NULL,
  `utmz`                   TEXT,
  `utma`                   TEXT,
  `utmb`                   TEXT,
  `utmc`                   TEXT,
  `ip`                     VARCHAR(40) NOT NULL,
  `session_id`             VARCHAR(63) DEFAULT NULL,
  `added`                  DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transactions` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `component_type_id`     INT(11) NOT NULL
  COMMENT 'Seller or Buyer',
  `transaction_status_id` INT(11) NOT NULL,
  `contact_id`            INT(11) NOT NULL,
  `source_id`             INT(11) NOT NULL,
  `mls_property_type_id`  INT(11) DEFAULT NULL,
  `address_id`            INT(11) DEFAULT NULL,
  `internal_referrer_id`  INT(11) DEFAULT NULL,
  `notes`                 VARCHAR(750) DEFAULT NULL,
  `updated`               DATETIME DEFAULT NULL,
  `updated_by`            INT(11) DEFAULT NULL,
  `added`                 DATETIME DEFAULT NULL,
  `added_by`              INT(11) DEFAULT NULL,
  `is_deleted`            TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `source_id` (`source_id`),
  KEY `transaction_status_id` (`transaction_status_id`),
  KEY `added_by` (`added_by`),
  KEY `address_id` (`address_id`),
  KEY `mls_property_type_id` (`mls_property_type_id`),
  KEY `contact_id` (`contact_id`),
  KEY `updated_by` (`updated_by`),
  KEY `internal_referrer_id` (`internal_referrer_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_address_lu` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` INT(11) DEFAULT NULL,
  `address_id`     INT(11) DEFAULT NULL,
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `address_id` (`address_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_closing_costs` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` INT(11) DEFAULT NULL,
  `sales_price`    DECIMAL(11, 2) DEFAULT NULL,
  `loan_amount`    DECIMAL(11, 2) DEFAULT NULL,
  `closing_date`   DATE DEFAULT NULL,
  `name`           VARCHAR(50) DEFAULT NULL,
  `total`          DECIMAL(11, 2) DEFAULT NULL,
  `is_deleted`     TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_closing_cost_items` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`        INT(11) DEFAULT NULL,
  `component_type_id` INT(11) DEFAULT NULL,
  `name`              INT(11) DEFAULT NULL,
  `formula`           VARCHAR(150) DEFAULT NULL,
  `sort_order`        TINYINT(2) DEFAULT NULL,
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_fields` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `component_type_id` INT(11) DEFAULT NULL
  COMMENT 'Buyer Seller only',
  `name`              VARCHAR(100) DEFAULT NULL,
  `label`             VARCHAR(150) DEFAULT NULL,
  `data_type_id`      INT(11) DEFAULT NULL,
  `sort_order`        TINYINT(4) DEFAULT NULL
  COMMENT 'may not be needed if views are custom',
  `is_deleted`        TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `component_type_id` (`component_type_id`),
  KEY `data_type_id` (`data_type_id`),
  KEY `name` (`name`) USING BTREE,
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_field_account_lu` (
  `id`                   INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`           INT(11) DEFAULT NULL,
  `transaction_field_id` INT(11) DEFAULT NULL,
  `name_override`        VARCHAR(63) DEFAULT NULL,
  `is_deleted`           TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_field_id` (`transaction_field_id`),
  KEY `account_id` (`account_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_field_values` (
  `id`                   INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_field_id` INT(11) DEFAULT NULL,
  `transaction_id`       INT(11) DEFAULT NULL,
  `value`                VARCHAR(750) DEFAULT NULL,
  `is_deleted`           TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_field_id` (`transaction_field_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `is_deleted` (`is_deleted`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_status` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(63) DEFAULT NULL,
  `is_core`    TINYINT(1) DEFAULT '0',
  `is_deleted` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `transaction_status_account_lu` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `account_id`            INT(11) DEFAULT NULL,
  `component_type_id`     INT(11) DEFAULT NULL,
  `transaction_status_id` INT(11) DEFAULT NULL,
  `override_name`         VARCHAR(63) DEFAULT NULL,
  `is_deleted`            TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_status_id` (`transaction_status_id`),
  KEY `account_id` (`account_id`),
  KEY `component_type_id` (`component_type_id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;

CREATE TABLE IF NOT EXISTS `viewed_pages` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `contact_id`   INT(11) DEFAULT NULL,
  `page_type_ma` TINYINT(1) DEFAULT NULL,
  `listing_id`   VARCHAR(50) DEFAULT NULL,
  `url`          VARCHAR(127) DEFAULT NULL,
  `ip`           VARCHAR(50) DEFAULT NULL,
  `added`        DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `page_type_ma` (`page_type_ma`) USING BTREE,
  KEY `listing_id` (`listing_id`) USING BTREE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =latin1;


ALTER TABLE `accounts`
ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `account_contact_lu`
ADD CONSTRAINT `account_contact_lu_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `account_contact_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `action_plans`
ADD CONSTRAINT `action_plans_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `action_plans_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `action_plan_applied_lu`
ADD CONSTRAINT `action_plan_applied_lu_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `action_plan_applied_lu_ibfk_2` FOREIGN KEY (`action_plan_item_id`) REFERENCES `action_plan_items` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `action_plan_items`
ADD CONSTRAINT `action_plan_items_ibfk_1` FOREIGN KEY (`action_plan_id`) REFERENCES `action_plans` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `action_plan_items_ibfk_2` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `action_plan_items_ibfk_3` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `action_plan_items_ibfk_4` FOREIGN KEY (`specific_user_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `active_guests`
ADD CONSTRAINT `active_guests_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `active_users`
ADD CONSTRAINT `active_users_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `activity_log`
ADD CONSTRAINT `activity_log_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `activity_log_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `activity_log_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `activity_log_ibfk_4` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `addresses`
ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `address_states` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `addresses_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `address_company_lu`
ADD CONSTRAINT `address_company_lu_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `address_company_lu_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `address_contact_lu`
ADD CONSTRAINT `address_contact_lu_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `address_contact_lu_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `appointments`
ADD CONSTRAINT `appointments_ibfk_2` FOREIGN KEY (`met_by_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `appointments_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `appointments_ibfk_4` FOREIGN KEY (`set_by_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `appointments_ibfk_5` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `appointments_ibfk_7` FOREIGN KEY (`set_activity_type_id`) REFERENCES `task_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `assignments`
ADD CONSTRAINT `assignments_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `assignments_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `assignments_ibfk_3` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `assignment_type_account_lu`
ADD CONSTRAINT `assignment_type_account_lu_ibfk_1` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `assignment_type_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `assignment_type_account_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `auth_assignment`
ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `auth_item` (`name`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `auth_item_child`
ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `closings`
ADD CONSTRAINT `closings_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `closings_ibfk_2` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `closing_contacts`
ADD CONSTRAINT `closing_contacts_ibfk_1` FOREIGN KEY (`closing_id`) REFERENCES `closings` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `cms_contents`
ADD CONSTRAINT `cms_contents_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `cms_contents_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `cms_contents_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `cms_contents_ibfk_4` FOREIGN KEY (`cms_widget_group_id`) REFERENCES `cms_widget_groups` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `cms_content_tag_lu`
ADD CONSTRAINT `cms_content_tag_lu_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `cms_content_tag_lu_ibfk_2` FOREIGN KEY (`cms_tag_id`) REFERENCES `cms_tags` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `cms_content_widget_lu`
ADD CONSTRAINT `cms_content_widget_lu_ibfk_1` FOREIGN KEY (`cms_content_id`) REFERENCES `cms_contents` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `cms_content_widget_lu_ibfk_2` FOREIGN KEY (`cms_widget_id`) REFERENCES `cms_widgets` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `cms_tags`
ADD CONSTRAINT `cms_tags_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `cms_widgets`
ADD CONSTRAINT `cms_widgets_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `cms_widget_groups`
ADD CONSTRAINT `cms_widget_groups_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `cms_widget_group_lu`
ADD CONSTRAINT `cms_widget_group_lu_ibfk_1` FOREIGN KEY (`cms_widget_id`) REFERENCES `cms_widgets` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `cms_widget_group_lu_ibfk_2` FOREIGN KEY (`cms_widget_group_id`) REFERENCES `cms_widget_groups` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `companies`
ADD CONSTRAINT `companies_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `companies_ibfk_2` FOREIGN KEY (`primary_contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `company_assignment_lu`
ADD CONSTRAINT `company_assignment_lu_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `company_types`
ADD CONSTRAINT `company_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `company_type_lu`
ADD CONSTRAINT `company_type_lu_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `company_type_lu_ibfk_2` FOREIGN KEY (`company_type_id`) REFERENCES `company_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `consultations`
ADD CONSTRAINT `consultations_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `consultations_ibfk_2` FOREIGN KEY (`met_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `consultations_ibfk_3` FOREIGN KEY (`set_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `contacts`
ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `contact_attributes`
ADD CONSTRAINT `contact_attributes_ibfk_1` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `contact_attribute_account_lu`
ADD CONSTRAINT `contact_attribute_account_lu_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `contact_attribute_account_lu_ibfk_2` FOREIGN KEY (`contact_attribute_id`) REFERENCES `contact_attributes` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `contact_attribute_values`
ADD CONSTRAINT `contact_attribute_values_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `contact_attribute_values_ibfk_2` FOREIGN KEY (`contact_attribute_id`) REFERENCES `contact_attributes` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `contact_types`
ADD CONSTRAINT `contact_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `contact_type_lu`
ADD CONSTRAINT `contact_type_lu_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `contact_type_lu_ibfk_2` FOREIGN KEY (`contact_type_id`) REFERENCES `contact_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `craigslist_templates`
ADD CONSTRAINT `craigslist_templates_ibfk_1` FOREIGN KEY (`craigslist_template_group_id`) REFERENCES `craigslist_template_groups` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `craigslist_templates_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `craigslist_template_groups`
ADD CONSTRAINT `craigslist_template_groups_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `custom_fields`
ADD CONSTRAINT `custom_fields_ibfk_1` FOREIGN KEY (`custom_field_group_id`) REFERENCES `custom_field_groups` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `custom_field_values`
ADD CONSTRAINT `custom_field_values_ibfk_1` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `documents`
ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `documents_ibfk_3` FOREIGN KEY (`document_type_id`) REFERENCES `document_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `document_types`
ADD CONSTRAINT `document_types_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `domains`
ADD CONSTRAINT `domains_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `emails`
ADD CONSTRAINT `emails_ibfk_1` FOREIGN KEY (`email_opt_status_id`) REFERENCES `email_opt_status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `emails_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `emails_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `email_messages`
ADD CONSTRAINT `email_messages_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `email_messages_ibfk_2` FOREIGN KEY (`to_email_id`) REFERENCES `emails` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `email_templates`
ADD CONSTRAINT `email_templates_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `featured_areas`
ADD CONSTRAINT `featured_areas_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `featured_area_types`
ADD CONSTRAINT `featured_area_types_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `featured_area_type_lu`
ADD CONSTRAINT `featured_area_type_lu_ibfk_1` FOREIGN KEY (`featured_area_id`) REFERENCES `featured_areas` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `featured_area_type_lu_ibfk_2` FOREIGN KEY (`featured_area_type_id`) REFERENCES `featured_area_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `form_account_lu`
ADD CONSTRAINT `form_account_lu_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `form_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `form_field_lu`
ADD CONSTRAINT `form_field_lu_ibfk_1` FOREIGN KEY (`form_field_id`) REFERENCES `form_fields` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `form_field_lu_ibfk_2` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `form_submissions`
ADD CONSTRAINT `form_submissions_ibfk_1` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `form_submissions_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `form_submissions_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `form_submission_values`
ADD CONSTRAINT `form_submission_values_ibfk_1` FOREIGN KEY (`form_submission_id`) REFERENCES `form_submissions` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `form_submission_values_ibfk_2` FOREIGN KEY (`form_account_lu_id`) REFERENCES `form_account_lu` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `form_submission_values_ibfk_3` FOREIGN KEY (`form_field_id`) REFERENCES `form_fields` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `goals`
ADD CONSTRAINT `goals_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `history_log`
ADD CONSTRAINT `history_log_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `history_log_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `homes_shown`
ADD CONSTRAINT `homes_shown_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `homes_shown_ibfk_2` FOREIGN KEY (`shown_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `homes_shown_ibfk_3` FOREIGN KEY (`homes_saved_id`) REFERENCES `saved_homes` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `leads`
ADD CONSTRAINT `leads_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `leads_ibfk_2` FOREIGN KEY (`assigned_to_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `lead_round_robbin_contact_lu`
ADD CONSTRAINT `lead_round_robbin_contact_lu_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `lead_round_robbin_contact_lu_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `lead_round_robbin_contact_lu_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `lead_routes`
ADD CONSTRAINT `lead_routes_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `lead_routes_ibfk_2` FOREIGN KEY (`default_contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `lead_routes_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `lead_route_rules`
ADD CONSTRAINT `lead_route_rules_ibfk_1` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `lead_shifts`
ADD CONSTRAINT `lead_shifts_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `lead_shifts_ibfk_2` FOREIGN KEY (`lead_route_id`) REFERENCES `lead_routes` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `lead_shifts_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `login_log`
ADD CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `market_trends`
ADD CONSTRAINT `market_trends_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `market_trend_data`
ADD CONSTRAINT `market_trend_data_ibfk_1` FOREIGN KEY (`market_trend_id`) REFERENCES `market_trends` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_boards`
ADD CONSTRAINT `mls_boards_ibfk_1` FOREIGN KEY (`stm_phone_id`) REFERENCES `stm_phones` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mls_boards_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `address_states` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_board_address_lu`
ADD CONSTRAINT `mls_board_address_lu_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mls_board_address_lu_ibfk_2` FOREIGN KEY (`stm_address_id`) REFERENCES `stm_addresses` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_board_contact_lu`
ADD CONSTRAINT `mls_board_contact_lu_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mls_board_contact_lu_ibfk_2` FOREIGN KEY (`stm_contact_id`) REFERENCES `stm_contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_error_log`
ADD CONSTRAINT `mls_error_log_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_features`
ADD CONSTRAINT `mls_features_ibfk_1` FOREIGN KEY (`mls_feature_group_id`) REFERENCES `mls_feature_groups` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_feed_log`
ADD CONSTRAINT `mls_feed_log_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mls_feed_log_ibfk_2` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_field_maps`
ADD CONSTRAINT `mls_field_maps_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mls_field_maps_ibfk_2` FOREIGN KEY (`mls_feature_id`) REFERENCES `mls_features` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mls_field_maps_ibfk_3` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mls_property_type_board_lu`
ADD CONSTRAINT `mls_property_type_board_lu_ibfk_1` FOREIGN KEY (`mls_board_id`) REFERENCES `mls_boards` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mls_property_type_board_lu_ibfk_2` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mobile_gateway_phone_lu`
ADD CONSTRAINT `mobile_gateway_phone_lu_ibfk_1` FOREIGN KEY (`mobile_gateway_id`) REFERENCES `mobile_gateways` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `mobile_gateway_phone_lu_ibfk_2` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `phones`
ADD CONSTRAINT `phones_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `phones_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `phones_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `projects`
ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `project_items`
ADD CONSTRAINT `project_items_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `recruits`
ADD CONSTRAINT `recruits_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `recruits_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `recruits_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `recruits_ibfk_4` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `referrals`
ADD CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`referred_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `referrals_ibfk_2` FOREIGN KEY (`referred_to`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `referrals_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `referrals_ibfk_4` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `saved_homes`
ADD CONSTRAINT `saved_homes_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `saved_home_searches`
ADD CONSTRAINT `saved_home_searches_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `saved_home_searches_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `saved_home_searches_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `setting_account_values`
ADD CONSTRAINT `setting_account_values_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `setting_account_values_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `setting_account_values_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `setting_configs`
ADD CONSTRAINT `setting_configs_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `setting_configs_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `setting_contact_values`
ADD CONSTRAINT `setting_contact_values_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `setting_contact_values_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `setting_contact_values_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `setting_options`
ADD CONSTRAINT `setting_options_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `sources`
ADD CONSTRAINT `sources_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `splash_screens`
ADD CONSTRAINT `splash_screens_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `stm_contact_phone_lu`
ADD CONSTRAINT `stm_contact_phone_lu_ibfk_1` FOREIGN KEY (`stm_contact_id`) REFERENCES `stm_contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `stm_contact_phone_lu_ibfk_2` FOREIGN KEY (`stm_phone_id`) REFERENCES `stm_phones` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `stm_emails`
ADD CONSTRAINT `stm_emails_ibfk_1` FOREIGN KEY (`stm_contact_id`) REFERENCES `stm_contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `stm_history_log`
ADD CONSTRAINT `stm_history_log_ibfk_1` FOREIGN KEY (`stm_contact_id`) REFERENCES `stm_contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `tasks`
ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`assigned_to_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`assigned_by_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `tasks_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `tasks_ibfk_4` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `tasks_ibfk_5` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `task_email_template_lu`
ADD CONSTRAINT `task_email_template_lu_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `task_email_template_lu_ibfk_2` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `task_type_component_lu`
ADD CONSTRAINT `task_type_component_lu_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `task_type_component_lu_ibfk_2` FOREIGN KEY (`task_type_id`) REFERENCES `task_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `task_type_component_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `term_component_lu`
ADD CONSTRAINT `term_component_lu_ibfk_2` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `term_component_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `term_criteria`
ADD CONSTRAINT `term_criteria_ibfk_1` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `term_criteria_ibfk_2` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `timeblocking`
ADD CONSTRAINT `timeblocking_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `timeblocking_ibfk_2` FOREIGN KEY (`assignment_type_id`) REFERENCES `assignment_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transactions`
ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`transaction_status_id`) REFERENCES `transaction_status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_5` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_6` FOREIGN KEY (`mls_property_type_id`) REFERENCES `mls_property_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_7` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_8` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transactions_ibfk_9` FOREIGN KEY (`internal_referrer_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transaction_address_lu`
ADD CONSTRAINT `transaction_address_lu_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transaction_address_lu_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transaction_closing_costs`
ADD CONSTRAINT `transaction_closing_costs_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transaction_closing_cost_items`
ADD CONSTRAINT `transaction_closing_cost_items_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transaction_closing_cost_items_ibfk_2` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transaction_fields`
ADD CONSTRAINT `transaction_fields_ibfk_1` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transaction_fields_ibfk_2` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transaction_field_account_lu`
ADD CONSTRAINT `transaction_field_account_lu_ibfk_1` FOREIGN KEY (`transaction_field_id`) REFERENCES `transaction_fields` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transaction_field_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transaction_field_values`
ADD CONSTRAINT `transaction_field_values_ibfk_1` FOREIGN KEY (`transaction_field_id`) REFERENCES `transaction_fields` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transaction_field_values_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `transaction_status_account_lu`
ADD CONSTRAINT `transaction_status_account_lu_ibfk_1` FOREIGN KEY (`transaction_status_id`) REFERENCES `transaction_status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transaction_status_account_lu_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `transaction_status_account_lu_ibfk_3` FOREIGN KEY (`component_type_id`) REFERENCES `component_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `viewed_pages`
ADD CONSTRAINT `viewed_pages_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS = 1;