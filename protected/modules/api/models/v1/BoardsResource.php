<?php

/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 8/12/13
 */
class BoardsResource extends ApiResource {

	/**
	 * Searchable attribute names for this resource
	 */
	public function attributeNames() {
		return array(
			'name',
		);
	}

	/**
	 * The base model used for this resource
	 */
	public function getResourceModel() {
		Yii::import('seizethemarket.protected.modules.hq.models.MlsBoards');
		return new MlsBoards;
	}
}