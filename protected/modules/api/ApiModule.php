<?php
Yii::setPathOfAlias('stm_app_root', SITE_ROOT . DS . 'stm_app');
Yii::setPathOfAlias('api_module', Yii::getPathOfalias('stm_app_root.protected.modules.api'));

class ApiModule extends CWebModule {

	public $defaultController = 'resource';

	public $layoutPath;
	public $layout = 'column1';

	public function init() {

		$this->setImport(array(
			'admin_module.components.StmBaseActiveRecord',
			'admin_module.models.*',
			'api_module.models.*',
		));

		parent::init();
	}
}
