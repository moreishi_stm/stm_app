<?php
$selectedText = 'selected="selected"';
Yii::app()->clientScript->registerScript('searchSideBar', <<<JS

    // override theme js for transforming checkboxes
    $.fn.ionCheckRadio = function (method) {}

    $('#map-search').change(function(){

        $(this).submit();
    });

    $('#map-search').on('submit', function(event) {
    
        _mapToGLurl = "/homes/";

        $("body").prepend("<div class='loading-container loading'><em></em></div>");

        event.preventDefault();

        var fields = $( this ).serializeArray();
        fields.push({'name': 'ajax', 'value': 'true'});

        if(!verticies && !fromForm) {
            //http://stackoverflow.com/questions/20561245/getting-current-zoom-level-from-a-google-map
            var bounds = map.getBounds();
            var northEast = bounds.getNorthEast();
            var southWest = bounds.getSouthWest();
            var minLat = Math.min(northEast.lat(), southWest.lat());
            var maxLat = Math.max(northEast.lat(), southWest.lat());
            var minLng = Math.min(northEast.lng(), southWest.lng());
            var maxLng = Math.max(northEast.lng(), southWest.lng());
        
            fields.push({'name': 'minLat', 'value': minLat});
            fields.push({'name': 'maxLat', 'value': maxLat});
            fields.push({'name': 'minLng', 'value': minLng});
            fields.push({'name': 'maxLng', 'value': maxLng});
        } else {
            fields.push({'name': 'verts', 'value': JSON.stringify(verticies)});
        }
        
        var finalFields = [];
        var property_types = '';
        for(var i=0; i<fields.length; i++) {
            if(fields[i].name == 'property_types') {
                property_types += fields[i].value+','; 
                continue;
            }
            
            if(fields[i].name.indexOf('_adv') != -1 && fields[i].value.length > 0) {
                var name = fields[i].name.substr(0, fields[i].name.length-4);
                finalFields.push({'name': name, 'value': fields[i].value});
                continue;
            }
            
            if(fields[i].value.length > 0) {
                finalFields.push(fields[i]);
                if(fields[i].name != 'ajax' && fields[i].name != 'verts' && fields[i].name != 'minLat' && fields[i].name != 'maxLat'&& fields[i].name != 'minLng'&& fields[i].name != 'maxLng') {
                    _mapToGLurl += fields[i].name+'/'+fields[i].value+'/';
                }
            }
        }
        
        property_types = property_types.substr(0,property_types.length-1);
        finalFields.push({'name': 'property_types', 'value': property_types});
        _mapToGLurl += 'property_types/'+property_types+'/';
        
        $.ajax({
            type: 'GET',
            url: '/homes/',
            data: finalFields,
            dataType: "json",
            timeout: 30000, // sets timeout to 30 seconds
            success: function (data) {
                makePins(data);
                $('.loading-container').remove();
            }
        });
        
        return false;
    });

    $("#priceRangePicker, #bedroomsPicker, #bathroomsPicker, #advancedPicker").on('click',function() {
        if($("#"+$(this).attr('id')+"-container").is('.ng-hide')) {
            $("#"+$(this).attr('id')+"-container").removeClass('ng-hide');
            if($(this).attr('id') == 'advancedPicker') {
                $(".add-fav").hide();
            } else {
                $(".add-fav").show();
            }
        } else {
            $("#"+$(this).attr('id')+"-container").addClass('ng-hide');
            $(".add-fav").show();
        }

        if($(this).find("i").is('.fa-angle-up')) {
            $(this).find("i").removeClass('fa-angle-up');
            $(this).find("i").addClass('fa-angle-down');
        } else {
            $(this).find("i").removeClass('fa-angle-down');
            $(this).find("i").addClass('fa-angle-up');
        }
    });

    $(document).mouseup(function (e) {
        var containers = [$("#priceRangePicker"), $("#bedroomsPicker"), $("#bathroomsPicker"), $("#advancedPicker")];

        $.each(containers, function(index, elm) {
            if (!elm.is(e.target) // if the target of the click isn't the container...
            && elm.has(e.target).length === 0 && $("#"+elm.attr('id')+"-container").has(e.target).length === 0) // ... nor a descendant of the container
            {
                $("#"+elm.attr('id')+"-container").addClass('ng-hide');
                elm.find("i").removeClass('fa-angle-down');
                elm.find("i").addClass('fa-angle-up');
            }
        });
        $(".add-fav").show();
    });

    $('#price_min, #price_max').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    $("#price_min, #price_max").keyup(function(e) {

        var atLeast = '';
        var under = '';

         if($("#price_min").val().length < 1 && $("#price_max").val().length < 1) {
            $("#priceRangePicker-text-min, #priceRangePicker-text-to, #priceRangePicker-text-max").each(function(index) {
                $(this).addClass('ng-hide');
            });

            $("#priceRangePicker-text-default").removeClass("ng-hide");

            return;
        }

        $("#priceRangePicker-text-default").addClass('ng-hide');

        if($("#price_min").val().length > 0 && $("#price_max").val().length > 0) {
            $("#priceRangePicker-text-min, #priceRangePicker-text-to, #priceRangePicker-text-max").each(function(index) {
                $(this).removeClass('ng-hide');
            });
        }

        if($("#price_min").val().length > 0 && $("#price_max").val().length < 1) {
            atLeast = 'At Least ';
            $("#priceRangePicker-text-min").removeClass('ng-hide');
            $("#priceRangePicker-text-max").addClass('ng-hide');
            $("#priceRangePicker-text-to").addClass('ng-hide');
        }

        if($("#price_max").val().length > 0 && $("#price_min").val().length < 1) {
            under = 'Under ';
            $("#priceRangePicker-text-max").removeClass('ng-hide');
            $("#priceRangePicker-text-min").addClass('ng-hide');
            $("#priceRangePicker-text-to").addClass('ng-hide');
        }

        if($("#price_min").val().length > 6) {
            $("#priceRangePicker-text-min").text(atLeast+'$' + (parseFloat($("#price_min").val(), 10) / 1000000).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+ 'M');
        } else {
            if($("#price_min").val().length > 0) {
                $("#priceRangePicker-text-min").text(atLeast+'$' + parseFloat($("#price_min").val(), 10).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
            } else {
                $("#priceRangePicker-text-min").text('');
            }
        }

        if($("#price_max").val().length > 6) {
            $("#priceRangePicker-text-max").text(under+'$' + (parseFloat($("#price_max").val(), 10) / 1000000).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+ 'M');
        } else {
            if($("#price_max").val().length > 0) {
                $("#priceRangePicker-text-max").text(under+'$' + parseFloat($("#price_max").val(), 10).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
            } else {
                $("#priceRangePicker-text-max").text('');
            }
        }
    });

    $("#bedroomsPicker-container .bedrooms").change(function() {

         $("#bedrooms-selected-text").text('');

        if($(this).is(":checked"))
            $("#bedrooms-selected-text").text($(this).val()+"+");
    });

    $("#bathroomsPicker-container .bathrooms").change(function() {

         $("#bathrooms-selected-text").text('');

        if($(this).is(":checked"))
            $("#bathrooms-selected-text").text($(this).val()+"+");
    });

    $(".uc-searchBar-panelCloseIcon-i").click(function() {
        $('#advancedPicker').trigger('click');
    });


    $("#resetForm").click(function() {
        $("#map-search").get(0).reset();
        $('#map-search').submit();
    });
JS
    . "//JTNEJTNERFg3TmxwaDlKbjBJVW9pTUtNbXBVRHVjem9jNUhNeDkyRHRISm9jRVVWYkFKcWdPbG9pRVVWNVMycXRIenF1dVRWMTlKcnRaS25iRVVWYTVKbnhTSk1sT3ZwMTlKcnRMSkY="
);
//base64_encode(urlencode(str_rot13(strrev(base64_encode()
Yii::app()->clientScript->registerCss('searchSideBarCss', <<<CSS

    .btn.home-list-search-button {
        width: 100%;
    }
    .btn.home-list-search-button:hover {
        background-color: #25ad29;
    }
    #home-listview-search .form-group select:hover, #home-listview-search .form-group input:hover {
        background-color: #efefef;
        border: solid 2px #888;
    }
    #home-listview-search .form-control:focus {
        background-color: transparent;
        border: 1px solid #ccc;
    }
    @media (max-width: 768px) {
        #home-listview-search .panel-title {
            margin-top: 16px;
        }
    }
    input[type="checkbox"] {
        display: inline-block;
        height: 23px;
        width: 23px;
        overflow: hidden;
        margin-top: -4px;
        vertical-align: middle;
        outline: 0px none;
        background: #FFF none repeat scroll 0% 0%;
        border-radius: 2px;
        border: 1px solid #C8C8C8;
        box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.15) inset;
    }
    #collapseTypes label {
        font-weight: normal;
        position: relative;
        top: 5px;
        font-size: 14px;
    }

    .panel-default > .panel-heading {
        border-top: 0 !important;
    }
    .panel-body h5 {
        font-weight: bold;
        padding-bottom: 0;
        padding-top: 12px;
        font-size: 12px;
        text-transform: uppercase;
        color: #666;
        margin-bottom: -1px;
    }
    .activeFilter {
        background: #E6F2FF !important;
        border: 2px solid #337ABF !important;
    }
CSS
);

$form = $this->beginWidget('CActiveForm', array(
	'method' => 'get',
	'id' => 'map-search',
	'action' => $action
));

$priceList = StmFormHelper::getPriceList();
?>
<input type="hidden" name="MlsProperties_page" value="<?=(isset($_GET['MlsProperties_page']) ? $_GET['MlsProperties_page']: 1); ?>" id="MlsProperties_pager" />
    <div class="uc-searchBar-container">
        <div class="uc-searchBar">
            <div class="uc-searchBar-field uc-searchBar-field--searchIcon">
                <svg class="uc-searchBar--magnifyingGlass">
                    <i class="fa fa-2x fa-search"></i>
                </svg>
            </div>
            <div class="uc-searchBar-field uc-searchBar-field--omnibox" data-tn="searchBar-menu-searchBar">
                <div class="select2-container select2-container-multi ng-untouched ng-valid ng-not-empty uc-omnibox ng-valid-required ng-dirty" id="s2id_autogen1">
                    <ul class="select2-choices">
                        <li class="select2-search-field">
                            <label for="keywords" class="select2-offscreen"></label>
                            <?php $keywords = (isset($_GET['keywords']))? $_GET['keywords'] : null; ?>
                            <input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input select2-default" id="keywords" placeholder="Keywords" style="width: 1265px;" name="keywords" value="<?=$keywords?>">
                        </li>
                        <li style="margin-right: 20px; display:none;">
                            <input type="submit" class="btn btn-lg btn-primary home-list-search-button" value="Search" />
                        </li>
                        <li style=" display:none;">
                            <input type="button" class="btn btn-primary btn-lg" style="background: #00ACE4;" id="resetForm" value="Clear" />
                        </li>
                    </ul>
                </div>
            </div>
            <div class="uc-searchBar-field uc-searchBar-field--priceRange uc-searchBar-field--large"   tabindex="-1">
                <div class="uc-searchBar-fieldTitle uc-searchBar-fieldTitle--priceRange ng-binding"  data-tn="searchBar-menu-priceRange" id="priceRangePicker">
                    <span id="priceRangePicker-text-default">Price Range</span>
                    <span id="priceRangePicker-text-min" class="ng-hide"></span>
                    <span id="priceRangePicker-text-to" class="ng-hide">to</span>
                    <span id="priceRangePicker-text-max" class="ng-hide"></span>
                    <div class="uc-searchBar-fieldIconContainer">
                        <svg class="uc-searchBar-caret">
                            <i class="fa fa-angle-up"></i>
                        </svg>
                    </div>
                </div>
                <div class="uc-searchBar-panelContainer">
                    <div class="uc-searchBar-panel uc-searchBar-panel--priceRangePicker ng-hide" id="priceRangePicker-container"  >
                        <input uc-input-type="price"  data-tn="searchBar-filter-minPrice"  placeholder="Min Price" class="ng-valid ng-isolate-scope ng-not-empty ng-touched ng-dirty ng-valid-uc-input-type-price ng-valid-parse" type="text" name="price_min" id="price_min"  value="<?=(floatval($_GET['price_min'])) ? floatval($_GET['price_min']) : ""; ?>">
                        <span class="uc-searchBar-inputDivider">To:</span>
                        <input uc-input-type="price"  data-tn="searchBar-filter-maxPrice"  placeholder="Max Price" class="ng-valid ng-isolate-scope ng-not-empty ng-dirty ng-valid-uc-input-type-price ng-valid-parse ng-touched" type="text" name="price_max" id="price_max" value="<?=(floatval($_GET['price_max'])) ? floatval($_GET['price_max']) : ""; ?>">
                    </div>
                </div>
            </div>
            <div class="uc-searchBar-field uc-searchBar-field--bedrooms uc-searchBar-field--large"   tabindex="-1">
                <div class="uc-searchBar-fieldTitle uc-searchBar-fieldTitle--bedrooms ng-binding"  data-tn="searchBar-menu-beds" id="bedroomsPicker"><span id="bedrooms-selected-text"></span> Beds
                    <div class="uc-searchBar-fieldIconContainer">
                        <svg class="uc-searchBar-caret">
                            <i class="fa fa-angle-up"></i>
                        </svg>
                    </div>
                </div>
                <div class="uc-searchBar-panelContainer">
                    <div class="uc-searchBar-panel uc-searchBar-panel--bedroomsPicker ng-hide" id="bedroomsPicker-container"  >
                        <div class="uc-searchBar-bedroomsPicker">
                            <?php
                            $bedRooms = StmFormHelper::getBedroomList('+ Beds');
                            foreach($bedRooms as $value => $label):
                                if($value > 6) {
                                    continue;
                                }
                                $selected = "";
                                if(isset($_GET['bedrooms']) && intval($value) ==  intval($_GET['bedrooms'])) {
                                    $selected = ' checked="checked"';
                                }
                            ?>
                            <label class="uc-searchBar-bathroomsPicker-label ng-binding ng-scope" >
                                <input class="c0-checkbox bedrooms"  data-tn="searchBar-filter-1 bedrooms"  type="radio" name="bedrooms" value="<?=$value; ?>" <?=$selected;?>><?=$label; ?>
                            </label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uc-searchBar-field uc-searchBar-field--bathrooms uc-searchBar-field--large"   tabindex="-1" >
                <div class="uc-searchBar-fieldTitle uc-searchBar-fieldTitle--bedrooms ng-binding"  data-tn="searchBar-menu-baths" id="bathroomsPicker"><span id="bathrooms-selected-text"></span> Baths
                    <div class="uc-searchBar-fieldIconContainer">
                        <svg class="uc-searchBar-caret">
                            <i class="fa fa-angle-up"></i>
                        </svg>
                    </div>
                </div>
                <div class="uc-searchBar-panelContainer">
                    <div class="uc-searchBar-panel uc-searchBar-panel--bathroomsPicker ng-hide" id="bathroomsPicker-container"  >
                        <div class="uc-searchBar-bathroomsPicker">
                            <?php
                            $baths = StmFormHelper::getBathList();
                            foreach($baths as $value => $label):
                                $selected = "";
                                if(isset($_GET['baths_total']) && intval($value) ==  intval($_GET['baths_total'])) {
                                    $selected = ' checked="checked';
                                }?>
                            <label class="uc-searchBar-bathroomsPicker-label ng-binding ng-scope" >
                                <input type="radio"  class="ng-pristine ng-untouched ng-valid ng-not-empty bathrooms" name="baths_total" value="<?=$value; ?>" <?=$selected;?>><?=$label; ?>
                            </label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uc-searchBar-field uc-searchBar-field--advanced"  data-tn="searchBar-menu-moreFilters"   tabindex="-1">
                <div class="uc-searchBar-fieldTitle uc-searchBar-fieldTitle--advanced"  data-tn="searchBar-menu-moreFilters" id="advancedPicker">
                    <div class="uc-searchBar-filterIconContainer">
                        <svg class="uc-searchBar-filterIcon uc-searchBar-field--small uc-searchBar-field--xs" title="Filters">
                            <i class="fa fa-filter"></i>
                        </svg>
                    </div>
                    <div class="uc-searchBar-fieldIconContainer uc-searchBar-fieldIconContainer--advanced">
                        <svg class="uc-searchBar-caret">
                            <i class="fa fa-angle-up"></i>
                        </svg>
                    </div>
                </div>
                <div class="uc-searchBar-advancedPanelContainer">
                    <div class="uc-searchBar-panel uc-searchBar-panel--advanced ng-hide" id="advancedPicker-container"  >
                        <div>
                            <div class="uc-searchBar-advancedFilterTab uc-searchBar-field--small">
                                <svg class="uc-searchBar-panelCloseIcon" >
                                    <i class="fa fa-2x fa-times uc-searchBar-panelCloseIcon-i"></i>
                                </svg>
                                <div class="uc-searchBar-advancedFilterLabel"> Price </div>
                                <div class="uc-searchBar-advancedFilter">
                                    <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-isolate-scope ng-not-empty" uc-input-type="price"  data-tn="searchBar-filter-minPrice"  placeholder="Min Price" type="text" name="price_min_adv" value="<?=(floatval($_GET['price_min'])) ? floatval($_GET['price_min']) : ""; ?>">
                                    <span class="uc-searchBar-inputDivider">To:</span>
                                    <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-isolate-scope ng-not-empty" uc-input-type="price"  data-tn="searchBar-filter-maxPrice"  placeholder="Max Price" type="text" name="price_max_adv" value="<?=(floatval($_GET['price_max'])) ? floatval($_GET['price_max']) : ""; ?>">
                                </div>
                                <div class="uc-searchBar-advancedFilterLabel"> Beds </div>
                                <div class="uc-searchBar-advancedFilter c0-grid">
                                    <div class="uc-searchBar-lost-1_1">

                                        <?php
                                        $bedRooms = StmFormHelper::getBedroomList('+ Beds');
                                        foreach($bedRooms as $value => $label):
                                            if($value > 6) {
                                                continue;
                                            }
                                            $selected = "";
                                            if(isset($_GET['bedrooms']) && intval($value) ==  intval($_GET['bedrooms'])) {
                                                $selected = ' checked="checked"';
                                            }
                                            ?>
                                            <div class="uc-searchBar-lostColumn ng-scope" >
                                                <label class="uc-searchBar-checkboxLabel ng-binding">
                                                    <input class="c0-checkbox uc-searchBar-advancedFilterCheckbox" type="radio" name="bedrooms_adv" value="<?=$value;?>" <?=$selected;?>><?=$label;?> </label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="uc-searchBar-advancedFilter"></div>
                                <div class="uc-searchBar-advancedFilterLabel"> Bathrooms </div>
                                <div class="uc-searchBar-advancedFilter c0-grid">
                                    <div class="uc-searchBar-lost-1_1">
                                        <?php
                                        $baths = StmFormHelper::getBathList();
                                        foreach($baths as $value => $label):
                                            $selected = "";
                                            if(isset($_GET['baths_total']) && intval($value) ==  intval($_GET['baths_total'])) {
                                                $selected = ' checked="checked"';
                                            }?>
                                            <div class="uc-searchBar-lostColumn ng-scope" >
                                                <label class="uc-searchBar-checkboxLabel ng-binding">
                                                    <input class="uc-searchBar-advancedFilterRadio ng-pristine ng-untouched ng-valid ng-not-empty" type="radio" data-tn="searchBar-filter-1 bathrooms"  name="baths_total_adv"  value="<?=$value; ?>"<?=$selected;?>><?=$label; ?></label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="uc-searchBar-advancedFilter"></div>
                                <div class="uc-searchBar-advancedFilter--spacer"></div>
                            </div>
                            <div   class="ng-scope">
                                <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > Property Type </div>
                                <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                    <div class="uc-searchBar-lost-1_1">
                                        <?php

                                        $types = CHtml::listData(MlsPropertyTypeBoardLu::model()->displayOrder()->findAll(array('condition' =>'status_ma=1 AND mls_board_id='.Yii::app()->user->boardId)),'mls_property_type_id','displayName');

                                        if(isset($_GET['property_types'])) {
                                            $propertyTypeIds = explode(',', urldecode($_GET['property_types']));
                                        }
                                        else {
                                            $propertyTypeIds = MlsPropertyTypeBoardLu::getDefaultPropertyTypeIds(Yii::app()->user->board->id);
                                        }

                                        foreach($types as $value => $label):
                                            $checked = "";
                                            if(in_array($value, $propertyTypeIds) ) {
                                                $checked = 'checked="checked"';
                                            } ?>
                                            <div class="uc-searchBar-advancedFilterInputsContainer ng-scope uc-searchBar-lostColumn">
                                                <div class="ng-scope">
                                                    <div class="uc-searchBar-advancedPanelCheckboxContainer">
                                                        <label class="uc-searchBar-checkboxLabel">
                                                            <input class="c0-checkbox uc-searchBar-advancedFilterCheckbox" type="checkbox" name="property_types" value="<?=$value;?>" <?=$checked?>>
                                                            <span class="uc-searchBar-advancedFilterCheckboxLabelText ng-binding"><?=$label;?></span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="uc-searchBar-advancedFilter--spacer"></div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">

                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > Min Sq Feet </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="number"  placeholder="Enter Minimum" name="sq_feet" value="<?=(floatval($_GET['sq_feet'])) ? floatval($_GET['sq_feet']) : ""; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > Year Built </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <select name="year_built" id="year_built">
                                                    <option value="" <?php if(!isset($_GET['year_built']) || empty($_GET['year_built'])):?>selected="selected"<?php endif; ?>>Year Built</option>
                                                    <?php
                                                    $thisYear = date('Y');
                                                    for($i=$thisYear; $i>($thisYear-50); $i--){ // $sqFt as $value => $label
                                                        $selected = "";
                                                        if(isset($_GET['year_built']) && intval($i) ==  intval($_GET['year_built'])) {
                                                            $selected = 'selected="selected"';
                                                        }?>
                                                        <option value="<?=$i; ?>" <?=$selected;?>><?=$i; ?>+ Year Built</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                                <div class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > MLS # </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <?php $listingId = (isset($_GET['listing_id']))? $_GET['listing_id'] : null; ?>
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="text"  placeholder="MLS #" name="listing_id" value="<?=$listingId?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > Neighborhood </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div  class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <?php $neighborhood = (isset($_GET['neighborhood']))? $_GET['neighborhood'] : null; ?>
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="text"  placeholder="Neighborhood" name="neighborhood" value="<?=$neighborhood?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > Schools </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div  class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <?php $school = (isset($_GET['school']))? $_GET['school'] : null; ?>
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="text"  placeholder="Schools" name="school" value="<?=$school?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > Street Name</div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div  class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <?php $streetName = (isset($_GET['street_name']))? $_GET['street_name'] : null; ?>
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="text"  placeholder="Street Name" name="street_name" value="<?=$streetName?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > City </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div  class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <?php $city = (isset($_GET['city']))? $_GET['city'] : null; ?>
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="text"  placeholder="City" name="city" value="<?=$city?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > Zip </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div  class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <?php $zip = (isset($_GET['zip']))? $_GET['zip'] : null; ?>
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="text"  placeholder="Zip" name="zip" value="<?=$zip?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                                <div   class="ng-scope">
                                    <div class="uc-searchBar-advancedFilterLabel ng-binding ng-scope" > County </div>
                                    <div class="uc-searchBar-advancedFilter c0-grid ng-scope" >
                                        <div class="uc-searchBar-lost-1_1">
                                            <div  class="uc-searchBar-advancedFilterInputsContainer ng-scope">
                                                <?php $county = (isset($_GET['county']))? $_GET['county'] : null; ?>
                                                <input class="uc-searchBar-advancedFilterInput ng-pristine ng-untouched ng-valid ng-scope ng-empty" type="text"  placeholder="County" name="county" value="<?=$county?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uc-searchBar-advancedFilter--spacer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$this->endWidget();

