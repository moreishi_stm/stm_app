<?php
$module = Yii::app()->controller->module->id;
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<style type="text/css">
    .row.no-gutters {
        margin-right: 0;
        margin-left: 0;

    & > [class^="col-"],
    & > [class*=" col-"] {
            padding-right: 0;
            padding-left: 0;
        }
    }
    body {
        overflow: hidden;
    }
    #map {
        min-height: 800px;
        height: 100%;
        height: calc(100vh - 116px - 80px);
        width: 100%;
    }
    #listings{
        min-width: 227px;
        margin: 10px 5px;
        overflow-y: auto;
        position: relative;
        height: 100%;
        height: calc(100vh - 116px - 80px);
    }
    #map-container {
        min-width: 200px;
    }
    .row.no-gutters {
        margin-right: 0;
        margin-left: 0;
    }
    .row.no-gutters > [class^="col-"],
    .row.no-gutters > [class*=" col-"] {
        padding-right: 0;
        padding-left: 0;
    }

    #map_draw, #map_remove, #map_cancel {
        position: absolute;
        cursor: pointer;
        z-index: 90;
    }

    .sp {
        background: url('//<?=(YII_DEBUG) ? "cdn.seizethemarket.local" : "cdn.seizethemarket.com"; ?>/assets/images/sprite_040315.png') no-repeat top left;
        display: inline-block;
    }

    .sp.m_draw {
        background-position: -12px -6037px;
        width: 143px;
        height: 38px;
    }

    .sp.m_remove {
        background-position: -12px -6079px;
        width: 143px;
        height: 38px;
    }

    .sp.m_cancel {
        background-position: -12px -6248px;
        width: 143px;
        height: 38px;
    }

    #draw_tooltip{
        position:fixed;
        top:209px;
        left:0;
        right:0;
        z-index:90;
        cursor:pointer
    }

    #draw_tooltip .draw_blurb{
        width:1000px;height:30px;margin:0 auto;text-align:center;background-color:#333333;background-color:rgba(0,0,0,0.80);color:#ffffff;
    }

    #draw_tooltip .draw_blurb h1{float:left;width:935px;font-size:14px;font-weight:normal;padding-top:7px}

    .home-list-photo {
        height: 157px;
    }

    .add-fav {
        right: 23px;
        top: 7px;
    }

    .flash {
        -moz-animation: flash 1s ease-out;
        -moz-animation-iteration-count: 1;

        -webkit-animation: flash 1s ease-out;
        -webkit-animation-iteration-count: 1;

        -ms-animation: flash 1s ease-out;
        -ms-animation-iteration-count: 1;
    }

    /**
     * Home List Column
     */
    @media (min-width: 768px) {
        .map-home-list {
            width: 317px;
        }
        #map-container {
            /*width: 100%;*/
            width: calc(100vw - 320px);
        }
    }
    @media (max-width: 768px) {
        /*#map-container {*/
            /*float: left;*/
        /*}*/
        /*.map-home-list {*/
            /*float: right;*/
        /*}*/
        /*.map-home-list, #listings {*/
            /*width: 320px;*/
        /*}*/
        /*#listings .product {*/
            /*width: 295px;*/
            /*margin-right: 0;*/
            /*margin-left: 0;*/
        /*}*/
    }

    @-webkit-keyframes flash {
        0% { background-color: none; }
        50% { background-color: #fbf8b2; }
        100% { background-color: none; }
    }

    @-moz-keyframes flash {
        0% { background-color: none; }
        50% { background-color: #fbf8b2; }
        100% { background-color: none; }
    }

    @-ms-keyframes flash {
        0% {
            background-color: none;
        }
        50% {
            background-color: #fbf8b2;
        }
        100% {
            background-color: none;
        }
    }

    .specs span{
        margin: 0;
        padding: 0;
        border: 0;
    }
    #mapview {
        background-color: rgb(243, 243, 243);
    }
    div.specs[class*="col-"] {
        padding-right: 0;
        padding-left: 0;
        margin-right: 0;
        margin-left: 0;
    }
    #listings .grid-description {
        background-color: white;
    }
    #listings .item {
        min-width: 290px;
    }

    #listings .item .address {
        /*min-width: 220px;*/
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        padding-top: 20px;
        min-height: 40px;
    }
    #listings .item .neighborhood {
        height: 60px;
        padding-top: 0;
    }
    #listings .product {
        margin-right: 4px;
        margin-bottom: 25px;
        padding: 0;
        margin-left: 4px;
    }
    #listings .item .specs-container .specs {
        padding-bottom: 6px;
        font-size: 13px;
    }
    #listings .product .description {
        max-width: 290px;
    }
    #listings .product .description div.quickview {
        margin-top: 30%;
    }
    #map-search-container {
        margin-top: -20px;
    }


    .ng-hide {
        display: none;
    }

    .uc-searchBar-panelCloseIcon-i {
        padding: 10px;
        cursor: pointer;
        display: block;
        text-align: right;
        padding-right: 20px;
    }
    .uc-searchBar-panelCloseIcon-i:hover {
        color: #990000;
    }

    /* CSS */

    .experimental--tooltip .alert--header, .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-neighborhood, .font-c0headlineN4 {
        font-family: Tiempos Headline, Times, Times New Roman, serif;
        font-weight: 400;
        font-style: normal
    }
    .font-c0headlineN9 {
        font-family: Tiempos Headline, Times, Times New Roman, serif;
        font-weight: 900;
        font-style: normal
    }
    .font-c0iconN4 {
        font-family: c0-Icons;
        font-weight: 400;
        font-style: normal
    }
    #listings-map .marker_class, #map .marker_class, #quick-listing .listing-alert, .font-c0monoN4 {
        font-family: GT Pressura Mono, Monaco, monospace;
        font-weight: 400;
        font-style: normal
    }
    .c0-card-subtitle, .font-c0sansN3, .uc-searchBar-panelHeader {
        font-family: Harmonia Sans, Helvetica, Arial, sans-serif;
        font-weight: 300;
        font-style: normal
    }
    #filter-mobile-toggle, #no-results-message h2, #no-search-results h2, #pagination_container .pagination li, #search_filter_container #search_filter .dropdown>a, #search_filter_container #search_filter ul.controls #filter-regions .consumerSearch-select2Container, #search_filter_container #search_filter ul.controls .checklist, #search_filter_container #search_filter ul.controls .checklist label, #search_filter_container #search_filter ul.controls>li input[type=text], #search_filter ul.controls #filter-regions .consumerSearch-select2Container .select2-choices .select2-input, #search_results_sort_by_wrapper, #simple_pagination_container .pagination a, #simple_pagination_container .search_results_stats, #ui-datepicker-div .ui-datepicker-next, #ui-datepicker-div .ui-datepicker-prev, .c0-card-title, .consumerSearch-select2Drop .select2-results, .experimental--tooltip, .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-card-body, .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-card-body li, .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-card-header, .font-c0sansN4, .pika-next, .pika-prev, .pika-single, .search1506, .search1506-hiddenListings, .search1506-lightText, .search1506-text, .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container .search_results_stats, .select2-input, .select2-no-results, .select2-results .select2-result-label, .uc-omnibox-list .select2-results .select2-result-label, .uc-omnibox-list .select2-results .select2-result-selectable .select2-result-label, .uc-omnibox-list .select2-results .select2-result-unselectable .select2-result-label, .uc-omnibox-list .select2-results .select2-result-with-children>.select2-result-label, .uc-omnibox-list .select2-results .select2-result-with-children>.select2-result-label:hover, .uc-searchBar-checkboxLabel, .uc-searchBar-container, .uc-searchBar-field--omnibox .select2-choices input.select2-input, .uc-searchBar-neighborhoodSelectorRegion {
        font-family: Harmonia Sans, Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-style: normal
    }
    #map .uc-tooltip-container, #no-results-message .clear-search-filters, #no-results-message h1, #no-search-results h1, #search_filter_container #search_filter UL.controls #filter_advanced em, #ui-datepicker-div .ui-datepicker-title, .experimental--tooltip .alert--preferencesHeader, .font-c0sansN6, .is-endrange .pika-button, .is-selected .pika-button, .is-startrange .pika-button, .is-today .pika-button, .pika-button:hover, .pika-label, .pika-table th, .uc-omnibox-list .select2-result-selectable .select2-match, .uc-searchBar-advancedFilterCheckbox:checked+.uc-searchBar-advancedFilterCheckboxLabelText, .uc-searchBar-advancedFilterLabel, .uc-searchBar-advancedFilterLabel--headline, .uc-searchBar-advancedPanelCheckboxLabel.is-checked, .uc-searchBar-neighborhoodSelectorRegion.is-active, .uc-searchBar-neighborhoodSelectorRegion.is-active:hover {
        font-family: Harmonia Sans, Helvetica, Arial, sans-serif;
        font-weight: 600;
        font-style: normal
    }
    .font-c0serifI4 {
        font-style: italic
    }
    .font-c0serifI4, .font-c0serifN4 {
        font-family: Tiempos Text, Times, Times New Roman, serif;
        font-weight: 400
    }
    .font-c0serifN4 {
        font-style: normal
    }
    .c0-card {
        margin-bottom: 30px;
        border: 1px solid #f3f3f3;
        width: 100%;
        min-width: 270px;
        background-color: #fff;
        width: 99.99%
    }
    .c0-card:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .c0-card:last-child {
        margin-right: 0
    }
    .c0-card:nth-child(1n) {
        margin-right: 0;
        float: right
    }
    .c0-card:nth-child(1n+1) {
        clear: left
    }
    .c0-card-content {
        padding: 20px 15px
    }
    .c0-card-title {
        margin: 0 0 5px;
        font-size: 16px;
        line-height: 18px;
        color: #000
    }
    .c0-card-subtitle {
        margin: 5px 0 15px;
        font-size: 12px;
        line-height: 15px;
        color: #474143;
        letter-spacing: .5px
    }
    .c0-card-complianceSBMLS {
        width: 54px;
        height: 18px;
        background-image: url(../../img/compliance/sbmls.png);
        background-size: contain
    }

    @media (min-width:690px) {
        .c0-card {
            width:calc(99.99% * 1/2 - 15px)
        }
        .c0-card:nth-child(1n) {
            float: left;
            margin-right: 30px;
            clear: none
        }
        .c0-card:last-child {
            margin-right: 0
        }
        .c0-card:nth-child(2n) {
            margin-right: 0;
            float: right
        }
        .c0-card:nth-child(2n+1) {
            clear: left
        }
    }

    @media (min-width:930px) {
        .c0-card {
            width:calc(99.99% * 1/3 - 20px)
        }
        .c0-card:nth-child(1n) {
            float: left;
            margin-right: 30px;
            clear: none
        }
        .c0-card:last-child {
            margin-right: 0
        }
        .c0-card:nth-child(3n) {
            margin-right: 0;
            float: right
        }
        .c0-card:nth-child(3n+1) {
            clear: left
        }
    }

    @media (min-width:1230px) {
        .c0-card {
            width:calc(99.99% * 1/4 - 22.5px)
        }
        .c0-card:nth-child(1n) {
            float: left;
            margin-right: 30px;
            clear: none
        }
        .c0-card:last-child {
            margin-right: 0
        }
        .c0-card:nth-child(4n) {
            margin-right: 0;
            float: right
        }
        .c0-card:nth-child(4n+1) {
            clear: left
        }
    }
    .pika-single {
        display: block;
        position: relative;
        border: 1px solid #f3f3f3;
        z-index: 1000;
        background: #fff;
        color: #474143
    }
    .pika-single:after, .pika-single:before {
        display: table;
        content: ' '
    }
    .pika-single:after {
        clear: both
    }
    .pika-single.is-hidden {
        display: none
    }
    .pika-single.is-bound, .ucDatePicker-popover {
        position: absolute;
        box-shadow: 0 5px 15px -5px #000
    }
    .pika-lendar {
        float: left;
        margin: 8px;
        width: 240px
    }
    .pika-title {
        position: relative;
        text-align: center
    }
    .pika-label {
        display: inline-block;
        position: relative;
        margin: 0;
        padding: 5px 3px;
        overflow: hidden;
        z-index: 1000;
        background-color: #fff;
        font-size: 14px;
        line-height: 20px
    }
    .pika-title select {
        position: absolute;
        top: 5px;
        left: 0;
        margin: 0;
        z-index: 1000;
        opacity: 0;
        cursor: pointer
    }
    .pika-next, .pika-prev {
        display: block;
        position: relative;
        border: 0;
        padding: 0;
        width: 20px;
        height: 20px;
        overflow: hidden;
        outline: none;
        background-color: transparent;
        text-indent: 20px;
        white-space: nowrap;
        cursor: pointer;
        font-size: 12px;
        color: #474143
    }
    .pika-next:after, .pika-prev:after {
        position: absolute;
        left: -13px;
        width: 100%
    }
    .pika-next:hover, .pika-prev:hover {
        color: #000
    }
    .is-rtl .pika-next, .pika-prev {
        float: left
    }
    .is-rtl .pika-next:after, .pika-prev:after {
        content: '\276E'
    }
    .is-rtl .pika-prev, .pika-next {
        float: right
    }
    .is-rtl .pika-prev:after, .pika-next:after {
        content: '\276F'
    }
    .pika-next.is-disabled, .pika-prev.is-disabled {
        opacity: .2;
        cursor: default
    }
    .pika-select {
        display: inline-block
    }
    .pika-table {
        border-spacing: 0;
        border-collapse: collapse;
        width: 100%
    }
    .pika-table td, .pika-table th {
        padding: 0;
        width: 14.28571%
    }
    .pika-table td {
        border: 1px solid #f3f3f3
    }
    .pika-table th {
        text-align: center;
        line-height: 25px
    }
    .pika-button, .pika-table th {
        color: #474143;
        font-size: 12px
    }
    .pika-button {
        display: block;
        margin: 0;
        border: 1px solid #fff;
        padding: 5px;
        width: 100%;
        box-sizing: border-box;
        outline: none;
        background: #fff;
        text-align: right;
        line-height: 15px;
        cursor: pointer
    }
    .pika-week {
        color: #474143;
        font-size: 11px
    }
    .is-today .pika-button {
        border-color: #474143;
        background-color: #fff;
        color: #000
    }
    .is-endrange .pika-button, .is-selected .pika-button, .is-startrange .pika-button {
        border-color: #ffd5ba;
        background: #ffebdf;
        color: #000
    }
    .is-inrange .pika-button {
        border-color: #f3f3f3;
        background: #f3f3f3;
        color: #000
    }
    .is-disabled .pika-button {
        color: #a3a3a3;
        cursor: default;
        pointer-events: none
    }
    .pika-button:hover {
        border-color: #d7d7d7;
        background: #d7d7d7
    }
    .pika-table abbr {
        border-bottom: none
    }
    .lost-1_12 {
        width:calc(99.99% * 1/12 - 27.5px)
    }
    .lost-1_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-1_12:last-child {
        margin-right: 0
    }
    .lost-1_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-1_12:nth-child(12n+1) {
        clear: left
    }
    .lost-2_12 {
        width:calc(99.99% * 2/12 - 25px)
    }
    .lost-2_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-2_12:last-child {
        margin-right: 0
    }
    .lost-2_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-2_12:nth-child(12n+1) {
        clear: left
    }
    .lost-3_12 {
        width:calc(99.99% * 3/12 - 22.5px)
    }
    .lost-3_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-3_12:last-child {
        margin-right: 0
    }
    .lost-3_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-3_12:nth-child(12n+1) {
        clear: left
    }
    .lost-4_12 {
        width:calc(99.99% * 4/12 - 20px)
    }
    .lost-4_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-4_12:last-child {
        margin-right: 0
    }
    .lost-4_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-4_12:nth-child(12n+1) {
        clear: left
    }
    .lost-5_12 {
        width:calc(99.99% * 5/12 - 17.5px)
    }
    .lost-5_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-5_12:last-child {
        margin-right: 0
    }
    .lost-5_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-5_12:nth-child(12n+1) {
        clear: left
    }
    .lost-6_12 {
        width:calc(99.99% * 6/12 - 15px)
    }
    .lost-6_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-6_12:last-child {
        margin-right: 0
    }
    .lost-6_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-6_12:nth-child(12n+1) {
        clear: left
    }
    .lost-7_12 {
        width:calc(99.99% * 7/12 - 12.5px)
    }
    .lost-7_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-7_12:last-child {
        margin-right: 0
    }
    .lost-7_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-7_12:nth-child(12n+1) {
        clear: left
    }
    .lost-8_12 {
        width:calc(99.99% * 8/12 - 10px)
    }
    .lost-8_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-8_12:last-child {
        margin-right: 0
    }
    .lost-8_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-8_12:nth-child(12n+1) {
        clear: left
    }
    .lost-9_12 {
        width:calc(99.99% * 9/12 - 7.5px)
    }
    .lost-9_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-9_12:last-child {
        margin-right: 0
    }
    .lost-9_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-9_12:nth-child(12n+1) {
        clear: left
    }
    .lost-10_12 {
        width:calc(99.99% * 10/12 - 5px)
    }
    .lost-10_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-10_12:last-child {
        margin-right: 0
    }
    .lost-10_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-10_12:nth-child(12n+1) {
        clear: left
    }
    .lost-11_12 {
        width:calc(99.99% * 11/12 - 2.5px)
    }
    .lost-11_12:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-11_12:last-child {
        margin-right: 0
    }
    .lost-11_12:nth-child(12n) {
        margin-right: 0;
        float: right
    }
    .lost-11_12:nth-child(12n+1) {
        clear: left
    }
    .lost-1_6 {
        width:calc(99.99% * 1/6 - 25px)
    }
    .lost-1_6:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-1_6:last-child {
        margin-right: 0
    }
    .lost-1_6:nth-child(6n) {
        margin-right: 0;
        float: right
    }
    .lost-1_6:nth-child(6n+1) {
        clear: left
    }
    .lost-2_6 {
        width:calc(99.99% * 2/6 - 20px)
    }
    .lost-2_6:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-2_6:last-child {
        margin-right: 0
    }
    .lost-2_6:nth-child(6n) {
        margin-right: 0;
        float: right
    }
    .lost-2_6:nth-child(6n+1) {
        clear: left
    }
    .lost-3_6 {
        width:calc(99.99% * 3/6 - 15px)
    }
    .lost-3_6:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-3_6:last-child {
        margin-right: 0
    }
    .lost-3_6:nth-child(6n) {
        margin-right: 0;
        float: right
    }
    .lost-3_6:nth-child(6n+1) {
        clear: left
    }
    .lost-4_6 {
        width:calc(99.99% * 4/6 - 10px)
    }
    .lost-4_6:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-4_6:last-child {
        margin-right: 0
    }
    .lost-4_6:nth-child(6n) {
        margin-right: 0;
        float: right
    }
    .lost-4_6:nth-child(6n+1) {
        clear: left
    }
    .lost-5_6 {
        width:calc(99.99% * 5/6 - 5px)
    }
    .lost-5_6:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-5_6:last-child {
        margin-right: 0
    }
    .lost-5_6:nth-child(6n) {
        margin-right: 0;
        float: right
    }
    .lost-5_6:nth-child(6n+1) {
        clear: left
    }
    .lost-1_4 {
        width:calc(99.99% * 1/4 - 22.5px)
    }
    .lost-1_4:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-1_4:last-child {
        margin-right: 0
    }
    .lost-1_4:nth-child(4n) {
        margin-right: 0;
        float: right
    }
    .lost-1_4:nth-child(4n+1) {
        clear: left
    }
    .lost-2_4 {
        width:calc(99.99% * 2/4 - 15px)
    }
    .lost-2_4:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-2_4:last-child {
        margin-right: 0
    }
    .lost-2_4:nth-child(4n) {
        margin-right: 0;
        float: right
    }
    .lost-2_4:nth-child(4n+1) {
        clear: left
    }
    .lost-3_4 {
        width:calc(99.99% * 3/4 - 7.5px)
    }
    .lost-3_4:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-3_4:last-child {
        margin-right: 0
    }
    .lost-3_4:nth-child(4n) {
        margin-right: 0;
        float: right
    }
    .lost-3_4:nth-child(4n+1) {
        clear: left
    }
    .lost-1_3 {
        width:calc(99.99% * 1/3 - 20px)
    }
    .lost-1_3:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-1_3:last-child {
        margin-right: 0
    }
    .lost-1_3:nth-child(3n) {
        margin-right: 0;
        float: right
    }
    .lost-1_3:nth-child(3n+1) {
        clear: left
    }
    .lost-2_3 {
        width:calc(99.99% * 2/3 - 10px)
    }
    .lost-2_3:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-2_3:last-child {
        margin-right: 0
    }
    .lost-2_3:nth-child(3n) {
        margin-right: 0;
        float: right
    }
    .lost-2_3:nth-child(3n+1) {
        clear: left
    }
    .lost-1_2 {
        width:calc(99.99% * 1/2 - 15px)
    }
    .lost-1_2:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-1_2:last-child {
        margin-right: 0
    }
    .lost-1_2:nth-child(2n) {
        margin-right: 0;
        float: right
    }
    .lost-1_2:nth-child(2n+1) {
        clear: left
    }
    .lost-1, .lost-1_1 {
        width: 99.99%
    }
    .lost-1:nth-child(1n), .lost-1_1:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .lost-1:last-child, .lost-1_1:last-child {
        margin-right: 0
    }
    .lost-1:nth-child(1n), .lost-1_1:nth-child(1n) {
        margin-right: 0;
        float: right
    }
    .lost-1:nth-child(1n+1), .lost-1_1:nth-child(1n+1) {
        clear: left
    }
    .c0-grid {
        box-sizing: border-box;
        *zoom:1;
        max-width: 100%;
        margin-left: auto;
        margin-right: auto;
        padding-left: 30px;
        padding-right: 30px
    }
    .c0-grid:after, .c0-grid:before {
        content: '';
        display: table
    }
    .c0-grid:after {
        clear: both
    }
    .c0-grid1230 {
        box-sizing: border-box;
        *zoom:1;
        max-width: 1230px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 30px;
        padding-right: 30px
    }
    .c0-grid1230:after, .c0-grid1230:before {
        content: '';
        display: table
    }
    .c0-grid1230:after {
        clear: both
    }
    .u-absoluteFill {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0
    }
    .u-alignCenter {
        text-align: center
    }
    .u-alignLeft {
        text-align: left
    }
    .u-alignRight {
        text-align: right
    }
    .u-clearfix:after {
        display: table;
        clear: both;
        content: ''
    }
    .u-displayBlock {
        display: block
    }
    .u-displayNone {
        display: none
    }
    .u-flexContainer--col {
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column
    }
    .u-flexContainer--col, .u-flexContainer--row {
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex
    }
    .u-flexContainer--row {
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row
    }
    .u-floatLeft {
        float: left
    }
    .u-floatRight {
        float: right
    }
    .u-inlineBlock {
        display: inline-block
    }
    .u-noScroll {
        overflow: hidden
    }
    .u-visibilityHidden {
        visibility: hidden!important
    }
    .u-noBorder {
        border: none!important
    }
    .u-noTopMargin {
        margin-top: 0!important
    }
    .u-offscreen {
        position: absolute!important;
        top: 0!important;
        left: 0!important;
        margin: 0!important;
        border: 0!important;
        padding: 0!important;
        width: 1px!important;
        height: 1px!important;
        clip: rect(0 0 0 0)!important;
        overflow: hidden!important;
        outline: 0!important
    }
    .u-positionRelative {
        position: relative
    }
    .u-unstyledButton {
        display: inline;
        border: none;
        background: transparent
    }
    .u-unstyledButton:focus {
        outline: none
    }
    .u-unstyledList {
        margin: 0;
        padding: 0;
        list-style-type: none
    }
    .u-uppercase {
        text-transform: uppercase
    }
    .uc-appWrapper {
        min-height: 100vh;
        flex-direction: column
    }
    .u-flexContainer, .uc-appWrapper {
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column
    }
    .u-flexContainer {
        flex-direction: column;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1
    }

    @media print {
        .u-print--displayBlock {
            display: block!important
        }
        .u-print--displayNone {
            display: none!important
        }
        .u-print--positionStatic {
            position: static!important
        }
    }
    .select2-container {
        display: inline-block;
        position: relative;
        margin: 0;
        vertical-align: middle;
        background: #fff
    }
    .select2-container, .select2-drop, .select2-search, .select2-search input {
        box-sizing: border-box
    }
    .select2-container .select2-choice {
        display: block;
        position: relative;
        border-radius: 4px;
        padding: 0 0 0 8px;
        height: 26px;
        overflow: hidden;
        color: #474143;
        white-space: nowrap;
        line-height: 26px;
        text-decoration: none;
        background-clip: padding-box;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: #fff
    }
    .select2-container.select2-drop-above .select2-choice {
        border-radius: 0 0 4px 4px
    }
    .select2-container .select2-choice>.select2-chosen {
        display: block;
        float: none;
        margin-right: 26px;
        width: auto;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis
    }
    .select2-container .select2-choice abbr {
        display: none
    }
    .select2-container.select2-allowclear .select2-choice abbr {
        display: inline-block;
        position: absolute;
        top: 1px;
        right: 0;
        border: 0;
        width: 14px;
        height: 14px;
        outline: 0;
        font-size: 1px;
        text-decoration: none;
        cursor: pointer
    }
    .select2-container .select2-choice abbr:after {
        font-size: 14px;
        content: '✕';
        color: #474143
    }
    .select2-drop-mask {
        position: fixed;
        top: 0;
        left: 0;
        margin: 0;
        border: 0;
        padding: 0;
        width: auto;
        min-width: 100%;
        height: auto;
        min-height: 100%;
        opacity: 0;
        z-index: 9998;
        background-color: #fff
    }
    .select2-drop {
        position: absolute;
        top: 100%;
        margin-top: -1px;
        border-radius: 0 0 4px 4px;
        width: 100%;
        z-index: 9999;
        background: #fff;
        color: #000;
        box-shadow: 0 4px 5px rgba(0,0,0,.15)
    }
    .select2-drop.select2-drop-above {
        margin-top: 1px;
        border-radius: 4px 4px 0 0
    }
    .select2-drop-auto-width {
        width: auto
    }
    .select2-container .select2-choice .select2-arrow {
        display: none
    }
    .select2-search {
        display: inline-block;
        position: relative;
        margin: 0;
        padding: 4px 4px 0;
        width: 100%;
        min-height: 26px;
        z-index: 10000;
        white-space: nowrap
    }
    .select2-search input {
        margin: 0;
        padding: 4px 20px 4px 5px;
        width: 100%;
        height: auto!important;
        min-height: 26px;
        outline: 0;
        font-size: 1em;
        box-shadow: none
    }
    .select2-dropdown-open .select2-choice {
        border-bottom-color: transparent;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0
    }
    .select2-dropdown-open .select2-choice .select2-arrow {
        border-left: none;
        background: transparent;
        -webkit-filter: none;
        filter: none
    }
    .select2-dropdown-open .select2-choice .select2-arrow b {
        background-position: -18px 1px
    }
    .select2-hidden-accessible {
        position: absolute;
        margin: -1px;
        border: 0;
        padding: 0;
        width: 1px;
        height: 1px;
        overflow: hidden;
        clip: rect(0 0 0 0)
    }
    .select2-results {
        position: relative;
        margin: 4px 4px 4px 0;
        padding: 0 0 0 4px;
        max-height: 200px;
        overflow-x: hidden;
        overflow-y: auto;
        -webkit-tap-highlight-color: rgb(0,0,0)
    }
    .select2-results ul.select2-result-sub {
        margin: 0;
        padding-left: 0
    }
    .select2-results li {
        display: list-item;
        list-style: none;
        background-image: none
    }
    .select2-results .select2-result-label {
        margin: 0;
        padding: 3px 7px 4px;
        min-height: 1em;
        cursor: pointer;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none
    }
    .select2-results-dept-1 .select2-result-label {
        padding-left: 20px
    }
    .select2-results-dept-2 .select2-result-label {
        padding-left: 40px
    }
    .select2-results-dept-3 .select2-result-label {
        padding-left: 60px
    }
    .select2-results-dept-4 .select2-result-label {
        padding-left: 80px
    }
    .select2-results-dept-5 .select2-result-label {
        padding-left: 100px
    }
    .select2-results-dept-6 .select2-result-label {
        padding-left: 110px
    }
    .select2-results-dept-7 .select2-result-label {
        padding-left: 120px
    }
    .select2-results .select2-highlighted {
        background: #f3f3f3
    }
    .select2-results .select2-highlighted ul {
        background: #fff
    }
    .select2-results .select2-ajax-error, .select2-results .select2-no-results, .select2-results .select2-searching, .select2-results .select2-selection-limit {
        display: list-item;
        padding-left: 5px;
        background: #fff
    }
    .select2-results .select2-selected {
        display: none
    }
    .select2-results .select2-ajax-error {
        background: rgba(255,50,50,.2)
    }
    .select2-more-results {
        display: list-item;
        background: #f3f3f3
    }
    .select2-container-multi .select2-choices {
        position: relative;
        margin: 0;
        padding: 0 5px 0 0;
        height: auto!important;
        cursor: text;
        overflow: hidden;
        background-color: #fff
    }
    .select2-locked {
        padding: 3px 5px!important
    }
    .select2-container-multi .select2-choices {
        min-height: 26px
    }
    .select2-container-multi .select2-choices li {
        float: left;
        list-style: none
    }
    .select2-container-multi .select2-choices .select2-search-field {
        margin: 0 0 10px;
        padding: 0;
        white-space: nowrap
    }
    .select2-container-multi .select2-choices .select2-search-field .select2-input {
        margin: 1px 0;
        border: 0;
        padding: 5px;
        font-size: 100%;
        outline: 0;
        box-shadow: none;
        background: transparent!important
    }
    .select2-focused {
        color: #474143
    }
    .select2-default {
        color: #a3a3a3
    }
    .select2-container-multi .select2-choices .select2-search-choice {
        position: relative;
        margin: 0 10px 0 0;
        border-radius: 0;
        padding: 5px 10px;
        background: #e7e8e7;
        line-height: 22px;
        cursor: default;
        background-clip: padding-box;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none
    }
    .select2-container-multi .select2-choices .select2-search-choice div {
        display: inline-block
    }
    .select2-container-multi .select2-choices .select2-search-choice .select2-chosen {
        cursor: default
    }
    .select2-container-multi .select2-choices .select2-search-choice-focus {
        background: #d7d7d7
    }
    .select2-container-multi .select2-search-choice-close {
        display: inline-block;
        position: static;
        left: inherit;
        height: 14px;
        line-height: 12px;
        vertical-align: middle;
        background: none
    }
    .select2-container-multi .select2-search-choice-close:after {
        font-size: 14px;
        content: '✕';
        color: #474143
    }
    .select2-offscreen, .select2-offscreen:focus {
        position: absolute!important;
        top: 0!important;
        left: 0!important;
        margin: 0!important;
        border: 0!important;
        padding: 0!important;
        width: 1px!important;
        height: 1px!important;
        outline: 0!important;
        overflow: hidden!important;
        clip: rect(0 0 0 0)!important
    }
    .select2-display-none {
        display: none
    }
    .select2-measure-scrollbar {
        position: absolute;
        top: -10000px;
        left: -10000px;
        width: 100px;
        height: 100px;
        overflow: scroll
    }
    .uc-omnibox.select2-container-multi .select2-choices {
        margin: 6px 0 5px
    }
    .uc-omnibox.select2-container-multi .select2-choices .select2-search-choice {
        margin: 0 10px 10px 0;
        border-radius: 0;
        padding: 5px 10px;
        background: #e7e8e7;
        line-height: 22px
    }
    .uc-omnibox.select2-container-multi .select2-choices .select2-search-choice div {
        display: inline-block
    }
    .uc-omnibox.select2-container-multi .select2-choices .select2-search-choice-focus {
        background: #ffebdf
    }
    .uc-omnibox-list {
        width: 100%
    }
    .uc-omnibox-list .select2-results .select2-no-results, .uc-omnibox-list .select2-results .select2-searching, .uc-omnibox-list .select2-results .select2-selection-limit {
        background: #fff;
        color: #a3a3a3
    }
    .uc-omnibox-list .select2-results .select2-result-label, .uc-omnibox-list .select2-results .select2-result-selectable .select2-result-label, .uc-omnibox-list .select2-results .select2-result-unselectable .select2-result-label {
        border: none;
        color: #474143
    }
    .uc-omnibox-list .select2-results .select2-result-selectable.select2-highlighted {
        color: #474143;
        background: #ffebdf
    }
    .uc-omnibox-list .select2-results .select2-result-with-children>.select2-result-label, .uc-omnibox-list .select2-results .select2-result-with-children>.select2-result-label:hover {
        color: #a3a3a3
    }
    .uc-omnibox-list .select2-result-selectable .select2-match, .uc-omnibox-list .select2-result-unselectable .select2-match {
        text-decoration: none
    }
    .uc-searchBar-container {
        position: relative;
        z-index: 1;
        /*box-shadow: 0 0 10px rgba(0,0,0,.15)*/
    }
    .uc-searchBar-container select::-ms-expand {
        display:none
    }
    .uc-searchBar-container .c0-checkbox {
        margin-top: -2px;
        vertical-align: middle
    }
    .uc-searchBar-panel {
        box-shadow: 0 5px 5px rgba(0,0,0,.2)
    }
    .uc-searchBar-mapPanelContainer {
        background-color: #fff;
        text-align: left;
        white-space: normal
    }
    .uc-searchBar {
        display: table;
        border-bottom: 1px solid #f3f3f3;
        width: 100%;
        min-height: 60px;
        background-color: #fff
    }
    .uc-searchBar-field {
        display: table-cell;
        border-left: 1px solid #e7e8e7;
        white-space: nowrap;
        text-align: center
    }
    .uc-searchBar-field:focus {
        outline: 0
    }
    .uc-searchBar-field.is-active {
        background-color: #f3f3f3
    }
    .uc-searchBar-field--iconContainer {
        border-left: none;
        padding: 10px 20px 9px;
        width: 1px
    }
    .uc-searchBar-field--searchIcon {
        display: none;
        position: relative;
        top: 5px;
        padding: 14px 10px 12px;
        width: 1px
    }
    .uc-searchBar--magnifyingGlass {
        margin-left: 7px;
        border-left: none;
        width: 0;
        height: 22px;
        fill: #a3a3a3
    }
    .uc-searchBar-caret {
        position: absolute;
        top: -17px;
        left: 0;
        margin-left: 15px;
        width: 20px;
        height: 20px;
        fill: #a3a3a3
    }
    .uc-searchBar-field--omnibox {
        position: relative;
        border-left: none;
        padding: 0;
        max-width: 150px;
        overflow: hidden
    }
    .uc-searchBar-field--omnibox .uc-omnibox {
        display: inline-block;
        position: relative;
        width: 100%
    }
    .uc-searchBar-field--omnibox input.select2-input {
        max-width: 350px
    }
    .uc-searchBar-field--omnibox .select2-choices input.select2-input {
        padding-top: 3px
    }
    .uc-searchBar-field--neighborhoodSelector, .uc-searchBar-field--shortTermRentalPeriodSelector {
        width: 105px
    }
    .uc-searchBar-field--priceRange {
        width: 150px
    }
    .uc-searchBar-field--bedrooms {
        width: 105px
    }
    .uc-searchBar-field--bathrooms {
        width: 115px
    }
    .uc-searchBar-fieldTitle {
        padding: 20px 20px 17px
    }
    .uc-searchBar-fieldTitle:focus {
        outline: 0
    }
    .uc-searchBar-fieldTitle:hover {
        color: #000
    }
    .search1506 .uc-searchBar-fieldTitle {
        padding: 36px 20px 23px
    }
    .uc-searchBar-fieldTitle--advanced, .uc-searchBar-fieldTitle--bathrooms, .uc-searchBar-fieldTitle--bedrooms, .uc-searchBar-fieldTitle--neighborhoodSelector, .uc-searchBar-fieldTitle--priceRange, .uc-searchBar-fieldTitle--shortTermRentalPeriodSelector {
        color: #474143;
        cursor: pointer
    }
    .uc-searchBar-field--advanced {
        width: 80px;
        cursor: pointer
    }
    .uc-searchBar-panelContainer {
        position: relative;
        text-align: left;
        cursor: default;
        color: #000
    }

    @media (min-width:1230px) {
        .uc-searchBar-panelContainer {
            height: 0
        }
    }
    .uc-searchBar-panelContainer--shortTermRentalPeriodPicker {
        margin-bottom: 15px
    }
    .uc-searchBar-panelContainer input[type=date], .uc-searchBar-panelContainer input[type=number], .uc-searchBar-panelContainer input[type=text], .uc-searchBar-panelContainer select {
        border: 1px solid #e7e8e7;
        padding: 9px;
        height: 35px;
        line-height: 15px
    }
    .uc-searchBar-checkboxLabel {
        display: inline;
        font-size: 13px;
        color: #474143
    }
    .uc-searchBar-panel--advanced .uc-searchBar-checkboxLabel.uc-searchBar-checkboxLabel-unknown {
        font-size: 12px
    }
    .uc-searchBar-panel--neighborhoodSelector {
        position: absolute;
        right: 0;
        left: 0;
        background-color: #fff;
        z-index: 1000
    }
    .uc-searchBar-neighborhoodSelectorAccordion {
        max-height: 668px;
        overflow-y: scroll;
        overflow-x: visible
    }
    .uc-searchBar-neighborhoodSelectorAccordion:last-child {
        border-bottom: none
    }
    .uc-searchBar-neighborhoodSelectorList {
        margin: 0;
        border-bottom: 1px solid #e7e8e7;
        padding: 30px 0 30px 30px
    }
    .uc-searchBar-neighborhoodSelectorRegion {
        margin: 0;
        border-bottom: 1px solid #e7e8e7;
        padding: 24px 20px;
        height: 68px;
        color: #000;
        font-size: 16px;
        line-height: normal;
        cursor: pointer;
        text-transform: uppercase
    }
    .uc-searchBar-neighborhoodSelectorRegion:hover {
        border-bottom: 1px solid #f3f3f3;
        background-color: #f3f3f3
    }
    .uc-searchBar-neighborhoodSelectorRegion>.uc-searchBar-caret {
        float: right
    }
    .uc-searchBar-neighborhoodSelectorRegion.is-active, .uc-searchBar-neighborhoodSelectorRegion.is-active:hover {
        border-bottom: 1px solid #ffd5ba;
        background-color: #ffebdf
    }
    .uc-searchBar-neighborhoodSelectorItem {
        margin: 0;
        padding: 5px 0;
        min-width: 150px;
        height: 25px;
        line-height: 15px;
        list-style-type: none
    }
    .uc-searchBar-neighborhoodSelectorItem:hover .uc-searchBar-neighborhoodSelectorItem--deleteIcon {
        display: inline
    }
    .uc-searchBar-neighborhoodSelectorItem .uc-searchBar-neighborhoodSelectorItem--deleteIcon {
        display: none;
        margin-left: 7px;
        width: 12px;
        height: 12px;
        fill: #000;
        cursor: pointer
    }
    .uc-searchBar-panel--neighborhoodSelector .angular-leaflet-map {
        width: 100%;
        height: 500px
    }
    .uc-searchBar-panel--shortTermRentalPeriodPicker {
        width: 286px
    }
    .uc-searchBar-panel--bathroomsPicker, .uc-searchBar-panel--bedroomsPicker, .uc-searchBar-panel--priceRangePicker, .uc-searchBar-panel--shortTermRentalPeriodPicker {
        position: absolute;
        right: 0;
        padding: 20px;
        color: #000;
        background-color: #f3f3f3
    }
    .uc-searchBar-panel--priceRangePicker input[type=text] {
        margin-bottom: 10px;
        border: 1px solid #a3a3a3
    }
    .uc-searchBar-shortTermRentalPeriodPicker {
        margin-bottom: 3px
    }
    .uc-searchBar-shortTermRentalPeriodPicker-input {
        margin-right: 3px
    }
    .uc-searchBar-bathroomsPicker, .uc-searchBar-bedroomsPicker {
        margin-bottom: 10px
    }
    .uc-searchBar-bathroomsPicker-label:not(:last-child) {
        margin-right: 20px
    }
    .uc-searchBar-panel--advanced {
        position: absolute;
        top: -59px;
        right: 0;
        left: inherit;
        width: 100vw;
        height: calc(100vh - 113px);
        background-color: #f3f3f3;
        overflow-y: auto;
        white-space: normal
    }

    @media (min-width:690px) {
        .uc-searchBar-panel--advanced {
            top: 0;
            width: 675px;
            height: calc(100vh - 169px)
        }
    }
    .valuation .uc-searchBar-panel--advanced {
        height: calc(100vh - 173px)
    }

    @media (min-width:690px) {
        .valuation .uc-searchBar-panel--advanced {
            top: 0;
            height:calc(100vh - (60px + 59px + 60px + 50px))
        }
    }
    .search1506 .uc-searchBar-panel--advanced {
        height: 100vh
    }

    @media (min-width:690px) {
        .search1506 .uc-searchBar-panel--advanced {
            top: 0;
            height: calc(100vh - 119px)
        }
    }

    @media (min-width:770px) {
        .search1506 .uc-searchBar-panel--advanced {
            height: calc(100vh - 129px)
        }
    }

    @media (min-width:1170px) {
        .search1506 .uc-searchBar-panel--advanced {
            height: calc(100vh - 139px)
        }
    }
    .search1506 #setAlert-popup {
        max-height: 100vh;
        overflow-y: scroll
    }

    @media (min-width:1170px) {
        .search1506 #setAlert-popup {
            max-height:calc(100vh - (59px + 55px + 80px))
        }
    }
    .uc-searchBar-panelCloseIcon {
        position: absolute;
        top: 15px;
        right: 30px;
        width: 24px;
        height: 24px;
        fill: #000;
        cursor: pointer
    }

    @media (min-width:690px) {
        .uc-searchBar-panelCloseIcon {
            display: none
        }
    }
    .uc-searchBar-advancedFilter {
        margin-left: 30px;
        padding: 5px 20px 0 0
    }
    .uc-searchBar-advancedFilter--compact {
        padding-top: 0
    }
    .uc-searchBar-advancedFilter--spacer {
        margin-bottom: 20px
    }
    .uc-searchBar-advancedFilterHelp {
        margin-top: 5px;
        font-size: 13px
    }
    .uc-searchBar-advancedFilterLabel {
        margin-top: 20px;
        padding: 0 0 5px 30px;
        font-size: 14px;
        line-height: 20px;
        color: #000
    }
    .uc-searchBar-advancedFilterLabel--headline {
        padding: 20px 0 20px 35px;
        width: 100%;
        font-size: 22px;
        line-height: 20px;
        background-color: #e7e8e7;
        color: #000
    }
    .uc-searchBar-advancedFilterInputsContainer {
        display: inline-block;
        overflow: hidden;
        vertical-align: middle
    }
    .uc-searchBar-advancedFilterInput {
        margin-bottom: 10px;
        border: 1px solid #e7e8e7;
        padding: 7px;
        height: 35px;
        line-height: 16px;
        font-size: 11px
    }
    .uc-searchBar-advancedFilterRadio {
        margin-right: 5px;
        width: 17px;
        height: 17px;
        line-height: 16px
    }
    .uc-searchBar-advancedFilterCheckbox:checked+.uc-searchBar-advancedFilterCheckboxLabelText {
        color: #000
    }
    .uc-searchBar-advancedFilterCheckbox {
        margin-right: 5px
    }
    .uc-searchBar-advancedPanelCheckboxContainer {
        margin-bottom: 5px
    }
    .uc-searchBar-advancedPanelCheckbox {
        display: none
    }
    .uc-searchBar-advancedPanelCheckboxLabel {
        padding: 10px;
        min-width: 125px;
        background-color: #fff;
        font-size: 13px;
        line-height: 15px;
        text-align: center;
        white-space: nowrap;
        cursor: pointer
    }
    .uc-searchBar-advancedPanelCheckboxLabel.is-checked {
        background-color: #a3a3a3;
        color: #474143
    }
    .uc-searchBar-advancedSelectMenu {
        min-width: 250px
    }
    .uc-searchBar-panelHeader {
        display: block;
        margin: 0;
        padding: 20px 40px;
        font-size: 36px;
        letter-spacing: .03em
    }
    .uc-searchBar-inputDivider {
        padding: 0 5px;
        vertical-align: middle
    }
    .uc-searchBar-advancedPanelContainer {
        position: relative;
        text-align: left;
        cursor: default
    }
    .uc-searchBar-field--large {
        display: none
    }

    @media (min-width:1230px) {
        .uc-searchBar-field--small {
            display: none
        }
        .uc-searchBar-field--large {
            display: table-cell
        }
    }
    .uc-searchBar-filterIcon {
        position: absolute;
        top: -20px;
        left: 0;
        width: 30px;
        height: 30px;
        fill: #a3a3a3;
        vertical-align: middle
    }
    .uc-searchBar-filterIconContainer {
        display: inline-block;
        position: relative;
        width: 30px
    }
    .uc-searchBar-fieldIconContainer {
        display: inline-block;
        position: relative;
        width: 35px
    }
    .uc-searchBar-fieldIconContainer--neighborhoodSelector {
        float: right;
        top: 10px
    }
    .uc-searchBar-fieldIconContainer--advanced {
        display: none
    }
    .uc-searchBar-field--small .uc-searchBar-bathroomsPicker, .uc-searchBar-field--small .uc-searchBar-bedroomsPicker, .uc-searchBar-field--small .uc-searchBar-pricePicker {
        display: inline-block
    }
    .uc-searchBar-panel--advanced .uc-searchBar-checkboxLabel {
        display: block;
        margin-left: 5px;
        padding: 5px 0 0 20px;
        text-indent: -25px;
        font-size: 14px
    }
    .uc-searchBar-lostColumn {
        width: 99.99%
    }
    .uc-searchBar-lostColumn:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .uc-searchBar-lostColumn:last-child {
        margin-right: 0
    }
    .uc-searchBar-lostColumn:nth-child(1n) {
        margin-right: 0;
        float: right
    }
    .uc-searchBar-lostColumn:nth-child(1n+1) {
        clear: left
    }

    @media (min-width:390px) {
        .uc-searchBar-field.uc-searchBar-field--searchIcon {
            display: table-cell
        }
        .uc-searchBar-fieldTitle--advanced:before {
            padding-right: 5px;
            content: 'Filters'
        }
        .search1506 .uc-searchBar-fieldTitle {
            padding: 22px 20px 19px
        }
        .uc-searchBar-lostColumn {
            width:calc(99.99% * 1/2 - 15px)
        }
        .uc-searchBar-lostColumn:nth-child(1n) {
            float: left;
            margin-right: 30px;
            clear: none
        }
        .uc-searchBar-lostColumn:last-child {
            margin-right: 0
        }
        .uc-searchBar-lostColumn:nth-child(2n) {
            margin-right: 0;
            float: right
        }
        .uc-searchBar-lostColumn:nth-child(2n+1) {
            clear: left
        }
    }

    @media (min-width:930px) {
        .uc-searchBar-lostColumn {
            width:calc(99.99% * 1/4 - 22.5px)
        }
        .uc-searchBar-lostColumn:nth-child(1n) {
            float: left;
            margin-right: 30px;
            clear: none
        }
        .uc-searchBar-lostColumn:last-child {
            margin-right: 0
        }
        .uc-searchBar-lostColumn:nth-child(4n) {
            margin-right: 0;
            float: right
        }
        .uc-searchBar-lostColumn:nth-child(4n+1) {
            clear: left
        }
    }

    /*@media (min-width:1230px) {*/
    @media (min-width:768px) {
        .uc-searchBar-field--large {
            display: table-cell
        }
        .uc-searchBar-fieldTitle--advanced:before {
            padding-right: 0;
            content: 'More Filters'
        }
        .uc-searchBar-filterIconContainer {
            display: none
        }
        .uc-searchBar-fieldIconContainer--advanced {
            display: inline-block
        }
    }

    @media only screen and (max-height:810px) {
        .uc-searchBar-panelHeader {
            padding: 12px 25px;
            line-height: 26px;
            font-size: 18px
        }
        .uc-searchBar-neighborhoodSelectorAccordion {
            max-height: 468px
        }
        .uc-searchBar-panel--neighborhoodSelector .angular-leaflet-map {
            height: 350px
        }
        .uc-searchBar-panel--neighborhoodSelector .uc-searchBar-checkboxLabel {
            padding-left: 10px;
            font-size: 14px;
            font-family: Harmonia Sans, Helvetica, Arial, sans-serif;
            font-weight: 400;
            font-style: normal
        }
        .uc-searchBar-panel--advanced .uc-searchBar-checkboxLabel {
            font-size: 13px
        }
        .uc-searchBar-advancedFilter {
            padding-bottom: 5px
        }
        .uc-searchBar-advancedFilterHelp {
            font-size: 10px
        }
        .uc-searchBar-advancedFilterLabel {
            line-height: 15px;
            font-size: 13px
        }
        .uc-searchBar-advancedPanelCheckboxLabel {
            min-width: 110px;
            line-height: 10px;
            font-size: 13px
        }
    }
    .uc-searchBar-lost-1_1 {
        width: 99.99%
    }
    .uc-searchBar-lost-1_1:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .uc-searchBar-lost-1_1:last-child {
        margin-right: 0
    }
    .uc-searchBar-lost-1_1:nth-child(1n) {
        margin-right: 0;
        float: right
    }
    .uc-searchBar-lost-1_1:nth-child(1n+1) {
        clear: left
    }

    @media (min-width:690px) {
        .uc-searchBar-field--omnibox {
            /*max-width: 100%*/
        }
        .uc-searchBar-panel--advanced {
            width: 675px
        }
    }

    @media (min-width:930px) {
        .uc-searchBar-panel--advanced {
            width: 675px
        }
    }
    #no-results-message h2, #no-search-results h2, #search_filter_container #search_filter .dropdown>a, #search_filter_container #search_filter ul.controls #filter-regions .consumerSearch-select2Container, #search_filter_container #search_filter ul.controls .checklist, #search_filter_container #search_filter ul.controls .checklist label, #search_filter_container #search_filter ul.controls>li input[type=text], #search_results_sort_by_wrapper, #simple_pagination_container .search_results_stats, .consumerSearch-select2Drop .select2-results, .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-card-body, .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-card-header, .search1506-text, .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container .search_results_stats {
        color: #000;
        font-size: 16px
    }
    #pagination_container .pagination li, #search_filter ul.controls #filter-regions .consumerSearch-select2Container .select2-choices .select2-input, #simple_pagination_container .pagination a, .experimental--tooltip, .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-card-body li, .search1506-lightText {
        color: #848484;
        font-size: 16px
    }
    #filter-mobile-toggle, #no-results-message .clear-search-filters, #search_filter .dropdown .popup, #search_filter_container #search_filter .dropdown>a, #search_filter_container #search_filter ul.controls #filter-regions .consumerSearch-select2Container, #search_filter_container #search_filter ul.controls>li input[type=text], .search1506-filterButton, .searchUiBuilderActive.secondarySearchFilter #view_toggle.toggle li, .toggle li {
        background: #fff;
        border: 1px solid #e8e8e8;
        color: #000
    }
    .search1506 {
        margin: 0;
        background: #fff;
        color: #848484;
        font-smoothing: antialiased;
        text-rendering: optimizeLegibility
    }
    .search1506 .consumerSearch-select2Container.select2-container-multi .select2-choices .select2-search-field input {
        padding: 5px;
        margin: 1px 0;
        font-family: sans-serif;
        font-size: 100%;
        color: #666;
        outline: 0;
        border: 0;
        box-shadow: none;
        background: transparent!important
    }
    .search1506 .consumerSearch-select2Container .select2-default {
        color: #a3a3a3
    }
    .search1506 .consumerSearch-select2Container.select2-container-multi .select2-choices .select2-search-field {
        margin: 0;
        padding: 0;
        vertical-align: middle;
        white-space: nowrap
    }
    .search1506 #search_filter_container #search_filter ul.controls #filter-regions .consumerSearch-select2Container {
        display: block;
        padding: 0 40px 0 0
    }
    .search1506 #search_filter_container #search_filter ul.controls #filter-regions .consumerSearch-select2Container:before {
        position: absolute;
        padding: 4px 17px 3px;
        z-index: 100;
        top: 6px;
        right: 0;
        background: #fff;
        border-left: 1px solid #e8e8e8;
        font-size: 28px;
        line-height: 1
    }
    .search1506 #search_filter ul.controls #filter-regions .consumerSearch-select2Container .select2-choices .select2-input {
        border: none;
        padding: 13px 17px
    }
    .search1506 #search_filter ul.controls #filter-regions .consumerSearch-select2Container .select2-choices .select2-input:focus {
        border: none!important;
        outline: 0
    }
    .search1506 #search_filter_container #search_filter ul.controls>li input[type=text] {
        display: block;
        width: 200px;
        padding: 11px;
        opacity: 1;
        border: 1px solid #e8e8e8;
        border-radius: 0;
        color: #000;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        transition: border .2s linear
    }
    .search1506 #search_filter_container #search_filter ul.controls>li input[type=text]:focus {
        border-color: #000
    }
    .search1506 #search_filter .consumerSearch-select2Container.select2-container-multi .select2-choices {
        margin: 0;
        padding: 0 5px 0 0;
        position: relative;
        cursor: text;
        overflow-x: auto;
        overflow-y: hidden;
        -webkit-overflow-scrolling: touch;
        background-color: #fff;
        min-height: 26px;
        white-space: nowrap;
        width: 100%
    }
    .search1506 #search_filter .consumerSearch-select2Container.select2-container-multi .select2-choices .select2-search-choice {
        display: inline-block;
        position: relative;
        padding: 5px 27px 5px 5px;
        margin: 12px 5px 12px 0;
        list-style: none;
        font-weight: 400
    }
    .search1506 #search_filter .consumerSearch-select2Container .select2-search-choice-close {
        display: block;
        width: 14px;
        height: 14px;
        position: absolute;
        right: 3px;
        top: 9px;
        font-size: 1px;
        outline: none;
        background: #000;
        border-radius: 50%;
        padding: 2px
    }
    .search1506 #search_filter .consumerSearch-select2Container .select2-search-choice-close:after {
        content: '\00d7';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        color: #fff;
        display: block;
        font-size: 12px;
        line-height: 12px;
        text-align: center
    }
    .search1506 .consumerSearch-select2Drop .select2-results .select2-ajax-error, .search1506 .consumerSearch-select2Drop .select2-results .select2-no-results, .search1506 .consumerSearch-select2Drop .select2-results .select2-searching, .search1506 .consumerSearch-select2Drop .select2-results .select2-selection-limit {
        display: list-item;
        padding-left: 5px
    }
    .search1506 .consumerSearch-select2Drop .select2-results {
        background: #fff;
        max-height: 300px;
        padding: 0;
        margin: 0;
        position: relative;
        overflow-x: hidden;
        overflow-y: auto;
        -webkit-tap-highlight-color: rgb(0,0,0)
    }
    .search1506 .consumerSearch-select2Drop .select2-results li {
        list-style: none;
        display: list-item;
        background-image: none;
        line-height: 28px;
        cursor: pointer;
        width: 100%
    }
    .search1506 .consumerSearch-select2Drop .select2-results .select2-result-label {
        border-top: 1px solid #e8e8e8;
        white-space: nowrap;
        padding-right: 40px
    }
    .search1506 .consumerSearch-select2Drop .select2-results .select2-highlighted>.select2-result-label, .search1506 .consumerSearch-select2Drop .select2-results .select2-result-label:hover {
        color: #000;
        background: #fff
    }
    .search1506 .consumerSearch-select2Drop>.select2-results>li:first-child>.select2-result-label {
        border-top: none
    }
    .search1506 .consumerSearch-select2Drop .select2-result .select2-result-label {
        padding-left: 20px
    }
    .search1506 .consumerSearch-select2Drop .select2-result-sub .select2-result-label {
        margin-left: -20px;
        padding-left: 40px
    }
    .search1506 .consumerSearch-select2Drop .select2-result-sub {
        padding-left: 20px;
        color: #848484
    }

    @media (max-width:774px) {
        .search1506 .consumerSearch-select2Drop.shortTerm {
            right: 25px;
            width: auto!important
        }
    }
    .search1506 #search_filter_secondary_results {
        display: block;
        margin-top: -45px
    }
    .search1506 .allocation .allocation__agents__list__cancel, .search1506 .contact-agent .allocation__agents__list__cancel {
        margin-bottom: 0
    }
    .search1506 .fb-login {
        transition: background-color .25s ease-in-out;
        -webkit-border-radius: 4px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 4px;
        -moz-background-clip: padding;
        border-radius: 4px;
        background-clip: padding-box;
        border-radius: 4px 4px 4px 4px;
        display: block;
        border: none;
        color: #fff;
        text-transform: uppercase;
        background-color: #3b5997;
        margin-top: 5px;
        padding: 10px 0 7px;
        text-align: center;
        font-family: proxima-nova, sans-serif;
        font-size: 1em;
        font-weight: 900;
        cursor: pointer
    }
    .search1506 [dir=rtl] .slick-prev:before {
        content: "\2192"
    }
    .search1506 a:focus {
        outline: thin dotted
    }
    .search1506 .button-edge.button--primary {
        transition: background-color .2s linear;
        background-color: #227dda
    }
    .search1506 #check-status {
        border-top: 1px solid #ebebeb;
        padding-top: 15px
    }
    .search1506 #curved-arrow {
        left: 50px;
        position: absolute;
        top: 23px
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_advanced #price-to {
        position: relative;
        top: 7px
    }
    .search1506 img {
        border: 0;
        vertical-align: middle
    }
    .search1506 #search_filter_container:after, .search1506 #search_filter_container:before {
        content: " ";
        display: table
    }
    .search1506 .allocation table img, .search1506 .contact-agent table img {
        max-width: 100%
    }
    .search1506 .modal a {
        transition: color .2s linear
    }
    .search1506 #search .uc-footer {
        display: none
    }
    .search1506 input[type=email]:-moz-placeholder, .search1506 input[type=password]:-moz-placeholder, .search1506 input[type=text]:-moz-placeholder {
        color:#878f98
    }
    .search1506 .features-table {
        width: 100%
    }
    .search1506 .modal .modal_close {
        position: absolute;
        right: -13px;
        top: -13px;
        display: inline;
        padding: 8px 9px;
        width: 34px;
        height: 34px;
        font-size: 13px;
        background: #fff;
        color: #6b6b6b;
        cursor: pointer;
        z-index: 1;
        border: 1px solid #dcdde0;
        -webkit-border-radius: 20px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 20px;
        -moz-background-clip: padding;
        border-radius: 20px;
        background-clip: padding-box;
        border-radius: 20px 20px 20px 20px;
        background-clip: border-box;
        transition: color .2s linear
    }
    .search1506 fieldset {
        border: 1px solid silver;
        margin: 0 2px;
        padding: .35em .625em .75em
    }
    .search1506 a:active, .search1506 a:hover {
        outline: 0
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_price .price_selection {
        white-space: nowrap
    }
    .search1506 .login-separator {
        text-align: center;
        font-weight: 600;
        font-size: 15px;
        margin: 15px 0
    }
    .search1506 #search #filter_advanced .activate .prefix, .search1506 #search #filter_bedrooms .activate .prefix, .search1506 #search #filter_price .activate .prefix {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        max-width: 95px
    }
    .search1506 .allocation table p, .search1506 .contact-agent table p {
        margin: 8px 12px!important
    }
    .search1506 .bold {
        font-weight: 700
    }
    .search1506 .uc-footer .uc-footer-link {
        color: #73797e
    }
    .search1506 input[type=email]:-ms-input-placeholder, .search1506 input[type=password]:-ms-input-placeholder, .search1506 input[type=text]:-ms-input-placeholder {
        color:#878f98
    }
    .search1506 .uc-footer .uc-footer-navMenuListItem:last-child {
        margin-right: 0
    }
    .search1506 .section {
        border-top-left-radius: 4px;
        -webkit-border-top-right-radius: 4px;
        -webkit-background-clip: padding-box;
        -moz-border-radius-topright: 4px;
        -moz-background-clip: padding;
        border-top-right-radius: 4px;
        background-clip: padding-box;
        width: 313px
    }
    .search1506 .uc-footer .uc-footer-link, .search1506 .uc-footer .uc-footer-socialLink {
        transition: opacity .2s linear
    }
    .search1506 .modal .modal_content form input[type=submit]:hover {
        background-color: #1e6fc1
    }
    .search1506 #map-introduction .button, .search1506 #welcome-to-urban-compass .button {
        -webkit-border-radius: 4px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 4px;
        -moz-background-clip: padding;
        border-radius: 4px;
        background-clip: padding-box;
        border-radius: 4px 4px 4px 4px;
        transition: background-color .2s linear;
        background-color: #227dda;
        color: #fff;
        cursor: pointer;
        display: block;
        font-size: 15px;
        font-weight: 700;
        margin-top: 12px;
        padding: 10px 0;
        text-transform: uppercase
    }
    .search1506 button::-moz-focus-inner, .search1506 input::-moz-focus-inner {
        border:0;
        padding:0
    }
    .search1506 .input-2 .input:last-child {
        margin-right: 0
    }
    .search1506 .map-container {
        position: relative
    }
    .search1506 .modal .modal_content form fieldset input[type=email]:focus, .search1506 .modal .modal_content form fieldset input[type=password]:focus, .search1506 .modal .modal_content form fieldset input[type=text]:focus {
        border: 1px solid #227dda;
        outline: none
    }
    .search1506 .uc-firstTimeGeographySelector {
        text-align: center
    }
    .search1506 .allocation p, .search1506 .contact-agent p {
        font-size: 15px
    }
    .search1506 .modal .modal_content form fieldset input[type=email]:-moz-placeholder, .search1506 .modal .modal_content form fieldset input[type=email]::-moz-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=email]::-ms-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=email]::-webkit-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=password]:-moz-placeholder, .search1506 .modal .modal_content form fieldset input[type=password]::-moz-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=password]::-ms-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=password]::-webkit-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=text]:-moz-placeholder, .search1506 .modal .modal_content form fieldset input[type=text]::-moz-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=text]::-ms-input-placeholder, .search1506 .modal .modal_content form fieldset input[type=text]::-webkit-input-placeholder, .search1506 .modal .modal_content form fieldset textarea:-moz-placeholder, .search1506 .modal .modal_content form fieldset textarea::-moz-input-placeholder, .search1506 .modal .modal_content form fieldset textarea::-ms-input-placeholder, .search1506 .modal .modal_content form fieldset textarea::-webkit-input-placeholder {
        color:#636363
    }
    .search1506 #search_filter_container #search_filter UL.controls>LI.dropdown A img {
        vertical-align: middle
    }
    .search1506 .ss-descend.right:after, .search1506 .ss-descend:before, .search1506 .ss-dropdown.right:after, .search1506 .ss-dropdown:before {
        content: '▾'
    }
    .search1506 .allocation table td, .search1506 .contact-agent table td {
        vertical-align: middle
    }
    .search1506 article, .search1506 aside, .search1506 details, .search1506 figcaption, .search1506 figure, .search1506 footer, .search1506 header, .search1506 hgroup, .search1506 main, .search1506 nav, .search1506 section, .search1506 summary {
        display: block
    }
    .search1506 #welcome-to-urban-compass .introduction-sign-up-button {
        margin-top: 17px!important
    }
    .search1506 #search_filter_container #search_filter UL.controls {
        margin: 0;
        padding: 0;
        position: relative
    }
    .search1506 .width_container {
        margin: 0 auto;
        width: 100%
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_advanced #filter_price {
        margin-top: 8px
    }
    .search1506 #listings:after {
        display: table;
        content: "";
        clear: both
    }
    .search1506 .uc-footer .uc-footer-company {
        color: #f7f6f7;
        font-size: 14px;
        font-weight: 600
    }
    .search1506 .uc-footer .uc-footer-navMenuList {
        list-style-type: none;
        padding: 0;
        margin-top: 3px
    }
    .search1506 .allocation .agent__input__phone, .search1506 .contact-agent .agent__input__phone {
        margin-bottom: 12px
    }
    .search1506 .modal .modal_content form fieldset input[type=email], .search1506 .modal .modal_content form fieldset input[type=password], .search1506 .modal .modal_content form fieldset input[type=text], .search1506 .modal .modal_content form fieldset select, .search1506 .modal .modal_content form fieldset textarea {
        -webkit-border-radius: 4px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 4px;
        -moz-background-clip: padding;
        border-radius: 4px;
        background-clip: padding-box;
        border-radius: 4px 4px 4px 4px;
        transition: border-color .2s linear;
        border: 1px solid #dcdde0;
        font-size: 15px;
        padding: 9px 10px
    }
    .search1506 .modal .modal_content, .search1506 .modal footer, .search1506 .modal header {
        padding: 20px
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_rentals {
        width: 300px
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_bedrooms .popup, .search1506 #search_filter_container #search_filter UL.controls #filter_rentals .popup {
        padding: 20px 20px 20px 15px
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_bedrooms .popup {
        min-width: 162px
    }
    .search1506 .button-edge.button--block {
        width: 100%;
        padding-left: 0!important;
        padding-right: 0!important
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_advanced #filter_bedrooms input[type=checkbox]:checked+label {
        background-color: #227dda!important;
        color: #fff!important
    }
    .search1506 #listings:after, .search1506 #listings:before {
        content: " ";
        display: table
    }
    .search1506 #click_dismissal_overlay {
        display: none;
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        background-color: transparent;
        z-index: 0
    }
    .search1506 .input:focus {
        border: 1px solid #227dda!important
    }
    .search1506 #delete-account input[type=submit]:hover {
        background-color: #1a5da1!important
    }
    .search1506 input[type=text]:disabled {
        background-color: #f1f1f1;
        opacity: .7
    }
    .search1506 .uc-footer {
        background-color: #35373b;
        box-sizing: border-box;
        padding-top: 15px;
        font: 14px/1.6 proxima-nova, Lucida Grande, sans-serif;
        font-weight: 300;
        color: #73797e;
        text-rendering: optimizeLegibility;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased
    }
    .search1506 .allocation .input__header, .search1506 .contact-agent .input__header {
        font-size: 14px;
        font-weight: 600;
        margin-top: 12px;
        margin-bottom: 6px
    }
    .search1506 input[type=checkbox], .search1506 input[type=radio] {
        box-sizing: border-box;
        padding: 0
    }
    .search1506 input[type=button]::-moz-focus-inner, .search1506 input[type=submit]::-moz-focus-inner {
        margin-bottom:-1px;
        padding-top:1px
    }
    .search1506 #search_filter_container #search_filter UL {
        list-style: none
    }
    .search1506 .toggle img {
        padding-right: 2px;
        position: relative;
        bottom: 1px;
        vertical-align: middle
    }
    .search1506 .features-filters {
        min-width: 600px!important;
        padding: 0!important
    }
    .search1506 #map-introduction a, .search1506 #welcome-to-urban-compass a {
        padding: 8px 0;
        text-align: center
    }
    .search1506 .allocation table td.center, .search1506 .contact-agent table td.center {
        text-align: center
    }
    .search1506 #some-results-for-loggedin-message #welcome-to-urban-compass .additional-results {
        font-weight: 700;
        font-size: 16px;
        line-height: 22px
    }
    .search1506 .input {
        transition: border .2s linear
    }
    .search1506 .uc-footer .uc-footer-copyright, .search1506 .uc-footer .uc-footer-license {
        color: #73797e
    }
    .search1506 .semibold {
        font-weight: 600
    }
    .search1506 .uc-firstTimeGeographySelector p {
        font-size: 16px
    }
    .search1506 button, .search1506 input[type=button], .search1506 input[type=reset], .search1506 input[type=submit] {
        -webkit-appearance: button;
        cursor: pointer
    }
    .search1506 #welcome-to-urban-compass {
        background-color: #e4eefc;
        box-sizing: border-box;
        padding: 25px;
        position: relative
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_advanced #filter_price input[type=text] {
        -webkit-border-radius: 4px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 4px;
        -moz-background-clip: padding;
        border-radius: 4px;
        background-clip: padding-box;
        border-radius: 4px 4px 4px 4px;
        height: auto!important;
        padding: 11px
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_advanced #filter_bedrooms {
        margin-top: 16px;
        margin-bottom: 20px;
        padding-left: 0!important
    }
    .search1506 #some-results-for-loggedin-message #welcome-to-urban-compass {
        font-size: 18px;
        line-height: 25px;
        padding-top: 60px
    }
    .search1506 .fb-login .ss-icon {
        font-size: 16px;
        vertical-align: middle;
        margin-right: 14px
    }
    .search1506 h1 {
        font-size: 2em;
        margin: .67em 0
    }
    .search1506 .uc-footer .uc-footer-navMenuListItem {
        display: inline-block;
        padding-top: 15px;
        font-size: 14px;
        margin: 0 12px 0 0
    }
    .search1506 .button-edge {
        font-weight: 600;
        -webkit-border-radius: 4px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 4px;
        -moz-background-clip: padding;
        border-radius: 4px;
        background-clip: padding-box;
        border-radius: 4px 4px 4px 4px;
        display: inline-block;
        display: inline;
        zoom: 1;
        color: #fff!important;
        cursor: pointer;
        font-size: 14px!important;
        letter-spacing: .5px;
        padding: 13px 25px!important;
        text-align: center;
        text-decoration: none!important;
        text-transform: uppercase
    }
    .search1506 .allocation .agent__action, .search1506 .contact-agent .agent__action {
        text-align: center;
        margin: 12px 0
    }
    .search1506 .clearfix:after, .search1506 .u-clearfix:after {
        clear: both;
        display: table;
        content: ""
    }
    .search1506 table {
        border-collapse: collapse;
        border-spacing: 0
    }
    .search1506 .sign-in-with-facebook p {
        margin-top: 10px!important;
        margin-bottom: 7px!important;
        text-align: center
    }
    .search1506 #welcome-to-urban-compass div {
        color: #5e5e5e;
        text-align: center;
        font-size: 22px;
        line-height: 26px
    }
    .search1506 #contact-to-visit-submitted-image {
        display: inline-block;
        display: inline;
        zoom: 1;
        width: 35%;
        text-align: right;
        vertical-align: top
    }
    .search1506 #contact-agent .form_section input[type=submit] {
        margin-top: 20px
    }
    .search1506 #contact-to-visit-submitted {
        font-size: 14px
    }
    .search1506 input[type=email]::-moz-placeholder, .search1506 input[type=password]::-moz-placeholder, .search1506 input[type=text]::-moz-placeholder {
        color:#878f98
    }
    .search1506 #modal_overlay {
        display: none;
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        background-color: rgba(0,0,0,.5);
        z-index: 200;
        overflow-y: scroll
    }
    .search1506 .uc-footer .uc-footer-social .uc-footer-socialLink {
        margin-left: 2px
    }
    .search1506 .inline-button {
        display: inline-block;
        display: inline;
        zoom: 1;
        width: 48%!important
    }
    .search1506 #contact-to-visit-submitted-explanation, .search1506 #contact-to-visit-submitted-explanation-subsequent {
        display: inline-block;
        display: inline;
        zoom: 1;
        width: 60%;
        margin-right: 3%;
        margin-bottom: 15px
    }
    .search1506 #search .prefix {
        display: inline-block;
        display: inline;
        zoom: 1
    }
    .search1506 .button-edge.button--primary:hover {
        background-color: #1a5da1
    }
    .search1506 .sign-in-with-facebook {
        margin-bottom: 10px;
        border-bottom: 1px solid #ebebeb
    }
    .search1506 #availability .h1, .search1506 #availability h1, .search1506 #beta .h1, .search1506 #beta h1, .search1506 #schedule .h1, .search1506 #schedule h1, .search1506 #search .h1, .search1506 #search_alerts .h1, .search1506 #search_alerts h1, .search1506 #search h1, .search1506 #summary .h1, .search1506 #summary h1, .search1506 .experimental--body .h1, .search1506 .experimental--body h1 {
        color: #575a5e;
        font-size: 26px;
        margin-bottom: 10px
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_advanced #filter_bedrooms input[type=checkbox]+label {
        background-image: none!important;
        padding-left: 14px!important
    }
    .search1506 .pill table td.pill__section .pill__section__caption, .search1506 .pill table td.pill__section .pill__section__figure {
        font-weight: 700;
        line-height: 1.18
    }
    .search1506 .modal .modal_content {
        background-color: #fff
    }
    .search1506 #welcome-to-urban-compass:after, .search1506 #welcome-to-urban-compass:before {
        content: " ";
        display: table
    }
    .search1506 #welcome-to-urban-compass a {
        display: inline-block!important;
        margin-top: 0!important;
        padding-left: 55px!important;
        padding-right: 55px!important
    }
    .search1506 .small--edge {
        font-size: 11px
    }
    .search1506 #responsive-feature-filters {
        display: none
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_price .price_selection span {
        margin: 0 10px;
        line-height: 31px
    }
    .search1506 #contact-to-visit-submitted .submit-apartments {
        margin-bottom: 0!important
    }
    .search1506 #welcome-to-urban-compass .additional-results, .search1506 #welcome-to-urban-compass .total-additional-results {
        color: #227dda;
        font-weight: 700
    }
    .search1506 .uc-footer .uc-footer-container {
        width: 960px;
        margin: 0 auto
    }
    .search1506 #map-introduction .button:hover, .search1506 #welcome-to-urban-compass .button:hover {
        background-color: #1a5da1
    }
    .search1506 #search-introduction, .search1506 .advanced_filters__accessory {
        display: none
    }
    .search1506 #footer {
        padding-top: 15px
    }
    .search1506 #yes-button {
        margin-right: 2%
    }
    .search1506 .uc-footer .uc-footer-navigation {
        float: left;
        width: 75%;
        position: relative;
        bottom: 0
    }
    .search1506 .uc-footer .uc-footer-social .uc-footer-socialImage {
        height: 22px
    }
    .search1506 [dir=rtl] .slick-next:before {
        content: "\2190"
    }
    .search1506 .button-edge.button--facebook .ss-social-regular {
        vertical-align: middle
    }
    .search1506 .allocation .allocation__success, .search1506 .contact-agent .allocation__success {
        -webkit-border-radius: 50px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 50px;
        -moz-background-clip: padding;
        border-radius: 50px;
        background-clip: padding-box;
        border-radius: 50px 50px 50px 50px;
        padding: 15px;
        background-color: #f0f0f0;
        color: #227dda;
        display: inline-block;
        font-size: 48px;
        height: 78px;
        width: 78px;
        margin: 0 auto
    }
    .search1506 #contact-agent {
        width: auto
    }
    .search1506 .fb-login:hover {
        color: #fff;
        background-color: #314f8d
    }
    .search1506 #contact-to-visit-submitted .button-edge {
        margin-bottom: 15px
    }
    .search1506 .uc-footer .uc-footer-navMenuLink {
        color: #fff;
        font-weight: 400;
        font-size: 15px;
        text-decoration: none;
        position: relative
    }
    .search1506 #search, .search1506 #search_alerts {
        background-color: #f0f2f5
    }
    .search1506 .uc-footer .u-clearfix:after {
        display: table;
        clear: both;
        content: ""
    }
    .search1506 .uc-footer .uc-footer-social {
        float: right;
        position: relative;
        top: 20px
    }
    .search1506 .allocation .allocation__agents__list, .search1506 .contact-agent .allocation__agents__list {
        margin-top: 10px;
        max-height: 250px;
        overflow-y: auto
    }
    .search1506 .uc-footer .uc-footer-link:hover, .search1506 .uc-footer .uc-footer-socialLink:hover {
        opacity: .7
    }
    .search1506 #search_filter_container:after {
        display: table;
        content: "";
        clear: both
    }
    .search1506 .button-edge.button--facebook {
        transition: background-color .2s linear;
        background-color: #3b5997
    }
    .search1506 .input.input--block {
        width: 100%
    }
    .search1506 .button-edge.button--facebook:hover {
        background-color: #314f8d
    }
    .search1506 .input-2 .input {
        display: inline-block;
        display: inline;
        zoom: 1;
        width: 48%!important;
        margin-right: 2%
    }
    .search1506 #welcome-to-urban-compass:after {
        display: table;
        content: "";
        clear: both
    }
    .search1506 a {
        text-decoration: none
    }
    .search1506 input[type=email]::-webkit-input-placeholder, .search1506 input[type=password]::-webkit-input-placeholder, .search1506 input[type=text]::-webkit-input-placeholder {
        color:#878f98
    }
    .search1506 textarea {
        overflow: auto;
        vertical-align: top
    }
    .search1506 .fb-login::-moz-focus-inner {
        margin-bottom:-1px;
        padding-top:1px
    }
    .search1506 button:focus, .search1506 input:focus, .search1506 select:focus, .search1506 textarea:focus {
        outline: none
    }
    .search1506 #contact-agent .form_section {
        width: 273px;
        padding-bottom: 10px
    }
    .search1506 button, .search1506 select {
        text-transform: none
    }
    .search1506 #wrap {
        min-height: 100%;
        height: auto;
        margin: 0 auto -116px
    }

    @media (max-width:1169px) {
        .search1506 #wrap.fixedHeight {
            height: 100%!important
        }
    }
    .search1506 #search_filter_container {
        margin: 0;
        background: #fff;
        border-bottom: 1px solid #e8e8e8;
        position: relative;
        position: -webkit-sticky;
        position: sticky;
        z-index: 100;
        top: 0;
        left: 0;
        right: 0
    }
    .search1506 #wrap.fixedHeight #search_filter_container {
        z-index: 100
    }

    @media all and (max-width:1169px) {
        .search1506 #wrap.fixedHeight #search_filter_container {
            z-index: 700
        }
    }
    .search1506 #filter-mobile-toggle {
        display: none!important;
        position: absolute;
        top: 0;
        right: 0;
        color: #000;
        font-size: 16px
    }

    @media (max-width:1169px) {
        .search1506 #filter-mobile-toggle {
            display: block!important
        }
    }
    .search1506 .search1506-mobileToggle {
        display: block;
        padding: 18px 11px;
        height: 47px
    }
    .search1506 .search1506-mobileToggle .search1506-mobileToggle-icons {
        width: 10px;
        height: 10px
    }
    .search1506 #search_filter_container #search_filter.expanded .search1506-mobileToggle-open, .search1506 .search1506-mobileToggle-close {
        display: none
    }
    .search1506 #search_filter_container #search_filter.expanded .search1506-mobileToggle-close {
        display: inline
    }
    .search1506 #search_filter {
        margin: 0 120px 0 0;
        padding: 25px 40px
    }

    @media (max-width:1169px) {
        .search1506 #search_filter {
            padding: 25px;
            margin-right: 0
        }
    }
    .search1506 #search_filter_container #search_filter ul.controls #filter-regions {
        position: relative;
        min-width: 307px;
        min-height: 49px;
        max-width: none
    }
    .search1506 #search_filter_container #search_filter ul.controls #filter-regions .filter-regions-search {
        position: absolute;
        padding-left: 15px;
        width: 25px;
        height: 25px;
        top: 13px;
        right: 30px;
        border-left: 1px solid #e7e8e7
    }
    .search1506 #search_filter_container #search_filter ul.controls #filter-regions .filter-regions-search-icon {
        width: 24px;
        height: 24px
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter ul.controls #filter-regions {
            min-width: 0;
            max-width: none;
            width: calc(100% - 58px)
        }
    }
    .search1506 #search_filter_container #search_filter ul.controls>li {
        display: inline-block;
        margin-right: 2px;
        vertical-align: top
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter ul.controls>li {
            margin-right: 0
        }
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter ul.controls .dropdown {
            display: none
        }
    }
    .search1506 #search_filter_container #search_filter .dropdown {
        position: relative
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter.expanded ul.controls .dropdown {
            display: block
        }
    }
    .search1506 #search_filter_container #search_filter .dropdown>a {
        padding: 13px 17px 12px;
        display: block;
        min-width: 160px
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter .dropdown>a {
            min-width: 0;
            margin-top: 10px
        }
    }
    .search1506 #search_filter_container #search_filter ul.controls>li.dropdown .triangle {
        float: right;
        margin-left: 40px;
        position: relative;
        vertical-align: top;
        display: inline-block
    }
    .search1506 #search_filter .dropdown .popup {
        display: none;
        min-width: 100%;
        z-index: 1;
        position: absolute;
        padding: 15px;
        margin-top: 10px!important
    }

    @media (max-width:1169px) {
        .search1506 #search_filter .dropdown .popup {
            left: 0;
            right: 0;
            min-width: 0!important
        }
    }
    .search1506 #save_search #setAlert-popup.popup {
        padding: 0;
        z-index: 900
    }

    @media (max-width:1169px) {
        .search1506 #save_search #setAlert-popup.popup {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            border: 0
        }
    }
    .search1506 #search_filter_container #search_filter ul.controls .checklist {
        list-style: none;
        margin: 0;
        padding: 0
    }
    .search1506 #search_filter_container #search_filter ul.controls .checklist label {
        line-height: 30px;
        margin-left: 15px
    }
    .search1506 #filter_bedrooms .checklist label:after {
        content: ' Bedrooms'
    }
    .search1506 #filter_bedrooms .checklist label[for=bd0]:after {
        content: ''
    }
    .search1506 #filter_bedrooms .checklist label[for=bd1]:after {
        content: ' Bedroom'
    }
    .search1506 #search_filter_container #search_filter #save_search>a {
        min-width: 0;
        cursor: pointer
    }
    .search1506 #search_filter_container #search_filter .dropdown #features-filters {
        right: 0;
        overflow-x: hidden;
        overflow-y: auto
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter .dropdown #features-filters {
            margin-top: 0!important;
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 1000;
            overflow-x: hidden;
            overflow-y: auto;
            -webkit-overflow-scrolling: touch;
            max-height: none!important
        }
    }
    .search1506 #filters-table {
        width: 100%;
        margin: -1px;
        width: calc(100% + 2px)
    }
    .search1506 #filters-table>tbody>tr>td {
        padding: 12px 15px;
        border: 1px solid #efefef;
        vertical-align: top
    }

    @media (max-width:774px) {
        .search1506 #filters-table>tbody>tr>td {
            display: block;
            padding: 20px
        }
    }
    .search1506 #features-filters .search1506-closeFeaturesFiltersBtn {
        display: none;
        position: absolute;
        top: 0;
        right: 20px;
        color: #000
    }

    @media (max-width:1169px) {
        .search1506 #features-filters .search1506-closeFeaturesFiltersBtn {
            display: block
        }
    }
    .search1506 #save_search .search1506-closeSaveSearchBtn {
        display: none;
        position: absolute;
        top: 0;
        right: 20px;
        color: #000
    }

    @media (max-width:1169px) {
        .search1506 #save_search .search1506-closeSaveSearchBtn {
            display: block
        }
    }
    .search1506 #search_filter_container #view_toggle {
        position: absolute;
        right: 45px;
        top: 25px
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #view_toggle {
            display: none
        }
    }

    @media (min-width:1169px) {
        .search1506 #search_filter_container #search_filter UL.controls #save_search {
            position: absolute;
            right: 0;
            top: 0
        }
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter UL.controls #save_search {
            min-height: 49px;
            min-width: 0;
            max-width: none;
            width: 100%
        }
    }
    .search1506 #search_filter_container #search_filter ul.controls #filter_price .price_selection input {
        display: inline-block;
        width: 100px
    }
    .search1506 #search_filter_container #search_filter UL.controls #filter_advanced em {
        line-height: 1.9em;
        width: 200px;
        font-size: 14px
    }
    .search1506 .toggle {
        float: right;
        list-style-type: none;
        margin: 0 10px 0 0;
        padding: 0;
        position: relative;
        top: 5px
    }
    .search1506 .toggle li {
        float: left;
        padding: 13px;
        transition: background-color .2s linear
    }
    .search1506 .toggle li:first-child {
        margin-right: -1px
    }
    .search1506 .toggle li a {
        cursor: pointer;
        display: block;
        width: 21px;
        height: 21px
    }
    .search1506 .toggle li a .viewToggle-icons {
        width: 100%;
        height: 21px
    }
    .search1506 .toggle li:not(.active) a .viewToggle-icons {
        fill: #a3a3a3
    }
    .search1506 .search1506-radioList {
        border: 1px solid #e8e8e8;
        list-style: none;
        margin: 0;
        padding: 0;
        overflow: hidden
    }
    .search1506 .search1506-radioList li {
        float: left
    }
    .search1506 .search1506-radioList input[type=radio] {
        display: none
    }
    .search1506 .search1506-radioList label {
        display: inline-block;
        border-left: 1px solid #e8e8e8;
        margin-left: -1px;
        text-align: center;
        color: #e8e8e8;
        padding: 10px 10px 9px;
        font-size: 16px
    }
    .search1506 .search1506-radioList input:checked+label {
        color: #000
    }
    .search1506 #filter_baths.search1506-radioList li, .search1506 #filter_ratings.search1506-radioList li {
        width: 20%
    }
    .search1506 #filter_baths.search1506-radioList label, .search1506 #filter_ratings.search1506-radioList label {
        display: block;
        box-sizing: border-box
    }
    .search1506 #filter_baths, .search1506 #filter_date_available, .search1506 #filter_fee_types, .search1506 #filter_ratings, .search1506 #max_monthly_fees, .search1506 #min_square_footage {
        margin: 15px 0 25px 3px
    }
    .search1506 #ui-datepicker-div {
        display: none;
        z-index: 1000;
        background-color: #fff;
        border: 1px solid #e8e8e8;
        margin-top: 5px;
        padding: 15px
    }
    .search1506 #ui-datepicker-div th {
        padding: 5px 6px
    }
    .search1506 #ui-datepicker-div td {
        border: 1px solid #e8e8e8;
        padding: 0!important;
        text-align: center
    }
    .search1506 #ui-datepicker-div td:hover {
        background-color: #fcfcfc
    }
    .search1506 #ui-datepicker-div .ui-state-disabled, .search1506 #ui-datepicker-div .ui-state-disabled:hover {
        background-color: #fff
    }
    .search1506 #ui-datepicker-div .ui-datepicker-unselectable {
        font-size: 14px;
        padding: 6px!important
    }
    .search1506 #ui-datepicker-div .ui-datepicker-title {
        font-size: 16px;
        margin-bottom: 5px;
        text-align: center;
        color: #000
    }
    .search1506 #ui-datepicker-div .ui-datepicker-prev {
        float: left
    }
    .search1506 #ui-datepicker-div .ui-datepicker-prev.ui-state-disabled {
        background-color: transparent;
        cursor: default;
        opacity: .5
    }
    .search1506 #ui-datepicker-div .ui-datepicker-next {
        float: right
    }
    .search1506 #ui-datepicker-div a {
        color: #000;
        cursor: pointer;
        font-size: 15px
    }
    .search1506 #ui-datepicker-div table a {
        display: block;
        padding: 6px
    }

    @media (max-width:1455px) {
        .search1506 #filter-mobile-toggle.shortTerm {
            display: block!important
        }
    }
    .search1506 #wrap.fixedHeight #search_filter_container.shortTerm {
        z-index: 800
    }

    @media (max-width:1455px) {
        .search1506 #search_filter.shortTerm {
            padding: 25px;
            margin-right: 0
        }
    }
    .search1506 #search_filter_container #search_filter.shortTerm ul.controls #filter_rentals {
        min-width: 325px;
        min-height: 49px;
        max-width: none
    }

    @media (max-width:700px) {
        .search1506 #search_filter_container #search_filter.shortTerm ul.controls #filter_rentals {
            width: calc(100% - 58px)
        }
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter ul.controls #filter_rentals.dropdown {
            display: inline-block!important
        }
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter ul.controls #filter_rentals.dropdown>a {
            min-width: 0;
            margin: 0
        }
    }
    .search1506 #search_filter_container #search_filter.shortTerm ul.controls #filter-regions {
        width: 325px
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container #search_filter.shortTerm ul.controls #filter-regions {
            width: calc(100% - 470px)
        }
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container #search_filter.shortTerm ul.controls #filter-regions {
            width: calc(100% - 365px)
        }
    }

    @media (max-width:700px) {
        .search1506 #search_filter_container #search_filter.shortTerm ul.controls #filter-regions {
            display: none
        }
    }

    @media (max-width:700px) {
        .search1506 #search_filter_container #search_filter.expanded.shortTerm ul.controls #filter-regions {
            display: block!important;
            margin-top: 10px;
            margin-bottom: 10px;
            width: 100%
        }
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container #search_filter.shortTerm ul.controls>li {
            margin-right: 0
        }
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container #search_filter.shortTerm ul.controls .hideMobile {
            display: none
        }
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container #search_filter.expanded.shortTerm ul.controls .hideMobile {
            display: block
        }
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container #search_filter.shortTerm .dropdown>a {
            min-width: 0;
            margin-bottom: 10px
        }
    }

    @media (max-width:1455px) {
        .search1506 #search_filter.shortTerm .dropdown .popup {
            left: 0;
            right: 0;
            min-width: 0!important;
            z-index: 900
        }
    }

    @media (max-width:1455px) {
        .search1506 #save_search.shortTerm #setAlert-popup.popup {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            border: 0
        }
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container #search_filter.shortTerm .dropdown #features-filters {
            margin-top: 0!important;
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 1000;
            overflow-x: hidden;
            overflow-y: auto;
            -webkit-overflow-scrolling: touch;
            max-height: none!important
        }
    }

    @media (max-width:1455px) {
        .search1506 #features-filters.shortTerm .search1506-closeFeaturesFiltersBtn, .search1506 #save_search.shortTerm .search1506-closeSaveSearchBtn {
            display: block
        }
    }

    @media (max-width:1169px) {
        .search1506 #search_filter_container.shortTerm #view_toggle {
            display: none
        }
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container.shortTerm #view_toggle {
            right: 50px
        }
    }
    .search1506 #search_filter_container #search_filter.shortTerm UL.controls #save_search {
        position: absolute;
        right: 0;
        top: 0
    }

    @media (max-width:1455px) {
        .search1506 #search_filter_container #search_filter.shortTerm UL.controls #save_search {
            position: static;
            min-height: 49px;
            min-width: 0;
            max-width: none;
            width: 100%
        }
    }
    .search1506 #search_filter_container.searchUiBuilderActive {
        border-bottom: 1px solid #f3f3f3
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container.active {
        display: inline-block!important
    }

    @media (min-width:690px) {
        .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container.active {
            float: left
        }
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle {
        display: none
    }

    @media (min-width:690px) {
        .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle {
            display: inline!important
        }
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter .saveSearch {
        float: right;
        padding-left: 10px
    }

    @media (min-width:690px) {
        .search1506 .searchUiBuilderActive.secondarySearchFilter .saveSearch {
            margin: 0 15px 0 0
        }
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #search_results_sort_by {
        border: none;
        padding: 10px 0 0 10px;
        width: 100%;
        background: transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none
    }

    @media (max-width:774px) {
        .search1506 .searchUiBuilderActive.secondarySearchFilter #search_results_sort_by {
            position: static
        }
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container .search_results_stats {
        float: left;
        line-height: 38px;
        font-size: 14px
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container .pagination {
        margin: 0 0 0 10px
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container .pagination a {
        padding: 10px
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle.toggle {
        position: relative!important;
        top: 0!important;
        right: 0!important;
        list-style-type: none
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle.toggle li {
        float: left;
        padding: 8px;
        transition: background-color .2s linear
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle.toggle li:first-child {
        margin-right: 0
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle .toggle-map {
        border-color: #d7d7d7;
        border-style: solid;
        border-width: 1px 0 1px 1px
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle .toggle-list {
        border-color: #d7d7d7;
        border-style: solid;
        border-width: 1px 1px 1px 0
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #view_toggle.toggle .active {
        border: 1px solid #ffd5ba;
        background-color: #ffebdf
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter .searchSort-icon {
        position: absolute;
        top: 8px;
        right: 10px;
        width: 20px;
        height: 20px;
        pointer-events: none
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter .saveSearch-button {
        padding: 10px;
        min-width: 0;
        font-size: 14px
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #search_results_sort_by_wrapper {
        display: none;
        float: left;
        position: relative;
        margin-left: 10px;
        margin-top: 0;
        border: 1px solid #d7d7d7;
        width: 165px;
        height: 39px;
        font-size: 14px
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #search_results_sort_by_wrapper.active {
        display: inline
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter {
        padding-top: 8px;
        padding-bottom: 8px;
        width: 100%;
        background-color: #f3f3f3
    }
    .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container {
        display: none;
        margin-top: 0;
        margin-left: 0
    }

    @media (min-width:690px) {
        .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container {
            margin-left: 40px
        }
    }

    @media (min-width:970px) {
        .search1506 .searchUiBuilderActive.secondarySearchFilter #simple_pagination_container {
            float: left
        }
    }
    .search1506 .pagination-arrow {
        width: 17px;
        height: 17px
    }
    .search1506 .secondarySearchFilter-leftColumn {
        padding-bottom: 10px;
        text-align: center
    }

    @media (min-width:690px) {
        .search1506 .secondarySearchFilter-leftColumn {
            float: left;
            padding-bottom: 0
        }
    }
    .search1506 .secondarySearchFilter-rightColumn {
        text-align: center
    }
    .search1506 .secondarySearchFilter-rightColumn-container {
        display: inline-block
    }

    @media (min-width:690px) {
        .search1506 .secondarySearchFilter-rightColumn-container {
            display: inline
        }
    }
    .search1506 .searchBar-lost-1_1 {
        width: 99.99%
    }
    .search1506 .searchBar-lost-1_1:nth-child(1n) {
        float: left;
        margin-right: 30px;
        clear: none
    }
    .search1506 .searchBar-lost-1_1:last-child {
        margin-right: 0
    }
    .search1506 .searchBar-lost-1_1:nth-child(1n) {
        margin-right: 0;
        float: right
    }
    .search1506 .searchBar-lost-1_1:nth-child(1n+1) {
        clear: left
    }
    .search1506 #listings_results_content {
        position: relative
    }
    .search1506 #listings {
        border-top: 0;
        display: none
    }
    .search1506 #listings_results_column {
        position: relative;
        padding: 0 25px;
        background-color: #f3f3f3
    }

    @media (max-width:1169px) {
        .search1506 #listings_results_column {
            padding-left: 25px!important;
            padding-right: 25px!important
        }
    }

    @media (min-width:630px) {
        .search1506 #listings_results_column {
            padding-left: 40px;
            padding-right: 40px;
            float: left;
            width: 340px
        }
    }

    @media (min-width:970px) {
        .search1506 #listings_results_column {
            width: 680px
        }
        @media (max-width:1169px) {
            .search1506 #listings_results_column {
                width:650px
            }
        }
    }

    @media (min-width:1280px) {
        .search1506 #listings_results_column {
            width: 990px
        }
    }

    @media (min-width:630px) {
        .search1506 #listings_results_column.searchUiBuilderActive {
            position: relative;
            width: 100%;
            padding: 0 25px;
            background-color: #f3f3f3
        }
    }

    @media (min-width:690px) {
        .search1506 #listings_results_column.searchUiBuilderActive {
            padding-left: 40px;
            padding-right: 40px;
            float: left;
            width: 340px
        }
    }

    @media (min-width:970px) {
        .search1506 #listings_results_column.searchUiBuilderActive {
            width: 680px
        }
    }

    @media (min-width:1280px) {
        .search1506 #listings_results_column.searchUiBuilderActive {
            width: 990px
        }
    }
    .search1506 #listings_map_column {
        visibility: hidden;
        position: fixed;
        left: 0;
        right: 0;
        bottom: 0;
        top: 100px
    }

    @media (min-width:630px) {
        .search1506 #listings_map_column {
            visibility: visible;
            left: 340px
        }
    }

    @media (min-width:970px) {
        .search1506 #listings_map_column {
            left: 650px
        }
        @media (max-width:1169px) {
            .search1506 #listings_map_column {
                left:680px
            }
        }
    }

    @media (min-width:1280px) {
        .search1506 #listings_map_column {
            left: 990px
        }
    }

    @media (min-width:630px) {
        .search1506 #listings_map_column.searchUiBuilderActive {
            visibility: hidden
        }
    }

    @media (min-width:690px) {
        .search1506 #listings_map_column.searchUiBuilderActive {
            visibility: visible;
            left: 340px
        }
    }

    @media (min-width:970px) {
        .search1506 #listings_map_column.searchUiBuilderActive {
            left: 650px
        }
        @media (max-width:1169px) {
            .search1506 #listings_map_column.searchUiBuilderActive {
                left:680px
            }
        }
    }

    @media (min-width:1280px) {
        .search1506 #listings_map_column.searchUiBuilderActive {
            left: 990px
        }
    }
    .search1506 #listings_results {
        list-style: none;
        margin: 0;
        padding: 0
    }
    .search1506 #listings_results .c0-pillBoxImage img {
        display: none
    }
    .search1506 #listings_results .listing {
        position: relative;
        display: block;
        margin: 0 auto 20px;
        width: 290px;
        height: 294px
    }

    @media (min-width:970px) {
        .search1506 #listings_results .listing {
            float: left
        }
        .search1506 #listings_results .listing:nth-child(2n) {
            margin-left: 20px
        }
    }

    @media (min-width:1280px) {
        .search1506 #listings_results .listing:nth-child(2n) {
            margin-left: 0
        }
        .search1506 #listings_results .listing:nth-child(3n), .search1506 #listings_results .listing:nth-child(3n - 1) {
            margin-left: 20px
        }
    }
    .search1506 #listings_results .listing .search1506-listingPill {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0
    }
    .search1506 #listings-map {
        width: 100%;
        height: 100%
    }
    .search1506 #search_filter_secondary {
        padding: 20px 0
    }
    .search1506 #search_results_sort_by_wrapper {
        height: 51px;
        position: relative;
        border: 1px solid #d7d7d7
    }
    .search1506 #search_results_sort_by_wrapper:before {
        position: absolute;
        top: 30%;
        right: 15px;
        pointer-events: none
    }

    @media (min-width:970px) {
        .search1506 #search_results_sort_by_wrapper {
            float: left
        }
    }
    .search1506 #search_results_sort_by {
        border: none;
        background: transparent;
        width: 100%;
        padding: 15px 55px 15px 15px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none
    }

    @media (max-width:774px) {
        .search1506 #search_results_sort_by {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 15px
        }
    }
    .search1506 #simple_pagination_container {
        margin-top: 10px
    }

    @media (max-width:375px) {
        .search1506 #simple_pagination_container {
            display: none!important
        }
    }

    @media (min-width:970px) {
        .search1506 #simple_pagination_container {
            float: right;
            margin-top: 0
        }
    }
    .search1506 #simple_pagination_container .search_results_stats {
        float: left;
        line-height: 54px
    }
    .search1506 #simple_pagination_container .pagination {
        display: inline-block;
        list-style: none;
        margin: 0 0 0 35px;
        padding: 0;
        float: right
    }
    .search1506 #simple_pagination_container .pagination li {
        float: left
    }
    .search1506 #simple_pagination_container .pagination li:last-child {
        margin-left: -1px
    }
    .search1506 #simple_pagination_container .pagination a {
        display: block;
        padding: 18px;
        border: 1px solid #d7d7d7;
        cursor: pointer
    }
    .search1506 #simple_pagination_container .pagination a:hover {
        background-color: #fff
    }
    .search1506 #simple_pagination_container .pagination a[disabled] {
        cursor: default;
        pointer-events: none
    }
    .search1506 #simple_pagination_container .pagination .c0-icon {
        display: block;
        width: 16px;
        height: 16px
    }
    .search1506 #simple_pagination_container .pagination .c0-icon:before {
        font-size: 36px;
        line-height: 16px;
        color: #000
    }
    .search1506 #pagination_container {
        text-align: center
    }
    .search1506 #pagination_container .pagination {
        display: inline-block;
        margin: 40px 0;
        list-style: none;
        padding: 0;
        text-align: center
    }
    .search1506 #pagination_container .pagination li {
        float: left;
        border: 1px solid #d7d7d7;
        margin-left: -1px;
        display: inline-block;
        background: #fff;
        padding: 18px
    }
    .search1506 #pagination_container .pagination a {
        cursor: pointer;
        padding: 18px;
        margin: -18px
    }
    .search1506 #pagination_container .pagination .active {
        color: #000
    }
    .search1506 #no-search-results {
        display: none;
        background: #fff;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 1;
        color: #000;
        padding: 15px 40px
    }
    .search1506 #no-results-message h1, .search1506 #no-search-results h1 {
        font-size: 24px;
        line-height: 26px;
        color: #000
    }
    .search1506 #no-results-message h2, .search1506 #no-search-results h2 {
        font-size: 20px;
        line-height: 23px
    }
    .search1506 #no-results-message {
        display: none;
        text-align: center;
        max-width: 450px;
        width: 100%;
        margin: 50px auto
    }
    .search1506 #no-results-message .clear-search-filters {
        cursor: pointer;
        margin: 40px 0;
        padding: 15px 40px;
        display: inline-block;
        font-size: 16px;
        color: #000
    }
    .search1506 .search1506-hiddenListings {
        display: none;
        padding: 20px 0;
        margin-bottom: 20px;
        color: #000;
        font-size: 13px;
        border-top: 1px solid #d7d7d7;
        border-bottom: 1px solid #d7d7d7
    }
    .search1506 #search-introduction {
        padding: 40px;
        position: relative;
        text-align: center;
        top: 2px
    }
    .search1506 .experimental--tooltip {
        display: none;
        background-color: #fff;
        border: 1px solid #e8e8e8;
        z-index: 1000
    }
    .search1506 .experimental--tooltip.experimental--tooltip--bottom {
        margin-top: 15px;
        position: absolute;
        right: 0
    }
    .search1506 .saveSearch.searchUiBuilderActive .experimental--tooltip.experimental--tooltip--bottom {
        margin-top: 10px
    }
    .search1506 .experimental--tooltip .tooltip__section {
        border-bottom: 1px solid #e8e8e8;
        padding: 30px;
        text-align: left
    }
    .search1506 .experimental--tooltip .tooltip__section:last-child {
        border-bottom: 0
    }
    .search1506 .experimental--tooltip .tooltip__section .tooltip__section__message {
        display: inline-block;
        width: 100%;
        font-size: 16px;
        vertical-align: middle
    }
    .search1506 .experimental--tooltip--overlay {
        display: none;
        background-color: rgba(0,0,0,.4);
        bottom: 0;
        left: 0;
        position: absolute;
        right: 0;
        top: 0;
        z-index: 900
    }
    .search1506 .experimental--tooltip--overlay--fixed {
        position: fixed;
        top: 42px
    }
    .search1506 .experimental--tooltip--overlay--transparent {
        background-color: transparent
    }

    @media (max-width:1169px) {
        .search1506 .experimental--tooltip.responsive-hover {
            width: 100%;
            min-width: 360px
        }
    }

    @media (min-width:1169px) {
        .search1506 .experimental--tooltip.responsive-hover {
            width: 360px
        }
    }
    .search1506 .experimental--tooltip.experimental--tooltip--alert-after .tooltip__section, .search1506 .experimental--tooltip.experimental--tooltip--alert-before .tooltip__section, .search1506 .experimental--tooltip.experimental--tooltip--alert-confirm .tooltip__section {
        width: 100%;
        min-width: 360px
    }
    .search1506 .experimental--tooltip.experimental--tooltip--alert-confirm .save_button {
        transition: background-color .2s linear
    }
    .search1506 .experimental--tooltip .alert--header {
        color: #000;
        line-height: 18px;
        font-size: 22px
    }
    .search1506 .experimental--tooltip .alert--added--message {
        margin-bottom: 60px
    }
    .search1506 .experimental--tooltip .tooltip__section__footer {
        background: #fff;
        border-top: 1px solid #e8e8e8;
        margin: -16px -30px -30px;
        padding: 30px;
        color: #000
    }
    .search1506 .experimental--tooltip .alert--preferencesHeader {
        margin-bottom: 15px;
        font-size: 14px
    }
    .search1506 .experimental--tooltip .alert-preferencesBody {
        margin-bottom: 30px
    }
    .search1506 .experimental--tooltip .alert-preferencesBody label {
        display: block;
        line-height: 30px
    }
    .search1506 .experimental--tooltip .alert-preferencesBody input {
        margin-right: 5px
    }
    .search1506 .experimental--tooltip .tooltip__section__footer .c0-btnConsumer {
        display: block
    }
    .search1506 .experimental--tooltip.experimental--tooltip--alert-before .alert--header, .search1506 .experimental--tooltip.experimental--tooltip--alert-confirm .ss-icon {
        display: none
    }
    .search1506 .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-card-body {
        border-top: 1px solid #e8e8e8;
        padding-top: 20px;
        margin-top: 20px
    }
    .search1506 .experimental--tooltip.experimental--tooltip--alert-confirm .search-alert-neighborhood {
        margin: 2px 0 15px;
        color: #000;
        line-height: 24px;
        font-size: 28px
    }
    .search1506 #listings-map .marker_class, .search1506 #map .marker_class {
        color: #000;
        font-size: 14px;
        cursor: pointer
    }
    .search1506 #listings-map .marker_class:hover, .search1506 #map .marker_class:hover {
        color: #fff
    }
    .search1506 #listings-map .marker_class {
        overflow: hidden;
        text-indent: -9999px
    }
    .search1506 #map .marker_class.marker_class--active {
        color: #fff
    }
    .search1506 #map .uc-tooltip-container {
        position: absolute;
        margin-bottom: 10px;
        background: #fff;
        border: 1px solid #000;
        padding: 10px 20px;
        color: #000;
        font-size: 16px;
        line-height: 1.4em
    }
    .search1506 #map .uc-tooltip-container .uc-tooltip {
        white-space: nowrap
    }
    .search1506 #listings_and_map {
        position: absolute;
        top: 180px;
        bottom: 0;
        width: 100%;
        display: none;
        z-index: 0
    }

    @media (max-width:1169px) {
        .search1506 #listings_and_map {
            top: 169px
        }
    }
    .search1506 #listings_and_map .map-container {
        height: 100%
    }
    .search1506 #listings_and_map .map-container #map {
        height: 100%;
        width: 100%
    }
    .search1506 .listing-picker-popup {
        display: none;
        width: 420px;
        max-height: 600px;
        position: relative;
        overflow: auto;
        overflow-x: hidden;
        background: #fff;
        box-shadow: 0 6px 15px rgba(0,0,0,.2)
    }
    .search1506 .listing-picker-popup::-webkit-scrollbar-thumb {
        background-color:#000
    }
    .search1506 .listing-picker-popup::-webkit-scrollbar {
        -webkit-appearance:none;
        width:5px;
        background-color:#e7e8e7
    }
    .search1506 .listing-picker-popup .listing {
        overflow: hidden;
        display: block
    }
    .search1506 .listing-picker-popup .images {
        float: left;
        width: 120px
    }
    .search1506 .listing-picker-popup h2 {
        margin: 0
    }
    .search1506 .listing-picker-popup .c0-noImagePlaceholder {
        background-size: 60px
    }
    .search1506 .listing-picker-popup .c0-dotGridBackground {
        position: relative;
        height: 120px
    }
    .search1506 .listing-picker-popup .search1506-listingPopupAddress, .search1506 .listing-picker-popup .search1506-listingPopupNeighborhood {
        padding-left: 23px;
        padding-right: 23px;
        margin-left: 120px
    }
    .search1506 .search1506-noImagePlaceholder {
        height: 100%;
        background: 50% no-repeat;
        background-size: 65%
    }
    .search1506 .search1506-listingPopupAddress {
        padding-top: 20px
    }
    .search1506 #modal_overlay {
        z-index: 1000
    }
    .search1506 #quick-listing {
        position: relative;
        max-width: 570px;
        background-color: #fff
    }
    .search1506 #quick-listing .modal_close {
        color: #fff;
        cursor: pointer;
        font-size: 60px;
        line-height: 1;
        position: absolute;
        right: 0;
        top: -55px;
        box-shadow: 0 0 0 #000
    }
    .search1506 .flexslider .slides>li {
        display: none;
        -webkit-backface-visibility: hidden
    }
    .search1506 #quick-listing .search1506-quickListingPillBoxCard {
        background: transparent
    }
    .search1506 #quick-listing .flexslider {
        position: relative
    }
    .search1506 #quick-listing .flexslider .slides {
        margin-top: 0;
        margin-left: -40px;
        overflow: hidden;
        height: 428px
    }
    .search1506 #quick-listing .flexslider .slides li {
        overflow: hidden;
        display: table!important;
        width: 100%;
        height: 100%
    }
    .search1506 #quick-listing .flexslider .slides li div {
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 100%
    }
    .search1506 #quick-listing .flexslider .slides li div.c0-pillBoxImage {
        height: 100%;
        width: 100%;
        display: table
    }
    .search1506 #quick-listing .flexslider .slides li div img {
        max-width: 100%;
        max-height: 100%;
        margin: auto
    }
    .search1506 #quick-listing .listing-alert {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 200;
        background: rgba(227,124,78,.9);
        padding: 10px 20px;
        color: #fff;
        font-size: 14px;
        text-transform: uppercase
    }
    .search1506 #quick-listing .flex-direction-nav {
        position: absolute;
        top: 50%;
        left: 0;
        right: 0;
        z-index: 100;
        list-style: none;
        margin: -15px 0 0;
        padding: 0
    }
    .search1506 #quick-listing .flex-direction-iconOverlay {
        position: absolute;
        top: 208px;
        z-index: 100;
        width: 100%
    }
    .search1506 #quick-listing .flex-direction-iconOverlay .flex-icons {
        padding: 2px;
        width: 30px;
        height: 30px;
        display: inline-block;
        background: rgba(0,0,0,.85)
    }
    .search1506 #quick-listing .flex-direction-iconOverlay .flex-icons svg {
        width: 25px;
        height: 25px;
        fill: #fff
    }
    .search1506 #quick-listing .flex-direction-iconOverlay .flex-icons.flex-icons-leftIcon {
        float: left
    }
    .search1506 #quick-listing .flex-direction-iconOverlay .flex-icons.flex-icons-rightIcon {
        float: right
    }
    .search1506 #quick-listing .flex-direction-nav a {
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        overflow: hidden;
        text-indent: -9999px
    }
    .search1506 #quick-listing .flex-direction-nav a.flex-prev {
        left: 0
    }
    .search1506 #quick-listing .flex-direction-nav a.flex-next {
        right: 0
    }
    .search1506 #quick-listing .search1506-quickListingButtons {
        padding: 25px 30px
    }
    .search1506 #quick-listing .search1506-quickListingButton {
        width: 48%;
        display: block;
        border-color: #e8e8e8
    }
    .search1506 #quick-listing .search1506-quickListingButton:first-child {
        float: left
    }
    .search1506 #quick-listing .search1506-quickListingButton:last-child {
        float: right
    }
    .search1506 .search1506-quickListingCourtesyOf {
        padding: 25px 30px 0
    }
    .search1506 .search1506-mapHiddenListings {
        display: none;
        background: rgba(0,0,0,.5);
        border: none;
        position: absolute;
        top: 10px;
        right: 10px;
        z-index: 1;
        color: #fff;
        padding: 15px 40px
    }
    .search1506 #listings_and_map.searchUiBuilderActive {
        top: 195px
    }

    @media (max-width:1169px) {
        .search1506 #listings_and_map.searchUiBuilderActive {
            top: 185px
        }
    }
    .search1506 * {
        box-sizing: border-box
    }
    .search1506 .header1506 {
        position: relative;
        z-index: 200
    }
    .search1506 .footer1506 {
        display: none
    }
    @-webkit-keyframes a {
        0% {
            opacity:0
        }
        to {
            opacity:1
        }
    }
    @keyframes a {
        0% {
            opacity:0
        }
        to {
            opacity:1
        }
    }
    @-webkit-keyframes b {
        0% {
            -webkit-transform:rotate(45deg) scaleY(0);
            transform:rotate(45deg) scaleY(0)
        }
        to {
            -webkit-transform:rotate(45deg) scaleX(1);
            transform:rotate(45deg) scaleX(1)
        }
    }
    @keyframes b {
        0% {
            -webkit-transform:rotate(45deg) scaleY(0);
            transform:rotate(45deg) scaleY(0)
        }
        to {
            -webkit-transform:rotate(45deg) scaleX(1);
            transform:rotate(45deg) scaleX(1)
        }
    }
    @-webkit-keyframes c {
        0% {
            -webkit-transform:rotate(45deg);
            transform:rotate(45deg)
        }
        60%, to {
            -webkit-transform:rotate(225deg);
            transform:rotate(225deg)
        }
    }
    @keyframes c {
        0% {
            -webkit-transform:rotate(45deg);
            transform:rotate(45deg)
        }
        60%, to {
            -webkit-transform:rotate(225deg);
            transform:rotate(225deg)
        }
    }
    #search-in-progress {
        display: block;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 1000;
        background: hsla(0,0%,100%,.9);
        -webkit-animation: a .5s forwards;
        animation: a .5s forwards
    }
    #search-in-progress .search-icon {
        position: absolute;
        top: 50%;
        right: 50%;
        background: #000;
        width: 4px;
        height: 125px;
        margin: -62px 0 0 -2px;
        -webkit-transform: rotate(45deg) scaleY(0);
        transform: rotate(45deg) scaleY(0)
    }
    #search-in-progress.is-animating .search-icon {
        -webkit-animation: b .5s .5s, c 1.2s cubic-bezier(.47, .01, 0, 1.49) 1s infinite;
        animation: b .5s .5s, c 1.2s cubic-bezier(.47, .01, 0, 1.49) 1s infinite
    }
    .search1506-disclaimer {
        font-size: 16px;
        padding-bottom: 60px
    }

    .pagination .page, .pagination .page a {
        font-size: 13px;
    }
</style>

<div class="row-fluid" id="map-search-container">
    <?php $this->renderPartial('_sideBarSearch', array(
            'model' => $model,
            'searchableFields'=> $searchableFields,
            'action' => $action
        )
    );
    ?>
</div>
<div class="pull-right no-margin" style="margin: 4px 4px 0px 0px !important;">
    <ul class="pagination no-margin" id="mapPager">
    </ul>
</div>
<div class="row no-gutters" id="mapview">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" id="map-container">
        <div id="map_draw" class="sp m_draw leaflet-draw-draw-polygon" onclick="enable_map_draw()" style="top: 10px; left: 10px; display: block;" title="Draw a custom search area"></div>
        <div id="map_remove" class="sp m_remove" onclick="remove_drawing()" style="display: none; top: 50px; left: 10px;" title="Remove"></div>
        <div id="map_cancel" class="sp m_cancel" onclick="cancel_drawing()" style="display: none; top: 10px; left: 10px;" title="Cancel"></div>
        <div id="map"></div>
    </div>
    <div class="map-home-list col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div id="maxListingsReached" class="ng-hide">
            <p class="bg-info" style="padding: 8px; font-weight: bold;">Displaying <span class="currentPosition">250</span> of <span class="totalHomes"></span> properties. Use filters to narrow your search.</p>
        </div>
        <div id="listingsFound" class="ng-hide">
            <p class="bg-success" style="padding: 8px; font-weight: bold;"><span class="totalHomes"></span> properties found.</p>
        </div>
        <section id="listings" class="row">

        </section>
    </div>
</div>

<?php

/*$this->widget('front_module.components.StmListView', array(
        'dataProvider' => $DataProvider,
        'template' => '{pager}{summary}{items}{pager}{summary}',
        'id' => 'home-list',
        'ajaxUpdate' => false,
        'itemView' => '_homeListRow',
        'itemsCssClass' => 'datatables',
        'afterAjaxUpdate' => 'js:function(id,data) {
		                                            $(\'#home-list\').replaceWith($(\'#home-list\', \'<div>\'+data+\'</div>\'));
		                                            $(\'div.loading-container.loading\').remove();
		                                        }',
    )
);*/
?>

<!-- JavaScript -->
<script type="text/javascript">

    var initLatLng, infowindow, locations, bounds;
    var markers = [];
    var map;

    // Google API key and map instance
    var apiKey = 'AIzaSyDjhLLd1T4xTZ031g87p_xCyQloz-dbW-Q', map;

    // Callback function for when the Google API has initialized
    function initMap() {

        // Update the container class to be 100% width, also remove some excess padding
        var wrapper = document.getElementById('map').parentNode.parentNode.parentNode;
        wrapper.classList.remove('container');
        wrapper.style.paddingTop = '116px';

        // Remove the footer
        document.getElementById('footer-house-values-container').remove();
        document.getElementsByTagName('footer')[0].remove();

        infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            scrollwheel: true,
            disableDefaultUI: true
            //center: startLatLng, //new google.maps.LatLng($center),
            //mapTypeId: google.maps.MapTypeId.SATELLITE
        });

        /*map.on('draw:created', function(e) {
           execute_map_draw(e.layer);
        });*/

        initLatLng = map.getCenter();

        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: false,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON,
                ]
            },
            markerOptions: {icon: 'http://cdn.seizethemarket.com/assets/images/map-house-marker-green.png'},
            polygonOptions: {
                //fillColor: '#ffff00',
                //fillOpacity: 1,
                //strokeWeight: 5,
                clickable: false,
                editable: false,
                zIndex: 1
            }
        });
        drawingManager.setDrawingMode(null);
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
            var positions = polygon;
            var _verts = polygon.getPath();

            verticies = [];
            for (var i = 0; i < _verts.length; i++) {
                var xy = _verts.getAt(i);
                verticies.push({'lat': xy.lat(), 'lng': xy.lng()});
            }

            $('#map-search').submit();
            is_drawing = false;
        });

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
            if (e.type != google.maps.drawing.OverlayType.MARKER) {
                // Switch back to non-drawing mode after drawing a shape.
                drawingManager.setDrawingMode(null);

                // Add an event listener that selects the newly-drawn shape when the user
                // mouses down on it.
                var newShape = e.overlay;
                newShape.type = e.type;
                google.maps.event.addListener(newShape, 'click', function () {
                    setSelection(newShape);
                });
                setSelection(newShape);

                is_drawing = false;
            }
        });
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        $('#listings').html('');
        markers = [];
    }

    function makePins(data) {
        deleteMarkers();

        var marker, i, pinsData = data['locations'];

        if(pinsData.length < 1) {
            map.setCenter(initLatLng);
            return;
        }

        bounds = new google.maps.LatLngBounds();

        for (i = 0; i < pinsData.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(pinsData[i].lat, pinsData[i].lon),
                icon: 'http://cdn.seizethemarket.com/assets/images/map-house-marker-green.png',//(locations[i].status == 'Sold') ? 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png' : 'http://cdn.seizethemarket.com/assets/images/map-house-marker-green.png',
                map: map
            });
            markers.push(marker);
            bounds.extend(marker.getPosition());

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(pinsData[i].mapPinContent);
                    infowindow.open(map, marker);
                    highlightSelected(pinsData[i]);
                }
            })(marker, i));

            makeListing(pinsData[i]);
        }

        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);

        /*$(".totalHomes").text(data['total']);
        $(".currentPosition").text((250*(parseInt(data['page'])+1)));
        */
        if(pinsData.length == 250) {
            //$("#maxListingsReached").removeClass('ng-hide');
           // $("#listingsFound").addClass('ng-hide');
            //makePagination(data)
        } else {
            //$("#maxListingsReached").addClass('ng-hide');
            //$("#listingsFound").removeClass('ng-hide');
        }

        makePagination(data)

        $('.add-fav').on('click', function(e) {
            var listingId = $(this).attr('data-listingid');
            var mlsBoardId = $(this).attr('data-mbid');
            var contact_id = $(this).attr('data-contact-id');

            if($(this).hasClass('active')) {
                $(this).removeClass("active");
                $.post('/myAccount/apiSavedHomes', { contact_id: contact_id, listing_id: listingId, mls_board_id: mlsBoardId, action: 'remove' }, function(data) {
                    //results....
                });

                $('.saved-count').html($('.saved-count').html()-1);
            } else {
                $(this).addClass("active");
                $.post('/myAccount/apiSavedHomes', { contact_id: contact_id, listing_id: listingId, mls_board_id: mlsBoardId, action: 'add' }, function(data) {
                    //results....
                });

                $('.saved-count').html($('.saved-count').html()*1+1);
            }
        });
    }

    function makePagination(data) {

        var pages = parseInt(data['page_count']), paginationHtml = '<li class="page"><a href="javascript:void(0);" title="Grid" class="grid-view" id="goToGrid"><i class="fa fa-th-large"></i></a></li>'+
            '<li class="page"><a href="javascript:void(0);" title="List" class="list-view" id="goToList"><i class="fa fa-th-list"></i></a></li>', currPage = (parseInt(data['page']) + 1), maxPages = 10;

        var i = 1;
        if(currPage >= 10) {
            i = currPage - Math.ceil(maxPages / 2); // 5
        }

        paginationHtml += '<li class="page hidden-xs"><a>'+((currPage == 1) ? "1" : (250*(parseInt(data['page'])))+1)+'-'+((currPage == 1) ? ((data['total'] < 250) ? data['total'] : "250") : (250*(parseInt(data['page'])+1)))+' of '+data['total']+' Results</a></li>';

        if(pages > 1) {
            if(currPage != 1) paginationHtml += '<li class="page previous"><a href="javascript:void(0);" data-prev="true" data-page="'+(currPage -1)+'" class="pageNav"><i class="fa fa-angle-left ng-hide"></i>&laquo; Prev</a></li>';
            paginationHtml += '<li class="page next"><a href="javascript:void(0);" data-page="' + (currPage+1) + '" data-next="true" class="pageNav">Next &raquo;<i class="fa fa-angle-right ng-hide"></i></a></li>'; // (currPage+1) for k
        }

        /*for( var j = 0; j<maxPages;j++) {

            var k = i+j;

            if(j == (maxPages-1)) {
                if(pages > (currPage+Math.ceil(maxPages / 2))) {
                    paginationHtml += '<li class="page next"><a href="javascript:void(0);" data-page="' + (currPage+1) + '" data-next="true" class="pageNav">Next &raquo;<i class="fa fa-angle-right ng-hide"></i></a></li>'; // (currPage+1) for k
                }
                break;
            }

            if(j == 0 && k == 1) {
                paginationHtml += '<li class="page previous hidden"><a href="javascript:void(0);" data-prev="true" class="pageNav"><i class="fa fa-angle-left ng-hide"></i>&laquo; Prev</a></li>';
                if(k == currPage) {
                    paginationHtml += '<li class="page active"><a href="javascript:void(0);" data-page="' + k + '" class="pageNav hidden-xs">' + k + '</a></li>';
                } else {
                    paginationHtml += '<li class="page"><a href="javascript:void(0);" data-page="' + k + '" class="pageNav hidden-xs">' + k + '</a></li>';
                }
                continue;
            }

            if(j == 0 && k != 1) {
                paginationHtml += '<li class="page previous"><a href="javascript:void(0);" data-prev="true" data-page="'+(currPage -1)+'" class="pageNav"><i class="fa fa-angle-left ng-hide"></i>&laquo; Prev</a></li>'; // (currPage+1) for k
                paginationHtml += '<li class="page"><a href="javascript:void(0);" data-page="' + k + '" class="pageNav hidden-xs">' + k + '</a></li>';
                continue;
            }

            if(k == currPage) {
                paginationHtml += '<li class="page active"><a href="javascript:void(0);" data-page="' + k + '" class="pageNav hidden-xs">' + k + '</a></li>';
            } else {
                paginationHtml += '<li class="page"><a href="javascript:void(0);" data-page="' + k + '" class="pageNav hidden-xs">' + k + '</a></li>';
            }
        }*/

        $("#mapPager").html(paginationHtml);
        
        $(".pageNav").click(function() {
            //if($(this).attr('data-first')) {
                $("#MlsProperties_pager").val($(this).attr('data-page'));
                $("#map-search").submit();
            //}
        });

        $("#goToGrid, #goToList").click(function(e) {

            if(typeof _mapToGLurl == 'undefined' || _mapToGLurl == null || _mapToGLurl.length < 2) {
                e.preventDefault();
                alert("Please try again.");
                return;
            }

            $("body").prepend("<div class='loading-container loading'><em></em></div>");

            if($(this).attr('id') == 'goToGrid') {
                window.location.href = _mapToGLurl+'view/grid/';
            } else {
                window.location.href = _mapToGLurl+'view/list/';
            }
        });
    }

    var selectedShape, drawingManager, map_draw, polygon_created, entity_group, is_drawing = false, verticies = false;

    function setSelection(shape) {
        deleteSelectedShape();
        selectedShape = shape;
        //shape.setEditable(true);
        //selectColor(shape.get('fillColor') || shape.get('strokeColor'));
    }

    function deleteSelectedShape() {
        if (selectedShape) {
            selectedShape.setMap(null);
            selectedShape = null
        }
    }

    function enable_map_draw() {
        if (!is_drawing) {
            $("#map_remove").show();
            $("#map_cancel").show();

            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);

            is_drawing = true;
        }
    }

    function remove_drawing(){
        is_drawing = false;
        deleteSelectedShape();
        drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
        verticies = false;
        $("#map-search").submit();
    }

    function cancel_drawing() {
        is_drawing = false;
        $("#map_remove").hide();
        $("#map_cancel").hide();
        drawingManager.setDrawingMode(null);
        deleteSelectedShape();
    }

    function highlightSelected(location) {
        $("div[id*='location_']").css('box-shadow', 'none');

        var parentOffset = $('#listings').offset();
        var elmToScrollTo = $('#location_' + location['listing_id']);
        var offSet = elmToScrollTo.offset();

        console.log(parentOffset);
        console.log(offSet);
        console.log(elmToScrollTo.position().top);
        console.log(elmToScrollTo.get(0).offsetTop);

        // custom fn fn.scrollTo(selector, speed [in ms], topOffset [adds spacing at top of scroll so you dont go directly to the selector top border])
        $('#listings').scrollTo('#location_' + location['listing_id'], 1000, 50);

        //elmToScrollTo.css('outline', '3px solid #990000').css('border-radius', '5px');
        elmToScrollTo.css('box-shadow', '0 0 0 4px #C20000');
    }

    function makeListing(location) {
        var html =
            '<div class="item col-lg-3 col-md-6 col-sm-12 col-xs-12 no-margin no-padding">' +
                '<div class="product">'+
                    '<div class="description" id="location_'+location['listing_id']+'">' +
                        '<div class="grid-description">' +

                            '<div class="add-fav-container">' +
                                '<a data-placement="right" class="add-fav'+(location['isSaved'] == 'true' || location['isSaved'] == true ? " active " : "")+'" data-listingid="'+location['listing_id']+'">' +
                                    '<i class="glyphicon glyphicon-heart"></i>' +
                                '</a>' +
                            '</div>' +

                            '<div class="image">' +
                                '<div class="quickview">' +
                                    '<a href="'+location['url']+'" title="Quick View" class="btn btn-xs  btn-quickview"> View Photos &amp; Details</a>' +
                                '</div>' +
                                '<a href="'+location['url']+'">' +
                                    '<img class="img-responsive home-list-photo" alt="img" src="'+location['imgSrc']+'">' +
                                '</a>' +
                            '</div>' +

                            '<div class="address">'+location['street']+'</div>' +
                            '<div class="neighborhood">'+location['city']+'</div>' +

                            '<div class="specs-container row-fluid no-gutters">' +
                                '<div class="specs beds col-lg-3 col-md-3 col-sm-3 col-xs-3 no-gutters"><span>'+location['beds']+' BD</span></div>' +
                                '<div class="specs bath col-lg-3 col-md-3 col-sm-3 col-xs-3 no-gutters"><span>'+location['baths']+' BA</span></div>' +
                                '<div class="specs price col-lg-6 col-md-6 col-sm-6 col-xs-6 no-gutters"><span>'+location['price']+'</span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="disclosure"></div>' +
                    '</div>' +
                '</div>' +
            '</div>';

        $('#listings').append(html);
    }

    var fromForm = false;
    var _mapToGLurl = "/homes/";
</script>

<?php

if(!empty($_GET)) {?>
<script>
var fromForm = true;
</script>
<?php
}

$js = <<<JS
$.fn.scrollTo = function(elem, speed, topOffset) {
    $(this).animate({
        scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top - (topOffset == undefined ? 0 : topOffset)
    }, speed == undefined ? 1000 : speed);
    return this;
};


$(document).ready(function() {

    if(fromForm) {
        $("#map-search").submit();
        return;
    }

    $("body").prepend("<div class='loading-container loading'><em></em></div>");

    $.ajax({
        type: 'GET',
        url: '/homes/',
        dataType: "json",
        data: [{'name': 'ajax', 'value': 'true'}],
        success: function (data) {
            makePins(data);
            $('.loading-container').remove();
        }
    });
});

JS;

Yii::app()->clientScript->registerScript('initMapCall', $js);
?>

<script src="https://maps.google.com/maps/api/js?v=3&libraries=places,drawing,geometry&key=AIzaSyDjhLLd1T4xTZ031g87p_xCyQloz-dbW-Q&callback=initMap" async defer></script>