<?php
    $host = $_SERVER['SERVER_NAME'];
    $uri = $_SERVER['REQUEST_URI'];
	Yii::app()->clientScript->registerScript('mapSearchFormJs', <<<JS
	$('#map-search').change(function() {
	    //$(this).submit();
    });

	$('#map-search').submit(function() {
        var params = "";

        if($('#MlsProperties_price_min').val() != "" && $('#MlsProperties_price_min').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "price_min=" + $('#MlsProperties_price_min').val();
        }

        if($('#MlsProperties_price_max').val() != "" && $('#MlsProperties_price_max').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "price_max=" + $('#MlsProperties_price_max').val();
        }

        if($('#MlsProperties_bedrooms').val() != "" && $('#MlsProperties_bedrooms').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "bedrooms=" + $('#MlsProperties_bedrooms').val();
        }

        if($('#MlsProperties_baths_total').val() != "" && $('#MlsProperties_baths_total').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "baths_total=" + $('#MlsProperties_baths_total').val();
        }

        if($('#MlsProperties_sq_feet').val() != "" && $('#MlsProperties_sq_feet').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "sq_feet=" + $('#MlsProperties_sq_feet').val();
        }

        if($('#MlsProperties_property_type').val() != "" && $('#MlsProperties_property_type').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "property_type=" + $('#MlsProperties_property_type').val();
        }

        // this is the Featured Areas version of fields - this is a fast hotfix, need better solution
        if($('#FeaturedAreas_price_min').val() != "" && $('#FeaturedAreas_price_min').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "price_min=" + $('#FeaturedAreas_price_min').val();
        }

        if($('#FeaturedAreas_price_max').val() != "" && $('#FeaturedAreas_price_max').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "price_max=" + $('#FeaturedAreas_price_max').val();
        }

        if($('#FeaturedAreas_bedrooms').val() != "" && $('#FeaturedAreas_bedrooms').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "bedrooms=" + $('#FeaturedAreas_bedrooms').val();
        }

        if($('#FeaturedAreas_baths_total').val() != "" && $('#FeaturedAreas_baths_total').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "baths_total=" + $('#FeaturedAreas_baths_total').val();
        }

        if($('#FeaturedAreas_sq_feet').val() != "" && $('#FeaturedAreas_sq_feet').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "sq_feet=" + $('#FeaturedAreas_sq_feet').val();
        }

        if($('#FeaturedAreas_property_type').val() != "" && $('#FeaturedAreas_property_type').val() != undefined) {
            params += (params != "") ? "&": "";
            params += "property_type=" + $('#FeaturedAreas_property_type').val();
        }

		$(document).ready(function() {
			$.ajax({
				type: 'POST',
				url: '/mapsearch/search',
				data: params,
				dataType: "json",
				//contentType: 'application/json',
				//processData: false,
				success: function (data) { 
					makePins(data);
				}
			});
		});

		return false;
	});

	$('#FeaturedAreas_foreclosure').click(function() {
	    $('#FeaturedAreas_short_sale').prop('checked', false);
	});

	$('#FeaturedAreas_short_sale').click(function() {
	    $('#FeaturedAreas_foreclosure').prop('checked', false);
	});

	$('#more-search-options').click(function() {
		if ($('button#more-search-options em').attr('class') == 'icon i_stm_add') {
			$('button#more-search-options').html('<em class=\"icon i_stm_delete\"></em>Hide Search Options');
		} else {
			$('button#more-search-options').html('<em class=\"icon i_stm_add\"></em>More Search Options');
		}

		$('.container-3').slideToggle('normal', function() {
			// Clear values in container-3
		});
	});
JS
);

	$form = $this->beginWidget('CActiveForm', array(
			'method' => 'post',
			'id' => 'map-search',
		)
	); ?>
	<div class="container-1">
		<?php echo $form->dropDownList($model, 'price_min', StmFormHelper::getPriceList(), $htmlOptions = array(
				'empty' => 'Price Min',
				'class' => 'price-min'
			)
		); ?>
		<?php echo $form->dropDownList($model, 'price_max', StmFormHelper::getPriceList(), $htmlOptions = array(
				'empty' => 'Price Max',
				'class' => 'price-max'
			)
		); ?>
		<?php echo $form->dropDownList($model, 'bedrooms', StmFormHelper::getBedroomList(), $htmlOptions = array(
				'empty' => 'Bedrooms',
				'class' => 'bedrooms'
			)
		); ?>
		<?php echo $form->dropDownList($model, 'baths_total', StmFormHelper::getBathList(), $htmlOptions = array(
				'empty' => 'Baths',
				'class' => 'baths'
			)
		); ?>
		<?php echo $form->dropDownList($model, 'sq_feet', StmFormHelper::getSqFeetList(), $htmlOptions = array(
				'empty' => 'Sq. Feet',
				'class' => 'sq-feet'
			)
		); ?>
	</div>
	<div class="container-2">
        <?php echo $form->dropDownList($model, 'property_type', CHtml::listData(MlsPropertyTypeBoardLu::model()->findAllByAttributes(array('status_ma'=>1,'mls_board_id'=>Yii::app()->user->boardId)),'mls_property_type_id','displayName'), $htmlOptions = array(
                'empty' => 'Property Types',
                //'style' => 'width: 250px;',
            )
        ); ?>
        <div class="g4 p-fr">
            <a href="/values" class="text-button" style="display: inline-block; font-size: 12px !important; text-decoration: none;"><em class="icon i_stm_dollar"></em>Free House Values Report</a>
        </div>
        <div class="g3 p-fr">
            <a href="/search" class="text-button" style="display: inline-block; font-size: 12px !important; text-decoration: none;"><em class="icon i_stm_search"></em>Advanced Search</a>
        </div>
<!--		--><?php //echo $form->dropDownList($model, 'year_built', StmFormHelper::getYrBuiltList(), $htmlOptions = array(
//				'empty' => 'Yr Built',
//				'class' => 'yr-built',
//				'style' => 'margin-left:0;'
//			)
//		); ?>
<!--		--><?php //echo $form->checkBox($model, 'pool', false, array()); ?><!--Pool-->
<!--		--><?php //echo $form->checkBox($model, 'new_construction', false, array()); ?><!--New Construction-->
<!--		--><?php //echo $form->checkBox($model, 'navig_waterfront', false, array()); ?><!--Navig Waterfront-->
		<?//=$form->checkBox($model_2,'nvgble_to_ocean', false, array());?><!-- Waterfront -->
<!--		--><?php //echo $form->checkBox($model, 'foreclosure', false, array()); ?><!--Foreclosures-->
<!--		--><?php //echo $form->checkBox($model, 'short_sale', false, array()); ?><!--Short Sale-->
<!--		--><?php //echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?>
	</div>
	<div class="container-3">
		<button id="more-search-options" class="text">
			<em class="icon i_stm_add"></em>More Search Options
		</button>
		<?//=$form->checkBox($model,'listing_id', false, array());?><!-- Oceanfront -->
		<?//=$form->checkBox($model,'listing_id', false, array());?><!-- Golf Community -->
		<?//=$form->checkBox($model_2,'gated_community_yn', false, array());?><!-- Gated -->
	</div>
<?php $this->endWidget();