<?php

$this->widget('admin_module.extensions.EChosen.EChosen', array(
    'target' => 'select#MlsProperties_county, select#MlsProperties_city, select#MlsProperties_neighborhood, select#MlsProperties_zip, select#MlsPropertiesSecondary_school, select#MlsPropertiesSecondary_masterbr_room_level',
    'options'=>array('allow_single_deselect'=>true)));

$js = <<<JS
        $(document).ready(function () {
            $("#MlsProperties_neighborhood").ajaxChosen({
                type: 'POST',
                url: '/searchAutocompleteNeighborhood',
                dataType: 'json'
            }, function (data) {
                return data;
            });
        });
JS;

Yii::app()->clientScript->registerScript('search-autocomplete-script', $js);

$form = $this->beginWidget('CActiveForm', array(
        'id' => 'search-form',
));
?>
    <style type="text/css">
        .chzn-container.chzn-container-single a {
            height: 25px;
            padding: 7px;
        }
        .chzn-container.chzn-container-single {
            font-size: 16px;
        }
        .chzn-container-single .chzn-single div b{
            top: 8px;
            position: relative;
        }
        .search-choice-close {
            top: 14px !important;
        }
    </style>
	<div style="width:100%; float:left;" class="full">
		<h1 style="width:540px;">Full Search</h1>
		<img src="http://cdn.seizethemarket.com/assets/images/search_magnifying_glass.png" width="170px;" style="right:10px; top:17px; position: absolute;">
		<hr/>
		<div>
			<div class="p-pb10">
				<?php echo $form->dropDownList($property, 'price_min', StmFormHelper::getPriceList(), $htmlOptions = array(
						'empty' => 'Price Min',
						'class' => 'price-min'
					)
				); ?>
				<?php echo $form->dropDownList($property, 'price_max', StmFormHelper::getPriceList(), $htmlOptions = array(
						'empty' => 'Price Max',
						'class' => 'price-max'
					)
				); ?>
				<?php echo $form->dropDownList($property, 'bedrooms', StmFormHelper::getBedroomList(), $htmlOptions = array(
						'empty' => 'Bedrooms',
						'class' => 'bedrooms'
					)
				); ?>
				<?php echo $form->dropDownList($property, 'baths_total', StmFormHelper::getBathList(), $htmlOptions = array(
						'empty' => 'Baths',
						'class' => 'baths'
					)
				); ?>
			</div>
			<div class="p-pb10">
				<?php echo $form->dropDownList($property, 'sq_feet', StmFormHelper::getSqFeetList(), $htmlOptions = array(
						'empty' => 'Sq. Feet',
						'class' => 'sq-feet'
					)
				); ?>
				<?php echo $form->dropDownList($property, 'year_built', StmFormHelper::getYrBuiltList(), $htmlOptions = array(
						'empty' => 'Yr Built',
						'class' => 'yr-built'
					)
				); ?>
                <?php $property->mls_property_type_id = ($property->mls_property_type_id) ? $property->mls_property_type_id : 1;
                    $mlsPropertyTypeListData = CHtml::listData(MlsPropertyTypeBoardLu::model()->with('propertyType')->findAllByAttributes(array(
                        'mls_board_id' => Yii::app()->getUser()->getBoard()->id,
                        'status_ma'=>MlsPropertyTypeBoardLu::ACTIVE,
                    )), 'mls_property_type_id', 'displayName');
                    echo $form->dropDownList($property, 'property_type', $mlsPropertyTypeListData, $htmlOptions = array(
                        'empty' => 'Property Type',
                        'class' => 'yr-built'
                    )
                ); ?>
			</div>
            <?php $this->renderPartial('_searchFullExtraFields', array('form' => $form, 'property' => $property)); ?>
                <div class="p-tl p-clr g100 left" style="display:inline-block;padding-top:15px;">
                    <div class="g12">
                        Keywords:<br/>
                        <?php echo $form->textField($property, 'keywords', $htmlOptions = array('class' => '','placeholder'=>'Keyword, MLS #, Neighborhood, City, Street Name, Zip')); ?>
                    </div>
                </div>
                <div class="g6">
                    Neighborhood:<br/>
                    <?php echo $form->dropDownList($property, 'neighborhood', array(), $htmlOptions = array(
                            'data-placeholder'=>'Type a Neighborhood...',
                            'empty'=>'',
                            'style' => 'width:98%;',
                        )); ?>
                </div>
                <div class="g3">
                    County:<br/>
                    <?php echo $form->dropDownList($property, 'county', (array) MlsPropertyFormHelper::getInstance()->getCountyOptions(), $htmlOptions = array(
                            'data-placeholder'=>'Select or Type a County...',
                            'empty'=>'',
                            'style' => 'width:99%;',
                        )); ?>
                </div>
                <div class="g3">
                    School:<br />
                    <?php echo $form->dropDownList($secondaryProperty, 'school', (array) MlsPropertyFormHelper::getInstance()->getSchoolOptions(), $htmlOptions = array(
                            'data-placeholder'=>'Select or Type a School...',
                            'empty'=>'',
                            'style' => 'width:98%;',
                        )); ?>
                </div>
                <div class="g3"></div>
			</div>
			<div class="p-tl p-clr g100 left" style="display:inline-block;padding-top:15px;">
				<div class="g2 p-pr3">
					Street #:<br/>
					<?php echo $form->textField($property, 'street_number', $htmlOptions = array('class' => 'g97')); ?>
				</div>
				<div class="g3">
					Street Name:<br/>
					<?php echo $form->textField($property, 'street_name', $htmlOptions = array('class' => 'g97')); ?>
				</div>
				<div class="g2 p-pr3">
					(Rd, St, Blvd)<br/>
					<?php echo $form->textField($property, 'street_suffix', $htmlOptions = array('class' => 'g97')); ?>
				</div>
				<div class="g3">
					City:<br/>
                    <?php echo $form->dropDownList($property, 'city', (array) MlsPropertyFormHelper::getInstance()->getCityOptions(), $htmlOptions = array(
                            'data-placeholder'=>'Type a City...',
                            'empty'=>'',
                            'style' => 'width:99%;',
                        )); ?>
				</div>
				<div class="g2">
					Zip:<br/>
					<?php echo $form->dropDownList($property, 'zip', (array) MlsPropertyFormHelper::getInstance()->getZipOptions(), $htmlOptions = array(
                            'data-placeholder'=>'Select or Type a Zip Code...',
                            'empty'=>'',
                            'style' => 'width:100%;',
                        )); ?>
                </div>
			</div>

            <? //TOTAL hack but temp fix for now as we will re-do front site soon + need some time to think of better scalable design
            if(strpos($_SERVER['SERVER_NAME'], 'myashevillerealestate.com') !== false): ?>

                <div class="p-tl p-clr g100 left" style="display:inline-block;padding-top:15px;">
                    <div class="g5">
                        Master Bedroom Floor Level:<br/>
                        <?php echo $form->dropDownList($secondaryProperty, 'masterbr_room_level', array(1=>'Master Bedroom on 1st Floor', 2=>'Master Bedroom on 2nd Floor', 3=>'Master Bedroom on 3rd Floor'), $htmlOptions = array(
                                'data-placeholder'=>'Select a Floor Level',
                                'empty'=>'',
                                'style' => 'width:97%;',
                            )); ?>
                    </div>
                </div>

            <? endif; ?>

			<div class="g12" style="text-align:center;">
				<input class="button" name="LoginForm[email]" id="LoginForm_email" type="submit" value="Search Now" style="width:150px;top:-4px;margin-top:20px;">
			</div>
		</div>
<?php $this->endWidget(); ?>