<?php
	$contactId = Yii::app()->getUser()->id;
	$listingId = Yii::app()->getRequest()->getQuery('id');

	$jsSavedHomesList = <<<JS
	$('.add-favorites-button').live('click', addFavorites);
	$('.remove-favorites-button').live('click', removeFavorites);

	function removeFavorites() {
		var listingId = $(this).attr('data');
		var mlsBoardId = $(this).attr('data-mbid');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, mls_board_id: mlsBoardId, action: 'remove' }, function(data) {
			//results....
		});

		$(this).removeClass('remove-favorites-button').addClass('add-favorites-button');
		$('.saved-count').html($('.saved-count').html()-1);

		// $('#add-favorites-button').html($('#add-favorites-button').html().replace('i_stm_delete','i_stm_star'));
		// $('#add-favorites-button').removeClass('remove-favorites').addClass('add-favorites');
		// $('#favorite-container').removeClass('remove-favorites').addClass('add-favorites');
	}

	function addFavorites() {
		var listingId = $(this).attr('data');
		var mlsBoardId = $(this).attr('data-mbid');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, mls_board_id: mlsBoardId, action: 'add' }, function(data) {
			//results....
		});

		$(this).removeClass('add-favorites-button').addClass('remove-favorites-button');
		$('.saved-count').html($('.saved-count').html()*1+1);

		// $(this).html().replace('remove-favorites-button','add-favorites-button');
		// $('#add-favorites-button').html($('#add-favorites-button').html().replace('Add to Favorites', 'Remove from Favorites'));
		// $('#add-favorites-button').html($('#add-favorites-button').html().replace('i_stm_star','i_stm_delete'));
		// $('#add-favorites-button').removeClass('add-favorites').addClass('remove-favorites');
		// $('#favorite-container').html($('#favorite-container').html().replace('i_stm_star_large_empty','i_stm_star_large'));
		// $('#favorite-container').removeClass('add-favorites').addClass('remove-favorites');
	}
JS;

	Yii::app()->getClientScript()->registerScript('savedHomesListJs', $jsSavedHomesList);
?>

<div id="home-list-view">
	<!-- <iframe id="map" scrolling="no" width="690" height="360" frameborder="0" src="http://www.bing.com/maps/embed/?v=2&amp;cp=30.184597~-81.630630&amp;lvl=15&amp;dir=0&amp;sty=r&amp;where1=2974%20Hartley%20Rd%2C%20Jacksonville%2C%20FL%2032257&amp;form=LMLTEW&amp;pp=30.18459701538086~-81.63063049316406&amp;emid=8c5f61b2-4fc1-9a5e-f053-146eac8ff048"></iframe>
	 -->
	<!-- 	<iframe width="690" height="360" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=1235 Terrace Oaks Ln, Jacksonville, FL 32223&amp;t=p&amp;z=14&amp;output=embed"></iframe>
	 -->
	<div id="home-listview-search" class="g99 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox', array(
				'model' => $model,
			)
		);
		?>
	</div>
	<?php

		$this->widget('front_module.components.StmListView', array(
				'dataProvider' => $DataProvider,
				'template' => '{pager}{summary}{items}{pager}{summary}',
				'id' => 'home-list',
				'ajaxUpdate' => false,
				'itemView' => '_homeListRow',
				'itemsCssClass' => 'datatables',
                'afterAjaxUpdate' => 'js:function(id,data) {
		                                            $(\'#home-list\').replaceWith($(\'#home-list\', \'<div>\'+data+\'</div>\'));
		                                            $(\'div.loading-container.loading\').remove();
		                                        }',
			)
		);
	?>
</div>
