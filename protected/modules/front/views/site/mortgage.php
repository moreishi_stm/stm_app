<style>
    h1 {
        margin-top: 20px;
    }
    /*#mortgage-tips {*/
        /*margin-top: 20px;*/
        /*margin-left: 20px;*/

    /*}*/
    /*#mortgage-tips ol{*/
        /*margin-left: 20px;*/
    /*}*/


    #pre-qualify-form {
        margin-top: 20px;
        display: block;
    }
    #pre-qualify-form p {margin: 20px 0;}
    #pre-qualify-form input,#pre-qualify-form textarea {margin-top: 12px;}
    #pre-qualify-form #FormSubmissionValues_data_1, #pre-qualify-form #FormSubmissionValues_data_3 { margin-right: 10px;}
    #pre-qualify-form #prequal-submit-button { width: 90%;}
    #pre-qualify-form #FormSubmissionValues_data_7 { width: 87%; font-family: verdana, arial, helvetica, sans-serif; font-size: 13px;}

    #thank-you-message {
        margin-top: 20px;
        background-color: #a0ffa0;
        text-align: justify;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        padding: 20px 10px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

        -moz-box-shadow:    0px 2px 2px 0px #ccc;
        -webkit-box-shadow: 0px 2px 2px 0px #ccc;
        box-shadow:         0px 2px 2px 0px #ccc;
    }
    .searchall {
        font-size:30px;
        font-weight: bold;
        color: black;
        clear: both;
        display: inline-block;
        text-decoration: none;
        background-color: #a9bbed;
        padding: 30px 40px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

        -moz-box-shadow:    0px 2px 2px 0px #ccc;
        -webkit-box-shadow: 0px 2px 2px 0px #ccc;
        box-shadow:         0px 2px 2px 0px #ccc;
    }
    .searchall:hover {
        /*color: #D20000;*/
        text-decoration: underline;
        background-color: yellow;
    }
    .lender-container {
        padding: 30px 40px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

        -moz-box-shadow:    0px 2px 2px 0px #ccc;
        -webkit-box-shadow: 0px 2px 2px 0px #ccc;
        box-shadow:         0px 2px 2px 0px #ccc;
        background-color: #DCDCDC;
        margin-top: 30px;
    }
    .lender-container:hover {
        background-color: #d1f5b9;
    }
    .lender-photo {
        margin-left: -15px;
    }
	@media screen and (max-width: 660px) {
		#pre-qualify-form #FormSubmissionValues_data_7 {
			width: 98%;
		}
	}
</style>
<h1>Mortgages & Lenders</h1>

<!--<div id="mortgage-tips">-->
<!--    <h2>Money Saving Mortgage Tips</h2>-->
<!--    <ol>-->
<!--        <li><h3>Bi-Weekly Payments = 6 years</h3>-->
<!--            <div>You can request from your bank to get on a bi-weekly payment schedule. Depending on the interest rate, it can result in paying off your loan 6 years earlier! The power of compounding interest.</div>-->
<!--        </li>-->
<!--        <li><h3>Common Loan Documents</h3>-->
<!--            <div>When getting a loan be prepared to provide 30 days of pay stubs, last 2 W-2's, 2 months of statements for bank account or any other assets. If you are self-employed or the majority of your income is from commission, 2 years of full tax returns will be required.</div>-->
<!--        </li>-->
<!--        <li><h3>Pre-Approval vs. Pre-Qualifying</h3>-->
<!--            <div>There is a big difference between pre-approval vs. pre-qualifying. It's the same difference between warming up for a jog (pre-qualifying) and being on the home stretch of a marathon (pre-approval). A Pre-Approval means you have a real approval from the lender after you've supplied them with all your documentation. This makes you much more valuable as a buyer which means in a multiple offer situation, this gives you an advantage over other buyers. Our lenders can help you get pre-approved and ready to buy!</div>-->
<!--        </li>-->
<!--        <li><h3>1 Extra Payment per Year = 8 years!</h3>-->
<!--            <div>When getting a loan be prepared to provide 30 days of pay stubs, last 2 W-2's, 2 months of statements for bank account or any other assets. If you are self-employed or the majority of your income is from commission, 2 years of full tax returns will be required.</div>-->
<!--        </li>-->
<!--        <li><h3>Competition = Great Rates</h3>-->
<!--            <div>When getting a loan be prepared to provide 30 days of pay stubs, last 2 W-2's, 2 months of statements for bank account or any other assets. If you are self-employed or the majority of your income is from commission, 2 years of full tax returns will be required.</div>-->
<!--        </li>-->
<!--    </ol>-->
<!--</div>-->

<?php $form = $this->beginWidget('CActiveForm', array(
        'id' =>'pre-qualify-form',
        'action' => array('/front/forms/register/formId/' . $formId),
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnChange' => false,
            'validateOnSubmit' => true,
            'inputContainer' => 'span',
            'beforeValidate' => 'js:function(form) {

                var firstNameInput = $("#FormSubmissionValues_data_1");
                var firstName = $("#FormSubmissionValues_data_1").val();

                var lastNameInput = $("#FormSubmissionValues_data_2");
                var lastName = $("#FormSubmissionValues_data_2").val();

                var emailInput = $("#FormSubmissionValues_data_3");
                var email = $("#FormSubmissionValues_data_3").val();

                var phoneInput = $("#FormSubmissionValues_data_4");
                var phone = $("#FormSubmissionValues_data_4").val();

                if(firstName == "" || lastName == "") {
                    alert("Please enter your Full Name.");
                    if(firstName == "") {
                        firstNameInput.focus();
                        return false;
                    }

                    if(lastName == "") {
                        lastNameInput.focus();
                        return false;
                    }

                }

                if(firstName.length < 2 || lastName.length < 2) {
                    alert("Please enter a valid Full Name.");
                    return false;
                }

                if(firstName == lastName) {
                    alert("Please double check your First & Last Name");
                    return false;
                }

                if(email=="" || !validateEmail(email)) {
                    alert("Please enter a valid Email.");
                    emailInput.focus();
                    return false;
                }

                if(phone=="" || phone.length < 10 || !validatePhone(phone)) {
                    alert("Please enter a valid Phone #.");
                    phoneInput.focus();
                    return false;
                } else if (phone.length >9) {

                    var badPhones = ["555-1212","111-1111","222-2222","333-3333","444-4444","555-5555","666-6666","777-7777","888-8888","999-9999","555-"];

                    $(badPhones).each(function(i, badPhone) {
                        if(phone.indexOf(badPhone) > -1) {
                            alert("Please enter a valid Phone #.");
                            return false;
                        }
                    });
               }

                $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");

                return true;

                function validateEmail(email) {
                   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                   if(reg.test(email) == false) {
                      return false;
                   }
                   return true;
                }

                function validatePhone(phone) {
                   var reg = /^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/;
                   if(reg.test(phone) == false) {
                      return false;
                   }
                   return true;
                }
            }',
            'afterValidate' => 'js:function(form, data, hasErrors) {
                $("div.loading-container.loading").remove();
                // If marked as spam then just refresh the current page
				if (data.isSpam) {
                    location.reload();
                    return;
				}

                if (!hasErrors) {
                    // Action was successful
					if(data.resubmit==true) {
						window.location = "/login?email="+data.email+"&resubmit=1";
					} else {
                        Message.create("success", "Congratulations! We have started getting your best rates.");
                        $("#form-fields").hide();
                        $("#thank-you-message").show("normal");
					}

                    return false;
                } else {
                    $("div.loading-container.loading").remove();
					$("#register-dialog-submit-button").prop("disabled", false);
					Message.create("error", "Please fill out the required fields and submit.");
					return false;
                }
            }',
        ),
    ));
?>
    <div id="form-fields" class="g7">
        <p>Get the best rates and expert advice! Pre-qualify in 5 mins or less! Start your home search the right way. Don't waste your time on homes that don't meet your financial needs.</p>
        <?php echo $form->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
        <?php echo $form->error($model, 'data['. $FormFields->getField('first_name')->id .']'); ?>
        <?php echo $form->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
        <?php echo $form->error($model, 'data['. $FormFields->getField('last_name')->id .']'); ?>
        <?php echo $form->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'g5', ));?>
        <?php echo $form->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
        <?php $this->widget('StmMaskedTextField', array(
                'model' => $model,
                'attribute' => 'data['. $FormFields->getField('phone')->id .']',
                'mask' => '(999) 999-9999',
                'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'g5'),
            )); ?>
        <?php echo $form->error($model, 'data['. $FormFields->getField('phone')->id .']'); ?>
        <?php echo $form->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('placeholder'=>'Comment', 'class'=>'g10', ));?>
        <?php echo $form->error($model, 'data['. $FormFields->getField('comments')->id .']'); ?>
        <input id="prequal-submit-button" class="g12" type="submit" value="Get the Best Rates">
    </div>
    <div id="thank-you-message" class="g6" style="display:none;">
        <p>
        <h3>Thank You!</h3> We received your request for the best rates. <br /><br /> We have started the process and will touch base shortly with any questions. Feel free to call us at <strong><?php echo Yii::app()->user->settings->office_phone;?></strong> for immediate assistance. <br /><br />We look forward to helping you. Have a great day! =)<br /><br /><a href="/area/all">Click here to continue your Home Search.</a>
        </p>
    </div>
    <div class="g5">
        <img src="http://cdn.seizethemarket.com/assets/images/mortgage.jpg" alt="Mortgage Tips" />
    </div>
<?php $this->endWidget(); ?>
<div class="g12" style="margin-top: 30px;"></div>

<?php if(is_array($Lenders)) {
    $displayLenderHeader = true;
    foreach($Lenders as $Lender) {
        if($SettingValue->getValue('profile_on_website', $Lender->contact->id)->value) {
            if($displayLenderHeader) {
                echo '<hr style="padding-top:40px;"/>';
                echo '<h2 style="font-size: 30px; clear:both;">Preferred Lenders</h2>';
                $displayLenderHeader = false;
            }
?>
            <div class="lender-container g10">
                <div class="g3">
                    <?php echo ($photo = $SettingValue->getValue('profile_photo', $Lender->contact->id)->value)?'<img class="lender-photo" src="'.Yii::app()->user->getProfileImageBaseUrl.'/'.$Lender->contact->id.'/'.$photo.'" style="width: 100%;">' : ''; ?>
                </div>
                <div class="g9">
                    <div class="g6">
                        <?php echo ($company = $SettingValue->getValue('lender_company_name', $Lender->contact->id)->value)? $company : ''; ?><br/>
                        <?php echo ($address = $SettingValue->getValue('lender_address', $Lender->contact->id)->value)? $address : ''; ?><br />
                        <?php echo ($city = $SettingValue->getValue('lender_city', $Lender->contact->id)->value)? $city : ''; ?>
                        <?php echo ($stateId = $SettingValue->getValue('lender_state_id', $Lender->contact->id)->value)? AddressStates::getShortNameById($stateId).', ': ''; ?>
                        <?php echo ($zip = $SettingValue->getValue('lender_zip', $Lender->contact->id)->value)?$zip : ''; ?><br />
                        <a href="<?php echo ($website = $SettingValue->getValue('lender_website', $Lender->contact->id)->value)?$website : ''; ?>">Lender Website</a>
                    </div>
                    <div class="g6">
                        <?php echo $Lender->contact->fullName; ?><br />
                        <?php echo $SettingValue->getValue('lender_license_number', $Lender->contact->id)->value; ?><br/>
                        <?php echo ($officePhone = $SettingValue->getValue('lender_office_phone', $Lender->contact->id)->value)?$officePhone : ''; ?>
                        <?php echo ($directPhone = $SettingValue->getValue('lender_direct_phone', $Lender->contact->id)->value)?$directPhone : ''; ?>
                        <?php echo ($fax = $SettingValue->getValue('lender_fax', $Lender->contact->id)->value)?$fax : ''; ?>
                    </div>
                    <div class="g12">
                        <?php echo ($bio = $SettingValue->getValue('bio', $Lender->contact->id)->value)?'<hr /><h4>Personal Bio:</h4>'.$bio : ''; ?>
                    </div>
                </div>
            </div>
<?php
        }
    }
}
?>
<div class="g12" style="margin-top: 30px;"></div>

<hr style="padding-top:40px;"/>
<div>
    <a href="/homes" class="searchall g10" style="margin-bottom: 60px;">Search All Homes on the Market</a>
</div>