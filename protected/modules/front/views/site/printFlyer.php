<div id="home-flyer">
    <?php echo $this->renderPartial(
        '../site/homeDetails/_flyerHeader', array(
            'model' => $model,
            'urlPath' => $urlPath
        )
    ); ?>
    <h1 style="line-height: 1.2; display: inline-block;"><?php echo $model->formatMlsAddress(
            $model, array('lineBreak' => true)
        ) ?></h1>

    <h2><?php echo Yii::app()->format->formatProperCase($model['common_subdivision']) ?></h2>
    <?php echo $this->renderPartial('../site/homeDetails/_flyerTop', array('model' => $model)); ?>

    <hr class="line"/>
    <div class="middle-cta" style="font-size: 20px; padding: 15px 0;">Call <?php echo Yii::app()->getUser()
            ->getSettings()->office_phone; ?> with questions or to see this home!
    </div>
    <hr class="line"/>

    <?php echo $this->renderPartial('../site/homeDetails/_flyerFeatures', array('model' => $model)); ?>

    <hr class="line"/>

    <div class="footer" style="margin-top: 20px; line-height: 1.3">
        <?php
            $mlsOffice = $model->getOffice();
            if (!empty($mlsOffice)):
                $officeName = (isset($mlsOffice->name)) ? $mlsOffice->name : $mlsOffice->office_name;
        ?>
            Courtesy of <?php echo ucwords(strtolower($officeName)); ?>.
        <?php endif; ?>
        <?php $this->renderPartial('_mlsDisclosure'); ?>
    </div>
</div>

