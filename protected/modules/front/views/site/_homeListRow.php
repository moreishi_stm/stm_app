<div class="<?php echo ($index % 2) ? 'odd' : 'even' ?> row">
	<div class="column-photo p-pt5">
		<em class="icon_ribbon <?php echo $data->ribbonType;?>"></em>
        <div class="home-photo-container">
            <a class="home-photo" href="<?php echo $data->getUrl($relative=true); ?>"><img class="home-list-photo" src="<?php echo (!empty($data->firstPhotoUrl))? $data->firstPhotoUrl : $data->getPhotoUrl($photoNumber=1); ?>" width="360" height="240"></a>
        </div>
	</div>
	<div class="column info-2">
        <span class="price"><?php echo Yii::app()->format->formatDollars($data->price)?></span><br />
        <?php if (!empty($data->common_subdivision)) : ?>
        <?php echo Yii::app()->format->formatProperCase($data->common_subdivision)?><br />
        <?php endif; ?>
        <?php echo Yii::app()->format->formatProperCase($data->city)?>, <?php echo $data->getStateShortName($lowercase=false).' '.substr($data->zip,0,5)?><br />
		<?php if($data->year_built):
                echo $data->year_built?> Yr Built<br />
        <?php endif; ?>
	</div>
	<div class="column location">
        <?php if (!empty($data->bedrooms) and !empty($data->baths_total)) : ?>
            <?php echo $data->bedrooms?> BR / <?php echo $data->baths_total?> Baths<br />
        <?php endif; ?>
        <?php if(!empty($data->sq_feet)):?>
            <?php echo Yii::app()->format->formatNumber($data->sq_feet)?> SF<br />
        <?php endif; ?>


        <?php if(!empty($data->sq_feet)):?>
            <?php echo '($'.$data->pricePerSf.'/SF)' ?><br />
        <?php endif; ?>
        <?php if($data->mls_property_type_id == MlsPropertyTypes::LAND_ID) {
            echo 'Vacant Land<br />';
        }?>
        <?php echo ($data->pool_yn)? '<span class="water">Pool</span><br />': '';?>
        <?php if(!empty($data->photo_count)): ?>
            <a href="<?php echo $data->getUrl($relative=true); ?>" class="photos"><?php echo $data->photo_count?> Photos</a>
        <?php endif; ?>
	</div>
	<div class="column days column-last">
        <?php if($data->status_change_date>0 && Yii::app()->user->boardId != 2): //@todo: need to change the hard coding for ferraro ?>
            <?php echo Yii::app()->format->formatDays($data->status_change_date)?>
        <?php endif; ?>
	</div>
    <div class="buttons">
        <a href="<?php echo $data->getUrl($relative=true); ?>" class="view-details-button"></a>
        <a href="javascript:void(0)" class="<?php echo ($data->isSaved)? 'remove' : 'add';?>-favorites-button" data="<?php echo $data->listing_id?>" data-mbid="<?=$data->getMlsBoardId()?>"></a>
    </div>
	<div class="listing-office">
        MLS#: <?php echo $data->listing_id?> |
        <?=$this->renderPartial('_mlsDisclosureRow')?>
		Courtesy of <?php echo ucwords(strtolower($data->getListingOfficeName()))?>
	</div>
</div>