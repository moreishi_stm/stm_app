<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
)); ?>
	<div style="width:445px; float:left;">
		<h1 style="width:360px;">MLS Number Search</h1>
		<hr />
		<div>
            <div style="width:360px;">Enter MLS #:</div>
			<div class="p-tl" style="display:inline-block;padding-top:5px;">
    			<?php echo $form->textField($property, 'listing_id', $htmlOptions=array('style'=>'width:150px;')); ?>
                <?php echo CHtml::submitButton('Search', array('class'=>'button','style'=>'width:85px;top:-4px;')); ?>
                <br />
                <?php echo $form->error($property, 'listing_id', $htmlOptions=array('style'=>'text-align:left;')); ?>
			</div>
			<div style="display:inline-block;">
			</div>
		</div>
	</div>
	<img src="http://cdn.seizethemarket.com/assets/images/search_computer_sign.png" style="max-width:250px;" class="p-fr">
<?php $this->endWidget(); ?>