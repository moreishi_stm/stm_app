<div id="area">
	<h1 class="p-mt10">Market Update</h1>
	<h3 class="p-tc">Newest properties to oldest</h3>
	<div class="p-tc">Based on your Customized Saved Search. Contact us with questions, showings and updates.</div>
	<br />
	<?php
		$this->renderPartial('_homeList', array(
				'DataProvider' => $DataProvider,
				'model' => $model,
				'listSearchBox'=>false,
			)
		);
	?>
	<div id="description-marker" class="p-clr p-p10"></div>
</div>