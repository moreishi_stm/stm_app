<div id="homepage">
	<div class="buyColumn">
		<div class="colTop">
			<h2>Buy a Home</h2>
<br />
<!-- 			<form action="#" method="post" id="featuredSeaches">
				<legend><span>Select a home search below:</span></legend>
				<select name="featuredSearchSelector">
					<option value="topSchoolCommunities">Top School Communities</option>
					<option value="topSchoolCommunities">Golf Communities</option>
					<option value="topSchoolCommunities">New Construction</option>
					<option value="topSchoolCommunities">Waterfront Communities</option>
					<option value="topSchoolCommunities">Oceanfront Communities</option>
					<option value="topSchoolCommunities">Ponte Vedra Communities</option>
					<option value="topSchoolCommunities">Gated Communities</option>
					<option value="topSchoolCommunities">$200,000 - $300,000</option>
					<option value="topSchoolCommunities">$300,000 - $400,000</option>
					<option value="topSchoolCommunities">$400,000 - $500,000</option>
					<option value="topSchoolCommunities">$500,000 - $600,000</option>
					<option value="topSchoolCommunities">$600,000 - $700,000</option>
					<option value="topSchoolCommunities">$700,000 - $800,000</option>
					<option value="topSchoolCommunities">$800,000 - $900,000</option>
					<option value="topSchoolCommunities">$900,000 - $1,000,000</option>
					<option value="topSchoolCommunities">$1 - $2 Million</option>
					<option value="topSchoolCommunities">$2 - $3 Million</option>
					<option value="topSchoolCommunities">$4 Million &amp; Up</option>
				</select>
				<input type="submit" class="go" value="Go"/>
			</form>
 -->			<img class="buyVideo" src="images/videoBuyer.png" alt="Buy a Home!"/>
		</div>
		<h3>Buyer Tools:</h3>
		<ul>
	                <li><a href="/this-month-in-real-estate">How's the Market?!</a></li>
	                <li><a href="/relocation-guide">Relocation Guide</a></li>
	                <li><a href="/cdd-fees">CCD Fee</a></li>
	                <!-- <li><a href="#">Upfront Costs</a></li> -->
	                <li><a href="/first-time-homebuyer">First Time Homebuyer</a></li>
	                <li><a href="/free-money-for-buyers">Free Money for Buyers</a></li>
	                <li><a href="/myths-about-loans-and-lending">Loans &amp; Lending Myth</a></li>
<!-- 			<li><a href="#">Free Foreclosure List</a></li>
			<li><a href="#">Step by Step Home Buying Process</a></li>
			<li><a href="#">Top School Report</a></li>
			<li><a href="#">How to Be On HGTV's House Hunters</a></li>
			<li><a href="#">How to Find the Killer Deals</a></li>
			<li><a href="#">How Home Construction List</a></li>
 -->		</ul>
	</div>

	<div class="sellColumn">
		<div class="colTop">
			<h2>Sell a Home</h2>
<br />
<!-- 			<form action="#" method="post" id="sell">
				<legend><span>Find out what your home is worth:</span></legend>
				<input type="text" name="zip" class="zip" value="Zip Code"/>
				<input type="submit" class="go" value="Go"/>
			</form>
 -->			<img class="sellVideo" src="images/videoSeller.png" alt="Sell a Home!"/>
		</div>
		<h3>Seller Tools:</h3>
		<ul>
            <li><a href="/find-out-what-your-home-is-worth">What's My Home Worth?</a></li>
            <li><a href="/stop-foreclosures-with-a-short-sale">Short Sales</a></li>
            <li><a href="/the-truth-about-zillow-and-other-avms">Truth About Zillow</a></li>
            <li><a href="/hot-staging-tips-help-your-home-sell-itself">Hot Staging Tips</a></li>
            <li><a href="homes-that-sell-vs.-homes-that-sit">Homes that Sell vs. Homes that Sit</a></li>
            <li><a href="/stop-foreclosures-with-a-short-sale">Stop Foreclosure</a></li>
<!-- 			<li><a href="#">Free Market Reports</a></li>
			<li><a href="#">How to Stage Your Home to Sell</a></li>
			<li><a href="#">Step by Step Home Selling Process</a></li>
			<li><a href="#">What is Cuttin Edge Marketing?</a></li>
			<li><a href="#">Agent Comparison Worksheet</a></li>
			<li><a href="#">How Teamwork Sells Homes</a></li>
 -->		</ul>
	</div>

	<div class="accountColumn">
		<div class="colTop">
			<span class="lock"></span>

			<h2>My Account</h2><br/>
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'login-form',
				'enableAjaxValidation' => false,
				'action' => array('/front/site/login'),
			));
			?>
			<div class="row">
				<?php echo $form->textField($loginFormModel, 'email', $htmlOptions = array(
					'class' => 'account',
					'placeholder' => 'Username',
				)); ?>
			</div>
			<div class="row">
				<?php echo $form->passwordField($loginFormModel, 'password', $htmlOptions = array(
					'class' => 'account',
					'placeholder' => 'Password',
				)); ?>
			</div>
			<span>
				<a href="#">Register</a> |
				<?php echo CHtml::link('Forgot Password', array('/front/site/forgotPassword')); ?>
				<input type="submit" class="go" value="Go"/>
			</span>
			<?php $this->endWidget(); ?>
		</div>
		<h3>Account Tools:</h3>
		<ul>
			<li><a href="javascript:void(0);">New Home Notifications</a></li>
			<li><a href="javascript:void(0);">Share Homes w/Spouse &amp; Friends</a></li>
			<li><a href="javascript:void(0);">Track Your Showings</a></li>
			<li><a href="javascript:void(0);">Home Ratings</a></li>
			<li><a href="javascript:void(0);">Saved Your Favorite Homes</a></li>
		</ul>
	</div>

	<div id="asSeenOn"><i>As seen on... </i><span></span></div>
</div>