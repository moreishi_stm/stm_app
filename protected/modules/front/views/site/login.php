<div id="login">
	<div id="login-container">
        <h1><em class="login-message"></em>Login to Your Account</h1>
		<?php if($model->resubmit) {
			echo '<h3 class="p-tc" style="margin-top:20px;color:#C20000;">Your already have an account. Please login below.</h3>';
			echo '<h3 class="p-tc" style="">If you forgot your password, please <a href="/forgotPassword">CLICK HERE.</a></h3>';
		}
		echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>
<!--<h2 style="text-align: center; font-size: 35px; margin-bottom: 20px;">You may be Surprised at What Your Home is Worth!</h2>-->
<!---->
<!--<div id="houseValues-container">-->
<!--    --><?php //echo $this->renderPartial('../site/_formHouseValues', array('model'=>$model)); ?>
<!--</div>-->