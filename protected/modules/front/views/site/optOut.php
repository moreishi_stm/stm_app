<style type="text/css">

    /* Container for out buttons */
    .btn-container {
        margin-top: 50px;
    }

    /* Make some pretty looking buttons */
    .btn-unsubscribe {
        display: inline-block;
        padding: 10px;
        border-radius: 6px;
        color: #fff;
        text-decoration: none;
        font-weight: bold;
        font-size: 1.6em;
        margin: 10px 10px;
        border: 1px solid transparent;
    }

    .btn-unsubscribe.yes {
        background-color: #d9534f;
        border-color: #d43f3a;
    }

    .btn-unsubscribe.yes:hover {
        background-color: #c9302c;
        border-color: #ac2925;
    }

    .btn-unsubscribe.no {
        background-color: #5cb85c;
        border-color: #4cae4c;
    }

    .btn-unsubscribe.no:hover {
        background-color: #449d44;
        border-color: #398439;
    }
</style>

<div style="padding: 10px;" align="center">
    <h2>Are you sure you want to opt-out of future email correspondence?</h2>
    <div class='btn-container'>
        <a href="/optout/confirm/<?=$id?>/eeid/<?=$eeid?>" class="btn-unsubscribe yes">Yes, Unsubscribe Me.</a>
        <a href="/" class="btn-unsubscribe no">No, I want to continue receiving real estate market emails.</a>
    </div>
</div>
