<?
// separated into partial view so the list view can be reused in the saved favorites list view
$this->renderPartial('_homeList', array(
                        'DataProvider'=>$DataProvider,
                        'model'=>$model,
                    ));
?>
<div id="mls-disclosure" style="color: #666; font-size: 11px; line-height: 1.3;">
    <?php $this->renderPartial('_mlsDisclosure'); ?>
</div>