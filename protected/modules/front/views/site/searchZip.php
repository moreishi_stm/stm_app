<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
)); ?>
	<div style="width:445px; float:left;">
		<h1 style="width:360px;">Zip Code Search</h1>
		<hr />
		<div>
			<div class="p-tc" style="display:inline-block;padding-top:15px;">
				Enter Zip Code:<br />
    			<?php echo $form->textField($property, 'zip', $htmlOptions=array('style'=>'width:150px;')); ?>
			</div>
			<div style="display:inline-block;">
				<?php echo CHtml::submitButton('Search', array('class'=>'button','style'=>'width:85px;top:-4px;')); ?>
			</div>
		</div>
	</div>
	<img src="http://cdn.seizethemarket.com/assets/images/search_zip.png" style="max-width:250px;" class="p-fr">
<?php $this->endWidget(); ?>