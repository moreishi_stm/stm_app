<?php
Yii::app()->getClientScript()->registerCss('sellerPage', <<<CSS
.row {
    float: left;
    display: block;
    width: 100%;
    padding: 10px 0;
}
.row label {
    float: left;
    width: 130px;
}
form textarea {
    font: 14px arial,helvetica,sans-serif;
    font-weight: bold;
}
#thank-you-message {
    background-color: #a0ffa0;
    text-align: center;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px;
    padding: 20px 10px;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

    -moz-box-shadow:    0px 2px 2px 0px #ccc;
    -webkit-box-shadow: 0px 2px 2px 0px #ccc;
    box-shadow:         0px 2px 2px 0px #ccc;
}

CSS
);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'buyer-form',
        'action'                 => array('/front/forms/buyer/formId/'.$formId),
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnChange' => false,
            'validateOnSubmit' => true,
            'beforeValidate' => 'js:function(form, attribute) {
                                    $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                    return true;
                                }',
            'afterValidate' => 'js:function(form, data, hasErrors) {
                                    if (!hasErrors) {
                                        Message.create("success", "Successfully submitted!");
                                        $("#thank-you-message").removeClass("hidden").hide().show("normal");
                                        $(".row").hide("normal");
                                    }

                                    $("div.loading-container.loading").remove();
                                    return false;
                                }',
        ),
    ));
?>
    <h1>Home Seller Request</h1>
    <hr />
    <div id="thank-you-message" class="hidden g10">
        <h3>Thank You!</h3>
        Your information has been submitted successfully.
    </div>
    <div class="row">
        <label class="g4">Name: <span class="required">*</span></label>
        <div class="g4">
            <?php echo $form->textField($model, 'data[' . $FormFields->getFieldIdByName('first_name') . ']',$htmlOptions = array('placeholder' => 'First Name', 'class' => 'g11',)); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('first_name') . ']'); ?>
        </div>
        <div class="g4">
            <?php echo $form->textField($model, 'data[' . $FormFields->getFieldIdByName('last_name') . ']',$htmlOptions = array('placeholder' => 'Last Name', 'class' => 'g12',)); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('last_name') . ']'); ?>
        </div>
    </div>
    <div class="row">
        <label class="g4">Email: <span class="required">*</span></label>
        <div class="g8">
            <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('email') . ']', $htmlOptions=array('placeholder' => 'Email', 'class'=>'g12')); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('email') . ']'); ?>
        </div>
    </div>
    <div class="row">
        <label class="g4">Address: <span class="required">*</span></label>
        <div class="g8">
            <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('address') . ']', $htmlOptions=array('placeholder' => 'Address', 'class'=>'g12')); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('address') . ']'); ?>
        </div>
    </div>
    <div class="row">
        <label class="g4">City: <span class="required">*</span></label>
        <div class="g4">
            <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('city') . ']', $htmlOptions=array('placeholder' => 'City', 'class'=>'g12')); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('city') . ']'); ?>
        </div>
        <div class="g2">
            <?php echo $form->dropDownList($model,'data[' . $FormFields->getFieldIdByName('state') . ']', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name ASC')), 'id', 'short_name'), $htmlOptions=array('empty' => 'Select State', 'class'=>'g12')); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('state') . ']'); ?>
        </div>
        <div class="g2">
            <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('zip') . ']', $htmlOptions=array('placeholder' => 'Zip', 'class'=>'g12')); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('zip') . ']'); ?>
        </div>
    </div>
    <div class="row">
        <label class="g4">Phone: <span class="required">*</span></label>
        <div class="g8">
            <?php $this->widget('StmMaskedTextField', array(
                    'model' => $model,
                    'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                    'mask' => '(999) 999-9999',
                    'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                    'htmlOptions' => array('class' => 'g12', 'placeholder' => 'Phone'),
                )); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('phone') . ']'); ?>
        </div>
    </div>
    <div class="row">
        <label class="g4">Source: <span class="required">*</span></label>
        <div class="g8">
            <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('source_description') . ']', $htmlOptions=array('placeholder' => 'Source', 'class'=>'g12')); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('source_description') . ']'); ?>
        </div>
    </div>
    <div class="row">
        <label>Timeframe:</label>
        <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('timeframe') . ']', $htmlOptions=array('placeholder' => 'Time frame', 'class'=>'g8')); ?>
        <?php echo $form->error($model,'timeframe'); ?>
    </div>
    <div class="row">
        <label class="g4">Comments:</label>
        <div class="g8">
            <?php echo $form->textArea($model,'data[' . $FormFields->getFieldIdByName('comments') . ']', $htmlOptions=array('placeholder' => 'Comments', 'class'=>'g12', 'rows'=>15)); ?>
            <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('comments') . ']'); ?>
        </div>
    </div>
    <div class="row submit">
        <button id="contact-agent-button" class="wide" type="submit" style="width: 67%; font-size: 15px; margin-left: 120px;">Submit</button>
    </div>
<br />
<? $this->endWidget(); ?>