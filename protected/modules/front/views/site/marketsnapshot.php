<link href='https://fonts.googleapis.com/css?family=Just+Another+Hand' rel='stylesheet' type='text/css'>
<?php
Yii::app()->clientScript->registerCss('marketsnapshotCss', <<<CSS
#market-snapshot {
    /*margin: 0px 0;*/
}
#market-snapshot hr {
    width: 300%;
    border-top: 1px solid #CCC;
    left: -100%;
    position: absolute;
}

/*.full-width-bg {*/
    /*position: absolute;*/
    /*left: -100%;*/
    /*height: 100%;*/
    /*width: 300%;*/
    /*background-color: #F5F5F5; *//*0ABB0A*/
    /*top: 0;*/
    /*z-index: -1;*/
/*}*/
#market-snapshot h1 {
    font-size: 60px;
    margin-top: 15px;
}
#market-snapshot h2 {
    font-size: 35px;
}
#market-snapshot .message-container {
    color: #192DD0;
    font-family: Arial, Helvetica, sans-serif; /*'Just Another Hand', cursive;*/
    font-size: 20px;
    line-height: 1.3;
    margin-bottom: 80px;
    padding: 40px 40px;
    background: #F5F5F5;
}
#market-snapshot .message-greeting {
    font-size: 20px;
    line-height: 1.3;
}
#market-stats1-container {
    /*background: #F5F5F5;*/
    margin-bottom: 100px;
}
#market-stats2-container {
    margin-bottom: 100px;
}
#market-snapshot .market-stats1 {
    /*background-color: #F5F5F5;*/
    padding: 40px 0 30px 0;
    text-align: center;
}
#market-snapshot .market-stats1 h2 {
    line-height: .8;
    font-size: 20px;
    font-weight: bold;
    padding: 0;
    color: black;
}
#market-snapshot .market-stats1 .value {
    color: #0ABB0A;
    margin-top: 20px;
    font-size: 60px;
    font-weight: bold;
}
#market-snapshot .market-stats2 {
    font-size: 20px;
    font-weight: bold;
    /*background-color: #F5F5F5;*/
    padding: 35px 0 40px 0;
    text-align: center;
    color: black;
}
#market-stats2-container .market-stats2 {
    padding: 25px 0 25px 0;
}
#market-stats2-container div.market-stats2:first-child {
    padding-top: 0;
}
#market-snapshot .market-stats2 h2 {
    font-size: 20px;
    font-weight: bold;
    padding: 0;
    color: black;
}

#market-snapshot .market-stats2 .value {
    color: #0ABB0A;
    margin-top: 20px;
    font-size: 40px;
}
.grid-map-container {
    margin-bottom: 100px;
}
#market-snapshot-grid-container {
    height: 550px;
    overflow: auto;
}
#market-snapshot-grid img {
    width: 98%;
    margin-right: 20px;
}
#market-snapshot-grid td {
    padding: 4px 0;
}
#market-snapshot-grid td:first-child {
    width: 25%;
}
#market-snapshot-grid td:nth-child(2) {
    width: 20%;
}
#market-snapshot-grid td:nth-child(3) {
    width: 35%;
}
#market-snapshot-grid td:nth-child(4) {
    width: 20%;
}
#market-snapshot-grid tr.odd {
    background: #efefef;
}
#market-snapshot-grid tr:hover, #market-snapshot-grid tr.selected {
    background: rgba(0, 100, 255, 0.24);
    cursor: pointer;
}
#map {
   height: 550px;
}
#map .status {
    font-weight: bold;
}
#map .status.active {
    color: #0ABB0A;
}
#map .status.sold {
    color: #C20000;
}
#map .price {
    font-weight: bold;
}
.chart1 {
    height: 300px;
    margin-bottom: 80px;
}
.chart2 {
    height: 320px;
    margin-bottom: 80px;
}
.flot-y1-axis .tickLabel{
    color: #2EB900;
    font-weight: bold;
}
.flot-y2-axis .tickLabel{
    color: #447DE8;
    font-weight: bold;
}
.legendLabel{
    padding-left: 4px;
}
#market-snapshot .form-container {
    padding: 40px 40px;
    background-color: #F5F5F5;
    margin-bottom: 100px;
    border: 1px solid #E0E0E0;
}
#market-snapshot .form-container .submit{
    width: 100%;
    font-size: 30px;
    background-color: #25ad29;
}
#market-snapshot .form-container .submit:hover{
    background-color: #28CA2D;
}
.estPrice {
    font-size: 17px;
}
#map-subject {
    height: 400px;
    margin: 0 0 100px 0;
}
#map-subject .gm-style-iw .subjectAddress{
    font-size: 28px;
    font-weight: bold;
    line-height: 35px;
}
#map-subject .gm-style-iw .subjectSpecs{
    font-size: 16px;
    /*font-weight: bold;*/
    line-height: 30px;
}
#map-subject-pano {
    height: 400px;
    margin: 0 0 100px 0;
}
.address {
    text-align: center;
    margin-top: 20px;
    background: #EFEFEF;
    padding: 30px 0 15px;
}
.specs {
    padding-bottom: 30px;
    background: #EFEFEF;
}
.date {
    text-align: center;
    margin: 10px 0px 50px
}
#market-overview {
    text-align: center;
    margin: 10px 0 40px 0;
}
.heat-meter {
    margin-bottom: 15px;
}
.pricing-container {
    margin-bottom: 100px;
}
.disclosure {
    font-size: 11px;
    color: #888;
    line-height: 12px;
    padding-bottom: 15px;
}
.thankYouMessage h3 {
    color: #14ca1e;
    font-size: 30px;
    font-weight: bold;
}
.thankYouMessage .homeSearch {
    font-size: 20px;
    font-weight: bold;
}
.thankYouMessage p {
    font-size: 20px;
}
CSS
);
$domainSuffix = (YII_DEBUG) ? 'local' : 'com';
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/flot/flot-0.8.3/excanvas.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/flot/flot-0.8.3/jquery.flot.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/flot/flot-0.8.3/jquery.flot.pie.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/flot/flot-plugin-curvedLines/curvedLines.js', CClientScript::POS_END);

$bedrooms = $address['bedrooms'];
$baths = $address['baths'];
$sf = $address['sq_feet'];

if(!Yii::app()->user->isGuest):

    extract($chartData);

    Yii::app()->clientScript->registerScript('marketsnapshotJs', <<<JS

    //*********************************************************************************
    // Chart #1
    //*********************************************************************************

    var data = [
            {
                color: "#528EFF",
                label : 'Sold Units',
                data: $soldUnits,
                bars: {
                    show: true,
                    align: 'center',
                    fill: true,
                    barWidth: 0.85,
                    fillColor: {
                        colors: [ { opacity: .95 }, { opacity: .95 } ]
                    }
                },
                yaxis: 2
            },
            {
                color: "#45D914",
                label : 'Average Sales Price',
                lines: {
                    lineWidth: 4
                },
                data: $avgPrice
            }
        ];

    var options = {
            xaxis: { tickLength:0, ticks: $xLabel },
            yaxes: [
                {
                    min: 0,
                    max : $maxChartY1,
//					position: 'left',
                    tickFormatter: function(val, axis) {
                        return (val) ? '$' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '';
                    }
                },
                {
                    min: 0,
                    max : $maxChartY2, //@todo: need to make this calculated
					position: 'right',
                    tickFormatter: function(val, axis) {
                        return (val) ? val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '';
                    }
                }
            ],
            grid: {
                borderWidth: {top: 0, right: 0, bottom: 0, left: 0}
            }
//            series: {
//                curvedLines: {
//                    apply: true,
//                    active: true
////                    monotonicFit: true
//                }
//            }
        };
    $.plot(".chart1", data, options );

    function labelFormatter(label, series)
    {
        return '$' + label;
    }

    //*********************************************************************************
    // Chart #2
    //*********************************************************************************
    var data2 = [
            {
                color: "#FFFD33",
                label : '$piePriceLabel1',
                data: $piePricePercent1
            },
            {
                color: "#7AE856",
                label : '$piePriceLabel2',
                data: $piePricePercent2
            },
            {
                color: "#6E9EFA",
                label : '$300,000\'s',
                data: $piePricePercent3
            },
            {
                color: "#AFD8F8",
                label : '$400,000\'s',
                data: $piePricePercent4
            },
            {
                color: "#D883FF",
                label : '$500,000\'s',
                data: $piePricePercent5
            },
            {
                color: "#FF8A98",
                label : '$600k to 1 Million',
                data: $piePricePercent6
            },
            {
                color: "#FFB34D",
                label : '$1 Million+',
                data: $piePricePercent7
            }
        ];

    var options2 = {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    label: {
                        show: true,
                        radius: 2/3,
                        formatter: pieLabelFormatter,
                        threshold: 0.1
                    }
                }
            },
            legend : {
                show: true
            }
        };
	function pieLabelFormatter(label, series) {
		return "<div style='font-size:8pt; text-align:center; padding:2px; color:#333; font-weight: bold;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
	}

    $.plot(".chart2", data2, options2 );


    //*********************************************************************************
    // Google Maps Subject
    //*********************************************************************************
    var panorama = new google.maps.StreetViewPanorama(document.getElementById("map-subject-pano"));
    var directionsService = new google.maps.DirectionsService();
    var sv = new google.maps.StreetViewService();
    var geocoder = new google.maps.Geocoder();
    var myLatLng;

    geocoder.geocode( { 'address': "{$streetAddress}, {$cityStZip}"}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            myLatLng = results[0].geometry.location;
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });

            // create the regular map view
            var infowindowSubject = new google.maps.InfoWindow();
            var mapSubject = new google.maps.Map(document.getElementById('map-subject'), {
                zoom: 30,
                disableDefaultUI: true,
                scrollwheel: false,
                zoomControl: true,
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.SATELLITE
            });
            var markerSubject = new google.maps.Marker({
                // Supply map and position params as usual.
                position: results[0].geometry.location,
                icon: 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png',
                map: mapSubject,
                title: '{$streetAddress}'
            });
            infowindowSubject.setContent('<div class="subjectAddress">{$streetAddress}<br>{$cityStZip}</div><div class="subjectSpecs">{$bedrooms} Beds &nbsp;|&nbsp; {$baths} Baths &nbsp;|&nbsp; {$sf} SF</div>');
            infowindowSubject.open(mapSubject, markerSubject);

            // create the panoramic map view
            // find a Streetview location on the road
            var request = {
                origin: "{$streetAddress}, {$cityStZip}",
                destination: "{$streetAddress}, {$cityStZip}",
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, directionsCallback);
        } else {
            $('#map-subject, #map-subject-pano').hide();
        }

    });








//    var infowindowSubject = new google.maps.InfoWindow();
//    var mapSubject = new google.maps.Map(document.getElementById('map-subject'), {
//        zoom: 30,
//        disableDefaultUI: true,
//        scrollwheel: false,
//        zoomControl: true,
//        center: new google.maps.LatLng({$subjectMapCenter}),
//        mapTypeId: google.maps.MapTypeId.SATELLITE
//    });
//    var markerSubject = new google.maps.Marker({
//        // Supply map and position params as usual.
//        position: new google.maps.LatLng($subjectMarkerCenter),
//        icon: 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png',
//        map: mapSubject,
//        title: '{$streetAddress}'
//
//    });
//    infowindowSubject.setContent('<div class="subjectAddress">{$streetAddress}<br>{$cityStZip}</div><div class="subjectSpecs">{$bedrooms} Beds &nbsp;|&nbsp; {$baths} Baths &nbsp;|&nbsp; {$sf} SF</div>');
//    infowindowSubject.open(mapSubject, markerSubject);

    var panorama = new google.maps.StreetViewPanorama(document.getElementById("map-subject-pano"));
    var sv = new google.maps.StreetViewService();
//    sv.getPanoramaByLocation(myLatLng, 50, processSVData);
    //var myLatLng = new google.maps.LatLng({$subjectCenter});

//----------------------------------------------
//    var directionsService = new google.maps.DirectionsService();
//    var geocoder = new google.maps.Geocoder();
//    var address = '{$geoAddress}';
//    var myLatLng;

//    geocoder.geocode({ 'address': address }, function(results, status) {
//        if (status == google.maps.GeocoderStatus.OK) {
//            myLatLng = results[0].geometry.location; console.log('hi'); console.log(results[0].geometry.location);
//
//            // find a Streetview location on the road
//            var request = {
//                origin: address,
//                destination: address,
//                travelMode: google.maps.DirectionsTravelMode.DRIVING
//            };
//            directionsService.route(request, directionsCallback);
//        } else {
//            alert("Geocode was not successful for the following reason: " + status);
//        }
//    });

    function directionsCallback(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            var latlng = response.routes[0].legs[0].start_location;
            sv.getPanoramaByLocation(latlng, 50, processSVData);
        } else {
            alert("Directions service not successfull for the following reason:" + status);
        }
    }

//----------------------------------------------

    // function for street view data
    function processSVData(data, status)
    {
        if (status == google.maps.StreetViewStatus.OK) {
            var markerPano = new google.maps.Marker({
                position: data.location.latLng,
                draggable: true,
                map: map,
                title: '{$streetAddress}'
            });

            panorama.setPano(data.location.pano);

            var heading = google.maps.geometry.spherical.computeHeading(data.location.latLng, myLatLng);
            // alert(data.location.latLng+":"+myLatLng+":"+heading);
            panorama.setPov({
                heading: heading,
                pitch: 0,
                zoom: 1
            });
            panorama.setVisible(true);

            google.maps.event.addListener(markerPano, 'click', function() {

            var markerPanoID = data.location.pano;
            // Set the Pano to use the passed panoID
            panorama.setPano(markerPanoID);
            panorama.setPov({
                heading: 270,
                pitch: 0,
                zoom: 1
            });
            panorama.setVisible(true);
            });
            panorama.setOptions({disableDefaultUI: true, zoom: 2});
        }
        else {
            $('#map-subject').removeClass('col-md-6');
            $('#map-subject').addClass('col-md-12');
            $('#map-subject-pano').hide();
            var r = google.maps.event.trigger(mapSubject, "resize");
            mapSubject.setCenter(new google.maps.LatLng({$subjectMapCenter}));
            return;
        }
    }

    //*********************************************************************************
    // Google Maps Properties
    //*********************************************************************************
    var infowindow = new google.maps.InfoWindow();
    var locations = $locationsData;
    var arrMarkers = [];
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        scrollwheel: false,
        disableDefaultUI: true,
        center: new google.maps.LatLng($center),
        mapTypeId: google.maps.MapTypeId.SATELLITE
    });
    var hasGeoData = $boardHasGeoData;

//    var bounds = new google.maps.LatLngBounds();
    var marker, i;

    for (i = 0; i < locations.length; i++) {

    if(hasGeoData){
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i].lat, locations[i].lon),
            icon: (locations[i].status == 'Sold') ? 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png' : 'http://cdn.seizethemarket.com/assets/images/map-house-marker-green.png',
            map: map
        });
        arrMarkers.push(marker);

        //extend the bounds to include each marker's position
//        bounds.extend(marker.position);
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i].mapPinContent);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
    else {
        geocoder.geocode( { 'address': locations[i].street + ", " + locations[i].cityStZip}, geoCallback(locations[i]));
    }

//-------------------------------------------------------------------------------

//                marker = new google.maps.Marker({
//                    position: results[0].geometry.location,
//                    icon: (locations[i].status == 'Sold') ? 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png' : 'http://cdn.seizethemarket.com/assets/images/map-house-marker-green.png',
//                    map: map
//                });
//                arrMarkers.push(marker);
//
//                //extend the bounds to include each marker's position
//            //        bounds.extend(marker.position);
//                google.maps.event.addListener(marker, 'click', (function (marker, i) {
//                    return function () {
//                        infowindow.setContent(locations[i].mapPinContent);
//                        infowindow.open(map, marker);
//                    }
//                })(marker, i));

//            } else {
////                $('#map').hide();
//            }
//        });
    }

    function geoCallback(location)
    {
        var geocodeCallBack = function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                marker = new google.maps.Marker({
                    position: results[0].geometry.location,
                    icon: (location.status == 'Sold') ? 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png' : 'http://cdn.seizethemarket.com/assets/images/map-house-marker-green.png',
                    map: map
                });
                arrMarkers.push(marker);

                //extend the bounds to include each marker's position
            //        bounds.extend(marker.position);
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(location.mapPinContent);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
        return geocodeCallBack;
    }

    function RowClick (i)
    {
        map.setZoom(19);
        if(arrMarkers[i] != undefined) {
            map.setCenter(arrMarkers[i].getPosition());
            google.maps.event.trigger (arrMarkers[i], 'click');
        }
    }
//    map.fitBounds(bounds);

    $(document).delegate('#market-snapshot-grid tr', 'click', function(arrMarkers){
        RowClick($(this).data('id'))
    });
JS
, CClientScript::POS_END);
endif;
?>
<script src="http://maps.google.com/maps/api/js" type="text/javascript"></script>

<div class="row">
    <br/>
    <!--left column-->

    <div class="col-lg-3 col-md-3 col-sm-12">
        <div id="home-listview-search">
            <?php $this->renderPartial('_sideBarSearch', array(
                    'model' => $model,
                    'searchableFields'=> $searchableFields,
                    'action' => $action
                )); ?>
        </div>
    </div>

    <!--right column-->
    <div class="col-lg-9 col-md-9 col-sm-12">

        <div id="market-snapshot" class="col-lg-12">
            <?
            if(Yii::app()->user->isGuest) { ?>
        <!--        echo 'What is the location of the Market Snapshot you would like?';-->

                <h1 class="text-center"><?php echo $this->pageTitle;?></h1>
                <h2><?=$address['address']?></h2>
                <h2 style="text-align: center; margin-top: 10px;"><?=date('F j, Y')?></h2>
                <div class="col-lg-12 message-container">
                    <div class="message-greeting text-center">Hello, Please login to view your House Values Report.<br>Call <?=Yii::app()->user->settings->office_phone?> for immediate service.</div>
                    <div class="full-width-bg"></div>
                </div>
            <?}
            else { ?>
<!--               <div class="col-lg-12 message-container">-->
<!--                   <div class="message-greeting">Hi --><?//=Yii::app()->user->first_name;?><!--,</div>-->
<!--                   <div class="message-body">-->
<!--                       Here is your Market Snapshot for <span style="font-weight: bold; text-decoration: underline;">--><?//=$address['address'].', '.$address['city'].', '.AddressStates::getShortNameById($address['state_id']).' '.$address['zip'].'</span>, '.$address['bedrooms'].' Bedroom, '.$address['baths'].' Baths, '.$address['sf'].' Sq. Feet.<br>Call '.Yii::app()->user->settings->office_phone.' with any questions. Thanks!'?>
<!--                   </div>-->
<!--                       <div class="full-width-bg"></div>-->
<!--               </div>-->
                <h1 class="text-center"><?php echo $this->pageTitle;?></h1>
                <h2 class="text-center" style="font-size: 25px;">Real Estate Market Trends Report</h2>
                <h2 class="date"><?=date('F j, Y')?></h2>
<!--                <h2 class="address">--><?//=$address['address'].', '.$address['city'].', '.AddressStates::getShortNameById($address['state_id']).', '.$address['zip']?><!--</h2>-->
<!--                <h3 class="specs text-center">--><?//=$address['bedrooms'].' Bedrooms / '.$address['baths'].' Baths / '.$address['sq_feet'].' Sqft'?><!--</h3>-->
                <div id="map-subject" class="col-md-6 col-xs-12 no-padding">
<!--                    <iframe width="100%" style="pointer-event:none;" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="--><?php //echo $mapUrl;?><!--"></iframe>-->
                </div>
                <div id="map-subject-pano" class="col-md-6 col-xs-12 no-padding"></div>

                <div class="col-xs-12 no-padding pricing-container">
                    <h2 class="text-center">
                        Estimated Price:
                        <? if($address['bedrooms'] && $address['sq_feet']):
                            echo Yii::app()->format->formatDollars($estPrices['price']);
                        else:
                            echo 'Almost there...';
                            echo "<h3>Please enter your Sq.Feet, Bedrooms and Bath below and submit to for us to complete our evaluation.</h3>";
                        endif; ?>
                        </h2>
                    <img class="heat-meter col-xs-12 no-padding" height="80" src="http://cdn.seizethemarket.com/assets/images/heat_meter.jpg" alt=""/>
                    <? if($address['bedrooms'] && $address['sq_feet']): ?>
                    <div class="col-xs-6 estPrice">
                        Low Price Estimate: <?=Yii::app()->format->formatDollars($estPrices['min'])?>
                    </div>
                    <div class="col-xs-6 text-right estPrice">
                        High Price Estimate: <?=Yii::app()->format->formatDollars($estPrices['max'])?>
                    </div>
                    <? endif; ?>
                </div>

                <div class="form-container col-lg-12">
                    <h2 class="update-estimate">Update Home Value Estimate</h2>
                    <h3 class="update-estimate">Provide home features and improvements to update your estimate.</h3>
                    <? $form2 = $this->beginWidget('CActiveForm', array(
                            'id'=>'market-snapshot-update-estimate-form',
                            'action' => array('/front/forms/marketSnapshotUpdateEstimate/formId/'.Forms::FORM_MARKET_SNAPSHOT_UPDATE_ESTIMATE),
                            'enableAjaxValidation'   => true,
                            'enableClientValidation' => false,
                            'clientOptions' => array(
                                'validateOnChange' => false,
                                'validateOnSubmit' => true,
                                'beforeValidate'    => 'js:function(form) {
                                            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                            return true;
                                        }',
                                'afterValidate' => 'js:function(form, data, hasErrors) {
                                        $("div.loading-container.loading").remove();
                                        if (!hasErrors) {
                                            Message.create("success", "Your request was sent successfully!");
                                            $("#market-snapshot-update-estimate-form").addClass("hidden");
                                            $("h2.update-estimate, h3.update-estimate").hide();
                                            $("div#thank-you-message-container-2").hide().removeClass("hidden").show();
                                        }

                                        return false;
                                    }',
                            )
                        ));
                    ?>

                    <div class="col-sm-6 col-xs-12 no-padding">

                        <?php echo $form2->textArea($SubmissionValues, 'data['.$FormFields->getField('question')->id.']', $htmlOptions=array('placeholder'=>"Home improvements & features to consider . . .",'class'=>'col-xs-12','rows'=>4, 'style'=>'height: 100%;'));?>
                        <?php echo $form2->error($SubmissionValues, 'data['.$FormFields->getField('question')->id.']'); ?>
                    </div>
                    <div class="col-sm-6 col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">
                            <div class="col-sm-6 col-xs-6 no-padding">
                                <?php echo $form2->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array('placeholder'=>'First Name','class'=>'col-xs-12',));?>
                                <?php echo $form2->error($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']'); ?>
                            </div>
                            <div class="col-sm-6 col-xs-6 no-padding">
                                <?php echo $form2->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array('placeholder'=>'Last Name','class'=>'col-xs-12',));?>
                                <?php echo $form2->error($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']'); ?>
                            </div>
                        </div>

                        <div class="col-xs-12 no-padding">
                            <div class="col-sm-6 col-xs-6 no-padding">
                                <?php echo $form2->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array('placeholder'=>'E-mail','class'=>'col-xs-12',));?>
                                <?php echo $form2->error($SubmissionValues, 'data['.$FormFields->getField('email')->id.']'); ?>
                            </div>
                            <div class="col-sm-6 col-xs-6 no-padding">
                                <?php $this->widget('StmMaskedTextField', array(
                                        'model' => $SubmissionValues,
                                        'attribute' => 'data['.$FormFields->getField('phone')->id.']',
                                        'mask' => '(999) 999-9999',
                                        'id' => 'FormSubmissionValues_data_4',
                                        'htmlOptions' => array('class'=>'col-xs-12','placeholder' => 'Phone',),
                                    )); ?>
                                <?php echo $form2->error($SubmissionValues, 'data['.$FormFields->getField('phone')->id.']'); ?>
                            </div>
                            <?php echo $form2->hiddenField($SubmissionValues, 'data['.$FormFields->getField('source_description')->id.']', $htmlOptions=array('value'=>'Market Snapshot Update Estimate')); ?>
                            <?php echo $form2->hiddenField($SubmissionValues, 'data['.$FormFields->getField('lead_url')->id.']', $htmlOptions=array('value'=>'Original Seller: http://www.'.$domainName.'/admin/sellers/'.$sellerId)); ?>
                            <?php echo CHtml::hiddenField('url',$_SERVER['REQUEST_URI']); ?>
                        </div>
                    </div>
                    <input id="submit-button" type="submit" class="submit btn btn-lg btn-primary" value="Submit Home Value Updates">
                    <?$this->endWidget();?>
                    <div id="thank-you-message-container-2" class="hidden thankYouMessage">
                        <h3>Thank You, we received your request!</h3>
                        <p>
                         We will touch base with you shortly. For immediate assistance call us at <strong><?php echo Yii::app()->user->settings->office_phone;?></strong> . We look forward to helping you and have a great day! =)<br /><br />Meanwhile, <a href="/homes" class="homeSearch">Click here to View All Homes for Sale</a>.
                        </p>
                    </div>
                </div>

                <div class="grid-map-container col-lg-12 no-padding">
                    <div id="market-snapshot-grid-container" class="col-lg-6 no-padding">
                        <? $this->widget('admin_module.components.StmGridView', array(
                                'id' => 'market-snapshot-grid',
                                'dataProvider' => $dataProvider,
                                'itemsCssClass' => 'datatables col-md-12 no-padding',
                                'htmlOptions' => array('class'=>'grid-view col-xs-12 no-padding'),
                                'template' => '{items}{pager}', //{summary}
                                'rowHtmlOptionsExpression' => 'array("data-id"=>($row + ($this->dataProvider->pagination->pageSize* $this->dataProvider->pagination->currentPage)))',
                                'summaryText'=>$start.'-'.$end.' of '.$count.' results.',
                                'pager'=> array('id'=>'marketSnapshotPager','class' => 'admin_module.components.StmListPager'),
                                'columns' => array(
                                    array(
                                        'type' => 'raw',
                                        'name' => 'Photo',
                                        'value' => 'Yii::app()->controller->action->printColumn1($data)',
                                    ),
                                    array(
                                        'type' => 'raw',
                                        'name' => 'Price / Status',
                                        'value' => 'Yii::app()->controller->action->printColumn2($data)',
                                    ),
                                    array(
                                        'type' => 'raw',
                                        'name' => 'Address',
                                        'value' => 'Yii::app()->controller->action->printColumn3($data)',
                                    ),
                                    array(
                                        'type' => 'raw',
                                        'name' => '',
                                        'value' => 'Yii::app()->controller->action->printColumn4($data)',
                                    ),
                                ),
                            )
                        );
                        ?>
                    </div>
                    <div id="map" class="col-lg-6"></div>
                </div>

                <h2 id="market-overview">Market Overview</h2>

                <div class="col-lg-8 chart1"></div>

                <div id="market-stats1-container" class="col-lg-4 col-xs-12 no-padding">
                    <div class="col-sm-12 col-xs-12 market-stats2">
                        <h2 class="col-lg-12">Homes for Sale</h2>
                        <div class="value col-lg-12"><?=$stats['activeCount']?></div>
                    </div>
                    <?if($stats['dom']):?>
                    <div class="col-sm-12 col-xs-6 market-stats2">
                        <h2 class="col-lg-12">Avg Days on Market</h2>
                        <div class="value col-lg-12"><?=$stats['dom']?></div>
                    </div>
                    <!--                    <div class="full-width-bg"></div>-->
                    <?endif;?>
                </div>

                <div id="market-stats2-container" class="col-lg-3 col-xs-12 no-padding">
<!--                    <div class="col-sm-12 col-xs-6 market-stats2">-->
<!--                        <h2 class="col-lg-12">Lowest Price</h2>-->
<!--                        <div class="value col-lg-12">--><?//=Yii::app()->format->formatDollars($stats['minPrice'])?><!--</div>-->
<!--                    </div>-->
<!--                    <div class="col-sm-12 col-xs-6 market-stats2">-->
<!--                        <h2 class="col-lg-12">Homes Sold Last 3 Months</h2>-->
<!--                        <div class="value col-lg-12">--><?//=$stats['soldCount']?><!--</div>-->
<!--                    </div>-->
                    <div class="col-sm-12 col-xs-6 market-stats2">
                        <h2 class="col-lg-12">Highest Price</h2>
                        <div class="value col-lg-12"><?=Yii::app()->format->formatDollars($stats['maxPrice'])?></div>
                    </div>
                    <div class="col-sm-12 col-xs-6 market-stats2">
                        <h2 class="col-lg-12">Average Price</h2>
                        <div class="value col-lg-12"><?=Yii::app()->format->formatDollars($stats['avgPrice'])?></div>
                    </div>
<!--                    <div class="col-sm-12 col-xs-6 market-stats2">-->
<!--                        <h2 class="col-lg-12">Avg $/Sq.Ft.</h2>-->
<!--                        <div class="value col-lg-12">--><?//=Yii::app()->format->formatDollars($stats['avgDollarSf'])?><!--</div>-->
<!--                    </div>-->
                    <div class="col-sm-12 col-xs-6 market-stats2">
                        <h2 class="col-lg-12">Avg Sq. Feet</h2>
                        <div class="value col-lg-12"><?=number_format($stats['avgSf'])?></div>
                    </div>
<!--                    <div class="full-width-bg"></div>-->
                </div>
                <div class="col-lg-9 chart2"></div>
            <? } ?>

            <div class="form-container col-lg-12">
                <h2 class="ask-question">Ask a Question / Thinking About Selling?</h2>
                <h3 class="ask-question">FREE & Quick Expert Advice</h3>
                <? $form = $this->beginWidget('CActiveForm', array(
                        'id'=>'market-snapshot-form',
                        'action' => array('/front/forms/marketSnapshotQuestion/formId/'.$formId),
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnChange' => false,
                            'validateOnSubmit' => true,
                            'beforeValidate'    => 'js:function(form) {
                                            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                            return true;
                                        }',
                            'afterValidate' => 'js:function(form, data, hasErrors) {
                                        $("div.loading-container.loading").remove();
                                        if (!hasErrors) {
                                            Message.create("success", "Your request was sent successfully!");
                                            $("#market-snapshot-form").addClass("hidden");
                                            $("h2.ask-question, h3.ask-question").hide();
                                            $("div#thank-you-message-container-1").hide().removeClass("hidden").show();
                                        }

                                        return false;
                                    }',
                        )
                    ));
                ?>

                <div class="col-sm-6 col-xs-12 no-padding">

                    <?php echo $form->textArea($SubmissionValues, 'data['.$FormFields->getField('question')->id.']', $htmlOptions=array('placeholder'=>"Ask a question here . . .",'class'=>'col-xs-12','rows'=>4, 'style'=>'height: 100%;'));?>
                    <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('question')->id.']'); ?>
                </div>
                <div class="col-sm-6 col-xs-12 no-padding">
                    <div class="col-xs-12 no-padding">
                        <div class="col-sm-6 col-xs-6 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array('placeholder'=>'First Name','class'=>'col-xs-12',));?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']'); ?>
                        </div>
                        <div class="col-sm-6 col-xs-6 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array('placeholder'=>'Last Name','class'=>'col-xs-12',));?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']'); ?>
                        </div>
                    </div>

                    <div class="col-xs-12 no-padding">
                        <div class="col-sm-6 col-xs-6 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array('placeholder'=>'E-mail','class'=>'col-xs-12',));?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('email')->id.']'); ?>
                        </div>
                        <div class="col-sm-6 col-xs-6 no-padding">
                            <?php $this->widget('StmMaskedTextField', array(
                                    'model' => $SubmissionValues,
                                    'attribute' => 'data['.$FormFields->getField('phone')->id.']',
                                    'mask' => '(999) 999-9999',
                                    'id' => 'FormSubmissionValues_data_4',
                                    'htmlOptions' => array('class'=>'col-xs-12','placeholder' => 'Phone',),
                                )); ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('phone')->id.']'); ?>
                        </div>
                        <?php echo $form->hiddenField($SubmissionValues, 'data['.$FormFields->getField('source_description')->id.']', $htmlOptions=array('value'=>'Market Snapshot Question')); ?>
                        <?php echo $form->hiddenField($SubmissionValues, 'data['.$FormFields->getField('lead_url')->id.']', $htmlOptions=array('value'=>'Original Seller: http://www.'.$domainName.'/admin/sellers/'.$sellerId)); ?>
                        <?php echo CHtml::hiddenField('url',$_SERVER['REQUEST_URI']); ?>
                    </div>
                </div>
                <input id="submit-button" type="submit" class="submit btn btn-lg btn-primary" value="Request More Information">
                <?$this->endWidget();?>
                <div id="thank-you-message-container-1" class="hidden thankYouMessage">
                    <h3>Thank You, we received your request!</h3>
                    <p>
                        We will touch base with you shortly. For immediate assistance call us at <strong><?php echo Yii::app()->user->settings->office_phone;?></strong> . We look forward to helping you and have a great day! =)<br /><br />Meanwhile, <a href="/homes" class="homeSearch">click here to View All Homes for Sale</a>
                    </p>
                </div>
            </div>

            <div class="disclosure col-xs-12">
                * This is not an official appraisal. All information provided by the listing agent/broker is presumed reliable but is not guaranteed, and should be independently verified. The information provided in this report is provided for personal use only and may not be copied or distributed.
            </div>

        </div>
    </div>
</div>