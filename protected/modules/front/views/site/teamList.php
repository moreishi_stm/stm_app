<h1>Meet Our Team</h1>
<br />
<div id="profile">
<?php
	$this->widget( 'front_module.components.StmListView', array(
		'dataProvider'         => $DataProvider,
		'id'                   => 'team-list',
		'template'			   => '{items}{pager}',
		'itemView'             => '_teamListRow',
		'itemsCssClass'        =>'datatables',
	));
?>
</div>