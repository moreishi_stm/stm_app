<div id="profile">
	<div class="g98 top-container">
		<div id="photo-container" class= "g3 p-pr10">
			<?php
				if ($file = $SettingContactValues->data[$model->getField('profile_photo')->id]) {
					$image = Yii::app()->user->profileImageBaseUrl . DS . Yii::app()->request->getQuery('id') . DS . $file;
				} else {
					$image = 'http://cdn.seizethemarket.com/assets/images/profile_placeholder_1.jpg';
				}
			?>
	        <img id="profile-photo" src="<?php echo $image;?>">

			<?php /** Add social icons here */ ?>
        </div>
        <div class="g8">
            <h3 class="name"><?php echo $Contact->fullName; ?></h3>
            <h4 class="title"><?php echo $SettingContactValues->data[$model->getField('title')->id]; ?></h4>
            <h4 class="title"><?php echo $SettingContactValues->data[$model->getField('subtitle')->id]; ?></h4>
            <h4>My Bio:</h4>

            <p><?php echo $SettingContactValues->data[$model->getField('bio')->id]; ?></p>

	        <div class="p-pt10">
				<h4>Fun Facts:</h4>
				<ul class="profile-details"><?php if ($books = $ContactAttributeValues->data[$ContactAttributes->getField('books')->id]) { ?>
						<li><em class="icon i_stm_books"></em><div class="profile-item-container"><label>Favorite Books:</label><?php echo $books; ?></div></li>
					<?php
					}
					if ($restaurants = $ContactAttributeValues->data[$ContactAttributes->getField('restaurants')->id]) {
						?>
						<li><em class="icon i_stm_restaurants"></em><div class="profile-item-container"><label>Favorite Restaurants:</label><?php echo $restaurants;?>
						</li>
					<?php
					}
					if ($movies = $ContactAttributeValues->data[$ContactAttributes->getField('movies')->id]) {
						?>
						<li><em class="icon i_stm_movies"></em><div class="profile-item-container"><label>Favorite Movies:</label><?php echo $movies; ?></div></li>
					<?php
					}
					if ($hobbies = $ContactAttributeValues->data[$ContactAttributes->getField('hobbies')->id]) {
						?>
						<li><em class="icon i_stm_hobbies"></em><div class="profile-item-container"><label>Hobbies:</label><?php echo $hobbies; ?></div></li>
					<?php
					}
					if ($words = $SettingContactValues->data[$model->getField('words_describe_self')->id]) {
						?>
						<li><em class="icon i_stm_words"></em><div class="profile-item-container"><label>Words to Describes Self:</label><?php echo $words; ?></div></li>
					<?php
					}
					if ($quotes = $SettingContactValues->data[$model->getField('favorite_quotes')->id]) {
						?>
						<li><em class="icon i_stm_quotes"></em><div class="profile-item-container"><label>Favorite Quotes:</label><?php echo $quotes; ?></div></li>
					<?php
					}
					if ($moments = $SettingContactValues->data[$model->getField('memorable_moments')->id]) {
						?>
						<li><em class="icon i_stm_smile"></em><div class="profile-item-container"><label>Most Memorable Real Estate
									Moments:</label><?php echo $moments; ?></div></li>
					<?php } ?>
				</ul>
	        </div>
        </div>
	</div>
</div>
