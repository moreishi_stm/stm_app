<style type="text/css">
	.property-tax-calculator-tab-container {
		margin: 18px;
		position: relative;
		float: left;
		width: 640px;
	}
	.property-tax-calculator-tab-container label {
		width: 260px;
		display: inline-block;
		text-align: right;
		font-size: 15px;
	}
	.property-tax-calculator-tab-container input, .property-tax-calculator-tab-container select{
		font-size: 25px;
		font-weight: bold;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		border: solid 1px #CCC;
		padding: 5px 10px;
	}
	.property-tax-calculator-tab-container input, .property-tax-calculator-tab-container select{
		color: #333;
	}
	.property-tax-calculator-tab-container select{
		border-color: #AAA;
		height: 43px;
		width: 27.5%;
	}
	.property-tax-calculator-tab-container #county{
		font-size: 12px;
	}
	.property-tax-calculator-tab-container h3 {
		padding-left: 0 !important;
		font-size: 25px;
		height: 45px;
		display: inline-block;
	}
	.property-tax-calculator-tab-container div.monthly-payment-row{
		margin-top: 10px;
	}
	.property-tax-calculator-tab-container #monthly-payment, .property-tax-calculator-tab-container #yearly-payment{
		border: 1px solid #62D164;
		background-color: #CDF7CE;
		font-size: 24px;
		font-weight: bold;
		padding: 10px 10px;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		width: 24.5%;
		display: inline-block;
	}
	.property-tax-calculator-tab-container .i_mortgage_calculator{
		height: 78px;
		width: 69px;
		display: inline-block;
		background-repeat: no-repeat;
		background-image: url(http://cdn.seizethemarket.com/assets/images/calculator_2.png);
		float: right;
		position: relative;
		top:20px;
	}
	.property-tax-calculator-tab-container #property-tax-calcualtor-fields div.row{
		padding: 5px 0;
	}
	.property-tax-calculator-tab-container #property-tax-calculator-button{
		font-size: 14px;
		margin-top: 15px;
		padding: 12px 30px;
	}

</style>
<?php
	$this->renderPartial('homeDetails/_propertyTaxCalculator');
?>