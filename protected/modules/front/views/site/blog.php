<?php
Yii::app()->getClientScript()->registerCss('blogCss', <<<CSS
    h1 {
        margin: 15px 0 20px 0 !important;
        font-weight: bold !important;
        font-size: 40px !important;
    }
    h3 {
        font-weight: bold !important;
    }
    .blog-entry {
        padding: 50px 0;
        border-bottom: 1px solid #CCC;
    }
    .blog-entry:nth-child(odd) {
        background: #F3F3F3;
    }
    .read-more-button {
        margin-top: 15px;
        display: inline-block;
        padding: 12px;
        border: 1px solid #EAEAEA;
        background: #F8F8F8;
    }
    .blog-entry:nth-child(odd) .read-more-button{
        background: #fcfcfc;
    }
    .blog-entry:hover {
        background: #EDEDFF;
    }
    .blog-entry:hover .read-more-button {
        background: #fcfcfc;
    }
    .blog-entry img {
        max-width: 100%;
    }
    .img-responsive {
        max-height: 200px;
    }
    .topic-label {
        margin-top: 40px;
        font-size: 18px;
    }
    #blog-grid .pager {
        float: right;
        margin: 20px 0 !important;
    }
    #blog-grid .pager  ul li a {
        padding: 10px;
        border-radius: 0;
    }
    #blog-grid .pager  ul li.next a {
        margin-left: 4px;
    }
CSS
);
?>
<div class="col-md-8 no-padding">
    <div class="topic-label">View by Topic</div>
    <h1><?php echo $this->pageTitle;?></h1>
    <?php
    $this->widget('zii.widgets.CListView', array('dataProvider' => $dataProvider, 'itemView' => '_blogEntry', 'summaryText' => '','id'=>'blog-grid'));
    ?>
</div>
<div id="cms-topics-container" class="col-md-3">
    <?
    //@todo: temp solution, fix with widget full featured out
    if(strpos($_SERVER['SERVER_NAME'], 'seizethemarket.com') === false && strpos($_SERVER['SERVER_NAME'], 'mylistingstoryboard.com') === false) {
        $this->renderPartial('_sideBarHouseValues');

        Yii::import('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget');
        $this->widget('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget', array(
                'areas' => [
                    ['name'=>'Top Communities', 'tag'=>'Top Communities Sidebar'],
                    ['name'=>'Top Areas','tag'=>'Top Areas Sidebar']
                ]
            ));

        Yii::import('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
        $this->widget('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
    }
    ?>
</div>

