<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<div align="center">
	<h1>Whoops!</h1>
	<?php if ($error['code'] == '404'): ?>
		<h3>We could not find the page you were looking for.</h3>
	<?php else: ?>
		<h3>We had an issue loading the page you requested.</h3>
	<?php endif; ?>
</div>

<div class="error" align="center" style="padding-top: 20px;">
	<?php
		$goBackImage = CHtml::image($this->module->imageAssetsUrl.DS.'go_back.png', 'Go Back', $htmlOptions=array(
			'width'=>250,
		));
		echo CHtml::link($goBackImage, Yii::app()->request->getUrlReferrer());
	?>
	<div>
		<h2><?php echo CHtml::link('Go Back', Yii::app()->request->getUrlReferrer()); ?></h2>
	</div>
</div>