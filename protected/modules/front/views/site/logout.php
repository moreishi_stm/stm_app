<style>
    #login-container h2 {
        text-align: center;
    }
</style>

<div id="login">
	<div id="login-container">
        <h1><em class="logout-success"></em>You've signed out successfully.</h1>
		<h2>Return to Your Account</h2>
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>