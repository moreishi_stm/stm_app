<div class="listing-thumbnail-container">
    <a class="home-photo" href="/home/<?php echo strtolower(AddressStates::getShortNameById(Yii::app()->user->board->state_id)); ?>/<?php echo $data->addressLink?>/<?php echo $data->listing_id?>"><img src="<?php echo $data->getPhotoUrl($photoNumber=1); ?>" style="width: 100%; margin:0;"></a>
    <div class="price"><?php echo Yii::app()->format->formatDollars($data->price)?></div>
    <div class="beds-bath"><?php echo ($data->bedrooms)? $data->bedrooms.' Beds':''; echo ($data->baths_total)? ' / '.$data->baths_total.' Baths':''; ?></div>
</div>