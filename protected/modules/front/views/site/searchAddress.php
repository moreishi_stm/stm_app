<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>'',
	'id'=>'search-form',
)); ?>
	<div style="width:445px; float:left;">
		<h1 style="width:445px;">Address Search</h1>
		<hr />
		<div class="p-tc" style="display:inline-block;padding-top:15px;">
				<div style="width:85px;display:inline-block;">
					Street #:
					<?php echo $form->textField($property,'street_number', $htmlOptions=array('class'=>'g95'));?>
				</div>
				<div style="width:200px;display:inline-block;display:inline-block;padding-left:3px;">
					Street Name:
					<?php echo $form->textField($property,'street_name', $htmlOptions=array('class'=>'g98'));?>
				</div>
				<div style="width:85px;display:inline-block;display:inline-block;padding-left:2px;">
					(Rd, St, Blvd)
					<?php echo $form->textField($property,'street_suffix', $htmlOptions=array('class'=>'g99'));?>
				</div>
		</div>
		<div>
			<div class="p-tc" style="display:inline-block;padding-top:15px;">
				<div style="width:200px;display:inline-block;">
					City:
					<?php echo $form->textField($property,'city', $htmlOptions=array('class'=>'g97'));?>
				</div>
				<div style="width:85px;display:inline-block;display:inline-block;">
					Zip:
					<?php echo $form->textField($property,'zip', $htmlOptions=array('class'=>'g95'));?>
				</div>
			</div>
			<div style="display:inline-block;padding-left:2px;">
				<?php echo CHtml::submitButton('Search', array('class'=>'button','style'=>'width:85px;top:-4px;')); ?>
			</div>
		</div>
	</div>
	<div class="p-mt10" style="width:250px; float:right;"><img src="http://cdn.seizethemarket.com/assets/images/search_pin.png" width="250px;"></div>
<?php $this->endWidget(); ?>