<h1 class="title">You may be Surprised at What Your Home is Worth!</h1>
<div class="top-area">
    <div class="form-box">
        <div class="form-container">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'houseValues-form',
                'action'=>array($formAction),
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'beforeValidate' => 'js:function(form, attribute) {
                        $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                        return true;
                    }',
                    'afterValidate' => 'js:function(form, data, hasErrors) {
                        if (!hasErrors && data.status =="successPart1") {
                            window.location = "http://'.$_SERVER["SERVER_NAME"].'/values/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/2";
                        } else if (!hasErrors) {
                            window.location = "http://'.$_SERVER["SERVER_NAME"].'/values/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/success";
                        } else {
                            $("div.loading-container.loading").remove();
                        }
                        return false;
                    }',
                ),
            ));

            $this->renderPartial('values/'.$view, array(
                    'form' => $form,
                    'SubmissionValues' => $SubmissionValues,
                    'FormFields' =>$FormFields,
            ));

            $this->endWidget(); ?>
        </div>
    </div>
</div>
<div style="clear:both;margin-bottom: 350px;"></div>
