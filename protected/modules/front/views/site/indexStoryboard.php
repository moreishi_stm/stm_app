<?php //require_once 'helpers/header.php'; ?>
<?php //require_once 'helpers/map.php'; ?>
<?php
    $js = <<<JS
//        $('#search-text').click(function(){
//            windows.location = '/storyboard';
//            alert('For demo only. You will be directed to the sample results page.');
//            window.location.href = "/storyboard";
//        });

//        var sliders = new Array('slider1','slider2','slider3','slider4');
//        setInterval(nextSlider, 5000);

//        function nextSlider(sliders) {
//            var sliderId = $('.currentSlider').attr('id');
//            if(sliderId == 'slider1') {
//                $('#slider2').fadeIn(600);
//                $('#slider1').fadeOut(600);
//                $('#slider2').addClass('currentSlider');
//                $('#slider1').removeClass('currentSlider');
//            } else if(sliderId == 'slider2') {
//                $('#slider3').show('slow');
//                $('#slider2').hide('slow');
//                $('#slider2').removeClass('currentSlider');
//                $('#slider3').addClass('currentSlider');
//            } else if(sliderId == 'slider3') {
//                $('#slider4').show('slow');
//                $('#slider3').hide('slow');
//                $('#slider3').removeClass('currentSlider');
//                $('#slider4').addClass('currentSlider');
//            } else if(sliderId == 'slider4') {
//                $('#slider1').show('slow');
//                $('#slider4').hide('slow');
//                $('#slider4').removeClass('currentSlider');
//                $('#slider1').addClass('currentSlider');
//            }
//        }
JS;

    Yii::app()->clientScript->registerScript('sliders', $js);
?>
<style type="text/css">
    #slider-container {
        height: 550px;
        overflow-y: hidden;
    }
    .slider-item {
        height: 550px;
        display: inline-block;
        overflow-y: hidden;
    }
    .slider-item.currentSlider {
        /*z-index:2;*/
    }
    .slider-item.nextSlider {
        z-index:3;
    }
    .slider-item.hide {
        display: none;
    }
    .slider-item img {
        width: 100%;
        position: absolute;
        top: 0px;
        left: 0px;
        bottom: 0px;
        right: 0px;
        z-index: -1;
    }
    #search-text {
        float:none;
        line-height: 75px;;
        height: 80px;
        font-size: 40px;
    }
    #input-container {
        /*z-index: 100;*/
        position: relative;
        top:250px;
        text-align: center;
    }
</style>
<div id="input-container">
    <a class="span10 query btn gray" id="search-text" href="storyboard"/>View All Listing Storyboards</a>
</div>
<div id="slider-container">
    <div id="slider1" class="slider-item currentSlider">
        <img src="/images/home_slider_3.jpg" alt="Listing Storyboard"/>
    </div>
    <div id="slider2" class="slider-item hide">
        <img src="/images/home_slider_6.jpg" alt="Listing Storyboard"/>
    </div>
    <div  id="slider3" class="slider-item hide">
        <img src="/images/home_slider_1.jpg" alt="Listing Storyboard"/>
    </div>
    <div  id="slider4" class="slider-item hide">
        <img src="/images/home_slider_2.jpg" alt="Listing Storyboard"/>
    </div>
</div>

<!--    <div class="container">-->
<!--        <div id="main">-->
<!--            <div class="row">-->
                <!--                <div class="span9">-->
<!--                    <h1 class="page-header">Featured properties</h1>-->
<!--                    --><?php //require_once 'helpers/properties-grid.php'; ?>
<!--                </div>-->
<!--                <div class="sidebar span3">-->
<!--                    --><?php //require_once 'helpers/widgets/our-agents.php'; ?>
<!--                    <div class="hidden-tablet">-->
<!--                        --><?php //require_once 'helpers/widgets/properties.php'; ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            --><?php //require_once 'helpers/carousel.php'?>
<!--            --><?php //require_once 'helpers/features.php'?>
<!--        </div>-->
<!--    </div>-->

<?php require_once 'helpers/bottom.php'; ?>
<?php //require_once 'helpers/footer.php'; ?>