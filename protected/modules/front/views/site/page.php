<div <?php echo (Yii::app()->params['layoutDirectory'] == 'storyboard')? 'class="container"': ''; //@todo: to remove after bootstrap refactor, this is so it would add container class for listing storyboard ?>>
	<?php
		ob_start();
		eval(' ?>'.$pageContent.'<?php ');
		ob_end_flush();
	?>
</div>

<?php if($display_tags && !$hideCmsTags): ?>
    <div id="cms-topics-container">
        <?php if(!empty($cmsTags)): ?>
            <div class="g11" style="padding: 8px; border: 1px solid #CCC; font-size: 16px; margin-top:50px;">
                Similar Topics:
                <?php $allTags = '';
                foreach($cmsTags as $cmsTag) {
                    $allTags .= ($allTags)? ', ' : '';
                    $allTags .= CHtml::link($cmsTag->name, '/topics/'.$cmsTag->url, $htmlOptions=array('style'=>'color: #2D78CC;'));
                }
                echo $allTags;
                ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php if($allow_comments) :
    $this->widget('front_module.components.widgets.CmsCommentWidget.CmsCommentWidget', array('commentPillView' => 'commentPill', 'commentEntryView' => '_commentEntry'));
endif;?>