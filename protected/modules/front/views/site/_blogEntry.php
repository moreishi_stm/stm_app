<div class="blog-entry col-sm-12">
    <?
    $content = ($data->type_ma == CmsContents::TYPE_BLOG) ? $data->getContentDataByName('blog-content') : $data->getContentDataByName('content');
    if($data->getContentDataByName('photo-1')) {
        $photoSource = (strpos($data->getContentDataByName('photo-1'),'http://') === false) ?'http://www.'.Yii::app()->user->primaryDomain->name : ''; ?><?php echo $data->getContentDataByName('photo-1');
    }
    elseif(strpos($content, 'www.youtube.com/embed/') !== false) {
        // check for youtube video
        preg_match('/(?:.*)youtube.com\/embed\/(.*?)\"/', $content, $matches);
        $youtubeCode = $matches[1];
        $photoSource = 'http://i1.ytimg.com/vi/' . $youtubeCode . '/mqdefault.jpg';
    }
    else{
        // if photo exists in body - jpg, jpeg, tiff, tif, png, gif ... http://... .{photoExt}
        preg_match('@<img.+src="(.*)".*>@Uims', $content, $matches);
        $src = $matches[1];
        $photoSource = $matches[1];
    }
    if($photoSource):?>
    <div class="img col-sm-5 no-padding">
        <img src="<?=$photoSource ?>" alt="" class="img-responsive" />
    </div>
    <?endif;?>
    <div class="text col-sm-6 col-sm-offset-1 no-padding">
        <h3><a href="/<?php echo $data->url?>"><?php echo ucwords($data->title);?></a></h3>
        <div class="content">
            <?php
            $contentStrippedPhpTags = preg_replace('/<\?php.*?\?\>/i', '', $content);
            $contentStrippedPhpTags = str_replace(array('&nbsp;'), ' ', $contentStrippedPhpTags);
            $contentStrippedPhpTags = str_replace(array(PHP_EOL,"\r","\n"), ' ', $contentStrippedPhpTags);
            $contentStrippedPhpTags = preg_replace('/\s+/', ' ', $contentStrippedPhpTags);
            echo substr(strip_tags($contentStrippedPhpTags),0,155).'...';

            $linkText = ($data->template_name == 'audio')? '<span style="width: 50px; height: 34px; position: relative; top: 10px; margin-top: -15px; margin-right: 6px; display: inline-block; background: url(\'http://cdn.seizethemarket.com/assets/images/play_button.png\') 0 79px"></span>Listen Now / Read More' : 'Read More';
            echo '<br>'.CHtml::link($linkText, array('/front/site/page', 'pageUrl'=>$data->url), $htmlOptions=array('class'=>'read-more-button','style'=>'col-sm-6'));?>
        </div>
    </div>
</div>