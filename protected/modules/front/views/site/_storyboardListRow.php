<?php
    $Content = CJSON::decode($data->template_data);
    $Transaction = $data->transaction;
    $Address = $Transaction->address;
    $link = '/storyboard/'.$data->url.'/'.$data->id;
    if(!empty($Transaction)):
?>
        <div class="property span12">
            <div class="row">
                <div class="image span6">
                    <div class="content">
                        <a href="<?php echo $link; ?>"></a>
                        <?php //1, 4

                            if(!empty($Content['photo-1'])) {
                                $thumbnailLocation = $Content['photo-1'];
                            } elseif(!empty($Content[1]['plaintext'])) { //this condition to be deprecated after new cms key based structure is seasoned
                                $thumbnailLocation = $Content[1]['plaintext'];
                            } elseif(!empty($Content['photo-2'])) {
                                $thumbnailLocation = $Content['photo-2'];
                            } elseif(!empty($Content[4]['plaintext'])) { //this condition to be deprecated after new cms key based structure is seasoned
                                $thumbnailLocation = $Content[4]['plaintext'];
                            } else {
                                $thumbnailLocation = 'http://cdn.seizethemarket.com/assets/images/default-placeholder.png';
                            }
                        ?>
                        <img class="property-thumbnail" src="<?php echo $thumbnailLocation; ?>" alt="">
                    </div><!-- /.content -->
                </div><!-- /.image -->

                <div class="body span6">
                    <div class="title-price row">
                        <div class="title span4">
                            <h2><a href="<?php echo $link; ?>"><?php echo $Address->address; ?></a></h2>
                        </div><!-- /.title -->

                        <div class="price">
                            <?php echo Yii::app()->format->formatDollars($Transaction->getFieldValue('price_current')); ?>
                        </div><!-- /.price -->
                    </div><!-- /.title -->

                    <div class="location"><?php echo $Address->city; ?>, <?php echo AddressStates::getShortNameById($Address->state_id); ?> <?php echo $Address->zip; ?></div><!-- /.location -->
                    <div class="area">
                        <span class="value"><?php echo $Transaction->getFieldValue('sq_feet'); ?></span><!-- /.value -->
                        <span class="key">SF</span><!-- /.key -->
                    </div><!-- /.area -->
                    <div class="bedrooms"><div class="content"><?php echo $Transaction->getFieldValue('bedrooms'); ?> Beds</div></div><!-- /.bedrooms -->
                    <div class="bathrooms"><div class="content">
                            <?php echo $Transaction->getFieldValue('baths');
                                if($halfBaths = $Transaction->getFieldValue('half_baths')) {
                                    if($halfBaths == 1) {
                                        echo '&frac12;';
                                    } else {
                                        echo '.'.$halfBaths;
                                    }
                                }
                            ?> Baths
                        </div>
                    </div><!-- /.bathrooms -->
                    <p><?php
                        $aboutHome = $Content[2]['plaintext'];
                        $aboutHome = preg_replace('/ style="(.*)"/', '', $aboutHome);

        //                $aboutHome = preg_replace('/font-size:(.*);/', '', $aboutHome);
        //                $aboutHome = preg_replace('/font-family:(.*);/', '', $aboutHome);
                        $maxLength = 500;
                        if(strlen($aboutHome) >$maxLength) {
                            $aboutHome = substr($aboutHome,0,$maxLength).'...';
                        }
                        echo $aboutHome;
                        ?>
                    </p>
                    <a href="<?php echo $link; ?>" class="btn btn-primary">View Storyboard</a>
                </div><!-- /.body -->
            </div><!-- /.property -->
        </div><!-- /.row -->
<?php endif; ?>