<div id="home-detail">
    <?php
    $similarPropertiesSidebarCount = 3;

    $this->widget(
        'admin_module.extensions.floatingFixed.FloatingFixed', array(
            'target'  => '.cta-buttons',
            'padding' => 0,
        )
    );

    echo $this->renderPartial('homeDetails/_ctaButtons', array('isFavorite' => $isFavorite));
    echo $this->renderPartial('homeDetails/_scripts', array('model' => $model, 'photoThumbnails' => $photoContent['thumbnails']));

    $this->renderPartial('_homeDetailsHeader');
    // echo $this->renderPartial('homeDetails/_header', array('model' => $model));
    echo $this->renderPartial(
        'homeDetails/_top', array(
            'model'        => $model,
            'isFavorite'   => $isFavorite,
            'photoContent' => $photoContent['fullSize'],
        )
    );
    echo $this->renderPartial(
        'homeDetails/_sidebar',
        array('similarProperties' => $similarProperties, 'model' => $model)
    );
    ?>
    <?php if (!empty($model->public_remarks)): ?>
        <div class="p-mt10 p-fl"
             style="width:675px; border: 1px solid #e5e5e5; font-size: 13px; border-radius: 4px; padding: 28px 23px 25px 35px; margin-bottom:5px; background: none repeat scroll 0% 0% whiteSmoke;">
            <h3>Property Description</h3>
            <hr style="margin: 0 0 5px 0;">
            <?php echo $model->public_remarks; ?>
        </div>
    <?php endif; ?>
    <div class="p-mt10 p-fl" style="width:735px;">
        <?php $this->widget(
            'zii.widgets.jui.CJuiTabs', array(
                'tabs' => array(
                    'Features'            => $this->renderPartial(
                        'homeDetails/_tabPropertyDetails', array(
                            'exteriorFeatures' => $exteriorFeatures,
                            'interiorFeatures' => $interiorFeatures,
                            'propertyFeatures' => $propertyFeatures,
                        ), true
                    ),
                    // 'Photos (16)' => $this->renderPartial('homeDetails/_tabInteriorFeatures', array('interiorFeatures' => $interiorFeatures), true),

                    'Schools'             => $this->renderPartial(
                        'homeDetails/_tabSchoolFeatures', array('schoolFeatures' => $schoolFeatures), true
                    ),
                    'Map View'            => array(
                        'ajax' => '/map/' . $model->listing_id,
                        'id'   => 'map-tab'
                    ),
                    'Mortgage Calculator' => array(
                        'ajax' => '/mortgagecalculator/' . $model->price,
                        'id'   => 'mortgage-calculator-tab'
                    ),
                    //				'Mortgage Calculator' => array('content' => $this->renderPartial('homeDetails/_mortgageCalculator', array('model'=>$model), true), 'id'=>'mortgage-calculator-tab'),
                    //				'House Values' => array('ajax' => '/map/' . $model->listing_id, 'id' => 'house-values-tab'),
                ),
                'id'   => 'home-property-widget'
            )
        );
        ?>
        <div class="cta-message">
            Call <em><?php echo $officePhone; ?></em> for showings, questions, &amp; hot deals!
        </div>
        <div class="g12 p-mv10" style="margin-top: 20px;">
            <?php $this->widget(
                'front_widgets.RecentSavedHomesWidget.RecentSavedHomesWidget',
                array('similarProperties' => $similarProperties)
            );?>
        </div>
        <? $this->renderPartial('_homeDetailsFooter'); ?>

        <div id="mls-disclosure" style="color: #666; font-size: 11px; line-height: 1.3;">
            <?php $this->renderPartial('_mlsDisclosure'); ?>
        </div>
    </div>
	<?php echo $this->renderPartial('homeDetails/_dialogs', array('model' => $model, 'viewsMaxReached' => $viewsMaxReached)); ?>
</div>
