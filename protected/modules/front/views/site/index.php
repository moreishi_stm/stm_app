<style type="text/css">
    .chzn-container.chzn-container-single a {
        height: 25px;
        padding: 7px;
    }
    .chzn-container.chzn-container-single {
        font-size: 16px;
    }
    .chzn-container-single .chzn-single div b{
        top: 8px;
        position: relative;
    }
    .search-choice-close {
        top: 14px !important;
    }
    ul.chzn-results{
        margin:0 !important;
    }
    .chzn-container .chzn-results li {
        padding: 5px 6px !important;
        margin: 0 !important;
        list-style: none !important;
    }
    #video-widget .video-thumbnail-container{
        position: relative;
        margin-top: 8px;
        border: 3px solid transparent;
        width: 23%;
        padding: 4px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        text-align: center;
        float: left;
    }
    #video-widget .video-thumbnail-container img {
        width: 180px;
        margin-left: auto;
        margin-right: auto;
        display: block;
        padding-top: 10px;
    }
    #video-widget .video-thumbnail-container:hover {
        border: 3px solid #C20000;
        background: #FFEBEB;
    }
    #video-widget .video-thumbnail-container a h5 {
        clear: both;
        font-size: 15px;
        cursor: pointer;
        color: #444;
        text-decoration: none;
    }
    #video-widget .video-thumbnail-container a .play-button {
        background-image: url(http://cdn.seizethemarket.com/assets/images/play_button.png);
        position: absolute;
        top: 50px;
        left: 44px;
        width: 152px;
        height: 41px;
        background-position: 0px 44px;
    }
	
	@media screen and (max-width: 1000px) {
		#video-widget .video-thumbnail-container {
			width: 23%;
			min-width: 152px;
			border: none;
			padding: 0;
			margin: 2px;
		}
		#video-widget .video-thumbnail-container:last-of-type,
		#video-widget .video-thumbnail-container:nth-of-type(4){
			margin-right: 0px;
		}
		
		#video-widget .video-thumbnail-container a .play-button {
			left: 0;
		}
		#video-widget .video-thumbnail-container img {
			width: 100%;
			float: none;
		}
	}
	
	@media screen and (max-width: 660px) {
		#video-widget .video-thumbnail-container {
			width: 98%;
			min-width: 152px;
			border: none;
			padding: 0;
			margin: 0 2px 0 0;
		}
		
		#houseValues-box {
			width: 100%;
			height: auto;
			padding: 0;
		}
		
		#houseValues-container #houseValues-box div.row {
			padding: 10px;
		}
		
		#houseValues-box h1 {
			display: block;
			margin: 0 auto;
		}
	}
</style>
<div id="homepage">
	<?php
    $js = <<<JS
        $(document).ready(function () {
            $("#MlsProperties_neighborhood").ajaxChosen({
                type: 'POST',
                url: '/searchAutocompleteNeighborhood',
                dataType: 'json'
            }, function (data) {
                return data;
            });
        });
JS;
    Yii::app()->clientScript->registerScript('search-autocomplete-script', $js);

    $this->widget('admin_module.extensions.EChosen.EChosen', array(
                                                             'target' => 'select#MlsProperties_neighborhood',
                                                             'options'=>array('allow_single_deselect'=>true)));

    $this->widget('admin_module.extensions.validationEngine.ValidationEngine', array('selector'=>'form'));

	$this->widget('admin_module.extensions.carouFredSel.ECarouFredSel', array(
	    'id' => 'carousel',
	    'target' => '#home-carousel',
	    'config' => array(
	        'items' => 1,
	        'scroll' => array(
	            'items' => 1,
	            'easing' => 'swing',
	            'duration' => 1000,
	            'pauseDuration' => 4500,
	            'pauseOnHover' => true,
	        ),
	        'pagination'=>'#page-dots',
	    ),
	));
	?>

	<div id="home-carousel">
		<div id="search-container">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'action'=>array('/front/site/search'),
				'id'=>'search-form',
			)); ?>
				<div id="search-box">
					<div class="p-pb10 g8">
						<span class="title">Search Homes for Sale...</span>
                        <?php echo $form->textField($model, 'keywords', $htmlOptions = array(
							'id' => 'homePageSearchForHomesKeywords',
                                'placeholder'=>'Type a Keyword, Neighborhood, City, Street Name, Zip',
                            )); ?>
					</div>
					<div class="g12 p-vt">
						<?php echo $form->dropDownList($model, 'price_min', StmFormHelper::getPriceList(),$htmlOptions=array('empty'=>'Price Min', 'class'=>'price-min g3'));?>
						<?php echo $form->dropDownList($model, 'price_max', StmFormHelper::getPriceList(),$htmlOptions=array('empty'=>'Price Max', 'class'=>'price-max g3'));?>
						<?php echo $form->dropDownList($model, 'bedrooms', StmFormHelper::getBedroomList(),$htmlOptions=array('empty'=>'Bedrooms', 'class'=>'bedrooms g3'));?>
						<?php echo $form->dropDownList($model, 'baths_total', StmFormHelper::getBathList(),$htmlOptions=array('empty'=>'Baths', 'class'=>'baths g3'));?>
						<?php echo $form->dropDownList($model, 'sq_feet', StmFormHelper::getSqFeetList(),$htmlOptions=array('empty'=>'Sq. Feet', 'class'=>'sq-feet g3'));?>
						<?php echo CHtml::submitButton('Search', array('class'=>'button','style'=>'width:85px;height:32px;top:-2px;font-size:12px;')); ?>
					</div>
				</div>
			<?php $this->endWidget(); ?>
		</div>

		<div id="houseValues-container">
			<?php

			$formId           = Forms::FORM_HOUSE_VALUES;
			$SubmissionValues = new FormSubmissionValues($formId);
			$FormFields       = new FormFields;

			$form=$this->beginWidget('CActiveForm', array(
				'action'=>'/values',
				'id'=>'houseValues-form',
			)); ?>
				<div id="houseValues-box">
					<span class="title">What's Your Home Worth?...</span>
					<div class="p-pb5 g12">
						<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'validate[required] p-fl search g11', 'id'=>'homePageHomeWorthAddress', 'style'=>'','placeholder'=>'Address', 'data-prompt-position'=>'topLeft'));?>
					</div>
					<div class="g12 p-vt">
						<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('city')->id.']', $htmlOptions=array('class'=>'validate[required] p-fl search g5', 'style'=>'','placeholder'=>'City','data-prompt-position'=>'bottomLeft'));?><?php $SubmissionValues->data[$FormFields->getField('state')->id] = 'FL'; ?>
						<?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getField('state')->id.']', CHtml::listData(AddressStates::model()->findAll(), 'short_name', 'short_name'),$htmlOptions=array('empty'=>'State', 'class'=>'g2'));?>
						<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']', $htmlOptions=array('class'=>'validate[required,custom[postcodeUS]] p-fl search g2', 'style'=>'','placeholder'=>'Zip','data-prompt-position'=>'bottomLeft'));?>
						<?php echo CHtml::submitButton('Search', array('class'=>'button','style'=>'width:80px;height:41px;top:0px;font-size:12px;')); ?>
					</div>
				</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
	<script>
		$(function() {
			var currentCarouselWidth  = '960px';
			var currentCarouselHeight = '366px';
			var opened = true;
			
			$(window).resize(function() {
				if(parseInt($(window).width()) <= 1000) {
					$("#home-carousel").trigger('stop', true);
					
					$("#home-carousel").css('width','auto');
					$("#home-carousel").css('height','auto');
					
					$(".caroufredsel_wrapper").css('width','auto');
					$(".caroufredsel_wrapper").css('height','auto');
					
					$("#home-carousel").css('position','');
					
					$("#page-dots").css('display','none');
					opened = false;
				} else {
					if(!opened) {
						$("#home-carousel").trigger('play', true);
						
						$("#home-carousel").css('width',currentCarouselWidth);
						$("#home-carousel").css('height',currentCarouselHeight);

						$(".caroufredsel_wrapper").css('width',currentCarouselWidth);
						$(".caroufredsel_wrapper").css('height',currentCarouselHeight);

						$("#home-carousel").css('position','absolute');
						$("#page-dots").css('display','block');
						opened = true;
					}
				}
			});
			
			setTimeout(function() {
				if(parseInt($(window).width()) <= 1000) {
					$("#home-carousel").trigger('stop', true);
					$("#home-carousel").css('width','auto');
					$("#home-carousel").css('height','auto');
					$(".caroufredsel_wrapper").css('width','auto');
					$(".caroufredsel_wrapper").css('height','auto');
					
					$("#home-carousel").css('position','');
					$("#page-dots").css('display','none');
					opened = false;
				}
			}, 100);
			
		});
	</script>

	<div class="clearfix"></div>
	<div id="page-dots" class="pagination"></div>
    <div id="listenNow">
        <a href="/topics/radio">
            <img src="/images/radioShow.png" width="940px" alt="Christine Lee Radio Show"/>
        </a>
    </div>
    <div id="video-widget">
        <div class="video-thumbnail-container">
            <a href="/certified-pre-own-homes-in-the-news">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/7HRhL22FZ8g/mqdefault.jpg">
                <h5>CPO Home Selling Strategy in the News</h5>
            </a>
        </div>
        <div class="video-thumbnail-container">
            <a href="/customer-review-eric">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/VWemgoz51dI/mqdefault.jpg">
                <h5>Bad Past Experience, Want Something Different</h5>
            </a>
        </div>
        <div class="video-thumbnail-container">
            <a href="/listing-storyboard-in-the-news">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/URDT17QaqKg/mqdefault.jpg">
                <h5>Listing Storyboard is a MUST for Home Sellers</h5>
            </a>
        </div>
        <div class="video-thumbnail-container">
            <a href="/customer-review-michelle">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/5fogTnzUOaM/mqdefault.jpg">
                <h5>Buying & Selling Simultaneously</h5>
            </a>
        </div>
        <div class="clearfix"></div>
        <div class="video-thumbnail-container">
            <a href="/rewardsforheroes">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/XfS4L5HsTPc/mqdefault.jpg">
                <h5>Rewards for Heroes</h5>
            </a>
        </div>
        <div class="video-thumbnail-container">
            <a href="/hiring">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/fyhwHov0hv0/mqdefault.jpg">
                <h5>Real Estate Career Opportunity</h5>
            </a>
        </div>
        <div class="video-thumbnail-container">
            <a href="/customer-review-jeff-kelly">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/k4fQerl660o/mqdefault.jpg">
                <h5>Aggressive Marketing & Strong Negotiation</h5>
            </a>
        </div>
        <div class="video-thumbnail-container">
            <a href="/hot-staging-tips-help-your-home-sell-itself">
                <em class="play-button"></em>
                <img class="video-thumbnail" src="http://i1.ytimg.com/vi/VhjR3lkyaLU/mqdefault.jpg">
                <h5>New Construction Homes Mythbuster</h5>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="p-p10">
		<h1 style="font-size:18px">Jacksonville Homes</h1>
            <p>
            	Now is the best time in recent years to check out the Christine Lee Team website for  Jacksonville homes for sale. North Florida is still one of the fastest growing areas in the nation and these unbelievable prices won't be available forever. We feature the best buys on thousands of Jacksonville homes in every prime real estate development in this area. You can use this website's search features to help narrow your choices or contact us directly for superior custom service and professional representation for both buyers and sellers.
            </p>
            <p>
            	Check out our collection of informative videos designed to leave "no stone unturned" when it comes to providing hot tips, busting myths and common misconceptions, and offering helpful advice for those who are relocating, building new or just shopping for great investments in Jacksonville real estate. We have conveniently categorized our Jacksonville homes listings by type of communities, waterfront locations, golf courses, school districts, geographic areas and names of the developer's subdivisions.
            </p>
            <p>
            	If you are familiar with the greater Jacksonville marketplace, you will likely recognize the investment opportunities that are currently available. If not, our talented staff will be glad to provide the assistance you need to select the best property for you and your family. Jacksonville is the largest city by area in the continental United States making it unique in real estate development. With no scarcity of land, a broad range of builders had the luxury of developing prime locations with a variety of prices for custom-built homes.
            </p>
            <p>
            	Jacksonville offers residents a colorful mix of backgrounds in military, banking, insurance, corporate, manufacturing, healthcare, tourism, education, distribution, service companies, professional athletes and the fine arts. Let us help you get started. Contact the Christine Lee Team for personalized assistance with your search for Jacksonville homes for sale.
            </p>
	</div>
</div>
