<style type="text/css">
    #storyboard-list .property-thumbnail {
        width: 100%;
    }
    .properties-rows .property .title-price .price {
        color: rgb(8, 192, 0);
        font-size: 30px;
    }
    a.btn.btn-primary {
        text-decoration: none;

        display: inline-block;
        margin-bottom: 0;
        font-size: 14px;
        line-height: 20px;
        vertical-align: middle;
        cursor: pointer;


        -webkit-font-smoothing: antialiased;
        font-weight: normal;
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);


        -webkit-transition: background-color 0.2s ease-in;
        -moz-transition: background-color 0.2s ease-in;
        -o-transition: background-color 0.2s ease-in;
        transition: background-color 0.2s ease-in;
        background-image: none;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        text-shadow: none;
        background-color: #0581b5;
        background-image: url("/assets-real/img/arrow-right-white.png");
        background-position: 90% center;
        background-repeat: no-repeat;
        background-size: 8px 11px;
        border: 0px;
        color: #fff;
        padding: 15px 40px 15px 25px;
    }

    a.btn.btn-primary:hover {
        background-color: #045e83;
    }
</style>

<div class="container">
    <div id="main">
        <div class="row">
            <div class="span12">
                <h1 class="page-header">Listing Storyboards</h1>
                <div class="properties-rows">
                    <div class="row">
                        <?php
                            $this->widget('front_module.components.StmListView', array(
                                    'dataProvider' => $DataProvider,
                                    'template' => '{items}{pager}',
//                                    'template' => '{pager}{summary}{items}{pager}{summary}',
                                    'id' => 'storyboard-list',
                                    'ajaxUpdate' => false,
                                    'itemView' => '_storyboardListRow',
                                    'itemsCssClass' => 'datatables',
                                    'afterAjaxUpdate' => 'js:function(id,data) {
                                                                        $(\'#home-list\').replaceWith($(\'#home-list\', \'<div>\'+data+\'</div>\'));
                                                                        $(\'div.loading-container.loading\').remove();
                                                                    }',
                                )
                            );
                        ?>
                    </div><!-- /.row -->
                </div><!-- /.properties-rows -->
            </div>
        </div>
    </div>
</div>
