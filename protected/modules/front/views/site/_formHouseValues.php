<?php
$css = <<<CSS
    #houseValues-container {
        width: 955px;
        height: 230;
        margin-right: 10px;
        background: url('http://cdn.seizethemarket.com/assets/images/house_aerial.png');
        -moz-border-radius:4px;
        -webkit-border-radius:4px;
        border-radius:4px;
        background-position-x: -51px;
        float:left;
    }
		
	@media screen and (max-width: 1000px) {
		#houseValues-container {
			width: 100%;
			margin-right: 0;
		}
		
		#houseValues-container select
	}

	@media screen and (max-width: 1000px) {
		#houseValues-container select {
			width: 100%;
		}
		#houseValues-container .city.g95 {
			width: 100%;
		}
	}
	
    #houseValues-container input{
        font-size: 24px;
    }
    #houseValues-container select{
        height: 43px;
        margin-top: 1px;
        font-size: 17px;
    }
    #houseValues-box h1{
        font-size: 23px !important;
        margin-left: -20px;
    }
    #houseValues-box {
        position: relative;
        float: right;
        width: 500px;
        height: 200px;
        border-top-left-radius: 6px;
        -moz-border-radius-topleft: 6px;
        -webkit-border-top-left-radius: 6px;
        border-bottom-left-radius: 6px;
        -moz-border-radius-bottomleft: 6px;
        -webkit-border-bottom-left-radius: 6px;

        padding: 15px 0px 15px 50px;
        background: none repeat scroll 0% 0% rgba(255, 255, 255, 0.8);
    }
    div.errorMessage {
        display: inline-block;
    }
    div.inline {
        display: inline-block;
    }
    .error select {
        color: #000;
        background: none repeat scroll 0% 0% #FFB8B8;
        border: 1px solid #FF5757;
    }
		
	@media screen and (max-width: 660px) {
		#houseValues-box {
			float: none;
			width: 100%;
			height: auto;
			padding: 10px;
		}
	}
CSS;
Yii::app()->clientScript->registerCss('houseValuesWidgetWide', $css);

$formId           = Forms::FORM_HOUSE_VALUES;
$formPart = 1;
$formName = $formId.'_Part'.$formPart;
$SubmissionValues = new FormSubmissionValues($formName);
$SubmissionValues->formPart = $formPart;
$FormFields       = new FormFields;
$formAction = '/front/forms/houseValues/formId/'.$formId;

$form=$this->beginWidget('CActiveForm', array(
        'id'=>'house-values-form',
        'action'=>array($formAction),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnChange' => false,
            'validateOnSubmit' => true,
            'beforeValidate' => 'js:function(form, attribute) {
                $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                return true;
            }',
            'afterValidate' => 'js:function(form, data, hasErrors) {
                if (!hasErrors && data.status =="successPart1") {
                    window.location = "http://'.$_SERVER["SERVER_NAME"].'/values/address/" + $("#houseValues-box .address").val() + "/city/" + $("#houseValues-box .city").val() + "/state/" + $("#houseValues-box .state").val() + "/zip/" + $("#houseValues-box .zip").val() + "/status/2";
                } else {
                    $("div.loading-container.loading").remove();
                }
                return false;
            }',
        ),
    ));
?>
<div id="houseValues-box">
    <h1>FREE <?php echo date('F Y'); ?> House Values Report</h1>
    <div class="row p-pb5 g12">
        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'address g11','placeholder'=>'Address'));?>
        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
    </div>
    <div class="row">
        <div class="inline g6">
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('city')->id.']', $htmlOptions=array('class'=>'city g95','data-prompt-position'=>'bottomLeft:0,6', 'placeholder'=>'City'));?>
        </div>
        <div class="inline g2">
            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getField('state')->id.']', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name ASC')), 'short_name', 'short_name'),$htmlOptions=array('empty'=>'State','class'=>'state'));?>
        </div>
        <div class="inline g3">
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']', $htmlOptions=array('class'=>'zip', 'placeholder'=>'Zip', 'maxlength'=>5));?>
        </div>
        <div class="inline g5">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
        </div>
        <div class="inline g3">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
        </div>
        <div class="inline g3">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
        </div>
        <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
    </div>
    <div class="row center">
        <?php echo CHtml::submitButton('Get my Free House Values Report', array('class'=>'button g11', 'style'=>'font-size: 16px; width: 92%; ')); ?>
    </div>
</div>
<? $this->endWidget(); ?>