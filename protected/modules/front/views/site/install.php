<?php
    Yii::app()->getClientScript()->registerCss('installCss', <<<CSS
        #install-frame {
            padding-top: 10px;
        }

        #install-details {
            padding: 5px;
        }

        #install-details div.row {

        }

        #install-details div.row span {
            font-weight: bold;
        }

        #install-details div.row span.status.success {
            color: limegreen;
        }

        #install-details div.row span.status.error {
            color: indianred;
        }

        #install-details div.row span.status:before {
            color: black;
            font-weight: normal;
            content: '................................................................ ';
        }

        #install-details div.row span.details {
            font-weight: normal;
        }

        #install-details div.row span.details:before {
            content :'(';
        }

        #install-details div.row span.details:after {
            content :')';
        }

        #install-details div.details {
            border: 1px solid #d3d3d3;
            border-radius: 4px;
            background: #E9E9E9;
            font-size: 11px;
            font-weight: normal;
            padding: 5px;
        }
CSS
);
?>
<div id="install-frame">
    <h2>Installation Validation</h2>
    <hr />

    <div id="install-details" class="g12">
        <?php foreach ($installDetails as $detail): ?>
            <div class="row g12">
                <span class="description g5"><?php echo $detail->description;?></span>

                <?php echo CHtml::tag('span', $htmlOptions=array(
                    'class' => 'g5 status ' . (($detail->getIsValid()) ? 'success' : 'error'),
                ), $detail->getStatus()); ?>

                <?php if($detail->getDetails()): ?>
                    <div class="details g12">
                        <?php foreach($detail->getDetails() as $detailMsg): ?>
                            <div class="message"><?php echo $detailMsg; ?></div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
