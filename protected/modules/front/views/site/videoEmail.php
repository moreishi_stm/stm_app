<div class="row transitionfx" id="home-detail">
    <div class="col-lg-12 col-md-12 col-sm-12 page-layout no-padding">
        <div  style="margin-top: 40px;" class="col-lg-9">
            <?=$body?>
        </div>

        <?php if(!$hideCmsTags): ?>
            <div id="cms-topics-container" class="col-lg-3">
                <?
                $this->renderPartial('_sideBarHouseValues');

                Yii::import('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget');
                $this->widget('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget', array(
                        'areas' => [
                            ['name'=>'Top Communities', 'tag'=>'Top Communities Sidebar'],
                            ['name'=>'Top Areas','tag'=>'Top Areas Sidebar']
                        ]
                    ));

                Yii::import('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
                $this->widget('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
                ?>
                <?php if(!empty($cmsTags)): ?>
                    <div class="col-lg-12" style="padding: 8px; border: 1px solid #CCC; font-size: 16px; margin: 10px 0;">
                        Similar Topics:
                        <?php $allTags = '';
                        foreach($cmsTags as $cmsTag) {
                            $allTags .= ($allTags)? ', ' : '';
                            $allTags .= CHtml::link($cmsTag->name, '/topics/'.$cmsTag->url, $htmlOptions=array('style'=>'color: #2D78CC;'));
                        }
                        echo $allTags;
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php /*<div class="col-lg-3 col-md-12 col-sm-12">
		<?php $this->widget('front_module.components.widgets.WidgetFactoryWidget.WidgetFactoryWidget'); ?>
	</div>*/ ?>
</div>
