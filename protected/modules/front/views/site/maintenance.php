<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Maintenance',
);
?>
<div align="center">
	<h1>Excuse the Mess!</h1>
	<h3>We are doing routine maintenance on our site at this time.</h3>
</div>

<div class="error" align="center" style="padding-top: 20px;">
	<?php
		$goBackImage = CHtml::image($this->module->imageAssetsUrl.DS.'go_back.png', 'Go Back', $htmlOptions=array(
			'width'=>250,
		));
		echo CHtml::link($goBackImage, Yii::app()->request->getUrlReferrer());
	?>
	<div>
		<h2><?php echo CHtml::link('Go Back', Yii::app()->request->getUrlReferrer()); ?></h2>
	</div>
</div>