<hr />
<div id="top-info">
	<div>
		<span class="price"><?php echo Yii::app()->format->formatDollars($model['price'])?></span>
	    <? if ($model['sq_feet'] ==0) {$model['sq_feet'] = 1;}; ?>
		<span>(<?php echo Yii::app()->format->formatDollars($model['price']/$model['sq_feet'])?>/SF)</span>
	</div>
	<div>
		<span class="spec"><?php echo $model['bedrooms']?></span> Beds &nbsp; <span class="spec"><?php echo $model['baths_total']?></span> Baths &nbsp;
		<span class="spec"><?php echo Yii::app()->format->formatNumber($model['sq_feet'])?></span> Sq. Feet
	</div>
	<br />
	<div style="clear:both">
		<span class="spec-blue">
            <?php echo ($model->pool_yn) ? 'Pool' : ''; ?>
            <?php echo ($model->waterfront_yn) ? 'Waterfront' : ''; ?>
        </span>
	</div>
	<div>
		<span><?php echo $model['year_built']?> Yr Built<br /></span>
	</div>
	<div>
		<span><?php echo Yii::app()->format->formatDays($model['status_change_date'], array('daysLabel'=>'Days'))?><br /></span>
	</div>
	<div>
		<span>MLS #: <?php echo $model['listing_id']?><br /></span>
	</div>
</div>

<div id="top-photos">
	<img src="<?php echo $model->getPhotoUrl(1); ?>" height="370px" />
</div>
<div id="mls-disclosure-top" style="font-size: 11px;">
    <?php $this->renderPartial('_mlsDisclosureTop', array('property'=>$model)); ?>
</div>

<div id="comments">
	<label>Comments:</label> <?php echo $model->public_remarks;?>
</div>