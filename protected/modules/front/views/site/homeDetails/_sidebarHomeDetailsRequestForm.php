<div class="side-container request-container">
	<div class="request-form">
		<?php
		$formId = Forms::FORM_HOME_DETAILS;
		$model = new FormSubmissionValues(Forms::FORM_HOME_DETAILS);
		$FormFields = new FormFields;

		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'home-details-request-form',
		    'action' => array('/front/forms/showing/formId/' . $formId),
		    'enableAjaxValidation'   => true,
		    'enableClientValidation' => false,
		    'clientOptions' => array(
		    	'validateOnChange' => false,
		    	'validateOnSubmit' => true,
		        'inputContainer' => 'span',
				'afterValidate' => 'js:function(form, data, hasErrors) {
		                // Enable the submit button
		                var submitButton = $("#home-details-request-form").find(".home-details-dialog-submit-button");
		                submitButton.removeAttr("disabled");

		                $(".home-details-dialog-loading").removeClass("loading");

						if (!hasErrors) {
							// Action was successful
							Message.create("success", "Your Showing Request has been submitted!");
							return false;
						}
					}',
		    ),
		));
		?>
			<div>
				<h3 class="p-tc" style="margin: 4px 0 18px 0; font-size:20px; color:#222;">Call <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?></h3>
			</div>
			<div>
				<?php echo $form->dropDownList($model, 'data['. $FormFields->getField('reason')->id .']',array(
					4=>'Quick question...',
					3=>'Request a Showing',
					2=>'Request Neighborhood Information',
					0=>'Request Recent Sales',
					1=>'Request School Information',


				), $htmlOptions=array('class'=>'g12')); ?>
			</div>

			<div>
				<label>Message:</label>
				<span>
					<?php echo $form->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('class'=>'g12', 'placeholder'=>'Message','rows'=>3));?>
				</span>
				<?php echo $form->error($model, 'data['. $FormFields->getField('comments')->id .']'); ?>
			</div>
			<div>
				<span>
					<?php echo $form->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array(
						'placeholder'=>'First Name',
						'class'=>'g6',
					));?>
					<?php echo $form->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array(
						'class'=>'g6',
						'placeholder'=>'Last Name',
						'style'=>'width:44%;'
					));?>
				</span>
				<?php echo $form->error($model, 'data['. $FormFields->getField('first_name')->id .']'); ?>
				<?php echo $form->error($model, 'data['. $FormFields->getField('last_name')->id .']'); ?>
			</div>
			<div>
				<span>
		    		<?php echo $form->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'g12', ));?>
		    	</span>
				<?php echo $form->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
			</div>
			<div>
				<span>
	                <?php $this->widget('StmMaskedTextField', array(
	                    'model' => $model,
	                    'attribute' => 'data['. $FormFields->getField('phone')->id .']',
	                    'mask' => '(999) 999-9999',
	                    'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'g12'),
	                )); ?>
	            </span>
				<?php echo $form->error($model, 'data['. $FormFields->getField('phone')->id .']'); ?>
			</div>
			<div class="p-pt10 g12" style="margin-top:10px;">
				<input type="submit" class="button p-fr g12" value="Submit Request"/>
			</div>

		<? $this->endWidget(); //end CActiveForm ?>
	</div>
</div>