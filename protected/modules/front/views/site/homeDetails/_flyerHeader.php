<?php
	// Add a delay just to ensure that map gets printed.
	$js = <<<'JS'
	setTimeout(function() {
		window.print();
	}, 1000);
JS;
	Yii::app()->clientScript->registerScript('printWindow', $js);
?>
<div class="header">
	<div id="logo">
		<img src="<?php echo Yii::app()->user->imagesBaseUrl; ?>logo_admin.png" width="200px"/>
	</div>
<!--	<div id="agent-photo">-->
<!--		<img src="--><?php //echo $urlPath; ?><!--" height="100px"/>-->
<!--	</div>-->
	<div id="agent-info">
        <h2 class="name"><?php echo Yii::app()->getUser()->getSettings()->office_name; ?></h2>
        <span class="phone"><?php echo Yii::app()->getUser()->getSettings()->office_phone; ?></span>
        <span class="website"><?php echo 'http://'.Yii::app()->getUser()->getPrimaryDomain()->getVerboseName(); ?></span>
	</div>
</div>