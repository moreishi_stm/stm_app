<?
	$js = <<<JS
		$('#mortgage-calculator-button').click(function() {
			if($('#mortgage-calcualtor-fields').css('display') == 'none') {
				$('#mortgage-calcualtor-fields').show('slow');
			} else {
				var percent = $('#down-payment').val();
				percent = percent.replace('%','');
				percent = (1-(percent /100));

				var loanAmount = $('#price').val();
				loanAmount = loanAmount.replace('$','');
				loanAmount = loanAmount.replace(/,/g,'');
				loanAmount = loanAmount * percent;
				var interest = $('#interest').val();
				interest = interest.replace('%','');
				interest = interest /100/12;
				var months = $('#years').val();
				var payment = Math.round((loanAmount*interest)/(1-Math.pow(1+interest,(-1*months)))*100)/100;
				$('#monthly-payment').html('$'+payment);
			}
		});
JS;
	Yii::app()->clientScript->registerScript('sidebarScripts', $js);
?>
<div class="side-container">
	<?
		$interest = ($mortgageRates['response']['today']['thirtyYearFixed'])?$mortgageRates['response']['today']['thirtyYearFixed']: 3.75;
		$loanAmount = $model['price'] * .8;
		$years = 30;
		$payment = mortgagePayment($interest, $years, $loanAmount);
		function mortgagePayment($interest, $years, $loanAmount) {
			$interest = $interest/100/12;
			$months = $years*12;
			return floor(($loanAmount*$interest/(1-pow(1+$interest,(-1*$months))))*100)/100;
		}
	?>
	<div class="tool-mortgage">
		<em class="i_mortgage_calculator"></em>
		<h3>Mortgage Calculator</h3>
		<div id="mortgage-calcualtor-fields" style="display:none;">
			<div style="padding-top:8px;">
				<label>Price:</label>
				<input type="text" id="price" class="p-f0 g5" size="15" placeholder="Price" value="<?php echo Yii::app()->format->formatDollars($model['price'])?>">
			</div>
			<div>
				<label>Interest:</label>
				<input type="text" id="interest" class="p-f0 g3" size="5" placeholder="Interest" value="<?php echo $interest?>%">
			</div>
			<div>
				<label>Down Pmt:</label>
				<input type="text" id="down-payment" class="p-f0 g3" size="5" placeholder="Down Payment" value="20%">
			</div>
			<div>
				<label>Years:</label>
				<select id="years"><option value="360">30 years</option><option value="240">20 years</option><option value="180">15 years</option></select>
			</div>
			<div class="monthly-payment-row">
				<label>Est. Payment:</label>
				<span id="monthly-payment">$<?php echo $payment?></span>
			</div>
		</div>
		<div class="p-pt10" style="margin-top:10px;">
			<a href="javascript:void(0);" id="mortgage-calculator-button" class="gray icon button i_stm_calculator calculate-button">Calculate My Payment</a>
		</div>
	</div>
</div>