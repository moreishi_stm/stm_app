<div class="features-container">
    <h3>Exterior Features</h3>
    <ul class='feature_list'>
        <?
        if (isset($exteriorFeatures)) {
            ksort($exteriorFeatures);
            foreach($exteriorFeatures as $label=>$value) {
                echo '<li><label>',$label,'</label>',  str_replace(',','<br />',$value),'</li>';
            }
        } else {
            // echo 'No data found...';
        }
        ?>
    </ul>
</div>

<div class="features-container">
    <h3>Interior Features</h3>
    <ul class='feature_list'>
        <?
        if (isset($interiorFeatures)) {
            ksort($interiorFeatures);
            foreach($interiorFeatures as $label=>$value) {
                echo '<li><label>',$label,'</label>', str_replace(',','<br />',$value),'</li>';
            }
        } else {
            // echo 'No data found...';
        }
        ?>
    </ul>
</div>

<div class="features-container">
    <h3>Property Features</h3>
    <ul class='feature_list'>
        <?
        if (isset($propertyFeatures)) {
            ksort($propertyFeatures);
            foreach($propertyFeatures as $label=>$value) {
                echo '<li><label>',$label,'</label>', str_replace(',','<br />',$value),'</li>';
            }
        } else {
            // echo 'No data found...';
        }
        ?>
    </ul>
</div>
<div style="clear:both;"></div>