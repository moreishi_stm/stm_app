<?
$large_star_suffix = ($isFavorite) ? '' : '_empty';
$addRemoveClass = ($isFavorite) ? 'remove-favorites' : 'add-favorites';
?>

<style type="text/css">
	.carousel-container {
		position: relative;
	}
	.carousel-container em.icon_ribbon{
		left: 342px;
		top: -3px;
	}
	.carousel-container div.slide {
		position: relative;
	}
	.carousel-container div.slide div {
		background-color: rgba(0, 0, 0, 0.6);
		width: 100%;
		display: none;
		position: absolute;
		bottom: 0;
	}
	.carousel-container div.slide h4 {
		font-size: 35px;
		padding: 30px 0 0 100px;
	}
	.carousel-container div.slide p {
		font-size: 16px;
		padding: 0 0 30px 100px;
	}
	.carousel-container div.slide h4, .carousel-container div.slide p {
		color: white;
		margin: 0;
	}
	div.thumbnails-container {
	}
	div.thumbnails {
		text-align: center;
		width: 380px;
		float: left;
	}
	div.thumbnails img {
		cursor: pointer;
		border: 1px solid #ccc;
		background-color: white;
		padding: 4px;
		margin: 5px;
		display: inline-block;
		width: 70px;
	}
	div.thumbnails img:hover {
		background-color: #eee;
	}
	div.thumbnails img.selected {
		background-color: #D20000;
	}
	.clearfix {
		float: none;
		clear: both;
	}

</style>

<div id="top-photos">
	<div class="home-info p-fl">
		<div id="favorite-container" class="<?php echo $addRemoveClass?>">
			<em class="icon i_stm_star_large<?php echo $large_star_suffix?> favorite "></em>
		</div>
		<div>
			<div>
				<span class="price"><?php echo Yii::app()->format->formatDollars($model['price'])?></span>
			    <? if ($model['sq_feet'] ==0) {$model['sq_feet'] = 1;}; ?>
				<span>(<?php echo Yii::app()->format->formatDollars($model['price']/$model['sq_feet'])?>/SF)</span>
			</div>
		</div>
		<div class="p-clr p-mv10">
			<span class="spec"><?php echo Yii::app()->format->formatNumber($model['sq_feet'])?></span> Sq. Feet
            <?if($model['baths_half']){ echo '<br/>';}?>

			<span class="spec"><?php echo $model['bedrooms']?></span> Beds &nbsp; <span class="spec"><?php echo $model['baths_full']?></span> Baths &nbsp;
            <? if($model['baths_half']){ echo '<span class="spec">'. $model['baths_half'].'</span> Half Bath'; }?>
		</div>
		<div style="clear:both">
			<span class="spec-blue">
				<?php echo ($model->pool_yn) ? 'Pool' : '';?>
				<?php echo ($model->waterfront_yn) ? 'Waterfront' : '';?>
			</span>
		</div>
		<div class="p-pl10 g11">
			<span class="address"><?php echo $model->formatMlsAddress($model, $opt=array('lineBreak'=>true))?></span><br />
            <?php echo ($model['common_subdivision'])? 'Neighborhood: '.Yii::app()->format->formatProperCase($model['common_subdivision']).'<br />' :'';?>

			<hr />
            <?php if(!empty($model['year_built'])): ?>
			    Year Built: <?php echo $model['year_built']?> <br />
            <?php endif; ?>
			<?php if($model['status_change_date']>0 && Yii::app()->user->boardId != 2):
                echo Yii::app()->format->formatDays($model['status_change_date'], array('daysLabel'=>'Days on Market'))?><br />
            <?php endif; ?>
			MLS #: <?php echo $model['listing_id']?><br />
			<br />

			<div class="spec-green">
				<?php echo ($model['photo_count']);?> Photos Available!
			</div>
            <div class="cta-make-offer" style="clear: both; margin-top: 6px;">
                <a id="make-an-offer-button" class=" inline-button text-button" type="button"><em class="icon i_stm_dollar"></em>Make an Offer</a>
            </div>

            <?php $this->renderPartial('_mlsDisclosureTop', array('property'=>$model)); ?>

		</div>
	</div>
	<!--	<div id="carousel"><em class="icon_ribbon --><?php //echo $model->ribbonType;?><!--"></em></div>-->
	<div class="carousel-container">
		<em class="icon_ribbon <?php echo $model->ribbonType;?>"></em>
		<div id="carousel">
			<?php echo $photoContent; ?>
		</div>
		<div class="thumbnails-container">
			<div class="thumbnails" id="thumbnails"></div>
		</div>
	</div>
</div>