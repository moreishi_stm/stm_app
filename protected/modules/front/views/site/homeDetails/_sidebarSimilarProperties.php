<?php Yii::import('front_widgets.RecentSavedHomesWidget.RecentSavedHomesWidget'); ?>
<style type="text/css">
	.side-container.similar-properties {
		text-align: center;
	}
	.side-container.similar-properties .mini-carousel img{
		width: 145px;
		height: 107px !important;
		border: none !important;
	}
	.side-container.similar-properties .mini-carousel a.carousel{
		padding: 4px;
		background-color: white;
		float: left;
		border: 1px solid #ccc;
		height: 107px;
	}
	.side-container.similar-properties .mini-carousel a.carousel:hover{
		background-color: #C20000;
	}
	.side-container.similar-properties .mini-carousel div.image {
		float: left;
		margin: 10px 0 0 20px;
		position: relative;
	}
	.side-container.similar-properties .mini-carousel .photo-count {
		position: absolute;
		left: 5px;
		top: 88px;
		background: rgb(15, 192, 0);
		color: white;
		font-size: 10px;
		font-weight: bold;
		padding: 0 6px 0 6px;
		border: solid 1px #CCC;
	}
	.side-container.similar-properties .mini-carousel label {
		font-weight: bold;
		font-size: 12px;
		color: #555;
		clear: both;
		float: left;
		width: 155px;
		text-align: center;
	}
	.side-container.similar-properties .mini-carousel label .price {
		font-size: 18px !important;
}
</style>
<?php if($similarPropertyIds): ?>
    <div class="side-container similar-properties">
        <h3 style="font-style: italic;">others Also Viewed...</h3>
        <?php echo RecentSavedHomesWidget::printTabContainer($similarPropertyIds); ?>
    </div>
<?php endif; ?>
