<?
$this->widget('admin_module.extensions.validationEngine.ValidationEngine', array('selector'=>'form'));
$defaultPrice = 400000;
$defaultMillage = 1.575;
$defaultYearlyTaxes = ((($defaultPrice*.85)-50000)*$defaultMillage/100);
$defaultMonthlyTaxes = number_format($defaultYearlyTaxes/12,2);

$js = <<<JS
		$('#property-tax-calculator-button').click(function() {
			if($("form").validationEngine("validate")) {
				//add validation of fields, numeric only in price and rate

				var millage = $('#millage').val();
				millage = millage.replace('%','');
				millage = millage / 100;

				var taxBase = $('#price').val();
				taxBase = taxBase * .85;
				if($('#homestead').val() == 1)
					taxBase = taxBase-50000;

				var paymentYearly = Math.round(taxBase * millage);
				var paymentMonthly = paymentYearly/12;

				$('#yearly-payment').html('$'+paymentYearly.toFixed(2));
				$('#monthly-payment').html('$'+paymentMonthly.toFixed(2));
			}
		});
JS;
Yii::app()->clientScript->registerScript('propertyTaxCalculatorScript', $js);
?>
	<?
//	$interest = ($mortgageRates['response']['today']['thirtyYearFixed'])?$mortgageRates['response']['today']['thirtyYearFixed']: 3.75;
//	$loanAmount = $price * .8;
//	$years = 30;
//	$payment = mortgagePayment($interest, $years, $loanAmount);
//	function mortgagePayment($interest, $years, $loanAmount) {
//		$interest = $interest/100/12;
//		$months = $years*12;
//		return floor(($loanAmount*$interest/(1-pow(1+$interest,(-1*$months))))*100)/100;
//	}
	?>
	<div class="property-tax-calculator-tab-container">
		<form>
			<div id="property-tax-calcualtor-fields">
				<div>
					<label><em class="i_mortgage_calculator"></em></label>
					<h3>Property Tax Calculator</h3>
				</div>
				<div style="padding-top:8px;">
					<label>Price:</label>
					<input type="text" id="price" class="p-f0 g3 validate[required,custom[number]]" data-prompt-position="topRight" size="15" placeholder="Price" value="<?php echo $defaultPrice;?>">
				</div>
				<div class="row">
					<label>County:</label>
					<select id="county"><option value="1.575">St Johns (1.572% - 2.255%)</option><option value="1.818">Duval (1.818% - 1.965%)</option><option value="2.199">Clay (1.699% - 2.199%)</option></select>
				</div>
				<div class="row">
					<label>Rate:</label>
					<input type="text" id="millage" class="p-f0 g3 validate[required,custom[number]]" size="5" placeholder="Millage Rate" value="<?php echo $defaultMillage;?>">
				</div>
				<div class="row">
					<label>Homestead:</label>
					<select id="homestead"><option value="1">Yes</option><option value="0">No</option></select>
				</div>
				<div class="monthly-payment-row">
					<label>Est. Monthly Taxes:</label>
					<span id="monthly-payment">$<?php echo number_format($defaultMonthlyTaxes,2)?></span>
				</div>
				<div class="monthly-payment-row">
					<label>Est. Yearly Taxes:</label>
					<span id="yearly-payment">$<?php echo number_format($defaultYearlyTaxes,2)?></span>
				</div>
			</div>
			<div style="margin: 15px 0 0 245px;">
				<a href="javascript:void(0);" id="property-tax-calculator-button" class="btn wide calculate-button">Calculate Taxes</a>
			</div>
		</form>
	</div>