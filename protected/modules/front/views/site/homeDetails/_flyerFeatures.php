<div id="exterior-features">
    <h3>Exterior Features</h3>
    <ul class='feature_list'>
        <?
        if (isset($model->secondaryFeatures->exteriorFeatures)) {
            $exteriorFeatures = $model->secondaryFeatures->exteriorFeatures;
            ksort($exteriorFeatures);
            foreach($exteriorFeatures as $label=>$value) {
                echo '<li><label>',$label,':</label> ',  $value,'</li>';
            }
        } else {
            echo 'No information is currently available. Please call for details.';
        }
        ?>
    </ul>
</div>

<div id="interior-features">
    <h3>Interior Features</h3>
    <ul class='feature_list'>
        <?
        if (isset($model->secondaryFeatures->interiorFeatures)) {
            $interiorFeatures = $model->secondaryFeatures->interiorFeatures;
            ksort($interiorFeatures);
            foreach($interiorFeatures as $label=>$value) {
                echo '<li><label>',$label,':</label> ',  $value,'</li>';
            }
        } else {
            echo 'No information is currently available. Please call for details.';
        }
        ?>
    </ul>
</div>