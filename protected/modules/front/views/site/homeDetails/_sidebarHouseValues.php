<!--<div class="side-container">-->
<!--	<div class="tool-house-values">-->
<!--		<em class="i_house_values"></em>-->
<!--		<h3>What's Your Home Worth?</h3>-->
<!--		<div class="p-pt10" style="margin-top:10px;">-->
<!--			<a href="/values" id="house-values-button" class="gray button calculate-button">FREE House Value Report</a>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->

<?php
$css = <<<CSS
    #house-values-widget-container {
        border: 1px solid #CCC;
        position: relative;
        padding: 15px;
        overflow-x: hidden;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        background-color: #F7F7FF;
        -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 0 8px rgba(0,0,0,0.2);
        box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
        margin-top: 16px;
        float:left;
        width: 190px;
    }
    #house-values-widget-container img{
        position: absolute;
        right: 3px;
        top: 11px;
        width: 86px;
    }
    #house-values-widget-container .title{
        padding-top: 24px;
        font-family: 'Arial';
    }
    #house-values-widget-container .year{
        font-size: 40px;
        font-family: 'Arial Black';
        line-height: 1.2;
    }
    #house-values-widget-container h2{
        font-size: 25px;
        line-height: 1;
        color: black;
    }
    #house-values-widget-container .item{
        margin-top: 10px;
    }
    #house-values-widget-container .item a{
        color: blue;
        display: block;
        font-weight: bold;
        padding: 4px 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        margin-left: -4px;
    }
    #house-values-widget-container .item a:hover{
        background-color: yellow;
    }
CSS;
Yii::app()->clientScript->registerCss('houseValuesWidgetCss', $css);
?>
<br/>
<br/>
<div id="house-values-widget-container">
    <div class="title">
        <div class="year"><?php echo date('Y'); ?></div>
        <h2>House Values Report</h2>
    </div>
    <img src="http://cdn.seizethemarket.com/assets/images/houseValuesWidget.png" border="0">
    <div class="item">
        Find out what your property is
        worth in today’s market.
        <a href="/values">FREE House Values Report</a>
    </div>
    <div class="item">
        The most local and accurate
        market information.
    </div>
    <div class="item">
        Ask about our FREE Real Estate
        Check-up!
        <a href="/values">CLICK HERE</a>
    </div>
</div>