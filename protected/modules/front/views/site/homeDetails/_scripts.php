<?php
	$model['photo_count'] = ($model['photo_count'])? $model['photo_count'] : 1;
	$thumbnailContainerWidth = $model['photo_count'] * 89;

	$this->widget('admin_module.extensions.carouFredSel.ECarouFredSel', array('id'     => 'main-home-images-carousel',
	                                                                          'target' => '#carousel',
	                                                                          'config' => array('items' => 1,
	                                                                                            'scroll' => array(
		                                                                                            'fx' => 'crossfade',
	                                                                                            ),
	                                                                                            'auto'  => false,
	                                                                                            'onCreate'  => new CJavaScriptExpression('function(){
	                                                                                                $("#thumbnails").attr("style","width:'.$thumbnailContainerWidth.'px");
	                                                                                            }'),
		                                                                                        'pagination'=>array(
																									'container'=> '#thumbnails',
			                                                                                        'anchorBuilder' => new CJavaScriptExpression('function(nr) {
			                                                                                            var photoUrls = '.$photoThumbnails.';
			                                                                                            var photoUrl = photoUrls[nr-1];
																										return "<img src=\""+photoUrl+"\" />";
																									}'),
		                                                                                        ),
	                                                                            ),
																		)
	);