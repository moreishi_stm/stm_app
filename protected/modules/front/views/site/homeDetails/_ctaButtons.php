<?php

$contactId = Yii::app()->user->id;
$listingId = Yii::app()->request->getQuery('id');
    $jsSavedHomes = <<<JS
	$('.add-favorites').live('click', addFavorites);
	$('.remove-favorites').live('click', removeFavorites);

	function removeFavorites() {
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: '$listingId', action: 'remove' }, function(data) {
			//results....
		});
		$('#add-favorites-button').html($('#add-favorites-button').html().replace('Remove from Favorites','Add to Favorites'));
		$('#add-favorites-button').html($('#add-favorites-button').html().replace('i_stm_delete','i_stm_star'));
		$('#add-favorites-button').removeClass('remove-favorites').addClass('add-favorites');
		$('.saved-count').html($('.saved-count').html()-1);
		$('#favorite-container').html($('#favorite-container').html().replace('i_stm_star_large','i_stm_star_large_empty'));
		$('#favorite-container').removeClass('remove-favorites').addClass('add-favorites');
	}

	function addFavorites() {
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: '$listingId', action: 'add' }, function(data) {
			//results....
		});
		$('#add-favorites-button').html($('#add-favorites-button').html().replace('Add to Favorites', 'Remove from Favorites'));
		$('#add-favorites-button').html($('#add-favorites-button').html().replace('i_stm_star','i_stm_delete'));
		$('#add-favorites-button').removeClass('add-favorites').addClass('remove-favorites');
		$('.saved-count').html($('.saved-count').html()*1+1);
		$('#favorite-container').html($('#favorite-container').html().replace('i_stm_star_large_empty','i_stm_star_large'));
		$('#favorite-container').removeClass('add-favorites').addClass('remove-favorites');
	}
JS;
	Yii::app()->clientScript->registerScript('savedHomesJs', $jsSavedHomes);

	$jsPrintFlyer = <<<JS
	$('#print-flyer-button').live('click', printFlyer);

	function printFlyer() {
		window.open('/printFlyer/$listingId', 'Home Details Flyer', 'width=785,height=700,scrollbars=no');
	}
JS;
Yii::app()->clientScript->registerScript('printFlyerJs', $jsPrintFlyer);

$addRemoveClass = ($isFavorite) ? 'remove-favorites' : 'add-favorites';
$addRemoveButtonClass = ($isFavorite) ? 'i_stm_delete' : 'i_stm_star';
$addRemoveText = ($isFavorite) ? 'Remove from Favorites' : 'Add to Favorites';
?>

<div class="cta-buttons fixed">
	<a id="add-favorites-button" class=" inline-button text-button <?php echo $addRemoveClass?>"><em class="icon <?php echo $addRemoveButtonClass?>"></em><?php echo $addRemoveText?></a>
    <a id="make-an-offer-button" class=" inline-button text-button" type="button"><em class="icon i_stm_dollar"></em>Make an Offer</a>
	<a id="send-to-friend-button" class=" inline-button text-button" type="button"><em class="icon i_stm_mail_2"></em>Send to Friend</a>
	<a id="showing-request-button" class=" inline-button text-button" type="button"><em class="icon i_stm_key"></em>Showings Request</a>
	<a id="ask-question-button" class=" inline-button text-button" type="button"><em class="icon i_stm_question_2"></em>Ask a Question</a>
	<a id="property-tax-button" class=" inline-button text-button" type="button"><em class="icon i_stm_percent"></em>Property Tax</a>
	<a id="print-flyer-button" class=" inline-button text-button" type="button"><em class="icon i_stm_print"></em>Print Flyer</a>
</div>
