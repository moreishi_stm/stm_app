<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'forgot-password-form',
	'enableAjaxValidation' => false,
	'action' => array('/forgotPassword'),
));
?>
	<div class="row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<?php echo $form->textField($model, 'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>
	<div class="row submit">
		<button id="forgotpassword-form-button" class="wide" type="submit">Recover Password</button>
	</div>
<?php $this->endWidget(); ?>