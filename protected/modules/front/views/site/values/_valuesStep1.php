<div class="right first" style="position: relative;">
    <h1 style="line-height: 1.2;">FREE <?php echo date('F Y')?><br />House Values Report</h1>
    <h3>Free Home Evaluation sent to your Inbox!</h3>

    <div class="row">
        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('address').']', $htmlOptions=array('class'=>'address g11','placeholder'=>'Address'));?>
        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('address') . ']'); ?>
    </div>
    <div class="row">
        <div class="inline g6">
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('city').']', $htmlOptions=array('class'=>'city g95','data-prompt-position'=>'bottomLeft:0,6', 'placeholder'=>'City'));?>
        </div>
        <div class="inline g2">
            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getFieldIdByName('state').']', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name ASC')), 'short_name', 'short_name'),$htmlOptions=array('empty'=>'State','class'=>'state'));?>
        </div>
        <div class="inline g3">
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('zip').']', $htmlOptions=array('class'=>'zip', 'placeholder'=>'Zip', 'maxlength'=>5));?>
        </div>
        <div class="inline g5">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('city') . ']'); ?>
        </div>
        <div class="inline g3">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('state') . ']'); ?>
        </div>
        <div class="inline g3">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('zip') . ']'); ?>
        </div>
        <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
    </div>
    <div class="row center">
        <?php echo CHtml::submitButton('Get My Free House Values Report', array('class'=>'button')); ?>
    </div>
</div>
<div class="bottom-map">
    <img src="http://cdn.seizethemarket.com/assets/images/house_values_map.png" class="img-responsive" alt="" />
</div>