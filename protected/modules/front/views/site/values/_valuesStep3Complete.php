<div class="left complete" style="height: 230px;">
    <div class="success"><em></em>Congratulations!</div>
    <h2 style="font-size: 28px; line-height: 1; padding-top: 20px;"><?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')].'<br />'.$SubmissionValues->data[$FormFields->getFieldIdByName('city')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('state')].' '.$SubmissionValues->data[$FormFields->getFieldIdByName('zip')]; ?></h2>
</div>
<div class="right verify" style="width: 380px;">
    <div class="sent">Your <?php echo date('F Y');?> House Values Report <br/>will be sent to your Inbox!<br /><br /><br />For immediate assistance call <?php echo Yii::app()->user->settings->office_phone; ?>.</div>
</div>
<div>
    <iframe class="map success" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('city')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('state')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('zip')];?>&amp;t=h&amp;z=20&amp;output=embed"></iframe>
</div>