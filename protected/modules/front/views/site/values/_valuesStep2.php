<div class="left verify">
    <div class="description" style="">One Last Step for Your House Value Report...</div>
    <div>
        <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition'). ']', array('Excellent'=>'Excellent','Average'=>'Average','Below Average'=>'Below Average','Poor'=>'Poor'), $htmlOptions = array('style' => 'width: 352px;', 'placeholder' => 'Baths', 'empty'=>'Select Condition'));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('condition').']'); ?>
    </div>
    <div class="inline">
        <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']', StmFormHelper::getBedroomList(' Bedrooms'), $htmlOptions = array('style' => 'width: 175px;', 'empty'=>'Select Beds'));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('bedrooms').']'); ?>
    </div>
    <div class="inline">
        <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']', StmFormHelper::getHouseValuesBathList(), $htmlOptions = array('style' => 'width: 175px;', 'empty'=>'Select Baths'));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('baths').']'); ?>
    </div>
    <div>
        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('sq_feet').']', $htmlOptions=array('class'=>'', 'style'=>'height: 34px; font-size: 14px; font-size: 23px;','placeholder'=>'Approx Sq. Feet'));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('sq_feet').']'); ?>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <h2 style="font-size: 28px; line-height: 1;"><?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')].'<br />'.$SubmissionValues->data[$FormFields->getFieldIdByName('city')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('state')].' '.$SubmissionValues->data[$FormFields->getFieldIdByName('zip')]; ?></h2>

    <div class="inline">
        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('first_name').']', $htmlOptions=array('class'=>'name', 'data-prompt-position'=>'topLeft','placeholder'=>'First Name'));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('first_name').']'); ?>
    </div>
    <div class="inline">
        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('last_name').']', $htmlOptions=array('class'=>'name', 'data-prompt-position'=>'topLeft', 'placeholder'=>'Last Name'));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('last_name').']'); ?>
    </div>
    <div class="inline">
        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('email').']', $htmlOptions=array('class'=>'email name', 'style'=>'','placeholder'=>'Email'));?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('email').']'); ?>
    </div>
    <div class="inline">
        <?php $this->widget('StmMaskedTextField', array(
                'model' => $SubmissionValues,
                'attribute' => 'data['. $FormFields->getFieldIdByName('phone') .']',
                'mask' => '(999) 999-9999',
                'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'name'),
            )); ?>
        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('phone').']'); ?>
    </div>

    <?php
    $addressFields = array('address','city','state','zip');
    foreach($addressFields as $addressField) {
        echo $form->hiddenField($SubmissionValues, 'data['.$FormFields->getFieldIdByName($addressField).']', $htmlOptions=array('value'=>$$addressField));
    }
    ?><?php //$form->error($SubmissionValues, 'address'); $form->error($SubmissionValues, 'city'); $form->error($SubmissionValues, 'state'); $form->error($SubmissionValues, 'zip');
    ?>
    <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
    <?php echo CHtml::submitButton('Get Your Free Report Now', array('class'=>'button', 'style'=>'width: 355px; margin-top: 20px; margin-bottom: 25px;')); ?>
</div>
<div class="right verify">
    <h1><?php echo date('F Y'); ?> House Values Report</h1>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <iframe class="map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('city')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('state')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('zip')];?>&amp;t=h&amp;z=19&amp;output=embed"></iframe>
    <div style="clear: both; margin-bottom: 20px;"></div>
</div>