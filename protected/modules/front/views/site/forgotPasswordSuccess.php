<div id="login">
    <div id="forgot-password-container" class="">
        <h1 class="green" style="padding-bottom: 25px;"><em class="icon_large i_stm_success_large"></em>Successful Password Recovery!</h1>
        <h4>Your password has been sent to your email address.</h4>
        <h4>For immediate assistance, call <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?>.</h4>
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>