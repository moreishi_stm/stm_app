<?php
$css = <<<CSS
	#area h2 {
	    padding-top: 20px;
	    color: black;
	}
CSS;
Yii::app()->clientScript->registerCss('areaStyle', $css);
?>
<div id="area">
    <?php $this->renderPartial('_homeListHeader'); ?>
    <?php echo $areaMessage; ?>
	<h1><? echo Yii::app()->format->properCase($model->name) . ' Homes for Sale'; ?></h1>

	<div id="description">
		<h2><?php echo $model->title; ?></h2>

		<p><?php echo nl2br($model->description); ?></p>
	</div>

	<?php
		$this->renderPartial('_homeList', array(
				'DataProvider' => $DataProvider,
				'model' => $model,
			)
		);
	?>
    <?php $this->renderPartial('_homeListFooter'); ?>
	<div id="description-marker" class="p-clr p-p10"></div>
    <div id="mls-disclosure" style="color: #666; font-size: 11px; line-height: 1.3;">
        <?php $this->renderPartial('_mlsDisclosure'); ?>
    </div>
</div>