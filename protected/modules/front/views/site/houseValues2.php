<?php
	$domain = str_replace('www', '', $_SERVER['SERVER_NAME']);
 	$isYiiDebug = (YII_DEBUG) ? 1 : 0;
	$js = <<<JS

		if (!$isYiiDebug) {
			_gaq.push(["_trackEvent", "House Values Site", "View", "House Values Page View"]);
			_gaq.push(['_setDomainName','$domain']);
		}

		$('#submit-button').click(function() {
			if(!$isYiiDebug) {
				_gaq.push(['_trackEvent', 'House Values Site', 'Click', 'House Values Click Submit']);
			}
		});

		var placeholderSupported = !!( 'placeholder' in document.createElement('input') );

		if (placeholderSupported !== true) {
		    $('input')
		      .focus(function() {
		            if (this.value === this.defaultValue) {
		                this.value = '';
		            }
		      })
		      .blur(function() {
		            if (this.value === '') {
		                this.value = this.defaultValue;
		            }
		    });
		}
JS;
	Yii::app()->clientScript->registerScript('placeHolderSupport', $js);
?>
<style type="text/css">
	body {
		position: static;
		clear: both;
		left: auto;
		right: auto;
		width: 1000px;
		margin: 40px auto;
	}
	section#column1 {
		width: 1000px;
	}
	#body h3 {
		color: white;
		font-size: 30px;
		line-height: 1;
		text-align: center;
	}
	#body h4 {
		color: #CCC;
		font-size: 20px;
		text-align: center;
		font-weight: normal;
	}
	section#body > div.g12 {
		border-radius: 5px;
		padding: 30px;
		border: 2px solid #555;
		width: 500px;
		background: #222;
	}
	#body input, #body select {
		font-size: 20px;
		height: 25px;
	}
	#body select {
		height: 39px;
	}
	#body .contact input{
		font-size: 14px;
		height: 20px;
	}
	#body input[type="submit"] {
		height: 50px;
		padding-left: 30px;
		padding-right: 30px;
		padding: 10px 30px;
		width: 80%;
		color: white;
		text-shadow: 1px 1px 0 black;
		/*font-style: italic;*/
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffca39', endColorstr='#ee7934');
		background: -webkit-gradient(linear, left top, left bottom, from(#ffca39), to(#ee7934));
		background: -moz-linear-gradient(top, #ffca39, #ee7934);
		background: -o-linear-gradient(top, #ffca39, #ee7934);
		border-color: #FFEE00 #CFA723 #CFA723 #FAD900;
		border-width: 2px;
		-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
		-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
		box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
/*		-moz-box-shadow: 0px 2px 2px 0px #ccc;
		-webkit-box-shadow: 0px 2px 2px 0px #ccc;
		box-shadow: 0px 2px 2px 0px #ccc;
*/	}
	#body input[type="submit"]:hover {
		cursor: pointer;
	}
	div.report {
		width: 350px;
		height: 460px;
		top: 4px;
		left: -20px;
		position: relative;
		background: url(/images/houseValuesReport.png);
		float: left;
	}
	div.report span.subtitle {
		display: block;
		position: relative;
		top: 154px;
		left: 60px;
		line-height: 1.2;
		font-size: 12px;
	}
	div.report span.date {
		display: block;
		position: relative;
		top: 350px;
		left: 24px;
		font-size: 23px;
		font-weight: bold;
		text-align: center;
		width: 284px;
		background-color: #D20000;
		color: white;
		padding: 10px;
	}
	div.report span.address {
		display: block;
		position: relative;
		top: 320px;
		left: 138px;
		font-size: 12px;
		text-align: center;
	}
	.footer {
		font-size: 11px;
		color: #888;
		position: absolute;
		bottom: 0;
		left: 0;
		text-align: center;
		width: 100%;
		clear: both;
	}
</style>
<section id="body">
	<div class="report">
		<!-- <span class="subtitle">The Most Accurate, Up-to-date, Local Real <br />Estate Market Report for Northeast Florida!</span> -->
		<span class="date"><?php echo date('F Y');?></span>
		<!-- <span class="address">2593 Cherry Blossom Trail<br />&nbsp;&nbsp;Jacksonville, FL 32082</span> -->
	</div>
	<div class="g12">
		<?php
			$formId           = Forms::FORM_HOUSE_VALUES;
			$SubmissionValues = new FormSubmissionValues($formId);
			$FormFields       = new FormFields;

			$form = $this->beginWidget('CActiveForm', array(
				'id'=>'house-values-form',
			    'action' => array('/front/forms/houseValues/formId/'.$formId),
			    'enableAjaxValidation'   => true,
			    'enableClientValidation' => false,
			    'clientOptions' => array(
			    	'inputContainer' => 'span',
			    	'validateOnChange' => false,
			    	'validateOnSubmit' => true,
					'afterValidate' => 'js:function(form, data, hasErrors) {
						if (!hasErrors) {
							if (!'.$isYiiDebug.') {
								_gaq.push(["_trackEvent", "House Values Site", "Submit", "Jax House Values Submit Success"]);
							}

							// Action was successful
							Message.create("success", "Your request was sent successfully!");
							window.location = "/";
							return false;
						} else {
							if (!'.$isYiiDebug.') {
								_gaq.push(["_trackEvent", "House Values Site", "Submit", "Jax House Values Submit Error"]);
							}
						}
					}',
			    )
			));
		?>
		<div class="g12">
			<div class="g12">
				<?php //echo CHtml::image(Yii::app()->user->imagesBaseUrl.'logo_admin.png', 'Logo', $htmlOptions=array('class'=>'House Values','width'=>200)); ?>
				<h4>FREE House Values Report</h4>
				<h3>Find Out What Your Home Worth!</h3>
				<!-- <h4>The Most Accurate, Up-to-Date, Local Real Estate Market Report for Northeast Florida!</h4> -->
			</div>
			<div class="g12 p-p5"></div>

			<div class="g1"></div>
			<div class="g10">
				<span>
				    <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array(
				    	'placeholder'=>$FormFields->getField('address')->label,
				    )); ?>
				</span>
		 		<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('address')->id.']'); ?>
			</div>
			<div class="g1">&nbsp;</div>
			<div class="g1 p-clr">&nbsp;</div>
			<div class="g4">
				<span>
				    <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('city')->id.']', $htmlOptions=array(
				    	'placeholder'=>$FormFields->getField('city')->label, 'class'=>'g11',
				    )); ?>
				</span>
		 		<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('city')->id.']'); ?>
			</div>
			<div class="g3" style="text-align: center;">
				<span>
					<?php
						$SubmissionValues->data[$FormFields->getField('state')->id] = 9;

						echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getField('state')->id.']',
						CHtml::listData(AddressStates::model()->findAll(), 'id', 'name'),
						$htmlOptions = array(
						'empty' => $FormFields->getField('state')->label,
						'class'=>'g11 p-pl10',
					)); ?>
				</span>
		 		<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('state')->id.']'); ?>
			</div>
			<div class="g3">
				<span>
				    <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']', $htmlOptions=array(
				    	'placeholder'=>$FormFields->getField('zip')->label,
				    )); ?>
				</span>
		 		<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']'); ?>
			</div>
			<div class="g1"></div>
		</div>
		<div class="g12 p-p5"></div>
		<div class="g12 contact">
			<div class="g12">
				<div class="g1"></div>
				<div class="g5">
					<span>
					    <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array(
					    	'placeholder'=>$FormFields->getField('first_name')->label,
					    )); ?>
					</span>
			 		<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']'); ?>
				</div>
				<div class="g5">
					<span>
					    <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array(
					    	'placeholder'=>$FormFields->getField('last_name')->label, 'class'=>'g11 p-fr',
					    )); ?>
					</span>
			 		<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']'); ?>
				</div>
				<div class="g1"></div>
			</div>

			<div class="g12">
				<div class="g1"></div>
				<div class="g5">
					<span>
					    <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array(
					    	'placeholder'=>$FormFields->getField('email')->label,
					    )); ?>
					</span>
			 		<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('email')->id.']'); ?>
			 	</div>
				<div class="g5">
	    			<span>
	                    <?php $this->widget('StmMaskedTextField', array(
	                        'model' => $SubmissionValues,
	                        'attribute' => 'data['. $FormFields->getField('phone')->id .']',
	                        'mask' => '(999) 999-9999',
	                        'htmlOptions' => $htmlOptions=array('placeholder'=>$FormFields->getField('phone')->label,'class'=>'g11 p-fr'),
	                    )); ?>
	    			</span>
	                <?php echo $form->error($SubmissionValues, 'data['. $FormFields->getField('phone')->id .']'); ?>
				</div>
				<div class="g1"></div>
			</div>
		</div>

		<div align="center" style="padding: 5px;">
		    <input id="submit-button" type="submit" value="Get My Free Report Now!" style="margin-top: 30px;">
		</div>
		<?php $this->endWidget(); ?>
	</div>
</section>
<div class="footer">
	Not intended to solicit currently listed properties. Copyright <?php echo date('Y');?>. Christine Lee Team, Keller Williams Jacksonville Realty.
</div>
<?// if (!YII_DEBUG) { ?>
<!--	<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>-->
<!--	<script type="text/javascript">-->
<!--		  var _gaq = _gaq || [];-->
<!---->
<!--		  --><?php //$domain = str_replace('www', '', $_SERVER['SERVER_NAME']); ?>
<!--		  // CLee-->
<!--		  _gaq.push(['_setAccount', 'UA-1080541-25']);-->
<!--		  _gaq.push(['_setDomainName',--><?php //echo "'".$domain."'";?><!--]);-->
<!--		  _gaq.push(['_trackPageview']);-->
<!---->
<!--		  // Stm-->
<!--		  _gaq.push(['b._setAccount', 'UA-38180271-1']);-->
<!--		  _gaq.push(['b._trackPageview']);-->
<!---->
<!--		  (function() {-->
<!--		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;-->
<!--		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';-->
<!--		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);-->
<!--		  })();-->
<!--	</script>-->
<?// } ?>
