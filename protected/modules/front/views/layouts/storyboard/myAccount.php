<style type="text/css">
    thead {
        background: #DDD;
    }
    .odd {
        background: white;;
    }
    .even {
        background: #e2eff9;;
    }
    #activity-log-grid_c1, #activity-log-grid_c2, #storyboard-grid_c1 {
        text-align: left;
    }
</style>
<?php
$js = <<<JS
    $('.datatables').addClass('container');
;
JS;
Yii::app()->clientScript->registerScript('contactPortletScript', $js);

$this->beginContent('/layouts/storyboard/column1'); ?>
<div class="container">
    <?php echo $content?>
    <?php $this->printActivityLog(); ?>
</div>
<?php $this->endContent(); ?>