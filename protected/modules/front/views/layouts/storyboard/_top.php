<?php $css = <<<CSS
    .breadcrumb-wrapper {
        padding: 10px;
    }
    .breadcrumb li a, .breadcrumb-wrapper .account ul li a {
        font-size: 16px;
        color: #06A7EA;
    }
    .breadcrumb li a:hover, .breadcrumb-wrapper .account ul li a:hover {
        color: white;
    }
    .breadcrumb li a:hover {
        color: white;
    }
    .username {
        color: white;
        font-size: 16px;
        padding-right: 15px;
        line-height: 26px;
    }
}
CSS;
Yii::app()->clientScript->registerCss('lsbheader', $css);
?>
<div id="wrapper-outer" >
    <div id="wrapper">
        <div id="wrapper-inner">
            <!-- BREADCRUMB -->
            <div class="breadcrumb-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <ul class="breadcrumb pull-left">
                                <li><a href="/">Home</a></li>
                                <li><a href="/storyboard">View Storyboards</a></li>
                                <!--                                <li><a href="/myAccount">Login / My Account</a></li>-->
                            </ul><!-- /.breadcrumb -->

                            <div class="account pull-right">
                                <ul class="nav nav-pills">
                                    <?php echo (Yii::app()->user->isGuest)? '' : '<li class="username">Hi '.Yii::app()->user->first_name.'!</li>'; ?>
                                    <li><a href="/myAccount"><?php echo (Yii::app()->user->isGuest)? 'Login' : 'My Account'; ?></a></li>
                                    <?php echo (Yii::app()->user->isGuest)? '' : '<li><a href="/logout">Logout</a></li>'; ?>
                                </ul>
                            </div>
                        </div><!-- /.span12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.breadcrumb-wrapper -->

            <!-- CONTENT -->
            <div id="content">