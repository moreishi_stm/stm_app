<style type="text/css">
    <?php if($this->action->id == 'storyboard'): ?>
    #content { background-color: white; }
    <?php endif; ?>
    #listingStandard-template { margin-bottom: 20px; }
    #listingStandard-template .storyboard-header {
        width: 900px !important;
    }
    #listingStandard-template .listingstoryboard-logo {
        width: 285px !important;
        height: 85px !important;
    }
    #listingStandard-template #video-container iframe.stmcms {
        width: 900px;
        height: 502px;
    }
    #listingStandard-template .label {
        background: none;
        color: black;
        padding: 0;
        font-size: 17px;
        line-height: 22px;
    }
    .cms-comment-entry {
        color: black;
        font-size: 16px !important;
    }
    .hidden {
        display: none !important;
    }
    #sneak-preview-form input:not([type="submit"]) {
        font-size: 15px;
        height: 42px;
    }
</style>
<?php $this->beginContent('/layouts/storyboard/main'); ?>
<?php echo $content; ?>
<?php $this->endContent(); ?>