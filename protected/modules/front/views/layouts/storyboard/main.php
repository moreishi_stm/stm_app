<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/png">
    <link rel="stylesheet" href="/assets-real/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="/assets-real/css/bootstrap-responsive.css" type="text/css">
    <link rel="stylesheet" href="/assets-real/libraries/chosen/chosen.css" type="text/css">
    <link rel="stylesheet" href="/assets-real/libraries/bootstrap-fileupload/bootstrap-fileupload.css" type="text/css">
    <link rel="stylesheet" href="/assets-real/libraries/jquery-ui-1.10.2.custom/css/ui-lightness/jquery-ui-1.10.2.custom.min.css" type="text/css">
    <link rel="stylesheet" href="/assets-real/css/realia-blue.css" type="text/css" id="color-variant-default">
    <link rel="stylesheet" href="#" type="text/css" id="color-variant">

    <title><?php echo $this->pageTitle;?></title>
</head>

<body>

<? $this->widget('front_module.components.widgets.PageViewTrackerWidget.PageViewTrackerWidget'); ?>
<? $this->widget('admin_module.components.widgets.FlashMessageWidget.FlashMessageWidget'); ?>

<? $this->renderPartial('/layouts/storyboard/_top'); ?>

<?php echo $content?>

<? $this->renderPartial('/layouts/storyboard/_footer'); ?>

</body>
</html>