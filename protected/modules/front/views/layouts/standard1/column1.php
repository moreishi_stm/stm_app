<?php
$this->module->cssAssets
    = array(
    'style.icons.css',
    'style.buttons.css',
    'style.form.css',
    'style.grid.css',
    'style.colors.css',
    'style.pager.css',
    'style.homes.css',
    'style.jui.css',
);

$this->module->registerScripts(); ?>
<?php $this->beginContent('/layouts/standard1/main'); ?>
    <section id="column1">
        <div id="content" class="g12">
            <div id="<?php echo $this->pageColor?>">
                <?php echo $content; ?>
            </div>
        </div>
    </section>
<?php $this->endContent(); ?>