<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en" />
	<meta name=viewport content="width=device-width, initial-scale=1" />
	<link type="text/css" rel="stylesheet" href="/css/style.css" media="screen,projection,tv" />
    <title><?php echo ($this->pageTitle)? $this->pageTitle : Yii::app()->name;?></title>
    <?php echo (!empty($this->pageDescription)) ? '<meta name="description" content="'.htmlentities($this->pageDescription).'" />':'';?>
    <?php echo (!empty($this->pageKeywords)) ? '<meta name="keywords" content="'.htmlentities($this->pageKeywords).'" />':'';?>
	<link rel="icon" type="image/png" href="/images/favicon.ico" />
</head>

<body>

	<div id="pageWrapper">
		<?php $this->widget('front_module.components.widgets.PageViewTrackerWidget.PageViewTrackerWidget'); ?>
		<?php $this->widget('admin_module.components.widgets.FlashMessageWidget.FlashMessageWidget'); ?>

		<? $this->renderPartial('/layouts/_top'); ?>

		<?php echo $content?>

		<? $this->renderPartial('/layouts/_footer'); ?>
	</div>

</body>
</html>
