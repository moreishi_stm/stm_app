<?php
$this->module->cssAssets
    = array(
    'style.icons.css',
    'style.buttons.css',
    'style.form.css',
    'style.grid.css',
    'style.colors.css',
    'style.pager.css',
    'style.homes.css',
    'style.jui.css',
);

$this->module->registerScripts(); ?>

<?php $this->beginContent('/layouts/standard1/column1'); ?>
<div id="myaccount">
	<div id="myaccount-nav" class="g2">
		<?php	$this->widget('zii.widgets.CMenu', array(
			    'items'=>array(
//                    array('label'=>'My Property', null, 'itemOptions'=>array('class'=>'title'), 'items'=>array(
//                        array('label'=>'123 Plantaiton Oaks Drive', 'url'=>array('/front/myAccount/marketUpdates')),
//                    )),
                    array('label'=>'My Account', null, 'itemOptions'=>array('class'=>'title'), 'items'=>array(
                        array('label'=>'My Profile', 'url'=>array('/front/myAccount/profile')),
                        array('label'=>'My Documents', 'url'=>array('/front/myAccount/documents')),
                        array('label'=>'My Storyboards', 'url'=>array('/front/myAccount/storyboards'),'visible'=>Yii::app()->user->contact->hasStoryboard()),
                    )),
			        array('label'=>'My Market', null, 'itemOptions'=>array('class'=>'title'), 'items'=>array(
						array('label'=>'Market Updates', 'url'=>array('/front/myAccount/marketUpdates')),
			            array('label'=>'Saved Homes ('.Yii::app()->user->getSavedHomesCount().')', 'url'=>array('/front/myAccount/index')),
			            array('label'=>'Recently Viewed', 'url'=>array('/front/myAccount/recentlyViewed')),
			        )),
//			        array('label'=>'My Agent', null, 'itemOptions'=>array('class'=>'title'), 'items'=>array(
//			            array('label'=>'Contact My Agent', 'url'=>array('/front/myAccount/contactAgent')),
//			            array('label'=>'Showing Request', 'url'=>array('/front/myAccount/showingRequest')),
//			            array('label'=>'Refer a Friend', 'url'=>array('/front/myAccount/referFriend')),
//			        )),
			    ),
			    'id'=>'left-nav',
			));
		?>
    </div>
	<div id="myaccount-content" class="g10">
		<div class="p-ph10">
			<?php echo $content?>
        </div>
	</div>
    <?php $this->printActivityLog(); ?>
</div>
<?php $this->endContent(); ?>