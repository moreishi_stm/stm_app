<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en" />
	<link type="text/css" rel="stylesheet" href="/css/style.css" media="screen,projection,tv" />
	<link rel="icon" type="image/png" href="/images/favicon.ico" />
	<title><?php echo $this->pageTitle;?></title>
</head>

<body class="flyer">
	<?php echo $content?>
</body>
</html>