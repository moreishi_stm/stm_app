<?php
$this->module->cssAssets
    = array(
    'style.icons.css',
    'style.buttons.css',
    'style.form.css',
    'style.grid.css',
    'style.colors.css',
    'style.pager.css',
    'style.homes.css',
    'style.jui.css',
);

$this->module->registerScripts(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en" />
	<link rel="icon" type="image/png" href="/images/favicon.ico" />
	<title><?php echo $this->pageTitle;?></title>
</head>

<body>
	<? $this->widget('admin_module.components.widgets.FlashMessageWidget.FlashMessageWidget'); ?>
	<div id="pageWrapper">
        <?php echo $content?>
	</div>
</body>
</html>