<?php $this->module->cssAssets
        = array(
        'style.icons.css',
        'style.buttons.css',
        'style.form.css',
        'style.grid.css',
        'style.colors.css',
        'style.pager.css',
        'style.homes.css',
        'style.jui.css',
    );

    $this->module->registerScripts();
?>

<?php $this->beginContent('/layouts/standard1/main'); ?>
	<div id="contentWrapper">
		<div id="content" class="column2">
			<div id="<?php echo $this->pageColor?>">
				<?php echo $content?>
			</div>
		</div>
		<div id="sidebar">
			<? $this->widget('front_module.components.widgets.WidgetFactoryWidget.WidgetFactoryWidget'); ?>
		</div>
	</div>
<?php $this->endContent(); ?>