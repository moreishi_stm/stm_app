<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en" />
	<link rel="icon" type="image/png" href="/images/favicon.ico" />
	<title><?php echo $this->pageTitle;?></title>
</head>

<body>
	<? $this->widget('admin_module.components.widgets.FlashMessageWidget.FlashMessageWidget'); ?>
	<div id="pageWrapper">
		<?php echo $content?>
	</div>
	<div class="footer">
        <div class="asSeenOn"><label>As Seen on...</label><em></em></div>
		<div class="credits">© Copyright <?php echo date('Y');?>. Not intended to solicit currently listed properties.  Christine Lee Team, Keller Williams Jacksonville Realty.</div>
	</div>
	<? if (0) { ?>
		<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
		<script type="text/javascript">
			  var _gaq = _gaq || [];

			  <?php $domain = str_replace('www', '', $_SERVER['SERVER_NAME']); ?>
			  // CLee
			  _gaq.push(['_setAccount', 'UA-1080541-25']);
			  _gaq.push(['_setDomainName',<?php echo "'".$domain."'";?>]);
			  _gaq.push(['_trackPageview']);

			  // Stm
			  _gaq.push(['b._setAccount', 'UA-38180271-1']);
			  _gaq.push(['b._trackPageview']);

			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
		</script>
	<? } ?>
</body>
</html>