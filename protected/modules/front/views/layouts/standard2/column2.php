<?php
$this->module->cssAssets
    = array(
    'style.jui.css?v=5',
);
$this->module->registerScripts();

$this->beginContent('/layouts/standard2/main');

    echo $content;

$this->endContent();