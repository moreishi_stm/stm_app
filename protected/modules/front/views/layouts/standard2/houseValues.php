<?php
$this->module->cssAssets = array();
$this->module->registerScripts();

header('Access-Control-Allow-Origin: *');

$cdnUrl = $this->module->cdnUrl;
$cdnUrlDev = (YII_DEBUG) ? str_replace('seizethemarket.com','seizethemarket.local',$cdnUrl) : $cdnUrl;
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/inputmask/inputmask.js?v=1', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/inputmask/jquery.inputmask.js?v=1', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/jquery.toastmessage.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/stm_Message.js', CClientScript::POS_END);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en" />
	<link rel="icon" type="image/png" href="/images/favicon.ico" />
	<title><?php echo $this->pageTitle;?></title>

	<link href="<?= $cdnUrl ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<?= $cdnUrl ?>assets/css/jquery.minimalect.min.css?v=6" />

	<!-- Custom styles for this template -->
	<link href="<?= $cdnUrlDev ?>assets/css/style.css?v=15" rel="stylesheet">

	<link href="http://cdn.seizethemarket.com/assets/v1/assets/css/jquery.toastmessage.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<?php $this->widget('admin_module.components.widgets.FlashMessageWidget.FlashMessageWidget'); ?>
	<div id="pageWrapper">
        <?php echo $content?>
	</div>
    <?php $this->renderPartial('/layouts/_trackingCodes'); ?>
</body>
</html>