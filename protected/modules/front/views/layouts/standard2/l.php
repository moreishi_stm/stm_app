<?php die("HERE"); ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en" />
	<link rel="icon" type="image/png" href="/images/favicon.ico" />
	<title><?php echo $this->pageTitle;?></title>

	<link href="<?= $cdnUrl ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<?= $cdnUrl ?>assets/css/jquery.minimalect.min.css?v=6" />

	<!-- Custom styles for this template -->
	<link href="<?= $cdnUrl ?>assets/css/style.css?v=1" rel="stylesheet">

	<link href="http://cdn.seizethemarket.com/assets/v1/assets/css/jquery.toastmessage.css" />
</head>

<body>
	<?php $this->widget('admin_module.components.widgets.FlashMessageWidget.FlashMessageWidget'); ?>
	<div id="pageWrapper">
		<?php echo $content?>
	</div>
	<div class="footer">
        <div class="asSeenOn"><label>As Seen on...</label><em></em></div>
		<div class="credits">© Copyright <?php echo date('Y');?>. Not intended to solicit currently listed properties.  Christine Lee Team, Keller Williams Jacksonville Realty.</div>
	</div>
    <?php $this->renderPartial('/layouts/_trackingCodes'); ?>
</body>
</html>