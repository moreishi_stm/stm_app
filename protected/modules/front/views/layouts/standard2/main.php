<?php
header('Access-Control-Allow-Origin: *');

$cdnUrl = $this->module->cdnUrl;
$domainSuffix = (YII_DEBUG) ? 'local': 'com';
$cdnUrlDev = (YII_DEBUG) ? str_replace('seizethemarket.com','seizethemarket.local',$cdnUrl) : $cdnUrl;

Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/idangerous.swiper-2.1.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrlDev.'assets/js/ion-checkRadio/ion.checkRadio.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrlDev.'assets/js/owl.carousel.min.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/bbq.jquery.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/inputmask/inputmask.js?v=1', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/inputmask/jquery.inputmask.js?v=1', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/jquery.toastmessage.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/stm_Message.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/jquery.cycle2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/jquery.easing.1.3.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/jquery.parallax-1.1.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/helper-plugins/jquery.mousewheel.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/jquery.mCustomScrollbar.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/grids.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/jquery.minimalect.min.js?v=4', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/jquery.autocomplete.min.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/bootstrap.touchspin.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrl.'assets/js/home.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile($cdnUrlDev.'assets/js/script.js?v=10', CClientScript::POS_END);
?>

<!DOCTYPE html>
<html>
	<head>
        <title><?php echo ($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
		<?php echo (!empty($this->pageDescription)) ? '<meta name="description" content="' . htmlentities($this->pageDescription) . '" />' : ''; ?>
		<?php echo (!empty($this->pageKeywords)) ? '<meta name="keywords" content="' . htmlentities($this->pageKeywords) . '" />' : ''; ?>
		<meta http-equiv="x-dns-prefetch-control" content="on">
		<meta http-equiv="Content-Language" content="en" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php echo (!empty($this->pageMetaTags)) ? $this->pageMetaTags : ''; ?>

		<link rel="dns-prefetch" href="http://cdn.seizethemarket.<?= (YII_DEBUG) ? 'local' : 'com' ?>">
		<!-- Fav and touch icons -->
		<link rel="icon" type="image/png" href="http://cdn.seizethemarket.<?= (YII_DEBUG) ? 'local' : 'com' ?>/assets/images/favicon_house.ico"/>
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $cdnUrl ?>assets/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $cdnUrl ?>assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $cdnUrl ?>assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= $cdnUrl ?>assets/ico/apple-touch-icon-57-precomposed.png">

		<!-- Bootstrap core CSS -->
		<link href="<?= $cdnUrl ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="<?= $cdnUrl ?>assets/css/jquery.minimalect.min.css?v=6" />

		<!-- Custom styles for this template -->
		<link href="<?=$cdnUrlDev ?>assets/css/style.css?v=22" rel="stylesheet">

		<link href="http://cdn.seizethemarket.com/assets/v1/assets/css/jquery.toastmessage.css" />

		<link href="<?= $cdnUrlDev ?>assets/css/skin-12.css?v=27" rel="stylesheet">
		<?php if(Yii::app()->themeSettings['skin']): ?><link href="<?= $cdnUrlDev ?>assets/css/skin-<?=(Yii::app()->themeSettings['skin']); ?>.css?v=6" rel="stylesheet"><?php endif; ?>

		<?php if(!empty($this->module->customCss)): ?>
		<link href="<?=$this->module->customCss; ?>" rel="stylesheet">
		<?php endif; ?>

		<!-- Just for debugging purposes. -->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

		<!-- include pace script for automatic web page progress bar  -->

		<script>
			paceOptions = {
				elements: true
			};

            <? // custom insert until figure out how to do this better
            if(!YII_DEBUG && Yii::app()->user->clientId == 1): ?>
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="//v2.zopim.com/?XETz6CNoB7T8N7PNgJC2XIzMOnTFWkqC";z.t=+new Date;$.
                type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

            <? if(!YII_DEBUG && Yii::app()->user->clientId == 1 && Yii::app()->user->id): ?>
            $zopim(function() {
                $zopim.livechat.setName('<?=Yii::app()->user->fullName?>' + '<?=(($zopEmail = Yii::app()->user->contact->primaryEmail) ? ' - '.$zopEmail : '') ?>');
                $zopim.livechat.setEmail('<?=Yii::app()->user->contact->primaryEmail?>');
            });
            <? endif; ?>
            <? endif; ?>

		</script>
		<script src="<?= $cdnUrl ?>assets/js/pace.min.js"></script>
	</head>

	<body class="header-version-2">
        <?php //die("<pre>".print_r(Yii::app()->getWidgetFactory(), 1)); ?>

		<?php
        $this->renderPartial('/layouts/_topNav');

        // only display for home page
		$controller = Yii::app()->getController();
		$isHome = (($controller->id === 'site') && ($controller->action->id === 'index')) ? true : false;

		if($isHome) {
			Yii::import('stm_app.widgets.ContainerWidget.ContainerWidget');
			Yii::import('stm_app.widgets.BannerImageWidget.BannerImageWidget');
			$this->widget('stm_app.widgets.ContainerWidget.ContainerWidget', array(
				'id' => 'BannerImage',
				'enableTitle' => false,
				'widgets' => array(
					'stm_app.widgets.BannerImageWidget.BannerImageWidget' => array(
						'imageLocation' => $this->mainHomePageImage
					)
				)
			));
		} ?>
		<!--/.banner style2-->

		<!-- Start Content //-->
        <?php if($this->fullWidth): ?>
            <?php echo $content?>
        <?php else: ?>
            <div class="container <?php if(!$isHome): ?> headerOffset<?php endif; ?>">
                <?php echo $content?>
            </div>
        <?php endif; ?>
		<!-- End Content //-->

        <? if(Yii::app()->controller->action->id !== 'values' && !$isHome && (/*strpos($_SERVER['SERVER_NAME'], 'kwadvantage2') === false && strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') === false && strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') === false && strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') === false && strpos($_SERVER['SERVER_NAME'], 'kwsarasotasiestakey') === false && strpos($_SERVER['SERVER_NAME'], 'kwclassicrealty') === false && strpos($_SERVER['SERVER_NAME'], 'growwithkwrc') === false && strpos($_SERVER['SERVER_NAME'], 'kwrealtyboise') === false &&  strpos($_SERVER['SERVER_NAME'], 'demotl.') === false*/Yii::app()->user->client->type !== 'Brokerage')):?>
            <?php $this->renderPartial('/layouts/_footerHouseValues'); ?>
        <? endif;?>
		<footer>
			<div class="footer">
				<div class="container">
					<div class="row">
						<?php $this->renderPartial('/layouts/_footer'); ?>

						<?php /*<div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
							<h3> Stay in touch </h3>
							<ul>
								<li>
									<div class="input-append newsLatterBox text-center">
										<input type="text" class="full text-center" placeholder="Email ">
										<button class="btn  bg-gray" type="button"> Subscribe <i
												class="fa fa-long-arrow-right"> </i></button>
									</div>
								</li>
							</ul>
							<ul class="social">
								<li><a href="http://facebook.com"> <i class=" fa fa-facebook"> &nbsp; </i> </a></li>
								<li><a href="http://twitter.com"> <i class="fa fa-twitter"> &nbsp; </i> </a></li>
								<li><a href="https://plus.google.com"> <i class="fa fa-google-plus"> &nbsp; </i> </a></li>
								<li><a href="http://youtube.com"> <i class="fa fa-pinterest"> &nbsp; </i> </a></li>
								<li><a href="http://youtube.com"> <i class="fa fa-youtube"> &nbsp; </i> </a></li>
							</ul>
						</div>*/ ?>

						<div style="clear:both" class="hide visible-xs"></div>

						<div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
							<h3> Contact</h3>
							<ul>
								<li class="supportLi">
									<?php /* <p> Lorem ipsum dolor sit amet, consectetur </p> */ ?>
									<h4><a class="inline" href="callto:+<?=str_replace('/[^0-9]/','',Yii::app()->getUser()->getSettings()->office_phone);?>"> <strong> <i class="fa fa-phone"> </i> <?=Yii::app()->getUser()->getSettings()->office_phone;?> </strong> </a></h4>
									<?php /*<h4><a class="inline" href="mailto:support@christineleeteam.com"> <i class="fa fa-envelope-o"> </i>support@christineleeteam.com</a></h4>*/ ?>
								</li>
                                <li>
                                    <a href="/contact">Questions</a>
                                </li>
                                <li>
                                    <a href="/contact">Contact Us</a>
                                </li>
							</ul>
						</div>
					</div>


					<!--/.row-->
				</div>
				<!--/.container-->
			</div>
			<!--/.footer-->

			<div class="footer-bottom">
				<div class="container">
                    <?
                    if($mlsDisclosure = Yii::app()->user->board->website_disclosure_footer) {
                        eval('echo '.$mlsDisclosure);
                    }
                    ?>
					<p class="pull-left"> &copy; Copyright <?php echo date("Y"); echo ' | '.Yii::app()->user->settings->office_name.' | '.Yii::app()->user->settings->website_footer_contact; ?>
                        <br>Powered by <a href="http://www.SeizetheMarket.com">Seize the Market</a>
                    </p>

					<?php /* <div class="pull-right paymentMethodImg"><img height="30" class="pull-right" src="/images/tempImages/images/site/payment/master_card.png" alt="img">
					  <img height="30" class="pull-right" src="/images/tempImages/images/site/payment/visa_card.png" alt="img">
					  <img height="30" class="pull-right" src="/images/tempImages/images/site/payment/paypal.png" alt="img">
					  <img height="30" class="pull-right" src="/images/tempImages/images/site/payment/american_express_card.png" alt="img">
					  <img height="30" class="pull-right" src="/images/tempImages/images/site/payment/discover_network_card.png" alt="img">
					  <img height="30" class="pull-right" src="/images/tempImages/images/site/payment/google_wallet.png" alt="img">
					  </div> */ ?>
				</div>
			</div>
			<!--/.footer-bottom-->
		</footer>
        <?php $this->renderPartial('/layouts/_trackingCodes'); ?>
	</body>
</html>
