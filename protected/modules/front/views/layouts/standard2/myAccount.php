<?php
Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/standard2/assets/css/font-awesome.css');
// keep this blank array assignment in here
$this->module->cssAssets = array();

$this->module->registerScripts(); ?>

<?php $this->beginContent('/layouts/standard2/column1'); ?>

<style type="text/css">
    #myaccount h1.pagetitle{
        margin-top: 16px;
    }
    #sidebar-container{
        border: 1px solid #e9e9e9;
        padding: 18px 24px;
        margin-bottom: 30px;
    }
    #savedSearchList2 .panel-default > .panel-heading{
        border-top: none;
    }
    #savedSearchList2 .categoryProduct .btn{
        width: 44%;
    }
    #savedSearchList2 .categoryProduct .btn.btn-danger{
        width: 25px;
        color: #666;
        background: transparent;
        float: right;
    }
    #savedSearchList2 .categoryProduct .btn.btn-danger:hover{
        background: #C20000;
        color: white;
    }
    #profile-form .row{
        margin-left:0;
    }
</style>
<div class="row">
    <h1 class="pagetitle"><?php echo $page_title;?></h1>
    <div id="sidebar-container" class="col-lg-2 col-sm-3 col-sx-12">
        <h3>My Account</h3>
        <ul>
            <li><a href="/myAccount">My Favorites</a></li>
            <li><a href="/myAccount/savedSearches/">Saved Searches</a></li>
            <li><a href="/myAccount/storyboards">My Storyboards</a></li>
            <li><a href="/myAccount/documents">My Documents</a></li>
    <!--        <li><a href="/myAccount/subscriptions/">Email Preferences</a></li>-->
            <li><a href="/myAccount/profile/">Settings</a></li>
            <li><a href="/logout">Logout</a></li>
        </ul>
    </div>
    <div class="col-lg-10 col-sm-9 col-sx-12">
        <?php echo $content?>
    </div>
    <?php $this->printActivityLog(); ?>
</div>
<?php
$this->endContent();
