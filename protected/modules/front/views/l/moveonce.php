<?php
$this->widget('admin_module.extensions.placeholder.Placeholder');

$domain = str_replace('www', '', $_SERVER['SERVER_NAME']);
$isYiiDebug = (YII_DEBUG) ? 1 : 0;
$js = <<<JS
		if (!$isYiiDebug) {
			_gaq.push(["_trackEvent", "Move Once Program Site", "View", "Move Once Program View"]);
			_gaq.push(['_setDomainName','$domain']);
		}

		$('#submit-button').click(function() {
			if(!$isYiiDebug) {
				_gaq.push(['_trackEvent', 'Move Once Program Site', 'Click', 'Move Once Program Click Submit']);
			}
		});
JS;
Yii::app()->clientScript->registerScript('moveOnceJS', $js);
?>
<style type="text/css">
	body {
		margin: 0;
		font: normal 85%/160% arial, helvetica, sans-serif;
	}

	.top-area {
		background-color: #999;
		background-repeat: repeat-x;
		background-size: 100% 900px;
		background-image: -moz-linear-gradient(top, #444444 0%, #999999 100%);
		background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #444444), color-stop(100%, #999999));
		background-image: -webkit-linear-gradient(top, #444444 0%, #999999 100%);
		background-image: -o-linear-gradient(top, #444444 0%, #999999 100%);
		background-image: -ms-linear-gradient(top, #444444 0%, #999999 100%);
		background-image: linear-gradient(to bottom, #444444 0%, #999999 100%);
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#444444', endColorstr='#999999', GradientType=0);
		height: 600px;
	}
	.form-box {
		-moz-border-radius: 4px;
		border-radius: 4px;
		-moz-box-shadow: 4px 4px 4px #666;
		-webkit-box-shadow: 4px 4px 4px #666;
		box-shadow: 4px 4px 4px #666;
		position: relative;
		top: 50px;
		margin-left: auto;
		margin-right: auto;
		width: 900px;
		background: rgba(255, 255, 255, 0.95);
		padding: 20px 20px 20px 30px;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}
	.form-box .form-container {
		width: 100%;
		position: relative;
		height: 400px;
	}
	.form-box .main-image-container {
		height: 340px;
		width: 375px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-moz-box-shadow: 4px 4px 4px #666;
		-webkit-box-shadow: 4px 4px 4px #666;
		box-shadow: 4px 4px 4px #666;
		display: inline-block;
		margin-right: 20px;
		background: url('/images/l/moveonce.jpg');
	}

	.form-box .arrow {
		background: url('/images/houseValues/arrow.png');
		height: 131px;
		width: 234px;
		position: relative;
		/*left: -114px;*/
		top: 11px;
	}

	.form-box .right.first h1 {
		margin-top: 2px;
	}

	.form-box .left.verify h1 {
		margin-top: 0;
		padding-top: 15px;
	}

	.form-box h2 {
		font-weight: normal;
		margin-bottom: 25px;
	}

	.form-box .verify h2 {
		font-weight: bold;
		margin-bottom: 25px;
	}

	.form-box .right.first h2 {
		margin-top: 2px;
		margin-bottom: 0px;
	}

	.form-box .right.first h3 {
		margin-top: 2px;
		font-style: italic;
	}

	.form-box .row {
		margin-bottom: 4px;
	}

	.form-box .center {
		text-align: center;
	}

	.form-box .right {
		display: inline-block;
	}
	.form-box .right.first {
		width: 490px;
		display: inline-block;
		position: absolute;
		top: 0;
		right: 0;
		float: right;
	}

	.form-box input {
		font-size: 14px;
		border: 1px solid #999;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		border-radius: 4px;
		padding: 6px;
		font-family: Arial;
	}

	.form-box select {
		height: 32px;
		margin-top: 1px;
		font-size: 16px;
	}

	.form-box form input.button {
		color: white;
		text-shadow: 0 1px 0 rgba(0, 0, 0, 1);
		background: #ff8875; /* Old browsers */
		/* IE9 SVG, needs conditional override of 'filter' to 'none' */
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmODg3NSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmYTAwMDAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
		background: -moz-linear-gradient(top,  #ff8875 0%, #fa0000 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ff8875), color-stop(100%,#fa0000)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  #ff8875 0%,#fa0000 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  #ff8875 0%,#fa0000 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  #ff8875 0%,#fa0000 100%); /* IE10+ */
		background: linear-gradient(to bottom,  #ff8875 0%,#fa0000 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff8875', endColorstr='#fa0000',GradientType=0 ); /* IE6-8 */

		border-color: #CC7676 #D20000 #A20000 #CC7676;
		width: 360px;
		height: 45px;
		top: 0px;
		font-size: 18px;
		margin-top: 10px;
		margin-right: 20px;
	}

	.form-box form .address {
		width: 460px;
	}

	.form-box form .city {
		width: 230px;
	}

	.form-box form .state {
		/*width:60px;*/
	}

	.form-box form .zip {
		width: 130px;
	}

	.form-box form .firstName, .form-box form .lastName, form-box form .email, form-box form .phone {
		width: 219px;
	}

	.bonus-area {
		width: 900px;
		height: 250px;
		margin-left: auto;
		margin-right: auto;
		padding-left: 20px;
	}

	.bonus-area ul {
		padding: 0;
		min-height: 250px;
	}

	.bonus-area ul li {
		display: inline-block;
		width: 260px;
		height: 235px;
		padding-right: 50px;
		float: left;
		list-style: none;
		position: relative;
	}

	.bonus-area ul li:last-child {
		padding-right: 0;
	}

	.bonus-area h3 {
		font-size: 24px;
		margin-bottom: 8px;
		line-height: 1.2;
	}

	.bonus-area .tagline {
		color: #D20000;
		font-weight: bold;
		font-size: 14px;
		margin-top: 15px;
		bottom: 0;
		position: absolute;
	}

	.bonus-area .forwardLink {
		font-size: 34px;
		color: black;
		position: relative;
		top: 100px;
		display: block;
		text-align: center;
	}

	.bonus-area .forwardMessage {
		font-size: 12px;
		color: #888;
		top: 120px;
		position: relative;
		text-align: center;
	}

	.footer {
		font-size: 11px;
		color: #888;
		margin-top: 25px;
		left: 0;
		text-align: center;
		width: 100%;
		clear: both;
		background-color: #e4e4df;
	}

	.footer .asSeenOn {
		font-family: "Times New Roman";
		color: #555;
		font-size: 16px;
		margin-right: 10px;
	}

	.footer .asSeenOn label {
		font-style: italic;
		top: -9px;
		position: relative;
		margin-right: 6px;
	}

	.footer .asSeenOn em {
		display: inline-block;
		height: 37px;
		width: 770px;
		background: url('../images/imgSprites.png') 0 0;
		margin-top: 20px;
	}

	.footer .credits {
		margin-top: 15px;
	}
</style>
<div class="top-area">
	<div class="form-box">
		<div class="form-container">
            <?php
            $formId = Forms::model()->findByName('Move Once Program')->id;
            $SubmissionValues = new FormSubmissionValues($formId);
            $FormFields = new FormFields;

			$form = $this->beginWidget('CActiveForm', array(
			                                          'id' => 'moveOnces-form',
			                                          'action' => array('/front/forms/houseValues/formId/' . $formId),
			                                          'enableAjaxValidation' => true,
			                                          'enableClientValidation' => false,
			                                          'clientOptions' => array(
				                                          'validateOnChange' => false,
				                                          'validateOnSubmit' => true,
				                                          'beforeValidate' => 'js:function(form, attribute) {
																var firstNameInput = $("#moveOnces-form #FormSubmissionValues_data_1");
																var firstName = $("#moveOnces-form #FormSubmissionValues_data_1").val();
																var lastNameInput = $("#moveOnces-form #FormSubmissionValues_data_2");
																var lastName = $("#moveOnces-form #FormSubmissionValues_data_2").val();
																var emailInput = $("#moveOnces-form #FormSubmissionValues_data_3");
																var email = $("#moveOnces-form #FormSubmissionValues_data_3").val();
																var phoneInput = $("#moveOnces-form #FormSubmissionValues_data_4");
																var phone = $("#moveOnces-form #FormSubmissionValues_data_4").val();

																var addressInput = $("#moveOnces-form #FormSubmissionValues_data_14");
																var address = $("#moveOnces-form #FormSubmissionValues_data_14").val();
																var cityInput = $("#moveOnces-form #FormSubmissionValues_data_16");
																var city = $("#moveOnces-form #FormSubmissionValues_data_16").val();
																var zipInput = $("#moveOnces-form #FormSubmissionValues_data_18");
																var zip = $("#moveOnces-form #FormSubmissionValues_data_18").val();

																if(address=="" || address.length < 6) {
																	alert("Please enter a valid Address.");
																	addressInput.focus();
																	return false;
																}

																if(city=="" || city.length < 4) {
																	alert("Please enter a valid City.");
																	cityInput.focus();
																	return false;
																}

																if(zip=="" || zip.length < 5) {
																	alert("Please enter a valid Zip.");
																	zipInput.focus();
																	return false;
																}

																if(firstName == "" || lastName == "") {
																	alert("Please enter your Full Name.");
																	if(firstName == "") {
																		firstNameInput.focus();
																		return false;
																	}

																	if(lastName == "") {
																		lastNameInput.focus();
																		return false;
																	}
																}

																if(firstName.length < 2 || lastName.length < 2) {
																	alert("Please enter a valid Full Name.");
																	return false;
																}

																if(email=="" || !validateEmail(email)) {
																	alert("Please enter a valid Email.");
																	emailInput.focus();
																	return false;
																}

																if(phone=="" || phone.length < 10 || !validatePhone(phone)) {
																	alert("Please enter a valid Phone #.");
																	phoneInput.focus();
																	return false;
																}

																$("#register-dialog-loading").addClass("loading");
																return true;

																function validateEmail(email) {
																   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
																   if(reg.test(email) == false) {
																	  return false;
																   }
																   return true;
																}

																function validatePhone(phone) {
																   var reg = /^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/;
																   if(reg.test(phone) == false) {
																	  return false;
																   }
																   return true;
																}

																Message.create("notice", "Processing your request...");
																var submitButton = $("#submit-button");
																submitButton.val("Please Wait...");
																submitButton.attr("disabled", "disabled");
																return true;
															}',
				                                          'afterValidate' => 'js:function(form, data, hasErrors) {
																if ($.isEmptyObject(data)) {
																	// Action was successful
									                                ' . $forwardScript . '
																} else {
																	var submitButton = $("#submit-button");
																	submitButton.val("Submit");
																	submitButton.removeAttr("disabled");
																}
																return false;
															}',
														),
													)); ?>

			<div class="video">
				<div class="main-image-container"></div>
			</div>
			<div class="right first">
				<h1>Move Once Program</h1>

				<h2>Move Once When Buying and Selling a Home!</h2>
				<h3>Find out how easy life would be if you only had to move once.</h3>


				<div class="row">
					<?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']', $htmlOptions = array('class' => 'validate[required] address', 'placeholder' => 'Address'));?>
					<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
				</div>
				<div class="row">
					<?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']', $htmlOptions = array('class' => 'validate[required] city', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'City', 'value' => $opt['city']));?>
					<? // @todo: update state to populate dynamically ?>
					<?php $SubmissionValues->data[$FormFields->getField('state')->id] = ($flStateId = 9); ?>
					<?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array('empty' => 'State', 'class' => 'state'));?>
					<?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']', $htmlOptions = array('class' => 'validate[required,custom[postcodeUS]] zip', 'placeholder' => 'Zip', 'value' => $opt['zip']));?>
					<div class="g4">
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
					</div>
					<div class="g4">
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
					</div>
					<div class="g4">
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
					</div>
				</div>
				<div class="row">
					<?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array('class' => 'validate[required] firstName', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'First Name'));?>
					<?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array('class' => 'validate[required] lastName', 'placeholder' => 'Last Name'));?>
					<div class="g6">
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
					</div>
					<div class="g6">
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
					</div>
				</div>
				<div class="row">
					<?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'validate[required,custom[email]] firstName', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'Email'));?>
					<?php $this->widget('StmMaskedTextField', array(
					                                          'model' => $SubmissionValues,
					                                          'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
					                                          'mask' => '(999) 999-9999',
					                                          'id' => $phoneFieldId,
					                                          'htmlOptions' => CMap::mergeArray($inputHtmlOptions, array('class' => 'validate[required, custom[phone]] lastName', 'placeholder' => 'Phone')),
					                                          )); ?>
					<div class="g6">
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
					</div>
					<div class="g6">
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
					</div>
				</div>
				<div class="row center">
					<?php echo CHtml::submitButton('Submit Now', array('class' => 'button', 'id' => 'submit-button')); ?>
					<div class="arrow"></div>
				</div>
			</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>
<div class="bonus-area">
	<ul>
		<li>
			<h3>Get a Cash Offer Now</h3>
			<span>Get a cash offer on your house that enables you to remove your debt and move forward with your life! Most sellers assume they can't sell without knowing all their options. Don't make that mistake and call now!</span>

<!--			<div class="tagline">- FREE Inside...</div>-->
		</li>
		<li>
			<h3>Buy While Selling</h3>
			<span>Many times you have to sell your house in order to qualify to buy the next one. The Move Once Program instantly qualifies you for your next home so you can solidify the purhcase of your new dream home.</span>

<!--			<div class="tagline">- FREE Inside...</div>-->
		</li>
		<li>
			<h3>Stay in the House</h3>
			<span>Find out how you can sell your house for cash and then continue to live in it until your next home is ready for purchase. This eliminates any stress and inconvenience of moving and uprooting your family or lifestyle multiple times.</span>

<!--			<div class="tagline">- FREE Inside...</div>-->
		</li>
	</ul>
</div>