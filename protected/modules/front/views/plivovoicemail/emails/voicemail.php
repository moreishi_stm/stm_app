<h1>Seize the Market - New Voicemail Alert</h1>

<h3>This caller left a voicemail message.</h3>

<table>
    <tr>
        <td>Called Number:</td>
        <td><?=Yii::app()->format->formatPhone($telephonyPhone->phone). ' ' .(($telephonyPhone->user_description) ? ' ('.$telephonyPhone->user_description.')' : '')?></td>
    </tr>
    <tr>
        <td>Call Hunt Group:</td>
        <td><?=$callHuntGroup->name?></td>
    </tr>
    <tr><td colspan="2"><hr/></td></tr>
    <tr><td colspan="2"><h2>Caller Information</h2></td></tr>
    <tr>
        <td>Caller Name:</td>
        <td><?=(!is_numeric(substr($_POST['CallerName'], 1)))? $_POST['CallerName'] : ''?>
            <?/*
            <?//=$phone?><br>
            <?//=$contact->getFullName()?><br>
            <?//=$address->address?><br>
            <?//=$address->city?>, <?//=$address->state->name?><br>
            <?//=$address->zip?>*/?>
        </td>
    </tr>
    <tr>
        <td>Caller Number:</td>
        <td><?=$fromPhone?></td>
    </tr>
    <tr>
        <td>Voicemail Length:</td>
        <td><?=(($_POST['Duration'])? $_POST['Duration'] : $_POST['RecordingDuration'])?> seconds</td>
    </tr>
    <tr><td colspan="2">
            <a href="http://www.<?=Yii::app()->user->primaryDomain->name?>/admin/voicemail/<?=$telephonyPhone->id?>?type=phone"><h1>Click here to View/Hear the Voicemail</h1></a>
            <a href="http://www.<?=Yii::app()->user->primaryDomain->name?>/admin/phone/callHistory/<?=$telephonyPhone->id?>"><h1>Click here to View/Log the Call</h1></a>
        </td>
    </tr>

    <?if($contacts):?>
    <tr><td colspan="2"><hr/></td></tr>
    <tr><td colspan="2"><h2>Contact Matches for Caller Phone Number</h2></td></tr>
        <?foreach ($contacts as $contact) {
            $links = '<a href="http://www.'.Yii::app()->user->primaryDomain->name.'/admin/contacts/'.$contact->id.'"> Contact</a>';
            if($transactions = $contact->transactions) {
                foreach($transactions as $transaction) {
                    $links .= ($links) ? '<br>': '';
                    $links .= '<a href="http://www.'.Yii::app()->user->primaryDomain->name.'/admin/'.$transaction->componentType->name.'/'.$transaction->id.'">'.$transaction->componentType->getSingularName().' - '.$transaction->status->name.' - View</a>';
                }
            }
            if($recruit = $contact->recruit) {
                $links .= ($links) ? '<br>': '';
                $links .= '<a href="http://www.'.Yii::app()->user->primaryDomain->name.'/admin/recruits/'.$recruit->id.'">View Recruit</a>';
            }

            echo '<tr>';
            echo '<td>'.$contact->fullName.'</td>';
            echo '<td>'.$links.'</td>';
            echo '</tr>';
        } ?>
    <?endif;?>

</table>