<?php
    $this->widget('admin_module.extensions.validationEngine.ValidationEngine', array('selector'=>'form'));

	$domain = str_replace('www', '', $_SERVER['SERVER_NAME']);
 	$isYiiDebug = (YII_DEBUG) ? 1 : 0;
	$js = <<<JS
		if (!$isYiiDebug) {
			_gaq.push(["_trackEvent", "House Values Site", "View", "House Values Page View"]);
			_gaq.push(['_setDomainName','$domain']);
		}

		$('#submit-button').click(function() {
			if(!$isYiiDebug) {
				_gaq.push(['_trackEvent', 'House Values Site', 'Click', 'House Values Click Submit']);
			}
		});

		var placeholderSupported = !!( 'placeholder' in document.createElement('input') );

		if (placeholderSupported !== true) {
		    $('input')
		      .focus(function() {
		            if (this.value === this.defaultValue) {
		                this.value = '';
		            }
		      })
		      .blur(function() {
		            if (this.value === '') {
		                this.value = this.defaultValue;
		            }
		    });
		}
JS;
	Yii::app()->clientScript->registerScript('placeHolderSupport', $js);
?>
<style type="text/css">
    body {
        margin:0;
        font: normal 85%/160% arial,helvetica,sans-serif;
    }
    .top-area {
        background: url('http://cdn.seizethemarket.com/assets/images/house_front_2.png');
        height: 500px;
    }
    .form-box {
        position: relative;
        top: 50px;
        margin-left: auto;
        margin-right: auto;
        width: 900px;
        background: rgba(255, 255, 255, 0.85);
        padding: 20px 20px 20px 30px;
        border-radius:5px;
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
    }
    .form-box:before {
        width: 950px;
        float: left;
    }
    .video{
        display: inline-block;
        margin-right: 20px;
    }
    .form-box .form-container{
        width: 100%;
        position: relative;
    }
    .form-box .right.first h1{
        margin-top: 2px;
    }
    .form-box .left.verify h1{
        margin-top: 0;
        padding-top: 15px;
    }
    .form-box h2{
        font-weight: normal;
        margin-bottom: 25px;
    }
    .form-box .verify h2{
        font-weight: bold;
        margin-bottom: 25px;
    }
    .form-box .right.first h2{
        margin-top: 2px;
        margin-bottom: 0px;
    }
    .form-box .right.first h3{
        margin-top: 2px;
        font-style: italic;
    }
    .form-box .row{
        margin-bottom: 10px;
    }
    .form-box .center{
        text-align: center;
    }
    .form-box .right{
        display: inline-block;
    }
    .form-box .left{
    }
    .form-box .left.verify,.form-box .left.complete{
        width: 490px;
        height: 370px;
        display: inline-block;
    }
    .form-box .right.verify{
        display: inline-block;
        float:right;
    }
    .form-box .right.first{
        width: 490px;
        display: inline-block;
        position: absolute;
        top: 0;
        right: 0;
        float: right;
    }
    .form-box input {
        font-size: 30px;
        border: 1px solid #999;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
        padding: 6px;
        font-family: Arial;
    }
    .form-box select {
        height: 49px;
        margin-top: 1px;
        font-size: 20px;
    }
    .form-box form input.button {
        color: white;
        text-shadow: 0 1px 0 rgba(0,0,0,1);
        background: -webkit-gradient(linear, left top, left bottom, from(#FF8875), to(#FA0000));
        background: -moz-linear-gradient(top, #FF8875, #FA0000);
        background: -o-linear-gradient(top, #FF8875, #FA0000);
        border-color: #CC7676 #D20000 #A20000 #CC7676;
        width:310px;
        height:45px;
        top:0px;
        font-size:16px;
        margin-top: 20px;
        margin-right: 20px;
    }
    .form-box form .address {
        width:460px;
    }
    .form-box form .city {
        width:230px;
    }
    .form-box form .state {
        width:75px;
        position: relative;
        top:-4px;
    }
    .form-box form .zip {
        width:130px;
    }
    .form-box .verify .description {
        font-size: 16px;
        margin-top: 15px;
        font-style: italic;
        margin-bottom: 10px;
        display: block;
    }
    .form-box .verify input, .form-box .complete input{
        font-size: 17px;
        width: 340px;
    }
    .form-box .verify input.name{
        width: 161px;
    }
    .form-box .success{
        font-size: 26px;
        font-weight: bold;
        margin-bottom:20px;
    }
    .form-box .success em{
        background: url(http://cdn.seizethemarket.com/assets/images/success_large.png);
        width: 42px;
        height: 42px;
        display: inline-block;
        margin: 0 10px -10px 0;
    }
    .form-box .sent{
        padding-top:20px;
        font-size: 20px;
        font-weight: bold;
        font-style: italic;
    }
    .form-box .complete .address{
        margin-top: 30px;
        margin-bottom:10px;
        margin-left:10px;
        font-size:20px;
    }
    .form-box .share{
        font-size:18px;
        margin-top: 35px;
    }
    .map {
        height: 370px;
        width: 400px;
    }
    .bonus-area {
        width: 900px;
        height: 250px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 20px;
    }
    .bonus-area ul{
        padding: 0;
        min-height: 250px;
    }
    .bonus-area ul li{
        display: inline-block;
        width: 260px;
        height: 235px;
        padding-right: 50px;
        float:left;
        list-style: none;
    }
    .bonus-area ul li:last-child{
        padding-right: 0;
    }
    .bonus-area h3 {
        font-size: 24px;
        margin-bottom: 8px;
        line-height: 1.2;
    }
    .bonus-area .tagline {
        color: #D20000;
        font-weight: bold;
        font-size: 14px;
        margin-top: 15px;
        bottom:0;
    }
    .bonus-area .forwardLink{
        font-size: 34px;
        color: black;
        position: relative;
        top: 100px;
        display: block;
        text-align: center;
    }
    .bonus-area .forwardMessage{
        font-size: 12px;
        color: #888;
        top: 120px;
        position: relative;
        text-align: center;
    }
    .footer {
        font-size: 11px;
        color: #888;
        margin-top: 25px;
        left: 0;
        text-align: center;
        width: 100%;
        clear: both;
        background-color: #e4e4df;
    }
    .footer .asSeenOn {
        font-family: "Times New Roman";
        color: #555;
        font-size: 16px;
        margin-right: 10px;
    }
    .footer .asSeenOn label{
        font-style: italic;
        top: -9px;
        position: relative;
        margin-right: 6px;
    }
    .footer .asSeenOn em{
        display: inline-block;
        height: 37px;
        width: 770px;
        background: url('../images/imgSprites.png') 0 0;
        margin-top: 20px;
    }
    .footer .credits {
        margin-top: 15px;
    }
</style>

<div class="top-area">
    <div class="form-box">
        <div class="form-container">
            <?php
            $formId           = Forms::FORM_HOUSE_VALUES;
            $SubmissionValues = new FormSubmissionValues($formId);
            $FormFields       = new FormFields;

            $step = null;
            $formAction = '';
            if(empty($_POST) && $_GET['step']!='complete')
                $step = '1';
            elseif ($_POST['step']=='verify' || isset($_POST[FormSubmissionValues][data])) {
                $step = 'verify';
                $formAction = '/front/forms/houseValues/formId/'.$formId;

                $address = ucwords($_POST[FormSubmissionValues][data][$FormFields->getField('address')->id]);
                $city = ucwords($_POST[FormSubmissionValues][data][$FormFields->getField('city')->id]);
                $state = $_POST[FormSubmissionValues][data][$FormFields->getField('state')->id];
//                $state_name = AddressStates::getNameById($_POST[FormSubmissionValues][data][$FormFields->getField('state')->id]);
                $zip = $_POST[FormSubmissionValues][data][$FormFields->getField('zip')->id];
                Yii::app()->user->setState('houseValues',$_POST[FormSubmissionValues][data]);
                $forwardUrl = 'http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"].'?step=complete';
                $forwardScript = 'window.location = "'.$forwardUrl.'";';
            } elseif ($_GET['step']=='complete') {
                $step = 'complete';
                if($houseValuesData = Yii::app()->user->getState('houseValues')) {
                    $address = $houseValuesData[$FormFields->getField('address')->id];
                    $city = $houseValuesData[$FormFields->getField('city')->id];
                    $state = $houseValuesData[$FormFields->getField('state')->id];
//                    $state_name = AddressStates::getNameById($state);
                    $zip = $houseValuesData[$FormFields->getField('zip')->id];
                    Yii::app()->user->setState('houseValues',null);
                }
                // @todo: create wrapper for getting the primary domain of an account
                Yii::app()->clientScript->registerScript('completeForwarder', 'setTimeout(function(){ window.location = "'.$opt['successLink'].'";}, 10000)');
            }

            if($step != '1') {
                $geoAddress = $address.', '.$city.', '.$state.' '.$zip;
                $mapUrl = 'http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q='.$geoAddress.'&amp;t=h&amp;z=18&amp;output=embed';
            }


            $form=$this->beginWidget('CActiveForm', array(
                'id'=>'houseValues-form',
                'action'=>array($formAction),
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'afterValidate' => 'js:function(form, data, hasErrors) {
							if ($.isEmptyObject(data)) {
								// Action was successful
                                '.$forwardScript.'
							}
							return false;
						}',
                    ),
                )); ?>
                <?php if($step=='1'){ ?>
                    <div class="video">
                        <object width="375" height="310">
                            <param name="movie" value="http://www.youtube.com/v/LBIdFaHNn6w">
                            <embed src="http://www.youtube.com/v/LBIdFaHNn6w&amp;rel=0&amp;color1=0x006699&amp;color2=0x54abd6&amp;border=1&amp;autoplay=1" type="application/x-shockwave-flash" wmode="transparent" width="375" height="310">
                        </object>
                    </div>
                    <div class="right first">
                        <h1><?php echo $opt['title'];?></h1>
                        <h2>Find Out What Your Home is Worth!</h2>
                        <h3>Free Home Evaluation sent to your Inbox</h3>
                        <div class="row">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'validate[required] address','placeholder'=>'Address'));?>
                        </div>
                        <div class="row">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('city')->id.']', $htmlOptions=array('class'=>'validate[required] city','data-prompt-position'=>'bottomLeft:0,6', 'placeholder'=>'City'));?>
                            <? // @todo: update state to populate dynamically ?>
                            <?php $SubmissionValues->data[$FormFields->getField('state')->id] = 'FL'; ?>
                            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getField('state')->id.']', CHtml::listData(AddressStates::model()->findAll(), 'short_name', 'short_name'),$htmlOptions=array('empty'=>'State','class'=>'state'));?>
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']', $htmlOptions=array('class'=>'validate[required,custom[postcodeUS]] zip', 'placeholder'=>'Zip'));?>
                            <?php echo CHtml::hiddenField('step', 'verify'); ?>
                        </div>
                        <div class="row center">
                            <?php echo CHtml::submitButton('Get My House Value', array('class'=>'button')); ?>
                        </div>
                    </div>
                <?php } elseif($step=='verify') { ?>
                    <div class="left verify">
                        <h1>Verify Your Property</h1>
                        <div class="description" style="">Analysis of your home value email to your Inbox!</div>
                        <h2><?php echo $address.'<br />'.$city.', '.$state.' '.$zip; ?></h2>
                        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array('class'=>'validate[required] name', 'data-prompt-position'=>'topLeft','placeholder'=>'First Name'));?>
                        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array('class'=>'validate[required] name', 'data-prompt-position'=>'topLeft', 'placeholder'=>'Last Name'));?>
                        <?php echo $form->error($SubmissionValues, 'first_name'); ?><?php echo $form->error($SubmissionValues, 'last_name'); ?>

                        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array('class'=>'validate[required,custom[email]] email', 'style'=>'','placeholder'=>'Email'));?>
                        <?php echo $form->error($SubmissionValues, 'email'); ?>
                        <?php $this->widget('StmMaskedTextField', array(
                            'model' => $SubmissionValues,
                            'attribute' => 'data['. $FormFields->getField('phone')->id .']',
                            'mask' => '(999) 999-9999',
                            'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'validate[required,custom[phone]]', 'data-prompt-position'=>'bottomRight'),
                        )); ?>
                        <?php echo $form->error($SubmissionValues, 'phone'); ?>
                        <?php
                            $addressFields = array('address','city','state','zip');
                            foreach($addressFields as $addressField) {
                                echo $form->hiddenField($SubmissionValues, 'data['.$FormFields->getField($addressField)->id.']', $htmlOptions=array('value'=>$$addressField));
                        }
                        ?>
                        <?php echo CHtml::hiddenField('step', 'complete'); ?>
                        <?php echo CHtml::submitButton('Verify Property', array('class'=>'button')); ?>
                    </div>
                    <div class="right verify">
                        <iframe class="map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $geoAddress;?>&amp;t=h&amp;z=19&amp;output=embed"></iframe>
                    </div>
                <?php } elseif($step=='complete') { ?>
                <div class="left complete">
                    <div class="success"><em></em>Congratulations!</div>
                    <div class="address"><?php echo ucwords($address); ?><br /> <?php echo ucwords($city); ?>, <?php echo $state;?> <?php echo $zip; ?></div>
                    <div class="sent">Your Report will be sent to your Inbox!<br /><br /><br />For immediate assistance call  <?php echo Yii::app()->user->settings->office_phone; ?>.</div>
<!--                    <div class="share">Share this report with a friend or resend it to yourself!</div>-->
                    <?//=$form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'email','placeholder'=>'Email','required'=>'required'));?>
                    <?php// echo CHtml::submitButton('Send Report Now', array('class'=>'button submit')); ?>
                </div>
                <div class="right verify">
                    <iframe class="map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $geoAddress;?>&amp;t=h&amp;z=20&amp;output=embed"></iframe>
                </div>
                <?php } ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<div class="bonus-area">

    <?php if($step==1 || $step=='verify'){ ?>
    <ul>
        <li>
            <h3>FREE Top Home Staging Tips</h3>
            <span>Ever wonder what the difference is between the homes that sit and the homes that SELL?! Find out what you need to do to stand out in the market to get your home SOLD!</span>
            <div class="tagline">- FREE Inside...</div>
        </li>
        <li>
            <h3>Homes that Sit vs. Homes that Sell</h3>
            <span>Find out how to make your home sell itself! Create the perfect first impression by staging to have buyers can fall in love with your home!</span>
            <div class="tagline">- FREE Inside...</div>
        </li>
        <li>
            <h3>FREE House Values Report</h3>
            <span>Find out what your home is worth in today’s market. Get the most up-to-date, accurate, local real estate market information. You'll be surprised by what's happening in your local market!</span>
            <div class="tagline">- FREE Inside...</div>
        </li>
    </ul>
    <?php } else { ?>
        <a class="forwardLink" href="<?php echo $opt['successLink'];?>">Click here to continue</a>
        <div class="forwardMessage">You will be forwarded in 8 seconds...</div>
    <?php }?>
</div>