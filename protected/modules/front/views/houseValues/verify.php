<?php
	$geoAddress = '2974 Hartley Rd, Jacksonville, FL 32257';
	$mapUrl = 'http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q='.$geoAddress.'&amp;t=h&amp;z=18&amp;output=embed';

	$domain = str_replace('www', '', $_SERVER['SERVER_NAME']);
 	$isYiiDebug = (YII_DEBUG) ? 1 : 0;
	$js = <<<JS
		// $('form').wl_Form({
		// 	ajax:false
		// });

		if (!$isYiiDebug) {
			_gaq.push(["_trackEvent", "House Values Site", "View", "House Values Page View"]);
			_gaq.push(['_setDomainName','$domain']);
		}

		$('#submit-button').click(function() {
			if(!$isYiiDebug) {
				_gaq.push(['_trackEvent', 'House Values Site', 'Click', 'House Values Click Submit']);
			}
		});

		var placeholderSupported = !!( 'placeholder' in document.createElement('input') );

		if (placeholderSupported !== true) {
		    $('input')
		      .focus(function() {
		            if (this.value === this.defaultValue) {
		                this.value = '';
		            }
		      })
		      .blur(function() {
		            if (this.value === '') {
		                this.value = this.defaultValue;
		            }
		    });
		}
JS;
	Yii::app()->clientScript->registerScript('placeHolderSupport', $js);
?>
<style>
	#houseValues #houseValues-container {
		background:#B4C9E0;
		height:420px;
	}
	#houseValues .verify .address {
		font-size:24px;
		font-weight: bold;
		line-height:1;
		margin:20px 0;
		display: block;
	}
    #houseValues .verify .description {
        font-size: 16px;
        margin-top: 20px;
        font-style: italic;
        float: left;
        margin-bottom: 10px;
    }
	#houseValues .verify .g4 {
		width: 31.5%;
	}
	#houseValues .verify label{
		font-size: 28px;
		font-weight: bold;
		text-decoration: underline;
	}
    #houseValues .verify input{
        font-size: 17px;
    }
	#houseValues .verify input.submit {
		width: 200px;
		height:41px;
		font-size:12px;
		float: left;
        margin-top: 20px;
	}
</style>

<?php

$formId           = Forms::FORM_HOUSE_VALUES;
$SubmissionValues = new FormSubmissionValues($formId);
$FormFields       = new FormFields;

$form=$this->beginWidget('CActiveForm', array(
	'action'=>'',
	'id'=>'houseValues-form',
)); ?>
	<div class="g6 verify" style="margin-left:20px;">
		<label>Verify Your Property</label>
        <span class="address" style="">2974 Hartley Rd<br /> Jacksonville, FL 32257</span>
        <div class="description" style="">Analysis of your home's approx. value email to your Inbox!</div>
		<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'p-fl search g4', 'style'=>'','placeholder'=>'First Name','required'=>'required'));?>
		<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'p-fl search g4', 'style'=>'','placeholder'=>'Last Name','required'=>'required'));?>
		<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'p-fl search g8', 'style'=>'','placeholder'=>'Email','required'=>'required'));?>
		<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'p-fl search g8', 'style'=>'','placeholder'=>'Phone','required'=>'required'));?>
		<?php echo CHtml::hiddenField('step', 'complete'); ?>
        <div class="g11">
            <?php echo CHtml::submitButton('Verify Property', array('class'=>'button submit')); ?>
        </div>
	</div>
	<iframe class="p-fr" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $geoAddress;?>&amp;t=h&amp;z=19&amp;output=embed"></iframe>
<?php $this->endWidget(); ?>