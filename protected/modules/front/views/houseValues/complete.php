<?php
	$geoAddress = '2974 Hartley Rd, Jacksonville, FL 32257';
	$mapUrl = 'http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q='.$geoAddress.'&amp;t=h&amp;z=18&amp;output=embed';

	$domain = str_replace('www', '', $_SERVER['SERVER_NAME']);
 	$isYiiDebug = (YII_DEBUG) ? 1 : 0;
	$js = <<<JS
		// $('form').wl_Form({
		// 	ajax:false
		// });

		if (!$isYiiDebug) {
			_gaq.push(["_trackEvent", "House Values Site", "View", "House Values Page View"]);
			_gaq.push(['_setDomainName','$domain']);
		}

		$('#submit-button').click(function() {
			if(!$isYiiDebug) {
				_gaq.push(['_trackEvent', 'House Values Site', 'Click', 'House Values Click Submit']);
			}
		});

		var placeholderSupported = !!( 'placeholder' in document.createElement('input') );

		if (placeholderSupported !== true) {
		    $('input')
		      .focus(function() {
		            if (this.value === this.defaultValue) {
		                this.value = '';
		            }
		      })
		      .blur(function() {
		            if (this.value === '') {
		                this.value = this.defaultValue;
		            }
		    });
		}
JS;
	Yii::app()->clientScript->registerScript('placeHolderSupport', $js);
?>
<style>
	#houseValues #houseValues-container {
		background:#B4C9E0;
		height:420px;
	}
	#houseValues .success{
		font-size: 26px;
		font-weight: bold;
		margin-bottom:20px;
	}
	#houseValues .success em{
		background: url(http://cdn.seizethemarket.com/assets/images/success_large.png);
		width: 42px;
		height: 42px;
		display: inline-block;
		margin: 0 10px -10px 0;
	}
	#houseValues .sent{
		padding-top:20px;
		font-size: 20px;
		font-weight: bold;
		font-style: italic;
	}
	#houseValues .share{
		font-size:18px;
		margin-top: 35px;
	}
	#houseValues .address{
		margin-top: 30px;
		margin-bottom:10px;
		margin-left:10px;
		font-size:20px;
	}
	#houseValues .verify input.submit {
		width: 200px;
		height:41px;
		top:20px;
		right: -10px;
		font-size:12px;
		float: right;
	}
</style>
<?php

$formId           = Forms::FORM_HOUSE_VALUES;
$SubmissionValues = new FormSubmissionValues($formId);
$FormFields       = new FormFields;

$form=$this->beginWidget('CActiveForm', array(
	'action'=>'/houseValues/complete',
	'id'=>'houseValues-form',
)); ?>
	<div class="g6 verify">
		<div class="success"><em></em>Congratulations!</div>
		<div class="address">2974 Hartley Rd<br /> Jacksonville, FL 32257</div>
		<div class="sent">Your Report has been Sent to your Inbox!</div>
		<div class="share">Share this report with a friend or resend it to yourself!</div>
		<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'p-fl search g11', 'style'=>'','placeholder'=>'Email','required'=>'required'));?>
		<div class="g11">
			<?php echo CHtml::submitButton('Send Report Now', array('class'=>'button submit')); ?>
		</div>
	</div>
	<iframe frameborder="0" scrolling="no" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $geoAddress;?>&amp;t=h&amp;z=19&amp;output=embed"></iframe>
<?php $this->endWidget(); ?>