<?php
$this->widget('admin_module.extensions.placeholder.Placeholder');

$domain = str_replace('www', '', $_SERVER['SERVER_NAME']);
$isYiiDebug = (YII_DEBUG) ? 1 : 0;
$js = <<<JS
		if (!$isYiiDebug) {
			_gaq.push(["_trackEvent", "House Values Site", "View", "House Values Page View"]);
			_gaq.push(['_setDomainName','$domain']);
		}

		$('#submit-button').click(function() {
			if(!$isYiiDebug) {
				_gaq.push(['_trackEvent', 'House Values Site', 'Click', 'House Values Click Submit']);
			}
		});
JS;
Yii::app()->clientScript->registerScript('houseValusJS', $js);
?>
<style type="text/css">
body {
    margin: 0;
    font: normal 85%/160% arial, helvetica, sans-serif;
}

.top-area {
    background-color: #999;
    background-repeat: repeat-x;
    background-size: 100% 900px;
    background-image: -moz-linear-gradient(top, #444444 0%, #999999 100%);
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #444444), color-stop(100%, #999999));
    background-image: -webkit-linear-gradient(top, #444444 0%, #999999 100%);
    background-image: -o-linear-gradient(top, #444444 0%, #999999 100%);
    background-image: -ms-linear-gradient(top, #444444 0%, #999999 100%);
    background-image: linear-gradient(to bottom, #444444 0%, #999999 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#444444', endColorstr='#999999', GradientType=0);
    height: 600px;
}
.form-box {
    -moz-border-radius: 4px;
    border-radius: 4px;
    -moz-box-shadow: 4px 4px 4px #666;
    -webkit-box-shadow: 4px 4px 4px #666;
    box-shadow: 4px 4px 4px #666;
}

div.form-container {
    height: 400px;
}

.form-box .main-image-container {
    height: 310px;
    width: 375px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -moz-box-shadow: 4px 4px 4px #666;
    -webkit-box-shadow: 4px 4px 4px #666;
    box-shadow: 4px 4px 4px #666;
	background: url('/images/houseValues/default.png');
}

.form-box .main-image-container.hamptonPark {
    background: url('/images/houseValues/hamptonPark.png');
}
.form-box .main-image-container.highlandGlen {
    background: url('/images/houseValues/highlandGlen.png');
}
.form-box .main-image-container.murabella {
    background: url('/images/houseValues/murabella.png');
}

.form-box .main-image-container.plantationOaks {
    background: url('/images/houseValues/plantationOaks.png');
}

.form-box .main-image-container.timberlinParc {
    background: url('/images/houseValues/timberlinParc.png');
}

.form-box .main-image-container.worthingtonPark {
	 background: url('/images/houseValues/worthingtonPark.png');
 }

.form-box .arrow {
	background: url('/images/houseValues/arrow.png');
	height: 131px;
	width: 234px;
	position: relative;
	/*left: -114px;*/
	top: 11px;
}

.form-box {
    position: relative;
    top: 50px;
    margin-left: auto;
    margin-right: auto;
    width: 900px;
    background: rgba(255, 255, 255, 0.95);
    padding: 20px 20px 20px 30px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.form-box:before {
    width: 950px;
    float: left;
}

.video {
    display: inline-block;
    margin-right: 20px;
}

.form-box .form-container {
    width: 100%;
    position: relative;
}

.form-box .right.first h1 {
    margin-top: 2px;
}

.form-box .left.verify h1 {
    margin-top: 0;
    padding-top: 15px;
}

.form-box h2 {
    font-weight: normal;
    margin-bottom: 25px;
}

.form-box .verify h2 {
    font-weight: bold;
    margin-bottom: 25px;
}

.form-box .right.first h2 {
    margin-top: 2px;
    margin-bottom: 0px;
}

.form-box .right.first h3 {
    margin-top: 2px;
    font-style: italic;
}

.form-box .row {
    margin-bottom: 4px;
}

.form-box .center {
    text-align: center;
}

.form-box .right {
    display: inline-block;
}

.form-box .left {
}

.form-box .left.verify, .form-box .left.complete {
    width: 490px;
    height: 370px;
    display: inline-block;
}

.form-box .right.verify {
    display: inline-block;
    float: right;
}

.form-box .right.first {
    width: 490px;
    display: inline-block;
    position: absolute;
    top: 0;
    right: 0;
    float: right;
}

.form-box input {
    font-size: 14px;
    border: 1px solid #999;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
    padding: 6px;
    font-family: Arial;
}

.form-box select {
    height: 32px;
    margin-top: 1px;
    font-size: 16px;
}

.form-box form input.button {
    color: white;
    text-shadow: 0 1px 0 rgba(0, 0, 0, 1);
    background: #ff8875; /* Old browsers */
    /* IE9 SVG, needs conditional override of 'filter' to 'none' */
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmODg3NSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmYTAwMDAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
    background: -moz-linear-gradient(top,  #ff8875 0%, #fa0000 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ff8875), color-stop(100%,#fa0000)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #ff8875 0%,#fa0000 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #ff8875 0%,#fa0000 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #ff8875 0%,#fa0000 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #ff8875 0%,#fa0000 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff8875', endColorstr='#fa0000',GradientType=0 ); /* IE6-8 */

    border-color: #CC7676 #D20000 #A20000 #CC7676;
    width: 310px;
    height: 45px;
    top: 0px;
    font-size: 16px;
    margin-top: 10px;
    margin-right: 20px;
}

.form-box form .address {
    width: 460px;
}

.form-box form .city {
    width: 230px;
}

.form-box form .state {
    /*width:60px;*/
}

.form-box form .zip {
    width: 130px;
}

.form-box form .firstName, .form-box form .lastName, form-box form .email, form-box form .phone {
    width: 219px;
}

.form-box .verify .description {
    font-size: 16px;
    margin-top: 15px;
    font-style: italic;
    margin-bottom: 10px;
    display: block;
}

.form-box .verify input, .form-box .complete input {
    font-size: 17px;
    width: 340px;
}

.form-box .verify input.name {
    width: 161px;
}

.form-box .success {
    font-size: 26px;
    font-weight: bold;
    margin-bottom: 20px;
}

.form-box .success em {
    background: url(http://cdn.seizethemarket.com/assets/images/success_large.png);
    width: 42px;
    height: 42px;
    display: inline-block;
    margin: 0 10px -10px 0;
}

.form-box .sent {
    padding-top: 20px;
    font-size: 20px;
    font-weight: bold;
    font-style: italic;
}

.form-box .complete .address {
    margin-top: 30px;
    margin-bottom: 10px;
    margin-left: 10px;
    font-size: 20px;
}

.form-box .share {
    font-size: 18px;
    margin-top: 35px;
}

#house-values-submit-button {
    font-size: 18px !important;
    width: 96%;
}
.map {
    height: 370px;
    width: 400px;
}

.bonus-area {
    width: 900px;
    height: 250px;
    margin-left: auto;
    margin-right: auto;
    padding-left: 20px;
}

.bonus-area ul {
    padding: 0;
    min-height: 250px;
}

.bonus-area ul li {
    display: inline-block;
    width: 260px;
    height: 235px;
    padding-right: 50px;
    float: left;
    list-style: none;
}

.bonus-area ul li:last-child {
    padding-right: 0;
}

.bonus-area h3 {
    font-size: 24px;
    margin-bottom: 8px;
    line-height: 1.2;
}

.bonus-area .tagline {
    color: #D20000;
    font-weight: bold;
    font-size: 14px;
    margin-top: 15px;
    bottom: 0;
}

.bonus-area .forwardLink {
    font-size: 34px;
    color: black;
    position: relative;
    top: 100px;
    display: block;
    text-align: center;
}

.bonus-area .forwardMessage {
    font-size: 12px;
    color: #888;
    top: 120px;
    position: relative;
    text-align: center;
}

.footer {
    font-size: 11px;
    color: #888;
    margin-top: 25px;
    left: 0;
    text-align: center;
    width: 100%;
    clear: both;
    background-color: #e4e4df;
}

.footer .asSeenOn {
    font-family: "Times New Roman";
    color: #555;
    font-size: 16px;
    margin-right: 10px;
}

.footer .asSeenOn label {
    font-style: italic;
    top: -9px;
    position: relative;
    margin-right: 6px;
}

.footer .asSeenOn em {
    display: inline-block;
    height: 37px;
    width: 770px;
    background: url('../images/imgSprites.png') 0 0;
    margin-top: 20px;
}

.footer .credits {
    margin-top: 15px;
}
</style>

<div class="top-area">
    <div class="form-box">
        <div class="form-container">
            <?php
            $formId = Forms::FORM_HOUSE_VALUES;
            $SubmissionValues = new FormSubmissionValues($formId);
            $FormFields = new FormFields;

            $step = null;
            $formAction = '';
            if (empty($_POST) && $_GET['step'] != 'complete') {
                $step = '1';
                $forwardUrl = 'http://' . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"] . '?step=complete';
                $forwardScript = 'window.location = "' . $forwardUrl . '";';
            } elseif ($_GET['step'] == 'complete') {
                $step = 'complete';
                $geoAddress = $address . ', ' . $city . ', ' . $state . ' ' . $zip;
                $mapUrl = 'http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=' . $geoAddress . '&amp;t=h&amp;z=18&amp;output=embed';

                if ($houseValuesData = Yii::app()->user->getState('houseValues')) {
                    $address = $houseValuesData[$FormFields->getField('address')->id];
                    $city = $houseValuesData[$FormFields->getField('city')->id];
                    $state = $houseValuesData[$FormFields->getField('state')->id];
//                  $state_name = AddressStates::getNameById($state);
                    $zip = $houseValuesData[$FormFields->getField('zip')->id];
                    Yii::app()->user->setState('houseValues', null);
                }
                // @todo: create wrapper for getting the primary domain of an account
                Yii::app()->clientScript->registerScript('completeForwarder', 'setTimeout(function(){ window.location = "' . $opt['successLink'] . '";}, 10000)');
            }

            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'houseValues-form',
                'action' => array('/front/forms/houseValues/formId/' . $formId),
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'beforeValidate' => 'js:function(form, attribute) {
							Message.create("notice", "Processing your request...");
							var submitButton = $("#house-values-submit-button");
							submitButton.attr("disabled", "disabled");
							return true;
						}',
                    'afterValidate' => 'js:function(form, data, hasErrors) {
							if ($.isEmptyObject(data)) {
								// Action was successful
                                ' . $forwardScript . '
							} else {
								var submitButton = $("#house-values-submit-button");
								submitButton.removeAttr("disabled");
							}
							return false;
						}',
                ),
            )); ?>
            <?php if ($step == '1') { ?>
                <div class="video">
                    <div class="main-image-container <?php echo $opt['mainPhotoClass']; ?>"></div>
                </div>
                <div class="right first">
                    <h1 style="line-height: 33px;"><?php echo 'FREE '.date('F Y').' Report<br />'.$opt['title'];?></h1>

                    <h2>Find Out What Your Home is Worth!</h2>

                    <h3>Free Home Evaluation sent to your Inbox</h3>

                    <div class="row">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']', $htmlOptions = array('class' => 'validate[required] address', 'placeholder' => 'Address'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']', $htmlOptions = array('class' => 'validate[required] city', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'City', 'value' => $opt['city']));?>
                        <? // @todo: update state to populate dynamically ?>
                        <?php $SubmissionValues->data[$FormFields->getField('state')->id] = ($flStateId = 9); ?>
                        <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array('empty' => 'State', 'class' => 'state'));?>
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']', $htmlOptions = array('class' => 'validate[required,custom[postcodeUS]] zip', 'placeholder' => 'Zip', 'value' => $opt['zip']));?>
                        <?php echo CHtml::hiddenField('step', 'complete'); ?>
                        <div class="g4">
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
                        </div>
                        <div class="g4">
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
                        </div>
                        <div class="g4">
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array('class' => 'validate[required] firstName', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'First Name'));?>
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array('class' => 'validate[required] lastName', 'placeholder' => 'Last Name'));?>
                        <div class="g6">
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
                        </div>
                        <div class="g6">
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'validate[required,custom[email]] firstName', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'Email'));?>
                        <?php $this->widget('StmMaskedTextField', array(
                            'model' => $SubmissionValues,
                            'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                            'mask' => '(999) 999-9999',
                            'id' => $phoneFieldId,
                            'htmlOptions' => CMap::mergeArray($inputHtmlOptions, array('class' => 'validate[required, custom[phone]] lastName', 'placeholder' => 'Phone')),
                        )); ?>
                        <div class="g6">
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
                        </div>
                        <div class="g6">
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
                        </div>
                    </div>
                    <div class="row center">
                        <?php echo CHtml::submitButton('Get House Values Now', array('class' => 'button', 'id' => 'house-values-submit-button')); ?>
                        <div class="arrow"></div>
                    </div>
                </div>
            <?php } elseif ($step == 'complete') { ?>
                <div class="left complete">
                    <div class="success"><em></em>Congratulations!</div>
                    <div class="address"><?php echo ucwords($address); ?><br/> <?php echo ucwords($city); ?>
                        , <?php echo $state;?> <?php echo $zip; ?></div>
                    <div class="sent">Your Report will be sent to your Inbox!<br/><br/><br/>For immediate assistance
                        call <?php echo Yii::app()->user->settings->office_phone; ?>.
                    </div>
                    <!--                    <div class="share">Share this report with a friend or resend it to yourself!</div>-->
                    <?//=$form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'email','placeholder'=>'Email','required'=>'required'));?>
                    <?php// echo CHtml::submitButton('Send Report Now', array('class'=>'button submit')); ?>
                </div>
                <div class="right verify">
                    <iframe class="map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                            src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo  $geoAddress; ?>&amp;t=h&amp;z=20&amp;output=embed"></iframe>
                </div>
            <?php } ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<div class="bonus-area">

    <?php if ($step == 1 || $step == 'verify') { ?>
        <ul>
            <li>
                <h3>FREE Top Home Staging Tips</h3>
                <span>Ever wonder what the difference is between the homes that sit and the homes that SELL?! Find out what you need to do to stand out in the market to get your home SOLD!</span>

                <div class="tagline">- FREE Inside...</div>
            </li>
            <li>
                <h3>Homes that Sit vs. Homes that Sell</h3>
                <span>Find out how to make your home sell itself! Create the perfect first impression by staging to have buyers can fall in love with your home!</span>

                <div class="tagline">- FREE Inside...</div>
            </li>
            <li>
                <h3>FREE House Values Report</h3>
                <span>Find out what your home is worth in today’s market. Get the most up-to-date, accurate, local real estate market information. You'll be surprised by what's happening in your local market!</span>

                <div class="tagline">- FREE Inside...</div>
            </li>
        </ul>
    <?php } else { ?>
        <a class="forwardLink" href="<?php echo $opt['successLink']; ?>">Click here to continue</a>
        <div class="forwardMessage">You will be forwarded in 8 seconds...</div>
    <?php }?>
</div>