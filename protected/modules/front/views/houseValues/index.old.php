<?php
	// Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl.DS.'wl_Form.js');
	// Yii::app()->clientScript->registerCssFile($this->module->cssAssetsUrl.DS.'jquery.tipsy.css');

	$domain = str_replace('www', '', $_SERVER['SERVER_NAME']);
 	$isYiiDebug = (YII_DEBUG) ? 1 : 0;
	$js = <<<JS
		// $('form').wl_Form({
		// 	ajax:false
		// });

		if (!$isYiiDebug) {
			_gaq.push(["_trackEvent", "House Values Site", "View", "House Values Page View"]);
			_gaq.push(['_setDomainName','$domain']);
		}

		$('#submit-button').click(function() {
			if(!$isYiiDebug) {
				_gaq.push(['_trackEvent', 'House Values Site', 'Click', 'House Values Click Submit']);
			}
		});

		var placeholderSupported = !!( 'placeholder' in document.createElement('input') );

		if (placeholderSupported !== true) {
		    $('input')
		      .focus(function() {
		            if (this.value === this.defaultValue) {
		                this.value = '';
		            }
		      })
		      .blur(function() {
		            if (this.value === '') {
		                this.value = this.defaultValue;
		            }
		    });
		}
JS;
	Yii::app()->clientScript->registerScript('placeHolderSupport', $js);
?>
<?php
$formId           = Forms::FORM_HOUSE_VALUES;
$SubmissionValues = new FormSubmissionValues($formId);
$FormFields       = new FormFields;

$form=$this->beginWidget('CActiveForm', array(
	'action'=>'',
	'id'=>'houseValues-form',
)); ?>
    <div class="video">
        <object width="375" height="310">
            <param name="movie" value="http://www.youtube.com/v/LBIdFaHNn6w">
            <embed src="http://www.youtube.com/v/LBIdFaHNn6w&amp;rel=0&amp;color1=0x006699&amp;color2=0x54abd6&amp;border=1&amp;autoplay=1" type="application/x-shockwave-flash" wmode="transparent" width="375" height="310">
        </object>
    </div>
	<div id="houseValues-box">
        <h1>Jacksonville House Values</h1>
		<h2>Find Out What Your Home is Worth!...</h2>
		<div class="p-pb5 g12">
			<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array('class'=>'p-fl search g11', 'style'=>'','placeholder'=>'Address','required'=>'required'));?>
		</div>
		<div class="g12 p-vt">
			<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('city')->id.']', $htmlOptions=array('class'=>'p-fl search g4', 'style'=>'','placeholder'=>'City','required'=>'required'));?>
			<?php $SubmissionValues->data[$FormFields->getField('state')->id] = AddressStates::STATE_FL; ?>
			<?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getField('state')->id.']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'),$htmlOptions=array('empty'=>'State', 'class'=>'g2'));?>
			<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']', $htmlOptions=array('class'=>'p-fl search g2', 'style'=>'','placeholder'=>'Zip','required'=>'required'));?>
			<?php echo CHtml::hiddenField('step', 'verify'); ?>
			<?php echo CHtml::submitButton('View Now', array('class'=>'button','style'=>'width:118px;height:41px;top:0px;font-size:12px;')); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
