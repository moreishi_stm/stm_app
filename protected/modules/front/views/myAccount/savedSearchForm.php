<?
Yii::app()->clientScript->registerScriptFile($this->module->getCdnAssetUrl() . DS . 'js' . DS . 'ajax_chosen.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('myAccountSavedSearchForm', <<<JS
    $("select.neighborhood").ajaxChosen({
        type: 'POST',
        url: '/myAccount/searchAutocomplete',
        dataType: 'json'
    }, function (data) {
        var results = [];

        $.each(data, function (i, val) {
            results.push({ value: val.value, text: val.text });
        });

        return results;
    });

    $('#SavedHomeSearchForm_frequency').change(function(){
        if($(this).val() == 1) {
            $('#days-of-week-container').hide('normal');
        } else if($(this).val() == 2) {
            $('#days-of-week-container').show('normal');
        }
    });

    $('.day-of-week').click(function() {
        $(this).attr('value', $(this).is(':checked') ? '1' : '0');
    });
    
    $("#home-search-dialog-submit-button").on("click",function(e){
    	var action = $("#saved-home-search-form").attr("action");
    	if(action != "/myAccount/searchAdd"){
    		return;
    	}
    	e.preventDefault();
    	$(".loading-container").addClass("loading");
    	$.post(
			action,
			$( "#saved-home-search-form").serializeArray(),		
			function(data) {
				$(".loading-container").removeClass("loading");
				if(data.status == 200){
					if(data.redirect){
						window.location = data.redirect;
						return;
					}
				} else {
					var scrollto = false;
					$.each(data, function( index, value ) {
						//if(!scrollto){scrollto = index.replace("SavedHomeSearches_","SavedHomeSearchForm_");}
						$("#"+index.replace("SavedHomeSearches_","SavedHomeSearchForm_")).addClass("error");
					});
					if(scrollto){
						
					}
					scrollto = false;
				}
			}
		);
	})

JS
);

?>
<div class="savedSearchEditForm">
	<?php
    $css = <<<CSS
    input:not([type="submit"]), select, textarea, .chzn-choices {
        font-size: 14px !important;
        height: 30px !important;
    }
    #home-search-dialog-submit-button {
        margin-top: 20px;
    }
    section:not(first-child) {
        margin-top: 10px;
    }
CSS;

    Yii::app()->clientScript->registerCss('myAccountHomeSearchChosenStyle', $css);
	if(!empty($model->id)){
		$postUrl = "/myAccount/searchEdit/".$model->id;
	}else{
		$postUrl = "/myAccount/searchAdd/";
	}

    $form = $this->beginWidget('CActiveForm', array(
    'id' => 'saved-home-search-form',
    'action' => array($postUrl),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnChange' => false,
        'validateOnSubmit' => false,
        ),
    )
    );

    // chosen for all select dropdowns applicable for this form
    $this->widget('admin_module.extensions.EChosen.EChosen', array(
            'target' => 'select.area, select.neighborhood, select.county, select.zip, select.city, select.region',
            'options' => array('width' => '100%'),
            )
        );
    ?>
    <fieldset>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'name'); ?>
        <div>
            <?php echo $form->textField($model, 'name', $htmlOptions = array(
                    'placeholder' => 'Search Name',
                    'class' => 'searchName col-lg-9 col-md-9 col-sm-12'
                )
            ); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'frequency'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'frequency', SavedHomeSearches::getFrequencyTypes(), $htmlOptions = array('class' => 'frequency')); ?>

            <span id="days-of-week-container">
                <label for="SavedHomeSearchForm_sunday">Sun</label><?php echo $form->checkBox($model, 'sunday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                <label for="SavedHomeSearchForm_monday">Mon</label><?php echo $form->checkBox($model, 'monday', array('value'=> 1, 'uncheckValue'=>0, 'class'=>'day-of-week')) ; ?>
                <label for="SavedHomeSearchForm_tuesday">Tues</label><?php echo $form->checkBox($model, 'tuesday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                <label for="SavedHomeSearchForm_wednesday">Wed</label><?php echo $form->checkBox($model, 'wednesday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                <label for="SavedHomeSearchForm_thursday">Thurs</label><?php echo $form->checkBox($model, 'thursday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                <label for="SavedHomeSearchForm_friday">Fri</label><?php echo $form->checkBox($model, 'friday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                <label for="SavedHomeSearchForm_saturday">Sat</label><?php echo $form->checkBox($model, 'saturday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                </span>
        </div>
    </section>
    <section <?=($hasMultipleBoards) ? "" : "style='display:none;'"?>>
        <?php echo $form->labelEx($model, 'mls_board_id'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'mls_board_id', CHtml::listData(MlsFeeds::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId)), 'mls_board_id', 'mlsBoard.name'), $htmlOptions = array()); ?>
        </div>
        <?php echo $form->error($model, 'mls_board_id'); ?>
    </section>
    <?php
    if($user_role === "admin"){ ?>
        <section class="col-xs-12">
            <?php echo $form->labelEx($model, 'agent_id'); ?>
            <div>
                <?php echo $form->dropDownList($model, 'agent_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->agent_id)->orderByName()->findAll(), 'id', 'fullPhoneticName'), $htmlOptions = array()); ?>
            </div>
            <?php echo $form->error($model, 'agent_id'); ?>
        </section>
    <?php
    }else{
        echo $form->hiddenField($model, 'agent_id');
    }
    ?>

    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'property_type'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'property_type', CHtml::listData(MlsPropertyTypeBoardLu::model()->findAllByAttributes(array('mls_board_id'=>Yii::app()->user->boardId,'status_ma'=>1)), 'mls_property_type_id', 'propertyType.name'), $htmlOptions = array('class' => 'propertyType')
            ); ?>
        </div>
        <?php echo $form->error($model, 'property_type'); ?><?php echo $form->error($model, 'frequency'); ?>
    </section>
    <? if(in_array('region', $searchableFields)): ?>
        <section class="col-xs-12">
            <?php echo $form->labelEx($model, 'region', array('class'=>'col-xs-12 no-padding')); ?>
            <div class="col-lg-9 col-md-9 col-sm-12 no-padding">
                <?php echo $form->dropDownList($model, 'region', CHtml::listData(MlsProperties::getRegionsList(Yii::app()->user->boardId), 'region', 'region'), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'region chzn-select',
                        'style' => 'width:300px; float:left;',
                        'data-placeholder' => ' ',
                        'multiple' => 'multiple'
                    )
                );
                ?>
            </div>
            <?php echo $form->error($model, 'region'); ?>
        </section>
    <?php endif;?>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'area', array('class'=>'col-xs-12 no-padding')); ?>
        <div class="col-lg-9 col-md-9 col-sm-12 no-padding">
            <?php echo $form->dropDownList($model, 'area', CHtml::listData(MlsProperties::getAreasList(Yii::app()->user->boardId), 'area', 'area'), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'area chzn-select',
                    'style' => 'width:300px; float:left;',
                    'data-placeholder' => ' ',
                    'multiple' => 'multiple'
                )
            );
            ?>
        </div>
        <?php echo $form->error($model, 'area'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'public_remarks'); ?>
        <div>
            <?php echo $form->textField($model, 'public_remarks',  $htmlOptions = array('class' => 'col-lg-9 col-md-9 col-sm-12')
            ); ?>
        </div>
        <?php echo $form->error($model, 'public_remarks'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'city', array('class'=>'col-xs-12 no-padding')); ?>
        <div class="col-lg-9 col-md-9 col-sm-12 no-padding">
            <?php echo $form->listBox($model, 'city', MlsProperties::getCityList(Yii::app()->user->boardId), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'city chzn-select',
                    'data-placeholder' => ' ',
                    'multiple' => 'multiple'
                )
            );
            ?>
        </div>
        <?php echo $form->error($model, 'city'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'zip', array('class'=>'col-xs-12 no-padding')); ?>
        <div class="col-lg-9 col-md-9 col-sm-12 no-padding">
            <?php echo $form->listBox($model, 'zip', MlsProperties::getZipList(Yii::app()->user->boardId), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'zip chzn-select ',
                    'data-placeholder' => ' ',
                    'multiple' => 'multiple'
                )
            );
            ?>
        </div>
        <?php echo $form->error($model, 'zip'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'county', array('class'=>'col-xs-12 no-padding')); ?>
        <div class="col-lg-9 col-md-9 col-sm-12 no-padding">
            <?php echo $form->listBox($model, 'county', MlsProperties::getCountyList(Yii::app()->user->boardId), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'county chzn-select',
                    'data-placeholder' => ' ',
                    'multiple' => 'multiple'
                )
            );
            ?>
        </div>
        <?php echo $form->error($model, 'area'); ?><?php echo $form->error($model, 'zip'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'price_min'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'price_min', StmFormHelper::getPriceList(), $htmlOptions = array(
                    'empty' => 'Price Min',
                    'class' => 'price-min'
                )
            ); ?> to
            <?php echo $form->dropDownList($model, 'price_max', StmFormHelper::getPriceList(), $htmlOptions = array(
                    'empty' => 'Price Max',
                    'class' => 'price-max'
                )
            ); ?>
            <?php echo $form->labelEx($model, 'sq_feet'); ?><?php echo $form->dropDownList($model, 'sq_feet', StmFormHelper::getSqFeetList(false), $htmlOptions = array(
                    'empty' => 'SF Min',
                    'class' => 'sq-feet'
                )
            ); ?>
                  to <?php echo $form->dropDownList($model, 'sq_feet_max', StmFormHelper::getSqFeetList(false), $htmlOptions = array(
                    'empty' => 'SF Max',
                    'class' => 'sq-feet'
                )
            ); ?>
        </div>
        <?php echo $form->error($model, 'price-min'); ?><?php echo $form->error($model, 'price_max'); ?><?php echo $form->error($model, 'sq_feet'); ?><?php echo $form->error($model, 'sq_feet_max'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'bedrooms'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'bedrooms', StmFormHelper::getBedroomList(' '), $htmlOptions = array(
                    'empty' => 'Beds Min',
                    'class' => 'bedrooms'
                )
            ); ?> to
            <?php echo $form->dropDownList($model, 'bedrooms_max', StmFormHelper::getBedroomList(' '), $htmlOptions = array(
                    'empty' => 'Beds Max',
                    'class' => 'bedrooms'
                )
            ); ?>
        <?php echo $form->error($model, 'bedrooms'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'baths_total'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'baths_total', StmFormHelper::getBathList(' '), $htmlOptions = array(
                    'empty' => 'Baths Min',
                    'class' => 'baths'
                )
            ); ?>
            to
            <?php echo $form->dropDownList($model, 'baths_total_max', StmFormHelper::getBathList(' '), $htmlOptions = array(
                    'empty' => 'Baths Max',
                    'class' => 'baths'
                )
            ); ?>
        </div>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'year_built'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'year_built', StmFormHelper::getYrBuiltList(' '), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'year-built'
                )
            ); ?>
            to
            <?php echo $form->dropDownList($model, 'year_built_max', StmFormHelper::getYrBuiltList(' '), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'year-built'
                )
            ); ?>
            <?php echo $form->error($model, 'year_built'); ?><?php echo $form->error($model, 'year_built_max'); ?>
        </div>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'neighborhood'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'neighborhood', array(), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'neighborhood g8 chzn-select',
                    'style' => 'width:300px;',
                    'data-placeholder' => 'Type Neighborhood Here...',
                    'multiple' => 'multiple'
                )
            );
            ?>
        </div>
        <?php echo $form->error($model, 'neighborhood'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'school'); ?>
        <div>
            <?php echo $form->textField($model, 'school', $htmlOptions = array(
                    'class' => 'col-lg-9 col-md-9 col-sm-12',
                )
            );
            ?>
        </div>
        <?php echo $form->error($model, 'school'); ?>
    </section>
    <? if(in_array('waterfront_yn', $searchableFields) || in_array('acreage', $searchableFields)): ?>
        <section class="col-xs-12">

            <label><? if(in_array('waterfront_yn', $searchableFields)): ?> Waterfront <? endif;?></label>
            <div>
                <? if(in_array('waterfront_yn', $searchableFields)): ?>
                    <?php echo $form->dropDownList($model, 'waterfront_yn', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
                            'empty' => '',
                            'class' => 'waterfront'
                        )
                    ); ?>
                    <?php echo $form->error($model, 'waterfront_yn'); ?><?php echo $form->error($model, 'pool'); ?>
                <?endif;?>
                <? if(in_array('pool', $searchableFields)): ?>
                    <?php echo $form->labelEx($model, 'pool'); ?>
                    <?php echo $form->dropDownList($model, 'pool', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
                            'empty' => '',
                            'class' => 'pool'
                        )
                    ); ?>
                <?endif;?>
                <? if(in_array('acreage', $searchableFields)): ?>
                    <?php echo $form->labelEx($model, 'acreage_min'); ?>
                    <?php
                    $acreageDropDownList = StmFormHelper::getAcreageList();
                    echo $form->dropDownList($model, 'acreage_min', $acreageDropDownList, $htmlOptions = array(
                            'empty' => '',
                            'class' => 'pool'
                        )
                    ); ?>
                    to
                    <?php echo $form->dropDownList($model, 'acreage_max', $acreageDropDownList, $htmlOptions = array(
                            'empty' => '',
                            'class' => 'pool'
                        )
                    ); ?>
                <?php endif;?>
            </div>
        </section>
    <?php endif;?>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'foreclosure'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'foreclosure', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'foreclosure'
                )
            ); ?>
        </div>
        <?php echo $form->error($model, 'foreclosure'); ?><?php echo $form->error($model, 'short_sale'); ?>
    </section>
    <section class="col-xs-12">
        <?php echo $form->labelEx($model, 'short_sale'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'short_sale', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'foreclosure'
                )
            ); ?>

            <? if(in_array('masterbr_room_level', $searchableFields)): ?>

                <label>Master BR Level:</label>
                <? echo $form->dropDownList($model, 'master_bedroom_level', array(1=>'1', 2=>'2', 3=>'3'), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'short_sale'
                    )
                );

            endif; ?>

        </div>
        <?php echo $form->error($model, 'short_sale'); ?>
    </section>
    <? if(YII_DEBUG || in_array('lot_description', $searchableFields)): ?>
        <section class="col-xs-12">
            <?php echo $form->labelEx($model, 'lot_description'); ?>
            <div>
                <?php echo $form->textField($model, 'lot_description', $htmlOptions = array(
                        'class' => 'col-lg-9 col-md-9 col-sm-12',
                    )
                );
                ?>
            </div>
            <?php echo $form->error($model, 'lot_description'); ?>
        </section>
    <?php endif;?>
    </fieldset>
    <?php echo $form->hiddenField($model, 'contact_id'); ?>
    <?php echo $form->hiddenField($model, 'component_type_id'); ?>
    <?php echo $form->hiddenField($model, 'component_id'); ?>
    <?php echo $form->hiddenField($model, 'id'); ?>
	<?php
		if(empty($model->id)){
			$buttonText = "Save Home Search";
		} else {
			$buttonText = "Update Home Search";
		}
	?>
    <div class="col-xs-12">
        <input id="home-search-dialog-submit-button" class="btn btn-lg btn-primary col-lg-9 col-md-9 col-sm-12" type="submit" value="<?php echo $buttonText;?>">
    </div>
    <div class="loading-container"><em></em></div>
    <?php $this->endWidget();?>
</div>