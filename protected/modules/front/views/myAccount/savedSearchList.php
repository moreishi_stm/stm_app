<div class="listSavedSearch">
	<a href="/myAccount/searchEdit/" class="btn btn-sm btn-success pull-right" style="font-weight: 400; font-size: 16px; padding: 4px 8px;">Create a New Search</a>
	<?php
	if(!empty($model)){
		$this->widget( 'front_module.components.StmListView', array(
			'dataProvider'    => $model,
			'template'        =>'{pager}{items}{pager}{summary}',
			'id'              =>'home-list',
			'itemView'        => "saved_search_list_item_html",
			'itemsCssClass' => 'row categoryProduct xsResponse clearfix',
			'summaryCssClass' => 'pull-right pull-right col-sm-4 col-xs-12 no-padding text-right text-left-xs',
			'pager' => array(
				'class' => 'CLinkPager',
				'header' => '<div class="pagination pull-left no-margin-top">',
				'footer' => '</div>',
				'firstPageCssClass' => '',
				'selectedPageCssClass' => 'active',
				'nextPageLabel' => "»",
				'prevPageLabel' => "«",
				'lastPageCssClass' => "hidden",
				'firstPageCssClass' => "hidden",
				'htmlOptions' => array('class' => 'pagination no-margin-top')
			)
		));
	}else{
		echo "<h1>empty</h1>";
	}
	$js = <<<JS
	$('.fav-home-conatiner > .panel-body').responsiveEqualHeightGrid();
	$(document).ajaxStop(function() {
  		$('.fav-home-conatiner > .panel-body').responsiveEqualHeightGrid();
	});
JS;
	Yii::app()->clientScript->registerScript('responsiveEqualHeightGridSavedHomesListJs', $js);
	?>
</div>