<?php
Yii::app()->getClientScript()->registerCss('profileCss', <<<CSS
    form {
    margin-left: 20px;
    }
    .row label {
        width: 200px;
    }
    .row input {
        width: 300px;
    }
	@media screen and (max-width: 660px) {
		#myaccount-content label {
			width: auto;
		}
	}
CSS
);

/**
Yii::app()->getClientScript()->registerScript('profileJs', <<<JS
    $(document).ajaxComplete(function(event, xhr) {
        Message.create("success", "Profile photo updated!");
        window.location.reload();
    })
JS
);
*/
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'profile-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'action' => '/myAccount/profile',
));
?>
    <h1>My Profile</h1>
<!--    <div class="row">-->
<!--        <label class="name_label">Name:</label>-->
<!--        <span class="field-data">--><?php //echo Yii::app()->user->fullName;?><!--</span>-->
<!--    </div>-->
    <div class="row">
        <label></label>
    </div>
    <div class="row">
        <label>Old Password:</label>
        <?php echo $form->passwordField($model, 'passwordOld', $htmlOptions=array('value'=>'')); ?>
        <?php echo $form->error($model, 'passwordOld'); ?>
    </div>
    <br/>

    <div class="row">
        <label>New Password:</label>
        <?php echo $form->passwordField($model, 'passwordNew'); ?>
        <?php echo $form->error($model, 'passwordNew'); ?>
    </div>
    <div class="row">
        <label>Confirm Password:</label>
        <?php echo $form->passwordField($model, 'passwordConfirm'); ?>
        <?php echo $form->error($model, 'passwordConfirm'); ?>
    </div>
    <div class="row">
        <label></label>
    </div>
    <button id="login-form-button" class="btn btn-lg btn-primary" type="submit">Update Profile</button>
<?php
$this->endWidget();
