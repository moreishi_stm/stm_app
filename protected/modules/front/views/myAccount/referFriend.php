<? $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-agent-form',
	'enableAjaxValidation'=>false,
));
?>
	<h1>Refer a Friend</h1>
	<hr />
<!-- 		<div class="row">
		<label>To:</label>
		<span class="field-data"><?//=Yii::app()->user->fullName;?></span>
	</div>
-->		<br />
	<div class="row">
		<label>From:</label>
		<span class="field-data"><?php echo Yii::app()->user->fullName;?></span>
	</div>
	<br />
	<div class="row">
		<label>Subject:</label>
		<?php echo $form->textField($model,'subject', $htmlOptions=array('class'=>'g8 p-f0')); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>
	<div class="row">
		<label>Message:</label>
		<?php echo $form->textArea($model,'content', $htmlOptions=array('class'=>'g8 p-f0','style'=>'vertical-align:top;','rows'=>'15')); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>
	<div class="row submit">
		<button id="contact-agent-button" class="wide" type="submit">Send Message</button>
	</div>
	<br />
<? $this->endWidget(); ?>
