<h1>Welcome!</h1>
<hr />

<div class="welcome">
	<h3>Congratulations  <?php echo Yii::app()->user->firstName; ?>,</h3>
	<p>
		 You have been granted Full Access to all our resources.
	</p>
	<p>Here is a list of some of the most commonly used tools and services:
		<ul>
			<label>Services:</label>
			<li>Dediated Full-time Real Estate Expert</li>
			<li>Highly Trained Team of Agents and Staff</li>
			<li>Nationally Recognized Team</li>
			<li>Customer Service 9am-9pm Daily</li>
			<li>Call our Real Estate Hotline at <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?> for immediate assistance.</li>

			<br /><label>Tools:</label>
			<li><a href="/myAccount/savedHomes">Saved Homes</a></li>
			<li><a href="/myAccount/savedHomes">Recently Viewed Homes</a></li>
			<li><a href="/myAccount/showingRequest">Priority Showing Request</a></li>
			<li><a href="/myAccount/referFriend">Refer a Friend</a></li>
			<li><a href="/myAccount/savedHomes">Manage My Profile</a></li>
			<li>Relocation Guide</li>
			<li>House Values Report</li>
		</ul>
	</p>
	<p>
		We look forward to serving your home buying and selling needs!<br /><br />
		Sincerely,
		<h2>Christine Lee Team</h2><br />
		<div>
			<i>As seen on...</i><br /><img src="/images/hgtvFox.jpg">
		</div>
	</p>
</div>