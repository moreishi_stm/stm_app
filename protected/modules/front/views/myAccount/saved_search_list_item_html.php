<?php
/**
 * caveat, this file must be included in the views folder in order to format each listing box
 */
if(!empty($data)): ?>
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 id<?php echo $data->listing_id;?>">
		<div class="panel panel-default fav-home-conatiner">
			<div class="panel-heading">
				<h3 class="panel-title">
					<strong>
						<?php echo $data->name;?> - <?php echo $data->getModelAttribute("getFrequencyTypes", $data->frequency);?>
					</strong>
				</h3>
			</div>
			<div class="panel-body">
				<strong>Available Results: <?php echo $data->getCountPropertyResults();?></strong>
				<hr>
				<strong>Search Paramaters:</strong>
				<br>
				<?php echo $data->printSearchCriteria() ;?>
			</div>
			<div class="panel-footer panel-footer-address">
				<a
					href="/myAccount/searchPreview/<?php echo $data->id;?>"
					class="btn btn-sm btn-success">Preview</a>
				<a
					href="/myAccount/searchEdit/<?php echo $data->id;?>"
					class="btn btn-sm btn-success">Edit</a>
			</div>
		</div>
	</div>
<?php endif;
