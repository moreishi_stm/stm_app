<h1>My Storyboards</h1>
<hr />
<?
    $this->widget('admin_module.components.StmGridView', array(
            'id' => 'storyboard-grid',
            'template' => '{summary}{items}{pager}',
            'enablePagination' => true,
            'dataProvider' => $DataProvider,
            'itemsCssClass' => 'datatables',
            'columns' => array(
//                'transaction.address.address',
//                'transaction.address.city',
//                'transaction.address.state.short_name',
//                'transaction.address.zip',
                array(
                    'type' => 'raw',
                    'name' => 'Type',
                    'value' => '($data->transaction->component_type_id==ComponentTypes::SELLERS)?"Listing Storyboard":"Buyer Storyboard"',
                    'htmlOptions' => array('style' => 'width:150px'),
                ),
                'title',
                array(
                    'type' => 'raw',
                    'name' => 'Updated',
                    'value' => 'Yii::app()->format->formatDate($data->updated)',
                    'htmlOptions' => array('style' => 'width:120px'),
                ),
                array(
                    'type' => 'raw',
                    'name' => 'Created',
                    'value' => 'Yii::app()->format->formatDate($data->added)',
                    'htmlOptions' => array('style' => 'width:120px'),
                ),
                array(
                    'type' => 'raw',
                    'name' => '',
                    'value' => '"<div><a href=\"/storyboard/".$data->url."/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
                    'htmlOptions' => array('style' => 'width:80px'),
                ),
                array(
                    'type' => 'raw',
                    'name' => '',
                    'value' => '
                "<div><a href=\"/myAccount/storyboards/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
            ',
                    'htmlOptions' => array('style' => 'width:80px'),
                ),
            ),
        )
    );
?>