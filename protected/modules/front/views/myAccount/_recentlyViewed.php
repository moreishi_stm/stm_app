<?
    $jsSavedHomesList = <<<JS
	$('.add-favorites-button').live('click', addFavorites);
	$('.remove-favorites-button').live('click', removeFavorites);

	function removeFavorites() {
		var listingId = $(this).attr('data');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, action: 'remove' }, function(data) {
			//results....
		});

		$(this).removeClass('remove-favorites-button').addClass('add-favorites-button');
		$('.saved-count').html($('.saved-count').html()-1);
	}

	function addFavorites() {
		var listingId = $(this).attr('data');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, action: 'add' }, function(data) {
			//results....
		});

		$(this).removeClass('add-favorites-button').addClass('remove-favorites-button');
		$('.saved-count').html($('.saved-count').html()*1+1);
	}
JS;

Yii::app()->clientScript->registerScript('savedHomesListJs', $jsSavedHomesList);
?>
<h1>Recently Viewed</h1>
<hr />
<div id="home-list">
<?
$DataProvider = $model->search();
if ($DataProvider->totalItemCount) {
	$this->widget( 'front_module.components.StmListView', array(
		'dataProvider'    =>$DataProvider,
		'id'              =>'saved-homes-grid',
		'itemView'        => '_homeListRow',
		'itemsCssClass'   =>'datatables',
	));
// } elseif ($recentlyViewedCount) {
// 	$this->renderPartial('_recentlyViewed');
} else {
	$this->renderPartial('_noResults');
}
?>
</div>