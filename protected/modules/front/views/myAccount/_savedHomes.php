<?
    $jsSavedHomesList = <<<JS
	$('.add-favorites-button').live('click', addFavorites);
	$('.remove-favorites-button').live('click', removeFavorites);

	function removeFavorites() {
		var listingId = $(this).attr('data');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, action: 'remove' }, function(data) {
			$(this).removeClass('remove-favorites-button').addClass('add-favorites-button');
			$('.saved-count').html($('.saved-count').html()-1);
			window.location = window.location;
		});

	}

	function addFavorites() {
		var listingId = $(this).attr('data');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, action: 'add' }, function(data) {
			$(this).removeClass('add-favorites-button').addClass('remove-favorites-button');
			$('.saved-count').html($('.saved-count').html()*1+1);
		});

	}
JS;

Yii::app()->clientScript->registerScript('savedHomesListJs', $jsSavedHomesList);
?>
<h1>Saved Homes</h1>
<hr />
<div id="saved-homes">
<?php
$DataProvider = $model->search();

if ($DataProvider->totalItemCount) {
	$this->widget( 'front_module.components.StmListView', array(
		'dataProvider'    =>$DataProvider,
		'template'        =>'{pager}{summary}{items}{pager}{summary}',
		'id'              =>'home-list',
		'itemView'        => '_homeListRow',
		'itemsCssClass'   =>'datatables',
	));
} else {
	$this->renderPartial('_noResults');
}
?>
</div>