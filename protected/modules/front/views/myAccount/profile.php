<?php
/**
 * @author jamesk
 * @author 813.330.0522
 * This file should no longer be needed but leaving it here for reference
 * please see app/front/views/partials/user/profile.php
 */
Yii::app()->getClientScript()->registerCss('profileCss', <<<CSS
    .profile_photo {
        margin-left: 205px;
        width: 500px;
    }

    .profilePhoto {
        border-radius: 5px;
        max-width: 100%;
    }

	.file_type {
		margin-left: 200px;
	}

	@media screen and (max-width: 660px) {
		.profile_photo {
			margin-left: 0;
			width: auto;
		}

		.file_type {
			margin-left: 0;
			display: block;
		}

		#myaccount-content label {
			width: auto;
		}
	}
CSS
);

Yii::app()->getClientScript()->registerScript('profileJs', <<<JS
    $(document).ajaxComplete(function(event, xhr) {
        Message.create("success", "Profile photo updated!");
        window.location.reload();
    })
JS
);

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'profile-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'action' => '/myAccount/profile',
));
?>
    <h1>My Profile</h1>
    <hr/>
    <br/>
    <div class="row">
        <label class="name_label">Name:</label>
        <span class="field-data"><?php echo Yii::app()->user->fullName;?></span>
    </div>
    <br/>
    <div class="row">
        <label>Old Password:</label>
        <?php echo $form->passwordField($model, 'passwordOld'); ?>
        <?php echo $form->error($model, 'passwordOld'); ?>
    </div>
    <br/>

    <div class="row">
        <label>New Password:</label>
        <?php echo $form->passwordField($model, 'passwordNew'); ?>
        <?php echo $form->error($model, 'passwordNew'); ?>
    </div>
    <div class="row">
        <label>Confirm Password:</label>
        <?php echo $form->passwordField($model, 'passwordConfirm'); ?>
        <?php echo $form->error($model, 'passwordConfirm'); ?>
    </div>
    <div class="row">
        <label>Profile Picture:</label>
        <?php echo $form->fileField($model, 'profilePhoto'); ?>
        <span class="file_type">File type: jpg, png, gif</span>
        <?php echo $form->error($model, 'profilePhoto'); ?>
        <div class="row profile_photo">
            <?php
            if (!empty($model->getSettings()->profile_photo)) {

                $this->widget('admin_exts.jcrop.EJcrop', array(
                    //
                    // Image URL
                    'url' => $model->getProfilePhotoUrl(),
                    //
                    // ALT text for the image
                    'alt' => 'Crop This Image',
                    //
                    // options for the IMG element
                    'htmlOptions' => array('id' => 'profilePhoto', 'class' => 'profilePhoto'),
                    //
                    // Jcrop options (see Jcrop documentation)
                    'options' => array(
                        'minSize' => array(50, 50),
                        'aspectRatio' => 1,
                        'onRelease' => "js:function() {ejcrop_cancelCrop(this);}",
                    ),
                    // if this array is empty, buttons will not be added
                    'buttons' => array(
                        'start' => array(
                            'label' => Yii::t('promoter', 'Crop Profile Photo'),
                            'htmlOptions' => array()
                        ),
                        'crop' => array(
                            'label' => Yii::t('promoter', 'Apply cropping'),
                        ),
                        'cancel' => array(
                            'label' => Yii::t('promoter', 'Cancel cropping')
                        )
                    ),

                    // URL to send request to (unused if no buttons)
                    'ajaxUrl' => '/admin/settings/cropProfilePhoto',

                    // Additional parameters to send to the AJAX call (unused if no buttons)
                    'ajaxParams' => array('contactId' => $model->id),
                ));
            } else {
                echo CHtml::image($model->getProfilePhotoUrl(), 'Profile Photo', $htmlOptions=array(
                    'class' => 'profilePhoto',
                ));
            }
            ?>
        </div>
    </div>
    <div class="row">
        <label></label>
        <?php
        ?>
    </div>
    <div class="row submit">
        <button id="login-form-button" class="wide" type="submit">Update Profile</button>
    </div>
<?php
$this->endWidget();
