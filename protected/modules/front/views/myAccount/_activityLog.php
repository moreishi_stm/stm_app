<div class="p-clear g12" style="padding-top: 30px;">
    <h2>Activities & Communication</h2>

<?
    $this->widget('admin_module.components.StmGridView', array(
            'id' => 'activity-log-grid',
            'template' => '{summary}{items}{pager}',
            'enablePagination' => true,
            'dataProvider' => $DataProvider,
            'itemsCssClass' => 'datatables',
            'columns' => array(
                array(
                    'type' => 'raw',
                    'name' => 'Date',
                    'value' => '"<div style=\"display:inline-block\">".Yii::app()->format->formatDate($data->activity_date, "n/j/y")." ".Yii::app()->format->formatTime($data->activity_date).
                  "<br>"."<input type=\"hidden\" class=\"log_id\" value=\"".$data->id."\"></div>"
        ',
                    'htmlOptions' => array(
                        'style' => 'width:150px;'
                    ),
                ),
                'note',
                array(
                    'type' => 'raw',
                    'name' => 'From',
                    'value' => '$data->addedBy->fullName',
                    'htmlOptions' => array(
                        'style' => 'width:150px;'
                    ),
                ),
            ),
        )
    );
?>
</div>
