<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'client-docs-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            'description',
            array(
                'type'=>'raw',
                'name'=>'Filesize',
                'value'=>'StmFormatter::formatFileSize($data[file_size])',
                'htmlOptions'=>array('style'=>'width:120px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/myAccount/documentView/".$data[id]."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
        ),
    )
);