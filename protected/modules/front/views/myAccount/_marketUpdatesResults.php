<?
    $jsSavedHomesList = <<<JS
	$('.add-favorites-button').live('click', addFavorites);
	$('.remove-favorites-button').live('click', removeFavorites);

	function removeFavorites() {
		var listingId = $(this).attr('data');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, action: 'remove' }, function(data) {
			//results....
		});

		$(this).removeClass('remove-favorites-button').addClass('add-favorites-button');
		$('.saved-count').html($('.saved-count').html()-1);
	}

	function addFavorites() {
		var listingId = $(this).attr('data');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, action: 'add' }, function(data) {
			//results....
		});

		$(this).removeClass('add-favorites-button').addClass('remove-favorites-button');
		$('.saved-count').html($('.saved-count').html()*1+1);
	}
JS;

Yii::app()->clientScript->registerScript('savedHomesListJs', $jsSavedHomesList);
?>
<a href="javascript:void(0)" onClick="history.go(-1)" class="text-button p-fl p-mt10" style="font-size: 14px !important;">Back</a>
<h1>Market Update Results</h1>
<hr />
<div id="home-list">
<?php

//if ($DataProvider->totalItemCount) {
	$this->widget( 'front_module.components.StmListView', array(
		'dataProvider'    =>$DataProvider,
		'template'        =>'{pager}{summary}{items}{pager}{summary}',
		'id'              =>'market-updates-grid',
		'itemView'        => '_homeListRow',
		'itemsCssClass'   =>'datatables',
	));
//} else {
//	$this->renderPartial('_noResults');
//}
?>
</div>