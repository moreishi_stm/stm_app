<?php
Yii::import('stm_app.widgets.BaseWidget');
Yii::setPathOfAlias('stm_app_root', SITE_ROOT . DS . 'stm_app');

class FrontModule extends CWebModule {

	// Added from AdminModule to make the cms widgets to work as it references this constant
	const REFRESH_CSS_ASSETS = null;
	const REFRESH_IMAGE_ASSETS = null;
	const REFRESH_JS_ASSETS = null;

	public $layoutPath;
	public $layout = 'column2';
    public $customCss;

	protected $_jsAssetsUrl = null;
	// Used in legacy standrd1 layout
	public $registerCss = true;
	// Used in legacy standrd1 layout
	public $cssAssets = array(
		'style.icons.css',
		'style.buttons.css',
		'style.form.css',
		'style.grid.css',
		'style.colors.css',
		'style.pager.css',
		'style.homes.css',
		'style.jui.css?v=5',
	);
	// Used in legacy standard1 layout
	protected $_imageAssetsUrl = null;
	// Used in legacy standard1 layout
	public $coreScripts = array(
		'jquery',
		'jqueryMigrate',
		'jquery.ui',
	);
	// Used in legacy standrd1 layout
	public $javaScriptAssets = array(
		'stm_Message.js',
		'stm_PlaceholderFix.js',
		'stm_BrokenImageFix.js',
	);
	// Used in legacy standrd1 layout
	protected $_cssAssetsUrl = null;

	protected $_cdnUrl = null;

	public function init() {
		$this->layoutPath = Yii::getPathOfAlias('front_module') . DS . 'views' . DS . 'layouts';

		// Define error handler for this module
		Yii::app()->setComponents(array(
			'errorHandler' => array(
				// use 'site/error' action to display errors
				'errorAction' => '/front/site/error',
			),
		));

		parent::init();

		if(Yii::app()->theme->name) {
			$this->_cdnUrl = 'http://cdn.seizethemarket.com/assets/'.Yii::app()->theme->name."/";
		}

		if(Yii::app()->themeSettings['custom_css']) {
			$this->customCss = (YII_DEBUG) ? '/css/'.Yii::app()->themeSettings['custom_css'] : 'http://sites.seizethemarket.com/site-files/'.Yii::app()->user->clientId.'/css/'.Yii::app()->themeSettings['custom_css'];
		}
	}

	public function getCdnUrl()
    {
		return $this->_cdnUrl;
	}

	// Used in legacy standrd1 layout
	public function registerScripts() {

		$this->getImageAssetsUrl();

		foreach ($this->coreScripts as $coreScript) {
			Yii::app()->clientScript->registerCoreScript($coreScript, CClientScript::POS_HEAD);
		}

		if ($this->registerCss) {
			foreach ($this->cssAssets as $cssAsset) {
				if ($this->themeAssetExists('css', $cssAsset)) {
					$cssAssetPath = $this->getThemeAssetUrl() . '/css/' . $cssAsset;
				} else {
					$cssAssetPath = $this->getCdnAssetUrl() . '/css/' . $cssAsset;
				}

				Yii::app()->clientScript->registerCssFile($cssAssetPath);
			}
		}

		foreach ($this->javaScriptAssets as $jsAsset) {
			if ($this->themeAssetExists('js', $jsAsset)) {
				$jsAssetPath = $this->getThemeAssetUrl() . '/js/' . $jsAsset;
			} else {
				$jsAssetPath = $this->getCdnAssetUrl() . '/js/' . $jsAsset;
			}

			Yii::app()->clientScript->registerScriptFile($jsAssetPath, CClientScript::POS_END);
		}

		Yii::app()->getClientScript()->scriptMap = array(
			'jquery.maskedinput.js' => $this->jsAssetsUrl . DS . 'inputmask' . DS . 'jquery.inputmask.js',
			'jquery.maskedinput.min.js' => $this->jsAssetsUrl . DS . 'inputmask' . DS . 'jquery.inputmask.js',
		);
	}

	// Used in legacy standrd1 layout
	public function getImageAssetsUrl() {

		if ($this->_imageAssetsUrl === null) {
			$this->_imageAssetsUrl = Yii::app()->getAssetManager()->publish(
					Yii::getPathOfAlias('stm_app_root.assets.images'), true, -1, FrontModule::REFRESH_IMAGE_ASSETS
			);
		}

		return $this->_imageAssetsUrl;
	}

	// Used in legacy standrd1 layout
	private function themeAssetExists($assetType, $assetName) {
		$validTypes = array(
			'js',
			'css',
			'image'
		);

		if (!in_array($assetType, $validTypes)) {
			return false;
		}

		return file_exists($this->getThemeAssetPath() . DS . $assetType . DS . $assetName);
	}

	// Used in standrd1 layout
	public function getThemeAssetPath() {
		return Yii::app()->getTheme()->basePath . DS . 'front' . DS . 'assets';
	}

	// Used in standrd1 layout
	public function getThemeAssetUrl() {

		return Yii::app()->getTheme()->baseUrl . DS . 'front' . DS . 'assets';
	}

	// Used in standrd1 layout
	public function getCssAssetsUrl() {

		if ($this->_cssAssetsUrl === null) {
			$this->_cssAssetsUrl = Yii::app()->getAssetManager()->publish(
					Yii::getPathOfAlias('stm_app_root.assets.css'), true, -1, FrontModule::REFRESH_CSS_ASSETS
			);
		}

		return $this->_cssAssetsUrl;
	}

	// Used in standrd1 layout
	public function getCdnAssetUrl() {
		$domain = (YII_DEBUG) ? 'local' : 'com';
		return "http://cdn.seizethemarket.{$domain}/assets";
	}

	// Used in standrd1 layout
	public function getCdnImageAssetsUrl() {
		return 'http://cdn.seizethemarket.com/assets/images/';
	}

	// This is called from /protected/modules/front/controllers/actions/site/IndexAction.php
	public function getJsAssetsUrl() {
		if ($this->_jsAssetsUrl === null) {
			$this->_jsAssetsUrl = Yii::app()->getAssetManager()->publish(
					Yii::getPathOfAlias('stm_app_root.assets.js'), true, -1, FrontModule::REFRESH_JS_ASSETS
			);
		}

		return $this->_jsAssetsUrl;
	}
}
