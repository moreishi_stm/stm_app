<?php
Yii::import('front_module.controllers.base.BasePlivoController');

/**
 * Plivo Lead Route Controller
 *
 * Used to easily test stuff
 */
class PlivoleadrouteController extends BasePlivoController
{
    /**
     * Action Test
     *
     * Use this to test functionality, leave commented out when committing
     * return @void
     */
//    public function actionTest()
//    {
//        // New lead adapter
//        $o = new \StmLeads\Adapters\Test(2);
//
//        // Internal data will be populated
//        $o->importData(array());
//
//        // Save data
//        $o->save();
//
//        // Stop right here!
//        Yii::app()->end();
//    }

    /**
     * Action Answer
     *
     * The URL invoked by Plivo when the outbound call is answered.
     * @return void
     */
    public function actionAnswer()
    {
        // Pul up the lead call session phone record
        $leadCallSessionPhone = LeadCallSessionPhones::model()->findByAttributes(array(
            'request_uuid'    =>    Yii::app()->request->getParam('RequestUUID')
        ));

        // If we don't have a lead call session phone record, then we have a problem
        if(!$leadCallSessionPhone) {
            throw new Exception('Error, unable to locate lead call session record!');
        }

        // Mark call as answered
        $leadCallSessionPhone->a_leg_uuid = Yii::app()->request->getParam('ALegUUID');
        $leadCallSessionPhone->status = 'Answered - Pending';

        // Attempt to save
        if(!$leadCallSessionPhone->save()) {
            throw new Exception('Unable to update status for lead call session phone record!');
        }

        // Add get digits to retrieve user input
        $getDigits = $this->_response->addGetDigits(array(
            'action'        =>  StmFunctions::getSiteUrl() . '/plivoleadroute/confirmhuman',
            'timeout'       =>  15, //seconds
        ));

        // Add speak to get digits to the user knows what to do
        $getDigits->addSpeak('Welcome. Press 1 to accept to this lead call connect.');

        // Notifies user that session is ending.
        $this->_response->addSpeak('Input not received. Your session is ending. Goodbye.');

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Confirm Human
     *
     * This runs once we've determined that the user is actually a human
     * @return void
     */
    public function actionConfirmhuman()
    {
        //Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Phone Call Lead Route - Confirm Human. Compare table column/data. See post data.', CLogger::LEVEL_ERROR);
//      [Digits] => 1
//      [Direction] => outbound
//      [From] => 19048007633
//      [ALegUUID] => 4b2e0582-2fca-11e5-94e7-353b72db4f31
//      [BillRate] => 0.01200
//      [To] => 19043433200
//      [CallUUID] => 4b2e0582-2fca-11e5-94e7-353b72db4f31
//      [ALegRequestUUID] => c8fb2608-4fc3-4ffa-a53d-cec9aee896e8
//      [RequestUUID] => c8fb2608-4fc3-4ffa-a53d-cec9aee896e8
//      [CallStatus] => in-progress
//      [Event] => Redirect

        // Retrieve lead call session phone record
        /** @var LeadCallSessionPhones $sourceLeadCallSessionPhone */
        $sourceLeadCallSessionPhone = LeadCallSessionPhones::model()->findByAttributes(array(
            'request_uuid'  =>  Yii::app()->request->getPost('ALegRequestUUID')
        ));

        // If we don't have a source lead call session phone record, then we have a problem
        if(!$sourceLeadCallSessionPhone) {
            throw new Exception('Error, unable to locate source lead call session record!');
        }

        // Mark this agent as the one who was lucky enough to answer
        $sourceLeadCallSessionPhone->status = 'Answered - Confirmed';

        // Attempt to save
        if(!$sourceLeadCallSessionPhone->save()) {
            throw new Exception('Unable to update source lead call session phone record');
        }

        // Retrieve all lead call sessions for the current lead call session
        /** @var LeadCallSessionPhones $leadCallSessionPhones */
        $leadCallSessionPhones = LeadCallSessionPhones::model()->findAllByAttributes(array(
            'lead_call_session_id'  =>  $sourceLeadCallSessionPhone->lead_call_session_id
        ));

        // if not cascading, need to sorry cancel all other calls that were initiated since this was accepted
        if(!$sourceLeadCallSessionPhone->leadCallSession->leadRoute->is_cascading) {
            // Send all the other agents to the sorry message
            foreach($leadCallSessionPhones as $leadCallSessionPhone) {

                // Skip this iteration if we are answered and confirmed
                if($leadCallSessionPhone->status == 'Answered - Confirmed') {
                    continue;
                }

                // Mark the call as missed
                $leadCallSessionPhone->status = $leadCallSessionPhone->status == 'Answered - Pending' ? 'Answered - Missed' : 'Missed';

                // Attempt to save
                if(!$leadCallSessionPhone->save()) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') URGENT: Unable to update call session phone record as missed');
                }

                if($leadCallSessionPhone->a_leg_uuid) {
                    // Send this caller to the sorry message
                    $this->_plivo->transfer_call(array(
                            'call_uuid' =>  $leadCallSessionPhone->a_leg_uuid,
                            'aleg_url'  =>  StmFunctions::getSiteUrl() . '/plivoleadroute/sorry'
                        ));
                }
                else {
                    // this may occur if agent never picked up causing a_leg_uuid to never exist. Only request_uuid.
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') LeadCallSessionPhone a_leg_uuid is missing. Data: '.print_r($leadCallSessionPhone->attributes, true), CLogger::LEVEL_ERROR);
                }
            }
        }

        // Get the lead call session
        /** @var LeadCallSessions $leadCallSession */
        $leadCallSession = $sourceLeadCallSessionPhone->leadCallSession;

        // Bridge the agent to the client
        if(!in_array($leadCallSession->component_type_id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))) {
            throw new Exception('We can only handle buyer and sellers at this time!');
        }
        $additionalTextInfo = '';
        $leadType = '';

        //@todo: later have it accept different types of leads like contact and recruits
        // Retrieve transaction record
        /** @var Transactions $transaction */
        $transaction = Transactions::model()->findByPk($leadCallSession->component_id);
        if(!$transaction) {
            throw new Exception('Unable to locate transaction record!');
        }
        else {
            $leadType = $transaction->componentType->getSingularName();
            //$additionalTextInfo
        }

        // Retrieve contact record
        /** @var Contacts $contact */
        $contact = $transaction->contact;
        if(!$contact) {
            throw new Exception('Unable to locate contact record!');
        }

        // Retrieve phone record
        /** @var Phones $phone */
        $phone = $contact->getPrimaryPhone(); //@todo: ************* need to get the submitted phone # which is not necessarily the primary one. *************
        if(!$phone) {
            throw new Exception('Unable to locate primary phone record!');
        }

        //@todo: temp logging

        // Send text message with info to the agent
        $this->_plivo->send_message(array(
            'src'   =>  '19043301885',
            'dst'   =>  $sourceLeadCallSessionPhone->phone_number, //. '<19043433200', //@todo: remove CLee when ready
            'text'  =>  'Accepted '.$leadType.' Lead Call Info:' . PHP_EOL
                        . $contact->getFullName() . PHP_EOL
                        . $contact->getPrimaryEmail() . PHP_EOL
                        . $leadCallSession->source_phone . PHP_EOL
                        . $additionalTextInfo
        ));

        // @todo: remove CLee copy of sms when ready. This has a little extra info.
        $this->_plivo->send_message(array(
                'src'   =>  '19043301885',
                'dst'   =>  '19043433200', //@todo: remove CLee when ready
                'text'  =>  'Accepted '.$leadType.' Lead Call Info:' . PHP_EOL
                    . $contact->getFullName() . PHP_EOL
                    . $contact->getPrimaryEmail() . PHP_EOL
                    . $leadCallSession->source_phone . PHP_EOL
                    . $additionalTextInfo.PHP_EOL
                    . '(Accepted by: '.$sourceLeadCallSessionPhone->phone_number.')'
            ));

        // Add message
        $this->_response->addSpeak('Please hold while we connect your call. Lead info has been sent to you as a text message.');

        // Add wait so agent has time to get the text message.
        //$this->_response->addWait(5);

        $sitePlivoDomain = (strlen(\StmFunctions::getSiteUrl()) > 15) ? \StmFunctions::getSiteUrl() : 'http://'.$_SERVER['SERVER_NAME'];

        // see if we can find a contact in settings for name for caller Id
        $agentName = Yii::app()->db->createCommand()
                        ->select('CONCAT(c.first_name," ",c.last_name)')
                        ->from('setting_contact_values v')
                        ->join('contacts c','c.id=v.contact_id')
                        ->where('v.setting_id='.Settings::SETTING_CELL_NUMBER)
                        ->andWhere('v.value=:value', array(':value' => substr($sourceLeadCallSessionPhone->phone_number,1)))
                        ->queryScalar();

        // add record before dial
        $this->_response->addRecord(array(
                'action'        =>  $sitePlivoDomain . '/plivoleadroute/recordaction?leadCallSessionId='.$leadCallSession->id,
                'redirect'      => false,
                'startOnDialAnswer' => true,
                'maxLength'     => 1800,
                'timeout'       => 30,
                'recordSession' => true,
                'callbackUrl'   =>  $sitePlivoDomain . '/plivoleadroute/recordcallback?leadCallSessionId='.$leadCallSession->id,
            ));

        // Create dial element
        $dial = $this->_response->addDial(array(
            'action'        =>  $sitePlivoDomain . '/plivoleadroute/dialaction',
            'callbackUrl'   =>  $sitePlivoDomain . '/plivoleadroute/dialcallback',
            'callerName'    => $agentName,
            'callerId'      => $sourceLeadCallSessionPhone->phone_number,
        ));

        // Add clients phone number to dial element
        $dial->addNumber('1' . $phone);

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Hangup
     *
     * The URL that is notified by Plivo when the call hangs up. Defaults to answer_url.
     * @return void
     */
    public function actionHangup()
    {
        // Pull up the lead call session phone record
        /** @var LeadCallSessionPhones $leadCallSessionPhone */
        $leadCallSessionPhone = LeadCallSessionPhones::model()->findByAttributes(array(
            'request_uuid'  =>  Yii::app()->request->getPost('RequestUUID')
        ));

        //Yii::app()->request->getParam('leadCallSessionId');

        // We have to have this available to continue
        if(!$leadCallSessionPhone) {
            throw new Exception('Error, unable to locate lead call session record for hangup processing!');
        }

        // Retrieve lead call session record
        /** @var LeadCallSessions $leadCallSession */
        $leadCallSession = $leadCallSessionPhone->leadCallSession;
        if(!$leadCallSession) {
            throw new Exception('Error, unable to locate lead call session record for hangup processing!');
        }

        // Figure out if we need to make another call
        if(Yii::app()->request->getPost('HangupCause') != 'NORMAL_CLEARING' || (Yii::app()->request->getPost('HangupCause') == 'NORMAL_CLEARING' && $leadCallSessionPhone->status != 'Answered - Confirmed')) {

            // Retrieve lead route
            /** @var LeadRoutes $leadRoute */
            $leadRoute = $leadCallSession->leadRoute;

            // Make sure we have a lead route
            if(!$leadRoute) {
                throw new Exception('Error, no lead route found!');
            }

            // Flag as missed if it was just ringing
            $leadCallSessionPhone->status = $leadCallSessionPhone->status == 'Ringing' ? 'Missed' : $leadCallSessionPhone->status;
            if(!$leadCallSessionPhone->save()) {
                throw new Exception('Unable to update lead call session phone status');
            }

            // If we're cascading, then we need to make another call
            if($leadRoute->type_ma == LeadRoutes::ROUTE_TYPE_ROUND_ROBIN_ID && $leadRoute->is_cascading) {

                // decode the list of contactIds for this instance of round robin recorded in beginning of call in Adapter/Base
                $cascadingContactIds = CJSON::decode($leadCallSession->cascading_data);

                // get the next contactId in line
                $cascadingPosition = array_search($leadCallSessionPhone->phone->contact_id, $cascadingContactIds);

                // check to see if we've reached the end, this assumes only 1 round of cascading for now
                if(count($leadCallSession->leadCallSessionPhones) >= count($cascadingContactIds) || !isset($cascadingContactIds[$cascadingPosition + 1])) {
                    \Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Cascading Round Robin reach the end. Please verify. Remove this log when done monitoring.'.PHP_EOL.'Cascading Data: '.print_r($cascadingContactIds, true), \CLogger::LEVEL_ERROR);
                    // end of round robin, the end
                    $this->_sendResponse();
                }

\Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Going to next on cascading round robin.'.PHP_EOL.'Cascading Data: '.print_r($cascadingContactIds, true), \CLogger::LEVEL_ERROR);

                $nextAgentContactId = $cascadingContactIds[$cascadingPosition + 1];

                $agentNumber = Yii::app()->db->createCommand()
                    ->select('value')
                    ->from('setting_contact_values')
                    ->where('setting_id=:settingId', array(':settingId'=> Settings::SETTING_CELL_NUMBER))
                    ->andWhere('contact_id=:contactId', array(':contactId' => $nextAgentContactId))
                    ->queryScalar();

                $agentNumber = Yii::app()->format->formatInteger($agentNumber);

                if($agentNumber) {
                    // get the phone object
                    $phone = Phones::model()->findByAttributes(array('contact_id'=> $nextAgentContactId, 'phone' => $agentNumber));
                }
                else {
                    $agent = Contacts::model()->findByPk($nextAgentContactId);
                    $agentNumber = $agent->getPrimaryPhone();
                    $phone = $agent->getPrimaryPhoneObj;
                }

                /** @var LeadCallSessionPhones $newLeadCallSessionPhone */
                $nextLeadCallSessionPhone = new LeadCallSessionPhones();
                $nextLeadCallSessionPhone->setAttributes(array(
                        'lead_call_session_id' => $leadCallSession->id,
                        'phone_number' => $agentNumber,
                        'phone_id' => ($phone) ? $phone->id : null,
                        'added' => new CDbExpression('NOW()'),
                    ));
                // Attempt to save lead call session
                if(!$nextLeadCallSessionPhone->save()) {
                    \Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Unable to save next cascading round robin lead call session phone! Calls were not initiated.'.PHP_EOL.'Number to Call: '.$agentNumber.PHP_EOL.'Error Data: '.print_r($nextLeadCallSessionPhone->getErrors(), true), \CLogger::LEVEL_ERROR);

                    // stop process
                    $this->_sendResponse();
                }

                $sitePlivoDomain = (strlen(\StmFunctions::getSiteUrl()) > 15) ? \StmFunctions::getSiteUrl() : 'http://'.$_SERVER['SERVER_NAME'];

                // Start call
                $response = $this->_plivo->make_call(array(
                    'from'          =>  '19048007633', // this # is specifically for Lead Connect Calls
                    'to'            =>  '1' . $nextLeadCallSessionPhone->phone_number,
                    'answer_url'    =>  $sitePlivoDomain . '/plivoleadroute/answer',
                    'hangup_url'    =>  $sitePlivoDomain . '/plivoleadroute/hangup',
                    'fallback_url'  =>  $sitePlivoDomain . '/plivoleadroute/fallback',
                    'ring_timeout'  =>  45
                ));

                // Update data
                $nextLeadCallSessionPhone->status = 'Ringing';
                $nextLeadCallSessionPhone->request_uuid = $response['response']['request_uuid'];

                // Attempt to save
                if(!$nextLeadCallSessionPhone->save()) {
                    throw new Exception('Error, unable to save next lead call session phone record!');
                }
            }
        }

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Fallback
     *
     * Invoked by Plivo only if answer_url is unavailable or the XML response is invalid. Should contain a XML response.
     * @return void
     */
    public function actionFallback()
    {
        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Fallback action. Unexpected results. See post data.', CLogger::LEVEL_ERROR);

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Sorry
     *
     * @return void
     */
    public function actionSorry()
    {
        // Add message of instruction for user
        $this->_response->addSpeak("We're sorry, another agent has already accepted this call. Goodbye.");
        $this->_sendResponse();
    }

    /**
     * Action Dial Action
     *
     * Redirect to this URL after leaving Dial. (When connecting the agent to the client)
     * @return void
     */
    public function actionDialaction()
    {
        //Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Dial action. See post data.', CLogger::LEVEL_ERROR);
        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Dial Callback
     *
     * URL that is notified by Plivo when one of the following events occur: Called party is bridged with caller, called party hangs up, or caller has pressed any digit
     * @return void
     */
    public function actionDialcallback()
    {
        //POST DATA
//      [DialBLegUUID] => 0b0af9a4-302b-11e5-8667-49df6a42d567
//      [AnswerTime] => 2015-07-22 00:35:15
//      [DialBLegTotalCost] => 0.01200
//      [DialBLegStatus] => hangup
//      [DialBLegBillRate] => 0.01200
//      [DialAction] => hangup
//      [DialBLegBillDuration] => 60
//      [Event] => DialHangup
//      [DialBLegFrom] => 19043433200
//      [DialALegUUID] => ff7163ee-302a-11e5-847c-49df6a42d567
//      [DialBLegPosition] => 1
//      [StartTime] => 2015-07-22 00:35:14
//      [DialBLegHangupCause] => NORMAL_CLEARING
//      [CallUUID] => ff7163ee-302a-11e5-847c-49df6a42d567
//      [EndTime] => 2015-07-22 00:35:33
//      [DialBLegDuration] => 18

        //@todo: can store data for if wanted: DialBLegBillDuration, DialBLegBillRate
//        $duration = Yii::app()->request->getPost('DialBLegDuration');
//        $event = Yii::app()->request->getPost('Event');//DialHangup
//
//        $leadCallSessionPhone = LeadCallSessionPhones::model()->findByAttributes(array(
//                'a_leg_uuid'  =>  Yii::app()->request->getPost('DialALegUUID')
//            ));
//
//        //@todo: Store extra Plivo data in to columns that haven't been put in the new table just yet
//        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: DialCallback action. See post data.', CLogger::LEVEL_ERROR);

        // Send XML response
        $this->_sendResponse();
    }

    public function actionRecordaction()
    {
//      [leadCallSessionId] => 818
//      [Digits] =>
//      [Direction] => outbound
//      [RecordUrl] => https://s3.amazonaws.com/recordings_2013/483099c8-3027-11e5-9dc2-842b2b096c5d.mp3
//      [From] => 19048007633
//      [ALegUUID] => 38a09760-3027-11e5-b81e-6927109a9659
//      [RecordingEndMs] => -1
//      [RecordingID] => 483099c8-3027-11e5-9dc2-842b2b096c5d
//      [RecordFile] => https://s3.amazonaws.com/recordings_2013/483099c8-3027-11e5-9dc2-842b2b096c5d.mp3
//      [BillRate] => 0.01200
//      [To] => 19043433200
//      [RecordingDurationMs] => -1
//      [RequestUUID] => 13aa910d-b819-460d-bd5a-10b42ae578d0
//      [ALegRequestUUID] => 13aa910d-b819-460d-bd5a-10b42ae578d0
//      [CallUUID] => 38a09760-3027-11e5-b81e-6927109a9659
//      [CallStatus] => in-progress
//      [Event] => Record
//      [RecordingDuration] => -1
//      [RecordingStartMs] => -1

        //this data contains the recording initial data. Recordcallback will be call when recording complete.
        $leadCallSession = LeadCallSessions::model()->findByPk(Yii::app()->request->getPost('leadCallSessionId'));
        if($leadCallSession) {
            $leadCallSession->setAttributes(array(
                    'recording_id' => Yii::app()->request->getPost('RecordingID'),
                    'recording_call_uuid' => Yii::app()->request->getPost('CallUUID'),
                    'recording_url' => Yii::app()->request->getPost('RecordUrl'),
                ));

            if(!$leadCallSession->save()) {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Recording not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
            }
        }
        else {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Lead Call Session not found. Recording info not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
        }

        // Send XML response
        $this->_sendResponse();
    }

    public function actionRecordcallback()
    {
        // find the call session by callUuid or something

//      POST DATA
//      [leadCallSessionId] => 818
//      [Direction] => outbound
//      [RecordUrl] => https://s3.amazonaws.com/recordings_2013/483099c8-3027-11e5-9dc2-842b2b096c5d.mp3
//      [From] => 19048007633
//      [ALegUUID] => 38a09760-3027-11e5-b81e-6927109a9659
//      [RecordingEndMs] => 1437538129223
//      [RecordingID] => 483099c8-3027-11e5-9dc2-842b2b096c5d
//      [RecordFile] => https://s3.amazonaws.com/recordings_2013/483099c8-3027-11e5-9dc2-842b2b096c5d.mp3
//      [BillRate] => 0.01200
//      [To] => 19043433200
//      [RecordingDurationMs] => 28100
//      [CallUUID] => 38a09760-3027-11e5-b81e-6927109a9659
//      [ALegRequestUUID] => 13aa910d-b819-460d-bd5a-10b42ae578d0
//      [RequestUUID] => 13aa910d-b819-460d-bd5a-10b42ae578d0
//      [CallStatus] => in-progress
//      [Event] => RecordStop
//      [RecordingDuration] => 28
//      [RecordingStartMs] => 1437538101123

        //this data contains the recording initial data. Recordcallback will be call when recording complete.
        $leadCallSession = LeadCallSessions::model()->findByPk(Yii::app()->request->getPost('leadCallSessionId'));
        if($leadCallSession) {
            $leadCallSession->setAttributes(array(
                    'recording_id' => Yii::app()->request->getPost('RecordingID'),
                    'recording_call_uuid' => Yii::app()->request->getPost('CallUUID'),
                    'recording_url' => Yii::app()->request->getPost('RecordUrl'),
                    'recording_duration' => Yii::app()->request->getPost('RecordingDuration'),
                    'recording_duration_ms' => Yii::app()->request->getPost('RecordingDurationMs'),
                    'recording_start_ms' => Yii::app()->request->getPost('RecordingStartMs'),
                    'recording_end_ms' => Yii::app()->request->getPost('RecordingEndMs'),
                    'recording_added' => new CDbExpression('NOW()'),
                ));

            if(!$leadCallSession->save()) {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Recording not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
            }
        }
        else {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Lead Call Session not found. Recording info not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
        }

        // Send XML response
        $this->_sendResponse();
    }
}