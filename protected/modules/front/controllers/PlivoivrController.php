<?php
require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmAws.S3')."/FileManagement.php");
Yii::import('front_module.controllers.base.BasePlivoController');

/**
 * Plivo IVR Controller
 *
 * Used to handle Plivo callbacks
 */
class PlivoivrController extends BasePlivoController
{
    /**
     * Hold Music URL
     *
     * @var string URL for location of music to play when agent is on hold
     */
    const HOLD_MUSIC_URL = 'http://cdn.seizethemarket.com/plivo/goldeneye-elevator.mp3';

    /**
     * To Phone
     *
     * @var string The To phone number
     */
    protected $_toPhone;

    /**
     * Telephony Phone
     *
     * Database record representing the telephony phone in use
     * @var TelephonyPhones
     */
    protected $_telephonyPhone;

    /**
     * Client Account
     *
     * Database record representing the client/account in use
     * @var ClientAccounts
     */
    protected $_clientAccount;

    /**
     * Send Email
     *
     * Helper function for sending emails for IVR events
     * @param $phone The incoming phone number
     * @param $extensionId The chosen extension ID
     * @param $templateName The template name to send via email
     * @param $subject The email subject
     * @param $to The email address we're sending to
     */
    protected function _sendEmail($phone, $extensionId, $templateName, $subject, $to, $digits)
    {
        // Remove leading 1 for lookup, if applicable
        if(strlen($phone) == 11 && (strrpos($phone, '1', -strlen($phone)) !== false)) {
            $phone = substr($phone, 1);
        }

        /** @var Phones $phone */
//        $phone = Phones::model()->findByAttributes(array(
//            'phone' =>  $phone
//        ));

        if($phone) {
            //@todo: error log
        }
        //@todo: emailTo should be found through settings on the dialed From phone number
        //@todo: address not found via contact, but through extension description... or later lookup to seller transaction

        /** @var Contacts $contact */
//        $contact = $phone->contact;

        /** @var Addresses $address */
//        $address = $contact->getPrimaryAddress();

        /** @var IvrExtensions $ivrExtension */
        $ivrExtension = IvrExtensions::model()->findByPk($extensionId);

        if($digits) {

            /** @var Sources $source */
            $sourceNumber = substr($digits, 3,2);
            /** @var IvrSources $ivrSource */
            $ivrSource = IvrSources::model()->findByAttributes(array('extension_suffix' => $sourceNumber));
            $source = $ivrSource->source;
        }

        // Render view partial and return contents
        $html = $this->renderPartial('emails/' . $templateName, array(
            'contact'       =>  $contact,
            'address'       =>  $ivrExtension->name, //$address.. along with price, zip, etc. link to property or seller, etc.
            'phone'         =>  Yii::app()->format->formatPhone($phone),
            'ivrExtension'  =>  $ivrExtension,
            'source'        =>  $source,
            'sourceNumber'  =>  $sourceNumber,
        ), true);

        // New test email
        $mail = new StmZendMail();
        $mail->setFrom('do-not-reply@seizethemarket.net', 'STM IVR Notification');
        $mail->addTo($to);
        $mail->setSubject($subject);
        $mail->setBodyHtml($html);

        // Send email
        $mail->send(StmZendMail::getAwsSesTransport());
    }

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        // Make sure we have a to phone number, removes the leading 1
        $this->_toPhone = (strlen(Yii::app()->request->getParam('To')) > 10) ? substr(Yii::app()->request->getParam('To'), 1) : Yii::app()->request->getParam('To');
        if(!$this->_toPhone) {
            throw new Exception('"To" phone number must be specified');
        }

        // Lookup the to phone number to get the client_id, account_id, and database
        $this->_telephonyPhone = TelephonyPhones::model()->findByAttributes(array(
            'phone'             =>  $this->_toPhone,
            'call_route_type'   =>  TelephonyPhones::CALL_ROUTE_TYPE_IVR
        ));

        if(!$this->_telephonyPhone) {
            throw new Exception('Unable to locate to telephony phone record in database, ' . $this->_toPhone);
        }

        // Locate client account record
        $this->_clientAccount = ClientAccounts::model()->findByPk($this->_telephonyPhone->client_account_id);
        if(!$this->_clientAccount) {
            throw new Exception('Unable to locate to client account record in database');
        }

        // Configure database connection for the specified client
        if(!StmFunctions::frontConnectDbByClientIdAccountId($this->_clientAccount->client_id, $this->_clientAccount->account_id)) {
            throw new Exception('Error: Cannot activate db connection for client!');
        }

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Action Answer
     *
     * Called when Plivo answers the phone
     * @return void
     */
    public function actionAnswer()
    {
        // Create a new IVR Session
        /** @var IvrSessions $ivrSession */
        $ivrSession = new IvrSessions();

        $ivrSession->call_uuid = Yii::app()->request->getPost('CallUUID');
        $ivrSession->status = IvrSessions::STATUS_ANSWERED;
        $ivrSession->added = new CDbExpression('NOW()');

        // Attempt to save
        if(!$ivrSession->save()) {
            throw new Exception('Error, unable to save ivr session!');
        }

        //
        // Log incoming call in database, set status as "Initial Incoming"
        //

        // Get entered digits (requesting extension)
        $getDigits = $this->_response->addGetDigits(array(
            'action'    =>  StmFunctions::getSiteUrl() . '/plivoivr/checkextension',
            'redirect'  =>  'true',
            'timeout'   =>  300,
            'retries'   =>  3,
            'numDigits' =>  5
        ));

        /** @var Ivrs $ivr */
        $ivr = Ivrs::model()->findByPk($this->_telephonyPhone->ivr_id);
        if(!$ivr) {
            throw new Exception('Unable to locate IVR record!');
        }

        /** @var ClientAccounts $clientAccount */
        $clientAccount = ClientAccounts::model()->findByPk($this->_telephonyPhone->client_account_id);
        if(!$clientAccount) {
            throw new Exception('Error, unable to locate client and account');
        }

        // If we want text to speech or recorded speech
        if($ivr->do_speech) {

            // Do text to speech
            $getDigits->addSpeak($ivr->speech);
        }
        else {

            // Play recording for the currently entered extension
            $getDigits->addPlay(\StmAws\S3\FileManagement::getBaseUrlByType(\StmAws\S3\FileManagement::SITE_FILES_BUCKET_TYPE) . '/' . $clientAccount->client_id . '/ivr/' . $ivr->id . '.mp3');
        }

        // Add speak to get digits to the user knows what to do
        $getDigits->addSpeak('Please enter your extension, then press the pound key.');

        // Send the response
        $this->_sendResponse();
    }

    /**
     * Action Hangup
     *
     * Called when Plivo hangs up the phone
     * @return void
     */
    public function actionHangup()
    {
        // Locate IVR session
        /** @var IvrSessions $ivrSession */
        $ivrSession = IvrSessions::model()->findByAttributes(array(
            'call_uuid'  =>  Yii::app()->request->getPost('CallUUID')
        ));

        // Send email if we entered an extension
        if($ivrSession->ivr_extension_id) {

            // Send email
            $this->_sendEmail(Yii::app()->request->getPost('From'), $ivrSession->ivr_extension_id, 'endOfCallNotConnected', 'STM IVR Notification, End of Call, not Direct Connected', 'debug@seizethemarket.com', $ivrSession->extension);
        }

        // Send empty response
        $this->_sendResponse();
    }

    /**
     * Action Check Extension
     *
     * Checks the user supplied extension
     * @return void
     */
    public function actionCheckextension()
    {
        // Locate IVR session
        /** @var IvrSessions $ivrSession */
        $ivrSession = IvrSessions::model()->findByAttributes(array(
            'call_uuid'  =>  Yii::app()->request->getPost('CallUUID')
        ));

        // If we can't locate the IVR session
        if(!$ivrSession) {
            throw new Exception('Error! Unable to locate IVR session!');
        }

        // Retrieve entered digits
        $digits = Yii::app()->request->getPost('Digits');

        // Lookup extension from database and make sure it is valid
        /** @var IvrExtensions $ivrExtension */
        $ivrExtension = IvrExtensions::model()->findByFullExtension($this->_telephonyPhone->ivr_id, $digits);

        // Store the digits entered as the extension
        $ivrSession->extension = $digits;

        // Check extension
        if(!$ivrExtension) {

            // Update caller status as "Invalid Extension Entered"
            $ivrSession->status = IvrSessions::STATUS_INVALID_EXTENSION_ENTERED;
            if(!$ivrSession->save()) {
                throw new Exception('Error, unable to save IVR session!');
            }

            // Get entered digits (requesting extension)
            $getDigits = $this->_response->addGetDigits(array(
                'action'    =>  StmFunctions::getSiteUrl() . '/plivoivr/checkextension',
                'redirect'  =>  'true',
                'timeout'   =>  300,
                'retries'   =>  3,
                'numDigits' =>  5
            ));

            // Add speak to get digits to the user knows what to do
            $getDigits->addSpeak('Invalid extension entered. Please try again.');

            // Send the response
            $this->_sendResponse();
        }

        // Send email
        $this->_sendEmail(Yii::app()->request->getPost('From'), $ivrExtension->id, 'extensionEntered', 'Lead Notification, Extension Accessed', 'debug@seizethemarket.com', $digits);

        // Update caller status as "Valid Extension Entered", log which property/lead/CTA (Not sure where yet)
        $ivrSession->ivr_extension_id = $ivrExtension->id;
        $ivrSession->status = IvrSessions::STATUS_VALID_EXTENSION_ENTERED;
        if(!$ivrSession->save()) {
            throw new Exception('Error, unable to save IVR session!');
        }

        /** @var ClientAccounts $clientAccount */
        $clientAccount = ClientAccounts::model()->findByPk($this->_telephonyPhone->client_account_id);
        if(!$clientAccount) {
            throw new Exception('Error, unable to locate client and account');
        }

        // Get entered digits (requesting extension)
        $getDigits = $this->_response->addGetDigits(array(
            'action'    =>  StmFunctions::getSiteUrl() . '/plivoivr/pressedone?extensionId=' . $ivrExtension->id,
            'redirect'  =>  'true',
            'timeout'   =>  300,
            'retries'   =>  3,
            'numDigits' =>  1
        ));

        // If we want text to speech or recorded speech
        if($ivrExtension->do_speech) {

            // Do text to speech
            $getDigits->addSpeak($ivrExtension->speech);
        }
        else {
            //@todo: this was being cached and hard to bypass caching - using plivo record url until solution found. \StmAws\S3\FileManagement::getBaseUrlByType(\StmAws\S3\FileManagement::SITE_FILES_BUCKET_TYPE) . '/' .$clientAccount->client_id . '/ivrExtension/' . $ivrExtension->id . '.mp3'
            // Play recording for the currently entered extension
            $getDigits->addPlay($ivrExtension->record_url);
        }

        // Add speak to get digits to the user knows what to do
        $getDigits->addSpeak('Press one to speak with a representative.');

        // Send the response
        $this->_sendResponse();
    }

    /**
     * Action Pressed One
     *
     * Called when the users presses one to be connected to a hunt group
     * @return void
     */
    public function actionPressedone()
    {
        // Locate IVR session
        /** @var IvrSessions $ivrSession */
        $ivrSession = IvrSessions::model()->findByAttributes(array(
            'call_uuid'  =>  Yii::app()->request->getPost('CallUUID')
        ));

        // Retrieve the user supplied digits
        $digits = Yii::app()->request->getPost('Digits');

        // If the user didn't enter a one
        if($digits != '1') {

            //
            // Notify users on incoming call and status
            //

            // Add speak to get digits to the user knows what to do
            $this->_response->addSpeak('Thank you for calling, goodbye.');

            // Send the response
            $this->_sendResponse();
        }


        // Update status to "Transfer in Progress"
        $ivrSession->status = IvrSessions::STATUS_TRANSFER_IN_PROGRESS;
        if(!$ivrSession->save()) {
            throw new Exception('Error, unable to save IVR session!');
        }

        /** @var IvrExtensions $ivrExtension */
        $ivrExtension = IvrExtensions::model()->findByPk(Yii::app()->request->getParam('extensionId'));
        if(!$ivrExtension) {
            throw new Exception('Error, unable to locate IVR extension!');
        }

        // Lookup the hunt group
        /** @var CallHuntGroups $callHuntGroup */
        $callHuntGroup = CallHuntGroups::model()->findByPk($ivrExtension->call_hunt_group_id);
        if(!$callHuntGroup) {
            throw new Exception('Error, unable to locate call hunt group!');
        }

        // Redirect to hunt group processing
        $this->_response->addRedirect(StmFunctions::getSiteUrl() . '/plivoivrhuntgroup/answer?huntGroupId=' . $callHuntGroup->id);

        // Send response
        $this->_sendResponse();
    }
}