<?php

Yii::import('front_module.controllers.base.BaseHuntGroupController');

/**
 * Plivo Call Controller
 *
 * Used to handle Plivo callbacks
 */
class PlivocallController extends BaseHuntGroupController
{
    /**
     * Get Hunt Group
     *
     * Returns the appropriate call hunt group to use
     * @return CallHuntGroups|null
     */
    protected function _getHuntGroup()
    {
        return CallHuntGroups::model()->findByPk($this->_telephonyPhone->call_hunt_group_id);
    }
}