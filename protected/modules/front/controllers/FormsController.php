<?php

class FormsController extends FrontController {

    public function actions() {
    	return array(
			'askQuestion'                     => 'stm_app.modules.front.controllers.actions.forms.AskQuestionAction',
			'showing'                         => 'stm_app.modules.front.controllers.actions.forms.ShowingAction',
			'propertyTax'                     => 'stm_app.modules.front.controllers.actions.forms.PropertyTaxAction',
			'sendToFriend'                    => 'stm_app.modules.front.controllers.actions.forms.SendToFriendAction',
            'buyer'                           => 'stm_app.modules.front.controllers.actions.forms.BuyerAction',
			'videoForm'                       => 'stm_app.modules.front.controllers.actions.forms.VideoFormAction',
			'register'                        => 'stm_app.modules.front.controllers.actions.forms.RegisterAction',
			'houseValues'                     => 'stm_app.modules.front.controllers.actions.forms.HouseValuesAction',
            'makeAnOffer'                     => 'stm_app.modules.front.controllers.actions.forms.MakeAnOfferAction',
            'recruiting'                      => 'stm_app.modules.front.controllers.actions.forms.RecruitingAction',
            'referral'                        => 'stm_app.modules.front.controllers.actions.forms.ReferralAction',
            'submit'                          => 'stm_app.modules.front.controllers.actions.forms.SubmitAction',
            'marketSnapshotQuestion'          => 'stm_app.modules.front.controllers.actions.forms.MarketSnapshotQuestionAction',
            'marketSnapshotUpdateEstimate'    => 'stm_app.modules.front.controllers.actions.forms.MarketSnapshotUpdateEstimateAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('askQuestion','showing','propertyTax','sendToFriend','videoForm','register','houseValues','makeAnOffer','recruiting','buyer','referral','marketSnapshotQuestion','marketSnapshotUpdateEstimate','submit'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
}
