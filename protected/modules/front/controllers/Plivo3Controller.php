<?php

// Set an exception handler to read the exception over the phone lolol
if(0) {

    // Exception handler function
    function handlePlivoExceptions(Exception $exception) {

        // Log contents to a file
        file_put_contents('/var/log/stm/dialer3/exceptions-' . date('Y-m-d_h-i-s') . '_' . microtime(true), $exception->getMessage() . "\n" . print_r($exception->getTraceAsString(), true) . "\n\nPOST Variables:\n" . print_r($_POST, true) . "\n\nGET Variables:\n" . print_r($_GET, true));

        // Generate error response
        $response = new \StmPlivoFunctions\Response();
        $response->addSpeak($exception->getMessage());

        // Clean the output buffer
        ob_clean();

        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $response->toXML();

        // Terminate execution
        Yii::app()->end();
    }

    // Set the exception handler
    set_exception_handler('handlePlivoExceptions');
}

Yii::import('admin_module.components.StmDialer.Lists.Factory', true);
Yii::import('admin_module.controllers.DialerController');
Yii::import('admin_module.components.StmPlivo.plivo', true);
Yii::import('admin_module.components.Dialer.LoadAbstractTrait', true);

/**
 * Plivo 3 Controller
 *
 * Used to handle Plivo callbacks
 */
class Plivo3Controller extends FrontController
{
    /**
     * Load up trait
     */
    use LoadAbstractTrait;

    /**
     * Max Call Count
     *
     * @todo Write a description for this
     */
    //const MAX_CALL_COUNT = 12;

    /**
     * Log Location
     *
     * Where to store logs
     * @var string Directory path
     */
    const LOG_LOCATION = '/var/log/stm/dialer3';

    /**
     * Flag for logging before action
     *
     * Used for debugging purposes
     */
    const LOG_BEFORE_ACTION = false;

    /**
     * Response
     *
     * Response object for Plivo
     * @var \StmPlivoFunctions\Response
     */
    protected $_response;

    /**
     * Node URL
     *
     * The URL for the Admin Socket Server
     * @var string Admin Socket server URL
     */
    protected $_nodeUrl;

    /**
     * Plivo
     *
     * The Plivo REST API object
     * @var \StmPlivoFunctions\RestAPI
     */
    protected $_plivo;

    /**
     * Hold Music URL
     *
     * @var string URL for location of music to play when agent is on hold
     */
//    const HOLD_MUSIC_URL = 'http://cdn.seizethemarket.com/plivo/mario2-overworld.mp3';
    const HOLD_MUSIC_URL = 'http://cdn.seizethemarket.com/plivo/goldeneye-elevator.mp3';
//    const HOLD_MUSIC_URL = 'http://cdn.seizethemarket.com/plivo/call-tha-shots.mp3';


    /**
     * Do the Boomerang
     *
     * @var bool When true, boomerang is enabled, when false, it's disabled
     */
    const DO_THE_BOOMERANG = false;

    /**
     * Init
     *
     * Things that happen before each controller action
     * @return void
     */
    public function init()
    {
        //@todo: a quick fix to to prevent google from crawling... ask nicole better way to do this - CLee 3/22/16
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'AdsBot-Google') !== false) {
            Yii::app()->end();
        }

        // New Plivo Response
//        $this->_response = new \StmPlivoFunctions\Response();

        // Get Plivo REST API object
        $this->_plivo = Yii::app()->plivo->getPlivo();

        // Chain parent init
        parent::init();
    }

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        if(self::LOG_BEFORE_ACTION && YII_DEBUG) {
            // Log things for informational purposes
            $this->_logThings();
        }

        // Determine what URL we will be using for the Admin Socket Server
        $this->_nodeUrl = YII_DEBUG ? 'http://dev-nicole.christineleeteam.com:8080' : 'http://node-admin-beta.seizethemarket.net';

        // New Plivo Response
        $this->_response = new \StmPlivoFunctions\Response();

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Answer Confirm Start Action
     *
     * Called by Plivo when the phone is answered initially to start session. This confirms that voicemail didn't pickup as it requires user input addHangup.
     * @param $id integer Call list ID
     * @return void
     */
    public function actionAnswerConfirmStart($id)
    {
        try {
            $callSession = CallSessions::model()->findByAttributes(array('request_uuid'=>Yii::app()->request->getPost('RequestUUID')));
            $userId = Yii::app()->request->getParam('user_id');
            $callerIdName = Yii::app()->request->getParam('callerIdName');
            $callerIdNumber = Yii::app()->request->getParam('callerIdNumber');

            // If we were unable to create the new call session
            if(!$callSession) {
                sleep(3);
                // try one more time.
                $callSession = CallSessions::model()->findByAttributes(array('request_uuid'=>Yii::app()->request->getPost('RequestUUID')));
                if(!$callSession) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call session not found after 2 attempts. Request UUID#: '.Yii::app()->request->getPost('RequestUUID'), CLogger::LEVEL_ERROR);
                    throw new Exception('Unable to find call session');
                }
            }

            // Locate call session record
            /** @var CallLists $callList */
            $callList = $callSession->callList;

            //@todo: call session is already started by now, find existing one. the call session is created and billed. save plivo data... use plivo's uuid as PK or our own... will us our own for now - ASK NICOLE
            $callSession->setAttributes(array(
                'direction' => Yii::app()->request->getPost('Direction'),
                'from_phone' => Yii::app()->request->getPost('From'),
                'a_leg_uuid' => Yii::app()->request->getPost('ALegUUID'),
                'bill_rate' => Yii::app()->request->getPost('BillRate'),
                'to_phone' => Yii::app()->request->getPost('To'),
                'call_uuid' => Yii::app()->request->getPost('CallUUID'),
                'a_leg_request_uuid' => Yii::app()->request->getPost('ALegRequestUUID'),
                'call_status' => Yii::app()->request->getPost('CallStatus'),
                'event' => Yii::app()->request->getPost('Event'), //will most likely be "StartApp"
            ));
            if(!$callSession->save()) {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call session did not save. Error Message:' . print_r($callSession->getErrors(), true).PHP_EOL.' Attributes: '.print_r($callSession->attributes, true), CLogger::LEVEL_ERROR);
                throw new Exception('Unable to update call session! Error Message:' . print_r($callSession->getErrors(), true).PHP_EOL.' Attributes: '.print_r($callSession->attributes, true));
            }

            // get caller ID & number of lines from session.
            $numToCall = $callSession->number_lines;
            $callerIdNumber = $callSession->caller_id_number;
            $callerIdName = $callSession->caller_id_name;

            // $numToCall = Yii::app()->request->getParam('num_to_call') ? Yii::app()->request->getParam('num_to_call') : 3;
            // Wait for user to press 1 to confirm start of session
            $getDigits = $this->_response->addGetDigits(array(
                'action'        =>  StmFunctions::getSiteUrl() . '/plivo3/answer?id=' . $id.'&user_id='.$userId.'&site='.urlencode(StmFunctions::getSiteUrl()) . '&presetId=' . $callList->preset_ma,
                'redirect'      =>  'true',
                'timeout'       =>  20, //seconds
            ));

            // Add speak to get digits to the user knows what to do
            $getDigits->addSpeak('Welcome. Press 1 to start your call session.');

            // Notifies user that session is ending.
            $this->_response->addSpeak('Input not received. Your session is ending. Goodbye.');

            $maxDialerCallCount = ($callList->filter_max_call_count) ? $callList->filter_max_call_count : DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
            $maxDialerCallDays = ($callList->filter_max_call_days) ? $callList->filter_max_call_days : DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;

            // Signal node to update
            $this->_update($callList->preset_ma, $callSession->call_list_id, $userId, $callSession->id, 'answerConfirmStart', $maxDialerCallCount, $maxDialerCallDays);

            // Send the response
            $this->_sendResponse();
        }
        catch(Exception $e) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Plivo Answer Action had an error: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
            throw $e;
        }
    }

    /**
     * Answer Action
     *
     * Called by Plivo when the phone is answered
     * @id integer call list ID
     * @return void
     */
    public function actionAnswer($id)
    {
        try {
            // Get number of people to call
            $userId = Yii::app()->request->getParam('user_id');

            // Locate call session record
            /** @var CallSessions $callSession */
            $callSession = CallSessions::model()->findByAttributes(array('request_uuid' => Yii::app()->request->getPost('RequestUUID')));
            if(!$callSession) {
                throw new Exception(__CLASS__.' (:'.__LINE__.') Call session not found. Critical error. See Post & Header Data below.');
            }

            $callerIdName = $callSession->caller_id_name;
            $callerIdNumber = $callSession->caller_id_number;
            $numToCall = $callSession->number_lines;
            $filters['maxDialerCallCount'] = $callSession->filter_max_call_count;
            $filters['maxDialerCallDays'] = $callSession->filter_max_call_days;

            /** @var CallLists $callList */
            $callList = $callSession->callList;

            // Retrieve calls to make, split them in to groups of 3 for simultaneous dialing
            $o = \StmDialer\Lists\Factory::getInstance($callList->preset_ma, $callSession->call_list_id);
            $callListPhones = $o->getQueue($id, $userId, $filters, $numToCall, $nextCallMode=true);

            if(count($callListPhones) > $numToCall) {
                throw new Exception(__CLASS__.' (:'.__LINE__.') Next segment call count of '.count($callListPhones).' is greater than the phone line limit '.$numToCall.'.');
            }


            // If we have calls to make, add them
            if(count($callListPhones)) {

                // Create DB criteria
                $criteria = new CDbCriteria();
                $criteria->addInCondition('status', array('Ringing', 'Answered'));
                $criteria->compare('updated_by', $callSession->contact_id);
                $criteria->compare('call_list_id', $callSession->call_list_id);

                // Get in progress calls
                $inProgressCalls = CallListPhones::model()->findAll($criteria);

                // Calculate number to call based on number of in progress calls
                $numToCall-= count($inProgressCalls);

                // Add each number to the call list, taking in to consideration the maximum number of phones to be called
                for($i = 0; $i < $numToCall; $i++) {

                    // If we're at the end of the list, break out
                    if(!isset($callListPhones[$i])) {
                        break;
                    }

                    // Set this for the current iteration
                    /** @var array $callListPhone */
                    $callListPhone = $callListPhones[$i];

                    // Check to see if dialer caller ID phone number settings exist and apply to add dial array
                    if($callerIdNumber) {
                        if(is_numeric($callerIdNumber) && strlen($callerIdNumber) == 10) {
                            $addDialData['callerId'] = '+1'.$callerIdNumber; //9043029891 CLee's number
                        }
                    }

                    // check to see if dialer caller ID name settings exist and apply to add dial array
                    if($callerIdName) {
                        if($callerIdName) {
                            $addDialData['callerName'] = $callerIdName;
                        }
                    }

                    // Construct call data
                    $callData = array(
                        'from'                      =>  '19043301885',
                        'to'                        =>  '1' . $callListPhone['phone'],
                        'machine_detection'         =>  'false',
                        'answer_url'                =>  StmFunctions::getSiteUrl() . '/plivo3/answerclient?site=' . urlencode(StmFunctions::getSiteUrl()) . '&presetId=' . $callList->preset_ma . '&user_id=' . $userId,
                        'hangup_url'                =>  StmFunctions::getSiteUrl() . '/plivo3/hangupclient?site=' . urlencode(StmFunctions::getSiteUrl()) . '&presetId=' . $callList->preset_ma . '&user_id=' . $userId,
                        'ring_url'                  =>  StmFunctions::getSiteUrl() . '/plivo3/ring?callListPhoneId='.$callListPhone['id'].'&callSessionId='.$callSession->id
                    );

                    // detect machine if enabling boomerang
                    if(self::DO_THE_BOOMERANG) {
                        $callData['machine_detection_url'] = StmFunctions::getSiteUrl() . '/plivo3/machinedetect';
                        $callData['machine_detection_time'] = 3000;
                    }

                    // Make a call to a client, we can make multiple calls at once here if we need to
                    $response = $this->_plivo->make_call($callData);

                    /** @var CallListPhones $record */
                    $record = CallListPhones::model()->findByPk($callListPhone['call_list_phone_id']);
                    if(!$record) {
                        throw new Exception('Error, no call list phone record found! (From within FOR loop)');
                    }

                    // Check old status
                    $oldStatus = $record->status;

                    // Update call list phone record
                    $record->last_call_session_id = $callSession->id;
                    $record->request_uuid = $response['response']['request_uuid'];
                    $record->status = 'Ringing';
                    $record->updated = new CDbExpression('NOW()');
                    $record->updated_by = $userId;
                    $record->call_count = $callListPhone->call_count + 1;

                    // If we aren't able to save
                    if(!$record->save()) {
                        throw new Exception('Error, unable to update call list phone record. Data: ' . print_r($callListPhone->getErrors(), true));
                    }
//Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI NumToCall: '.$numToCall.' CallListPhone Attributes: '.print_r($record->attributes, true).PHP_EOL.'Response: '.print_r($response, true), CLogger::LEVEL_ERROR);

                    // Create phone record
                    $call = new Calls();
                    $call->setAttributes(array(
                        'call_session_id'           => $callSession->id,
                        'call_list_phone_id'        => $record->id,
                        'dial_b_leg_request_uuid'   => $response['response']['request_uuid'],
                        'dial_a_leg_uuid'           => $callSession->a_leg_uuid,
                        'request_api_id'            => $response['response']['api_id'],
                        'added'                     => new CDbExpression('NOW()'),
                    ));

                    // Attempt to save phone record
                    if(!$call->save()) {
                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call did not save. Error Message:' . print_r($call->getErrors(), true) . PHP_EOL . ' Attributes: ' . print_r($call->attributes, true), CLogger::LEVEL_ERROR);
                        throw new Exception('Unable to save new phone record. ' . print_r($call->getErrors(), true));
                    }
                    else {
                        // make initial log of the call. Will change type and note later as status/results change for answered or not.
                        $this->_logActivity($call, TaskTypes::DIALER_DIAL, 'Dialed');
                    }

                    // If this is a boomerang call, break out of here (we only want one boomerang call at a time)
                    if($oldStatus == 'Boomerang') {
                        break;
                    }
                }

                // Infinitely play music
                $this->_response->addPlay(self::HOLD_MUSIC_URL, array(
                    'loop'  =>  0
                ));
            }
            else {
                // Message of completion
                $this->_response->addSpeak('Congratulations. All calls in your list are complete, your session is ending, goodbye.');

                // A wait is necessary for unknown reasons (the call will drop if this isn't here)
                $this->_response->addWait(array(
                    'length' => 3
                ));
            }

            //@todo: this maxDialer__ variables need a reusable way to get it's values. Being use the same way above last _update() call
            $maxDialerCallCount = ($callList->filter_max_call_count) ? $callList->filter_max_call_count : DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
            $maxDialerCallDays = ($callList->filter_max_call_days) ? $callList->filter_max_call_days : DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;

            // Signal node to update
            $this->_update($callList->preset_ma, $callSession->call_list_id, $userId, $callSession->id, 'answerRinging', $maxDialerCallCount, $maxDialerCallDays);

            // Send the response
            $this->_sendResponse();
        }
        catch(Exception $e) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Plivo Answer Action had an error: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
            throw $e;
        }
    }

    /**
     * Action Answer Client
     *
     * Called when a client picks up
     * @return void
     */
    public function actionAnswerclient()
    {
        // Grab the call list phone record
        /** @var CallListPhones $callListPhone */
        $callListPhone = CallListPhones::model()->findByAttributes(array(
            'request_uuid'  =>  Yii::app()->request->getPost('ALegRequestUUID')
        ));

        // We better have a record
        if(!$callListPhone) {
            throw new Exception('Error, no call list phone found!');
        }

        // Determine whether or not this is a machine
        $isMachine = Yii::app()->request->getPost('Machine', 'false');

        // Retrieve call session from call list phone record
        /** @var CallSessions $callSession */
        $callSession = $callListPhone->lastCallSession;

        // Update call list phone with more details
        $callListPhone->a_leg_uuid = Yii::app()->request->getPost('ALegUUID');

        // Locate call record
        /** @var Calls $call */
        $call = Calls::model()->findByAttributes(array(
            'dial_b_leg_request_uuid'   =>  Yii::app()->request->getPost('RequestUUID')
        ));

        // Make sure we have a call record
        if(!$call) {
            throw new Exception('Error, unable to locate call record!');
        }

        // Mark this call as answered
        $this->_logActivity($call, TaskTypes::DIALER_ANSWER, 'Call Answered');

        // Check to see if we are the first person to pick up, if we aren't first, hang up and log, if we are, transfer calls to conference
        if($isMachine === 'true' && $callSession->default_voicemail_id) {

            // Wait for the beep
            $this->_response->addWait(array(
                'length'    =>  120,
                'beep'      =>  'true'
            ));

            /** @var CallRecordings $callRecording */
            $callRecording = $callSession->callRecording;

            // Play selected callback message
            //@todo: Determine if we are allowed to play a pre-recorded message for this client, also determine what message to play for them.
            $this->_response->addPlay($callRecording->recording_url);

            // Set status to No Answer
            $callListPhone->status = 'No Answer';

            // Notate that we left a voicemail
            $this->_logActivity($call, TaskTypes::DIALER_LEFT_VOICEMAIL, 'Left Voicemail "' . $callRecording->title . '"');
        }

        // If the agent is NOT available and a real live person picked up
        elseif($callSession->availability != 'Available') {

            // Hangup the call if nobody is available
            $this->_response->addHangup(array(
                'reason'    =>  'busy'
            ));

            //@todo: Figure out logic for virtual assistants to handle situation, also store some extra data to indicate that this happened

            // if agent not available, handle according to boomerang flag
            if(self::DO_THE_BOOMERANG) {
                // Set status to BOOMERANG!
                $callListPhone->status = 'Boomerang';
            }
            else {
                // hangup if call_uuid exists and if that fails assume not answered yet so attempt the hangup request which handles before calls are picked up.
                if($call->call_uuid) {
                    // Attempt to hangup the call
                    $response = $this->_plivo->hangup_call(array(
                            'call_uuid' =>  $call->call_uuid
                        ));

                    // If we are unable to because it hasn't yet been answered yet, we need to use hangup_request instead
                    if($response['status'] >= 200 && $response['status'] <= 299) {
                        $this->_plivo->hangup_request(array(
                                'request_uuid'  =>  $call->dial_b_leg_request_uuid
                            ));
                    }
                }
                // cancels the request if not answered yet.
                elseif($call->dial_b_leg_request_uuid) {
                    $this->_plivo->hangup_request(array(
                            'request_uuid'  =>  $call->dial_b_leg_request_uuid
                        ));
                }
            }
        }

        // If the agent is available and a real live person picked up
        else {

            // Mark the agent as in a call
            $callSession->availability = 'In Call';
            if(!$callSession->save()) {
                throw new Exception('Error, unable to save existing call session! ' . print_r($callSession, true));
            }

            // Generate a random UUID to identify the conference call
            $conferenceId = md5(uniqid());

            // Bridge the two calls together with a conference call
            $this->_response->addConference($conferenceId, array(
                'beep'                      =>  'false',
                'startConferenceOnEnter'    =>  'true',
                'endConferenceOnExit'       =>  'false'
            ));

            // Transfer the agent to the conference call URL
            $response = $this->_plivo->transfer_call(array(
                'call_uuid' =>  $callSession->a_leg_uuid,
                'aleg_url'  =>  StmFunctions::getSiteUrl() . '/plivo3/agentconferencejoin?id=' . $conferenceId . '&callId=' . $call->id
            ));

            // Set call list phone status to answered
            $callListPhone->status = 'Answered';
            $callListPhone->a_leg_uuid = Yii::app()->request->getPost('ALegUUID');

            // Set call status to answer
            $call->status = 'Answer';

            // Set call dial b leg status to answer
            $call->dial_b_leg_status = 'answer';

            // Hangup on all other people pending
            if(!self::DO_THE_BOOMERANG) {

                // Initiate hangup on all other individuals who never got the chance to talk to an agent =[
                $callsToHangup = Yii::app()->db->createCommand("SELECT call_uuid, dial_b_leg_request_uuid FROM calls WHERE call_session_id=" . intval($callSession->id))->queryAll();
                foreach($callsToHangup as $callToHangup) {

                    /** @var Calls $callListPhone */

                    // Check to see if we want to skip this iteration (for the call in progress)
                    if($callToHangup->call_list_phone_id == $callListPhone->id) {
                        continue;
                    }

                    if($callToHangup['call_uuid']) {

                        // Attempt to hangup the call
                        $response = $this->_plivo->hangup_call(array(
                            'call_uuid' =>  $callToHangup['call_uuid']
                        ));

                        // If we are unable to because it hasn't yet been answered yet, we need to use hangup_request instead
                        if($response['status'] >= 200 && $response['status'] <= 299) {
                            $this->_plivo->hangup_request(array(
                                'request_uuid'  =>  $callToHangup['dial_b_leg_request_uuid']
                            ));
                        }
                    }
                }
            }
        }

        // Update record data
        $callListPhone->updated_by = $callSession->contact_id;
        $callListPhone->updated = new CDbExpression('NOW()');
        $callListPhone->is_deleted = 0;

        // Attempt to save record
        if(!$callListPhone->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving call to update status to Answered. Call List Phone Attributes: ' . print_r($callListPhone->attributes, true) . ' Error Message: ' . print_r($callListPhone->getErrors(), true), CLogger::LEVEL_ERROR);
            throw new Exception('Error saving call list phone! ' . print_r($callListPhone->getErrors()));
        }

        // Update attributes

        // The A Leg becomes the B Leg since we are using the make_call API in place of the <dial> element.
        $call->dial_b_leg_uuid = Yii::app()->request->getPost('ALegUUID');

        // Add additional attributes for the connected call
        $call->event = Yii::app()->request->getPost('Event');

        $call->call_uuid = Yii::app()->request->getPost('CallUUID') ? Yii::app()->request->getPost('CallUUID') : $call->call_uuid;

        $call->dial_b_leg_to = Yii::app()->request->getPost('To') ? Yii::app()->request->getPost('To') : $call->dial_b_leg_to;
        $call->dial_b_leg_total_cost = Yii::app()->request->getPost('TotalCost') ? Yii::app()->request->getPost('TotalCost') : $call->dial_b_leg_total_cost;
        $call->dial_b_leg_bill_rate = Yii::app()->request->getPost('BillRate') ? Yii::app()->request->getPost('BillRate') : $call->dial_b_leg_bill_rate;
        $call->dial_b_leg_bill_duration = Yii::app()->request->getPost('BillDuration') ? Yii::app()->request->getPost('BillDuration') : $call->dial_b_leg_bill_duration;
        $call->dial_b_leg_from = Yii::app()->request->getPost('From') ? Yii::app()->request->getPost('From') : $call->dial_b_leg_from;
        $call->dial_b_leg_position = Yii::app()->request->getPost('Position') ? Yii::app()->request->getPost('Position') : $call->dial_b_leg_position;
        $call->dial_b_leg_hangup_cause = Yii::app()->request->getPost('HangupCause') ? Yii::app()->request->getPost('HangupCause') : $call->dial_b_leg_hangup_cause;
        $call->dial_b_leg_duration = Yii::app()->request->getPost('Duration') ? Yii::app()->request->getPost('Duration') : $call->dial_b_leg_duration;

        $call->dial_action = Yii::app()->request->getPost('DialAction') ? Yii::app()->request->getPost('DialAction') : $call->dial_action;
        $call->event = Yii::app()->request->getPost('Event') ? Yii::app()->request->getPost('Event') : $call->event;

        $call->answer_time = Yii::app()->request->getPost('AnswerTime') ? Yii::app()->request->getPost('AnswerTime') : $call->answer_time;
        $call->start_time = Yii::app()->request->getPost('StartTime') ? Yii::app()->request->getPost('StartTime') : $call->start_time;
        $call->end_time = Yii::app()->request->getPost('EndTime') ? Yii::app()->request->getPost('EndTime') : $call->end_time;

        // Attempt to save call record
        if(!$call->save()) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call did not save. Error Message:' . print_r($call->getErrors(), true).PHP_EOL.' Attributes: '.print_r($call->attributes, true), CLogger::LEVEL_ERROR);
            throw new Exception('Error, unable to save call record! Details: ' . print_r($callListPhone->getErrors(), true));
        }

        // Do some cleanup
        switch(CallLists::getTableTypeById($callListPhone->call_list_id)) {

            case CallLists::TABLE_TYPE_TIME:
//                $this->_cleanTimeBasedQueue($callListPhone->call_list_id);
            break;

            case CallLists::TABLE_TYPE_TASK:
                $this->_cleanTaskBasedQueue($callListPhone->call_list_id);
            break;
        }

        /** @var CallLists $callList */
        $callList = $callSession->callList;

        //@todo: this maxDialer__ variables need a reusable way to get it's values. Being use the same way above last _update() call
        $maxDialerCallCount = ($callList->filter_max_call_count) ? $callList->filter_max_call_count : DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
        $maxDialerCallDays = ($callList->filter_max_call_days) ? $callList->filter_max_call_days : DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;

        // Signal node to update
        $this->_update($callList->preset_ma, $callSession->call_list_id, $callSession->contact_id, $callSession->id, 'answerRinging', $maxDialerCallCount, $maxDialerCallDays);

        // Send the XML response
        $this->_sendResponse();
    }

    /**
     * Machine Detect
     *
     * Placeholder for machine detection stuff, note the actual machine detection is doing in actionAnswerClient()
     * @return void
     */
    public function actionMachinedetect()
    {
        // Send the XML response
        $this->_sendResponse();
    }

    /**
     * Action Hangup Client
     *
     * Called when an client hangs up
     * @return void
     */
    public function actionHangupclient()
    {
        // Grab the call list phone
        /** @var CallListPhones $callListPhone */
        $callListPhone = CallListPhones::model()->findByAttributes(array(
            'request_uuid'  =>  Yii::app()->request->getPost('RequestUUID'),
        ));

        // We better have a record
        if(!$callListPhone) {
            throw new Exception('Error, no call list phone found!');
        }

        // Retrieve call session
        /** @var CallSessions $callSession */
        $callSession = $callListPhone->lastCallSession;

        // If call was answered, then we want to mark the agent as available and transfer them back to waiting
        if($callListPhone->status == 'Answered') {

            // Mark the agent as available once again
            $callSession->availability = 'Available';
            if(!$callSession->save()) {
                throw new Exception('Unable to save call session! ' . print_r($callSession->getErrors(), true));
            }

            // Transfer the agent back to the hold music
            $this->_plivo->transfer_call(array(
                'call_uuid' =>  $callSession->a_leg_uuid,
                'aleg_url'  =>  StmFunctions::getSiteUrl() . '/plivo3/nextcall'
            ));
        }

        // If this wasn't an active call, check to see if it was the last open call so we can transfer the agent back
        else {

            // Retrieve all call list phones to reference
            $callListPhones = CallListPhones::model()->findByAttributes(array(
                'call_list_id'  =>  $callListPhone->call_list_id,
                'status'        =>  'Answered'
            ));

            // If we don't have any calls that are currently ringing or answered, transfer the agent
            if(!count($callListPhones)) {

                // Sanity check
                if(empty($callSession->a_leg_uuid)) {
                    throw new Exception('Error, a leg UUID is empty, unable to transfer agent back to the hold music! Call Session: ' . print_r($callSession->attributes, true) . ' Call list phone: ' . print_r($callListPhone->attributes, true));
                }

                // Transfer the agent back to the hold music
                $this->_plivo->transfer_call(array(
                    'call_uuid' =>  $callSession->a_leg_uuid,
                    'aleg_url'  =>  StmFunctions::getSiteUrl() . '/plivo3/nextcall'
                ));
            }
        }

        // Locate call record
        $call = Calls::model()->findByAttributes(array(
//            'dial_b_leg_uuid'   =>  Yii::app()->request->getPost('ALegUUID')
            'dial_b_leg_request_uuid'   =>  Yii::app()->request->getPost('RequestUUID')
        ));

        // Make sure we have a call record
        if(!$call) {
            throw new Exception('Error, unable to locate call record! UUID: ' . Yii::app()->request->getPost('ALegUUID'));
        }

        // Get the reason for hanging up
        switch(Yii::app()->request->getParam('HangupCause') ) {
            case 'USER_BUSY':
            case 'NORMAL_TEMPORARY_FAILURE':
            case 'NORMAL_CIRCUIT_CONGESTION';
            case 'NO_ANSWER':
                $hangupCause = 'No Answer';
                $this->_logActivity($call, TaskTypes::DIALER_NO_ANSWER, 'No Answer');
                break;

            case 'ORIGINATOR_CANCEL':
                // agent disconnected in the middle?... need to read logs to confirm.
                $hangupCause = 'No Answer';
                break;

            case 'NORMAL_CLEARING':
                $hangupCause = Yii::app()->request->getPost('Duration') > 10 ? 'Complete' : 'No Answer';
                break;

            case 'CALL_REJECTED':
            case 'UNKNOWN';             // "Welcome to verizon wireless. The number you have dialed has been change, disconnect or is no longer in service..."
            case 'UNALLOCATED_NUMBER':
            case 'NORMAL_UNSPECIFIED':
            case 'NO_USER_RESPONSE';    // "You have reached a non-working number..."
                $hangupCause = 'Bad Number';
                $this->_logActivity($call, TaskTypes::DIALER_BAD_NUMBER_DELETED, 'Bad Number Deleted');

                // remove the phone # from the list, save happens below
                $callListPhone->is_deleted = 1;

                //delete the bad phone #
                $phone = $callListPhone->phone;
                $phone->delete();

                break;

            default:
                $hangupCause = 'No Answer';
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Plivo Client Hangup reason unrecognized. DialBLegHangupCause: '.Yii::app()->request->getParam('DialBLegHangupCause'), CLogger::LEVEL_ERROR);
                break;
        }

        // Update record data
        $callListPhone->status = $callListPhone->status == 'Boomerang' ? 'Boomerang' : $hangupCause;
        $callListPhone->updated_by = $callSession->contact_id;
        $callListPhone->updated = new CDbExpression('NOW()');

        // Attempt to save record
        if(!$callListPhone->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving call to update status to "' . $hangupCause . '". Call List Phone Attributes: ' . print_r($callListPhone->attributes, true) . ' Error Message: '.print_r($callListPhone->getErrors(), true), CLogger::LEVEL_ERROR);
            throw new Exception('Error, unable to save call list phone! Data: ' . print_r($callListPhone->getErrors()));
        }

        // Update attributes
        $call->event = Yii::app()->request->getPost('Event');

        $call->dial_b_leg_total_cost = Yii::app()->request->getPost('TotalCost') ? Yii::app()->request->getPost('TotalCost') : $call->dial_b_leg_total_cost;
        $call->dial_b_leg_bill_rate = Yii::app()->request->getPost('BillRate') ? Yii::app()->request->getPost('BillRate') : $call->dial_b_leg_bill_rate;
        $call->dial_b_leg_bill_duration = Yii::app()->request->getPost('BillDuration') ? Yii::app()->request->getPost('BillDuration') : $call->dial_b_leg_bill_duration;
        $call->dial_b_leg_from = Yii::app()->request->getPost('From') ? Yii::app()->request->getPost('From') : $call->dial_b_leg_from;
        $call->dial_b_leg_position = Yii::app()->request->getPost('Position') ? Yii::app()->request->getPost('Position') : $call->dial_b_leg_position;
//        $call->dial_b_leg_status = Yii::app()->request->getPost('Status') ? Yii::app()->request->getPost('Status') : $call->dial_b_leg_status;
        $call->dial_b_leg_status = 'hangup';
        $call->dial_b_leg_hangup_cause = Yii::app()->request->getPost('HangupCause') ? Yii::app()->request->getPost('HangupCause') : $call->dial_b_leg_hangup_cause;
        $call->dial_b_leg_duration = Yii::app()->request->getPost('Duration') ? Yii::app()->request->getPost('Duration') : $call->dial_b_leg_duration;

        $call->dial_action = Yii::app()->request->getPost('DialAction') ? Yii::app()->request->getPost('DialAction') : $call->dial_action;
        $call->event = Yii::app()->request->getPost('Event') ? Yii::app()->request->getPost('Event') : $call->event;

        $call->answer_time = Yii::app()->request->getPost('AnswerTime') ? Yii::app()->request->getPost('AnswerTime') : $call->answer_time;
        $call->start_time = Yii::app()->request->getPost('StartTime') ? Yii::app()->request->getPost('StartTime') : $call->start_time;
        $call->end_time = Yii::app()->request->getPost('EndTime') ? Yii::app()->request->getPost('EndTime') : $call->end_time;

        // Attempt to save call record
        if(!$call->save()) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call did not save. Error Message:' . print_r($call->getErrors(), true).PHP_EOL.' Attributes: '.print_r($call->attributes, true), CLogger::LEVEL_ERROR);
            throw new Exception('Error, unable to save call record! Details: ' . print_r($callListPhone->getErrors(), true));
        }

        // Do some cleanup
        switch(CallLists::getTableTypeById($callListPhone->call_list_id)) {

            case CallLists::TABLE_TYPE_TIME:
//                $this->_cleanTimeBasedQueue($callListPhone->call_list_id);
                break;

            case CallLists::TABLE_TYPE_TASK:
                $this->_cleanTaskBasedQueue($callListPhone->call_list_id);
                break;
        }

        /** @var CallLists $callList */
        $callList = $callSession->callList;

        //@todo: this maxDialer__ variables need a reusable way to get it's values. Being use the same way above last _update() call
        $maxDialerCallCount = ($callList->filter_max_call_count) ? $callList->filter_max_call_count : DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
        $maxDialerCallDays = ($callList->filter_max_call_days) ? $callList->filter_max_call_days : DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;

        // Signal node to update
        $this->_update($callList->preset_ma, $callSession->call_list_id, $callSession->contact_id, $callSession->id, 'hangupClient', $maxDialerCallCount, $maxDialerCallDays);

        // Send the XML response
        $this->_sendResponse();
    }

    /**
     * Action Next Call
     *
     * The agent winds up here when a client ends a call
     * @return void
     */
    public function actionNextcall()
    {
        // Grab the call list phone
        /** @var CallSessions $callListPhone */
        $callSession = CallSessions::model()->findByAttributes(array(
            'a_leg_uuid'  =>  Yii::app()->request->getParam('ALegUUID')
        ));

        // Wait for user to press 1 to continue
        $getDigits = $this->_response->addGetDigits(array(
            'action'        =>  StmFunctions::getSiteUrl() . '/plivo3/answer?id=' . $callSession->call_list_id . '&num_to_call=' . $callSession->number_lines . '&user_id=' . $callSession->contact_id . '&callerIdName=' . $callSession->caller_id_name . '&callerIdNumber=' . $callSession->caller_id_number . '&site='.urlencode(StmFunctions::getSiteUrl()),
            'redirect'      =>  'true',
            'timeout'       =>  300,
        ));

        // Add speak to get digits to the user knows what to do
        $getDigits->addSpeak('Press 1 to continue, or your session will end in five minutes.');

        // Send the XML response
        $this->_sendResponse();
    }

    /**
     * Action Agent Conference Join
     *
     * Bridges calls together by means of conference (I THINK this is the only way to do this)
     * @return void
     */
    public function actionAgentconferencejoin()
    {
        // Retrieve conference ID
        $conferenceId = Yii::app()->request->getParam('id');
        if(!$conferenceId) {
            throw new Exception('Error, no conference ID found!');
        }

        // Data for conference response
        $conferenceData = array(
            'beep'                      =>  'false',
            'startConferenceOnEnter'    =>  'true',
            'endConferenceOnExit'       =>  'true'
        );

        // Record conditionally
        if(in_array(strtolower($_SERVER['SERVER_NAME']), array('www.michiganhomehunt.com','www.christineleeteam.com')) ) {
            $callId = Yii::app()->request->getParam('callId');
            $conferenceData['record'] = 'true';
            $conferenceData['redirect'] = false;
            $conferenceData['callbackUrl'] = StmFunctions::getSiteUrl() . '/plivo3/recordingCallback?callId=' . $callId; // all recording data is sent here
        }

        // Bridge the two calls together with a conference call
        $this->_response->addConference($conferenceId, $conferenceData);

        // Send the XML response
        $this->_sendResponse();
    }

    /**
     * Transfer Return to Answer
     * Used to transfer A leg to this so that it can re-route it back to calling the queue, while hanging up on B leg
     * @param $id integer call_list_id
     */
    public function actionTransferReturnToAnswer($id)
    {
        // Locate call session record
        /** @var CallSessions $callSession */
        $callSession = CallSessions::model()->findByAttributes(array('request_uuid' => Yii::app()->request->getPost('RequestUUID')));
        if(!$callSession) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Call session not found. Critical error. See Post & Header Data below.');
        }

        /** @var CallLists $callList */
        $callList = $callSession->callList;

        // Get number of people to call
        $userId = Yii::app()->request->getParam('user_id');

        // Wait for user to press 1 to continue
        $getDigits = $this->_response->addGetDigits(array(
            'action'        =>  StmFunctions::getSiteUrl() . '/plivo3/answer?id=' . $id.'&user_id='.$userId.'&site='.urlencode(StmFunctions::getSiteUrl()),
            'redirect'      =>  'true',
            'timeout'       =>  300,
        ));

        // Add speak to get digits to the user knows what to do
        $getDigits->addSpeak('Press 1 to continue, or your session will end in five minutes.');

        /** @var CallLists $callList */
        $callList = $callSession->callList;

        //@todo: this maxDialer__ variables need a reusable way to get it's values. Being use the same way above last _update() call
        $maxDialerCallCount = ($callList->filter_max_call_count) ? $callList->filter_max_call_count : DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
        $maxDialerCallDays = ($callList->filter_max_call_days) ? $callList->filter_max_call_days : DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;

        // Signal node to update
        $this->_update($callList->preset_ma, $callSession->call_list_id, $callSession->contact_id, $callSession->id, 'transferReturnToAnswer', $maxDialerCallCount, $maxDialerCallDays);

        $this->_sendResponse();
    }

    /**
     * Transfer Hangup
     *
     * Hangups up on whatever call was entered here
     * @param $id integer call ID
     */
    public function actionTransferHangup()
    {
        if($callListPhoneId = Yii::app()->request->getParam('callListPhoneId')) {
            Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."' WHERE id={$callListPhoneId}")->execute();
        }
        /**
         * Hang up on this call by leaving this blank.
         * Nothing here causes hangup on call.
         */
        $this->_sendResponse();
    }

    /**
     * Leaves Voicemail for the b-leg passed to this callback action
     * @param $id integer Call ID
     */
    public function actionTransferLeaveVoicemail($id)
    {
        $voicemailId = Yii::app()->request->getPost('voicemailId');

        if(!$voicemailId) {

            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') URGENT ERROR: Voicemail ID not found to leave voicemail message. See post data.');
        }

        if($recording = CallRecordings::model()->findByPk($voicemailId)) {

            // Set header with call list ID
            header('x-stm-calllistid: ' . $id);
            header('x-stm-clientid: ' . Yii::app()->user->clientId);
            header('x-stm-accountid: ' . Yii::app()->user->accountId);

            // change activity log
            if($activityLog = ActivityLog::model()->findByAttributes(array('call_id' => $id))) {

                $phone = Yii::app()->db->createCommand("select phone from calls c LEFT JOIN call_list_phones c2 ON c.call_list_phone_id=c2.id LEFT JOIN phones p ON c2.phone_id=p.id where c.id=".$activityLog->call_id)->queryScalar();

                $activityLog->is_spoke_to = 0;
                $activityLog->note = 'Left Voicemail "'.$recording->title.'" - '.Yii::app()->format->formatPhone($phone);
                $activityLog->task_type_id = TaskTypes::DIALER_LEFT_VOICEMAIL;

                if(!$activityLog->save()) { //undo command: update activity_log set task_type_id=42 WHERE id=226623
                    // activity log did not save, send error message
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating  activity log for voicemail. Did not save properly. Error Message: '.print_r($activityLog->getErrors(), true).' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
                }
            }
            else {
                // did not find activity log based on id,  send error message
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Activity log not found to log voicemail message left. ActivityLogId# '.$id.PHP_EOL.'Voicemail Recording Data: '.print_r($recording->attributes, true), CLogger::LEVEL_ERROR);
            }

            $this->_response->addWait(array(
                'beep' => true,
                'length' => 120,
            ));

            $this->_response->addPlay($recording->recording_url);

            $this->_sendResponse();
        }
        else {
            // log error for voicemail not found
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Voicemail Greeting not found. Voicemail ID# '.$voicemailId, CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Action Hangup Agent
     *
     * Called by Plivo when the agent hangs up
     * @return void
     */
    public function actionHangupagent()
    {
        // Completion of a session. Only a call_session completion has post data RequestUUID
        if (Yii::app()->request->getPost('RequestUUID') && Yii::app()->request->getPost('TotalCost') > 0 && Yii::app()->request->getPost('BillRate') > 0) {

            // Pull up call session
            /** @var CallSessions $callSession */
            $callSession = CallSessions::model()->findByAttributes(array('request_uuid'=>Yii::app()->request->getPost('RequestUUID')));
            if(!$callSession) {

                if(!Yii::app()->request->getPost('ALegRequestUUID')) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') ALegRequestUUID is null, should have value since RequestUUID is empty. Investigate or add more sanity checking for this.', CLogger::LEVEL_ERROR);
                }

                // if you're here, the hangup is for b-leg not a call_session. Use ALegRequestUUID to find the call session.
                if(!($callSession = CallSessions::model()->findByAttributes(array('a_leg_request_uuid'=>Yii::app()->request->getPost('ALegRequestUUID'))))) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call session not found for hangup based on reqeust_uuid AND a_leg_request_uuid. This does not fall into scenario of a call_session hangup or b-leg hangup. Review request variables and investigate.', CLogger::LEVEL_ERROR);
                    throw new Exception(__CLASS__ . ' (' . __LINE__ . ') Call session not found for hangup based on reqeust_uuid AND a_leg_request_uuid. This does not fall into scenario of a call_session hangup or b-leg hangup. Review request variables and investigate.');
                }
            }

            // Update call session with details
            $callSession->setAttributes(array(
                'total_cost' => Yii::app()->request->getPost('TotalCost'),
                'hang_up_cause' => Yii::app()->request->getPost('HangupCause'),
                'duration' => Yii::app()->request->getPost('Duration'),
                'bill_duration' => Yii::app()->request->getPost('BillDuration'),
                'answer_time' => Yii::app()->request->getPost('AnswerTime'),
                'start_time' => Yii::app()->request->getPost('StartTime'),
                'end_time' => Yii::app()->request->getPost('EndTime'),
                'call_status' => Yii::app()->request->getPost('CallStatus'), // will most likely be "completed"
                'event' => Yii::app()->request->getPost('Event'), // will most likely be "Hangup"
                'end' => new CDbExpression('NOW()'),
                'availability' => 'Offline'
            ));

            // Attempt to save call session
            if(!$callSession->save()) {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call session did not save. Error Message:' . print_r($callSession->getErrors(), true).PHP_EOL.' Attributes: '.print_r($callSession->attributes, true), CLogger::LEVEL_ERROR);
            }

            // Sets all callListPhones Ringing status to No Answer since the session is complete.
            Yii::app()->db->createCommand("UPDATE call_list_phones SET status='" . CallListPhones::NO_ANSWER."', updated=NOW() WHERE status='Answered' AND DATE(updated)='" . date('Y-m-d') . "' AND updated_by=".$callSession->contact_id)->execute();

            // Terminate all related calls
            foreach($callSession->callListPhones as $callListPhone) {

                // If we have an in progress call, terminate it
                if($callListPhone->a_leg_uuid) {
                    $this->_plivo->hangup_call(array(
                        'call_uuid' =>  $callListPhone->a_leg_uuid
                    ));
                }
            }
        }

        // Completion of a b-leg (NOT a session). B-leg hangup does not, only has CallUUID and ALegRequestUUID.
        elseif(Yii::app()->request->getPost('TotalCost') == 0 && Yii::app()->request->getPost('BillRate') == 0) {

        }

        else {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Unexpected hangup scenario. Review Post Data..', CLogger::LEVEL_ERROR);
        }


        /** @var CallLists $callList */
        $callList = $callSession->callList;

        //@todo: this maxDialer__ variables need a reusable way to get it's values. Being use the same way above last _update() call
        $maxDialerCallCount = ($callList->filter_max_call_count) ? $callList->filter_max_call_count : DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
        $maxDialerCallDays = ($callList->filter_max_call_days) ? $callList->filter_max_call_days : DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;

        // Signal node to update
        $this->_update($callList->preset_ma, $callSession->call_list_id, $callSession->contact_id, $callSession->id, 'hangupagent', $maxDialerCallCount, $maxDialerCallDays);

        // Send response
        $this->_sendResponse();
    }

    /**
     * Fallback Action
     *
     * Called by Plivo when all else fails
     * @return void
     */
    public function actionFallback()
    {
        if($_POST['Event'] !== 'StartApp') {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Fallback Url. See Post Data and fix immediately.', CLogger::LEVEL_ERROR);
        }
        $this->_sendResponse();
    }

    /**
     * Ring Action
     *
     * Called by Plivo when ringing begins, we aren't using this anywhere but some how it's still being called...
     * @return void
     */
    public function actionRing()
    {
        $this->_sendResponse();
    }

    /**
     * Clean time bases list, removing any calls for leads that have already been spoken to within the required timeframe
     *
     * @param integer $callListId Call List ID
     * @return void
     */
    protected function _cleanTimeBasedQueue($callListId) //@todo: pass the factory class, it has a lot more info we don't have to query for
    {
        $callList = CallLists::model()->findByPk($callListId);
        if(!$callList) {
            //@todo: error log
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') ERROR: Call List not found by ID: '.$callListId.' Investigate Immediately!', CLogger::LEVEL_ERROR);
        }

        $presetId = $callList->preset_ma;

        switch($presetId) {
            case CallLists::ALL_NEW_SELLER_PROSPECTS:
            case CallLists::ALL_NURTURING_SELLERS:
            case CallLists::LEAD_POOL_SELLERS_TASKS:
            case CallLists::CUSTOM_SELLERS:
                $componentTypeId = ComponentTypes::SELLERS;
                break;

            case CallLists::ALL_NEW_BUYER_PROSPECTS:
            case CallLists::ALL_NURTURING_BUYERS:
            case CallLists::LEAD_POOL_BUYERS_TASKS:
            case CallLists::CUSTOM_BUYERS:
                $componentTypeId = ComponentTypes::BUYERS;
                break;

            case CallLists::LEAD_POOL_RECRUITS:
            case CallLists::CUSTOM_RECRUITS:
                $componentTypeId = ComponentTypes::RECRUITS;
                break;

            case CallLists::LEAD_POOL_CONTACTS:
            case CallLists::CUSTOM_CONTACTS:
                $componentTypeId = ComponentTypes::CONTACTS;
                break;

            default:
                throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') ERROR: Component Type Id NOT FOUND using Call List Preset ID: '.$presetId.' Investigate Immediately!');
        }

        $lastActivityInRangeCountSubQuery = '(select count(id) from activity_log a WHERE activity_date > "'.CallLists::getCallbackDateByListId($callListId).'" AND a.component_id=t.id AND a.component_type_id='.$componentTypeId.' AND (a.task_type_id IN('.TaskTypes::BAD_PHONE.','.TaskTypes::DIALER_ANSWER.') OR (a.task_type_id='.TaskTypes::PHONE.' AND a.is_spoke_to=1)) ORDER BY activity_date DESC LIMIT 1)';

        //@todo: ***************** this if() wrapper is hacky but need fix to make custom lists work... need to account for other recruit/contact preset types *****************
        // this is not applicable for custom lists
        if(!in_array($presetId, array(CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_RECRUITS, CallLists::CUSTOM_CONTACTS))) {

            $transactionStatusId = ($presetId == CallLists::ALL_NURTURING_SELLERS || $presetId == CallLists::ALL_NURTURING_BUYERS) ? TransactionStatus::NURTURING_ID : TransactionStatus::NEW_LEAD_ID;

            //@todo: this query can probably be combined with the one below as subquery
            // get list of all contactIds related to callListPhones for the current callListId
            $contactIds = Yii::app()->db->createCommand("select distinct p.contact_id from call_list_phones c LEFT JOIN phones p ON p.id=c.phone_id AND c.is_deleted=0 AND c.call_list_id={$callListId} AND c.status NOT IN('".CallListPhones::ANSWERED."','".CallListPhones::RINGING."') WHERE p.contact_id IS NOT NULL")->queryColumn();

            // pre-filter out contactsIds that need to be removed - reasons are because transaction status changed or spoke to changed
            if($contactIds) {


                // select contactIds that no longer have a transaction with the required status
                $contactIdsToRemove = Yii::app()->db->createCommand("select distinct c.id from contacts c LEFT JOIN phones p ON c.id=p.contact_id LEFT JOIN call_list_phones c2 ON c2.phone_id=p.id AND c2.is_deleted=0 LEFT JOIN transactions t ON t.contact_id=c.id AND t.transaction_status_id IN ({$transactionStatusId}) AND t.component_type_id={$componentTypeId} AND t.is_deleted=0 WHERE c2.call_list_id={$callListId} AND  t.id IS NULL AND c.id IN (".implode(',',$contactIds).")")->queryColumn();

                if($contactIdsToRemove) {
                    // remove the contact Ids from the call_list_phones without transaction with proper status related to them
                    $contactIdsDeleted = Yii::app()->db->createCommand("UPDATE call_list_phones c LEFT JOIN phones p ON c.phone_id=p.id SET c.is_deleted=1 WHERE c.call_list_id={$callListId} AND p.contact_id IN(".implode(',',$contactIdsToRemove).") AND c.status NOT IN ('".CallListPhones::ANSWERED."','".CallListPhones::RINGING."')")->execute();

                    if($contactIdsDeleted) {
                        $this->_log2(__CLASS__, __LINE__, $contactIdsDeleted.' callListPhones have been deleted where the transaction status id is no longer a match for the list type.'.PHP_EOL.'Contact Ids to remove: '.print_r($contactIdsToRemove, true));
                    }
                }
            }
        }

        //select any transaction/leads that have been spoken to within the time frame according to CallLists::getCallbackDateByListId() and remove them from the phone list to prevent calling again before desired timeframe
        $leadsToRemoveCommand = Yii::app()->db->createCommand()
            ->select('c2.id call_list_phone_id')
            ->from('transactions t')
            ->leftJoin('contacts c', 't.contact_id = c.id')

            ->leftJoin('phones p', 'p.contact_id = c.id')
            ->leftJoin('call_list_phones c2', 'c2.phone_id = p.id')
            ->leftJoin('call_lists c3', 'c2.call_list_id = c3.id')

            // narrows it down to last activity before the time frame threshold based on the call list setting
            ->where($lastActivityInRangeCountSubQuery.' >= 1 OR c2.call_count > '.DialerController::FILTER_DEFAULT_MAX_CALL_COUNT)
            ->andWhere('t.component_type_id='.$componentTypeId)
            ->andWhere('t.is_deleted=0 AND c.is_deleted=0')

            ->andWhere('c3.preset_ma='.$presetId)
            ->andWhere('c2.status NOT IN ("'.CallListPhones::ANSWERED.'","'.CallListPhones::RINGING.'")') //@todo: add any other status needed ... check with $beforeLastDate criteria for all scenarios, had to rush to put it in for hotfix
            ->andWhere('c2.is_deleted=0')

            ->order('c.last_login DESC, t.added DESC, t.id, t.contact_id');

        // if it's not a custom list then we care about transaction status id and add a condition for it.
        if(!in_array($presetId, array(CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_RECRUITS, CallLists::CUSTOM_CONTACTS))) {
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI, preset ID: '.$presetId, CLogger::LEVEL_ERROR);
            $leadsToRemoveCommand->andWhere('t.transaction_status_id='.$transactionStatusId);
        }

        if($callListPhonesToRemove = $leadsToRemoveCommand->queryColumn()) {
            //@todo: REMOVE THIS LOGGING LATER - log result for now until proven correct result set for a week.
            //log data of leads to remove that have spoke to activities within the timeframe. This should occurred from the phone calls made with the dialer.
            $logContent = date('Y-m-d H:i:s').': NOTE: Call List Preset: '.$presetId.' | Transaction Status ID: '.$transactionStatusId.' | Data to Remove: '.implode(',',$callListPhonesToRemove) . ' | Query: ' . $leadsToRemoveCommand->getText().PHP_EOL;

            $callListPhonesToRemoveIds = implode(',',$callListPhonesToRemove);

            $leadsToRemoveResult = Yii::app()->db->createCommand('UPDATE call_list_phones c SET c.is_deleted=1 WHERE c.id IN('.$callListPhonesToRemoveIds.') AND c.status NOT IN ("'.CallListPhones::RINGING.'","'.CallListPhones::ANSWERED.'")')->execute();

            if($leadsToRemoveResult) {
                $logContent = date('Y-m-d H:i:s').': RESULT: Call List Preset: '.$presetId.' | Transaction Status ID: '.$transactionStatusId.' | '.$leadsToRemoveResult.' call_list_phones records Successfully Removed!'.PHP_EOL;
                $this->_log2(__CLASS__, __LINE__, $leadsToRemoveResult.' callListPhones have been deleted due to number of activities in timeframe.'.PHP_EOL.'Call List Phone Ids to remove: '.print_r($callListPhonesToRemove, true));
            }
        }
    }

    /**
     * Clean time bases list, removing any calls for leads that have already been spoken to within the required timeframe
     *
     * @param integer $callListId Call List ID
     * @return void
     */
    protected function _cleanTaskBasedQueue($callListId)
    {

    }

    /**
     * Action Log Activity
     *
     * Logs activity to database
     * @return void
     */
    protected function _logActivity(Calls $call, $taskTypeId, $message)
    {
        // this is a task type
        if($call->callListPhone->task_id && ($task = $call->callListPhone->task)) {
            // find out if there is a spoke to activity previous to determine if it's new lead gen or follow-up
            $spokeToCount = Yii::app()->db->createCommand("select count(*) from activity_log where component_type_id={$task->component_type_id} AND component_id={$task->component_id} AND is_spoke_to=1 AND (is_deleted=0 OR is_deleted IS NULL)")->queryScalar();
            $componentTypeId = $task->component_type_id;
            $componentId = $task->component_id;
        }
        else {

            if(!($contactId = $call->callListPhone->phone->contact_id)) {
                $contactId = Yii::app()->db->createCommand("select c2.id from calls c LEFT JOIN call_list_phones l ON l.id=c.call_list_phone_id LEFT JOIN phones p ON p.id=l.phone_id LEFT JOIN contacts c2 ON c2.id=p.contact_id WHERE c.id=".$call->id)->queryScalar();
                $foundMessage = PHP_EOL.'-------------------------------------------------------------------------------'.PHP_EOL
                    .' NOTE: **** Secondary Query found results. This means the error is due to soft deletes. Consider switching to this raw query if bug persists. **** '
                    .PHP_EOL.'-------------------------------------------------------------------------------'.PHP_EOL;
                if(!$contactId) {
                    $foundMessage = '';
                    // 2nd query didn't find record. Now it's a serious issue.
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT ERROR: Call record could not find the contact ID. 2nd query did not find it either!!'.$foundMessage.PHP_EOL
                        .'Call Attributes: '.print_r($call->attributes, true).PHP_EOL
                        .'Call List Phone: '.print_r($call->callListPhone->attributes, true).PHP_EOL
                        .'Phone Attributes: '.print_r($call->callListPhone->phone->attributes, true), CLogger::LEVEL_ERROR);
                }
            }

            switch($presetId = $call->callListPhone->callList->preset_ma) {

                case CallLists::MY_SELLERS_TASKS:
                case CallLists::ALL_NEW_SELLER_PROSPECTS:
                case CallLists::ALL_NURTURING_SELLERS:
                case CallLists::LEAD_POOL_SELLERS_TASKS:
                case CallLists::ALL_D_SELLER_PROSPECTS:
                case CallLists::CUSTOM_SELLERS:
                    //$transactionStatusId = ($presetId == CallLists::ALL_NEW_SELLER_PROSPECTS) ? TransactionStatus::NEW_LEAD_ID : TransactionStatus::NURTURING_ID;
                    switch($presetId) {
                        case CallLists::ALL_NEW_SELLER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::NEW_LEAD_ID;
                            break;

                        case CallLists::ALL_NURTURING_SELLERS:
                            $transactionStatusId = TransactionStatus::NURTURING_ID;
                            break;

                        case CallLists::ALL_D_SELLER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::D_NURTURING_SPOKE_TO_ID;
                            break;
                    }

                    $componentTypeId = ComponentTypes::SELLERS;

                    // only add this if call list type is NOT custom
                    $statusCondition = (in_array($presetId, array(CallLists::LEAD_POOL_SELLERS_TASKS, CallLists::CUSTOM_SELLERS))) ? '' : "transaction_status_id={$transactionStatusId} AND ";

                    $query = "select id from transactions where {$statusCondition} component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";

                    $componentId = Yii::app()->db->createCommand($query)->queryScalar();
                    if(!$componentId) {

                        $query2 = "select id from transactions where component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";
                        if($componentId2 = Yii::app()->db->createCommand($query2)->queryScalar()) {
                            $comment2 = ($componentId2) ? 'Found result on second query="'.$query2.'". Replaced with found componentId. This means transaction status has changed. Second query may be a better choice. Investigate immediately.' : '';
                            $componentId = $componentId2;
                        }

                        //@todo: re-anable this after custom list stuff is figured out. Custom lists triggers this 100% of the time so turning it off for now.
                        //                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Did not find Component ID in 1st query="'.$query.' '.$comment2.' Preset ID: '.$presetId.' Call Data: '.print_r($call->attributes, true).' TaskTypeId: '.$taskTypeId.' Message: '.$message, CLogger::LEVEL_ERROR);
                    }
                    break;

                case CallLists::MY_BUYERS_TASKS:
                case CallLists::ALL_NEW_BUYER_PROSPECTS:
                case CallLists::ALL_NURTURING_BUYERS:
                case CallLists::LEAD_POOL_BUYERS_TASKS:
                case CallLists::ALL_D_BUYER_PROSPECTS:
                case CallLists::CUSTOM_BUYERS:
                    //$transactionStatusId = ($presetId == CallLists::ALL_NEW_BUYER_PROSPECTS) ? TransactionStatus::NEW_LEAD_ID : TransactionStatus::NURTURING_ID;

                    switch($presetId) {
                        case CallLists::ALL_NEW_BUYER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::NEW_LEAD_ID;
                            break;

                        case CallLists::ALL_NURTURING_BUYERS:
                            $transactionStatusId = TransactionStatus::NURTURING_ID;
                            break;

                        case CallLists::ALL_D_BUYER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::D_NURTURING_SPOKE_TO_ID;
                            break;
                    }

                    $componentTypeId = ComponentTypes::BUYERS;

                    // only add this if call list type is NOT custom
                    //$statusCondition = ($presetId == CallLists::CUSTOM_BUYERS) ? '' : "transaction_status_id={$transactionStatusId} AND ";
                    $statusCondition = (in_array($presetId, array(CallLists::LEAD_POOL_BUYERS_TASKS, CallLists::CUSTOM_BUYERS))) ? '' : "transaction_status_id={$transactionStatusId} AND ";

                    $query = "select id from transactions where {$statusCondition} component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";

                    $componentId = Yii::app()->db->createCommand($query)->queryScalar();
                    if(!$componentId) {
                        // try a query without the transaction status
                        $query2 = "select id from transactions where component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";
                        $componentId = Yii::app()->db->createCommand($query2)->queryScalar();

                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Did not find Component ID. Could be due to status change in transaction. Found '.(($componentId)? '1': 'NO' ).' result with Query 2.'.PHP_EOL
                            .'Query 1:"'.$query.PHP_EOL
                            .'Query 2: "'.$query2.'"'.PHP_EOL
                            .'Preset ID: '.$presetId.PHP_EOL
                            .'Call Data: '.print_r($call->attributes, true).' TaskTypeId: '.$taskTypeId.' Message: '.$message, CLogger::LEVEL_ERROR);
                    }

                    break;

                case CallLists::MY_CONTACTS_TASKS:
                case CallLists::CUSTOM_CONTACTS:
                case CallLists::LEAD_POOL_CONTACTS_TASKS:
                    $componentTypeId = ComponentTypes::CONTACTS;
                    $componentId = $contactId;
                    break;


                case CallLists::MY_RECRUITS_TASKS:
                case CallLists::CUSTOM_RECRUITS:
                case CallLists::LEAD_POOL_RECRUITS_TASKS:
                    $componentTypeId = ComponentTypes::RECRUITS;
                    $componentId = Yii::app()->db->createCommand("SELECT id FROM recruits WHERE contact_id={$contactId}")->queryScalar();
                    break;

                default:
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Unknown type of table for getting calls by list Preset ID: '.$presetId.' Call Data: '.print_r($call->attributes, true).' TaskTypeId: '.$taskTypeId.' Message: '.$message, CLogger::LEVEL_ERROR);
                    throw new Exception('Unknown type of table for getting calls by list Preset ID. '.$presetId.' Call Data: '.print_r($call, true).' TaskTypeId: '.$taskTypeId);
                    break;
            }


            if($componentId) {
                $spokeToCount = Yii::app()->db->createCommand("select count(*) from activity_log where component_type_id={$componentTypeId} AND component_id={$componentId} AND is_spoke_to=1 AND (is_deleted=0 OR is_deleted IS NULL)")->queryScalar();
            }
        }

        // Check for pre-existing activity log entry for update
        $activityLog = ActivityLog::model()->findByAttributes(array(
            'call_id' => $call->id
        ));

        // Update existing, or create new
        if($activityLog) {

            // Update activity log details
            $activityLog->setAttributes(array(
                'task_type_id' => $taskTypeId,
                'component_type_id' => $componentTypeId,
                'component_id' => $componentId,
                'lead_gen_type_ma' => ($spokeToCount)? ActivityLog::LEAD_GEN_FOLLOW_UP_ID : ActivityLog::LEAD_GEN_NEW_ID,
                'is_spoke_to' => 0,
                'note' => $message. ' - '.Yii::app()->format->formatPhone($call->callListPhone->phone->phone)
            ));
        }
        else {

            // Create the new activity log
            $activityLog = new ActivityLog();
            $activityLog->setAttributes(array(
                'task_type_id' => $taskTypeId,
                'account_id' => Yii::app()->user->accountId,
                'component_type_id' => $componentTypeId,
                'component_id' => $componentId,
                'activity_date' => new CDbExpression('NOW()'),
                'lead_gen_type_ma' => ($spokeToCount)? ActivityLog::LEAD_GEN_FOLLOW_UP_ID : ActivityLog::LEAD_GEN_NEW_ID,
                'is_spoke_to' => 0,
                'call_id' => $call->id,
                'note' => $message. ' - '.Yii::app()->format->formatPhone($call->callListPhone->phone->phone),
                'added_by' => $call->callSession->contact_id,
            ));
        }

        // Attempt to save activity log
        if(!$activityLog->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Activity Log. Attributes: '.print_r($activityLog->attributes, true).' Error Message: '.print_r($activityLog->getErrors(), true), CLogger::LEVEL_ERROR);
//            throw new Exception('Unable to insert/update activity log entry! Params: ' . $activityLog->getErrors());
        }
    }

    /**
     * Callback for Completed Call Recording
     *
     * Updates DB calls table with received information
     * @return void
     */
    public function actionRecordingCallback()
    {
        //Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Review POST data.', CLogger::LEVEL_ERROR);
        $response = json_decode($_REQUEST['response'], true);

        $callId = Yii::app()->request->getParam('callId');

        if(!$callId) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call Id is missing. Review POST data.', CLogger::LEVEL_ERROR);
            return;
        }

        if(!($call = Calls::model()->findByPk($callId))) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call not found by callId. Review POST data.', CLogger::LEVEL_ERROR);
            return;
        }

        $call->setAttributes(array(
            'recording_id' => $response['recording_id'],
            'record_api_id' => $response['api_id'],
            'record_url' => $response['record_url'],
            'recording_call_uuid' => $response['call_uuid'],
            'recording_duration' => $response['recording_duration'],
            'recording_duration_ms' => $response['recording_duration_ms'],
            'recording_start_ms' => $response['recording_start_ms'],
            'recording_end_ms' => $response['recording_end_ms'],
            'record_message' => $response['message'],
        ));
        if(!$call->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Recording Callback Error: Recording Data did not save. Review POST data.', CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Recording Start
     * Takes post data sent by Plivo when a call recording starts and store is with the correct record in the calls table
     *
     * @return void
     */
    public function actionRecordingStart()
    {
//        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Recording Start START info. Review POST data.', CLogger::LEVEL_ERROR);
//        $callUuid = Yii::app()->request->getParam('CallUUID');
//
//        if(!$callUuid) {
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call Uuid is missing. Review POST data.', CLogger::LEVEL_ERROR);
//            return;
//        }
//
//        $criteria = new CDbCriteria();
//        $criteria->compare('call_uuid', $callUuid);
//        $criteria->compare('dial_b_leg_status', 'answer');
//        $criteria->addCondition('added >= DATE_SUB(NOW(), INTERVAL 20 second)');
//        $criteria->addCondition('record_url IS NULL');
//        $criteria->order = 'id DESC';
//
//        if(!($call = Calls::model()->find($criteria))) {
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call not found by callUuid. Review POST data.', CLogger::LEVEL_ERROR);
//            return;
//        }
//
//        $call->setAttributes(array(
//                'recording_id' => Yii::app()->request->getPost('RecordingID'),
//                'record_url' => Yii::app()->request->getPost('RecordUrl'),
//            ));
//        if(!$call->save()) {
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Recording Start Error: Recording Data did not save. Review POST data.', CLogger::LEVEL_ERROR);
//        }

//        $result = Yii::app()->db->createCommand()->update('calls', $columns=array(
////                'record_api_id' => Yii::app()->request->getPost('????'), // doesn't exist in API class calls but exists in Response class that gets an XML response
//                'recording_id' => Yii::app()->request->getPost('RecordingID'),
//                'record_url' => Yii::app()->request->getPost('RecordUrl'),
//            ),
//            $conditions='dial_b_leg_to=:dialBLegTo AND call_uuid=:calUuid',
//            $params = array(
//                ':dialBLegTo'=>Yii::app()->request->getPost('To'),
//                ':calUuid' => Yii::app()->request->getPost('CallUUID'),
//            )
//        );

//        if($result==1) {
//            // if updated at least 1 row, this is the desired result... no action needed for now
//        }
//        elseif($result > 1) {
//            // multiple rows effected, which is an error. There should only be one row.
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Start Recording  Error: More than 1 row was updated for call recording. Condition= Review POST data.', CLogger::LEVEL_ERROR);
//        }
//        else {
//            // no rows update, which means no matches for recording_id, not good. Data saving error in the record action part of the process
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Start Recording  Error: Recording Data did not save. Review POST data.', CLogger::LEVEL_ERROR);
//        }

        //@todo: delete when done testing
        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Start Recording function. Review POST data.', CLogger::LEVEL_ERROR);
    }

    public function  actionCleanDeletedPhones()
    {
        //@todo: moving this whole method to node in callback route 3/23/15 - this is a temporary solution
        $callListId = Yii::app()->request->getParam('id');
        if(Yii::app()->request->getParam('key') != 'tr5chAPra4ra') {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Incorrect key with the cleanDeletePhones. Potential unauthorized intrusion. Please review server data below.');
        }

        // search and remove any phone #'s that have been deleted from the contact. NOTE: Did this select first to prevent unnecessary UPDATE calls if not needed
        //query: "SELECT count(c.id) FROM call_list_phones c LEFT JOIN phones p ON c.phone_id = p.id WHERE c.call_list_id={$callListId} AND p.is_deleted=1 AND c.is_deleted=0"
        $hasRemoveItems = \Yii::app()->db->createCommand()
            ->select('count(c.id)')
            ->from('call_list_phones c')
            ->leftJoin('phones p','c.phone_id = p.id')
            ->where("c.call_list_id={$callListId} AND p.is_deleted=1 AND c.is_deleted=0")
            ->queryScalar();

        if($hasRemoveItems) {
            $removeInactivePhones = \Yii::app()->db->createCommand("UPDATE call_list_phones c LEFT JOIN phones p ON c.phone_id = p.id SET c.is_deleted=1, c.updated=NOW() WHERE c.call_list_id={$callListId} AND p.is_deleted=1 AND c.is_deleted=0 AND c.status NOT IN('".CallListPhones::ANSWERED."','".CallListPhones::RINGING."')")->execute();

            // log if deletes existed
            if($removeInactivePhones) {
                $this->_log2(__CLASS__, __LINE__, $removeInactivePhones.' callListPhones have been deleted due to phones being deleted.');
            }
        }
    }

    /**
     * Send Response
     *
     * Send the XML response
     * @return void
     */
    protected function _sendResponse()
    {
        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $this->_response->toXML();

        // Terminate execution
        Yii::app()->end();
    }

    /**
     * Log Things
     *
     * Logs API calls from Plivo for informational purposes
     * @return void
     */
    protected function _logThings()
    {
//        if(!YII_DEBUG) {
//            return;
//        }

        // Clean output buffer
        ob_clean();
        ob_start();
        print $this->action->id." Action: \n\n";

        // Test receiving data
        print "POST Variables \n";
        print "------------- \n\n";
        print_r($_POST);

        print "\n\nGET Variables \n";
        print "------------- \n\n";
        print_r($_GET);

        print "\n\nSERVER Variables \n";
        print "---------------- \n\n";
        print_r($_SERVER);

        // Nginx doesn't have this functionality =(
        if(function_exists('getallheaders')) {
            print "\n\nPost Headers \n";
            print "--------- \n\n";
            print_r(getallheaders());
        }

        // Store the data
        file_put_contents(self::LOG_LOCATION.'/plivo-log-' . date('Y-m-d_h-i-s') .'_'.microtime(true). '-' . Yii::app()->controller->action->id, ob_get_contents(), FILE_APPEND); //microtime(true)
        ob_clean();
    }

    /**
     * Log2
     *
     * Newer log method used to debug and find final bug for why "Answered" is not popping up. Logging all delete updates when it occurs.
     * @return void
     */
    protected function _log2($class, $line, $message)
    {
//        if(!YII_DEBUG) {
//            return;
//        }

        // Clean output buffer
        ob_clean();
        ob_start();
        print "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        print date('Y-m-d_h:i:s') ." $class (:$line) [Action: ".$this->action->id."] - $message";
        print "UserId: ".Yii::app()->request->getParam('user_id')." | SERVER Request URI: ".$_SERVER['REQUEST_URI']."\n";

        // Store the data
        file_put_contents(self::LOG_LOCATION.'/plivo-log-' .  date('Y-m-d_h-i-s') .'_'.microtime(true). '-' . str_replace('http://www.','',StmFunctions::getSiteUrl()) , ob_get_contents(), FILE_APPEND);
        ob_clean();
    }

    /** Added: Bkozak - 3/26/2015 - Vicemail Greetings */
    public function actionAnswerConfirmStartVm()
    {
        $contact_id = Yii::app()->request->getParam('contact_id');
        $title = Yii::app()->request->getParam('title');

        $getDigits = $this->_response->addGetDigits(array(
            'action'        => StmFunctions::getSiteUrl() . '/plivo3/recordVm?contact_id=' . $contact_id."&title=".urlencode($title),
            'redirect'      => 'true',
            'validDigits'	=> '1',
            'timeout'       => 30, //seconds
        ));
        $getDigits->addSpeak('Hello. Press 1 to record your voicemail greeting.');

        $this->_response->addSpeak('Input not received. Your session is ending. Goodbye.');

        $this->_sendResponse();
    }

    public function actionHangupVm()
    {
        $this->_sendResponse();
    }

    public function actionRecordVm()
    {
        $contact_id = Yii::app()->request->getParam('contact_id');
        $title = Yii::app()->request->getParam('title');

        $this->_response->addSpeak('Please record your message after the beep. Press pound to end the recording.');

        $this->_response->addRecord(array(
            'maxLength' => 60,
            'finishOnKey' => '#',
            'playBeep' => true,
            'action' => StmFunctions::getSiteUrl() . '/plivo3/saveRecordVm?contact_id=' . $contact_id."&title=".urldecode($title),
        ));

        $this->_response->addSpeak('Recording not received. Please hang up and try again. Good bye.');

        $this->_sendResponse();
    }

    public function actionSaveRecordVm()
    {
        $callRecording = new CallRecordings();
        $callRecording->setAttributes(array(
            'type' => CallRecordings::TYPE_VOICEMAIL_GREETING,
            'title' => urldecode($_POST['title']),
            'contact_id' => $_POST['contact_id'],
            'from_number' => $_POST['From'],
            'to_number' => $_POST['To'],
            'recording_id' => $_POST['RecordingID'],
            'recording_url' => $_POST['RecordUrl'],
            'call_uuid' => $_POST['CallUUID'],
            'is_default' => 0,
            'recording_duration' => $_POST['RecordingDuration'],
            'recording_duration_ms' => $_POST['RecordingDurationMs'],
            'recording_start_ms' => $_POST['RecordingStartMs'],
            'recording_end_ms' => $_POST['RecordingEndMs'],
            'added' => new CDbExpression('NOW()'),
        ));

        if(!$callRecording->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Voicemail Recording.'.PHP_EOL.'Attributes: : '.print_r($callRecording->attributes, true).' Query: '.print_r($callRecording->getErrors(), true), CLogger::LEVEL_ERROR);
        }

        $this->_response->addSpeak('Your voice mail recording is complete. Have a great day. Goodbye.');

        $this->_sendResponse();
    }

    /** End Added: Bkozak - 3/26/2015 - Vicemail Greetings */

    /** Added: Bkozak - 3/30/2015 - SMS Message Sent Callback */
    public function actionHandleSmsSentCallback() {
        /*Array
(
      [Status] => queued
      [From] => 19043301885
      [ParentMessageUUID] => -
      [PartInfo] => -
      [To] => 19042629505
      [MessageUUID] => e282992f-42bf-4e5c-bf41-2e776c1357b7
)
Array
(
      [Status] => undelivered
      [From] => 19043301885
      [ParentMessageUUID] => e282992f-42bf-4e5c-bf41-2e776c1357b7
      [To] => 19042629505
      [PartInfo] => 1 of 1
      [TotalRate] => 0.00350
      [Units] => 1
      [TotalAmount] => 0.00350
      [MessageUUID] => e282992f-42bf-4e5c-bf41-2e776c1357b7
)
*/
    }

    public function actionReceivedSms() {
        /*
         * Array
(
      [Status] => sent
      [From] => 19043301885
      [ParentMessageUUID] => 57100ba4-ab07-4733-9c58-b533fc6c6417
      [To] => 12162170382
      [PartInfo] => 1 of 1
      [TotalRate] => 0.00350
      [Units] => 1
      [TotalAmount] => 0.00350
      [MessageUUID] => 57100ba4-ab07-4733-9c58-b533fc6c6417
)

Array
(
      [From] => 12162170382
      [TotalRate] => 0.00000
      [Text] => Hello world
      [To] => 19048006995
      [Units] => 1
      [TotalAmount] => 0.00000
      [Type] => sms
      [MessageUUID] => d5da72ca-d808-11e4-8b98-22000acb8068
)
         */

        if(!isset($_POST) || empty($_POST)) {
            $this->_sendResponse();
        }

        if(!isset($_POST['From']) || empty($_POST['From'])) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') SMS Recieved With No FROM.'.PHP_EOL.'POST: : '.print_r($_POST, true), CLogger::LEVEL_ERROR);
            Yii::app()->end();
        }
        $_POST['From'] = preg_replace('/[^0-9]/','',trim(strip_tags($_POST['From'])));

        if(!isset($_POST['To']) || empty($_POST['To'])) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') SMS Recieved With No To.'.PHP_EOL.'POST: : '.print_r($_POST, true), CLogger::LEVEL_ERROR);
            Yii::app()->end();
        }
        $_POST['To'] = preg_replace('/[^0-9]/','',trim(strip_tags($_POST['To'])));

        if(!isset($_POST['Text']) || empty($_POST['Text'])) {
            $_POST['Text'] = "";
        }
        $_POST['Text'] = trim(strip_tags($_POST['Text']));

        $inboundTo = $_POST['To'];
        if(strlen($inboundTo) > 10 && $inboundTo[0] == "1") {
            $inboundTo = substr($inboundTo, 1);
        }

        // Who was it sent To (originating SMS Number / Contact) ?
        $toCriteria = new CDbCriteria;
        $toCriteria->condition = 'phone = :phone AND status = :status';
        $toCriteria->params = array(
            ':phone'    => $inboundTo,
            ':status'		=> 1
        );
        $checkForSmsNumberHQ = TelephonyPhones::model()->find($toCriteria);

        // Could not find a registered SMS number in HQ
        if(empty($checkForSmsNumberHQ)) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: SMS Recieved but could not match contact Phone.'.PHP_EOL.'POST: : '.print_r($_POST, true), CLogger::LEVEL_ERROR);
            Yii::app()->end();
        }

        $inboundFrom = $_POST['From'];
        if(strlen($inboundFrom) > 10 && $inboundFrom[0] == "1") {
            $inboundFrom = substr($inboundFrom, 1);
        }

        StmFunctions::connectDbByClientIdAccountId($checkForSmsNumberHQ->clientAccount->client_id, $checkForSmsNumberHQ->clientAccount->account_id);

        $tryMatchFromPhones = Phones::model()->byIgnoreAccountScope()->findAllByAttributes(array('account_id' => $checkForSmsNumberHQ->clientAccount->account_id, 'phone' => trim($inboundFrom)));

        if(empty($tryMatchFromPhones)) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not match Inbound SMS From to a Phone: '.$inboundFrom.' Client ID: '.$checkForSmsNumberHQ->clientAccount->client_id.', AccountID: '.$checkForSmsNumberHQ->clientAccount->account_id.PHP_EOL, CLogger::LEVEL_ERROR);
        }

        $origniatingMessageCheckFound = false;
        if($tryMatchFromPhones) {
            foreach ($tryMatchFromPhones as $tryMatchFromPhone) {
                $origniatingMessagCheckCriteria = new CDbCriteria;
                $origniatingMessagCheckCriteria->condition = 'to_phone_id = :toPhoneId AND sent_by_contact_id = :sentByContactId';
                $origniatingMessagCheckCriteria->params = array(
                    ':toPhoneId'    => $tryMatchFromPhone->id,
                    ':sentByContactId' => $checkForSmsNumberHQ->contact_id
                );

                $origniatingMessagCheck = SmsMessages::model()->find($origniatingMessagCheckCriteria);

                if(!empty($origniatingMessagCheck)) {
                    $origniatingMessageCheckFound = true;
                    break;
                }
            }
        }

        $smsMessage = new SmsMessages;
        $smsMessage->component_type_id = NULL;
        $smsMessage->component_id = NULL;
        $smsMessage->sent_by_contact_id = NULL;
        if($origniatingMessageCheckFound) {
            $smsMessage->sent_by_contact_id = $origniatingMessagCheck->sent_to_contact_id;

            //@todo: somehow if text is from a distinct component tuple, the can assume this is the same...
//            if($origniatingMessagCheck->component_type_id) {
//                $smsMessage->component_type_id = $origniatingMessagCheck->component_type_id;
//            }
//
//            if($origniatingMessagCheck->component_id) {
//                $smsMessage->component_id = $origniatingMessagCheck->component_id;
//            }
        }
        $smsMessage->to_phone_id = NULL;
        $smsMessage->from_phone_number = $_POST['From'];
        $smsMessage->to_phone_number = $_POST['To'];
        $smsMessage->inbound_to_sms_number_id = $checkForSmsNumberHQ->id;
        $smsMessage->direction = SmsMessages::DIRECTION_INBOUND;
        $smsMessage->content = $_POST['Text'];
        $smsMessage->vendor_response = NULL;
        $smsMessage->message_uuid = $_POST['MessageUUID'];
        $smsMessage->processed_datetime = new \CDbExpression('NOW()');

        if(!$smsMessage->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not save Inbound SMS.'.PHP_EOL.'POST: '.print_r($_POST, true).PHP_EOL."Attributes: ".print_r($smsMessage->attributes, true).PHP_EOL."Errors: ".print_r($smsMessage->getErrors(), 1), CLogger::LEVEL_ERROR);
            Yii::app()->end();
        }

        // send an SMS to the owner of the phone # with text and the link for STM to check his messages
        $domain = Domains::model()->byIgnoreAccountScope()->findByAttributes(array('account_id' => $checkForSmsNumberHQ->clientAccount->account_id, 'is_primary' => 1));
        $message = 'You have a STM Text Message: '.$_POST['Text'].PHP_EOL.'Login to reply to the text. http://www.'.$domain->name.'/admin/sms/reply/'.$smsMessage->id;

        if($agent = Contacts::model()->byIgnoreAccountScope()->findByAttributes(array('account_id' => $checkForSmsNumberHQ->clientAccount->account_id, 'id' => $checkForSmsNumberHQ->contact_id))) {

            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI Agent Data: '.print_r($agent->attributes, true), CLogger::LEVEL_ERROR);
            $agentPhoneNumber = Yii::app()->db->createCommand("select value from setting_contact_values WHERE contact_id=".$checkForSmsNumberHQ->contact_id." AND setting_id=".Settings::SETTING_CELL_NUMBER)->queryScalar();

            if($agentPhoneNumber) {
                $response = Yii::app()->plivo->sendMessage($agentPhoneNumber, $message);
            }
            else {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not find agent phone. Telephony Data: '.print_r($checkForSmsNumberHQ->attributes, true), CLogger::LEVEL_ERROR);
            }
            $agentEmail = Yii::app()->db->createCommand("select email from emails WHERE contact_id=".$checkForSmsNumberHQ->contact_id." ORDER BY is_primary DESC, id DESC")->queryScalar();
            StmZendMail::easyMail(array('do-not-reply@seizethemarket.net'=> 'STM SMS Alert'), array($agentEmail => $agent->fullName), 'STM SMS Received', $message);
        }
        else {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not find contact for telephony phone #: '.$_POST['To'], CLogger::LEVEL_ERROR);

        }


        #Yii::log(__CLASS__ . ' (:' . __LINE__ . ') SMS Recieved & Saved.'.PHP_EOL.'POST: : '.print_r($_POST, true).PHP_EOL."Attributes: ".print_r($smsMessage->attributes, true), CLogger::LEVEL_ERROR);
        $this->_sendResponse();
    }
    /** End Added: Bkozak - 3/30/2015 - SMS Message Sent Callback */

    /**
     * Update
     *
     * Helper function called to tell node to update client data
     * @param int $presetId The preset ID
     * @param int $calllistId The call list ID
     * @param int $userId The user ID
     */
    protected function _update($presetId, $calllistId, $userId, $callSessionId, $eventType, $maxDialerCallCount, $maxDialerCallDays)
    {
        // Log when update method is called
        file_put_contents('/var/log/stm/dialer3/update-method', date(DATE_RFC2822) . ' - Update method Called, event type: [' . $eventType . "]\n", FILE_APPEND);

        // Run load helper and get results
        $results = $this->_loadHelper(
            $calllistId,
            $presetId,
            $userId,
            $callSessionId,
            $eventType,
            $maxDialerCallCount,
            $maxDialerCallDays
        );

        // Encode JSON data
        $jsonData = json_encode($results);

        // Initialize cURL
        $ch = curl_init($this->_nodeUrl . '/plivo3/update?site=' . urlencode(StmFunctions::getSiteUrl()) . '&presetId=' . urlencode($presetId) . '&callListId=' . urlencode($calllistId) . '&clientId=' . urlencode(Yii::app()->user->clientId) . '&accountId=' . urlencode(Yii::app()->user->accountId) . '&eventType=' . $eventType . ($userId ? '&user_id=' . urlencode($userId) : ''));

        // Set cURL options
        curl_setopt_array($ch, array(
            CURLOPT_CUSTOMREQUEST   =>  'POST',
            CURLOPT_POSTFIELDS      =>  $jsonData,
            CURLOPT_RETURNTRANSFER  =>  true,
            CURLOPT_HTTPHEADER      =>  array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData))
        ));

        // Execute cURL call and close
        $response = curl_exec($ch);
        curl_close($ch);
    }
}