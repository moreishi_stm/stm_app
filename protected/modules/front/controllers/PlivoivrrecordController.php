<?php
require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmAws.S3')."/FileManagement.php");
Yii::import('front_module.controllers.base.BasePlivoController');

/**
 * Plivo Call Controller
 *
 * Used to handle Plivo callbacks
 */
class PlivoivrrecordController extends BasePlivoController
{
    /**
     * Action Answer
     *
     * Called when someone answers
     * @return void
     */
    public function actionAnswer()
    {
        // Make sure we passed a valid record type
        if(!in_array(Yii::app()->request->getParam('recordType'), array('ivr', 'extension', 'greeting'))) {
            throw new Exception('Error, a valid record type must be passed.');
        }

        // Add message of instruction to the caller
        $this->_response->addSpeak('Please record your message after the tone. Press any key when you are finished.');

        // Begin recording
        $this->_response->addRecord(array(
            'action'        => StmFunctions::getSiteUrl() . '/plivoivrrecord/record',
            'callbackUrl'   => StmFunctions::getSiteUrl() . '/plivoivrrecord/callback?recordType=' . Yii::app()->request->getParam('recordType'),
            'playBeep'      => true,
            'maxLength'     => 300,
        ));

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Hangup
     *
     * Called when someone hangs up
     * @return void
     */
    public function actionHangup()
    {
        // Send empty response
        $this->_sendResponse();
    }

    /**
     * Action Ring
     *
     * Called when the phone is ringing
     * @return void
     */
    public function actionRing()
    {
        // Send empty response
        $this->_sendResponse();
    }

    /**
     * Action Fallback
     *
     * Called when something goes wrong
     * @return void
     */
    public function actionFallback()
    {
        // Send empty response
        $this->_sendResponse();
    }

    /**
     * Action Record
     *
     * User winds up here when recording has initially completed
     * @return void
     */
    public function actionRecord()
    {
        // Add message of completion
        $this->_response->addSpeak('Recording has completed successfully, goodbye.');

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Callback
     *
     * Called when the recording process is complete and the file is ready for use
     * @return void
     */
    public function actionCallback()
    {
        // Handle each record type
        switch(Yii::app()->request->getParam('recordType')) {

            // An IVR
            case 'ivr':
                $prefix = 'ivr';
                $record = Ivrs::model()->findByAttributes(array(
                    'record_call_request_uuid'  =>  Yii::app()->request->getPost('RequestUUID')
                ));
            break;

            // An extension
            case 'extension':
                $prefix = 'ivrExtension';
                $record = IvrExtensions::model()->findByAttributes(array(
                    'record_call_request_uuid'  =>  Yii::app()->request->getPost('RequestUUID')
                ));
            break;

            case 'greeting':
                $prefix = 'greeting';
                $record = CallRecordings::model()->findByAttributes(array(
                    'record_call_request_uuid'  =>  Yii::app()->request->getPost('RequestUUID')
                ));
            break;

            // No valid type found
            default:
                throw new Exception('Error, unknown type passed in or no type.');
            break;
        }

        // Make sure we have a record
        if(!$record) {
            throw new Exception('Error, db record not found!');
        }

        // Handle each record type
        switch(Yii::app()->request->getParam('recordType')) {

            // An IVR
            case 'ivr':
            case 'extension':
                $record->recording_call_uuid = Yii::app()->request->getPost('CallUUID');
                $record->record_url = Yii::app()->request->getPost('RecordUrl');
                $record->recording_added = new CDbExpression('NOW()');
            break;

            case 'greeting':
                $record->call_uuid = Yii::app()->request->getPost('CallUUID');
                $record->recording_url = Yii::app()->request->getPost('RecordUrl');
            break;

            // No valid type found
            default:
                throw new Exception('Error, unknown record type!');
            break;
        }

        // Store info about the recording
        $record->recording_id = Yii::app()->request->getPost('RecordingID');

        $record->recording_duration = Yii::app()->request->getPost('RecordingDuration');
        $record->recording_duration_ms = Yii::app()->request->getPost('RecordingDurationMs');
        $record->recording_start_ms = Yii::app()->request->getPost('RecordingStartMs');
        $record->recording_end_ms = Yii::app()->request->getPost('RecordingEndMs');

        // Save changes
        $record->save();

        // New AWS S3 client
        $s3 = \Aws\S3\S3Client::factory(\StmFunctions::getAwsConfig());

        // Generate temp file name
        $tempFile = '/tmp/plivo-ivr-recording-' . md5(uniqid());

        // Download recording from Plivo
        if(!file_put_contents($tempFile, fopen(Yii::app()->request->getPost('RecordUrl'), 'r'))) {
            throw new Exception('Error, unable to download recording from Plivo!');
        }

        // Store the recording in AWS S3
        $result = $s3->putObject(array(
            'Bucket'        =>  \StmAws\S3\FileManagement::getBucketNameByType(\StmAws\S3\FileManagement::SITE_BUCKET_TYPE),
            'Key'           =>  'site-files/' . Yii::app()->user->clientId . '/'. $prefix . '/' . $record->id . '.mp3',
            'SourceFile'    =>  $tempFile,
            'CacheControl'  =>  'no-cache, no-store, max-age=0, must-revalidate',
            'ContentType'   =>  'audio/mpeg'
        ));

        if(!isset($result['ObjectURL']) || empty($result['ObjectURL'])) {
            Yii::log(__CLASS__.' (:'.__LINE__.') Plivo IVR recording did not save to S3.'.PHP_EOL.print_r($result, true), CLogger::LEVEL_ERROR);
        }

        // Delete the recording from the local file system
        if(!unlink($tempFile)) {
            throw new Exception('Error, unable to delete Plivo recording from local storage!');
        }

        // Send empty response
        $this->_sendResponse();
    }
}