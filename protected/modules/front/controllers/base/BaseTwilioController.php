<?php
// Include Twilio library
Yii::import('admin_module.components.StmTwilio.twilio-php.Services.Twilio.Twiml', true);

// Set an exception handler to read the exception over the phone lolol
if(YII_DEBUG) {

    // Exception handler function
    function handleTwilioExceptions(Exception $exception) {

        // Log contents to a file
        file_put_contents('/var/log/stm/twilio-exceptions-' . date('Y-m-d_h-i-s') . '_' . microtime(true), $exception->getMessage() . "\n" . print_r($exception->getTraceAsString(), true));

        // New Twilio XML object
        $xml = new Services_Twilio_Twiml();
        $xml->speak($exception->getMessage());

        // Clean the output buffer
        ob_clean();

        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $xml;

        // Terminate execution
        Yii::app()->end();
    }

    // Set the exception handler
    set_exception_handler('handleTwilioExceptions');
}

/**
 * Base Twilio Controller
 *
 * All Twilio API controllers should extend from this controller
 */
class BaseTwilioController extends FrontController
{
    /**
     * Xml
     *
     * @var Services_Twilio_Twiml
     */
    protected $_xml;

    /**
     * Before Action
     *
     * @param $action
     * @return bool|void
     */
    public function beforeAction($action)
    {
        // New Twilio XML object
        $this->_xml = new Services_Twilio_Twiml();

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Send Response
     *
     * Send the XML response
     * @return void
     */
    protected function _sendResponse()
    {
        // Clean the output buffer
        ob_clean();

        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $this->_xml;

        // Terminate execution
        Yii::app()->end();
    }
}