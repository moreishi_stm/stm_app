<?php
Yii::import('front_module.controllers.base.BasePlivoController');

/**
 * Base Hunt Group Controller
 *
 * Used to handle Plivo callbacks
 */
abstract class BaseHuntGroupController extends BasePlivoController
{
    const FROM_NUMBER = '19046013911';
    const FROM_NAME = 'STM Live Call';

    /**
     * Hold Music URL
     *
     * @var string URL for location of music to play when agent is on hold
     */
    const CONNECT_RINGING = 'http://cdn.seizethemarket.com/assets/audio/call-ringing-60.mp3';

    const HOLD_MUSIC_URL = 'http://cdn.seizethemarket.com/plivo/goldeneye-elevator.mp3';

    const CONNECT_CHIME_URL = 'http://cdn.seizethemarket.com/assets/audio/call-chime.mp3';

    const CONNECT_HAPPY_URL = 'http://cdn.seizethemarket.com/assets/audio/call-connecting-enjoy-happy-music-female.mp3';

    const CONNECT_HAPPY_NO_GREETING_URL = 'http://cdn.seizethemarket.com/assets/audio/call-connecting-enjoy-happy-music-female-no-greeting.mp3';

    /**
     * To Phone
     *
     * @var string The To phone number
     */
    protected $_toPhone;

    /**
     * Telephony Phone
     *
     * Database record representing the telephony phone in use
     * @var TelephonyPhones
     */
    protected $_telephonyPhone;

    /**
     * Client Account
     *
     * Database record representing the client/account in use
     * @var ClientAccounts
     */
    protected $_clientAccount;


    /**
     * Get Hunt Group
     *
     * This function must be implemented in a child class
     * @return CallHuntGroups|null
     */
    protected abstract function _getHuntGroup();

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        //@todo: CLee temp fix
        if($action->id == 'recordaction' || $action->id == 'recordcallback') {
            return parent::beforeAction($action);
        }

        // Make sure we have a to phone number, removes the leading 1
        $this->_toPhone = (strlen(Yii::app()->request->getParam('To')) > 10) ? substr(Yii::app()->request->getParam('To'), 1) : Yii::app()->request->getParam('To');
        if(!$this->_toPhone) {
            throw new Exception('A to phone number must be specified');
        }

        // Lookup the to phone number to get the client_id, account_id, and database
        $this->_telephonyPhone = TelephonyPhones::model()->findByAttributes(array(
            'phone' =>  $this->_toPhone
        ));

        if(!$this->_telephonyPhone) {
            throw new Exception('Unable to locate to telephony phone record in database, ' . $this->_toPhone);
        }

        // Locate client account record
        $this->_clientAccount = ClientAccounts::model()->findByPk($this->_telephonyPhone->client_account_id);
        if(!$this->_clientAccount) {
            throw new Exception('Unable to locate to client account record in database');
        }

        // Configure database connection for the specified client
        if(!StmFunctions::frontConnectDbByClientIdAccountId($this->_clientAccount->client_id, $this->_clientAccount->account_id)) {
            throw new Exception('Error: Cannot activate db connection for client!');
        }

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Post Call Hunt Group Session Creation
     *
     * Basically use this like an event, override this to get information about the call hunt group session after it's been created.
     * @param CallHuntGroupSessions $callHuntGroupSession
     */
    protected function _postCallHuntGroupSessionCreation(CallHuntGroupSessions $callHuntGroupSession) {}

    /**
     * Action Message
     *
     * When a phone number assigned to this application receives a SMS Plivo will request this URL.
     * @return void
     */
    public function actionMessage()
    {
        $this->_sendResponse();
    }

    /**
     * Action Answer
     *
     * The URL Plivo will request when a phone number or endpoint assigned to this application receives a call. (From Plivo Docs)
     * @return void
     */
    public function actionAnswer()
    {
        // see if the from_number is on the block list
        if($callBlock = CallBlocks::model()->findByAttributes(array('phone' => substr(Yii::app()->request->getPost('From'), 1, 10)))) {

            // notify CLee for now just to keep watch
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call Block found for this incoming number. Live Call to ' . $this->_telephonyPhone->phone . '('.$this->_telephonyPhone->user_description .'). Reason for block: '.$callBlock->reason.'.  See post data.', CLogger::LEVEL_ERROR);

            // end call here
            $this->_sendResponse();
        }

        //----------------------------------------------------------------------------------------------------------
        // @todo: send CLee a text for now, remove when done observing
        $fromPhone = substr(Yii::app()->request->getPost('From'), 1, 10);
        $toPhone = substr(Yii::app()->request->getPost('To'), 1, 10);
        $existingPhones = Phones::model()->findAllByAttributes(array('phone' => $fromPhone));
        $phoneAdditionalInfo = '';
        if(count($existingPhones) == 1) {
            //figure out if it's a phone for a contact
            if($contact = $existingPhones[0]->contact) {

                $phoneAdditionalInfo = PHP_EOL . 'Contact found with this phone number.' . PHP_EOL
                    . 'Name: ' . $contact->fullName . PHP_EOL
                    . 'Email: ' . $contact->getPrimaryEmail();
            }
            elseif($company = $existingPhones[0]->company) {

                $phoneAdditionalInfo = PHP_EOL . 'Contact found with this phone number.' . PHP_EOL
                    . 'Company: ' . $company->name . PHP_EOL;
            }
        }
        elseif(count($existingPhones) > 1) {
            $phoneAdditionalInfo = PHP_EOL . count($existingPhones) . ' counts of this phone number exists.';
        }
//Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI initial Live Call post data. Telephone phone data:' . print_r($this->_telephonyPhone->attributes, true), CLogger::LEVEL_ERROR);

        $this->_response->addMessage($body = 'Live Call ('.$this->_telephonyPhone->user_description .')'.PHP_EOL.'From '.Yii::app()->format->formatPhone($fromPhone).' To: '.Yii::app()->format->formatPhone($toPhone).$phoneAdditionalInfo, array(
            'src' => self::FROM_NUMBER,
            'dst' => '19043433200',
        ));
        //----------------------------------------------------------------------------------------------------------

        // Make sure this is within normal hours of operation
        $currentTime = new DateTime();
        $businessStart = DateTime::createFromFormat('H:i a', "8:00 am");
        $businessEnd = DateTime::createFromFormat('H:i a', "9:00 pm");
        if ($currentTime < $businessStart || $currentTime > $businessEnd) {

            // Inform the caller that this is outside normal business hours
//            $this->_response->addSpeak("We're sorry, our offices are now closed. Please try your call again between 8am and 9pm. Thank you, goodbye.");
//            $this->_sendResponse();
        }

        // Retrieve hunt group
        if(!($huntGroup = $this->_getHuntGroup())) {
            // error, could not find hunt group
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call Hunt Group not found by Hunt Group ID. Telephone phone data:' . print_r($this->_telephonyPhone->attributes, true), CLogger::LEVEL_ERROR);
            $this->_sendResponse();
        }

        // Add call hunt group session record
        $callHuntGroupSession = new CallHuntGroupSessions();
        $callHuntGroupSession->call_uuid = Yii::app()->request->getPost('CallUUID');
        $callHuntGroupSession->from_number = Yii::app()->request->getPost('From');
        $callHuntGroupSession->from_caller_name = Yii::app()->request->getPost('CallerName');
        $callHuntGroupSession->to_number = Yii::app()->request->getPost('To');
        $callHuntGroupSession->call_hunt_group_id = $huntGroup->id;
        $callHuntGroupSession->added = new CDbExpression('NOW()');

        // Attempt to save
        if(!$callHuntGroupSession->save()) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call Hunt Group Session did not save. Error Message:' . print_r($callHuntGroupSession->getErrors(), true) . PHP_EOL.' Attributes: ' . print_r($callHuntGroupSession->attributes, true), CLogger::LEVEL_ERROR);
        }

        // Run this method if it exists, this would be defined in a child class extending this class
        $this->_postCallHuntGroupSessionCreation($callHuntGroupSession);

        // Retrieve hunt group phones
        $huntGroupPhones = $huntGroup->callHuntGroupPhones;

        if(is_array($huntGroupPhones) && count($huntGroupPhones)) {

            // Add agent numbers to the dial element, also add log entries for each
            foreach($huntGroupPhones as $huntGroupPhone) {

                if(!$huntGroupPhone->status_ma) {
                    // ignore if inactive hunt group phone
                    continue;
                }

                switch($this->_telephonyPhone->incoming_caller_id_display) {
                    case TelephonyPhones::CALLER_ID_DISPLAY_THIS_PHONE_NUMBER:
                        $fromCallerId = '1'.$this->_telephonyPhone->phone;
                        break;

                    case TelephonyPhones::CALLER_ID_DISPLAY_CALLER_NUMBER:
                        $fromCallerId = $callHuntGroupSession->from_number;
                        break;

                    default:
                        $fromCallerId = $callHuntGroupSession->from_number;
                        break;
                }

                // Construct call data
                $callData = array(
                    'from'                      =>  $fromCallerId,
                    'caller_name'               =>  self::FROM_NAME,
                    'to'                        =>  (strlen($huntGroupPhone->phone) < 11)? '1'.$huntGroupPhone->phone : $huntGroupPhone->phone,
                    'answer_url'                =>  StmFunctions::getSiteUrl() . '/plivocall/answeragent?To=' . $this->_toPhone.'&From='.$fromPhone,
                    'hangup_url'                =>  StmFunctions::getSiteUrl() . '/plivocall/hangupagent?To=' . $this->_toPhone.'&From='.$fromPhone,
                    'ring_url'                  =>  StmFunctions::getSiteUrl() . '/plivocall/ringagent?To=' . $this->_toPhone,
                    'ring_timeout'              =>  60
                );

                // Make a call to a client, we can make multiple calls at once here if we need to
                $response = $this->_plivo->make_call($callData);

                // Add call hunt group session call record
                $callHuntGroupSessionCall = new CallHuntGroupSessionCalls();
                $callHuntGroupSessionCall->call_hunt_group_session_id = $callHuntGroupSession->id;
                $callHuntGroupSessionCall->dial_a_leg_uuid = $callHuntGroupSession->call_uuid;
                $callHuntGroupSessionCall->dial_b_leg_request_uuid = $response['response']['request_uuid'];
                $callHuntGroupSessionCall->dial_b_leg_to = '1'.$huntGroupPhone->phone;
                $callHuntGroupSessionCall->call_uuid = Yii::app()->request->getPost('CallUUID');
                $callHuntGroupSessionCall->status = 'Missed';
                $callHuntGroupSessionCall->added = new CDbExpression('NOW()');

                // Attempt to save
                if(!$callHuntGroupSessionCall->save()) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call Hunt Group Session call did not save. Error Message:' . print_r($callHuntGroupSessionCall->getErrors(), true) . PHP_EOL.' Attributes: ' . print_r($callHuntGroupSessionCall->attributes, true), CLogger::LEVEL_ERROR);
                }
            }
        }
        elseif(count($huntGroupPhones) == 0) {
            //@todo: notify owner or something
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Hunt Group has no one in incoming call.', CLogger::LEVEL_ERROR);
        }
        else {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Hunt group is missing phone #s for this incoming call.', CLogger::LEVEL_ERROR);
        }

        // Add please hold message
        //$this->_response->addSpeak('Please wait while we connect your call');

        // Bridge the two calls together with a conference call
        $this->_response->addConference(md5(uniqid()), array(
            'beep'                      =>  'false',
            'startConferenceOnEnter'    =>  'false',
            'endConferenceOnExit'       =>  'true',
            'timeLimit'                 =>  $this->_telephonyPhone->ring_timeout,
            'waitSound'                 =>  StmFunctions::getSiteUrl() . '/plivocall/ringSound?To=' . $this->_toPhone,
        ));

        // Get voicemail Greeting
        /** @var CallRecordings $voicemailGreeting */
        $voicemailGreeting = CallRecordings::model()->findByPk($this->_telephonyPhone->voicemail_greeting_id);
        //$voicemailGreeting = $huntGroup->voicemailGreeting;

        // Transfer to voicemail after waiting on hold for 30 seconds
        $this->_response->addRedirect(StmFunctions::getSiteUrl() . '/plivovoicemail/answer?To=' . $this->_toPhone . '&recordType=hunt-group&id=' . $callHuntGroupSession->id . ($voicemailGreeting ? '&greetingId=' . $voicemailGreeting->id : ''));

        // Send the XML response
        $this->_sendResponse();
    }

    /**
     * Action Hold Music
     *
     * Plays hold music while caller is on hold in conference room
     * @return void
     */
    public function actionHoldmusic()
    {
        // Infinitely play hold music
        $this->_response->addPlay(self::HOLD_MUSIC_URL, array(
            'loop'  =>  0
        ));

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Hold Music
     *
     * Plays hold music while caller is on hold in conference room
     * @return void
     */
    public function actionRingSound()
    {
        // Infinitely play hold music
        $this->_response->addPlay(self::CONNECT_RINGING, array(
                'loop'  =>  0
            ));

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Fallback
     *
     * The URL that Plivo will request if the Answer URL fails or returns a non 200 HTTP status code.
     * @return void
     */
    public function actionFallback()
    {
        Yii::log(__CLASS__ . ' (' . __LINE__ . ') Direct Call Fallback. See post data.', CLogger::LEVEL_ERROR);
        $this->_sendResponse();
    }

    /**
     * Action Hangup
     *
     * The URL that Plivo will request when a call to a phone number or endpoint assigned to this application is completed.
     * @return void
     */
    public function actionHangup()
    {
        /** @var CallHuntGroupSessions $callHuntGroupSession */
        $callHuntGroupSession = CallHuntGroupSessions::model()->findByAttributes(array(
            'call_uuid' =>  Yii::app()->request->getPost('CallUUID')
        ));

        // We better have a record
        if(!$callHuntGroupSession) {
            throw new Exception('Error, no call hunt group session found!');
        }

        // Update call data
        $callHuntGroupSession->total_cost = Yii::app()->request->getPost('TotalCost');
        $callHuntGroupSession->bill_duration = Yii::app()->request->getPost('BillDuration');
        $callHuntGroupSession->hangup_cause = Yii::app()->request->getPost('HangupCause');
        $callHuntGroupSession->bill_rate = Yii::app()->request->getPost('BillRate');
        $callHuntGroupSession->answer_time = Yii::app()->request->getPost('AnswerTime');
        $callHuntGroupSession->start_time = Yii::app()->request->getPost('StartTime');
        $callHuntGroupSession->duration = Yii::app()->request->getPost('Duration');
        $callHuntGroupSession->end_time = Yii::app()->request->getPost('EndTime');
        $callHuntGroupSession->call_status = Yii::app()->request->getPost('CallStatus');
        $callHuntGroupSession->event = Yii::app()->request->getPost('Event');

        // Attempt to save call hunt group session record
        if(!$callHuntGroupSession->save()) {
            throw new Exception('Error, unable to save call hunt group session on agent hangup! ' . print_r($callHuntGroupSession->getErrors(), true));
        }

        // Get emails to notify
        $emails = explode(',', str_replace(' ', '', $this->_telephonyPhone->notification_emails));

        // Check duration and hangup type to see if a voicemail was left
        if(Yii::app()->request->getPost('Duration') < $this->_telephonyPhone->ring_timeout && Yii::app()->request->getPost('HangupCause') == 'NORMAL_CLEARING') {
            $this->_sendEmail(Yii::app()->request->getParam('From'), $callHuntGroupSession->call_hunt_group_id, 'hangup', 'STM Call Notifications - Missed Call', $emails);
        }

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Answer Agent
     *
     * URL that is invoked when an agent picks up the phone
     * @return void
     */
    public function actionAnsweragent()
    {
        // Grab the call hunt group session call record
        /** @var CallHuntGroupSessionCalls $callHuntGroupSessionCall */
        $callHuntGroupSessionCall = CallHuntGroupSessionCalls::model()->findByAttributes(array(
            'dial_b_leg_request_uuid'  =>  Yii::app()->request->getPost('ALegRequestUUID')
        ));

        // We need this to continue
        if(!$callHuntGroupSessionCall) {
            $this->_logInfo('test', 'no call hunt group session call found');
            throw new Exception('Error, no call hunt group session call found!');
        }

        // Retrieve call session from call list phone record
        /** @var CallHuntGroupSessions $callHuntGroupSession */
        $callHuntGroupSession = $callHuntGroupSessionCall->callHuntGroupSession;

        // We need this to continue
        if(!$callHuntGroupSession) {
            $this->_logInfo('test', 'no call hunt group session found');
            throw new Exception('Error, no call hunt group session found!');
        }

        // Update call hunt group session call record with more details
        $callHuntGroupSessionCall->status = 'Answered-Unconfirmed';
        $callHuntGroupSessionCall->dial_b_leg_uuid = Yii::app()->request->getPost('ALegUUID');
        if(!$callHuntGroupSessionCall->save()) {
            throw new Exception('Unable to save call hunt group session call record!');
        }

        // If we are already in progress, this agent was too late, play message notifying them
        if($callHuntGroupSession->call_hunt_group_session_call_id) {
            $this->_response->addSpeak('You had a client phone call but were too late to answer. Another agent has taken the call. Thank you, goodbye.');
        }
        else {

            // Confirm that the agent can accept the call
            $getDigits = $this->_response->addGetDigits(array(
                'action'    =>  StmFunctions::getSiteUrl() . '/plivocall/confirmagent?To=' . $this->_toPhone,
                'timeout'   =>  20,
                'numDigits' =>  1
            ));

            // Add a message of instruction for the agent
            $getDigits->addSpeak('You have a incoming call. Press any key to connect immediately.');
        }

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Answer Agent
     *
     * URL that is invoked when an agent picks up the phone
     * @return void
     */
    public function actionConfirmagent()
    {
        // Grab the call hunt group session call record
        /** @var CallHuntGroupSessionCalls $callHuntGroupSessionCall */
        $callHuntGroupSessionCall = CallHuntGroupSessionCalls::model()->findByAttributes(array(
            'dial_b_leg_request_uuid'  =>  Yii::app()->request->getPost('ALegRequestUUID')
        ));

        // We need this to continue
        if(!$callHuntGroupSessionCall) {
            throw new Exception('Error, no call hunt group session call found!');
        }

        // Retrieve call session from call list phone record
        /** @var CallHuntGroupSessions $callHuntGroupSession */
        $callHuntGroupSession = $callHuntGroupSessionCall->callHuntGroupSession;

        // We need this to continue (this can not continue if this record does not exist, there is no reason this record shouldn't already exist at this point, if it doesn't we need to stop)
        if(!$callHuntGroupSession) {
            throw new Exception('Error, no call hunt group session found!');
        }

        // If we are already in progress, this agent was too late, play message notifying them
        if($callHuntGroupSession->call_hunt_group_session_call_id) {

            // Update the status as accepted late
            $callHuntGroupSessionCall->status = 'Accepted Late';
            if(!$callHuntGroupSessionCall->save()) {
                throw new Exception('Unable to save call hunt group session call record!');
            }

            // Send message of failure to the user
            $this->_response->addSpeak('You had a client phone call but were too late to answer. Another agent has taken the call. Thank you, goodbye.');
            $this->_sendResponse();
        }

        // Mark call as accepted
        $callHuntGroupSessionCall->status = 'Accepted';
        if(!$callHuntGroupSessionCall->save()) {
            throw new Exception('Error, unable to save call hunt group session call record!');
        }

        // Mark this call as linked up
        $callHuntGroupSession->call_hunt_group_session_call_id = $callHuntGroupSessionCall->id;
        if(!$callHuntGroupSession->save()) {
            throw new Exception('Error, unable to save call hunt group session record!');
        }

        // Generate a random UUID to identify the conference call
        $conferenceId = md5(uniqid());

        $this->_response->addPlay(self::CONNECT_CHIME_URL, array(
                'loop'  =>  1
            ));
        // record call
        $this->_plivo->record(array(
                'call_uuid' =>  $callHuntGroupSessionCall->call_uuid,
                'time_limit' => 3600, // 1 hour
                'redirect' => false,
                'callback_url' => StmFunctions::getSiteUrl() . '/plivocall/recordcallback?callHuntGroupSessionId='.$callHuntGroupSession->id,
            ));

        // Bridge the two calls together with a conference call
        $this->_response->addConference($conferenceId, array(
            'beep'                      =>  'false',
            'startConferenceOnEnter'    =>  'true',
            'endConferenceOnExit'       =>  'true'
        ));

        // Transfer the client to the conference call URL
        $this->_plivo->transfer_call(array(
            'call_uuid' =>  $callHuntGroupSession->call_uuid,
            'aleg_url'  =>  StmFunctions::getSiteUrl() . '/plivocall/clientconferencejoin?id=' . $conferenceId
        ));

        // see if the phone exists with a contact. Send data if matching contact found. If multiple matches, notify of that too.
        $fromPhone = $callHuntGroupSession->from_number;
        $existingPhones = Phones::model()->findAllByAttributes(array('phone' => $fromPhone));
        $phoneAdditionalInfo = '';
        if(count($existingPhones) == 1) {

            //figure out if it's a phone for a contact
            if($contact = $existingPhones[0]->contact) {
                $phoneAdditionalInfo = PHP_EOL . 'Contact found with this phone number.' . PHP_EOL . 'Name: ' . $contact->fullName . PHP_EOL . 'Email: ' . $contact->getPrimaryEmail();
            }
            elseif($company = $existingPhones[0]->company) {
                $phoneAdditionalInfo = PHP_EOL . 'Contact found with this phone number.' . PHP_EOL . 'Company: ' . $company->name . PHP_EOL;
            }
        }
        elseif(count($existingPhones) > 1) {
            $phoneAdditionalInfo = PHP_EOL . count($existingPhones) . ' counts of this phone number exists.';
        }

        // Send SMS messages if we aren't in debug mode (avoid sending junk SMS)
        if(!YII_DEBUG) {
            Yii::app()->plivo->sendMessage(substr($callHuntGroupSessionCall->dial_b_leg_to, 1), $message = 'Accepted Live Call Info:'.PHP_EOL
                    .'[Source: '.$this->_telephonyPhone->user_description .']'.PHP_EOL
                    .'From: '.Yii::app()->format->formatPhone($fromPhone).$phoneAdditionalInfo
                , self::FROM_NUMBER);

            Yii::app()->plivo->sendMessage('9043433200', $message = 'Accepted Live Call Info (CLee Copy):'.PHP_EOL
                    .'[Source: '.$this->_telephonyPhone->user_description .']'.PHP_EOL
                    .'From: '.Yii::app()->format->formatPhone($fromPhone).$phoneAdditionalInfo.PHP_EOL.' BLegTo: '.$callHuntGroupSessionCall->dial_b_leg_to
                , self::FROM_NUMBER);
        }

        // Send the response
        $this->_sendResponse();
    }

    /**
     * Action Client Conference Join
     *
     * Bridges calls together by means of conference (I THINK this is the only way to do this)
     * @return void
     */
    public function actionClientconferencejoin()
    {
        // Retrieve conference ID
        $conferenceId = Yii::app()->request->getParam('id');
        if(!$conferenceId) {
            throw new Exception('Error, no conference ID found!');
        }

        // Data for conference response
        $conferenceData = array(
            'beep'                      =>  'false',
            'startConferenceOnEnter'    =>  'true',
            'endConferenceOnExit'       =>  'true'
        );

        // Bridge the two calls together with a conference call
        $this->_response->addConference($conferenceId, $conferenceData);

        // Send the XML response
        $this->_sendResponse();
    }

    /**
     * Action Hangup Agent
     *
     * URL that is invoked when an agent hangs up the phone
     * @return void
     */
    public function actionHangupagent()
    {
        /** @var CallHuntGroupSessionCalls $callHuntGroupSessionCall */
        $callHuntGroupSessionCall = CallHuntGroupSessionCalls::model()->findByAttributes(array(
            'dial_b_leg_request_uuid' =>  Yii::app()->request->getPost('RequestUUID')
        ));

        // We better have a record
        //@todo: from what I can see this means a call is missed without anyone in the hunt group picking up. Should this be handled differently?? Maybe look at "Duration", if less than 50-60 sec ignore? Didn't get missed call email... how to differentiate between voicemail left, voicemail answered but message not left and hung up before voicemail picked up.
        if(!$callHuntGroupSessionCall) {
            throw new Exception('Error, no call hunt group session call found!');
        }

        // If we don't have one of these, we have a problem
        $callHuntGroupSessionCall->updated = new CDbExpression('NOW()');

        // Add additional attributes for the connected call
        $callHuntGroupSessionCall->dial_b_leg_from = Yii::app()->request->getPost('From') ? Yii::app()->request->getPost('From') : $callHuntGroupSessionCall->dial_b_leg_from;

        $callHuntGroupSessionCall->dial_b_leg_total_cost = Yii::app()->request->getPost('TotalCost') ? Yii::app()->request->getPost('TotalCost') : $callHuntGroupSessionCall->dial_b_leg_total_cost;
        $callHuntGroupSessionCall->dial_b_leg_bill_rate = Yii::app()->request->getPost('BillRate') ? Yii::app()->request->getPost('BillRate') : $callHuntGroupSessionCall->dial_b_leg_bill_rate;
        $callHuntGroupSessionCall->dial_b_leg_bill_duration = Yii::app()->request->getPost('BillDuration') ? Yii::app()->request->getPost('BillDuration') : $callHuntGroupSessionCall->dial_b_leg_bill_duration;

        $callHuntGroupSessionCall->dial_b_leg_status = Yii::app()->request->getPost('CallStatus') ? Yii::app()->request->getPost('CallStatus') : $callHuntGroupSessionCall->dial_b_leg_status;
        $callHuntGroupSessionCall->dial_b_leg_hangup_cause = Yii::app()->request->getPost('HangupCause') ? Yii::app()->request->getPost('HangupCause') : $callHuntGroupSessionCall->dial_b_leg_hangup_cause;
        $callHuntGroupSessionCall->dial_b_leg_duration = Yii::app()->request->getPost('Duration') ? Yii::app()->request->getPost('Duration') : $callHuntGroupSessionCall->dial_b_leg_duration;

        $callHuntGroupSessionCall->event = Yii::app()->request->getPost('Event') ? Yii::app()->request->getPost('Event') : $callHuntGroupSessionCall->event;

        $callHuntGroupSessionCall->answer_time = Yii::app()->request->getPost('AnswerTime') ? Yii::app()->request->getPost('AnswerTime') : $callHuntGroupSessionCall->answer_time;
        $callHuntGroupSessionCall->start_time = Yii::app()->request->getPost('StartTime') ? Yii::app()->request->getPost('StartTime') : $callHuntGroupSessionCall->start_time;
        $callHuntGroupSessionCall->end_time = Yii::app()->request->getPost('EndTime') ? Yii::app()->request->getPost('EndTime') : $callHuntGroupSessionCall->end_time;

        // Attempt to save
        if(!$callHuntGroupSessionCall->save()) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call Hunt Group Session Call did not save. Error Message:' . print_r($callHuntGroupSessionCall->getErrors(), true) . PHP_EOL.' Attributes: ' . print_r($callHuntGroupSessionCall->attributes, true), CLogger::LEVEL_ERROR);
            throw new Exception('Call Hunt Group Session Call did not save. Error Message:' . print_r($callHuntGroupSessionCall->getErrors(), true));
        }

        // Send an empty response
        $this->_sendResponse();
    }

    /**
     * Action Ring Agent
     *
     * Placeholder for future usages
     * @return void
     */
    public function actionRingagent()
    {
        // Send an empty response
        $this->_sendResponse();
    }


    public function actionRecordaction()
    {
//Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Record action. See post data.', CLogger::LEVEL_ERROR);
        //      [callHuntGroupSessionId] => 1215
        //      [Digits] =>
        //      [Direction] => inbound
        //      [RecordUrl] => https://s3.amazonaws.com/recordings_2013/ff5ceb84-3030-11e5-a16a-842b2b4be7e2.mp3
        //      [From] => 19045681436
        //      [CallerName] => HOLLIS AUSTIN
        //      [RecordingEndMs] => -1
        //      [RecordingID] => ff5ceb84-3030-11e5-a16a-842b2b4be7e2
        //      [RecordFile] => https://s3.amazonaws.com/recordings_2013/ff5ceb84-3030-11e5-a16a-842b2b4be7e2.mp3
        //      [BillRate] => 0.00850
        //      [Event] => Record
        //      [To] => 19046384718
        //      [RecordingDurationMs] => -1
        //      [RecordingDuration] => -1
        //      [CallStatus] => ringing
        //      [CallUUID] => fe2ad636-3030-11e5-99ff-014944537c4b
        //      [RecordingStartMs] => -1

        //this data contains the recording initial data. Recordcallback will be call when recording complete.
//        $session = CallHuntGroupSessions::model()->findByPk(Yii::app()->request->getPost('callHuntGroupSessionId'));
//        if($session) {
//            $session->setAttributes(array(
//                    'recording_id' => Yii::app()->request->getPost('RecordingID'),
//                    'recording_call_uuid' => Yii::app()->request->getPost('CallUUID'),
//                    'recording_url' => Yii::app()->request->getPost('RecordUrl'),
//                ));
//
//            if(!$session->save()) {
//                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Recording not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
//            }
//        }
//        else {
//            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Lead Call Session not found. Recording info not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
//        }

        // Send XML response
        $this->_sendResponse();
    }

    public function actionRecordcallback()
    {
//Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Record Callback action. See post data.', CLogger::LEVEL_ERROR);
        // find the call session by callUuid or something

        //      POST DATA
        //      [callHuntGroupSessionId] => 1215
        //      [Direction] => inbound
        //      [RecordUrl] => https://s3.amazonaws.com/recordings_2013/ff5ceb84-3030-11e5-a16a-842b2b4be7e2.mp3
        //      [From] => 19045681436
        //      [CallerName] => HOLLIS AUSTIN
        //      [RecordingEndMs] => 1437542319400
        //      [RecordingID] => ff5ceb84-3030-11e5-a16a-842b2b4be7e2
        //      [RecordFile] => https://s3.amazonaws.com/recordings_2013/ff5ceb84-3030-11e5-a16a-842b2b4be7e2.mp3
        //      [BillRate] => 0.00850
        //      [Event] => RecordStop
        //      [To] => 19046384718
        //      [RecordingDurationMs] => 33240
        //      [RecordingDuration] => 33
        //      [CallStatus] => ringing
        //      [CallUUID] => fe2ad636-3030-11e5-99ff-014944537c4b
        //      [RecordingStartMs] => 1437542286160

        $response = CJSON::decode(Yii::app()->request->getPost('response'));
        //this data contains the recording initial data. Recordcallback will be call when recording complete.
        $session = CallHuntGroupSessions::model()->findByPk(Yii::app()->request->getPost('callHuntGroupSessionId'));
        if($session) {
            $session->setAttributes(array(
                'recording_id' => $response['recording_id'],
                'recording_call_uuid' => $response['call_uuid'],
                'recording_url' => $response['record_url'],
                'recording_duration' => $response['recording_duration'],
                'recording_duration_ms' => $response['recording_duration_ms'],
                'recording_start_ms' => $response['recording_start_ms'],
                'recording_end_ms' => $response['recording_end_ms'],
                'recording_added' => new CDbExpression('NOW()'),
            ));

            if(!$session->save()) {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Recording not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
            }
        }
        else {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Lead Call Session not found. Recording info not saved for lead connect call. See post data.', CLogger::LEVEL_ERROR);
        }

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Send Email
     *
     * Helper function for sending emails for IVR events
     * @param $phone The incoming phone number
     * @param $huntGroupId The chosen hunt group ID
     * @param $templateName The template name to send via email
     * @param $subject The email subject
     * @param $emails The email addresses we're sending to
     */
    protected function _sendEmail($fromPhone, $huntGroupId, $templateName, $subject, $emails)
    {
        // Skip sending email
        if(!count($emails)) {
            return;
        }

        // Remove leading 1 for lookup, if applicable
        if(strlen($fromPhone) == 11 && (strrpos($fromPhone, '1', -strlen($fromPhone)) !== false)) {
            $fromPhone = substr($fromPhone, 1);
        }

        //@todo: address not found via contact, but through extension description... or later lookup to seller transaction

        // search all Contacts in DB to match potential contacts
        $contacts = null;
        $contacts = null;
        if($contactIds = Yii::app()->db->createCommand("SELECT distinct contact_id FROM phones where phone='".$fromPhone."' AND is_deleted=0 AND contact_id IS NOT NULL")->queryColumn()) {
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $contactIds);
            $contacts = Contacts::model()->findAll($criteria);
        }

        /** @var CallHuntGroups $callHuntGroups */
        $callHuntGroup = CallHuntGroups::model()->findByPk($huntGroupId);

        // Render view partial and return contents
        $html = $this->renderPartial('emails/' . $templateName, array(
            'contacts'      =>  $contacts,
            'address'       =>  $callHuntGroups->name, //$address.. along with price, zip, etc. link to property or seller, etc.
            'fromPhone'     =>  Yii::app()->format->formatPhone($fromPhone),
            'telephonyPhone'=> $this->_telephonyPhone,
            'callHuntGroup' =>  $callHuntGroup,
        ), true);

        // New test email
        $mail = new StmZendMail();
        $mail->setFrom('do-not-reply@seizethemarket.net', 'STM Notification');

        foreach($emails as $email) {
            $mail->addTo($email);
        }

        $mail->setSubject($subject);
        $mail->setBodyHtml($html);

        // Send email
        $mail->send(StmZendMail::getAwsSesTransport());
    }
}