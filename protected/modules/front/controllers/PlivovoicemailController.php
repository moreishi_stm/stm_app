<?php
Yii::import('front_module.controllers.base.BasePlivoController');

/**
 * Plivo Voicemail Controller
 *
 * Used to handle Plivo callbacks
 */
class PlivovoicemailController extends BasePlivoController
{
    const FROM_NUMBER = '19046013911';
    const FROM_NAME = 'STM Live Call';

    /**
     * Hold Music URL
     *
     * @var string URL for location of music to play when agent is on hold
     */
    const HOLD_MUSIC_URL = 'http://cdn.seizethemarket.com/plivo/goldeneye-elevator.mp3';

    /**
     * To Phone
     *
     * @var string The To phone number
     */
    protected $_toPhone;

    /**
     * Telephony Phone
     *
     * Database record representing the telephony phone in use
     * @var TelephonyPhones
     */
    protected $_telephonyPhone;

    /**
     * Client Account
     *
     * Database record representing the client/account in use
     * @var ClientAccounts
     */
    protected $_clientAccount;

    /**
     * Record Type
     *
     * The type of voicemail we are saving
     * @var string
     */
    protected $_recordType;

    /**
     * Greeting
     *
     * The voicemail greeting to play, if any
     * @var CallRecordings
     */
    protected $_greeting;

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @throws Exception When something goes wrong
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        // Make sure we have a to phone number, removes the leading 1
        $this->_toPhone = (strlen(Yii::app()->request->getParam('To')) > 10) ? substr(Yii::app()->request->getParam('To'), 1) : Yii::app()->request->getParam('To');
        if(!$this->_toPhone) {
            throw new Exception('A to phone number must be specified');
        }

        // Get and check record type
        $this->_recordType = Yii::app()->request->getParam('recordType');
        if(!in_array($this->_recordType, array('hunt-group', 'ivr-extension'))) {
            throw new Exception('Error, invalid record type!');
        }

        // Lookup the to phone number to get the client_id, account_id, and database
        $this->_telephonyPhone = TelephonyPhones::model()->findByAttributes(array(
            'phone' =>  $this->_toPhone
        ));

        if(!$this->_telephonyPhone) {
            throw new Exception('Unable to locate to telephony phone record in database, ' . $this->_toPhone);
        }

        // Locate client account record
        $this->_clientAccount = ClientAccounts::model()->findByPk($this->_telephonyPhone->client_account_id);
        if(!$this->_clientAccount) {
            throw new Exception('Unable to locate to client account record in database');
        }

        // Configure database connection for the specified client
        if(!StmFunctions::frontConnectDbByClientIdAccountId($this->_clientAccount->client_id, $this->_clientAccount->account_id)) {
            throw new Exception('Error: Cannot activate db connection for client!');
        }

        // If we specified a greeting, use that
        if(Yii::app()->request->getParam('greetingId')) {
            $this->_greeting = CallRecordings::model()->findByPk(Yii::app()->request->getParam('greetingId'));
        }

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Answer
     *
     * Internal method used to call answer
     */
    public function actionAnswer()
    {
        // Get emails to notify
        $emails = explode(',', str_replace(' ', '', $this->_telephonyPhone->notification_emails));

        // Send notification email
        $this->_sendEmail(Yii::app()->request->getParam('From'), Yii::app()->request->getParam('id'), 'nomessageyet', 'STM Call Notifications - Missed Call', $emails);

        // Check to see if we have a custom greeting, if we do, use that, if we don't, play default
        if($this->_greeting) {
            $this->_response->addPlay($this->_greeting->recording_url);
//            $this->_response->addPlay(\StmAws\S3\FileManagement::getBaseUrlByType(\StmAws\S3\FileManagement::SITE_FILES_BUCKET_TYPE) . '/' . $this->_clientAccount->client_id . '/greeting/' . $this->_greeting->id . '.mp3');
        }
        else {
            $this->_response->addSpeak('Please record your message after the tone. Press any key when you are finished.');
        }

        // Begin recording
        $this->_response->addRecord(array(
            'action'        =>  StmFunctions::getSiteUrl() . '/plivovoicemail/record?recordType=' . $this->_recordType . '&To=' . $this->_toPhone . '&id=' . Yii::app()->request->getParam('id'),
            'callbackUrl'   =>  StmFunctions::getSiteUrl() . '/plivovoicemail/callback?recordType=' . $this->_recordType . '&To=' . $this->_toPhone . '&id=' . Yii::app()->request->getParam('id') . (Yii::app()->request->getParam('greetingId') ? '&greetingId=' . Yii::app()->request->getParam('greetingId') : ''),
            'playBeep'      =>  true,
            'maxLength'       => 600,
        ));

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Record
     *
     * User winds up here when recording has initially completed
     * @return void
     */
    public function actionRecord()
    {
        // Add message of completion
        $this->_response->addSpeak('Thank you for calling, goodbye.');

        // Send response
        $this->_sendResponse();
    }

    /**
     * Action Callback
     *
     * Called when the recording process is complete and the file is ready for use
     * @return void
     */
    public function actionCallback()
    {
        // Create a new voicemail record
        $voiceMailMessage = new VoicemailMessages();

        // Store info about the recording
        $voiceMailMessage->recording_id = Yii::app()->request->getPost('RecordingID');
        $voiceMailMessage->recording_call_uuid = Yii::app()->request->getPost('CallUUID');
        $voiceMailMessage->record_url = Yii::app()->request->getPost('RecordUrl');
        $voiceMailMessage->recording_duration = Yii::app()->request->getPost('RecordingDuration');
        $voiceMailMessage->recording_duration_ms = Yii::app()->request->getPost('RecordingDurationMs');
        $voiceMailMessage->recording_start_ms = Yii::app()->request->getPost('RecordingStartMs');
        $voiceMailMessage->recording_end_ms = Yii::app()->request->getPost('RecordingEndMs');
        $voiceMailMessage->recording_added = new CDbExpression('NOW()');
        $voiceMailMessage->added = new CDbExpression('NOW()');

        // Save changes
        if(!$voiceMailMessage->save()) {
            throw new Exception('Error! Unable to save voicemail message record: "' . print_r($voiceMailMessage->getErrors(), true) . '"');
        }

        // Handle each record type
        switch($this->_recordType) {

            // Hunt Group
            case 'hunt-group':

                // Link the records
                $huntGroupVm = new CallHuntGroupSessionVoicemailMessages();
                $huntGroupVm->call_hunt_group_session_id = Yii::app()->request->getParam('id');
                $huntGroupVm->voicemail_message_id = $voiceMailMessage->id;

                // Make sure we can save
                if(!$huntGroupVm->save()) {
                    throw new Exception("Error, unable to save hunt group voicemail!");
                }
            break;

            // IVR Extension
            case 'ivr-extension':

                $ivrExtensionVm = new IvrSessionVoicemailMessages();
                $ivrExtensionVm->ivr_session_id = Yii::app()->request->getParam('id');
                $ivrExtensionVm->voicemail_message_id = $voiceMailMessage->id;

                // Make sure we can save
                if(!$ivrExtensionVm->save()) {
                    throw new Exception("Error, unable to save ivr extension voicemail!");
                }
            break;

            // No valid type found
            default:
                throw new Exception('Error, unknown type passed in or no type.');
            break;
        }

        // New AWS S3 client
        $s3 = \Aws\S3\S3Client::factory(\StmFunctions::getAwsConfig());

        // Generate temp file name
        $tempFile = '/tmp/plivo-vm-recording-' . md5(uniqid());

        // Download recording from Plivo
        if(!file_put_contents($tempFile, fopen(Yii::app()->request->getPost('RecordUrl'), 'r'))) {
            throw new Exception('Error, unable to download recording from Plivo!');
        }

        // Store the recording in AWS S3
        $s3->putObject(array(
            'Bucket'        =>  'cdn.seizethemarket.com',
            'Key'           =>  'voicemails/' . Yii::app()->user->clientId . '/' . Yii::app()->user->accountId . '/' . $voiceMailMessage->id . '.mp3',
            'SourceFile'    =>  $tempFile,
            'ContentType'   =>  'audio/mpeg'
        ));

        // Delete the recording from the local file system
        if(!unlink($tempFile)) {
            throw new Exception('Error, unable to delete Plivo recording from local storage!');
        }

        // Get emails to notify
        $emails = explode(',', str_replace(' ', '', $this->_telephonyPhone->notification_emails));

        // Send notification email
        $this->_sendEmail(Yii::app()->request->getParam('From'), Yii::app()->request->getParam('id'), 'voicemail', 'STM Notifications - New Voicemail', $emails);

        // Send empty response
        $this->_sendResponse();
    }

    /**
     * Send Email
     *
     * Helper function for sending emails for IVR events
     * @param $phone The incoming phone number
     * @param $huntGroupId The chosen hunt group ID
     * @param $templateName The template name to send via email
     * @param $subject The email subject
     * @param $emails The email addresses we're sending to
     */
    protected function _sendEmail($fromPhone, $huntGroupId, $templateName, $subject, $emails)
    {
        foreach($emails as $i => $email) {
            if(empty($email)) {
                unset($emails[$i]);
            }
        }

        // Skip sending email
        if(!count($emails)) {
            return;
        }

        // Remove leading 1 for lookup, if applicable
        if(strlen($fromPhone) == 11 && (strrpos($fromPhone, '1', -strlen($fromPhone)) !== false)) {
            $fromPhone = substr($fromPhone, 1);
        }

        //@todo: address not found via contact, but through extension description... or later lookup to seller transaction

        // search all Contacts in DB to match potential contacts
        $contacts = null;
        if($contactIds = Yii::app()->db->createCommand("SELECT distinct contact_id FROM phones where phone='".$fromPhone."' AND is_deleted=0 AND contact_id IS NOT NULL")->queryColumn()) {
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $contactIds);
            $contacts = Contacts::model()->findAll($criteria);
        }

        /** @var CallHuntGroups $callHuntGroups */
        $callHuntGroup = CallHuntGroups::model()->findByPk($huntGroupId);

        // Render view partial and return contents
        $html = $this->renderPartial('emails/' . $templateName, array(
            'contacts'      =>  $contacts,
            'address'       =>  $callHuntGroups->name, //$address.. along with price, zip, etc. link to property or seller, etc.
            'fromPhone'     =>  Yii::app()->format->formatPhone($fromPhone),
            'telephonyPhone'=> $this->_telephonyPhone,
            'callHuntGroup' =>  $callHuntGroup,
        ), true);

        // New test email
        $mail = new StmZendMail();
        $mail->setFrom('do-not-reply@seizethemarket.net', 'STM Notification');

        foreach($emails as $email) {
            $mail->addTo($email);
        }

        $mail->setSubject($subject);
        $mail->setBodyHtml($html);

        // Send email
        try {
            $mail->send(StmZendMail::getAwsSesTransport());
        }
        catch(Exception $e) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL
                .'Sender: '.$mail->getFrom().' | Recipients'.print_r($mail->getRecipients(), true).'Email Var: '.print_r($emails, true), CLogger::LEVEL_ERROR);
        }
    }
}
