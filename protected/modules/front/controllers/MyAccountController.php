<?php

class MyAccountController extends FrontController
{
    public $pageColor = 'red';
	public $layout='myAccount';
	public $pageTitle = 'My Home Account';
    public $title; //temporary - to remove after converting all controller->title to controller->pageTitle
	public $userRole = "user"; # (user/admin/??)
	public $parentModel; //Instance of SavedHomeSearches

    public function actions()
    {
    	return array(
			'index'                 =>'stm_app.modules.front.controllers.actions.myAccount.IndexAction',
            'documents'             =>'stm_app.modules.front.controllers.actions.myAccount.DocumentsAction',
            'documentView'          =>'stm_app.modules.front.controllers.actions.myAccount.DocumentViewAction',
            'storyboards'           =>'stm_app.modules.front.controllers.actions.myAccount.StoryboardsAction',
            'storyboardsEdit'       =>'stm_app.modules.front.controllers.actions.myAccount.StoryboardsEditAction',
			'savedHomes'            =>'stm_app.modules.front.controllers.actions.myAccount.SavedHomesAction',
			'marketUpdates'         =>'stm_app.modules.front.controllers.actions.myAccount.MarketUpdatesAction',
			'marketUpdatesResults'  =>'stm_app.modules.front.controllers.actions.myAccount.MarketUpdatesResultsAction',
			'recentlyViewed'        =>'stm_app.modules.front.controllers.actions.myAccount.RecentlyViewedAction',
			'profile'               =>'stm_app.modules.front.controllers.actions.myAccount.ProfileAction',
			'welcome'               =>'stm_app.modules.front.controllers.actions.myAccount.WelcomeAction',
			'contactAgent'          =>'stm_app.modules.front.controllers.actions.myAccount.ContactAgentAction',
			'showingRequest'        =>'stm_app.modules.front.controllers.actions.myAccount.ShowingRequestAction',
			'referFriend'           =>'stm_app.modules.front.controllers.actions.myAccount.ReferFriendAction',
			'apiSavedHomes'         =>'stm_app.modules.front.controllers.actions.myAccount.ApiSavedHomesAction',
			'savedSearches'         =>'stm_app.modules.front.controllers.actions.myAccount.SavedSearchListAction',
			'searchEdit'            =>'stm_app.modules.front.controllers.actions.myAccount.SavedSearchEditAction',
			'searchAdd'             =>'stm_app.modules.front.controllers.actions.myAccount.SavedSearchAddAction',
			'searchPreview'         =>'stm_app.modules.front.controllers.actions.myAccount.SavedSearchPreviewAction',
			'subscriptions'         =>'stm_app.modules.front.controllers.actions.myAccount.EmailSubscriptionPreferencesAction',
            'searchAutocomplete'    =>'stm_app.modules.front.controllers.actions.myAccount.SearchAutocompleteAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
					'index','savedHomes',
					'storyboards','storyboardsEdit','apiSavedHomes','recentlyViewed',
					'profile','contactAgent','showingRequest','referFriend','welcome',
					'marketUpdates','marketUpdatesResults','documents','documentView',
					"savedSearches","searchAdd","searchEdit","searchPreview","subscriptions",'searchAutocomplete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    protected function beforeAction($action)
    {
        if(Yii::app()->user->isGuest) {
            Yii::app()->controller->redirect('/login');
        }
        return parent::beforeAction($action);
    }

	/**
	 * Logs in user and redirect to My Account page.
	 */
	public function actionLogin() {
		$model = new LoginForm;

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {

            	$defaultUrl = Yii::app()->createUrl('front/myAccount/index');
				$this->redirect(Yii::app()->user->getReturnUrl($defaultUrl));
            }
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout(false);
		Yii::app()->user->setFlash('success', 'You have successfully Logged Out.');
		$this->redirect('front/site/index');
	}

	/**
	 * Handle errors thrown from the Front module
	 * @return null
	 */
	public function actionError()
	{
		if ($error=Yii::app()->errorHandler->error)
			$this->render('error', $error);
	}

    public function printActivityLog() {

        $tuples = array();
        // contacts tuples
        $contactTuples = array('componentTypeId'=>ComponentTypes::CONTACTS, 'componentId'=>Yii::app()->user->id);
        array_push($tuples, $contactTuples);

        // transaction tuples
        if($Transactions = Transactions::model()->byContactId(Yii::app()->user->id)->findAll()) {
            foreach($Transactions as $Transaction) {
                array_push($tuples, array('componentTypeId'=> $Transaction->component_type_id, 'componentId'=>$Transaction->id));
            }
        }

        // closings tuples
        if($Closings = Closings::model()->byContactId(Yii::app()->user->id)->findAll()) {
            foreach($Closings as $Closing) {
                array_push($tuples, array('componentTypeId'=> ComponentTypes::CLOSINGS, 'componentId'=>$Closing->id));
            }
        }


        $DataProvider = new CActiveDataProvider(ActivityLog::model()->byComponentTypes($tuples)->byIsPublic(),
                            array(
                                 'criteria' => array(
                                     'order' => 'activity_date desc',
                                 ),
                                 'pagination'=>array(
                                     'pageSize'=>5,
                                 ),
                             ));

        $this->renderPartial('_activityLog', array(
                                             'DataProvider' => $DataProvider,
                                             ));
    }
}