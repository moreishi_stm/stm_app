<?php

Yii::import('front_module.controllers.base.BaseHuntGroupController');

/**
 * Plivo Call Controller
 *
 * Used to handle Plivo callbacks
 */
class PlivoivrhuntgroupController extends BaseHuntGroupController
{
    /**
     * Get Hunt Group
     *
     * Returns the appropriate call hunt group to use
     * @return CallHuntGroups|null
     */
    protected function _getHuntGroup()
    {
        return CallHuntGroups::model()->findByPk(Yii::app()->request->getParam('huntGroupId'));
    }

    /**
     * Post Call Hunt Group Session Creation
     *
     * Called after the hunt group session record is created
     * @param CallHuntGroupSessions $callHuntGroupSession
     * @throws Exception When unable to save IVR Session
     */
    protected function _postCallHuntGroupSessionCreation(CallHuntGroupSessions $callHuntGroupSession)
    {
        // Locate IVR session
        /** @var IvrSessions $ivrSession */
        $ivrSession = IvrSessions::model()->findByAttributes(array(
            'call_uuid'  =>  Yii::app()->request->getPost('CallUUID')
        ));

        // Link IVR session to hunt group session and mark as successfully transferred
        $ivrSession->call_hunt_group_session_id = $callHuntGroupSession->id;
        $ivrSession->status = IvrSessions::STATUS_TRANSFERRED_TO_HUNT_GROUP;
        if(!$ivrSession->save()) {
            throw new Exception('Error, unable to save IVR session!');
        }
    }
}