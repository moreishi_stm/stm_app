<?php

class LandingPagesController extends FrontController
{
	public $pageTitle = '';
	public $layout = 'l';

    public function actions()
    {
    	return array(
            'index'          =>'stm_app.modules.front.controllers.actions.landingPages.IndexAction',
    	);
    }

	/**
	 * @return array action filters
	 */
    public function filters()
    {
        return CMap::mergeArray(
            parent::filters(), array()
        );
    }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('webuyhomesnowjax','moveonce','homesoldguarantee','tripleguarantee','freesellerreport'),
//				'users'=>array('*'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
}