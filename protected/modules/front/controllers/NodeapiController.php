<?php
// Set an exception handler to read the exception over the phone lolol
if(YII_DEBUG) {

    // Exception handler function
    function handleCustomExceptions(Exception $exception) {

        // Log contents to a file
        file_put_contents('/var/log/stm/dialer3/exceptions-' . date('Y-m-d_h-i-s') . '_' . microtime(true), $exception->getMessage() . "\n" . print_r($exception->getTraceAsString(), true) . "\n\nPOST Variables:\n" . print_r($_POST, true) . "\n\nGET Variables:\n" . print_r($_GET, true));

        // Clean the output buffer
        ob_clean();

        // Set output to XML
        header('Content-type: application/json');

        // Echo out the exception in JSON format so we can easily read it
        echo(json_encode(array(
            'message'       => $exception->getMessage(),
            'stack_trace'   =>  $exception->getTraceAsString(),
            'post_vars'     =>  $_POST,
            'get_vars'      =>  $_GET
        )));

        // Terminate execution
        Yii::app()->end();
    }

    // Set the exception handler
    set_exception_handler('handleCustomExceptions');
}

// Import stuff we want to utilize
Yii::import('admin_module.components.StmDialer.Lists.Factory', true);
Yii::import('admin_module.components.Dialer.LoadAbstractTrait', true);

/**
 * Node API Controller
 *
 * Used to handle Plivo callbacks
 */
class NodeapiController extends FrontController
{
    /**
     * Load up trait
     */
    use LoadAbstractTrait;

    /**
     * API Key
     *
     * Shared key between Node and this controller
     */
    const API_KEY = 'o0g[H0=pJn^x(c77g55"77tlf?Z(ZW';

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        // Check API key
        if(Yii::app()->request->getParam('api_key') != self::API_KEY) {

            // Set HTTP Response code to 403 (Access denied for supplied credentials), give reason for error, terminate application
            http_response_code(403);
            echo "Error 403 - Access Denied";
            Yii::app()->end();
        }

//        // Make sure we have an account id and client id
//        if(!Yii::app()->request->getPost('account_id') || !Yii::app()->request->getPost('client_id')) {
//            throw new Exception('Account ID and Client ID are both required');
//        }

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Load Action
     *
     * Returns JSON formatted data for DataTable
     * @return void
     */
    public function actionLoad()
    {
        // Get and check ID, which is the type_ma
        $id = Yii::app()->request->getParam('id');
        $callListId = Yii::app()->request->getParam('callListId'); //@todo: refactor and test to turn into $id, this is temp implementation
        $presetId = Yii::app()->request->getParam('presetId');
        $userId = Yii::app()->request->getParam('userId');
        $callSessionId  = Yii::app()->request->getParam('callsession');
        $filters['maxDialerCallCount'] = Yii::app()->request->getParam('maxDialerCallCount');
        $filters['maxDialerCallDays'] = Yii::app()->request->getParam('maxDialerCallDays');

        if($callListId) {
            $callList = \CallLists::model()->findByPk($callListId);
            $presetId = $callList->preset_ma;
        }

        if(!$presetId) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Preset ID is required.');
        }

        if(!$callListId) {
            $callList = \CallLists::getCallListByPresetId($presetId, $filters);
            $callListId = $callList->id;
        }

//        if(!$id && !$presetId) {
//            throw new Exception(__CLASS__.'(:'.__LINE__.') Dialer load missing Call List ID and Preset ID. At least one is required.');
//        }

        if($id && $id !='undefined' && !is_numeric($id)) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Invalid Call List ID.');
        }

        if($presetId && !is_numeric($presetId)) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Invalid Preset ID.');
        }

        // Use new adapter pattern to get a list instance
        $o = \StmDialer\Lists\Factory::getInstance($presetId, $callListId);
        $id = $o->getCallList()->id;

        // Retrieve active call sessions
        $inProgresses = array();
        $activeSessionUserIds = array();
        $activeSessions = CallSessions::model()->getActiveSessionsByCallListId($id);
        foreach($activeSessions as $activeSession) {

            if(empty($activeSession->end)) {
                $activeSessionUserIds[$activeSession->contact_id] = $activeSession->id;
            }
        }

        if($callSessionId && $callSessionId !== 'null') {
            if($callSession = CallSessions::model()->findByPk($callSessionId)) {

                if(isset($activeSessionUserIds[$callSession->contact_id]) && $activeSessionUserIds[$callSession->contact_id] != $callSessionId) {
                    //@todo: error - call session does not match!
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') **** URGENT Dialer Error: Passed in session does not match active session! Investigate immediately. ****'.PHP_EOL.'Call Session Data: '.print_r($callSession->attributes, true).PHP_EOL.'Active Call Sessions: '.print_r($activeSessionUserIds, true), CLogger::LEVEL_ERROR);
                }
                elseif($callSession->end) {
                    //@todo: session is not active!!!
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') **** URGENT Dialer Error: Passed in session is not active! Investigate immediately. ****'.PHP_EOL.'Call Session Data: '.print_r($callSession->attributes, true).PHP_EOL.'Active Call Sessions: '.print_r($activeSessionUserIds, true), CLogger::LEVEL_ERROR);
                }
            }
        }

        // Get phone numbers to call
        $callListPhones = $o->getQueueBySortOrder($id, $userId, $filters);

        // Get in progress call, if we have one
        $active = array();
        foreach($callListPhones as $callListPhone) {

            // We only care about currently in progress calls
            if($callListPhone['status'] != 'Answered') {
                continue;
            }

            // if "Answered call does not have a active session, set the call as "No Answer" & email error log it.
            if(!isset($activeSessionUserIds[$callListPhone['updated_by']])) {
                // get data for call_list_phone before updating it to "No Answer"
                $callListPhoneMissingSession = Yii::app()->db->createCommand("select * from  call_list_phones WHERE id={$callListPhone['call_list_phone_id']}")->queryRow();
                Yii::app()->db->createCommand("UPDATE call_list_phones SET `status`='".CallListPhones::NO_ANSWER."' WHERE id={$callListPhone['call_list_phone_id']}")->execute();

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') **** URGENT Error****'.PHP_EOL.'A call_list_phone record is marked as Answered without a call session ID. Marking this call as No Answer. Please investigate. Call List Phone ID: '.$callListPhone['call_list_phone_id'].' | Call List ID#: '.$id.' | Phone#: '.$callListPhone['phone'].' | Call List Phone Record: '.print_r($callListPhoneMissingSession, true).' | Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);

                //@todo: Figure out if we can leave this here
                throw new Exception('Error! A call_list_phone record is marked as Answered without a call session ID.');

                continue;
            }

            $callId = Calls::getIdByAnsweredCallListPhoneId($callListPhone['call_list_phone_id'], $activeSessionUserIds[$callListPhone['updated_by']]);

            if(!$callId) { // && !YII_DEBUG
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find CallID.  Please investigate. '.PHP_EOL
                    .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                    .'Session ID: '.$callSessionId.PHP_EOL
                    .'Call List ID#: '.$id.PHP_EOL
                    .'Phone#: '.$callListPhone['phone'].PHP_EOL
                    .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);

                //@todo: Figure out if we can leave this here
                throw new Exception('Error! Answered status did not find callID!');

                continue;
            }

            // Get phone record
            $phone = Phones::model()->skipSoftDeleteCheck()->findByPk($callListPhone['phone_id']);

            if(!$phone) { // && !YII_DEBUG
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find Phone model.  Please investigate. '.PHP_EOL
                    .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                    .'Session ID: '.$callSessionId.PHP_EOL
                    .'Call List ID#: '.$id.PHP_EOL
                    .'Phone#: '.$callListPhone['phone'].PHP_EOL
                    .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);

                //@todo: Figure out if we can leave this here
                throw new Exception('Error! Answered status did not find phone model!');

                continue;
            }

            // Get the associated contact
            $contact = $phone->contact;

            $command = Yii::app()->db->createCommand()
                ->select('a.id')
                ->from('activity_log a')
                ->where('a.call_id='.$callId)
                ->order('a.id DESC');

            $answeredActivityLogIds = $command->queryAll();

            // get the activity log that is marked as Answered for this call to pass to GUI as it may be a voicemail
            // @todo: one can not be found between the lag of being answered and marking as voicemail??? - keep an eye on this, watch error log emails
            if(!$answeredActivityLogIds) {

                // run the same query without the task_type_id... @todo: temporary check
                $secondQuery = Yii::app()->db->createCommand()
                    ->select('a.*')
                    ->from('activity_log a')
                    ->leftJoin('calls c', 'a.call_id=c.id')
                    ->leftJoin('call_list_phones c2', 'c.call_list_phone_id=c2.id')
                    ->andWhere('c2.id='.$callListPhone['call_list_phone_id'])
                    ->andWhere('DATE(a.added)="'.date('Y-m-d').'"')
                    ->andWhere('a.added_by='.Yii::app()->user->id)
                    ->andWhere('c.call_session_id='.$callSessionId)
                    ->order('a.id DESC')
                    ->queryAll();

                // threw error cuz no call data
                if($callId) {
                    // third query using just call_id
                    $thirdQuery = Yii::app()->db->createCommand()
                        ->select('a.*')
                        ->from('activity_log a')
                        ->where('a.call_id='.$callId)
                        ->order('a.id DESC')
                        ->queryAll();
                }

                if(count($secondQuery)) {
                    $secondaryNote = PHP_EOL.'NOTE: running the Second Query without the task_type_id as "Answer" provided '.count($secondQuery).' result(s). Result Data: '.print_r($secondQuery->attributes, true).PHP_EOL.'Call ID#: '.$callId;

                    //assign value to variable for further processing, if data found by the second query
                    $answeredActivityLogId = $secondQuery[0];
                }
                if(count($thirdQuery)) {
                    $thirdNote = PHP_EOL.'NOTE: running the Third Query with only call_id provided '.count($thirdQuery).' result(s). Result Data: '.print_r($thirdQuery->attributes, true).PHP_EOL.'Call ID#: '.$callId;
                    if(!$answeredActivityLogId){
                        $thirdNote .= PHP_EOL.'Second query also had no results but third query did. Using activity_log from third query.';
                        $answeredActivityLogId = $thirdQuery[0];
                    }
                }

                // log error, no activity log id was found
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: No activity log record was found for an answered call. Call ID#: '.$callId.' Call List ID#: '.$id.' Call List Phone ID#: '.$callListPhone['call_list_phone_id'].' Query: '.$command->getText().$secondaryNote.$thirdNote, CLogger::LEVEL_ERROR);

                //@todo: this would indicated call_list_phone with status of Answered BUT may be "stuck" and really hung up. Possibly need to hang up and send error log note FYI untill fully fixed?
                //@todo: this may also be caused by lag between answer and updating activity log. Can check this by last_updated and now being more than 5 seconds apart?
                // check to see if it is linked to current call session, if not it's a leftover "Answered" bug caused by a different session
            }
            else {
                $answeredActivityLogId = $answeredActivityLogIds[0];

                // there should only be one result, but
                if(count($answeredActivityLogIds)>1) {

                    $activityLogsAttributesToReport = array();
                    foreach($answeredActivityLogIds as $answeredActivityLogIdToReport) {
                        $activityLogsAttributesToReport[] = $answeredActivityLogIdToReport->attributes;
                    }

                    // error in finding more than 1 activity_log entry. Check query logic to narrow down more effectively
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: More than activity log was found. Query needs to be narrowed down more or a call status of Answered is left in that status unnecessarily. Please investigate immediately. Call ID#: '.$callId.' Call List ID#: '.$id.PHP_EOL.'All Activity Log Attributes: '.print_r($activityLogsAttributesToReport, true).' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
                }
            }

            $componentType = ComponentTypes::model()->findByPk($callListPhone['component_type_id']);

            $callListIds = Yii::app()->db->createCommand()
                ->select('clp.call_list_id id, l.name name, CONCAT("'.$phone->id.'") as phone_id, '.$callListPhone['component_type_id'].' component_type_id, '.$callListPhone['component_id'].' component_id')
                ->from('call_list_phones clp')
                ->join('call_lists l', 'l.id=clp.call_list_id')
                ->where('clp.phone_id='.$phone->id)
                ->andWhere('clp.is_deleted=0')
                ->group('clp.call_list_id')
                ->queryAll();

            // Mode data for active content
            $active[$callListPhone['updated_by']] = array(
                'call_id'           =>  $callId,
                'call_list_ids'       => $callListIds,
                'call_list_phone_id'=>  $callListPhone['call_list_phone_id'],
//                    'source'            =>  $callListPhone['source'], //@todo: test on task based first to make sure the blank doesn't error out
                'activity_log_id'   =>  $answeredActivityLogId['id'],
                'phone_id'          =>  $phone->id,
                'first_name'        =>  $contact->first_name,
                'last_name'         =>  $contact->last_name,
                'spouse_first_name' =>  ($contact->spouse_first_name)? ' & '.$contact->spouse_first_name : '',
                'spouse_last_name'  =>  $contact->spouse_last_name,
                'address'           =>  $contact->primaryAddress->address,
                'city'              =>  $contact->primaryAddress->city,
                'state'             =>  AddressStates::getShortNameById($contact->primaryAddress->state_id),
                'zipcode'           =>  $contact->primaryAddress->zip,
                'phone'             =>  $phone->phone,
                'task_id'           =>  $callListPhone['task_id'],
                'description'       =>  $callListPhone['description'],
                'component_id'      =>  $callListPhone['component_id'],
                'component_type_id' =>  $callListPhone['component_type_id'],
                'component_name'    =>  $componentType->name,
                'component_label'   =>  $componentType->getSingularName(),
            );
        }

        // Retrieve a list of phone numbers to call for the current session
        $this->_sendDialerJson(array(
            'status'    =>  'success',
            'results'   =>  $callListPhones,
            'meta'	=>	array(
                //'in_progress'               =>  $inProgresses, //@todo: consolidated into session_id - delete after confirm bug-free - CLee 12/6/2015
                'session_id'                =>  (empty($activeSessionUserIds)) ? null : $activeSessionUserIds,
                'active'                    =>  (empty($active)) ? null : $active,
            )
        ));
    }

    /**
     * Send Dialer JSON
     *
     * Outputs JSON data and terminates application
     * @param array $data Array of data
     */
    protected function _sendDialerJson($data)
    {
        // Set header and output JSON data
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }
}