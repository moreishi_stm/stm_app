<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the General submits that do not require any special modifications to ajax performRequest
 *
 */
class SubmitAction extends AbstractFormSubmitAction {

	protected function performRequest($formId)
    {

	}
}
