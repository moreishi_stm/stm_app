<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Video' form.
 * This is originally designed for referring to other out of town agents.
 */
class ReferralAction extends AbstractFormSubmitAction {

    public function run($formId) {

        // Sanity check the post before processing
        if (!$this->formSubmissionIsValid()) {
            return false;
        }

        $this->sendSubmissionValuesToCLee($formId);
        $rawValuesRecordId = $this->saveRawValues();
        $this->spamCheck($rawValuesRecordId);

        $errors = $this->performAjaxRequest($formId);
        $this->hasError = CJSON::decode($errors);

        if (!empty($this->hasError)) {
            if(Yii::app()->request->isAjaxRequest) {
                echo $errors;
                Yii::app()->end();
            }
            else {
                $this->failed = true;
            }
        } else {
            if(!$this->recordFormSubmission($formId)) {
                echo CJSON::encode($this->hasError);
                Yii::app()->end();
            }
        }
        $this->performRequest($formId);

        Yii::app()->end();
    }

	protected function performRequest($formId) {
        $formSubmitData = $_POST['FormSubmissionValues']['data'];

        //hard code felicia & christine for now.
        $message = new YiiMailMessage;
        $logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Team Logo" style="width:200px;">';
        $content =
            '<div style="font-weight:bold;font-size:25px;">'
            .'New Recruit Lead!<br /><br />'
            .'<span style="color: #D20000;">'. $formSubmitData[FormFields::FIRST_NAME_ID].' '.$formSubmitData[FormFields::LAST_NAME_ID] . '<br />'
            .$formSubmitData[FormFields::EMAIL_ID].', '.$formSubmitData[FormFields::PHONE_ID].'</span><br /><br />'
            .$logoImage
            .'</div>';
        $message->view = 'plain';
        $message->setBody(array(
                'content' => $content
            ), 'text/html'
        );
        $message->setSubject('New Referral Lead - '.$formSubmitData[FormFields::FIRST_NAME_ID].' '.$formSubmitData[FormFields::LAST_NAME_ID]);

        //@todo: $toEmail pull from lead route
        $leadRoute = LeadRoutes::model()->findByPk(3); //@todo: hard code for now. Need to update tables so this has component type id 1 and correct context_ma
        $contactsToNotify = $leadRoute->getActingAgents();

        if (YII_DEBUG) {
            $message->addTo(StmMailMessage::DEBUG_EMAIL_ADDRESS, 'New Referral Lead');
        } else {
            foreach($contactsToNotify as $contact) {
                $message->addTo($contact->getPrimaryEmail(), $contact->fullName);
            }
            //CC CLee on all leads for now.
            $message->addTo('christine@christineleeteam.com', 'Christine Lee');
        }

        $message->addFrom('Do-Not-Reply@SeizetheMarket.com', 'Lead Alert');
        Yii::app()->mail->send($message);
	}

    protected function recordFormSubmission($formId) {
        if($this->failed)
            return false;

        $googleTrackingData = StmFormHelper::getGoogleTrackingData();

        // Create the instance of the form submission
        $FormSubmission = new FormSubmissions;
        $FormSubmission->form_id = $formId;
        $FormSubmission->url = Yii::app()->request->urlReferrer;
        $FormSubmission->domain = str_replace('www.','',$_SERVER['SERVER_NAME']);
        $FormSubmission->campaign = $googleTrackingData['campaign_name'];
        $FormSubmission->source = $googleTrackingData['campaign_source'];
        $FormSubmission->keywords = $googleTrackingData['campaign_term'];
        $FormSubmission->medium = $googleTrackingData['campaign_medium'];

        if ($FormSubmission->save()) {

            $form = Forms::model()->findByPk($formId);

            $email = $_POST['FormSubmissionValues']['data'][FormFields::EMAIL_ID];
            $isResubmit = (boolean) Emails::model()->countByAttributes(array('email'=>$email));

            // Get the contact instance from the form submission, as required for a lead.
            $contact = $FormSubmission->retrieveContactInstance();

            if($contact) {
                // log resubmit
                $formSubmitData = $_POST['FormSubmissionValues']['data'];
                $activityLog = new ActivityLog;
                $activityLog->account_id = Yii::app()->user->accountId;
                $activityLog->task_type_id = ($isResubmit)? TaskTypes::RESUBMIT : TaskTypes::NEW_LEAD_SUBMIT;
                $activityLog->component_type_id = ComponentTypes::CONTACTS;
                $activityLog->component_id = $contact->id;
                $activityLog->activity_date = new CDbExpression('NOW()');
                $resubmitLabel = ($isResubmit)? '(Re-submit) ' : '';
                $activityLog->note = 'Agent Referral Request ' . $resubmitLabel . ': '.$formSubmitData[FormFields::FIRST_NAME_ID].' '.$formSubmitData[FormFields::LAST_NAME_ID].', '.$formSubmitData[FormFields::EMAIL_ID].', '.$formSubmitData[FormFields::PHONE_ID];
                $activityLog->save();

                // if status is inactive set to active
                if($contact->contact_status_ma==StmFormHelper::INACTIVE) {
                    $contact->contact_status_ma = StmFormHelper::ACTIVE;
                    if(!$contact->save()) {
                        Yii::log(__FILE__.'('.__LINE__.'): Could not reactivate contact on resubmit. Error: '.print_r($contact->getErrors(),true).PHP_EOL.' Contact Model: '.print_r($contact->attributes, true), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                    }
                }
            }
            if(!$isResubmit) {
                $contact->contact_status_ma = 1;
                $contact->source_id = 1;
            }

            foreach ($_POST['FormSubmissionValues']['data'] as $formFieldId => $fieldValue) {
                if ($fieldValue) {
                    $FormSubmissionValue = new FormSubmissionValues;
                    $FormSubmissionValue->form_submission_id = $FormSubmission->id;
                    $FormSubmissionValue->form_field_id      = $formFieldId;
                    $FormSubmissionValue->value = $fieldValue;
                    if(!$FormSubmissionValue->save()) {
                        $values = print_r($FormSubmissionValue->attributes, true);
                        $errors = print_r($FormSubmissionValue->getErrors(), true);
                        Yii::log(__FILE__.'('.__LINE__.'): Form Submission Value did not save properly.'.$errors.PHP_EOL.' Submission Value: '.$values, CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                    }
                    $FormSubmission->formValueTuples[$formFieldId] = $fieldValue;
                }
            }

            if(!($contactSaveSuccess = $contact->save()) && !$isResubmit) {
                $errors = print_r($contact->getErrors(), true);
                Yii::log(__FILE__.'('.__LINE__.'): New Contact did not save properly.'.$errors.PHP_EOL.' Contact values: '.print_r($contact->attributes, true), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            }

            /**
             * Auto login the user if not a resubmit and the web lead was created successfully
             */
            if (!$contact->getIsNewRecord() && !$isResubmit) {
                $loginForm = new LoginForm();
                $loginForm->email = $contact->getPrimaryEmailObj()->email;
                $loginForm->password = $contact->password;
                if (!($loginForm->validate() && $loginForm->login())) {
                    Yii::log('Could not auto-login user created by web lead.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                } else {
                    PageViewTrackerWidget::processLogin();
                    RecentSavedHomesWidget::processLogin();
                    LoginLog::processLogin();
                }
            }
            Contacts::updateLastLogin($contact); //last login should update for new and resubmits
        }

        $this->formSubmission = $FormSubmission;
        return true;
    }
}