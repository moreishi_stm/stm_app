<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Send to A Friend' form.
 *
 */
class SendToFriendAction extends AbstractFormSubmitAction {

	protected function performRequest($formId) {
		//Additional Actions to take place here
	}
}
