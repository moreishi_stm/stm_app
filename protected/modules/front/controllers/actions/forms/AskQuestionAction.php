<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Ask A Question' form.
 *
 */
class AskQuestionAction extends AbstractFormSubmitAction {

	protected function performRequest($formId) {
		//Additional Actions to take place here
	}
}
