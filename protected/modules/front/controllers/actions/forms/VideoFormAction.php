<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Video' form.
 *
 */
class VideoFormAction extends AbstractFormSubmitAction {

    protected function performRequest($formId) {}
}
