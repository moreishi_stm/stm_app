<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Request A Showing' form.
 *
 */
class ShowingAction extends AbstractFormSubmitAction {

	protected function performRequest($formId) {
		//Additional Actions to take place here
	}
}
