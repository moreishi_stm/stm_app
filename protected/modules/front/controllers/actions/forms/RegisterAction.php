<?php
include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Request A Showing' form.
 *
 */
class RegisterAction extends AbstractFormSubmitAction {

	protected function performRequest($formId) {
		header('Content-type: application/json');
		echo CJSON::encode(array(
			'email' => $this->_webAdapter->getContact()->getPrimaryEmail(),
			'resubmit' => $this->_webAdapter->getIsResubmit(),
		));
		return;
	}
}