<?php
Yii::import('front_widgets.PageViewTrackerWidget.PageViewTrackerWidget');
Yii::import('front_widgets.RecentSavedHomesWidget.RecentSavedHomesWidget');
Yii::import('admin_module.components.StmLeads.Adapters.Web', true);

/**
 * The <code>AbstractFormSubmitAction<code> Houses common
 * form submission logic.
 *
 */
abstract class AbstractFormSubmitAction extends CAction {

	// Defines the category name used when processing Yii logs
	const LOG_CATEGORY = 'abstractform';
	const LOG_CATEGORY_SPAM = 'spam';

    public $failed = false;
	public $hasError;
    public $autoLogin = true;

    protected $_webAdapter;

	/** @var $_isResubmit boolean determines if the web lead that is created is a resubmission */
    protected $_isResubmit;

    public function run($formId) {

	    // Sanity check the post before processing
	    if (!(isset($_POST['FormSubmissionValues']['data']) && !empty($_POST['FormSubmissionValues']['data']))) {
			return false;
	    }

        // New web adapter
        $this->_webAdapter = new \StmLeads\Adapters\Web($formId);
        $this->_webAdapter->importData($_POST['FormSubmissionValues']['data']);
        $this->_webAdapter->sendToCLee(); //leave here until CLee no longer wants copies of everything.

        // Perform spam check
        if($this->_webAdapter->isSpam()) {
			header('Content-type: application/json');
            echo CJSON::encode(array('isSpam' => true));
            Yii::app()->end();
        }

        if(Yii::app()->request->isAjaxRequest) {
            $this->beforeValidateWebForm($formId);
        }

        // Validate data
        if(!$this->_webAdapter->validate()) {

            // If we had an AJAX request, return the AJAX response to the view
            if(Yii::app()->request->isAjaxRequest) {
                $validationResult = $this->_webAdapter->validateWebForm();
                $this->afterValidateWebForm($formId, $validationResult);
            }
            else {
                $this->failed = true;
                return;
            }
        }
        else {
            if(Yii::app()->request->isAjaxRequest) {
                $validationResult = $this->_webAdapter->validateWebForm();
				header('Content-type: application/json');
                echo $this->afterValidateWebForm($formId, $validationResult);
            }
        }

        // Attempt to save data and the auto login if lead is not a resubmit and was created successfully
        if($this->_webAdapter->save() && !$this->_webAdapter->getIsResubmit() && !$this->_webAdapter->isNewTransaction() && $this->autoLogin === true) {
            $loginForm = new LoginForm();
            $loginForm->email = $this->_webAdapter->getContact()->getPrimaryEmail();
            $loginForm->password = $this->_webAdapter->getContact()->password;
            if (!($loginForm->validate() && $loginForm->login())) {
                Yii::log(__CLASS__.' (:'.__LINE__.') Could not auto-login user created by web lead. Error: '.print_r($loginForm->getErrors(), true), CLogger::LEVEL_ERROR);
            } else {

                PageViewTrackerWidget::processLogin();
                RecentSavedHomesWidget::processLogin();
                LoginLog::processLogin();
            }
        }

        if($this->autoLogin) {
            //last login should update for new and resubmits
            Contacts::updateLastLogin($this->_webAdapter->getContact());
        }

        $this->performRequest($formId);
        // Terminate app
        Yii::app()->end();
    }

    /**
     * Perform Request
     *
     * Abstract method which allows each individual form to handle its own Action Specific workflow leaving all common functionality encapsulated in this abstract action.
     * @returns void
     */
    protected abstract function performRequest($formId);

    protected function beforeValidateWebForm($formId) {}

    /**
     * After Validate Web Form - this allows custom result handling to take place after validation (Ex. Seller or House Values Leads)
     * @param $formId
     * @param $validationResult
     *
     * @return mixed
     */
    protected function afterValidateWebForm($formId, $validationResult) {
        if($validationResult !== '[]') {
			header('Content-type: application/json');
            echo $validationResult;
            \Yii::app()->end();
        }
    }
}
