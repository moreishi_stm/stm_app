<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Video' form.
 *
 */
class HouseValuesAction extends AbstractFormSubmitAction
{
    protected function beforeValidateWebForm($formId)
    {
        if(isset($_POST['FormSubmissionValues']['formPart'])) {

            $this->_webAdapter->formScenario = $formId.'_Part'. $_POST['FormSubmissionValues']['formPart'];
        }
    }

    protected function afterValidateWebForm($formId, $validationResult)
    {
        if($validationResult !== '[]') {
            echo $validationResult;
            \Yii::app()->end();
        }

        $formPart = isset($_POST['FormSubmissionValues']['formPart']) ? $_POST['FormSubmissionValues']['formPart'] : null;

        //echo out something to move to Part 2 & 3 so it doesn't do full submit yet
        if(($formPart == 1) || ($formPart == '1_details')) {

            // emails the address as an address only lead, in case the prospect doesn't complete the form. @todo: This can be a setting later to turn on/off based on customer preference.
            if(!empty($this->_webAdapter->data()['address']) && !empty($this->_webAdapter->data()['city']) && !empty($this->_webAdapter->data()['zip'])) {

                // Old email procedure @todo: Determine if we still need this!?
                $this->_emailAddressOnlyLeadAlert();

                // Throw this through the lead route machine
                $this->_webAdapter->submissionStep = 'Partial';
                $this->_webAdapter->saveFormSubmissionData();

                // Report success for this part
                echo \CJSON::encode(array(
                    'status'                =>  'successPart'.$formPart,
                    'form_submission_id'    =>  $this->_webAdapter->getFormSubmissionId()
                ));
            }

            \Yii::app()->end();
        }
    }

	protected function performRequest($formId)
    {
        // cleared all validation, send market snapshot email if settings enabled
        if(Yii::app()->user->settings->market_snapshot === '1') {

            if(!($agentContactId = Yii::app()->db->createCommand("SELECT value FROM setting_account_values WHERE setting_id=".Settings::SETTING_MARKET_SNAPSHOT_CONTACT_ID)->queryScalar())) {
                //@todo: notify agents/owners log error
                Yii::log(__CLASS__.' (:'.__LINE__.') Market Snapshot does not have an assigned Contact ID in settings.', CLogger::LEVEL_ERROR);
                return;
            }
            $agentContact = Contacts::model()->findByPk($agentContactId);

            if(in_array($formId, array(Forms::FORM_HOUSE_VALUES,  Forms::FORM_HOUSE_VALUES_SIMPLE))) {

                StmFunctions::emailMarketSnapshot($this->_webAdapter->getComponentId(), $this->_webAdapter->data()['email'], $agentContact->id, $this->_webAdapter->data()['address'], $this->_webAdapter->data()['city'], $this->_webAdapter->data()['state'], $this->_webAdapter->data()['zip'], $this->_webAdapter->data()['first_name'], $this->_webAdapter->data()['last_name'], Yii::app()->user->primaryDomain->name, Yii::app()->user->board->id, Yii::app()->user->accountId);
            }
        }
    }

    protected function _emailAddressOnlyLeadAlert()
    {
        // Get lead route and agents to notify
        $leadRoute = \LeadRoutes::model()->byComponentTypeId(\ComponentTypes::SELLERS)->find(array('order' => 'sort_order asc'));
        $agentsToNotify = \LeadRoutes::getContactsToNotify($leadRoute, true);

        $logoImage = '<br /><br /><img src="' . \Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Display Team Logo" style="width:200px;">';
        $content = '<b>Seller Lead Partial Submit (Address Only)</b><br /><br />'
            .'This info is sent to you in case the seller prospect does not complete the form. This way you can mail your marketing package or door knock this prospect. If the prospect does complete the form you will get another seller lead notification and you can disregard this email. Go get \'em! =)<br /><br />'
            .'<div style="font-weight:bold;font-size:25px;">'
        ;

        // build body content of email populating form submission values
        foreach($this->_webAdapter->data() as $fieldName => $value) {

            // Special handling for state
            if($fieldName == 'state' && is_numeric($value)) {
                $value = \AddressStates::getShortNameById($value);
            }

            $content .= ucwords(\FormFields::getFieldLabelByName($fieldName)) . ': ' . $value . '<br />';
        }

        // Generate message content
        $content .= $logoImage.'<br/><br/>'
            .'</div>';

        foreach($agentsToNotify as $agent) {
            $to[$agent->primaryEmail] = $agent->fullName;
        }

        if (YII_DEBUG) {
            $to = \StmMailMessage::DEBUG_EMAIL_ADDRESS;
        }

        // Send the emails
        if (!YII_DEBUG) {
            $from = array('do-Not-Reply@seizethemarket.net'=>'Lead Alert');
            $subject = 'Seller Lead Alert - ' . $this->_webAdapter->data()->address . ' (Partial Submit - Address Only)';
            $bcc = 'christine@seizethemarket.com';
            \StmZendMail::easyMail($from, $to, $subject, $content, $bcc, 'text/html');
        }
    }
}