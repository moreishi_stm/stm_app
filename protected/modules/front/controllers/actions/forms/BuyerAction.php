<?php

include_once("AbstractFormSubmitAction.php");

/**
 * Processes a form submission for the 'Property Tax Info Request' form.
 *
 */
class BuyerAction extends AbstractFormSubmitAction {

    public $autoLogin = false;

    protected function performRequest($formId) {
		//Additional Actions to take place here
	}
}
