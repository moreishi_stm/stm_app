<?php
Yii::import('front_widgets.PageViewTrackerWidget.PageViewTrackerWidget');
Yii::import('front_widgets.RecentSavedHomesWidget.RecentSavedHomesWidget');

class LoginAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
//        if(!Yii::app()->user->isGuest) {
//            $this->controller->redirect('/myAccount');
//        }

		if(isset($_REQUEST["addListingID"])){
			header('Content-Type: application/json');
			 echo json_encode($this->getListingData($_REQUEST["addListingID"]));
            return;
		}
		$model = new LoginForm;

		$this->controller->layout = 'column1';

        //@todo: temp for storyboard CSS - this will not be necessary once we get over to bootstrap 100%
        if(Yii::app()->params['layoutDirectory'] == 'storyboard') {
            $this->registerStoryboardCss();
        }

        // collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			$model->email = trim($model->email);
			$model->password = trim($model->password);

			// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
            	PageViewTrackerWidget::processLogin();
            	RecentSavedHomesWidget::processLogin();
            	Contacts::updateLastLogin();
            	LoginLog::processLogin();
				if(
					isset($_SERVER["HTTP_REFERER"])
					&&
					!empty($_SERVER["HTTP_REFERER"])
					&&
					!stristr($_SERVER["HTTP_REFERER"],"/login",1)
					&&
					!stristr($_SERVER["HTTP_REFERER"],"/logout",1)
				){
					$redirectUrl = $_SERVER["HTTP_REFERER"];
				} else{
					$redirectUrl = '/myAccount';
				}
				if(!Yii::app()->request->getPost("isAjax")){
					return $this->controller->redirect(Yii::app()->user->getReturnUrl($redirectUrl));
				}else{
					header('Content-type: application/json');
					echo CJSON::encode(array("status"=>200,"redirect"=>$redirectUrl));
					return;
				}

            }
		} elseif (isset($_GET['email'])) {
			if(isset($_GET['resubmit']))
				$model->resubmit = $_GET['resubmit'];

			$email = str_replace(' ','',trim($_GET['email']));
			$model->email = $email;
		}
		if(!Yii::app()->request->getPost("isAjax")){
			return $this->controller->render('login', array('model'=>$model));
		} else {
			header('Content-type: application/json');
			echo CJSON::encode(array("status"=>404));
			return;
		}


	}

    protected function registerStoryboardCss() {
        $css = <<<CSS
    #login {
        margin-top: 80px;
    }
    #login #login-container {
        margin: 40px auto 60px;
        background: none repeat scroll 0% 0% #F5F5F5;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
        border: 1px solid #DDD;
        border-radius: 6px;
        padding: 30px 30px 60px;
        width: 500px;
    }
    #login h1, h1.green {
        font-size: 20px;
        color: #5BA71B;
        text-align: center;
        line-height: 1;
    }
    #login h2 {
        font-size: 22px;
        text-align: center;
        font-weight: bold;
    }
    #login label {
        width: 145px;
        display: inline-block;
        text-align: right;
        font-weight: bold;
    }
    #login form input{
        width: 300px;
        font-size: 20px;
        margin-bottom: 10px;
        padding: 5px 2px 5px 4px;
        height: 100%;
        border: 1px solid #BBB;
        padding: 4px 2px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    input, textarea, input:invalid, input:required, textarea:required, select {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
    }

    input:hover, input:focus, textarea:hover, textarea:focus, select:hover, select:focus {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    }
    #login .row.submit {
        height: 25px;
        text-align: left;
        position: relative;
        left: 145px;
    }
    #login-form-button {
        font-size: 11px;

        background: #fceec7;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjZWVjNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjOWEzMzkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
        background: -moz-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fceec7), color-stop(100%, #c9a339));
        background: -webkit-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -o-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -ms-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: linear-gradient(to bottom, #fceec7 0%, #c9a339 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fceec7', endColorstr='#c9a339', GradientType=0);
        -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#fceec7', endColorstr='#c9a339')"; /* IE8 */
        border: 1px solid;
        border-color: #CC9F70 #B36310 #B36310 #c68f56;
        cursor: pointer;

        color: #333;
        text-transform: uppercase;
        padding: 10px 28px;
        -webkit-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);

        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;

        position: relative;
        display: inline-block;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        text-decoration: none !important;
        font-weight: 700;
        outline: 0 none;
        text-align: center;
        margin: 2px;
        min-height: 14px;
        min-width: 8px;
        white-space: pre-line;
        vertical-align: baseline;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.7);
    }
    div.errorMessage {
        width: 300px;
        margin-left: 150px;
        margin-bottom: 10px;
        clear: both;
        color: #FF5757;
        font-weight: bold;
        font-size: 11px;
    }
    input.error {
        background: none repeat scroll 0% 0% #FFB8B8;
    }
CSS;
        Yii::app()->clientScript->registerCss('forgotPasswordStoryboardCss', $css);
    }

	public function getListingData($id){
		$mlsBoardId = Yii::app()->request->getQuery('mlsBoardId');
		if($mlsBoardId && ($mlsBoard = MlsBoards::model()->findByPk($mlsBoardId))) {
			$tablePrefixedName = $mlsBoard->prefixedName;
		}
		else {
			$tablePrefixedName = Yii::app()->user->board->prefixedName;
		}
		if(!($Property = MlsProperties::model($tablePrefixedName)->findByPk($id))) {
            //if property not found
            return;
        }
		$hasPhotoUrl = Yii::app()->user->board->has_photo_url;

		if($hasPhotoUrl) {
			$boardName = Yii::app()->user->board->getPrefixedName();
			$mlsPropertyPhoto = MlsPropertiesPhoto::model($boardName)->findByAttributes(array(
				'listing_id' => $id,
				'photo_number' => 1,
			));
			if($mlsPropertyPhoto) {
				$photoUrl = new MlsPropertyPhoto($Property, 1);
			}
		}
		else {
			$photoUrl = new MlsPropertyPhoto($Property, 1);
		}
		$propertyDetails = $Property->getHomeDetails($Property->listing_id);
		$photoUrls = explode(',', $propertyDetails['photoUrls']);
		$photoUrl = $photoUrls[0];

//@todo: temp quick fix for non-photo url
		if(!Yii::app()->user->board->has_photo_url) {
			$photoUrl = $Property->getPhotoUrl(1);
		}

		if($Property->listing_id) {
			$photCountText = ($Property->photo_count) ? $Property->photo_count.' Photos Available!' : ' ';
			$todayDate =  date('F j, Y');
			$html = <<<HTML
			<div style="margin-bottom: 10px; clear: both;">
				<div class="top_image">
					<img src="{$photoUrl}" width="220px">
				</div>
				<div class="top_description">
					<div style="text-align:center;color: #00c300; font-weight: bold; font-size: 18px;margin-top: 20px;">{$photCountText}</div>
					<div style="text-align:center;font-weight: bold; font-size: 14px; margin-top: 4px;">View All Photos, Maps, Details</div>
					<div style="text-align:center; margin-top: 15px;"> All Properties current as of<br />Today <strong>{$todayDate}</strong>.</div>
				</div>
				<div class="clearfix"></div>
			</div>
HTML;

		}else{
			$html = "";
		}

		$formFields =  array(
			FormFields::getFieldIdByName('listing_id') => $id,
			FormFields::getFieldIdByName('photo_url') => $photoUrl,
			FormFields::getFieldIdByName('home_url')=> $Property->getUrl(),
			FormFields::getFieldIdByName('price') => $Property->price,
			FormFields::getFieldIdByName('bedrooms') => $Property->bedrooms,
			FormFields::getFieldIdByName('baths') => $Property->baths_total,
			FormFields::getFieldIdByName('sq_feet') => $Property->sq_feet,
			FormFields::getFieldIdByName('address') => $Property->streetAddress,
			FormFields::getFieldIdByName('state') => $Property->state,
			FormFields::getFieldIdByName('zip') => $Property->zip
		);
		$formFieldsText = "";
		foreach ($formFields as $k => $v) {
			$formFieldsText .= "<input value=\"{$v}\" name=\"FormSubmissionValues[data][{$k}]\" id=\"FormSubmissionValues_data_{$k}\" type=\"hidden\">".PHP_EOL;
		}

		return  array(
			"response" => 200,
			"header" => $html,
			"formFieldHtml" => $formFieldsText,
			"formFields" => $formFields
		);
	}
}
