<?php

	class MarketUpdatesAction extends CAction {

		/**
		 * Manages models
		 */
		public function run($id) {
			$this->controller->pageTitle = 'Market Update';

			$tableName = Yii::app()->user->board->prefixedName;
			$model = MlsProperties::model($tableName);
			$Criteria = $model->getCriteria(ComponentTypes::SAVED_SEARCHES, $id);

			$ActiveCriteria = $model->getCriteriaByTermName('status', 'Active');
			$Criteria->mergeWith($ActiveCriteria);
			$modelName = get_class($model);

			// process listbox submission
			if (isset($_GET[$modelName])) {
				foreach ($_GET[$modelName] as $key => $value) {
					if (!empty($value)) {
						$model->$key = $value;
						$Criteria->mergeWith($model->getCriteriaByTermName($key, $value));
					}
				}
			}

			$DataProvider = $model->getDataProvider($Criteria);
			$DataProvider->sort->defaultOrder = 'property_last_updated DESC';

			$areaMessage = (Yii::app()->user->getState('areaMessage')) ? Yii::app()->user->getState('areaMessage') : null;
			Yii::app()->user->setState('areaMessage', null);
			$areaMessage = '<h3 style="text-align:center;padding:15px;">' . $areaMessage . '</h3>';

			$this->controller->render('marketUpdate', array(
											  'DataProvider' => $DataProvider,
											  'model' => $model,
											  'areaMessage' => $areaMessage,
											  )
			);
		}
	}