<?php

class BuyerAction extends CAction {

	/**
	 * Manages models
	 */
	public function run()
    {
        $formId = Forms::FORM_BUYER_DREAM_HOME_SHORT;
        $SubmissionValues = new FormSubmissionValues($formId);
        $FormFields = new FormFields;

        $this->controller->render('buyer', array(
                'model' => $SubmissionValues,
                'formId' => $formId,
                'FormFields' => $FormFields,
            ));
	}
}