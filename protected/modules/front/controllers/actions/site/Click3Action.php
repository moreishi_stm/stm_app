<?php
/**
 * Click Action
 *
 * Used to manage email link tracking
 * @author Nicole Xu
 */
class Click3Action extends CAction
{
    /**
     * Run
     *
     * Called when this action is ran
     * @return void
     */
    public function run()
    {
        // Get data
        $encodedContactId = Yii::app()->request->getParam('ec');
        $rawContactId = Yii::app()->request->getParam('rc');
        $encodedEmailMessageId = Yii::app()->request->getParam('em');
        $rawEmailMessageId = Yii::app()->request->getParam('rei');
        $encodedEmailTemplateId = Yii::app()->request->getParam('etid');
        $rawEmailTemplateId = Yii::app()->request->getParam('tid');
        $url = Yii::app()->request->getParam('url');

        $isRot13 = false;
        if(strpos($url,'uggc') === 0) {
            $isRot13 = true;
            $url = str_rot13($url);
            $encodedContactId = str_rot13($encodedContactId);
            $rawContactId = str_rot13($rawContactId);
            $encodedEmailMessageId = str_rot13($encodedEmailMessageId);
            $rawEmailMessageId = str_rot13($rawEmailMessageId);
            $encodedEmailTemplateId = str_rot13($encodedEmailTemplateId);
            $rawEmailTemplateId = str_rot13($rawEmailTemplateId);
        }

        // decode & verify contact ID match with encode and raw
        if($encodedContactId || $rawContactId) {
            $decodedContactId = StmFunctions::click3Decode($encodedContactId);

            if(intval($rawContactId) != $decodedContactId && date('Y-m-d') < '2015-02-28') {
                Yii::log(__FILE__.'('.__LINE__.'): Decoded Contact ID: '.$decodedContactId.' is not equal to the Raw Contact ID: '.$rawContactId, CLogger::LEVEL_ERROR);
            }
            // verify that decoded and raw contact id match
            if($decodedContactId == intval($rawContactId) || strpos($rawContactId, $decodedContactId) == 0) {
                // Retrieve contact
                $contact = Contacts::model()->findByPk($decodedContactId);
                if(!$contact instanceof Contacts) {
                    Yii::log(__FILE__.'('.__LINE__.'): Contact ID# '.$rawContactId.' is invalid.', CLogger::LEVEL_ERROR);
                    $url = ($url) ? $url : '/';
                    $this->controller->redirect($url);
                }
            }
            else {
                Yii::log(__FILE__.'('.__LINE__.'): Error in Decoding Email Tracking Link. User was sent to :'.$url, CLogger::LEVEL_ERROR);
                $url = ($url) ? $url : '/';
                $this->controller->redirect($url, true, 302);
            }
        }
        else {
            $this->controller->redirect('/', true, 302);
        }

        // Get and check email template id
        $decodedEmailTemplateId = null;
        if($encodedEmailTemplateId && $rawEmailTemplateId) {

            //@todo: there are situations where 2 digits are added to the back - dunno what causes it but check if it happens to contactId, if it always does for everything else, then make flag so other variables can look out for it.

            $decodedEmailTemplateId = StmFunctions::click3Decode($encodedEmailTemplateId);

            if(intval($rawEmailTemplateId) != $decodedEmailTemplateId) {
                Yii::log(__FILE__.'('.__LINE__.'): Decoded Email Template ID: '.$decodedEmailTemplateId.' is not equal to the Raw Email Temmplate ID: '.$rawEmailTemplateId, CLogger::LEVEL_ERROR);
            }

        }

        if(($encodedEmailMessageId && $rawEmailMessageId) || ($decodedContactId == $rawContactId)) {

            $decodedEmailMessageId = StmFunctions::click3Decode($encodedEmailMessageId);

            if(intval($rawEmailMessageId) != $decodedEmailMessageId && date('Y-m-d') < '2015-02-28') {
                Yii::log(__FILE__.'('.__LINE__.'): Decoded Email Message ID: '.$decodedEmailMessageId.' is not equal to the Raw Email Message ID: '.$rawEmailMessageId, CLogger::LEVEL_ERROR);
            }

            // if the email message id is valid, encoded and raw matches
            if($decodedEmailMessageId == intval($rawEmailMessageId) || strpos($rawEmailMessageId, $decodedEmailMessageId) == 0 || ($decodedContactId == $rawContactId)) {
                // Retrieve email message
                if(!empty($decodedEmailMessageId) && ($decodedEmailMessageId == intval($rawEmailMessageId) || strpos($rawEmailMessageId, $decodedEmailMessageId) == 0)) {

                    $emailMessage = EmailMessage::model()->findByPk($decodedEmailMessageId);
                    if(!$emailMessage instanceof EmailMessage) {
                        if(!YII_DEBUG) {
                            Yii::log(__FILE__.'('.__LINE__.'): Email ID '.$decodedEmailMessageId.' is invalid.', CLogger::LEVEL_ERROR);
                        }
                        $url = ($url) ? $url : '/';

                        // send click notification
                        $this->_sendEmail($contact, $url);

                        $this->controller->redirect($url);
                    }
                }

                // Catalog the email click tracking data
                $emailClickTracking = new EmailClickTracking();
                $emailClickTracking->setAttributes(array(
                        'email_message_id'  =>  $emailMessage->id,
                        'contact_id'        =>  $decodedContactId,
                        'email_template_id' =>  $decodedEmailTemplateId,
                        'url'               =>  $url
                    ));
                if(!$emailClickTracking->save()) {
                    Yii::log(__FILE__.'('.__LINE__.'): Email Click Tracking did not save. Errors: '.print_r($emailClickTracking->getErrors(), true).' Attributes:'.print_r($emailClickTracking->attributes, true), CLogger::LEVEL_ERROR);
                }
                else {
                    $emailClickId = $emailClickTracking->id;
                }
            }
        }

        // Log the user in automatically
        $loginForm = new LoginForm;
        $loginForm->email = $contact->getPrimaryEmail();
        $loginForm->password = $contact->password;
        $loginForm->rememberMe = false;
        if(!$loginForm->login()) {
            Yii::log(__FILE__.'('.__LINE__.'): Email Auto Login error. Errors: '.print_r($loginForm->getErrors(), true).' Attributes:'.print_r($loginForm->attributes, true), CLogger::LEVEL_ERROR);
        }

        if(strpos($url,'Market Update') !== false) {
            $url = str_replace('seizethemarket.com','christineleeteam.com',$url);
        }

        // send click notification
        $this->_sendEmail($contact, $url, $emailClickId);

        // Redirect to the URL requested
        if(strpos($url, 'http://') == 0 && strpos($url, '%2F') !== false) {
            $url = urldecode($url);
        }
        $this->controller->redirect($url, true, 302);
    }

    protected function _sendEmail(Contacts $contact, $url, $emailClickId=null)
    {
        // only do CLT right now
        if(!in_array(Yii::app()->user->clientId , array(1, 16, 9))) {
            return;
        }

        // check to see if there is a to email'

        if(!($toEmails = Yii::app()->user->settings->email_click_notification_emails)) {
            return;
        }

        $from = array('do-no-reply@seizethemarket.net' => 'STM Click Notification');
        $subject = 'STM Email Click Notification - '.$contact->fullName.(($contact->spouseFullName) ? ' & '.$contact->spouseFullName : '');
//        $bcc = 'debug@seizethemarket.com';

        $body = '<h1>Seize the Market Email Click Notification</h1>';
        $body .= '<h1>'.$contact->fullName.' has clicked on an email link and returned to the site.</h1>';



        $body .= '<h3>URL: '.EmailClickTracking::printGridUrl($url).'</h3>';
        $body .= '<h1><a href="http://www.'.Yii::app()->user->primaryDomain->name.'/admin/tracking/emailClicks'.(($emailClickId) ? '#'.$emailClickId : '').'">View Email Click Report (Click here)</a></h1>';
        $body .= '<hr>';

        // only do this for non stm_hq clients
        if(!in_array(Yii::app()->user->clientId , array(9))) {

            $criteria = new CDbCriteria();
            $criteria->compare('contact_id', $contact->id);
            $criteria->addInCondition('component_type_id', array(ComponentTypes::SELLERS, ComponentTypes::BUYERS));
            //$criteria->addInCondition('transaction_status_id', array(ComponentTypes::SELLERS, ComponentTypes::BUYERS));
            $criteria->order = 'component_type_id DESC, transaction_status_id ASC';
            $tnxs = Transactions::model()->findAll($criteria);

            $sellerLeads = '';
            $buyerLeads = '';
            if($tnxs) {
                $body .= '<table style="width: 800px; padding: 6px;"  cellspacing="0">';

                foreach($tnxs as $tnx) {
                    $row = '<tr>';
                    $row .= '<td>'.$tnx->status->name.'</td>';

                    //assignments
                    $assignmentsString = '';
                    if($tnx->buyerSellerAssignments) {
                        foreach($tnx->buyerSellerAssignments as $assignment) {
                            $assignmentsString .= ($assignmentsString) ? '<br>' : '';
                            $assignmentsString .= $assignment->contact->fullName.' ('.$assignment->assignmentType->display_name.')';
                        }
                    }
                    $row .= '<td>'.$assignmentsString.'</td>';

                    $row .= '<td>'.Yii::app()->format->formatDateTime($tnx->added, array("break"=>true)).'</td>';
                    $row .= '<td><a href="http://'.Yii::app()->user->verboseDomainName.'/admin/'.ComponentTypes::getNameById($tnx->component_type_id).'/'.$tnx->id.'">View Details</a></td>';
                    $row .= '</tr>';

                    switch($tnx->component_type_id) {
                        case ComponentTypes::SELLERS:
                            $sellerLeads .= $row;
                            break;

                        case ComponentTypes::BUYERS:
                            $buyerLeads .= $row;
                            break;
                    }
                }

                $leadHeader = '<tr style="background: #EFEFEF;"><td style="width: 200px;">Type / Status</td><td style="width: 250px;">Assigned To</td><td style="width: 150px;">Added</td><td style="width: 100px;">Link</td></tr>';
                // active seller leads
                if($sellerLeads) {
                    $body .= '<tr ><td colspan="5"><h2>Seller Leads/Transactions</h2></td></tr>';
                    $body .= $leadHeader;
                    $body .= $sellerLeads;
                }
                // active buyer leads
                if($buyerLeads) {
                    $body .= '<tr ><td colspan="5"><h2>Buyer Leads/Transactions</h2></td></tr>';
                    $body .= $leadHeader;
                    $body .= $buyerLeads;
                }

                $body .= '</table>';
            }

            // recently viewed homes
            if($viewedHomes = ViewedPages::model()->getViewedHomesData($contact->id, 10)) {
                $body .= '<hr/>';
                $body .= '<h2>Viewed Homes</h2>';
                $body .= '<table style="width: 800px; padding: 6px;"  cellspacing="0">';
                $body .= '<tr style="background: #EFEFEF;"><td style="width: 100px;">Date/Time</td><td style="width: 150px;">Price / MLS #</td><td>Property Info</td><td>Address/Neighborhood</td></tr>';
                foreach($viewedHomes as $viewedHome) {
                    $body .= '<tr>';
                    $body .= '<td>'.Yii::app()->format->formatDateTime($viewedHome["added"],array("break"=>true)).'</td>';
                    $body .= '<td>'.Yii::app()->format->formatDollars($viewedHome["price"]).'<br> MLS #: '.$viewedHome["listing_id"].'</td>';
                    $body .= '<td>'.(($viewedHome["bedrooms"]) ? $viewedHome["bedrooms"]."BR / ".$viewedHome["baths_full"]." Baths <br>".number_format($viewedHome["sq_feet"])." SF" : " ").'</td>';
                    $body .= '<td>'.(($viewedHome["common_subdivision"]) ? "<strong><u>".$viewedHome["common_subdivision"]."</u></strong><br>": "").CHtml::link(StmFormatter::formatProperCase(StmFormatter::formatRemoveExtraSpaces($viewedHome["streetAddress"])), "http://".Yii::app()->user->verboseDomainName."/home/".$viewedHome["state"]."/".strtolower(StmFormatter::formatLink($viewedHome["city"]))."/".strtolower(StmFormatter::formatLink($viewedHome["streetAddress"]))."/".$viewedHome["listing_id"], $htmlOptions=array('target'=>'_blank')).", ".Yii::app()->format->formatProperCase($viewedHome["city"]).", ".strtoupper($viewedHome["state"])." ".$viewedHome["zip"].'</td>'; //
                    $body .= '</tr>';
                }
                $body .= '</table>';
            }
        }

        StmZendMail::easyMail($from, $toEmails, $subject, $body, $bcc, $type='text/html', $awsTransport = true, $ignoreOnDebug = false);

//        $toEmails = explode(',', $toEmails);
//        foreach($toEmails as $toEmail) {
//            StmZendMail::easyMail($from, $toEmail, $subject, $body, $bcc, $type='text/html', $awsTransport = true, $ignoreOnDebug = false);
//        }
    }
}