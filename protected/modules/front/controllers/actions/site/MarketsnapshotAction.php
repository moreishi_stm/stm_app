<?php
class MarketsnapshotAction extends CAction
{
    public function run($id)
    {
        Yii::log(__CLASS__.' ('.__LINE__.') FYI Market Snapshot is being viewed. Remove log when done observing.', CLogger::LEVEL_ERROR);

        $this->controller->pageTitle = 'House Values Report';
        $this->controller->layout = 'column2';

        if(Yii::app()->user->settings->market_snapshot != '1') {
            //@todo: notify admin?? - do something useful
            $this->controller->redirect('/');
        }

        //verify market snapshot setting from transaction field value valid/enabled
        if(!Yii::app()->db->createCommand('SELECT value from transaction_field_values WHERE transaction_id='.intval($id).' AND transaction_field_id='.TransactionFields::MARKET_SNAPSHOT_FREQUENCY)->queryScalar()) {
            //@todo: notify admin?? - do something useful
            //$this->controller->redirect('/');
        }

        if(!Yii::app()->user->isGuest) {

            $bedroomsSubquery = '(SELECT value from transaction_field_values v1 where t.id=v1.transaction_id AND v1.transaction_field_id='.TransactionFields::BEDROOMS_SELLER.')';
            $bathsSubquery = '(SELECT value from transaction_field_values v2 where t.id=v2.transaction_id AND v2.transaction_field_id='.TransactionFields::BATHS_SELLER.')';
            $sfSubquery = '(SELECT value from transaction_field_values v3 where t.id=v3.transaction_id AND v3.transaction_field_id='.TransactionFields::SQ_FEET_SELLER.')';

            //verify address for transaction exists
            $address = Yii::app()->db->createCommand("SELECT address, city, state_id, zip, {$bedroomsSubquery} bedrooms, {$bathsSubquery} baths, {$sfSubquery} sq_feet , IF(t.mls_property_type_id IS NULL OR t.mls_property_type_id='', 1, t.mls_property_type_id) propertyTypeId FROM transactions t JOIN addresses a ON a.id=t.address_id WHERE t.id=".intval($id))->queryRow();
            if(empty($address['address'])) {
                //@todo: notify admin?? - do something useful
                $this->controller->redirect('/');
            }

            $propertyTypeId = $address['propertyTypeId'];
            $geoAddress = $address['address'].', '.$address['city'].', '.AddressStates::getShortNameById($address['state_id']).', '.$address['zip'];
            $googleDataSubject = StmFunctions::getGoogleAddressData($address['address'], $address['city'], AddressStates::getShortNameById($address['state_id']), $address['zip']); // get zip only
            if(isset($googleDataSubject['results'][0]['geometry']['location']['lat'])) {
                $subjectMarkerCenter = ($googleDataSubject['results'][0]['geometry']['location']['lat']).','.$googleDataSubject['results'][0]['geometry']['location']['lng']; //['lat']+.0001
                $subjectMapCenter = ($googleDataSubject['results'][0]['geometry']['location']['lat']).','.$googleDataSubject['results'][0]['geometry']['location']['lng']; //['lat']+.00015
                $subjectCenter = $googleDataSubject['results'][0]['geometry']['location']['lat'].','.$googleDataSubject['results'][0]['geometry']['location']['lng'];
            }
//45.3941049,-122.7837036    // 3a,75y,39h,90t
            $boardName = Yii::app()->user->board->getPrefixedName();

            $zip = $address['zip'];

            $baseStatCommand = Yii::app()->stm_mls->createCommand()
                ->from($boardName.'_properties')
                ->andWhere('status="Active" OR (status="Sold" AND status_change_date >= "'.date('Y-m-d', strtotime('-12 months')).'")')
                ->andWhere('zip='.$zip);

            $activeCountSubquery = '(SELECT count(listing_id) FROM '.$boardName.'_properties WHERE status="Active" AND zip="'.$zip.'" AND status_change_date >= "'.date('Y-m-d', strtotime('-3 months')).'")';
            $soldCountSubquery = '(SELECT count(listing_id) FROM '.$boardName.'_properties WHERE status="Sold" AND zip="'.$zip.'")';
            $priceStatCommand = clone $baseStatCommand;
            $stats = $priceStatCommand->select($activeCountSubquery.' activeCount, '.$soldCountSubquery.' soldCount, MIN(price) minPrice, MAX(price) maxPrice, ROUND(AVG(price)) avgPrice, ROUND(AVG(sq_feet)) avgSf, ROUND(AVG(DATEDIFF(sold_date, list_date))) dom, ROUND(AVG(price)/AVG(sq_feet)) avgDollarSf')->queryRow();
//            $stats['activeCount'] = $priceStats['activeCount'];
//            $stats['avgSf'] = $priceStats['avgSf'];
//            $stats['avgDollarSf'] = round($priceStats['avgPrice'] / $priceStats['avgSf']);
//            $stats['lowPrice'] = $priceStats['minPrice'];
//            $stats['highPrice'] = $priceStats['maxPrice'];
//            $stats['averagePrice'] = $priceStats['avgPrice'];
//            $stats['dom'] = $priceStats['dom'];



//            $data = Yii::app()->stm_mls->createCommand()
//                ->select('status_change_date, mls_property_type_id, listing_id, status, price, bedrooms, baths_full, sq_feet, geo_lat, geo_lon, status, street_number, street_name, street_suffix, city, state, zip')
//                ->from($boardName.'_properties')
//                ->where('zip="'.$zip.'"')
//    //            ->andWhere('sq_feet >= '.($sqFeet *.9)) // -10%
//    //            ->andWhere('sq_feet <= '.($sqFeet *1.2)) // +20%
//                ->andWhere('status="Active" OR status_change_date >= "'.date('Y-m-d', strtotime('-6 months')).'"') // +20%
//                ->andWhere(array('in', 'status', array('Active','Sold')))
//                ->queryAll();

    //        var locations = [
    //        ['Bondi Beach', -33.890542, 151.274856, 4],
    //        ['Coogee Beach', -33.923036, 151.259052, 5],
    //        ['Cronulla Beach', -34.028249, 151.157507, 3],
    //        ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
    //        ['Maroubra Beach', -33.950198, 151.259302, 1]
    //    ];

            // get lat/long from google for subject address
            $googleData = StmFunctions::getGoogleAddressData($address['address'], $address['city'], AddressStates::getShortNameById($address['state_id']), $address['zip'], true); // get zip only
            if(isset($googleData['results'][0]['geometry']['location']['lat'])) {
                $center = $googleData['results'][0]['geometry']['location']['lat'].','.$googleData['results'][0]['geometry']['location']['lng'];
            }

            /**
             * Chart #1 Data
             */
            $chartData = array();

            $xLabel = [];
            for($i=12; $i>=1; $i--) {
                $xLabel[] = date('M', strtotime("-{$i} months"));
            }
            $chartData['xLabel'] = StmFunctions::formatChartDataToJson($xLabel);

            $toDate = date('Y-m-t', strtotime('-1 months'));
            $fromDate = date('Y-m-d', strtotime('-12 months'));


            $avgPriceData = Yii::app()->stm_mls->createCommand("SELECT ROUND(AVG(price)) FROM {$boardName}_properties WHERE status='Sold' AND zip='{$zip}' AND mls_property_type_id IN({$propertyTypeId}) AND DATE(status_change_date) BETWEEN '{$fromDate}' AND '{$toDate}' GROUP BY YEAR(status_change_date), MONTH(status_change_date) ORDER BY YEAR(status_change_date), MONTH(status_change_date) ASC")->queryColumn(); //
            $chartData['avgPrice'] = StmFunctions::formatChartDataToJson($avgPriceData, false);
            $chartData['maxChartY1'] = Yii::app()->stm_mls->createCommand("SELECT CEIL((AVG(price) * 1.25) / 50000) * 50000 chartMaxPrice FROM {$boardName}_properties WHERE status='Sold' AND zip='{$zip}' AND mls_property_type_id IN({$propertyTypeId}) AND DATE(status_change_date) BETWEEN '{$fromDate}' AND '{$toDate}' GROUP BY YEAR(status_change_date), MONTH(status_change_date) ORDER BY chartMaxPrice DESC")->queryScalar(); //

            $soldUnits = Yii::app()->stm_mls->createCommand("SELECT count(listing_id) FROM {$boardName}_properties WHERE status='Sold' AND zip='{$zip}' AND mls_property_type_id IN({$propertyTypeId}) AND DATE(status_change_date) BETWEEN '{$fromDate}' AND '{$toDate}' GROUP BY YEAR(status_change_date), MONTH(status_change_date) ORDER BY YEAR(status_change_date), MONTH(status_change_date) ASC")->queryColumn(); //
            $chartData['soldUnits'] = StmFunctions::formatChartDataToJson($soldUnits, false);
            $chartData['maxChartY2'] = round(max($soldUnits) * 1.3); //Yii::app()->stm_mls->createCommand("SELECT CEIL((count(listing_id) * 1.3) / ROUND(count(listing_id) * .2, -2)) * ROUND(count(listing_id) * .2, -2) chartMaxUnit FROM {$boardName}_properties WHERE status='Sold' AND zip='{$zip}' AND mls_property_type_id IN(1) AND DATE(status_change_date) BETWEEN '{$fromDate}' AND '{$toDate}' GROUP BY YEAR(status_change_date), MONTH(status_change_date) ORDER BY chartMaxUnit DESC")->queryScalar(); //
    //CEIL((count(listing_id) * 1.3) / ROUND(count(listing_id) * .2, -2)) * ROUND(count(listing_id) * .2, -2)
            /**
             * Chart #2 Data
             */
            $piePrices = array(
                1 => ['min' => 0, 'max' => 100000, 'label' => 'Up to $100,000'],
                2 => ['min' => 100000, 'max' => 200000, 'label' => '$200,000\\\'s'],
                3 => ['min' => 200000, 'max' => 300000, 'label' => '$300,000\'s'],
                4 => ['min' => 300000, 'max' => 400000, 'label' => '$400,000\'s'],
                5 => ['min' => 400000, 'max' => 500000, 'label' => '$500,000\'s'],
                6 => ['min' => 500000, 'max' => 1000000, 'label' => '$600,000 to $1 Million'],
                7 => ['min' => 1000000, 'max' => 99000000, 'label' => 'Over $1 Million'],
            );

            foreach($piePrices as $i => $piePrice) {
                $results = Yii::app()->stm_mls->createCommand("SELECT count(listing_id) FROM {$boardName}_properties WHERE status='Sold' AND mls_property_type_id IN({$propertyTypeId}) AND zip='{$zip}' AND DATE(status_change_date) BETWEEN '{$fromDate}' AND '{$toDate}' AND price BETWEEN ".$piePrice['min']." AND ".$piePrice['max'])->queryScalar(); //

                $chartData['piePricePercent'.$i] = round(($results/array_sum($soldUnits)) * 100, 1);
                $chartData['piePriceLabel'.$i] = $piePrice['label'];
            }

            /**
             * Est Value
             */
            $fromSf = $address['sq_feet'] * .9;
            $toSf = ($address['sq_feet']) ? $address['sq_feet'] * 1.2 : 9999;
            $bedrooms = ($address['bedrooms']) ? $address['bedrooms'] : 3;
            $estPrices['price'] = Yii::app()->stm_mls->createCommand("SELECT ROUND(AVG(price)) FROM {$boardName}_properties WHERE status='Sold' AND zip='{$zip}' AND mls_property_type_id IN({$propertyTypeId}) AND DATE(status_change_date) BETWEEN '{$fromDate}' AND '{$toDate}' AND sq_feet BETWEEN '{$fromSf}' AND '{$toSf}' AND bedrooms >= ".$bedrooms)->queryScalar();

            // remove sq_feet if no results
            if(empty($estPrices['price'])) {

                $estPrices['price'] = Yii::app()->stm_mls->createCommand("SELECT ROUND(AVG(price)) FROM {$boardName}_properties WHERE status='Active' AND zip='{$zip}' AND mls_property_type_id IN({$propertyTypeId}) AND DATE(status_change_date) >'{$fromDate}' AND bedrooms >= ".$bedrooms)->queryScalar();
            }
            $estPrices['min'] = round($estPrices['price'] * .85);
            $estPrices['max'] = round($estPrices['price'] * 1.2);

            $subjectComparableData = Yii::app()->stm_mls->createCommand()
                ->select('status_change_date, mls_property_type_id, listing_id, status, price, bedrooms, baths_full, sq_feet, geo_lat, geo_lon, status, street_number, street_name, street_suffix, city, state, zip')
                ->from($boardName.'_properties')
                ->where('zip="'.$zip.'"')
                ->andWhere('bedrooms >= '.$bedrooms)
                ->andWhere('sq_feet >= '.$fromSf)
                ->andWhere('sq_feet <= '.$toSf)
                ->andWhere('status="Active" OR status_change_date >= "'.date('Y-m-d', strtotime('-12 months')).'"') // +20%
                ->andWhere(array('in', 'status', array('Active','Sold')))
                ->order('status DESC, price DESC')
                ->queryAll();

            // remove SF criteria
            if(empty($subjectComparableData)) {
                $subjectComparableData = Yii::app()->stm_mls->createCommand()
                    ->select('status_change_date, mls_property_type_id, listing_id, status, price, bedrooms, baths_full, sq_feet, geo_lat, geo_lon, status, street_number, street_name, street_suffix, city, state, zip')
                    ->from($boardName.'_properties')
                    ->where('zip="'.$zip.'"')
                    ->andWhere('bedrooms >= '.$bedrooms)
                    ->andWhere('status="Active" OR status_change_date >= "'.date('Y-m-d', strtotime('-12 months')).'"') // +20%
                    ->andWhere(array('in', 'status', array('Active','Sold')))
                    ->order('status DESC, price DESC')
                    ->queryAll();
            }

            $locationsData = '';
            $locationInfo = array();
            foreach($subjectComparableData as $i => $row) {
                $street = $row['street_number'].' '.$row['street_name'].' '.$row['street_suffix'];
                $cityStZip = $row['city'].', '.Yii::app()->user->board->state->short_name.' '.$row['zip'];

                $mapPinContent = '<div class="status '.(($row['status'] == 'Sold') ? 'sold' : 'active').'">'.$row['status'].'</div>';
                $mapPinContent .= '<div class="price">'.Yii::app()->format->formatDollars($row['price']).'</div>';
                $mapPinContent .= '<div>'.$street.'</div>';
                $mapPinContent .= '<div>'.(($row['mls_property_type_id'] == 3) ? 'Vacant Land' : $row['bedrooms'].' Beds | '.$row['baths_full'].' Baths | '.$row['sq_feet'].' SF').'</div>';

                $locationInfo[] = array(
                    'street' => $street,
                    'cityStZip' => $cityStZip,
                    'status' => $row['status'],
                    'lat' => $row['geo_lat'],
                    'lon' => $row['geo_lon'],
                    'listing_id' => $row['listing_id'],
                    'mapPinContent' => $mapPinContent,
                );
            }
            $locationsData = CJSON::encode($locationInfo);

            $dataProvider = new CArrayDataProvider($subjectComparableData, array(
                'pagination'=>array(
                    'pageSize'=> 200,
                ),
            ));
        }

        $this->controller->render(
            'marketsnapshot', array(
                'searchableFields' => MlsFieldMaps::getSearchableColumns(Yii::app()->user->board->id),
                'sellerId'=>$id,
                'estPrices' => $estPrices,
                'domainName' => Yii::app()->user->primaryDomain->name,
                'boardHasGeoData' => (strpos($_SERVER['SERVER_NAME'], 'thehupmangroup') !== false) ? 0 : 1, //@todo: pull this from board
                'locationsData' => $locationsData,
                'center' => $center,
                'dataProvider' => $dataProvider,
                'chartData' => $chartData,
                'SubmissionValues' => new FormSubmissionValues(Forms::FORM_MARKET_SNAPSHOT_QUESTION),
                'FormFields' => new FormFields,
                'formId' => Forms::FORM_MARKET_SNAPSHOT_QUESTION,
                'stats' => $stats,
                'address' => $address,
                'subjectCenter' =>$subjectCenter,
                'subjectMarkerCenter' => $subjectMarkerCenter,
                'subjectMapCenter' => $subjectMapCenter,
                'geoAddress' => $geoAddress,
                'streetAddress' => $address['address'],
                'cityStZip' => $address['city'].', '.AddressStates::getShortNameById($address['state_id']).' '.$address['zip'],
                'mapUrl' => 'http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q='.$geoAddress.'&amp;aq=&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear='.$geoAddress.'&amp;z=20&amp;output=embed'
            )
        );
    }

    public function printColumn1($data)
    {
        $hasPhotoUrl = Yii::app()->user->board->has_photo_url;

        $boardName = Yii::app()->user->board->getPrefixedName();
        if($hasPhotoUrl) {
            $mlsPropertyPhoto = MlsPropertiesPhoto::model($boardName)->findByAttributes(array(
                    'listing_id' => $data['listing_id'],
                    'photo_number' => 1,
                ));
            if($mlsPropertyPhoto) {
                $photoUrl = $mlsPropertyPhoto->getUrl();
            }
        }
        else {
            $mlsProperties = new MlsProperties($boardName);
            $photoUrl = new MlsPropertyPhoto($mlsProperties, 1);
        }

        if($photoUrl) {
            $string = "<img class=\"\" src=\"{$photoUrl}\">";
            //return 'http://mlsimages.seizethemarket.com/'.$boardName.'/'.$data['mls_property_type_id'].'/'.$data['listing_id'].'/'.$data['listing_id'].'-1.jpg';
        }


        return $string;
    }

    public function printColumn2($data)
    {
        $color = ($data["status"] == 'Sold') ? '#C20000' : '#0ABB0A';
        $string = "<div style='color:{$color}; font-weight: bold;'>".$data["status"]."</div>";
        $string .= "<div style='color: #0ABB0A; font-weight: bold; font-size: 17px;'>".Yii::app()->format->formatDollars($data["price"])."</div>";

        return $string;
    }

    public function printColumn3($data)
    {
        $string = $data["street_number"]." ".$data["street_name"]." ".$data["street_suffix"]."<br>";
        $string .= (($data['mls_property_type_id'] == 3) ? 'Vacant Land' : $data["bedrooms"]." Beds | ".$data["baths_full"]." Baths | ".number_format($data["sq_feet"])." SF ");

        switch($data["status"]) {
            case 'Sold':
                $string .= ($data["sold_date"]) ? '<br>Sold Date: '.date('m/d/Y', strtotime($data["sold_date"])) : '';
                break;

            case 'Active':
                $string .= ($data["list_date"]) ? '<br>List Date: '.date('m/d/Y', strtotime($data["list_date"])) : '';
                break;
        }
//        $string .= $data["bedrooms"]." Beds | ".$data["baths_full"]." Baths | ".number_format($data["sq_feet"])." SF ";

        $string .= '<br><span style="text-decoration: underline; color: blue;">View on Map - Click here</span>';

        return $string;
    }

    public function printColumn4($data)
    {
//        $string = $data["street_number"]." ".$data["street_name"]." ".$data["street_suffix"]."<br>";
//        $string .= $data["bedrooms"]." Beds ".$data["baths_full"]." Baths";

        return $string;
    }
}