<?php

class SearchSuggestionsAction extends CAction {
	 public function run() {
        $this->processAjaxRequest();
    }

    protected function processAjaxRequest() {
		header('Content-type: application/json');
        if (!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array('suggestions' => array()));
			Yii::app()->end();
		}

        $keyword = Yii::app()->request->getParam('query');

        // ********* USE: MATCH AGAINST in mysql *********
        // featured area / neighborhoods

        // subdivision / legal/common
        $tableName = Yii::app()->user->getBoard()->getPrefixedName().'_properties';
        // listing_id
        $results['MLS Number'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT listing_id FROM $tableName WHERE status='Active' AND listing_id='$keyword'")->queryColumn();

        // neighborhood
        $neighborhood1 = Yii::app()->stm_mls->createCommand("SELECT DISTINCT legal_subdivision FROM $tableName WHERE status='Active' AND legal_subdivision='$keyword'")->queryColumn();
        $neighborhood2 = Yii::app()->stm_mls->createCommand("SELECT DISTINCT common_subdivision FROM $tableName WHERE status='Active' AND common_subdivision='$keyword'")->queryColumn();
        $results['Neighborhood'] = array_unique(CMap::mergeArray($neighborhood1, $neighborhood2));

        // street name
        $results['Street Name'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT street_name FROM $tableName WHERE status='Active' AND street_name='street_name'")->queryColumn();

        // region/area

        // counties
        $results['County'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT county from $tableName WHERE status='Active' AND county like '%$keyword%'")->queryColumn();

        // cities
        $results['City'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT city from $tableName WHERE status='Active' AND city like '%$keyword%'")->queryColumn();

        // zip
        $results['Zip'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT zip from $tableName WHERE status='Active' AND zip like '%$keyword%' AND LENGTH(zip)=5")->queryColumn();

        // schools (all 3)
//        $results['Elementary School'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT elementary_school from {$tableName}_secondary p2 JOIN $tableName p ON p.listing_id=p2.listing_id WHERE p.status='Active' AND elementary_school like '%$keyword%'")->queryColumn();
//        $results['Middle School'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT middle_school from {$tableName}_secondary p2 JOIN $tableName p ON p.listing_id=p2.listing_id WHERE p.status='Active' AND middle_school like '%$keyword%'")->queryColumn();
//        $results['High School'] = Yii::app()->stm_mls->createCommand("SELECT DISTINCT high_school from {$tableName}_secondary p2 JOIN $tableName p ON p.listing_id=p2.listing_id WHERE p.status='Active' AND high_school like '%$keyword%'")->queryColumn();

        // public remarks
        $results['Property Comments'] = Yii::app()->stm_mls->createCommand("SELECT IF(COUNT(listing_id) > 0, '$keyword', NULL) from $tableName WHERE status='Active' AND public_remarks like '%$keyword%'")->queryColumn();

        // keywords
        $suggestions = array();
        foreach($results as $category => $rows) {

            foreach ($rows as $value) {
                if($value) {
                    $suggestions[] = array('value' => $value, 'data'=> array('category'=> $category));
                }
            }
        }
        $data = array('suggestions'=> $suggestions);

//		$suggestions = array("suggestions" => array(
//			array("value" => "United Arab Emirates", "data" => array("category" => "Countires")),
//			array("value" => "United Kingdom", "data" => array("category" => "Countires")),
//			array("value"=> "United States", "data" => array("category" => "Countires")),
//			array("value" => "United Arab Emirates", "data" => array("category" => "States")),
//			array("value" => "United Kingdom", "data" => array("category" => "States")),
//			array("value"=> "United States", "data" => array("category" => "States"))
//		));


		echo CJSON::encode($data);
		Yii::app()->end();
	}
}


