<?php

class MortgagecalculatorAction extends CAction {

	/**
	 * Manages models
	 */
	public function run($id=250000) {

		// Mortgage rates from Zillow
		$url = 'http://www.zillow.com/webservice/GetRateSummary.htm?zws-id=X1-ZWz1df9lsuj6dn_7yjzz&state=FL&output=json';
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$mortgageRates = CJSON::decode(curl_exec($curl));
		curl_close($curl);

		if (Yii::app()->request->isAjaxRequest)
			$this->performAjaxRequest($id, $mortgageRates);

		$this->controller->render('mortgageCalculator', array('mortgageRate'=>$mortgageRates,'price'=>200000));
	}

	protected function performAjaxRequest($price, $mortgageRates) {

		if (Yii::app()->request->isAjaxRequest) {
			$this->controller->renderPartial('homeDetails/_mortgageCalculator', array('mortgageRates'=>$mortgageRates, 'price'=>$price));

			Yii::app()->end();
		}
	}
}