<?php

class InstallAction extends CAction {

    const AUTH_CODE = 'qwerfdsazxcv';

    private $_sshConnection;

    public function run($authCode) {
        if (!$this->verifyAuthCode($authCode)) {
            throw new CHttpException(500, 'Auth code incorrect.');
        }

        $this->getController()->layout = 'column1';

        $installDetails = array();

        /**
         * Validate that the primary domain exist
         */
        $primaryDomain = Domains::model()->findByAttributes(array(
            'is_primary' => 1,
            'account_id' => Yii::app()->user->getAccountId(),
        ));
        $primaryDomainDetail = new InstallDetail;
        $primaryDomainDetail->setIsValid(!empty($primaryDomain));
        $primaryDomainDetail->description = 'Primary Domain Exists';
        if (!empty($primaryDomain)) {
            $primaryDomainDetail->addDetail('Primary domain: '.$primaryDomain->name);
        }
        array_push($installDetails, $primaryDomainDetail);

        /**
         * Validate that there is a valid domain for sending emails
         */
        $emailDomain = Domains::model()->findByAttributes(array(
            'for_emails' => 1,
            'account_id' => Yii::app()->user->getAccountId(),
        ));
        $emailDomainDetail = new InstallDetail;
        $emailDomainDetail->setIsValid(!empty($emailDomain));
        $emailDomainDetail->description = 'Email Domain Exists';
        if (!empty($emailDomain)) {
            $emailDomainDetail->addDetail('Domain for emails: '.$emailDomain->name);
        }
        array_push($installDetails, $emailDomainDetail);

        /**
         * Validate that the OpenDKIM ssh keys exist
         */
        $openDkimKeyDetail = new InstallDetail;
        $openDkimValid = false;
        if (!empty($primaryDomain)) {
            $keyExistsCmd = 'ls /etc/opendkim/keys/:domain | wc -l';
            $keyExistsCmd = strtr($keyExistsCmd, array(':domain' => $primaryDomain->name));
            $openDkimValid = ($this->getSshConnection()->exec($keyExistsCmd) == $numberOfValidKeys=2) ? true : false;
        } else {
            $openDkimKeyDetail->addDetail('Unable to retrieve primary domain.');
        }
        $openDkimKeyDetail->setIsValid($openDkimValid);
        $openDkimKeyDetail->description = 'OpenDKIM Keys On Server';
        array_push($installDetails, $openDkimKeyDetail);

        /**
         * Validate that the OpenDKIM configuration files are correct
         */
        $openDkimKeysConfDetail = new InstallDetail;
        $openDkimKeysConfValid = false;
        if (!empty($emailDomain)) {
            // Validate the KeyTable configuration
            $keyTableConfiguredCmd = 'grep -q "default._domainkey.:domain" /etc/opendkim/KeyTable && echo $?';
            $keyTableConfiguredCmd = strtr($keyTableConfiguredCmd, array(':domain' => $emailDomain->name));
            $keyTableConfigured = $this->getSshConnection()->exec($keyTableConfiguredCmd);
            $keyTableConfigured = ($keyTableConfigured === "0\n");
            if (false === $keyTableConfigured) {
                $openDkimKeysConfDetail->addDetail('Unable to validate the KeyTable configuration.');
            }

            // Validate the SignedTable configuration
            $signedTableConfiguredCmd = 'grep -q "*@:domain" /etc/opendkim/SigningTable && echo $?';
            $signedTableConfiguredCmd = strtr($signedTableConfiguredCmd, array(':domain' => $emailDomain->name));
            $signedTableConfigured = $this->getSshConnection()->exec($signedTableConfiguredCmd);
            $signedTableConfigured = ($signedTableConfigured === "0\n");
            if (false === $signedTableConfigured) {
                $openDkimKeysConfDetail->addDetail('Unable to validate the SignedTable configuration.');
            }

            $openDkimKeysConfValid = ($signedTableConfigured and $keyTableConfigured);
        } else {
            $openDkimKeysConfDetail->addDetail('Unable to retrieve domain for emails.');
        }
        $openDkimKeysConfDetail->setIsValid($openDkimKeysConfValid);
        $openDkimKeysConfDetail->description = 'OpenDKIM Keys Configuration';
        array_push($installDetails, $openDkimKeysConfDetail);

        /**
         * Validate SPF TXT
         */
        $spfDetail = new InstallDetail;
        $spfTxtIsValid = false;
        if (!empty($emailDomain)) {
            $spfTxtCmd = 'dig txt :domain | grep v=spf | cut -d : -f2 | cut -d \'"\' -f1';
            $spfTxtCmd = strtr($spfTxtCmd, array(':domain' => $emailDomain->name));
            $spfTxt = $this->getSshConnection()->exec($spfTxtCmd);
            if (false !== strpos($spfTxt, 'mail.seizethemarket.com +all')) {
               $spfTxtIsValid = true;
            }
            $spfDetail->addDetail("TXT: {$spfTxt}");
        } else {
            $spfDetail->addDetail('Unable to retrieve domain for emails.');
        }
        $spfDetail->setIsValid($spfTxtIsValid);
        $spfDetail->description = 'SPF TXT';
        array_push($installDetails, $spfDetail);

        /**
         * Validate the DKIM TXT
         */
        $openDkimTxtDetail = new InstallDetail;
        $openDkimTxtIsValid = false;
        if (!empty($emailDomain)) {
            $openDkimTxtCmd = 'opendkim-testkey -d :domain -s default -k /etc/opendkim/keys/:domain/default.private';
            $openDkimTxtCmd = strtr($openDkimTxtCmd, array(':domain' => $emailDomain->name));
            $openDkimTxtResult = $this->getSshConnection()->exec($openDkimTxtCmd);
            $openDkimTxtIsValid = empty($openDkimTxtResult);
        } else {
            $openDkimTxtDetail->addDetail('Unable to retrieve domain for emails.');
        }
        $openDkimTxtDetail->setIsValid($openDkimTxtIsValid);
        $openDkimTxtDetail->description = 'OpenDKIM TXT';
        array_push($installDetails, $openDkimTxtDetail);

        /**
         * Validate that web@domain mailbox exists
         */
        $webUserDetail = new InstallDetail;
        $webUserValid = false;
        if (!empty($emailDomain)) {
            $webUser = 'web@:domain';
            $webUser = strtr($webUser, array(':domain' => $emailDomain->name));
            $webUserValid = Yii::app()->imap->changeUser($webUser, Yii::app()->imap->password);
        } else {
            $webUserDetail->addDetail('Unable to retrieve domain for emails.');
        }
        $webUserDetail->setIsValid($webUserValid);
        $webUserDetail->description = "Mailbox '{$webUser}' Exists";
        array_push($installDetails, $webUserDetail);

        /**
         * Crontab Entries
         */
        $cronTabDetail =  new InstallDetail;
        $cronTabValid = false;
        if (!empty($primaryDomain)) {
            // Validate the nightly cron script
            $nightlyCronTabCmd = 'crontab -l | grep ":domain/protected/data/./nightly-cron-scripts.sh"';
            $nightlyCronTabCmd = strtr($nightlyCronTabCmd, array(':domain' => $primaryDomain->name));
            $nightlyCronTabConfigured = $this->getSshConnection()->exec($nightlyCronTabCmd);
            if (empty($nightlyCronTabConfigured)) {
                $cronTabDetail->addDetail('Nightly cronjob not configured.');
            }

            // Validate the email cron script
            $emailCronTabCmd = 'crontab -l | grep ":domain/protected/data/./email-queue-scripts.sh"';
            $emailCronTabCmd = strtr($emailCronTabCmd, array(':domain' => $primaryDomain->name));
            $emailCronTabConfigured = $this->getSshConnection()->exec($emailCronTabCmd);
            if (empty($emailCronTabConfigured)) {
                $cronTabDetail->addDetail('Email cronjob not configured.');
            }

            $cronTabValid = (!empty($nightlyCronTabConfigured) and !empty($emailCronTabConfigured));
        } else {
            $cronTabDetail->addDetail('Unable to retrieve primary domain.');
        }
        $cronTabDetail->setIsValid($cronTabValid);
        $cronTabDetail->description = 'Crontab Entries Exists';
        array_push($installDetails, $cronTabDetail);

        /**
         * Validate MLS Board
         */
        $mlsBoardDetail = new InstallDetail;
        $mlsBoardValid = false;
        if (Yii::app()->user->getAccount()->getBoard() instanceof MlsBoards) {
            $mlsBoardValid = true;
        } else {
            $mlsBoardDetail->addDetail('MLS board not found.');
        }
        $mlsBoardDetail->setIsValid($mlsBoardValid);
        $mlsBoardDetail->description = 'MLS Board Configured';
        array_push($installDetails, $mlsBoardDetail);

        /**
         * Validate settings
         */
        $settingsDetail = new InstallDetail;
        $settingsValid = false;
        $accountSettings = SettingAccountValues::model()->findAll();
        if (!empty($accountSettings)) {
            $settingsValid = true;
            foreach ($accountSettings as $accountSetting) {
                $settingsDetail->addDetail("{$accountSetting->setting->name}: {$accountSetting->value}");
            }
        }
        $settingsDetail->setIsValid($settingsValid);
        $settingsDetail->description = 'Account Settings Exists';
        array_push($installDetails, $settingsDetail);

        /**
         * Validate CMS Permissions
         */
        $cmsPermissionDetail = new InstallDetail;
        $cmsPermissionValid = false;
        if (!empty($primaryDomain)) {
            $cmsPermissionCmd = 'ls -Fla /var/www/stm_domains/:domain/images | grep "cms" | grep "drwxrwxrwx"';
            $cmsPermissionCmd = strtr($cmsPermissionCmd, array(':domain' => $primaryDomain->name));
            $cmsPermissionResult = $this->getSshConnection()->exec($cmsPermissionCmd);
            if (empty($cmsPermissionResult)) {
                $cmsPermissionDetail->addDetail('Cms permissions not configured.');
            } else {
                $cmsPermissionValid = true;
            }
        } else {
            $cmsPermissionDetail->addDetail('Unable to retrieve primary domain.');
        }
        $cmsPermissionDetail->setIsValid($cmsPermissionValid);
        $cmsPermissionDetail->description = 'CMS Permissions';
        array_push($installDetails, $cmsPermissionDetail);

        /**
         * Validate CDN Directory
         */
        $cdnDetail = new InstallDetail;
        $cdnValid = false;
        if (!empty($primaryDomain)) {
            $cdnCmd = 'ls -Fla /mnt/stm_images/:domain';
            $cdnCmd = strtr($cdnCmd, array(':domain' => $primaryDomain->name));
            $cdnResult = $this->getSshConnection()->exec($cdnCmd);
            if (false !== strpos($cdnResult, 'No such file or directory')) {
                $cdnDetail->addDetail('Could not locate cdn directory.');
            } else {
                $cdnValid = true;
            }
        } else {
            $cdnDetail->addDetail('Unable to retrieve primary domain.');
        }
        $cdnDetail->setIsValid($cdnValid);
        $cdnDetail->description = 'CDN Directory Exists';
        array_push($installDetails, $cdnDetail);

        /**
         * Render the install view
         */
        $this->getController()->render('install', array(
            'installDetails' => $installDetails,
        ));
    }

    /**
     * @param $authCode
     *
     * @return bool
     */
    private function verifyAuthCode($authCode) {

        return ($authCode === self::AUTH_CODE);
    }

    /**
     * @return mixed
     */
    private function getSshConnection() {
        if (empty($this->_sshConnection)) {
            $this->_sshConnection = Yii::app()->phpseclib->createSSH2('www.seizethemarket.com', 2974);
            $this->_sshConnection->login('root', '$#@!stmapp!@#$');
        }

        return $this->_sshConnection;
    }
}

/**
 * Class InstallDetail
 */
class InstallDetail {

    /**
     * @var string Description of detail
     */
    public $description;

    /**
     * @var bool Does the detail pass validation?
     */
    private $_isValid;

    /**
     * @var array Developer detail msgs
     */
    private $_details = array();

    public function setIsValid($bool) {
        $this->_isValid = $bool;
    }

    public function getIsValid() {
        return $this->_isValid;
    }

    public function getStatus() {
        return (true == $this->_isValid) ? 'OK' : 'Failed';
    }

    public function addDetail($detailMsg) {
        array_push($this->_details, $detailMsg);
    }

    public function getDetails() {
        return $this->_details;
    }
}
