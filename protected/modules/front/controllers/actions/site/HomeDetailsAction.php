<?php
Yii::import('front_widgets.PageViewTrackerWidget.PageViewTrackerWidget');

class HomeDetailsAction extends CAction {

	// Used to find a price point to find relating homes
	const SIMILAR_COUNT_MIN = 12;
	const SIMILAR_COUNT_MAX = 35;
    const MIN_PHOTO_COUNT = 5;
	const LOG_CATEGORY = 'leadgen';
	protected $_baseCriteria;
	protected $Property;

	/**
	 * Manages models
	 */
	public function run($id) {
		$this->_mortgageCalculatorScript();

//		$this->trackingScript();
		$this->controller->module->layout = 'column1';

        // check to see if mlsBoardId is a $_GET param
        $mlsBoardId = Yii::app()->request->getQuery('mlsBoardId');
        if($mlsBoardId && ($mlsBoard = MlsBoards::model()->findByPk($mlsBoardId))) {
            $tablePrefixedName = $mlsBoard->prefixedName;
        }
        else {
            $tablePrefixedName = Yii::app()->user->board->prefixedName;
        }

        //@todo: temp logging - remove when it's not needed - CLee 2016-04-24
        if($tablePrefixedName == '_properties') {
            Yii::log(__FILE__.'('.__LINE__.') MLS Table Prefix is invalid. Investigate immediately. $tablePrefixedName = '.$tablePrefixedName.' | Yii::app()->request->getQuery("mlsBoardId") = '. Yii::app()->request->getQuery("mlsBoardId"), CLogger::LEVEL_ERROR);
        }

		$this->Property = MlsProperties::model($tablePrefixedName);

        $this->Property = $this->Property->active()->findByPk($id); //isFavorite($id)-> @todo: test on production to see if saving a separate db call reduces load time. Inconclusive locally cuz network lag not exist.
        if(!empty($this->Property)) {
            $this->Property->getHomeDetails($id);
        }
		if(empty($this->Property)) {
			Yii::app()->user->setState('areaMessage', 'The property requested was not found. Please search from<br />the homes below or <a href="/search/full">click here</a> for a Full Home Search.');
			$this->controller->redirect('/area/all');
		}

		if($this->Property)
			$this->controller->pageTitle = $this->Property->formatMlsAddress($this->Property).' - Home Details';

        $calculateSimilarPropertiesFlag = false;

        // track & save homes viewed
        $pageViewTracker = new PageViewTrackerWidget;
        $pageViewTracker->save();

		if (Yii::app()->user->isGuest) {
			$viewsMaxSetting = Yii::app()->user->settings->views_max;
			// Need +1 to the count because page viewed gets counted AFTER this action is processed so it will always be 1 count behind.
			$homeDetailViews = PageViewTrackerWidget::countHomesViewed($unique = true);
			$viewsMaxReached = ($homeDetailViews + 1 > $viewsMaxSetting) ? true : false;

			// Temp CLee super tracking code :)
			if($viewsMaxReached) {
                // removing this for now until we figure out how to ignore when it is a bot. Way too many records.
                if(0) { //&& !YII_DEBUG
                    $FormViews = new FormViews;
                    $FormViews->home_detail_view_count = $homeDetailViews;
                    $FormViews->domain = str_replace(array('www.','http://'),'',$_SERVER['SERVER_NAME']);
                    $FormViews->page = $_SERVER['REQUEST_URI'];
                    $FormViews->url = str_replace(array('www.','http://'),'',$_SERVER['SERVER_NAME']).$_SERVER['REQUEST_URI'];
                    $FormViews->referrer = str_replace(array('www.','http://'),'',$_SERVER['HTTP_REFERER']);
                    $FormViews->utmz = $_COOKIE['__utmz'];
                    $FormViews->utma = $_COOKIE['__utma'];
                    $FormViews->utmb = $_COOKIE['__utmb'];
                    $FormViews->utmc = $_COOKIE['__utmc'];
                    $FormViews->added = date('Y-m-d H:i:s');
                    $FormViews->ip = Yii::app()->stmFunctions->getVisitorIp();
                    //					$browser = get_browser(null, true);
                    //					$browser = print_r($browser,true);
                    //					$FormViews->browser = $browser;
                    $FormViews->session_id = session_id();
                    if(!$FormViews->save()) {
                        $errorMsg = print_r($FormViews->getErrors(),true);
                        Yii::log(__FILE__.'('.__LINE__.') Error in saving Form Views. Error Message: '.$errorMsg, CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                    }
                }

				$message = date('Y-m-d | H:i:s');
				$message .= PHP_EOL.'Home Details Register Dialog Viewed';
				$referrer = str_replace(array('www.','http://'),'',$_SERVER['HTTP_REFERER']);
				$message .= PHP_EOL.'URL: => '.str_replace(array('www.','http://'),'',$_SERVER['SERVER_NAME']).$_SERVER['REQUEST_URI'];
				$message .= PHP_EOL.'Referrer: => '.$referrer;
				$message .= PHP_EOL.'UTMZ: =>'.$FormViews->utmz;
				$message .= PHP_EOL.'IP: => '.Yii::app()->stmFunctions->getVisitorIp();

				if(!YII_DEBUG) {
//					$mySelf = Contacts::model()->findByPk(1); // Contact model for myself
//					$myGateway = MobileGateways::model()->findByPk(4); // my cell carrier
//					$myGateway = $mySelf->settings->cell_number.$myGateway->sms_gateway;
//					@mail($myGateway, "", $message, "From: RegisterDialogViews@SeizetheMarket.com"); // temp until sms component is working
//					@mail("Admin@SeizetheMarket.com", "Register Dialog Viewed", $message, "From: RegisterDialogViews@SeizetheMarket.com");
				}
			} else {
                $calculateSimilarPropertiesFlag = true;
				//PageViewTrackerWidget::countHomesViewed($unique = true);
			}
		} else {
            $calculateSimilarPropertiesFlag = true;
        }

        // this reduces the database call to reduce load time. If register dialog is going to appear, user will never see similar properties so no need to calculate.
        $similarProperties = array();
		if($calculateSimilarPropertiesFlag) {
            $similarProperties = $this->getSimilarProperties($this->Property);
        }

        $this->controller->render('homeDetailsModern', array(
			'model' => $this->Property,
			'similarProperties' => $similarProperties,
            'mlsBoardId' => $mlsBoardId,
			'interiorFeatures' => $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::INTERIOR_FEATURE_ID),
			'exteriorFeatures' => $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::EXTERIOR_FEATURE_ID),
			'propertyFeatures' => $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::PROPERTY_FEATURE_ID),
			'schoolFeatures' => $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::SCHOOL_FEATURE_ID),
			'viewsMaxReached' => $viewsMaxReached,
			'isFavorite' => (Yii::app()->user->isGuest)? false: SavedHomes::isHomeSaved($id, Yii::app()->user->id), //@todo: test on production to see if saving a separate db call reduces load time.
			'photoContent' => $this->getCarouselPhotos(),
			'officePhone' => Yii::app()->getUser()->getSettings()->office_phone,
		));
	}

    public function getSimilarProperties(MlsProperties $mlsProperty) {

        $similarProperties = array();
        $boardPrefix = Yii::app()->user->board->prefixedName;
        $tableName = $boardPrefix . MlsProperties::getTableSuffix();
        $tableNamePhotos = $boardPrefix . MlsPropertiesPhoto::getTableSuffix();
        $stateShortName =  $mlsProperty->getActiveMlsBoard()->getStateShortName($lowercase=true);

        // DATA to pull out: string of listing_id, price , photo1 ... CJSON::decode ... calc count and decide which one to use
//        $select = <<<SQL
//          select a.listing_id, GROUP_CONCAT('{"price":','"', price,'","photo_count":"', photo_count,'","listing_id":"', a.listing_id, '","photoUrl:"', url,'","propertyUrl":"/home/$stateShortName/',LOWER(REPLACE(city,' ','-')),'/',LOWER(REPLACE(street_name,' ','-')),'/',a.listing_id,'"}') as data
//SQL;

        $baseQuery = "status='Active' AND mls_property_type_id={$mlsProperty->mls_property_type_id}";
        $limitGroup = " GROUP BY a.listing_id ORDER BY a.listing_id DESC, photo_number ASC LIMIT ".self::SIMILAR_COUNT_MAX;

        $priceMin1 = $mlsProperty->price - round($mlsProperty->price * MlsProperties::SIMILAR_PRICE_PERCENT);
        $priceMax1 = $mlsProperty->price + round($mlsProperty->price * MlsProperties::SIMILAR_PRICE_PERCENT);
        $basePriceCondition1 = "price >= '{$priceMin1}' AND price <= '{$priceMax1}'";

        $priceMin2 = $mlsProperty->price - round($mlsProperty->price * MlsProperties::SIMILAR_PRICE_PERCENT * 2);
        $priceMax2 = $mlsProperty->price + round($mlsProperty->price * MlsProperties::SIMILAR_PRICE_PERCENT * 2);
        $basePriceCondition2 = "price >= '{$priceMin2}' AND price <= '{$priceMax2}'";

        $priceMin3 = $mlsProperty->price - round($mlsProperty->price * MlsProperties::SIMILAR_PRICE_PERCENT * 4);
        $priceMax3 = $mlsProperty->price + round($mlsProperty->price * MlsProperties::SIMILAR_PRICE_PERCENT * 4);
        $basePriceCondition3 = "price >= '{$priceMin3}' AND price <= '{$priceMax3}'";

        // ---------------------------------- TIER CONDITIONS------------------------------------------
        $zip = substr($mlsProperty->zip,0,5);

        $tierCondition['t1'] = $baseQuery." AND ".$basePriceCondition1." AND zip like '{$zip}%'";
        $tierCondition['t2'] = $baseQuery." AND ".$basePriceCondition1." AND area like '".addslashes($mlsProperty->area)."'";
        $tierCondition['t3'] = $baseQuery." AND ".$basePriceCondition1." AND region like '".addslashes($mlsProperty->region)."'";
        $tierCondition['t4'] = $baseQuery." AND ".$basePriceCondition2." AND SUBSTRING(zip, 1,5) like '{$zip}'";
        $tierCondition['t5'] = $baseQuery." AND ".$basePriceCondition1." AND county like '".addslashes($mlsProperty->county)."'";

        // for homes under 100k
        $tierCondition['t6'] = $baseQuery." AND ".$basePriceCondition2." AND county like '".addslashes($mlsProperty->county)."'";
        //for homes under 100k - assumes it has bedrooms/baths/SF
        if($mlsProperty->bedrooms && $mlsProperty->sq_feet) {
            $tierCondition['t7'] = $baseQuery." AND ".$basePriceCondition1." AND bedrooms >=". $mlsProperty->bedrooms. " AND sq_feet >= ".$mlsProperty->sq_feet." <= ".($mlsProperty->sq_feet * 1.25);
        }
        //luxury tiers
        $tierCondition['l1'] = $baseQuery." AND ".$basePriceCondition2." AND zip like '{$zip}%'";
        $tierCondition['l2'] = $baseQuery." AND ".$basePriceCondition2." AND area like '".addslashes($mlsProperty->area)."'";
        $tierCondition['l3'] = $baseQuery." AND ".$basePriceCondition2." AND county like '".addslashes($mlsProperty->county)."'";
        $tierCondition['l4'] = $baseQuery." AND ".$basePriceCondition3." AND county like '".addslashes($mlsProperty->county)."'";
        //@todo: later include bedroom/bath criteria for propertyType 1 and 2??

        $tierConditionsToApply = array();
        // handles different prices ranges differently
        switch(1) {
            case ($mlsProperty->price < 100000):
                $tierConditionsToApply['t4'] =  $tierCondition['t4'];
                $tierConditionsToApply['t5'] =  $tierCondition['t5'];
                $tierConditionsToApply['t6'] =  $tierCondition['t6'];
                $tierConditionsToApply['l4'] =  $tierCondition['l4'];
                $tierConditionsToApply['t6'] =  $tierCondition['t6'];
                if($mlsProperty->bedrooms && $mlsProperty->sq_feet) {
                    $tierConditionsToApply['t7'] =  $tierCondition['t7'];
                }

                break;
            case ($mlsProperty->price < 1500000):
                $tierConditionsToApply['t1'] =  $tierCondition['t1'];
                $tierConditionsToApply['t2'] =  $tierCondition['t2'];
                if(empty($mlsProperty->region)) {
                    $tierConditionsToApply['t6'] =  $tierCondition['t6'];
                } else {
                    $tierConditionsToApply['t3'] =  $tierCondition['t3'];
                }

                $tierConditionsToApply['t4'] =  $tierCondition['t4'];
                $tierConditionsToApply['t5'] =  $tierCondition['t5'];
                break;
            case ($mlsProperty->price >= 1500000 && $mlsProperty->price < 7000000):
                $tierConditionsToApply['l1'] =  $tierCondition['l1'];
                $tierConditionsToApply['l2'] =  $tierCondition['l2'];
                $tierConditionsToApply['l3'] =  $tierCondition['l3'];
                $tierConditionsToApply['l4'] =  $tierCondition['l4'];
                break;
            case ($mlsProperty->price >= 7000000):
                $tierConditionsToApply['l1'] =  $tierCondition['l1'];
                $tierConditionsToApply['l2'] =  $tierCondition['l2'];
                $tierConditionsToApply['l3'] =  $tierCondition['l3'];
                $tierConditionsToApply['l4'] =  $tierCondition['l4'];
                break;
            default:
                Yii::log(__FILE__.'('.__LINE__.') Similar Properties unexpected condition. Please investigate. MLS Property Price is: '.$mlsProperty->price, CLogger::LEVEL_ERROR);
                break;
        }

        $tierQuery = array();
        $tierSelect = '';
        foreach($tierConditionsToApply as $key => $value) {
            $tierQuery[$key] = "select count(*) from {$tableName} WHERE ".$baseQuery." AND ".$value;
            $tierSelect .= "(".$tierQuery[$key].") as {$key},";
        }

        $tierSelect = substr($tierSelect, 0, -1); //remove the comma a the end
        $countQuery = "select $tierSelect";

        //@todo: hotfix - temp zip alias needs work ... TOTAL TEMP FIX
        $countQuery = str_replace(' zip', " {$tableName}.zip", $countQuery);

        $countSimilarProperties = $mlsProperty->getDbConnection()->createCommand($countQuery)->queryRow();

        $tierMatch = null;
        foreach($countSimilarProperties as $tierNumber => $tierCount) {
            if($tierCount >= self::SIMILAR_COUNT_MIN) {
                $tierMatch = $tierNumber;
                break;
            }
        }
        if(empty($tierMatch)) {
            $tierMatch = array_keys($countSimilarProperties, max($countSimilarProperties));
            if(is_array($tierMatch)) { //this is if there was a tie in max, it will return an array of keys instead of one. Choose the first one
                $tierMatch = $tierMatch[0];
            }
        }
        if($countSimilarProperties[$tierMatch] < self::SIMILAR_COUNT_MIN) {
            if($mlsProperty->price > 100000 && $mlsProperty->price < 1500000) {
                $tierCondition[$tierMatch] = $tierCondition['l3'];
            }
        }

        if(Yii::app()->user->board->has_photo_url) {
            $photoSelect = '(select url from '.$boardPrefix.'_property_photos where listing_id=a.listing_id AND photo_number=1 LIMIT 1)';
        } else {
            $domain = Yii::app()->user->domain->name;
            $photoSelect = 'CONCAT("http://mlsimages.seizethemarket.com/'.$boardPrefix.'/",mls_property_type_id,"/",listing_id,"/",listing_id,"-1.jpg")';
        }

        $select = <<<SQL
          SELECT price, bedrooms, baths_total, photo_count, a.listing_id, $photoSelect as url, a.street_name, a.city, CONCAT("/home/$stateShortName/", LOWER(REPLACE(TRIM(a.city),' ','-')), "/", LOWER(REPLACE(CONCAT(TRIM(a.street_number)," ",TRIM(a.street_name), " ", TRIM(a.street_suffix)),' ','-')), "/", a.listing_id) as propertyUrl
SQL;

        // if mls board has thumbnail disclosure assume you neee office info
        if($mlsDisclosureThumbnail = $this->Property->getActiveMlsBoard()->website_disclosure_thumbnail) {
            $select .= ', o.name  listingOfficeName';
            $join = ' LEFT JOIN  '.$boardPrefix.'_offices o ON o.office_id=a.listing_office_id';
        }

        //NOTE: had to add a. alias as it was ambiguous with office table zip but did not require a. above for the previous query. Hacky but best solution for now.
        $similarPropertyQuery = $select.' FROM '.$tableName.' a ' . $join . ' WHERE '.str_replace('zip', 'a.zip', $baseQuery).' AND '.str_replace('zip', 'a.zip', $tierCondition[$tierMatch]). ' LIMIT '. self::SIMILAR_COUNT_MAX;; //$limitGroup
        $similarPropertiesResults = $mlsProperty->getDbConnection()->createCommand($similarPropertyQuery)->queryAll();

        if(count($similarPropertiesResults) < self::SIMILAR_COUNT_MIN) {
            //comment out as needed
//            Yii::log(__FILE__.'('.__LINE__.') ERROR: Similar Properties did not find enough properties ('.$countSimilarProperties[$tierMatch].' found, Tier Match: '.$tierMatch.'). Please investigate this situation. MlsProperties attributes: '.print_r($mlsProperty->attributes, true), CLogger::LEVEL_ERROR);
        }

        // carousel data formatting
        $similarPropertiesCarouselData = array();
        foreach($similarPropertiesResults as $key => $similarProperty) {
            $carouselFormat = array();
            $carouselFormat['photoUrl'] = $similarProperty['url'];
            $carouselFormat['price'] = $similarProperty['price'];
            $carouselFormat['bedrooms'] = $similarProperty['bedrooms'];
            $carouselFormat['baths_total'] = $similarProperty['baths_total'];
            $carouselFormat['listing_id'] = $similarProperty['listing_id'];
            $carouselFormat['propertyUrl'] = $similarProperty['propertyUrl'];
            $carouselFormat['photo_count'] = $similarProperty['photo_count'];
            $carouselFormat['listingOfficeName'] = $similarProperty['listingOfficeName'];

            array_push($similarPropertiesCarouselData, $carouselFormat);
        }

        return $similarPropertiesCarouselData;
    }

    public function getCarouselPhotos() {

        $photoContent = '';
        $mlsBoard = $this->Property->getActiveMlsBoard();
        $tablePrefix = $mlsBoard->prefixedName;
        $photoUrls = array();

        if(!Yii::app()->user->board->has_photo_url) {

            $boardPrefix = $mlsBoard->getPrefixedName();
            for($i=0; $i < $this->Property->photo_count; $i++) {

                //NOTE: Condition for specific board that has duplicate photos that hasn't been resolved for 2 years
                if($mlsBoard->id == 5 && $i===0) {
                    continue;
                }
                $photoUrls[$i] = 'http://mlsimages.seizethemarket.com/'.$boardPrefix.'/'.$this->Property->mls_property_type_id.'/'.$this->Property->listing_id.'/'.$this->Property->listing_id.'-'.($i+1).'.jpg';
            }
        } else {
            $listingPhotoTableName = $tablePrefix . MlsPropertiesPhoto::getTableSuffix();
            $query = "select url from ".$listingPhotoTableName." where listing_id='{$this->Property['listing_id']}' ORDER BY photo_number ASC";
            $photoUrls = Yii::app()->stm_mls->createCommand("$query")->queryColumn();
        }

        $photoContent['thumbnails'] = CJSON::encode($photoUrls);

        foreach($photoUrls as $imageUrl) {
            $photoContent['fullSize'] .= '<div class="slide"><img src="'.$imageUrl.'" width="370" height="265" /></div>';
			$photoContent['fullSizeUrl'][] = $imageUrl;
        }

		return $photoContent;
	}


	public function trackingScript() {
		$isYiiDebug = (YII_DEBUG) ? 1 : 0;
		$js = <<<JS
		$(function() {
			if (!$isYiiDebug) {
				// Google Event Tracking: categories, actions, label
//				_gaq.push(["_trackEvent", "Page", "View", "Home Details Page View"]);
//				_gaq.push(["b._trackEvent", "Page", "View", "Home Details Page View"]);
			}
		});
JS;
        if(!empty(Yii::app()->user->settings->google_tracking_code)) {
            // Positioned this at end of body so it can come in after the accounts have been set properly
            Yii::app()->clientScript->registerScript('homeDetailsTracking', $js, CClientScript::POS_END);
        }
	}

    protected function _mortgageCalculatorScript()
    {
        $js = <<<JS
		$('#mortgage-calcualtor-fields input, #mortgage-calcualtor-fields select').on('change',function() {
			var percent = $('#down-payment').val();
				percent = percent.replace('%','');
				percent = (1-(percent /100));

				var loanAmount = $('#price').val();
				loanAmount = loanAmount.replace('$','');
				loanAmount = loanAmount.replace(/,/g,'');
				loanAmount = loanAmount * percent;
				var interest = $('#interest').val();
				interest = interest.replace('%','');
				interest = interest /100/12;
				var months = $('#years').val();
				var payment = Math.round((loanAmount*interest)/(1-Math.pow(1+interest,(-1*months)))*100)/100;
				$('#monthly-payment').html('$'+payment.toFixed(2));
				var price = $("#price").val();
				var downPayment = price.toFixed(2) * percent;
				$("#down-payment-amount").html("$"+downPayment.toFixed(2));
		});
JS;
        Yii::app()->clientScript->registerScript('mortgageCalculatorScript', $js);
    }
}
