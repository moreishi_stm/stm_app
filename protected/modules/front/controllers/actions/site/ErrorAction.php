<?
class ErrorAction extends CAction
{
	/**
	 * Handle errors thrown from the Front module
	 * @return null
	 */
	public function run()
	{
		if ($error = Yii::app()->errorHandler->error) {

			/** Use a different view if we are maintaining the site */
			if (YII_MAINTENANCE) {
				$view = "maintenance";
			} else {
				$view = "error";
			}

			$this->controller->render($view, array('error'=>$error));
		}
	}
}
