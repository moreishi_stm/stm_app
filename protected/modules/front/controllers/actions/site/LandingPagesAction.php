<?php
	class LandingPagesAction extends CAction {

		/**
		 * Manages models
		 *
		 * @param $pageUrl Comes from request
		 */
		public function run($url) {
			$this->controller->layout = 'houseValues';
			$CmsContentModel = CmsContents::model()->byUrl($url)->find();

			// If we cannot find the specified content throw a 404.
			if (!$CmsContentModel) {
				throw new CHttpException(404, 'Could not locate content.');
			}

			$Template = new CmsTemplates($CmsContentModel->template_name, $CmsContentModel->type_ma, $CmsContentModel->id);
			$this->controller->pageTitle = $CmsContentModel->title;

            $this->controller->render('page', array(
                    'cmsUrl' => $url,
					'pageContent' => $Template->content,
                    'Template' => $Template,
                    'hideCmsTags'=>true,
                    'FormFields' => new FormFields,
				)
			);
		}
	}