<?php

	class SearchAction extends CAction {

		public function run($id='full') {

			$this->controller->layout = 'search';

			$boardName = Yii::app()->getUser()->getBoard()->getPrefixedName();
			$property = new MlsProperties($boardName, 'search');
            $secondaryProperty = new MlsPropertiesSecondary($boardName, 'search');

            if (!empty($_POST['MlsProperties']) or !empty($_POST['MlsPropertiesSecondary'])) {

                $property->attributes = $_POST['MlsProperties'];
                $secondaryProperty->attributes = $_POST['MlsPropertiesSecondary'];

                $safeAttributes = $property->getAttributes($property->getSafeAttributeNames());
                $safeAttributes = CMap::mergeArray($secondaryProperty->getAttributes($secondaryProperty->getSafeAttributeNames()), $safeAttributes);

                $queryParams = array();
                foreach ($safeAttributes as $attribute => $value) {
                    if (!empty($value)) {
                        $queryParams[$attribute] = $value;
                    }
                }

                // NOTE: total hack, but since we are moving over to zend soon, no need to do major refactors
                if($property->property_type) {
                    $queryParams['property_type'] = $property->property_type;
                }

                $this->getController()->redirect(Yii::app()->createUrl('/front/site/homes', $queryParams));
            }

            $searchView = 'search'.ucwords($id);
            Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js');
            $this->getController()->render($searchView, array(
                                                            'property' => $property,
                                                            'secondaryProperty' => $secondaryProperty,
                                                        ));
		}

	}