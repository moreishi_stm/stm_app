<?php
class BlogTagAction extends CAction
{
    /**
     * Manages models
     *
     * @param $pageUrl Comes from request
     */
    public function run($tagUrl) {
        $model = CmsTags::model()->byUrlOrName(strtolower($tagUrl))->find();

        $this->getController()->pageTitle = ($model)? $model->name : 'Topic Not Found. Please try another topic.';

        $blogDataProvider = new CActiveDataProvider('CmsContents', array(
            'criteria' => array(
                'condition' => 'cmsTags.url=:cmsTagUrl AND (type_ma=:type_ma OR type_ma=:type_ma2)',
                'params' => array(':cmsTagUrl'=>$model->url,':type_ma'=> CmsContents::TYPE_PAGE,':type_ma2'=>CmsContents::TYPE_BLOG),
                'order' => 'published DESC',
                'with'=>'cmsTags',
                'together'=>true,
            ),
        ));

        // If we cannot find the specified content throw a 404.
        if (empty($blogDataProvider)) {
            throw new CHttpException(404, 'Could not locate blog entries.');
        }


        $this->controller->render(
            'blog', array('model'=>$model,'dataProvider' => $blogDataProvider)
        );
    }
}