<?
class LogoutAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$model = new LoginForm;

		$this->controller->layout = 'column1';
		Yii::app()->user->logout(false);
		// Yii::app()->user->setFlash('success', 'You have successfully Logged Out.');
		$this->controller->render('logout', array('model'=>$model));
		// $this->redirect('/');
	}
}