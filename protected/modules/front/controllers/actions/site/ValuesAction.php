<?php

class ValuesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
        $this->controller->layout = 'column1';
        $this->controller->fullWidth = true; //applicable for standard2

        $FormFields       = new FormFields;
        $formId           = Forms::FORM_HOUSE_VALUES;
        $SubmissionValues = new FormSubmissionValues($formId);
        $FormFields       = new FormFields;
        $formAction       = '/front/forms/houseValues/formId/'.$formId;

        $this->registerCssScript();

        $formId = Forms::FORM_HOUSE_VALUES;
        if(isset($_GET['address']) && isset($_GET['city']) && isset($_GET['state']) && isset($_GET['zip'])) {
            if($_GET['status']==2) {
                $formPart = '2_details';
                $view = '_valuesStep2';
            } elseif($_GET['status']=='success') {
                $formPart = 'success';
                $view = '_valuesStep3Complete';
//                $this->_sendMarketSnapshotEmail();
            }
        } else {
            $formPart = 1;
            $view = '_valuesStep1';
        }

        $formName = $formId.'_Part'.$formPart;

        $SubmissionValues = new FormSubmissionValues($formName);
        $SubmissionValues->formPart = $formPart;

        if(isset($_GET['address']) && isset($_GET['city']) && isset($_GET['state']) && isset($_GET['zip'])  && ($_GET['status']=='success' || $_GET['status']==2)) {
            $SubmissionValues->data = array(FormFields::ADDRESS_ID=>ucwords(strtolower($_GET['address'])), FormFields::CITY_ID=>ucwords(strtolower($_GET['city'])), FormFields::STATE_ID=>$_GET['state'], FormFields::ZIP_ID=>$_GET['zip']);
        }

        $this->controller->render('values', array(
                'SubmissionValues' =>$SubmissionValues,
                'formAction' => $formAction,
                'FormFields' =>$FormFields,
                'view' => $view,
            ));
	}

    public function registerCssScript()
    {
        $css = <<<CSS
            body {
                margin:0;
                font: normal 85%/160% arial,helvetica,sans-serif;
            }
            h1.title {
                font-size: 30px !important;
                margin: 20px 0;
            }
            .top-area {
                height: 400px;
            }
            .form-box {
                position: relative;
                top: 10px;
                margin-left: auto;
                margin-right: auto;
                width: 900px;
                background: #eee;
                padding: 20px 20px 20px 30px;
                border-radius:5px;
                -moz-border-radius:5px;
                -webkit-border-radius:5px;
            }
            .form-box:before {
                width: 950px;
                float: left;
            }
            .video{
                display: inline-block;
                margin-right: 20px;
            }
            .form-box .form-container{
                width: 100%;
                position: relative;
            }
            .form-box #houseValues-form .right.first h1{
                margin-top: 2px;
                text-align: left;
                font-size: 35px;
            }
            .form-box form .left.verify h1{
                text-align: left;
                margin-top: 0;
                padding-top: 15px;
                font-size: 25px !important;
            }
            .form-box h2{
                font-weight: normal;
                margin-bottom: 25px;
            }
            .form-box .verify h2{
                font-weight: bold;
                margin-bottom: 5px;
            }
            .form-box .verify input[type=input]{
                margin-bottom: 25px;
            }
            .form-box .right.first h2{
                margin-top: 2px;
                margin-bottom: 0px;
            }
            .form-box .right.first h3{
                margin-top: 2px;
                font-style: italic;
            }
            .form-box .bottom-map{
                width: 100%;
            }
            .form-box .bottom-map .map {
                height: 100%;
                width: 100%;
            }
            .form-box .row{
                margin-bottom: 10px;
            }
            .form-box .center{
                text-align: center;
            }
            .form-box .right{
                display: inline-block;
            }
            .form-box .left{
            }
            .form-box .left.verify,.form-box .left.complete{
                width: 490px;
                min-height: 370px;
                display: inline-block;
            }
            .form-box .right.verify{
                display: inline-block;
                float:right;
            }
            .form-box .right.first{
                width: 490px;
                margin-right: 200px;
                display: inline-block;
                position: absolute;
                top: 0;
                right: 0;
                float: right;
            }
            .form-box input {
                font-size: 30px;
                border: 1px solid #999;
                -moz-border-radius: 4px;
                -webkit-border-radius: 4px;
                border-radius: 4px;
                padding: 6px;
                font-family: Arial;
            }
            .form-box select {
                height: 36px;
                margin-top: 1px;
                font-size: 20px;
            }
            .form-box form input.button {
                color: white;
                text-shadow: 0 1px 0 rgba(0,0,0,1);
                background: -webkit-gradient(linear, left top, left bottom, from(#FF8875), to(#FA0000));
                background: -moz-linear-gradient(top, #FF8875, #FA0000);
                background: -o-linear-gradient(top, #FF8875, #FA0000);
                border-color: #cca992 #a78100 #a26a26 #ccaa76;
                height:45px;
                top:0px;
                font-size:16px !important;
                margin-right: 20px;
                width: 95%;
            }
            .form-box form .address {
                width:460px;
            }
            .form-box form .city {
                width:230px;
            }
            .form-box form .state {
                width:75px;
                position: relative;
            }
            .form-box form .zip {
                width:136px;
            }
            .form-box .verify .description {
                font-size: 16px;
                margin-top: 15px;
                font-style: italic;
                margin-bottom: 10px;
                display: block;
            }
            .form-box .verify input, .form-box .complete input{
                font-size: 17px;
                width: 340px;
            }
            .form-box .verify input.name{
                width: 161px;
            }
            .form-box .success{
                font-size: 26px;
                font-weight: bold;
                margin-bottom:20px;
            }
            .form-box .success em{
                background: url(http://cdn.seizethemarket.com/assets/images/success_large.png);
                width: 42px;
                height: 42px;
                display: inline-block;
                margin: 0 10px -10px 0;
            }
            .form-box .sent{
                padding-top:20px;
                font-size: 20px;
                font-weight: bold;
                font-style: italic;
            }
            .form-box .complete .address{
                margin-top: 30px;
                margin-bottom:10px;
                margin-left:10px;
                font-size:20px;
            }
            .form-box .share{
                font-size:18px;
                margin-top: 35px;
            }
            .map {
                height: 370px;
                width: 400px;
            }
            .map.success {
                height: 370px;
                width: 875px;
            }
            .bonus-area {
                /*width: 900px;*/
                width: 98%;
                height: 350px;
                margin-left: auto;
                margin-right: auto;
                padding-left: 20px;
            }

            .bonus-area ul{
                padding: 0;
                min-height: 250px;
            }
            .bonus-area ul li{
                display: inline-block;
                width: 260px;
                height: 235px;
                padding-right: 50px;
                float:left;
                list-style: none;
            }
            .bonus-area ul li:last-child{
                padding-right: 0;
            }
            .bonus-area h3 {
                font-size: 24px;
                margin-bottom: 8px;
                line-height: 1.2;
            }
            .bonus-area .tagline {
                color: #D20000;
                font-weight: bold;
                font-size: 14px;
                margin-top: 15px;
                bottom:0;
            }
            .bonus-area .forwardLink{
                font-size: 34px;
                color: black;
                position: relative;
                top: 100px;
                display: block;
                text-align: center;
            }
            .bonus-area .forwardMessage{
                font-size: 12px;
                color: #888;
                top: 120px;
                position: relative;
                text-align: center;
            }
            .footer {
                font-size: 11px;
                color: #888;
                margin-top: 25px;
                left: 0;
                text-align: center;
                width: 100%;
                clear: both;
                background-color: #e4e4df;
            }
            .footer .asSeenOn {
                font-family: "Times New Roman";
                color: #555;
                font-size: 16px;
                margin-right: 10px;
            }
            .footer .asSeenOn label{
                font-style: italic;
                top: -9px;
                position: relative;
                margin-right: 6px;
            }
            .footer .asSeenOn em{
                display: inline-block;
                height: 37px;
                width: 770px;
                background: url('../images/imgSprites.png') 0 0;
                margin-top: 20px;
            }
            .footer .credits {
                margin-top: 15px;
            }
            div.inline {
                display: inline-block;
                vertical-align: top;
            }
            .error select {
                color: #000;
                background: none repeat scroll 0% 0% #FFB8B8;
                border: 1px solid #FF5757;
            }
CSS;
        Yii::app()->clientScript->registerCss('siteHouseValuesPageCss', $css);
    }
}