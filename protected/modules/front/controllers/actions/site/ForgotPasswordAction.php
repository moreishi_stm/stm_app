<?php

	class ForgotPasswordAction extends CAction {

		public function run() {
			$this->controller->layout = 'column1';
			$model = new Emails('forgotPassword');

			if ($_POST['Emails']) {
				$isAjax = Yii::app()->request->getPost('isAjax');
				if($isAjax){
					$dataErrors = array();
				}
				$passwordReset = "error";
				$model->attributes = $_POST['Emails'];

				if ($model->validate()) {
					if ($contact = Contacts::findByEmail($model->email)) {

						// Send the Forgot Password Email
						$signature = SettingAccountValues::model()->getValue('email_signature');

                        if(!$contact->password) {
                            $contact->password = Yii::app()->passwordGenerator->generate();
                            if(!$contact->save()) {
                                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Contact did not save password: "' . print_r($contact->getErrors(), true) . '" '.PHP_EOL.'Contact Data:' . print_r($contact->attributes, true), CLogger::LEVEL_ERROR);
                            }
                        }

                        $domainName = str_replace('www.','',$_SERVER['SERVER_NAME']);
                        $viewPath = YiiBase::getPathOfAlias('admin_module.views.emails.forgotPasswordEmail').'.php';
                        $emailBody = Yii::app()->controller->renderFile($viewPath, array(
								'contact' => $contact,
								'email' => $model->email,
                                'signature' => $signature->value,
                                'domainName' => $domainName,
                            ), $returnContent=true);

                        $from = array('do-not-reply@seizethemarket.net'=>'Notification');
                        $stmZendMail = new StmZendMail();
                        $stmZendMail->checkBounceUndeliverable = false;
                        $stmZendMail->easyMail($from, $model->email, $subject='Account Password Reminder', $emailBody);

						// Record in activity log, the Password reset
						$ActivityLog = new ActivityLog;
						$ActivityLog->account_id = Yii::app()->user->accountId;
						$ActivityLog->activity_date = date('Y-m-d H:i:s');
						$ActivityLog->note = 'Forgot Password Reset request received and sent.';
						$ActivityLog->component_type_id = 1;
						$ActivityLog->component_id = $contact->id;
						$ActivityLog->task_type_id = TaskTypes::PASSWORD_RESET;
						$ActivityLog->added = date('Y-m-d H:i:s');
						$ActivityLog->added_by = 0; // System

						if(!$ActivityLog->save()) {
							$errorMessage = "There was an error with sending your Password Reminder. Please call us directly for immediate support.";
							if(!$isAjax){
								Yii::app()->user->setFlash('error', $errorMessage);

							}else{
								$dataErrors[] = $errorMessage;
							}

                        } else {
							$passwordReset = "success";
							$message = "Successfully sent password reminder to your Email.";
							if(!$isAjax){
								Yii::app()->user->setFlash('success', $message);
								$LoginForm = new LoginForm;
								$this->controller->render('forgotPasswordSuccess', array('model' => $LoginForm));
								Yii::app()->end();
							}else{
								$officePhone = Yii::app()->getUser()->getSettings()->office_phone;
								$message = <<<HTML
        <h1 class="green" style="padding-bottom: 25px;"><em class="icon_large i_stm_success_large"></em>Successful Password Recovery!</h1>
        <h4>Your password has been sent to your email address.</h4>
        <h4>For immediate assistance, call {$officePhone}.</h4>
HTML;

							}


                        }
					}
				}else{
					$dataErrors[] = current($model->getErrors())[0];//"There was an error finding your email. Please call us directly for immediate support.";
				}
			}
			if(!$isAjax){
				//@todo: temp for storyboard CSS - this will not be necessary once we get over to bootstrap 100%
				if(Yii::app()->params['layoutDirectory'] == 'storyboard') {
					$this->registerStoryboardCss();
				}

				$this->controller->render('forgotPassword', array(
						'model' => $model
					)
				);
			}
			else{
				$dataResponse = array("status"=>200,"passWordReset"=>$passwordReset);
				if(!empty($dataErrors)){
					$dataResponse["hasError"] = 1;
					$dataResponse["errors"] = $dataErrors;
				}
				if(isset($message) && !empty($message)){
					$dataResponse["hasMessage"] = 1;
					$dataResponse["messages"][] = $message;
				}
				header('Content-type: application/json');

				echo CJSON::encode($dataResponse);
				return;
			}

		}

        protected function registerStoryboardCss() {
            $css = <<<CSS
    #login {
        margin-top: 80px;
    }
    #login #forgot-password-container {
        margin: 40px auto 60px;
        background: none repeat scroll 0% 0% #F5F5F5;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
        border: 1px solid #DDD;
        border-radius: 6px;
        padding: 30px 30px 60px;
        width: 500px;
    }
    #login h1, h1.green {
        font-size: 20px;
        color: #5BA71B;
        text-align: center;
        line-height: 1;
    }
    #login h2 {
        font-size: 22px;
        text-align: center;
        font-weight: bold;
    }
    #login h4 {
        text-align: center;
    }
    #login label {
        width: 145px;
        display: inline-block;
        text-align: right;
        font-weight: bold;
    }
    #login form input{
        width: 300px;
        font-size: 20px;
        margin-bottom: 10px;
        padding: 5px 2px 5px 4px;
        height: 100%;
        border: 1px solid #BBB;
        padding: 4px 2px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    input, textarea, input:invalid, input:required, textarea:required, select {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
    }

    input:hover, input:focus, textarea:hover, textarea:focus, select:hover, select:focus {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    }
    #login .row.submit {
        height: 25px;
        text-align: left;
        position: relative;
        left: 145px;
    }
    #forgotpassword-form-button, #login-form-button {
        font-size: 11px;

        background: #fceec7;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjZWVjNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjOWEzMzkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
        background: -moz-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fceec7), color-stop(100%, #c9a339));
        background: -webkit-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -o-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -ms-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: linear-gradient(to bottom, #fceec7 0%, #c9a339 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fceec7', endColorstr='#c9a339', GradientType=0);
        -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#fceec7', endColorstr='#c9a339')"; /* IE8 */
        border: 1px solid;
        border-color: #CC9F70 #B36310 #B36310 #c68f56;
        cursor: pointer;

        color: #333;
        text-transform: uppercase;
        padding: 10px 28px;
        -webkit-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);

        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;

        position: relative;
        display: inline-block;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        text-decoration: none !important;
        font-weight: 700;
        outline: 0 none;
        text-align: center;
        margin: 2px;
        min-height: 14px;
        min-width: 8px;
        white-space: pre-line;
        vertical-align: baseline;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.7);
    }
    div.errorMessage {
        width: 300px;
        margin-left: 150px;
        margin-bottom: 10px;
        clear: both;
        color: #FF5757;
        font-weight: bold;
        font-size: 11px;
    }
    input.error {
        background: none repeat scroll 0% 0% #FFB8B8;
    }
CSS;
            Yii::app()->clientScript->registerCss('forgotPasswordStoryboardCss', $css);
        }
	}