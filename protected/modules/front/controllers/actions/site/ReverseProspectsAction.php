<?php
class ReverseProspectsAction extends CAction
{
	/**
	 * Manages models
	 */
    public function run() {
        $this->performAjaxRequest();
    }

    protected function performAjaxRequest() {
        if (Yii::app()->request->isAjaxRequest) {
            $tableName = Yii::app()->user->board->prefixedName;
            $model = MlsProperties::model($tableName);

            Yii::app()->end();
        }
    }
}
