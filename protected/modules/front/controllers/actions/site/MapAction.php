<?php
class MapAction extends CAction {
	protected $Property;

	/**
	 * Manages models
	 */
	public function run($id) {

		$this->controller->renderPartial('homeDetails/_map', array(
			'mapUrl' => $this->getMapUrl($id),
		));
	}

	/**
	 * getMapUrl
	 * @return string url for the map, either google or bing
	 */
	public function getMapUrl($id) {

		$tableName = Yii::app()->user->board->prefixedName;
		$this->Property = MlsProperties::model($tableName);
        $mapUrl = null;
        $Property = $this->Property->findByPk($id);
		if($Property) {
            // create mapUrl
            $geoAddress = $this->Property->formatMlsAddress($Property, array('geo' => true));
            $geoUrl = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$geoAddress.'&sensor=false';
            //get Geo Data for property from Google API
            $curl = curl_init($geoUrl);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $geoData = CJSON::decode(curl_exec($curl));
            $lat = (isset($geoData['results'][0]['geometry']['location']['lat']))? $geoData['results'][0]['geometry']['location']['lat'] :'';
            $lng = (isset($geoData['results'][0]['geometry']['location']['lng']))? $geoData['results'][0]['geometry']['location']['lng'] :'';
            // // $mapUrl = "http://dev.virtualearth.net/embeddedMap/v1/ajax/Birdseye?zoomLevel=10&center=".$lat_lng."&pushpins=".$lat_lng;
            // $mapUrl = "http://dev.virtualearth.net/embeddedMap/v1/ajax/Birdseye?zoomLevel=10&center=".$lat_lng."&pushpins=".$lat_lng;
            // $mapUrl = 'http://www.bing.com/maps/default.aspx?where1='.$geoAddress.'&sty=a';
            $mapUrl = 'http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q='.$geoAddress.'&amp;aq=&amp;sll='.$lat.','.$lng.'&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear='.$geoAddress.'&amp;z=17&amp;ll='.$lat.','.$lng.'&amp;output=embed';

            //		$temp = 'https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=1235+Terrace+Oaks+Ln,+Jacksonville,+FL+32223&amp;aq=&amp;sll=30.144531,-81.671584&amp;sspn=0.007107,0.013078&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=1235+Terrace+Oaks+Ln,+Jacksonville,+Florida+32223&amp;z=18&amp;ll=30.144531,-81.671584&amp;output=embed';

        } else {
            //@todo: how to handle?
        }
        return $mapUrl;
    }
}
