<?php
	class StoryboardListAction extends CAction {

//		public $assetDir;
//		public $_cssAssetsUrl;

		public function run() {
            $this->controller->pageTitle = 'Listing Storyboards';
			$this->controller->layout = 'column1';

            $CmsContentModel=new CmsContents;
            $CmsContentModel->unsetAttributes();

            $CmsContentModel->status_ma = CmsContents::STATUS_ACTIVE;
            $CmsContentModel->type_ma = CmsContents::TYPE_LISTING_SB;
//            $CmsContentModel->domain_id = Yii::app()->user->domain->id;
            $CmsContentModel->accountId = Yii::app()->user->accountId;

			$this->controller->render('storyboardList', array(
                    'DataProvider'=>$CmsContentModel->search(),
				)
			);
		}
	}