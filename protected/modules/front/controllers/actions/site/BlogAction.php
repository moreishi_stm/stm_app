<?php
class BlogAction extends CAction
{

    public $templateType;

    public $assetDir;

    public $_cssAssetsUrl;

    /**
     * Manages models
     *
     * @param $pageUrl Comes from request
     */
    public function run()
    {

        $blogDataProvider = new CActiveDataProvider('CmsContents', array(
            'criteria' => array(
                'condition' => 'type_ma = :type AND status_ma=:status',
                'params' => array(':type' => CmsContents::TYPE_BLOG, ':status'=>CmsContents::STATUS_ACTIVE),
                'order' => 'id DESC'
            ),
        ));

        // If we cannot find the specified content throw a 404.
        if (empty($blogDataProvider)) {
            throw new CHttpException(404, 'Could not locate blog entries.');
        }

        $this->getController()->pageTitle = 'All Topics';

        $this->controller->render(
            'blog', array('dataProvider' => $blogDataProvider)
        );
    }
}