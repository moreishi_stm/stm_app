<?php
class PageAction extends CAction
{

    public $templateType;

    public $assetDir;

    public $_cssAssetsUrl;

    /**
     * Manages models
     *
     * @param $pageUrl Comes from request
     */
    public function run($pageUrl)
    {
        //@todo: temp fix, need to be based off of package or something DB based - temp for TL package
        if(/*strpos($_SERVER['SERVER_NAME'], 'kwadvantage2') !== false || strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') !== false || strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') !== false || strpos($_SERVER['SERVER_NAME'], 'kwsarasotasiestakey') !== false || strpos($_SERVER['SERVER_NAME'], 'growwithkwrc') !== false || strpos($_SERVER['SERVER_NAME'], 'kwrealtyboise') !== false || strpos($_SERVER['SERVER_NAME'], 'demotl.') !== false*/Yii::app()->user->client->type == 'Brokerage') {
            $this->controller->module->layout = 'column1';
        }

        /** @var CmsContents $CmsContentModel */
        $CmsContentModel = CmsContents::model()->byUrl($pageUrl)->find();

        // If we cannot find the specified content throw a 404.
        if (!$CmsContentModel) {

            throw new CHttpException(404, 'Could not locate content.'); //@todo: CLee replaced with default data for now.

        } /*elseif(!$CmsContentModel->template_data) {

            $defaultHeader = '';
            $defaultContent = '';
            $defaultCmsData = array(
                'header' => array(
//                    'content'=>$defaultHeader,
//                    'plaintext'=>$defaultHeader,
//                    'raw'=>$defaultHeader,
                    $defaultHeader
                ),
                'content'=>array(
//                    'content'=>$defaultContent,
//                    'plaintext'=>$defaultContent,
//                    'raw'=>$defaultContent,
                    $defaultContent
                ));
            $CmsContentModel->template_data = CJSON::encode($defaultCmsData);
            Yii::log(__CLASS__ . '(' . __LINE__ . ') Could not locate CMS content: ' . print_r($CmsContentModel->attributes, true), CLogger::LEVEL_ERROR);
        }*/

        $Template = new CmsTemplates($CmsContentModel->template_name, $CmsContentModel->type_ma, $CmsContentModel->id);

        $cssAssetsUrl = $this->getCssAssetsUrl($Template->templateName);
        if ($cssAssetsUrl) {
            Yii::app()->clientScript->registerCssFile($cssAssetsUrl . DS . 'style.css');
        }

        // Set the page title
        $this->controller->pageTitle = $CmsContentModel->title;
        $this->controller->pageDescription = $CmsContentModel->seo_description;
        $this->controller->pageKeywords = $CmsContentModel->seo_keywords;
        $this->controller->pageMetaTags = $CmsContentModel->meta_tags;
        $this->controller->trackingScripts = $CmsContentModel->tracking_scripts;

        $this->setLayout($CmsContentModel);

        $pageRenderArgs = array('pageId'=>$CmsContentModel->id, 'pageContent' => $CmsContentModel->getContent(), 'fields' => $Template->loadedFields, 'cmsTags'=>$CmsContentModel->cmsTags);

        if (!STM_HQ && !empty($CmsContentModel->transaction)) {
            $contentModelTransaction = current($CmsContentModel->transaction);
            $pageRenderArgs          = CMap::mergeArray(
                $pageRenderArgs, array('Transaction' => $contentModelTransaction, 'Address' => $contentModelTransaction->address,)
            );
        }

        // Add in special options for page rendering
        $pageRenderArgs['display_tags'] = $CmsContentModel->display_tags ? true : false;
        $pageRenderArgs['allow_comments'] = $CmsContentModel->allow_comments ? true : false;

        $this->controller->render(
            'page', $pageRenderArgs
        );
    }

    public function getCssAssetsUrl($templateName)
    {

        $this->assetDir = Yii::getPathOfAlias('admin_module.views.cms.templates') . DS . $templateName . DS . 'assets' . DS . 'css';
        $styleCssPath = $this->assetDir . DS . 'style.css';

        if (!file_exists($styleCssPath)) {
            return false;
        }

        if (!$this->_cssAssetsUrl) {
            $this->_cssAssetsUrl = Yii::app()->getAssetManager()->publish($this->assetDir, false, -1, FrontModule::REFRESH_CSS_ASSETS);
        }

        return $this->_cssAssetsUrl;
    }

    public function setLayout($model)
    {

        // ********* NEED TO FIND A BETTER SOLUTION FOR TEMPLATES WITH 1 COLUMN LAYOUT, MADE IT WORK FOR NOW ***************
        if ($model->template_name == 'video') {
//            $this->controller->layout = 'column1';
        }

        return;
    }
}