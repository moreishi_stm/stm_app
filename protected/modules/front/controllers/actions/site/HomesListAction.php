<?php

	class HomesListAction extends CAction {
        const HOME_LIST_LIMIT = 250;
		/**
		 * Manages properties
		 */
		public function run() {
			$boardName = Yii::app()->getUser()->getBoard()->getPrefixedName();
            $property = new MlsProperties($boardName, 'search');
            $property->unsetAttributes();
            $secondaryProperty = new MlsPropertiesSecondary($boardName, 'search');

            $homeVars = array();

            if($_GET) {
                $homeVars = $_GET;
            }

            // default for property types
            if(!isset($homeVars['property_types']) && empty($homeVars['keywords'])) {

                // if no keyword do the normal default property type
                $defaultPropertyTypesIds = MlsPropertyTypeBoardLu::getDefaultPropertyTypeIds(Yii::app()->user->board->id);
                $homeVars['property_types'] = implode(',', $defaultPropertyTypesIds);
            }

            if ($_POST || (isset($homeVars))) {
				if(isset($homeVars['keywords'])) {
					$this->controller->mainSearchValue = $homeVars['keywords'];
				}

                $searchingSecondaryPropertyValues = false;

                /**
                 * The search filter on the homes list page overrides the url parameters
                 */
                if (isset($_POST['MlsProperties'])) {
                    $homeVars = CMap::mergeArray($homeVars, $_POST['MlsProperties']);
                }
//                else {
//                    $propertyTypeCriteria = new CDbCriteria;
//                    $propertyTypeCriteria->addInCondition('mls_property_type_id',array(1,2));
//                }

                /**
                 * Only filter properties based on the safe attribute list derived from the "search" scenario
                 */
                $safeAttributes = $property->getAttributes($property->getSafeAttributeNames());
                $secondaryPropertySafeAttrs = $secondaryProperty->getAttributes($secondaryProperty->getSafeAttributeNames());
                foreach ($homeVars as $attribute => $value) {
                    if (array_key_exists($attribute, $safeAttributes) and empty($property->$attribute) && !empty($value)) {
//                        if($attribute == 'property_type') {
//                            $property->mls_property_type_id = null;
//                        }
						//$_GET['FeaturedAreas'][$attribute] = $value;
                        $property->$attribute = $value;
                    } else if (array_key_exists($attribute, $secondaryPropertySafeAttrs) and empty($secondaryProperty->$attribute) && !empty($value)) {
                        $secondaryProperty->$attribute = $value;
                        $searchingSecondaryPropertyValues = true;
						//$_GET['FeaturedAreas'][$secondaryProperty->$attribute] = $value;
                    } else {
                        Yii::log("Could not filter properties by unknown attribute: {$attribute}.", CLogger::LEVEL_WARNING);
                    }
                }
                $property->status = 'Active';
                /**
                 * If we are searching through secondary property values, then we will do a search and retrieve the listing ids
                 * and pass them to the property search data provider for retrieval
                 *
                 * If nothing is found during this search, then no results should show as all criteria is considered to be "AND" related
                 */
                if ($searchingSecondaryPropertyValues) {

                    $secondaryPropertyProvider = $secondaryProperty->searchFull();

                    /**
                     * We want all of the results possible
                     */
                    $secondaryPropertyProvider->pagination = false;

                    /**
                     * This allows us to extend the property lookup criteria before a search occurs for a client
                     * clients can provide a "beforeSecondaryPropertyLookup" call to override default secondary criteria
                     */
                    $propertyBeforeLuEvent = $this->beforeSecondaryPropertyLookup();
                    if ($propertyBeforeLuEvent->sender instanceof CDbCriteria) {
                        $secondaryPropertyProvider->getCriteria()->mergeWith($propertyBeforeLuEvent->sender);
                    }

                    /**
                     * This retrieves all of the matched properties based on the MlsPropertiesSecondary criteria
                     */
                    $secondaryPropertiesFound = (bool) (int) $secondaryPropertyProvider->totalItemCount;
                    if ($secondaryPropertiesFound) {

                        $idsOnly = new CDbCriteria();
                        $idsOnly->select = "listing_id";
                        $secondaryPropertyProvider->getCriteria()->mergeWith($idsOnly);

                        $secondaryPropertyListingIds = array();

                        $data = $secondaryPropertyProvider->getData();
                        foreach ($data as $secondaryProperty) {
                            array_push($secondaryPropertyListingIds, $secondaryProperty->listing_id);
                        }

                        unset($data);

                        $secondaryListingIdCriteria = new CDbCriteria;
                        $secondaryListingIdCriteria->addInCondition('listing_id', $secondaryPropertyListingIds);
                        $property->getDbCriteria()->mergeWith($secondaryListingIdCriteria);
                    }
                }

                $latLngCriteria = new CDbCriteria;
                if((!isset($homeVars['verts']) || empty($homeVars['verts']))) {
                    if(isset($homeVars['minLat'])) {

                        $latLngCriteria->addBetweenCondition('geo_lat', $homeVars['minLat'], $homeVars['maxLat']);
                        $latLngCriteria->addBetweenCondition('geo_lon', $homeVars['minLng'], $homeVars['maxLng']);

                        $property->getDbCriteria()->mergeWith($latLngCriteria);
                    }
                }

                if(isset($homeVars['verts']) && !empty($homeVars['verts']) && $homeVars['verts'] !== 'false') {

                    $verts = json_decode(urldecode($homeVars['verts']));

                    $containsText = "ST_CONTAINS(ST_GeomFromText('POLYGON((";

                    $i = 0;
                    foreach($verts as $vert) {
                        if($i == 0) {
                            $firstSet = $vert->lat." ".$vert->lng;
                        }
                        $containsText .= $vert->lat." ".$vert->lng.", ";
                        $i++;
                    }

                    $containsText .= $firstSet."))'), POINT(geo_lat, geo_lon))";

                    $latLngCriteria->condition = $containsText;
                    $property->getDbCriteria()->mergeWith($latLngCriteria);
                }

                if($homeVars['ajax'] == 'true') {
                    $noEmtpyLatLong = new CDbCriteria;
                    $noEmtpyLatLong->condition = "geo_lat <> 0 AND geo_lon <> 0";
                    $property->getDbCriteria()->mergeWith($noEmtpyLatLong);
                }

                /**
                 * Retrieves the data provider to use in the CListView
                 */
                $propertySearchProvider = $property->searchFull();

                // this default property type was set up top
//                if($propertyTypeCriteria) {
//                    $propertySearchProvider->getCriteria()->mergeWith($propertyTypeCriteria);
//                }


                /**
                 * This allows us to extend the property lookup criteria before a search occurs for a client
                 * clients can provide a "beforePropertyLookup" call to override default criteria
                 */
                $propertyBeforeLuEvent = $this->beforePropertyLookup();
                if ($propertyBeforeLuEvent->sender instanceof CDbCriteria) {
                    $propertySearchProvider->getCriteria()->mergeWith($propertyBeforeLuEvent->sender);
                }

                /**
                 * If we are searching by the secondary properties table and no results are found, then negate the entire result set
                 */
                if ($searchingSecondaryPropertyValues and !$secondaryPropertiesFound) {
                    $noResultsCriteria = new CDbCriteria;
                    $noResultsCriteria->condition = '1=0';
                    $propertySearchProvider->getCriteria()->mergeWith($noResultsCriteria);
                }

            }

            $propertySearchProvider->pagination->pageSize = ($homeVars['ajax'] == 'true') ? self::HOME_LIST_LIMIT : 30;

            if($homeVars['ajax'] == "true") {
                $data = $propertySearchProvider->getData();

                $locationInfo = array();

                foreach($data as $row) {
                    $street = $row['street_number'].' '.$row['street_name'].' '.$row['street_suffix'];

                    $url = $row->getUrl($relative=true);
                    $imgSrc =(($firstPhotoUrl = $row->firstPhotoUrl)? $firstPhotoUrl : $row->getPhotoUrl($photoNumber=1));

                    $price = Yii::app()->format->formatDollars($row['price']);

                    $img = '<a class="home-photo" href="'.$url.'"><img src="'.$imgSrc.'" width="168" border="0" /></a>';

                    $mapPinContent = '<div class="location_container container-fluid">';
                    $mapPinContent .= '<div class="row">';
                    $mapPinContent .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding" style="overflow: hidden;">'.$img.'</div>';
                    $mapPinContent .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-right: 0;">';
                    //$mapPinContent .= '<div class="status '.(($row['status'] == 'Sold') ? 'sold' : 'active').'">'.$row['status'].'</div>';
                    $mapPinContent .= '<div class="price">'.$price.'</div>';
                    $mapPinContent .= '<div >'.ucwords(strtolower($street)).'</div>';
                    $mapPinContent .= '<div>'.(($row['mls_property_type_id'] == 3) ? 'Vacant Land' : $row['bedrooms'].' BD | '.$row['baths_full'].' BA | '.$row['sq_feet'].' SF').'</div>';
                    $mapPinContent .= '<div><a href="'.$url.'" style="color: #C20000;text-decoration: underline; font-weight: bold;" title="Quick View - '.$street.'">View Details</a></div>';
                    $mapPinContent .= "</div>";
                    $mapPinContent .= "</div>";
                    $mapPinContent .= "</div>";

                    $locationInfo[] = array(
                        'isSaved' => $row->getIsSaved(),
                        'price' => $price,
                        'price_raw' => $row['price'],
                        'url' => $url,
                        'imgSrc' => $imgSrc,
                        'street' => ucwords(strtolower($street)),
                        'city' => ucwords(strtolower($row['city'])),
                        'beds' => (($row['mls_property_type_id'] == 3) ? 'Vacant Land' : $row['bedrooms']),
                        'baths' => $row['baths_full'],
                        'status' => $row['status'],
                        'lat' => (empty($row['geo_lat']) ? 0 : $row['geo_lat']),
                        'lon' => (empty($row['geo_lon']) ? 0 : $row['geo_lon']),
                        'listing_id' => $row['listing_id'],
                        'mapPinContent' => $mapPinContent,
                    );
                }

                $this->renderJSON(array(
                    'total' => $propertySearchProvider->getTotalItemCount(),
                    'page' => $propertySearchProvider->getPagination()->getCurrentPage(),
                    'page_count' => $propertySearchProvider->getPagination()->getPageCount(),
                    'locations' => $locationInfo
                ));
                return;
            }

			$this->getController()->render('homeList', array(
					'model' => $property,
                    'homeListLimit' => self::HOME_LIST_LIMIT,
					'DataProvider' => $propertySearchProvider,
					'action' => '/homes/',
				)
			);
		}

        /**
         * Return data to browser as JSON and end application.
         * @param array $data
         */
        protected function renderJSON($data)
        {
            header('Content-type: application/json');
            echo CJSON::encode($data);

            foreach (Yii::app()->log->routes as $route) {
                if($route instanceof CWebLogRoute) {
                    $route->enabled = false; // disable any weblogroutes
                }
            }
            Yii::app()->end();
        }

        /**
         * Executed before the property lookup has occurred, allows for property criteria extension at the client level
         * @return CEvent
         */
        protected function beforeSecondaryPropertyLookup() {
            if ($this->hasEventHandler('onBeforeSecondaryPropertyLookup')) {
                $clientExtendedCriteria = new CDbCriteria;
                $propertyEvent = new CEvent($clientExtendedCriteria);
                $this->onBeforeSecondaryPropertyLookup($propertyEvent);

                return $propertyEvent;
            }
        }

        /**
         * @param CEvent $propertyEvent
         */
        protected function onBeforeSecondaryPropertyLookup(CEvent $propertyEvent) {
            $this->raiseEvent('onBeforeSecondaryPropertyLookup', $propertyEvent);
        }

        /**
         * Executed before the property lookup has occurred, allows for property criteria extension at the client level
         * @return CEvent
         */
        protected function beforePropertyLookup() {
            if ($this->hasEventHandler('onBeforePropertyLookup')) {
                $clientExtendedCriteria = new CDbCriteria;
                $propertyEvent = new CEvent($clientExtendedCriteria);
                $this->onBeforePropertyLookup($propertyEvent);

                return $propertyEvent;
            }
        }

        /**
         * @param CEvent $propertyEvent
         */
        protected function onBeforePropertyLookup(CEvent $propertyEvent) {
            $this->raiseEvent('onBeforePropertyLookup', $propertyEvent);
        }
	}