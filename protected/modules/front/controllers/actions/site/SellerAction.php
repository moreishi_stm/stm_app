<?php

class SellerAction extends CAction {

	/**
	 * Manages models
	 */
	public function run()
    {
        $formId = Forms::FORM_HOUSE_VALUES;
        $SubmissionValues = new FormSubmissionValues($formId);
        $FormFields = new FormFields;

        $this->controller->render('seller', array(
                'model' => $SubmissionValues,
                'formId' => $formId,
                'FormFields' => $FormFields,
            ));
	}
}