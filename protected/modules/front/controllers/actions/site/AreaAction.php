<?php

	class AreaAction extends CAction {

		/**
		 * Manages models
		 */
		public function run($area)
        {
			$model = FeaturedAreas::model()->findByUrl($area);
            $this->trackingScript($model);
			if (!$model) {
				$this->getController()->redirect('/');
			}

            $homeVars = array();
            if($_GET) {
                $homeVars = $_GET;
            }

            // when someone does a search from listSearchbox and there is a page get parameter, it needs to be changed to 1 as this is a new search. Otherwise, it stays at that page.
            if (Yii::app()->request->isAjaxRequest) {
                $_REQUEST['MlsProperties_page']=1;
            }

			$this->controller->pageTitle = $model->name . ' Homes for Sale';

			$Criteria = $model->getCriteria(ComponentTypes::FEATURED_AREAS, $model->id);

//			$ActiveCriteria = $model->getCriteriaByTermName('status', 'Active');
//			$Criteria->mergeWith($ActiveCriteria);
            $Criteria->addCondition('status like "%Active%" AND status != "Inactive"');
			$modelName = get_class($model);

            if(isset($_REQUEST[$modelName])) {
                foreach ($_REQUEST[$modelName] as $key => $value) {
                    if (!empty($value)) {
                        $model->$key = $value;
                        $Criteria->mergeWith($model->getCriteriaByTermName($key, $value));
                    }
                }
            } elseif($homeVars) {
                foreach ($homeVars as $key => $value) {

                    $approvedTerms = array('price_min','price_max','bedrooms', 'total_baths','sq_feet','property_type','property_types','zip','foreclosure','short_sale','year_built','street_name','city','county','school','listing_id','waterfront_yn','new_construction_yn','agent_id');
                    if(in_array($key, $approvedTerms)) {

                        //@todo: this is temporary hotfix - handling values that come into price_max = 400000/ - removes the non-numeric slash to prevent errors. Crawlers are hitting this like that and causing errors.
                        $numericTerms = array('price_min','price_max','bedrooms', 'total_baths','sq_feet','property_type','zip','foreclosure','short_sale','year_built');
                        if(in_array($key, $numericTerms)) {
                            $value = Yii::app()->format->formatInteger($value);
                        }

                        // handle property_types (multiple) temporarily like this
                        if($key == 'property_types') {
                            $propertyTypeIds = explode(',', $value);
                            $Criteria->addInCondition('mls_property_type_id', $propertyTypeIds);
                        }
                        else {
                            $model->$key = $value;
                            $Criteria->mergeWith($model->getCriteriaByTermName($key, $value));
                        }
                    }

//                    $safeAttributes = $property->getAttributes($property->getSafeAttributeNames());
//                    $secondaryPropertySafeAttrs = $secondaryProperty->getAttributes($secondaryProperty->getSafeAttributeNames());
//                    if (array_key_exists($attribute, $safeAttributes) and empty($property->$attribute)) {
//                        //                        if($attribute == 'property_type') {
//                        //                            $property->mls_property_type_id = null;
//                        //                        }
//                        $property->$attribute = $value;
//                    } else if (array_key_exists($attribute, $secondaryPropertySafeAttrs) and empty($secondaryProperty->$attribute)) {
//                        $secondaryProperty->$attribute = $value;
//                        $searchingSecondaryPropertyValues = true;
//                    } else {
//                        Yii::log("Could not filter properties by unknown attribute: {$attribute}.", CLogger::LEVEL_WARNING);
//                    }
                }
            } else {
//                $model->property_type = 1;
//                $propertyTypeCriteria = $model->getCriteriaByTermName('property_type', 1);
                $propertyTypeCriteria = new CDbCriteria;

                //@todo: temp hack for CLee as I do not want all property types
//                if(Yii::app()->user->board->id == 1) {
//                    $propertyTypeCriteria->addInCondition('mls_property_type_id',array(1,2));
//                }

                $Criteria->mergeWith($propertyTypeCriteria);
			}

            $Criteria->select = $model->getListRowSelect();
			$DataProvider = $model->getDataProvider($Criteria);
            $DataProvider->pagination->pageSize = 30;

			$areaMessage = (Yii::app()->user->getState('areaMessage')) ? Yii::app()->user->getState('areaMessage') : "";
			Yii::app()->user->setState('areaMessage', null);
			if(!empty($areaMessage)) {
				$areaMessage = '<h3 style="text-align:center;padding:15px;">' . $areaMessage . '</h3>';
			}

			$Criteria = new CDbCriteria();
			$Criteria->condition = "featured_area_id = :featured_area_id";
			$Criteria->order = "sort_order ASC";
			$Criteria->params = array(
				':featured_area_id' => $model->id
			);
			$images = FeaturedAreaImages::model()->findAll($Criteria);

			$MainImage = null;
			$faImages = array();
			foreach($images as $image) {
				if($image->is_primary) {
					$MainImage = $image->image_location;
				}

				$faImages[] = $image->attributes;
			}

			$this->controller->render('area', array(
				'mainImage' => (!empty($MainImage)) ? $MainImage : "",
				'DataProvider' => $DataProvider,
				'model' => $model,
				'areaMessage' => $areaMessage,
				'action' => '/area/'.$area,
				'images' => $faImages
			));
		}

		public function trackingScript($model) {
			$YII_DEBUG = (YII_DEBUG) ? 1 : 0;
            if(!$model->display_description_top) {
                $js = <<<JS
                    $(function() {

                        $('div#description').insertAfter('div#description-marker');
                    });
JS;
                // Positioned this at end of body so it can come in after the accounts have been set properly
                Yii::app()->clientScript->registerScript('areaViewTracking', $js, CClientScript::POS_END);
            }
		}
	}