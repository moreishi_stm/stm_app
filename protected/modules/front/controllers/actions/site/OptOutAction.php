<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 7/24/13
 */

class OptOutAction extends CAction {

    public function run($id)
    {
        $encodedEmailId = Yii::app()->request->getParam('eeid');
        // Render view for opt out page
		$this->getController()->render('optOut', array(
            'id'             =>  $id,
            'eeid' => $encodedEmailId,
		));
	}
}