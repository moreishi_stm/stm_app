<?php
/**
 * Class OptOutConfirmAction
 */
class OptOutConfirmAction extends CAction {

    /**
     * Run
     *
     * Default page load action
     * @param int $id The email to opt out
     */
    public function run($id)
    {
        if($encodedEmailId = Yii::app()->request->getParam('eeid')) {
            $decodedEmailId = StmFunctions::click3Decode($encodedEmailId);

            if($decodedEmailId != $id) {
                throw new Exception(__CLASS__.' (:'.__LINE__.') Decoded Email ID and raw id does not match. Raw Email ID: '.$id.', Decoded Email ID: '.$decodedEmailId.'. Did not opt out email id as this is an invalid request.');
            }
        }

        /** @var Emails $emailEntry */
        $emailEntry = Emails::model()->findByPk($id);
        $emailEntry->email_status_id = EmailStatus::OPT_OUT;

        // No model validation required here
        $emailEntry->save(false);

        // email notify someone here

        //@todo: find all the people assigned to her in contacts, buyer/seller (not lenders, agents only), recruits

        // make it a setting - owners notified of opt out - universal - ACCOUNT:

        // scenario: 1) account level tell me instantly when it happens 2) notify assignees to all trx? 3) give me once a week report


        $this->getController()->render('optOutConfirm', array(
            'emailEntry' => $emailEntry,
        ));
    }
}