<?php
class VideoEmailAction extends CAction
{
    public function run()
    {
        if(!isset($_GET['eid'])) {
            $this->controller->redirect('/');
        }

        //cross reference with encoded email id so people can't just mess with things
        $decodedEmailTemplateId = StmFunctions::click3Decode(Yii::app()->request->getParam('etid'));
        if($decodedEmailTemplateId != intval($_GET['eid'])) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Email Template ID and Encoded ID does not match.');
        }

        $emailTemplate = EmailTemplates::model()->findByPk($_GET['eid']);
        if(!$emailTemplate) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Email Template not found for that ID');
        }

        $this->controller->layout = 'column1';
        $this->controller->pageTitle = $emailTemplate->subject;

        // replace MAIL MERGE {{first_name}} with contact info
        $contact = (Yii::app()->user->contact) ? Yii::app()->user->contact : new Contacts;
        $body = $emailTemplate->filterBodyByContactData($contact);

        //replace bb & youtube with iframes
        //http://www.rachelkendallteam.com/videoEmail?vid=rppX3bIIK0E&vtype=youtube&eid=1&etid=E06bt

        preg_match_all("/<a[^>]*(?:\/videoEmail(?:.*)vid\=)(?:.*)(?:vtype\=)(?:.*)(?:eid\=)(?:.*)(?:etid\=)(?:.*)><\/a>/ismU", $body, $matches);

        // this handles a unexpected case where the closing "a" tag was missing and throwing off the regex, doing a match just for the image with class of stmVideoEmail
        if(empty($matches[0])) {
            preg_match_all("/<img class=\"(?:.*) stmVideoEmail\" (?:.*)\/\>/ismU", $body, $matches);
        }

        if(!empty($matches[0])) {
            foreach($matches[0] as $i => $videoLink) {
                //Example Link Data: <a href="http://www.palmcitystuartareahomes.com/bb?bbid=5ed54d5c-9e88-47b3-9e9b-31f2f055dfe5&amp;vid=cd1c6afd-88b6-940b-279f-ad89e27aa49f">
                //Example Image Data: <img id="cd1c6afd-88b6-940b-279f-ad89e27aa49f" class="bbVideoIframe stmBombBomb" style="max-width: 300px; max-height: 225px;" src="http://s3.amazonaws.com/bbemail/PROD/video_thumbs/cd1c6afd-88b6-940b-279f-ad89e27aa49f.jpg" alt="" /></a>

                //<a href="http://www.palmcitystuartareahomes.com/videoEmail?vid=cd1c6afd-88b6-940b-279f-ad89e27aa49f&vtype=bombbomb&eid=12"><img alt="" class="bbVideoIframe stmVideoEmail" id="cd1c6afd-88b6-940b-279f-ad89e27aa49f" src="http://s3.amazonaws.com/bbemail/PROD/video_thumbs/cd1c6afd-88b6-940b-279f-ad89e27aa49f.jpg" width="300" height="225"/></a>
                //preg_match("/<img class=\"(.*) stmVideoEmail\" id=\"(.*)\" src=\"(.*)\" width=\"(.*)\" height=\"(.*)\"\/\>/ismU", $videoLink, $matches2);

                preg_match("/<img class=\"(.*) stmVideoEmail\" (?:.*)\/\>/ismU", $videoLink, $matchesAttribute);
                $class = $matchesAttribute[1];

                if(!$class) {
                    preg_match("/<img(?:.*)class=\"(.*) stmVideoEmail\" (?:.*)\/\>/ismU", $videoLink, $matchesAttribute);
                    $class = $matchesAttribute[1];
                }

                preg_match("/<img(?:.*)id=\"(.*)\"(?:.*)\/\>/ismU", $videoLink, $matchesAttribute);
                $id = $matchesAttribute[1];

                preg_match("/<img(?:.*)src=\"(.*)\"(?:.*)\/\>/ismU", $videoLink, $matchesAttribute);
                $source = $matchesAttribute[1];

                preg_match("/<img(?:.*)height=\"(.*)\"(?:.*)\/\>/ismU", $videoLink, $matchesAttribute);
                $height = $matchesAttribute[1];

                preg_match("/<img(?:.*)width=\"(.*)\"(?:.*)\/\>/ismU", $videoLink, $matchesAttribute);
                $width = $matchesAttribute[1];

                if(!$id || !$class || !$width || !$height) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Missing core information to display video. Review video link. Text value: '.$videoLink);
                }

                switch($class) {

                    case 'youtube_video':
                        $videoSource = 'http://www.youtube.com/embed/'.$id.'?rel=0&showinfo=0';
                        break;

                    case 'responsive_image':
                    case 'bbVideoIframe':
                        $videoSource = 'http://bbemaildelivery.com/bbext/?p=vidEmbed&id='.$id;
                        break;

                    default:
                        Yii::log(__CLASS__.' (:'.__LINE__.') Unexpected Video Image class. Video Image text: '.$videoLink, CLogger::LEVEL_ERROR);
                        continue;
                        break;
                }

                $replaceWith = '<iframe class="bbVideoIframe" width="'.$width.'" height="'.$height.'" src="'.$videoSource.'" frameborder="0" scrolling="no"></iframe>';

                // image with iframe youtube/bbVideos to play in browser
                $body = str_replace($videoLink, $replaceWith, $body);
            }
        }

        $this->controller->render(
            'videoEmail', array('body' => $body)
        );
    }
}