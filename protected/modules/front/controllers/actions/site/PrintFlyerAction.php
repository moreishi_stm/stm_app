<?
	class PrintFlyerAction extends CAction {

		/**
		 * Manages models
		 */
		public function run($id) {
			$this->controller->layout = 'flyer';
			$boardName = Yii::app()->user->board->getPrefixedName();

            /** @var MlsProperties $model */
			$model = MlsProperties::model($boardName)->findByAttributes(array('listing_id'=>$id, 'status'=>'Active'));

            if (empty($model)) {
                Yii::app()->user->setFlash('notice', 'Property Listing ID '.$id.' is not available. Please search for another property.');
                $this->controller->redirect('/search');
                throw new CHttpException(500, "Property with Listing ID {$id} is not available.");
            }

			Yii::app()->clientScript->registerCssFile($this->controller->module->cssAssetsUrl . DS . 'style.homes.css');

			$FlyerContact = SettingAccountValues::getWebsiteFlyerContact();
			$profilePhotoFile = SettingContactValues::model()->getValue('profile_photo', $FlyerContact->id)->value;

			$filePath = Yii::app()->user->profileImageBaseFilepath . DS . $FlyerContact->id . DS . $profilePhotoFile;
			if (file_exists($filePath)) {
				$urlPath = Yii::app()->user->profileImageBaseUrl . DS . $FlyerContact->id . DS . $profilePhotoFile;
			} else {
				$urlPath = null;
			}

            /*
            try
            {
                $pdf = new HTML2PDF('P', 'A4', 'en');
                $pdf>setDefaultFont('Arial');
                $pdf>writeHTML($content, isset($_GET['vuehtml']));
                $pdf>Output('exemple00.pdf');
            }
            catch(HTML2PDF_exception $e) {
                echo $e;
                exit;
            }
            */

			$this->controller->render('printFlyer', array(
					'model' => $model,
					'urlPath' => $urlPath
				)
			);
		}
	}

?>