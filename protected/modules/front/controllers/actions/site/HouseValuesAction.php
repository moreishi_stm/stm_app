<?php
	class HouseValuesAction extends CAction {

		/**
		 * Manages models
		 *
		 * @param $pageUrl Comes from request
		 */
		public function run($url)
        {
			$this->controller->layout = 'houseValues';
			$CmsContentModel = CmsContents::model()->byUrl($url)->find();

			// If we cannot find the specified content throw a 404.
			if (!$CmsContentModel) {
				throw new CHttpException(404, 'Could not locate content.');
			}

			$Template = new CmsTemplates($CmsContentModel->template_name, $CmsContentModel->type_ma, $CmsContentModel->id);
			$this->controller->pageTitle = $CmsContentModel->title;

            $formId = Forms::FORM_HOUSE_VALUES;

            switch($CmsContentModel->template_name) {
                case 'modern':
                case 'modern2':
                    $formId = Forms::FORM_HOUSE_VALUES_SIMPLE;
                    break;
            }

            if(isset($_GET['address']) && isset($_GET['city']) && isset($_GET['state']) && isset($_GET['zip'])) {
                if($_GET['status']==2) {
                    $formPart = ($CmsContentModel->template_name == 'modern2') ? "2B" : 2;
                } elseif($_GET['status']=='success') {
                    $formPart = 'success';
                }
            } else {
                $formPart = '1_details';
            }

            $formName = $formId.'_Part'.$formPart;

            $SubmissionValues = new FormSubmissionValues($formName);
            $SubmissionValues->formPart = $formPart;

            if(isset($_GET['address']) && isset($_GET['city']) && isset($_GET['state']) && isset($_GET['zip'])  && ($_GET['status']=='success' || $_GET['status']==2)) {
                $SubmissionValues->data = array(
                    FormFields::ADDRESS_ID          =>  ucwords(strtolower($_GET['address'])),
                    FormFields::CITY_ID             =>  ucwords(strtolower($_GET['city'])),
                    FormFields::STATE_ID            =>  $_GET['state'],
                    FormFields::ZIP_ID              =>  $_GET['zip'],
                    FormFields::BEDROOMS_ID         =>  $_GET['bedrooms'],
                    FormFields::BATHS_ID            =>  $_GET['baths'],
                    FormFields::SQ_FEET_ID          =>  $_GET['sq_feet'],
                    FormFields::CONDITION_ID        =>  $_GET['condition'],
                    FormFields::FORM_SUBMISSION_ID  =>  $_GET['fsid']
                );
            }

            $this->controller->render('page', array(
                    'houseValuesActionId' => $url,
					'pageContent' => $Template->content,
					'fields' => $Template->loadedFields,
                    'hideCmsTags'=>true,
                    'FormFields' => new FormFields,
                    'formId' => $formId,
                    'SubmissionValues' => $SubmissionValues,
				)
			);
		}
	}