<?php
class BbAction extends CAction
{
    public function run()
    {
        if(!isset($_GET['bbid'])) {
            $this->controller->redirect('/');
        }
        $emailTemplate = EmailTemplates::model()->findByAttributes(array('bombbomb_id'=>$_GET['bbid']));
        if(!$emailTemplate) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Email Template not found for that Bombbomb ID', CLogger::LEVEL_ERROR);
        }

        $this->controller->layout = 'column1';
        $this->controller->pageTitle = $emailTemplate->subject;

        $body = $emailTemplate->body;

        //replace bb & youtube with iframes
        preg_match_all("/<a[^>]*(?:\/bb\?bbid\=)(.*)(?:\&amp\;vid\=)(?:.*)><\/a>/ismU", $body, $matches);
        if(!empty($matches[0])) {
            foreach($matches[0] as $i => $videoLink) {
                //Example Link Data: <a href="http://www.palmcitystuartareahomes.com/bb?bbid=5ed54d5c-9e88-47b3-9e9b-31f2f055dfe5&amp;vid=cd1c6afd-88b6-940b-279f-ad89e27aa49f">
                //Example Image Data: <img id="cd1c6afd-88b6-940b-279f-ad89e27aa49f" class="bbVideoIframe stmBombBomb" style="max-width: 300px; max-height: 225px;" src="http://s3.amazonaws.com/bbemail/PROD/video_thumbs/cd1c6afd-88b6-940b-279f-ad89e27aa49f.jpg" alt="" /></a>
                preg_match("/<img id=\"(.*)\" class=\"(.*) stmBombBomb\"(?:.*)max-width: (.*)px(?:.*)max-height: (.*)px(?:.*)src=\"(.*)\"\>/ismU", $body, $matches2);
                if(!empty($matches2)) {
                    $id = $matches2[1];
                    $class = $matches2[2];
                    $width = $matches2[3];
                    $height = $matches2[4];
                    $source = $matches2[5];
                }

                if(!$id || !$class || !$width || !$height) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Missing core information to display video. Review video link. Text value: '.$videoLink, CLogger::LEVEL_ERROR);
                }

                //NOTE: Can use this multiple regex if you change the pattern want to extract individually for more flexibility/reliability
                // extract width & height, default can be 300w x 250h
//                preg_match("/width=\"(.*)\"/ismU", $videoImage, $match2);
//                $width = trim($matchFrame[1]);
//
//                preg_match("/height=\"(.*)\"/ismU", $videoImage, $match2);
//                $height = trim($matchFrame[1]);
//
//                preg_match("/class=\"(.*)\"/ismU", $videoImage, $match2);
//                $class = trim($matchFrame[1]);
//
//                preg_match("/name=\"(.*)\"/ismU", $videoImage, $match2);
//                $name = trim($matchFrame[1]);
//
//                preg_match("/id=\"(.*)\"/ismU", $videoImage, $match2);
//                $id = trim($matchFrame[1]);
//
//                preg_match("/src=\"(.*)\"/ismU", $videoImage, $match2);
//                $src = trim($matchFrame[1]);
//
                switch($class) {

                    case 'youtube_video':
                        $videoSource = 'http://www.youtube.com/embed/'.$id;
                        break;

                    case 'responsive_image':
                    case 'bbVideoIframe':
                        $videoSource = 'http://bbemaildelivery.com/bbext/?p=vidEmbed&id='.$id;
                        break;

                    default:
                        Yii::log(__CLASS__.' (:'.__LINE__.') Unexpected Video Image class. Video Image text: '.$videoLink, CLogger::LEVEL_ERROR);
                        continue;
                        break;
                }

                $replaceWith = '<iframe class="bbVideoIframe" width="'.$width.'" height="'.$height.'" src="'.$videoSource.'" frameborder="0" scrolling="no"></iframe>';

                // image with iframe youtube/bbVideos to play in browser
                $body = str_replace($videoLink, $replaceWith, $body);
            }
        }

        $this->controller->render(
            'bb', array('body' => $body)
        );
    }
}