<?php

class MortgageAction extends CAction {

	/**
	 * Manages models
	 */
	public function run() {
        $this->controller->pageTitle = 'Mortgage & Lenders';
//		// pull only the settings relevant for profile....
//		// check to see if the user's profile is active, if not sent to a general list page with message
//		$model = new Settings;
//		$model->type = 'contact';
//		$settings = $model->contactType()->findAll();
//
//		$Contact = Contacts::model()->find(array(
//			'condition' => 'id=:id AND contact_status_ma=:status',
//			'params' => array(':id' => $id, ':status' => Contacts::STATUS_ACTIVE),
//		));
//
//		if ($Contact) {
//			$this->controller->pageTitle = 'About '.$Contact->fullName;
//			$showProfile = SettingContactValues::model()->getValue('profile_on_website', $Contact->id);
//		}
//
//		if ($showProfile) {
//			$this->controller->layout = 'column1';
//			$ContactAttributes = new ContactAttributes;
//
//			$SettingContactValues = new SettingContactValues;
//			foreach ($settings as $Setting) {
//				$SettingContactValue = $Setting->getSettingContactValue($id);
//				$SettingContactValues->data[$SettingContactValue->setting_id] = $SettingContactValue->value;
//			}
//
//			// Create a collection of contact attribute values from what may already be existing
//			$ContactAttributeValues = new ContactAttributeValues;
//			if ($Contact->getProfileAttributeList()) {
//				foreach ($Contact->getProfileAttributeList() as $ContactAttributeValue) {
//					$ContactAttributeValues->data[$ContactAttributeValue->contact_attribute_id] = $ContactAttributeValue->value;
//				}
//			}
//
        $AuthAssignments = AuthAssignment::model()->findAllByAttributes(array('itemname'=>'lender'));


        $formId = Forms::FORM_PREQUAL;
        $SubmissionValues = new FormSubmissionValues($formId);
        $FormFields = new FormFields;

			$this->controller->render('mortgage', array(
                'formId' => $formId,
                'model' => $SubmissionValues,
                'FormFields' => $FormFields,
                'Lenders'=>$AuthAssignments,
                'SettingValue' => new SettingContactValues,


//				'model' => $model,
//				'Contact' => $Contact,
//				'ContactAttributes' => $ContactAttributes,
//				'ContactAttributeValues' => $ContactAttributeValues,
//				'SettingContactValues' => $SettingContactValues,
			));
//		} else {
//
//			$DataProvider = new CActiveDataProvider(Contacts::model()->byActiveAdmins($together=true)->byDisplayOnWebsite());
//            $DataProvider->pagination->pageSize = 25;
//			$this->controller->render('teamList', array('DataProvider'=>$DataProvider));
//		}
	}
}