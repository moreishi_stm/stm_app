<?php
	/**
	 * @author: Chris Willard <chriswillard.dev@gmail.com>
	 * @since : 7/25/13
	 */
	class TrackLinkAction extends CAction {

		public function run($encodedLinkId=null, $encodedContactId=null, $emailMessageId=null) {

            if(empty($encodedLinkId) && empty($encodedContactId) && empty($encodedContactId) && isset($_GET)) {
                Yii::log(__FILE__.'('.__LINE__.'): Alternative method of extracting $_GET variables had to be implemented. Verify and see if there is any other hotfixes that are needed.', CLogger::LEVEL_ERROR);
                $encodedLinkId = current(array_keys($_GET));
                $encodedContactId = $_GET[$encodedLinkId];
                $emailMessageId = next(array_keys($_GET));
            }

			$decodedLinkId = StmMailMessage::urlSafeBase64Decode((urldecode($encodedLinkId)));
			$linkId = Yii::app()->getSecurityManager()->decrypt($decodedLinkId, EmailLink::ID_ENCRYPTION_KEY);

            if (empty($encodedContactId)) {
                Yii::log(__FILE__.'('.__LINE__.'): Encoded Contact ID is empty for track link.', CLogger::LEVEL_ERROR);
                $this->getController()->redirect('/');
            }
			$decodedContactId = StmMailMessage::urlSafeBase64Decode((urldecode($encodedContactId)));
			$contactId = Contacts::unHash($decodedContactId);
            if (empty($contactId)) {
                Yii::log(__FILE__.'('.__LINE__.'): Contact ID is empty for track link.', CLogger::LEVEL_ERROR);
                $this->getController()->redirect('/');
            }


			$contact = Contacts::model()->findByPk($contactId);
			if (!$contact instanceof Contacts) {
				Yii::log(__FILE__.'('.__LINE__.'): Could not auto-login in contact with id: ' . $contactId, CLogger::LEVEL_ERROR);
                $this->getController()->redirect('/');
			}

			// Log the user in automatically
			$loginForm = new LoginForm;
			$loginForm->email = $contact->getPrimaryEmail();
			$loginForm->password = $contact->password;
			$loginForm->rememberMe = false;
			$loginForm->login();

			/** @var EmailMessage $emailMessageEntry */
			$emailMessageEntry = EmailMessage::model()->findByPk($emailMessageId);
			if (!$emailMessageEntry) {
				Yii::log(__FILE__.'('.__LINE__.'): Could not locate email message instance', CLogger::LEVEL_ERROR);
                $this->getController()->redirect('/');
			}
			/** @var Contacts $contact */
			$contact = $emailMessageEntry->toEmail->contact;
			if (!$contact) {
                Yii::log(__FILE__.'('.__LINE__.'): Could not locate contact',CLogger::LEVEL_ERROR);
                $this->getController()->redirect('/');
			}

			/** @var EmailLink $emailLinkEntry */
			//$emailLinkEntry = EmailLink::model()->findByPk($linkId);
//			if (!$emailLinkEntry) {
//				Yii::log(__FILE__.'('.__LINE__.'): Could not locate email link instance',CLogger::LEVEL_ERROR);
//                $this->getController()->redirect('/');
//			}
			$redirectUrl = '/';

			// Track the click through
			$emailLinkTracking = new EmailClickTracking;
			$emailLinkTracking->email_message_id = $emailMessageId;
			$emailLinkTracking->contact_id = $contact->id;
			$emailLinkTracking->email_link_id = $linkId;
			if(!$emailLinkTracking->save()) {
                Yii::log(__FILE__.'('.__LINE__.'): Email link tracking did not save properly. Error Message: '.print_r($emailLinkTracking->getErrors(),true), CLogger::LEVEL_ERROR);
                $this->getController()->redirect('/');
            }

			$this->getController()->redirect($redirectUrl);
		}
    }