<?php

class TeamAction extends CAction {

	/**
	 * Manages models
	 */
	public function run($id=null) {

		// pull only the settings relevant for profile....
		// check to see if the user's profile is active, if not sent to a general list page with message
		$model = new Settings;
		$model->type = 'contact';
		$settings = $model->contactType()->findAll();

		$Contact = Contacts::model()->find(array(
			'condition' => 'id=:id AND contact_status_ma=:status',
			'params' => array(':id' => $id, ':status' => Contacts::STATUS_ACTIVE),
		));

		if ($Contact) {
			$this->controller->pageTitle = 'About '.$Contact->fullName;
			$showProfile = SettingContactValues::model()->getValue('profile_on_website', $Contact->id);
		}

		if ($showProfile) {
			$this->controller->layout = 'column1';
			$ContactAttributes = new ContactAttributes;

			$SettingContactValues = new SettingContactValues;
			foreach ($settings as $Setting) {
				$SettingContactValue = $Setting->getSettingContactValue($id);
				$SettingContactValues->data[$SettingContactValue->setting_id] = $SettingContactValue->value;
			}

			// Create a collection of contact attribute values from what may already be existing
			$ContactAttributeValues = new ContactAttributeValues;
			if ($Contact->getProfileAttributeList()) {
				foreach ($Contact->getProfileAttributeList() as $ContactAttributeValue) {
					$ContactAttributeValues->data[$ContactAttributeValue->contact_attribute_id] = $ContactAttributeValue->value;
				}
			}

			$this->controller->render('team', array(
				'model' => $model,
				'Contact' => $Contact,
				'ContactAttributes' => $ContactAttributes,
				'ContactAttributeValues' => $ContactAttributeValues,
				'SettingContactValues' => $SettingContactValues,
			));
		} else {
            $Criteria = Contacts::model()->byActiveAdmins($together=true)->byDisplayOnWebsite()->getDbCriteria();
            $Criteria->addNotInCondition('itemname', array('lender'));
            $Criteria->order = 'first_name ASC';
			$DataProvider = new CActiveDataProvider('Contacts',array(
                'criteria' => $Criteria,
            ));
            $DataProvider->pagination->pageSize = 25;
			$this->controller->render('teamList', array('DataProvider'=>$DataProvider));
		}
	}
}