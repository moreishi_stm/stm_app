<?php
Yii::import('front.controllers.actions.site.HomeDetailsAction');
class HomeDetailsAjaxAction extends HomeDetailsAction {

	// Used to find a price point to find relating homes
	const SIMILAR_COUNT_MIN = 12;
	const SIMILAR_COUNT_MAX = 35;
    const MIN_PHOTO_COUNT = 5;
	const LOG_CATEGORY = 'leadgen';
	protected $_baseCriteria;
	protected $Property;

	/**
	 * Manages models
	 */
	public function run($id) {
        //$this->_mortgageCalculatorScript();

        //@todo: temp hotfix for market update messup on url and need to auto login, put a time limit on this
        if(($cid = Yii::app()->request->getParam('cid')) && date('Y-m-d') < '2014-12-01') {
            $tempContact = Contacts::model()->findByPk($cid);

            $tempIdentity =new StmUserIdentity($tempContact->getPrimaryEmail(), $tempContact->password);
            $tempIdentity->authenticate();

            if ($tempIdentity->errorCode===StmUserIdentity::ERROR_NONE)
            {
                $tempIdentity->contact->last_login = new CDbExpression('NOW()');
                $tempIdentity->contact->last_login_ip = Yii::app()->stmFunctions->getVisitorIp();
                $tempIdentity->contact->save();

//                $duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
                Yii::app()->user->login($tempIdentity,(3600*24*30));
            }
        }

//		$this->trackingScript();
//		$this->controller->module->layout = 'column1';

        // check to see if mlsBoardId is a $_GET param
        $mlsBoardId = Yii::app()->request->getQuery('mlsBoardId');
        if($mlsBoardId && ($mlsBoard = MlsBoards::model()->findByPk($mlsBoardId))) {
            $tablePrefixedName = $mlsBoard->prefixedName;
        }
        else {
            $tablePrefixedName = Yii::app()->user->board->prefixedName;
        }


		$this->Property = MlsProperties::model($tablePrefixedName);

        $this->Property = $this->Property->active()->findByPk($id); //isFavorite($id)-> @todo: test on production to see if saving a separate db call reduces load time. Inconclusive locally cuz network lag not exist.
        if(!empty($this->Property)) {
            $this->Property->getHomeDetails($id);
        }
		if(empty($this->Property)) {
			Yii::app()->user->setState('areaMessage', 'The property requested was not found. Please search from<br />the homes below or <a href="/search/full">click here</a> for a Full Home Search.');
			$this->controller->redirect('/area/all');
		}

		if($this->Property)
			$this->controller->pageTitle = $this->Property->formatMlsAddress($this->Property).' - Home Details';

		if (Yii::app()->user->isGuest) {
			$viewsMaxSetting = Yii::app()->user->settings->views_max;
			// Need +1 to the count because page viewed gets counted AFTER this action is processed so it will always be 1 count behind.
			$homeDetailViews = PageViewTrackerWidget::countHomesViewed($unique = false);
			$viewsMaxReached = ($homeDetailViews + 1 > $viewsMaxSetting) ? true : false;

			// Temp CLee super tracking code :)
			if($viewsMaxReached) {
                // removing this for now until we figure out how to ignore when it is a bot. Way too many records.
                if(0) { //&& !YII_DEBUG
                    $FormViews = new FormViews;
                    $FormViews->home_detail_view_count = $homeDetailViews;
                    $FormViews->domain = str_replace(array('www.','http://'),'',$_SERVER['SERVER_NAME']);
                    $FormViews->page = $_SERVER['REQUEST_URI'];
                    $FormViews->url = str_replace(array('www.','http://'),'',$_SERVER['SERVER_NAME']).$_SERVER['REQUEST_URI'];
                    $FormViews->referrer = str_replace(array('www.','http://'),'',$_SERVER['HTTP_REFERER']);
                    $FormViews->utmz = $_COOKIE['__utmz'];
                    $FormViews->utma = $_COOKIE['__utma'];
                    $FormViews->utmb = $_COOKIE['__utmb'];
                    $FormViews->utmc = $_COOKIE['__utmc'];
                    $FormViews->added = date('Y-m-d H:i:s');
                    $FormViews->ip = Yii::app()->stmFunctions->getVisitorIp();
                    //					$browser = get_browser(null, true);
                    //					$browser = print_r($browser,true);
                    //					$FormViews->browser = $browser;
                    $FormViews->session_id = session_id();
                    if(!$FormViews->save()) {
                        $errorMsg = print_r($FormViews->getErrors(),true);
                        Yii::log(__FILE__.'('.__LINE__.') Error in saving Form Views. Error Message: '.$errorMsg, CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                    }
                }

				$message = date('Y-m-d | H:i:s');
				$message .= PHP_EOL.'Home Details Register Dialog Viewed';
				$referrer = str_replace(array('www.','http://'),'',$_SERVER['HTTP_REFERER']);
				$message .= PHP_EOL.'URL: => '.str_replace(array('www.','http://'),'',$_SERVER['SERVER_NAME']).$_SERVER['REQUEST_URI'];
				$message .= PHP_EOL.'Referrer: => '.$referrer;
				$message .= PHP_EOL.'UTMZ: =>'.$FormViews->utmz;
				$message .= PHP_EOL.'IP: => '.Yii::app()->stmFunctions->getVisitorIp();

			}
		}

		$stdClass = new \stdClass();
		$stdClass->model = $this->Property;
		$stdClass->interiorFeatures = $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::INTERIOR_FEATURE_ID);
		$stdClass->exteriorFeatures = $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::EXTERIOR_FEATURE_ID);
		$stdClass->propertyFeatures = $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::PROPERTY_FEATURE_ID);
		$stdClass->schoolFeatures = $this->Property->getDataByFeatureGroupId($id, MlsFeatureGroups::SCHOOL_FEATURE_ID);
		$stdClass->viewsMaxReached = $viewsMaxReached;
		$stdClass->isFavorite = (Yii::app()->user->isGuest)? null: SavedHomes::isHomeSaved($id); //(boolean) $this->Property->isFavorite,//@todo: test on production to see if saving a separate db call reduces load time;
		$stdClass->photoContent = $this->getCarouselPhotos();
		$stdClass->officePhone = Yii::app()->getUser()->getSettings()->office_phone;
		
		header('Content-type: application/json');
        echo json_encode($stdClass);
        Yii::app()->end();
	}

}
