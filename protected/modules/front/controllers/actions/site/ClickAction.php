<?php
/**
 * Click Action
 *
 * Used to manage email link tracking
 * @author Nicole Xu
 */
class ClickAction extends CAction
{
    /**
     * Run
     *
     * Called when this action is ran
     * @return void
     */
    public function run()
    {
        // Get data
        $data = Yii::app()->request->getParam('data');

        if(empty($data)) {
            $this->controller->redirect('/', true, 302);
        }

        // Decode and decrypt params
        try{
            $params = EmailClickTracking::decodeLink($data);

        } catch (Exception $e) {

            Yii::log(__FILE__.'('.__LINE__.'): Error in Decoding Email Tracking Link. User was sent to the home page as alternate route to provide good user experience and avoid error page. Error Message: '. $e->getMessage(), CLogger::LEVEL_ERROR);
            $this->controller->redirect('/', true, 302);
        }

        if(preg_match('(\#)', $params['url'])) {
            $params['url'] = str_replace(array('#'), '', $params['url']);
        }

        // Retrieve contact
        $contact = Contacts::model()->findByPk($params['contactId']);
        if(!$contact instanceof Contacts) {
            Yii::log(__FILE__.'('.__LINE__.'): Contact ID# '.$params['contactId'].' is invalid. Params: '.print_r($params, true), CLogger::LEVEL_ERROR);
            $this->controller->redirect('/');
        }

        // Retrieve email message
        $emailMessage = EmailMessage::model()->findByPk($params['emailMessageId']);
        if(!$emailMessage instanceof EmailMessage) {
            Yii::log(__FILE__.'('.__LINE__.'): Email ID '.$params['emailMessageId'].' is invalid. Params: '.print_r($params, true), CLogger::LEVEL_ERROR);
            if(!empty($params['url'])) {
                $this->controller->redirect($params['url'], true, 302);
            }
            else {
                $this->controller->redirect('/');
            }
        }

        // Catalog the email click tracking data
        $emailClickTracking = new EmailClickTracking();
        $emailClickTracking->setAttributes(array(
            'email_message_id'  =>  $emailMessage->id,
            'contact_id'        =>  $contact->id,
            'url'               =>  $params['url']
        ));
        if(!$emailClickTracking->save()) {
            Yii::log(__FILE__.'('.__LINE__.'): Email Click Tracking did not save. Errors: '.print_r($emailClickTracking->getErrors(), true).' Attributes:'.print_r($emailClickTracking->attributes, true), CLogger::LEVEL_ERROR);
        }

        // Log the user in automatically
        $loginForm = new LoginForm;
        $loginForm->email = $contact->getPrimaryEmail();
        $loginForm->password = $contact->password;
        $loginForm->rememberMe = false;
        if(!$loginForm->login()) {
            Yii::log(__FILE__.'('.__LINE__.'): Email Auto Login error. Errors: '.print_r($loginForm->getErrors(), true).' Attributes:'.print_r($loginForm->attributes, true), CLogger::LEVEL_ERROR);
        }

        if(strpos($params['url'],'Market Update') !== false) {
            $params['url'] = str_replace('seizethemarket.com','christineleeteam.com',$params['url']);
        }
        // Redirect to the URL requested
        $this->controller->redirect($params['url'], true, 302);
    }
}