<?php

class IndexStoryboardAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'Listing Storyboard';
        $this->controller->layout = 'column1';

		$this->controller->render('indexStoryboard',array(
		));
	}
}