<?php

	class PrivacyAction extends CAction {

		public function run() {
			$this->controller->module->layout = 'column1';

			$this->controller->render('privacy');
		}
	}