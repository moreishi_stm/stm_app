<?php
	class StoryboardAction extends CAction {

//		public $assetDir;
//		public $_cssAssetsUrl;

		/**
		 * Manages models
		 *
		 * @param $pageUrl Comes from request
		 */
		public function run($id) {
			$this->controller->layout = 'column1';
			$CmsContentModel = CmsContents::model()->findByPk($id);

            //@todo: temp for storyboard CSS - this will not be necessary once we get over to bootstrap 100%
            if(Yii::app()->params['layoutDirectory'] == 'storyboard') {
                $this->registerStoryboardCss();
            }

			// If we cannot find the specified content throw a 404.
			if (!$CmsContentModel) {
				throw new CHttpException(404, 'Could not locate content.');
			}

			$Template = new CmsTemplates($CmsContentModel->template_name, $CmsContentModel->type_ma, $id);

			// Set the page title
			$this->controller->pageTitle = $CmsContentModel->title.' - Listing Storyboard &trade;';

			$this->controller->render('page', array(
					'pageContent' => $Template->content,
					'fields' => $Template->loadedFields,
					'Transaction'=>$CmsContentModel->transaction,
					'Address' => $CmsContentModel->transaction->address,
                    'hideCmsTags'=>true,
				)
			);
		}
        protected function registerStoryboardCss() {
            Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/style.grid.css');
        }
	}