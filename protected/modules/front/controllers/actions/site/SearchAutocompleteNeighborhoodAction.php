<?php

class SearchAutocompleteNeighborhoodAction extends CAction {

    public function run() {
        $this->processAjaxRequest();
    }

    protected function processAjaxRequest()
    {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['term'])) {
                $searchedNeighborhood = $_POST['term'];
            }

            $neighborhoods = MlsPropertyFormHelper::getInstance()->getNeighborhoodOptions($searchedNeighborhood);

            echo CJSON::encode($neighborhoods);
            Yii::app()->end();
        }
    }
}
