<?php

class IndexAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = Yii::app()->name.' - Homes for Sale';
		$this->controller->module->layout = 'column1';

		$tableName = Yii::app()->user->board->prefixedName;
		$model = MlsProperties::model($tableName);

		$terms = $this->getTerms($model);

		$params = array();
		if(isset($_POST['MlsProperties'])) {
			$model->attributes = $_POST['MlsProperties'];
			foreach($_POST['MlsProperties'] as $key => $value)
				if (in_array($key, $terms)  && !empty($value))
					$params[$key] = $model->$key;
			$this->controller->redirect(Yii::app()->createUrl('front/site/homes',$params));
		}

		$loginFormModel = new LoginForm;
		
		// Added bkozak 5/28 to leave legacy code in front module out of new theme
		if(Yii::app()->theme->name !== "standard2") {
			Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);
		} else {
			$mainImageSetting = Settings::model()->findByAttributes(array('name' => 'homePageMainImage'));
			$mainImageAccountValue = SettingAccountValues::model()->findByAttributes(array('account_id' => Yii::app()->user->accountId, 'setting_id' => $mainImageSetting->id));
		}
		
		$this->controller->mainHomePageImage = (!empty($mainImageAccountValue)) ? $mainImageAccountValue->value : "";
		
		$this->controller->render('index',array(
			'loginFormModel'=>$loginFormModel,
			'model'=>$model,
            'searchModel' => new SavedHomeSearchForm(),
		));
	}

	public function getTerms($Property)
	{
		$attributes = $Property->attributes;
		$terms = array();
		foreach ($attributes as $key => $name) {
			array_push($terms, $key);
		}
		$terms = array_merge($terms, array('neighborhood','school','price_min','price_max'));
		return $terms;
	}
}