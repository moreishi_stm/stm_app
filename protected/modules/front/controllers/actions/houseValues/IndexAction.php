<?php

class IndexAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id=null)
	{

        switch ($id) {
	        case 'hamptonpark':
	        case 'hamptonPark':
		        $opt['title'] = 'Hampton Park House Values';
		        $opt['city'] = 'Jacksonville';
		        $opt['zip'] = '32256';
	            $opt['mainPhotoClass'] = 'hamptonPark';
	            $opt['subdivision'] = 'Hampton Park';
	            $opt['successLink'] = 'http://www.christineleeteam.com/area/hamptonpark';
		        break;

	        case 'highlandglen':
	        case 'highlandGlen':
		        $opt['title'] = 'Highland Glen House Values';
		        $opt['city'] = 'Jacksonville';
		        $opt['zip'] = '32224';
		        $opt['mainPhotoClass'] = 'highlandGlen';
		        $opt['subdivision'] = 'Highland Glen';
		        $opt['successLink'] = 'http://www.christineleeteam.com/area/highlandglen';
		        break;

	        case 'plantationoaks':
	        case 'plantationOaks':
		        $opt['title'] = 'Plantation Oaks House Values';
		        $opt['city'] = 'Ponte Vedra Beach';
		        $opt['zip'] = '32082';
		        $opt['mainPhotoClass'] = 'plantationOaks';
		        $opt['subdivision'] = 'Plantation Oaks';
		        $opt['successLink'] = 'http://www.christineleeteam.com/area/plantationoaks';
		        break;

	        case 'timberlinparc':
	        case 'timberlinParc':
		        $opt['title'] = 'Timberlin Parc House Values';
		        $opt['city'] = 'Jacksonville';
		        $opt['zip'] = '32256';
		        $opt['mainPhotoClass'] = 'timberlinParc';
		        $opt['subdivision'] = 'Timberlin Parc';
		        $opt['successLink'] = 'http://www.christineleeteam.com/area/timberlinparc';
		        break;

	        case 'worthingtonpark':
	        case 'worthingtonPark':
		        $opt['title'] = 'Worthington Park House Values';
		        $opt['city'] = 'St Johns';
		        $opt['zip'] = '32259';
	            $opt['mainPhotoClass'] = 'worthingtonPark';
		        $opt['subdivision'] = 'Worthington Park';
		        $opt['successLink'] = 'http://www.christineleeteam.com/area/worthingtonpark';
		        break;

	        default:
                $opt['title'] = 'Jacksonville House Values';
                break;
        }

		if(!isset($opt['successLink']))
			$opt['successLink'] = 'http://www.christineleeteam.com/area/all';

		$this->controller->pageTitle = $opt['title'];
        $this->controller->render('index',array('title'=>$title, 'opt'=>$opt));
    }
}