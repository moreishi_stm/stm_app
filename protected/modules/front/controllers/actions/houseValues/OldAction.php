<?php

class IndexAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->registerScripts();

		if(empty($_POST))
			$this->controller->render('index');
		elseif ($_POST['step']=='verify')
			$this->controller->render('verify');
		elseif ($_POST['step']=='complete')
			$this->controller->render('complete');
	}

	/**
	 * registerScripts Css for houseValues pages
	 * @return none
	 */
	public function registerScripts()
	{
		$css = <<<CSS
		body {
			background: #F9F9F9;
			font: normal 85%/160% arial,helvetica,sans-serif;
		}
		#pageWrapper {
			width: 965px;
			margin: 0 auto;
			text-align: left;
		}
		#houseValues {
			overflow: visible;
			margin-left: 0;
			margin-right: 0;
			width: 100%;
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
			border-radius: 4px;
		}
		#houseValues #houseValues-container {
			margin-top: 0;
			width: 100%;
			height: 365px;
			background: url('http://cdn.seizethemarket.com/assets/images/house_front_1.png');
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
			border-radius: 4px;
			background-position-x: 0px;
		}
		#houseValues h1 {
			text-align: right;
			font-weight: normal;
			text-transform: capitalize;
			font-size: 26px;
			color: #888;
			padding: 0px 5px 10px 0;
			margin-bottom: 4px;
		}
		#houseValues h2 {
			color: black;
			display: block;
			text-shadow: 1px 1px #FFF;
			margin-top: 12px;
			margin-bottom: 8px;
			font-size: 25px;
			font-weight: bold;
		}
		#houseValues #houseValues-box {
			position: relative;
			top: 50px;
			float: right;
			width: 500px;
			height: 220px;
			background: rgba(203, 223, 248, 0.95);
			padding: 15px 0px 15px 50px;
            border-top-left-radius:5px;
            -moz-border-radius-topleft:5px;
            -webkit-border-top-left-radius:5px;
            border-bottom-left-radius:5px;
            -moz-border-radius-bottomleft:5px;
            -webkit-border-bottom-left-radius:5px;
		}
		#houseValues #houseValues-box h1 {
		    color:black;
		    text-align: center;
		    font-weight: bold;
        }
        #houseValues .video{
            position:relative;
            top:25px;
            left:15px;
            float:left;
        }
		#houseValues input, #houseValues select {
			font-size: 24px;
			border: 1px solid #999;
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
			border-radius: 4px;
			padding: 6px;
			font-family: Arial;
		}
		#houseValues select {
			height: 43px;
			margin-top: 1px;
			font-size: 17px;
		}
		#houseValues iframe {
			margin: 10px 10px 0 0;
			width:400px;
			height:400px;
			float: right;
		}


		#houseValues .verify {
			padding: 30px 15px 25px 30px;
		}



		#houseValues #houseValues-border {
			padding:6px;
			background: white;
			position: relative;
			margin-left: auto;
			margin-right: auto;
			-webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
			-moz-box-shadow: 0 0 10px rgba(0,0,0,0.15);
			box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
			border: 1px solid #CCC;

	/*		-moz-box-shadow: 0 0 5px #ccc;
			-webkit-box-shadow: 0 0 5px #ccc;
			box-shadow: 0 0 5px #ccc;
	*/		-moz-border-radius: 10px;
			-webkit-border-radius: 10px;
			border-radius: 10px;
		}
		#houseValues .button {
			color: white;
			text-shadow: 0 1px 0 rgba(0,0,0,1);
			background: -webkit-gradient(linear, left top, left bottom, from(#FF8875), to(#FA0000));
            background: -moz-linear-gradient(top, #FF8875, #FA0000);
            background: -o-linear-gradient(top, #FF8875, #FA0000);
			border-color: #CC7676 #D20000 #A20000 #CC7676;
		}
		.footer {
			font-size: 11px;
			color: #888;
			position: absolute;
			bottom: 0;
			left: 0;
			text-align: center;
			width: 100%;
			clear: both;
		}
CSS;
		Yii::app()->clientScript->registerCss('houseValuesCss', $css);
	}
}