<?php

class DocumentsAction extends CAction
{
    /**
     * Manages models
     */
    public function run()
    {
        if (Yii::app()->user->isGuest)
            $this->controller->redirect(array('/myAccount'));

        $this->controller->pageTitle = 'My Documents';

        // docs for transactions
        $transactionDocs = Yii::app()->db->createCommand()
            ->select("d.id, d.file_size, d.description")
            ->from("documents d")
            ->leftJoin("document_permissions p","d.id=p.document_id AND p.component_type_id IN(".ComponentTypes::SELLERS.",".ComponentTypes::BUYERS.")")
            ->leftJoin("transactions t", "t.id=p.component_id")
            ->andWhere("visibility_ma=".Documents::VISIBILITY_PUBLIC)
            ->andWhere("t.contact_id=".Yii::app()->user->id)
            ->queryAll();

        // docs for closings
        $closingDocs = Yii::app()->db->createCommand()
            ->select("d.id, d.file_size, d.description")
            ->from("documents d")
            ->leftJoin("document_permissions p","d.id=p.document_id AND p.component_type_id IN(".ComponentTypes::CLOSINGS.")")
            ->leftJoin("closings c", "c.id=p.component_id")
            ->leftJoin("transactions t", "t.id=c.transaction_id")
            ->andWhere("visibility_ma=".Documents::VISIBILITY_PUBLIC)
            ->andWhere("contact_id=".Yii::app()->user->id)
            ->queryAll();

        $rawData = $transactionDocs;
        $rawData = ($closingDocs)? CMap::mergeArray($rawData, $closingDocs) : $rawData;

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));

        $this->getController()->render('documents', array('dataProvider' => $dataProvider));
    }

}