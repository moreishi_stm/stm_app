<?php

class IndexAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($email='')
	{
        $model=new SavedHomes;
		$model->contact_id = Yii::app()->user->id;

		$view = (Yii::app()->user->isGuest) ? 'guest' : 'index' ;

		if (Yii::app()->user->isGuest) {
			$model = new LoginForm;
			$model->email = $email;
			$this->controller->layout = 'column1';
		}

        $this->controller->render($view, array('model'=>$model));
	}
}