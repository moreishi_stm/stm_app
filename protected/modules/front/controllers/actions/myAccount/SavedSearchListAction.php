<?php

class SavedSearchListAction extends CAction
{
	// when this cookie should expire
	public $cookieExpirationTime = null;

	// Maximum number of characters in cookie json value
	const MAX_COOKIE_CHAR_LENGTH = 4905;

	public function run()
	{
		if (!$this->cookieExpirationTime){
			$this->cookieExpirationTime = time() + 60*60*24*180; // 6 months
		}
		$this->controller->pageTitle = "My Saved Search List";
		$model  = new SavedHomeSearches();

		$model->contact_id = Yii::app()->user->id;
		$model = $model->search();

		return $this->controller->render('savedSearchList', array('model'=>$model));
	}
}