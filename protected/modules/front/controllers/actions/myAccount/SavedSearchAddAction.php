<?php

/**
 * Views a task.
 *
 */
class SavedSearchAddAction extends CAction {

	public function run() {
		$model = new SavedHomeSearches();
		if(!Yii::app()->user->hasMultipleBoards()) {
			$model->mls_board_id = Yii::app()->user->board->id;
		}


		if (isset($_POST['SavedHomeSearchForm'])) {
			$form = new SavedHomeSearchForm;
			$form->attributes = $_POST['SavedHomeSearchForm'];

			$model->name       = $form->name;
			$model->contact_id = $form->contact_id;

			if(Yii::app()->user->hasMultipleBoards()) {
				$model->mls_board_id = $form->mls_board_id;
			}

			$model->component_type_id = $form->component_type_id;
			$model->component_id = $form->component_id;
			$model->agent_id = $form->agent_id;
			$model->frequency  = $form->frequency;
			$model->sunday      = $form->sunday;
			$model->monday      = $form->monday;
			$model->tuesday     = $form->tuesday;
			$model->wednesday   = $form->wednesday;
			$model->thursday    = $form->thursday;
			$model->friday      = $form->friday;
			$model->saturday    = $form->saturday;
			$model->added      = date('Y-m-d H:i:s');
			$model->added_by   = Yii::app()->user->id;
			$model->updated    = date('Y-m-d H:i:s');
			$model->updated_by = Yii::app()->user->id;

			if (!$model->save()) {
				echo CActiveForm::validate($model);
                Yii::app()->end();
			} else {
				$TermValues = array();
				foreach($form->fields as $field) {
					if(is_array($form->$field)) {
						$groupId = null;
						foreach($form->$field as $submitValueSingle) {
							if(!empty($submitValueSingle) || $submitValueSingle == '0') {
								$TermValue = new TermComponentLu;
								$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
								$TermValue->component_id = $model->id;
								$TermValue->term_id = Terms::model()->getIdByName($field);
								$TermValue->value = $submitValueSingle;
								$TermValue->group_id = $groupId;

								if($groupId) {
									$TermValue->term_conjunctor = 'OR';
								}

								if($TermValue->value != null) {
									if(!$TermValue->save()) {
										header('Content-Type: application/json');
										echo CJSON::encode($TermValue->getErrors());
									} elseif(!$groupId) {
										$groupId = $TermValue->id;
									}
								}
							}
						}
					} else {
						$TermValue = new TermComponentLu;
						$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
						$TermValue->component_id = $model->id;
						$TermValue->term_id = Terms::model()->getIdByName($field);
						$TermValue->value = $form->$field;

						if($TermValue->value != null) {
							if(!$TermValue->save()) {
								header('Content-Type: application/json');
								echo CJSON::encode($TermValue->getErrors());
								return;
							}
						}
					}
					array_push($TermValues, $TermValue);
				}

                header('Content-Type: application/json');
                echo CJSON::encode(array("status"=>200,"redirect"=>"/myAccount/savedSearches"));
                Yii::app()->end();
			}
		}
		Yii::app()->end();

	}
}