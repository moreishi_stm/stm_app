<?php

class MarketUpdatesResultsAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->pageTitle = 'Market Updates Results';

		$model=new SavedHomeSearches;
		$model->contact_id = Yii::app()->user->id;

		$Criteria = $model->getCriteria(ComponentTypes::SAVED_SEARCHES, $model->id);
		$ActiveCriteria = $model->getCriteriaByTermName('status', 'Active');
		$Criteria->mergeWith($ActiveCriteria);
		$Criteria->order = 'property_last_updated DESC';
		$DataProvider = $model->getDataProvider($Criteria);

		$view = (Yii::app()->user->isGuest) ? 'guest' : '_marketUpdatesResults' ;

		if (Yii::app()->user->isGuest) {
			$model = new LoginForm;
			$this->controller->layout = 'column1';
		}

		$this->controller->render($view, array('model'=>$model,'DataProvider'=>$DataProvider));
	}
}