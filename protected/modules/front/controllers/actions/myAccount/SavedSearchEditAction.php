<?php

class SavedSearchEditAction extends CAction
{
	// when this cookie should expire
	public $cookieExpirationTime = null;
	public $multiSelectFields = array('area','zip','neighborhood','county','city');

	public $triggerElement = ".savedTriggerElemet";
	public $parentModel;

	// Maximum number of characters in cookie json value
	const MAX_COOKIE_CHAR_LENGTH = 4905;

	protected $_formInvalid = NULL;


	public function run($id = NULL)
	{
		$this->controller->title = "Saved Home Search";
		if(is_null($id)){
			/**
			 * @author jamesk apr 22 2016
			 * @TODO: verify the default variables below
			 */
			$model = new SavedHomeSearches();
			$model->component_type_id = ComponentTypes::CONTACTS;
			$model->component_id = Yii::app()->user->id;

            //check to see if there is more than 1 admin user, if so, use that user id
            $ownerAssignments = AuthAssignment::model()->findAllByAttributes(array('itemname'=>'owner'));
            $model->agent_id = $ownerAssignments[0]->userid;

            // if there is more than 1 user, see if this client has a buyer lead and find the assignment for that. If not go to default user ID #1, verify that it is a valid admin user
            if(count($ownerAssignments) > 1) {

                // check for buyer transactions
                $criteria = new CDbCriteria();
                $criteria->compare('component_type_id', ComponentTypes::BUYERS);
                $criteria->addNotInCondition('transaction_status_id', array(TransactionStatus::TRASH_ID, TransactionStatus::UNDER_CONTRACT_ID));
                $criteria->order = 'FIELD(transaction_status_id,"'.TransactionStatus::UNDER_CONTRACT_ID.'","'.TransactionStatus::NEW_LEAD_ID.'","'.TransactionStatus::NURTURING_ID.'","'.TransactionStatus::D_NURTURING_SPOKE_TO_ID.'","'.TransactionStatus::C_LEAD_ID.'","'.TransactionStatus::B_LEAD_ID.'","'.TransactionStatus::HOT_A_LEAD_ID.'") DESC';
                $criteria->limit = 1;

                $foundMatch = false;
                // see if there are existing transactions for this lead where we can use the best assignment match
                if($transactions = Transactions::model()->findAll($criteria)) {

                    foreach($transactions as $transaction) {

                        $criteriaAssignment = new CDbCriteria();
                        $criteriaAssignment->compare('component_type_id', $transaction->component_type_id);
                        $criteriaAssignment->compare('component_id', $transaction->id);
                        $criteriaAssignment->addInCondition('assignment_type_id', array(AssignmentTypes::BUYER_AGENT, AssignmentTypes::CO_BUYER_AGENT, AssignmentTypes::BUYER_ISA));
                        $criteriaAssignment->order = 'FIELD(assignment_type_id,"'.AssignmentTypes::BUYER_ISA.'","'.AssignmentTypes::CO_BUYER_AGENT.'","'.AssignmentTypes::BUYER_AGENT.'") DESC';

                        if($assignments = Assignments::model()->findAll($criteriaAssignment)) {
                            if(count($assignments) == 1) {
                                $model->agent_id = $assignments[0]->assignee_contact_id;
                                $foundMatch = true;
                            }
                            else {
                                // loop through assignments to grab any agent or ISA assignments
                                foreach($assignments as $assignment) {
                                    $model->agent_id = $assignment->assignee_contact_id;
                                    $foundMatch = true;
                                    break 2;
                                }
                            }
                        }
                    }
                }

                // if no match found through existing transactions, lead through lead routing
                if(!$foundMatch) {
                    // assign default assignee contact ID so it can be overridden if proper match found
                    if($leadPoolContactId = Yii::app()->user->settings->lead_pool_contact_id) {
                        $defaultAssigneeContactId = $leadPoolContactId;
                    }
                    else {
                        $leadRouteCriteria = new CDbCriteria();
                        $leadRouteCriteria->compare('component_type_id', ComponentTypes::BUYERS);
                        $leadRouteCriteria->order = 'id ASC';
                        $leadRoute = LeadRoutes::model()->find($leadRouteCriteria);
                        if($agents = $leadRoute->getActingAgents()) {
                            $defaultAssigneeContactId = $agents[0]->id;
                        }
                    }
                }
                // log error for any other scenarios ???
            }

			$model->contact_id = Yii::app()->user->id;
		} else {
			$model = SavedHomeSearches::model()->findByPk($id);
		}
		$TermValues = TermComponentLu::model()->getSavedSearchValues($model->id);
		$formModel = new SavedHomeSearchForm;
		$formModel->attributes = $model->attributes;
		$agentId = $model->agent_id;
		$searchableFields = MlsFieldMaps::getSearchableColumns(Yii::app()->user->board->id);

		$hasMultipleBoards = Yii::app()->user->hasMultipleBoards();
		if(!$hasMultipleBoards) {
			$formModel->mls_board_id = Yii::app()->user->board->id;
		}

		foreach($TermValues as $TermValue) {

			$termName = $TermValue->term->name;
			if(in_array($termName,$this->multiSelectFields)) {

				if(!isset($formModel->$termName) || !is_array($formModel->$termName)) {
					$formModel->$termName = array();
					array_push($formModel->$termName, $TermValue->value);
				}
				else
					array_push($formModel->$termName, $TermValue->value);
			} else
				$formModel->$termName=$TermValue->value;
		}

		if (isset($_POST['SavedHomeSearchForm'])) {

			$form = new SavedHomeSearchForm();
			$form->attributes = $_POST['SavedHomeSearchForm'];

			$model->attributes = $form->attributes;
			$model->agent_id   = $agentId;
			$model->name       = $form->name;
			$model->contact_id = $form->contact_id;
			$model->component_type_id = $form->component_type_id;
			$model->component_id = $form->component_id;
			$model->frequency  = $form->frequency;
			$model->updated    = date('Y-m-d H:i:s');
			$model->updated_by = Yii::app()->user->id;

			if (!$model->save()){
				$formModel->addErrors($model->getErrors());
			}
			else {
				$isValidated = true;

				$formFields = $form->fields;
				foreach($formFields as $field) { // go through each possible submitted field
					$termId = Terms::model()->getIdByName($field);
					$submitValue = $form->$field;

					$TermValues = TermComponentLu::model()->getSavedSearchValueByTermId($termId, $model->id);
					$count = count($TermValues);
					if($count) {
						if($count==1) {
							$TermValue = $TermValues[0];
							if(empty($submitValue) && $submitValue !='0'){ // delete if model exists and empty value submitted
								if(!$TermValue->delete())
									echo CJSON::encode($TermValue->getErrors());

							} else { // if submit value not empty
								$isArray = is_array($submitValue);
								if($isArray && !empty($submitValue)) { //array indicates multiple values, need to have group_id implementation
									$key = array_search($TermValue->value,$submitValue);
									if($key != null || $key == 0) {
										unset($submitValue[$key]);
										if($submitValue != null) {
											$groupId = $TermValue->id;
											foreach($submitValue as $submitValueSingle) {
												if(!empty($submitValueSingle) || $submitValueSingle == 0) {
													$TermValue = new TermComponentLu;
													$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
													$TermValue->component_id = $model->id;
													$TermValue->term_id = $termId;
													$TermValue->value = $submitValueSingle;
													$TermValue->group_id = $groupId;

													if($groupId) {
														$TermValue->term_conjunctor = 'OR';
													}

													if(!$TermValue->save()) {
														$isValidated = false;
													}
													elseif(!$groupId)
														$groupId = $TermValue->id;
												}
											}
										}
									} else {
										if(!$TermValue->delete()) {
											echo CJSON::encode($TermValue->getErrors());
										}
										else {
											$groupId = TermComponentLu::model()->getParentIdFromSavedSearchValueByTermId($termId, $model->id);
											foreach($submitValue as $submitValueSingle) {
												if(!empty($submitValueSingle) || $submitValueSingle == 0) {
													$TermValue = new TermComponentLu;
													$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
													$TermValue->component_id = $model->id;
													$TermValue->term_id = $termId;
													$TermValue->value = $submitValueSingle;
													$TermValue->group_id = $groupId;

													if($groupId) {
														$TermValue->term_conjunctor = 'OR';
													}

													if(!$TermValue->save()) {
														$isValidated = false;
													}
													elseif(!$groupId)
														$groupId = $TermValue->id;
												}
											}
										}
									}

								} else {
									$TermValue->value = $submitValue;
									if(!$TermValue->save()) {
										$isValidated = false;
									}
								}
							}
						} elseif($count>1) {
							// run through the models first, unsetting each submit value, then process what is left over in submitValue
							foreach($TermValues as $TermValue) {
								if($submitValue == null)
									$key = null;
								else
									$key = array_search($TermValue->value,$submitValue);

								if($key || $key===0) {
									$TermValue->value = $submitValue[$key];
									if(!$TermValue->save()) {
										$isValidated = false;
									}
									else
										unset($submitValue[$key]);
								} else {
									if(!$TermValue->delete())
										echo CJSON::encode($TermValue->getErrors());
								}
							}

							// run through whatever is left in submitValue
							if(count($submitValue)>0)
								foreach($submitValue as $submitValueSingle) { // adds these values
									// check to see if other parents exist, if not record as parent, else record as child
									$groupId = 	TermComponentLu::model()->getParentIdFromSavedSearchValueByTermId($termId, $model->id);

									$TermValue = new TermComponentLu;
									$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
									$TermValue->component_id = $model->id;
									$TermValue->term_id = $termId;
									$TermValue->value = $submitValueSingle;
									$TermValue->group_id = $groupId;

									if($groupId)
										$TermValue->term_conjunctor = 'OR';

									if(!$TermValue->save()) {
										$isValidated = false;
									}
								}
						}

					} else { // isNewRecord
						if($submitValue || $submitValue == '0') {
							if(is_array($submitValue)) {
								$groupId = TermComponentLu::model()->getParentIdFromSavedSearchValueByTermId($termId, $model->id);
								foreach($submitValue as $submitValueSingle) {
									if(!empty($submitValueSingle) || $submitValueSingle == '0') {
										$TermValue = new TermComponentLu;
										$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
										$TermValue->component_id = $model->id;
										$TermValue->term_id = $termId;
										$TermValue->value = $submitValueSingle;
										$TermValue->group_id = $groupId;

										if($groupId) {
											$TermValue->term_conjunctor = 'OR';
										}

										if(!$TermValue->save()) {
											$isValidated = false;
										}
										elseif(!$groupId)
											$groupId = $TermValue->id;
									}
								}
							} else {
								$TermValue = new TermComponentLu;
								$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
								$TermValue->component_id = $model->id;
								$TermValue->term_id = $termId;
								$TermValue->value = $submitValue;

								if(!$TermValue->save()) {
									$isValidated = false;
								}
							}
						}
					}
				}
				if($isValidated) {
					$this->controller->redirect(array(
							'savedSearches',
						)
					);
				}
			}
		}
		return $this->controller->render('savedSearchForm', array('model'=> $formModel, 'searchableFields'=>$searchableFields, 'hasMultipleBoards' => $hasMultipleBoards));
	}
}
