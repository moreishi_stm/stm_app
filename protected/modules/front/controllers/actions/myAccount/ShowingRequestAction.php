<?php

class ShowingRequestAction extends CAction
{
	public function run()
	{
		$model = new EmailMessage;

		if (Yii::app()->user->isGuest)
			$this->controller->redirect(array('/front/myAccount/index'));

		$this->controller->pageTitle = 'Showing Request';

		if (isset($_POST['EmailMessage'])) {

			$model->attributes = $_POST['EmailMessage'];
			$model->subject = 'Showing Request - '.$model->subject;
			$model->to_email_id = 1; // ************************** GET ASSIGNED TO EMAIL ID ********************************
			$model->from_email_id = Yii::app()->user->contact->getPrimaryEmailObj()->id;
			if (!$model->save())
				echo CActiveForm::validate($model);

			$message = new StmMailMessage;
			$message->init($model);
			if (Yii::app()->mail->send($message))
				Yii::app()->user->setFlash('success', 'Successfully sent your message!');
		}
		$this->controller->render('showingRequest', array('model'=>$model));
	}
}