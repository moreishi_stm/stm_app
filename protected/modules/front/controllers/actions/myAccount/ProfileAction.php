<?php

class ProfileAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		if (Yii::app()->user->isGuest)
			$this->controller->redirect(array('/myAccount'));

		$this->controller->pageTitle = 'My Profile';
		$model = new Contacts();
		$model = $model->findByPk(Yii::app()->user->id);
        $model->passwordOld = null;

		if (isset($_POST['Contacts'])) {
			$model->scenario = 'frontProfileUpdate';
			$model->attributes = $_POST['Contacts'];

			$profilePhotoImg = CUploadedFile::getInstance($model, 'profilePhoto');
			if ($profilePhotoImg) {
				$tempProfilePhoto = Yii::app()->phpThumb->create($profilePhotoImg->tempName);
				$tempProfilePhoto->resize(500); //width x height

				// Ensure path exists before trying to write out file
				$model->ensureProfilePhotoPathExists();

				// Save photo
				$tempProfilePhoto->save($model->getProfilePhotoPath());

				if (empty($model->getSettings()->profile_photo)) {
					$profileContactSetting = new SettingContactValues;
					$profileContactSetting->setAttributes(array(
						'contact_id' => $model->id,
						'setting_id' => Settings::SETTING_PROFILE_PHOTO_ID,
						'value' => 'profile_photo' . $profilePhotoImg->extensionName
					));
					$profileContactSetting->save();
				}
			}

			if ($model->passwordOld and $model->validate()) {
				$model->password = $model->passwordNew;
				if ($model->save(false)) {
					Yii::app()->user->setFlash('success', 'Successfully update Profile.');
					$model->unsetAttributes();
					$this->controller->redirect(array('profile'));
				}
			}
		}

        return $this->controller->render('profile2', array('model'=>$model));
    }

}