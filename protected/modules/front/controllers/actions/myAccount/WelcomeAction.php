<?php

class WelcomeAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'Welcome';

		if (Yii::app()->user->isGuest)
			$this->controller->redirect(array('/myAccount'));

		$this->controller->render('welcome');
	}
}