<?php

class ReferFriendAction extends CAction
{
	public function run()
	{
		$model = new EmailMessage;

		if (Yii::app()->user->isGuest)
			$this->controller->redirect(array('/front/myAccount/index'));

		$this->controller->pageTitle = 'Contact My Agent';

		if (isset($_POST['EmailMessage'])) {

			$model->attributes = $_POST['EmailMessage'];
			$model->subject = 'Refer a Friend - '.$model->subject;
			$model->to_email_id = 1; // ************************** GET ASSIGNED TO EMAIL ID ********************************
			$model->from_email_id = Yii::app()->user->contact->getPrimaryEmailObj()->id;
			if (!$model->save())
				echo CActiveForm::validate($model);

			$message = new StmMailMessage;
			$message->init($model);
			if (Yii::app()->mail->send($message))
				Yii::app()->user->setFlash('success', 'Successfully sent your message!');
		}
		$this->controller->render('referFriend', array('model'=>$model));
	}
}