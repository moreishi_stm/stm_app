<?php
Yii::import('admin_module.controllers.actions.cms.EditAction');

class StoryboardsEditAction extends EditAction
{
    protected function render(CmsContents $cmsContentEntry, CmsTemplates $template)
    {
        $this->controller->layout = 'column1';

        //@todo: temp for storyboard CSS - this will not be necessary once we get over to bootstrap 100%
        if(Yii::app()->params['layoutDirectory'] == 'storyboard') {
            $this->registerStoryboardCss();
        }

        if (Yii::app()->user->isGuest) {
            $model = new LoginForm;
            $this->controller->layout = 'column1';
            $this->controller->render('guest', array('model'=>$model));

        } else {
            $this->controller->pageTitle = 'Edit Storyboard';

            $cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('stm_app_root.assets.css'), true, -1, FrontModule::REFRESH_CSS_ASSETS);
            Yii::app()->clientScript->registerCssFile($cssAssetsUrl . DS . 'style.cms.css');


            $commentDataProvider
                = new CActiveDataProvider('CmsComment', array('criteria' => array('condition' => 'cms_content_id = :cms_content_id',
                                                                                  'params'    => array(':cms_content_id' => $cmsContentEntry->id),),));

            $this->controller->render('storyboardsEdit',
                array('model'     => $cmsContentEntry,
                      'Template'            => $template,
                      'commentDataProvider' => $commentDataProvider)
            );
        }
    }

    protected function processPostSaveOperations(CmsContents $cmsContentEntry)
    {
        $this->controller->redirect(
            array('storyboards')
        );
    }

    protected function registerStoryboardCss() {
        Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/style.grid.css');
        Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/style.colors.css');

        $css = <<<CSS
    table#cms-editor input{
        font-size: 20px;
        margin-bottom: 10px;
        background: white;
        padding: 5px 2px 5px 4px;
        height: 100%;
        border: 1px solid #BBB;
        padding: 4px 2px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    input, textarea, input:invalid, input:required, textarea:required, select {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
    }

    input:hover, input:focus, textarea:hover, textarea:focus, select:hover, select:focus {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    }

    button.submit {
        font-size: 11px;

        background: #fceec7;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjZWVjNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjOWEzMzkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
        background: -moz-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fceec7), color-stop(100%, #c9a339));
        background: -webkit-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -o-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -ms-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: linear-gradient(to bottom, #fceec7 0%, #c9a339 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fceec7', endColorstr='#c9a339', GradientType=0);
        -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#fceec7', endColorstr='#c9a339')"; /* IE8 */
        border: 1px solid;
        border-color: #CC9F70 #B36310 #B36310 #c68f56;
        cursor: pointer;

        color: #333;
        text-transform: uppercase;
        padding: 10px 28px;
        -webkit-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);

        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;

        position: relative;
        display: inline-block;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        text-decoration: none !important;
        font-weight: 700;
        outline: 0 none;
        text-align: center;
        margin: 2px;
        min-height: 14px;
        min-width: 8px;
        white-space: pre-line;
        vertical-align: baseline;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.7);
    }
CSS;
        Yii::app()->clientScript->registerCss('forgotPasswordStoryboardCss', $css);
    }
}