<?php
class SavedSearchPreviewAction extends CAction
{
	public function run($id)
    {
		$model =  new SavedHomeSearches();
		$model = $model->findByPk($id);

		$this->controller->title = 'Home Search Preview';
		$Criteria = $model->getCriteria(ComponentTypes::SAVED_SEARCHES, $model->id);
		$ActiveCriteria = $model->getCriteriaByTermName('status', 'Active');
		$Criteria->mergeWith($ActiveCriteria);
		Yii::app()->clientScript->registerCssFile(
			$this->getController()->getModule()->getCssAssetsUrl() . DS . 'style.homes.css'
		);
		return $this->controller->render('savedSearchPreview', array('htmlString'=>$model->createEmailHtml()));
	}
}
