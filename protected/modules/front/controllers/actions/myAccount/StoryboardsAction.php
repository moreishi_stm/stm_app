<?php

class StoryboardsAction extends CAction
{
	public function run()
	{
        //@todo: temp for storyboard CSS - this will not be necessary once we get over to bootstrap 100%
        if(Yii::app()->params['layoutDirectory'] == 'storyboard') {
            $this->registerStoryboardCss();
        }

        if (Yii::app()->user->isGuest) {
            $model = new LoginForm;
            $this->controller->layout = 'column1';
            $this->controller->render('guest', array('model'=>$model));

        } else {

            $DataProvider = new CActiveDataProvider(CmsContents::model()->byPageType(array(CmsContents::TYPE_LISTING_SB,CmsContents::TYPE_BUYER_SB))->byContactId(Yii::app()->user->id),
                array(
                'criteria' => array(
                    'order' => 'id desc',
                ),
                'pagination'=>array(
                    'pageSize'=>5,
                ),
                ));

            $this->controller->render('storyboards', array('DataProvider'=>$DataProvider));
        }
	}

    protected function registerStoryboardCss() {
        Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/style.grid.css');

        $css = <<<CSS
    #login {
        margin-top: 80px;
    }
    #login #login-container {
        margin: 40px auto 60px;
        background: none repeat scroll 0% 0% #F5F5F5;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
        border: 1px solid #DDD;
        border-radius: 6px;
        padding: 30px 30px 60px;
        width: 500px;
    }
    #login h1, h1.green {
        font-size: 20px;
        color: #5BA71B;
        text-align: center;
        line-height: 1;
    }
    #login h2 {
        font-size: 22px;
        text-align: center;
        font-weight: bold;
    }
    #login label {
        width: 145px;
        display: inline-block;
        text-align: right;
        font-weight: bold;
    }
    #login form input{
        width: 300px;
        font-size: 20px;
        margin-bottom: 10px;
        padding: 5px 2px 5px 4px;
        height: 100%;
        border: 1px solid #BBB;
        padding: 4px 2px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    input, textarea, input:invalid, input:required, textarea:required, select {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
    }

    input:hover, input:focus, textarea:hover, textarea:focus, select:hover, select:focus {
        -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2), 0 0 3px rgba(0, 0, 0, 0.2);
    }
    #login .row.submit {
        height: 25px;
        text-align: left;
        position: relative;
        left: 145px;
    }
    #login-form-button {
        font-size: 11px;

        background: #fceec7;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjZWVjNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjOWEzMzkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
        background: -moz-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fceec7), color-stop(100%, #c9a339));
        background: -webkit-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -o-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: -ms-linear-gradient(top, #fceec7 0%, #c9a339 100%);
        background: linear-gradient(to bottom, #fceec7 0%, #c9a339 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fceec7', endColorstr='#c9a339', GradientType=0);
        -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#fceec7', endColorstr='#c9a339')"; /* IE8 */
        border: 1px solid;
        border-color: #CC9F70 #B36310 #B36310 #c68f56;
        cursor: pointer;

        color: #333;
        text-transform: uppercase;
        padding: 10px 28px;
        -webkit-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        -moz-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);

        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;

        position: relative;
        display: inline-block;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        text-decoration: none !important;
        font-weight: 700;
        outline: 0 none;
        text-align: center;
        margin: 2px;
        min-height: 14px;
        min-width: 8px;
        white-space: pre-line;
        vertical-align: baseline;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.7);
    }
    div.errorMessage {
        width: 300px;
        margin-left: 150px;
        margin-bottom: 10px;
        clear: both;
        color: #FF5757;
        font-weight: bold;
        font-size: 11px;
    }
    input.error {
        background: none repeat scroll 0% 0% #FFB8B8;
    }
CSS;
        Yii::app()->clientScript->registerCss('forgotPasswordStoryboardCss', $css);
    }
}