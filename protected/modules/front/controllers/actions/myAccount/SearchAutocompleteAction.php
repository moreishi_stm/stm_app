<?php
class SearchAutocompleteAction extends CAction {

    public function run() {

        $tableName = Yii::app()->user->board->prefixedName;
        $model = MlsProperties::model($tableName);

        $this->processAjaxRequest($model);
    }

    protected function processAjaxRequest(MlsProperties $model)
    {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['term'])) {
//                $neighborhood = $_POST['term'];
                $collection = array();

                //$data = $model->listDistinctNeighborhood($_POST['term']);
//                $tableName = Yii::app()->user->board->prefixedName;
//
//                $result = Yii::app()->stm_mls->createCommand()
//                    ->selectDistinct('LOWER(common_subdivision) neighborhood')
//                    ->from($tableName)
//                    ->where("common_subdivision like :neighborhood AND common_subdivision !=''", array(':neighborhood', "%$neighborhood%"))
//                    ->setUnion("SELECT DISTINCT LOWER(legal_subdivision) neighborhood FROM {$tableName} WHERE legal_subdivision like '%$neighborhood%' AND (`legal_subdivision` != `common_subdivision` OR `common_subdivision` IS NULL) AND legal_subdivision !=''")
//                    ->queryColumn();
//
//                $list = array_unique($result);

                // legal subdivision
                $Criteria = new CDbCriteria;
                $Criteria->select = 'legal_subdivision';
                $Criteria->condition = 'legal_subdivision like :legal_subdivision AND status="Active"';
                $Criteria->params = array(':legal_subdivision'=>'%'.trim(Yii::app()->request->getPost('term')).'%');
                $Criteria->distinct = true;
                $Properties = $model->findAll($Criteria);

                $allSubdivisions = array();
                foreach($Properties as $Property)
                    array_push($allSubdivisions, $Property->legal_subdivision);

                // common subdivision
                $Criteria = new CDbCriteria;
                $Criteria->select = 'common_subdivision';
                $Criteria->condition = 'common_subdivision like :common_subdivision AND status="Active"';
                $Criteria->params = array(':common_subdivision'=>'%'.trim(Yii::app()->request->getPost('term')).'%');
                $Criteria->distinct = true;
                $Properties = $model->findAll($Criteria);

                foreach($Properties as $Property) {
                    if(!in_array($Property->common_subdivision, $allSubdivisions))
                        array_push($allSubdivisions, $Property->common_subdivision);
                }
                //select distinct legal subdivision, then select distinct common sub where it doesn't exist in legal
                //put both in array an the merge and get unique... or check to see if it exists each time?

                foreach($allSubdivisions as $row)
                    array_push($collection, array(
                            'value'=>$row,
                            'text'=>ucwords(strtolower($row)),
                        ));

                echo CJSON::encode($collection);
            }
            Yii::app()->end();
        }
    }
}
