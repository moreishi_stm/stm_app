<?php

class MarketUpdatesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'Market Updates';

		$model=new SavedHomeSearches;
		$model->contact_id = Yii::app()->user->id;

		$view = (Yii::app()->user->isGuest) ? 'guest' : '_marketUpdates' ;

		if (Yii::app()->user->isGuest) {
			$model = new LoginForm;
			$this->controller->layout = 'column1';
		}

		$this->controller->render($view, array('model'=>$model));
	}
}