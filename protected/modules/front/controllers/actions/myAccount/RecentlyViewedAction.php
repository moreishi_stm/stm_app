<?php

class RecentlyViewedAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'Recently Viewed';

		$model=new ViewedPages('search');
		$model->contact_id = Yii::app()->user->id;
		$model->page_type_ma = ViewedPages::HOME_PAGES;

		$view = (Yii::app()->user->isGuest) ? 'guest' : '_recentlyViewed' ;

		$viewContent = array('model'=>$model);
		if (Yii::app()->user->isGuest) {
			$model = new LoginForm;
			$this->controller->layout = 'column1';
		}

		$this->controller->render($view, array('model'=>$model));
	}
}