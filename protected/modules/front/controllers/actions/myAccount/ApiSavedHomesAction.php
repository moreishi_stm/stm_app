<?php

class ApiSavedHomesAction extends CAction
{
	// when this cookie should expire
	public $cookieExpirationTime = null;

	// Maximum number of characters in cookie json value
	const MAX_COOKIE_CHAR_LENGTH = 4905;

	/**
	 * Manages models
	 */
	public function run()
	{
		if (!$this->cookieExpirationTime)
			$this->cookieExpirationTime = time() + 60*60*24*180; // 6 months

		$action = Yii::app()->request->getPost('action');

			if ($action=='add' && Yii::app()->user->isGuest)
				$result = $this->guestAddHome();

			if ($action=='remove' && Yii::app()->user->isGuest)
				$result = $this->guestRemoveHome();

			if ($action=='add' && Yii::app()->user->id)
				$result = $this->userAddHome();

			if ($action=='remove' && Yii::app()->user->id)
				$result = $this->userRemoveHome();

		echo CJSON::encode($result);
		Yii::app()->end();
	}

	/**
	 *
	 *
	 * @return array
	 * 4/4/2016
	 * @author RapidMod.com
	 * @author 813.330.0522
	 *
	 * @todo is this broken?, the variables dont look they are getting set
	 */
	protected function guestAddHome() {
		// If the cookie exists then lets pre-populate the tracking data array, otherwise create a new instance of it
		if (Yii::app()->request->hasCookie(SavedHomes::COOKIE_KEY)) {
			if (SavedHomes::isHomeSaved($listingId, $contactId))
				return array('results' => false);

			$cookieJSON = Yii::app()->request->cookies[SavedHomes::COOKIE_KEY]->value;
			$homesSavedData  = (CJSON::decode($cookieJSON))? CJSON::decode($cookieJSON) : array();

			if ($homesSavedData == null)
				$homesSavedData = array();

			// If the maximum length of the cookie is reached then pop off the first element (FIFO)
			if (mb_strlen($cookieJSON) >= self::MAX_COOKIE_CHAR_LENGTH)
				array_shift($homesSavedData);

			$homesSavedData[Yii::app()->request->getPost('listing_id')] = array('ip'=>Yii::app()->stmFunctions->getVisitorIp(),'added'=>date('Y-m-d H:i:s'));
		}

		Yii::app()->request->setCookie(SavedHomes::COOKIE_KEY, CJSON::encode($homesSavedData), $this->cookieExpirationTime);

		return array('key' => SavedHomes::COOKIE_KEY, 'data' => $homesSavedData,'exp' => $this->cookieExpirationTime);
	}

	protected function guestRemoveHome() {
		if (Yii::app()->request->hasCookie(SavedHomes::COOKIE_KEY)) {
			$cookieJSON = Yii::app()->request->cookies[SavedHomes::COOKIE_KEY]->value;
			$homesSavedData  = (CJSON::decode($cookieJSON))? CJSON::decode($cookieJSON) : array();
			if (array_key_exists(Yii::app()->request->getPost('listing_id'), $homesSavedData))
				unset($homesSavedData[Yii::app()->request->getPost('listing_id')]);

			Yii::app()->request->setCookie(SavedHomes::COOKIE_KEY, CJSON::encode($homesSavedData), $this->cookieExpirationTime);
			return array('results' => 'removed guest');
		}
	}

	protected function userAddHome() {
		$listingId = Yii::app()->getRequest()->getPost('listing_id');
        $mlsBoardId = Yii::app()->getRequest()->getPost('mls_board_id');
		$contactId = (Yii::app()->request->getPost('contact_id'))? Yii::app()->request->getPost('contact_id') : Yii::app()->user->id;
        // see if record exists
        $savedHomeEntry = SavedHomes::model()->skipSoftDeleteCheck()->findByAttributes(array(
                'listing_id' => $listingId,
                'contact_id' => $contactId,
            ));
		if (!$savedHomeEntry) {
			$savedHomeEntry = new SavedHomes;
            //check to see if it already exists
            if(!$savedHomeEntry->countByAttributes(array('contact_id'=> $contactId, 'listing_id'=>$listingId))) {
                $savedHomeEntry->contact_id = $contactId;
                $savedHomeEntry->listing_id = $listingId;
                $savedHomeEntry->mls_board_id = $mlsBoardId;
                $savedHomeEntry->ip = Yii::app()->stmFunctions->getVisitorIp();
                $savedHomeEntry->added = new CDbExpression('NOW()');
                $savedHomeEntry->added_by = Yii::app()->user->id;
                $savedHomeEntry->is_deleted = 0;
                if(!$savedHomeEntry->save()) {
                    return array('results' => 'failed new');
                }
            }

            return array('results' => 'true new');
		}

		if ($savedHomeEntry->is_deleted == 1) {
			$savedHomeEntry->is_deleted = 0;
			if(!$savedHomeEntry->save()) {
				return array('results' => 'failed update');
			}
			return array('results' => 'true updated');
		} else {
			return array('results' => 'No Action Already Saved');
		}
	}
	/**
	 *
	 * 4/4/2016 added support of multiple row delete and return response
	 * @author RapidMod.com
	 * @author 813.330.0522
	 *
	 */
	protected function userRemoveHome() {
		$listingId = Yii::app()->getRequest()->getPost('listing_id');
        $mlsBoardId = Yii::app()->getRequest()->getPost('mls_board_id');
		$contactId = (Yii::app()->user->isGuest) ? null : Yii::app()->user->id;
		$userHasHomeSaved = SavedHomes::isHomeSaved($listingId, $contactId, $mlsBoardId);
		if ($userHasHomeSaved && !Yii::app()->user->isGuest) {
			$criteria = new CDbCriteria();
			$criteria->compare('listing_id', $listingId);
			$criteria->compare('contact_id', $contactId);
			if ($mlsBoardId) {
				$criteria->addCondition('mls_board_id=:mls_board_id OR mls_board_id IS NULL');
				$criteria->params[':mls_board_id'] = $mlsBoardId;
			}
			$savedHomeEntry = SavedHomes::model()->findAll($criteria);
			$record_count = 0;
			foreach($savedHomeEntry as $saved_home){
				$saved_home->delete();
				$record_count++;
			}
			return array('response' => 200,"deleted"=>(int)$record_count);
		}

	}
}