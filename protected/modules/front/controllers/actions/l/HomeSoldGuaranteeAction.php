<?php

class HomeSoldGuaranteeAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = '59 Day Home Sold Guarantee';
        $this->controller->render('homesoldguarantee');
    }
}