<?php

class TripleGuaranteeAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'Triple Guarantee Program - Christine Lee Team, Jacksonville & Ponte Vedra, FL Homes for Sale';
        $this->controller->render('tripleguarantee');
    }
}