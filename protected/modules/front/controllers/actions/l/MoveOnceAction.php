<?php

class MoveOnceAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'Move Once Program';
        $this->controller->render('moveonce');
    }
}