<?php

class WeBuyHomesNowJaxAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'We Buy Homes Now in Jacksonville';
        $this->controller->render('webuyhomesnowjax');
    }
}