<?php

class FreeSellerReportAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->pageTitle = 'Free Seller Reports - Christine Lee Team, Jacksonville & Ponte Vedra, FL Homes for Sale';
        $this->controller->render('freesellerreport');
    }
}