<?php
Yii::import('admin_module.components.StmDialer.Lists.Factory', true);
Yii::import('admin_module.controllers.DialerController');
Yii::import('admin_module.components.StmPlivo.plivo', true);
/**
 * Plivo 2 Controller
 *
 * Used to handle Plivo callbacks
 */
class PlivoController extends FrontController
{
    const MAX_CALL_COUNT = 12;
    /**
     * Log Location
     *
     * Where to store logs
     * @var string Directory path
     */
    const LOG_LOCATION = '/var/log/plivo';

    /**
     * Flag for logging before action
     *
     * Used for debugging purposes
     */
    const LOG_BEFORE_ACTION = false;

    /**
     * Response
     *
     * Response object for Plivo
     * @var \StmPlivoFunctions\Response
     */
    protected $_response;

    /**
     * Node URL
     *
     * The URL for the Admin Socket Server
     * @var string Admin Socket server URL
     */
    protected $_nodeUrl;

    /**
     * Init
     *
     * Things that happen before each controller action
     * @return void
     */
    public function init()
    {
        // New Plivo Response
//        $this->_response = new \StmPlivoFunctions\Response();

        // Chain parent init
        parent::init();
    }

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        if(self::LOG_BEFORE_ACTION) {
            // Log things for informational purposes
            $this->_logThings();
        }

        // Determine what URL we will be using for the Admin Socket Server
        $this->_nodeUrl = YII_DEBUG ? 'http://dev-nicole.christineleeteam.com:8080' : 'http://node-admin.seizethemarket.net';

        // New Plivo Response
        $this->_response = new \StmPlivoFunctions\Response();

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Ring Action
     *
     * Called by Plivo when ringing begins
     * @return void
     */
    public function actionRing()
    {
        //Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI Ring Action - see all post data.'.PHP_EOL, CLogger::LEVEL_ERROR); ...

        // Example of POST data that is passed:
        //[Direction] => outbound
        //[From] => +19043301885
        //[To] => 19043433200
        //[RequestUUID] => 8aba2a63-9532-45f3-8a34-ebfd486cc328
        //[CallUUID] => a0d6c662-9a07-11e4-b1de-71f618784e1e
        //[CallStatus] => ringing
        //[Event] => Ring
        //[id] => 1
    }

    /**
     * Answer Confirm Start Action
     *
     * Called by Plivo when the phone is answered initially to start session. This confirms that voicemail didn't pickup as it requires user input addHangup.
     * @param $id integer Call list ID
     * @return void
     */
    public function actionAnswerConfirmStart($id)
    {
        try {
            $callSession = CallSessions::model()->findByAttributes(array('request_uuid'=>Yii::app()->request->getPost('RequestUUID')));
            $userId = Yii::app()->request->getParam('user_id');
            $callerIdName = Yii::app()->request->getParam('callerIdName');
            $callerIdNumber = Yii::app()->request->getParam('callerIdNumber');

            // Set header with call list ID
            header('x-stm-calllistid: ' . $callSession->call_list_id);
            header('x-stm-clientid: ' . Yii::app()->user->clientId);
            header('x-stm-accountid: ' . Yii::app()->user->accountId);

            // If we were unable to create the new call session
            if(!$callSession) {
                sleep(3);
                // try one more time.
                $callSession = CallSessions::model()->findByAttributes(array('request_uuid'=>Yii::app()->request->getPost('RequestUUID')));
                if(!$callSession) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call session not found after 2 attempts. Request UUID#: '.Yii::app()->request->getPost('RequestUUID'), CLogger::LEVEL_ERROR);
                    throw new Exception('Unable to find call session');
                }
            }

            //@todo: call session is already started by now, find existing one. the call session is created and billed. save plivo data... use plivo's uuid as PK or our own... will us our own for now - ASK NICOLE
            $callSession->setAttributes(array(
                'direction' => Yii::app()->request->getPost('Direction'),
                'from_phone' => Yii::app()->request->getPost('From'),
                'a_leg_uuid' => Yii::app()->request->getPost('ALegUUID'),
                'bill_rate' => Yii::app()->request->getPost('BillRate'),
                'to_phone' => Yii::app()->request->getPost('To'),
                'call_uuid' => Yii::app()->request->getPost('CallUUID'),
                'a_leg_request_uuid' => Yii::app()->request->getPost('ALegRequestUUID'),
                'call_status' => Yii::app()->request->getPost('CallStatus'),
                'event' => Yii::app()->request->getPost('Event'), //will most likely be "StartApp"
            ));
            if(!$callSession->save()) {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call session did not save. Error Message:' . print_r($callSession->getErrors(), true).PHP_EOL.' Attributes: '.print_r($callSession->attributes, true), CLogger::LEVEL_ERROR);
            }

            // get caller ID & number of lines from session.
            $numToCall = $callSession->number_lines;
            $callerIdNumber = $callSession->caller_id_number;
            $callerIdName = $callSession->caller_id_name;

            // $numToCall = Yii::app()->request->getParam('num_to_call') ? Yii::app()->request->getParam('num_to_call') : 3;
            // Wait for user to press 1 to confirm start of session
            $getDigits = $this->_response->addGetDigits(array(
                'action'        =>  $this->_nodeUrl . '/plivo/answer?id=' . $id.'&user_id='.$userId.'&site='.urlencode(StmFunctions::getSiteUrl()),
                'redirect'      =>  'true',
                'timeout'       =>  20, //seconds
            ));

            // Add speak to get digits to the user knows what to do
            $getDigits->addSpeak('Welcome. Press 1 to start your call session.');

            // Notifies user that session is ending.
            $this->_response->addSpeak('Input not received. Your session is ending. Goodbye.');

            $this->_sendResponse();
        }
        catch(Exception $e) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Plivo Answer Action had an error: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Answer Action
     *
     * Called by Plivo when the phone is answered
     * @id integer call list ID
     * @return void
     */
    public function actionAnswer($id)
    {
        // Get number of people to call
        $userId = Yii::app()->request->getParam('user_id');
        // $digit = Yii::app()->request->getPost('Digits'); //@todo: right now we are doing anything with this. Treating all digits as 1 for continue.

        // Set header with call list ID
        header('x-stm-calllistid: ' . $id);
        header('x-stm-clientid: ' . Yii::app()->user->clientId);
        header('x-stm-accountid: ' . Yii::app()->user->accountId);

        $callSession = CallSessions::model()->findByAttributes(array('request_uuid'=>Yii::app()->request->getPost('RequestUUID')));

        if(!$callSession) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Call session not found. Critical error. See Post & Header Data below.');
        }

        $callerIdName = $callSession->caller_id_name;
        $callerIdNumber = $callSession->caller_id_number;
        $numToCall = $callSession->number_lines;
        $filters['maxDialerCallCount'] = $callSession->filter_max_call_count; //@todo: prolly not needed after 4/2/16 Queue Logic Refactor
        $filters['maxDialerCallDays'] = $callSession->filter_max_call_days; //@todo: prolly not needed after 4/2/16 Queue Logic Refactor

        // check if any calls that are Ringing or Answered, there shouldn't be. If found, report error and update to No Answer. A bit of garbage collection. - //@todo: still relevant as of 4/6/16 - CLee
        $answeredRingingClpIds = Yii::app()->db->createCommand("SELECT id FROM call_list_phones WHERE call_list_id={$id} AND status IN ('".CallListPhones::ANSWERED."','".CallListPhones::RINGING."') AND updated_by={$userId}")->queryColumn();
        if(!empty($answeredRingingClpIds)) {

            // log error
            $answeredRingingClpIdsString = implode(',',$answeredRingingClpIds);
            $answeredRingingClpData = Yii::app()->db->createCommand("SELECT * FROM call_list_phones WHERE id IN ({$answeredRingingClpIdsString})")->queryAll();
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: There were still callListPhones in actionAnswer() with Answered or Ringing status that were changed to No Answer. Investigate immediately. CallListPhoneIds: '.$answeredRingingClpIdsString.PHP_EOL.'CallListPhones Data: '.print_r($answeredRingingClpData, true), CLogger::LEVEL_ERROR);

            // change to No Answer
            Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."', updated=NOW(), updated_by={$userId} WHERE call_list_id={$id} AND id IN({$answeredRingingClpIdsString})")->execute();
        }

        // Retrieve calls to make, split them in to groups of 3 for simultaneous dialing
        $o = \StmDialer\Lists\Factory::getInstance($callSession->callList->preset_ma, $callSession->call_list_id);
        $callListPhones = $o->getQueueBySortOrder($id, $userId, $filters, $numToCall, $nextCallMode=true);

        if(count($callListPhones) > $numToCall) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Next segment call count of '.count($callListPhones).' is greater than the phone line limit of'.$numToCall.'.');
        }

        // Get phone IDs for this dial iteration
        $ids = array();
        foreach($callListPhones as $key => $callListPhone) {
            $ids[] = $callListPhone['call_list_phone_id'];
        }

        // If we have calls to make, add them
        if(count($callListPhones)) {

            // populate basic dialer add dial data
            $addDialData = array(
                //'action'            =>  $this->_nodeUrl.'/plivo/dial?id='.$id,
                'callbackUrl'       =>  $this->_nodeUrl . '/plivo/callback?id=' . $id .'&user_id=' . $userId . '&ids=' . implode('-', $ids).'&callerIdName='.$callerIdName.'&callerIdNumber='.$callerIdNumber.'&site='.urlencode(StmFunctions::getSiteUrl()),
                'timeout'           =>  (CallLists::isLongRingByListId($id))? 50 : 50,
            );

            // check to see if dialer caller ID phone number settings exist and apply to add dial array
            if($callerIdNumber) {
                if(is_numeric($callerIdNumber) && strlen($callerIdNumber) == 10) {
                    $addDialData['callerId'] = '+1'.$callerIdNumber; //9043029891 CLee's number
                }
            }

            // check to see if dialer caller ID name settings exist and apply to add dial array
            if($callerIdName) {
                if($callerIdName) {
                    $addDialData['callerName'] = $callerIdName;
                }
            }

            // Create dial
            $dial = $this->_response->addDial($addDialData);

            // Add each number to the call list
            foreach($callListPhones as $callListPhone) {
                $dial->addNumber('1' . $callListPhone['phone']);
            }

            $callListPhoneIds = implode(',', $ids);

            // sets the dialing numbers to status "Ringing" and increments call count in call list
            Yii::app()->db->createCommand("UPDATE call_list_phones SET status='Ringing', updated=NOW(), updated_by=$userId, call_count=call_count+1 WHERE call_list_id={$id} AND id IN($callListPhoneIds)")->execute();

            // Wait for user to press 1 to continue
            $getDigits = $this->_response->addGetDigits(array(
                'action'        =>  $this->_nodeUrl . '/plivo/answer?id=' . $id .'&num_to_call='.$numToCall.'&user_id='.$userId.'&callerIdName='.$callerIdName.'&callerIdNumber='.$callerIdNumber.'&site='.urlencode(StmFunctions::getSiteUrl()),
                'redirect'      =>  'true',
                'timeout'       =>  300, // 5 minutes
            ));

            // Add speak to get digits to the user knows what to do
            $getDigits->addSpeak('Press 1 to continue, or your session will end in five minutes.');
        }
        else {
            // Message of completion
            $this->_response->addSpeak('Congratulations. All calls in your list are complete, your session is ending, goodbye.');

            // A wait is necessary for unknown reasons (the call will drop if this isn't here)
            $this->_response->addWait(array(
                'length' => 3
            ));
        }

        // Send the response
        $this->_sendResponse();
    }

    /**
     * DialNext Action
     *
     * Handles the ongoing dialing. If Dial status is no answer, it keeps on dialing and does not ask to press 1 to continue.
     * @id integer Call Session ID
     * @return void
     */
    //@todo: THIS FUNCTION IS NOT READY - still under construction
    public function actionDialNext($id)
    {
        $callSession = CallSessions::model()->findByPk($id);

        if(!$callSession) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Call session not found. Critical error. See Post & Header Data below.');
        }

        /**
         *
         * This is pretty much copied right from actionAnswer() - //@todo: ask Nicole on how to re-use this. - CLee 4/7/16
         *
         */

        // check if any calls that are Ringing or Answered, there shouldn't be. If found, report error and update to No Answer. A bit of garbage collection. - //@todo: still relevant as of 4/6/16 - CLee
        $answeredRingingClpIds = Yii::app()->db->createCommand("SELECT id FROM call_list_phones WHERE call_list_id={$callSession->call_list_id} AND status IN ('".CallListPhones::ANSWERED."','".CallListPhones::RINGING."') AND updated_by={$callSession->contact_id}")->queryColumn();
        if(!empty($answeredRingingClpIds)) {

            // log error
            $answeredRingingClpIdsString = implode(',',$answeredRingingClpIds);
            $answeredRingingClpData = Yii::app()->db->createCommand("SELECT * FROM call_list_phones WHERE id IN ({$answeredRingingClpIdsString})")->queryAll();
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: There were still callListPhones in actionAnswer() with Answered or Ringing status that were changed to No Answer. Investigate immediately. CallListPhoneIds: '.$answeredRingingClpIdsString.PHP_EOL.'CallListPhones Data: '.print_r($answeredRingingClpData, true), CLogger::LEVEL_ERROR);

            // change to No Answer
            Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."', updated=NOW(), updated_by={$callSession->contact_id} WHERE call_list_id={$callSession->call_list_id} AND id IN({$answeredRingingClpIdsString})")->execute();
        }

        // Retrieve calls to make, split them in to groups of 3 for simultaneous dialing
        $o = \StmDialer\Lists\Factory::getInstance($callSession->callList->preset_ma, $callSession->call_list_id);
        $callListPhones = $o->getQueueBySortOrder($callSession->call_list_id, $callSession->contact_id, $filters=array(), $callSession->number_lines, $nextCallMode=true);

        if(count($callListPhones) > $callSession->number_lines) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Next segment call count of '.count($callListPhones).' is greater than the phone line limit of'.$callSession->number_lines.'.');
        }

        // Get phone IDs for this dial iteration
        $callListPhoneIdsArray = array();
        foreach($callListPhones as $key => $callListPhone) {
            $callListPhoneIdsArray[] = $callListPhone['call_list_phone_id'];
        }

        // If we have calls to make, add them
        if(count($callListPhones)) {

            // populate basic dialer add dial data
            $addDialData = array(
                //'action'            =>  $this->_nodeUrl.'/plivo/dial?id='.$callSession->call_list_id,
                'callbackUrl'       =>  $this->_nodeUrl . '/plivo/callback?id=' . $callSession->call_list_id .'&user_id=' . $callSession->contact_id . '&ids=' . implode('-', $callListPhoneIdsArray).'&callerIdName='.$callSession->caller_id_name.'&callerIdNumber='.$callSession->caller_id_number.'&site='.urlencode(StmFunctions::getSiteUrl()),
                'timeout'           =>  (CallLists::isLongRingByListId($callSession->call_list_id))? 50 : 50,
            );

            // check to see if dialer caller ID phone number settings exist and apply to add dial array
            if($callSession->caller_id_number) {
                if(is_numeric($callSession->caller_id_number) && strlen($callSession->caller_id_number) == 10) {
                    $addDialData['callerId'] = '+1'.$callSession->caller_id_number;
                }
            }

            // check to see if dialer caller ID name settings exist and apply to add dial array
            if($callSession->caller_id_name) {
                if($callSession->caller_id_name) {
                    $addDialData['callerName'] = $callSession->caller_id_name;
                }
            }

            // Create dial
            $dial = $this->_response->addDial($addDialData);

            // Add each number to the call list
            foreach($callListPhones as $callListPhone) {
                $dial->addNumber('1' . $callListPhone['phone']);
            }

            $callListPhoneIds = implode(',', $callListPhoneIdsArray);

            // sets the dialing numbers to status "Ringing" and increments call count in call list //@todo: Ideally this occurs in callback so if a call fails it doesn't show as ringing??? but if it fails will it get removed from the list? Hmm...
            Yii::app()->db->createCommand("UPDATE call_list_phones SET status='Ringing', updated=NOW(), updated_by={$callSession->contact_id}, call_count=call_count+1 WHERE call_list_id={$callSession->call_list_id} AND id IN($callListPhoneIds)")->execute();

            // Wait for user to press 1 to continue
            $getDigits = $this->_response->addGetDigits(array(
                    'action'        =>  $this->_nodeUrl . '/plivo/dialNext?id=' . $callSession->call_list_id .'&num_to_call='.$callSession->number_lines.'&user_id='.$callSession->contact_id.'&callerIdName='.$callSession->caller_id_name.'&callerIdNumber='.$callSession->caller_id_number.'&site='.urlencode(StmFunctions::getSiteUrl()),
                    'redirect'      =>  'true',
                    'timeout'       =>  300, // 5 minutes
                ));

            $getDigits->addSpeak('Call complete. You have 5 minutes to start your next call or your session will time out.');
            $getDigits->addWait(array('length' => 295)); //takes into account the time it takes to speak
            $getDigits->addSpeak('Your session has timed out, goodbye.');
        }
        else {
            // Message of completion
            $this->_response->addSpeak('Congratulations. All calls in your list are complete, your session is ending, goodbye.');

            // A wait is necessary for unknown reasons (the call will drop if this isn't here)
            $this->_response->addWait(array(
                    'length' => 3
                ));
        }

        $this->_sendResponse();
    }

    /**
     * Transfer Return to Answer
     * Used to transfer A leg to this so that it can re-route it back to calling the queue, while hanging up on B leg
     * @param $id integer call_list_id
     */
    public function actionTransferReturnToAnswer($id)
    {
        // Set header with call list ID
        header('x-stm-calllistid: ' . $id);
        header('x-stm-clientid: ' . Yii::app()->user->clientId);
        header('x-stm-accountid: ' . Yii::app()->user->accountId);

        // Get number of people to call
        $userId = Yii::app()->request->getParam('user_id');

        // Wait for user to press 1 to continue
        $getDigits = $this->_response->addGetDigits(array(
            'action'        =>  $this->_nodeUrl . '/plivo/answer?id=' . $id.'&user_id='.$userId.'&site='.urlencode(StmFunctions::getSiteUrl()),
            'redirect'      =>  'true',
            'timeout'       =>  300,
        ));

        // Add speak to get digits to the user knows what to do
        $getDigits->addSpeak('Press 1 to continue, or your session will end in five minutes.');

        $this->_sendResponse();
    }

    /**
     * Transfer Hangup
     *
     * Hangups up on whatever call was entered here
     * @param $id integer call ID
     */
    public function actionTransferHangup()
    {
        //$callDisposition variable will get passed form dialer/callNext //@todo: CLee 4/7/16
        //$callDisposition = Yii::app()->request->getParam('callDisposition');

        if($callListPhoneId = Yii::app()->request->getParam('callListPhoneId')) {
            Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."' WHERE id={$callListPhoneId}")->execute();
        }
        /**
         * Hang up on this call by leaving this blank.
         * Nothing here causes hangup on call.
         */
        $this->_sendResponse();
    }

    /**
     * Leaves Voicemail for the b-leg passed to this callback action
     * @param $id integer Call ID
     */
    public function actionTransferLeaveVoicemail($id)
    {
        $voicemailId = Yii::app()->request->getPost('voicemailId');

        if(!$voicemailId) {

            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') URGENT ERROR: Voicemail ID not found to leave voicemail message. See post data.');
        }

        if($recording = CallRecordings::model()->findByPk($voicemailId)) {

            // Set header with call list ID
            header('x-stm-calllistid: ' . $id);
            header('x-stm-clientid: ' . Yii::app()->user->clientId);
            header('x-stm-accountid: ' . Yii::app()->user->accountId);

            // change activity log
            if($activityLog = ActivityLog::model()->findByAttributes(array('call_id' => $id))) {

                $phone = Yii::app()->db->createCommand("select phone from calls c LEFT JOIN call_list_phones c2 ON c.call_list_phone_id=c2.id LEFT JOIN phones p ON c2.phone_id=p.id where c.id=".$activityLog->call_id)->queryScalar();

                $activityLog->is_spoke_to = 0;
                $activityLog->note = 'Left Voicemail "'.$recording->title.'" - '.Yii::app()->format->formatPhone($phone);
                $activityLog->task_type_id = TaskTypes::DIALER_LEFT_VOICEMAIL;

                if(!$activityLog->save()) { //undo command: update activity_log set task_type_id=42 WHERE id=226623
                    // activity log did not save, send error message
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating  activity log for voicemail. Did not save properly. Error Message: '.print_r($activityLog->getErrors(), true).' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
                }
            }
            else {
                // did not find activity log based on id,  send error message
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Activity log not found to log voicemail message left. ActivityLogId# '.$id.PHP_EOL.'Voicemail Recording Data: '.print_r($recording->attributes, true), CLogger::LEVEL_ERROR);
            }

            $this->_response->addWait(array(
                    'beep' => true,
                    'length' => 120,
                ));

            $this->_response->addPlay($recording->recording_url);

            $this->_sendResponse();
        }
        else {
            // log error for voicemail not found
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Voicemail Greeting not found. Voicemail ID# '.$voicemailId, CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Action Dial Action
     *
     * Action for dial element
     * @return void
     */
    public function actionDialAction()
    {

    }

    /**
     * Hangup Action
     *
     * Called by Plivo when the phone is hung up
     * @return void
     */
    public function actionHangup()
    {
        switch(1) {
            // detect conditions for completion of a session. Only a call_session completion has post data RequestUUID
            case ((Yii::app()->request->getPost('RequestUUID') || Yii::app()->request->getPost('CallUUID')) && Yii::app()->request->getPost('BillRate') > 0): //&& Yii::app()->request->getPost('TotalCost') > 0

                if(Yii::app()->request->getPost('RequestUUID')) {
                    $findAttributes = array('request_uuid'=>Yii::app()->request->getPost('RequestUUID'));

                }elseif(Yii::app()->request->getPost('CallUUID')) {
                    $findAttributes = array('call_uuid'=>Yii::app()->request->getPost('CallUUID'));
                }

                if(!($callSession = CallSessions::model()->findByAttributes($findAttributes))) {
                    // this means this is not a hangup of a call session.
                    break;
                }

                $callSession->setAttributes(array(
                        'total_cost' => Yii::app()->request->getPost('TotalCost'),
                        'hang_up_cause' => Yii::app()->request->getPost('HangupCause'),
                        'duration' => Yii::app()->request->getPost('Duration'),
                        'bill_duration' => Yii::app()->request->getPost('BillDuration'),
                        'answer_time' => Yii::app()->request->getPost('AnswerTime'),
                        'start_time' => Yii::app()->request->getPost('StartTime'),
                        'end_time' => Yii::app()->request->getPost('EndTime'),
                        'call_status' => Yii::app()->request->getPost('CallStatus'), // will most likely be "completed"
                        'event' => Yii::app()->request->getPost('Event'), // will most likely be "Hangup"
                        'end' => new CDbExpression('NOW()'),
                    ));

                if(!$callSession->save()) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call session did not save. Error Message:' . print_r($callSession->getErrors(), true).PHP_EOL.' Attributes: '.print_r($callSession->attributes, true), CLogger::LEVEL_ERROR);
                }

                // sets all callListPhones Ringing status to No Answer since the session is complete.
                if(Yii::app()->request->getParam('user_id')) {
                    Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."', updated=NOW() WHERE status='Answered' AND DATE(updated)='".date('Y-m-d')."' AND updated_by=".Yii::app()->request->getParam('user_id'))->execute();
                    //@todo: add activity log about hanging up????
                    //            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Query: '."UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."', updated=NOW() WHERE status='Answered' AND updated_by=".Yii::app()->request->getParam('user_id'), CLogger::LEVEL_ERROR);
                }
                else {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') User ID is missing. Cannot update call_list_phone status without this and may leave status stuck as Ringing or Answered. Investigate immediately.', CLogger::LEVEL_ERROR);
                }

                // Set header with call list ID
                header('x-stm-calllistid: ' . $callSession->call_list_id);
                header('x-stm-clientid: ' . Yii::app()->user->clientId);
                header('x-stm-accountid: ' . Yii::app()->user->accountId);

                break;

            // completion of a b-leg (NOT a session). B-leg hangup does not, only has CallUUID and ALegRequestUUID.
            case (Yii::app()->request->getPost('TotalCost') == 0 && Yii::app()->request->getPost('BillRate') == 0):
                break;

            default:
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Unexpected hangup scenario. Review Post Data..', CLogger::LEVEL_ERROR);
                break;
        }

        $this->_sendResponse();
    }

    /**
     * Fallback Action
     *
     * Called by Plivo when all else fails
     * @return void
     */
    public function actionFallback()
    {
        if($_POST['Event'] !== 'StartApp') {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Fallback Url. See Post Data and fix immediately.', CLogger::LEVEL_ERROR);
        }
        $this->_sendResponse();
    }

    /**
     * Callback Action
     *
     * This occurs when a called party is bridged with the caller, hung up or a digit is pressed.
     * Used in the XML call list, Plivo calls this when things happen with the associated numbers of a dialer list
     * @return void
     */
    public function actionCallback()
    {
        //Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Callback event. See post data.', CLogger::LEVEL_ERROR);
        $phoneNumber = Yii::app()->request->getParam('DialBLegTo');
        $userId = Yii::app()->request->getParam('user_id');
        // call list id
        $id = Yii::app()->request->getParam('id');

        // call_list_phone_ids delimited by dash
        $ids = (strpos(Yii::app()->request->getParam('ids'), '-') !== false)? explode('-', Yii::app()->request->getParam('ids')) : (array) Yii::app()->request->getParam('ids');

        // NOTE: Plivo post data provides call_uuid in callbacks but NOT request_uuid
        $callSession = CallSessions::model()->findByAttributes(array('call_uuid'=>Yii::app()->request->getPost('CallUUID')));

        // If we were unable to create the new call session - NOTE: This hasn't really happened, only handful almost 6 months ago
        if(!$callSession) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call session not found based on Call UUID#');
        }

        // Set header with call list ID
        header('x-stm-calllistid: ' . $callSession->call_list_id);
        header('x-stm-clientid: ' . Yii::app()->user->clientId);
        header('x-stm-accountid: ' . Yii::app()->user->accountId);

        // Retrieve the phone record with phone number and call_list_phone_ids
        $phone = Phones::model()->getPhoneFromDialerCallback($phoneNumber, $ids, $callSession);

        if(count($phone)>1) {
            //@todo: this is caused when a contact has the same phone # in there twice. Need logic to pic one????

            // error!! should only be one. Need to investigate.
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: More than 1 phoneId match found for Phones::model()->getPhoneFromDialerCallback. Called Phone#: '.$phoneNumber.PHP_EOL.' All phone results: '.print_r($phone, true), CLogger::LEVEL_ERROR);
        }
        if(count($phone)<1) {
            // error!! should only be one. Need to investigate.
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: No phoneId match found for Phones::model()->getPhoneFromDialerCallback. Called Phone#: '.$phoneNumber.PHP_EOL.' All phone results: '.print_r($phone, true), CLogger::LEVEL_ERROR);
        }
        $phoneRecord = $phone[0];

        // get matching call list phone record
        $callListPhone = CallListPhones::model()->skipSoftDeleteCheck()->findByPk($phoneRecord['call_list_phone_id']);

        if(!$callListPhone) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call List Phone was not found. Please investigate immediately. This affects Answered pop-up showing up. Phone Record info: '.print_r($phoneRecord, true), CLogger::LEVEL_ERROR);
        }

        $call = new Calls();
        $call->setAttributes(array(
            'call_session_id' => $callSession->id,
            'call_list_phone_id' => $callListPhone->id,
            'dial_b_leg_uuid' => Yii::app()->request->getPost('DialBLegUUID'),
            'answer_time' => Yii::app()->request->getPost('AnswerTime'),
            'dial_b_leg_' => Yii::app()->request->getPost('DialBLegTotalCost'),
            'dial_b_leg_status' => Yii::app()->request->getPost('DialBLegStatus'),
            'dial_b_leg_rate' => Yii::app()->request->getPost('DialBLegBillRate'),
            'dial_action' => Yii::app()->request->getPost('DialAction'),
            'dial_b_leg_bill_duration' => Yii::app()->request->getPost('DialBLegBillDuration'),
            'event' => Yii::app()->request->getPost('Event'),
            'dial_b_leg_from' => Yii::app()->request->getPost('DialBLegFrom'),
            'dial_a_leg_uuid' => Yii::app()->request->getPost('DialALegUUID'),
            'dial_b_leg_position' => Yii::app()->request->getPost('DialBLegPosition'),
            'start_time' => Yii::app()->request->getPost('StartTime'),
            'dial_b_leg_hangup_cause' => Yii::app()->request->getPost('DialBLegHangupCause'),
            'call_uuid' => Yii::app()->request->getPost('CallUUID'),
            'end_time' => Yii::app()->request->getPost('EndTime'),
            'dial_b_leg_duration' => Yii::app()->request->getPost('DialBLegDuration'),
            'dial_b_leg_to' => Yii::app()->request->getPost('DialBLegTo'),
            'added' => new CDbExpression('NOW()'),
        ));

        if(!$call->save()) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Call did not save. Error Message:' . print_r($call->getErrors(), true).PHP_EOL.' Attributes: '.print_r($call->attributes, true), CLogger::LEVEL_ERROR);
        }
        elseif($call->dial_b_leg_bill_duration > 1500) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: This call duration has been flagged exceeding 1500 sec. Actual duration secs: '.$call->dial_b_leg_bill_duration. PHP_EOL.'Call Attributes: '.print_r($call->attributes, true), CLogger::LEVEL_ERROR);
        }

        //@todo: connect $call model data to a activity_log and a phone_id

        // Handle different events
        switch($call->event) {
            case 'DialAnswer':
                // this affects the /dialer/load function which pulls the phone # that is "Answered" and passes that for activity log container.  only log 1 answered or it will mess up stats
                $dialBLegHangupCause = 'Answered';
                //@todo: make setting for this
                if(in_array(strtolower($_SERVER['SERVER_NAME']), array('www.michiganhomehunt.com','www.mywestchesterhomesearch.com','www.christineleeteam.com','www.seizethemarket.com', 'www.greaterbirminghamhomesforsale.com','www.norkusrealtygroup.com')) ) {
                    // start Recording
                    try{
                        $response = Yii::app()->plivo->getPlivo()->record(array(
                                'call_uuid' =>  $call->call_uuid,
                                'time_limit' => 3600, // 1 hour
                                'redirect' => false,
                                'callback_url' => StmFunctions::getSiteUrl() . '/plivo/recordingCallback?callId='.$call->id, // all recording data is sent here
                            ));
                    }
                    catch(\StmPlivoFunctions\PlivoError $e) {
                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') Error recording call. Error Code: ' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage(), CLogger::LEVEL_ERROR);
                    }

                    if($response['status'] >= 200 && $response['status'] <= 299) {

                        // log a initial response recording data into the call record
                        $call->setAttributes(array(
                            'record_call_uuid' => $call->dial_b_leg_uuid,//@todo: can delete this column later when verify this matches original b-leg uuid
                            'record_message' => $response['response']['message'],
                            'record_api_id' => $response['response']['api_id'],
                            'recording_added' => new CDbExpression('NOW()'),
                        ));
                        if(!$call->save()) {
                            // recording start initial data saving error
                            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving recording call initial data.  Call model Error Message: '.print_r($call->getErrors(), true).' Plivo Response: '.print_r($response, true).PHP_EOL.'B-Leg UUID: '.$call['dial_b_leg_uuid'].PHP_EOL.' Call ID: '.$call->id, CLogger::LEVEL_ERROR);
                        }
                    }
                    else {
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Error recording call. Plivo Response: '.print_r($response, true).PHP_EOL.'UUID: '.$call['dial_b_leg_uuid'].PHP_EOL.' call_id: '.$call->id, CLogger::LEVEL_ERROR);
                    }
                }

                $this->_logActivity($call, TaskTypes::DIALER_ANSWER, 'Call Answered');

                // get ids of the other ones if answered.
                $otherIds = $ids; //$phoneRecord['call_list_phone_id']
                $answerCallKey = array_search($phoneRecord['call_list_phone_id'], $ids);
                unset($otherIds[$answerCallKey]);

                // update other calls as No Answer
                if(count($otherIds)) {
                    $noAnswerIds = implode(',',$otherIds);
                    foreach($otherIds as $otherId) {
                        $noAnswerCall = new Calls();
                        $noAnswerCall->setAttributes(array(
                            'call_session_id' => $callSession->id,
                            'call_list_phone_id' => $otherId,
                            'status' => 'No Answer',
                            'added' => new CDbExpression('NOW()'),
                        ));
                        if($noAnswerCall->save()) {
                            $this->_logActivity($noAnswerCall, TaskTypes::DIALER_NO_ANSWER, 'No Answer');
                        }
                        else {
                            //error message
                            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Calls record did not save. Error Message:' . print_r($noAnswerCall->getErrors(), true).PHP_EOL.' Attributes: '.print_r($noAnswerCall->attributes, true), CLogger::LEVEL_ERROR);
                        }
                    }

                    try {
                        Yii::app()->db->createCommand("UPDATE call_list_phones SET status='No Answer', last_called=NOW(), updated=NOW(), updated_by={$userId} WHERE id IN($noAnswerIds)")->execute();
                    }
                    catch(Exception $e) {
                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') Error updating No Answers in callback action for CallListPhone IDs:'.print_r($noAnswerIds, true).PHP_EOL.' Error Code: ' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
                    }
                }
                // log any other calls as no answer.
                break;

            case 'DialHangup':

                // Get the reason for hanging up
                switch(Yii::app()->request->getParam('DialBLegHangupCause') ) {
                    case 'USER_BUSY':
                    case 'NORMAL_TEMPORARY_FAILURE':
                    case 'NORMAL_CIRCUIT_CONGESTION';
                    case 'NO_ANSWER':
                        $dialBLegHangupCause = 'No Answer';
                        $this->_logActivity($call, TaskTypes::DIALER_NO_ANSWER, 'No Answer');
                        break;

                    case 'ORIGINATOR_CANCEL':
                        // agent disconnected in the middle?... need to read logs to confirm.
                        $dialBLegHangupCause = 'No Answer';

                        break;

                    case 'NORMAL_CLEARING':

                        // this is hangup from a successful completion of a call answer.
                        //@todo: if the duration was less than 10 seconds, need to see if this needs special handling. Will log for now until confirmed
                        if(Yii::app()->request->getPost('DialBLegDuration') < 10) {
                            $phone = $callListPhone->phone;
//                            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Call lasted less than 10 seconds. Could be if answered call is voice mail. Verify to see how it is handled. Call List Phone Data: '.print_r($callListPhone->attributes, true).' Phone#: '.$phone->phone, CLogger::LEVEL_ERROR);
                        }

                        $isVoicemail = Yii::app()->db->createCommand("select count(*) from activity_log where call_id={$call->id} AND note like '%Voicemail%' AND task_type_id=".TaskTypes::DIALER_NO_ANSWER)->queryScalar();
                        // see if this was a hangup from voicemail, if so....
                        if($isVoicemail) {
                            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Detected as a voicemail button hangup. Verify data to confirm. Delete this log in code when ready. Call List Phone Data: '.print_r($callListPhone->attributes, true).' Phone#: '.$phone->phone, CLogger::LEVEL_ERROR);
                        }

                        $dialBLegHangupCause = ((Yii::app()->request->getPost('DialBLegDuration') > 10) && !$isVoicemail) ? 'Complete' : 'No Answer';

                        // file_put_contents('/var/log/plivo/plivo-log-' . date('Y-m-d') . '-' . Yii::app()->controller->action->id.'-hangupComplete', '$dialBLegHangupCause: '.$dialBLegHangupCause.',  calls: '.print_r($callListPhone, true).PHP_EOL.' POST: '.print_r($_POST, true), FILE_APPEND);

                        break;

                    case 'CALL_REJECTED':
                    case 'UNKNOWN';             // "Welcome to verizon wireless. The number you have dialed has been change, disconnect or is no longer in service..."
                    case 'UNALLOCATED_NUMBER':
                    case 'NORMAL_UNSPECIFIED':
                    case 'NO_USER_RESPONSE';    // "You have reached a non-working number..."
                        $dialBLegHangupCause = 'Bad Number';
                        $this->_logActivity($call, TaskTypes::DIALER_BAD_NUMBER_DELETED, 'Bad Number Deleted');

                        // remove the phone # from the list, save happens below
                        $callListPhone->is_deleted = 1;

                        //delete the bad phone #
                        $phone = $callListPhone->phone;
                        $phone->delete();

                        // update call phone list status - or let the auto cleanup script take care of it?
                        break;

                    default:
                        $dialBLegHangupCause = 'No Answer';
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Plivo Dial Hangup reason unrecognized. DialBLegHangupCause: '.Yii::app()->request->getParam('DialBLegHangupCause').' IDs:'.print_r($ids, true), CLogger::LEVEL_ERROR);
                        break;
                }
                break;

            default:
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Unknown event type for callback. Event Name: '.$call->event, CLogger::LEVEL_ERROR);
                throw new Exception('An unknown event type was passed to the callback');
                break;
        }

        $callListPhone->status = $dialBLegHangupCause;
        $callListPhone->updated_by = $userId;
        $callListPhone->updated = new CDbExpression('NOW()');

        // fixes bug on different filters deleting each other. May not need after minor refactor/hotfix.
        if($dialBLegHangupCause = CallListPhones::ANSWERED) {
            $callListPhone->is_deleted = 0;
        }

        if(!$callListPhone->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving call to update status to "'.$dialBLegHangupCause.'". Call List Phone Attributes: '.print_r($callListPhone->attributes, true).' Error Message: '.print_r($callListPhone->getErrors(), true), CLogger::LEVEL_ERROR);
        }
        $ids = implode(',',$ids);
        // update all call list phone last called date to NOW()
        Yii::app()->db->createCommand("UPDATE call_list_phones SET updated=NOW(), updated_by=$userId, last_called=NOW() WHERE id IN($ids)")->execute(); // @todo: this can also be accomplished by making that field auto timestamp update
    }

    /**
     * Action Log Activity
     *
     * Logs activity to database
     * @return void
     */
    protected function _logActivity(Calls $call, $taskTypeId, $message)
    {
        // accommodate for deleted call_list_phones. ex. if task deleted in middle of call session
        if(!($callListPhone = CallListPhones::model()->skipSoftDeleteCheck()->findByPk($call->call_list_phone_id))) {
            //throw error
        }

        // this is a task type
        //@todo: protected against lists that are deleted in between sessions??? - CLee 4/6/16 , doesn't handle right now, could happen, not likely.
        if($callListPhone->callList->type->table_type == CallListTypes::TASK_TABLE_TYPE) {

            // find the task even if it's deleted
            //$call->callListPhone->task_id && ($task = $call->callListPhone->task);
            $task = Tasks::model()->skipSoftDeleteCheck()->findByPk($call->callListPhone->task_id);

            // find out if there is a spoke to activity previous to determine if it's new lead gen or follow-up
            $spokeToCount = Yii::app()->db->createCommand("select count(*) from activity_log where component_type_id={$task->component_type_id} AND component_id={$task->component_id} AND is_spoke_to=1 AND (is_deleted=0 OR is_deleted IS NULL)")->queryScalar();
            $componentTypeId = $task->component_type_id;
            $componentId = $task->component_id;
        }
        else {

            if(!($contactId = $call->callListPhone->phone->contact_id)) {
                $contactId = Yii::app()->db->createCommand("select c2.id from calls c LEFT JOIN call_list_phones l ON l.id=c.call_list_phone_id LEFT JOIN phones p ON p.id=l.phone_id LEFT JOIN contacts c2 ON c2.id=p.contact_id WHERE c.id=".$call->id)->queryScalar();
                $foundMessage = PHP_EOL.'-------------------------------------------------------------------------------'.PHP_EOL
                    .' NOTE: **** Secondary Query found results. This means the error is due to soft deletes. Consider switching to this raw query if bug persists. **** '
                    .PHP_EOL.'-------------------------------------------------------------------------------'.PHP_EOL;
                if(!$contactId) {
                    $foundMessage = '';
                    // 2nd query didn't find record. Now it's a serious issue.
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT ERROR: Call record could not find the contact ID. 2nd query did not find it either!!'.$foundMessage.PHP_EOL
                        .'Call Attributes: '.print_r($call->attributes, true).PHP_EOL
                        .'Call List Phone: '.print_r($call->callListPhone->attributes, true).PHP_EOL
                        .'Phone Attributes: '.print_r($call->callListPhone->phone->attributes, true), CLogger::LEVEL_ERROR);
                }
            }

            switch($presetId = $call->callListPhone->callList->preset_ma) {

                case CallLists::ALL_NEW_SELLER_PROSPECTS:
                case CallLists::ALL_NURTURING_SELLERS:
                case CallLists::LEAD_POOL_SELLERS_TASKS:
                case CallLists::ALL_D_SELLER_PROSPECTS:
                case CallLists::CUSTOM_SELLERS:
                    //$transactionStatusId = ($presetId == CallLists::ALL_NEW_SELLER_PROSPECTS) ? TransactionStatus::NEW_LEAD_ID : TransactionStatus::NURTURING_ID;
                    switch($presetId) {
                        case CallLists::ALL_NEW_SELLER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::NEW_LEAD_ID;
                            break;

                        case CallLists::ALL_NURTURING_SELLERS:
                            $transactionStatusId = TransactionStatus::NURTURING_ID;
                            break;

                        case CallLists::ALL_D_SELLER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::D_NURTURING_SPOKE_TO_ID;
                            break;
                    }

                    $componentTypeId = ComponentTypes::SELLERS;

                    // only add this if call list type is NOT custom
                    $statusCondition = (in_array($presetId, array(CallLists::LEAD_POOL_SELLERS_TASKS, CallLists::CUSTOM_SELLERS))) ? '' : "transaction_status_id={$transactionStatusId} AND ";

                    $query = "select id from transactions where {$statusCondition} component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";

                    $componentId = Yii::app()->db->createCommand($query)->queryScalar();
                    if(!$componentId) {

                        $query2 = "select id from transactions where component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";
                        if($componentId2 = Yii::app()->db->createCommand($query2)->queryScalar()) {
                            $comment2 = ($componentId2) ? 'Found result on second query="'.$query2.'". Replaced with found componentId. This means transaction status has changed. Second query may be a better choice. Investigate immediately.' : '';
                            $componentId = $componentId2;
                        }

                        //@todo: re-anable this after custom list stuff is figured out. Custom lists triggers this 100% of the time so turning it off for now.
//                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Did not find Component ID in 1st query="'.$query.' '.$comment2.' Preset ID: '.$presetId.' Call Data: '.print_r($call->attributes, true).' TaskTypeId: '.$taskTypeId.' Message: '.$message, CLogger::LEVEL_ERROR);
                    }

                    break;

                case CallLists::ALL_NEW_BUYER_PROSPECTS:
                case CallLists::ALL_NURTURING_BUYERS:
                case CallLists::LEAD_POOL_BUYERS_TASKS:
                case CallLists::ALL_D_BUYER_PROSPECTS:
                case CallLists::CUSTOM_BUYERS:
                    //$transactionStatusId = ($presetId == CallLists::ALL_NEW_BUYER_PROSPECTS) ? TransactionStatus::NEW_LEAD_ID : TransactionStatus::NURTURING_ID;

                    switch($presetId) {
                        case CallLists::ALL_NEW_BUYER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::NEW_LEAD_ID;
                            break;

                        case CallLists::ALL_NURTURING_BUYERS:
                            $transactionStatusId = TransactionStatus::NURTURING_ID;
                            break;

                        case CallLists::ALL_D_BUYER_PROSPECTS:
                            $transactionStatusId = TransactionStatus::D_NURTURING_SPOKE_TO_ID;
                            break;
                    }

                    $componentTypeId = ComponentTypes::BUYERS;

                    // only add this if call list type is NOT custom
                    //$statusCondition = ($presetId == CallLists::CUSTOM_BUYERS) ? '' : "transaction_status_id={$transactionStatusId} AND ";
                    $statusCondition = (in_array($presetId, array(CallLists::LEAD_POOL_BUYERS_TASKS, CallLists::CUSTOM_BUYERS))) ? '' : "transaction_status_id={$transactionStatusId} AND ";

                    $query = "select id from transactions where {$statusCondition} component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";

                    $componentId = Yii::app()->db->createCommand($query)->queryScalar();
                    if(!$componentId) {
                        // try a query without the transaction status
                        $query2 = "select id from transactions where component_type_id={$componentTypeId} AND contact_id={$contactId} AND is_deleted=0 ORDER BY id ASC LIMIT 1";
                        $componentId = Yii::app()->db->createCommand($query2)->queryScalar();

                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Did not find Component ID. Could be due to status change in transaction. Found '.(($componentId)? '1': 'NO' ).' result with Query 2.'.PHP_EOL
                            .'Query 1:"'.$query.PHP_EOL
                            .'Query 2: "'.$query2.'"'.PHP_EOL
                            .'Preset ID: '.$presetId.PHP_EOL
                            .'Call Data: '.print_r($call->attributes, true).' TaskTypeId: '.$taskTypeId.' Message: '.$message, CLogger::LEVEL_ERROR);
                    }

                    break;

                case CallLists::MY_CONTACTS_TASKS:
                case CallLists::CUSTOM_CONTACTS:
                case CallLists::LEAD_POOL_CONTACTS_TASKS:
                    $componentTypeId = ComponentTypes::CONTACTS;
                    $componentId = $contactId;
                    break;

                case CallLists::MY_RECRUITS_TASKS:
                case CallLists::LEAD_POOL_RECRUITS_TASKS:
                case CallLists::CUSTOM_RECRUITS:
                    $componentTypeId = ComponentTypes::RECRUITS;
                    $componentId = Yii::app()->db->createCommand("SELECT id FROM recruits WHERE contact_id={$contactId}")->queryScalar();
                    break;

                default:
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Unknown type of table for getting calls by list Preset ID: '.$presetId.' Call Data: '.print_r($call->attributes, true).' TaskTypeId: '.$taskTypeId.' Message: '.$message, CLogger::LEVEL_ERROR);
//                    throw new Exception('Unknown type of table for getting calls by list Preset ID. '.$presetId.' Call Data: '.print_r($call, true).' TaskTypeId: '.$taskTypeId);
                    break;
            }

            if($componentId) {
                $spokeToCount = Yii::app()->db->createCommand("select count(id) from activity_log where component_type_id={$componentTypeId} AND component_id={$componentId} AND is_spoke_to=1 AND (is_deleted=0 OR is_deleted IS NULL)")->queryScalar();
            }
        }


        // Create the new activity log
        $activityLog = new ActivityLog();
        $activityLog->setAttributes(array(
            'task_type_id' => $taskTypeId,
            'account_id' => Yii::app()->user->accountId,
            'component_type_id' => $componentTypeId,
            'component_id' => $componentId,
            'activity_date' => new CDbExpression('NOW()'),
            'lead_gen_type_ma' => ($spokeToCount)? ActivityLog::LEAD_GEN_FOLLOW_UP_ID : ActivityLog::LEAD_GEN_NEW_ID,
            'is_spoke_to' => 0, //($taskTypeId == TaskTypes::DIALER_ANSWER) ? 1 : 0
            'call_id' => $call->id,
            'note' => $message. ' - '.Yii::app()->format->formatPhone($call->callListPhone->phone->phone),
            'added_by' => $call->callSession->contact_id,
        ));
//        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI. Attributes: '.print_r($activityLog->attributes, true).' Call Attributes: '.print_r($call->attributes, true), CLogger::LEVEL_ERROR);

        if(!$activityLog->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Activity Log. Attributes: '.print_r($activityLog->attributes, true).' Error Message: '.print_r($activityLog->getErrors(), true), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Callback for Completed Call Recording
     *
     * Updates DB calls table with received information
     * @return void
     */
    public function actionRecordingCallback()
    {
        //Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Review POST data.', CLogger::LEVEL_ERROR);
        $response = json_decode($_REQUEST['response'], true);

        $callId = Yii::app()->request->getParam('callId');

        if(!$callId) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call Id is missing. Review POST data.', CLogger::LEVEL_ERROR);
            return;
        }

        if(!($call = Calls::model()->findByPk($callId))) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Call not found by callId. Review POST data.', CLogger::LEVEL_ERROR);
            return;
        }

        $call->setAttributes(array(
                'recording_id' => $response['recording_id'],
                'record_api_id' => $response['api_id'],
                'record_url' => $response['record_url'],
                'recording_call_uuid' => $response['call_uuid'],
                'recording_duration' => $response['recording_duration'],
                'recording_duration_ms' => $response['recording_duration_ms'],
                'recording_start_ms' => $response['recording_start_ms'],
                'recording_end_ms' => $response['recording_end_ms'],
                'record_message' => $response['message'],
            ));
        if(!$call->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Recording Callback Error: Recording Data did not save. Review POST data.', CLogger::LEVEL_ERROR);
        }
    }

    public function  actionCleanDeletedPhones()
    {
        //@todo: moving this whole method to node in callback route 3/23/15 - this is a temporary solution
        $callListId = Yii::app()->request->getParam('id');
        if(Yii::app()->request->getParam('key') != 'tr5chAPra4ra') {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Incorrect key with the cleanDeletePhones. Potential unauthorized intrusion. Please review server data below.');
        }

        // search and remove any phone #'s that have been deleted from the contact. NOTE: Did this select first to prevent unnecessary UPDATE calls if not needed
        //query: "SELECT count(c.id) FROM call_list_phones c LEFT JOIN phones p ON c.phone_id = p.id WHERE c.call_list_id={$callListId} AND p.is_deleted=1 AND c.is_deleted=0"
        $hasRemoveItems = \Yii::app()->db->createCommand()
            ->select('count(c.id)')
            ->from('call_list_phones c')
            ->leftJoin('phones p','c.phone_id = p.id')
            ->where("c.call_list_id={$callListId} AND p.is_deleted=1 AND c.is_deleted=0")
            ->queryScalar();

        if($hasRemoveItems) {
            $removeInactivePhones = \Yii::app()->db->createCommand("UPDATE call_list_phones c LEFT JOIN phones p ON c.phone_id = p.id SET c.is_deleted=1, c.updated=NOW() WHERE c.call_list_id={$callListId} AND p.is_deleted=1 AND c.is_deleted=0 AND c.status NOT IN('".CallListPhones::ANSWERED."','".CallListPhones::RINGING."')")->execute();

            // log if deletes existed
            if($removeInactivePhones) {
                $this->_log2(__CLASS__, __LINE__, $removeInactivePhones.' callListPhones have been deleted due to phones being deleted.');
            }
        }
    }

    /**
     * Send Response
     *
     * Send the XML response
     * @return void
     */
    protected function _sendResponse()
    {
        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $this->_response->toXML();

        // Terminate execution
        Yii::app()->end();
    }

    /**
     * Log Things
     *
     * Logs API calls from Plivo for informational purposes
     * @return void
     */
    protected function _logThings()
    {
        // Clean output buffer
        ob_clean();
        ob_start();
        print $this->action->id." Action: \n\n";

        // Test receiving data
        print "POST Variables \n";
        print "------------- \n\n";
        print_r($_POST);

        print "\n\nGET Variables \n";
        print "------------- \n\n";
        print_r($_GET);

        print "\n\nSERVER Variables \n";
        print "---------------- \n\n";
        print_r($_SERVER);

        // Nginx doesn't have this functionality =(
        if(function_exists('getallheaders')) {
            print "\n\nPost Headers \n";
            print "--------- \n\n";
            print_r(getallheaders());
        }

        // Store the data
        file_put_contents(self::LOG_LOCATION.'/plivo-log-' . date('Y-m-d_h-i-s') .'_'.microtime(true). '-' . Yii::app()->controller->action->id, ob_get_contents(), FILE_APPEND); //microtime(true)
        ob_clean();
    }

    /**
     * Log2
     *
     * Newer log method used to debug and find final bug for why "Answered" is not popping up. Logging all delete updates when it occurs.
     * @return void
     */
    protected function _log2($class, $line, $message)
    {
        // Clean output buffer
        ob_clean();
        ob_start();
        print "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        print date('Y-m-d_h:i:s') ." $class (:$line) [Action: ".$this->action->id."] - $message";
        print "UserId: ".Yii::app()->request->getParam('user_id')." | SERVER Request URI: ".$_SERVER['REQUEST_URI']."\n";

        // Store the data
        file_put_contents(self::LOG_LOCATION.'/plivo-log-'. str_replace('http://www.','',StmFunctions::getSiteUrl()) , ob_get_contents(), FILE_APPEND);
        ob_clean();
    }

	/** Added: Bkozak - 3/26/2015 - Vicemail Greetings */
	public function actionAnswerConfirmStartVm()
    {
		$contact_id = Yii::app()->request->getParam('contact_id');
		$title = Yii::app()->request->getParam('title');

		$getDigits = $this->_response->addGetDigits(array(
			'action'        => StmFunctions::getSiteUrl() . '/plivo/recordVm?contact_id=' . $contact_id."&title=".urlencode($title),
			'redirect'      => 'true',
			'validDigits'	=> '1',
			'timeout'       => 30, //seconds
		));
        $getDigits->addSpeak('Hello. Press 1 to record your voicemail greeting.');

		$this->_response->addSpeak('Input not received. Your session is ending. Goodbye.');

		$this->_sendResponse();
    }

	public function actionHangupVm()
    {
		Yii::app()->end();
	}

	public function actionRecordVm()
    {
		$contact_id = Yii::app()->request->getParam('contact_id');
		$title = Yii::app()->request->getParam('title');

        $this->_response->addSpeak('Please record your message after the beep. Press pound to end the recording.');

        $this->_response->addRecord(array(
                'maxLength' => 60,
                'finishOnKey' => '#',
                'playBeep' => true,
                'action' => StmFunctions::getSiteUrl() . '/plivo/saveRecordVm?contact_id=' . $contact_id."&title=".urldecode($title),
            ));

        $this->_response->addSpeak('Recording not received. Please hang up and try again. Good bye.');

        $this->_sendResponse();
	}

	public function actionSaveRecordVm()
    {
        $callRecording = new CallRecordings();
        $callRecording->setAttributes(array(
                'type' => CallRecordings::TYPE_VOICEMAIL_GREETING,
                'title' => urldecode($_POST['title']),
                'contact_id' => $_POST['contact_id'],
                'from_number' => $_POST['From'],
                'to_number' => $_POST['To'],
                'recording_id' => $_POST['RecordingID'],
                'recording_url' => $_POST['RecordUrl'],
                'call_uuid' => $_POST['CallUUID'],
                'is_default' => 0,
                'recording_duration' => $_POST['RecordingDuration'],
                'recording_duration_ms' => $_POST['RecordingDurationMs'],
                'recording_start_ms' => $_POST['RecordingStartMs'],
                'recording_end_ms' => $_POST['RecordingEndMs'],
                'added' => new CDbExpression('NOW()'),
            ));

        if(!$callRecording->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Voicemail Recording.'.PHP_EOL.'Attributes: : '.print_r($callRecording->attributes, true).' Query: '.print_r($callRecording->getErrors(), true), CLogger::LEVEL_ERROR);
        }

        $this->_response->addSpeak('Your voice mail recording is complete. Have a great day. Goodbye.');

        $this->_sendResponse();
	}

	/** End Added: Bkozak - 3/26/2015 - Vicemail Greetings */

	/** Added: Bkozak - 3/30/2015 - SMS Message Sent Callback */
	public function actionHandleSmsSentCallback() {
		/*Array
(
      [Status] => queued
      [From] => 19043301885
      [ParentMessageUUID] => -
      [PartInfo] => -
      [To] => 19042629505
      [MessageUUID] => e282992f-42bf-4e5c-bf41-2e776c1357b7
)
Array
(
      [Status] => undelivered
      [From] => 19043301885
      [ParentMessageUUID] => e282992f-42bf-4e5c-bf41-2e776c1357b7
      [To] => 19042629505
      [PartInfo] => 1 of 1
      [TotalRate] => 0.00350
      [Units] => 1
      [TotalAmount] => 0.00350
      [MessageUUID] => e282992f-42bf-4e5c-bf41-2e776c1357b7
)
*/
        $this->_sendResponse();
	}

	public function actionReceivedSms() {
		/*
		 * Array
(
      [Status] => sent
      [From] => 19043301885
      [ParentMessageUUID] => 57100ba4-ab07-4733-9c58-b533fc6c6417
      [To] => 12162170382
      [PartInfo] => 1 of 1
      [TotalRate] => 0.00350
      [Units] => 1
      [TotalAmount] => 0.00350
      [MessageUUID] => 57100ba4-ab07-4733-9c58-b533fc6c6417
)

Array
(
      [From] => 12162170382
      [TotalRate] => 0.00000
      [Text] => Hello world
      [To] => 19048006995
      [Units] => 1
      [TotalAmount] => 0.00000
      [Type] => sms
      [MessageUUID] => d5da72ca-d808-11e4-8b98-22000acb8068
)
		 */

        if(!isset($_POST) || empty($_POST)) {
            $this->_sendResponse();
        }

        if(!isset($_POST['From']) || empty($_POST['From'])) {
			Yii::log(__CLASS__ . ' (:' . __LINE__ . ') SMS Recieved With No FROM.'.PHP_EOL.'POST: : '.print_r($_POST, true), CLogger::LEVEL_ERROR);
			Yii::app()->end();
		}
		$_POST['From'] = preg_replace('/[^0-9]/','',trim(strip_tags($_POST['From'])));

		if(!isset($_POST['To']) || empty($_POST['To'])) {
			Yii::log(__CLASS__ . ' (:' . __LINE__ . ') SMS Recieved With No To.'.PHP_EOL.'POST: : '.print_r($_POST, true), CLogger::LEVEL_ERROR);
			Yii::app()->end();
		}
		$_POST['To'] = preg_replace('/[^0-9]/','',trim(strip_tags($_POST['To'])));

		if(!isset($_POST['Text']) || empty($_POST['Text'])) {
			$_POST['Text'] = "";
		}
		$_POST['Text'] = trim(strip_tags($_POST['Text']));

		$inboundTo = $_POST['To'];
		if(strlen($inboundTo) > 10 && $inboundTo[0] == "1") {
			$inboundTo = substr($inboundTo, 1);
		}

		// Who was it sent To (originating SMS Number / Contact) ?
		$toCriteria = new CDbCriteria;
		$toCriteria->condition = 'phone = :phone AND status = :status';
		$toCriteria->params = array(
			':phone'    => $inboundTo,
			':status'		=> 1
		);
		$checkForSmsNumberHQ = TelephonyPhones::model()->find($toCriteria);

        // prevents weird loop issue
        if(strpos($_POST['Text'], 'You have a STM Text Message: You have a STM Text Message:') !== false) {

            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Some type of loop situation. Investigate immediately. From: '.$_POST['From'].' Client ID: '.$checkForSmsNumberHQ->clientAccount->client_id.', AccountID: '.$checkForSmsNumberHQ->clientAccount->account_id.PHP_EOL, CLogger::LEVEL_ERROR);
            $this->_sendResponse();
        }

        // Could not find a registered SMS number in HQ
		if(empty($checkForSmsNumberHQ)) {
			Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: SMS Recieved but could not match contact Phone.'.PHP_EOL.'POST: : '.print_r($_POST, true), CLogger::LEVEL_ERROR);
            $this->_sendResponse();
		}

		$inboundFrom = $_POST['From'];
		if(strlen($inboundFrom) > 10 && $inboundFrom[0] == "1") {
			$inboundFrom = substr($inboundFrom, 1);
		}

		StmFunctions::connectDbByClientIdAccountId($checkForSmsNumberHQ->clientAccount->client_id, $checkForSmsNumberHQ->clientAccount->account_id);

		$tryMatchFromPhones = Phones::model()->byIgnoreAccountScope()->findAllByAttributes(array('account_id' => $checkForSmsNumberHQ->clientAccount->account_id, 'phone' => trim($inboundFrom)));

		if(empty($tryMatchFromPhones)) {

            // prevents weird loop issue
            if(strpos($_POST['Text'], 'You have a STM Text Message: You have a STM Text Message:') !== false) {

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Some type of loop situation. Investigate immediately. From: '.$inboundFrom.' Client ID: '.$checkForSmsNumberHQ->clientAccount->client_id.', AccountID: '.$checkForSmsNumberHQ->clientAccount->account_id.PHP_EOL, CLogger::LEVEL_ERROR);
                $this->_sendResponse();
            }

            // this means a new phone # not in the system is texting. This should still go through to the client.
			Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not match Inbound SMS From to a Phone: '.$inboundFrom.' Client ID: '.$checkForSmsNumberHQ->clientAccount->client_id.', AccountID: '.$checkForSmsNumberHQ->clientAccount->account_id.PHP_EOL, CLogger::LEVEL_ERROR);
		}

        $origniatingMessageCheckFound = false;
        if($tryMatchFromPhones) {
            foreach ($tryMatchFromPhones as $tryMatchFromPhone) {
                $origniatingMessagCheckCriteria = new CDbCriteria;
                $origniatingMessagCheckCriteria->condition = 'to_phone_id = :toPhoneId AND sent_by_contact_id = :sentByContactId';
                $origniatingMessagCheckCriteria->params = array(
                    ':toPhoneId'    => $tryMatchFromPhone->id,
                    ':sentByContactId' => $checkForSmsNumberHQ->contact_id
                );

                $origniatingMessagCheck = SmsMessages::model()->find($origniatingMessagCheckCriteria);

                if(!empty($origniatingMessagCheck)) {
                    $origniatingMessageCheckFound = true;
                    break;
                }
            }
        }

		$smsMessage = new SmsMessages;
		$smsMessage->component_type_id = NULL;
		$smsMessage->component_id = NULL;
		$smsMessage->sent_by_contact_id = NULL;
		if($origniatingMessageCheckFound) {
			$smsMessage->sent_by_contact_id = $origniatingMessagCheck->sent_to_contact_id;

            //@todo: somehow if text is from a distinct component tuple, the can assume this is the same...
//            if($origniatingMessagCheck->component_type_id) {
//                $smsMessage->component_type_id = $origniatingMessagCheck->component_type_id;
//            }
//
//            if($origniatingMessagCheck->component_id) {
//                $smsMessage->component_id = $origniatingMessagCheck->component_id;
//            }
		}
		$smsMessage->to_phone_id = NULL;
        $smsMessage->from_phone_number = $_POST['From'];
        $smsMessage->to_phone_number = $_POST['To'];
		$smsMessage->inbound_to_sms_number_id = $checkForSmsNumberHQ->id;
		$smsMessage->direction = SmsMessages::DIRECTION_INBOUND;
		$smsMessage->content = $_POST['Text'];
		$smsMessage->vendor_response = NULL;
        $smsMessage->message_uuid = $_POST['MessageUUID'];
		$smsMessage->processed_datetime = new \CDbExpression('NOW()');

		if(!$smsMessage->save()) {
			Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not save Inbound SMS.'.PHP_EOL.'POST: '.print_r($_POST, true).PHP_EOL."Attributes: ".print_r($smsMessage->attributes, true).PHP_EOL."Errors: ".print_r($smsMessage->getErrors(), 1), CLogger::LEVEL_ERROR);
			Yii::app()->end();
		}

        // send an SMS to the owner of the phone # with text and the link for STM to check his messages
        $domain = Domains::model()->byIgnoreAccountScope()->findByAttributes(array('account_id' => $checkForSmsNumberHQ->clientAccount->account_id, 'is_primary' => 1));
        $message = 'You have a STM Text Message: '.$_POST['Text'].PHP_EOL.'Login to reply to the text. http://www.'.$domain->name.'/admin/sms/reply/'.$smsMessage->id;

        if($agent = Contacts::model()->byIgnoreAccountScope()->findByAttributes(array('account_id' => $checkForSmsNumberHQ->clientAccount->account_id, 'id' => $checkForSmsNumberHQ->contact_id))) {

            $agentPhoneNumber = Yii::app()->db->createCommand("select value from setting_contact_values WHERE contact_id=".$checkForSmsNumberHQ->contact_id." AND setting_id=".Settings::SETTING_CELL_NUMBER)->queryScalar();

            if($agentPhoneNumber) {
                if(trim($message) != 'You have a STM Text Message:') {
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Message is blank. Investigate. Telephony Data: '.print_r($checkForSmsNumberHQ->attributes, true), CLogger::LEVEL_ERROR);
                    $this->_sendResponse();
                }
                else {
                    $response = Yii::app()->plivo->sendMessage($agentPhoneNumber, $message);
                }
            }
            else {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not find agent phone. Telephony Data: '.print_r($checkForSmsNumberHQ->attributes, true), CLogger::LEVEL_ERROR);
            }
            $agentEmail = Yii::app()->db->createCommand("select email from emails WHERE contact_id=".$checkForSmsNumberHQ->contact_id." ORDER BY is_primary DESC, id DESC")->queryScalar();
            StmZendMail::easyMail(array('do-not-reply@seizethemarket.net'=> 'STM SMS Alert'), array($agentEmail => $agent->fullName), 'STM SMS Received', $message);
        }
        else {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Could not find contact for telephony phone #: '.$_POST['To'], CLogger::LEVEL_ERROR);

        }


		#Yii::log(__CLASS__ . ' (:' . __LINE__ . ') SMS Recieved & Saved.'.PHP_EOL.'POST: : '.print_r($_POST, true).PHP_EOL."Attributes: ".print_r($smsMessage->attributes, true), CLogger::LEVEL_ERROR);
        $this->_sendResponse();
	}

	/** End Added: Bkozak - 3/30/2015 - SMS Message Sent Callback */
}