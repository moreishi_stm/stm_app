<?php
Yii::import('admin_module.controllers.actions.HomesListAction');
class MapsearchController extends FrontController
{

    protected $_boardName = NULL;


    public function actionIndex()
    {
        $this->_boardName = Yii::app()->getUser()->getBoard()->getPrefixedName();
        $property = new MlsProperties($this->_boardName, 'search');
        $property->unsetAttributes();
        $secondaryProperty = new MlsPropertiesSecondary($this->_boardName, 'search');

        // Render the view
        $this->render('index', array(
            'searchableFields' => MlsFieldMaps::getSearchableColumns(Yii::app()->user->board->id),
            'model' => $property,
        ));
    }

}