<?php

Yii::import('admin_module.components.StmPlivo.plivo', true);
Yii::import('front_module.controllers.base.BasePlivoController');

/**
 * Plivo Click to Call Controller
 */
class PlivoclicktocallController extends BasePlivoController
{
    /**
     * Action Test
     */
    public function actionTest()
    {
        // Fire up Plivo
        /** @var \StmPlivoFunctions\RestAPI $plivo */
        $plivo = Yii::app()->plivo->getPlivo();

        // To phone number
        $agentNumber = '19043433200';
//        $agentNumber = '19413122215';    // Nicole's Skype Phone Number
        $clientPhoneNumber = '2167025892';    // Nicole's Cell Phone Number

        // Make the call
        print_r($plivo->make_call(array(
            'from'          =>  '19415311786',  // Nicole's Super Awesome Test Phone Number
            'to'            =>  $agentNumber,
            'answer_url'    =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/answer?toPhoneNumber=' . $clientPhoneNumber,
            'hangup_url'    =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/hangup',
            'fallback_url'  =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/fallback',
        )));

        exit;
    }

    /**
     * Action Answer
     *
     * The URL invoked by Plivo when the outbound call is answered.
     * @return void
     */
    public function actionAnswer()
    {
//        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Click to Call Answer Action. See post data.', CLogger::LEVEL_ERROR);
        // Add click to call session record
        $clickToCallSession = new ClickToCallSessions();
        $clickToCallSession->call_uuid = Yii::app()->request->getPost('CallUUID');
        $clickToCallSession->component_id = Yii::app()->request->getParam('componentId');
        $clickToCallSession->component_type_id = Yii::app()->request->getParam('componentTypeId');
        $clickToCallSession->to_number = Yii::app()->request->getParam('toPhoneNumber');
        $clickToCallSession->from_number = Yii::app()->request->getPost('From');
        $clickToCallSession->added = new CDbExpression('NOW()');
        $clickToCallSession->added_by = Yii::app()->request->getParam('userId');
        $clickToCallSettingSkipConfirmation = Yii::app()->request->getParam('clickToCallSettingSkipConfirmation');

        // Attempt to save click to call session
        if(!$clickToCallSession->save()) {
            throw new Exception('Error, unable to save click to call session!');
        }

        if($clickToCallSettingSkipConfirmation) {
            $this->actionConfirmhuman($clickToCallSession->id);
        }
        else {
            // Add get digits to place call
            $getDigits = $this->_response->addGetDigits(array(
                    'action'        =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/confirmhuman/id/'.$clickToCallSession->id.'?toPhoneNumber=' . Yii::app()->request->getParam('toPhoneNumber') . '&callerIdNumber=' . Yii::app()->request->getParam('callerIdNumber') . '&userId=' . Yii::app()->request->getParam('userId'),
                    'redirect'      =>  'true',
                    'timeout'       =>  20, //seconds
                ));

            // Add speak to get digits to the user knows what to do
            $getDigits->addSpeak('Welcome. Press 1 to confirm your call connect.');

            // Notifies user that session is ending.
            $this->_response->addSpeak('Input not received. Your session is ending. Goodbye.');
        }

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Confirm Human
     *
     * This runs once we've determined that the user is actually a human
     * @return void
     */
    public function actionConfirmhuman($id=null)
    {
//        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Click to Confirm Human Action. See post data.', CLogger::LEVEL_ERROR);
        // Testing for Nicole
        if(YII_DEBUG) {
            $sitePlivoDomain = StmFunctions::getSiteUrl();
        }
        else {
            $sitePlivoDomain = (strlen(\StmFunctions::getSiteUrl()) > 15) ? \StmFunctions::getSiteUrl() : 'http://'.$_SERVER['SERVER_NAME'];
        }

        $clickToCallSession = ClickToCallSessions::model()->findbyPk($id);

        // Add activity log entry
        $activityLog = new ActivityLog();
        $activityLog->component_type_id = $clickToCallSession->component_type_id;
        $activityLog->component_id = $clickToCallSession->component_id;
        $activityLog->task_type_id = TaskTypes::CLICK_TO_CALL;
        $activityLog->activity_date = new CDbExpression('NOW()');
        $activityLog->note = 'Click to Call - ' . Yii::app()->format->formatPhone($clickToCallSession->to_number);
        $activityLog->lead_gen_type_ma = ActivityLog::LEAD_GEN_NONE_ID;
        $activityLog->added = new CDbExpression('NOW()');
        $activityLog->added_by = Yii::app()->request->getParam('userId');

        // Attempt to save activity log entry
        if(!$activityLog->save()) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Unable to save activity log entry for click to call in confirm human Errors: '.print_r($activityLog->getErrors(), 1).PHP_EOL.'Attributes: '.print_r($activityLog->attributes, 1).PHP_EOL.'Click to Call Attributes: '.print_r($clickToCallSession->attributes, 1));
        }

        $clickToCallSession->activity_log_id = $activityLog->id;
        if(!$clickToCallSession->save()) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Unable to save activity log ID in click to call session. Errors: '.print_r($clickToCallSession->getErrors(), 1).PHP_EOL.'Attributes: '.print_r($clickToCallSession->attributes, 1));
        }

        // Record the call
        $this->_response->addRecord(array(
            'action'            =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/record',
            'callbackUrl'       =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/recordcallback',
            'redirect'          =>  false,
            'playBeep'          =>  false,
            'recordSession'     => true,
            'maxLength'         => 7200,
            'startOnDialAnswer' =>  true
        ));

        // config for dial action
        $dialConfig = array(
            'action'        =>  $sitePlivoDomain . '/plivoclicktocall/dialaction',
            'callbackUrl'   =>  $sitePlivoDomain . '/plivoclicktocall/dialcallback'
        );

        if(Yii::app()->request->getParam('callerIdNumber')) {
            $dialConfig['callerId'] = Yii::app()->request->getParam('callerIdNumber');
            //$dialConfig['callerName'] = ;
        }

        // Create dial element
        $dial = $this->_response->addDial($dialConfig);

        // Add clients phone number to dial element
        $dial->addNumber('1' . Yii::app()->request->getParam('toPhoneNumber'));

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Record
     *
     * @return void
     */
    public function actionRecord()
    {
        // Send an empty XML response
        $this->_sendResponse();
    }

    /**
     * Action Record Callback
     *
     * @return void
     */
    public function actionRecordcallback()
    {
        // Locate click to call session
//        if(!($callUuid = Yii::app()->request->getPost('CallUUID'))) {
//            $response = CJSON::decode(Yii::app()->request->getPost('request'));
//            if(isset($response['call_uuid'])) {
//                $callUuid = $response['call_uuid'];
//            }
//        }
        /** @var ClickToCallSessions $clickToCallSession */
        $clickToCallSession = ClickToCallSessions::model()->findByAttributes(array(
            'call_uuid' => Yii::app()->request->getPost('CallUUID')
        ));

        // If we can't find the click to call session stop right here
        if(!$clickToCallSession) {
            throw new Exception('Unable to locate click to call session');
        }

        // Update session w/ record stuff
        $clickToCallSession->recording_id = Yii::app()->request->getPost('RecordingID');
        $clickToCallSession->recording_url = Yii::app()->request->getPost('RecordFile');
        $clickToCallSession->recording_end_ms = Yii::app()->request->getPost('RecordingEndMs');
        $clickToCallSession->bill_rate = Yii::app()->request->getPost('BillRate');
        $clickToCallSession->recording_duration_ms = Yii::app()->request->getPost('RecordingDurationMs');
        $clickToCallSession->recording_duration = Yii::app()->request->getPost('RecordingDuration');
        $clickToCallSession->recording_start_ms = Yii::app()->request->getPost('RecordingStartMs');

        // Attempt to save click to call session
        if(!$clickToCallSession->save()) {
            throw new Exception('Error, unable to save click to call session!');
        }

        if(Yii::app()->request->getPost('RecordingDuration') < 60) {
            $duration = Yii::app()->request->getPost('RecordingDuration').' seconds';
        }
        elseif(Yii::app()->request->getPost('RecordingDuration') < 120) {
            $duration = '1 minute '.(Yii::app()->request->getPost('RecordingDuration') - 60).' seconds';
        }
        elseif(Yii::app()->request->getPost('RecordingDuration') >= 120) {
            $duration = (Yii::app()->request->getPost('RecordingDuration') / 60).' minutes '.(Yii::app()->request->getPost('RecordingDuration') % 60).' seconds';
        }

        if($duration) {
            // Add activity log entry
            $activityLog = $clickToCallSession->activityLog;
            $activityLog->note .= ' - Call length: ' . $duration;

            // Attempt to save activity log entry
            if(!$activityLog->save()) {
                throw new Exception('Unable to save activity log entry for click to call hangup event! Errors: '.print_r($activityLog->getErrors(), 1).PHP_EOL.'Attributes: '.print_r($activityLog->attributes, 1));
            }
        }

        // Send an XML response
        $this->_sendResponse();
    }

    /**
     * Action Dial Action
     *
     * Redirect to this URL after leaving Dial. (When connecting the agent to the client)
     * @return void
     */
    public function actionDialaction()
    {
//        $callUuid = Yii::app()->request->getPost('CallUUID');
//
//        // If we can't find the click to call session stop right here
//        if(!$callUuid) {
//            throw new Exception('Unable to locate click to call uuid');
//        }

//        $response = Yii::app()->plivo->getPlivo()->record(array(
//                'call_uuid' =>  $callUuid,
//                'time_limit' => 3600, // 1 hour
//                'redirect' => false,
//                'callback_url' => StmFunctions::getSiteUrl() . '/plivoclicktocall/recordcallback',
//            ));

        //Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Dial action. See post data.', CLogger::LEVEL_ERROR);

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Dial Callback
     *
     * URL that is notified by Plivo when one of the following events occur: Called party is bridged with caller, called party hangs up, or caller has pressed any digit
     * @return void
     */
    public function actionDialcallback()
    {
        //@todo: Store extra Plivo data in to columns that haven't been put in the new table just yet
//        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: DialCallback action. See post data.', CLogger::LEVEL_ERROR);

        // Send XML response
        $this->_sendResponse();
    }

    /**
     * Action Hangup
     *
     * The URL that is notified by Plivo when the call hangs up. Defaults to answer_url.
     * @return void
     */
    public function actionHangup()
    {
//        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Click to Call Hangup Action. See post data.', CLogger::LEVEL_ERROR);
        // Locate click to call session
        /** @var ClickToCallSessions $clickToCallSession */
        $clickToCallSession = ClickToCallSessions::model()->findByAttributes(array(
            'call_uuid' =>  Yii::app()->request->getPost('CallUUID')
        ));

        // If we can't find the click to call session stop right here
        if(!$clickToCallSession) {
            throw new Exception('Unable to locate click to call session');
        }

        // Update session info
        $clickToCallSession->answer_time = Yii::app()->request->getPost('AnswerTime');
        $clickToCallSession->total_cost = Yii::app()->request->getPost('TotalCost');
        $clickToCallSession->bill_rate = Yii::app()->request->getPost('BillRate');
        $clickToCallSession->bill_duration = Yii::app()->request->getPost('BillDuration');
        $clickToCallSession->event = Yii::app()->request->getPost('Event');
        $clickToCallSession->start_time = Yii::app()->request->getPost('StartTime');
        $clickToCallSession->hangup_cause = Yii::app()->request->getPost('HangupCause');
        $clickToCallSession->end_time = Yii::app()->request->getPost('EndTime');
        $clickToCallSession->duration = Yii::app()->request->getPost('Duration');

        // Attempt to save call session
        if(!$clickToCallSession->save()) {
            throw new Exception('Unable to save click to call session! Errors: '.print_r($clickToCallSession->getErrors(), 1).PHP_EOL.'Attributes: '.print_r($clickToCallSession->attributes, 1));
        }
    }

    /**
     * Action Fallback
     *
     * Invoked by Plivo only if answer_url is unavailable or the XML response is invalid. Should contain a XML response.
     * @return void
     */
    public function actionFallback()
    {
        // Log that something went wrong
        Yii::log(__CLASS__ . ' (' . __LINE__ . ') ERROR: Click to Call Fallback action. Unexpected results. See post data.', CLogger::LEVEL_ERROR);

        // Send XML response
        $this->_sendResponse();
    }
}