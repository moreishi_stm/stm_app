<?php
Yii::import('front_module.controllers.base.BaseTwilioController');

/**
 * Twilio Phone Controller
 *
 * Used to handle Twilio stuff
 */
class TwiliophoneController extends BaseTwilioController
{
    /**
     * Action Voice
     *
     * Handles voice requests for the browser phone app
     * @return voic
     */
    public function actionVoice()
    {
        // Get phone number
        $toCall = Yii::app()->request->getParam('tocall');

        // Store session
        $webRtcCallSession = new WebrtcCallSessions();
        $webRtcCallSession->contact_id = Yii::app()->request->getParam('contactId');
        $webRtcCallSession->call_sid = Yii::app()->request->getParam('CallSid');
        $webRtcCallSession->to = $toCall;
        $webRtcCallSession->from = Yii::app()->request->getParam('From');
        $webRtcCallSession->direction = Yii::app()->request->getParam('Direction');
        $webRtcCallSession->call_status = Yii::app()->request->getParam('CallStatus');
        $webRtcCallSession->added = new CDbExpression('NOW()');

        // Attempt to save call session
        if(!$webRtcCallSession->save()) {
            throw new Exception('Unable to create new webrtc call session!');
        }

        // Make the call
        $this->_xml->dial($toCall, array(
            'callerId'  =>  '+1' . YII_DEBUG ? '9412001056' : '9044793200',   // Nicole's Test Phone Number for development
            'record'    =>  'record-from-ringing',
            'action'    =>  StmFunctions::getSiteUrl() . '/twiliophone/callback'
        ));

        // Send the response
        $this->_sendResponse();
    }

    /**
     * Callback
     *
     * Called during certain call events
     * @throws Exception When unable to update call session
     * @return void
     */
    public function actionCallback()
    {
        // Lookup call session
        /** @var WebrtcCallSessions $webRtcCallSession */
        $webRtcCallSession = WebrtcCallSessions::model()->findByAttributes(array(
            'call_sid' => Yii::app()->request->getParam('CallSid')
        ));

        // Stop here if we can't find the record
        if(!$webRtcCallSession) {
            throw new Exception('Error, unable to locate webrtc call session on hangup!');
        }

        // Update data about call
        $webRtcCallSession->call_status = Yii::app()->request->getParam('CallStatus') ? Yii::app()->request->getParam('CallStatus') : $webRtcCallSession->call_status;
        $webRtcCallSession->duration = Yii::app()->request->getParam('Duration') ? Yii::app()->request->getParam('Duration') : $webRtcCallSession->duration;
        $webRtcCallSession->call_duration = Yii::app()->request->getParam('CallDuration') ? Yii::app()->request->getParam('CallDuration') : $webRtcCallSession->call_duration;
        $webRtcCallSession->timestamp = Yii::app()->request->getParam('Timestamp') ? Yii::app()->request->getParam('Timestamp') : $webRtcCallSession->timestamp;

        $webRtcCallSession->recording_sid = Yii::app()->request->getParam('RecordingSid') ? Yii::app()->request->getParam('RecordingSid') : $webRtcCallSession->recording_sid;
        $webRtcCallSession->recording_url = Yii::app()->request->getParam('RecordingUrl') ? Yii::app()->request->getParam('RecordingUrl') : $webRtcCallSession->recording_url;

        // Attempt to save call session
        if(!$webRtcCallSession->save()) {
            throw new Exception('Unable to update webrtc call session on hangup!');
        }

        // Send the response
        $this->_sendResponse();
    }
}