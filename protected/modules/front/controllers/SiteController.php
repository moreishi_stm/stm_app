<?php

class SiteController extends FrontController
{
	public $mainHomePageImage;
    public $pageDescription;
    public $pageKeywords;
    public $pageMetaTags;
    public $trackingScripts;
	public $mainSearchValue = "";

    public function actions()
    {
        return array('index'                          => 'stm_app.modules.front.controllers.actions.site.IndexAction',
                     'indexStoryboard'                => 'stm_app.modules.front.controllers.actions.site.IndexStoryboardAction',
                     'optout'                         => 'stm_app.modules.front.controllers.actions.site.OptOutAction',
                     'optout-confirm'                 => 'stm_app.modules.front.controllers.actions.site.OptOutConfirmAction',
                     'bb'                             => 'stm_app.modules.front.controllers.actions.site.BbAction',
                     'blog'                           => 'stm_app.modules.front.controllers.actions.site.BlogAction',
                     'blogTag'                        => 'stm_app.modules.front.controllers.actions.site.BlogTagAction',
                     'click'                          => 'stm_app.modules.front.controllers.actions.site.TrackLinkAction',
                     'click2'                         => 'stm_app.modules.front.controllers.actions.site.ClickAction',
                     'click3'                         => 'stm_app.modules.front.controllers.actions.site.Click3Action',
                     'login'                          => 'stm_app.modules.front.controllers.actions.site.LoginAction',
                     'logout'                         => 'stm_app.modules.front.controllers.actions.site.LogoutAction',
                     'error'                          => 'stm_app.modules.front.controllers.actions.site.ErrorAction',
                     'area'                           => 'stm_app.modules.front.controllers.actions.site.AreaAction',
                     'buyer'                          => 'stm_app.modules.front.controllers.actions.site.BuyerAction',
                     'marketsnapshot'                 => 'stm_app.modules.front.controllers.actions.site.MarketsnapshotAction',
                     'mortgagecalculator'             => 'stm_app.modules.front.controllers.actions.site.MortgagecalculatorAction',
                     'propertytaxcalculator'          => 'stm_app.modules.front.controllers.actions.site.PropertytaxcalculatorAction',
                     'team'                           => 'stm_app.modules.front.controllers.actions.site.TeamAction',
                     'contact'                        => 'stm_app.modules.front.controllers.actions.site.ContactusAction',
                     'mortgage'                       => 'stm_app.modules.front.controllers.actions.site.MortgageAction',
                     'home'                           => 'stm_app.modules.front.controllers.actions.site.HomeDetailsAction',
                     'homes'                          => 'stm_app.modules.front.controllers.actions.site.HomesListAction',
                     'houseValues'                    => 'stm_app.modules.front.controllers.actions.site.HouseValuesAction',
                     'landingPages'                   => 'stm_app.modules.front.controllers.actions.site.LandingPagesAction',
                     'storyboard'                     => 'stm_app.modules.front.controllers.actions.site.StoryboardAction',
                     'storyboardList'                 => 'stm_app.modules.front.controllers.actions.site.StoryboardListAction',
                     'values'                         => 'stm_app.modules.front.controllers.actions.site.ValuesAction',
                     'map'                            => 'stm_app.modules.front.controllers.actions.site.MapAction',
                     'marketUpdates'                  => 'stm_app.modules.front.controllers.actions.site.MarketUpdatesAction',
                     'page'                           => 'stm_app.modules.front.controllers.actions.site.PageAction',
                     'seller'                         => 'stm_app.modules.front.controllers.actions.site.SellerAction',
                     'search'                         => 'stm_app.modules.front.controllers.actions.site.SearchAction',
                     'searchAutocompleteNeighborhood' => 'stm_app.modules.front.controllers.actions.site.SearchAutocompleteNeighborhoodAction',
                     'reverseProspects'               => 'stm_app.modules.front.controllers.actions.site.ReverseProspectsAction',
                     'printFlyer'                     => 'stm_app.modules.front.controllers.actions.site.PrintFlyerAction',
                     'install'                        => 'stm_app.modules.front.controllers.actions.site.InstallAction',
                     'forgotPassword'                 => 'stm_app.modules.front.controllers.actions.site.ForgotPasswordAction',
                     'privacy'                        => 'stm_app.modules.front.controllers.actions.site.PrivacyAction',
					 'home-ajax'					  => 'stm_app.modules.front.controllers.actions.site.HomeDetailsAjaxAction',
					 'searchSuggestions'			  => 'stm_app.modules.front.controllers.actions.site.SearchSuggestionsAction',
                     'videoEmail'                     => 'stm_app.modules.front.controllers.actions.site.VideoEmailAction',
                     'referanagent'                   => 'stm_app.modules.front.controllers.actions.site.ReferAnAgentAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return CMap::mergeArray(
            parent::filters(), array()
        );
    }
}
