<?php
Yii::import('admin_module.components.StmPlivo.plivo', true);
// Set an exception handler to read the exception over the phone lolol
if(YII_DEBUG) {

    // Exception handler function
    function handlePlivoExceptions(Exception $exception) {

        // Log contents to a file
//        file_put_contents('/tmp/plivo/exceptions-' . date('Y-m-d_h-i-s') . '_' . microtime(true), $exception->getMessage() . "\n" . print_r($exception->getTraceAsString(), true));

        // Generate error response
        $response = new \StmPlivoFunctions\Response();
        $response->addSpeak($exception->getMessage());

        // Clean the output buffer
        ob_clean();

        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $response->toXML();

        // Terminate execution
        Yii::app()->end();
    }

    // Set the exception handler
    set_exception_handler('handlePlivoExceptions');
}

/**
 * Plivo 2 Controller
 *
 * Used to handle Plivo callbacks
 */
class PhoneController extends FrontController
{
    /**
     * Log Location
     *
     * Where to store logs
     * @var string Directory path
     */
    const LOG_LOCATION = '/tmp/plivo';

    /**
     * Response
     *
     * Response object for Plivo
     * @var \StmPlivoFunctions\Response
     */
    protected $_response;

    /**
     * Init
     *
     * Things that happen before each controller action
     * @return void
     */
    public function init()
    {
        // New Plivo Response
        $this->_response = new \StmPlivoFunctions\Response();

        // Chain parent init
        parent::init();
    }

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        // Log things for informational purposes
//        $this->_logThings();

        // Chain parent init
        return parent::beforeAction($action);
    }

    /**
     * Action Answer
     *
     * The URL Plivo will request when a phone number or endpoint assigned to this application receives a call. (From Plivo Docs)
     * @return void
     */
    public function actionAnswer()
    {
        // Retrieve the to phone number
        $to = Yii::app()->request->getPost('To');

        // Add dial element
        $dial = $this->_response->addDial(array(
//            'callbackUrl'   =>  StmFunctions::getSiteUrl() . '/plivocall/callback?To=' . $to,
        ));

        // Add the to phone number
        $dial->addNumber($to);

        // Send the response
        $this->_sendResponse();
    }

    /**
     * Action Fallback
     *
     * The URL that Plivo will request if the Answer URL fails or returns a non 200 HTTP status code.
     * @return void
     */
    public function actionFallback()
    {
        $this->_sendResponse();
    }

    /**
     * Action Hangup
     *
     * The URL that Plivo will request when a call to a phone number or endpoint assigned to this application is completed.
     * @return void
     */
    public function actionHangup()
    {
        $this->_sendResponse();
    }

    /**
     * Send Response
     *
     * Send the XML response
     * @return void
     */
    protected function _sendResponse()
    {
        // Clean the output buffer
        ob_clean();

        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $this->_response->toXML();

        // Terminate execution
        Yii::app()->end();
    }

    /**
     * Log Things
     *
     * Logs API calls from Plivo for informational purposes
     * @return void
     */
    protected function _logThings()
    {
        // Clean output buffer
        ob_clean();
        ob_start();

        print "Full URL: " . 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}\n\n";
        print "Action: " . $this->action->id."\n\n";

        // Test receiving data
        print "POST Variables \n";
        print "------------- \n\n";
        print_r($_POST);

        print "\n\nGET Variables \n";
        print "------------- \n\n";
        print_r($_GET);

        print "\n\nSERVER Variables \n";
        print "---------------- \n\n";
        print_r($_SERVER);

        // Nginx doesn't have this functionality =(
        if(function_exists('getallheaders')) {
            print "\n\nPost Headers \n";
            print "--------- \n\n";
            print_r(getallheaders());
        }

        // Store the data
        file_put_contents(self::LOG_LOCATION . '/plivo-phone-in-log-' . date('Y-m-d_h-i-s') .'_'.microtime(true). '-' . Yii::app()->controller->action->id, ob_get_contents(), FILE_APPEND); //microtime(true)
        ob_clean();
    }
}