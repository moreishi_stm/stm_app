<?php
class PageViewTrackerWidget extends CWidget {

	// defines the cookie key storing the page views
	const PAGE_VIEW_COOKIE_KEY       = 'page_views';
	const PAGE_TYPE_PAGES            = 0;
	const PAGE_TYPE_PAGES_KEY        = 'url';
	const PAGE_TYPE_HOME_DETAILS     = 1;
	const PAGE_TYPE_HOME_DETAILS_KEY = 'listing_id';
	const DUPLICATE_TIME_LIMIT       = '300 seconds'; //must be 'X seconds'

	// when this cookie should expire
	public $cookieExpirationTime = null;

	// Maximum number of characters in cookie json value
	const MAX_COOKIE_CHAR_LENGTH = 4905;

	// Store the page view data
	private $_pageViewData,
			$ignore_pages=array('/logout', '/myAccount');

	public function init() {

		if (!$this->cookieExpirationTime)
			$this->cookieExpirationTime = time() + 60*60*24*180; // 6 months
	}

	public function run() {

		if ($this->isPageInIgnoreList)
			return;

		$this->save();
	}

	public function save() {

		// Store the page view data in a cookie if the user is a guest
		if (Yii::app()->user->isGuest) {
			$this->saveToCookie();
		} else {
			$this->saveToDb();
			Contacts::updateLastLogin();
		}
	}

	public function getIsPageInIgnoreList() {

		return in_array(Yii::app()->request->url, $this->ignore_pages);
	}

	public function getPageViewData() {

		if (!$this->_pageViewData) {

			$this->_pageViewData = array(
				$this->cookieKey => $this->cookieValue,
				'ip' => Yii::app()->stmFunctions->getVisitorIp(),
				'added' => date('Y-m-d H:i:s'),
			);
		}

		return $this->_pageViewData;
	}

	/**
	 * Wrapper to determine what key to store in the cookie
	 * @return string key identifier
	 */
	public function getCookieKey() {

		if ($this->pageType === self::PAGE_TYPE_HOME_DETAILS)
			return self::PAGE_TYPE_HOME_DETAILS_KEY;

		return self::PAGE_TYPE_PAGES_KEY;
	}

	/**
	 * Wrapper to extract the appropriate value to store in the cookie
	 * MLS_ID : for Home Details Action
	 * URL : All others
	 * @return mixed
	 */
	public function getCookieValue() {

		if ($this->pageType === self::PAGE_TYPE_HOME_DETAILS)
			return Yii::app()->request->getQuery('id');

		return Yii::app()->request->url;
	}

	/**
	 * Determine what type of page we are tracking
	 * @return int
	 */
	public function getPageType() {

		if ((Yii::app()->controller->action->id == 'home')) {
			return self::PAGE_TYPE_HOME_DETAILS;
		}

		return self::PAGE_TYPE_PAGES;
	}

	protected function saveToCookie() {

		$pageViewsData = array();
        $listingIds = array();
		// If the cookie exists then lets pre-populate the tracking data array, otherwise create a new instance of it
		if (Yii::app()->request->hasCookie(self::PAGE_VIEW_COOKIE_KEY)) {

			$cookieJSON = Yii::app()->request->cookies[self::PAGE_VIEW_COOKIE_KEY]->value;
			$pageViewsData  = CJSON::decode($cookieJSON);

			// If home view, check to see if it's in duplicate timeframe parameters
			$now = new DateTime;

            if(!empty($pageViewsData)) {
                // If current view exists in cookie + w/in dup time limit setting, ignore and don't save info
                foreach($pageViewsData as $i => $view) {

                    //remove duplicates listingIds
                    if($view['listing_id']) {
                        if(!in_array($view['listing_id'], $listingIds)) {
                            $listingIds[] = $view['listing_id'];
                        }
                        else {
                            // remove duplicates
                            unset($pageViewsData[$i]);
                        }
                    }

                    $added = new DateTime($view['added']);
                    $time_limit = $added->add(DateInterval::createFromDateString(self::DUPLICATE_TIME_LIMIT));
                    // if the current page is equal to existing cookie, is dupe so exist function as no more processing needed
                    if (($now < $time_limit) && isset($view[$this->cookieKey]) && ($view[$this->cookieKey]==$this->cookieValue)) {
                        return;
                    }
                }
            }

			// If the maximum length of the cookie is reached then pop off the first element (FIFO)
			if (mb_strlen($cookieJSON) >= self::MAX_COOKIE_CHAR_LENGTH) {
                array_shift($pageViewsData);
            }
		}

        if(empty($pageViewsData) && !is_array($pageViewsData)) {
            $pageViewsData = array();
        }

        if(!is_array($pageViewsData)) {
            Yii::log(__CLASS__.' ('.__LINE__.') Page View Data is not an array'.print_r($pageViewsData,true), CLogger::LEVEL_ERROR);
            $pageViewsData = (array) $pageViewsData;
        }

        // merges cookie with pageView data array if not already exist in cookie
        $currentViewData = $this->pageViewData;
        if(isset($currentViewData['listing_id']) && !in_array($currentViewData['listing_id'], $listingIds)) {
            array_push($pageViewsData, $this->pageViewData);
        }

        // get unique
		return Yii::app()->request->setCookie(self::PAGE_VIEW_COOKIE_KEY, CJSON::encode($pageViewsData), 60*60*24*180); //6 months
	}

	/**
	 * deleteCookie deletes the PageViews tracking cookie, used when someone logs in and cookie is no longer needed
	 * @return none
	 */
	protected function deleteCookie() {

		unset(Yii::app()->request->cookies[PageViewTrackerWidget::PAGE_VIEW_COOKIE_KEY]);
	}

	protected function saveToDb()
    {
		$model = ViewedPages::model()->find(array('condition'=>$this->cookieKey.'=:value AND contact_id=:contact_id',
												  'params'=>array(':value'=>$this->cookieValue,':contact_id'=>Yii::app()->user->id),
												  'order'=>'added DESC'
											));
        //@todo: this is not catching dups********************
		if (!empty($model) && $this->isDupPerTimeLimit($model->added)){
            return;
        }

		$model = new ViewedPages;
		$model->added = date('Y-m-d H:i:s');
		$model->ip = Yii::app()->stmFunctions->getVisitorIp();
		$model->{$this->cookieKey} = $this->cookieValue;
		$model->page_type_ma = $this->pageType;

        // if page is home detail, listing_id has value, then assign mls_board_id
        if($model->listing_id) {
            if(Yii::app()->user->hasMultipleBoards) {
                // check to see if this account has multiple active mls feeds
                $model->mls_board_id = Yii::app()->request->getQuery('mlsBoardId');
            }
            else {
                $model->mls_board_id = Yii::app()->user->board->id;
            }
        }

		$model->contact_id = Yii::app()->user->id;
		if(!$model->save()) {
            Yii::log(__CLASS__.' (:'.__LINE__.') Viewed Page did not save. Attributes: '.print_r($model->attributes, true).' Error Message: '.print_r($model->getErrors(), true), CLogger::LEVEL_ERROR);
        }
	}

	protected function isDupPerTimeLimit($compareTime) {
		$now = new DateTime;
		$added = new DateTime($compareTime);
		$time_limit = $added->add(DateInterval::createFromDateString(self::DUPLICATE_TIME_LIMIT));
		return ($now < $time_limit) ? true : false;
	}

	/**
	 * [countHomesViewed description]
	 * @param  boolean $unique if set to true, counts unique listing_id only, ignore count of views of same home multiple times
	 * @return int $count
	 */
	static function countHomesViewed($unique=false) {
		if (Yii::app()->user->isGuest) {
			$cookieJSON    = Yii::app()->request->cookies[self::PAGE_VIEW_COOKIE_KEY]->value;
			$pageViewsData = CJSON::decode($cookieJSON);
            $count = 0;
			$viewedListingIds = array();
			if ($pageViewsData)
				foreach($pageViewsData as $view) {
					if (isset($view[self::PAGE_TYPE_HOME_DETAILS_KEY])) {
						if ($unique==true) {
							// See if it's unique by checking viewListingId's array, if so then increment counter
							if (!in_array($view[self::PAGE_TYPE_HOME_DETAILS_KEY], $viewedListingIds)) {
								array_push($viewedListingIds, $view[self::PAGE_TYPE_HOME_DETAILS_KEY]);
								$count++;
							}
						} else {
							$count++;
						}
					}
				}
			return $count;
		} else {
			return ViewedPages::countHomesViewed();
		}
	}

	static function listHomesViewed() {
		if (Yii::app()->user->isGuest) {
			if (isset(Yii::app()->request->cookies[self::PAGE_VIEW_COOKIE_KEY]) && Yii::app()->request->cookies[self::PAGE_VIEW_COOKIE_KEY]->value!='null') {
				$cookieJSON = Yii::app()->request->cookies[self::PAGE_VIEW_COOKIE_KEY]->value;
				$pageViewsData  = CJSON::decode($cookieJSON);

				$listingIds = array();
				foreach($pageViewsData as $key=>$viewData)
					array_push($listingIds, $viewData['listing_id']);
			}
		} else {
			$listingIds = ViewedPages::getListingIds();
		}
		return $listingIds;
	}

	static function processLogin() {
		$widget = new PageViewTrackerWidget;
		$widget->cookieToDb();
		$widget->deleteCookie();
	}

	/**
	 * cookieToDb Handles the cookies when a user logins in. Pushes cookie data to the db and empties the cookie. Called from LoginAction
	 * @return none
	 */
	private function cookieToDb() {
		if (Yii::app()->user->isGuest)
			return;

		// Handles the process of uploading pageview tracking from cookie to db for logged in user
		$cookieJSON    = Yii::app()->request->cookies[self::PAGE_VIEW_COOKIE_KEY]->value;
		if ($cookieJSON) {
			$pageViewsData = CJSON::decode($cookieJSON);

			foreach($pageViewsData as $view) {
				$model = new ViewedPages;
				if (isset($view[self::PAGE_TYPE_HOME_DETAILS_KEY])) {
					$model->{self::PAGE_TYPE_HOME_DETAILS_KEY} = $view[self::PAGE_TYPE_HOME_DETAILS_KEY];
				} elseif (isset($view[self::PAGE_TYPE_PAGES_KEY])) {
					$model->{self::PAGE_TYPE_PAGES_KEY} = $view[self::PAGE_TYPE_PAGES_KEY];
				}

                //@todo: add mls_board_id
                // check to see if more than 1 board, if yes use default mls_board_id

				$model->added = $view['added'];
				$model->ip = $view['ip'];
				$model->page_type_ma = (isset($view[self::PAGE_TYPE_HOME_DETAILS_KEY])) ? self::PAGE_TYPE_HOME_DETAILS : self::PAGE_TYPE_PAGES ;
				$model->contact_id = Yii::app()->user->id;
				$model->save();
			}
		}
	}
}