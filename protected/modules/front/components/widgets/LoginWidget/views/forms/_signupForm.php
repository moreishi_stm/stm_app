<span id="close">X</span>
<h3>I'm a New User!</h3>
<hr />
<? $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-widget-form',
    'action'=>array('/front/forms/register/formId/'.$formId),
	'enableAjaxValidation'=>true,
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnChange' => false,
        'validateOnSubmit' => true,
	    'beforeValidate' => 'js:function(form) {
			$("#login-widget-form #login-form-button").prop("disabled", true);

			return true;
	    }',
        'afterValidate' => 'js:function(form, data, hasErrors) {
			if (!hasErrors) {
				// Action was successful
				window.location = "/myAccount/"+data.email;
				return false;
			} else {
				Message.create("error", "Please fill out the required fields and re-submit.");
				$("#login-widget-form #login-form-button").prop("disabled", false);
			}
		}',
    ),
));
?>
	<div class="row">
		<label id="name" class="label">Name<span class="required">*</span></label>
	    <?php echo $form->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('class'=>'validate[required] name','placeholder'=>'First Name','data-prompt-position'=>'topLeft'))?>
	    <?php echo $form->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('class'=>'validate[required] name','placeholder'=>'Last Name','data-prompt-position'=>'topRight'))?>
        <?php echo $form->error($model, 'data['. $FormFields->getField('first_name')->id .']'); ?><?php echo $form->error($model, 'data['. $FormFields->getField('last_name')->id .']'); ?>
    </div>
	<div class="row">
		<!-- <label id="email" class="label"><span class="required">*</span>E-mail:</label> <input type="text" name="email" /> -->
	    <?php echo $form->labelEx($model, 'data['. $FormFields->getField('email')->id .']')?>
	    <?php echo $form->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email','class'=>'validate[required,custom[email]]'))?>
	    <?php echo $form->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
	</div>
	<div class="row">
        <?php echo $form->labelEx($model, 'data['. $FormFields->getField('phone')->id .']')?>
        <?php $this->widget('StmMaskedTextField', array(
            'model' => $model,
            'attribute' => 'data['. $FormFields->getField('phone')->id .']',
            'mask' => '(999) 999-9999',
            'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'validate[required,custom[phone]]', 'data-prompt-position'=>'bottomRight'),
        )); ?>
	    <?php echo $form->error($model, 'data['. $FormFields->getField('phone')->id .']'); ?>
		<!-- <label id="password" class="label"><span class="required">*</span>Password</label> <input type="password" name="password" /> -->
	</div>
	<div class="row">
		<button id="login-form-button" type="submit">Login</button>
	</div>
	<div class="row">
		<span id="forgot-password" class="link">Forgot Password?</span>
		<span id="return-to-login" class="link child">Return to Login</span>
	</div>
<?php $this->endWidget(); ?>
