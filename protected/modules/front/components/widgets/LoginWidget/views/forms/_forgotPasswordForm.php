<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'forgot-password-form',
	'enableAjaxValidation' => false,
	'action' => '/forgotPassword',
));
?>
	<span id="close">X</span>
	<h3>Forgot Password</h3>
	<hr/>
	<div class="row" style="margin:17px 0px 9px 0px;">
		<?php echo $form->labelEx($model, 'email'); ?>
		<?php echo $form->textField($model, 'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>
	<p style="margin:8px 0px 10px 0px; clear:both;">For immediate support Call <strong><?php echo Yii::app()->getUser()->getSettings()->office_phone; ?>.</strong></p>
	<div class="row">
		<button id="forgot-password-button" type="submit">Send Reminder</button>
	</div>
	<div class="row">
		<span id="return-to-login" class="link">Return to Login</span>
	</div>

<?php $this->endWidget(); ?>