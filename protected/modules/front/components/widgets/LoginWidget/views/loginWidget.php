<?php if (Yii::app()->user->isGuest): ?>

	<div id="login-widget" class="guest">
		<button id="login-button" class="gray">Sign In / New User</button>

		<!-- ======= LOGIN FORM ========================================================================================= -->

		<div id="login-form" class="form">
			<?php echo $this->render('forms/_loginForm', array('model' => $loginFormModel)); ?>
		</div>

		<div id="new-user-form" class="form">
			<?php echo $this->render('forms/_signupForm', array(
				'formId'                  => $formId,
				'model'                   => $SubmissionValues,
				'FormFields'              => $FormFields,
			)); ?>
		</div>

		<div id="forgot-password-form" class="form">
			<?php echo $this->render('forms/_forgotPasswordForm', array('model' => $forgotPasswordFormModel)); ?>
		</div>

		<div id="password-sent-form" class="form">
			<?php echo $this->render('forms/_passwordSentForm'); ?>
		</div>

	</div>
<?php else: ?>
	<div id="login-widget" class="user">
		Hi <?php echo Yii::app()->user->firstName?>!
		<?php echo CHtml::link('Logout', array('/front/site/logout'), $htmlOptions = array(
			'class' => 'btn gray',
		)); ?>
	</div>
<?php endif; ?>
