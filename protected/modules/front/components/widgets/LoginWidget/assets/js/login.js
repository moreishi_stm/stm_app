$(function() {
	var $loginForm            = $('div#login-form');
	var $loginButton          = $('#login-button');
	var $newUserForm          = $('div#new-user-form');
	var $newUser              = $('span#new-user');
	var $closeButton          = $('span#close');
	var $forgotPasswordForm   = $('div#forgot-password-form');
	var $forgotPassword       = $('span#forgot-password');
	var $forgotPasswordButton = $('button#forgot-password-button');
	var $passwordSentForm     = $('div#password-sent-form');
	var $returnToLogin        = $('span#return-to-login');

	$loginButton.on('click',function() {
		console.log('login-button clicked');
		openLoginTypeForm($loginForm);
		return false;
	});

	$newUser.click(function() {
		openLoginTypeForm($newUserForm);
		return false;
	});

	$forgotPassword.click(function() {
		openLoginTypeForm($forgotPasswordForm);
		return false;
	});

	$returnToLogin.click(function() {
		openLoginTypeForm($loginForm);
		return false;
	});

	// =====================================================================================

	// Close all divs containing forms, then open the selected form
	function openLoginTypeForm($form) {
		closeForms();
		$loginButton.addClass('selected');
		$form.show();
	}

	$closeButton.click(function() {
		return closeForms();
	});

	$('html').mousedown(function(e) {
		var $target = $(e.target);

		if($loginButton.is(e.target))
			return false;

		// Do not close the window if a input, button, or textarea is selected
		if($target.parents('#login-widget').length)
			return ($target.is('input, button, textarea')) ? null : false;

		return closeForms();
	});

	// Hide any visible divs containing a form
	function closeForms() {
		$visibleForms = $('#login-widget div.form:visible').hide();
		$loginButton.removeClass('selected');
		return;
	}
});