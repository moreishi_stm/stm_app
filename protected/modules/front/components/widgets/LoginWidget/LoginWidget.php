<?php

class LoginWidget extends StmWidget {

	private $_cssAssetsUrl;
	private $_jsAssetsUrl;

	public function init() {

        if(YII_DEBUG) { //@todo: total temp - delete this if and move else into outside when done dev
            Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.local/assets/v1/assets/css/login-s2.css?v=3');
        }
        else {
            Yii::app()->clientScript->registerCssFile($this->cssAssetsUrl . DS . ((Yii::app()->theme->name == "standard2") ? 'login-s2.css?v=3' : 'login.css'));
        }

		//Yii::app()->clientScript->registerScriptFile($this->jsAssetsUrl . DS . 'login.js', CClientScript::POS_END);
	}

	public function run() {
		$loginFormModel = new LoginForm;
		$forgotPasswordFormModel = new Emails('forgotPassword');

        $formId           = Forms::FORM_LOGIN_WIDGET_REGISTER;
        $SubmissionValues = new FormSubmissionValues($formId);
        $FormFields       = new FormFields;

		$this->render('loginWidget', array(
				'loginFormModel'          => $loginFormModel,
				'forgotPasswordFormModel' => $forgotPasswordFormModel,
				'formId'                  => $formId,
				'SubmissionValues'        => $SubmissionValues,
				'FormFields'              => $FormFields,
			)
		);
	}

	public function getCssAssetsUrl() {
		if (!$this->_cssAssetsUrl) {
			$this->_cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_module.components.widgets.LoginWidget.assets.css'), false, -1, FrontModule::REFRESH_CSS_ASSETS);
		}

		return $this->_cssAssetsUrl;
	}

	public function getJsAssetsUrl() {
		if (!$this->_jsAssetsUrl) {
			$this->_jsAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_module.components.widgets.LoginWidget.assets.js'), false, -1, FrontModule::REFRESH_JS_ASSETS);
		}

		return $this->_jsAssetsUrl;
	}
}