<?php
class ForeclosureListWidget extends CWidget{
	public $imageAssetsUrl;

	public function init() {
		$this->imageAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_module.components.widgets.ForeclosureListWidget.assets.images'), false, -1, FrontModule::REFRESH_IMAGE_ASSETS);
	}

	public function run() {
		$this->render('foreclosureListWidget', array('imageAssetsUrl'=>$this->imageAssetsUrl));
	}
}