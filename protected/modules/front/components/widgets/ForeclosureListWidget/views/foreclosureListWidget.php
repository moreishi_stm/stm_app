<style>
    #foreclosure-list-widget {
        padding: 0;
    }
    #foreclosure-list-widget a{
        color: blue;
        text-decoration: none;
    }
    #foreclosure-list-widget a:hover{
        color: #D20000;
        text-decoration: underline;
    }
    #foreclosure-list-widget span {
        width: 244px;
        display: block;
        text-align: center;
        background: none repeat scroll 0% 0% white;
        top: -57px;
        position: relative;
        font-size: 20px;
        font-weight: bold;
        padding: 10px 0;
        opacity:0.85;
    }
</style>
<div id="foreclosure-list-widget" class="foreclosure-list-widget-container">
    <a href="/area/foreclosures">
        <img src="<?php echo $this->imageAssetsUrl ?>/foreclosureList.jpg" width="244" border="0">
        <span>View Foreclosures</span>
    </a>
</div>
