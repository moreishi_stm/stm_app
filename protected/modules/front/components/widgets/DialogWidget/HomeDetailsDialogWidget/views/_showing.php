<?php
$showingRequestForm = $this->beginWidget('CActiveForm', array(
	'id' => $this->type . '-dialog-form',
    'action' => array('/front/forms/showing/formId/' . $this->formId),
    'enableAjaxValidation'   => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
    	'validateOnChange' => false,
    	'validateOnSubmit' => true,
        'inputContainer' => 'span',
		'afterValidate' => 'js:function(form, data, hasErrors) {
		        homeDetailsAfterValidate();

				if (!hasErrors) {
					// Action was successful
					$("#'.$this->id.'").dialog("close");
					Message.create("success", "Your Showing Request has been submitted!");
					return false;
				}
			}',
    ),
));

$model->setScenario(Forms::FORM_SHOWING_REQUEST);

?>
<div class="dialog-form-container">
    <table>
    	<tr>
    		<th>Name <span class="required">*</span>:</th>
    		<td>
                <span>
    	    		<?php echo $showingRequestForm->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                </span>
                <span>
    	    		<?php echo $showingRequestForm->textField($model,  'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $showingRequestForm->error($model, 'data['. $FormFields->getField('first_name')->id .']');?>
                <?php echo $showingRequestForm->error($model, 'data['. $FormFields->getField('last_name')->id .']');?>
        	</td>
    	</tr>
    	<tr>
    		<th><?php echo $showingRequestForm->labelEx($model, 'data['. $FormFields->getField('email')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $showingRequestForm->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'g10', ));?>
                </span>
                <?php echo $showingRequestForm->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $showingRequestForm->labelEx($model, 'data['. $FormFields->getField('phone')->id .']')?>:</th>
    		<td>
                <span>
                    <?php $this->widget('StmMaskedTextField', array(
                        'model' => $model,
                        'attribute' => 'data['. $FormFields->getField('phone')->id .']',
                        'mask' => '(999) 999-9999',
                        'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'g5'),
                    )); ?>
                </span>
                <?php echo $showingRequestForm->error($model, 'data['. $FormFields->getField('phone')->id .']');?>
        	</td>
    	</tr>
    	<tr>
    		<th>Date/Time <span class="required">*</span>:</th>
    		<td>
                <span>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'data['. $FormFields->getField('contact_date')->id .']',
                        'options' => array('showAnim' => 'fold'),
                        'htmlOptions' => array('placeholder' => 'Date','class' =>'g5'),
                    )); ?>
                </span>
                <span>
    	    		<?php echo $showingRequestForm->textField($model, 'data['. $FormFields->getField('contact_time')->id .']', $htmlOptions=array('placeholder'=>'Time', 'class'=>'g5', ));?>
                </span>
                <?php echo $showingRequestForm->error($model, 'data['. $FormFields->getField('contact_date')->id .']');?>
                <?php echo $showingRequestForm->error($model, 'data['. $FormFields->getField('contact_time')->id .']');?>
        	</td>
    	</tr>
    	<tr>
    		<th></th>
    		<td>
				<!-- <input type="checkbox" />Yes, show me other similar homes. -->
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $showingRequestForm->labelEx($model, 'data['. $FormFields->getField('comments')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $showingRequestForm->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('placeholder'=>'Comments', 'class'=>'g10', ));?>
                </span>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" class="submit-button-row">
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$this->property->listing_id));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$this->photoUrl));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('home_url') .']', $htmlOptions=array('value'=>$this->property->getUrl()));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$this->property->price));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$this->property->bedrooms));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$this->property->baths_total));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$this->property->sq_feet));?>

                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->streetAddress)))));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->city)))));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->state)))));?>
                <?php echo $showingRequestForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$this->property->zip));?>

                <div class="home-details-dialog-loading"></div>
                <input class="home-details-dialog-submit-button btn btn-success btn-primary btn-lg" type="submit" value="Submit Now">
            </td>
        </tr>
    </table>
</div>
<? $this->endWidget(); //end CActiveForm ?>
