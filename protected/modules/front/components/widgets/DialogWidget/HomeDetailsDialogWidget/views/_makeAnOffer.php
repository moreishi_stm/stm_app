<?php
$makeAnOfferForm = $this->beginWidget('CActiveForm', array(
	'id' => $this->type.'-dialog-form',
    'action' => array('/front/forms/makeAnOffer/formId/' . $this->formId),
    'enableAjaxValidation'   => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
    	'validateOnChange' => false,
    	'validateOnSubmit' => true,
        'inputContainer' => 'span',
		'afterValidate' => 'js:function(form, data, hasErrors) {
		        homeDetailsAfterValidate();

				if (!hasErrors) {
					// Action was successful
					$("#'.$this->id.'").dialog("close");
					Message.create("success", "Your Offer Request has been submitted!");
					return false;
				}
			}',
    ),
));

$model->setScenario(Forms::FORM_MAKE_AN_OFFER_HOME_DETAILS);
?>

<div class="dialog-form-container">
    <table>
        <tr>
            <th><?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('price')->id .']')?>:</th>
            <td>
                <span>
    	    		<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('price')->id .']', $htmlOptions=array('placeholder'=>'Offer Price', 'class'=>'g10', ));?>
                </span>
                <?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('price')->id .']'); ?>
            </td>
        </tr>
    	<tr>
    		<th>Name <span class="required">*</span>:</th>
    		<td>
                <span>
    	    		<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                </span>
                <span>
    	    		<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('first_name')->id .']');?>
                <?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('last_name')->id .']');?>
        	</td>
    	</tr>
    	<tr>
    		<th><?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('email')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'g10', ));?>
                </span>
                <?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('phone')->id .']')?>:</th>
    		<td>
                <span>
                    <?php $this->widget('StmMaskedTextField', array(
                        'model' => $model,
                        'attribute' => 'data['. $FormFields->getField('phone')->id .']',
                        'mask' => '(999) 999-9999',
                        'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'g5'),
                    )); ?>
                </span>
                <?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('phone')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('comments')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $makeAnOfferForm->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('placeholder'=>'Comments', 'class'=>'g10', ));?>
                </span>
                <?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('comments')->id .']'); ?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$this->property->listing_id));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$this->photoUrl));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$this->property->price));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$this->property->bedrooms));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$this->property->baths_total));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$this->property->sq_feet));?>

                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->streetAddress)))));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->city)))));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->state)))));?>
                <?php echo $makeAnOfferForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$this->property->zip));?>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" class="submit-button-row">
                <div class="home-details-dialog-loading"></div>
                <input class="home-details-dialog-submit-button btn btn-success btn-primary btn-lg" type="submit" value="Submit Now">
            </td>
        </tr>
    </table>
</div>
<? $this->endWidget(); //end CActiveForm ?>
