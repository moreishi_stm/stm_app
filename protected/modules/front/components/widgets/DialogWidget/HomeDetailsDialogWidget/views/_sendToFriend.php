<?php
$sendToFriendForm = $this->beginWidget('CActiveForm', array(
	'id' => $this->type.'-dialog-form',
    'action' => array('/front/forms/sendToFriend/formId/' . $this->formId),
    'enableAjaxValidation'   => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
    	'validateOnChange' => false,
    	'validateOnSubmit' => true,
        'inputContainer' => 'span',
		'afterValidate' => 'js:function(form, data, hasErrors) {
		        homeDetailsAfterValidate();

                if (!hasErrors) {
					// Action was successful
					$("#'.$this->id.'").dialog("close");
					Message.create("success", "This Property has been sent to your Friend!");
					return false;
                }
			}',
    )
));

$model->setScenario(Forms::FORM_SHARE_FRIEND);
?>
<div class="dialog-form-container">
    <table>
    	<tr>
            <th>From Name <span class="required">*</span>:</th>
    		<td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                <span>
                </span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('first_name')->id .']');?>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('last_name')->id .']');?>
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $sendToFriendForm->labelEx($model, 'data['. $FormFields->getField('email')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'g10', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('email')->id .']');?>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" style="text-align:center;"><hr /></td>
        </tr>
        <tr>
            <th>To Name <span class="required">*</span>:</th>
            <td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('to_first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                </span>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('to_last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('to_first_name')->id .']');?>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('to_last_name')->id .']');?>
            </td>
        </tr>
        <tr>
            <th><?php echo $sendToFriendForm->labelEx($model, 'data['. $FormFields->getField('to_email')->id .']')?>:</th>
            <td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('to_email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'g10', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('to_email')->id .']'); ?>
            </td>
        </tr>
    	<tr>
            <th><?php echo $sendToFriendForm->labelEx($model, 'data['. $FormFields->getField('comments')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $sendToFriendForm->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('placeholder'=>'Comments', 'class'=>'g10', ));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$this->property->listing_id));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$this->photoUrl));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$this->property->price));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$this->property->bedrooms));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$this->property->baths_total));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$this->property->sq_feet));?>

                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->streetAddress)))));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->city)))));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->state)))));?>
                    <?php echo $sendToFriendForm->hiddenField($model, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$this->property->zip));?>
                </span>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" class="submit-button-row">
                <div class="home-details-dialog-loading"></div>
                <input class="home-details-dialog-submit-button btn btn-success btn-primary btn-lg" type="submit" value="Submit Now">
            </td>
        </tr>
    </table>
</div>
<? $this->endWidget(); //end CActiveForm ?>
