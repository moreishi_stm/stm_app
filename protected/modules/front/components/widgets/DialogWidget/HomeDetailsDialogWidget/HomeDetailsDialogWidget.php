<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class HomeDetailsDialogWidget extends DialogWidget {

	const FORM_SHOWING_REQUEST_ID = 2;
	const FORM_SEND_TO_FRIEND_ID  = 3;
	const FORM_ASK_QUESTION_ID    = 4;
	const FORM_PROPERTY_TAX_ID    = 5;

	public $dialogClass = 'home-details-dialog';
	public $property;
	public $formView;
	public $type;
    public $photoUrl;
    public $isGuest=null;

	public function init() {
		$this->registerAssets();
		$this->homeDetailsDialogScript();
		parent::init();
	}

	protected function registerAssets() {
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_widgets.DialogWidget.HomeDetailsDialogWidget.assets.css'), false, -1, FrontModule::REFRESH_CSS_ASSETS);
		if(Yii::app()->theme->name != "standard2") {
			Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'homeDetails.css');
		}
	}

	public function run() {
        if($this->isGuest===null) {
            $this->isGuest = Yii::app()->user->isGuest;
        }

		// Can't run without a way to call the dialog
		if (!$this->triggerElement) {
			return false;
		}

		$SubmissionValues = new FormSubmissionValues;
		$FormFields = new FormFields;

		switch ($this->type) {
			case 'send-to-friend':
				$this->formId = self::FORM_SEND_TO_FRIEND_ID;
				break;
			case 'showing-request':
				$this->formId = self::FORM_SHOWING_REQUEST_ID;
				break;
			case 'ask-question':
				$this->formId = self::FORM_ASK_QUESTION_ID;
				break;
			case 'property-tax':
				$this->formId = self::FORM_PROPERTY_TAX_ID;
				break;
            case 'make-an-offer':
                $this->formId = Forms::FORM_MAKE_AN_OFFER_HOME_DETAILS;
                break;
			default:
                $this->formId = null; //@todo: prolly need to throw error of some sort
				break;
		}


		// If the form fields exist, and a user is logged in, prepopulate the dialogs with the user's information
		if (!$this->isGuest) {

			if ($FormFields->getField('first_name'))
				$SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;

			if ($FormFields->getField('last_name'))
				$SubmissionValues->data[$FormFields->getField('last_name')->id] = Yii::app()->user->lastName;

			if ($FormFields->getField('email'))
				$SubmissionValues->data[$FormFields->getField('email')->id] = Yii::app()->user->getContact()->getPrimaryEmail();

			if ($FormFields->getField('phone'))
				$SubmissionValues->data[$FormFields->getField('phone')->id] = Yii::app()->user->getContact()->getPrimaryPhone();
		}

		$this->render('homeDetailsDialog', array(
			'model'      => $SubmissionValues,
			'FormFields' => $FormFields
		));
	}
	/**
	  * Used to register the trigger script for calling the dialog. Override this to change this functionality.
	  * @return null
	  */
	public function homeDetailsDialogScript() {
        $formType = $this->type;
 		$isYiiDebug = (YII_DEBUG) ? 1 : 0;
		$js = <<<JS
		    function homeDetailsAfterValidate() {
                $("div.loading-container.loading").remove();
//                // Enable the submit button
//                var submitButton = $("#'.$this->type.'-dialog-form").find(".home-details-dialog-submit-button");
//                submitButton.removeAttr("disabled");
//
//                $(".home-details-dialog-loading").removeClass("loading");
		    }

			Message.messagePosition = "top-center";

            var formNameId = "#" + "$formType" + "-dialog-form";
            $(formNameId).submit(function() {
                $("body").prepend("<div class='loading-container loading'><em></em></div>");

//					Message.create("notice", "Processing your request...");

//					var submitButton = $(formNameId).find(".home-details-dialog-submit-button");
//					submitButton.attr("disabled","disabled");

//					$(".home-details-dialog-loading").addClass("loading");

                if (!'.$isYiiDebug.') {
            // 		_gaq.push(["_trackEvent", "Required Register Form", "Click", "Required Register Click Submit"]);
            // 		_gaq.push(["b._trackEvent", "Required Register Form", "Click", "Required Register Click Submit"]);
                }
            });

            if (!$isYiiDebug) {
                // Google Goals tracking event: categories, actions, label
            // 	_gaq.push(["_trackEvent", "Required Register Form", "View", "Required Register Form View"]);
            // 	_gaq.push(["b._trackEvent", "Required Register Form", "View", "Required Register Form View"]);
            }
JS;

    	Yii::app()->clientScript->registerScript('homeDetailsDialog-'.$this->id, $js);
	}
}
