<?
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class RegisterDialogWidget extends DialogWidget {

	public $property;
    public $photoUrl;
	public $dialogClass = 'home-details-dialog home-details-register-dialog';
	public $formId;

	public function init() {
//		$this->registerAssets();
		$this->registerTriggerScript();
		parent::init();
	}

	protected function registerAssets() {
//		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_widgets.DialogWidget.RegisterDialogWidget.assets.css'), false, -1, FrontModule::REFRESH_CSS_ASSETS);
//		Yii::app()->clientScript->registerCssFile($cssAssetsUrl . DS . 'registerDialog.css');
	}

	public function run() {
		$this->formId = Forms::FORM_FORCED_REG;
		$SubmissionValues = new FormSubmissionValues($this->formId);
		$FormFields = new FormFields;

		$this->render('registerDialog', array(
			'model' => $SubmissionValues,
			'FormFields' => $FormFields,
		));
	}

	/**
	  * Used to register the trigger script for calling the dialog. Override this to change this functionality.
	  * @return null
	  */
	public function registerTriggerScript() {
		return;
 		$isYiiDebug = (YII_DEBUG) ? 1 : 0;
 		$isGuest = Yii::app()->user->isGuest;
        $hasGoogleTrackingCode = (Yii::app()->user->settings->google_tracking_code)? 1 : 0;
		$js = <<<JS
			Message.messagePosition = "top-center";

			$(function() {
				$("#register-dialog-form").submit(function() {

//					Message.create("notice", "Processing your request...");

					if (!$isYiiDebug && $hasGoogleTrackingCode) {
//						_gaq.push(["_trackEvent", "Required Register Form", "Click", "Required Register Click Submit"]);
//						_gaq.push(["b._trackEvent", "Required Register Form", "Click", "Required Register Click Submit"]);
					}
				});

				if (!$isYiiDebug && $hasGoogleTrackingCode) {
					// Google Goals tracking event: categories, actions, label
//					_gaq.push(["_trackEvent", "Required Register Form", "View", "Required Register Form View"]);
//					_gaq.push(["b._trackEvent", "Required Register Form", "View", "Required Register Form View"]);
				}
			});
JS;

    	Yii::app()->clientScript->registerScript('popupTrigger-'.$this->id, $js);
	}
}