<?php
$isYiiDebug = (YII_DEBUG) ? 1 : 0;
$hasGoogleTrackingCode = (Yii::app()->user->settings->google_tracking_code) ? 1 : 0;

$css = <<<CSS
    body {
        overflow: hidden;
    }
    .home-details-register-dialog .submit-button-row{
        padding: 23px 15px 15px 15px;
        text-align: center;
        position: relative;
    }
    .home-details-register-dialog #register-dialog-submit-button{
        text-transform: uppercase;
        font-size: 11px;
        padding-left: 18px;
        padding-right: 18px;
        margin-left: 28px;
        width: 360px;
    }
    .home-details-register-dialog #register-dialog-loading{
        width: 32px;
        height: 32px;
        text-align: center;
        position: absolute;
        margin-left: -15px;
        top: -6px;
        left: 50%;
    }
    .home-details-register-dialog #register-dialog-loading.loading{
        background: url(http://cdn.seizethemarket.com/assets/images/loading.gif) no-repeat;
        opacity: 1 !important;
        position: relative;
    }
    #register-dialog-form th {
        width: 80px;
    }
    .home-details-register-dialog {
        position: fixed;
    }
CSS;
Yii::app()->clientScript->registerCss('registerDialogCss', $css);

$this->beginWidget(
    'zii.widgets.jui.CJuiDialog', array(
        'id'      => $this->id,
        'options' => array(
            'title'         => $this->title,
            'autoOpen'      => true,
            'closeOnEscape' => false,
            'dialogClass'   => $this->dialogClass,
            'modal'         => true,
            'draggable'     => false,
            'resizable'     => false,
            'width'         => 530,
            'beforeClose'   => 'js:function() {
                if (!' . $isYiiDebug . ' && ' . $hasGoogleTrackingCode . ' && 0) {
                    _gaq.push(["_trackEvent", "Required Register Form", "Click", "Required Register Click Close"]);
                    _gaq.push(["b._trackEvent", "Required Register Form", "Click", "Required Register Click Close"]);
                }
		}',
        ),
    )
);
$form = $this->beginWidget(
    'CActiveForm', array(
        'id'                     => 'register-dialog-form',
        'action'                 => array('/front/forms/register/formId/' . $this->formId),
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'clientOptions'          => array(
            'validateOnChange' => false,
            'validateOnSubmit' => true,
            'inputContainer'   => 'span',
            'beforeValidate'   => 'js:function() {
				$("#register-dialog-loading").addClass("loading");
				$(".loading-container").addClass("loading");

				return true;
            }',
            'afterValidate'    => 'js:function(form, data, hasErrors) {
				$("#register-dialog-loading").removeClass("loading");
				$(".loading-container").removeClass("loading");

                // If marked as spam then just refresh the current page
				if (data.isSpam) {
                    location.reload();
                    return;
				}
                if (!hasErrors) {
                    // Action was successful
					$(".loading-container").addClass("loading");

					// Google Goals tracking event: categories, actions, label
			        if (!' . $isYiiDebug . ' && ' . $hasGoogleTrackingCode . ' && 0) {
						_gaq.push(["_trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Success"]);
						_gaq.push(["b._trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Success"]);
					}
					if(data.resubmit==true) {
						window.location = "/login?email="+data.email+"&resubmit=1";
					} else {
						Message.create("success", "Congratulations! Full Access has been granted!");
						window.location = "/values";
					}

                    return false;
                } else {
					$(".loading-container").removeClass("loading");
					$("#register-dialog-submit-button").prop("disabled", false);
					Message.create("error", "Please fill out the required fields and submit.");

					// Google Goals tracking event: categories, actions, label
			        if (!' . $isYiiDebug . ' && ' . $hasGoogleTrackingCode . ' && 0) {
						_gaq.push(["_trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Error"]);
						_gaq.push(["b._trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Error"]);
					}
                }
            }',
        ),
    )
);
?>
    <div class="dialog-form-container">
<!--        <div style="text-align:center; padding:10px 0 15px 0;">-->
<!--            <h3>Search EVERY Home on the Market!</h3>-->
<!--        </div>-->
        <div class="g12" style="margin-bottom: 10px; clear: both;">
            <div class="g6 p-pt0" style="width: 220px; float: left;">
                <img src="<?php echo $this->photoUrl; ?>" width="220px">
            </div>
            <div class="g6" style="width: 270px; padding-left: 8px; float: left;">
<!--                <div style="color: black; font-size: 12px; margin-top: 8px; margin-bottom: 8px; text-align: center;">-->
<!--                    --><?php //echo Yii::app()->format->formatDollars($this->property->price);?>
<!--                    <span style="padding-left:10px; color: black; font-weight: normal;">-->
<!--                        --><?php //echo ($this->property->bedrooms)? $this->property->bedrooms.' Beds ':'';
//                        echo ($this->property->baths_total)? ' / '.$this->property->baths_total.' Baths':'';
//                        ?>
<!--                    </span>-->
<!--                </div>-->
                <div style="text-align:center;color: #00c300; font-weight: bold; font-size: 18px;margin-top: 20px;"> <?php if(!empty($this->property->photo_count)) echo ($this->property->photo_count)?$this->property->photo_count.' Photos Available!':' '; ?></div>
                <div style="text-align:center;font-weight: bold; font-size: 14px; margin-top: 4px;">View All Photos, Maps, Details</div>
                <div style="text-align:center; margin-top: 15px;"> All Properties current as of<br />Today <strong><?php echo date('F j, Y'); ?></strong>.</div>
<!--                <div style="text-align:center;padding: 8px 0;">-->
<!--                    <strong>BONUS:</strong> Special deals &amp; alerts.-->
<!--                </div>-->
            </div>
			<div class="clearfix"></div>
        </div>
        <table>
            <tr>
                <th>Name:</th>
                <td>
	    			<span>
			    		<?php echo $form->textField(
                            $model, 'data[' . FormFields::getFieldIdByName('first_name') . ']',
                            $htmlOptions = array('placeholder' => 'First Name', 'class' => 'g5',)
                        ); ?>
	    			</span>
	    			<span>
			    		<?php echo $form->textField(
                            $model, 'data[' . FormFields::getFieldIdByName('last_name') . ']',
                            $htmlOptions = array('placeholder' => 'Last Name', 'class' => 'g5',)
                        ); ?>
	    			</span>
                    <?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('first_name') . ']'); ?>
                    <?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('last_name') . ']'); ?>
                </td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>
	    			<span class="g5">
			    		<?php echo $form->textField(
                            $model, 'data[' . FormFields::getFieldIdByName('email') . ']',
                            $htmlOptions = array('placeholder' => 'Email', 'class' => 'g12',)
                        ); ?>
                        <?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('email') . ']', $htmlOptions=array('style'=>'display: inline-block;')); ?>
	    			</span>
	    			<span class="g5 p-ml5">
                        <?php $this->widget(
                            'StmMaskedTextField', array(
                                'model'       => $model,
                                'attribute'   => 'data[' . FormFields::getFieldIdByName('phone') . ']',
                                'mask'        => '(999) 999-9999',
                                'htmlOptions' => $htmlOptions = array('placeholder' => 'Phone', 'class' => 'g12'),
                            )
                        ); ?>
                        <?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('phone') . ']'); ?>

                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$this->property->listing_id));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$this->photoUrl));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('home_url') .']', $htmlOptions=array('value'=>$this->property->getUrl()));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$this->property->price));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$this->property->bedrooms));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$this->property->baths_total));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$this->property->sq_feet));?>

                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->streetAddress)))));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->city)))));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->state)))));?>
                        <?php echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$this->property->zip));?>
	    			</span>
                </td>
            </tr>
            <!--		    <tr>-->
            <!--			    <th>-->
            <?php //echo $form->labelEx($model, 'data['. $FormFields->getField('password')->id .']')?><!--:</th>-->
            <!--			    <td>-->
            <!--	    			<span>-->
            <!--			    		--><?php //echo $form->passwordField($model, 'data['. $FormFields->getField('password')->id .']', $htmlOptions=array('placeholder'=>'Password', 'class'=>'g8', ));?>
            <!--	    			</span>-->
            <!--				    --><?php //echo $form->error($model, 'data['. $FormFields->getField('password')->id .']'); ?>
            <!--			    </td>-->
            <!--		    </tr>-->
            <tr>
                <td colspan="2" class="submit-button-row">
                    <div id="register-dialog-loading"></div>
                    <input id="register-dialog-submit-button" type="submit" value="See Photos & Details Now" class="btn btn-success btn-primary btn-lg">
                </td>
            </tr>
            <!--		    <tr>-->
            <!--			    <td colspan="2" style="padding-left: 150px;">-->
            <?php //echo CHtml::CheckBox('foreclosureList',false, array ('value'=>'y')); ?><!--Send me great deals including Foreclosures</td>-->
            <!--		    </tr>-->
            <!--		    <tr>-->
            <!--			    <td colspan="2" style="padding-left: 150px;">-->
            <?php //echo CHtml::CheckBox('priorityAlerts',false, array ('value'=>'y')); ?><!--Grant me Priority Alert on New Homes.</td>-->
            <!--		    </tr>-->
            <!--		    <tr>-->
            <!--			    <td colspan="2" style="padding-left: 150px;">-->
            <?php //echo CHtml::CheckBox('buyingguide',false, array ('value'=>'y')); ?><!--Send me -->
            <?php //echo date('Y');?><!-- Guide to Buying Homes Successfully.</td>-->
            <!--		    </tr>-->
            <!--		    <tr>-->
            <!--			    <td colspan="2" style="padding-left: 150px;">-->
            <?php //echo CHtml::CheckBox('sellingguide',false, array ('value'=>'y')); ?><!--Send me -->
            <?php //echo date('Y');?><!-- Guide to Selling Your Home for Top Dollar.</td>-->
            <!--		    </tr>-->
            <!-- 	    	<tr>
	    		<th><?php // echo $form->labelEx($model, 'data['. $FormFields->getField('password')->id .']')?>:</th>
	    		<td>
	    			<span>
			    		<?php // echo $form->passwordField($model, 'data['. $FormFields->getField('password')->id .']', $htmloptions=array('placeholder'=>'Password', 'class'=>'g8',));?>
	    			</span>
	                <?php // echo $form->error($model, 'data['. $FormFields->getField('password')->id .']'); ?>
	        	</td>
	    	</tr>
 -->
            <!--		    <tr>-->
            <!--			    <td colspan="2" class="p-tc"><strong>As Seen on...<img src="/images/hgtvFox.jpg"></strong></td>-->
            <!--		    </tr>-->
            <tr>
                <td colspan="2">
                    <h3 style="text-align: center;">Be Surprised at What Your Home is Worth!</h3>
                    <a href="/values">
                        <img src="http://cdn.seizethemarket.com/assets/images/house_values_tag.jpg" alt="" width="510px;"/>
                    </a>
<!--                    <div style="width:400px; height: 110px; background: url('http://cdn.seizethemarket.com/assets/images/emails/house_values_bg.jpg') repeat scroll 0% 0% transparent; position: relative;">-->
<!--                        <a href="--><?php //echo $httpDomainPrefix; ?><!--/value" style="display: inline-block; width: 400px; height: 110px;">-->
<!--                            <span style="color: black; text-decoration: none; font-weight: bold; font-size: 20px; position: absolute; top: 8px; right: 12px; width: 300px; text-align: center;">New June 2014 House Values Report</span>-->
<!--                        </a>-->
<!--                    </div>-->
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div style="text-align:center;padding-bottom: 10px;">
<!--                        <a href="javascript:void(0);">Make an Offer? (click here)</a> <br/><br/>-->
                        I already have an account. <a href="/login">Click here to Login.</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="loading-container"><em></em></div>
<?php
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');
