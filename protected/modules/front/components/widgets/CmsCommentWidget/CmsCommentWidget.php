<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since : 1.0
 */
class CmsCommentWidget extends CWidget
{

    /**
     * The pill view for the comments widget
     * @var string
     */
    public $commentPillView = 'front_module.components.widgets.CmsCommentWidget.views.commentPill';

    /**
     * Each comment entry view
     * @var string
     */
    public $commentEntryView = 'front_module.components.widgets.CmsCommentWidget.views._commentEntry';

    /**
     * The action triggered when the comment form is submitted
     * @var array
     */
    public $commentFormAction = array('/admin/cms/addComment');

    public function run()
    {
        $id = Yii::app()->getRequest()->getQuery('id');

        if (empty($id)) {
            $pageUrl = Yii::app()->getRequest()->getQuery('pageUrl');
            if (empty($pageUrl)) {
                throw new CHttpException(500, 'Can not call ' . __CLASS__ . ' without a page url.');
            }

            $cmsContentEntry = CmsContents::model()->findByAttributes(array(
                'url' => $pageUrl,
            ));
        } else {
            $cmsContentEntry = CmsContents::model()->findByPk($id);
        }


        $cmsCommentEntry = new CmsComment;

        if (!Yii::app()->user->getIsGuest()) {
            $cmsCommentEntry->email = Yii::app()->user->contact->primaryEmail;
            $cmsCommentEntry->name = Yii::app()->user->getFullName();
        } else {
            $cmsCommentEntry->scenario = 'addFromWidget';
        }

		//$this->commentPillView

        $this->render('commentPill', array(
            'cmsContentEntry' => $cmsContentEntry,
            'cmsCommentEntry' => $cmsCommentEntry,
        ));
    }
}