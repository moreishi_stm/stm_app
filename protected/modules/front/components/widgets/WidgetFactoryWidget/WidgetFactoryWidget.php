<?php
class WidgetFactoryWidget extends CWidget{
	public function init() {
		// getPathofAlias... is_file(.".php") - put it in init, checks in the beginning
	}

	public function run() {
        if(preg_match('[blog|page]', Yii::app()->controller->action->id)) {
            $this->widget('front_module.components.widgets.HouseValuesWidget.HouseValuesWidget');
            $this->widget('front_module.components.widgets.PopularTopicsListWidget.PopularTopicsListWidget');
            $this->widget('front_module.components.widgets.VideoWidget.VideoWidget');

        } else {
            $this->widget('front_module.components.widgets.MarketStatsWidget.MarketStatsWidget');
            $this->widget('front_module.components.widgets.VideoWidget.VideoWidget');
            $this->widget('front_module.components.widgets.FeaturedAreasWidget.FeaturedAreasWidget');
            $this->widget('front_module.components.widgets.HouseValuesWidget.HouseValuesWidget');
            $this->widget('front_module.components.widgets.ForeclosureListWidget.ForeclosureListWidget');
        }
	}
}