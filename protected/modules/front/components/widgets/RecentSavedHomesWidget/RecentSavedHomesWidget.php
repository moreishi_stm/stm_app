<?php
Yii::import('front_widgets.PageViewTrackerWidget.PageViewTrackerWidget');

class RecentSavedHomesWidget extends CWidget {
	public $similarPropertyIds;
	public $similarPropertiesCount;
    public $similarProperties;

	public function init() {
		$this->registerAssets();
		if ($this->similarProperties) {
			$this->similarPropertiesCount = count($this->similarProperties);
        }
        parent::init();
	}

	protected function registerAssets() {
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_widgets.RecentSavedHomesWidget.assets.css'), false, -1, FrontModule::REFRESH_CSS_ASSETS);
		Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'recentSavedHomes.css');
	}

	public function run() {

//		$recentlyViewedCount = PageViewTrackerWidget::countHomesViewed();
//		$recentlyViewedContent = PageViewTrackerWidget::listHomesViewed();
//		$savedFavoritesCount = SavedHomes::getCount();
//		$savedFavoritesContent = SavedHomes::getListingIds();

		$this->render('recentSavedHomes', array(
						'similarPropertiesCount'   => $this->formatTabCount($this->similarPropertiesCount),
						'similarPropertiesContent' => self::printTabContainer($this->similarProperties),
						// 'recentlyViewedCount'      => $this->formatTabCount($recentlyViewedCount),
//						'recentlyViewedContent'    => self::printTabContainer($recentlyViewedContent),
//						'savedFavoritesCount'      => $this->formatTabCount($savedFavoritesCount),
//						'savedFavoritesContent'    => self::printTabContainer($savedFavoritesContent),
					 ));
	}

	public function formatTabCount($count) {
		return $count = ($count)? '('.$count.')' : '(0)' ;
	}

	public static function printTabContainer($homes) {

		$carouselContent = ($homes)? self::printHomeContentForTab($homes) : '';

		return ($carouselContent)?$tabContent =  '<div class="prev"></div><div class="next"></div>
						<div class="mini-carousel">'.$carouselContent.'</div>
						':'';
	}

    public static function printHomeContentForTab($mlsProperties) {
        $html = '';
        if (!is_array($mlsProperties)) {
            return $html;
        }

        /** @var MlsProperties $mlsProperty */
        foreach ($mlsProperties as $mlsProperty) {
            $miniSingleImage = CHtml::image($mlsProperty['photoUrl'], '', $htmlOptions=array());
            $miniSingleImage .= CHtml::tag('span', $htmlOptions=array(
                'class' => 'photo-count',
            ), $mlsProperty['photo_count'].' Photos');
            $miniSingleImageLink = CHtml::link($miniSingleImage, $mlsProperty['propertyUrl'], array('class'=>'carousel'));
            $singleImageContent = $miniSingleImageLink.'<label><span class="price">'.Yii::app()->format->formatDollars($mlsProperty['price']).'</span></label>';

            $html .= CHtml::tag('div', array('class'=>'image'), $singleImageContent);
        }

        return $html;
    }

	//<div class="image"><a class="carousel" href="/home/fl/st-johns/4664-pecos-ct/653422"><img src="http://mls.cleeteam.com/fl_nefar/653422-1.jpg" alt="" /><span class="photo-count">33 Photos</span></a><label><span class="price">$489,000</span></label></div>
	public static function processLogin() {
		$widget = new RecentSavedHomesWidget;
		$widget->cookieToDb();
		$widget->deleteCookie();
	}

	/**
	 * cookieToDb Handles the cookies when a user logins in. Pushes cookie data to the db and empties the cookie. Called from LoginAction
	 * @return none
	 */
	private function cookieToDb() {
		if (Yii::app()->user->isGuest)
			return;

		// Handles the process of uploading Saved Homes from cookie to db for logged in user
		$cookieJSON    = Yii::app()->getRequest()->cookies[SavedHomes::COOKIE_KEY]->value;
		if ($cookieJSON) {
			$savedHomesData = CJSON::decode($cookieJSON);
            if(count($savedHomesData)>0) {
                foreach($savedHomesData as $listingId => $data) {
                    $model = new SavedHomes;
                    $model->contact_id = Yii::app()->user->id;
                    $model->listing_id = $listingId;
                    $model->ip = $data['id'];
                    $model->added = $data['added'];
                    $model->added_by = Yii::app()->user->id;
                    $model->save();
                }
            }
		}
	}

	/**
	 * deleteCookie deletes the SavedHomes tracking cookie, used when someone logs in and cookie is no longer needed
	 * @return none
	 */
	protected function deleteCookie() {

		unset(Yii::app()->request->cookies[SavedHomes::COOKIE_KEY]);
	}
}