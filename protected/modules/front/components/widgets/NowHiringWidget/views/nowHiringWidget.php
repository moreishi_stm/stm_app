<style>
    #now-hiring-widget span.hiring {
        display: block;
        background: url('http://www.<?php echo Yii::app()->user->getDomain()->name.$imageAssetsUrl; ?>/hiring.png') no-repeat;
        height: 94px;
        width: 120px;
        float: left;
        position: relative;
        top: -22px;
        left: -8px;
    }
    #now-hiring-widget a {
        font-size: 18px;
        font-family: Arial, Helvetica, sans-serif !important;
        font-weight: bold;
        color: #0063B0;
    }
    #now-hiring-widget a:hover {
        color: #D20000;
        text-decoration: underline;
    }
</style>

<div id="now-hiring-widget" class="now-hiring-widget-container">
    <a href="/hiring" class="hiring">
        <span class="hiring"></span>
        Now Hiring<br />
        Apply Here!
    </a>
</div>

