<style type="text/css">
    #market-stats-widget{
        font-family: Arial;
        font-size: 13px;
        border: 2px solid;
        border-top-color: #CCC;
        border-right-color: #BBB;
        border-left-color: #CCC;
        border-bottom-color: #AAA;
        width: 100%;
        border-collapse: separate;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }
    div.market-stats-widget-body div.data label {
        margin-right: 4px;
    }
    #market-stats-widget .market-stats-widget-title{
        color: #370;
        padding: 10px 0;
        font-family: "Times New Roman", Times, serif;
        font-size: 21px;
        font-weight: normal;
        background-color: #D0E4C2;
        vertical-align: middle;
        text-align: center;
    }
    #market-stats-widget .date{
        font-size: 11px;
        padding: 0 0 0 10px;
        color: #888;
    }
    #market-stats-widget .market-stats-widget-body{
        text-align: left;
        padding: 5px;
        background-color: #EAF9E6;
    }
    #market-stats-widget h4{
        font-family: "Times New Roman", Times, serif;
        font-size: 18px;
        color: #390;
        font-weight: normal;
        line-height: 1;
        padding-bottom: 5px;
    }
    #market-stats-widget h4 .homesCount{
        color: black;
        font-weight: bold;
        font-family: Arial;
    }
    #market-stats-widget section{
        display: block;
        padding: 10px;
        line-height: 15px;
    }
    #market-stats-widget section .data{
        color: black;
        padding-left: 25px;
    }
    #market-stats-widget section label{
        padding-right: 5px;
        font-weight: bold;
    }
    #market-stats-widget .toggle{
        font-size: 12px;
        position: relative;
        width: 240px;
        top: 5px;
        left: -5px;
        display: block;
        text-align: right;
        text-decoration: none;
        font-style: italic;
        color: #555;
    }
    #market-stats-widget .toggle:hover{
        color: #333;
        /*font-weight: bold;*/
        text-decoration: underline;
        background-color: #D0E4C2;
    }
    #market-stats-widget .toggle span{
        padding-right: 15px;
    }
</style>


<div id="market-stats-widget" class="market-stats-widget-container">
	<div class="market-stats-widget-title">Real Estate Trends<br /> <?php echo $model->name;?></div>
	<div class="market-stats-widget-body">
		<h4 class="p-pl10 p-pt10"><span class="homesCount"><?php echo Yii::app()->format->formatNumber($model->count_homes);?></span> Homes for Sale</h4>
		<section id="market-stats-all-prices" style="display:block;">
			<div class="data">
				<label><?php echo Yii::app()->format->formatDollars($model->min_price);?></label>Lowest Price<br />
				<label><?php echo Yii::app()->format->formatDollars($model->avg_price);?></label>Average Price<br />
				<label><?php echo Yii::app()->format->formatDollars($model->max_price);?></label>Highest Price<br />
			</div>
		</section>
		<section>
			<h4>Typical Property:</h4>
			<div class="data">
				<label><?php echo $model->avg_bedrooms?></label>Bedrooms<br />
				<label><?php echo $model->avg_baths?></label>Bath<br />
				<label><?php echo Yii::app()->format->formatNumber($model->avg_sf);?></label>Sq. Feet<br />
				<label><?php echo Yii::app()->format->formatDollars($model->avg_price);?></label>Average List Price<br />
				<label><?php echo Yii::app()->format->formatDollars($model->avg_price_sf);?></label>Average $/SF<br />
			</div>
		</section>
		<section  id="market-stats-all-bedrooms" style="display:block;">
			<h4>Average Price per Bedroom:</h4>
			<div class="data">
			<?
				for($i=1; $i<6; $i++) {
					if (isset($model->{avg_br.$i}) AND $model->{avg_br.$i} > 0)
						echo $i.' Bedrooms: <label>'.Yii::app()->format->formatDollars($model->{avg_br.$i}).'</label><br />';
				}
			?>
		</div>
		</section>
		<div class="date" style="display:none;">Last updated: Today <?php echo date('n/j/Y')?></div>
		<a href="javascript:void(0);" class="toggle see-more"><span>[ Minimize ]</span></a>
	</div>
</div>