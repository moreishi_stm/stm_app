<?php
	class MarketStatsWidget extends CWidget {

		public $baseModel;

		public function init() {
//			$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_module.components.widgets.MarketStatsWidget.assets.css'), true, -1, FrontModule::REFRESH_CSS_ASSETS);
//			Yii::app()->clientScript->registerCssFile($cssAssetsUrl . DS . 'market_stats.css');

			$this->baseModel = new FeaturedAreas;
			$this->marketStatsScripts();
		}

		public function run() {
			$model = $this->baseModel->findByUrl(Yii::app()->request->getQuery('area'));
			$this->render('marketStatsWidget', array(
					'model' => $model,
				)
			);
		}

		/**
		 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
		 *
		 * @return null
		 */
		public function marketStatsScripts() {
			$isYiiDebug = (YII_DEBUG) ? 1 : 0;
			$js = <<<JS

			$(function() {
				$("#market-stats-widget .see-more").toggle(
				    function(){
						$("#market-stats-all-prices, #market-stats-all-bedrooms").show("normal");
						$("#market-stats-widget .see-more span").html("[ Minimize ]")
				},
					function(){
						$("#market-stats-all-prices, #market-stats-all-bedrooms").hide("normal");
						$("#market-stats-widget .see-more span").html("[ See More... ]")
				});

				if (!$isYiiDebug) {
					// Google Goals tracking event: categories, actions, label
				// 	_gaq.push(["_trackEvent", "Required Register Form", "View", "Required Register Form View"]);
				// 	_gaq.push(["b._trackEvent", "Required Register Form", "View", "Required Register Form View"]);
				}
			});
JS;

			Yii::app()->clientScript->registerScript('homeDetailsDialog-' . $this->id, $js);
		}
	}
