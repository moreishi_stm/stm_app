<?php
class FeaturedAreasWidget extends CWidget{
	public $cssAssetsUrl;

	public function init() {
		$this->cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_module.components.widgets.FeaturedAreasWidget.assets.css'), false, -1, FrontModule::REFRESH_IMAGE_ASSETS);
		Yii::app()->clientScript->registerCssFile($this->cssAssetsUrl.DS.'featured_areas.css');
	}

	public function run() {
		$this->render('featuredAreas', array('imageAssetsUrl'=>$this->cssAssetsUrl));
	}
}