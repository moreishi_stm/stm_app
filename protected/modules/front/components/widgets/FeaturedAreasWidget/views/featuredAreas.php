<div id="featured-area-widget">
	<div class="category-container">
		<div class="title">Top Communities</div>
		<div class="item"><a href="/area/aberdeen">Aberdeen</a></div>
		<div class="item"><a href="/area/cimaroone">Cimarrone</a></div>
		<div class="item"><a href="/area/durbincrossing">Durbin Crossing</a></div>
		<div class="item"><a href="/area/cimarrone">Cimarrone</a></div>
		<div class="item"><a href="/area/deerwood">Deerwood</a></div>
		<div class="item"><a href="/area/deercreek">Deercreek</a></div>
		<div class="item"><a href="/area/eagleharbor">Eagle Harbor</a></div>
		<div class="item"><a href="/area/eaglelanding">Eagle Landing</a></div>
		<div class="item"><a href="/area/glenkernan">Glen Kernan</a></div>
		<div class="item"><a href="/area/flemingisland">Fleming Island</a></div>
		<div class="item"><a href="/area/julingtoncreekplantation">Julington Creek Plantation</a></div>
		<div class="item"><a href="/area/marshlanding">Marsh Landing</a></div>
		<div class="item"><a href="/area/oakleafplantation">Oakleaf Plantation</a></div>
		<div class="item"><a href="/area/queensharbour">Queens Harbour</a></div>
		<div class="item"><a href="/area/sawgrass">Sawgrass</a></div>
		<div class="item"><a href="/area/southhampton">South Hampton</a></div>
		<div class="item"><a href="/area/worldgolfvillage">World Golf Village</a></div>
		<div class="item"><a href="/area/stjohnsgolfcc">St Johns Golf & CC</a></div>
	</div>
	<div class="category-container">
		<div class="title">Top Areas</div>
		<div class="item"><a href="/area/pontevedra">Ponte Vedra Beach</a></div>
		<div class="item"><a href="/area/stjohns">St Johns</a></div>
		<div class="item"><a href="/area/staugustine">St Augustine</a></div>
		<div class="item"><a href="/area/southside">Southside</a></div>
		<div class="item"><a href="/area/intracoastal">Intracoastal</a></div>
		<div class="item"><a href="/area/jacksonvillebeach">Jacksonville Beach</a></div>
		<div class="item"><a href="/area/atlanticbeach">Atlantic Beach</a></div>
		<div class="item"><a href="/area/flemingisland">Fleming Island</a></div>
	</div>
</div>