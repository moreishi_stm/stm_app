<?php
Yii::import('front_widgets.PageViewTrackerWidget.PageViewTrackerWidget');

class RecentSavedHomesWidget extends CWidget {
	public $similarPropertyIds;
	public $similarPropertiesCount;

	public function init() {
		$this->registerAssets();
		parent::init();
		if ($this->similarPropertyIds)
			$this->similarPropertiesCount = count($this->similarPropertyIds);
	}

	protected function registerAssets() {
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_widgets.RecentSavedHomesWidget.assets.css'), false, -1, FrontModule::REFRESH_CSS_ASSETS);
		Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'recentSavedHomes.css');
	}

	public function run() {

//		$recentlyViewedCount = PageViewTrackerWidget::countHomesViewed();
//		$recentlyViewedContent = PageViewTrackerWidget::listHomesViewed();
//		$savedFavoritesCount = SavedHomes::getCount();
//		$savedFavoritesContent = SavedHomes::getListingIds();

		$this->render('recentSavedHomes', array(
						'similarPropertiesCount'   => $this->formatTabCount($this->similarPropertiesCount),
						'similarPropertiesContent' => self::printTabContainer($this->similarPropertyIds),
						// 'recentlyViewedCount'      => $this->formatTabCount($recentlyViewedCount),
//						'recentlyViewedContent'    => self::printTabContainer($recentlyViewedContent),
//						'savedFavoritesCount'      => $this->formatTabCount($savedFavoritesCount),
//						'savedFavoritesContent'    => self::printTabContainer($savedFavoritesContent),
					 ));
	}

	public function formatTabCount($count) {
		return $count = ($count)? '('.$count.')' : '(0)' ;
	}

	public static function printTabContainer($homes) {

		$carouselContent = ($homes)? self::printHomeContentForTab($homes) : '';

		return ($carouselContent)?$tabContent =  '<div class="prev"></div><div class="next"></div>
						<div class="mini-carousel">'.$carouselContent.'</div>
						':'';
	}

	/**
	 * printHomeContentForTab Takes input of
	 * @param  mixed $homes array of listingId or MlsProperties objects??
	 * @return string html content for Carousel in CJui Tab panel
	 */
	public static function printHomeContentForTab($homes) {
		if (!$homes)
			return;

		$tableName = Yii::app()->user->board->prefixedName;
		$model = MlsProperties::model($tableName);
		$params = array();
		foreach ($homes as $key => $value) {
			$condition .= ($condition)? ' OR '.'listing_id=:lid'.$key : 'listing_id=:lid'.$key;
			$params = array_merge($params, array(':lid'.$key=>$value));
		}
		$results = $model->findAll(array('condition' => $condition,
		                           		 'params'    => $params,
		                           ));

		if ($results) {
			$miniCarouselImages = '';
			foreach($results as $PropertyData) {
				$listingId = $PropertyData['listing_id'];
				$miniSingleImage = CHtml::image($PropertyData->getPhoto(1)->getUrl(), '', $htmlOptions=array());
				if($PropertyData['photo_count'])
					$miniSingleImage .= '<span class="photo-count">'.$PropertyData['photo_count'].' Photos</span>';
				$url = '/home/'.strtolower(AddressStates::getShortNameById(Yii::app()->user->board->state_id)).'/'.$PropertyData->addressLink.'/'.$PropertyData['listing_id'];
				$miniSingleImage = CHtml::link($miniSingleImage, $url, array('class'=>'carousel'));
				$singleImageContent = $miniSingleImage.'<label><span class="price">'.Yii::app()->format->formatDollars($PropertyData['price']).'</span></label>';
				$miniCarouselImageCollection .= CHtml::tag('div', array('class'=>'image'), $singleImageContent);
			}
		}
		return $miniCarouselImageCollection;
	}

	//<div class="image"><a class="carousel" href="/home/fl/st-johns/4664-pecos-ct/653422"><img src="http://mls.cleeteam.com/fl_nefar/653422-1.jpg" alt="" /><span class="photo-count">33 Photos</span></a><label><span class="price">$489,000</span></label></div>
	public static function processLogin() {
		$widget = new RecentSavedHomesWidget;
		$widget->cookieToDb();
		$widget->deleteCookie();
	}

	/**
	 * cookieToDb Handles the cookies when a user logins in. Pushes cookie data to the db and empties the cookie. Called from LoginAction
	 * @return none
	 */
	private function cookieToDb() {
		if (Yii::app()->user->isGuest)
			return;

		// Handles the process of uploading Saved Homes from cookie to db for logged in user
		$cookieJSON    = Yii::app()->request->cookies[SavedHomes::COOKIE_KEY]->value;
		if ($cookieJSON) {
			$savedHomesData = CJSON::decode($cookieJSON);

			foreach($savedHomesData as $listingId => $data) {
				$model = new SavedHomes;
				$model->contact_id = Yii::app()->user->id;
				$model->listing_id = $listingId;
				$model->ip = $data['id'];
				$model->added = $data['added'];
				$model->added_by = Yii::app()->user->id;
				$model->save();
			}
		}
	}

	/**
	 * deleteCookie deletes the SavedHomes tracking cookie, used when someone logs in and cookie is no longer needed
	 * @return none
	 */
	protected function deleteCookie() {

		unset(Yii::app()->request->cookies[SavedHomes::COOKIE_KEY]);
	}
}