<?
$tabsArray = array();

if ($similarPropertiesCount) {
    $similaryPropertiesTab = array('Similar Properties '.$similarPropertiesCount=>array(
                                        'content' =>$similarPropertiesContent,
                                        'id' =>'similar-properties-tab',
                                    ),
                                );
    $tabsArray = array_merge($similaryPropertiesTab, $tabsArray);
}

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>$tabsArray,
    'id'=>'recent-saved-homes-widget',
));

$this->widget('admin_module.extensions.carouFredSel.ECarouFredSel', array('id'     => 'similar-properties-tab',
                                                                          'target' => '#similar-properties-tab .mini-carousel',
                                                                          'config' => array('items' => 4,
                                                                                            'auto'  => false,
                                                                                            'prev'  => array('button' => new CJavaScriptExpression('function() {
															                                            return $(this).parent().siblings("#similar-properties-tab .prev");
															                                    }'),
                                                                                            ),
                                                                                            'next'  => array('button' => new CJavaScriptExpression('function() {
															                                            return $(this).parent().siblings("#similar-properties-tab .next");
															                                    }'),
                                                                                            ),
                                                                          ),
                                                                    )
);

//	$this->widget('admin_module.extensions.carouFredSel.ECarouFredSel', array('id'     => 'saved-favorites-tab',
//	                                                                          'target' => '#saved-favorites-tab .mini-carousel',
//	                                                                          'config' => array('items' => 4,
//	                                                                                            'auto'  => false,
//	                                                                                            'prev'  => array('button' => new CJavaScriptExpression('function() {
//															                                            return $(this).parent().siblings(".prev");
//															                                    }'),
//	                                                                                            ),
//	                                                                                            'next'  => array('button' => new CJavaScriptExpression('function() {
//															                                            return $(this).parent().siblings(".next");
//															                                    }'),
//	                                                                                            ),
//	                                                                          ),
//	                                                                    )
//	);

//	$this->widget('admin_module.extensions.carouFredSel.ECarouFredSel', array('id'     => 'recently-viewed-tab',
//	                                                                          'target' => '#recently-viewed-tab .mini-carousel',
//	                                                                          'config' => array('items' => 4,
//	                                                                                            'auto'  => false,
//	                                                                                            'prev'  => array('button' => new CJavaScriptExpression('function() {
//															                                                return $(this).parent().siblings("#recently-viewed-tab .prev");
//															                                            }'),
//	                                                                                            ),
//	                                                                                            'next'  => array('button' => new CJavaScriptExpression('function() {
//															                                                return $(this).parent().siblings("#recently-viewed-tab .next");
//															                                            }'),
//	                                                                                            ),
//	                                                                                            'onCreate'  => new CJavaScriptExpression('function() {
//															                                                alert("here");
//															                                            }'),
//	                                                                          ),
//	                                                                    )
//	);
?>