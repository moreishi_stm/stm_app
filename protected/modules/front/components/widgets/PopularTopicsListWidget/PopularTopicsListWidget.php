<?php
class PopularTopicsListWidget extends CWidget{

	public function run() {
        $cmsTags = CmsTags::model()->orderNameAsc()->isPopularLink()->findAll();
		$this->render('popularTopicsListWidget', array('cmsTags'=>$cmsTags));
	}
}