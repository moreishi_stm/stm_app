<?php
class FrontController extends CController {

	//this is the default color theme for pages, can be specified in controller
	public $pageColor = 'red';

	// Used to store the breadcrumbs
	public $breadcrumbs = array();

	public $domainId = NULL;

	public $useDBMenu = false;

	public $menus = array();

	public $menu_positions = array();

    public $fullWidth = false;

    protected function beforeRender($view) {
        $layoutName = (empty($this->layout))? $this->module->layout : $this->layout;
        if(!empty(Yii::app()->params['layoutDirectory'])) {
            $this->layout = Yii::app()->params['layoutDirectory'].'/'.$layoutName;
        }

		$this->domainId = Yii::app()->domain->id;

		$Criteria = new CDbCriteria();
		$Criteria->condition = "`cms_theme_id` = :cmsThemeId";
		$Criteria->order = "`id` ASC"; // @TODO: why do we need asc?? (comment by: Bkozak)
		$Criteria->params = array(
			":cmsThemeId"	=> Yii::app()->domain->cms_theme_id,
		);

		$this->menu_positions = CHtml::listData(CmsMenuPositions::model()->findAll($Criteria), 'id', 'name');
		$this->menu_positions = array_merge($this->menu_positions, CHtml::listData(CmsMenuPositions::model()->findAll($Criteria), 'name', 'id'));

		$Criteria = new CDbCriteria();
		$Criteria->condition = "domain_id = :domainId";
		$Criteria->params = array(
			":domainId" => $this->domainId
		);

		$cmsDomainMenus = new CmsDomainMenus();
		$domainMenus = $cmsDomainMenus->findAll($Criteria);

		if(!empty($domainMenus)) {

			$domainMenusByPosition = array();

			foreach($domainMenus as $domainMenu) {
				$domainMenusByPosition[$domainMenu->menu_position_id] = $domainMenu->attributes;
				$links = CmsMenus::model()->findByPk($domainMenu->menu_id)->getRelated('cmsMenuLinks',true);

				if(empty($links)) {
					$domainMenusByPosition[$domainMenu->menu_position_id][0] = array();
					continue;
				}

				foreach($links as $link) {
					if(empty($link->parent_id)) {
						$domainMenusByPosition[$domainMenu->menu_position_id][0][] = $link->attributes;
					} else {
						$domainMenusByPosition[$domainMenu->menu_position_id][$link->parent_id][] = $link->attributes;
					}
				}
				$this->useDBMenu[$domainMenu->menu_position_id] = true;
				$this->useDBMenu[$this->menu_positions[$domainMenu->menu_position_id]] = true;
			}

			$this->menus = $domainMenusByPosition;
		}

        return parent::beforeRender($view);
    }
}
