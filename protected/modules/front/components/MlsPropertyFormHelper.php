<?php

/**
 * Class MlsPropertyFormHelper
 */
class MlsPropertyFormHelper {

    /** @var MlsPropertyFormHelper $instance */
    private static $instance;

    /** @var MlsBoards $_activeMlsBoard */
    private $_activeMlsBoard;

    private function __construct() {

        /**
         * Set the board so we know which mls properties table to go to
         */
        $this->_activeMlsBoard = Yii::app()->getUser()->getBoard();
    }

    /**
     * @return MlsPropertyFormHelper
     */
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new MlsPropertyFormHelper();
        }

        return self::$instance;
    }

    /**
     * @return array
     */
    public function getNeighborhoodOptions($searchedNeighborhood=null) {

        $legalPreFilterCriteria = new CDbCriteria;
        $legalPreFilterCriteria->compare('legal_subdivision', $searchedNeighborhood, true);

        $commonPreFilterCriteria = new CDbCriteria;
        $commonPreFilterCriteria->compare('common_subdivision', $searchedNeighborhood, true);

        $legalSubdivisionOptions = $this->getDistinctOptions('legal_subdivision', $legalPreFilterCriteria);
        $commonSubdivisionOptions = $this->getDistinctOptions('common_subdivision', $commonPreFilterCriteria);

        $neighborhoodOptions = array_merge($legalSubdivisionOptions, $commonSubdivisionOptions);

        return $neighborhoodOptions;
    }

    /**
     * @return array
     */
    public function getZipOptions() {

        $zipOptions = $this->getDistinctOptions('zip');

        /**
         * Ensure the zip codes are formatted
         */
        if (!empty($zipOptions)) {
            foreach ($zipOptions as $zipKey => $zip) {
                unset($zipOptions[$zipKey]);
                $zip = substr($zip, 0, 5);
                $zipOptions[$zip] = $zip;
            }
        }

        return $zipOptions;
    }

    /**
     * @return mixed
     */
    public function getSchoolOptions() {

        $elementarySchoolOptions = $this->getDistinctOptions('elementary_school');
        $middleSchoolOptions = $this->getDistinctOptions('middle_school');
        $highSchoolOptions = $this->getDistinctOptions('high_school');

        if ($elementarySchoolOptions)
            $schoolOptions['Elementary Schools'] = $elementarySchoolOptions;

        if ($middleSchoolOptions)
            $schoolOptions['Middle Schools'] = $middleSchoolOptions;

        if ($highSchoolOptions)
            $schoolOptions['High Schools'] = $highSchoolOptions;

        return $schoolOptions;
    }

    /**
     * @return array
     */
    public function getCountyOptions() {

        return $this->getDistinctOptions('county');
    }

    /**
     * @return array
     */
    public function getCityOptions() {

        return $this->getDistinctOptions('city');
    }

    /**
     * @param $column
     * @param CDbCriteria $preFilterCriteria allows for the pre-filtering of the list
     *
     * @return array
     */
    private function getDistinctOptions($column, CDbCriteria $preFilterCriteria=null) {

        $distinctOptions = array();

        $mlsProperty = MlsProperties::model($this->_activeMlsBoard->getPrefixedName());
        $mlsSecondaryProperty = MlsPropertiesSecondary::model($this->_activeMlsBoard->getPrefixedName());

        /**
         * Determine which mls property model instance to search on
         */
        $mlsModel = ($mlsProperty->hasAttribute($column)) ? $mlsProperty : $mlsSecondaryProperty;

        $findCriteria = new CDbCriteria;
        $findCriteria->select = "DISTINCT {$column}";
        if ($preFilterCriteria instanceof CDbCriteria) {
            $findCriteria->mergeWith($preFilterCriteria);
        }

        $distinctRecords = $mlsModel->findAll($findCriteria);
        if ($distinctRecords) {
            foreach ($distinctRecords as $distinctRecord) {
                $properCaseValue = Yii::app()->format->properCase($distinctRecord->$column);
                $distinctOptions[strtolower($distinctRecord->$column)] = $properCaseValue;
            }
        }

        return $distinctOptions;
    }


}