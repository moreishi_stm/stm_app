<?php
Yii::import('zii.widgets.CListView');

class StmListView extends CListView
{
    public $enableSorting = false;
    public $extraParams;
    public $itemsCssClass = 'datatables';
    public $beforeAjaxUpdate =  'js:function(id) {$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");}';
    public $afterAjaxUpdate =  'js:function(id) {$("div.loading-container.loading").remove();}';

	// protected function createDataColumn($text)
	// {
	//     if (!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$text,$matches))
	//         throw new CException(Yii::t('zii','The column must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
	//     $column=new StmDataColumn($this);
	//     $column->name=$matches[1];
	//     if (isset($matches[3]) && $matches[3]!=='')
	//         $column->type=$matches[3];
	//     if (isset($matches[5]))
	//         $column->header=$matches[5];
	//     return $column;
	// }
}