<?php
class IntegrationsController extends AdminController
{
    public $pageColor = 'purple';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return CMap::mergeArray(parent::filters(), array());
    }

    public function actionIndex()
    {
        $this->title = 'Integrations';
        $uuid = Yii::app()->stm_hq->createCommand("select id from client_accounts where client_id=".Yii::app()->user->clientId." AND account_id=".Yii::app()->user->accountId)->queryScalar();
        $this->render('list', array('uuid'=>$uuid));
    }

    public function actionGoogleCalendarList()
    {
        Yii::import('admin_module.components.StmOAuth2.Google', true);

        $this->title = 'Google Calendar';
        $stmGoogle = new \StmOAuth2\Google();

//        $calendarService = new Google_Service_Calendar($client);
//        $calendarList = $calendarService->calendarList->listCalendarList();
//        $myCalendar = $calendarService->calendarList->get('lee@christineleeteam.com');
//        $e = $calendarService->events->listEvents('lee@christineleeteam.com', array('timeMin'=>'2014-11-01T00:00:00-05:00', 'timeMax'=>'2014-11-30T00:00:00-05:00'));

        $this->render('temp', array('data'=> $stmGoogle->listCalendars()));
    }


    public function actionGoogleCalendar($id)
    {
        Yii::import('admin_module.components.StmOAuth2.Google', true);

        $this->title = 'Google Calendar';
        $stmGoogle = new \StmOAuth2\Google();
//        $this->render('temp', array('data'=> $stmGoogle->viewCalendarEvents($id)));
    }

    public function actionGoogleCalendarAddEvent($id)
    {
        Yii::import('admin_module.components.StmOAuth2.Google', true);

        $this->title = 'Google Calendar';
        $stmGoogle = new \StmOAuth2\Google();

        $summary = 'Test: '.date('m/d/Y H:i:s');
        $location = '2974 Hartley Rd, Jacksonville, FL 32257';
        $startDatetime = date('m/d/Y 13:00');
        $endDatetime = date('m/d/Y 14:00');
        $description = 'This is a test event. =)';
        echo $stmGoogle->addCalendarEvent($id, $startDatetime, $endDatetime, $summary, $location, $description);
    }

    /**
     * Edit Google Calendar Event
     *
     * @param string $id Google Calendar ID
     * @param string $eventId Google Calendar Event ID
     */
    public function actionGoogleCalendarEditEvent($calendarId, $eventId)
    {
        // Example Url: http://www.christineleeteam.local/admin/integrations/googleCalendarEditEvent/calendarId/lee@christineleeteam.com/eventId/91laij3ta4b6nq1nd7oto3lht4
        Yii::import('admin_module.components.StmOAuth2.Google', true);

        $this->title = 'Google Calendar';
        $stmGoogle = new \StmOAuth2\Google();

        // edit existing event
        $startDatetime = date('m/d/Y 16:00');
        $endDatetime = date('m/d/Y 17:00');
        $summary = 'Test Update: '.date('m/d/Y H:i:s');
        $location = '123 Main St, Jacksonville, FL 32202';
        $description = 'Updated Test event! Wohooo!';

        echo $stmGoogle->editCalendarEvent($calendarId, $eventId, $startDatetime, $endDatetime, $summary, $location, $description);
    }
}