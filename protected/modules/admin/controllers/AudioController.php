<?php
	class AudioController extends AdminController
    {
		public $pageColor = 'red';

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
            $this->displayName = 'Audio Files';
		}

        public function actionIndex()
        {
            $this->title = 'Audio Files';

            $model = new AudioFiles('search');
            $model->unsetAttributes();  // clear any default values
//            $model->status_ma = 1;
//
            if (isset($_GET['AudioFiles'])) {
                $model->attributes=$_GET['AudioFiles'];
            }

//            $model->operationManualTagLu = new OperationManualTagLu('search');
//            if (isset($_GET['OperationManualTagLu'])) {
//                $model->operationManualTagLu->attributes = $_GET['OperationManualTagLu'];
//            }

            $this->render('list',array(
                    'model'=>$model
                ));
        }

        public function actionAdd()
        {
            $this->title = 'Add Audio File';

            $model = new AudioFiles();

            // used for gridview search box inputs
            if (isset($_POST['AudioFiles'])) {
                $model->attributes=$_POST['AudioFiles'];

                if ($model->save()) {
                    if(!empty($model->tagCollection)) {
                        foreach ($model->tagCollection as $attributeId) {
                            $tagLu = new AudioOperationManualTagLu;
                            $tagLu->audio_id = $model->id;
                            $tagLu->operation_manual_tag_id = $attributeId;
                            $tagLu->save();
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Successfully Added Audio File.');
                    $this->redirect(array(
                            'index',
                        )
                    );
                }
            }

            $this->render('form',array(
                    'model'=>$model
                ));
        }

        public function actionEdit($id)
        {
            $this->title = 'Add Audio File';

            $model = AudioFiles::model()->findByPk($id);
            $model->tagCollection = Yii::app()->db->createCommand("SELECT operation_manual_tag_id FROM audio_operation_manual_tag_lu WHERE audio_file_id=".$model->id)->queryColumn();

            // used for gridview search box inputs
            if (isset($_POST['AudioFiles'])) {
                $model->attributes=$_POST['AudioFiles'];
                if(!isset($_POST['AudioFiles']['tagCollection'])) {
                    $model->tagCollection = null;
                }

                if ($model->save()) {
                    $opsManualTagIds = Yii::app()->db->createCommand("SELECT operation_manual_tag_id FROM audio_operation_manual_tag_lu WHERE audio_file_id=".$model->id)->queryColumn();
                    if($model->tagCollection) {

                        foreach ($model->tagCollection as $i => $tagId) {

                            if(in_array($tagId, $opsManualTagIds)) {
                                // if already exists, do nothing, unset from collection
                                unset($opsManualTagIds[$i]);
                            }
                            else {
                                // create new if not exist
                                $tagLu = new AudioOperationManualTagLu;
                                $tagLu->audio_file_id = $model->id;
                                $tagLu->operation_manual_tag_id = $tagId;
                                $tagLu->save();
                                unset($opsManualTagIds[$i]);
                            }
                        }
                    }

                    // delete all the others
                    foreach ($opsManualTagIds as $i => $tagId) {
                        AudioOperationManualTagLu::model()->deleteAllByAttributes(array('audio_file_id'=>$model->id, 'operation_manual_tag_id'=>$tagId));
                    }

                    Yii::app()->user->setFlash('success', 'Successfully Updated Audio File.');
                    $this->redirect(array(
                            'index',
                        )
                    );
                }
            }

            $this->render('form',array(
                    'model'=>$model
                ));
        }

        public function actionDelete()
        {

        }

        public function actionView($id)
        {
            $this->title = 'Listen to Audio File';

            $model = AudioFiles::model()->findByPk($id);

            // Determine the full file path
            $cloudFrontSubdomain = (YII_DEBUG) ? 'd7p4c6zeo248w' : 'd3r4abrvltzvc';
            $url = strtr('https://'.$cloudFrontSubdomain.'.cloudfront.net/:clientId/:componentName/:id', array(
                    ':clientId'         =>  Yii::app()->user->clientId,
                    ':componentName'    =>  'audio',
                    ':id'               =>  $model->id
                ));

            // New CloudFront client
            $cfc = Aws\CloudFront\CloudFrontClient::factory(array(
                    'key_pair_id'   =>  'APKAJTZQI3R5N5JR5LVA',
                    'private_key'   =>  Yii::getPathOfAlias('admin_module') . '/certs/pk-APKAJTZQI3R5N5JR5LVA.pem'
                ));

            // Generate custom policy for CloudFront URL signing
            $policy = array(
                "Statement"	=>	array(
                    array(
                        'Resource'	=>	$url,
                        'Condition'	=>	array(
                            'DateLessThan'	=>	array(
                                'AWS:EpochTime'	=>	time() + (60 * 5)
                            )
                        )
                    )
                )
            );

            // If we are not in development, add an IP restriction.
            if(!YII_DEBUG) {
                $ip = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                $policy['Statement'][0]['Condition']['IpAddress'] = array(
                    'AWS:SourceIp'	=>	$ip . '/32'
                );
            }

            // Generate signed URL for access
            $audioUrl = $cfc->getSignedUrl(array(
                    'url'			=>	$url,
                    'policy'		=>	Zend_Json_Encoder::encode($policy),
                    'key_pair_id'	=>	'APKAJTZQI3R5N5JR5LVA',
                    'private_key'	=>	Yii::getPathOfAlias('admin_module') . '/certs/pk-APKAJTZQI3R5N5JR5LVA.pem'
                ));

            $this->render('view',array(
                    'model'=>$model,
                    'audioUrl' => $audioUrl,
                ));
        }
	}
