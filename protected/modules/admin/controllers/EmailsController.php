<?php
class EmailsController extends AdminController
{
    protected $_bounceDetectedEmails = array();
	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array());
	}

	/**
	 * Initializes the controller
	 * @return void
	 */
	public function init()
    {

        if(strpos($_SERVER['SERVER_NAME'], 'christineleeteam') === false && strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') === false && strpos($_SERVER['SERVER_NAME'], 'refer') === false) {
            Yii::app()->end();
        }
		$this->displayName = 'Emails';
	}

    public function actionBlast()
    {
        $this->title = 'Email Blast';
        $emailBlast = new EmailBlasts();
        $contacts = new Contacts();
        $contacts->unsetAttributes(); // clear any default values

        if (isset($_GET['Contacts'])) {
            $contacts->attributes = $_GET['Contacts'];
            $contacts->dateSearchFilter = 'added_desc';
        }

        $contacts->emails = new Emails();
        $contacts->emails->email = '.';

        $contacts->contactTypeLu = new ContactTypeLu('search');
        if (isset($_GET['ContactTypeLu'])) {
            $contacts->contactTypeLu->attributes = $_GET['ContactTypeLu'];
        }

        $contacts->source = new Sources('search');
        if (isset($_GET['Sources'])) {
            $contacts->source->attributes = $_GET['Sources'];
        }

        if(!isset($_GET['Sources']) && !isset($_GET['ContactTypeLu']) && !$_GET['Contacts']['fromSearchDate']) {
            $contacts->id = 0;
        }

        $emailTemplates = new EmailTemplates();
        if (isset($_GET['EmailBlasts']) && !empty($_GET['EmailBlasts']['email_template_id'])) {
            //$emailTemplates->attributes = $_GET['EmailTemplates'];
            $emailTemplates = $emailTemplates->findByPk($_GET['EmailBlasts']['email_template_id']);
        }

        if(!Yii::app()->user->settings->office_name) {
            $error = 'Missing Office Name in Account Settings. Please Go to Tools => Account Settings and provide the complete office information.';
        }

        if(!Yii::app()->user->settings->office_address) {
            $error .= ($error) ? "<br>": "";
            $error = 'Missing Office Address in Account Settings. Please Go to Tools => Account Settings and provide the complete office information.';
        }

        if(!Yii::app()->user->settings->office_city) {
            $error .= ($error) ? "<br>": "";
            $error = 'Missing Office City in Account Settings. Please Go to Tools => Account Settings and provide the complete office information.';
        }

        if(!Yii::app()->user->settings->office_state) {
            $error .= ($error) ? "<br>": "";
            $error = 'Missing Office State in Account Settings. Please Go to Tools => Account Settings and provide the complete office information.';
        }

        if(!Yii::app()->user->settings->office_zip) {
            $error .= ($error) ? "<br>": "";
            $error = 'Missing Office Zip in Account Settings. Please Go to Tools => Account Settings and provide the complete office information.';
        }

        $this->render('blast', array(
                'error' => $error,
                'emailTemplates' => $emailTemplates,
                'contacts' => $contacts,
                'emailBlast' => $emailBlast,
            )
        );
    }

    public function actionBlastSend()
    {
        if(!isset($_POST['Sources']) && !isset($_POST['ContactTypeLu']) && !isset($_POST['Contacts'])) {
            return;
        }

        if(!isset($_POST['EmailBlasts']['sender_name']) || !isset($_POST['EmailBlasts']['email_subject']) || !isset($_POST['EmailBlasts']['description'])) {
            return;
        }

        // check to see if a blast already went out today. Limit it to 1 per day :)
        $alreadySentToday = Yii::app()->db->createCommand("select count(*) from email_blasts where DATE(complete_datetime) = '".date('Y-m-d')."'")->queryScalar();
        if($alreadySentToday) {
            //@todo: have a better filter/limit
            throw new Exception('Already sent an Email Blast today. Limit 1 per day.');
        }

        //verify office name and address exists for can spam compliance data
        if(!Yii::app()->user->settings->office_name) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Missing office name in settings.');
        }

        if(!Yii::app()->user->settings->office_address) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Missing office address in settings.');
        }

        if(!Yii::app()->user->settings->office_city) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Missing office city in settings.');
        }

        if(!Yii::app()->user->settings->office_state) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Missing office state in settings.');
        }

        if(!Yii::app()->user->settings->office_zip) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Missing office zip in settings.');
        }

        $contacts = new Contacts();
        $criteria = new CDbCriteria();
        $criteria->together = true;
        $criteria->compare('t.is_deleted', 0);

        $contacts->attributes = $_POST['Contacts'];
        if (!empty($contacts->fromSearchDate) && !empty($contacts->toSearchDate)) {
            $criteria->addBetweenCondition('DATE(t.added)', $contacts->fromSearchDate, $contacts->toSearchDate);
        }

        $criteria->with = CMap::mergeArray($criteria->with, array('emails'));
        $criteria->compare('emails.email', '.', true);

        $contacts->contactTypeLu = new ContactTypeLu('search');
        if (isset($_POST['ContactTypeLu'])) {
            $contacts->contactTypeLu->attributes = $_POST['ContactTypeLu'];
            $criteria->with = CMap::mergeArray($criteria->with, array('contactTypeLu'));
            if($contacts->contactTypeLu->idCollection) {
                $criteria->addInCondition('contactTypeLu.contact_type_id', $contacts->contactTypeLu->idCollection);
            }

            if($contacts->contactTypeLu->excludeIds) {
                $criteria->addNotInCondition('contactTypeLu.contact_type_id', $contacts->contactTypeLu->excludeIds);
            }
        }

        $contacts->source = new Sources('search');
        if (isset($_POST['Sources'])) {
            $contacts->source->attributes = $_POST['Sources'];
            $criteria->with = CMap::mergeArray($criteria->with, array('source'));
            if($contacts->source->idCollection) {
                $criteria->addInCondition('source.id', $contacts->source->idCollection);
            }

            if($contacts->source->excludeIds) {
                $criteria->addNotInCondition('source.id', $contacts->source->excludeIds);
            }
        }

        if($contactsToEmail = $contacts->findAll($criteria)) {

            //create and save email blast record if not exist
            $emailBlast = new EmailBlasts();
            $emailBlast->attributes = $_POST['EmailBlasts'];
            $emailTemplates = EmailTemplates::model()->findByPk($emailBlast->email_template_id);
            $emailBlast->email_body = $emailTemplates->body;
            if(!$emailBlast->save()) {
                // error handling stop process
                //throw new Exception('Email Blast error saving: '.print_r($emailBlast->getErrors(), 1));
                echo CJSON::encode(array('status'=>'error','message'=>current($emailBlast->getErrors())[0]));
                Yii::app()->end();
            }

            // collection criteria to store
            if($contacts->fromSearchDate) {
                $recipientCriteria['fromSearchDate'] = $contacts->fromSearchDate;
            }
            if($contacts->toSearchDate) {
                $recipientCriteria['toSearchDate'] = $contacts->toSearchDate;
            }

            if($contacts->contactTypeLu->idCollection) {
                $recipientCriteria['contactTypeIds'] = $contacts->contactTypeLu->idCollection;
            }

            if($contacts->source->idCollection) {
                $recipientCriteria['sourceIds'] = $contacts->source->idCollection;
            }

            if($contacts->contactTypeLu->excludeIds) {
                $recipientCriteria['contactTypeExcludeIds'] = $contacts->contactTypeLu->excludeIds;
            }

            if($contacts->source->excludeIds) {
                $recipientCriteria['sourceExcludeIds'] = $contacts->source->excludeIds;
            }

            // save initial data for email blast
            $emailBlast->status = 'Draft';
            $emailBlast->recipient_count = count($contactsToEmail);
            $emailBlast->start_send_datetime = null;
            $emailBlast->recipient_criteria = CJSON::encode($recipientCriteria);
            if(!$emailBlast->save()) {
                //throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Email Blast initial start data logging save had error. Attributes: ' . print_r($emailBlast->attributes, true) . PHP_EOL.'Error: ' . print_r($emailBlast->getErrors(), true));
                echo CJSON::encode(array('status'=>'error','message'=>current($emailBlast->getErrors())[0]));
            }
            else {

                if(isset($_GET['draft']) && $_GET['draft'] == 1) {
                    echo CJSON::encode(array('status'=>'success','message'=>'Email Blast draft is saved.'));
                }
                else {
                    echo CJSON::encode(array('status'=>'success','message'=>'Email Blast is scheduled successfully.'));
                }
                Yii::app()->end();

                // send email to sender that email blast has started
                /*$subject = 'Starting Email Blast #: '.$emailBlast->id.'.';
                $body = 'Starting Email Blast #: '.$emailBlast->id.'.';
                StmZendMail::easyMail(array('do-not-reply@seizethemarket.net' => 'STM Notification'), 'christine@seizethemarket.com', $subject, $body);*/
            }

            //$this->_sendEmailToContacts($emailBlast, $contactsToEmail, $emailTemplates);

        }

        // send email to sender that email has been sent
        /*$subject = 'Email Blast #: '.$emailBlast->id.'. Complete Confirmation';
        $body = 'This email is to confirm that the Email Blast #: '.$emailBlast->id.' send is complete.'
            .'<br>Emails that were opt-outs or bounced detected and prevented from being sent:<br>'.print_r($this->_bounceDetectedEmails, true);
        StmZendMail::easyMail(array('do-not-reply@seizethemarket.net' => 'STM Notification'), 'christine@seizethemarket.com', $subject, $body);*/

        Yii::app()->end();
    }

    /*public function actionSendDraft($id)
    {
        if(!($emailBlast = EmailBlasts::model()->findByPk($id))) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Email blast criteria is required.');
        }

        if($emailBlast->status == 'Sent') {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Email blast already finished sending.');
        }

        $recipientCriteria = CJSON::decode($emailBlast->recipient_criteria);

        $contacts = new Contacts();
        $criteria = new CDbCriteria();
        $criteria->together = true;
        $criteria->compare('t.is_deleted', 0); //@todo: this should be part of default. Inspect and remove.

        // collection criteria to store
        if($recipientCriteria['fromSearchDate'] && $recipientCriteria['toSearchDate']) {
            $criteria->addBetweenCondition('DATE(t.added)', $recipientCriteria['fromSearchDate'], $recipientCriteria['toSearchDate']);
        }

        if($recipientCriteria['contactTypeIds']) {
            $criteria->with = CMap::mergeArray($criteria->with, array('contactTypeLu'));
            $criteria->addInCondition('contactTypeLu.contact_type_id', $recipientCriteria['contactTypeIds']);
        }

        if($recipientCriteria['sourceIds']) {
            $criteria->with = CMap::mergeArray($criteria->with, array('source'));
            $criteria->addInCondition('source.id', $recipientCriteria['sourceIds']);
        }

        // pull any contact Ids that have already been sent to so you don't re-send
        if($sentContactIds = Yii::app()->db->createCommand("select contact_id from email_blast_contacts b where b.email_blast_id=".$id)->queryColumn()) {
            $criteria->addNotInCondition('t.id', $sentContactIds);
        }

        if($contactsToEmail = $contacts->findAll($criteria)) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: # Contacts found: '.count($contactsToEmail), CLogger::LEVEL_ERROR);
            $this->_sendEmailToContacts($emailBlast, $contactsToEmail, $emailBlast->emailTemplate);
        }

        // send email to sender that email has been sent
        $subject = 'Email Blast #: '.$emailBlast->id.'. Complete Confirmation';
        $body = 'This email is to confirm that the Email Blast #: '.$emailBlast->id.' send is complete.'
                .'<br>Emails that were opt-outs or bounced detected and prevented from being sent:<br>'.print_r($this->_bounceDetectedEmails, true);
        StmZendMail::easyMail(array('do-not-reply@seizethemarket.net' => 'STM Notification'), 'christine@seizethemarket.com', $subject, $body);

        Yii::app()->end();
    }*/

    /*protected function _sendEmailToContacts($emailBlast, $contactsToEmail, $emailTemplates)
    {
        // prevents script from timing out
        set_time_limit(1800);
        ini_set('max_execution_time', 1800);

        $domainName = Yii::app()->user->domain->name;
        $officeAddress =  Yii::app()->user->settings->office_address.', '.Yii::app()->user->settings->office_city.', '.Yii::app()->user->settings->office_state.''.Yii::app()->user->settings->office_zip;
        $successfulSent = 0;

        $signature = Yii::app()->db->createCommand("select value from setting_contact_values where contact_id=".$emailBlast->senderEmail->contact->id." AND setting_id=".Settings::SETTING_EMAIL_SIGNATURE)->queryScalar();

        $emailMessage = new EmailMessage();
        $emailMessage->id = StmZendMail::NO_EMAIL_ID;
        $emailMessage->setAttributes(array(
                //'id' => 0, // @todo: email message id - put in a fake one??? need to have the right reply to format
                'component_type_id' => ComponentTypes::CONTACTS,
                'from_email_id' => $emailBlast->sender_email_id,
            ));

        //sets variables to make work, hack way into $emailMessage->fromEmail->contact->getFullName()) - only doing this cuz we are moving to zend and no need to rewrite
        $emailMessage->fromEmail = $emailBlast->senderEmail;
        $emailMessage->fromEmail->contact = $emailBlast->senderEmail->contact;

        $alreadySentContactIds = Yii::app()->db->createCommand("SELECT contact_id FROM email_blast_contacts where email_blast_id=".$emailBlast->id)->queryColumn();

        // pre-filter bounced emails - for some reason, zend class not doing it properly?
        $bouncedEmails = Yii::app()->stm_hq->createCommand("SELECT TRIM(LOWER(email)) FROM emails_undeliverable WHERE client_id IS NULL OR (client_id=".Yii::app()->user->clientId." AND account_id=".Yii::app()->user->accountId.") ORDER BY email")->queryColumn();

        $optOutEmails = Yii::app()->db->createCommand("SELECT TRIM(LOWER(email)) FROM emails WHERE email_status_id IN(".EmailStatus::OPT_OUT.",".EmailStatus::HARD_BOUNCE.") ORDER BY email")->queryColumn();

        $doNotDeliverEmails = CMap::mergeArray(array_flip($bouncedEmails), array_flip($optOutEmails));
        $doNotDeliverEmails = array_flip($doNotDeliverEmails);

        foreach($contactsToEmail as $contactToEmail) {

            // prevents from dupe sending
            if(in_array($contactToEmail->id, $alreadySentContactIds)) {
                continue;
            }

            // update component_id to adjust reply-to email result
            $emailMessage->component_id = $contactToEmail->id;

            //sets variables to make work, hack way into $emailMessage->toEmail->contact->id - only doing this cuz we are moving to zend and no need to rewrite
            $emailMessage->toEmail = $contactToEmail->getPrimaryEmailObj();

            if(!$emailMessage->toEmail) {
                // if the contact does not have an email skip this contact
                continue;
            }

            // if bounce detected, add to the collection to report on later
            if(in_array(trim(strtolower($emailMessage->toEmail->email)), $doNotDeliverEmails)) {
                $this->_bounceDetectedEmails[] = $emailMessage->toEmail->email;
                continue;
            }

            $emailMessage->toEmail->contact = $contactToEmail;

            $emailMessage->content = $emailTemplates->filterBodyByContactData($contactToEmail);
            $emailMessage->content .= $signature;
            $emailMessage->content .= StmFunctions::getCanSpamFooter($domainName, Yii::app()->user->settings->office_name, $officeAddress, $emailMessage->toEmail->id);

            $zendMessage = new StmZendMail();
            $zendMessage->addTo($contactToEmail->getPrimaryEmail(), Yii::app()->format->formatProperCase($contactToEmail->getFullName()));
            $replyTo = $zendMessage->getReplyToEmail($emailMessage, $domainName); //@todo: verify it worked without a saved email message ... NEED emailMessageId ***** create new componentType for message Id???
            $zendMessage->setFrom($replyTo, Yii::app()->format->formatProperCase($emailBlast->senderEmail->contact->getFullName()));
            $zendMessage->setSubject($emailBlast->email_subject);

            $zendMessage->setBodyHtml(StmZendMail::replaceLinksForTracking($emailMessage->content, $emailMessage->toEmail->contact->id, $emailMessage->id, $domainName, $trackEmailMessages=false, $emailTemplates->id));

            // Attempt to send message
            try {
                if($zendMessage->send(StmZendMail::getAwsSesTransport())) {
                    $successfulSent++;
                    $alreadySentContactIds[] = $contactToEmail->id;

//                    if($successfulSent > 10) {
//                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI Bounce Emails: '.print_r($this->_bounceDetectedEmails, true), CLogger::LEVEL_ERROR);
//                        Yii::app()->end();
//                    }
                }

                // add to emailBlastSent table
                $emailBlastContact = new EmailBlastContacts();
                $emailBlastContact->setAttributes(array('contact_id'=>$contactToEmail->id, 'email_blast_id'=>$emailBlast->id, 'sent'=> new CDbExpression('NOW()')));
                if(!$emailBlastContact->save()) {
                    throw new Exception('Email Blast contact did not save.');
                }
            }
            catch(Exception $e) {
                //                    //@todo: need better logging in case we need to see it all in one list
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL
                    .'Email Blast #: '.$emailBlast->id.' | Contact Attributes: '.print_r($contactToEmail->attributes, true), CLogger::LEVEL_ERROR);

                // let the system rest for a couple seconds
                sleep(2);
                continue;
            }

            if(YII_DEBUG) {
                // only run once for debug
                break;
            }
        }

        // record when email blast went out complete
        $emailBlast->status = 'Sent';
        $emailBlast->sent_count = $successfulSent;
        $emailBlast->complete_datetime = new CDbExpression('NOW()');
        if(!$emailBlast->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Email Blast complete save had error. Attributes: "' . print_r($emailBlast->attributes, true) . '" '.PHP_EOL.'Error: ' . print_r($emailBlast->getErrors(), true));
        }
    }*/

    public function actionEmailTemplate()
    {
        $model = new EmailBlasts();
        if (!empty($_POST['EmailBlasts']['email_template_id'])) {
            $model->attributes = $_POST['EmailBlasts'];

            if($emailTemplate = EmailTemplates::model()->findByPk($model->email_template_id)) {
                $data = array(
                    'id' => $emailTemplate->id,
                    'subject' => $emailTemplate->subject,
                    'description' => $emailTemplate->description,
                    'body' => $emailTemplate->body,
                );
                echo CJSON::encode($data);
            }
        }

        Yii::app()->end();
    }

    public function actionEncoding()
    {
        $this->title = 'Email Encoding / Decoding';

        if(Yii::app()->request->isAjaxRequest) {

            if($_POST['data']) {
                echo (is_numeric($_POST['data']))? StmFunctions::click3Encode($_POST['data']) : StmFunctions::click3Decode($_POST['data']);
            }

            Yii::app()->end();
        }

        $this->render('encoding', array());
    }

    public function actionExist()
    {
        $this->title = 'Check Email Exist';
        if(isset($_POST['emails'])) {
            $emails = array();

            if(strpos($_POST['emails'], ',') !== false) {
                $emailCommas = explode(',', $_POST['emails']);
                foreach($emailCommas as $emailComma) {
                    $emails[strtolower(trim($emailComma))] = '';
                }
            }

            if(strpos($_POST['emails'], "\n") !== false) {
                $emailNewLines = explode("\n", $_POST['emails']);
                foreach($emailNewLines as $emailNewLine) {
                    $emails[strtolower(trim($emailNewLine))] = '';
                }
            }

            if(strpos($_POST['emails'], "\n") == false && strpos($_POST['emails'], ",") == false) {
                $emails[strtolower(trim($_POST['emails']))] = '';
            }

            $emails = array_keys($emails);
            $emailValues = array();
            $emailVars = '';

            foreach($emails as $key => $email) {

                $emailVars .= ($emailVars)? ',':'';
                $emailVars .= ':'.$key.'EmailVar';
                $emailValues[':'.$key.'EmailVar'] = $email;
            }

            $emailExists = array();
            if(count($emails)) {
                $emailExists = Yii::app()->db->createCommand()
                    ->select('email')
                    ->from('emails')
                    ->where('email IN('.$emailVars.')', $emailValues)
                    ->andWhere('is_deleted=0')
                    ->queryColumn();
            }
            Yii::app()->end(CJSON::encode($emailExists));
        }
        $this->render('exist', array());
    }

    public function actionMassTags()
    {
//        $this->title = 'Check Email Exist';
//        if(isset($_POST['emails'])) {
//            $emails = array();
//
//            if(strpos($_POST['emails'], ',') !== false) {
//                $emailCommas = explode(',', $_POST['emails']);
//                foreach($emailCommas as $emailComma) {
//                    $emails[strtolower(trim($emailComma))] = '';
//                }
//            }
//
//            if(strpos($_POST['emails'], "\n") !== false) {
//                $emailNewLines = explode("\n", $_POST['emails']);
//                foreach($emailNewLines as $emailNewLine) {
//                    $emails[strtolower(trim($emailNewLine))] = '';
//                }
//            }
//
//            if(strpos($_POST['emails'], "\n") == false && strpos($_POST['emails'], ",") == false) {
//                $emails[strtolower(trim($_POST['emails']))] = '';
//            }
//
//            $emails = array_keys($emails);
//            $emailValues = array();
//            $emailVars = '';
//
//            foreach($emails as $key => $email) {
//
//                $emailVars .= ($emailVars)? ',':'';
//                $emailVars .= ':'.$key.'EmailVar';
//                $emailValues[':'.$key.'EmailVar'] = $email;
//            }
//
//            $emailExists = array();
//            if(count($emails)) {
//                $emailExists = Yii::app()->db->createCommand()
//                    ->select('email')
//                    ->from('emails')
//                    ->where('email IN('.$emailVars.')', $emailValues)
//                    ->andWhere('is_deleted=0')
//                    ->queryColumn();
//            }
//            Yii::app()->end(CJSON::encode($emailExists));
//        }
//        $this->render('exist', array());
    }
}
