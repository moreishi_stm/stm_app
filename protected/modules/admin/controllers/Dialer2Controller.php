<?php
// Import stuff we need
Yii::import('admin_module.components.StmDialer.Lists.Factory', true);

/**
 * Dialer Controller
 */
class Dialer2Controller extends AdminController
{
    public $title = 'Dialer';

    const FROM_NUMBER = '19048006995';
    const MAX_CALL_COUNT = 12;

    /**
     * Response
     *
     * Response object for Plivo
     * @var \Plivo\Response
     */
    protected $_response;

    /**
     * Action Index
     *
     * Default page load action
     * @return void
     */
    public function actionIndex()
    {
        if($callUuid = CallSessions::model()->hasActiveSession(Yii::app()->user->id)) {
            $response = Yii::app()->plivo->getPlivo()->get_live_calls(array(
                'call_uuid'            => $callUuid,
            ));
        }

        $callerIdName = Yii::app()->user->settings->dialer_caller_id_name;
        $callerIdNumber = Yii::app()->user->settings->dialer_caller_id;
//        $callerIdName = ($_COOKIE['callerIdName']) ? $_COOKIE['callerIdName'] : $dialerCallerId = Yii::app()->user->settings->dialer_caller_id_name;
//        $callerIdNumber = ($_COOKIE['callerIdNumber']) ? $_COOKIE['callerIdNumber'] : $dialerCallerId = Yii::app()->user->settings->dialer_caller_id;

        //@todo: temp fix!!
        $callUuid = false;

        // Render the view
        $this->render('index', array(
            'hasActiveSession'=>(boolean) $callUuid,
            'callerIdName'=>$callerIdName,
            'callerIdNumber'=>$callerIdNumber,
            'nodeUrl' => YII_DEBUG ? 'dev-nicole.christineleeteam.com:8080' : 'node-admin.seizethemarket.net'
        ));
    }

    public function actionSessions()
    {
        Yii::import('admin_widgets.DateRanger.DateRanger');
        if(isset($_GET['contact_id'])) {
            $contactId = $_GET['contact_id'];
        }
        else {
            $contactId = Yii::app()->user->id;
        }

        $DateRanger = new DateRanger();
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $rawData = Yii::app()->db->createCommand()
            ->select('contact_id, SUM(bill_duration) total_time, CONCAT(c.first_name, " ", c.last_name) fullName')
            ->from("call_sessions s")
            ->leftJoin("contacts c", "c.id=s.contact_id")
            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'")
            ->group("s.contact_id")
            ->queryAll();

        $totalSum = Yii::app()->db->createCommand()
            ->select('SUM(bill_duration)')
            ->from("call_sessions s")
            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'")
            ->queryScalar();

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        // Render the view
        $this->render('session', array(
                'dataProvider'=>$dataProvider,
                'totalSum' => $totalSum,
                'fromDate' => date('m/d/Y', strtotime($dateRange['from_date'])),
                'toDate' => date('m/d/Y', strtotime($dateRange['to_date'])),
            ));
    }

    public function actionCallfinder()
    {
        $model = new Calls();
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Calls'])) {
            $model->attributes=$_GET['Calls'];
            $model->dial_b_leg_status = 'Hangup';
        }
        else {
            $model->id = 0;
        }
        $this->render('callfinder', array('model'=>$model));
    }

    /**
     * Load Action
     *
     * Returns JSON formatted data for DataTable
     * @return void
     */
    public function actionLoad()
    {
        // Get and check ID, which is the type_ma
        $id = Yii::app()->request->getParam('id');
        $presetId = Yii::app()->request->getParam('presetId');
        $currentUser = Yii::app()->user->id;
        $sessionId  = Yii::app()->request->getParam('callsession');
        $filters['maxDialerCallCount'] = Yii::app()->request->getParam('maxDialerCallCount');
        $filters['maxDialerCallDays'] = Yii::app()->request->getParam('maxDialerCallDays');

        if(!$presetId) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Preset ID is required.');
        }

//        if(!$id && !$presetId) {
//            throw new Exception(__CLASS__.'(:'.__LINE__.') Dialer load missing Call List ID and Preset ID. At least one is required.');
//        }

        if($id && !is_numeric($id)) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Invalid Call List ID.');
        }

        if($presetId && !is_numeric($presetId)) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Invalid Preset ID.');
        }

        // In this initialize, checks to see if current user has any sessions active today. If not any "Ringing" call_list_phones records that belong to this user are set to "No Answer"
        if(Yii::app()->request->getParam('initialize') == 1) {

            // check to see if current user has any open sessions
            if(!CallSessions::model()->hasActiveSession(Yii::app()->user->id)) {

                // this can be reused in the actual update and error reporting if set to true. Prevents condition from going out of sync.
                $ringingCallListCondition = "l.preset_ma={$presetId} AND c.status IN ('Ringing','Answered') AND (l.contact_id IS NULL OR l.contact_id={$currentUser} OR c.updated_by={$currentUser})";

                // uses this flag so if need to report Ringings via error log, set to true. Otherwise, should be set to false to minimize overhead
                if($reportRingingErrFlag = true) {

                    // query so "Ringing" can be reported
                    if($ringingIds = Yii::app()->db->createCommand("select c.id call_list_phone_id FROM call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id WHERE {$ringingCallListCondition}")->queryColumn()) {
                        // send email of list of erroneously "Ringing" call_list_phone Id's
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Error: Ringing phone # before today detected. Changing to "No Answer".'.PHP_EOL.'You can update the flag above this line to disable this reporting.'.PHP_EOL.'Call List Phone Id #: '.print_r($ringingIds, true), CLogger::LEVEL_ERROR);
                    }
                }

                // set the ringing Id's to No Answer
                Yii::app()->db->createCommand("UPDATE call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id SET status='No Answer' WHERE {$ringingCallListCondition}")->execute();
            }
        }

        // checks to see if a "preset" input was given... later it could be a specific list type creation. In the beginning, everything will be preset to make it easy for agents
        if($presetId){

            $callList = CallLists::getCallListByPresetId($presetId);

            //@todo: this if statement + the call to _getCallListPhonesToAdd() was the actionCreate() function... get rid of it or organize differently? - ask Nicole
            // query for preset is shared or not and the contact_id search may be removed
            if($callList) {
                $id = $callList->id;
            }
            else {
                $callList = new CallLists();
                $callList->setAttributes(array(
                    'preset_ma' => $presetId,
                    'contact_id' => null,
                    'added' =>  new CDbExpression('NOW()'),
                ));

                $isShared = CallLists::getIsPresetShared($presetId);
                $callList->name = ($isShared) ? $callList->getPresetTypes()[$presetId].' List' : Yii::app()->user->contact->fullName.' - '.$callList->getPresetTypes()[$presetId].' List';
                $callList->contact_id = ($isShared) ? null : Yii::app()->user->id;

                if(!$callList->save()) {
                    throw new Exception('Error in creating Preset Call List! Error Message: '.print_r($callList->getErrors(), true));
                }
                $id = $callList->id;
            }

            // Use new adapter pattern to get a list instance
            $o = \StmDialer\Lists\Factory::getInstance($presetId, $id);
            $o->update($id, $filters);
        }

        // Retrieve current call session
        $inProgress = true;
        if($sessionId) {
            $callSession = CallSessions::model()->findByPk($sessionId);
            if($callSession->end) {
                $inProgress = false;
            }
        }

        // Get phone numbers to call
        //@todo: this function needs to go into an adapter???? 3/21/2015 - CLee ... this is very similar or the SAME login as buildCallListPhones() in adapter
        $callListPhones = $o->getQueueBySortOrder($id, Yii::app()->user->id, $filters);
        //$callListPhones = CallListPhones::getCallsByList($id, Yii::app()->user->id, $filters);

        // Get in progress call, if we have one
        $active = null;
        foreach($callListPhones as $callListPhone) {
            if($callListPhone['status'] == 'Answered') {
                // if "Answered call does not have a active session, set the call as "No Answer" & email error log it.
                if(!$sessionId) {
                    // get data for call_list_phone before updating it to "No Answer"
                    $callListPhoneMissingSession = Yii::app()->db->createCommand("select * from  call_list_phones WHERE id={$callListPhone['call_list_phone_id']}")->queryRow();
                    Yii::app()->db->createCommand("UPDATE call_list_phones SET `status`='".CallListPhones::NO_ANSWER."' WHERE id={$callListPhone['call_list_phone_id']}")->execute();

                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') **** URGENT Error****'.PHP_EOL.'A call_list_phone record is marked as Answered without a call session ID. Marking this call as No Answer. Please investigate. Call List Phone ID: '.$callListPhone['call_list_phone_id'].' | Call List ID#: '.$id.' | Phone#: '.$callListPhone['phone'].' | Call List Phone Record: '.print_r($callListPhoneMissingSession, true).' | Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                $callId = Calls::getIdByAnsweredCallListPhoneId($callListPhone['call_list_phone_id'], $sessionId);

                if(!$callId) { // && !YII_DEBUG
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find CallID.  Please investigate. '.PHP_EOL
                        .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                        .'Session ID: '.$sessionId.PHP_EOL
                        .'Call List ID#: '.$id.PHP_EOL
                        .'Phone#: '.$callListPhone['phone'].PHP_EOL
                        .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                // Get phone record
                $phone = Phones::model()->findByPk($callListPhone['phone_id']);

                if(!$phone) { // && !YII_DEBUG
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find Phone model.  Please investigate. '.PHP_EOL
                        .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                        .'Session ID: '.$sessionId.PHP_EOL
                        .'Call List ID#: '.$id.PHP_EOL
                        .'Phone#: '.$callListPhone['phone'].PHP_EOL
                        .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                // Get the associated contact
                $contact = $phone->contact;

                $command = Yii::app()->db->createCommand()
                    ->select('a.id')
                    ->from('activity_log a')
                    ->where('a.call_id='.$callId)
                    ->order('a.id DESC');

                $answeredActivityLogIds = $command->queryAll();

                // get the activity log that is marked as Answered for this call to pass to GUI as it may be a voicemail
                // @todo: one can not be found between the lag of being answered and marking as voicemail??? - keep an eye on this, watch error log emails
                if(!$answeredActivityLogIds) {

                    // run the same query without the task_type_id... @todo: temporary check
                    $secondQuery = Yii::app()->db->createCommand()
                        ->select('a.*')
                        ->from('activity_log a')
                        ->leftJoin('calls c', 'a.call_id=c.id')
                        ->leftJoin('call_list_phones c2', 'c.call_list_phone_id=c2.id')
                        ->andWhere('c2.id='.$callListPhone['call_list_phone_id'])
                        ->andWhere('DATE(a.added)="'.date('Y-m-d').'"')
                        ->andWhere('a.added_by='.Yii::app()->user->id)
                        ->andWhere('c.call_session_id='.$sessionId)
                        ->order('a.id DESC')
                        ->queryAll();

                    // threw error cuz no call data
                    if($callId) {
                        // third query using just call_id
                        $thirdQuery = Yii::app()->db->createCommand()
                            ->select('a.*')
                            ->from('activity_log a')
                            ->where('a.call_id='.$callId)
                            ->order('a.id DESC')
                            ->queryAll();
                    }

                    if(count($secondQuery)) {
                        $secondaryNote = PHP_EOL.'NOTE: running the Second Query without the task_type_id as "Answer" provided '.count($secondQuery).' result(s). Result Data: '.print_r($secondQuery->attributes, true).PHP_EOL.'Call ID#: '.$callId;

                        //assign value to variable for further processing, if data found by the second query
                        $answeredActivityLogId = $secondQuery[0];
                    }
                    if(count($thirdQuery)) {
                        $thirdNote = PHP_EOL.'NOTE: running the Third Query with only call_id provided '.count($thirdQuery).' result(s). Result Data: '.print_r($thirdQuery->attributes, true).PHP_EOL.'Call ID#: '.$callId;
                        if(!$answeredActivityLogId){
                            $thirdNote .= PHP_EOL.'Second query also had no results but third query did. Using activity_log from third query.';
                            $answeredActivityLogId = $thirdQuery[0];
                        }
                    }

                    // log error, no activity log id was found
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: No activity log record was found for an answered call. Call ID#: '.$callId.' Call List ID#: '.$id.' Call List Phone ID#: '.$callListPhone['call_list_phone_id'].' Query: '.$command->getText().$secondaryNote.$thirdNote, CLogger::LEVEL_ERROR);

                    //@todo: this would indicated call_list_phone with status of Answered BUT may be "stuck" and really hung up. Possibly need to hang up and send error log note FYI untill fully fixed?
                    //@todo: this may also be caused by lag between answer and updating activity log. Can check this by last_updated and now being more than 5 seconds apart?
                    // check to see if it is linked to current call session, if not it's a leftover "Answered" bug caused by a different session
                }
                else {
                    $answeredActivityLogId = $answeredActivityLogIds[0];

                    // there should only be one result, but
                    if(count($answeredActivityLogIds)>1) {

                        $activityLogsAttributesToReport = array();
                        foreach($answeredActivityLogIds as $answeredActivityLogIdToReport) {
                            $activityLogsAttributesToReport[] = $answeredActivityLogIdToReport->attributes;
                        }

                        // error in finding more than 1 activity_log entry. Check query logic to narrow down more effectively
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: More than activity log was found. Query needs to be narrowed down more or a call status of Answered is left in that status unnecessarily. Please investigate immediately. Call ID#: '.$callId.' Call List ID#: '.$id.PHP_EOL.'All Activity Log Attributes: '.print_r($activityLogsAttributesToReport, true).' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
                    }
                }

                // Mode data for active content
                $active = array(
                    'call_id'           =>  $callId,
                    'call_list_phone_id'=>  $callListPhone['call_list_phone_id'],
                    'activity_log_id'   =>  $answeredActivityLogId['id'],
                    'phone_id'          =>  $phone->id,
                    'first_name'        =>  $contact->first_name,
                    'last_name'         =>  $contact->last_name,
                    'spouse_first_name' =>  ($contact->spouse_first_name)? ' & '.$contact->spouse_first_name : '',
                    'spouse_last_name'  =>  $contact->spouse_last_name,
                    'address'           =>  $contact->primaryAddress->address,
                    'city'              =>  $contact->primaryAddress->city,
                    'state'             =>  AddressStates::getShortNameById($contact->primaryAddress->state_id),
                    'zipcode'           =>  $contact->primaryAddress->zip,
                    'phone'             =>  $phone->phone,
                    'task_id'           =>  $callListPhone['task_id'],
                    'description'       =>  $callListPhone['description'],
                    'component_id'      =>  $callListPhone['component_id'],
                    'component_type_id' =>  $callListPhone['component_type_id'],
                    'component_name'    =>  ComponentTypes::model()->findByPk($callListPhone['component_type_id'])->name,
                );
            }
        }

        // Retrieve a list of phone numbers to call for the current session
        $this->_sendJson(array(
            'status'    =>  'success',
            'results'   =>  $callListPhones,
            'meta'	=>	array(
                'in_progress'               =>  $inProgress,
                'active'                    =>  $active,
            )
        ));
    }

    /**
     * Create Action
     *
     * Used to create a new call session
     * @param integer $id Preset ID
     * @return void
     */

//    public function actionCreate($id)
//    {
    // Get people to call
//        $data = $this->_getPeopleToCall($id);

//        if($callList = CallLists::model()->findByAttributes(array('preset_ma'=>$id, 'contact_id'=> Yii::app()->user->id))) {
//        }
//        else {
//            // Create a new call session record
//            $callList = new CallLists();
//            $callList->contact_id = Yii::app()->user->getContact()->id;
//            $callList->name = Yii::app()->request->getPost('name');
//            $callList->added = new CDbExpression('NOW()');
//            $callList->save();
//        }

    // Add all phone numbers to table
//        foreach($data['tasks'] as $task) {
//
//
//            // If we don't have any phone numbers, skip adding this
//            if(!array_key_exists($task['contact_id'], $data['phones'])) {
//                continue;
//            }
//
//            // Add each number for the task, check for duplicates for task_id and phone_id, allow to see if new phone # is added since previous list create
//            foreach($data['phones'][$task['contact_id']] as $phone) {
//
//                if(!isset($data['phoneIdsByTaskId'][$task['id']]) || !in_array($phone['id'], $data['phoneIdsByTaskId'][$task['id']])) {
//
//                    // Create a new call record
//                    $callListPhones = new CallListPhones();
//                    $callListPhones->phone_id = $phone['id'];
//                    $callListPhones->task_id = $task['id'];
//                    $callListPhones->call_list_id = $callList->id;
//                    $callListPhones->added = new CDbExpression('NOW()');
//                    $callListPhones->save();
//                }
//            }
//        }
//    }

    /**
     * Action Start
     *
     * Stars the dialer session
     * @return string
     */
    public function actionStart()
    {
        // see if there is another session active first for this contact. If so, get the b-leg-uuid and stop the call
        if($sessionCallUuid = CallSessions::model()->hasActiveSession(Yii::app()->user->id)) {
            //@todo: see if there are any answered call_sessions that are active and hang those up first. Send log note when this happens and let user determine how to handle it.
            //            $this->_sendJson(array(
            //                    'status'            =>  'sessionExists',
            //                ));

            $bLegUuid = Yii::app()->db->createCommand("SELECT dial_b_leg_uuid FROM calls where dial_a_leg_uuid='".$sessionCallUuid."'")->queryScalar();
            $this->actionHangupSpecificCall($bLegUuid);

            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Session existed upon start dialer. Session Call UUID: '.$sessionCallUuid.' B Leg UUID: '.$bLegUuid, CLogger::LEVEL_ERROR);
        }

        try {
            // Make sure we have a call list ID
            $id = Yii::app()->request->getPost('call_list_id');
            $presetId = Yii::app()->request->getPost('preset_id');
            $callerIdName = Yii::app()->request->getPost('callerIdName');
            $callerIdNumber = Yii::app()->request->getPost('callerIdNumber');
            $maxDialerCallCount = Yii::app()->request->getParam('maxDialerCallCount');
            $maxDialerCallDays = Yii::app()->request->getParam('maxDialerCallDays');

            if($presetId){
                $isShared = CallLists::getIsPresetShared($presetId);
                $contactId = ($isShared)? null : Yii::app()->user->id;
                if($callList = CallLists::model()->findByAttributes(array('preset_ma'=>$presetId, 'contact_id'=> $contactId))) {
                    $id = $callList->id;
                }
            }


            // 1/27/15 - CLee - added to clear out any Answered that is needs to be cleared out
//            if($initial = Yii::app()->request->getParam('initial')) {
//
//                $ringingIds = Yii::app()->db->createCommand("select c.id FROM call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id WHERE c.status='Ringing' AND l.preset_ma={$presetId} AND (l.contact_id IS NULL OR l.contact_id=".Yii::app()->user->contactId.")")->queryColumn();
//
//                if($ringingIds) {
//                    // send email of list of erroneously "Ringing" call_list_phone Id's
//                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Error: Ringing phone # before today detected. Changing to "No Answer". Call List Phone Id #: '.print_r($ringingIds, true), CLogger::LEVEL_ERROR);
//
//                    // set the ringing Id's to No Answer
//                    Yii::app()->db->createCommand("update call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id SET status='No Answer' WHERE l.preset_ma={$presetId} AND (l.contact_id IS NULL OR l.contact_id=".Yii::app()->user->contactId.")")->execute();
//                }
//            }

            if(!is_numeric($id) ) {
                throw new Exception('Call list ID is missing or cannot be found.');
            }

            // Get number of people to call
            $numToCall = Yii::app()->request->getParam('num_to_call');

            // Make sure we have a phone number
            $phoneNumber = '1' . preg_replace('/[^0-9,.]/', '', Yii::app()->request->getPost('phone_number'));

            // Determine what URL we will be using for the Admin Socket Server, Special URL for testing Node things
            $nodeUrl = YII_DEBUG ? 'http://dev-nicole.christineleeteam.com:8080' : 'http://node-admin.seizethemarket.net';

            // Make the call
            $response = Yii::app()->plivo->getPlivo()->make_call(array(
                'to'            => $phoneNumber,
                'from'          => self::FROM_NUMBER ,

//                'answer_url'    =>  StmFunctions::getSiteUrl() . '/plivo/answerConfirmStart/id/' . $id. '/callerIdNumber/'.$callerIdNumber.'/callerIdName/'.$callerIdName.'/num_to_call/' . $numToCall.'/user_id/'.Yii::app()->user->id,
//                'hangup_url'    =>  StmFunctions::getSiteUrl() . '/plivo/hangup/id/' . $id,
                'ring_url'      =>  StmFunctions::getSiteUrl() . '/plivo/ring/id/' . $id,
                'fallback_url'  =>  StmFunctions::getSiteUrl() . '/plivo/fallback/id/' . $id,

                'answer_url'    =>  $nodeUrl . '/plivo/answerConfirmStart?id=' . $id. '&callerIdNumber='.$callerIdNumber.'&callerIdName='.$callerIdName.'&num_to_call=' . $numToCall.'&user_id='.Yii::app()->user->id.'&site='.urlencode(StmFunctions::getSiteUrl()),
                'hangup_url'    =>  $nodeUrl . '/plivo/hangup?id=' . $id . '&site='.urlencode(StmFunctions::getSiteUrl()),
//                'ring_url'      =>  $nodeUrl . '/plivo/ring?id=' . $id,
//                'fallback_url'  =>  $devUrl . '/plivo/fallback?id=' . $id

            ));

            // Start call session
            $callSession = new CallSessions();
            $callSession->setAttributes(array(
                'call_list_id' => $id,
                'number_lines' => $numToCall,
                'contact_id' => Yii::app()->user->id,
                'request_uuid' => $response['response']['request_uuid'], //this is available most actions except callback. Callbacks have call_uuid.
                'start' => new CDbExpression('NOW()'),
                'caller_id_name' => $callerIdName,
                'caller_id_number' => StmFormatter::formatInteger($callerIdNumber),
                'filter_max_call_count' => $maxDialerCallCount,
                'filter_max_call_days' => $maxDialerCallDays,
            ));

            if(!$callSession->save()) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Call session did not save. Call List ID: '.$id.PHP_EOL
                    .' Error message: '.print_r($callSession->getErrors(), true).PHP_EOL.
                    ' Call Response: '.print_r($response, true), CLogger::LEVEL_ERROR);
                throw new Exception('Unable to create new call session');
            }

            // Return JSON response
            $this->_sendJson(array(
                'status'            =>  'success',
                'call_session_id'   =>  $callSession->id,
                'call_list_id'      =>  $id,
            ));
        }
        catch(Exception $e) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Dialer Start Action had an error: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Logs Voicemail from dialer GUI when answered and discovers it is a voicemail and not a real answered call.
     *
     * @param $id integer activity_log_id
     * return void
     */
    public function actionGotVoicemail($id)
    {
        if($activityLog = ActivityLog::model()->findByPk($id)) {
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Voicemail Data Check. Pls review and make sure this SHOULD be changed to voicemail. Possibility of someone pressing inadvertently. Current Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);

            $phone = Yii::app()->db->createCommand("select phone from calls c LEFT JOIN call_list_phones c2 ON c.call_list_phone_id=c2.id LEFT JOIN phones p ON c2.phone_id=p.id where c.id=".$activityLog->call_id)->queryScalar();

            $activityLog->is_spoke_to = 0;
            $activityLog->note = 'Voicemail - '.Yii::app()->format->formatPhone($phone);
            $activityLog->task_type_id = TaskTypes::DIALER_NO_ANSWER;

            if(!$activityLog->save()) { //undo command: update activity_log set task_type_id=42 WHERE id=226623
                // activity log did not save, send error message
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating  activity log for voicemail. Did not save properly. Error Message: '.print_r($activityLog->getErrors(), true).' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
            }

            // disconnect the leg for the voicemail call
            $this->actionHangupSpecificCall($activityLog->call_id);

            $this->_sendJson(array('status'=>'success'));
        }
        else {
            // did not find activity log based on id,  send error message
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Did not find activity log to log voicemail. ActivityLogId# '.$id, CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Adds a Call List Phone back to Queue by updating status
     * @param $id integer call_list_phone_id
     */
    public function actionAddToQueue($id)
    {
        if(Yii::app()->db->createCommand('UPDATE call_list_phones set status="'.CallListPhones::QUEUED.'", updated=NOW() WHERE id='.$id)->execute()) {
            $this->_sendJson(array('status'=>'success'));
        }
    }

    /**
     * Skips the call list phone item, until the next timeframe (1 day, 3 hrs, etc) based on upaated date
     * @param $id integer call_list_phone_id
     */
    public function actionSkipQueue($id)
    {
        $callList = Yii::app()->db->createCommand('select c2.preset_ma preset_ma FROM call_list_phones c LEFT JOIN call_lists c2 ON c2.id=c.call_list_id WHERE c.id='.$id)->queryRow();
        if(Yii::app()->db->createCommand('UPDATE call_list_phones set status="'.CallListPhones::SKIPPED.'", last_called=NOW(), updated=NOW() WHERE id='.$id)->execute()) {

            switch($callList['preset_ma']) {

                case CallLists::MY_SELLERS_TASKS:
                case CallLists::MY_BUYERS_TASKS:
                case CallLists::MY_RECRUITS_TASKS:
                case CallLists::LEAD_POOL_BUYERS_TASKS:
                case CallLists::LEAD_POOL_SELLERS_TASKS:
                    $command = Yii::app()->db->createCommand()
                        ->select('t.component_type_id component_type_id, t.component_id component_id, p.phone')
                        ->from('call_list_phones c')
                        ->leftJoin('phones p','c.phone_id=p.id')
                        ->leftJoin('tasks t','c.task_id=t.id')
                        ->where('c.id='.$id);

                    // keep the $command variable so it can be access later if error
                    $callListPhoneData = $command->queryRow();

                    $componentTypeId = $callListPhoneData['component_type_id'];
                    $componentId = $callListPhoneData['component_id'];

                    break;

                case CallLists::ALL_NEW_SELLER_PROSPECTS:
                case CallLists::ALL_NURTURING_SELLERS:
                case CallLists::ALL_NEW_BUYER_PROSPECTS:
                case CallLists::ALL_NURTURING_BUYERS:

                    $componentTypeId = ($callList['preset_ma'] == CallLists::ALL_NEW_SELLER_PROSPECTS || $callList['preset_ma'] == CallLists::ALL_NURTURING_SELLERS) ? ComponentTypes::SELLERS : ComponentTypes::BUYERS;

                    $command = Yii::app()->db->createCommand()
                        ->select('t.id component_id, p.phone phone')
                        ->from('call_list_phones c')
                        ->leftJoin('phones p','c.phone_id=p.id')
                        ->leftJoin('contacts c2','p.contact_id=c2.id')
                        ->leftJoin('transactions t','t.contact_id=c2.id AND t.component_type_id='.$componentTypeId)
                        ->where('c.id='.$id);

                    // keep the $command variable so it can be access later if error
                    $callListPhoneData = $command->queryRow();

                    if(!$callListPhoneData) {
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Did not find call_list_phones record when attempting to skip. Query: '.$command->getText(), CLogger::LEVEL_ERROR);
                    }

                    $componentId = $callListPhoneData['component_id'];

                    break;

                default:
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Unknown type of table for getting calls by list Preset ID: '.$callList['preset_ma'], CLogger::LEVEL_ERROR);
                    throw new Exception('Unknown type of table for getting calls by list Preset ID. '.$callList['preset_ma']);
                    break;
            }



            // add this note to activity log
            $activityLog = new ActivityLog();
            $activityLog->setAttributes(array(
                'component_type_id' => $componentTypeId,
                'component_id' => $componentId,
                'task_type_id' => TaskTypes::DIALER_SKIP,
                'activity_date' =>  new CDbExpression('NOW()'),
                'note' => CallListPhones::SKIPPED.' - '.Yii::app()->format->formatPhone($callListPhoneData['phone']),
                'lead_gen_type_ma' => 0,
                'is_spoke_to' => 0,
                'is_public' => 0,
                'added' => new CDbExpression('NOW()')
            ));
            if(!$activityLog->save()) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Activity Log for Skip Queue did not save. Error Message: '.print_r($activityLog->getErrors(), true).' Attributes: '.print_r($activityLog->attributes, true).' Call List Phone Data: '.print_r($callListPhoneData, true).' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
            }
            $this->_sendJson(array('status'=>'success'));
        }
    }

    /**
     * Hangup specific call
     * @param $id integer call_id
     */
    public function actionHangupSpecificCall($id)
    {
        // get active call session for user and grab call uuid from that ... there is no uuid when it's ringing and no one has picked up yet.
        $call = Yii::app()->db->createCommand()
            ->select('dial_b_leg_uuid, call_list_phone_id, call_session_id')
            ->from('calls c')
            ->where('c.id="'.$id.'"')
            ->queryRow();

        if($call['dial_b_leg_uuid']) {
//            $this->_response = new \Plivo\Response();
//            // hangup specific call by call_id
//            $this->_response->addHangup(array(
//                    'call_uuid'    =>  $callSession['call_uuid'],
//                ));
//             // Wait for user to press 1 to continue
//            $getDigits = $this->_response->addGetDigits(array(
//                    'action'        =>  StmFunctions::getSiteUrl() . '/plivo/answer/id/' . $callSession['call_list_id'] .'/num_to_call/'.$numToCall,
//                    'redirect'      =>  'true',
//                    'timeout'       =>  300,
//                ));
//
//            // Add speak to get digits to the user knows what to do
//            $getDigits->addSpeak('Press 1 to continue, or your session will end in five minutes.');
//
//            $this->_response->addSpeak('Input not received. Your session is ending. Goodbye.');
//
//            try {
//                $this->_sendResponse();
//            }
//            catch(Exception $e) {
//                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Hangup specific call had error: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
//            }

            //@todo / NOTE: IF double click prevention on GUI side doesn't work, can do a quick query to see if a call record with session & call_list_phone id exists with dial_b_leg_status of "hangup". If so, then call already hungup and we can ignore the hangup call and prevent dupe and further error log.


            // hangup specific call by call_id
            $response = Yii::app()->plivo->getPlivo()->hangup_call(array(
                'call_uuid' =>  $call['dial_b_leg_uuid'],
            ));

            if($response['status'] == 204) {
                $this->_sendJson(array('status'=>'success'));
            }
            else {

                //@todo: search all calls with this session_id and call_list_phone_id to see if it's already hung-up... check within the last 5 minutes???
                $hangupCall = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('calls c')
                    ->where('c.call_list_phone_id='.$call['call_list_phone_id'])
                    ->andWhere('c.call_session_id='.$call['call_session_id'])
                    ->andWhere('c.dial_b_leg_status="hangup"')
                    ->queryRow();

                //$backTrace = debug_backtrace();

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error hanging up call. Plivo Response: '.print_r($response, true).PHP_EOL.'B_UUID: '.$call['dial_b_leg_uuid'].PHP_EOL.' call_id: '.$id.PHP_EOL.'Hangup Call Data - If this exists, that means this was already hungup when trying to hangup again, possibly a double-click issue. Consider disabling button once clicked. '.print_r($hangupCall, true).PHP_EOL.'Backtrace: '.print_r($backTrace, true), CLogger::LEVEL_ERROR);
            }

            // either way mark as No Answer call_list_phone_id
            //@todo: see if there is a hangup callback that will increment the call_count or not. ... that is supposed to mark these as No Answer but may be having issues.
            Yii::app()->db->createCommand('UPDATE call_list_phones set status="'.CallListPhones::NO_ANSWER.'", last_called=NOW(), updated=NOW() WHERE id='.$call['call_list_phone_id'])->execute();
        }
        else {
            if(!YII_DEBUG) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not find call to hangup specific call.'.PHP_EOL.'UUID: '.$call['dial_b_leg_uuid'].PHP_EOL.' call_id: '.$id, CLogger::LEVEL_ERROR);
            }
        }
    }


    public function actionHangupCallListPhone($id)
    {
        $numToCall = Yii::app()->request->getParam('num_to_call') ? Yii::app()->request->getParam('num_to_call') : 3;
        $callerIdName = (Yii::app()->request->getParam('callerIdName')) ? Yii::app()->request->getParam('callerIdName') : Yii::app()->user->settings->dialer_caller_id_name;
        $callerIdNumber = (Yii::app()->request->getParam('callerIdNumber')) ? Yii::app()->request->getParam('callerIdNumber') : Yii::app()->user->settings->dialer_caller_id;

        // get active call session for user and grab call uuid from that ... there is no uuid when it's ringing and no one has picked up yet.
        // this checks for  just the session first. If it's the first call, then the other data doesn't exist and causes errors.
        $command = Yii::app()->db->createCommand()
            ->select('id, call_uuid, call_list_id')
            ->from('call_sessions s')
            ->where('s.contact_id='.Yii::app()->user->id)
            ->andWhere('s.end IS NULL')
            ->order('s.id DESC');

        $callSession = $command->queryRow();

        //Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI Hanging Up call... See what data exists for Call Session.'.PHP_EOL.'Call Session: '.print_r($callSession, true).PHP_EOL.' call_list_phone_id: '.$id.' Query: '.$command->getText(), CLogger::LEVEL_ERROR);

        if($callSession['call_uuid']) {

//            $command = Yii::app()->db->createCommand()
//                ->select('c.dial_b_leg_uuid')
//                ->from('calls c')
//                ->where('c.call_session_id='.$callSession['id'])
//                ->andWhere('c.call_list_phone_id='.$id);
//
//            $bleg = $command->queryRow();
            //Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI Hanging Up call Part 2... See what data exists for B LEG: '.print_r($bleg, true).PHP_EOL.' call_list_phone_id: '.$id.' Query: '.$command->getText(), CLogger::LEVEL_ERROR);

            // @todo: having trouble hanging up on a call that is RINGING only and doesn't have a b_leg_uuid assigned to it yet
            $response = Yii::app()->plivo->getPlivo()->transfer_call(array(
//                    'call_uuid' =>  ($bleg['dial_b_leg_uuid']) ? $bleg['dial_b_leg_uuid'] : $callSession['call_uuid'],
                'call_uuid' =>  $callSession['call_uuid'],
                'aleg_url' => StmFunctions::getSiteUrl() . '/plivo/transferReturnToAnswer?id=' . $callSession['call_list_id'].'&user_id='.Yii::app()->user->id, //todo: should this go to nodeUrl???
                'bleg_url' => StmFunctions::getSiteUrl() . '/plivo/transferHangup',
                'legs'      => 'both', //bleg
            ));
            if($response['status'] == 202) {

                //@todo: ACTIVITY LOG needs to get logged here if desired. Right now there is no logging so no attempt is visible.


                $this->_sendJson(array('status'=>'success'));
            }

            // hangup specific call by call_id .... @todo: transfer this to a url that hangs it up
//            $response = Yii::app()->plivo->getPlivo()->hangup_call(array(
//                    'call_uuid' =>  $callSession['call_uuid']
//                ));
//
//            if($response['status'] == 204) {
//                $this->_sendJson(array('status'=>'success'));
//            }
            else {

                //@todo: If we keep getting errors, you can query to see if a call record with same session and call_list_phone id and hangup status exists and this may indicate double-clicking error. See about disabling once clicked.

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error hanging up call. Plivo Response: '.print_r($response, true).PHP_EOL.'UUID: '.$callSession['call_uuid'].PHP_EOL.' call_list_phone_id: '.$id.' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
            }
        }
        else {

            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not find call to hangup a call.'.PHP_EOL.'UUID: '.$callSession['call_uuid'].' This may be from a session that already ended and the status "Ringing" is stuck for some reason.'.PHP_EOL.' call_list_phone_id: '.$id.' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Send JSON
     *
     * Outputs JSON data and terminates application
     * @param array $data Array of data
     */
    protected function _sendJson($data)
    {
        // Set header and output JSON data
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }

    /**
     * Send Response
     *
     * Send the XML response
     * @return void
     */
    protected function _sendResponse()
    {
        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $this->_response->toXML();

        // Terminate execution
        Yii::app()->end();
    }

    public function actionRecordings()
    {
        $model = new Calls();
        $this->render('recordings', array(
            'model' => $model
        ));

    }

    //special temporary action to remove dupes in call_list_phones... to be deleted!
    public function actionRemoveDupes()
    {
        echo 'Starting Dupe Removal.<br>';

        $dbName = Yii::app()->db->createCommand("select DATABASE()")->queryScalar();
        $date = date('y-m-d-H-i-s');
        $dbHost = (YII_DEBUG)? 'localhost' : STM_DB_BACKUP_HOST;
        exec('mkdir -p /var/log/stm/dialer');
        $filename = "$dbName-call_list_phones-$date";

        echo 'Dumping table clp.<br>';
        exec(((YII_DEBUG) ? 'mysqldump' : '/usr/bin/mysqldump')." -h$dbHost $dbName call_list_phones > /var/log/stm/dialer/$filename.sql");

        // finds all dupe time-based call_list_phones records
        $dupes = Yii::app()->db->createCommand("SELECT count(id) count, sum(call_count) dialed,id, phone_id, call_list_id FROM `call_list_phones` WHERE task_id IS NULL GROUP BY phone_id, call_list_id HAVING count(id) >1")->queryAll();

        echo count($dupes).' dupes found.<br>';

        foreach($dupes as $i => $dupe) {

            // get all dupe records order by id ASC
            $callListPhoneDupes = Yii::app()->db->createCommand("SELECT id FROM call_list_phones WHERE call_list_id=".$dupe['call_list_id']." AND phone_id=".$dupe['phone_id']." ORDER BY id ASC")->queryColumn();

            $dupeData = Yii::app()->db->createCommand("SELECT SUM(call_count) totalCallCount, MIN(is_deleted) isAllDeleted FROM call_list_phones WHERE call_list_id=".$dupe['call_list_id']." AND phone_id=".$dupe['phone_id']." ORDER BY id ASC")->queryRow();

            // grabs record from the last updated record to save that data for update, update_by, last_called, status
            $lastUpdatedRecord = Yii::app()->db->createCommand("SELECT status, last_called, updated, updated_by FROM call_list_phones WHERE call_list_id=".$dupe['call_list_id']." AND phone_id=".$dupe['phone_id']." ORDER BY updated DESC LIMIT 1")->queryRow();

            $mergeToId = $callListPhoneDupes[0];
            $mergeFromIdsString = implode(',',array_slice($callListPhoneDupes, 1));

            // update call_list_phone_id in calls
            $dbTransaction = Yii::app()->db->beginTransaction();
            try {
                // merge all into the first/earliest record - add the count into the 1st one and set the rest to zero, pick the highest last_updated and select that as the last_called, updated_by, updated value... can have done for you by using MAX() in sql query... check to see if they are all deleted?
                Yii::app()->db->createCommand("UPDATE call_list_phones clp SET status='".$lastUpdatedRecord['status']."', call_count=".$dupeData['totalCallCount'].", last_called='".$lastUpdatedRecord['last_called']."', updated='".$lastUpdatedRecord['updated']."', updated_by=".$lastUpdatedRecord['updated_by'].", is_deleted=".$dupeData['isAllDeleted']." WHERE clp.id={$mergeToId}")->execute();

                // updates all call_list_phone_id column in calls records to $mergeToId
                Yii::app()->db->createCommand("UPDATE calls c LEFT JOIN call_list_phones clp ON clp.id=c.call_list_phone_id LEFT JOIN call_lists l ON l.id=clp.call_list_id SET c.call_list_phone_id={$mergeToId} WHERE call_list_phone_id IN({$mergeFromIdsString})")->execute();

                // permanently delete ... OR option #2 is set to zero on the counts and soft delete
                Yii::app()->db->createCommand("DELETE FROM call_list_phones where id IN({$mergeFromIdsString})")->execute();

                $dbTransaction->commit();
            }
            catch (Exception $e) {
                $dbTransaction->rollback();
                echo 'Error in removing dupe. Error Message: '.$e->getMessage().'<br>Dupe Data: '.print_r($dupe, true).'<br>';
            }

            if($i%100 == 0) {
                echo 'Progress - '.$i.' of '.count($dupes).'.<br>';
            }
        }
        echo '<br>Completed Removing Dupes.';
    }
}