<?php
class FlashcardsController extends AdminController
{
    public $displayName = 'Flash Cards';

    /**
     * Action Index
     *
     * Default page load action
     * @return void
     */
    public function actionIndex()
    {
        $this->title = 'Flash Cards Topics';

        $model = new FlashCardTopics();
        $model->unsetAttributes();

        if(isset($_GET['FlashCardTopics'])) {

            $model->attributes = $_POST['FlashCardTopics'];
        }

        $this->render('list', array('model' => $model));
    }

    public function actionCards($id=null)
    {
        $this->title = 'Flash Cards List';

        $model = new FlashCards();
        $model->unsetAttributes();

        if(isset($_GET['FlashCards'])) {

            $model->attributes = $_GET['FlashCards'];
        }
        elseif($id) {
            $model->topicId = $id;
        }

        $this->render('listCards', array('model' => $model));
    }

    public function actionOrderTopicCards($id)
    {
        $this->title = 'Flash Cards Sort Order';
        $model = FlashCardTopics::model()->findByPk($id);
        if(Yii::app()->request->isAjaxRequest) {

            // save the sort order
            $items = $_POST['items'];
            foreach($items as $i => $flashCardId) {
                if($lookup = FlashCardTopicLu::model()->findByAttributes(array('flash_card_topic_id' => $id, 'flash_card_id' => $flashCardId))) {
                    $lookup->sort_order = $i+1;
                    if(!$lookup->save()) {
                        $x=1;
                    }
                }
            }

            Yii::app()->end();
        }

        $criteria=new CDbCriteria;
        $criteria->with = array('flashCardTopicLu');
        $criteria->order = 'flashCardTopicLu.sort_order';
        $criteria->compare('flashCardTopicLu.flash_card_topic_id',$id);

        $flashCards = FlashCards::model()->findAll($criteria);
        foreach($flashCards as $flashCard) {
            $listData[$flashCard->id] = '<a href="/'.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id.'/editCards/'.$flashCard->id.'">'.$flashCard->name.'<span>[Edit]</span></a>';
        }
        $this->render('orderTopicCards', array('model'=> $model,'listData' => $listData));
    }

    public function actionEditTopics($id)
    {
        $this->title = 'Edit Flash Card Topics';
        $model = FlashCardTopics::model()->findByPk($id);

        if(isset($_POST['FlashCardTopics'])) {

            $model->attributes = $_POST['FlashCardTopics'];
            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Updated Flash Card Topic.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id);
            }
            else {
                Yii::app()->user->setFlash('error', 'Error adding Flash Card Topic.');
            }
        }

        $this->render('formTopics', array(
                'model' => $model,
            ));
    }

    public function actionEditCards($id)
    {
        $this->title = 'Edit Flash Card';
        $model = FlashCards::model()->findByPk($id);
        $model->topicCollection = Yii::app()->db->createCommand("SELECT flash_card_topic_id FROM flash_card_topic_lu WHERE flash_card_id=".intval($id))->queryColumn();

        if(isset($_POST['FlashCards'])) {

            $model->topicCollection = array();//unset this or any unselect/empty will not work for multi select
            $model->attributes = $_POST['FlashCards'];
            if($model->save()) {

                $collectionFlipped = array_flip($model->topicCollection);
                foreach ($model->flashCardTopicLu as $flashCardLu) {
                    if (!in_array($flashCardLu->flash_card_topic_id, $model->topicCollection)) {
                        $flashCardLu->delete();
                    } else {
                        unset($collectionFlipped[$flashCardLu->flash_card_topic_id]);
                    }
                }

                if ($model->topicCollection) {
                    $model->topicCollection = array_flip($collectionFlipped);

                    // Attach the contact types to the newly contact
                    foreach ($model->topicCollection as $topicId) {
                        $flashCardTopicLu = new FlashCardTopicLu();
                        $flashCardTopicLu->flash_card_id = $model->id;
                        $flashCardTopicLu->flash_card_topic_id = $topicId;
                        $flashCardTopicLu->save();
                    }
                }

                Yii::app()->user->setFlash('success', 'Successfully Updated Flash Card Topic.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id.'/cards');
            }
            else {
                Yii::app()->user->setFlash('error', 'Error adding Flash Card.');
            }
        }

        $this->render('formCards', array(
                'model' => $model,
            ));
    }

    public function actionAddTopics()
    {
        $this->title = 'Add Flash Card Topics';
        $model = new FlashCardTopics();

        if(isset($_POST['FlashCardTopics'])) {

            $model->attributes = $_POST['FlashCardTopics'];
            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added Flash Card Topic.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id);
            }
            else {
                Yii::app()->user->setFlash('error', 'Error adding Flash Card Topic. '.current($model->getErrors())[0]);
            }
        }
        $this->render('formTopics', array(
                'model' => $model,
        ));
    }

    public function actionAddCards($id=null)
    {
        $this->title = 'Add Flash Cards';
        $model = new FlashCards();

        if($id) {
            $model->topicCollection = array($id);
        }

        if(isset($_POST['FlashCards'])) {

            $model->attributes = $_POST['FlashCards'];
            if($model->save()) {
                foreach ($model->topicCollection as $topicId) {
                    $flashCardTopicLu = new FlashCardTopicLu();
                    $flashCardTopicLu->flash_card_id = $model->id;
                    $flashCardTopicLu->flash_card_topic_id = $topicId;
                    $flashCardTopicLu->save();
                }

                Yii::app()->user->setFlash('success', 'Successfully added Flash Cards.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id);
            }
            else {
                Yii::app()->user->setFlash('error', 'Error adding Flash Card. Message: '.$model->getErrors()[0]);
            }
        }
        $this->render('formCards', array(
            'model' => $model,
        ));
    }

    public function actionStudy($id)
    {
        $this->title = 'Study Flash Card Topics';
        $criteria = new CDbCriteria();
        $criteria->with = array('flashCardTopics','flashCardTopicLu');
        $criteria->compare('flashCardTopics.id', $id);
        $criteria->order = 'flashCardTopicLu.sort_order ASC';
        $flashCards = FlashCards::model()->findAll($criteria);

        $this->render('study', array(
                'flashCards' => $flashCards,
                'topic' =>FlashCardTopics::model()->findByPk($id)
            ));
    }
    public function printTopics($topics)
    {
        if(empty($topics)) {
            return null;
        }
        $string = '';
        foreach($topics as $topic) {
            $string .= ($string) ? ', ': '';
            $string .= '<a href="/'.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id.'/cards/'.$topic->id.'">'.$topic->name.'</a>';
        }
        return $string;
    }
}