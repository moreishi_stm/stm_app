<?php
class CompanyTypesController extends AdminController
{
	public $pageColor = 'purple';

    public function actions()
    {
    	return array(
			'index' =>'stm_app.modules.admin.controllers.actions.companyTypes.ListAction',
			'edit'  =>'stm_app.modules.admin.controllers.actions.companyTypes.EditAction',
			'view'  =>'stm_app.modules.admin.controllers.actions.companyTypes.EditAction',
			'add'   =>'stm_app.modules.admin.controllers.actions.companyTypes.AddAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new CompanyTypes;
		$this->displayName = 'Company Types';
	}
}