<?php

/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class BucketController extends AdminController
{

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return array
     */
    public function actions()
    {

        $bucketActionDirAlias = 'admin_module.controllers.actions.bucket';

        return array(
            'index'  => $bucketActionDirAlias . '.IndexAction',
            'add'    => $bucketActionDirAlias . '.AddAction',
            'edit'   => $bucketActionDirAlias . '.EditAction',
            'delete' => $bucketActionDirAlias . '.DeleteAction',
        );
    }
}