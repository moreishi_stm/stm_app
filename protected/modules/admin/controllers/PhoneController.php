<?php
Yii::import('admin_module.components.StmTwilio.twilio-php.Services.Twilio.Capability', true);

/**
 * Phone Controller
 *
 * Provides in-browser telephone functionality
 */
class PhoneController extends AdminController
{
    /**
     * Send JSON
     *
     * Outputs JSON data and terminates application
     * @param array $data Array of data
     */
    protected function _sendJson($data)
    {
        // Set header and output JSON data
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }

    /**
     * Action Index
     *
     * Default page load action
     * @return void
     */
    public function actionDial()
    {
//        $this->redirect(array('numbers'));
//        Yii::app()->end();

        if(YII_DEBUG || strpos($_SERVER['SERVER_NAME'], 'christineleeteam') !== false) {
            // Render the view
            $this->render('index', array());
        }
    }

    /**
     * Action Generate Token
     *
     * Generates an API access token for the browser to use for placing a call
     * @return void
     */
    public function actionGeneratetoken()
    {
        // Prepare token generation
        $capability = new Services_Twilio_Capability('AC35d8934c5baa6c3efbd2a385f66129e4', 'c2b8598778253c1073833c95561007bc');
        $capability->allowClientOutgoing(YII_DEBUG ? 'APca36c3cd123e1875d60d06395cfb73cf' : 'APadf38643641936d6c833d5fc66d8b70c');  // Nicole's Test Application for development

        // Generate a token that will expire in 10 seconds (we only need this to place the call)
        $token = $capability->generateToken();

        // Send the token down the wire via JSON
        $this->_sendJson(array(
            'token' =>  $token
        ));
    }

    public function actionIndex()
    {
        $this->redirect(array('numbers'));
    }

    public function actionNumbers()
    {
        Yii::import('admin_widgets.DateRanger.DateRanger');
        $this->title = 'My Phone Numbers';

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'last_30_days';
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId));
        $model = new TelephonyPhones();
        $model->client_account_id = $clientAccount->id;

        // row data
        $callCountSubQuery = "(select count(s.id) FROM call_hunt_group_sessions s WHERE s.to_number = CONCAT('1',t.phone) AND DATE(s.added) >= '".$dateRange['from_date']."' AND DATE(s.added) <='".$dateRange['to_date']."')";
        $callAnsweredCountSubQuery = "(select count(ca.id) FROM call_hunt_group_session_calls ca JOIN call_hunt_group_sessions s2 ON s2.id=ca.call_hunt_group_session_id WHERE status='Accepted' AND s2.to_number=CONCAT('1',t.phone) AND DATE(ca.added) >= '".$dateRange['from_date']."' AND DATE(ca.added) <='".$dateRange['to_date']."')";
        $callLogCountSubQuery = "(select count(DISTINCT a.component_id) FROM activity_log a JOIN call_hunt_group_sessions s3 ON s3.id=a.component_id WHERE a.component_type_id=".ComponentTypes::DIRECT_INCOMING_CALLS." AND s3.to_number=CONCAT('1',t.phone) AND DATE(s3.added) >= '".$dateRange['from_date']."' AND DATE(s3.added) <='".$dateRange['to_date']."')";

        $rawDataCommand = Yii::app()->db->createCommand()
            ->select('t.id, t.call_hunt_group_id, t.contact_id, t.phone, t.is_priority, t.user_description, t.user_notes, CONCAT(c.first_name," ",c.last_name) fullName, '.$callCountSubQuery.' call_count, '.$callAnsweredCountSubQuery.' call_answered_count, ROUND(100 * '.$callAnsweredCountSubQuery.'/'.$callCountSubQuery.') call_answered_percent, '.$callLogCountSubQuery.' call_log_count, ROUND(100 * '.$callLogCountSubQuery.'/'.$callCountSubQuery.') call_log_percent')
            ->from('stm_hq.telephony_phones t')
            ->leftJoin("contacts c", "c.id=t.contact_id")
            ->leftJoin("call_hunt_groups g", "g.id=t.call_hunt_group_id")
            ->where("t.client_account_id=:clientAccountId", array(':clientAccountId' => $clientAccount->id));

        if($_GET['TelephonyPhones']['phone']) {
            $rawDataCommand->andWhere('t.phone like :phone', array(':phone' => '%'.Yii::app()->format->formatInteger($_GET['TelephonyPhones']['phone']).'%'));
        }

        if($_GET['TelephonyPhones']['user_description']) {
            $rawDataCommand->andWhere('t.user_description like :userDescription', array(':userDescription' => '%'.$_GET['TelephonyPhones']['user_description'].'%'));
        }
        if($_GET['TelephonyPhones']['isDedicated']) {
            $rawDataCommand->andWhere('t.contact_id IS NOT NULL');
        }

        if($_GET['TelephonyPhones']['call_hunt_group_id']) {
            $rawDataCommand->andWhere('t.call_hunt_group_id = :callHuntGroupId', array(':callHuntGroupId' => $_GET['TelephonyPhones']['call_hunt_group_id']));
        }

        if($_GET['TelephonyPhones']['tagCollection'] && !empty($_GET['TelephonyPhones']['tagCollection']) && is_array($_GET['TelephonyPhones']['tagCollection'])) {
            $rawDataCommand
                ->join('telephony_phone_tag_lu lu','lu.telephony_phone_id=t.id')
                ->join('telephony_phone_tags tag','tag.id=lu.telephony_phone_tag_id')
                ->andWhere(array('in','tag.id', $_GET['TelephonyPhones']['tagCollection']));

            if(!empty($_GET['TelephonyPhones']['tagCollection']) && is_array($_GET['TelephonyPhones']['tagCollection'])) {
                foreach($_GET['TelephonyPhones']['tagCollection'] as $tagId) {
                    if(is_numeric($tagId)) {
                        $tagIds[] = $tagId;
                    }
                }
                $tagCondition = 'AND tag.id IN('.implode(',', $tagIds).')';
                $tag2Condition = 'AND tag2.id IN('.implode(',', $tagIds).')';
                $tag3Condition = 'AND tag3.id IN('.implode(',', $tagIds).')';
            }
            $countTagJoin = 'JOIN stm_hq.telephony_phones t ON t.phone=SUBSTRING(s.to_number,2) JOIN telephony_phone_tag_lu lu ON lu.telephony_phone_id=t.id JOIN telephony_phone_tags tag ON tag.id=lu.telephony_phone_tag_id';
            $summaryCallCountSubQuery = "(select count(s.id) FROM call_hunt_group_sessions s {$countTagJoin} WHERE DATE(s.added) >= '".$dateRange['from_date']."' AND DATE(s.added) <='".$dateRange['to_date']."' {$tagCondition} AND s.exclude_from_stats=0)";

            $countAnsweredTagJoin = 'JOIN call_hunt_group_sessions s2 ON s2.id=ca.call_hunt_group_session_id JOIN stm_hq.telephony_phones t2 ON t2.phone=SUBSTRING(s2.to_number,2) JOIN telephony_phone_tag_lu lu2 ON lu2.telephony_phone_id=t2.id JOIN telephony_phone_tags tag2 ON tag2.id=lu2.telephony_phone_tag_id';
            $summaryCallAnsweredCountSubQuery = "(select count(ca.id) FROM call_hunt_group_session_calls ca {$countAnsweredTagJoin} WHERE ca.status='Accepted' AND DATE(ca.added) >= '".$dateRange['from_date']."' AND DATE(ca.added) <='".$dateRange['to_date']."' {$tag2Condition} AND s2.exclude_from_stats=0)";


            $countLogTagJoin = 'JOIN stm_hq.telephony_phones t3 ON t3.phone=SUBSTRING(s3.to_number,2) JOIN telephony_phone_tag_lu lu3 ON lu3.telephony_phone_id=t3.id JOIN telephony_phone_tags tag3 ON tag3.id=lu3.telephony_phone_tag_id';
            $summaryCallLogCountSubQuery = "(select count(DISTINCT a.component_id) FROM activity_log a JOIN call_hunt_group_sessions s3 ON s3.id=a.component_id {$countLogTagJoin} WHERE a.component_type_id=".ComponentTypes::DIRECT_INCOMING_CALLS." AND s3.to_number=CONCAT('1',t3.phone) AND DATE(a.activity_date) >= '".$dateRange['from_date']."' AND DATE(a.activity_date) <='".$dateRange['to_date']."' {$tag3Condition} AND s3.exclude_from_stats=0)";
        }
        else {

            //@todo: use joins to exclude - s.exclude_from_stats=0
            $summaryCallCountSubQuery = "(SELECT count(s.id) FROM call_hunt_group_sessions s WHERE DATE(s.added) >= '".$dateRange['from_date']."' AND DATE(s.added) <='".$dateRange['to_date']."' AND s.exclude_from_stats=0)";
            $summaryCallAnsweredCountSubQuery = "(SELECT count(ca.id) FROM call_hunt_group_session_calls ca JOIN call_hunt_group_sessions s2 ON s2.id=ca.call_hunt_group_session_id WHERE status='Accepted' AND DATE(ca.added) >= '".$dateRange['from_date']."' AND DATE(ca.added) <='".$dateRange['to_date']."' AND s2.exclude_from_stats=0)";
            $summaryCallLogCountSubQuery = "(SELECT count(DISTINCT a.component_id) FROM activity_log a JOIN call_hunt_group_sessions s3 ON s3.id=a.component_id WHERE a.component_type_id=".ComponentTypes::DIRECT_INCOMING_CALLS." AND DATE(a.activity_date) >= '".$dateRange['from_date']."' AND DATE(a.activity_date) <='".$dateRange['to_date']."' AND s3.exclude_from_stats=0)";
        }

        // summary / footer data
        $summaryData = Yii::app()->db->createCommand("select $summaryCallCountSubQuery summaryCallCount, $summaryCallAnsweredCountSubQuery summaryCallAnswered, $summaryCallLogCountSubQuery summaryLogCount")->queryRow();
        $summaryData['summaryAnsweredPercent'] = ($summaryData['summaryCallCount']) ? round(100 * $summaryData['summaryCallAnswered'] /$summaryData['summaryCallCount']) : '';
        $summaryData['summaryLogPercent'] = ($summaryData['summaryLogCount']) ? round(100 * $summaryData['summaryLogCount'] /$summaryData['summaryCallCount']) : '';

        $rawData = $rawDataCommand->queryAll();

        $sort = new CSort();
        $sort->defaultOrder = 'is_priority DESC, call_count DESC';
        $sort->attributes = array(
            'name'=>array(
                'desc'=>'user_description DESC',
                'asc'=> 'user_description ASC',
                'label' => 'Name',
            ),
            'call_count'=>array(
                'desc'=>'call_count DESC',
                'asc'=> 'call_count ASC',
                'label' => 'Calls',
            ),
            'call_answered_count'=>array(
                'desc'=>'call_answered_count DESC',
                'asc'=> 'call_answered_count ASC',
                'label' => 'Answered',
            ),
            'call_answered_percent'=>array(
                'desc'=>'call_answered_percent DESC',
                'asc'=> 'call_answered_percent ASC',
                'label' => '%',
            ),
        );

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        $dataProvider->sort = $sort;


        // Render the view
        $this->render('numbers', array(
                'model' => $model,
                'summaryData' => $summaryData,
                'dataProvider' => $dataProvider,
            ));
    }

    public function actionEditNumbers($id)
    {
        $this->title = 'Edit Phone Number';

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId));
        $model = TelephonyPhones::model()->findByAttributes(array('id'=>$id, 'client_account_id'=>$clientAccount->id));

        if (isset($_POST['TelephonyPhones'])) {

            $model->attributes = $_POST['TelephonyPhones'];

            $model->setScenario('editForm');

            if ($model->save()) {
                //process tags
                if(is_array($model->tagCollection) && !empty($model->tagCollection)) {
                    TelephonyPhoneTagLu::model()->deleteAllByAttributes(array('telephony_phone_id' => $model->id));
                    foreach($model->tagCollection as $tagIdToAdd) {
                        $tag = new TelephonyPhoneTagLu();
                        $tag->setAttributes(array('telephony_phone_id' => $model->id, 'telephony_phone_tag_id' => $tagIdToAdd));
                        if(!$tag->save()) {
                            //@todo: error log? - Should we put this in a transaction?
                            throw new Exception(__CLASS__.''.__LINE__.'): Error saving Phone Tag. Error Message: '.print_r($tag->getErrors(), true));
                        }
                    }
                }

                Yii::app()->user->setFlash('success', 'Successfully updated Phone Number.');
                $this->redirect(array('numbers'));
            }
        }

        $this->render('formNumbers', array(
                'model' => $model,
            ));
    }

    public function actionCallHistory($id=null)
    {
        Yii::import('admin_widgets.DateRanger.DateRanger');
        $this->title = 'Call History';

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId));
        $model = ($id) ? TelephonyPhones::model()->findByPk($id) : new TelephonyPhones();

        $DateRanger = new DateRanger();
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);
        $DateRanger->defaultSelect = 'last_30_days';

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId));
        $phoneResult = Yii::app()->stm_hq->createCommand("SELECT id, phone, user_description FROM telephony_phones WHERE client_account_id='".$clientAccount->id."' ORDER BY user_description")->queryAll();
        $phoneListData = array();
        foreach($phoneResult as $phoneRow) {
            $phoneListData[$phoneRow['phone']] = $phoneRow['user_description'].' - '. Yii::app()->format->formatPhone($phoneRow['phone']);
        }

        $statusSubQuery = "(select IF(count(sc.id) >= 1, 1, 0) status FROM call_hunt_group_session_calls sc WHERE s.id=sc.call_hunt_group_session_id AND sc.status='Accepted')";
        $answeredBySubQuery = "(select IF(count(sc.id) > 0, CONCAT(c.first_name,' ',c.last_name), NULL) FROM call_hunt_group_session_calls sc JOIN setting_contact_values v ON v.setting_id=".Settings::SETTING_CELL_NUMBER." AND v.value=SUBSTRING(sc.dial_b_leg_to,2) JOIN contacts c ON v.contact_id=c.id WHERE s.id=sc.call_hunt_group_session_id AND sc.status='Accepted')";
        $followupCountSubQuery = "(select count(a.id) FROM activity_log a WHERE a.component_type_id=".ComponentTypes::DIRECT_INCOMING_CALLS." AND a.component_id=s.id)";

        $rawDataCommand = Yii::app()->db->createCommand()
            ->select('s.id, s.from_number, s.to_number, s.added, IF('.$statusSubQuery.'>0, "Answered", "Missed") status, s.recording_url, s.exclude_from_stats, '.$answeredBySubQuery.' answeredBy, '.$followupCountSubQuery.' followupCount, p.id phone_id, vm.record_url vm_url') //, '.$contactMatchesSubQuery.' contactMatches
            ->from('call_hunt_group_sessions s')
            ->leftJoin("phones p", "p.phone=SUBSTRING(s.from_number, 2) AND p.is_deleted=0")
            ->leftJoin("call_hunt_group_session_voicemail_messages vmlu", "vmlu.call_hunt_group_session_id=s.id") //callHuntGroupSessionVoicemailMessages
            ->leftJoin("voicemail_messages vm", "vm.id=vmlu.voicemail_message_id")
            ->group('s.id')
            ->order('s.added')
//            ->leftJoin("call_hunt_groups g", "g.id=t.call_hunt_group_id")
            ->where("DATE(s.added) >=:fromDate AND DATE(s.added) <=:toDate", array(':fromDate' => $dateRange['from_date'], ':toDate' => $dateRange['to_date']));
//            ->limit(100);

        if($_GET['TelephonyPhones']['phone'] && !empty($_GET['TelephonyPhones']['phone'])) {
            $rawDataCommand->andWhere('s.to_number = :toNumber ', array(':toNumber' => '1'.$_GET['TelephonyPhones']['phone']));
        }
        elseif($id && !isset($_GET['TelephonyPhones']['phone'])) {
            $rawDataCommand->andWhere('s.to_number = :toNumber ', array(':toNumber' => '1'.$model->phone));
        }

        if(is_numeric($_GET['status'])) {
            $rawDataCommand->andWhere($statusSubQuery.'=:status', array(':status' => $_GET['status']));
        }

        if($_GET['fromNumber']) {
            $rawDataCommand->andWhere('s.from_number like :fromNumber', array(':fromNumber' => '%'.$_GET['fromNumber'].'%'));
        }

        $sort = new CSort();
        $sort->defaultOrder = 'added DESC';
        $sort->attributes = array(
            'fromNumber'=>array(
                'desc'=>'from_number DESC',
                'asc'=> 'from_number ASC',
                'label' => 'From Number',
            ),
            'toNumber'=>array(
                'desc'=>'to_number DESC',
                'asc'=> 'to_number ASC',
                'label' => 'STM Number',
            ),
            'status'=>array(
                'desc'=>'status DESC',
                'asc'=> 'status ASC',
                'label' => 'Status<br>(Answered by)',
            ),
            'called'=>array(
                'desc'=>'added DESC',
                'asc'=> 'added ASC',
                'label' => 'Date',
            ),
        );

//        if($_GET['page'] >1) {
//            $rawDataCommand->offset(($_GET['page']-1)*100);
//        }

        $rawData = $rawDataCommand->queryAll();
//        $rawDataCommand->setText('')->select('count(s.id)')->group('');
//        $dataCount =  $rawDataCommand->queryScalar();

        $summaryData['calls_count'] = count($rawData);

        $answeredCommand = clone $rawDataCommand;
        $summaryData['answeredCount'] = $answeredCommand
            ->select('count(s.id)')
            ->andWhere($statusSubQuery.'>0 AND s.exclude_from_stats=0')
            ->setText('')
            ->group('')
            ->queryScalar();

        $summaryData['answeredPercent'] = (count($rawData)) ? round(100 * $summaryData['answeredCount'] / count($rawData)).'%' : '';

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        $dataProvider->sort = $sort;

//        $dataProvider->setTotalItemCount($dataCount);
//        $dataProvider->pagination->setItemCount($dataCount);
//        $dataProvider->setData($rawData);

        $this->render('callHistory', array(
                'model' => $model,
                'phoneListData' => $phoneListData,
                'summaryData' => $summaryData,
                'dataProvider' => $dataProvider,
            ));
    }

    public function printCallHistoryReportAction($data)
    {
        $contentString = StmFormHelper::getBooleanImage(!$data["exclude_from_stats"],"report-status-image-".$data["id"]).' ';
        //$contentString .= "<span class=\"report-status-action-text\" style=\"\">".(($data["ignore"]) ? "[+ Include]" : "[- Exclude]")."</span>";
        $contentString .= "<div class=\"report-status-action-button report-status-text-".$data["id"]."\">&#9660;";//report-status-text
        $contentString .= "<div class=\"call-report-status-dialog-button report-status-action-text ".(($data["exclude_from_stats"]) ? "include-call-reporting" : "exclude-call-reporting")."\" data-id=\"".$data["id"]."\">".(($data["exclude_from_stats"]) ? "Include in Report" : "Exclude from Report")."</div>";
        $contentString .= "</div>";
        //$contentString .= "<a href=\"javascript:void(0)\" class=\"report-status-text report-status-text-".$data["id"]."\">".(($data["ignore"]) ? "Include" : "Exclude")."</a>";
        return $contentString;
    }

    public function actionEditCallReportingStatus()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new Exception('Invalid access method.');
        }

        $model = new CallReportingStatusForm();
        $model->attributes = $_POST['CallReportingStatusForm'];
        $results = CActiveForm::validate($model);

        if($results != '[]') {
            echo $results;
            Yii::app()->end();
        }

        if (isset($_POST['CallReportingStatusForm'])) {

            $model->attributes = $_POST['CallReportingStatusForm'];

            // change the call reporting status in session
            $callHuntGroupSession = CallHuntGroupSessions::model()->findByPk($model->call_hunt_group_session_id);
            $callHuntGroupSession->exclude_from_stats = $model->exclude_from_stats;
            $callHuntGroupSession->exclude_reason = $model->exclude_reason;
            $callHuntGroupSession->exclude_reason_other = $model->exclude_reason_other;
            if($callHuntGroupSession->save()) {

                // add Activity Log
                $activityLog = new ActivityLog();
                $activityLog->setAttributes(array(
                        'component_type_id' => ComponentTypes::DIRECT_INCOMING_CALLS,
                        'component_id' => $model->call_hunt_group_session_id,
                        'task_type_id' => $model->task_type_id,
                        'note' => $model->notes,
                        'activity_date' => new CDbExpression('NOW()'),
                        'added' => new CDbExpression('NOW()'),
                    ));
                if(!$activityLog->save()) {
                    Yii::log(__CLASS__.' (:'.__LINE__.') Activity Log did not save for status change in reporting for a call. Errors: '.print_r($activityLog->getErrors(), true), CLogger::LEVEL_ERROR);
                }

                echo CJSON::encode(array('actionType' => ($model->task_type_id == TaskTypes::INCLUDE_CALL_REPORTING) ? 'include' : 'exclude',
                                         'status' => 'success',
                                         'id' => $model->call_hunt_group_session_id,
                    ));
            }
        }

        Yii::app()->end();

    }

    public function actionLeadCallHistory()
    {
        Yii::import('admin_widgets.DateRanger.DateRanger');
        $this->title = 'Lead Connect Call History';

//        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId));
//        $model = ($id) ? TelephonyPhones::model()->findByPk($id) : new TelephonyPhones();

        $DateRanger = new DateRanger();
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $statusSubQuery = "(select IF(count(sc.id) >= 1, 1, 0) status FROM lead_call_session_phones sc WHERE s.id=sc.lead_call_session_id AND sc.status='Answered - Confirmed')";
        $answeredBySubQuery = "(select IF(count(sc.id) > 0, CONCAT(c.first_name,' ',c.last_name), NULL) FROM lead_call_session_phones sc JOIN setting_contact_values v ON v.setting_id=".Settings::SETTING_CELL_NUMBER." AND v.value=SUBSTRING(sc.phone_number,2) JOIN contacts c ON v.contact_id=c.id WHERE s.id=sc.lead_call_session_id AND sc.status='Answered - Confirmed')";

        $rawDataCommand = Yii::app()->db->createCommand()
            ->select('s.component_type_id,
                    SUBSTRING(ct.display_name, 1, CHAR_LENGTH(ct.display_name) - 1) componentDisplayName,
                    ct.name component_name,
                    s.component_id,
                    CONCAT(c.first_name," ",c.last_name) fullName,
                    s.added,
                    IF('.$statusSubQuery.'>0, "Answered", "Missed") status,
                    s.recording_url,
                    '.$answeredBySubQuery.' answeredBy') //, '.$contactMatchesSubQuery.' contactMatches
            ->from('lead_call_sessions s')
            ->join('component_types ct','ct.id=s.component_type_id')
            ->join('transactions t','t.id=s.component_id')
            ->join('contacts c','c.id=t.contact_id')
            ->group('s.id')
            ->order('s.added')
            //            ->leftJoin("call_hunt_groups g", "g.id=t.call_hunt_group_id")
            ->where("DATE(s.added) >=:fromDate AND DATE(s.added) <=:toDate", array(':fromDate' => $dateRange['from_date'], ':toDate' => $dateRange['to_date']));
        //            ->limit(100);

        if($_GET['status'] || $_GET['status']==='0') {
            $rawDataCommand->andWhere($statusSubQuery.'=:status', array(':status' => $_GET['status']));
        }

        $sort = new CSort();
        $sort->defaultOrder = 'added ASC';
        $sort->attributes = array(
            'status'=>array(
                'desc'=>'status DESC',
                'asc'=> 'status ASC',
                'label' => 'Status',
            ),
            'called'=>array(
                'desc'=>'added DESC',
                'asc'=> 'added ASC',
                'label' => 'Date',
            ),
        );
//
//        //        if($_GET['page'] >1) {
//        //            $rawDataCommand->offset(($_GET['page']-1)*100);
//        //        }
//
        $rawData = $rawDataCommand->queryAll();
//        //        $rawDataCommand->setText('')->select('count(s.id)')->group('');
//        //        $dataCount =  $rawDataCommand->queryScalar();

        $summaryData['calls_count'] = count($rawData);

        $answeredCommand = clone $rawDataCommand;
        $summaryData['answeredCount'] = $answeredCommand
            ->select('count(s.id)')
            ->andWhere($statusSubQuery.'>0')
            ->setText('')
            ->group('')
            ->queryScalar();

        $summaryData['answeredPercent'] = (count($rawData)) ? round(100 * $summaryData['answeredCount'] / count($rawData)).'%' : '';

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        $dataProvider->sort = $sort;
//
//        //        $dataProvider->setTotalItemCount($dataCount);
//        //        $dataProvider->pagination->setItemCount($dataCount);
//        //        $dataProvider->setData($rawData);

        $this->render('leadCallHistory', array(
//                'model' => $model,
                'summaryData' => $summaryData,
                'dataProvider' => $dataProvider,
            ));
    }

    public function printContactMatches($data)
    {
        if(empty($data['phone_id'])) {
           return '';
        }

        $string = '';

        $contactsData = Yii::app()->db->createCommand()
            ->select('c.id contact_id, CONCAT(c.first_name," ",c.last_name) fullName')
            ->from('phones p')
            ->where('p.phone=:phone', array(':phone'=>substr($data['from_number'], 1)))
            ->andWhere('p.is_deleted=0')
            ->join('contacts c','c.id=p.contact_id')
            ->group('p.contact_id')
//            ->andWhere("DATE(sc.added) >=:fromDate AND DATE(sc.added) <=:toDate", array(':fromDate' => $dateRange['from_date'], ':toDate' => $dateRange['to_date']))
            ->queryAll();

        foreach($contactsData as $contactData) {
            $string .= ($string) ? '<br>' : '';
            $string .= '<a href="/admin/contacts/'.$contactData['contact_id'].'" target="_blank">'.$contactData['fullName'].'</a>';
        }

        return $string;
    }

    public function printStatus($data)
    {
        if(empty($data['status'])) {
            return '';
        }
        $color = ($data['status'] == 'Answered')? '#2EBF35' : '#D20000';

        $string = '<span style="color: '.$color.'; font-weight: bold;">'.$data['status'].'</span>';

        return $string;
    }

    public function printFollowUpStatus($data)
    {
        if($data['status'] == 'Answered') {
            return '-';
        }

        if($data['followupCount'] > 0) {
            $color = '#2EBF35';
            $followUpStatus = 'Yes';
        }
        else {
            $color = '#D20000';
            $followUpStatus = 'No';
        }

        $string = '<span style="color: '.$color.'; font-weight: bold;">'.$followUpStatus.'</span>';

        return $string;
    }

    public function printFollowupNotes($data)
    {
        if($data['followupCount'] > 0) {
            $logs = Yii::app()->db->createCommand("select a.activity_date, a.note, a.added_by, CONCAT(c.first_name, ' ', c.last_name) fullName FROM activity_log a JOIN contacts c ON c.id=a.added_by where component_type_id=".ComponentTypes::DIRECT_INCOMING_CALLS." AND component_id=".$data['id'])->queryAll();
            foreach($logs as $log) {
                $string .= ($string) ? '<br>' : '';
                $string .= Yii::app()->format->formatDateTime($log['activity_date']).' - '.$log['note'].'<br>&nbsp;&nbsp;&nbsp;&nbsp;(By: '.$log['fullName'].')';
            }
        }
        return $string;
    }

    public function actionTags()
    {
        $this->title = 'Phone Tags';

        $model = new TelephonyPhoneTags();
        $model->unsetAttributes();

        if(isset($_GET['TelephonyPhoneTags'])) {

            $model->attributes = $_POST['TelephonyPhoneTags'];
        }
        $this->render('listTags', array(
                'model' => $model,
            ));
    }

    public function actionEditTags($id)
    {
        $this->title = 'Edit Phone Tags';
        $model = TelephonyPhoneTags::model()->findByPk($id);

        if(isset($_POST['TelephonyPhoneTags'])) {

            $model->attributes = $_POST['TelephonyPhoneTags'];
            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated Phone Tag.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/phone/tags');
            }
            else {
                Yii::app()->user->setFlash('error', 'Error adding Phone tag.');
            }
        }
        $this->render('formTags', array(
                'model' => $model,
            ));
    }

    public function actionAddTags()
    {
        $this->title = 'Add Phone Tags';
        $model = new TelephonyPhoneTags();

        if(isset($_POST['TelephonyPhoneTags'])) {

            $model->attributes = $_POST['TelephonyPhoneTags'];
            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added Phone Tag.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/phone/tags');
            }
            else {
                Yii::app()->user->setFlash('error', 'Error adding Phone tag.');
            }
        }
        $this->render('formTags', array(
                'model' => $model,
            ));
    }
}