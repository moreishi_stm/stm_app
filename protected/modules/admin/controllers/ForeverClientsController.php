<?php
class ForeverClientsController extends AdminController
{

    public $pageColor = 'orange';

    public $CachedComponentType;

    const PAGE_SIZE = 50;

    /**
     * @return array action filters
     */
    public function filters()
    {

        return CMap::mergeArray(
            parent::filters(), array()
        );
    }

    public function actionIndex()
    {
        $this->title = 'Forever Clients';

        $data = Closings::getForeverClients($_GET, Closings::FOREVER_CLIENT_RETURN_TYPE_DATA);
        $dataProvider = new CArrayDataProvider($data['leads'], array(
            'pagination'=>array(
                'pageSize'=> self::PAGE_SIZE,
            ),
        ));

        $dataProvider->setTotalItemCount($data['count']);
        $dataProvider->pagination->setItemCount($data['count']);
        $dataProvider->setData($data['leads']);

        if($_GET['page']) {
            $end =  (count($data['leads']) < self::PAGE_SIZE) ? (($_GET['page'] - 1) * self::PAGE_SIZE) + count($data['leads']) : $_GET['page'] * self::PAGE_SIZE;
        }
        else {
            $end = ($data['count'] < self::PAGE_SIZE)? $data['count'] : self::PAGE_SIZE;
        }

        if(isset($_GET['export']) && $_GET['export']==1){
            $contacts = Closings::getForeverClients($_GET, Closings::FOREVER_CLIENT_RETURN_TYPE_CONTACT_MODELS);
            $exportData = Contacts::model()->getExportData($contacts);
            $date = date('Y-m-d');
            header( "Content-Type: application/vnd.ms-excel; charset=utf-8" );
            header( "Content-Disposition: inline; filename=\"forever_clients_export_$date.xls\"" );
            echo $exportData;
            Yii::app()->end();
        }

        if($_GET['dialerProcess'] && $_GET['dialerAction'] && $_GET['dialerListId'] && Yii::app()->request->isAjaxRequest) {

            $this->_dialerListActions($_GET['dialerAction'], $_GET['dialerListId']);
        }

        if(Yii::app()->user->checkAccess('foreverClientEmails')) {
            $emailBlast = new EmailBlasts();
            $emailBlast->sender_name = Yii::app()->user->contact->fullName;
            $criteria = new CDbCriteria();
            $emailContactIds = array(Yii::app()->user->id);
            if($leadPoolId = Yii::app()->user->settings->lead_pool_contact_id) {
                array_push($emailContactIds, $leadPoolId);
            }
            $criteria->addInCondition('contact_id', $emailContactIds);
            $emailModels = Emails::model()->findAll($criteria);
        }

        $this->render('list', array(
                'dataProvider' => $dataProvider,
                'count'=> $data['count'],
                'start'=> ($_GET['page']) ? $_GET['page'] * self::PAGE_SIZE - (self::PAGE_SIZE - 1) : 1,
                'end'  => $end,
                'emailBlast' => $emailBlast,
                'emailModels' => $emailModels,
            )
        );
    }

    public function actionScheduleEmailBlast()
    {
        // check to see if there's already one scheduled
        $criteria = new CDbCriteria();
        $criteria->addCondition('DATE(send_datetime)="'.date('Y-m-d', strtotime($_POST['EmailBlasts']['send_datetime'])).'"');
        if($existingEmailBlast = EmailBlasts::model()->find($criteria)) {

            echo CJSON::encode(array('status'=>'error','message'=>'An email blast already exists for this date. Limit 1 email blast per day.'));
            Yii::app()->end();
        }

        $officeName = Yii::app()->db->createCommand("SELECT value FROM setting_account_values WHERE setting_id=".Settings::OFFICE_NAME_ID)->queryScalar();
        $officeStreetAddress = Yii::app()->db->createCommand("SELECT value FROM setting_account_values WHERE setting_id=".Settings::OFFICE_ADDRESS_ID)->queryScalar();
        $officeCity = Yii::app()->db->createCommand("SELECT value FROM setting_account_values WHERE setting_id=".Settings::OFFICE_CITY_ID)->queryScalar();
        $officeState = AddressStates::getShortNameById(Yii::app()->db->createCommand("SELECT value FROM setting_account_values WHERE setting_id=".Settings::OFFICE_STATE_ID)->queryScalar());
        $officeZip = Yii::app()->db->createCommand("SELECT value FROM setting_account_values WHERE setting_id=".Settings::OFFICE_ZIP_ID)->queryScalar();

        if(!$officeName || !$officeName || !$officeStreetAddress || !$officeCity || !$officeState || !$officeZip) {
            echo CJSON::encode(array('status'=>'error','message'=>'Account Settings is missing Office Name/Address information. This is required for sending mass emails. Please update and try again.'));
            Yii::app()->end();
        }

        $emailBlast = new EmailBlasts();
        $emailBlast->attributes = $_POST['EmailBlasts'];
        $emailTemplate = EmailTemplates::model()->findByPk($emailBlast->email_template_id);
        $emailBlast->email_body = $emailTemplate->body; //@todo: should we save this now or let it pull fresh at runtime?

        if(!($emailBlast->recipient_count = Closings::getForeverClients($_GET, Closings::FOREVER_CLIENT_RETURN_TYPE_COUNT))) {
            echo CJSON::encode(array('status'=>'error','message'=>'No contacts found to send emails to. Please check your filters and try again.'));
            Yii::app()->end();
        }

        // save criteria for foreverClients
        $params = array('emailBlastType'=>'foreverClients');
        $paramVars = array('last_spoke_to_days','closingStatus','firstName','lastName','email','phone','address','closingDateFrom','closingDateTo','specialDateFrom','specialDateTo');
        foreach($paramVars as $paramVar) {
            if(trim($_POST[$paramVar])) {
                $params[$paramVar] = $_POST[$paramVar];
            }
        }
        $emailBlast->recipient_criteria = CJSON::encode($params);

        if(!$emailBlast->save()) {
            $status = 'error';
            $message = current($emailBlast->getErrors())[0];
        }
        else {
            $status = 'success';
            $message = 'Successfully schedule your Forever Client Email Blast.';
        }

        echo CJSON::encode(array('status'=>$status,'message'=>$message));
        Yii::app()->end();
    }

    protected function _dialerListActions($dialerAction, $dialerListId)
    {
        $errorLog = array();

        if($contacts = Closings::getForeverClients($_GET, Closings::FOREVER_CLIENT_RETURN_TYPE_CONTACT_MODELS)) {

            foreach($contacts as $contact) {

                switch($dialerAction) {
                    case 'add':
                        CallListPhones::addToCallListByContactId($dialerListId, $contact->id, null, $errorLog);
                        break;

                    case 'remove':
                        CallListPhones::removeFromCallListByContactId($dialerListId, $contact->id, $errorLog);
                        break;
                }
            }
        }
    }

    public function printContactInfo($data)
    {
        $name = $data['first_name'].' '.$data['last_name'];

        if($data['spouse_first_name']) {
            if($data['last_name'] == $data['spouse_last_name']) {
                $name = $data['first_name'].' & '.$data['spouse_first_name'].' '.$data['spouse_last_name'];
            }
            else {
                $name .= ' & '.$data['spouse_first_name'].' '.$data['spouse_last_name'];
            }
        }

        echo '<a href="/admin/contacts/'.$data['contact_id'].'" style="font-weight:bold;">'.$name.'</a>';

        if($data['phone_id_pairs']) {
            $data['phone_id_pairs'] = explode(',', $data['phone_id_pairs']);
            $phoneString = '';
            foreach($data['phone_id_pairs'] as $phoneIdPair) {
                $phoneIdPair = explode('-', $phoneIdPair);

                $phoneString .= ($phoneString)? '<br>' : '';
                $phoneString .= Yii::app()->format->formatPhone($phoneIdPair[1]);
                $phoneString .= '<button class="text click-to-call-button" type="button" data-cid="'.$data['contact_id'].'" ctid="'.ComponentTypes::CONTACTS.'" data-phone="'.Yii::app()->format->formatPhone($phoneIdPair[1]).'" style="margin-left: 0; padding-top: 3px; padding-bottom: 6px;"><em class="icon i_stm_phone" style="width: 20px;"></em>CALL NOW</button>';
//                $phoneString .= '<a style="padding:4px 8px; display: inline;" class="text-button phone-status-button" href="javascript:void(0)" data-id="'.$phoneIdPair[0].'" data-ctid="'.ComponentTypes::CONTACTS.'" data-cid="'.$data['contact_id'].'" data-phone="'.Yii::app()->format->formatPhone($phoneIdPair[1]).'"><em class="icon i_stm_question"></em>Action</a>';
            }
        }
        echo ($phoneString) ? '<br>'.$phoneString : '';

        if($data['emails']) {
            $data['emails'] = explode(',', $data['emails']);
            $emailString = '';
            foreach($data['emails'] as $email) {
                $emailString .= ($emailString)? '<br>' : '';
                $emailString .= $email;
            }
        }
        echo ($emailString) ? '<br>'.$emailString : '';
    }
}
