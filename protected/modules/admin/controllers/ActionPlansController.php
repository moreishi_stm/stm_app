<?php
class ActionPlansController extends AdminController {

	public $pageColor = 'blue';

	public function actions() {
		return array('add'                  => 'stm_app.modules.admin.controllers.actions.actionPlans.AddAction',
			'addItem'              => 'stm_app.modules.admin.controllers.actions.actionPlans.AddItemAction',
			'editItems'            => 'stm_app.modules.admin.controllers.actions.actionPlans.EditItemsAction',
			'delete'               => 'stm_app.modules.admin.controllers.actions.actionPlans.DeleteAction',
			'itemsDelete'          => 'stm_app.modules.admin.controllers.actions.actionPlans.ItemsDeleteAction',
			'copy'                 => 'stm_app.modules.admin.controllers.actions.actionPlans.CopyAction',
			'view'                 => 'stm_app.modules.admin.controllers.actions.actionPlans.ViewAction',
			'edit'                 => 'stm_app.modules.admin.controllers.actions.actionPlans.EditAction',
			'index'                => 'stm_app.modules.admin.controllers.actions.actionPlans.ListAction',
			'listMiniAppliedPlans' => 'stm_app.modules.admin.controllers.actions.actionPlans.ListMiniAppliedPlansAction',
			'apply'                => 'stm_app.modules.admin.controllers.actions.actionPlans.ApplyAction',
			'unapply'              => 'stm_app.modules.admin.controllers.actions.actionPlans.UnapplyAction',
			'listShared'           => 'stm_app.modules.admin.controllers.actions.actionPlans.ListSharedAction',
			'copyShared'           => 'stm_app.modules.admin.controllers.actions.actionPlans.CopySharedAction',
			'library'           	=> 'stm_app.modules.admin.controllers.actions.actionPlans.LibraryAction'
		);
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 *
	 * @return none
	 */
	public function init() {
		$this->baseModel = new ActionPlans;
		$this->displayName = 'Action Plans';
	}
}
