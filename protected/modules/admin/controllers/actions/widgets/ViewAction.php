<?php

class ViewAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
        $this->controller->title = 'View CMS Widget';
		$model= CmsWidgets::model()->findByPk($id);

		$this->controller->render('view',array(
			'model'=>$model,
		));
	}
}