<?php

class AddGroupAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
        $this->controller->title = 'Add CMS Widget Groups';

        $model = new CmsWidgetGroups;
        $model->unsetAttributes();

        $WidgetModel=new CmsWidgets('search');
        $WidgetModel->unsetAttributes();  // clear any default values

        if (isset($_POST['CmsWidgetGroups'])) {
            $model->attributes = $_POST['CmsWidgetGroups'];

            $model->account_id = Yii::app()->user->accountId;

            if ($model->save()) {
                if ($model->widgetsToAdd) { //var_dump($model->widgetsToAdd);exit;
                    // Attach the contact types to the newly contact
                    foreach($model->widgetsToAdd as $sort_order => $widgetId) {
                        $CmsWidgetGroupLu = new CmsWidgetGroupLu;
                        $CmsWidgetGroupLu->cms_widget_group_id = $model->id;
                        $CmsWidgetGroupLu->cms_widget_id = $widgetId;
                        $CmsWidgetGroupLu->sort_order = $sort_order;
                        $CmsWidgetGroupLu->save();
                    }
                }

                Yii::app()->user->setFlash('success', 'Successfully added Widget Group.');
                $this->controller->redirect(array('index'));
            }
        }

		$this->controller->render('formGroup',array(
			'model'=>$model,
            'WidgetModel' =>$WidgetModel,
		));
	}
}