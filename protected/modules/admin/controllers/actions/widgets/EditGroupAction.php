<?php

class EditGroupAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
        $this->controller->title = 'Edit CMS Widget Groups';

        $model = CmsWidgetGroups::model()->findByPk($id);
		$WidgetModel=new CmsWidgets('search');
		$WidgetModel->unsetAttributes();  // clear any default values

		if (isset($_POST['CmsWidgetGroups']))
		{
			$model->attributes=$_POST['CmsWidgetGroups'];
			if ($model->save()) {
				// We only will add new ones when one is not assigned to the contact or when one is deleted
				if ($model->widgetsToAdd) {
					$widgetsToAddFlipped = array_flip($model->widgetsToAdd);
					foreach($model->cmsWidgetGroupLu as $CmsWidgetGroupLu) {
						// If db record is no longer in the new array of selection, delete it
						if (!in_array($CmsWidgetGroupLu->cms_widget_id, $model->widgetsToAdd))
							$CmsWidgetGroupLu->delete();
						else{
							$newSortOrder = $widgetsToAddFlipped[$CmsWidgetGroupLu->cms_widget_id];

							// if sort order is different, update it, else it is exactly the same, so don't touch it
							if ($CmsWidgetGroupLu->sort_order != $newSortOrder) {
								$CmsWidgetGroupLu->sort_order = $newSortOrder;
								$CmsWidgetGroupLu->save();
							}
							// the existing record has been processed, remove from ToAdd array
							unset($widgetsToAddFlipped[$CmsWidgetGroupLu->cms_widget_id]);
						}
					}
					$model->widgetsToAdd = array_flip($widgetsToAddFlipped);

					// Attach the contact types to the newly contact
					foreach($model->widgetsToAdd as $widgetId) {
						$CmsWidgetGroupLu = new CmsWidgetGroupLu;
						$CmsWidgetGroupLu->cms_widget_group_id = $model->id;
						$CmsWidgetGroupLu->cms_widget_id = $widgetId;
						$CmsWidgetGroupLu->sort_order = array_search($widgetId, $model->widgetsToAdd);
						$CmsWidgetGroupLu->save();
					}
				}

				Yii::app()->user->setFlash('success', 'Successfully Updated!');

				$this->controller->redirect(array('index'));
			}
		}else{ // Pull existing values
			// load widgetToAdd array
			if ($model->cmsWidgetGroupLu)
				foreach($model->cmsWidgetGroupLu as $CmsWidgetGroupLu)
					$model->widgetsToAdd[$CmsWidgetGroupLu->sort_order] = $CmsWidgetGroupLu->cms_widget_id;
				ksort($model->widgetsToAdd);

		}

		$this->controller->render('formGroup',array(
			'model'=>$model,
			'WidgetModel'=>$WidgetModel,
		));
	}
}