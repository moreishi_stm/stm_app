<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
        $this->controller->title = 'CMS Widget Groups';
		$model=new CmsWidgets('search');
		$model->unsetAttributes();  // clear any default values


		$WidgetGroupsModel=new CmsWidgetGroups('search');
		$WidgetGroupsModel->unsetAttributes();  // clear any default values

		// // used for gridview search box inputs
		// if (isset($_GET['ActionPlans']))
		// 	$model->attributes=$_GET['ActionPlans'];

		$this->controller->render('list',array(
			'model'=>$model,
			'WidgetGroupsModel' => $WidgetGroupsModel,
		));
	}
}