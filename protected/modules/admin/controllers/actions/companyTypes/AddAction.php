<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Add Company Type';
		$model = $this->controller->baseModel;

		if (isset($_POST['CompanyTypes'])) {
			$model->attributes = $_POST['CompanyTypes'];
			$model->account_id = Yii::app()->user->accountId;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Added Company Type.');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model,
		));
	}
}