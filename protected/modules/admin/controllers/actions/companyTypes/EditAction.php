<?php

class EditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'Edit Company Type';
		$model=$this->controller->loadModel($id);

		if (isset($_POST['CompanyTypes'])) {
			$model->attributes = $_POST['CompanyTypes'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Updated Company Type.');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}