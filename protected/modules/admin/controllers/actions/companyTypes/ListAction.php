<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Company Types';
		$model=$this->controller->baseModel;
		$model->unsetAttributes();  // clear any default values

        // used for gridview search box inputs
        if (isset($_GET['CompanyTypes']))
            $model->attributes=$_GET['CompanyTypes'];

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
