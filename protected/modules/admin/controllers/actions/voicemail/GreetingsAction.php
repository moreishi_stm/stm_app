<?php

class GreetingsAction extends CAction
{
    public function run()
    {
        /** @var CallRecordings $model */
        $model = CallRecordings::model();
        $model->contact_id = Yii::app()->user->id;

        // Render the view
        $this->controller->render('greetings', array(
            'model' =>  $model
        ));
    }
}