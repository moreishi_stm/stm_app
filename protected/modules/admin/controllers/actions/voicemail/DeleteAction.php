<?php

class DeleteAction extends CAction
{
    public function run($id)
    {
        if ($model = VoicemailMessages::model()->findByPk($id)) {
            $model->updated_by = Yii::app()->user->id;
            $model->updated = new CDbExpression('NOW()');
            $model->is_deleted=1;

            if (Yii::app()->request->isAjaxRequest) {

                if ($model->save()) {
                    echo CJSON::encode(array('status' => 'success'));
                }
                else {
                    echo CJSON::encode(array('errorMessage' => 'Voicemail did not delete. Error Message: '.$model->getErrors()[0], 'status' => 'error'));
                }
                Yii::app()->end();
            }
        }
    }
}