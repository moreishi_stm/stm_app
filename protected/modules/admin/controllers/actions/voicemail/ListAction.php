<?php

class ListAction extends CAction
{
    public function run($id)
    {
        // Instantiate model
        $model = new VoicemailMessages();

        // Locate telephony phone record
        /** @var TelephonyPhones $telephonyPhone */
//        $telephonyPhone = TelephonyPhones::model()->findByAttributes(array(
//            'call_hunt_group_id'    =>  Yii::app()->request->getParam('id')
//        ));

        // Retrieve the data provider
        switch(Yii::app()->request->getParam('type')) {

            // Hunt group specific voicemails
//            case 'hunt-group':
//                $provider = $model->searchByCallHuntGroupId(Yii::app()->request->getParam('id'));
//            break;

            case 'phone':
                if(Yii::app()->request->getParam('number')) {
                    $telephonyPhone = TelephonyPhones::model()->findByAttributes(array('phone' => Yii::app()->request->getParam('number')));
                }
                elseif(Yii::app()->request->getParam('id')) {
                    $telephonyPhone = TelephonyPhones::model()->findByPk(Yii::app()->request->getParam('id'));
                }
                $provider = $model->searchByTelephonyPhoneId(Yii::app()->request->getParam('id'));
                break;

            // Extension specific voicemails
            case 'extension':
                $provider = $model->searchByIvrExensionId(Yii::app()->request->getParam('id'));
            break;

            default:
                throw new Exception('Error, invalid voicemail type!');
            break;
        }

        // Render the view
        $this->controller->render('index', array(
            'provider' => $provider,
            'telephonyPhone' => $telephonyPhone,
            'type' => Yii::app()->request->getParam('type')
        ));
    }
}