<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddGreetingAction extends CAction
{
    public function run()
    {
        $this->controller->title = 'Add Greeting';
        $model = new CallRecordings();

        if (isset($_POST['CallRecordings'])) {

            $model->attributes = $_POST['CallRecordings'];
            $model->type = CallRecordings::TYPE_VOICEMAIL_GREETING;
            $model->added = new CDbExpression('NOW()');
            $model->contact_id = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added Greeting!');
                $this->controller->redirect(array('/admin/voicemail/editGreeting/' . $model->id));
            }
            else {
                throw new Exception('Error, unable to save: "' . print_r($model->getErrors(), true) . '"');
            }
        }

        // Render view
        $this->controller->render('form', array(
            'model'=>$model
        ));
    }
}