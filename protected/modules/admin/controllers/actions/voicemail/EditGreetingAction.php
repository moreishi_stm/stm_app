<?php

class EditGreetingAction extends CAction
{
    public function run($id)
    {
        $model = CallRecordings::model()->findByPk($id);
        if(!$model) {
            throw new Exception('Error, unable to locate record');
        }

        $this->controller->title = 'Edit Greeting';

        if (isset($_POST['CallRecordings'])) {

            $model->attributes = $_POST['CallRecordings'];
            $model->contact_id = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated Greeting.');
//                $this->controller->redirect(array('/admin/ivr/record?id=' . $model->id . '&type=ivr'));
                $this->controller->redirect(array('/admin/voicemail/greetings'));
            }
        }

        // Render view
        $this->controller->render('form', array(
            'model' =>  $model,
            'clientId'  =>  Yii::app()->user->clientId,
            'accountId'  =>  Yii::app()->user->accountId
        ));
    }
}