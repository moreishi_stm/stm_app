<?php
	class ABCListAction extends CAction {

		public function run($id = null) {

			Yii::import('stm_app.modules.admin.controllers.actions.transactions.PipelineAction');

			$id = ($id) ? $id : Yii::app()->user->id;

			$TransactionType = Yii::app()->request->getQuery('transactionType');

			$model = Transactions::newInstance($TransactionType, 'search');

			$model->unsetAttributes();

			$modelName = get_class($model);
			$this->controller->title = 'A, B, C ' . $modelName;

			$HotAProvider = PipelineAction::getProviderByCategory('a', $modelName, $id);
			$BProvider = PipelineAction::getProviderByCategory('b', $modelName, $id);
			$CProvider = PipelineAction::getProviderByCategory('c', $modelName, $id);

			$this->controller->render('abcList', array(
					'model' => $model,
					'userId' => $id,
					'HotAProvider' => $HotAProvider,
					'BProvider' => $BProvider,
					'CProvider' => $CProvider,
				)
			);
		}
	}
