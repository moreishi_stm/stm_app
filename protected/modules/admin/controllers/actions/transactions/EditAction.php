<?php

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id the ID of the model to be updated
	 */
	class EditAction extends CAction {

		public function run($id) {
			$this->registerScript();
			$componentType = Yii::app()->request->getQuery('transactionType');

			$model = Transactions::newInstance($componentType);
			$model = $model->findByPk($id);

            if(!$model) {
                if($model2 = Transactions::model()->byIgnoreAccountScope()->findByPk($id)) {

                    throw new Exception(__CLASS__.' (:'.__LINE__.') Transaction is not in this account.');
                }
                else {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Transaction not found.');
                }
            }

			$model->component_type_id = $this->controller->componentType->id;
			$this->controller->title = $model->contact->fullName;

            if (isset($_POST['TransactionTagLu'])) {
                $model->transactionTagLu = new TransactionTagLu;
                $model->transactionTagLu->attributes = $_POST['TransactionTagLu'];
            } else {
                if($TransactionTagLus = $model->transactionTagLu) {
                    $tagIds = array();
                    foreach($TransactionTagLus as $TransactionTagLu) {
                        array_push($tagIds, $TransactionTagLu->transaction_tag_id);
                    }

                }
                $TransactionTagLu = new TransactionTagLu;
                $TransactionTagLu->idCollection = $tagIds;
                $model->transactionTagLu = $TransactionTagLu;
            }

			// Set the scenario based on the transaction type
			switch ($model->component_type_id) {
				case ComponentTypes::SELLERS:
				case ComponentTypes::LISTINGS:
					$model->scenario = 'sellerForm';
					break;
				default:
					$model->scenario = 'buyerForm';
					break;
			}

			// Create a new instance of the transaction fields to use for labeling fields
			$TransactionFields = new TransactionFields;
			$TransactionFields->component_type_id = $model->component_type_id;

			// Create a collection of transaction field values from what may already be existing from the transaction
			$TransactionFieldValues = new TransactionFieldValues;
			foreach ($model->transactionFieldValues as $TransactionFieldValue) {
				$TransactionFieldValues->data[$TransactionFieldValue->transaction_field_id] = $TransactionFieldValue->value;
			}

            // validate market snapshot
            $marketSnapshotValidated = true;
            if(Yii::app()->user->settings->market_snapshot == '1' && !empty($_POST['TransactionFieldValues']['data'][TransactionFields::MARKET_SNAPSHOT_FREQUENCY])) {
                // check to see if address
                if(empty($_POST['Sellers']['address_id'])) {
                    $marketSnapshotValidated = false;
                    $TransactionFieldValues->addError('data[' . $TransactionFields->getField('market_snapshot')->id . ']', 'Address is required for Market Snapshot.');
                }
            }

            // load opportunity
            $Opportunity = Opportunities::model()->findByAttributes(array('component_type_id'=>$model->component_type_id, 'component_id'=>$id));
            if (!$Opportunity) {
                $Opportunity = new Opportunities;
                $Opportunity->component_type_id = $model->component_type_id;
                $Opportunity->component_id = $id;
            }
            $opportunityValidated = true;
            if (isset($_POST['Opportunities'])) {

//                $Appointment->isEmpty = true;
//                foreach ($_POST['Appointments'] as $key => $value) {
//                    if (!empty($value)) {
//                        if(in_array($key, array('met_by_id','set_by_id','signed_date','met_status_ma','met_status_reason','set_activity_type_id','is_deleted'))) {
//                            $Appointment->isEmpty = false;
//                            break;
//                        }
//                    }
//                }
//
                $Opportunity->attributes = $_POST['Opportunities'];
                if ($Opportunity->isEmpty == false) { // && $_POST['Opportunities']['toDelete'] != 1
                    if(!($opportunityValidated = $Opportunity->validate())) {
                        $error = current($Opportunity->getErrors());
                        Yii::app()->user->setFlash('error', $error[0]);
                    }
//
                }
//                if($Appointment->toDelete) {
//                    $opportunityValidated = true;
//                }
            }

            // load appointment
			$Appointment = Appointments::model()->byComponentTuple($model->component_type_id, $id)->find();
			if (!$Appointment) {
				$Appointment = new Appointments;
				$Appointment->isEmpty = true;
				$Appointment->component_type_id = $model->component_type_id;
				$Appointment->component_id = $id;
			}

            // Process the Appointments model
			$appointmentValidated = true;
			if (isset($_POST['Appointments'])) {

				//[set_on_datetime] => 11/19/2015 12:00 am
    			//[set_for_datetime] => 11/19/2015 12:00 am

				$Appointment->isEmpty = true;
				foreach ($_POST['Appointments'] as $key => $value) {
					if (!empty($value)) {
						if(in_array($key, array('met_by_id','set_by_id','signed_date','met_status_ma','met_status_reason','set_activity_type_id','is_deleted'))) {
							$Appointment->isEmpty = false;
							break;
						}
					}
				}

				$Appointment->attributes = $_POST['Appointments'];

				$set_for_datetime = explode(" ",$_POST['Appointments']['set_for_datetime']);
				if(strtolower($set_for_datetime[2]) == "am" && $set_for_datetime[1] == 12) {
					Yii::app()->user->setFlash("error","Please select a valid appointment SET time. 12am is invalid.");
					$appointmentValidated = false;
				}

				$set_on_datetime = explode(" ",$_POST['Appointments']['set_on_datetime']);
				if(strtolower($set_on_datetime[2]) == "am" && $set_on_datetime[1] == 12) {
					Yii::app()->user->setFlash("error","Please select a valid appointment time to MEET. 12am is invalid.");
					$appointmentValidated = false;
				}

				if($appointmentValidated) {

					if ($Appointment->isEmpty == false && $_POST['Appointments']['toDelete'] != 1) {
						if (!($appointmentValidated = $Appointment->validate())) {
							$error = current($Appointment->getErrors());
							Yii::app()->user->setFlash('error', $error[0]);
						}

					}
					if ($Appointment->toDelete) {
						$appointmentValidated = true;
					}
				}
			}

			// Set contact and address fields for form equivalent to the relationship value
			$model->tempContact = $model->contact;

			// Process assignments for the transactions if any
			$assignmentValidated = true;
			if (isset($_POST['Assignments'])) {
                $assignmentsToDelete = array();
                $assignmentsIdsToDelete = array();
				foreach ($_POST['Assignments'] as $i => $assignmentData) {
                    if(($assignmentData['assignment_type_id'] == AssignmentTypes::LOAN_OFFICER) && (empty($assignmentData['assignee_contact_id']) && $assignmentData['assignee_contact_id']!==0 )) {
                        continue;
                    }
					if ($assignmentData['id']) {
						$Criteria = new CDbCriteria;
						$Criteria->condition = 'id = :id';
						$Criteria->params = array(
							':id' => $assignmentData['id'],
						);
						$TransactionAssignment = Assignments::model()->find($Criteria);
					} else {
						$TransactionAssignment = new Assignments;
						$assignmentData['component_type_id'] = $model->component_type_id;
						$assignmentData['component_id'] = $model->id;
					}

					$TransactionAssignment->attributes = $assignmentData; // @todo: on test, Check to see component_type_id is correct for new record

					if ($TransactionAssignment->remove) {
                        array_push($assignmentsToDelete, $TransactionAssignment);
                        $assignmentsIdsToDelete[] = $TransactionAssignment->assignee_contact_id;
						continue;
					}

					//  Mark that the contact e-mails section had a failure
					if (!$TransactionAssignment->validate() && $assignmentValidated) {
						$assignmentValidated = false;
                        $error = current($TransactionAssignment->getErrors());
                        Yii::app()->user->setFlash('error', $error[0]);
					}
					array_push($model->transactionAssignments, $TransactionAssignment);
				}
			}

			if ((isset($_POST[get_class($model)]) && empty($model->transactionAssignments))) { // @todo: Need to pass post data back into field
                if(!empty($assignmentsToDelete)) {
                    Yii::app()->user->setFlash('error', 'Must at least 1 Assignment after deleting. Consider assigning to the Lead Pool instead of deleting.'); //@todo: need validation to be flagged
                }
                else {
                    Yii::app()->user->setFlash('error', 'Please have at least 1 Assignment.'); //@todo: need validation to be flagged
                }
                $assignmentValidated = false;
			}

			$modelClass = get_class($model);
			if (isset($_POST[$modelClass]) && isset($_POST['TransactionFieldValues'])) { // && isset($_POST['Assignments'])
				$model->attributes = $_POST[$modelClass];

				if ($assignmentValidated && $appointmentValidated && $opportunityValidated && $marketSnapshotValidated && $model->save()) {
					// Process the transaction field values
					foreach ($_POST['TransactionFieldValues']['data'] as $transactionFieldId => $value) {
						unset($TransactionValue);
						$value = trim($value);

						if (isset($TransactionFieldValues->data[$transactionFieldId]) || !empty($value) || $value === "0") {
							if (isset($TransactionFieldValues->data[$transactionFieldId])) {
								// If the value was previously set then pull a instance of the value's record and modify it.
								$Criteria = new CDbCriteria;
								$Criteria->condition = 'transaction_id = :transaction_id AND transaction_field_id = :transaction_field_id';
								$Criteria->params = array(
									':transaction_id' => $id,
									':transaction_field_id' => $transactionFieldId,
								);
								$TransactionValue = TransactionFieldValues::model()->find($Criteria);
							} else {
								// The transaction value did not previously exist create it
								$TransactionValue = new TransactionFieldValues;
								$TransactionValue->transaction_id = $model->id;
								$TransactionValue->transaction_field_id = $transactionFieldId;
							}

							$TransactionValue->value = $value;
							if(!$TransactionValue->save()) {
                                Yii::log(__CLASS__ . '(' . __LINE__ . ') TransactionFieldValues did not save. Error Message: '.print_r($TransactionValue->getErrors(), true).' TransactionFieldValues Attributes: '.print_r($TransactionValue->attributes, true).PHP_EOL.'$TransactionFieldValues->data values: '.print_r($TransactionFieldValues->data, true), CLogger::LEVEL_ERROR);
                            }
						}
					}

					// already ran validation
					if (!$Appointment->isEmpty && $Appointment->toDelete!=1) {
						$Appointment->save(false);
					} elseif (!$Appointment->isNewRecord && $Appointment->toDelete==1) {
						if(!$Appointment->delete()) {
                            // FYI: delete (soft) will always return false due to the nature of the way soft deletes are handled. The log below doesn't need to be flagged.
//                            Yii::log(__CLASS__ . '(' . __LINE__ . ') Appointment did not delete. Error Message: '.print_r($Appointment->getErrors(), true).' Appointment Attributes: '.print_r($Appointment->attributes, true), CLogger::LEVEL_ERROR);
                        }
					}

                    // check for post data as there is not edit for an opportunity
                    if(isset($_POST['Opportunities']) && !$Opportunity->isEmpty && $Opportunity->toDelete!=1) {
                        $Opportunity->save(false);
                    } elseif (!$Opportunity->isNewRecord && $Opportunity->toDelete==1) {
                        $Opportunity->delete();
                    }

                    $model->transactionTagLu->deleteAllByAttributes(array('transaction_id'=>$model->id));
                    if (isset($_POST['TransactionTagLu'])) {
                        if(is_array($_POST['TransactionTagLu'])) {
                            foreach($_POST['TransactionTagLu']['idCollection'] as $transactionTagId) {
                                $TransactionTagLu = new TransactionTagLu;
                                $TransactionTagLu->transaction_id = $model->id;
                                $TransactionTagLu->transaction_tag_id = $transactionTagId;
                                $TransactionTagLu->save();
                            }
                        }
                    }

					// Assign a contact to this transaction
					if ($model->transactionAssignments) {
						foreach ($model->transactionAssignments as $TransactionAssignment) {
							if ($TransactionAssignment->isNewRecord) {
								$TransactionAssignment->component_id = $model->id;
							}

							$TransactionAssignment->logChange = true;
							$TransactionAssignment->save(false); // already ran validation
						}
					} else {
						// handles if there are not any assignments
						$this->controller->redirect(array('/admin/' . strtolower($this->controller->componentType->name) . '/edit/' . $model->id));
					}
                    if(!empty($assignmentsToDelete) && is_array($assignmentsToDelete)) {
                        foreach($assignmentsToDelete as $assignmentToDelete) {
                            // delete will always return false due to soft delete, therefore removed the error message
                            $assignmentToDelete->delete();
                        }
                    }

					Yii::app()->user->setFlash('success', 'Successfully Updated!');

					if ($returnUrl = Yii::app()->user->getState('transactionReferrer')) {
						Yii::app()->user->setState('transactionReferrer', null);
						$this->controller->redirect($returnUrl);
					} else {
						$this->controller->redirect(array(
								'view',
								'id' => $model->id,
								'transactionType' => strtolower($this->controller->componentType->name)
							)
						);
					}
				} else {
                    if(!empty($model->transactionAssignments) && is_array($model->transactionAssignments)) {
                        if(empty($model->assignments)) {
                            $model->assignments = $model->transactionAssignments;
                        }
                        else {
                            $existingAssignmentIds = array();
                            if(is_array($model->assignments)) {
                                foreach($model->assignments as $i => $existingAssignment) {
                                    $existingAssignmentIds[] = $existingAssignment->assignee_contact_id;
                                    if(in_array($existingAssignment->assignee_contact_id, $assignmentsIdsToDelete)) {
                                        $model->assignments[$i]->remove = 1;
                                    }
                                }
                            }
                            $allAssignments = $model->assignments;
                            foreach($model->transactionAssignments as $newAssignment) {
                                if(!in_array($newAssignment->assignee_contact_id, $existingAssignmentIds)) {
                                    array_push($allAssignments, $newAssignment);
                                }
                            }

                            $model->assignments = $allAssignments;
                        }
                    }

					foreach ($_POST['TransactionFieldValues']['data'] as $transactionFieldId => $value) {
						$TransactionFieldValues->data[$transactionFieldId] = trim($value);
					}
                }
			}

			$this->controller->render('form', array(
					'model' => $model,
					'Appointment' => $Appointment,
                    'Opportunity' => $Opportunity,
					'TransactionFields' => $TransactionFields,
					'TransactionFieldValues' => $TransactionFieldValues,
				)
			);
		}

		public function registerScript() {
			Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_AssignmentsGui.js');
		}
	}
