<?php
class ComingSoonAction extends CAction {

	public function run() {

		$transactionType = Yii::app()->request->getQuery('transactionType');
        $model = $this->controller->getSearchModel($transactionType);

		$model->transaction_status_id = TransactionStatus::COMING_SOON_ID;

		$DataProvider = $model->search();

		$this->controller->title = 'Coming Soon Listings';
		$this->controller->render('comingSoon', array(
			'DataProvider' => $DataProvider,
		));
	}

    public function printPropertyInfo($data)
    {
        echo "<div><strong><u>".Yii::app()->format->formatAddress($data->address, array(streetOnly=>true))."</u></strong><br />".Yii::app()->format->formatAddress($data->address, array(CityStZipOnly=>true))."</div>";

        $price = $data->getFieldValue("price_current");
        echo "<div>Price: ".(($price) ? Yii::app()->format->formatDollars($price) : "-")."</div>";

        $listingDate = $data->getFieldValue("listing_date");
        echo "<div>Listing Date: ".(($listingDate) ? $listingDate : "-")."</div>";

        echo "<div>".$br = (Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false)) ? Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false).Yii::app()->format->formatGridValue(" / Baths:", $data->getFieldValue("baths", 0),false) : Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false).
            "</div>";

        echo "<div>".Yii::app()->format->formatGridValue("SF:", $data->getFieldValue("sq_feet", 0),false)."</div>";

        //echo "<div>".Yii::app()->format->formatGridValue("Submit:", Yii::app()->format->formatDateDays($data->contact->added))."</div>";

    }
}
