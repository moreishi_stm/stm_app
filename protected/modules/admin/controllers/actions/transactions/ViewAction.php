<?php

class ViewAction extends CAction {

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id) {
		$TransactionType = Yii::app()->request->getQuery('transactionType');

        switch($TransactionType) {
            case 'buyers':
                $TransactionTypeSingular = 'Buyer';
                if(Yii::app()->user->checkAccess('noBuyerAccess')) {
                    $this->controller->render('/main/accessDenied', array());
                    Yii::app()->end();
                }
                break;

            case 'sellers':
                $TransactionTypeSingular = 'Seller';
                if(Yii::app()->user->checkAccess('noSellerAccess')) {
                    $this->controller->render('/main/accessDenied', array());
                    Yii::app()->end();
                }
                break;
        }

        $model = Transactions::newInstance($TransactionType, 'search');
		$model = $model->findByPk($id);
        if(!$model) {
            $crossAccountRecord = Transactions::model()->byIgnoreAccountScope()->findByPk($id);
        }

        if(!$model && $crossAccountRecord) {
            $errorMessage = 'This '.$TransactionTypeSingular.' record is in a different market.<br><br>Please login to the appropriate market to access this record.';
        }elseif(!$model) {
            $errorMessage = $TransactionTypeSingular.' record not Found. Please verify the search or URL and try again.';
        }

        $returnUrl = $this->getController()->createUrl("/admin/$TransactionType/view", array('id'=>$id)).'?returnContactIdVerify='.$model->contact->id;
        Yii::app()->user->setState('returnContactIdVerify',$returnUrl);

        $this->controller->title=$model->contact->fullName;

		$this->controller->render('view', array(
			'model'=> $model,
            'errorMessage' => $errorMessage,
		));
	}
}