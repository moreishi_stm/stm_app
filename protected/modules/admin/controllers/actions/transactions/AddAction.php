<?php
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class AddAction extends CAction {

		public function run($contactId) {

			$this->controller->title = 'Add '.ucwords(Yii::app()->request->getQuery('transactionType'));
			// Create new instance of the transaction
			$model = new Transactions;
			$model->source_id = ($_GET['source']) ? $_GET['source'] : '';
			$model->component_type_id = $this->controller->componentType->id;
            $model->address_id = ($_GET['addressId']) ? $_GET['addressId'] : '';
            $model->transactionTagLu = new TransactionTagLu;
            if (isset($_POST['TransactionTagLu'])) {
                $model->transactionTagLu->attributes = $_POST['TransactionTagLu'];
            }
			$model->tempContact = Contacts::model()->findByPk($contactId);
			$model->contact_id = $contactId;

			$model->appointment = new Appointments;
			$model->appointment->component_type_id = $model->component_type_id;

			// Create a new instance of the transaction fields to use for labeling fields
			$TransactionFieldValues = new TransactionFieldValues;
			if (isset($_POST['TransactionFieldValues'])) {
				$TransactionFieldValues->attributes = $_POST['TransactionFieldValues'];
			}

			$TransactionFields = new TransactionFields;
			$TransactionFields->component_type_id = $model->component_type_id;

			// Set the scenario based on the transaction type
			switch ($model->component_type_id) {
				case ComponentTypes::SELLERS:
				case ComponentTypes::LISTINGS:
					$model->scenario = 'sellerForm';
					break;
				default:
					$model->scenario = 'buyerForm';
					break;
			}

			if (isset($_POST['Transactions']) && isset($_POST['TransactionFieldValues'])) {
				$model->attributes = $_POST['Transactions'];

				$assignmentValidated = false;
				// Process the Assignments model
				if (isset($_POST['Assignments'])) {
					$assignmentValidated = true;

					$transactionAssignments = array();
					foreach ($_POST['Assignments'] as $i => $assignmentData) {
                        if(($assignmentData['assignment_type_id'] == AssignmentTypes::LOAN_OFFICER) && empty($assignmentData['assignee_contact_id'])) {
                            unset($_POST['Assignments'][$i]);
                            continue;
                        }

						$Assignment = new Assignments('addTransaction');
						$Assignment->attributes = $assignmentData;
                        if(($Assignment->assignment_type_id == AssignmentTypes::LOAN_OFFICER) && empty($Assignment->assignee_contact_id)) {
                            continue;
                        }

						if(!$Assignment->validate(array('assignee_contact_id','assignment_type_id'))) {
                            Yii::app()->user->setFlash('error', 'Assignment not valid.');
							$assignmentValidated = false;
						}
						array_push($transactionAssignments,$Assignment);
					}
                    if(!(($assignmentData['assignment_type_id'] == AssignmentTypes::LOAN_OFFICER) && empty($assignmentData['assignee_contact_id']))) {
                        $model->assignments = $transactionAssignments;
                    }

				} else {
					Yii::app()->user->setFlash('error', 'Please add at least 1 Assignment.');
				}

				// Process the Appointments model
				$appointmentValidated = false;
				if (isset($_POST['Appointments'])) {

					$appointmentIsEmpty = true;
					foreach ($_POST['Appointments'] as $key => $value) {
						if (!empty($value) && !in_array($key, array('googleCalendarData', 'google_calendar_id', 'google_calendar_event_id','toDelete'))) {
							$appointmentIsEmpty = false;
							break;
						}
					}

					$model->appointment->attributes = $_POST['Appointments'];

					$continue = true;
					$set_for_datetime = explode(" ",$_POST['Appointments']['set_for_datetime']);
					if(strtolower($set_for_datetime[2]) == "AM" && $set_for_datetime[1] == 12) {
                        Yii::app()->user->setFlash("error","Please select a valid appointment time to MEET. 12am is invalid.");
                        $appointmentValidated = false;
					}

					$set_on_datetime = explode(" ",$_POST['Appointments']['set_on_datetime']);
					if(strtolower($set_on_datetime[2]) == "AM" && $set_on_datetime[1] == 12) {
                        Yii::app()->user->setFlash("error","Please select a valid appointment SET time. 12am is invalid.");
                        $appointmentValidated = false;
					}

					if($continue) {
						if ($appointmentIsEmpty == false) {
							if (!($appointmentValidated = $model->appointment->validate(array(
									'component_type_id',
									'set_by_id',
									'met_by_id',
									'met_status_ma',
									'set_on_datetime',
									'set_for_datetime',
									'googleCalendarData',
									'location_ma'
								)
							))
							) {
								$error = current($model->appointment->getErrors());
								Yii::app()->user->setFlash('error', $error[0]);
							}

						} else {
							$appointmentValidated = true;
						}
					}
				}

				if ($model->validate() && $appointmentValidated && $assignmentValidated) {

					if ($model->save(false)) {

						// Process the transaction field values
						foreach ($_POST['TransactionFieldValues']['data'] as $transactionFieldId => $value) {
							if (!empty($value) || $value === "0") {
								$TransactionValue = new TransactionFieldValues;
								$TransactionValue->transaction_id = $model->id;
								$TransactionValue->transaction_field_id = $transactionFieldId;
								$TransactionValue->value = $value;
								$TransactionValue->save();
							}
						}

						if (!$appointmentIsEmpty) {
							$model->appointment->component_id = $model->id;
							$model->appointment->save(false); // Already validated
						}

						if (isset($_POST['Assignments'])) {
							foreach ($_POST['Assignments'] as $i => $assignmentData) {
								$assignment = new Assignments;
								$assignment->attributes = $assignmentData;
								$assignment->component_type_id = $model->component_type_id;
								$assignment->component_id = $model->id;
								if (!$assignment->save(false)) {
									Yii::app()->user->setFlash('error', 'Error: Assignment did not save properly.');
								}
							}
						}

                        if (isset($_POST['TransactionTagLu'])) {
                            if(is_array($_POST['TransactionTagLu'])) {
                                foreach($_POST['TransactionTagLu']['idCollection'] as $transactionTagId) {
                                    $TransactionTagLu = new TransactionTagLu;
                                    $TransactionTagLu->transaction_id = $model->id;
                                    $TransactionTagLu->transaction_tag_id = $transactionTagId;
                                    $TransactionTagLu->save();
                                }
                            }
                        }
					}

					Yii::app()->user->setFlash('success', 'Successfully created transaction!');
					$this->controller->redirect(array(
							'view',
							'id' => $model->id,
							'transactionType' => strtolower($this->controller->componentType->name)
						)
					);
				}
			}

			Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_AssignmentsGui.js');

			$statusIds = array(TransactionStatus::LEAD_POOL_ID,TransactionStatus::HOT_A_LEAD_ID,TransactionStatus::B_LEAD_ID,TransactionStatus::C_LEAD_ID,TransactionStatus::NURTURING_ID);
			$existingOpenTransactions = Transactions::model()->byContactId($contactId)->byComponentTypeId($model->component_type_id)->byStatusIds($statusIds)->findAll();

			$this->controller->render('form', array(
					'model' => $model,
					'Appointment' => $model->appointment,
					'TransactionFields' => $TransactionFields,
					'TransactionFieldValues' => $TransactionFieldValues,
					'existingOpenTransactions'=> $existingOpenTransactions,
				)
			);
		}
	}