<?php
class ReverseProspectAction extends CAction {

	public function run()
    {
        $this->controller->title = 'Reverse Prospect';

		$TransactionType = Yii::app()->request->getQuery('transactionType');
		$model = Transactions::newInstance($TransactionType, 'search');
		$model->unsetAttributes();

		$tableName = Yii::app()->user->board->prefixedName;
		$MlsProperties = MlsProperties::model($tableName);

		if (isset($_GET['MlsProperties'])) {
            $MlsProperties->attributes = $_GET['MlsProperties'];
        }

		$this->controller->title = 'Reverse Prospect';
		$this->controller->render('reverseProspect', array(
			'model' => $model,
			'MlsProperties' => $MlsProperties,
		));
	}
}
