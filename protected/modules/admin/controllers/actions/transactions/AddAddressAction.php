<?php
class AddAddressAction extends CAction {

	public function run() {
		$Address = new Addresses;
		$this->processAjaxRequest($Address);
	}

	protected function processAjaxRequest(Addresses $Address)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$successFlag = true;
			if (isset($_POST['Addresses']) && isset($_GET['contact_id']) && !empty($_GET['contact_id']) && isset($_GET['transaction_id']) && !empty($_GET['transaction_id'])) {

                // see if this address already exists and not associated with another transaction or something...
                $criteria = new CDbCriteria();
                $criteria->compare('transactions.id', $_GET['transaction_id']);
                $criteria->compare('address', $_GET['address']);
                $criteria->compare('city', $_GET['city']);
                $criteria->compare('zip', $_GET['zip']);
                $criteria->with = array('transactions');
                $existingAddress = Addresses::model()->find($criteria);

                if($existingAddress) {
                    echo CJSON::encode(array('id'=>$existingAddress->id, 'fullAddress'=>$existingAddress->fullAddress, 'existing'=> true));
                    Yii::app()->end();
                }


				$Address->account_id = Yii::app()->user->accountId;
				$Address->attributes = $_POST['Addresses'];
				if(!$Address->save()) {
                    echo CActiveForm::validate($Address);
					Yii::app()->end();
				} else {
					$AddressContactLu = new AddressContactLu;
					$AddressContactLu->address_id = $Address->id;
					$AddressContactLu->contact_id = $_GET['contact_id'];
					if(!$AddressContactLu->save())
						$successFlag = false;

					$Transaction = Transactions::model()->findByPk($_GET['transaction_id']);
					$Transaction->address_id = $Address->id;
					if(!$Transaction->save())
						$successFlag = false;
				}
			}

			if($successFlag == false)
				echo false;
			else
				echo CJSON::encode(array('id'=>$Address->id, 'fullAddress'=>$Address->fullAddress));

			Yii::app()->end();
		}
	}
}
