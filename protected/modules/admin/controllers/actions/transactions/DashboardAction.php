<?php

/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class DashboardAction extends CAction
{
	public function run($id)
	{
		$model = $this->controller->baseModel;
		$model->unsetAttributes();  // clear any default values
		$model->transaction_status_id = $id;
		$model->component_type_id = ComponentTypes::getByName(Yii::app()->request->getQuery('transactionType'))->id;

		switch($id)
		{
			case TransactionStatus::NEW_LEAD_ID:
				$partialView = '_dashboardLeadsNew';
				break;

			case (TransactionStatus::HOT_A_LEAD_ID || TransactionStatus::B_LEAD_ID || TransactionStatus::C_LEAD_ID):
				$partialView = '_dashboardLeads';
				$model->transaction_status_id = $id;
				break;
		}

		$model->transaction_status_id = $id;
		$TransactionFields = new TransactionFields;

		$this->controller->renderPartial($partialView, array(
			'model'=>$model,
			'TransactionFields'=>$TransactionFields,
		));
	}
}