<?php

	class PipelineAction extends CAction {

		public function run($category)
        {
            $modelName = ucwords(Yii::app()->request->getQuery('transactionType'));
			$dataProvider = PipelineAction::getProviderByCategory($category, $modelName);

			$view = ($category == 'new') ? '_leadsNew' : '_leads';

			$this->controller->renderPartial($view, array(
					'dataProvider' => $dataProvider
				)
			);
		}

		public static function getProviderByCategory($category, $transactionName, $id = null) {
			$id = ($id) ? $id : Yii::app()->user->id;
			$dataProvider = null;

			switch ($category) {
				case 'new':
					$transactionStatusId = TransactionStatus::NEW_LEAD_ID;
					break;

				case 'a':
					$transactionStatusId = TransactionStatus::HOT_A_LEAD_ID;
					break;

				case 'b':
					$transactionStatusId = TransactionStatus::B_LEAD_ID;
					break;

				case 'c':
					$transactionStatusId = TransactionStatus::C_LEAD_ID;
					break;
			}

			$Criteria = new CDbCriteria;
			$Criteria->with = 'assignments';
			$Criteria->together = true;
			$Criteria->condition = 'transaction_status_id = :transaction_status_id';
			$Criteria->params = array(':transaction_status_id' => $transactionStatusId);
			$Criteria->order = 't.added desc';

            $Criteria->condition .= ' AND assignments.assignee_contact_id = :contact_id';
            $Criteria->params = CMap::mergeArray($Criteria->params, array(':contact_id' => $id));

			$dataProvider = new CActiveDataProvider($transactionName, array(
				'criteria' => $Criteria,
				'pagination' => array(
					'pageSize' => 50,
				),
			));

			return $dataProvider;
		}
	}
