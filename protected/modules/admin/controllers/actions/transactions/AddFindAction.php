<?php

	/**
	 * Finds the contact to add a transaction to
	 */
	class AddFindAction extends CAction {

		public function run() {

			$this->controller->pageColor = 'red';

			$model = new Contacts('addFind');
			$model->unsetAttributes(); // clear any default values
			if (isset($_GET['Contacts'])) {
				$model->attributes = $_GET['Contacts'];
			}

			$model->emails = new Emails('search');
			if (isset($_GET['Emails'])) {
				$model->emails->attributes = $_GET['Emails'];
			}

			$model->phones = new Phones('search');
			if (isset($_GET['Phones'])) {
				$model->phones->attributes = $_GET['Phones'];
			}

			if (Yii::app()->request->isAjaxRequest && isset($_POST['Contacts'])) {
				$this->processAjaxRequest($model);
			}

			$transactionType = Yii::app()->request->getQuery('transactionType');
			$transactionTypeSingular = Yii::app()->format->formatProperCase(substr($transactionType, 0, strlen($transactionType) - 1));

			$this->controller->title = 'Add New ' . $transactionTypeSingular;

			$DataProvider = $model->searchBeforeAdd();

			if (Yii::app()->request->isAjaxRequest && isset($_GET['Contacts'])) {
				$exactEmailMatch = $this->isExactEmailMatch($model->emails);
				$exactPhoneMatch = $this->isExactPhoneMatch($model->phones);
				$exactMatches = array_merge($exactEmailMatch, $exactPhoneMatch);
				if (!empty($exactMatches)) {
					$Criteria = new CDbCriteria;
					$Criteria->addInCondition('id', $exactMatches);
					$DataProvider = new CActiveDataProvider($model, array('criteria' => $Criteria));
				}
			}

			$this->controller->render('addFind', array(
					'model' => $model,
					'DataProvider' => $DataProvider,
					'transactionTypeSingular' => $transactionTypeSingular,
					'transactionTypeName' => $transactionType,
				)
			);
		}

		protected function processAjaxRequest(Contacts $model) {
			if (Yii::app()->request->isAjaxRequest) {
				if (isset($_POST['Contacts'])) {
					$model->attributes = $_POST['Contacts'];

					$model->scenario = 'addFind';
					$contactValidate = CJSON::decode(CActiveForm::validate($model));
					$emailValidate = CJSON::decode(CActiveForm::validate($model->emails));
					$model->phones->scenario = 'addFind';
					//				$phoneValidate = CJSON::decode(CActiveForm::validate($model->phones));

					$allValidate = CMap::mergeArray($contactValidate, $emailValidate);
					//				$allValidate = CMap::mergeArray($allValidate, $phoneValidate);
					echo CJSON::encode($allValidate);
				}

				Yii::app()->end();
			}
		}

		protected function isExactEmailMatch(Emails $Email) {
			$ids = array();
			$results = $Email->byEmail($Email->email)->findAll();
			if (count($results)) {
				foreach ($results as $model) {
					array_push($ids, $model->id);
				}
			}

			return $ids;
		}

		protected function isExactPhoneMatch(Phones $Phone) {
			$ids = array();
			if ($Phone->phone) {
				$results = $Phone->byPhoneNumber($Phone->phone)->findAll();
				if (count($results)) {
					foreach ($results as $model) {
						array_push($ids, $model->id);
					}
				}
			}

			return $ids;
		}
	}