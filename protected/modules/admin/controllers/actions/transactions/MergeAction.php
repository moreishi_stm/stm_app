<?php

class MergeAction extends CAction {

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run() {
		$TransactionType = Yii::app()->request->getQuery('transactionType');
		$this->controller->title = 'Merge '.$TransactionType;

		$model = Transactions::newInstance($TransactionType, 'search');

		$this->performAjaxRequest($model);

		$this->controller->title=$model->contact->fullName;

		$this->controller->render('merge', array(
			'model'=> $model,
			'TransactionType'=>$TransactionType,
		));
	}
	protected function performAjaxRequest($model) {

		if (Yii::app()->request->isAjaxRequest) {
			if($id = $_POST['id']) {
				$componentTypeId = $model->componentTypeId;
				$model = $model->findByPk($id);
				$data['primaryMerge'] = $this->getTransactionComponentData($model);
				$Transactions = $model->byContactId($model->contact->id)->byComponentTypeId($componentTypeId)->excludeIds($model->id)->findAll();

				if(count($Transactions) > 1) {
					$data['toDeletedropdownList'] = '<option>Select One</option>';
				} else {
					$data['toDeleteMerge'] = $this->getTransactionComponentData($Transactions[0]);
				}

				foreach($Transactions as $Transaction) {
					$label = ($Address = $Transaction->address) ? $Transaction->id.' - '.$Address->address : $Transaction->id;
					$data['toDeletedropdownList'] .= '<option value="'.$Transaction->id.'">'.$label.'</option>';
				}

				echo CJSON::encode($data);
			} elseif(($id = $_POST['toDeleteId']) && ($primaryId = $_POST['primaryId'])) {
				$mergeModel = $model->findByPk($id);
				$primaryModel = $model->findByPk($primaryId);
				$data['toDeleteMerge'] = $this->getTransactionComponentData($mergeModel);
				$data['conflictFields'] = $this->getTransactionFieldsConflictData($primaryModel, $mergeModel);
				echo CJSON::encode($data);
			}

			Yii::app()->end();
		}
	}

	protected function getTransactionComponentData($model) {

		$componentId = $model->id;
		$componentTypeId = $model->componentTypeId;
		$data['transactionId'] = $model->id;
		$data['createDate'] = Yii::app()->format->formatDate($model->added);
		$data['source'] = $model->source->name;
		$data['addressId'] = ($model->address)? $model->address->id: '';
		$data['closings'] = ($model->closing)? 1 : 0;
		$data['activityLog'] = ActivityLog::model()->byComponentTypes($model->getComponentChain())->count(); //@todo: attn! tasks completed that link to activityLog
		$data['tasks'] = Tasks::model()->byComponentTuple(array('componentId'=>$componentId,'componentTypeId'=>$componentTypeId))->count();
		if($Appointment = Appointments::model()->byComponentTuple($componentTypeId, $componentId)->find()) {
			$AppointmentFields = array();
			$AppointmentFields['Set By'] = $Appointment->setBy->fullName;
			$AppointmentFields['Set Datetime'] = Yii::app()->format->formatDateTime($Appointment->set_on_datetime);
			$AppointmentFields['Set Via'] = $Appointment->taskType->name;
			$AppointmentFields['Met Status'] = $Appointment->getModelAttribute('getMetStatusTypes', $Appointment->met_status_ma);
			$AppointmentFields['Set for'] = $Appointment->metBy->fullName;
			$AppointmentFields['Set for Datetime'] = Yii::app()->format->formatDateTime($Appointment->set_for_datetime);
			$AppointmentFields['Location'] = $Appointment->getModelAttribute('getMetLocationTypes',$Appointment->location_ma);
			$AppointmentFields['Location Other'] = $Appointment->location_other;
			$AppointmentFields['Signed'] = StmFormHelper::getYesNoName($Appointment->is_signed);
			$AppointmentFields['Not Signed Reason'] = $Appointment->not_signed_reason_other;

			$data['appointments'] = $AppointmentFields;
		} else {
			$data['appointments'] = '';
		}
		$data['assignments'] = Assignments::model()->byComponentTuple($componentTypeId, $componentId)->count();
		$data['documents'] = Documents::getCount($componentId, $componentTypeId);
		return $data;
	}

	protected function getTransactionFieldsConflictData($primaryModel, $mergeModel) {
		// go through both sets of transaction fields, compare and flag the conflicting ones
		$PrimaryValues = $this->getTransactionValuesArray($primaryModel);
		$MergeValues = $this->getTransactionValuesArray($mergeModel);

		// go through the primary fields, unset the ones that are equal
		foreach($PrimaryValues as $key => $value) {
			if((isset($MergeValues[$key]) && ($value['value'] == $MergeValues[$key]['value']))
				|| (!isset($MergeValues[$key])) || (empty($MergeValues[$key]['value']))) {
				unset($PrimaryValues[$key]);
				unset($MergeValues[$key]);
			}
		}

		foreach($MergeValues as $key => $value) {
			if(!isset($PrimaryValues[$key])) {
				unset($MergeValues[$key]);
			}
		}

		if($PrimaryValues)
			$data['primary'] = $PrimaryValues;

		if($MergeValues)
			$data['toDelete'] = $MergeValues;

		return $data;
	}

	protected function getTransactionValuesArray($model) {

		$FieldValues = $model->transactionFieldValues;
		$transactionValuesArray = array();
		foreach($FieldValues as $Field) {
			$transactionValuesArray[$Field->transaction_field_id] = array('value'=>$Field->value,'id'=>$Field->id,'label'=>$Field->transactionField->label);
		}

		return $transactionValuesArray;
	}

	//@todo: NOTES FOR ACTUAL MERGE. If primary has value && toDelete is empty, keep primary and ignore toDelete. Same with if primary and toDelete is the same.
}