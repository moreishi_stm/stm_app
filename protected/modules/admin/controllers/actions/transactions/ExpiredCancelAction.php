<?php
	class ExpiredCancelAction extends CAction {

		public function run() {

			$transactionType = Yii::app()->request->getQuery('transactionType');
			$model = $this->controller->getSearchModel($transactionType);

			$this->controller->title = 'Expired Cancelled Listings';
			$this->controller->render('listSellerExpiredCancel', array(
					'model' => $model,
				)
			);
		}

		public function printExpiredCancelDate($model) {
			if($model->transaction_status_id == TransactionStatus::EXPIRED_ID) {
				return "<div>".Yii::app()->format->formatGridValue("Expire Date:", (Yii::app()->format->formatDate($model->getFieldValue("listing_expire_date"))? Yii::app()->format->formatDate($model->getFieldValue("listing_expire_date")):"<span class=\"follow-up-flag\">MISSING</span>"))."</div>";

			} elseif($model->transaction_status_id == TransactionStatus::CANCELLED_ID) {
				return "<div>".Yii::app()->format->formatGridValue("Cancel Date:", (Yii::app()->format->formatDate($model->getFieldValue("cancel_date"))? Yii::app()->format->formatDate($model->getFieldValue("cancel_date")):"<span class=\"follow-up-flag\">MISSING</span>"))."</div>";
			}
		}
	}
