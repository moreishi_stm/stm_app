<?php
class ReverseProspectCountAction extends CAction {

	public function run()
    {
        $this->controller->title = 'Reverse Prospect Count';

		$tableName = Yii::app()->user->board->prefixedName;
		$MlsProperties = MlsProperties::model($tableName);

        $this->performAjaxRequest($MlsProperties);

		$this->controller->render('reverseProspectCount', array(
			'MlsProperties' => $MlsProperties,
		));
	}

    protected function performAjaxRequest(MlsProperties $model) {
        if (Yii::app()->request->isAjaxRequest) {
            $tableName = Yii::app()->user->board->prefixedName.'_properties';

            $model->attributes = $_POST['MlsProperties'];

            $where = '';
            if(!empty($model->listing_id)) {
                $where .= (empty($where))? '' : ' AND ';
                $where .= 'listing_id="'.$model->listing_id.'"';
            }

            if(!empty($model->zip)) {
                $where .= (empty($where))? '' : ' AND ';
                $where .= 'zip="'.$model->zip.'"';
            }

            if(!empty($model->price_min)) {
                $where .= (empty($where))? '' : ' AND ';
                $where .= 'price>="'.$model->price_min.'"';
            }

            if(!empty($model->price_max)) {
                $where .= (empty($where))? '' : ' AND ';
                $where .= 'price<="'.$model->price_max.'"';
            }

            if(!empty($model->street_name)) {
                $where .= (empty($where))? '' : ' AND ';
                $where .= 'street_name like "%'.$model->street_name.'%"';
            }

            if(!empty($where)) {
                $d1 = date("Y-m-d", strtotime("-1 month"));
                $d2 = date("Y-m-d", strtotime("-2 month"));
                $d3 = date("Y-m-d", strtotime("-3 month"));

                $listingIds = Yii::app()->stm_mls->createCommand()
                    ->select("listing_id")
                    ->from($tableName)
                    ->where($where)
                    ->queryColumn();

                $listingIds = '"'.implode('","', $listingIds).'"';
                $q1 = 'select count(*) from viewed_pages where listing_id IN ('.$listingIds.') AND added > "'.$d1.'"';
                $q2 = 'select count(*) from viewed_pages where listing_id IN ('.$listingIds.') AND added > "'.$d2.'"';
                $q3 = 'select count(*) from viewed_pages where listing_id IN ('.$listingIds.') AND added > "'.$d3.'"';

                $prospectCount = Yii::app()->db->createCommand()
                    ->select("({$q1}) as d30, ({$q2}) as d60, ({$q3}) as d90")
                    ->from('viewed_pages')
                    ->queryRow();
            }

            echo CJSON::encode($prospectCount);

            Yii::app()->end();
        }
    }
}
