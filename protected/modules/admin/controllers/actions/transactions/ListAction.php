<?php
	class ListAction extends CAction {

		public function run() {

			$transactionType = Yii::app()->request->getQuery('transactionType');
			$model = $this->controller->getSearchModel($transactionType);


            switch($transactionType) {
                case 'buyers':
                    if(Yii::app()->user->checkAccess('noBuyerAccess')) {
                        $this->controller->render('/main/accessDenied', array());
                        Yii::app()->end();
                    }
                    break;

                case 'sellers':
                    if(Yii::app()->user->checkAccess('noSellerAccess')) {
                        $this->controller->render('/main/accessDenied', array());
                        Yii::app()->end();
                    }
                    break;
            }

            if($this->id=='my') {
                $_REQUEST[get_class($model)]['assignmentId'] = $model->assignmentId = Yii::app()->user->id;
            }

            if(isset($_GET['export']) && $_GET['export']==1){
                $DataProvider = $this->controller->getTransactionSearchProvider();
                $DataProvider->pagination = False;
                $transactions = $DataProvider->data;
                unset($DataProvider);
                $contactIds = array();
                foreach($transactions as $key => $transaction) {
                    array_push($contactIds, $transaction->contact_id);
                    unset($transactions[$key]);
                }

                $criteria = new CDbCriteria;
                $criteria->addInCondition('id',$contactIds);
                $contacts = Contacts::model()->findAll($criteria);
                $exportData = Contacts::model()->getExportData($contacts);
                $date = date('Y-m-d');
                header( "Content-Type: application/vnd.ms-excel; charset=utf-8" );
                header( "Content-Disposition: inline; filename=\"{$transactionType}_contacts_export_{$date}.xls\"" );
                echo $exportData;
                Yii::app()->end();
            }

			$this->controller->title = 'All ' . ucwords($transactionType);
			$this->controller->render('list', array(
					'model' => $model,
				)
			);
		}
	}
