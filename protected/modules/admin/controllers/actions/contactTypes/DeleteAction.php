<?php

class DeleteAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'Delete Contact Type';
		$model=$this->controller->loadModel($id);

        if(count($model->contactTypeLus)>0) {
            Yii::app()->user->setFlash('error', 'Contact Type has associated contacts. Please remove associations before deleting.');
            $this->controller->redirect(array('index'));
        } else {
            $model->delete();
            if ($model->is_deleted) {
                Yii::app()->user->setFlash('success', 'Successfully deleted Contact Type.');
                $this->controller->redirect(array('index'));
            }
        }

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}