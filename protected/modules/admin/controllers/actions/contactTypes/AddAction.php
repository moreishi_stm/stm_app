<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Add Contact Type';
		$model = $this->controller->baseModel;

		if (isset($_POST['ContactTypes'])) {
			$model->attributes = $_POST['ContactTypes'];
			$model->account_id = Yii::app()->user->accountId;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Added Contact Type.');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model,
		));
	}
}