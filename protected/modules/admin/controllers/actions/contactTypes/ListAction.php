<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Contact Types';
		$model=$this->controller->baseModel;
		$model->unsetAttributes();  // clear any default values

        // used for gridview search box inputs
        if (isset($_GET['ContactTypes']))
            $model->attributes=$_GET['ContactTypes'];

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
