<?php

class EditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'Edit Contact Type';
		$model=$this->controller->loadModel($id);

		if (isset($_POST['ContactTypes'])) {
			$model->attributes = $_POST['ContactTypes'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Updated Contact Type.');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}