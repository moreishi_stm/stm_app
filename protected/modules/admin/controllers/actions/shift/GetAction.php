<?php

class GetAction extends CAction {

	public function run($id) {

		$this->processAjaxRequest($id);
	}

	protected function processAjaxRequest($id) {

		if (Yii::app()->request->isAjaxRequest) {

			$shifts = LeadShift::model()->findAll(array('condition'=>'lead_route_id=:lead_route_id', 'params'=>array(':lead_route_id'=>$id)));
			if ($shifts) {

				$shiftData = array();
				foreach($shifts as $shift) {

					array_push($shiftData, array(
						'title'   => $shift->contact->fullName,
						'start'   => $shift->calendarStartDateTime,
						'end'     => $shift->calendarEndDateTime,
						'shiftId' => $shift->id,
						'allDay'  => false,
					));
				}

				echo CJSON::encode($shiftData);
			}

			Yii::app()->end();
		}
	}
}