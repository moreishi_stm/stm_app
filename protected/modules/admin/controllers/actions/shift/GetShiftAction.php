<?php

class GetShiftAction extends CAction {

	public function run($id) {

		$this->processAjaxRequest($id);
	}

	protected function processAjaxRequest($id) {

		if (Yii::app()->request->isAjaxRequest) {

			$shift = LeadShift::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$id)));
			if ($shift) {

				$shiftData = array(
						'title'   => $shift->contact->fullName,
						'contactId' => $shift->contact->id,
						'date' => Yii::app()->format->formatDate($shift->calendarStartDateTime),
						'startTime' => Yii::app()->format->formatTime($shift->calendarStartDateTime),
						'endTime' => Yii::app()->format->formatTime($shift->calendarEndDateTime),
						'shiftId' => $shift->id,
						'allDay'  => false,
					);

				echo CJSON::encode($shiftData);
			}

			Yii::app()->end();
		}
	}
}