<?php

class AddAction extends CAction {

	public function run() {

		$model = new LeadShift;
		$this->processAjaxRequest($model);
	}

	protected function processAjaxRequest(LeadShift $model) {

		if (Yii::app()->request->isAjaxRequest) {

			if (isset($_POST['LeadShift'])) {

				$model->attributes = $_POST['LeadShift'];

				// If the model fails to save then return the validation json
				if (!$model->save())
					echo CActiveForm::validate($model);
			}

			Yii::app()->end();
		}

	}
}