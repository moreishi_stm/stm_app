<?php

class DeleteAction extends CAction {

	public function run() {

		if (isset($_POST['LeadShift'])) {

			$shiftId = $_POST['LeadShift']['id'];

            if($model = LeadShift::model()->findByPk($shiftId)) {
                $model->delete();
                echo CActiveForm::validate($model);
            }
			Yii::app()->end();
		}
	}
}