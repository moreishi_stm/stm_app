<?php

class EditAction extends CAction {

	public function run($id) {

		$this->processAjaxRequest($id);
	}

	protected function processAjaxRequest($id) {

		if (Yii::app()->request->isAjaxRequest) {

			// Need to ensure the lead route id since we use a separate form for the removal process
			$leadShift = LeadShift::model()->findByPk($id);
			$leadShift->attributes = $_POST['LeadShift'];
			if (!$leadShift->save()) {
				echo CActiveForm::validate($leadShift);
			}

			Yii::app()->end();
		}
	}
}