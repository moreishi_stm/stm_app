<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
class ViewAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		$model = $this->controller->loadModel($id);
		$this->controller->title = 'Project '.$model->name;
		$componentTuple = $model->asComponentTuple();

		$DateRanger = new DateRanger();
		$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

		if (!empty($_GET['task_types'])) {
			$taskTypeIds = array($_GET['task_types']);
		} else {
			$taskTypeIds = null;
		}
		$projectItemsModel = new ProjectItems;
		$projectItemsModel->unsetAttributes();
		$projectItemsModel->byDueDateDesc()->byPriority();
		$projectItemsModel->project_id = $id;

        $overdueTask = new Tasks;
        $overdueCritera = $overdueTask->overdue()->byPriority()->byComponentTuple($componentTuple)->byTaskType($taskTypeIds)->notCompleted()->asc()->getDbCriteria();
        $overdueCount = $overdueTask->count($overdueCritera);

        $dataTask = new Tasks;
        $DataProvider = new CActiveDataProvider('Tasks', array(
            'criteria'=>$dataTask->byComponentTuple($componentTuple)->notCompleted()->asc()->byPriority()->byTaskType($taskTypeIds)->getDbCriteria(),
																																			 'pagination' => array(
																																				 'pageSize' => 100,
																																			 ),
																																			 ));

        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);

        $parentModel = new Projects;
		$parentModel->parent_id = $id;

		$this->controller->render('view',array(
			'model'=>$model,
			'parentModel'=>$parentModel,
			'projectItemsModel'=>$projectItemsModel,
			'dataProvider' => $DataProvider,
			'overdueCount' => $overdueCount
		));
	}
}