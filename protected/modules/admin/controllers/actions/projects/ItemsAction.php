<?php

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class ItemsAction extends CAction {

		public function run($id) {
			$this->controller->pageTitle = 'Project Item';

			$ProjectItem = ProjectItems::model()->findByPk($id);

			if (isset($_POST['ProjectItems'])) {
				$ProjectItem->attributes = $_POST['ProjectItems'];

				if ($ProjectItem->save()) {
					Yii::app()->user->setFlash('success', 'Successfully updated Project Item!');

					$this->controller->redirect(array(
							'view',
							'id' => $ProjectItem->project_id
						)
					);
				}
			}

			$this->controller->render('formItem', array(
					'model' => $ProjectItem
				)
			);
		}
	}
