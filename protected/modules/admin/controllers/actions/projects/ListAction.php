<?php

class ListAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Projects List';

		$model = new Projects('search');
		$model->byDueDateDesc()->byPriorityByName();
		$model->unsetAttributes();  // clear any default values
        $model->status_ma = StmFormHelper::ACTIVE;
        $model->contact_id = Yii::app()->user->id;

		// used for gridview search box inputs
        if (isset($_GET['Projects'])) {
			$model->attributes=$_GET['Projects'];
            if(isset($_GET['Projects']['showOthersProjects']) && $_GET['Projects']['showOthersProjects']==StmFormHelper::NO) {
                $model->contact_id = null;
            }
            if(isset($_GET['Projects']['assignedTo']) && !empty($_GET['Projects']['assignedTo'])) {
                $model->assignedTo = $_GET['Projects']['assignedTo'];
            }
        }

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}