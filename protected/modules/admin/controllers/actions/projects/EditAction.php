<?php

	class EditAction extends CAction {

		/**
		 * Manages models
		 */
		public function run($id) {
			$model = $this->controller->loadModel($id);
			$this->controller->title = 'Edit Project';

			if (isset($_POST['Projects'])) {
				$model->attributes = $_POST['Projects'];

				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Successfully updated Project.');
					$this->controller->redirect(array(
							'view',
							'id' => $model->id
						)
					);
				}
			}

			$this->controller->render('form', array(
					'model' => $model
				)
			);
		}
	}