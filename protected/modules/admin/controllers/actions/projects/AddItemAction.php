<?php

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class AddItemAction extends CAction {

		public function run($id) {
			$this->controller->title = 'Add Project Item';
			$Project = $this->controller->loadModel($id);
			$model = new ProjectItems;
			$model->project_id = $id;
			$model->project = $Project;

			if (isset($_POST['ProjectItems'])) {
				$model->attributes = $_POST['ProjectItems'];

				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Successfully added Project Item.');
					$this->controller->redirect(array('/'.Yii::app()->controller->module->id.'/projects/view', 'id' => $model->project_id));
				}
			}

			$this->controller->render('formItem', array(
					'model' => $model,
				)
			);
		}
	}
