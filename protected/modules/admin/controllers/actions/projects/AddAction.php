<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction {

	public function run() {
		$this->controller->title = 'Add Project';
		$model = new Projects;

		if (isset($_POST['Projects'])) {
			$model->attributes = $_POST['Projects'];

			$model->account_id = Yii::app()->user->accountId;
			$model->contact_id = Yii::app()->user->id;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully added Project.');
				$this->controller->redirect(array(
											'view',
											'id' => $model->id
											)
				);
			}
		}

		$this->controller->render('form', array(
			'model' => $model,
		));
	}
}
