<?php

class ListAction extends CAction
{
    public function run()
    {
        $this->controller->title = 'Call Hunt Groups List';

        $model = $this->controller->baseModel;
        $model->unsetAttributes();
        // Render the view
        $this->controller->render('index', array(
            'model' =>  $model
        ));
    }
}