<?php

class ListPhoneAction extends CAction
{
    public function run($id=null)
    {
        $model = new CallHuntGroupPhones();

        if($id) {
            $model->call_hunt_group_id = $id;
        }

        // Render the view
        $this->controller->render('listPhone', array(
            'model' => $model
        ));
    }
}