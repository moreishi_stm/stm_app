<?php

class EditPhoneAction extends CAction
{
    public function run($id)
    {
        $model = CallHuntGroupPhones::model()->findByPk($id);

        $this->controller->title = 'Edit Hunt Group Phone';

        if (isset($_POST['CallHuntGroupPhones'])) {

            $model->attributes = $_POST['CallHuntGroupPhones'];
            $model->updated = new CDbExpression('NOW()');
            $model->updated_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated hunt group.');
                $this->controller->redirect(array('index'));
            }
        }

        // Render view
        $this->controller->render('formPhone', array(
            'model' =>  $model,
        ));
    }
}