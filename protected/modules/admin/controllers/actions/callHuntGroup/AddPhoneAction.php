<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddPhoneAction extends CAction
{
    public function run($id=null)
    {
        $this->controller->title = 'Add Hunt Group Phone';
        $model = new CallHuntGroupPhones();

        if($id) {
            $model->call_hunt_group_id = $id;
        }

        if (isset($_POST['CallHuntGroupPhones'])) {

            $model->attributes = $_POST['CallHuntGroupPhones'];
            $model->added = new CDbExpression('NOW()');
            $model->added_by = Yii::app()->user->id;
            $model->updated = new CDbExpression('NOW()');
            $model->updated_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added hunt group phone!');
                $this->controller->redirect(array('index'));
            }
        }

        // Render view
        $this->controller->render('formPhone', array(
            'model'=>$model,
        ));
    }
}