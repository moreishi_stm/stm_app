<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction
{
    public function run()
    {
        $this->controller->title = 'Add Hunt Group';
        $model = $this->controller->baseModel;

        if (isset($_POST['CallHuntGroups'])) {

            $model->attributes = $_POST['CallHuntGroups'];
            $model->added = new CDbExpression('NOW()');
            $model->added_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added hunt group!');
                $this->controller->redirect(array('index'));
            }
            else {
                Yii::app()->user->setFlash('error', 'Error saving Hunt Group: '.current($model->getErrors())[0]);
            }
        }

        // Render view
        $this->controller->render('form', array(
            'model'=>$model,
        ));
    }
}