<?php

class EditAction extends CAction
{
    public function run($id)
    {
        $model = $this->controller->loadModel($id);

        $this->controller->title = 'Edit Hunt Group';

        if (isset($_POST['CallHuntGroups'])) {

            $model->attributes = $_POST['CallHuntGroups'];
            $model->updated = new CDbExpression('NOW()');
            $model->updated_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated hunt group.');
                $this->controller->redirect(array('index'));
            }
        }

        // Render view
        $this->controller->render('form', array(
            'model' =>  $model,
        ));
    }
}