<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Transaction Tags';
		$model=$this->controller->baseModel;
		$model->unsetAttributes();  // clear any default values

        // used for gridview search box inputs
        if (isset($_GET['TransactionTags']))
            $model->attributes=$_GET['TransactionTags'];

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
