<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Add Transaction Tag';
		$model = $this->controller->baseModel;

		if (isset($_POST['TransactionTags'])) {
			$model->attributes = $_POST['TransactionTags'];
			$model->account_id = Yii::app()->user->accountId;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Added Transaction Tag.');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model,
		));
	}
}