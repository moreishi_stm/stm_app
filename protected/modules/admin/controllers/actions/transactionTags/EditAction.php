<?php

class EditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'Edit Transaction Tag';
		$model=$this->controller->loadModel($id);

		if (isset($_POST['TransactionTags'])) {
			$model->attributes = $_POST['TransactionTags'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Updated Transaction Tag.');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}