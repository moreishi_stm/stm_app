<?php

class DeleteAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'Delete Transaction Tag';
		$model=$this->controller->loadModel($id);

        if(count($model->transactionTagLus)>0) {
            Yii::app()->user->setFlash('error', 'Contact Type has associated contacts. Please remove associations before deleting.');
            $this->controller->redirect(array('index'));
        } else {
            $model->delete();
            if ($model->is_deleted) {
                Yii::app()->user->setFlash('success', 'Successfully deleted Transaction Tags.');
                $this->controller->redirect(array('index'));
            }
        }

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}