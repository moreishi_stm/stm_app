<?php

class ViewAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		$marketTrendData = new MarketTrendData;
		$marketTrendData->market_trend_id = $id;

		$this->controller->render('view',array(
			'model'=>$this->controller->loadModel($id),
			'marketTrendData'=>$marketTrendData,
		));
	}
}