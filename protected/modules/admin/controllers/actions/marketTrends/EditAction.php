<?php

class EditAction extends CAction
{
	public function run($id)
	{

		$model=$this->controller->loadModel($id);

		if (isset($_POST['MarketTrends'])) {
			$model->attributes = $_POST['MarketTrends'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully updated Market Trend.');
				$this->controller->redirect(array('view','id'=>$model->id));
			}
		}

		$this->controller->render('form', array(
			'model'=>$model,
		));
	}
}