<?
class AddAction extends CAction {

	public function run() {
		$model = $this->controller->baseModel;

		if (isset($_POST['MarketTrends'])) {
			$model->attributes = $_POST['MarketTrends'];

			$model->contact_id = Yii::app()->user->id;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully added Market Trend.');
				$this->controller->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->controller->render('form', array(
			'model' => $this->controller->baseModel,
		));
	}
}