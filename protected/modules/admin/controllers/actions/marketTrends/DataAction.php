<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class DataAction extends CAction {

	public function run($id) {
		$models = array();
		// Process additional e-mails for the contact if any
		$marketDataValidated = true;
  // var_dump($_POST['MarketTrendData']); exit;
		if (isset($_POST['MarketTrendData'])) {
			foreach ($_POST['MarketTrendData'] as $i => $marketData) {
				if ($marketData['id']) {
					$Criteria = new CDbCriteria;
					$Criteria->condition = 'id = :id';
					$Criteria->params = array(
						':id'=>$marketData['id'],
					);
					$MarketTrendData = MarketTrendData::model()->find($Criteria);
                } else {
					$MarketTrendData = new MarketTrendData;
                }

				$MarketTrendData->attributes = $marketData;

				if ($MarketTrendData->remove) {
				     $MarketTrendData->delete();
				     continue;
				}

				//  Mark that the market data had a failure
                if (!$MarketTrendData->validate() && $marketDataValidated) {
					$marketDataValidated = false;
                }

				array_push($models, $MarketTrendData);
			}

			if ($marketDataValidated) {
				if ($models) {
					foreach ($models as $MarketTrendData) {
						if ($MarketTrendData->isNewRecord) {
							$MarketTrendData->market_trend_id = $id;
						}

						$MarketTrendData->save(false); // already ran validation
					}
				}
			}

			Yii::app()->user->setFlash('success', 'Successfully updated Content!');
			$this->controller->redirect(array('view','id'=>$model->id));
		} else {
			$model = MarketTrendData::model()->findAll(array(
			    'condition'=>'market_trend_id=:market_trend_id',
			    'params'=>array(':market_trend_id'=>$id),
			    'order'=>'date asc',
			));
		}

		Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl.DS.'stm_MarketTrendsGui.js');

		$this->controller->render('formData',array(
			'model'=>$model,
			'marketTrendname'=>MarketTrends::model()->findByPk($id)->name,
		));
	}
}
