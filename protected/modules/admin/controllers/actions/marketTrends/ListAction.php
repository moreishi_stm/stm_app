<?php

// class ListAction extends CAction
// {
// 	/**
// 	 * Manages models
// 	 */
// 	public function run()
// 	{
// 		// $model=new LeadRoutes('search');
// 		// $model->unsetAttributes();  // clear any default values

// 		// $RulesModel=new LeadRouteRules('search');
// 		// $RulesModel->unsetAttributes();  // clear any default values


// 		// // used for gridview search box inputs
// 		// if (isset($_GET['LeadRoutes']))
// 		// 	$model->attributes=$_GET['LeadRoutes'];

// 		// if (isset($_GET['LeadRouteRules']))
// 		// 	$model->attributes=$_GET['LeadRouteRules'];

// 		// $this->controller->render('list',array(
// 		// 	'model'=>$model,
// 		// 	'RulesModel'=>$RulesModel,
// 		// ));
// 	}
// }

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class ListAction extends CAction
{
	public function run()
	{
		// $this->controller->pageColor = 'blue';
		// $this->controller->baseModel = new MarketTrends('search');
		// $this->controller->baseModel->unsetAttributes();  // clear any default values
		// $this->controller->baseModel->id = $id;

		// switch ($id) {
		// 	case null && $action == null:
		// 		$viewName = 'marketTrendsList';
		// 		$mode = 'List';
		// 		break;

		// 	case $id == 'add':
		// 		$viewName = 'marketTrendsForm';
		// 		$mode = 'Add';
		// 		break;

		// 	case is_numeric($id):
		// 		$viewName = 'marketTrendsForm';
		// 		$mode = 'Edit';
		// 		$this->controller->baseModel = $this->controller->baseModel->findByPk($id);
		// 		break;
		// }

		$this->controller->render('list', array(
			'model'=>$this->controller->baseModel,
		));
	}
}