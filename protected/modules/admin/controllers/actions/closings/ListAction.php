<?php
	class ListAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {
			// $TransactionType = $this->controller->transactionType;
			$this->controller->title = 'All Closings';

			$model = new Closings;
			$model->unsetAttributes(); // clear any default values

			if (isset($_GET['Closings'])) {
				$model->attributes = $_GET['Closings'];
			} else {
				$model->closing_status_ma = 1;
			}

			$model->transaction = new Transactions('search');

			$model->transaction->contact = new Contacts('search');
			if (isset($_GET['Contacts'])) {
				$model->transaction->contact->attributes = $_GET['Contacts'];
			}

			$model->transaction->contact->emails = new Emails('search');
			if (isset($_GET['Emails'])) {
				$model->transaction->contact->emails->attributes = $_GET['Emails'];
			}

			$model->transaction->address = new Addresses('search');
			if (isset($_GET['Addresses'])) {
				$model->transaction->address->attributes = $_GET['Addresses'];
			}


            if($_GET['Closings']['assignmentIds']) {
                $model->assignmentIds = $_GET['Closings']['assignmentIds'];
            }

            switch(1) {
                case (Yii::app()->user->checkAccess('noSellerAccess')):
                case (Yii::app()->user->checkAccess('noBuyerAccess')):
                    $model->assignmentIds = Yii::app()->user->id;
                    break;
            }
			if(isset($_GET["export"]) && $_GET["export"] === "1"){
				return $this->exportCsv($model);
			}
            $this->controller->render('list', array(
					'model' => $model,
				)
			);
		}

		/**
		 *
		 * Name exportCsv
		 * @param Closings $closings
		 * @return csv file
		 *
		 * @author jamesk
		 * @author 813.330.0522
		 *
		 */
		private function exportCsv(Closings $closings){
			if(empty($closings)) {
				return null;
			}
			/**
			 * IDK why i need search() and getdata()
			 */
			$closings = $closings->search();
			//die("data:<pre>".print_r($closings->getData(),1));
			$closings = $closings->getData();
			$csvData = array();
			/**
			 * start with the header row
			 */
			foreach ($this->csvColumns() as $col){
				$csvRow[] = $col["label"];
			}
			$csvData[] = implode("\t",$csvRow);
			foreach( $closings as $closing ) {
				$data = $closing->attributes;
				//die("data:<pre>".print_r($data,1));
				//die("data:<pre>".print_r($closing->transaction->address->state->attributes,1));
				//die("data:<pre>".print_r($closing->transaction->contact->attributes,1));
				$data["first_name"] = $closing->transaction->contact->first_name;
				$data["last_name"] = $closing->transaction->contact->last_name;
				$data["spouse_first_name"] = $closing->transaction->contact->spouse_first_name;
				$data["spouse_last_name"] = $closing->transaction->contact->spouse_last_name;

                // closing fields
                $data["closing_date"] = Yii::app()->format->formatDate($closing->actual_closing_datetime);
				$data["closing_address"] = $closing->transaction->address->address;
				$data["closing_city"] = $closing->transaction->address->city;
				$data["closing_state"] = $closing->transaction->address->state->name;
				$data["closing_zip"] = $closing->transaction->address->zip;

				if($emails = $closing->transaction->contact->emails) {
					$maxEmails = 4;
					$num = 1;
					foreach($emails as  $email) {
						if($num <= $maxEmails) {
							$data["email_address_".$num] = $email->email;
                            $num++;
						}
					}

				}

				if($phones = $closing->transaction->contact->phones) {
					$num =1;
					$maxPhones = 4;
					foreach($phones as $phone) {
						if($num <= $maxPhones) {
							$data["phone_".$num] = Yii::app()->format->formatPhone($phone->phone);
                            $num++;
						}
					}
				}

				$csvRow = array();
				foreach ($this->csvColumns() as  $col){
					//die("<pre>".print_r($data,1));
					if( !empty($col["key"]) && isset($data[$col["key"]]) ){
						$csvRow[] = $data[$col["key"]];
					}else{
						$csvRow[] = " ";
					}
				}
				//die("data:<pre>".print_r($data,1));
				$csvData[] = implode("\t",$csvRow);
			}
			$date = date('Y-m-d');
			header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			header("Content-Disposition:  inline; filename=\"closings_export_{$date}.xls\"" );
			header("Cache-Control: max-age=0");
			echo implode(PHP_EOL,$csvData);
			return;
		}

		/**
		 *
		 * Name csvColumns
		 * @return array
		 *
		 * @author jamesk
		 * @author 813.330.0522
		 *
		 * @important in order to arrange the position and data of the columns
		 * 	displayed adjust the order / keynames accordingly
		 */
		protected function csvColumns(){
			return array(
				array(
					"key" => "id",
					"label" => "ID"
				),
				array(
					"key" => "first_name",
					"label" => "First Name"
				),
				array(
					"key" => "last_name",
					"label" => "Last Name"
				),
				array(
					"key" => "spouse_first_name",
					"label" => "Spouse First Name"
				),
				array(
					"key" => "spouse_last_name",
					"label" => "Spouse Last Name"
				),
				array(
					"key" => "email_address_1",
					"label" => "Email 1"
				),
				array(
					"key" => "email_address_2",
					"label" => "Email 2"
				),array(
					"key" => "email_address_3",
					"label" => "Email 3"
				),array(
					"key" => "email_address_4",
					"label" => "Email 4"
				),
				array(
					"key" => "phone_1",
					"label" => "Phone 1"
				),
				array(
					"key" => "phone_2",
					"label" => "Phone 2"
				),
				array(
					"key" => "phone_3",
					"label" => "Phone 3"
				),
				array(
					"key" => "phone_4",
					"label" => "Phone 4"
				),
				array(
					"key" => "closing_address",
					"label" => "Closing Address"
				),
				array(
					"key" => "closing_city",
					"label" => "Closing City"
				),
				array(
					"key" => "closing_state",
					"label" => "Closing State"
				),
				array(
					"key" => "closing_zip",
					"label" => "Closing Zip"
				),
				array(
					"key" => "closing_date",
					"label" => "Closing Date"
				)
			);
		}
	}