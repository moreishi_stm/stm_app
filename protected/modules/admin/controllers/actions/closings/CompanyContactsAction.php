<?php
class CompanyContactsAction extends CAction {

    private $_validTypes = array('title', 'otheragent', 'lender');

    public function run($type) {

        // Get the assignment types
        $types = $this->getAssignmentTypes($type);
        $assignmentTypes = AssignmentTypes::model()->byTypes($types)->findAll();

        // Get the company so we can gather the contacts for the company
        $companyId = $this->getCompanyId($type);
        $company = Companies::model()->findByPk($companyId);

        // Check for invalid data
        if (!$assignmentTypes || (!$company || !$company->contacts)) {
            return null;
        }

        // Get the closing id, we need it to do a lookup for existing contacts
        $closingId = $_POST['Closings']['id'];

        // Echo out the options for the combobox in the format of <optgroup label=[Contact]><option>[AssignmentTypes]</option></optgroup>
		echo CHtml::openTag('optgroup', array('label' => CHtml::encode($company->name)));
        foreach ($company->contacts as $contact) {
            foreach ($assignmentTypes as $assignmentType) {
                $contactAlreadyAdded = Assignments::model()->find(array(
                    'condition' => 'assignment_type_id = :assignment_type_id AND assignee_contact_id = :assignee_contact_id AND
                        component_id = :component_id AND assignee_contact_id is not null',
						'params' => array(
							':assignment_type_id' => $assignmentType->id,
							':assignee_contact_id' => $contact->id,
							':component_id' => $closingId,
						),
                ));

                echo CHtml::tag('option', array(
                    'value' => $assignmentType->id.'-'.$contact->id,
                    'selected' => ($contactAlreadyAdded) ? 'selected' : '',
                ), CHtml::encode($contact->fullName.((count($types)>1)? ' ('.$assignmentType->display_name.')' : '')), true); //$assignmentType->id == AssignmentTypes::TITLE_AGENT || $assignmentType->id == AssignmentTypes::TITLE_PROCESSOR
            }
//            echo CHtml::closeTag('optgroup');
        }
		echo CHtml::closeTag('optgroup');
    }

    protected function getAssignmentTypes($type) {

       $assignmentTypes = array();
       switch ($type) {
           case 'title':
               $assignmentTypes = $this->getTitleTypes();
           break;

           case 'otheragent':
               $assignmentTypes = $this->getOtherAgentTypes();
           break;

           case 'lender':
               $assignmentTypes = $this->getLenderTypes();
           break;

           default: break;
       }

       return $assignmentTypes;
    }

    protected function getCompanyId($type) {

        if (!isset($_POST['Closings']))
            return null;

        $companyId = null;
        switch ($type) {
            case 'title':
                $companyId = $_POST['Closings']['title_company_id'];
            break;

            case 'otheragent':
                $companyId = $_POST['Closings']['other_agent_id'];
            break;

            case 'lender':
                $companyId = $_POST['Closings']['lender_id'];
            break;

            default: break;
        }

        return $companyId;
    }

    protected function getTitleTypes() {

        return array(AssignmentTypes::TITLE_AGENT, AssignmentTypes::TITLE_PROCESSOR);
    }

    protected function getOtherAgentTypes() {

        return array(AssignmentTypes::CO_BROKER_AGENT);
    }

    protected function getLenderTypes() {

        return array(AssignmentTypes::LOAN_PROCESSOR, AssignmentTypes::LOAN_OFFICER);
    }
}
