<?php

class AgentsAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		// $documentsProvider = Documents::getAll($id, $this->controller->transactionType->id);
		$this->controller->renderPartial('_agents',array(
			// 'dataProvider'=>$documentsProvider,
		));
	}
}