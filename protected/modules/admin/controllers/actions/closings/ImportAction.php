<?php
/**
 * Import Action
 *
 * Used to import closings
 */
class ImportAction extends CAction
{
    protected $_importErrorRows = array();
    protected $_startTime;
    protected $_duplicateCount = 0;
    protected $_contactMissingDataCount = 0;

    protected $_sourceCollection;
    protected $_contactTypeCollection;

    /**
     * Send JSON
     *
     * Test function that should be moved up the chain of hierarchy later if found useful
     * @return void
     */
    protected function _sendJson($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    /**
     * Run
     *
     * This method is called when the page is loaded
     * @return void
     */
    public function run()
    {
        // Handle postback vs rendering page content
        if(Yii::app()->request->isPostRequest) {

            // Handle each type of post
            switch(Yii::app()->request->getPost('method')) {
                case 'preview-file':
                    $this->_handlePreviewFile();
                    break;
                case 'import':
                    $this->_performImport();
                break;
            }
        }

        // Set page title
        $this->controller->title = 'Import Closings';

        // Pass data to view
        $this->controller->render('import', array(
            // Data can be passed in to view here
        ));
    }

    /**
     * Handle Import
     *
     * Helper function to handle the actual import process
     * @return void
     */
    protected function _performImport()
    {
        $pw = Yii::app()->request->getPost('pw');
        if($pw !== 'stmstm123') {
            $this->_sendJson(array(
                    'status'    =>  'error',
                    'message'   =>  'Invalid password.'
                ));
        }

        // clear the log
        $this->_startTime = microtime(true);

        // Retrieve and decode JSON for contacts to import
        $newRecords = CJSON::decode(Yii::app()->request->getPost('data'));
        if(!$newRecords) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'No contacts have been selected'
            ));
        }

        list($stateFullNames, $stateShortNames, $emailsExisting, $namesExisting, $phonesExisting, $addressesExisting) = StmImportFunctions::initialData();

        // Store all new contacts
        foreach($newRecords as $newRecord) {

            // reset contact variable
            $contact = null;

            // do this until we have a field for this. This is used on contacts. Keep this on top so every record has the same number of fields.
            if(!$newRecord['added']) {
                $newRecord['added'] = Yii::app()->format->formatDate($newRecord['contract_close_date'], StmFormatter::MYSQL_DATETIME_FORMAT);
            }

            // trims and lower cases all email fields
            StmImportFunctions::formatEmails($newRecord, 1, 4);

            // If we don't have any emails, skip this
            if(empty(trim($newRecord['email_1'])) && empty(trim($newRecord['email_2'])) && empty(trim($newRecord['email_3'])) && empty(trim($newRecord['email_4']))) {

                $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Missing email. This record has not been imported.');
                continue;
            }

            // strips all non-numeric characters from all phone fields
            StmImportFunctions::formatPhones($newRecord, 1, 4);

            // If we don't have any phone numbers, skip this
//            if(empty($newRecord['phone_1']) && empty($newRecord['phone_2']) && empty($newRecord['phone_3']) & empty($newRecord['phone_4'])) {
//                $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Missing phone. This record has not been imported.');
//                continue;
//            }

            // emails matches are found, verify further and merge with existing contact if match confirmed
            if(StmImportFunctions::fieldsetExists($newRecord, 'email', $emailsExisting, 1, 4, false)) {

                // get all contactIds associated with email data set
                $emails = StmImportFunctions::getFieldSet($newRecord, 'email', 1, 4);
                $contactIds = StmImportFunctions::getContactIdsByEmails($emails);

                // if only ONE contact exists for email data set, merge contact data. Else not, do not import, send error message.
                if(count($contactIds) == 1) {

                    // Exact single email/contact match. Merge any additional contact info.
                    $mergeResult = StmImportFunctions::mergeContactInfo($contactIds[0], Yii::app()->user->accountId, $newRecord, $numEmailFields=4, $numPhoneFields=4 , $emailsExisting, $phonesExisting, $this->_importErrorRows);
                    if($mergeResult['status'] == 'success') {
                        $contact = $mergeResult['contact'];
                    }
                    elseif($mergeResult['status'] == 'error') {
                        // skip if contact merge has errors, errors already logged in merge function.
                        continue;
                    }
                }
                elseif(count($contactIds) > 1) {

                    // error if found more than 1 contact, send error message and do not import. Contact should be consolidated.
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Multiple contacts found for this record emails ('.implode(',', $contactIds).' | Emails: '.implode(',',$emails).'). Needs further review. This record has not been imported.');
                    continue;
                }
            }
            // phones matches are found, verify further and merge with existing contact if match confirmed
            elseif(StmImportFunctions::fieldsetExists($newRecord, 'phone', $phonesExisting, 1, 4, false)) {

                // get all contactIds associated with phone data set
                $phones = StmImportFunctions::getFieldSet($newRecord, 'phone', 1, 4);
                $contactIds = StmImportFunctions::getContactIdsByPhones($phones);

                // if only ONE contact exists for phone data set, merge contact data. Else not, do not import, send error message.
                if(count($contactIds) == 1) {

                    // if first and last name match and matching address, merge contact data.
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, '1 contact found for phones ( Contact Ids:'.implode(',', $contactIds).' | Phones: '.implode(',',$phones).'). Add an email match and re-run to automatically contact merge this data. This record has not been imported.');
                    continue;

                }
                elseif(count($contactIds) > 1) {
                    // error if found more than 1 contact, send error message and do not import. Contact should be consolidated.
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Multiple contacts found for phones ( Contact Ids:'.implode(',', $contactIds).' | Phones: '.implode(',',$phones).'). Needs further review. This record has not been imported.');
                    continue;
                }
            }
            // address matches are found, verify further and merge with existing contact if match confirmed
            elseif(array_key_exists(strtolower($newRecord['address']), $addressesExisting)) {

                // get all contactIds associated with address data set
                $contactIds = StmImportFunctions::getContactIdsByFullNameAddress($newRecord['first_name'], $newRecord['last_name'], $newRecord['address'], $newRecord['zip']);

                if(count($contactIds) == 1) {

                    // existing contacts will not be imported for now.
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Existing address with First & Last name match. Compare name, phone email, address for inconsistent data sets. Needs further review. This record has not been imported.');
                    continue;
                }
                elseif(count($contactIds) > 1) {
                    // error if found more than 1 contact, send error message and do not import. Contact should be consolidated.
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Multiple contacts found for address ('.implode(',', $contactIds).' | Address: '.$newRecord['address'].'). Needs further review. This record has not been imported.');
                    continue;
                }
            }

            // if contact still blank after email, phone, address check, create contact
            if(!$contact) {
                // new contact not successfully created, skip and send error message.
                if(!($contact = StmImportFunctions::addNewContact($newRecord, $this->_importErrorRows))) {

                    // error already logged from StmImportFunctions, no need to log again. Just skip record.
                    continue;
                }
            }
            // contact was found, add fields to existing contact record assigned to $contact.
            else {
                $newRecord['price'] = Yii::app()->format->formatInteger($newRecord['price']);
                if(!$newRecord['price']) {
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Closing Price missing. This record has not been imported.');
                    continue;
                }

                if(!$newRecord['contract_execute_date'] || $newRecord['contract_execute_date']==0) {
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Closing Contract Execute Date missing. This record has not been imported.');
                    continue;
                }
                $newRecord['contract_execute_date'] = Yii::app()->format->formatDate($newRecord['contract_execute_date'], StmFormatter::MYSQL_DATE_FORMAT);

                if(!$newRecord['contract_close_date'] || $newRecord['contract_close_date']==0) {
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Closing Contract Closing Date missing. This record has not been imported.');
                    continue;
                }
                $newRecord['contract_close_date'] = Yii::app()->format->formatDate($newRecord['contract_close_date'], StmFormatter::MYSQL_DATE_FORMAT);

                if(!in_array($newRecord['tnx_type'], array(ComponentTypes::SELLERS, ComponentTypes::BUYERS))) {
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Closing Transaction Type (Buyer/Seller) invalid. This record has not been imported.');
                    continue;
                }

                // check for other closings for this contact and compare closing date, side and price for dupe.
                $existingClosings = Yii::app()->db->createCommand()
                    ->select('cl.*')
                    ->from('closings cl')
                    ->join('transactions t','t.id=cl.transaction_id')
                    ->join('contacts c','c.id=t.contact_id')
                    ->where('c.id='.$contact->id)
                    ->andWhere('cl.price='.$newRecord['price'])
                    ->andWhere('t.component_type_id='.$newRecord['tnx_type'])
                    ->andWhere('cl.contract_closing_date="'.$newRecord['contract_close_date'].'"')
                    ->queryAll();

                // if dupe, log error and skip
                if($existingClosings) {
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Duplicate closing detected that has the same price, side and closing date. This record has not been imported.');
                    continue;
                }
            }

            StmImportFunctions::saveEmailFields($newRecord, $contact->id, Yii::app()->user->accountId, $emailsExisting, 1, 4, $this->_importErrorRows);
            StmImportFunctions::savePhoneFields($newRecord, $contact->id, Yii::app()->user->accountId, $phonesExisting, 1, 4, $this->_importErrorRows);
            list($address, $addressErrors) = StmImportFunctions::saveAddressFields($newRecord, $contact->id, Yii::app()->user->accountId, $addressesExisting, $stateFullNames, $stateShortNames, $this->_importErrorRows);

            if(!$address || $address->isNewRecord) {
                // address errors were already logged, skip record
                continue;
            }

            $sourceId = Sources::UNKNOWN;
            if(trim($newRecord['source'])) {

                // check for existing matching source name
                $sourceIdsExisting = Yii::app()->db->createCommand()
                    ->select('s.id')
                    ->from('sources s')
                    ->where('TRIM(LOWER(s.name))=:sourceName', array(':sourceName' => strtolower(trim($newRecord['source']))))
                    ->queryColumn();

                // source has ONE match, assign source ID to variable
                if(count($sourceIdsExisting)==1) {
                    $sourceId = $sourceIdsExisting[0];
                }
                // source not exist, create it
                elseif(count($sourceIdsExisting) == 0) {
                    $source = new Sources();
                    $source->setAttributes(array(
                            'account_id' => Yii::app()->user->accountId,
                            'name' => trim($newRecord['source']),
                        ));
                    if($source->save()) {
                        $sourceId = $source->id;
                    }
                    else {
                        // log error, revert source ID back to unknown
                        $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Source did not save for this transaction. Reverted to Source "Unknown". Error message:' . print_r($source->getErrors(), true));
                        $sourceId = Sources::UNKNOWN;
                    }
                }
            }


            if(!in_array($newRecord['tnx_type'], array(ComponentTypes::SELLERS, ComponentTypes::BUYERS))) {
                $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Invalid Transaction Type.');
                continue;
            }
            // create/save transaction
            $transaction = new Transactions();
            $transaction->setAttributes(array(
                    'component_type_id' => $newRecord['tnx_type'],
                    'transaction_status_id' => TransactionStatus::CLOSED_ID,
                    'address_id' => $address->id,
                    'contact_id' => $contact->id,
                    'source_id' => $sourceId,
                ));
            if(!$transaction->save()) {
                $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Transaction did not save. Error message:' . print_r($transaction->getErrors(), true));
                continue;
            }
            else {
                // create/save closing
                $closing = new Closings();
                $closing->setAttributes(array(
                        'transaction_id' => $transaction->id,
                        'closing_status_ma' => Closings::STATUS_TYPE_CLOSED_ID,
                        'price' => Yii::app()->format->formatInteger($newRecord['price']),
                        'contract_execute_date' => date('Y-m-d', strtotime($newRecord['contract_execute_date'])),
                        'contract_closing_date' => date('Y-m-d', strtotime($newRecord['contract_close_date'])),
                        'actual_closing_datetime' => date('Y-m-d', strtotime($newRecord['contract_close_date'])).' 00:00:00',
                        'sale_type_ma' => Closings:: SALE_TYPE_NORMAL_ID, //@todo: can import field map later
                        'financing_type_ma' => Closings::FINANCING_TYPE_CONVENTIONAL_ID, //@todo: can import field map later
                        'is_referral' => 0, //@todo: can import field map later
                    ));
                if(!$closing->save()) {
                    $this->_importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Closing did not save. Error message:' . print_r($closing->getErrors(), true));
                    continue;
                }
            }
        }

        // send log results
        $this->_emailLog();

        // End
        $this->_sendJson(array(
            'status'    =>  'success'
        ));
    }

    /**
     * Checks to see if value exists
     *
     * return boolean
     */
    protected function _checkExist($newRecord, $fieldName, $existingData)
    {
        if(!$newRecord[$fieldName]) {
            return false;
        }
        return array_key_exists(strtolower($newRecord[$fieldName]), $existingData);
    }

    /**
     * Email Log
     *
     * Emails import error log via email including attachment for data that was not imported with reasons why.
     */

    protected function _emailLog()
    {
        $now = microtime(true);
        $runtime = round($now - $this->_startTime, 2);

        $body = '';
        $body .= '<br><br>This import was completed on '.date('Y-m-d H:i:s').'<br><br>';

        if(is_array($this->_importErrorRows)) {

            $body .= '<br><br>---------------------------------------------------------------------------------------------------------------<br><br>';
            $body .= '<br>Import Complete.<br>Duration: ' . $runtime . ' seconds';
            $body .= '<br><br>This is the end of your Import Log.';
            $body .= '<br><br>Thank you.<br><br>Seize the Market';

            // Create email message
            $mail = new StmZendMail();

            // Set parts
            $mail->setFrom('do-not-reply@seizethemarket.net', 'STM Notification');
//            $mail->addTo((YII_DEBUG) ? StmMailMessage::DEBUG_EMAIL_ADDRESS : Yii::app()->user->contact->primaryEmail);
            $mail->addTo('debug@seizethemarket.com');
            $mail->setSubject('Closing Import Log - ' . Yii::app()->user->domain->name);
            $mail->setBodyHtml(((strpos($body, '<html>') === false || strpos($body, '<html>') > 5))? nl2br($body) : $body);

            // Create attachment for CSV file of failed imports
            if($this->_importErrorRows) {

                // Create and open temp file
                $fileName = '/tmp/dialerimporttemp' . uniqid() . '.csv';
                if(($handle = fopen($fileName, "w")) !== FALSE) {

                    // Add each CSV entry to the file
                    foreach($this->_importErrorRows as $errorRow) {
                        fputcsv($handle, $errorRow);
                    }

                    // Close file handle
                    fclose($handle);

                    // Add CSV attachment to email
                    $mail->createAttachment(file_get_contents($fileName), 'text/csv', Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, 'failed-closing-imports-' . date('Y-m-d_H.i.s') . '.csv');

                    // Delete file
                    unlink($fileName);
                }
            }

            // Attempt to send email message
            try {
                $mail->send(StmZendMail::getAwsSesTransport());
            }
            catch(Exception $e) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') An error has occurred while attempting to send a dialer import log (Zend Mail). Code: "' . $e->getCode() . '" Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
            }
        }
    }

    /**
     * Handle Process File
     *
     * Handles processing a file
     * @throws Exception When issues have come up with a file upload
     */
    protected function _handlePreviewFile()
    {
        // Make sure we had a file upload
            if(!$_FILES['import-file']) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'No file upload detected'
            ));
        }

        // this setting is required for Mac environment
        ini_set("auto_detect_line_endings", true);

        // Make sure it is actually an uploaded file and not some hack
        if(!is_uploaded_file($_FILES['import-file']['tmp_name'])) {
            throw new Exception('File upload is not really a file upload!');
        }

        // Use file info extension to make sure the file is a proper csv file and not some weird file, we can't process excel files
        $info = new finfo(FILEINFO_MIME_TYPE);
        if(!in_array($info->file($_FILES['import-file']['tmp_name']), array('text/plain','text/csv','text/tsv'))) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'The selected file is not a proper csv file, please make sure it is a plain text file.'
            ));
        }
        $csvData = array();
        $row = 1;
        $maxNum = 99999;
        $charset = null;
        if (($handle = fopen($_FILES['import-file']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",")) !== FALSE && ($row < $maxNum)) {
                if(!$charset) {
                }

                // prevents yii grid erroring out due to charset issues
                array_walk($data, function(&$value, $key) {
//                    $charset = mb_detect_encoding($value, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
//                    $value = mb_convert_encoding($value,'UTF-8',$charset);
                    // removes any weird encoding
                    $charset = mb_detect_encoding($value, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                    if($charset !== 'UTF-8') {
                        $value = iconv($charset,'UTF-8//IGNORE', $value);
                    }
                });

                if($row == 1 && $_POST['ignoreFirstRow']) {
                    $row++;
                    continue;
                }
                // $num = count($data);
                $row++;
                if(count($data) !== 22) {
                    $this->_sendJson(array(
                            'status'    =>  'error',
                            'message'   =>  'Column do not match the required format.'
                        ));
                    Yii::app()->end();
                }

                $csvData[] = $data;
            }
            fclose($handle);
        }

        // Simply return the contents for the view to process
        $this->_sendJson(array(
            'status'    =>  'success',
            'data'      =>  $csvData,
        ));
    }
}