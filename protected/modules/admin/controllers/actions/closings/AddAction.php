<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction {

	public function run($id) {
        $this->controller->title = 'Add New Closing';
        $criteria = new CDbCriteria();
        $criteria->compare('transaction_id', $id);
        $criteria->addCondition('closing_status_ma <> '.Closings::STATUS_TYPE_FALLOUT_ID);

		if(($Closing = Closings::model()->find($criteria))) {
            Yii::app()->user->setFlash('notice', 'This closing already exists for the transaction.');
            $this->controller->redirect(array('view', 'id' => $Closing->id));
        } else {
            $Closing = new Closings;
        }
		$Closing->transaction_id = $id;

		// Get the related transaction
		$Closing->transaction = Transactions::model()->findByPk($Closing->transaction_id);

		if (!$Closing->transaction) {
			throw new CHttpException(500, 'Could not find the transaction to add a Closing.');
		}

		$Closing->sale_type_ma      = $Closing->transaction->getFieldValue('sale_type_ma');
		$Closing->mls_id            = $Closing->transaction->getFieldValue('mls_num', null);
		$Closing->closing_status_ma = Closings::STATUS_TYPE_ACTIVE_ID;

        // process closing model changes
		if (isset($_POST['Closings'])) {
			$Closing->attributes = $_POST['Closings'];

            $closingSaved = $Closing->save();

            $expensesValidated = true;
            if(isset($_POST['AccountingTransactions'])) {
                foreach ($_POST['AccountingTransactions'] as $i => $expense) {

                    // if deleted, ignore this record
                    if($expense['remove']) {
                        continue;
                    }

                    $accountingTransaction = new AccountingTransactions('closings');
                    $accountingTransaction->date = ($Closing->actual_closing_datetime)? $Closing->actual_closing_datetime : $Closing->contract_closing_date;

                    $accountingTransaction->attributes = $expense; // @todo: on test, Check to see component_type_id is correct for new record

                    if(empty($accountingTransaction->amount) && empty($accountingTransaction->agent_id) && empty($accountingTransaction->accounting_account_id)) {
                        // skip if empty
                        continue;
                    }

                    //  Mark that the contact e-mails section had a failure
                    if ($expensesValidated && !$accountingTransaction->validate(array('amount','agent_id','accounting_account_id'))) {
                        $expensesValidated = false;
                        $error = current($accountingTransaction->getErrors());
                        Yii::app()->user->setFlash('error', $error[0]);
                    }
                    array_push($Closing->expenseCollection, $accountingTransaction);
                }
            }

		}

        // process assignment groups
        $inspectorAssignmentGroup = new InspectorAssignmentGroup($Closing);
        $inspectorAssignmentGroup = $this->getController()->processAssignmentGroup($inspectorAssignmentGroup);

        $lenderAssignmentGroup = new LenderAssignmentGroup($Closing);
        $lenderAssignmentGroup = $this->getController()->processAssignmentGroup($lenderAssignmentGroup);

        $coBrokerAssignmentGroup = new CoBrokerAssignmentGroup($Closing);
        $coBrokerAssignmentGroup = $this->getController()->processAssignmentGroup($coBrokerAssignmentGroup);

        $titleCompanyAssignmentGroup = new TitleCompanyAssignmentGroup($Closing);
        $titleCompanyAssignmentGroup = $this->getController()->processAssignmentGroup($titleCompanyAssignmentGroup);

        // process assignments
        $assignmentsProcessed = $this->getController()->processAssignments($Closing);
        $assignmentValidated = true;
        if (!$assignmentsProcessed) {
            $assignmentValidated = false;
            Yii::app()->user->setFlash('error', 'Sorry, we were unable to save your assignments.');
        }

        // process post save operations
        if ($closingSaved && $assignmentValidated && $expensesValidated) {

            // Handles add/delete expenses for this closing
            if ($Closing->expenseCollection) {
                foreach ($Closing->expenseCollection as $expense) {

                    $expense->closing_id = $Closing->id;
                    $expense->save(false); // already ran validation
                }
            }

            Yii::app()->user->setFlash('success', 'Successfully added Closing.');
            $this->controller->redirect(array('view', 'id' => $Closing->id));
        }

		$this->controller->breadcrumbs = array(
			'Add' => array('/admin/closings/add', 'id' => $Closing->id),
		);

		$this->controller->pageTitle = 'Add New Closing';
        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_AssignmentsGui.js');
        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_ClosingExpensesGui.js');

		$this->controller->render('form', array(
			'model' => $Closing,
            'inspectorAssignmentGroup' => $inspectorAssignmentGroup,
            'lenderAssignmentGroup' => $lenderAssignmentGroup,
            'coBrokerAssignmentGroup' => $coBrokerAssignmentGroup,
            'titleCompanyAssignmentGroup' => $titleCompanyAssignmentGroup,
		));
	}
}