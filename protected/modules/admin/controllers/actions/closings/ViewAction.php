<?php

class ViewAction extends CAction {

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
    {
		$model = $this->controller->loadModel($id);

//        $isAssignedTransaction = Yii::app()->db->createCommand('select COUNT(*) from assignments where component_type_id='.$model->transaction->component_type_id.' AND assignee_contact_id='.$model->transaction->id)->queryScalar();

        if(Yii::app()->user->checkAccess('limitedClosingAccess') && !Yii::app()->user->checkAccess('owner')) {

            $transactionId = $model->transaction->id;
            $componentTypeId = $model->transaction->component_type_id;
            $componentId = $model->transaction->id;
            // see if this user is in the
            if(Yii::app()->db->createCommand("select count(*) from assignments WHERE component_type_id={$componentTypeId} AND component_id={$componentId} AND assignee_contact_id=".Yii::app()->user->id." AND is_deleted=0")->queryScalar() < 1) {

                $this->controller->render('/main/accessDenied', array());
                Yii::app()->end();
            }
        }

        switch($model->transaction->component_type_id) {
            case ComponentTypes::BUYERS:
                if(Yii::app()->user->checkAccess('noBuyerAccess')) {
                    $this->controller->render('/main/accessDenied', array());
                    Yii::app()->end();
                }
                break;

            case ComponentTypes::SELLERS:
                if(Yii::app()->user->checkAccess('noSellerAccess')) {
                    $this->controller->render('/main/accessDenied', array());
                    Yii::app()->end();
                }
                break;
        }


		$this->controller->title = $model->transaction->contact->fullName;

        $returnUrl = $this->getController()->createUrl("/admin/closings/view", array('id'=>$id)).'?returnContactIdVerify='.$model->transaction->contact->id;;
        Yii::app()->user->setState('returnContactIdVerify',$returnUrl);

        // Use the contact's full name if the transaction's address cannot be found
        $viewBreadcrumb = ( $model->transaction->address) ?
            Yii::app()->format->formatAddress($model->transaction->address, array('streetOnly' => true)) : '';

		$this->controller->breadcrumbs = array(
            $model->transaction->contact->getFullName() =>'',
			$viewBreadcrumb => '',
		);

		$this->controller->render('view', array(
			'model' => $model,
		));
	}
}
