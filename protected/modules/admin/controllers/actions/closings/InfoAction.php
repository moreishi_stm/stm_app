<?php

class InfoAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		$model = $this->controller->loadModel($id);

		$this->controller->renderPartial('_info',array(
			'model'=>$model,
		));
	}
}