<?php

class PendingAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		// $TransactionType = $this->controller->transactionType;

		$this->controller->renderPartial('_pending',array(
			// 'model'=>$this->controller->loadModel($id),
			// 'TransactionType'=>$TransactionType,
		));
	}
}