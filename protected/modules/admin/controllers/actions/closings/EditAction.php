<?php

/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class EditAction extends CAction {

	public function run($id) {

		$Closing = $this->controller->loadModel($id);

        if(Yii::app()->user->checkAccess('limitedClosingAccess') && !Yii::app()->user->checkAccess('owner')) {

            $this->controller->render('/main/accessDenied', array());
            Yii::app()->end();
        }

		$closingContactsFullName = $Closing->transaction->contact->fullName;
		$this->controller->title = 'Edit Closing: ' . $closingContactsFullName;

        // process the assignment groups
        $inspectorAssignmentGroup = new InspectorAssignmentGroup($Closing);
        $inspectorAssignmentGroup = $this->getController()->processAssignmentGroup($inspectorAssignmentGroup);

        $lenderAssignmentGroup = new LenderAssignmentGroup($Closing);
        $lenderAssignmentGroup = $this->getController()->processAssignmentGroup($lenderAssignmentGroup);

        $coBrokerAssignmentGroup = new CoBrokerAssignmentGroup($Closing);
        $coBrokerAssignmentGroup = $this->getController()->processAssignmentGroup($coBrokerAssignmentGroup);

        $titleCompanyAssignmentGroup = new TitleCompanyAssignmentGroup($Closing);
        $titleCompanyAssignmentGroup = $this->getController()->processAssignmentGroup($titleCompanyAssignmentGroup);

		if (isset($_POST['Closings'])) {

			$Closing->attributes = $_POST['Closings'];

            $expensesValidated = true;
            if(isset($_POST['AccountingTransactions']) && is_array($_POST['AccountingTransactions'])) {
                $expensesToDelete = array();
                $expensesIdsToDelete = array();
                foreach ($_POST['AccountingTransactions'] as $i => $expense) {

                    if ($expense['id']) {
                        $Criteria = new CDbCriteria;
                        $Criteria->condition = 'id = :id';
                        $Criteria->params = array(':id' => $expense['id']);
                        $accountingTransaction = AccountingTransactions::model()->find($Criteria);
                    } else {
                        $accountingTransaction = new AccountingTransactions('closings');
                        $expense['closing_id'] = $Closing->id;
                        $accountingTransaction->closing_id = $Closing->id;
                        $accountingTransaction->date = ($Closing->actual_closing_datetime)? $Closing->actual_closing_datetime : $Closing->contract_closing_date;
                    }

                    $accountingTransaction->attributes = $expense; // @todo: on test, Check to see component_type_id is correct for new record

                    if ($accountingTransaction->remove) {
                        array_push($expensesToDelete, $accountingTransaction);
                        $expensesAgentIdsToDelete[] = $accountingTransaction->agent_id;
                        continue;
                    }

                    if(empty($accountingTransaction->amount) && empty($accountingTransaction->agent_id) && empty($accountingTransaction->accounting_account_id)) {
                        // skip if empty
                        continue;
                    }

                    //  Mark that the contact e-mails section had a failure
                    if ($expensesValidated && !$accountingTransaction->validate()) {
                        $expensesValidated = false;
                        $error = current($accountingTransaction->getErrors());
                        Yii::app()->user->setFlash('error', $error[0]);
                    }
                    array_push($Closing->expenseCollection, $accountingTransaction);
                }
            }

            // Process additional addresses for the contact if any
			$validateAddress = true;
            if (isset($_POST['Sellers']['address_id']) || isset($_POST['Buyers']['address_id'])) {
				$modelName = ($_POST['Sellers']) ? 'Sellers' : 'Buyers';

                $addressId = $_POST[$modelName]['address_id'] ;
                $Transaction = $modelName::model()->findByPk($Closing->transaction_id);
                if($addressId != $Transaction->address_id) {
					$Transaction->address_id = $addressId;
					if(!($validateAddress = $Transaction->validate())) {
                        $error = current($Transaction->getErrors());
                        Yii::app()->user->setFlash('error', 'Commission Expense: ' . $error[0]);
                    }
				}
            }

			if ($validateAddress && $expensesValidated && $Closing->save()) {

				if ($validateAddress) {
					$Transaction->saveAttributes(array('address_id'=>$addressId));
				}

                // process assignments
                $assignmentsProcessed = $this->getController()->processAssignments($Closing);
                if (!$assignmentsProcessed) {
                    Yii::app()->user->setFlash('error', 'Sorry, we were unable to save your assignments.');
                    $this->getController()->redirect(array('edit', 'id' => $Closing->id));
                }

                if(Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('commissionEditAccess')) {

                    // Handles add/delete expenses for this closing
                    if ($Closing->expenseCollection && is_array($Closing->expenseCollection)) {
                        foreach ($Closing->expenseCollection as $expense) {

                            $expense->date = ($Closing->actual_closing_datetime)? $Closing->actual_closing_datetime : $Closing->contract_closing_date;
                            $expense->save(false); // already ran validation
                        }
                    }

                    if(!empty($expensesToDelete) && is_array($expensesToDelete)) {
                        foreach($expensesToDelete as $expenseToDelete) {
                            // delete will always return false due to soft delete, therefore removed the error message
                            $expenseToDelete->delete();
                        }
                    }
                }

				Yii::app()->user->setFlash('success', 'Successfully updated Closing.');
				$this->controller->redirect(array('view', 'id' => $Closing->id));
			}
            else {
                if($Closing->expenseCollection && $expensesToDelete) {
                    // load expenses data if any validation or save for closing failed
                    $Closing->expenseCollection = CMap::mergeArray($Closing->expenseCollection, $expensesToDelete);
                }
            }
		}
        else {
            $Closing->expenseCollection = $Closing->expenses;
        }

        // Use the contact's full name if the transaction's address cannot be found
        $viewBreadcrumb = ( $Closing->transaction->address) ?
            Yii::app()->format->formatAddress($Closing->transaction->address, array('streetOnly' => true)) : '';

        $this->controller->breadcrumbs = array(
			$closingContactsFullName => array(
				'/admin/contacts/view', 'id' => $Closing->contact->id
			),
            $viewBreadcrumb=>array(
                '/admin/'.$Closing->transaction->componentType->name.'/view', 'id' => $Closing->transaction->id,
            ),
            'Edit' => '',
		);

        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_AssignmentsGui.js');
        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_ClosingExpensesGui.js');

		$this->controller->render('form', array(
			'model' => $Closing,
            'inspectorAssignmentGroup' => $inspectorAssignmentGroup,
            'lenderAssignmentGroup' => $lenderAssignmentGroup,
            'coBrokerAssignmentGroup' => $coBrokerAssignmentGroup,
            'titleCompanyAssignmentGroup' => $titleCompanyAssignmentGroup,
		));
	}
}
