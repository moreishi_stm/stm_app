<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {

		$this->controller->title = 'Email Templates List';

		$model = $this->controller->baseModel;
        $model->status_ma = 1;

        if($_COOKIE['EmailTemplates']) {
            $model->attributes = $_COOKIE['EmailTemplates'];
        }

		if (isset($_GET['EmailTemplates'])) {
			$model->attributes=$_GET['EmailTemplates'];
            foreach($_GET['EmailTemplates'] as $field => $value) {
                setcookie("EmailTemplates[$field]", $value, time()+3600*24*30);
            }
		}

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
