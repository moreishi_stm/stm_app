<?php

class EditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
        // Load up model
		$model = $this->controller->loadModel($id);

        // Set page title
		$this->controller->title = 'Email Template Edit';

        // Do this only if we have POST data
		if (isset($_POST['EmailTemplates'])) {

            // Set attributes
			$model->attributes = $_POST['EmailTemplates'];

            // Attempt to save
			if ($model->save()) {

                // Copy all attachments to S3
                if(count($_FILES['EmailTemplates']['name']['attachments'])) {

                    for($i = 0; $i < count($_FILES['EmailTemplates']['name']['attachments']); $i++) {

                        if(!$_FILES['EmailTemplates']['tmp_name']['attachments'][$i]) {
                            continue;
                        }

                        // Determine Content Type (MIME type)
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $contentType = finfo_file($finfo, $_FILES['EmailTemplates']['tmp_name']['attachments'][$i]);
                        finfo_close($finfo);

                        // Store attachment in database
                        $EmailTemplateAttachment = new EmailTemplateAttachments();
                        $EmailTemplateAttachment->email_template_id = $model->id;
                        $EmailTemplateAttachment->name = $_FILES['EmailTemplates']['name']['attachments'][$i];
                        $EmailTemplateAttachment->type = $contentType;
                        $EmailTemplateAttachment->size = $_FILES['EmailTemplates']['size']['attachments'][$i];
                        $EmailTemplateAttachment->added = new CDbExpression('NOW()');

                        if (!$EmailTemplateAttachment->save()) {
                            echo CActiveForm::validate($EmailTemplateAttachment);
                            Yii::app()->end();
                        }

                        // Copy attachments to S3
                        $s3client = Aws\S3\S3Client::factory(array(
                            'key'       =>  'AKIAITXC4AAKGIAYACBA',
                            'secret'    =>  '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'
                        ));

                        $s3client->putObject(array(
                            'Bucket'                =>  ((YII_DEBUG)?'dev.':'') .'client-private.seizethemarket.com',
                            'Key'                   =>  Yii::app()->user->clientId. '/' . Yii::app()->user->accountId . '/email-template-attachments/' . $EmailTemplateAttachment->id,
                            'SourceFile'            =>  $_FILES['EmailTemplates']['tmp_name']['attachments'][$i],
                            'ContentDisposition'    =>  'attachment; filename="' . $_FILES['EmailTemplates']['name']['attachments'][$i],
                            'ContentType'           =>  $contentType,
                            'StorageClass'          =>  'REDUCED_REDUNDANCY'
                        ));
                    }
                }

                // Report success
				Yii::app()->user->setFlash('success', 'Successfully updated Email Template.');
				$this->controller->redirect(array('view','id'=>$model->id));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}