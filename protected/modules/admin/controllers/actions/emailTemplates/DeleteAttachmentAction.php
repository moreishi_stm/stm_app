<?php

/**
 * Delete Attachment Action
 */
class DeleteAttachmentAction extends CAction
{
    /**
     * Manages models
     */
    public function run()
    {
        // Retrieve ID
        $id = Yii::app()->request->getPost('attachment_id');
        if(!$id) {
            throw new Exception('Error, no attachment ID has been specified!');
        }

        // Load up the model
        $attachment = EmailTemplateAttachments::model()->findByAttributes(array(
            'id'    =>  $id
        ));

        // Use AWS S3
        $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

        // Delete the file from S3
        $s3client->deleteObject(array(
            'Bucket'    =>  ((YII_DEBUG)?'dev.':'') .'client-private.seizethemarket.com',
            'Key'       =>  Yii::app()->user->clientId. '/' . Yii::app()->user->accountId . '/email-template-attachments/' . $attachment->id,
        ));

        // Delete the email attachment record
        $attachment->delete();
    }
}