<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class DeleteAction extends CAction {

    public function run($id) {

        $ActionPlanItem = EmailTemplates::model()->findByPk($id);
        if (isset($_POST['EmailTemplates'])) {
            $ActionPlanItem->attributes = $_POST['EmailTemplates'];
        }

        $this->performAjaxRequest($ActionPlanItem);
    }

    protected function performAjaxRequest(EmailTemplates $model) {

        if (Yii::app()->request->isAjaxRequest) {
            $model->status_ma=0;
            $status='success';
            if (!$model->save()) {
                $status='error';
            }

            echo CJSON::encode(array('status'=>$status));

            Yii::app()->end();
        }
    }
}