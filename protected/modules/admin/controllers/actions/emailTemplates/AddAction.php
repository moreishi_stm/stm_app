<?php
include_once(Yii::getPathOfAlias('admin_module.extensions.') . DS . 'SimpleHtmlDom.php');

class AddAction extends CAction {

    /**
     * Manages models
     */
    public function run() {

        $this->controller->title = 'Email Templates List';

        $model = new EmailTemplates;

        // populate bombbomb content if bombbomb template ID provided in url GET params
        if(isset($_GET['bbid'])) {
            $bbid = $_GET['bbid'];
            $subject = $_GET['subject'];
            $description = $_GET['desc'];

            if($exitingTemplate = EmailTemplates::model()->findByAttributes(array('bombbomb_id'=>$bbid))) {
                $this->controller->redirect('/'.Yii::app()->controller->module->name.'/emailTemplates/'.$exitingTemplate->id);
            }
            else {

                // get the bombbomb email template content html body
                $bbBody = file_get_contents('http://bbemaildelivery.com/bbext/?p=email&id='.$bbid);
                preg_match("/<body[^>]*>(.*?)<\/body>/is", $bbBody, $matches);
                $body = $matches[1];

                // replace unnecessary javascript that comes from bombBomb templates
                if(strpos($body, '<script type="text/javascript">') !== false) {
                    preg_match("/\<script type=\"text\/javascript\"\>(.*)\<\/script\>/ismU", $body, $matchScript);
                    $script = trim($matchScript[0]);
                    $body = trim(str_replace($script, '', $body));
                }

                $shareUrlEncoded = urlencode('http://www.'.Yii::app()->user->primaryDomain->name.'/videoEmail?bbid='.$bbid); //#todo: verify params after converting to "videoEmail" Action

                // replace javascript:twitter('') //https://twitter.com/intent/tweet?text=Check%20Out%20This%20Email%20!&url={url}&original_referer=http%3A%2F%2Flink2.bblink.co%2Fbbext%2F%3Fp%3Dshare%26cid%3D2288116424C44BB3E0530100007FF3D8%26email_id%3Ddfb8e758-1347-4be6-094c-07f1cae1f12c%26type%3Dt
                //url = http%3A%2F%2Fvid.us%2F7uy5ge (urlencode)
                $body = str_replace("javascript:twitter('')", 'https://twitter.com/intent/tweet?text=Check%20Out%20This%20Email%20!&url='.$shareUrlEncoded, $body);
                // replace javascript:facebook('') //http://www.facebook.com/sharer/sharer.php?u={url}
                $body = str_replace("javascript:facebook('')", 'http://www.facebook.com/sharer/sharer.php?u='.$shareUrlEncoded, $body);

                $model->body = $body;
                $model->bombbomb_id = $bbid;
                $model->subject = $subject;
                $model->description = $description;
            }
        }

        if (isset($_POST['EmailTemplates'])) {

            // Set attributes
            $model->attributes = $_POST['EmailTemplates'];

            // Attempt to save
            if ($model->save()) {

                // Copy all attachments to S3
                if(count($_FILES['EmailTemplates']['name']['attachments'])) {

                    for($i = 0; $i < count($_FILES['EmailTemplates']['name']['attachments']); $i++) {

                        if(!$_FILES['EmailTemplates']['tmp_name']['attachments'][$i]) {
                            continue;
                        }

                        // Determine Content Type (MIME type)
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $contentType = finfo_file($finfo, $_FILES['EmailTemplates']['tmp_name']['attachments'][$i]);
                        finfo_close($finfo);

                        // Store attachment in database
                        $EmailTemplateAttachment = new EmailTemplateAttachments();
                        $EmailTemplateAttachment->email_template_id = $model->id;
                        $EmailTemplateAttachment->name = $_FILES['EmailTemplates']['name']['attachments'][$i];
                        $EmailTemplateAttachment->type = $contentType;
                        $EmailTemplateAttachment->size = $_FILES['EmailTemplates']['size']['attachments'][$i];
                        $EmailTemplateAttachment->added = new CDbExpression('NOW()');

                        if (!$EmailTemplateAttachment->save()) {
                            echo CActiveForm::validate($EmailTemplateAttachment);
                            Yii::app()->end();
                        }

                        // Copy attachments to S3
                        $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

                        $s3client->putObject(array(
                            'Bucket'                =>  ((YII_DEBUG)?'dev.':'') .'client-private.seizethemarket.com',
                            'Key'                   =>  Yii::app()->user->clientId. '/' . Yii::app()->user->accountId . '/email-template-attachments/' . $EmailTemplateAttachment->id,
                            'SourceFile'            =>  $_FILES['EmailTemplates']['tmp_name']['attachments'][$i],
                            'ContentDisposition'    =>  'attachment; filename="' . $_FILES['EmailTemplates']['name']['attachments'][$i],
                            'ContentType'           =>  $contentType,
                            'StorageClass'          =>  'REDUCED_REDUNDANCY'
                        ));
                    }
                }

                // Report success
                Yii::app()->user->setFlash('success', 'Successfully added Email Template.');
                $this->controller->redirect('/'.Yii::app()->controller->module->name.'/emailTemplates/' . $model->id);
            }
        }

        // Render view
        $this->controller->render('form', array(
                'model' => $model
            )
        );
    }
}
