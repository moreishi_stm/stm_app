<?php
	class AddAction extends CAction {

		/**
		 * Manages models
		 */
		public function run()
        {
			$this->controller->title = 'Lockboxes';

			$model = new Lockboxes();

			if (isset($_POST['Lockboxes'])) {
				$model->attributes = $_POST['Lockboxes'];
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully added Lockbox.');
                    $this->controller->redirect('/'.Yii::app()->controller->module->name.'/lockboxes');
                }
			}

			$this->controller->render('form', array(
					'model' => $model,
				)
			);
		}
	}
