<?php
	class ListAction extends CAction {

		/**
		 * Manages models
		 */
		public function run()
        {
			$this->controller->title = 'Lockboxes';

			$model = new Lockboxes();

			if (isset($_GET['Lockboxes'])) {
				$model->attributes = $_GET['Lockboxes'];
			}

			$this->controller->render('list', array(
					'model' => $model,
				)
			);
		}
	}
