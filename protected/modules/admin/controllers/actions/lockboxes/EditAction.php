<?php
	class EditAction extends CAction {

		/**
		 * Manages models
		 */
		public function run($id)
        {
			$this->controller->title = 'Edit Lockboxes';

			$model = Lockboxes::model()->findByPk($id);

			if (isset($_POST['Lockboxes'])) {
				$model->attributes = $_POST['Lockboxes'];
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully updated Lockbox.');
                    $this->controller->redirect('/'.Yii::app()->controller->module->name.'/lockboxes');
                }
			}

			$this->controller->render('form', array(
					'model' => $model,
				)
			);
		}
	}
