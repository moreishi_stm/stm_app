<?php
class ListAction extends CAction
{
	public function run() {

		$this->controller->title = 'Craigslist Posting';
		$tableName = Yii::app()->user->board->prefixedName;
		$model = MlsProperties::model($tableName);

        if(Yii::app()->user->settings->agent_mls_num) {
            $model->listing_agent_id = Yii::app()->user->settings->agent_mls_num;
        }
        else {
            if($setting = SettingAccountValues::model()->findByAttributes(array('account_id'=>Yii::app()->user->accountId,'setting_id'=>Settings::SETTING_AGENT_MLS_ID))) {
                $model->listing_agent_id = $setting->value;
            }
        }

		//@todo: collaborate mls id's for all agents?
        $model->status = 'Active';

		$this->controller->render('list',array(
			'model'=>$model,
		));
    }
}
