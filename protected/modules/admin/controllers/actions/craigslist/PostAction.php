<?php
/**
 * @author Chris Willard <chriswillard.dev@gmail.com>
 * @since  06/17/2013
 */

class PostAction extends CAction
{

    public function run($propertyTypeName, $listingId)
    {

        $propertyType = MlsPropertyTypes::model()->findByAttributes(array('name' => $propertyTypeName));

        /** Get the property information to create the craigslist post */
        $mlsBoardName = Yii::app()->getUser()->getBoard()->getPrefixedName();
        $propertyToPost = MlsProperties::model($mlsBoardName)->findByPk(
            array(
                'listing_id'       => $listingId,
                'mls_property_type_id' => $propertyType->id,
            )
        );
        if (!$propertyToPost) {
            throw new CHttpException(500, 'Could not locate a valid MlsProperty instance.');
        }


        /** Set the layout to plain to remove all of our decorators */
        $this->getController()->layout = 'plain';
        $postString = 'go=I will abide by these guidelines&cryptedStepCheck='.$_POST['cgCrypt'];
        $craigslistPostContent = $this->getController()->getCraigslistPostContent($_POST['cgAction'], $postString);
        $this->renderPropertyPostPage($propertyToPost, $craigslistPostContent);
    }

    protected function renderPropertyPostPage($propertyToPost, $craigslistPostContent) {
        $propertyData = array(
            'title'        => 'New Listing: ' . $propertyToPost->city,
            'location'     => $propertyToPost->street_number . ' ' . $propertyToPost->street_name,
            'description'  => $this->controller->renderPartial(
                    'templates/listingStandard3', array('Property' => $propertyToPost), true
                ),
            'price'        => $propertyToPost->price,
            'bedrooms'     => $propertyToPost->bedrooms,
            'bathrooms'    => ($propertyToPost->baths_total+(1+($propertyToPost->baths_total))),
            // Compensate for the craigslist's default options
            'housing_type' => 6,
            'parking'      => 1,
            'sqft'         => $propertyToPost->sq_feet,
            'furnished'    => 0,
            'show_on_maps' => 1,
            'street'       =>
                $propertyToPost->street_number . ' ' . $propertyToPost->street_name . ' ' . $propertyToPost->street_suffix,
            'city'         => $propertyToPost->city,
            'postal'       => $propertyToPost->zip,
            'region'       => substr($propertyToPost->state, 0, 2),
            //            'fromEmail'    => '',
            //            'confirmEmail' => '',
        );

        $this->controller->render(
            'post', array(
                'propertyJsonData'      => CJSON::encode($propertyData),
                'craigslistPostContent' => $craigslistPostContent,
            )
        );
    }
}