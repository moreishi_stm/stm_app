<?php
	/**
	 * @author Chris Willard <chriswillard.dev@gmail.com>
	 * @since  06/17/2013
	 */

	class ViewAction extends CAction {

		public function run($id) {

			$this->controller->title = 'Craigslist Post';

			$Property = $this->getController()->getPropertyModel();
			$Property = $Property->findByPk($id);

			$this->getController()->layout = 'plain';

			$this->controller->render('templates/listingStandard3', array(
					'Property' => $Property,
				)
			);
		}
	}