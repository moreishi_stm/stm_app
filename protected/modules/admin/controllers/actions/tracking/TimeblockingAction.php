<?php
	Yii::import('admin_widgets.DateRanger.DateRanger');

	class TimeblockingAction extends CAction {

		public function run($id = null) {
			if ($id == null) {
				$id = Yii::app()->user->id;
			}

			$this->controller->title = 'Timeblocking';
			$model = new Timeblockings;
			$model->unsetAttributes(); // clear any default values
			$model->timeblock_type_ma = Timeblockings::LEAD_GEN_TYPE_PHONE_ID;

			// used for gridview search box inputs
			if (isset($_GET['Timeblockings'])) {
				$model->attributes = $_GET['Timeblockings'];
				if($model->contact_id)
					$id = $model->contact_id;
			}

			$DateRanger = new DateRanger();
			$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

            $timeblockingBaseModel = Timeblockings::model()->byContactId($id)->byDate($dateRange);
            if (($timeblockType = $_GET['Timeblockings']['timeblock_type_ma'])) {
                $timeblockingBaseModel->byTypes($timeblockType);
            }
			$DataProvider = new CActiveDataProvider($timeblockingBaseModel, array(
               'criteria' => array(
                   'order' => 'timeblock_type_ma asc, date asc',
               )
            ));
			$DataProvider->pagination->pageSize = 50;

			if(!$model->contact_id)
				$model->contact_id = Yii::app()->user->id;


            if(Yii::app()->user->settings->timeblocks_past_due_redirect == 1 && Yii::app()->user->settings->timeblocks_past_due_redirect !== '0') {
                $timeblockingFromDate = '';
                $timeBlockingMissingDates = Timeblockings::getMissingDays(Yii::app()->user->id, $timeblockingFromDate, null, 'n/j/y');

                if($timeBlockingMissingDates) {

                    $timeblockMissingDatesFormatted = array_map(function($mDate){return date('n/j/y', strtotime($mDate));}, $timeBlockingMissingDates);
                    $missingDates = implode(', ', $timeblockMissingDatesFormatted);
                }
            }

			$this->controller->render('listTimeblocking', array(
					'model' => $model,
					'DataProvider' => $DataProvider,
					'dateRange' => $dateRange,
					'DateRangerSelect' => $DateRanger->defaultSelect,
                    'missingDates' => $missingDates,
				)
			);
		}
	}