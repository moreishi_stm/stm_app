<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddTimeblockingAction extends CAction
{
	public function run()
	{
		$model = new Timeblockings;
        if (isset($_POST['Timeblockings'])) {
            $model->attributes = $_POST['Timeblockings'];
		}

		$this->performAjaxRequest($model);
	}

	protected function performAjaxRequest(Timeblockings $model) {

		if (Yii::app()->request->isAjaxRequest) {
            if (!$model->save()) {
                echo CActiveForm::validate($model);
            }

			Yii::app()->end();
		}
	}
}