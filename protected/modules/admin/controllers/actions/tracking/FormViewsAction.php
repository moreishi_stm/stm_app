<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class FormViewsAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Form Views';
		$this->controller->pageColor = 'magenta';
		 $model = new FormViews;

		$this->controller->render('formViews', array(
			 'model'=>$model,
		));
	}
}