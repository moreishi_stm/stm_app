<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AppointmentsAction extends CAction
{
    public $agentAppointments;
    public $totalAppointments;

	public function run($id=null)
	{
		$this->controller->title = 'Appointments Reporting';
		$this->controller->pageColor = 'red';

		$model = new Appointments;
		$Criteria = $model->getDbCriteria();
		$Criteria->order = 'set_on_datetime ASC';

		$DateRanger = new DateRanger();
		$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $toDate = date('Y-m-d', strtotime($dateRange['to_date']. '+1 days'));

        $condition = '';
        $dateFilter = (isset($_GET['by_date_filter']))? $_GET['by_date_filter'] : null;
        switch($dateFilter) {
            case 'met':
                $condition .= (empty($condition))?'':' OR ';
                $condition .= '(DATE(set_for_datetime) >= :metDateFrom AND DATE(set_for_datetime) <:metDateTo)';
                $Criteria->params = CMap::mergeArray($Criteria->params, array(':metDateFrom'=>$dateRange['from_date'], ':metDateTo'=>$toDate ));
                $Criteria->order = 'set_for_datetime ASC';
                break;

            case 'both':
                $condition .= (empty($condition))?'':' OR ';
                $condition .= '(DATE(set_on_datetime) >= :setDateFrom AND DATE(set_on_datetime) <:setDateTo) AND (DATE(set_for_datetime) >= :metDateFrom AND DATE(set_for_datetime) <:metDateTo)';
                $Criteria->params = CMap::mergeArray($Criteria->params, array(':setDateFrom'=>$dateRange['from_date'],':setDateTo'=>$toDate,':metDateFrom'=>$dateRange['from_date'], ':metDateTo'=>$toDate ));
                break;

            case 'either':
                $condition .= (empty($condition))?'':' OR ';
                $condition .= '(DATE(set_on_datetime) >= :setDateFrom AND DATE(set_on_datetime) <:setDateTo) OR (DATE(set_for_datetime) >= :metDateFrom AND DATE(set_for_datetime) <:metDateTo)';
                $Criteria->params = CMap::mergeArray($Criteria->params, array(':setDateFrom'=>$dateRange['from_date'],':setDateTo'=>$toDate,':metDateFrom'=>$dateRange['from_date'], ':metDateTo'=>$toDate ));
                break;

            case 'signed':
                $condition .= (empty($condition))?'':' OR ';
                $condition .= '(signed_date >= :signedDateFrom AND signed_date <:signedDateTo)';
                $Criteria->params = CMap::mergeArray($Criteria->params, array(':signedDateFrom'=>$dateRange['from_date'], ':signedDateTo'=>$toDate ));
                $Criteria->order = 'signed_date ASC';
                break;

            case 'set':
            default:
                $condition .= (empty($condition))?'':' OR ';
                $condition .= '(DATE(set_on_datetime) >= :setDateFrom AND DATE(set_on_datetime) <:setDateTo)';
                $Criteria->params = CMap::mergeArray($Criteria->params, array(':setDateFrom'=>$dateRange['from_date'],':setDateTo'=>$toDate));
                break;
        }

        $Criteria->addCondition($condition);

        if(!empty($id) && !isset($_GET['ajax'])) {
            $Criteria->addCondition('set_by_id='.$id.' OR met_by_id='.$id);
        }
		if(!empty($_GET['set_by_id'][0]))  {
			$Criteria->addInCondition('set_by_id', $_GET['set_by_id']);
		}

        if(!empty($_GET['met_by_id'][0]))  {
            $Criteria->addInCondition('met_by_id', $_GET['met_by_id']);
        }

        if(!empty($_GET['past_due']) && $_GET['past_due']==1)  {
            $Criteria->addCondition('DATE(set_for_datetime) < "'.date('Y-m-d'). '" AND (met_status_ma = '.Appointments::MET_STATUS_SET.')');
        }

        if(!empty($_GET['component_type_id'][0]))  {
            $Criteria->addInCondition('component_type_id', $_GET['component_type_id']);
        }

        if(!is_array($_GET['met_status']) && (!empty($_GET['met_status'])))  {
            $Criteria->addCondition('(met_status_ma = '.$_GET['met_status'].')');
        }

        if(!is_array($_GET['signed']) && (!empty($_GET['signed']) || ($_GET['signed']==='0')))  {
            $Criteria->addCondition('(is_signed = '.$_GET['signed'].')');
        }

		$DataProvider = new CActiveDataProvider('Appointments', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>100)));
        $Appointment = new Appointments;

        $settingAgentsIds = SettingAccountValues::model()->findByAttributes(array('account_id'=>Yii::app()->user->accountId, 'setting_id'=>Settings::APPOINTMENTS_EXECUTIVE_SUMMARY_AGENTS_ID));
        if(!empty($settingAgentsIds)) {
            $agentsIds = CJSON::decode($settingAgentsIds->value);
        }

        $appointmentData = $Appointment->reportBySetDateRange($dateRange['from_date'], $dateRange['to_date']); //->byMetByIds($_GET['met_by_id'][0])->bySetByIds($_GET['set_by_id'][0])
        $agentsIds = CMap::mergeArray($agentsIds, $appointmentData['agentIds']);
        // also select any agent that set appointment
        $agents = empty($agentsIds) ? Contacts::model()->byActiveAdmins()->orderByName()->findAll() : Contacts::model()->orderByName()->findAllByPk($agentsIds);

        $this->controller->render('appointments', array(
			 'DataProvider'=>$DataProvider,
			 'dateRange'=>$dateRange,
             'setAppts'=>$Appointment->count($Criteria),
             'metAppts'=>$Appointment->met()->count($Criteria),
             'signed'=>$Appointment->signed()->count($Criteria),
             'agents'=>$agents,
             'reportData' => $appointmentData['appointmentsReportData'],
             'agentAppointments' => $this->agentAppointments = $appointmentData['agentAppointments'],
             'totalAppointments' => $this->totalAppointments = $appointmentData['totalAppointments'],
		));
	}

    public function printAgentField($agentId, $field, $emptyValue='-', $wrapper=null)
    {
        return $this->printField('agent', $agentId, $field, $emptyValue, $wrapper);
    }

    public function printAgentFieldSum($agentId, $fields, $emptyValue='-', $wrapper=null)
    {
        $value = 0;
        if(is_array($fields)) {
            foreach($fields as $field) {
                $singleValue = $this->printField('agent', $agentId, $field, '');
                $value += (!empty($singleValue))? $singleValue : 0;
            }
        }

        if($wrapper && $value > 0) {
            switch($wrapper) {
                case '()':
                    $value = '('.$value.')';
                    break;
                case '[]':
                    $value = '['.$value.']';
                    break;
                case '{}':
                    $value = '{'.$value.'}';
                    break;
            }
        } elseif(empty($value)) {
            $value = $emptyValue;
        }

        return $value;
    }

    public function printTotalField($field, $emptyValue='-', $wrapper=null)
    {
        return $this->printField('total', null, $field, $emptyValue, $wrapper);
    }

    protected function printField($type, $agentId=null, $field, $emptyValue='-', $wrapper=null)
    {
        switch($type) {
            case 'agent':
                if(empty($agentId)) {
                    throw new Exception('Agent ID is missing.');
                }
                $value = $this->agentAppointments[$agentId][$field];
                break;

            case 'total':
                $value = $this->totalAppointments[$field];
                break;

            default:
                throw new Exception('Print field type must be agent or total.');
                break;
        }

        if($wrapper && $value > 0) {
            switch($wrapper) {
                case '()':
                    $value = '('.$value.')';
                    break;
                case '[]':
                    $value = '['.$value.']';
                    break;
                case '{}':
                    $value = '{'.$value.'}';
                    break;
            }
        } elseif(empty($value)) {
            $value = $emptyValue;
        }

        return $value;
    }
}