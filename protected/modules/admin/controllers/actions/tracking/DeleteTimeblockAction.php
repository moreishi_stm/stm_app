<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class DeleteTimeblockAction extends CAction {

	public function run($id) {
		$timeblockEntry = Timeblockings::model()->findByPk($id);

		$this->performAjaxRequest($timeblockEntry);
	}

	protected function performAjaxRequest(Timeblockings $timeblockEntry) {
		if (Yii::app()->request->isAjaxRequest) {
            $timeblockEntry->delete();
			echo CJSON::encode(array(
				'success' => $timeblockEntry->is_deleted,
			)) ;

			Yii::app()->end();
		}
	}
}