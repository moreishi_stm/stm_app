<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class TrafficDetailsAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Traffic Tracker';
		$this->controller->pageColor = 'teal';
		 $model = new TrafficTracker;

		// used for gridview search box inputs
		if (isset($_GET['TrafficTracker'])) {
			$model->attributes=$_GET['TrafficTracker'];
		}

		$DataProvider = $model->search();
//		$DataProvider->criteria = array('group'=>'session_id');

		$this->controller->render('trafficDetails', array(
			'model'=>$model,
			'DataProvider'=>$DataProvider,
		));
	}
}