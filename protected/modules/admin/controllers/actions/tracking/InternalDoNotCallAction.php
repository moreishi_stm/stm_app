<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class InternalDoNotCallAction extends CAction
{
	public function run($id=null)
    {
		$this->controller->title = 'Internal Do Not Call Report';
		$this->controller->pageColor = 'teal';

        $model = new CallListDoNotCall();
        $model->unsetAttributes();

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'last_3_months';
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);
        $model->fromDate = ($dateRange['from_date']) ? $dateRange['from_date'] : null;
        $model->toDate = ($dateRange['to_date']) ? $dateRange['to_date'] : null;

        if(isset($_GET['CallListDoNotCall'])) {
            $model->attributes = $_GET['CallListDoNotCall'];
        }
//        elseif($id)  {
//            $model->added_by = $id;
//        }

//        $tableSummaryData = $this->_getSummaryTableData($model);

		$this->controller->render('internalDoNotCall', array(
            'model' => $model,
            'dateRange'=>$dateRange,
//            'tableSummaryData' => $tableSummaryData,
        ));
	}

    public function printContactInfo($data)
    {
        if($contact = $data->contactModel) {
            echo '<a href="/admin/contacts/'.$contact->id.'" target="_blank">'.$contact->fullName.'</a>';
        }
    }

    public function printLeads($data)
    {
        $buttons = '';
        if($contact = $data->contactModel) {
            $buttons = $contact->existingComponentRecordButton(ComponentTypes::SELLERS);
        }
        return $buttons;
    }
}