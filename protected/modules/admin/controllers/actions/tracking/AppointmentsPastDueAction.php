<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AppointmentsPastDueAction extends CAction
{
    public $agentAppointments;
    public $totalAppointments;

	public function run($id=null)
	{
		$this->controller->title = 'Past Due Appointments Reporting';
		$this->controller->pageColor = 'red';

		$model = new Appointments;
		$Criteria = $model->byPastDue()->getDbCriteria();
		$Criteria->order = 'added DESC';

        if(!empty($_GET['met_by_id']))  {
            $Criteria->addInCondition('met_by_id', (array) $_GET['met_by_id']);
        }
        elseif(!$_GET) {
            $Criteria->addInCondition('met_by_id', (array) Yii::app()->user->id);
        }

        $DataProvider = new CActiveDataProvider('Appointments', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>100)));

        $this->controller->render('appointmentsPastDue', array(
			 'DataProvider'=>$DataProvider,
             'vimeoCode' => Yii::app()->stm_hq->createCommand("SELECT vimeo_code from support_videos where id=79")->queryScalar(),
		));
	}
}