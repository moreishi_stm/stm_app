<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class SessionAction extends CAction
{
	public function run($id)
	{
		$this->controller->title = 'Session Tracker';
		$this->controller->pageColor = 'magenta';

		$model = new TrafficTracker;
		$model->session_id = $id;

		$this->controller->render('session', array(
			 'model'=>$model,
		));
	}
}