<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class SourcesAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Source of Business Reports';
		$this->controller->pageColor = 'teal';

		$DateRanger = new DateRanger();
		if($_POST['DateRanger']) {
            if($_POST['DateRanger']['date_preset']) {
                $DateRanger->defaultSelect = $_POST['DateRanger']['date_preset'];
            }
            if($_POST['DateRanger']['custom_from_date']) {
                $DateRanger->custom_from_date = $_POST['DateRanger']['custom_from_date'];
            }
            if($_POST['DateRanger']['custom_to_date']) {
                $DateRanger->custom_to_date = $_POST['DateRanger']['custom_to_date'];
            }
        }
		$dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

		// get sources for closings grouped by sources w/count
		$model = new Closings;
		$ClosedCriteria = $model->byActualClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_CLOSED_ID))->getDbCriteria();
		$ClosedTotal = $model->count($ClosedCriteria);
		$ClosedVolume = Closings::getVolumeTotal($ClosedCriteria);
		$ClosedSourceCriteria = $this->buildSourceCriteria($ClosedCriteria,$ClosedTotal);
		$ClosedDataProvider = $this->buildDataProvider('Closings', $ClosedSourceCriteria);

        $results = Yii::app()->db->createCommand()
            ->select('c.*, s.name parentSource, count(*) as countTotal, sum(price) as volumeTotal, ROUND((count(*)/'.$ClosedTotal.')*100) as businessPercent')
            ->from('closings c')
            ->leftJoin('transactions t','c.transaction_id = t.id')
            ->leftJoin('sources s','s.id = t.source_id')
            ->where('DATE(c.actual_closing_datetime) >=:fromDate', array(':fromDate'=>$dateRange['from_date']))
            ->andWhere('DATE(c.actual_closing_datetime) <=:toDate', array(':toDate'=>$dateRange['to_date']))
            ->andWhere('c.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID)
            ->group('s.id')
            ->order('businessPercent DESC')
            ->queryAll();

        $ClosedGraphLabel = '';
        $ClosedGraphData = '';
        $ClosedGraphDataMax = 100;
        foreach($results as $key => $row) {

            if($key==0 && $row['businessPercent'] > 0) {
                $ClosedGraphDataMax = ceil($row['businessPercent'] / 5) * 5;
            }

            $ClosedGraphLabel .= ($ClosedGraphLabel) ? ',' : '';
            $ClosedGraphLabel .= "[$key, '{$row['parentSource']}']";

            $ClosedGraphData .= ($ClosedGraphData) ? ',' : '';
            $ClosedGraphData .= "[$key, {$row['businessPercent']}]";
        }

		$model = new Closings;
		$PendingCriteria = $model->byClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_ACTIVE_ID))->getDbCriteria();
		$PendingTotal = $model->count($PendingCriteria);
		$PendingVolume = Closings::getVolumeTotal($PendingCriteria);
		$PendingSourceCriteria = $this->buildSourceCriteria($PendingCriteria,$PendingTotal);
		$PendingDataProvider = $this->buildDataProvider('Closings', $PendingSourceCriteria);

		$model = new Closings;
		$FalloutCriteria = $model->byClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_FALLOUT_ID))->getDbCriteria();
		$FalloutTotal = $model->count($FalloutCriteria);
		$FalloutVolume = Closings::getVolumeTotal($FalloutCriteria);
		$FalloutSourceCriteria = $this->buildSourceCriteria($FalloutCriteria,$FalloutTotal);
		$FalloutDataProvider = $this->buildDataProvider('Closings', $FalloutSourceCriteria);

        // Appointments Set
        $AppointmentsTotal = Appointments::model()->countSetAppointments($dateRange);
        $AppointmentsVolume = null;
        $AppointmentData = $this->getAppointmentsData($dateRange, $AppointmentsTotal);
        $AppointmentsDataProvider = new CArrayDataProvider($AppointmentData, array(
            'pagination'=>array(
                'pageSize'=>100,
            )
        ));

        // Leads
        $LeadsTotal = Yii::app()->db->createCommand('select count(*) from transactions where origin_type NOT IN("'.Transactions::ORIGIN_TYPE_DIALER_IMPORT.'") AND DATE(added) >="'.$dateRange['from_date'].'" AND DATE(added) <="'.$dateRange['to_date'].'"')->queryScalar();
        $LeadsVolume = null;
        $LeadsData = $this->getLeadsData($dateRange, $LeadsTotal);
        $LeadsDataProvider = new CArrayDataProvider($LeadsData, array(
            'pagination'=>array(
                'pageSize'=>100,
            )
        ));

		$this->controller->render('sources', array(
			 'dateRange'=>$dateRange,
             'DateRanger' => $DateRanger,
             'ClosedGraphDataMax' => $ClosedGraphDataMax,
             'ClosedGraphData' => $ClosedGraphData,
             'ClosedGraphLabel' => $ClosedGraphLabel,
			 'ClosedDataProvider'=>$ClosedDataProvider,
			 'ClosedVolume'=>$ClosedVolume,
			 'ClosedTotal'=>$ClosedTotal,
			 'PendingDataProvider'=>$PendingDataProvider,
			 'PendingTotal'=>$PendingTotal,
			 'PendingVolume'=>$PendingVolume,
			 'FalloutDataProvider'=>$FalloutDataProvider,
			 'FalloutTotal'=>$FalloutTotal,
			 'FalloutVolume'=>$FalloutVolume,
             'AppointmentsDataProvider'=>$AppointmentsDataProvider,
             'AppointmentsTotal'=>$AppointmentsTotal,
             'AppointmentsVolume'=>$AppointmentsVolume,
             'LeadsDataProvider'=>$LeadsDataProvider,
             'LeadsTotal'=>$LeadsTotal,
             'LeadsVolume'=>$LeadsVolume,
		));
	}

	public function buildSourceCriteria($OriginalCriteria, $allTotal)
    {
		$Criteria = new CDbCriteria;
		$Criteria->select = 't.*, sources.name parentSource, count(*) as countTotal, sum(price) as volumeTotal, count(*)/'.$allTotal.' as businessPercent'; //
		$Criteria->join = 'LEFT JOIN transactions ON t.transaction_id = transactions.id LEFT JOIN sources ON sources.id = transactions.source_id';
        $Criteria->group = 'sources.id ASC';
//		$Criteria->group = 'parentSource ASC';
		$Criteria->order = 'businessPercent DESC';
		$OriginalCriteria->mergeWith($Criteria);
		return $OriginalCriteria;
	}

    public function getAppointmentsData($dateRange, $allTotal)
    {
        $sourceIdCommand = Yii::app()->db->createCommand()
            ->select('DISTINCT(s.id)')
            ->from('sources s')
            ->join('transactions t','t.source_id=s.id')
            ->leftJoin('closings c','c.transaction_id=t.id')
            ->leftJoin('appointments a','t.id=a.component_id AND t.component_type_id=a.component_type_id AND t.component_type_id IN ('.ComponentTypes::SELLERS.','.ComponentTypes::BUYERS.')')
            ->where('DATE(a.set_on_datetime) >=:from_date AND DATE(a.set_on_datetime) <=:to_date', array(':from_date' => $dateRange['from_date'], ':to_date' => $dateRange['to_date']))
            ->orWhere('c.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID.' AND c.actual_closing_datetime >=:closed_from_date AND c.actual_closing_datetime <=:closed_to_date',array(':closed_from_date' => $dateRange['from_date'], ':closed_to_date' => $dateRange['to_date']));


        return Yii::app()->db->createCommand()
            ->select('s.name source_name, count(a.id) as countTotal, IF(count(a.id) > 0,(count(a.id)/'.$allTotal.'*100), 0) businessPercent, '

                // total closings count
                .'(SELECT count(c.id) FROM closings c LEFT JOIN transactions t2 ON t2.id=c.transaction_id WHERE c.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID.' AND DATE(c.actual_closing_datetime) >="'.$dateRange['from_date'].'" AND DATE(c.actual_closing_datetime) <="'.$dateRange['to_date'].'" AND t2.source_id=s.id) total_closings, '

                // total closing volume
                .'(SELECT SUM(c2.price) FROM closings c2 LEFT JOIN transactions t3 ON t3.id=c2.transaction_id WHERE c2.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID.' AND c2.actual_closing_datetime >="'.$dateRange['from_date'].'" AND c2.actual_closing_datetime <="'.$dateRange['to_date'].'" AND t3.source_id=s.id) total_closing_volume')

            ->from('sources s')
            ->join('transactions t','t.source_id=s.id')
            ->leftJoin('appointments a','t.id=a.component_id AND t.component_type_id=a.component_type_id AND t.component_type_id IN ('.ComponentTypes::SELLERS.','.ComponentTypes::BUYERS.') AND DATE(a.set_on_datetime) >=:from_date AND DATE(a.set_on_datetime) <=:to_date', array(':from_date' => $dateRange['from_date'], ':to_date' => $dateRange['to_date']))
            ->andWhere(array('in', 's.id', $sourceIdCommand->queryColumn()))
            ->group('s.id')
            ->order('businessPercent DESC')
            ->queryAll();
    }

    public function getLeadsData($dateRange, $allTotal)
    {
        return Yii::app()->db->createCommand()
            ->select('s.name source_name, count(t.id) as countTotal, IF(count(t.id) > 0,(count(*)/'.$allTotal.'*100), 0) businessPercent')

//                // total closings count
//                .'(SELECT count(c.id) FROM closings c LEFT JOIN transactions t2 ON t2.id=c.transaction_id WHERE c.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID.' AND DATE(c.actual_closing_datetime) >="'.$dateRange['from_date'].'" AND DATE(c.actual_closing_datetime) <="'.$dateRange['to_date'].'" AND t2.source_id=s.id) total_closings, '
//
//                // total closing volume
//                .'(SELECT SUM(c2.price) FROM closings c2 LEFT JOIN transactions t3 ON t3.id=c2.transaction_id WHERE c2.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID.' AND c2.actual_closing_datetime >="'.$dateRange['from_date'].'" AND c2.actual_closing_datetime <="'.$dateRange['to_date'].'" AND t3.source_id=s.id) total_closing_volume')
//
            ->from('sources s')
            ->join('transactions t','t.source_id=s.id AND origin_type NOT IN("'.Transactions::ORIGIN_TYPE_DIALER_IMPORT.'") AND DATE(t.added) >=:from_date AND DATE(t.added) <=:to_date', array(':from_date' => $dateRange['from_date'], ':to_date' => $dateRange['to_date']))
            ->group('s.id')
            ->order('businessPercent DESC')
            ->queryAll();
    }

	public function buildDataProvider($model, $Criteria, $pageSize=100)
    {
		return new CActiveDataProvider($model, array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>$pageSize))); //@todo: remove pagination
	}
}