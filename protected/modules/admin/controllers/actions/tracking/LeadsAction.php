<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class LeadsAction extends CAction
{

    public function run()
    {
        $this->controller->title = 'Leads Report';
        $this->controller->pageColor = 'teal';

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'last_12_months';
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $Criteria = new CDbCriteria;
        $Criteria->select = 'date_format(added, "%Y-%m-01") as leadDate, COUNT(*) as leadCount, component_type_id';
        $Criteria->addNotInCondition('origin_type', array(Transactions::ORIGIN_TYPE_DIALER_IMPORT));
        $Criteria->order = 'added DESC';
        $Criteria->group = 'YEAR(added), MONTH(added)';

        $leadsArray = array();

        $buyerModel = new Buyers('search');
        if ($buyerResults = $buyerModel->byDateRange($dateRange, 'added')->byComponentTypeId(ComponentTypes::BUYERS)->findAll($Criteria)) {
            $buyersArray = array();
            foreach ($buyerResults as $buyer) {
                $buyersArray[$buyer->leadDate] = $buyer->leadCount;
                $leadsArray[$buyer->leadDate]['buyers'] = $buyer->leadCount;
                $leadsArray[$buyer->leadDate]['date'] = date($buyer->leadDate, strtotime("m-Y"));
                $leadsArray[$buyer->leadDate]['dateLabel'] = date('M Y', strtotime($buyer->leadDate));
            }
            $buyerLeadCount = array_sum($buyersArray);
        }

        $sellerModel = new Sellers('search');
        if ($sellerResults = $sellerModel->byDateRange($dateRange, 'added')->byComponentTypeId(ComponentTypes::SELLERS)->findAll($Criteria)) {
            $sellersArray = array();
            foreach ($sellerResults as $seller) {
                $sellersArray[$seller->leadDate] = $seller->leadCount;
                $leadsArray[$seller->leadDate]['sellers'] = $seller->leadCount;
                $leadsArray[$seller->leadDate]['date'] = date($seller->leadDate, strtotime("m-Y"));
                if(!isset($leadsArray[$seller->leadDate]['dateLabel'])) {
                    $leadsArray[$seller->leadDate]['dateLabel'] = date('M Y', strtotime($seller->leadDate));
                }
            }
            $sellerLeadCount = array_sum($sellersArray);
        }

        foreach($leadsArray as $key => $monthlyLeads) {
            if(!isset($leadsArray[$key]['buyers'])) {
                $leadsArray[$key]['buyers'] = 0;
            }
            if(!isset($leadsArray[$key]['sellers'])) {
                $leadsArray[$key]['sellers'] = 0;
            }
            $leadsArray[$key]['buyerLeadPercent'] = ($leadsArray[$key]['buyers'])? Yii::app()->format->formatPercentages($leadsArray[$key]['buyers']/($leadsArray[$key]['buyers']+$leadsArray[$key]['sellers'])): '0%';
            $leadsArray[$key]['sellerLeadPercent'] = ($leadsArray[$key]['sellers'])? Yii::app()->format->formatPercentages($leadsArray[$key]['sellers']/($leadsArray[$key]['buyers']+$leadsArray[$key]['sellers'])): '0%';
        }

        $leadsArrayModified = array();
        if (!empty($leadsArray)) {
            krsort($leadsArray);
            foreach ($leadsArray as $key => $value) {
                $leadsArrayModified[] = $value;
            }
        }
        $DataProvider = new CArrayDataProvider($leadsArrayModified, array('pagination' => false));

        $this->controller->render(
            'leads', array(
                'DataProvider'    => $DataProvider,
                'dateRange'       => $dateRange,
                'totalCount'      => $buyerLeadCount + $sellerLeadCount,
                'buyerLeadCount'  => $buyerLeadCount,
                'sellerLeadCount' => $sellerLeadCount,
            )
        );
    }
}
