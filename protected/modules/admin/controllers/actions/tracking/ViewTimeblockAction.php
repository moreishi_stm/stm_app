<?php
/**
 * @author Chris Willard <chriswillard.dev@gmail.com>
 * @since 07/23/2013
 */

class ViewTimeblockAction extends CAction {

    public function run($id) {

        $timeblockEntry = Timeblockings::model()->findByPk($id);

        $this->processAjaxRequest($timeblockEntry);
    }

    protected function processAjaxRequest(Timeblockings $timeblockModel) {

        if (!Yii::app()->getRequest()->isAjaxRequest) {
            return false;
        }

        $timeblockModel->date = Yii::app()->format->formatDate($timeblockModel->date);
        $timeblockModel->startHour = date('G', strtotime($timeblockModel->start_time));
        $timeblockModel->endHour = date('G', strtotime($timeblockModel->end_time));
        $timeblockModel->startMinute = date('i', strtotime($timeblockModel->start_time));
        $timeblockModel->endMinute = date('i', strtotime($timeblockModel->end_time));

        $timeblockData = CMap::mergeArray($timeblockModel->attributes, array(
            'startHour' => $timeblockModel->startHour,
            'endHour' => $timeblockModel->endHour,
            'startMinute' => $timeblockModel->startMinute,
            'endMinute' => $timeblockModel->endMinute,
        ));

        $jsonTimeblockData = CJSON::encode($timeblockData);
        echo $jsonTimeblockData;

        Yii::app()->end();
    }
}