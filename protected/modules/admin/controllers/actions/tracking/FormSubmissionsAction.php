<?php

class FormSubmissionsAction extends CAction
{
    public function run($id = null)
    {

        $this->controller->title = 'Seller Leads Report';
        $this->controller->pageColor = 'teal';

        $model = new FormSubmissions();
        $model->unsetAttributes();

        if ($_GET['FormSubmissions']) {
            $model->attributes = $_GET['FormSubmissions'];
        }

        $this->controller->render('formSubmissions', array(
            'model' => $model,
//            'dateRange'=>$dateRange,
        ));
    }
}