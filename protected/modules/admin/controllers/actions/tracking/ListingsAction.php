<?php
	Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class ListingsAction extends CAction {

		public function run() {
			$this->controller->title = 'Listings Tracker';
			$this->controller->pageColor = 'teal';

			$model = new Appointments;

			$DateRanger = new DateRanger();
			if($_POST['DateRanger']['date_preset'])
				$DateRanger->defaultSelect = $_POST['DateRanger']['date_preset'];
			$dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

			$opts = $dateRange;
			$opts['component_type_id'] = ComponentTypes::SELLERS;

			$Appointments = new Appointments;
			$apptSetCriteria = $Appointments->dateRange($dateRange,'set_on_datetime')->byComponentTypeIds(ComponentTypes::SELLERS)->byAppointmentMetStatus(array(Appointments::MET_STATUS_SET,Appointments::MET_STATUS_MET))->addedAsc()->getDbCriteria();
			$apptMetCriteria = $Appointments->dateRange($dateRange,'set_on_datetime')->byComponentTypeIds(ComponentTypes::SELLERS)->byAppointmentMetStatus(Appointments::MET_STATUS_MET)->addedAsc()->getDbCriteria();
			$apptSetDataProvider = new CActiveDataProvider('Appointments', array('criteria' => $apptSetCriteria,'pagination' => array('pageSize' => 100)));
			$apptMetDataProvider = new CActiveDataProvider('Appointments', array('criteria' => $apptMetCriteria,'pagination' => array('pageSize' => 100)));

			$Sellers = new Sellers;
            $newListingsCount = $listingsTakenCount = Sellers::countListingsTaken($opts);
			$newListingsVolume = Sellers::listingsTakenVolume($opts);
			$newListingsCriteria = $Sellers->byListingDate($opts)->getDbCriteria();
			$newListingsDataProvider = new CActiveDataProvider('Sellers', array('criteria'=>$newListingsCriteria,'pagination'=>array('pageSize'=>100))); //@todo: remove pagination

			$Sellers = new Sellers;
			$expiredWithdrawnListingsCriteria = $Sellers->byStatusIds(array(TransactionStatus::EXPIRED_ID,TransactionStatus::CANCELLED_ID))->getDbCriteria(); //@todo:add expired/withdrawn daterange criteria
			$expiredWithdrawnListingsDataProvider = new CActiveDataProvider('Sellers', array('criteria'=>$expiredWithdrawnListingsCriteria,'pagination'=>array('pageSize'=>100))); //@todo: remove pagination
			$expiredWithdrawnListingsVolume = Sellers::expiredWithdrawnVolume($opts);

			$Closings = new Closings;
			$executedContractCriteria = $Closings->dateRangeContractExecuted($opts)->byComponentTypeIds(ComponentTypes::SELLERS)->getDbCriteria();
			$executedContractCriteria->order = 'contract_execute_date DESC';
			$executedContractDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$executedContractCriteria,'pagination'=>array('pageSize'=>100))); //@todo: remove pagination
			$executedContractVolume = Closings::model()->volumeTotal($dateRange = $opts, $status = 'executed', ComponentTypes::SELLERS);

			// dateRange, field, status, componentType
			$Closings = new Closings;
			$closedCriteria = $Closings->byClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_CLOSED_ID))->byComponentTypeIds(ComponentTypes::SELLERS)->getDbCriteria();
			$closedCriteria->order = 'contract_execute_date DESC';
			$closedDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$closedCriteria,'pagination'=>array('pageSize'=>100))); //@todo: remove pagination
			$closedVolume = Closings::model()->volumeTotal($dateRange = $opts, $status = 'closed', ComponentTypes::SELLERS);

			$scheduledClosingsCount = Closings::model()->byClosingDate($opts)->byComponentTypeIds(ComponentTypes::SELLERS)->byStatus(Closings::STATUS_TYPE_ACTIVE_ID)->count();
			$scheduledClosingsVolume = Closings::model()->volumeTotal($dateRange = $opts, $status = 'scheduled', ComponentTypes::SELLERS);

			$Closings = new Closings;
			$falloutCriteria = $Closings->byClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_FALLOUT_ID))->byComponentTypeIds(ComponentTypes::SELLERS)->getDbCriteria();
			$falloutCriteria->order = 'contract_execute_date DESC';
			$falloutDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$falloutCriteria,'pagination'=>array('pageSize'=>100))); //@todo: remove pagination
			$falloutVolume = Closings::getVolumeTotal($falloutCriteria);

			$dataArray = array(
				'dateRange' => $dateRange,
				'newListingsCount' => $newListingsCount,
				'newListingsVolume' => $newListingsVolume,
				'newListingsDataProvider' => $newListingsDataProvider,
				'expiredWithdrawnListingsCount'=>$expiredWithdrawnListingsDataProvider->totalItemCount,
				'expiredWithdrawnListingsVolume'=>$expiredWithdrawnListingsVolume,
				'expiredWithdrawnListingsDataProvider'=> $expiredWithdrawnListingsDataProvider,
				'executedContractCount' => $executedContractDataProvider->totalItemCount,
				'executedContractVolume' => $executedContractVolume,
				'executedContractDataProvider' => $executedContractDataProvider,
				'scheduledClosingsCount' => $scheduledClosingsCount,
				'scheduledClosingsVolume' => $scheduledClosingsVolume,
				'closedCount' => $closedDataProvider->totalItemCount,
				'closedVolume' => $closedVolume,
				'closedDataProvider'=>$closedDataProvider,
				'falloutVolume' => $falloutVolume,
				'falloutDataProvider' => $falloutDataProvider,
				'apptSetCount' => $apptSetDataProvider->totalItemCount,
				'apptSetDataProvider' => $apptSetDataProvider,
				'apptMetCount' => $apptMetDataProvider->totalItemCount,
				'apptMetDataProvider' => $apptMetDataProvider,
				'listingsTakenCount' => $listingsTakenCount,
				'apptListingConversion' => ($apptMetDataProvider->totalItemCount) ? number_format(($newListingsCount / $apptMetDataProvider->totalItemCount) * 100 ): 0,
				'DateRangerSelect'=>$DateRanger->defaultSelect,
			);

			if (Yii::app()->request->isAjaxRequest) {
				$this->controller->renderPartial('listings', $dataArray);
			} else {
				$this->controller->render('listings', $dataArray);
			}
		}
		//	public function performAjaxRequest() {
		//	}
	}