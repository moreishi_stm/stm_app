<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class TrafficAction extends CAction
{
	public function run()
	{
		$this->controller->pageColor = 'teal';
		// $model = new Timeblockings;

		$this->controller->render('traffic', array(
			// 'model'=>$model,
		));
	}
}