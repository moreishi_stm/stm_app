<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
Yii::import('admin_widgets.ActivityLogPortlet.ActivityLogPortlet');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class ActivitiesAction extends CAction {

	public function run($id=null) {

		$this->controller->title = 'Activity Details Report';
		$this->controller->pageColor = 'teal';

        if(!empty($_GET['contact_id']))  {
            $contactIds = $_GET['contact_id'];
        } elseif($id==null) {
            $contactIds = Yii::app()->user->id;
        } else {
            $Contact = Contacts::model()->findByPk($id);
            $contactIds = $Contact->id;
        }

        if(!empty($_GET['assigned_by_contact_id'])) {
            $assignedById = $_GET['assigned_by_contact_id'];
        }

        if(!empty($_GET['task_type_id'])) {
            $taskTypeId = $_GET['task_type_id'];
        }

		$DateRanger = new DateRanger();
		$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

		$model = new ActivityLog;
		$spokeToValues = $_GET['spoke_to'];
		if($spokeToValues==null || $spokeToValues==2)
            $Criteria = $model->byAddedBy($contactIds,$dateRange)->byTaskAssignedBy($assignedById)->byTaskType($taskTypeId)->getDbCriteria();
		else
            $Criteria = $model->bySpokeTo($spokeToValues)->byAddedBy($contactIds,$dateRange)->byTaskAssignedBy($assignedById)->getDbCriteria();

        if(!empty($_GET['notes'])) {

            $Criteria->compare('note', $_GET['notes'], true);

            $model->setDbCriteria($Criteria);
        }

        $Criteria->order = 'activity_date ASC';

        $sort = new CSort();
        $sort->defaultOrder = 'activity_date DESC';
        $sort->attributes = array(
            'date'=>array(
                'asc'=> 'activity_date ASC',
                'desc'=>'activity_date DESC',
                'label' => 'Date',
            ),
        );

		$DataProvider = new CActiveDataProvider($model, array('pagination'=>array('pageSize'=>100)));
        $DataProvider->sort = $sort;

		$this->controller->render('activities', array(
										   'DataProvider'=>$DataProvider,
										   'dateRange'=>$dateRange,
                                           'contactIds'=>$contactIds,
										   ));
	}
}
