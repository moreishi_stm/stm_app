<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AppointmentTimesAction extends CAction
{
    public $agentAppointments;
    public $totalAppointments;

	public function run($id=null)
	{
		$this->controller->title = 'Appointment Times Report';
		$this->controller->pageColor = 'red';

        $DateRanger = new DateRanger();
        if($_GET['DateRanger']['date_preset']){
            $DateRanger->defaultSelect = $_GET['DateRanger']['date_preset'];
        }
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $assignedToCondition = (is_numeric($_GET['set_by_id'])) ? ' AND set_by_id='.$_GET['set_by_id'].' ' : '';
        $componentTypeIds = (is_numeric($_GET['component_type_id']) || $_GET['component_type_id'] == '2,3') ? $_GET['component_type_id']: '2,3,5';
        $dateRangeCondition = ($dateRange) ? ' AND DATE(set_on_datetime) >= "'.$dateRange['from_date'].'" AND DATE(set_on_datetime) <= "'.$dateRange['to_date'].'" ' : '';

        // this is used for percentages
        $appointmentCount = Yii::app()->db->createCommand("select count(id) count from appointments where component_type_id IN({$componentTypeIds}) {$assignedToCondition} {$dateRangeCondition}")->queryScalar();
        $results = Yii::app()->db->createCommand("select DATE_FORMAT(set_on_datetime, '%k') hour, count(id) count, ROUND((count(id) / {$appointmentCount}) * 100) percent FROM appointments where component_type_id IN({$componentTypeIds}) {$assignedToCondition} {$dateRangeCondition} GROUP BY DATE_FORMAT(set_on_datetime, '%H') ORDER BY DATE_FORMAT(set_on_datetime, '%H') ASC")->queryAll();
        $maxPercent = 0;
        $counts = array();
        $dataString = '';
        if(!empty($results)) {
            foreach($results as $i =>$row) {
                $counts[$row['hour']]['count'] = $row['count'];
                $counts[$row['hour']]['percent'] = $row['percent'];
            }

            for($i=6; $i<24; $i++) {
                $dataString .= ($dataString) ? ',' : '';
                $percent = ($counts[$i]['percent'])? $counts[$i]['percent'] : 0;
                $dataString .= '['.($i-6).', '.$percent.']'; // [0,$kpiclosingsytdPercent],
                $maxPercent = ($counts[$i]['percent'] > $maxPercent) ? $counts[$i]['percent'] : $maxPercent;
            }
            $maxPercent = ($maxPercent == 0) ? 100 : $maxPercent;
        }

        $this->controller->render('appointmentTimes', array(
            'dataString' => ($dataString) ? $dataString : '[]',
            'maxPercent' => ($maxPercent) ? $maxPercent : '100',
            'dateRangerSelect' =>$DateRanger->defaultSelect,
            'dateRange' =>$dateRange,
		));
	}

    public function printAgentField($agentId, $field, $emptyValue='-', $wrapper=null)
    {
        return $this->printField('agent', $agentId, $field, $emptyValue, $wrapper);
    }

    public function printAgentFieldSum($agentId, $fields, $emptyValue='-', $wrapper=null)
    {
        $value = 0;
        if(is_array($fields)) {
            foreach($fields as $field) {
                $singleValue = $this->printField('agent', $agentId, $field, '');
                $value += (!empty($singleValue))? $singleValue : 0;
            }
        }

        if($wrapper && $value > 0) {
            switch($wrapper) {
                case '()':
                    $value = '('.$value.')';
                    break;
                case '[]':
                    $value = '['.$value.']';
                    break;
                case '{}':
                    $value = '{'.$value.'}';
                    break;
            }
        } elseif(empty($value)) {
            $value = $emptyValue;
        }

        return $value;
    }

    public function printTotalField($field, $emptyValue='-', $wrapper=null)
    {
        return $this->printField('total', null, $field, $emptyValue, $wrapper);
    }

    protected function printField($type, $agentId=null, $field, $emptyValue='-', $wrapper=null)
    {
        switch($type) {
            case 'agent':
                if(empty($agentId)) {
                    throw new Exception('Agent ID is missing.');
                }
                $value = $this->agentAppointments[$agentId][$field];
                break;

            case 'total':
                $value = $this->totalAppointments[$field];
                break;

            default:
                throw new Exception('Print field type must be agent or total.');
                break;
        }

        if($wrapper && $value > 0) {
            switch($wrapper) {
                case '()':
                    $value = '('.$value.')';
                    break;
                case '[]':
                    $value = '['.$value.']';
                    break;
                case '{}':
                    $value = '{'.$value.'}';
                    break;
            }
        } elseif(empty($value)) {
            $value = $emptyValue;
        }

        return $value;
    }
}