<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class ClosingsAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Closings Reports';
		$this->controller->pageColor = 'teal';

		$DateRanger = new DateRanger();
		if($_POST['DateRanger']['date_preset'])
			$DateRanger->defaultSelect = $_POST['DateRanger']['date_preset'];
		$dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

        $model = new Closings;
		$PendingCriteria = $model->byClosingDate(array('to_date' => $dateRange['to_date']))->byStatus(array(Closings::STATUS_TYPE_ACTIVE_ID))->byAssigneeId($_POST['contactId'])->byComponentTypeIds($_POST['componentTypeId'])->byAccountId(Yii::app()->user->accountId)->getDbCriteria();
		$PendingCriteria->order = 'contract_execute_date DESC';
		$PendingVolume = Closings::getVolumeTotal($PendingCriteria);
        $PendingGCI = Closings::getGCITotal($PendingCriteria);
        $PendingDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$PendingCriteria,'pagination'=>array('pageSize'=>1000))); //@todo: remove pagination

        $PendingAgentCommissionTotal = Yii::app()->db->createCommand()
            ->select("SUM(at.amount)")
            ->from("accounting_transactions at")
            ->join("closings c","at.closing_id=c.id")
            ->join("transactions t","c.transaction_id=t.id")
            ->where("DATE(c.contract_closing_date) BETWEEN :fromDate AND :toDate", array(':fromDate'=> $dateRange['from_date'],':toDate'=>$dateRange['to_date']))
            ->andWhere("accounting_account_id IN (".AccountingAccounts::SALES_COMMISSION.") AND at.is_deleted=0")
            ->andWhere("c.closing_status_ma=".Closings::STATUS_TYPE_ACTIVE_ID)
            ->andWhere(array('in','t.component_type_id',(($_POST['componentTypeId']) ? (array) $_POST['componentTypeId'] : array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))));
		if(!empty((int)$_POST["contactId"])){

			$PendingAgentCommissionTotal->andWhere("at.agent_id=:agentId", array(':agentId'=>(int)$_POST["contactId"]));
		}
		$PendingAgentCommissionTotal = $PendingAgentCommissionTotal->queryScalar();



        $PendingInternalReferralCommissionTotal = Yii::app()->db->createCommand()
            ->select(" SUM(at.amount)")
            ->from("accounting_transactions at")
            ->join("closings c","at.closing_id=c.id")
            ->join("transactions t","c.transaction_id=t.id")
            ->where("DATE(c.contract_closing_date) BETWEEN :fromDate AND :toDate", array(':fromDate'=> $dateRange['from_date'],':toDate'=>$dateRange['to_date']))
            ->andWhere("c.closing_status_ma = ".Closings::STATUS_TYPE_ACTIVE_ID." AND accounting_account_id IN (".AccountingAccounts::REFERRAL_FEE_INTERNAL.") AND at.is_deleted=0")
            ->andWhere(array('in','t.component_type_id',(($_POST['componentTypeId']) ? (array) $_POST['componentTypeId'] : array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))));

		if(!empty($_POST["contactId"])){
			$PendingInternalReferralCommissionTotal
				->leftJoin(
					"assignments",
					"assignments.component_id = t.id AND assignments.component_type_id = t.component_type_id"
				);
			$PendingInternalReferralCommissionTotal
				->andWhere(
					"assignments.assignee_contact_id = :assignee_contact_id"
					,array(":assignee_contact_id" =>$_POST["contactId"])
				);
		}

		$PendingInternalReferralCommissionTotal = $PendingInternalReferralCommissionTotal->queryScalar();

		//$PendingGCI = "1050000000";
		//$PendingAgentCommissionTotal = "101000000";

        $PendingNetCommissionTotal = $PendingGCI - $PendingAgentCommissionTotal - $PendingInternalReferralCommissionTotal;

        $model = new Closings;
        $Criteria = new CDbCriteria;
        //$Criteria->compare('DATE(contract_execute_date)', '<=' . $dateRange['to_date'], true);

        $AllPendingCriteria = $model->byStatus(array(Closings::STATUS_TYPE_ACTIVE_ID))->byAssigneeId($_POST['contactId'])->byComponentTypeIds($_POST['componentTypeId'])->byAccountId(Yii::app()->user->accountId)->getDbCriteria();
        $AllPendingCriteria->order = 'contract_closing_date ASC';
        $AllPendingCriteria->mergeWith($Criteria);

		$AllPendingDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$AllPendingCriteria,'pagination'=>array('pageSize'=>1000))); //@todo: remove pagination

		$AllPendingGCI = Closings::getGCITotal($AllPendingCriteria);

		$AllPendingVolume = Closings::getVolumeTotal($AllPendingCriteria);

		$AllPendingAgentCommissionTotal = Yii::app()->db->createCommand()
            ->select(" SUM(at.amount)")
            ->from("accounting_transactions at")
            ->join("closings c","at.closing_id=c.id")
            ->join("transactions t","c.transaction_id=t.id")
            ->andWhere("c.closing_status_ma = ".Closings::STATUS_TYPE_ACTIVE_ID." AND accounting_account_id IN (".AccountingAccounts::SALES_COMMISSION.") AND at.is_deleted=0")
            ->andWhere(array('in','t.component_type_id',(($_POST['componentTypeId']) ? (array) $_POST['componentTypeId'] : array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))));

		if(!empty((int)$_POST["contactId"])){
			$AllPendingAgentCommissionTotal->leftJoin("assignments","assignments.component_id = t.id AND assignments.component_type_id = t.component_type_id");
			$AllPendingAgentCommissionTotal->where("assignments.assignee_contact_id = :assignee_contact_id",array(
					":assignee_contact_id" => $_POST["contactId"] )
			);
		}

		$AllPendingAgentCommissionTotal = $AllPendingAgentCommissionTotal->queryScalar();

        $AllPendingInternalReferralCommissionTotal = Yii::app()->db->createCommand()
            ->select(" SUM(at.amount)")
            ->from("accounting_transactions at")
            ->join("closings c","at.closing_id=c.id")
            ->join("transactions t","c.transaction_id=t.id")
            ->andWhere("c.closing_status_ma = ".Closings::STATUS_TYPE_ACTIVE_ID." AND accounting_account_id IN (".AccountingAccounts::REFERRAL_FEE_INTERNAL.") AND at.is_deleted=0")
			->andWhere(array('in','t.component_type_id',(($_POST['componentTypeId']) ? (array) $_POST['componentTypeId'] : array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))))
            ->queryScalar();

        $AllPendingNetCommissionTotal = $AllPendingGCI - $AllPendingAgentCommissionTotal - $AllPendingInternalReferralCommissionTotal;

		$model = new Closings;
		$ClosedCriteria = $model->byActualClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_CLOSED_ID))->byAssigneeId($_POST['contactId'])->byComponentTypeIds($_POST['componentTypeId'])->byAccountId(Yii::app()->user->accountId)->getDbCriteria();
		$ClosedCriteria->order = 'actual_closing_datetime DESC';
		$ClosedDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$ClosedCriteria,'pagination'=>array('pageSize'=>1000))); //@todo: remove pagination
        $ClosedGCI = Closings::getGCITotal($ClosedCriteria);
		$ClosedVolume = Closings::getVolumeTotal($ClosedCriteria);

		$ClosedAgentCommissionTotalCommand = Yii::app()->db->createCommand()
            ->select(" SUM(at.amount)")
            ->from("accounting_transactions at")
            ->join("closings c","at.closing_id=c.id")
            ->join("transactions t","c.transaction_id=t.id")
            ->where('DATE(c.actual_closing_datetime) >= "'.$dateRange['from_date'].'" AND DATE(c.actual_closing_datetime) <= "'.$dateRange['to_date'].'"')
            ->andWhere("c.closing_status_ma = ".Closings::STATUS_TYPE_CLOSED_ID." AND accounting_account_id IN (".AccountingAccounts::SALES_COMMISSION.") AND at.is_deleted=0")
            ->andWhere(array('in','t.component_type_id',(($_POST['componentTypeId']) ? (array) $_POST['componentTypeId'] : array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))))
            ;
		if(!empty((int)$_POST["contactId"])){
            $ClosedAgentCommissionTotalCommand->andWhere("at.agent_id=:agentId", array(':agentId'=>(int)$_POST["contactId"]));
		}
		$ClosedAgentCommissionTotal = $ClosedAgentCommissionTotalCommand->queryScalar();

        $ClosedInternalReferralCommissionTotalCommand = Yii::app()->db->createCommand()
            ->select(" SUM(at.amount)")
            ->from("accounting_transactions at")
            ->join("closings c","at.closing_id=c.id")
            ->join("transactions t","c.transaction_id=t.id")
            ->where('DATE(c.actual_closing_datetime) >= "'.$dateRange['from_date'].'" AND DATE(c.actual_closing_datetime) <= "'.$dateRange['to_date'].'"')
            ->andWhere("c.closing_status_ma = ".Closings::STATUS_TYPE_CLOSED_ID." AND accounting_account_id IN (".AccountingAccounts::REFERRAL_FEE_INTERNAL.") AND at.is_deleted=0")
            ->andWhere(array('in','t.component_type_id',(($_POST['componentTypeId']) ? (array) $_POST['componentTypeId'] : array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))));
        if(!empty((int)$_POST["contactId"])){
            $ClosedInternalReferralCommissionTotalCommand->andWhere("at.agent_id=:agentId", array(':agentId'=>(int)$_POST["contactId"]));
        }
        $ClosedInternalReferralCommissionTotal = $ClosedInternalReferralCommissionTotalCommand->queryScalar();

        $ClosedNetCommissionTotal = $ClosedGCI - $ClosedAgentCommissionTotal - $ClosedInternalReferralCommissionTotal;

		$model = new Closings;
		$ExecutedCriteria = $model->byExecuteDate($dateRange)->byAssigneeId($_POST['contactId'])->byAccountId(Yii::app()->user->accountId)->getDbCriteria();
		$ExecutedCriteria->order = 'contract_execute_date DESC';
		$ExecutedDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$ExecutedCriteria,'pagination'=>array('pageSize'=>1000))); //@todo: remove pagination
        $ExecutedGCI = Closings::getGCITotal($ExecutedCriteria);
		$ExecutedVolume = Closings::getVolumeTotal($ExecutedCriteria);

		$model = new Closings;
		$FalloutCriteria = $model->byFalloutDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_FALLOUT_ID))->byComponentTypeIds(ComponentTypes::BUYERS)->byAssigneeId($_POST['contactId'])->byComponentTypeIds($_POST['componentTypeId'])->byAccountId(Yii::app()->user->accountId)->getDbCriteria();
		$FalloutCriteria->order = 'contract_execute_date DESC';
		$FalloutDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$FalloutCriteria,'pagination'=>array('pageSize'=>1000))); //@todo: remove pagination
        $FalloutGCI = Closings::getGCITotal($FalloutCriteria);
		$FalloutVolume = Closings::getVolumeTotal($FalloutCriteria);

        /**
         * Get last 12 months reporting buyers / sellers
         */
        $results = Yii::app()->db->createCommand()
            ->select(   'DATE_FORMAT(c.actual_closing_datetime,"%b %y") date,
                        DATE_FORMAT(c.actual_closing_datetime,"%m") month,
                        DATE_FORMAT(c.actual_closing_datetime,"%Y") year,
                        COUNT(c.id) closings,
                        COALESCE (ROUND(SUM(commission_amount)), 0) grossCommission,
                        t.component_type_id,
                        ROUND(COALESCE ((SELECT SUM(amount) FROM accounting_transactions a JOIN closings c2 ON c2.id = a.closing_id JOIN transactions t2 ON t2.id = c2.transaction_id WHERE t2.component_type_id = t.component_type_id AND DATE_FORMAT(c.actual_closing_datetime, "%Y%m") = DATE_FORMAT(c2.actual_closing_datetime, "%Y%m") AND (c2.closing_status_ma = 3)),0)) AS expense
                        ')
            ->from('closings c')
            ->join('transactions t','c.transaction_id = t.id')
            ->where('DATE(c.actual_closing_datetime) >=:fromDate', array(':fromDate'=>date('Y-m-01', strtotime("-12 months")))) //$dateRange['from_date']
            ->andWhere('DATE(c.actual_closing_datetime) <=:toDate', array(':toDate'=>date('Y-m-t'))) //$dateRange['to_date']
            ->andWhere('c.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID.' AND c.is_deleted=0')
            ->andWhere('t.account_id=:accountId', array(':accountId'=>Yii::app()->user->accountId))
            ->group('DATE_FORMAT(c.actual_closing_datetime,"%Y%m"), t.component_type_id')
            ->order('DATE_FORMAT(c.actual_closing_datetime,"%Y%m") ASC')
            ->queryAll();

        $ClosedGraphLabelData = array();
        $graphData['ClosedGraphLabel'] = '';
        $ClosedGraphBuyerUnitByTypeData = array(2 => array(), 3 => array());
        $graphData['ClosedGraphUnitDataMax'] = 0;
        $ClosedGraphUnitData = array();

        foreach($results as $key => $row) {

            // gather all the total units per month then pick max
            $ClosedGraphUnitData[$row['date']] += $row['closings'];

//            if(!in_array($row['date'], $ClosedGraphLabelData)) {
//                $ClosedGraphLabelData[] = $row['date'];
//            }
            $ClosedGraphBuyerUnitByTypeData[$row['component_type_id']][$row['year'].$row['month']] = $row['closings'];
        }

        //@todo: need to handle if result has very little data, likely for new onboarding clients
        if(count($results) < 13) {

            // figure out what months they do have and fill in the rest with zeros
            /**
             * DO SOMETHING!!!!!
             */

        }

        // build x axis label data
        for($i=0; $i<13; $i++) {

            $key = date('Ym', strtotime(date("Y-m-01")." -$i months"));
            $ClosedGraphLabelData[$key] = date('M', strtotime(date("Y-m-01")." -$i months"));

            // if $ClosedGraphBuyerUnitByTypeData has missing data, fill with zero
            if(!isset($ClosedGraphBuyerUnitByTypeData[2][$key])) {
                $ClosedGraphBuyerUnitByTypeData[2][$key] = 0;
            }

            if(!isset($ClosedGraphBuyerUnitByTypeData[3][$key])) {
                $ClosedGraphBuyerUnitByTypeData[3][$key] = 0;
            }
        }
        ksort($ClosedGraphLabelData);
        ksort($ClosedGraphBuyerUnitByTypeData[2]);
        ksort($ClosedGraphBuyerUnitByTypeData[3]);

        if(!empty($ClosedGraphUnitData)) {
            $graphData['ClosedGraphUnitDataMax'] = max($ClosedGraphUnitData);
            $graphData['ClosedGraphLabel'] = StmFunctions::formatChartDataToJson($ClosedGraphLabelData);
            $graphData['ClosedGraphSellerUnitData'] = isset($ClosedGraphBuyerUnitByTypeData[3]) ? StmFunctions::formatChartDataToJson($ClosedGraphBuyerUnitByTypeData[3]) : null;
            $graphData['ClosedGraphBuyerUnitData'] = isset($ClosedGraphBuyerUnitByTypeData[2]) ? StmFunctions::formatChartDataToJson($ClosedGraphBuyerUnitByTypeData[2]) : null;
        }
        else {
            $graphData['ClosedGraphLabel'] = '[]';
            $graphData['ClosedGraphSellerUnitData'] = '[]';
            $graphData['ClosedGraphBuyerUnitData'] = '[]';
        }

        /**
         * Get year over year closing data
         */
        $ClosedAnnualUnitData = array();
        $ClosedAnnualVolumeData = array();
        $results = Yii::app()->db->createCommand()
            ->select(   'DATE_FORMAT(c.actual_closing_datetime,"%b %y") date, DATE_FORMAT(c.actual_closing_datetime,"%Y") year, DATE_FORMAT(c.actual_closing_datetime,"%c")month,
                        COUNT(c.id) closings, ROUND(SUM(c.price)) volume, ROUND(SUM(c.commission_amount)) gci')
            ->from('closings c')
            ->join('transactions t','c.transaction_id = t.id')
            ->where('DATE(c.actual_closing_datetime) >=:fromDate', array(':fromDate'=>date('Y-01-01', strtotime("-24 months"))))
            ->andWhere('DATE(c.actual_closing_datetime) <=:toDate', array(':toDate'=>date('Y-m-t'))) //array(':toDate'=>date('Y-m-01', strtotime("-1 days"))))
            ->andWhere('c.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID.' AND c.is_deleted=0')
            ->andWhere('t.account_id=:accountId', array(':accountId'=>Yii::app()->user->accountId))
            ->group('DATE_FORMAT(c.actual_closing_datetime,"%Y%m")')
            ->order('DATE_FORMAT(c.actual_closing_datetime,"%Y%m") ASC')
            ->queryAll();

        foreach($results as $row) {
            $ClosedAnnualUnitData[$row['year']][$row['month']] = $row['closings'];
            $ClosedAnnualVolumeData[$row['year']][$row['month']] = $row['volume'];
        }

        // sanitize data, add zeros for missing months, then sort order it
        foreach($ClosedAnnualUnitData as $year => $annualData) {

            for($i=1; $i<=12; $i++) {
                // if not set, set it with zero and after this month
                if(!isset($ClosedAnnualUnitData[$year][$i])) {
                    $ClosedAnnualUnitData[$year][$i] = (($year<=date('Y') && $i <= date('m'))) ? 0 : '';
                }

                // if not set, set it with zero and after this month
                if(!isset($ClosedAnnualVolumeData[$year][$i])) {
                    $ClosedAnnualVolumeData[$year][$i] = (($year<=date('Y') && $i <= date('m'))) ? 0 : '';
                }
            }
        }

        // json-ify the annual data
        foreach($ClosedAnnualUnitData as $year => $annualData) {

            ksort($ClosedAnnualUnitData[$year]);
            $ClosedAnnualUnitData[$year] = StmFunctions::formatChartDataToJson($ClosedAnnualUnitData[$year], false);
        }

        foreach($ClosedAnnualVolumeData as $year => $annualData) {

            ksort($ClosedAnnualVolumeData[$year]);
            $ClosedAnnualVolumeData[$year] = StmFunctions::formatChartDataToJson($ClosedAnnualVolumeData[$year], false);
        }

        /**
         * get pending data for units & volume
         */
        $results = Yii::app()->db->createCommand()
            ->select('DATE_FORMAT(c.contract_closing_date,"%b %y") date, DATE_FORMAT(c.contract_closing_date,"%Y") year, DATE_FORMAT(c.contract_closing_date,"%c")month,
                        COUNT(c.id) closings, ROUND(SUM(c.price)) volume, ROUND(SUM(c.commission_amount)) gci')
            ->from('closings c')
            ->join('transactions t','c.transaction_id = t.id')
            ->where('c.closing_status_ma='.Closings::STATUS_TYPE_ACTIVE_ID.' AND c.is_deleted=0')
            //->aneWhere('DATE(c.actual_closing_datetime) >=:toDate', array(':toDate'=>date('Y-m-01')))
            ->andWhere('t.account_id=:accountId', array(':accountId'=>Yii::app()->user->accountId))
            ->group('DATE_FORMAT(c.contract_closing_date,"%Y%m")')
            ->order('DATE_FORMAT(c.contract_closing_date,"%Y%m") ASC')
            ->queryAll();

        if(!empty($results)) {

            foreach($results as $row) {
                $PendingAnnualUnitData[$row['year']][$row['month']] = $row['closings'];
                $PendingAnnualVolumeData[$row['year']][$row['month']] = $row['volume'];
            }
            foreach($PendingAnnualUnitData as $year => $annualData) {

                for($i=1; $i<=12; $i++) {
                    // if not set, set it with zero and after this month
                    if(($year==date('Y') && $i >= date('m')) && !isset($PendingAnnualUnitData[$year][$i])) {
                        $PendingAnnualUnitData[$year][$i] = 0;
                    }

                    // if not set, set it with zero and after this month
                    if(($year==date('Y') && $i >= date('m')) && !isset($PendingAnnualVolumeData[$year][$i])) {
                        $PendingAnnualVolumeData[$year][$i] = 0;
                    }
                }
                ksort($PendingAnnualUnitData[$year]);
                ksort($PendingAnnualVolumeData[$year]);
            }
            // json-ify the annual data
            foreach($PendingAnnualUnitData as $year => $annualData) {

                $string = '';
                foreach($PendingAnnualUnitData[$year] as $month => $row) {
                    $key = $month-1;
                    $string .= ($string) ? ',' : '';
                    $string .= "[{$key}, $row]";
                }

                $PendingAnnualUnitData[$year] = '['.$string.']';
            }

            foreach($PendingAnnualVolumeData as $year => $annualData) {

                $string = '';
                foreach($PendingAnnualVolumeData[$year] as $month => $row) {
                    $key = $month-1;
                    $string .= ($string) ? ',' : '';
                    $string .= "[{$key}, $row]";
                }
                $PendingAnnualVolumeData[$year] = '['.$string.']';
            }
        }

        $thisYear = date('Y');
        $lastYear = $thisYear-1;

        // if no data put in fillers
        //@todo: make it NOT hard coded
        if(empty($PendingAnnualUnitData)) {
            //date('Y-m-01', strtotime("-12 months")
            $PendingAnnualUnitData[$thisYear] = '[]';
            $PendingAnnualUnitData[$lastYear] = '[]';
        }
        $graphData['PendingAnnualUnitDataThisYear'] = ($PendingAnnualUnitData[$thisYear]) ? $PendingAnnualUnitData[$thisYear] : '[]';
        $graphData['PendingAnnualUnitDataLastYear'] = ($PendingAnnualUnitData[$lastYear]) ? $PendingAnnualUnitData[$lastYear] : '[]';

        if(empty($PendingAnnualVolumeData)) {
            $PendingAnnualVolumeData[$thisYear] = '[]';
            $PendingAnnualVolumeData[$lastYear] = '[]';
        }
        $graphData['PendingAnnualVolumeDataThisYear'] = ($PendingAnnualVolumeData[$thisYear]) ? $PendingAnnualVolumeData[$thisYear] : '[]';
        $graphData['PendingAnnualVolumeDataLastYear'] = ($PendingAnnualVolumeData[$lastYear]) ? $PendingAnnualVolumeData[$lastYear] : '[]';

        if(empty($ClosedAnnualUnitData)) {
            $ClosedAnnualUnitData[$thisYear] = '[]';
            $ClosedAnnualUnitData[$lastYear] = '[]';
        }

        $graphData['ClosedAnnualUnitDataThisYear'] = ($ClosedAnnualUnitData[$thisYear]) ? $ClosedAnnualUnitData[$thisYear] : '[]';
        $graphData['ClosedAnnualUnitDataLastYear'] = ($ClosedAnnualUnitData[$lastYear]) ? $ClosedAnnualUnitData[$lastYear] : '[]';

        if(empty($ClosedAnnualVolumeData)) {
            $ClosedAnnualVolumeData[$thisYear] = '[]';
            $ClosedAnnualVolumeData[$lastYear] = '[]';
        }
        $graphData['ClosedAnnualVolumeDataThisYear'] = ($ClosedAnnualVolumeData[$thisYear]) ? $ClosedAnnualVolumeData[$thisYear] : '[]';
        $graphData['ClosedAnnualVolumeDataLastYear'] = ($ClosedAnnualVolumeData[$lastYear]) ? $ClosedAnnualVolumeData[$lastYear] : '[]';

        $this->controller->render('closings', array(
            'graphData' => $graphData,

            'PendingDataProvider'=>$PendingDataProvider,
            'PendingVolume'=>$PendingVolume,
            'PendingGCI' => $PendingGCI,
            'PendingAgentCommissionTotal' => $PendingAgentCommissionTotal,
            'PendingNetCommissionTotal' => $PendingNetCommissionTotal,
            'PendingInternalReferralCommissionTotal' => $PendingInternalReferralCommissionTotal,

            'AllPendingDataProvider'=>$AllPendingDataProvider,
            'AllPendingGCI' => $AllPendingGCI,
            'AllPendingVolume'=>$AllPendingVolume,
            'AllPendingAgentCommissionTotal' => $AllPendingAgentCommissionTotal,
            'AllPendingNetCommissionTotal' => $AllPendingNetCommissionTotal,
            'AllPendingInternalReferralCommissionTotal' => $AllPendingInternalReferralCommissionTotal,

            'ClosedDataProvider'=>$ClosedDataProvider,
            'ClosedGCI' => $ClosedGCI,
            'ClosedVolume'=>$ClosedVolume,
            'ClosedAgentCommissionTotal' => $ClosedAgentCommissionTotal,
            'ClosedNetCommissionTotal' => $ClosedNetCommissionTotal,
            'ClosedInternalReferralCommissionTotal' => $ClosedInternalReferralCommissionTotal,

            'ExecutedDataProvider'=>$ExecutedDataProvider,
            'ExecutedGCI' => $ExecutedGCI,
            'ExecutedVolume'=>$ExecutedVolume,



            'FalloutDataProvider'=>$FalloutDataProvider,
            'FalloutGCI' => $FalloutGCI,
            'FalloutVolume'=>$FalloutVolume,
            'dateRange'=>$dateRange,
            'DateRangerSelect'=>$DateRanger->defaultSelect,
		));
	}
}