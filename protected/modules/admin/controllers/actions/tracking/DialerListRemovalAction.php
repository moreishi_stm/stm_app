<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class DialerListRemovalAction extends CAction
{
	public function run($id=null)
    {
		$this->controller->title = 'Dialer List Removal Report';
		$this->controller->pageColor = 'teal';

        $model = new CallListPhones();
        $model->unsetAttributes();

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'last_3_months';
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);
        $model->hardDeletedFromDate = ($dateRange['from_date']) ? $dateRange['from_date'] : null;
        $model->hardDeletedToDate = ($dateRange['to_date']) ? $dateRange['to_date'] : null;

        if(isset($_GET['CallListPhones'])) {
            $model->attributes = $_GET['CallListPhones'];
        }

		$this->controller->render('dialerListRemoval', array(
            'model' => $model,
            'dateRange'=>$dateRange,
        ));
	}

    public function printPhone($data)
    {
        if(!($phone = $data->phone)) {
            $phone = new Phones();
            $phone = $phone->skipSoftDeleteCheck()->findByPk($data->phone_id);
            $deleteFlag = '<br>**** Deleted ****';
        }
        echo Yii::app()->format->formatPhone($phone->phone).$deleteFlag;
    }

    public function printContactInfo($data)
    {
        if($contact = $data->phone->contact) {
            echo '<a href="/admin/contacts/'.$contact->id.'" target="_blank">'.$contact->fullName.'</a>';
        }
        else {
            // phone may be deleted
            $phone = new Phones();
            $phone = $phone->skipSoftDeleteCheck()->findByPk($data->phone_id);
            $contact = $phone->contact;
            echo '<a href="/admin/contacts/'.$contact->id.'" target="_blank">'.$contact->fullName.'</a>';
            echo '<br>**** Phone # Deleted ****';
        }
    }

    public function printLeads($data)
    {
        $buttons = '';
        $componentTypeId = $data->callList->type->component_type_id;
        if($contact = $data->phone->contact) {
            $buttons = $contact->existingComponentRecordButton($componentTypeId);
        }
        else {
            $phone = new Phones();
            $phone = $phone->skipSoftDeleteCheck()->findByPk($data->phone_id);
            $contact = $phone->contact;
            $buttons = $contact->existingComponentRecordButton($componentTypeId);
        }
        return $buttons;
    }
}