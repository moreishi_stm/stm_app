<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class CallPerformanceTimesAction extends CAction
{
    public $agentAppointments;
    public $totalAppointments;

	public function run($id=null)
	{
		$this->controller->title = 'Call Performance by Times Report';
		$this->controller->pageColor = 'red';

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'last_3_months';
        if($_GET['DateRanger']['date_preset']){
            $DateRanger->defaultSelect = $_GET['DateRanger']['date_preset'];
        }
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $assignedToCondition = (is_numeric($_GET['contact_id'])) ?  'added_by='.$_GET['contact_id'] : '';
        $componentTypeIds = (is_numeric($_GET['component_type_id']) || $_GET['component_type_id'] == '2,3') ? (array) $_GET['component_type_id'] : explode(',','2,3,5');
        $dateRangeCondition = ($dateRange) ? 'DATE(activity_date) >= "'.$dateRange['from_date'].'" AND DATE(activity_date) <= "'.$dateRange['to_date'].'" ' : '';

        $defaultTaskTypeIds = array(TaskTypes::PHONE, TaskTypes::DIALER_ANSWER, TaskTypes::DIALER_NO_ANSWER);
        if($_GET['task_type_id']) {
            $taskTypeIds = explode(',',$_GET['task_type_id']);
            foreach($taskTypeIds as $i => $value) {
                if(!is_numeric($value)) {
                    unset($taskTypeIds[$i]);
                }
            }
        }
        $taskTypeIds = (empty($taskTypeIds)) ? $defaultTaskTypeIds : $taskTypeIds;

        // this is used for percentages
//        $appointmentCount = Yii::app()->db->createCommand("select count(id) count from appointments where component_type_id IN({$componentTypeIds}) {$assignedToCondition} {$dateRangeCondition}")->queryScalar();
//        $results = Yii::app()->db->createCommand("select DATE_FORMAT(set_on_datetime, '%k') hour, count(id) count, ROUND((count(id) / {$appointmentCount}) * 100) percent FROM appointments where component_type_id IN({$componentTypeIds}) {$assignedToCondition} {$dateRangeCondition} GROUP BY DATE_FORMAT(set_on_datetime, '%H') ORDER BY DATE_FORMAT(set_on_datetime, '%H') ASC")->queryAll();

        $results['dials'] = Yii::app()->db->createCommand()
                            ->select("DATE_FORMAT(activity_date, '%k') hour, count(distinct component_type_id,component_id) value")
                            ->from("activity_log")
                            ->where(array("IN", "component_type_id", $componentTypeIds))
                            ->andWhere(array("IN", "task_type_id", $taskTypeIds))
                            ->andWhere("$assignedToCondition")
                            ->andWhere("$dateRangeCondition")
                            ->andWhere("DATE_FORMAT(activity_date, '%H') >= '06' AND DATE_FORMAT(activity_date, '%H') <= '23'")
                            ->group("DATE_FORMAT(activity_date, '%H')")
                            ->order("DATE_FORMAT(activity_date, '%H') ASC")
                            ->queryAll();

        $results['spokeTos'] = Yii::app()->db->createCommand()
                            ->select("DATE_FORMAT(activity_date, '%k') hour, count(distinct component_type_id,component_id) value")
                            ->from("activity_log")
                            ->where(array("IN", "component_type_id", $componentTypeIds))
                            ->andWhere(array("IN", "task_type_id", $taskTypeIds))
                            ->andWhere("$assignedToCondition")
                            ->andWhere("$dateRangeCondition")
                            ->andWhere("is_spoke_to=1")
                            ->andWhere("DATE_FORMAT(activity_date, '%H') >= '06' AND DATE_FORMAT(activity_date, '%H') <= '23'")
                            ->group("DATE_FORMAT(activity_date, '%H')")
                            ->order("DATE_FORMAT(activity_date, '%H') ASC")
                            ->queryAll();

        $results['appointments'] = Yii::app()->db->createCommand()
                            ->select("DATE_FORMAT(set_on_datetime, '%k') hour, count(distinct component_type_id,component_id) value")
                            ->from("appointments")
                            ->where(array("IN", "component_type_id", $componentTypeIds))
                            ->andWhere(($_GET['contact_id']) ? "set_by_id=".$_GET['contact_id'] : '')
                            ->andWhere("DATE(set_on_datetime) >= '".$dateRange['from_date']."' AND DATE(set_on_datetime) <= '".$dateRange['to_date']."'")
                            ->andWhere("is_deleted=0")
                            ->andWhere("DATE_FORMAT(set_on_datetime, '%H') >= '06' AND DATE_FORMAT(set_on_datetime, '%H') <= '23'")
                            ->group("DATE_FORMAT(set_on_datetime, '%H')")
                            ->order("DATE_FORMAT(set_on_datetime, '%H') ASC")
                            ->queryAll();

        $dataValues = array();
        $dataJson = array();
        $maxValues = array();
        if(!empty($results)) {
            foreach($results as $type => $result) {
                $dataValues[$type] = StmFunctions::to1DArray($result, 'hour', 'value');
                $dataValues[$type] = StmFunctions::arrayFiller($dataValues[$type], 6, 23, 0);
                $dataJson[$type] = StmFunctions::formatChartDataToJson($dataValues[$type], false);
                $maxValues[$type] = (max($dataValues[$type])) ? max($dataValues[$type]) * 1.1 : '100';
            }
        }

        //calculate populate array for spoke tos per hour
        $dataValues['spokeToPercent'] = StmFunctions::array2Percent($dataValues['spokeTos'], $dataValues['dials'], true, 100, 10);
        $dataJson['spokeToPercent'] = StmFunctions::formatChartDataToJson($dataValues['spokeToPercent'], false);
        $maxValues['spokeToPercent'] = (max($dataValues['spokeToPercent'])) ? max($dataValues['spokeToPercent']) + 1 : '100';

        $dataValues['appointmentPercent'] = StmFunctions::array2Percent($dataValues['appointments'], $dataValues['spokeTos'], true, 100, 2);
        $dataJson['appointmentPercent'] = StmFunctions::formatChartDataToJson($dataValues['appointmentPercent'], false);
        $maxValues['appointmentPercent'] = (max($dataValues['appointmentPercent'])) ? max($dataValues['appointmentPercent']) + 1 : '100';

        $this->controller->render('callPerformanceTimes', array(

            'spokeToPercentDataString' => $dataJson['spokeToPercent'],
            'spokeToPercentMax' => $maxValues['spokeToPercent'],

            'dialsDataString' => $dataJson['dials'],
            'dialsMax' => $maxValues['dials'],

            'spokeTosDataString' => $dataJson['spokeTos'],
            'spokeTosMax' => $maxValues['spokeTos'],

            'appointmentsDataString' => $dataJson['appointments'],
            'appointmentsMax' => $maxValues['appointments'],

            'appointmentsPercentDataString' => $dataJson['appointmentPercent'],
            'appointmentsPercentMax' => $maxValues['appointmentPercent'],

            'dateRangerSelect' =>$DateRanger->defaultSelect,
            'dateRange' =>$dateRange,
		));
	}
}