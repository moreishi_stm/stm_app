<?php Yii::import('admin_widgets.DateRanger.DateRanger');

class NoActivityAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id) {
		$this->controller->title = 'No Activity';

        $model = new Contacts;
        $Criteria = $model->byActiveAdmins(true)->orderByName()->getDbCriteria();

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'this_month';
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        if($_GET) {
            if(!empty($_GET['assignment_type_id']) && !empty($_GET['assignment_type_id'][0]))  {
                $AssignmentTypeCriteria = $model->byAssignmentTypeSetting($_GET['assignment_type_id'])->getDbCriteria();
                $Criteria->mergeWith($AssignmentTypeCriteria);
            }
            if(!empty($_GET['contact_id']) && !empty($_GET['contact_id'][0]))  {
                $ContactCriteria = $model->getDbCriteria()->addInCondition('t.id',(array) $_GET['contact_id']);
                $Criteria->mergeWith($ContactCriteria);
            }
        }

        $DataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>50)));

        $this->controller->render('noActivity', array(
                'model' => $model,
                'DataProvider' => $DataProvider,
            )
        );
	}
}
