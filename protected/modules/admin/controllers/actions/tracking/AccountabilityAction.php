<?php Yii::import('admin_widgets.DateRanger.DateRanger');

class AccountabilityAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {
        $this->controller->pageColor = 'teal';
		$this->controller->title = '1st Responder Ranking';

        $model = new Contacts;
        $Criteria = $model->byActiveAdmins(true)->orderByName()->getDbCriteria();

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'this_month';
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        if($_GET) {
            if(!empty($_GET['assignment_type_id']) && !empty($_GET['assignment_type_id'][0]))  {
                $AssignmentTypeCriteria = $model->byAssignmentTypeSetting($_GET['assignment_type_id'])->getDbCriteria();
                $Criteria->mergeWith($AssignmentTypeCriteria);
            }
            if(!empty($_GET['contact_id']) && !empty($_GET['contact_id'][0]))  {
                $ContactCriteria = $model->getDbCriteria()->addInCondition('t.id',(array) $_GET['contact_id']);
                $Criteria->mergeWith($ContactCriteria);
            }
        }

        $DataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>50)));

        $this->controller->render('accountability', array(
                'model' => $model,
                'DataProvider' => $DataProvider,
                'dateRange'=>$dateRange,
            )
        );
	}

    public function printCountAndPercent($count, $total) {
        $percent = ($total >0) ? $count / $total : 0;
        return $count.' ('.Yii::app()->format->formatPercentages($percent).')';
    }
}
