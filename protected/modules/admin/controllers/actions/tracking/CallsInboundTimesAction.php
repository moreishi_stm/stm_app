<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class CallsInboundTimesAction extends CAction
{
    public $agentAppointments;
    public $totalAppointments;

	public function run($id=null)
	{
		$this->controller->title = 'Call Trends (Inbound) by Time Report';
		$this->controller->pageColor = 'red';

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'last_3_months';
        if($_GET['DateRanger']['date_preset']){
            $DateRanger->defaultSelect = $_GET['DateRanger']['date_preset'];
        }
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId));
        $phoneResult = Yii::app()->stm_hq->createCommand("SELECT id, phone, user_description FROM telephony_phones WHERE client_account_id='".$clientAccount->id."' ORDER BY user_description")->queryAll();
        $phoneListData = array();
        foreach($phoneResult as $phoneRow) {
            $phoneListData[$phoneRow['phone']] = $phoneRow['user_description'].' - '. Yii::app()->format->formatPhone($phoneRow['phone']);
        }

        $phoneNumbersCondition = (is_numeric($_GET['telephony_phone_id'])) ? 'to_number="1'.$_GET['telephony_phone_id'].'"' : '';
        $dateRangeCondition = ($dateRange) ? 'DATE(c.added) >= "'.$dateRange['from_date'].'" AND DATE(c.added) <= "'.$dateRange['to_date'].'" ' : '';

        // this is used for percentages
        $inboundCallsCommand = Yii::app()->db->createCommand()
                            ->select("DATE_FORMAT(c.added, '%k') hour, count(distinct c.id) value")
                            ->from("call_hunt_group_sessions c")
                            ->andWhere("$phoneNumbersCondition")
                            ->andWhere("exclude_from_stats=0")
                            ->andWhere("$dateRangeCondition")
                            ->andWhere("DATE_FORMAT(c.added, '%H') >= '06' AND DATE_FORMAT(c.added, '%H') <= '23'")
                            ->group("DATE_FORMAT(c.added, '%H')")
                            ->order("DATE_FORMAT(c.added, '%H') ASC");
        if(isset($_GET['is_priority']) && !empty($_GET['is_priority'])) {
            $inboundCallsCommand->join('stm_hq.telephony_phones','CONCAT("1",phone)=to_number')
                                ->andWhere('is_priority=1');
        }

        $results['inboundCalls']  = $inboundCallsCommand->queryAll();

        $dataValues = array();
        $dataJson = array();
        $maxValues = array();
        if(!empty($results)) {
            foreach($results as $type => $result) {
                $dataValues[$type] = StmFunctions::to1DArray($result, 'hour', 'value');
                $dataValues[$type] = StmFunctions::arrayFiller($dataValues[$type], 6, 23, 0);
                $dataJson[$type] = StmFunctions::formatChartDataToJson($dataValues[$type], false);
                $maxValues[$type] = (max($dataValues[$type])) ? max($dataValues[$type]) * 1.05 : '100';
            }
        }

        $this->controller->render('callsInboundTimes', array(

            'inboundCallsDataString' => $dataJson['inboundCalls'],
            'inboundCallsMax' => $maxValues['inboundCalls'],

            'dateRangerSelect' =>$DateRanger->defaultSelect,
            'dateRange' =>$dateRange,
            'phoneListData' => $phoneListData,
		));
	}
}