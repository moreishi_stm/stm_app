<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class EmailClicksAction extends CAction
{
	public function run($id=null) {

		$this->controller->title = 'Email Click Tracking';
		$this->controller->pageColor = 'teal';

//        if(Yii::app()->user->clientId == 1 && !Yii::app()->user->checkAccess('owner')) {
//            echo 'This report has been disabled. Please contact your administrator.';
//            Yii::app()->end();
//        }

        $DateRanger = new DateRanger();
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

		$model = new EmailClickTracking();
        $model->unsetAttributes();
        $model->fromDate = $dateRange['from_date'];
        $model->toDate = $dateRange['to_date'];

        if($_GET['EmailClickTracking']) {
            $model->attributes = $_GET['EmailClickTracking'];
        }

		$this->controller->render('emailClicks', array(
            'model' => $model,
            'dateRange'=>$dateRange,
        ));
	}
}
