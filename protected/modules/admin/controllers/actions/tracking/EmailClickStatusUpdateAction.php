<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class EmailClickStatusUpdateAction extends CAction
{
	public function run($id)
    {
        if (Yii::app()->request->isAjaxRequest) {

            if($model = EmailClickTracking::model()->findByPk($id)) {

                switch($_POST['action']) {

                    case 'complete';
                        $model->status_ma = EmailClickTracking::STATUS_FOLLOWED_UP_ID;
                        break;

                    case 'incomplete';
                        $model->status_ma = EmailClickTracking::STATUS_NO_FOLLOW_UP_ID;
                        break;
                }
            }

            $model->updated = new CDbExpression('NOW()');
            $model->updated_by = Yii::app()->user->id;
            if($model->save()) {
                echo CJSON::encode(array('status'=> 'success'));
            }
            else {
                echo CJSON::encode(array('status'=> 'error', 'message'=>current($model->getErrors()[0])));
            }
        }

        Yii::app()->end();
	}
}
