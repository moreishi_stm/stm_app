<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class TasksAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Tasks Reporting';
		$this->controller->pageColor = 'teal';

		$model = new Contacts;
		$Criteria = $model->byActiveAdmins(true)->getDbCriteria();
		$Criteria->order = 'first_name ASC';

		if($_GET) {
			if(!empty($_GET['assignment_type_id']) && !empty($_GET['assignment_type_id'][0]))  {
				$AssignmentTypeCriteria = $model->byAssignmentTypeSetting($_GET['assignment_type_id'])->getDbCriteria();
				$Criteria->mergeWith($AssignmentTypeCriteria);
			}
		}

		$DateRanger = new DateRanger();
		$DateRanger->defaultSelect = 'month_to_date';
		$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

		$DataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>100)));

		$this->controller->render('tasks', array(
			 'DataProvider'=>$DataProvider,
			 'dateRange'=>$dateRange,
		));
	}
}