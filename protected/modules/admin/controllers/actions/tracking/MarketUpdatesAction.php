<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class MarketUpdatesAction extends CAction {

	public function run($id=null) {
//		if (Yii::app()->request->isAjaxRequest)
//			$this->processAjaxRequest($model);

		$this->controller->title = 'Market Updates Report';
		$this->controller->pageColor = 'teal';

//		$model = new Contacts;
//		$Criteria = $model->byAdmins(true)->getDbCriteria();
//		$Criteria->order = 'first_name ASC';
//
//		if($_GET) {
//			if(!empty($_GET['assignment_type_id']) && !empty($_GET['assignment_type_id'][0]))  {
//				$AssignmentTypeCriteria = $model->byAssignmentTypeSetting($_GET['assignment_type_id'])->getDbCriteria();
//				$Criteria->mergeWith($AssignmentTypeCriteria);
//			}
//		}

		$DateRanger = new DateRanger();
		$dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

//		$model = new EmailMessage;
//		$data['countSent'] = $model->byComponentTypeId(ComponentTypes::SAVED_SEARCHES)->byDateRange($dateRange)->count();
//		$model = new EmailMessage;
//		$data['countSentUnique'] = $model->byComponentTypeId(ComponentTypes::SAVED_SEARCHES)->groupByToEmail()->byDateRange($dateRange)->count();

        $dateRangeQuery = $this->dateRangeQuery('processed_datetime', $dateRange);
        $query = 'select count(*) as count from email_messages where component_type_id='.ComponentTypes::SAVED_SEARCHES;
        $data['countSent'] = Yii::app()->db->createCommand($query.$dateRangeQuery)->queryScalar();

        $query = 'select count(distinct(to_email_id)) as count from email_messages where component_type_id='.ComponentTypes::SAVED_SEARCHES;
        $data['countSentUnique'] = Yii::app()->db->createCommand($query.$dateRangeQuery)->queryScalar();


        $model = new EmailClickTracking;
		$data['countClicks'] = $model->byEmailMessageComponentTypeId(ComponentTypes::SAVED_SEARCHES)->byDateRange($dateRange)->count();
        $model = new EmailClickTracking;
		$data['countClicksUnique'] = $model->byEmailMessageComponentTypeId(ComponentTypes::SAVED_SEARCHES)->byUniqueContacts()->byDateRange($dateRange)->count();

        $clickDateRangeQuery = $this->dateRangeQuery('added', $dateRange);
        $clickQuery = 'select count(*) as count from email_click_tracking a LEFT JOIN email_messages b ON a.email_message_id = b.id where b.component_type_id='.ComponentTypes::SAVED_SEARCHES;
        $data['countClicks'] = Yii::app()->db->createCommand($clickQuery.$clickDateRangeQuery)->queryScalar();

        $clickQuery = 'select count(distinct(contact_id)) as count from email_click_tracking a LEFT JOIN email_messages b ON a.email_message_id = b.id where b.component_type_id='.ComponentTypes::SAVED_SEARCHES;
        $data['countClicksUnique'] = Yii::app()->db->createCommand($clickQuery.$clickDateRangeQuery)->queryScalar();

//		$PendingCriteria = $model->byClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_ACTIVE_ID))->getDbCriteria();
//		$PendingCriteria->order = 'contract_execute_date DESC';
//		$PendingDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$PendingCriteria,'pagination'=>array('pageSize'=>100))); //@todo: remove pagination
//		$PendingVolume = Closings::getVolumeTotal($PendingCriteria);






        $emailMessage = new EmailMessage;
        $emailMessage->fromDate = $dateRange['from_date'];
        $emailMessage->toDate = $dateRange['to_date'];

        $emailMessage->component_type_id = ComponentTypes::SAVED_SEARCHES;
        $DataProvider = $emailMessage->search();
        $DataProvider->sort = array('defaultOrder'=>'processed_datetime DESC');
        $DataProvider->pagination = array('pageSize'=>50);

		$this->controller->render('marketUpdates', array(
										   'dataProvider'=>$DataProvider,
										   'defaultSelect'=>($_POST['DateRanger']['date_preset'])? $_POST['DateRanger']['date_preset'] : 'this_month',
										   'dateRange'=>$dateRange,
										   'data'=>$data,
										   ));
	}

	protected function processAjaxRequest(Contacts $model) {
		if (Yii::app()->request->isAjaxRequest) {

//			echo $this->controller->renderPartial('_conversion', array(), true);
//			Yii::app()->end();
		}
	}

    protected function dateRangeQuery($field, $dateRange) {
        $query = '';
        if ($dateRange['from_date']) {
            $query .= ' AND '.$field.'>="'.$dateRange['from_date'].'"';
        }

        if ($dateRange['to_date']) {
            $query .= ' AND '.$field.'<="'.$dateRange['to_date'].' 23:59:59"';
        }

        return $query;
    }
}
