<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class ClosingsExecutiveSummaryAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Closings Executive Summary Report';
		$this->controller->pageColor = 'teal';

		$DateRanger = new DateRanger();
		if($_POST['DateRanger']['date_preset'])
			$DateRanger->defaultSelect = $_POST['DateRanger']['date_preset'];
		$dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

        $ClosingsSummaryData = array();
        $ClosingsSummaryQuarterAnnualTotals = array();
        $agentCommissionSubquery = '(SELECT SUM(at.amount) FROM accounting_transactions at JOIN closings c2 ON at.closing_id=c2.id JOIN transactions t2 ON t2.id=c2.transaction_id WHERE accounting_account_id IN ('.AccountingAccounts::SALES_COMMISSION.') AND MONTH(at.date)=MONTH(c.actual_closing_datetime) AND YEAR(at.date)='.date('Y').' AND c.closing_status_ma=c2.closing_status_ma AND t2.component_type_id=t.component_type_id)';

        $internalReferralCommissionSubquery = '(SELECT SUM(at.amount) FROM accounting_transactions at JOIN closings c2 ON at.closing_id=c2.id JOIN transactions t2 ON t2.id=c2.transaction_id WHERE accounting_account_id IN ('.AccountingAccounts::REFERRAL_FEE_INTERNAL.') AND MONTH(at.date)=MONTH(c.actual_closing_datetime) AND YEAR(at.date)='.date('Y').' AND c.closing_status_ma=c2.closing_status_ma AND t2.component_type_id=t.component_type_id)';

        $referralInboundSubquery = '(SELECT IFNULL(SUM(r.referral_paid), 0) FROM referrals r WHERE r.referral_direction_ma='.Referrals::REFERRAL_DIRECTION_IN.' AND MONTH(c.actual_closing_datetime)=MONTH(r.closed_date) AND YEAR(c.actual_closing_datetime)=YEAR(r.closed_date) AND t.component_type_id=r.transaction_type_id)';

        $referralOutboundSubquery = '(SELECT IFNULL(SUM(r2.referral_paid), 0) FROM referrals r2 WHERE r2.referral_direction_ma IN('.Referrals::REFERRAL_DIRECTION_INTERNAL.','.Referrals::REFERRAL_DIRECTION_OUT.') AND MONTH(c.actual_closing_datetime)=MONTH(r2.closed_date) AND YEAR(c.actual_closing_datetime)=YEAR(r2.closed_date) AND t.component_type_id=r2.transaction_type_id)';

//        $referralCommissionSubquery = '(SELECT SUM(at2.amount)*-1 FROM accounting_transactions at2 JOIN closings c3 ON at2.closing_id=c3.id JOIN transactions t3 ON t3.id=c3.transaction_id WHERE accounting_account_id IN ('.AccountingAccounts::REFERRAL_FEE.') AND MONTH(at2.date)=MONTH(c.actual_closing_datetime) AND YEAR(at2.date)='.date('Y').' AND t3.component_type_id=t.component_type_id)';

        $referralCommissionSubquery = "({$referralInboundSubquery} - {$referralOutboundSubquery})";

        if($closingSummaryResults = Yii::app()->db->createCommand()
            ->select("t.component_type_id, count(c.id) units, SUM(c.price) volume, ROUND(AVG(c.price)) averagePrice, SUM(c.commission_amount) grossCommission, MONTH(c.actual_closing_datetime) month, QUARTER(c.actual_closing_datetime) quarter, {$agentCommissionSubquery} agentCommission, {$internalReferralCommissionSubquery} internalReferralCommission, {$referralCommissionSubquery} referralCommission") //, {$referralInboundSubquery} referralInboundCommission, {$referralOutboundSubquery} referralOutboundCommission
            ->from("closings c")
            ->join("transactions t", "t.id=c.transaction_id")
            ->join("component_types ct", "ct.id=t.component_type_id")
            ->where("DATE(c.actual_closing_datetime) BETWEEN '".date('Y-01-01')."' AND '".date('Y-12-31')."'") //@todo: dynamic later
            ->andWhere("c.closing_status_ma=".Closings::STATUS_TYPE_CLOSED_ID)
            ->group("MONTH(c.actual_closing_datetime), t.component_type_id")
            ->order("MONTH(c.actual_closing_datetime) ASC, t.component_type_id DESC")
            ->queryAll()) {

//@todo: referralCommission, totalCommission


            foreach($closingSummaryResults as $closingSummaryRow) {
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['units'] = $closingSummaryRow['units'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['volume'] = $closingSummaryRow['volume'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['averagePrice'] = $closingSummaryRow['averagePrice'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['grossCommission'] = $closingSummaryRow['grossCommission'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['agentCommission'] = $closingSummaryRow['agentCommission'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['internalReferralCommission'] = $closingSummaryRow['internalReferralCommission'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['netCommission'] = $closingSummaryRow['grossCommission'] - $closingSummaryRow['agentCommission'] - $closingSummaryRow['internalReferralCommission'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['avgCommission'] = $closingSummaryRow['grossCommission'] / $closingSummaryRow['units'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['referralCommission'] = $closingSummaryRow['referralCommission'];
                $ClosingsSummaryData[$closingSummaryRow['month']][$closingSummaryRow['component_type_id']]['totalCommission'] = $closingSummaryRow['grossCommission'] - $closingSummaryRow['agentCommission'] + $closingSummaryRow['referralCommission'];

                // populate quarterly totals
                $ClosingsSummaryQuarterAnnualTotals['q'.$closingSummaryRow['quarter']]['units'] += $closingSummaryRow['units'];
                $ClosingsSummaryQuarterAnnualTotals['q'.$closingSummaryRow['quarter']]['volume'] += $closingSummaryRow['volume'];
                $ClosingsSummaryQuarterAnnualTotals['q'.$closingSummaryRow['quarter']]['grossCommission'] += $closingSummaryRow['grossCommission'];
                $ClosingsSummaryQuarterAnnualTotals['q'.$closingSummaryRow['quarter']]['agentCommission'] += $closingSummaryRow['agentCommission'];
                $ClosingsSummaryQuarterAnnualTotals['q'.$closingSummaryRow['quarter']]['referralCommission'] += $closingSummaryRow['referralCommission'];
                $ClosingsSummaryQuarterAnnualTotals['q'.$closingSummaryRow['quarter']]['internalReferralCommission'] += $closingSummaryRow['internalReferralCommission'];
            }
            // populate quarterly sum/totals from previous population
            for($q=1; $q <=4; $q++) {
                $ClosingsSummaryQuarterAnnualTotals['q'.$q]['averagePrice'] = ($ClosingsSummaryQuarterAnnualTotals['q'.$q]['units']) ? $ClosingsSummaryQuarterAnnualTotals['q'.$q]['volume'] / $ClosingsSummaryQuarterAnnualTotals['q'.$q]['units'] : '-';
                $ClosingsSummaryQuarterAnnualTotals['q'.$q]['netCommission'] = $ClosingsSummaryQuarterAnnualTotals['q'.$q]['grossCommission'] - $ClosingsSummaryQuarterAnnualTotals['q'.$q]['agentCommission']  - $ClosingsSummaryQuarterAnnualTotals['q'.$q]['internalReferralCommission'];
                $ClosingsSummaryQuarterAnnualTotals['q'.$q]['avgCommission'] = ($ClosingsSummaryQuarterAnnualTotals['q'.$q]['units']) ? $ClosingsSummaryQuarterAnnualTotals['q'.$q]['grossCommission'] / $ClosingsSummaryQuarterAnnualTotals['q'.$q]['units'] : '-';
                $ClosingsSummaryQuarterAnnualTotals['q'.$q]['totalCommission'] = ($ClosingsSummaryQuarterAnnualTotals['q'.$q]['units']) ? $ClosingsSummaryQuarterAnnualTotals['q'.$q]['grossCommission'] - $ClosingsSummaryQuarterAnnualTotals['q'.$q]['agentCommission'] - $ClosingsSummaryQuarterAnnualTotals['q'.$q]['internalReferralCommission'] + $ClosingsSummaryQuarterAnnualTotals['q'.$q]['referralCommission'] : '-';
            }

            // populate totals
            for($i=1; $i <=12; $i++) {
                $row = $ClosingsSummaryData[$i];
                // populate totals
                $ClosingsSummaryData[$i]['total']['units'] = ($row) ? $row[ComponentTypes::SELLERS]['units'] + $row[ComponentTypes::BUYERS]['units'] : "";
                $ClosingsSummaryData[$i]['total']['volume'] = ($row) ? $row[ComponentTypes::SELLERS]['volume'] + $row[ComponentTypes::BUYERS]['volume'] : "";
                $ClosingsSummaryData[$i]['total']['averagePrice'] = ($row) ? $ClosingsSummaryData[$i]['total']['volume'] / $ClosingsSummaryData[$i]['total']['units'] : "";
                $ClosingsSummaryData[$i]['total']['grossCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['grossCommission'] + $row[ComponentTypes::BUYERS]['grossCommission'] : "";
                $ClosingsSummaryData[$i]['total']['agentCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['agentCommission'] + $row[ComponentTypes::BUYERS]['agentCommission'] : "";
                $ClosingsSummaryData[$i]['total']['internalReferralCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['internalReferralCommission'] + $row[ComponentTypes::BUYERS]['internalReferralCommission'] : "";
                $ClosingsSummaryData[$i]['total']['netCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['netCommission'] + $row[ComponentTypes::BUYERS]['netCommission'] : "";
                $ClosingsSummaryData[$i]['total']['avgCommission'] = ($row) ? $ClosingsSummaryData[$i]['total']['grossCommission'] / $ClosingsSummaryData[$i]['total']['units'] : "";

                // populate annual totals
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units'] += $row[ComponentTypes::SELLERS]['units'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'] += $row[ComponentTypes::SELLERS]['volume'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'] += $row[ComponentTypes::SELLERS]['grossCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['agentCommission'] += $row[ComponentTypes::SELLERS]['agentCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['internalReferralCommission'] += $row[ComponentTypes::SELLERS]['internalReferralCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['netCommission'] += $row[ComponentTypes::SELLERS]['netCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['referralCommission'] += $row[ComponentTypes::SELLERS]['referralCommission'];

                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units'] += $row[ComponentTypes::BUYERS]['units'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'] += $row[ComponentTypes::BUYERS]['volume'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'] += $row[ComponentTypes::BUYERS]['grossCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['agentCommission'] += $row[ComponentTypes::BUYERS]['agentCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['internalReferralCommission'] += $row[ComponentTypes::BUYERS]['internalReferralCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['netCommission'] += $row[ComponentTypes::BUYERS]['netCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['referralCommission'] += $row[ComponentTypes::BUYERS]['referralCommission'];

                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['units'] += $row[ComponentTypes::SELLERS]['units'] + $row[ComponentTypes::BUYERS]['units'];
                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['volume'] += $row[ComponentTypes::SELLERS]['volume'] + $row[ComponentTypes::BUYERS]['volume'];
                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] += $row[ComponentTypes::SELLERS]['grossCommission'] + $row[ComponentTypes::BUYERS]['grossCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['agentCommission'] += $row[ComponentTypes::SELLERS]['agentCommission'] + $row[ComponentTypes::BUYERS]['agentCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['internalReferralCommission'] += $row[ComponentTypes::SELLERS]['internalReferralCommission'] + $row[ComponentTypes::BUYERS]['internalReferralCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['netCommission'] += $row[ComponentTypes::SELLERS]['netCommission'] + $row[ComponentTypes::BUYERS]['netCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['referralCommission'] += $row[ComponentTypes::SELLERS]['referralCommission'] + $row[ComponentTypes::BUYERS]['referralCommission'];
                $ClosingsSummaryQuarterAnnualTotals['annual']['total']['totalCommission'] += $row[ComponentTypes::SELLERS]['netCommission'] + $row[ComponentTypes::BUYERS]['netCommission'] + $row[ComponentTypes::SELLERS]['referralCommission'] + $row[ComponentTypes::BUYERS]['referralCommission'];
            }

            $ClosingsSummaryQuarterAnnualTotals['annual']['total']['averagePrice'] = $ClosingsSummaryQuarterAnnualTotals['annual']['total']['volume'] / $ClosingsSummaryQuarterAnnualTotals['annual']['total']['units'];
            $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['averagePrice'] = ($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units']) ? $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'] / $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units'] : 0;
            $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['averagePrice'] = ($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units']) ? $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'] / $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units'] : 0;
            $ClosingsSummaryQuarterAnnualTotals['annual']['total']['avgCommission'] = $ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] / $ClosingsSummaryQuarterAnnualTotals['annual']['total']['units'];

            $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['avgCommission'] = ($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units']) ?$ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'] / $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units'] : 0;
            $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['avgCommission'] = ($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units']) ? $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'] / $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units'] : 0;
            $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['totalCommission'] = $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['netCommission'] + $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['referralCommission'];
            $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['totalCommission'] = $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['netCommission'] + $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['referralCommission'];

        }
        ksort($ClosingsSummaryData);

        // @todo: NOTE - This is the ALMOST same set of logic above (except for status, I think closed vs. pending), need to figure out a way to consolidate and re-use
        // populate pending data

        $PendingsSummaryData = array();
        $PendingsSummaryQuarterAnnualTotals = array();
        $pendingAgentCommissionSubquery = '(SELECT SUM(at.amount) FROM accounting_transactions at JOIN closings c2 ON at.closing_id=c2.id JOIN transactions t2 ON t2.id=c2.transaction_id WHERE accounting_account_id IN ('.AccountingAccounts::SALES_COMMISSION.') AND MONTH(at.date)=MONTH(c.contract_closing_date) AND YEAR(at.date)='.date('Y').' AND c.closing_status_ma=c2.closing_status_ma AND t2.component_type_id=t.component_type_id)';

        $pendingInternalReferralCommissionSubquery = '(SELECT SUM(at.amount) FROM accounting_transactions at JOIN closings c2 ON at.closing_id=c2.id JOIN transactions t2 ON t2.id=c2.transaction_id WHERE accounting_account_id IN ('.AccountingAccounts::REFERRAL_FEE_INTERNAL.') AND MONTH(at.date)=MONTH(c.contract_closing_date) AND YEAR(at.date)='.date('Y').' AND c.closing_status_ma=c2.closing_status_ma AND t2.component_type_id=t.component_type_id)';

        $pendingReferralInboundSubquery = '(SELECT IFNULL(SUM(r.referral_paid), 0) FROM referrals r WHERE r.referral_direction_ma='.Referrals::REFERRAL_DIRECTION_IN.' AND MONTH(c.contract_closing_date)=MONTH(r.closed_date) AND YEAR(c.contract_closing_date)=YEAR(r.closed_date) AND t.component_type_id=r.transaction_type_id)';

        $pendingReferralOutboundSubquery = '(SELECT IFNULL(SUM(r2.referral_paid), 0) FROM referrals r2 WHERE r2.referral_direction_ma IN('.Referrals::REFERRAL_DIRECTION_INTERNAL.','.Referrals::REFERRAL_DIRECTION_OUT.') AND MONTH(c.contract_closing_date)=MONTH(r2.closed_date) AND YEAR(c.contract_closing_date)=YEAR(r2.closed_date) AND t.component_type_id=r2.transaction_type_id)';

        $pendingReferralCommissionSubquery = "({$pendingReferralInboundSubquery} - {$pendingReferralOutboundSubquery})";

        if($pendingSummaryResults = Yii::app()->db->createCommand()
            ->select("t.component_type_id, count(c.id) units, SUM(c.price) volume, ROUND(AVG(c.price)) averagePrice, SUM(c.commission_amount) grossCommission, MONTH(c.contract_closing_date) month, QUARTER(c.contract_closing_date) quarter, {$pendingAgentCommissionSubquery} agentCommission, {$pendingInternalReferralCommissionSubquery} internalReferralCommission, {$pendingReferralCommissionSubquery} referralCommission")
            ->from("closings c")
            ->join("transactions t", "t.id=c.transaction_id")
            ->join("component_types ct", "ct.id=t.component_type_id")
            ->where("DATE(c.contract_closing_date) BETWEEN '".date('Y-01-01')."' AND '".date('Y-12-31')."'") //@todo: dynamic later
            ->andWhere("c.closing_status_ma=".Closings::STATUS_TYPE_ACTIVE_ID)
            ->group("MONTH(c.contract_closing_date), t.component_type_id")
            ->order("MONTH(c.contract_closing_date) ASC, t.component_type_id DESC")
            ->queryAll()) {

            foreach($pendingSummaryResults as $pendingSummaryRow) {
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['units'] = $pendingSummaryRow['units'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['volume'] = $pendingSummaryRow['volume'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['averagePrice'] = $pendingSummaryRow['averagePrice'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['grossCommission'] = $pendingSummaryRow['grossCommission'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['agentCommission'] = $pendingSummaryRow['agentCommission'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['internalReferralCommission'] = $pendingSummaryRow['internalReferralCommission'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['netCommission'] = $pendingSummaryRow['grossCommission'] - $pendingSummaryRow['agentCommission'] - $pendingSummaryRow['internalReferralCommission'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['avgCommission'] = $pendingSummaryRow['grossCommission'] / $pendingSummaryRow['units'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['referralCommission'] = $pendingSummaryRow['referralCommission'];
                $PendingsSummaryData[$pendingSummaryRow['month']][$pendingSummaryRow['component_type_id']]['totalCommission'] = $pendingSummaryRow['grossCommission'] - $pendingSummaryRow['agentCommission'] + $pendingSummaryRow['referralCommission'];

                // populate quarterly totals
                $PendingsSummaryQuarterAnnualTotals['q'.$pendingSummaryRow['quarter']]['units'] += $pendingSummaryRow['units'];
                $PendingsSummaryQuarterAnnualTotals['q'.$pendingSummaryRow['quarter']]['volume'] += $pendingSummaryRow['volume'];
                $PendingsSummaryQuarterAnnualTotals['q'.$pendingSummaryRow['quarter']]['grossCommission'] += $pendingSummaryRow['grossCommission'];
                $PendingsSummaryQuarterAnnualTotals['q'.$pendingSummaryRow['quarter']]['agentCommission'] += $pendingSummaryRow['agentCommission'];
                $PendingsSummaryQuarterAnnualTotals['q'.$pendingSummaryRow['quarter']]['referralCommission'] += $pendingSummaryRow['referralCommission'];
                $PendingsSummaryQuarterAnnualTotals['q'.$pendingSummaryRow['quarter']]['internalReferralCommission'] += $pendingSummaryRow['internalReferralCommission'];
            }
            // populate quarterly sum/totals from previous population
            for($q=1; $q <=4; $q++) {
                $PendingsSummaryQuarterAnnualTotals['q'.$q]['averagePrice'] = ($PendingsSummaryQuarterAnnualTotals['q'.$q]['units']) ? $PendingsSummaryQuarterAnnualTotals['q'.$q]['volume'] / $PendingsSummaryQuarterAnnualTotals['q'.$q]['units'] : '-';
                $PendingsSummaryQuarterAnnualTotals['q'.$q]['netCommission'] = $PendingsSummaryQuarterAnnualTotals['q'.$q]['grossCommission'] - $PendingsSummaryQuarterAnnualTotals['q'.$q]['agentCommission']  - $PendingsSummaryQuarterAnnualTotals['q'.$q]['internalReferralCommission'];
                $PendingsSummaryQuarterAnnualTotals['q'.$q]['avgCommission'] = ($PendingsSummaryQuarterAnnualTotals['q'.$q]['units']) ? $PendingsSummaryQuarterAnnualTotals['q'.$q]['grossCommission'] / $PendingsSummaryQuarterAnnualTotals['q'.$q]['units'] : '-';
                $PendingsSummaryQuarterAnnualTotals['q'.$q]['totalCommission'] = ($PendingsSummaryQuarterAnnualTotals['q'.$q]['units']) ? $PendingsSummaryQuarterAnnualTotals['q'.$q]['grossCommission'] - $PendingsSummaryQuarterAnnualTotals['q'.$q]['agentCommission'] - $PendingsSummaryQuarterAnnualTotals['q'.$q]['internalReferralCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.$q]['referralCommission'] : '-';
            }
            ksort($PendingsSummaryData);

            // populate totals
            for($i=1; $i <=12; $i++) {
                $row = $PendingsSummaryData[$i];
                // populate totals
                $PendingsSummaryData[$i]['total']['units'] = ($row) ? $row[ComponentTypes::SELLERS]['units'] + $row[ComponentTypes::BUYERS]['units'] : "";
                $PendingsSummaryData[$i]['total']['volume'] = ($row) ? $row[ComponentTypes::SELLERS]['volume'] + $row[ComponentTypes::BUYERS]['volume'] : "";
                $PendingsSummaryData[$i]['total']['averagePrice'] = ($row) ? $PendingsSummaryData[$i]['total']['volume'] / $PendingsSummaryData[$i]['total']['units'] : "";
                $PendingsSummaryData[$i]['total']['grossCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['grossCommission'] + $row[ComponentTypes::BUYERS]['grossCommission'] : "";
                $PendingsSummaryData[$i]['total']['agentCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['agentCommission'] + $row[ComponentTypes::BUYERS]['agentCommission'] : "";
                $PendingsSummaryData[$i]['total']['internalReferralCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['internalReferralCommission'] + $row[ComponentTypes::BUYERS]['internalReferralCommission'] : "";
                $PendingsSummaryData[$i]['total']['netCommission'] = ($row) ? $row[ComponentTypes::SELLERS]['netCommission'] + $row[ComponentTypes::BUYERS]['netCommission'] : "";
                $PendingsSummaryData[$i]['total']['avgCommission'] = ($row) ? $PendingsSummaryData[$i]['total']['grossCommission'] / $PendingsSummaryData[$i]['total']['units'] : "";

                // populate annual totals
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units'] += $row[ComponentTypes::SELLERS]['units'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'] += $row[ComponentTypes::SELLERS]['volume'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'] += $row[ComponentTypes::SELLERS]['grossCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['agentCommission'] += $row[ComponentTypes::SELLERS]['agentCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['internalReferralCommission'] += $row[ComponentTypes::SELLERS]['internalReferralCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['netCommission'] += $row[ComponentTypes::SELLERS]['netCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['referralCommission'] += $row[ComponentTypes::SELLERS]['referralCommission'];

                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units'] += $row[ComponentTypes::BUYERS]['units'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'] += $row[ComponentTypes::BUYERS]['volume'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'] += $row[ComponentTypes::BUYERS]['grossCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['agentCommission'] += $row[ComponentTypes::BUYERS]['agentCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['internalReferralCommission'] += $row[ComponentTypes::BUYERS]['internalReferralCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['netCommission'] += $row[ComponentTypes::BUYERS]['netCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['referralCommission'] += $row[ComponentTypes::BUYERS]['referralCommission'];

                $PendingsSummaryQuarterAnnualTotals['annual']['total']['units'] += $row[ComponentTypes::SELLERS]['units'] + $row[ComponentTypes::BUYERS]['units'];
                $PendingsSummaryQuarterAnnualTotals['annual']['total']['volume'] += $row[ComponentTypes::SELLERS]['volume'] + $row[ComponentTypes::BUYERS]['volume'];
                $PendingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] += $row[ComponentTypes::SELLERS]['grossCommission'] + $row[ComponentTypes::BUYERS]['grossCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual']['total']['agentCommission'] += $row[ComponentTypes::SELLERS]['agentCommission'] + $row[ComponentTypes::BUYERS]['agentCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual']['total']['internalReferralCommission'] += $row[ComponentTypes::SELLERS]['internalReferralCommission'] + $row[ComponentTypes::BUYERS]['internalReferralCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual']['total']['netCommission'] += $row[ComponentTypes::SELLERS]['netCommission'] + $row[ComponentTypes::BUYERS]['netCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual']['total']['referralCommission'] += $row[ComponentTypes::SELLERS]['referralCommission'] + $row[ComponentTypes::BUYERS]['referralCommission'];
                $PendingsSummaryQuarterAnnualTotals['annual']['total']['totalCommission'] += $row[ComponentTypes::SELLERS]['netCommission'] + $row[ComponentTypes::BUYERS]['netCommission'] + $row[ComponentTypes::SELLERS]['referralCommission'] + $row[ComponentTypes::BUYERS]['referralCommission'];
            }

            $PendingsSummaryQuarterAnnualTotals['annual']['total']['averagePrice'] = $PendingsSummaryQuarterAnnualTotals['annual']['total']['volume'] / $PendingsSummaryQuarterAnnualTotals['annual']['total']['units'];
            $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['averagePrice'] = ($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units']) ? $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'] / $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units'] : 0;
            $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['averagePrice'] = ($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units']) ? $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'] / $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units'] : 0;
            $PendingsSummaryQuarterAnnualTotals['annual']['total']['avgCommission'] = ($PendingsSummaryQuarterAnnualTotals['annual']['total']['units']) ? $PendingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] / $PendingsSummaryQuarterAnnualTotals['annual']['total']['units'] : 0;

            $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['avgCommission'] = ($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units']) ?$PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'] / $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units'] : 0;
            $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['avgCommission'] = ($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units']) ? $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'] / $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units'] : 0;
            $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['totalCommission'] = $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['netCommission'] + $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['referralCommission'];
            $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['totalCommission'] = $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['netCommission'] + $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['referralCommission'];
        }


        // fill in any missing months???

        $this->controller->render('closingsExecutiveSummary', array(

             'ClosingsSummaryData' => $ClosingsSummaryData,
             'ClosingsSummaryQuarterAnnualTotals' => $ClosingsSummaryQuarterAnnualTotals,

             'PendingsSummaryData' => $PendingsSummaryData,
             'PendingsSummaryQuarterAnnualTotals' => $PendingsSummaryQuarterAnnualTotals,

			 'dateRange'=>$dateRange,
			 'DateRangerSelect'=>$DateRanger->defaultSelect,
		));
	}
}