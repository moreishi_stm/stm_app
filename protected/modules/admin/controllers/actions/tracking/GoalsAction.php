<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class GoalsAction extends CAction
{
	public $renderType = 'render';

	public function run($id=null)
	{
		$this->controller->pageColor = 'blue';
		$this->controller->baseModel = new Goals('search');
		$this->controller->baseModel->unsetAttributes();  // clear any default values
		$this->controller->baseModel->id = $id;

		switch ($id) {
			case null && $action == null:
				$viewName = 'goalsList';
				$mode = 'List';
				break;

			case $id == 'add':
				$viewName = 'goalsForm';
				$mode = 'Add';
				break;

			case is_numeric($id):
				$viewName = 'goalsForm';
				$mode = 'Edit';
				$this->controller->baseModel = $this->controller->baseModel->findByPk($id);
				break;

			case 'goalsTab':
				$viewName = '_goalsTab'; //partialview
				$this->renderType = 'renderPartial';
				break;
		}

		$this->controller->{$this->renderType}($viewName, array(
			'model'=>$this->controller->baseModel,
			'mode'=>$mode,
		));
	}
}