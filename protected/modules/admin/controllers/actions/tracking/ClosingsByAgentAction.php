<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

/**
 * Closings By Agent
 *
 */
class ClosingsByAgentAction extends CAction
{
    public function run()
    {
        // Set title and theme
        $this->controller->title = 'Closings By Agent';
        $this->controller->pageColor = 'teal';

        // Get date range
        $DateRanger = new DateRanger();
        if($_POST['DateRanger']['date_preset'])
            $DateRanger->defaultSelect = $_POST['DateRanger']['date_preset'];
        $dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

        // Closings query
//        $closingsQuery = Yii::app()->db->createCommand()
//            ->select("c.id, c.closing_status_ma, t.component_type_id, c.contract_execute_date, c.contract_closing_date, c.actual_closing_datetime, c.price, s.name source_name, CONCAT(c2.first_name, ' ' , c2.last_name) full_name, a.address, a.city, a.state_id, a.zip, (" . $subQuery->text . ") assignments")
//            ->from('closings c')
//            ->leftJoin('transactions t', 'c.transaction_id = t.id')
//            ->leftJoin('sources s', 's.id = t.source_id')
//            ->leftJoin('contacts c2', 't.contact_id = c2.id')
//            ->leftJoin('addresses a', 't.address_id = a.id')
//            ->where('(c.is_deleted = 0 OR c.is_deleted IS NULL)');

        // Get closings
        $model = new Closings;
        $ClosedCriteria = $model->byActualClosingDate($dateRange)->byStatus(array(Closings::STATUS_TYPE_CLOSED_ID))->byAccountId(Yii::app()->user->accountId)->getDbCriteria();
        $ClosedCriteria->order = 'actual_closing_datetime DESC';


        // Get data provider
        $ClosedDataProvider = new CActiveDataProvider('Closings', array('criteria'=>$ClosedCriteria,'pagination'=>array('pageSize'=>1000))); //@todo: remove pagination

//        $ClosedGCI = Closings::getGCITotal($ClosedCriteria);
//        $ClosedVolume = Closings::getVolumeTotal($ClosedCriteria);

        // Render view
        $this->controller->render('closingsByAgent', array(
            'ClosedDataProvider'    =>  $ClosedDataProvider,
            'dateRange'             =>  $dateRange,
            'DateRangerSelect'      =>  $DateRanger->defaultSelect,
        ));
    }
}