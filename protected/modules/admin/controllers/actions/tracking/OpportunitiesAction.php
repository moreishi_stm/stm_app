<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class OpportunitiesAction extends CAction
{
	public function run($id=null)
    {
		$this->controller->title = 'Opportunities Report';
		$this->controller->pageColor = 'teal';

        $model = new Opportunities();
        $model->unsetAttributes();

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'last_3_months'; //@todo: change later to 30 days or this month
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        if(isset($_GET['Opportunities'])) {
            $model->attributes = $_GET['Opportunities'];
            $model->fromDate = ($dateRange['from_date']) ? $dateRange['from_date'] : null;
            $model->toDate = ($dateRange['to_date']) ? $dateRange['to_date'] : null;
            // allow there to be no selection

        }
        elseif($id)  {
            $model->added_by = $id;
        }

        $tableSummaryData = $this->_getSummaryTableData($model);

		$this->controller->render('opportunities', array(
            'model' => $model,
            'dateRange'=>$dateRange,
            'tableSummaryData' => $tableSummaryData,
        ));
	}

    protected function _getSummaryTableData($model)
    {
        $dataAgents = array(); //array('{added_by}' => array('name'=> {fullName}, '{component_type_id - seller/buyer}' => array('a'=> #, 'b' => #...)))
        $dataTotal = array();
        if($opportunities = Opportunities::model()->findAll($model->search()->criteria)) {
            foreach($opportunities as $opportunity) {

                // checks to see if array element exists, if not sets to 1, else increments by 1
                $transactionStatusId = $opportunity->getTransactionStatusByDate();
                if(isset($dataAgents[$opportunity->addedBy->fullName][$opportunity->component_type_id][$transactionStatusId])) {
                    $dataAgents[$opportunity->addedBy->fullName][$opportunity->component_type_id][$transactionStatusId]++;
                }
                else {
                    $dataAgents[$opportunity->addedBy->fullName][$opportunity->component_type_id][$transactionStatusId] = 1;
                }

                $dataTotal[$opportunity->component_type_id][$transactionStatusId] = (isset($dataTotal[$opportunity->component_type_id][$transactionStatusId])) ? $dataTotal[$opportunity->component_type_id][$transactionStatusId] + 1 : 1;
            }
        }
        ksort($dataAgents);

        return array('agents'=>$dataAgents, 'totals'=>$dataTotal);
    }

    public function printAppointment($appointmentId)
    {
        if(empty($appointmentId)) {
            return;
        }

        if($appointment = Appointments::model()->findByPk($appointmentId)) {
            echo 'Status: '.$appointment->getStatusTypes()[$appointment->met_status_ma].'<br>';
            echo 'Appt Date: '.Yii::app()->format->formatDate($appointment->set_for_datetime).'<br>';
            echo 'Set for: '.$appointment->metBy->fullName.'<br>';
        }
    }

    public function printTransaction($data)
    {
        if(empty($data->transaction)) {
            return;
        }

        $text = '';
        switch($data->transaction->transaction_status_id) {
            case TransactionStatus::HOT_A_LEAD_ID:
                $text = 'A';
                break;

            case TransactionStatus::B_LEAD_ID:
                $text = 'B';
                break;

            case TransactionStatus::C_LEAD_ID:
                $text = 'C';
                break;

            case TransactionStatus::D_NURTURING_SPOKE_TO_ID:
                $text = 'D';
                break;
        }
//        echo ($text) ? "<span class='status status$text'>$text</span>" : null;

        echo '<a href="/admin/'.$data->transaction->componentType->name.'/'.$data->transaction->id.'" target="_blank">'.(($text) ? "<span class='status status$text'>$text</span>" : null).$data->transaction->contact->fullName.' ('.$data->transaction->status->name.')</a><br>';


        echo '<div><strong>Action Plans:</strong> ';
        if($actionPlans = Yii::app()->db->createCommand("select distinct ap.name from tasks t LEFT JOIN action_plan_applied_lu lu ON t.id=lu.task_id LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id LEFT JOIN action_plans ap ON ap.id=i.action_plan_id WHERE t.component_type_id=".$data->transaction->componentType->id." AND t.component_id=".$data->transaction->id." AND t.complete_date IS NULL AND t.is_deleted=0 ORDER BY ap.name ASC")->queryColumn()) {
            echo implode(',', $actionPlans).'<br>';
        }
        else {
            $warningSpanStyle = ' style="display:inline-block; background-color: #D20000; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; top: 0px; margin-bottom: 1px;"';
            echo '<span'.$warningSpanStyle.'>NONE</span>';
        }
        echo '</div>';


        echo '<div><strong>Next 30-day Task:</strong> ';
        $result = Yii::app()->db->createCommand("select min(due_date) dueDate, CONCAT(first_name,' ',last_name) fullName from tasks t JOIN contacts c ON c.id=t.assigned_to_id where component_type_id=".$data->transaction->componentType->id." AND component_id=".$data->transaction->id." AND due_date>='".date('Y-m-d')."' AND due_date<='".date("Y-m-d", strtotime('30 days'))."' AND t.is_deleted=0")->queryRow();
        if($result['dueDate']) {
            echo Yii::app()->format->formatDate($result['dueDate'], 'm/d/Y').' ('.$result['fullName'].')';
        }
        else {
            $warningSpanStyle = ' style="display:inline-block; background-color: #D20000; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; top: 0px; margin-bottom: 1px;"';
            echo '<span'.$warningSpanStyle.'>NONE</span>';
        }
        echo '</div>';

        // display if no follow-up in last 30 days
        $activityTypeIds = array(TaskTypes::PHONE, TaskTypes::DIALER_ANSWER, TaskTypes::TODO, TaskTypes::EMAIL_MANUAL);
        if(!Yii::app()->db->createCommand("select max(activity_date) from activity_log where component_type_id=".$data->transaction->componentType->id." AND component_id=".$data->transaction->id." AND activity_date>='".date('Y-m-d', strtotime("-1 month"))."' AND task_type_id IN (".implode(',', $activityTypeIds).") AND is_deleted=0")->queryScalar()) {
            echo '<div><strong>Alert:</strong> ';

            $warningSpanStyle = ' style="display:inline-block; background-color: #D20000; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; top: 0px; margin-bottom: 16px;"';
            echo '<span'.$warningSpanStyle.'>FOLLOW-UP OVERDUE</span>';
            echo '</div>';
        }
    }

    public function printStatus($data)
    {

        //calculate diff between target/current date and determine heat status
        $addedDate = new DateTime($data->added);
        $targetDate = new DateTime($data->target_date);
        $dateDiff = $targetDate->diff($addedDate);
        $daysDiff = $dateDiff->days;

        $text = '';
        if($daysDiff <= 30) {
            $text = 'A';

        } elseif($daysDiff <= 60) {
            $text = 'B';

        } elseif($daysDiff <= 90) {
            $text = 'C';

        } elseif($daysDiff > 90) {
            $text = 'D';

        }

        echo ($text) ? "<span class='status status$text'>$text</span>" : null;
        echo '<br>'.$data->componentType->singularName.'<br>';

        echo Yii::app()->format->formatDate($data->target_date, "n/j/y");

    }
}