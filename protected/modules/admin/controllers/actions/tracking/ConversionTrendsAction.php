<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class ConversionTrendsAction extends CAction
{
    protected $_xAxisTickMap = array();
    protected $_setByContacts = array();
    protected $_setByContactIds = array();
    protected $_componentTypeIds = array(ComponentTypes::SELLERS,ComponentTypes::BUYERS);
    protected $_labelTotalGraphs = array(
                                        'allAppointmentsSet' => 'All Appointments Set',
                                        'allAppointmentsMet' => 'All Appointments Met',
                                        'allSignedAgreements' => 'All Signed Agreements',
                                        'allSetMetConversion' => 'All Set-Met Conversion %',
                                        'allMetSignedConversion' => 'All Met-Signed Conversion %',

                                        'sellersAppointmentsSet' => 'Seller Appointments Set',
                                        'sellersAppointmentsMet' => 'Seller Appointments Met',
                                        'sellersSignedAgreements' => 'Seller Signed Agreements',
                                        'sellersSetMetConversion' => 'Seller Set-Met Conversion %',
                                        'sellersMetSignedConversion' => 'Seller Met-Signed Conversion %',

                                        'buyersAppointmentsSet' => 'Buyer Appointments Set',
                                        'buyersAppointmentsMet' => 'Buyer Appointments Met',
                                        'buyersSignedAgreements' => 'Buyer Signed Agreements',
                                        'buyersSetMetConversion' => 'Buyer Set-Met Conversion %',
                                        'buyersMetSignedConversion' => 'Buyer Met-Signed Conversion %',
                                    );

	public function run($id=null)
    {
		$this->controller->title = 'Conversion Trends Report';
		$this->controller->pageColor = 'teal';

        $graphData = array();
        $dateRange = array('from_date'=> date('Y-m-01', strtotime('-12 months')), 'to_date' => date('Y-m-d'));

        // get contacts that have set appts in this timeframe
        $results = Yii::app()->db->createCommand("SELECT DISTINCT set_by_id id, first_name, last_name, phonetic_name FROM appointments a JOIN contacts c ON c.id=a.set_by_id where DATE(set_on_datetime) BETWEEN '".$dateRange['from_date']."' AND '".$dateRange['to_date']."' ORDER BY first_name")->queryAll();
        foreach($results as $row) {
            $this->_setByContacts[$row['id']] = $row['first_name']." ".$row['last_name'].(($row['phonetic_name']) ? ' ('.$row['phonetic_name'].')' : '');
            $this->_setByContactIds[] = $row['id'];
        }

        // build x-axis ticks based on current date
        $xAxisTicks = array();
        for($i=12; $i>=0; $i--) {
            $xAxisTicks[] = date('M', strtotime("-{$i} months"));
            $this->_xAxisTickMap[date('Y-m', strtotime("-{$i} months"))] = count($xAxisTicks)-1;
        }
        // populate x-axis labels
        $graphData['xAxisTicks'] = StmFunctions::formatChartDataToJson($xAxisTicks);

        if (Yii::app()->request->isAjaxRequest) {
            $this->_processAjaxRequest($dateRange);
        }

        $this->controller->render('conversionTrends', array(
										   'dateRange'=>$dateRange,
                                           'graphData' => $graphData,
                                           'xAxisTicks' => $graphData['xAxisTicks'],
                                           'contactIdsListData' => $this->_setByContacts,
										   ));
	}

    protected function _populateLineGraphData($data, $color=null)
    {
        // fill in zero all missing months
        $this->_populateEmptyMonths($data);
        ksort($data);

        // populate line graph data
        $lineGraphData = '';
        foreach($data as $key => $contactData) {

            $rowChartData = array();
            $i=0;
            if(is_array($contactData)) {

                foreach($contactData as $value) {
                    $rowChartData[] = array($i, intval($value));
                    $i++;
                }
            }

            $label = '';
            if($this->_setByContacts[$key]) {
                $label = $this->_setByContacts[$key];
            }
            elseif($this->_labelTotalGraphs[$key]) {
                $label = $this->_labelTotalGraphs[$key];
            }

            $lineGraphArray = array('label'=> $label,'data' => $rowChartData);
            if($color && $color[$key]) {
                $lineGraphArray['color'] = $color[$key];
            }

            // array version of line graph data
            $lineGraphData[] = $lineGraphArray;
        }
        return $lineGraphData;
    }

    protected function _populateEmptyMonths(&$data)
    {
        if(!is_array($data)) {
            return;
        }

        foreach($data as $contactId => $contactData) {

            foreach($this->_xAxisTickMap as $yearMonth => $i) {

                if(!isset($data[$contactId][$yearMonth])) {

                    $data[$contactId][$yearMonth] = 0;
                }
            }
            ksort($data[$contactId]);
        }
    }

    protected function _processAjaxRequest($dateRange) {

        if (Yii::app()->request->isAjaxRequest) {

            $componentTypeIds = ($_POST['componentTypeIds'])? $_POST['componentTypeIds'] : $this->_componentTypeIds;
            $contactIds = ($_POST['contactIds'])? $_POST['contactIds'] : $this->_setByContactIds;

            if($_POST['activeAdminsOnly']) {
                $activeAdmins = Contacts::model()->byActiveAdmins()->findAll();
                $activeAdminIds = array();
                foreach($activeAdmins as $activeAdmin) {
                    $activeAdminIds[] = $activeAdmin->id;
                }

                $contactIds = array_intersect($activeAdminIds, $contactIds);
            }

            // populate set appointment data
            $baseTotalCommand = Yii::app()->db->createCommand()
                ->select('CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) yearMonth, a.set_by_id, a.component_type_id, ct.name componentName, count(a.id) count')
                ->from('appointments a')
                ->join('component_types ct','ct.id=a.component_type_id')
                ->where('DATE(a.set_on_datetime) BETWEEN "'.$dateRange['from_date'].'" AND "'.$dateRange['to_date'].'"')
                ->andWhere(array('in','a.component_type_id', array(ComponentTypes::SELLERS, ComponentTypes::BUYERS)))
                ->andWhere('a.is_deleted=0')
                ->group('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime)')
                ->order('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime)');

            $baseCommand = Yii::app()->db->createCommand()
                ->select('CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) yearMonth, a.component_type_id, ct.name componentName, a.set_by_id, count(a.id) count')
                ->from('appointments a')
                ->join('component_types ct','ct.id=a.component_type_id')
                ->where('DATE(a.set_on_datetime) BETWEEN "'.$dateRange['from_date'].'" AND "'.$dateRange['to_date'].'"')
                ->andWhere(array('in','a.set_by_id', $contactIds))
                ->andWhere(array('in','a.component_type_id', $componentTypeIds))
                ->andWhere('a.is_deleted=0')
                ->group('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime), a.set_by_id')
                ->order('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime), a.set_by_id');

            // appointment set by user
            $apptSetData = array();
            $apptSetCommand = clone $baseCommand;
            if($results = $apptSetCommand->queryAll()) {
                foreach($results as $row) {
                    $apptSetData[$row['set_by_id']][$row['yearMonth']] = $row['count'];
                }
            }
            $graphData['apptSet'] = $this->_populateLineGraphData($apptSetData);


            // appointment set - TOTAL
            $apptSetTotalData = array();
            $apptSetTotalCommand = clone $baseTotalCommand;
            $apptSetTotalCommand->group('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime), a.component_type_id');
            if($results = $apptSetTotalCommand->queryAll()) {
                foreach($results as $row) {
                    $apptSetTotalData['allAppointmentsSet'][$row['yearMonth']] += $row['count'];
                    $apptSetTotalData[$row['componentName'].'AppointmentsSet'][$row['yearMonth']] += $row['count'];
                }
            }
            $graphData['executiveSummarySetAppointments'] = $this->_populateLineGraphData($apptSetTotalData, array('allAppointmentsSet'=>'#F772BE','sellersAppointmentsSet'=>'#67EB67','buyersAppointmentsSet'=>'#FAED3E'));

            // appointment met by user
            $apptMetData = array();
            $apptMetCommand = clone $baseCommand;
            if($results = $apptMetCommand->andWhere('a.met_status_ma='.Appointments::MET_STATUS_MET)->queryAll()) {
                foreach($results as $row) {
                    $apptMetData[$row['set_by_id']][$row['yearMonth']] += $row['count'];
                }
            }
            $graphData['apptMet'] = $this->_populateLineGraphData($apptMetData);

            // appointment met - TOTAL
            $apptMetTotalData = array();
            $apptMetTotalCommand = clone $baseTotalCommand;
            $apptMetTotalCommand->group('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime), a.component_type_id');
            if($results = $apptMetTotalCommand->queryAll()) {
                foreach($results as $row) {
                    $apptMetTotalData['allAppointmentsMet'][$row['yearMonth']] += $row['count'];
                    $apptMetTotalData[$row['componentName'].'AppointmentsMet'][$row['yearMonth']] += $row['count'];
                }
            }
            $graphData['executiveSummaryMetAppointments'] = $this->_populateLineGraphData($apptMetTotalData, array('allAppointmentsMet'=>'#F772BE','sellersAppointmentsMet'=>'#67EB67','buyersAppointmentsMet'=>'#FAED3E'));


            // appointment signed data by user
            $apptSignedData = array();
            $apptSignedCommand = clone $baseCommand;
            if($results = $apptSignedCommand->andWhere('a.is_signed=1 AND a.signed_date > 0')->queryAll()) {
                foreach($results as $row) {
                    $apptSignedData[$row['set_by_id']][$row['yearMonth']] += $row['count'];
                }
            }
            $graphData['isaApptSigned'] = $this->_populateLineGraphData($apptSignedData);

            // appointment signed - TOTAL
            $apptSignedTotalData = array();
            $apptSignedTotalCommand = clone $baseTotalCommand;
            $apptSignedTotalCommand->group('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime), a.set_by_id, a.component_type_id');
            if($results = $apptSignedTotalCommand->andWhere('a.is_signed=1 AND a.signed_date > 0')->queryAll()) {
                foreach($results as $row) {
                    $apptSignedTotalData['allSignedAgreements'][$row['yearMonth']] += $row['count'];
                    $apptSignedTotalData[$row['componentName'].'SignedAgreements'][$row['yearMonth']] += $row['count'];
                }
            }
            $graphData['executiveSummarySigned'] = $this->_populateLineGraphData($apptSignedTotalData, array('allSignedAgreements'=>'#F772BE','sellersSignedAgreements'=>'#67EB67','buyersSignedAgreements'=>'#FAED3E'));


            // appointment set-met % data - user
            $apptSetMetConversionData = array();
            $apptSetMetConversionCommand = clone $baseCommand;
            $metSubQuery = 'SELECT count(a2.id) FROM appointments a2 WHERE a2.set_by_id=a.set_by_id AND CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a2.set_on_datetime), "-", DATE_FORMAT(a2.set_on_datetime,"%m")) AND a2.is_deleted=0 AND a2.met_status_ma='.Appointments::MET_STATUS_MET.' AND a2.component_type_id IN('.implode(',', $componentTypeIds).')';
            $setSubQuery = 'SELECT count(a3.id) FROM appointments a3 WHERE a3.set_by_id=a.set_by_id AND CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a3.set_on_datetime), "-", DATE_FORMAT(a3.set_on_datetime,"%m")) AND a3.is_deleted=0 AND a3.component_type_id IN('.implode(',', $componentTypeIds).')';
            $setMetSubquery = '(ROUND(('.$metSubQuery.') / ('.$setSubQuery.') * 100 ))';
            if($results = $apptSetMetConversionCommand->select('CONCAT(YEAR(set_on_datetime), "-", DATE_FORMAT(set_on_datetime,"%m")) yearMonth, set_by_id, '.$setMetSubquery.' count')->queryAll()) {
                foreach($results as $row) {
                    $apptSetMetConversionData[$row['set_by_id']][$row['yearMonth']] = $row['count'];
                }
            }
            $graphData['apptSetMetConversion'] = $this->_populateLineGraphData($apptSetMetConversionData);


            // appointment set-met % data - TOTAL
            $apptSetMetConversionTotalData = array();
            $apptSetMetConversionTotalCommand = clone $baseTotalCommand;
            $metTotalSubQuery = 'SELECT count(a2b.id) FROM appointments a2b WHERE CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a2b.set_on_datetime), "-", DATE_FORMAT(a2b.set_on_datetime,"%m")) AND a2b.is_deleted=0 AND a2b.met_status_ma='.Appointments::MET_STATUS_MET.' AND a2b.component_type_id IN('.ComponentTypes::SELLERS.','.ComponentTypes::BUYERS.')';
            $setTotalSubQuery = 'SELECT count(a3b.id) FROM appointments a3b WHERE CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a3b.set_on_datetime), "-", DATE_FORMAT(a3b.set_on_datetime,"%m")) AND a3b.is_deleted=0 AND a3b.component_type_id IN('.ComponentTypes::SELLERS.','.ComponentTypes::BUYERS.')';
            $setMetTotalSubquery = '(ROUND(('.$metTotalSubQuery.') / ('.$setTotalSubQuery.') * 100 ))';
            if($results = $apptSetMetConversionTotalCommand->select('CONCAT(YEAR(set_on_datetime), "-", DATE_FORMAT(set_on_datetime,"%m")) yearMonth, '.$setMetTotalSubquery.' count, ct.name componentName')->queryAll()) {
                foreach($results as $row) {
                    $apptSetMetConversionTotalData['allSetMetConversion'][$row['yearMonth']] += $row['count'];
//                    $apptSetMetConversionTotalData[$row['componentName'].'SetMetConversion'][$row['yearMonth']] = $row['count'];
                }
            }
            $graphData['executiveSummarySetMetConversion'] = CMap::mergeArray((array) $graphData['executiveSummarySetMetConversion'], $this->_populateLineGraphData($apptSetMetConversionTotalData, array('allSetMetConversion'=>'#F772BE')));

            // appointment set-met % data - TOTAL Seller/Buyer
            $apptSetMetConversionComponentTotalData = array();
            $apptSetMetConversionComponentTotalCommand = clone $baseTotalCommand;
            $apptSetMetConversionComponentTotalCommand->group('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime), a.component_type_id');
            $metComponentTotalSubQuery = 'SELECT count(a2b.id) FROM appointments a2b WHERE a.component_type_id=a2b.component_type_id AND CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a2b.set_on_datetime), "-", DATE_FORMAT(a2b.set_on_datetime,"%m")) AND a2b.is_deleted=0 AND a2b.met_status_ma='.Appointments::MET_STATUS_MET.' AND a2b.component_type_id IN('.ComponentTypes::SELLERS.','.ComponentTypes::BUYERS.')';
            $setComponentTotalSubQuery = 'SELECT count(a3b.id) FROM appointments a3b WHERE a.component_type_id=a3b.component_type_id AND CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a3b.set_on_datetime), "-", DATE_FORMAT(a3b.set_on_datetime,"%m")) AND a3b.is_deleted=0 AND a3b.component_type_id IN('.ComponentTypes::SELLERS.','.ComponentTypes::BUYERS.')';
            $setMetComponentTotalSubquery = '(ROUND(('.$metComponentTotalSubQuery.') / ('.$setComponentTotalSubQuery.') * 100 ))';
            if($results = $apptSetMetConversionComponentTotalCommand->select('CONCAT(YEAR(set_on_datetime), "-", DATE_FORMAT(set_on_datetime,"%m")) yearMonth, '.$setMetComponentTotalSubquery.' count, ct.name componentName')->queryAll()) {
                foreach($results as $row) {
                    $apptSetMetConversionComponentTotalData[$row['componentName'].'SetMetConversion'][$row['yearMonth']] = $row['count'];
                }
            }
            $graphData['executiveSummarySetMetConversion'] = CMap::mergeArray((array) $graphData['executiveSummarySetMetConversion'], $this->_populateLineGraphData($apptSetMetConversionComponentTotalData, array('sellersSetMetConversion'=>'#67EB67', 'buyersSetMetConversion'=>'#FAED3E')));


            // appointment met-signed % data
            $apptMetSignedConversionData = array();
            $apptMetSignedConversionCommand = clone $baseCommand;
            $signedSubQuery = 'SELECT count(a4.id) FROM appointments a4 WHERE a4.set_by_id=a.set_by_id AND CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a4.set_on_datetime), "-", DATE_FORMAT(a4.set_on_datetime,"%m")) AND a4.is_deleted=0 AND a4.is_signed=1 AND a4.signed_date > 0 AND a4.component_type_id IN('.implode(',', $componentTypeIds).')';
            $metSignedSubquery = '(ROUND(('.$signedSubQuery.') / ('.$metSubQuery.') * 100 ))';
            if($results = $apptMetSignedConversionCommand->select('CONCAT(YEAR(set_on_datetime), "-", DATE_FORMAT(set_on_datetime,"%m")) yearMonth, set_by_id, '.$metSignedSubquery.' count')->queryAll()) {
                foreach($results as $row) {
                    $apptMetSignedConversionData[$row['set_by_id']][$row['yearMonth']] = $row['count'];
                }
            }
            $graphData['apptMetSignedConversion'] = $this->_populateLineGraphData($apptMetSignedConversionData);


            // appointment met-signed % data - TOTAL
            $apptMetSignedConversionTotalData = array();
            $apptMetSignedConversionTotalCommand = clone $baseTotalCommand;
            $signedTotalSubQuery = 'SELECT count(a4b.id) FROM appointments a4b WHERE CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a4b.set_on_datetime), "-", DATE_FORMAT(a4b.set_on_datetime,"%m")) AND a4b.is_deleted=0 AND a4b.is_signed=1 AND a4b.signed_date > 0 AND a4b.component_type_id IN('.implode(',', $componentTypeIds).')';
            $metSignedTotalSubquery = '(ROUND(('.$signedTotalSubQuery.') / ('.$metTotalSubQuery.') * 100 ))';
            if($results = $apptMetSignedConversionTotalCommand->select('CONCAT(YEAR(set_on_datetime), "-", DATE_FORMAT(set_on_datetime,"%m")) yearMonth, '.$metSignedTotalSubquery.' count, ct.name componentName')->queryAll()) {
                foreach($results as $row) {
                    $apptMetSignedConversionTotalData['allMetSignedConversion'][$row['yearMonth']] += $row['count']; //@toto: conversion total slightly diff than sum
                }
            }
            $graphData['executiveSummaryMetSignedConversion'] = CMap::mergeArray((array) $graphData['executiveSummaryMetSignedConversion'], $this->_populateLineGraphData($apptMetSignedConversionTotalData, array('allMetSignedConversion'=>'#F772BE')));


            // appointment met-signed % data - TOTAL Sellers/Buyers
            $apptMetSignedConversionComponentTotalData = array();
            $apptMetSignedConversionComponentTotalCommand = clone $baseTotalCommand;
            $apptMetSignedConversionComponentTotalCommand->group('YEAR(a.set_on_datetime), MONTH(a.set_on_datetime), a.component_type_id');
            $signedComponentTotalSubQuery = 'SELECT count(a4c.id) FROM appointments a4c WHERE a.component_type_id=a4c.component_type_id AND CONCAT(YEAR(a.set_on_datetime), "-", DATE_FORMAT(a.set_on_datetime,"%m")) = CONCAT(YEAR(a4c.set_on_datetime), "-", DATE_FORMAT(a4c.set_on_datetime,"%m")) AND a4c.is_deleted=0 AND a4c.is_signed=1 AND a4c.signed_date > 0 AND a4c.component_type_id IN('.ComponentTypes::SELLERS.','.ComponentTypes::BUYERS.')';
            $metSignedComponentTotalSubquery = '(ROUND(('.$signedComponentTotalSubQuery.') / ('.$metComponentTotalSubQuery.') * 100 ))';
            if($results = $apptMetSignedConversionComponentTotalCommand->select('CONCAT(YEAR(set_on_datetime), "-", DATE_FORMAT(set_on_datetime,"%m")) yearMonth, '.$metSignedComponentTotalSubquery.' count, ct.name componentName')->queryAll()) {
                foreach($results as $row) {
                    $apptMetSignedConversionComponentTotalData[$row['componentName'].'MetSignedConversion'][$row['yearMonth']] = $row['count'];
                }
            }
            $graphData['executiveSummaryMetSignedConversion'] = CMap::mergeArray((array) $graphData['executiveSummaryMetSignedConversion'], $this->_populateLineGraphData($apptMetSignedConversionComponentTotalData, array('sellersMetSignedConversion'=>'#67EB67', 'buyersMetSignedConversion'=>'#FAED3E')));



            echo CJSON::encode($graphData);
            Yii::app()->end();
        }
    }
}
