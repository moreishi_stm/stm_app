<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class ConversionAction extends CAction {

	public function run($id=null) {

		$this->controller->title = 'Conversion Report';
		$this->controller->pageColor = 'teal';

		$model = new Contacts;
		$Criteria = $model->byActiveAdmins(true)->getDbCriteria();
		$Criteria->order = 'first_name ASC';
        $defaultAssignmentTypes = array(AssignmentTypes::LISTING_AGENT, AssignmentTypes::BUYER_AGENT, AssignmentTypes::ISA);

		if($_GET) {
			if(!empty($_GET['assignment_type_id']) && !empty($_GET['assignment_type_id'][0]))  {
				$AssignmentTypeCriteria = $model->byAssignmentTypeSetting($_GET['assignment_type_id'])->getDbCriteria();
				$Criteria->mergeWith($AssignmentTypeCriteria);
			}
            if(!empty($_GET['contact_id']) && !empty($_GET['contact_id'][0]))  {
                $ContactCriteria = $model->getDbCriteria()->addInCondition('t.id',(array) $_GET['contact_id']);
                $Criteria->mergeWith($ContactCriteria);
            }
		}
        else {
            $AssignmentTypeCriteria = $model->byAssignmentTypeSetting($defaultAssignmentTypes)->getDbCriteria();
            $Criteria->mergeWith($AssignmentTypeCriteria);
        }

		$DateRanger = new DateRanger();
		$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

		$DataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>20)));

		$this->controller->render('conversion', array(
										   'DataProvider'=>$DataProvider,
										   'dateRange'=>$dateRange,
                                           'defaultAssignmentTypes' => $defaultAssignmentTypes,
										   ));
	}

	protected function processAjaxRequest(Contacts $model) {
		if (Yii::app()->request->isAjaxRequest) {

			echo $this->controller->renderPartial('_conversion', array(), true);
			Yii::app()->end();
		}
	}
}
