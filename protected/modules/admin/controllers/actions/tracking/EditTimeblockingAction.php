<?php

class EditTimeblockingAction extends CAction {

	public function run($id) {

		$timeblockModel = Timeblockings::model()->findByPk($id);
        if (isset($_POST['Timeblockings'])) {
            $timeblockModel->attributes = $_POST['Timeblockings'];
		}

		$this->performAjaxRequest($timeblockModel);
	}

	protected function performAjaxRequest(Timeblockings $timeblockModel) {

		if (Yii::app()->request->isAjaxRequest) {
            if (!$timeblockModel->save()) {
                echo CActiveForm::validate($timeblockModel);
            }

			Yii::app()->end();
		}
	}
}