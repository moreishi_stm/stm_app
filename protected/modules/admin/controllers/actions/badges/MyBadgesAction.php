<?php
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class MyBadgesAction extends CAction {

		public function run($id = null) {
			$this->controller->title = 'My Badges';
			if (!$id) {
				$id = Yii::app()->user->id;
			}

			$Badge = Badges::model()->findByPk(Badges::THANK_YOU);

			//@todo: need to refactor after we figure out our master plan for badges. Currently only designed to truly for thank yous badges.

			$BadgePointsSent = new BadgePointsEarned;
			$BadgePointSentDataProvider = new CActiveDataProvider($BadgePointsSent->byAwarder($id), array());

			$BadgePointsReceived = new BadgePointsEarned;
			$BadgePointReceivedDataProvider = new CActiveDataProvider($BadgePointsReceived->byAwardee($id), array());

			$this->controller->render('myBadges', array(
					'Badge' => $Badge,
					'BadgePointReceivedDataProvider' => $BadgePointReceivedDataProvider,
					'BadgePointSentDataProvider' => $BadgePointSentDataProvider,
				)
			);
		}
	}
