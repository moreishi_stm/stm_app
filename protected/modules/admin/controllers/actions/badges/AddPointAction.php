<?php
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class AddPointAction extends CAction {

		public function run($id) {
			$this->controller->title = 'Award Badge Point';

			//if referrer is not this page set returnUrl
			//		if(strpos($_SERVER['REQUEST_URI'],'/'.Yii::app()->controller->module->id.'/badges/addPoint') != false) {
			//			$returnUrl = $_SERVER['REQUEST_URI'];
			//		} else {
			//			$returnUrl = $this->controller->createUrl('/'.Yii::app()->controller->module->id.'/badges');
			//		}

			$Badge = Badges::model()->findByPk($id);
			$model = new BadgePointsEarned;
			$model->badge_point_type_id = $id;
			if ($id == BadgePointTypes::THANK_YOU) {
				$model->scenario = 'thankyou';
			}

			if (isset($_POST['BadgePointsEarned'])) {
				$model->attributes = $_POST['BadgePointsEarned'];
				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Successfully sent Thank You Badge!');
					$this->controller->redirect($this->controller->createUrl('/'.Yii::app()->controller->module->id.'/badges'));
				}
			}

			$this->controller->render('addPoint', array(
					'model' => $model,
					'Badge' => $Badge
				)
			);
		}
	}
