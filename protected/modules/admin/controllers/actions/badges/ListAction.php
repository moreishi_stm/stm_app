<?php
	Yii::import('admin_widgets.DateRanger.DateRanger');

	class ListAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Badges List';

			$DateRanger = new DateRanger();
			$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

			$model = new BadgePointsEarned;
			$model->byDateRange($dateRange,'added');
			$model->unsetAttributes(); // clear any default values

			$this->controller->render('list', array(
					'model' => $model,
					'dateRange'=>$dateRange,
				)
			);
		}
	}
