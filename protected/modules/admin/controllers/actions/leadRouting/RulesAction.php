<?php

class RulesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->baseModel = new LeadRouteRules;
		$model=$this->controller->loadModel($id);
		if (isset($_POST['LeadRouteRules']))
		{
			$model->attributes=$_POST['LeadRouteRules'];
			if ($model->save())
			{
				// We only will add new ones when one is not assigned to the contact or when one is deleted
				if ($model->contactsToAdd)
				{
					$contactsToAddFlipped = array_flip($model->contactsToAdd);
					foreach($model->leadRouteContactLu as $LeadRouteContactLu)
					{
						if (!in_array($LeadRouteContactLu->contact_id, $model->contactsToAdd))
							$LeadRouteContactLu->delete();
						else
							unset($contactsToAddFlipped[$LeadRouteContactLu->contact_id]);
					}
					$model->contactsToAdd = array_flip($contactsToAddFlipped);

					// // Attach the contact types to the newly contact
					foreach($model->contactsToAdd as $contactId)
					{
						$LeadRouteContactLu = new LeadRouteContactLu;
						$LeadRouteContactLu->lead_route_id = $model->id;
						$LeadRouteContactLu->contact_id = $contactId;
						$LeadRouteContactLu->save();
					}
				}

				Yii::app()->user->setFlash('success', 'Successfully Updated!');

				$this->controller->redirect(array('rules','id'=>$model->id));
			}
		}else{ // Pull existing values
			// Process the contact types
			// if ($model->contacts)
			// 	foreach($model->contacts as $Contact)
			// 		array_push($model->contactsToAdd, $Contact->id);
		}
		$this->controller->render('formRule',array(
			'model'=>$model
		));
	}
}