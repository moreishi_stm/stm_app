<?php

class RoutesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$model=$this->controller->loadModel($id);
        $triggeredRoutes = $model->triggeredRoutes;

        // this is singular vs. the relationship which is plural
        $model->triggeredRouteId = ($triggeredRoutes)? current($triggeredRoutes)->id : null;

		$this->controller->title = $model->name;

        if (isset($_POST['LeadRouteAutoresponder'])) {
            LeadRouteAutoresponder::model()->deleteAllByAttributes(array(
                'lead_route_id' => $model->id,
            ));
            foreach ($_POST['LeadRouteAutoresponder'] as $autoResponderPost) {
                if ($autoResponderPost['type_ma'] and $autoResponderPost['component_id']) {
                    $autoResponder = new LeadRouteAutoresponder;
                    $autoResponder->lead_route_id = $model->id;
                    $autoResponder->attributes = $autoResponderPost;
                    $autoResponder->save();
                }
            }
        } else if (isset($_POST['LeadRoutes'])) {
            /**
             * If we enter this block then we have removed all auto responders from the page
             */
            LeadRouteAutoresponder::model()->deleteAllByAttributes(array(
                'lead_route_id' => $model->id,
            ));
        }


		if (isset($_POST['LeadRoutes']))
		{
			$model->attributes=$_POST['LeadRoutes'];
            $model->cc_contact_ids = (isset($_POST['LeadRoutes']['cc_contact_ids'])) ? $_POST['LeadRoutes']['cc_contact_ids'] : null;
			if ($model->save())
			{
				// We only will add new ones when one is not assigned to the contact or when one is deleted
				if ($model->contactsToAdd)
				{
					$contactsToAddFlipped = array_flip($model->contactsToAdd);
                    $roundRobinSortOrder = 1;
                    $roundRobbins = $model->leadRouteRoundRobinContactLu;
                    //checking for soft deletes //@todo: may want to remove soft deletes later if not used.
                    if($deletedRoundRobins = LeadRoundRobinContactLu::model()->skipSoftDeleteCheck()->findAllByAttributes(array('lead_route_id'=>$model->id, 'is_deleted'=>1))) {
                        foreach($deletedRoundRobins as $key => $deletedRoundRobbin) {
                            $deletedRoundRobinsTemp[$deletedRoundRobbin->contact_id] = $deletedRoundRobbin;
                        }
                        $deletedRoundRobins = $deletedRoundRobinsTemp;
                    }
					foreach($model->leadRouteRoundRobinContactLu as $key => $LeadRouteContactLu)
					{
                        if (isset($deletedRoundRobins[$LeadRouteContactLu->contact_id])) {
                            $roundRobinToUndelete = $deletedRoundRobins[$LeadRouteContactLu->contact_id];
                            $roundRobinToUndelete->is_deleted = 0;
                            $roundRobinToUndelete->sort_order = $contactsToAddFlipped[$LeadRouteContactLu->contact_id]+1;
                            $roundRobinToUndelete->save();
                            unset($contactsToAddFlipped[$LeadRouteContactLu->contact_id]);
                        } elseif (!in_array($LeadRouteContactLu->contact_id, $model->contactsToAdd)) {
                            $LeadRouteContactLu->delete();
                        } else {
                            $LeadRouteContactLu->sort_order = $contactsToAddFlipped[$LeadRouteContactLu->contact_id]+1;
                            $LeadRouteContactLu->save();
                            unset($contactsToAddFlipped[$LeadRouteContactLu->contact_id]);
                        }
					}
					$model->contactsToAdd = array_flip($contactsToAddFlipped);

					// // Attach the contact types to the newly contact
					foreach($model->contactsToAdd as $roundRobinSortOrder => $contactId)
					{
                        if (isset($deletedRoundRobins[$contactId])) {
                            $LeadRouteContactLu = $deletedRoundRobins[$contactId];
                        } else {
                            $LeadRouteContactLu = new LeadRoundRobinContactLu;
                        }

						$LeadRouteContactLu->lead_route_id = $model->id;
                        $LeadRouteContactLu->sort_order = $roundRobinSortOrder+1;
						$LeadRouteContactLu->contact_id = $contactId;
                        $LeadRouteContactLu->is_deleted = 0;
						if(!$LeadRouteContactLu->save()) {
                            Yii::app()->user->setFlash('error', 'Error Updating Lead Round Robin Contact!');
                        }
					}
				}

                // check to see if triggered route data exists, if so add if needed or delete all if empty
                if($model->triggeredRouteId) {
                    $existingTriggeredRoute = LeadRoutesTriggered::model()->findByAttributes(array('parent_route_id'=>$model->id, 'triggered_route_id'=>$model->triggeredRouteId));
                    if(!$existingTriggeredRoute) {
                        LeadRoutesTriggered::model()->deleteAllByAttributes(array('parent_route_id'=>$model->id));
                        $leadRouteTrigger = new LeadRoutesTriggered;
                        $leadRouteTrigger->parent_route_id = $model->id;
                        $leadRouteTrigger->triggered_route_id = $model->triggeredRouteId;
                        $leadRouteTrigger->save();
                    }
                }
                else {
                    LeadRoutesTriggered::model()->deleteAllByAttributes(array('parent_route_id'=>$model->id));
                }

				Yii::app()->user->setFlash('success', 'Successfully Updated!');

				$this->controller->redirect(array('index'));
			} else {
				Yii::app()->user->setFlash('error', 'Error Updating Lead Route!');
			}
		}else{
            if($model->type_ma == LeadRoutes::ROUTE_TYPE_ROUND_ROBIN_ID) {
                // load the round robbin contacts for multi-select widget, uses contactsToAdd property
                $model->contactsToAdd = StmFormHelper::multiSelectLoader($model, $options=array('relationName'=>'leadRouteRoundRobinContactLu','relationPKColumn'=>'contact_id'));
            }
		}

		$this->controller->render('formRoute',array(
			'model'=>$model,
		));
	}
}