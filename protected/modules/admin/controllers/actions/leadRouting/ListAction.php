<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'Lead Routes';
		$model=new LeadRoutes('search');
		$model->unsetAttributes();  // clear any default values

		$RulesModel=new LeadRouteRules('search');
		$RulesModel->unsetAttributes();  // clear any default values


		// used for gridview search box inputs
		if (isset($_GET['LeadRoutes']))
			$model->attributes=$_GET['LeadRoutes'];

		if (isset($_GET['LeadRouteRules']))
			$model->attributes=$_GET['LeadRouteRules'];

		$this->controller->render('list',array(
			'model'=>$model,
			'RulesModel'=>$RulesModel,
		));
	}
}