<?php

class DomainListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$model=new Domains('search');
		$model->unsetAttributes();  // clear any default values

		if (isset($_GET['Domains'])) {
			$model->attributes = $_GET['Domains'];
		}

		// if the account only has 1 domain, it automatically forwards to that domain's list of pages
		if ($model->count == 1) {
			$model->account_id = Yii::app()->user->accountId;
			$model = $model->find();
			$this->controller->redirect(array('domain','id'=>$model->id));
		}

		$this->controller->title = 'CMS Websites List';
		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}