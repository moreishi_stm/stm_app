<?php

class TagsAddAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'Add CMS Tag';

		$model = new CmsTags;
        $this->performAjaxRequest($model);

		if (isset($_POST['CmsTags'])) {
			$model->attributes = $_POST['CmsTags'];

            $tagUrl = trim(strtolower($model->name));
            $tagUrl = preg_replace("/[^a-zA-Z0-9-\s]+/", "", $tagUrl);
            $tagUrl = str_replace(" ", "-", $tagUrl);
            $tagUrl = preg_replace('/-{2,}/', '-', $tagUrl);
            $model->url = $tagUrl;

			if($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully added CMS Tag.');
				$this->controller->redirect(array('tags', 'id' => $model->id));
			} else {
				Yii::app()->user->setFlash('error', 'Error adding CMS Tag.');
			}
		}

		$this->controller->render('tagsForm',array(
			'model'=>$model
		));
	}

    protected function performAjaxRequest(CmsTags $model) {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['CmsTags'])) {
                $model->attributes = $_POST["CmsTags"];

                $tagUrl = trim(strtolower($model->name));
                $tagUrl = preg_replace("/[^a-zA-Z0-9-\s]+/", "", $tagUrl);
                $tagUrl = str_replace(" ", "-", $tagUrl);
                $tagUrl = preg_replace('/-{2,}/', '-', $tagUrl);
                $model->url = $tagUrl;

                if (!$model->save()) {
                    echo CActiveForm::validate($model);
                } else {
                    echo CJSON::encode(array(
                            'id'=>$model->id,
                            'name'=>$model->name,
                        ));
                }
            }

            Yii::app()->end();
        }
    }
}