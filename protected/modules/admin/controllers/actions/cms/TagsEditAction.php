<?php

class TagsEditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'CMS Tags List';

		$model = CmsTags::model()->findByPk($id);

		if (isset($_POST['CmsTags'])) {
			$model->attributes = $_POST['CmsTags'];

            if(empty($model->url)) {
                $rawUrl = $model->name;
            } else {
                $rawUrl = $model->url;
            }

            $tagUrl = trim(strtolower($rawUrl));
            $tagUrl = preg_replace("/[^a-zA-Z0-9-\s]+/", "", $tagUrl);
            $tagUrl = str_replace(" ", "-", $tagUrl);
            $tagUrl = preg_replace('/-{2,}/', '-', $tagUrl);
            $model->url = $tagUrl;

			if($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully updated CMS Tag.');
				$this->controller->redirect(array('tags', 'id' => $model->id));
			} else {
				Yii::app()->user->setFlash('error', 'Error updating CMS Tag.');
			}
		}

		$this->controller->render('tagsForm',array(
			'model'=>$model
		));
	}
}