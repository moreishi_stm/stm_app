<?php

class AssignMenuPositionAction extends CAction {
	
    public function run() {
		header('Content-type: application/json');
		
		if(!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Invalid request."
			));
			Yii::app()->end();
		}
		
		if(!isset($_POST['domain_id']) || empty($_POST['domain_id'])) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Domain ID not found."
			));
			Yii::app()->end();
		}
		
		$domain = Domains::model()->findByAttributes(array('id' => $_POST['domain_id']));
		if(empty($domain)) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "The selected domain could not be found."
			));
			Yii::app()->end();
		}
		
		if(!isset($_POST['menu_id'])) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu ID not found."
			));
			Yii::app()->end();
		}
		
		if(!isset($_POST['position_id']) || empty($_POST['position_id'])) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu Position ID not found."
			));
			Yii::app()->end();
		}

		$menuPosition = CmsMenuPositions::model()->findByAttributes(array('id' => $_POST['position_id']));
		
		if(empty($menuPosition)) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "The selected menu position could not be found."
			));
			Yii::app()->end();
		}
		
		if(empty($_POST['menu_id'])) {
			$menu = CmsDomainMenus::model()->findByAttributes(array('domain_id' => $_POST['domain_id'], 'menu_position_id' => $_POST['position_id']));
			
			if(empty($menu)) {			
				echo CJSON::encode(array(
					'results' => false,
					'message' => "Could not unassign the menu."
				));
				Yii::app()->end();
			}
			
			if(!$menu->delete()) {
				echo CJSON::encode(array(
					'results' => false,
					'message' => "Could not delete the menu."
				));
				Yii::app()->end();
			}
			
			echo CJSON::encode(array('results' => true, 'deleted' => true));
			Yii::app()->end();
		}
		$menu = CmsMenus::model()->findByAttributes(array('id' => $_POST['menu_id']));
		
		if(empty($menu)) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "The selected menu could not be found."
			));
			Yii::app()->end();
		}
		
		$isNew = false;
		$domainMenu = CmsDomainMenus::model()->findByAttributes(array('menu_position_id' => $_POST['position_id'], 'domain_id' => $_POST['domain_id']));
		if(empty($domainMenu)) {
			$isNew = true;
			$domainMenu = new CmsDomainMenus();
		}
		
		$domainMenu->setAttribute('menu_position_id', $_POST['position_id']);
		$domainMenu->setAttribute('domain_id', $_POST['domain_id']);
		$domainMenu->setAttribute('menu_id', $_POST['menu_id']);
		$domainMenu->setAttribute('updated_by', Yii::app()->user->id);
		$domainMenu->setAttribute('updated', date("Y-m-d H:i:s", time()));
		
		if($isNew) {
			$domainMenu->setAttribute('added', date("Y-m-d H:i:s", time()));
			$domainMenu->setAttribute('added_by', Yii::app()->user->id);
		}
		
		if ($domainMenu->save()) {			
			echo CJSON::encode(array(
				'results' => true,
				'menuName' => $menu->name,
				'id' => $domainMenu->id
			));
			Yii::app()->end();
		} else {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "There has been an error saving the menu. Please refresh the page and try again."
			));
			Yii::app()->end();
		}
    }
}