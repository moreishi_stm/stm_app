<?php

abstract class EditAction extends CAction
{

    const STATUS_CHANGED = 'changed';

    /**
     * Determines the type of cms page we are creating
     * 1 = Page
     * 2 = Blog
     * 3 = Storyboard
     *
     * @var
     */
    public $pageType;

    /**
     * The string template type from CmsTemplates
     *
     * @var
     */
    public $templateType;

    public function run($id, $template = null, $status = null)
    {
        $this->controller->title = 'Edit Web Page';
        // Find the contents
        $model = CmsContents::model()->findByPk($id);

        if ($status === self::STATUS_CHANGED) {
            $model->save();
        }

        // Initialize the template and gather all of the .stmcms css class fields
        $template             = ($model->template_name && !$template) ? $model->template_name : $template;
        $Template             = new CmsTemplates($template, $model->type_ma, $id); // @todo: this needs to NOT save media
        $model->template_name = $template;

        // Process just the page detail updates
        if (isset($_POST['CmsContents'])) {
            if(isset($_POST['CmsContents']['title'])) {
                $_POST['CmsContents']['title'] = preg_replace("/[^a-zA-Z0-9-\s]+/", "", $_POST['CmsContents']['title']);
                $_POST['CmsContents']['title'] = preg_replace('/\s{2,}/', ' ', $_POST['CmsContents']['title']);
            }
            if(isset($_POST['CmsContents']['title'])) {
                $_POST['CmsContents']['url'] = preg_replace("/[^a-zA-Z0-9-\s]+/", "", $_POST['CmsContents']['url']);
                $_POST['CmsContents']['url'] = preg_replace('/\-{2,}/', '-', $_POST['CmsContents']['url']);
            }

            $model->attributes = $_POST['CmsContents'];

            /** Handle tag deletions */
            if (empty($_POST['CmsContents']['cmsTagsToAdd'])) {
                CmsContentTagLu::model()->deleteAllByAttributes(
                    array('cms_content_id' => $this->id,)
                );
            } else {
                foreach ($model->cmsContentTagLu as $contentTagLu) {
                    $tagHasBeenRemoved = !in_array($contentTagLu->cms_tag_id, $_POST['CmsContents']['cmsTagsToAdd']);
                    if ($tagHasBeenRemoved) {
                        CmsContentTagLu::model()->deleteByPk($contentTagLu->id);
                    }
                }
            }

            // Process any cms field modifications
            if (isset($_POST['CmsTemplateField'])) {

                $Template->cmsContentId = $model->id;
                $loadedFieldData        = $Template->loadFields($_POST['CmsTemplateField'], $saveMedia=true);

                // Pull existing data
                $mdlCmsContentValues = new CmsContentValues();
                $records = $mdlCmsContentValues->findAllByAttributes(array(
                    'cms_content_id'    =>  $model->id
                ));

                // Model data for easy lookup
                $cmsContentvalues = array();
                foreach($records as $record) {
                    $cmsContentvalues[$record->element_id] = $record;
                }

                // We may need S3
                $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

                // Save new / update existing values
                foreach($loadedFieldData as $elementId => $loadedFieldDataItem) {

                    // Update existing
                    if(array_key_exists($elementId, $cmsContentvalues)) {
                        $contentValue = $cmsContentvalues[$elementId];
                        $contentValue->value = $loadedFieldDataItem;
                    }

                    // Create new
                    else {
                        $contentValue = new CmsContentValues();
                        $contentValue->cms_content_id = $model->id;
                        $contentValue->element_id = $elementId;
                        $contentValue->value = $loadedFieldDataItem;
                        $contentValue->added = new CDbExpression('NOW()');
                        $contentValue->added_by = Yii::app()->user->id;
                    }

                    // Set updated value
                    $contentValue->updated = new CDbExpression('NOW()');
                    $contentValue->updated_by = Yii::app()->user->id;

                    // Process file upload if we had one
                    if(isset($_FILES['CmsTemplateField']['tmp_name'][$elementId]) && in_array($elementId, array('photo-1', 'photo-2', 'photo-3', 'photo-4'))) {

                        // Security check
                        if(file_exists($_FILES['CmsTemplateField']['tmp_name'][$elementId]) && !is_uploaded_file($_FILES['CmsTemplateField']['tmp_name'][$elementId])) {
                            throw new Exception('Unauthorized file upload detected');
                        }

                        // If we had no errors, stash the item away in S3
                        if($_FILES['CmsTemplateField']['error'][$elementId] === UPLOAD_ERR_OK) {

                            // Use finfo to determine MIME type and get the extension
                            $finfo = finfo_open(FILEINFO_MIME_TYPE);
                            $ext = explode('.', $_FILES['CmsTemplateField']['name'][$elementId]);

                            // Generate S3 key
                            $key = 'site-files/' . Yii::app()->user->clientId . '/cms/' . $contentValue->cms_content_id . '/' . $elementId . '.' . $ext[count($ext) - 1];

                            // Store the file as an object in S3
                            $s3client->putObject(array(
                                'Bucket'        =>  YII_DEBUG ? 'dev.sites.seizethemarket.com' : 'sites.seizethemarket.com',
                                'Key'           =>  $key,
                                'SourceFile'    =>  $_FILES['CmsTemplateField']['tmp_name'][$elementId],
                                'ContentType'   =>  finfo_file($finfo, $_FILES['CmsTemplateField']['tmp_name'][$elementId]),
                                'CacheControl'  =>  'max-age=10'
                            ));

                            // Store the content value
                            $contentValue->value = 'http://' . (YII_DEBUG ? 'dev.sites.seizethemarket.com' : 'sites.seizethemarket.com') . '/' . $key;
                        }
                    }

                    // Save content value
                    if(!$contentValue->save()) {
                        throw new Exception('Error, unable to save CMS content value! "' . print_r($contentValue->getErrors(), true) . '""');
                    }
                }

                // Save the template data parsed
                $jsonFieldData        = CJSON::encode($loadedFieldData);
            }

            if (!$model->save()) {
                $errors = $model->getErrors();
                $error = current($errors);
                Yii::app()->user->setFlash('error', 'Error: '.$error[0]);
            }
        }
        else {
            $cmsTagLus = $model->cmsContentTagLu;
            $cmsTags = array();
            foreach($cmsTagLus as $cmsTagLu) {
                $cmsTags[] = $cmsTagLu->cms_tag_id;
            }
            $model->cmsTagsToAdd = $cmsTags;
        }

        $this->getController()->pageTitle = 'Edit ' . ucwords($this->templateType) . ': ' . $model->title;

        $this->render($model, $Template);
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param CmsContents  $cmsContentEntry
     * @param CmsTemplates $template
     *
     * @return mixed
     */
    abstract protected function processPostSaveOperations(CmsContents $cmsContentEntry);

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param CmsContents  $cmsContentEntry
     * @param CmsTemplates $template
     *
     * @return mixed
     */
    abstract protected function render(CmsContents $cmsContentEntry, CmsTemplates $template);
}