<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class ListCommentsAction extends CAction
{

    public function run()
    {

        $cmsCommentEntry = new CmsComment;
        $this->getController()->pageTitle = 'Viewing All Comments';

        $this->getController()->render(
            'listComments', array('model' => $cmsCommentEntry)
        );
    }
}