<?php

class ListLandingPagesAction extends CAction
{
    public $templateType;

    public function run()
    {
        $this->controller->title = 'Landing Pages';

        $model=new CmsContents;
        $model->unsetAttributes();
        $model->type_ma = CmsContents::TYPE_LANDING_PAGES;

        if (isset($_GET['CmsContents'])) {
            $model->attributes = $_GET['CmsContents'];
        }

        $isMaxed = $this->isMaxed();
        $this->controller->render('listLandingPages', array(
                'DataProvider' => $model->search(),
                'isMaxed' => $isMaxed,
            )
        );
    }

    protected function isMaxed() {
        $cmsContents = new CmsContents();
        $currentLandingPageCount = $cmsContents->countByAttributes(array('type_ma'=>CmsContents::TYPE_LANDING_PAGES));
        if($currentLandingPageCount <= Yii::app()->user->settings->max_landing_pages) {
            return false;
        } else {
            return true;
        }
    }
}