<?php

class SaveMenuLinksAction extends CAction {

    public function run() {
		header('Content-type: application/json');

		if(!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Invalid request."
			));
			Yii::app()->end();
		}

		if(!isset($_POST['data']) || empty($_POST['data'])) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu Links not found."
			));
			Yii::app()->end();
		}

		if(!isset($_POST['menu_id']) || empty($_POST['menu_id'])) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu ID not found."
			));
			Yii::app()->end();
		}

		$menu = CmsMenus::model()->findByAttributes(array('id' => $_POST['menu_id']));

		if(empty($menu)) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "The selected menu could not be found."
			));
			Yii::app()->end();
		}




		 /** Array
			(
				[data] => Array
					(
						[0] => {"id":"181","children":[{"id":"244","name":"MTA2NSBSaWRnZXdvb2QgTGFuZSAzMjA4Ng==","page_id":244}],"name":"MTA1NTAgQmF5bWVhZG93cyBSb2FkIDYyMSAzMjI1Ng==","page_id":181}
						[1] => {"id":"191","children":[{"id":"147","name":"MTA5NjEgQnVybnQgTWlsbCAxMjI3IDMyMjU2","page_id":147}],"name":"MTA4IENhcmRlbiBQbGFjZSAzMjI1OQ==","page_id":191}
					)

				[menu_id] => 6
			)

		die("<pre>".print_r(urldecode(base64_decode($_POST['items'][1]['name'])),1)); -> 10550 Baymeadows Road 621 32256
		*/

		$items = array();

		foreach($_POST['data'] as $item) {
			$items[] = json_decode($item);
		}

		if(empty($items)) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu Links not found."
			));
			Yii::app()->end();
		}

		$cmsMenuLinks = new CmsMenuLinks();
		$Criteria =  new CDbCriteria();
		$Criteria->condition = 'cms_menu_id = :cms_menu_id';
		$Criteria->params = array(
			':cms_menu_id' => $_POST['menu_id']
		);

		$menuLink = CHtml::listData($cmsMenuLinks->findAll($Criteria), 'id','id');

		$savedLinks = array();
		foreach($items as $order => $link) {
			$this->_buildLink($order, $link, NULL, $savedLinks);
		}

		$linkIdsSaved = array();
		foreach($savedLinks as $savedLink) {
			if($savedLink['result']) {
				if($savedLink['id']) {
					$linkIdsSaved[] = $savedLink['id'];
				}
			}
		}

		$linksToDelete = array_diff($menuLink, $linkIdsSaved);

		$deleteCondition =  new CDbCriteria();
		$deleteCondition->addInCondition('id', $linksToDelete);

		CmsMenuLinks::model()->deleteAll($deleteCondition);

		echo CJSON::encode(array(
			'results' => true,
			'menuId' => $_POST['menu_id'],
			'links' => $savedLinks
		));
		Yii::app()->end();
    }

	protected function _buildLink($order, $link, $parentID = NULL, &$savedLinks) {
		$isNew = false;

		if(isset($link->link_id) && !empty($link->link_id)) {
			$menuLink = CmsMenuLinks::model()->findByAttributes(array('id' => $link->link_id));

			if(empty($menuLink)) {
				$savedLinks[] = array(
					'result' => false,
					'title' => $menuLink->title,
					'url' => $menuLink->url,
					'message' => "Could not find Menu Link with ID: ".$link->link_id
				);

				return;
			}
		} else {
			$menuLink = new CmsMenuLinks();
			$isNew = true;
		}

		if(isset($link->custom) && !empty($link->custom)) {
			$linkData = json_decode(base64_decode($link->custom));

			/*
			 * $testCustom = json_decode(base64_decode($items[2]->custom));
			 * die("<pre>".print_r($testCustom, 1));
			 * stdClass Object (
					[url] => http://www.google.com
					[title] => google.com
				)
			 */


			$menuLink->setAttribute('name', $linkData->title);
			$menuLink->setAttribute('title',$linkData->title);
			$menuLink->setAttribute('url', $linkData->url);
		} else {
            switch($link->entity_type) {
                case 'featured_area':
                    $featuredArea = FeaturedAreas::model()->findByPk($link->page_id);
                    $url = '/area/'.$featuredArea->url;
                    break;

                default:
                    $cmsPage = CmsContents::model()->findByPk($link->page_id);
                    $url = $cmsPage->url;
                    break;
            }

			if($isNew && empty($url)) {
				$savedLinks[] = array(
					'result' => false,
					'title' => $menuLink->title,
					'url' => $menuLink->url,
					'message' => "CMS or Featured Area Page for ID '".$link->page_id."' could not be found. Some links may have been saved. Please refresh the page and try again."
				);

				return;
			}

			$menuLink->setAttribute('name',  base64_decode($link->name));
			$menuLink->setAttribute('title',  base64_decode($link->name));

			if($isNew) {
				$menuLink->setAttribute('url', $url);
			} else {
				$menuLink->setAttribute('url',  $menuLink->url);
			}
		}

		if($isNew) {
			$menuLink->setAttribute('added', date("Y-m-d H:i:s", time()));
			$menuLink->setAttribute('added_by', Yii::app()->user->id);
			$menuLink->setAttribute('entity_type', $link->entity_type);
			$menuLink->setAttribute('entity_id', $link->entity_id);
		} else {
			$menuLink->setAttribute('entity_type', $menuLink->entity_type);
			$menuLink->setAttribute('entity_id', $menuLink->entity_id);
		}

		$menuLink->setAttribute('order',$order);
		$menuLink->setAttribute('parent_id', $parentID);
		$menuLink->setAttribute('append_to_parent_url', 1);
		$menuLink->setAttribute('css_classes', NULL);
		$menuLink->setAttribute('cms_menu_id', $_POST['menu_id']);
		$menuLink->setAttribute('updated_by', Yii::app()->user->id);
		$menuLink->setAttribute('updated', date("Y-m-d H:i:s", time()));

		if (!$menuLink->save()) {
			$savedLinks[] = array(
				'result' => false,
				'title' => $menuLink->title,
				'url' => $menuLink->url,
				'errors' => $menuLink->getErrors(),
			);

			return;
		}

		$savedLinks[] = array(
			'result' => true,
			'id' => $menuLink->id,
			'selector' => $link->selector
		);

		if(isset($link->children) && !empty($link->children)) {
			foreach($link->children as $childOrder => $child) {
				$this->_buildLink($childOrder, $child, $menuLink->id, $savedLinks);
			}
		}

		return;
	}
}