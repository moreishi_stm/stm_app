<?php

abstract class AddAction extends CAction
{

    /**
     * Determines the type of cms page we are creating
     * 1 = Page
     * 2 = Blog
     * 3 = Storyboard
     *
     * @var
     */
    public $pageType;

    /**
     * The string template type from CmsTemplates
     *
     * @var
     */
    public $templateType;

    /**
     * Manages models
     */
    public function run($domainId, $template = null)
    {
        $this->init();
        $this->controller->title = 'Add New Page';
        if (empty($this->pageType)) {
            throw new CHttpException(500, 'Page type not found.');
        }

        $model          = new CmsContents('create');
        $model->type_ma = $this->pageType;
        $model->status_ma = CmsContents::STATUS_ACTIVE;
        $model          = $this->preloadAttributes($model);

        $defaultTemplate      = ($template)? $template :'standard';
        $CmsTemplate             = new CmsTemplates($defaultTemplate, $model->type_ma);
        $model->template_name = $defaultTemplate;

        if (isset($_POST['CmsContents'])) {
            $_POST['CmsContents']['title'] = preg_replace("/[^a-zA-Z0-9-\s]+/", "", $_POST['CmsContents']['title']);
            $_POST['CmsContents']['title'] = preg_replace('/\s{2,}/', ' ', $_POST['CmsContents']['title']);

            $_POST['CmsContents']['url'] = preg_replace("/[^a-zA-Z0-9-\s]+/", "", $_POST['CmsContents']['url']);
            $_POST['CmsContents']['url'] = str_replace(" ", "-", $_POST['CmsContents']['url']);
            $_POST['CmsContents']['url'] = preg_replace('/-{2,}/', '-', $_POST['CmsContents']['url']);

            $model->attributes = $_POST['CmsContents'];

            // remove any content tags that have been removed via the gui
            //@todo: is this relevant since cms is not created yet?
            foreach ($model->cmsContentTagLu as $contentTagLu) {
                $tagHasBeenRemoved = !in_array($contentTagLu->cms_tag_id, $_POST['CmsContents']['cmsTagsToAdd']);
                if ($tagHasBeenRemoved) {
                    CmsContentTagLu::model()->deleteByPk($contentTagLu->id); //@todo: is this relevant since cms is not created yet?
                }
            }

            #die("<pre>".print_r(array($_POST, $_FILES), 1)."</pre>");

            if ($model->save()) {

                // process the content modifications
                $CmsTemplate->cmsContentId = $model->id;
                $loadedFieldData        = $CmsTemplate->loadFields($_POST['CmsTemplateField']);
                $jsonFieldData          = CJSON::encode($loadedFieldData);
                $model->save();

                // Save new / update existing values
                foreach($loadedFieldData as $elementId => $loadedFieldDataItem) {

                    $contentValue = new CmsContentValues();
                    $contentValue->value = $loadedFieldDataItem;
                    $contentValue->cms_content_id = $model->id;
                    $contentValue->element_id = $elementId;
                    $contentValue->added = new CDbExpression('NOW()');
                    $contentValue->added_by = Yii::app()->user->id;
                    $contentValue->updated = new CDbExpression('NOW()');
                    $contentValue->updated_by = Yii::app()->user->id;

                    // Save content value
                    if(!$contentValue->save()) {
                        throw new Exception('Error, unable to save CMS content value! "' . print_r($contentValue->getErrors(), true) . '""');
                    }

                    if(isset($_FILES['CmsTemplateField']['tmp_name'][$elementId]) && in_array($elementId, array('photo-1', 'photo-2', 'photo-3', 'photo-4'))) {

                        $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());
                        // Security check
                        if(file_exists($_FILES['CmsTemplateField']['tmp_name'][$elementId]) && !is_uploaded_file($_FILES['CmsTemplateField']['tmp_name'][$elementId])) {
                            throw new Exception('Unauthorized file upload detected');
                        }

                        // If we had no errors, stash the item away in S3
                        if($_FILES['CmsTemplateField']['error'][$elementId] === UPLOAD_ERR_OK) {

                            // Use finfo to determine MIME type and get the extension
                            $finfo = finfo_open(FILEINFO_MIME_TYPE);
                            $ext = explode('.', $_FILES['CmsTemplateField']['name'][$elementId]);

                            // Generate S3 key
                            $key = 'site-files/' . Yii::app()->user->clientId . '/cms/' . $contentValue->cms_content_id . '/' . $elementId . '.' . $ext[count($ext) - 1];

                            // Store the file as an object in S3
                            $s3client->putObject(array(
                                'Bucket'        =>  YII_DEBUG ? 'dev.sites.seizethemarket.com' : 'sites.seizethemarket.com',
                                'Key'           =>  $key,
                                'SourceFile'    =>  $_FILES['CmsTemplateField']['tmp_name'][$elementId],
                                'ContentType'   =>  finfo_file($finfo, $_FILES['CmsTemplateField']['tmp_name'][$elementId]),
                                'CacheControl'  =>  'max-age=10'
                            ));

                            // Store the content value
                            $contentValue->value = 'http://' . (YII_DEBUG ? 'dev.sites.seizethemarket.com' : 'sites.seizethemarket.com') . '/' . $key;
                        }
                    }

                    // Save content value
                    if(!$contentValue->save()) {
                        throw new Exception('Error, unable to save CMS content value! "' . print_r($contentValue->getErrors(), true) . '""');
                    }
                }

                Yii::app()->user->setFlash('success', 'Successfully Added New Page!');
                $this->processPostSaveOperations($model, $CmsTemplate);
            } else {
                Yii::app()->user->setFlash('error', 'Error with saving the page.');
            }
        }

        $this->getController()->pageTitle = 'Add New ' . ucwords($this->templateType);
        $this->render($model, $CmsTemplate);
    }

    /**
     * Performs any initial functions needed
     *
     * @return void
     */
    protected function init()
    {
        return;
    }

    /**
     * Preloads cms content related attributes before processing occurs.  The parent implementation must be called.
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param CmsContents $cmsContentEntry
     *
     * @return CmsContents
     */
    protected function preloadAttributes(CmsContents $cmsContentEntry)
    {

        return $cmsContentEntry;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param CmsContents  $cmsContentEntry
     * @param CmsTemplates $template
     *
     * @return mixed
     */
    abstract protected function processPostSaveOperations(CmsContents $cmsContentEntry);

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param CmsContents  $cmsContentEntry
     * @param CmsTemplates $template
     *
     * @return mixed
     */
    abstract protected function render(CmsContents $cmsContentEntry, CmsTemplates $template);

}