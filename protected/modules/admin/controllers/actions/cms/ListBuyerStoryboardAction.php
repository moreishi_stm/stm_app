<?php
Yii::import('admin_module.controllers.actions.cms.ListStoryboardAction');

class ListBuyerStoryboardAction extends ListStoryboardAction
{
    protected $_view = 'listBuyerStoryboard';
    protected $_displayName = 'Buyer Storyboards';

    protected function getDataProvider() {
        $CmsContentModel=new CmsContents;
        $CmsContentModel->unsetAttributes();

        $CmsContentModel->type_ma = $this->pageType;
        $CmsContentModel->domain_id = Yii::app()->user->domain->id;
        return $CmsContentModel->search();
    }
    protected function getView() {
        return $this->_view;
    }

    protected function getDisplayName() {
        return $this->_displayName;
    }
}