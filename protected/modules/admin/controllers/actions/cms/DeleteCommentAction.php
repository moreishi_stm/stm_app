<?php

/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 * Class DeleteCommentAction
 */
class DeleteCommentAction extends CAction
{

    /**
     * Manages models
     */
    public function run($id)
    {
        $cmsCommentEntry = CmsComment::model()->findByPk($id);
        if (empty($cmsCommentEntry)) {
            throw new CHttpException(500, 'Could not locate comment entry to remove comment');
        }

        $this->processAjaxRequest($cmsCommentEntry);
    }

    /**
     * @param CmsContents $cmsContentEntry
     */
    protected function processAjaxRequest(CmsComment &$cmsContentEntry)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            $entryRemoved = $cmsContentEntry->delete();
            echo CJSON::encode(array('success' => $entryRemoved));
            Yii::app()->end();
        }
    }
}