<?php

class ContentAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id, $template)
	{
		$model = new CmsContent;
		$model = $model->findByPk($id);

		$Template = new CmsTemplates($template);

		if (isset($_POST['submit'])) {

			$contentHtml = $Template->loadContent($_POST);
			$model->content_public = $contentHtml;
			$model->updated = date('Y-m-d H:i:s');
			$model->updated_by = Yii::app()->user->id;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Updated Page Content!');
				$this->controller->redirect(array('edit','id'=>$model->id));
			}
		}

		$this->controller->title = 'Edit Content';
		$this->controller->render('content',array(
			'model'    =>$model,
			'Template' =>$Template,
		));
	}
}