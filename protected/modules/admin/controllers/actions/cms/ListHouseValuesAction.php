<?php

class ListHouseValuesAction extends CAction
{
    public $templateType;

    public function run()
    {
        $this->controller->title = 'House Values Site';

        $model=new CmsContents;
        $model->unsetAttributes();
        $model->type_ma = CmsContents::TYPE_HOUSE_VALUES;

        if (isset($_GET['CmsContents'])) {
            $model->attributes = $_GET['CmsContents'];
        }

        $isMaxed = $this->isMaxed();
        $this->controller->render('listHouseValues', array(
                'DataProvider' => $model->search(),
                'isMaxed' => $isMaxed,
            )
        );
    }

    protected function isMaxed() {
        $cmsContents = new CmsContents();
        $currentHouseValuesSiteCount = $cmsContents->countByAttributes(array('type_ma'=>CmsContents::TYPE_HOUSE_VALUES));
        if($currentHouseValuesSiteCount <= Yii::app()->user->settings->max_house_values_sites) {
            return false;
        } else {
            return true;
        }
    }
}