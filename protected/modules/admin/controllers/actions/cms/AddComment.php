<?php
Yii::import('front_widgets.PageViewTrackerWidget.PageViewTrackerWidget');
Yii::import('front_widgets.RecentSavedHomesWidget.RecentSavedHomesWidget');
/**
 * Class AddComment
 */
class AddComment extends CAction
{

    public function run($cmsContentId)
    {

        $cmsCommentEntry = new CmsComment;
        if (isset($_POST['CmsComment'])) {
            $cmsCommentEntry->attributes = $_POST['CmsComment'];
        }

        $cmsCommentEntry->cms_content_id = $cmsContentId;

        if (!Yii::app()->user->getIsGuest()) {
            $cmsCommentEntry->contact_id = Yii::app()->user->id;
        } else {

            if($this->isResubmit($cmsCommentEntry)) {
                //@todo: what about re-submits???***********

            } else {
                /**
                 * Create the new contact from the blog post and log them in
                 */
                $cmsCommentEntry->scenario = 'addFromWidget';
                $contact = new Contacts('blog-insert');
                $contact->first_name = $cmsCommentEntry->first_name;
                $contact->last_name = $cmsCommentEntry->last_name;
//                $contact->source_id = $cmsCommentEntry->last_name; //@todo: figure this out - CLee
                $contact->account_id = Yii::app()->user->getAccountId();
                if(!$contact->save()) {
                    //@todo: error log?
                } else {
                    Contacts::updateLastLogin($contact);
                }

                $email = new Emails;
                $email->contact_id = $contact->id;
                $email->email = $cmsCommentEntry->email;
                $email->is_primary = 1;
                if($email->save()) {
                    //@todo: error log?
                }

                /**
                 * Create the welcome email
                 */
                $emailMessage = new EmailMessage;
                $emailMessage->subject = 'Your New Account Info!'; //@todo: make this a blog comment welcome email
                $emailMessage->component_type_id = ComponentTypes::CONTACTS;
                $emailMessage->component_id = $contact->id;
                $emailMessage->to_email_id = $contact->getPrimaryEmailObj()->id;
                $emailMessage->from_email_id = $email->id;
                if ($emailMessage->save()) {
                    $message = new StmMailMessage;
                    $message->view = 'welcomeEmail'; //@todo: need to do a welcomeBlog email
                    $message->init($emailMessage);

                    if(!YII_DEBUG) {
                        Yii::app()->mail->send($message);
                    }
                }

                /**
                 * Automatically login the new user
                 */
                $loginForm = new LoginForm();
                $loginForm->email = $email->email;
                $loginForm->password = $contact->password;
                if (($loginForm->validate() && $loginForm->login())) {
                    PageViewTrackerWidget::processLogin();
                    RecentSavedHomesWidget::processLogin();
                    Contacts::updateLastLogin($contact);
                    LoginLog::processLogin();
                }

                //@todo: Add Activity Log for commenting on blog.

                $cmsCommentEntry->contact_id = $contact->id;
            }

        }

        $this->processAjaxRequest($cmsCommentEntry);
    }

    protected function isResubmit(CmsComment $cmsCommentEntry) {
        return false;
    }

    /**
     * @param CmsComment $cmsCommentEntry
     */
    private function processAjaxRequest(CmsComment $cmsCommentEntry)
    {
        if (!$cmsCommentEntry->validate()) {
            echo CActiveForm::validate($cmsCommentEntry);
            Yii::app()->end();
        }

        $cmsCommentEntry->save();
        Yii::app()->end();
    }
}