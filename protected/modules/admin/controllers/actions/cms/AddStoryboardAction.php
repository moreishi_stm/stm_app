<?php
Yii::import('admin_module.controllers.actions.cms.AddAction');

/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class AddStoryboardAction extends AddAction
{

    /**
     * @var Transactions $_transaction stores the transaction
     */
    private $_transaction;

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null|string $transactionId
     */
    public function run($transactionId)
    {
        $this->controller->title = 'Add Listing Storyboard';

        $this->_transaction = Transactions::model()->findByPk($transactionId);
        if (empty($this->_transaction)) {
            throw new CHttpException(500, 'Cannot add a storyboard without a valid transaction.');
        }

        $domainId = Yii::app()->user->domain->id;
        parent::run($domainId);
    }

    protected function preloadAttributes(CmsContents $cmsContentEntry)
    {
        if($this->pageType == CmsContents::TYPE_LISTING_SB) {
            $address = $this->getTransaction()->address;
            if (!$address instanceof Addresses) {
                throw new CHttpException(500, 'Sorry, you cannot create a storyboard without a valid address.');
            }

            $titleFormat = ':streetNum :street :zipCode';
            $title       = strtr(
                $titleFormat, array(':streetNum' => $address->getStreetNumber(),
                                    ':street'    => $address->getStreet(),
                                    ':zipCode'   => $address->zip)
            );
        }

        $url = preg_replace("/[^a-zA-Z0-9\s]+/", "", $title);
        $url = preg_replace('/\s{1,}/', '-', $url);
        $url = str_replace(' ', '-', strtolower($url));

        if(CmsContents::model()->findByAttributes(array('url'=>$url))) {
            $url .= '_2';
        }

        $cmsContentEntry->setAttributes(
            array('template_name' => 'standard',
                  'title'         => $title,
                  'url'           => $url,)
        );

        return parent::preloadAttributes($cmsContentEntry);
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return Transactions
     */
    protected function getTransaction()
    {

        return $this->_transaction;
    }

    /**
     * @param CmsContents  $model
     * @param CmsTemplates $Template
     */
    protected function render(CmsContents $model, CmsTemplates $template)
    {

        $this->controller->render(
            'add', array('model'    => $model,
                         'Template' => $template,)
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param CmsContents $cmsContentEntry
     *
     * @return CmsContents
     */
    protected function processPostSaveOperations(CmsContents $cmsContentEntry)
    {

        $transactionCmsLu = new TransactionCmsLu;
        $transactionCmsLu->setAttributes(
            array('cms_id'         => $cmsContentEntry->id,
                  'transaction_id' => $this->_transaction->id)
        );
        if (!$transactionCmsLu->save()) {
            Yii::app()->user->setFlash('error', 'Page did not link to a transaciton.');
        }

        if ($cmsContentEntry->enableClientAccess) {
            $cmsContentContactLu = new CmsContentContactLu;
            $cmsContentContactLu->setAttributes(
                array('cms_content_id' => $cmsContentEntry->id,
                      'contact_id'     => $this->_transaction->contact->id,)
            );
            if (!$cmsContentContactLu->save()) {
                Yii::app()->user->setFlash('error', 'Error: Client access was not enabled.');
            }
        }

        $this->controller->redirect(
            array("edit" . ucwords($this->templateType),
                  'id' => $cmsContentEntry->id)
        );
    }
}