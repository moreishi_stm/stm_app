<?php

class TagsListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'CMS Tags List';

		$model=new CmsTags('search');
		$model->unsetAttributes();  // clear any default values

		if (isset($_GET['CmsTags'])) {
			$model->attributes = $_GET['CmsTags'];
		}

		$this->controller->render('tags',array(
			'model'=>$model
		));
	}
}