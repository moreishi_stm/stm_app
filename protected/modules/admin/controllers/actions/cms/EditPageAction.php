<?php
Yii::import('admin_module.controllers.actions.cms.EditAction');
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class EditPageAction extends EditAction
{

    /**
     * @param CmsContents  $model
     * @param CmsTemplates $Template
     */
    protected function render(CmsContents $cmsContentEntry, CmsTemplates $template)
    {

        $commentDataProvider
            = new CActiveDataProvider('CmsComment', array('criteria' => array('condition' => 'cms_content_id = :cms_content_id',
                                                                              'params'    => array(':cms_content_id' => $cmsContentEntry->id),),));

        $this->controller->render(
            'edit', array('model'               => $cmsContentEntry,
                          'Template'            => $template,
                          'viewPageLink'        => $cmsContentEntry->url,
                          'commentDataProvider' => $commentDataProvider)
        );
    }

    /**
     * @param CmsContents  $model
     * @param CmsTemplates $template
     */
    protected function processPostSaveOperations(CmsContents $cmsContentEntry)
    {

        $this->controller->redirect(
            array("edit" . ucwords($this->templateType),
                  'id' => $cmsContentEntry->id)
        );
    }
}