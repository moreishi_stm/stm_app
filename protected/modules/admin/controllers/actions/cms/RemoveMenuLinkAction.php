<?php

class RemoveMenuLinkAction extends CAction {

    public function run() {
		header('Content-type: application/json');

		if(!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Invalid request."
			));
			Yii::app()->end();
		}

		if(!isset($_POST['linkId']) || empty($_POST['linkId'])) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu Link not found."
			));
			Yii::app()->end();
		}

		$menuLink = CmsMenuLinks::model()->findByPk($_POST['linkId']);

		if(empty($menuLink)) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu Link not found."
			));
			Yii::app()->end();
		}

		$Criteria = new CDbCriteria();
		$Criteria->select='id,parent_id';
		$Criteria->condition = "parent_id = :parent_id";
		$Criteria->params = array(
			':parent_id'    => $_POST['linkId'],
		);
		$childLinks = CmsMenuLinks::model()->find($Criteria);

		if(!empty($childLinks)) {
			$updated = Yii::app()->db->createCommand("UPDATE cms_menu_links SET parent_id = ".(!empty($menuLink->parent_id) ? $menuLink->parent_id : 'NULL')." WHERE parent_id = ".$menuLink->id)->execute();
			if(!$updated) {
				echo CJSON::encode(array(
					'results' => false,
					'message' => "Could not re-assign child links. The menu was not removed."
				));
				Yii::app()->end();
			}
		}

		if(!$menuLink->delete()) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Menu Link could not be removed."
			));
			Yii::app()->end();
		}

		echo CJSON::encode(array('results' => true));
		Yii::app()->end();
    }
}