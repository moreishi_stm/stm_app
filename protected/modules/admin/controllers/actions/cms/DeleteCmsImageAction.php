<?php

class DeleteCmsImageAction extends CAction
{
    /**
     * Manages models
     */
    public function run()
    {
        // Locate the record
        $cmsContentValue = CmsContentValues::model()->findByAttributes(array(
            'cms_content_id'    =>  $_GET['contentId'],
            'element_id'        =>  $_GET['elementId']
        ));

        if(!$cmsContentValue) {
            throw new Exception('Error, invalid cms content value specified');
        }

        // Safety check
        if(!in_array($cmsContentValue->element_id, array('photo-1', 'photo-2', 'photo-3', 'photo-4'))) {
            throw new Exception('Error, only photos can be deleted Invalid type detected! (' . $cmsContentValue->element_id . ')!');
        }

        // Determine key for deletion
        $key = parse_url($cmsContentValue->value);
        $key = ltrim($key['path'], '/');

        // Delete the object from S3
        /** @var Aws\S3\S3Client $s3client */
        $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());
        $s3client->deleteObject(array(
            'Bucket'    =>  YII_DEBUG ? 'dev.sites.seizethemarket.com' : 'sites.seizethemarket.com',
            'Key'       =>  $key
        ));

        // Wipe the db record
        $cmsContentValue->value = '';
        if(!$cmsContentValue->save()) {
            throw new Exception('Error, unable to update db record');
        }

        // Get us outa here
        $this->controller->redirect(array($_GET['redirect']));
        Yii::app()->end();
    }
}