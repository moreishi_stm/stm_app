<?php

class CheckMenuNameAndSaveAction extends CAction {
	
    public function run() {
		header('Content-type: application/json');
		
		if(!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "Invalid request."
			));
			Yii::app()->end();
		}
		
		if(!isset($_POST['name']) || empty($_POST['name'])) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "A name must be at least 5 characters in length. Usable Characters are: A-Za-z 0-9 - _ space"
			));
			Yii::app()->end();
		} else {
			$_POST['name'] = strip_tags($_POST['name']);
			$_POST['name'] = preg_replace('/[^a-zA-Z0-9\-\_\s]/', '', $_POST['name']);
			if(strlen($_POST['name']) < 5) {
				echo CJSON::encode(array(
					'results' => false,
					'message' => "A name must be at least 5 characters in length. Usable Characters are: A-Za-z 0-9 - _ space"
				));
				Yii::app()->end();
			}
		}
		
		if(strtolower($_POST['name']) == 'create menu') {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "The menu cannot be named 'Create Menu'"
			));
			Yii::app()->end();
		}
		
		$Criteria = new CDbCriteria();
		$Criteria->condition = "account_id = :accountID AND name = :name";
		$Criteria->params = array(
			':accountID'    => Yii::app()->user->accountId,
			':name'			=> $_POST['name']
		);

		$cmsMenu = new CmsMenus();
		$menus = $cmsMenu->find($Criteria);
		
		if(!empty($menus)) {			
			echo CJSON::encode(array(
				'results' => false,
				'message' => "A menu with this name already exists for your account."
			));
			Yii::app()->end();
		}
		
		if(!isset($_POST['css_classes'])) {
			$css_classes = NULL;
		} else {
			$css_classes = strip_tags($_POST['css_classes']);
		}
		
		$cmsMenu = new CmsMenus();
		$cmsMenu->unsetAttributes();
		$cmsMenu->setAttribute('account_id',Yii::app()->user->accountId);
		$cmsMenu->setAttribute('name',$_POST['name']);
		$cmsMenu->setAttribute('css_classes', $css_classes);
		$cmsMenu->setAttribute('added', date("Y-m-d H:i:s", time()));
		$cmsMenu->setAttribute('added_by', Yii::app()->user->id);
		
		if ($cmsMenu->save()) {			
			echo CJSON::encode(array(
				'results' => true,
				'name' => $_POST['name'],
				'id' => $cmsMenu->id
			));
			Yii::app()->end();
		} else {
			echo CJSON::encode(array(
				'results' => false,
				'message' => "There has been an error saving the menu. Please refresh the page and try again."
			));
			Yii::app()->end();
		}
    }
}