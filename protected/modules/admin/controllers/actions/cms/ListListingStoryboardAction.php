<?php
Yii::import('admin_module.controllers.actions.cms.ListStoryboardAction');

class ListListingStoryboardAction extends ListStoryboardAction
{
    protected $_view = 'listListingStoryboard';
    protected $_displayName = 'Listing Storyboards';

    protected function getDataProvider() {
        $CmsContentModel=new CmsContents;
        $CmsContentModel->unsetAttributes();

        $CmsContentModel->type_ma = $this->pageType;
//        $CmsContentModel->domain_id = Yii::app()->user->domain->id;
        $CmsContentModel->accountId = Yii::app()->user->accountId;

        return $CmsContentModel->search();
    }
    protected function getView() {
        return $this->_view;
    }

    protected function getDisplayName() {
        return $this->_displayName;
    }
}