<?php

class DomainPagesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$model = $this->controller->loadModel($id); //domains model

		$CmsContentModel=new CmsContents;
		$CmsContentModel->unsetAttributes();  // clear any default values
        $CmsContentModel->status_ma = 1;
        if(Yii::app()->controller->module->id == 'admin') {
            $CmsContentModel->type_ma = CmsContents::TYPE_PAGE;
        }

		// used for gridview search box inputs
		if (isset($_GET['CmsContents'])) {
            $CmsContentModel->attributes=$_GET['CmsContents'];
        }

		$this->controller->title = 'CMS :: '.$model->name;

		$this->controller->render('domainPages',array(
			'model'=>$model,
			'CmsContentModel'=>$CmsContentModel,
		));
	}
}