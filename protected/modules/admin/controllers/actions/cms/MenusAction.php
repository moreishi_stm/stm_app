<?php

class MenusAction extends CAction
{

    /**
     * Manages models
     */
    public function run() {
        $this->controller->title = 'CMS Menus';

		$domains = Domains::model()->findAllByAttributes(array(
			'account_id' => Yii::app()->user->account_id,
        ));

		$domainIds = array();
		$domainIdsToName = array();
		$domainAvailablePositions = array();
		$cmsDomainPositions = new CmsMenuPositions();

		$Criteria = new CDbCriteria();
		$Criteria->condition = "cms_theme_id = :cms_theme_id";
		$Criteria->order = "id ASC";

		$usableDomains = array();

		foreach($domains as $domain) {
			$domainIds[] = $domain->id;
			$domainIdsToName[$domain->id] = $domain->name;
			$Criteria->params = array(
				':cms_theme_id'    => $domain->cms_theme_id,
			);
			$domainAvailablePositions[$domain->id] = CHtml::listData($cmsDomainPositions->findAll($Criteria), 'id', 'name');
			$usableDomains[$domain->id] = $domain;
		}

		$Criteria = new CDbCriteria();
		$Criteria->condition = "account_id = :accountID";
		$Criteria->params = array(
			':accountID'    => Yii::app()->user->accountId,
		);
        $Criteria->order = "id ASC";

		$cmsMenus = new CmsMenus();
		$menus = $cmsMenus->findAll($Criteria);

		$menuLinks = array();

		foreach($menus as $menu) {
			$links = $menu->getRelated('cmsMenuLinks',true);

			if(empty($links)) {
				$menuLinks[$menu->id] = array();
				$menuLinks[$menu->id][0] = array();
				continue;
			}

			foreach($links as $link) {
				if(empty($link->parent_id)) {
                    $menuLinks[$menu->id][0][] = $link->attributes;
                } else {
					$menuLinks[$menu->id][$link->parent_id][] = $link->attributes;
				}
			}
		}

		$Criteria = new CDbCriteria();
		$Criteria->addInCondition("domain_id",$domainIds);

		$cmsDomainMenus = new CmsDomainMenus();
		$domainMenus = $cmsDomainMenus->findAll($Criteria);

		$domainMenusSorted = array();
		foreach($domainMenus as $domainMenu) {
			$domainMenusSorted[$domainMenu->domain_id][$domainMenu->menu_position_id] = $domainMenu->attributes;
		}

		$Criteria = new CDbCriteria();
		$Criteria->select = 'id, domain_id, title, url';
		$Criteria->addInCondition("domain_id",$domainIds);
		$Criteria->order = "title ASC";
		$CmsContents = new CmsContents();
		$pages = $CmsContents->findAll($Criteria);

		$cmsPagesByDomain = array();
		foreach($pages as $page) {
			$cmsPagesByDomain[$domainIdsToName[$page->domain_id]]['domain_id'] = $page->domain_id;
			$cmsPagesByDomain[$domainIdsToName[$page->domain_id]]['pages'][$page->id] = $page->attributes;
		}
		unset($pages);

		$Criteria = new CDbCriteria();
		$Criteria->select = 'id, name, title, url';
		$Criteria->addInCondition("domain_id",$domainIds);
		$Criteria->order = "title ASC";
        $featuredAreas = FeaturedAreas::model()->findAll(array('order'=>'name'));

		$featuredAreasData = array();
		if(!empty($featuredAreas)) {
			foreach($featuredAreas as $featuredArea) {
                $featuredArea->url = '/area/'.$featuredArea->url;
				$featuredAreasData[] = $featuredArea->attributes;
			}
		}

		unset($featuredAreas);

		$this->init();
		$this->controller->render("cmsMenus", array(
			'model' => new CmsMenus(),
			'menus' => $menus,
			'menuLinks' => $menuLinks,
			'domains' => $usableDomains,
			'domainMenus' => $domainMenusSorted,
			'domainAvailablePositions' => $domainAvailablePositions,
			'cmsPagesByDomain' => $cmsPagesByDomain,
            'featuredAreas' => $featuredAreasData,
		));
    }

    /**
     * Performs any initial functions needed
     *
     * @return void
     */
    protected function init()
    {
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/standard2/assets/css/font-awesome.css');
		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/jquery.mjs.nestedSortable.js', CClientScript::POS_END);
        return;
    }
}