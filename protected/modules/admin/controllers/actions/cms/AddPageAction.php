<?php
Yii::import('admin_module.controllers.actions.cms.AddAction');

/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class AddPageAction extends AddAction
{

    /**
     * @param CmsContents  $model
     * @param CmsTemplates $Template
     */
    protected function render(CmsContents $model, CmsTemplates $template)
    {

        $this->controller->render(
            'add', array('model'    => $model,
                         'Template' => $template,)
        );
    }

    protected function init()
    {
        $cmsContents = new CmsContents;

        switch($this->pageType) {
            case CmsContents::TYPE_HOUSE_VALUES:
                $count = $cmsContents->countByAttributes(array('type_ma'=>CmsContents::TYPE_HOUSE_VALUES));
                if($count >= Yii::app()->user->settings->max_house_values_sites) {
                    Yii::app()->user->setFlash('info', 'You have reached your max House Values Sites. Please contact customer service to get credits for additional sites.');
                    $this->controller->redirect(array('houseValues'));
                }
                break;

            case CmsContents::TYPE_LANDING_PAGES:
                $count = $cmsContents->countByAttributes(array('type_ma'=>CmsContents::TYPE_LANDING_PAGES));
                if($count >= Yii::app()->user->settings->max_landing_pages) {
                    Yii::app()->user->setFlash('info', 'You have reached your max Landing Pages. Please contact customer service to get credits for additional pages.');
                    $this->controller->redirect(array('landingPages'));
                }
                break;
        }
        return;
    }
    /**
     * @param CmsContents  $model
     * @param CmsTemplates $template
     */
    protected function processPostSaveOperations(CmsContents $cmsContentEntry)
    {

        $this->controller->redirect(
            array("edit" . ucwords($this->templateType),
                  'id' => $cmsContentEntry->id)
        );
    }
}