<?php
Yii::import('admin_module.controllers.actions.cms.EditAction');
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class EditHouseValuesAction extends EditAction
{

    /**
     * @param CmsContents  $model
     * @param CmsTemplates $Template
     */
    protected function render(CmsContents $cmsContentEntry, CmsTemplates $template)
    {
        $this->controller->title = 'Edit House Values Site';

        $commentDataProvider
            = new CActiveDataProvider('CmsComment', array('criteria' => array('condition' => 'cms_content_id = :cms_content_id',
                                                                              'params'    => array(':cms_content_id' => $cmsContentEntry->id),),));

        $this->controller->render(
            'edit', array('model'               => $cmsContentEntry,
                          'Template'            => $template,
                          'viewPageLink'        => 'houseValues/'.$cmsContentEntry->url,
                          'commentDataProvider' => $commentDataProvider
                    )
        );
    }

    /**
     * @param CmsContents  $model
     * @param CmsTemplates $template
     */
    protected function processPostSaveOperations(CmsContents $cmsContentEntry)
    {

        if ($cmsContentEntry->enableClientAccess && !$cmsContentEntry->isClientEnabled()) {
            $cmsContentContactLu = new CmsContentContactLu;
            $cmsContentContactLu->setAttributes(
                array('cms_content_id' => $cmsContentEntry->id,
                      'contact_id'     => $cmsContentEntry->transaction->contact->id,)
            );
            if (!$cmsContentContactLu->save()) {
                Yii::app()->user->setFlash('error', 'Error: Client access was not enabled.');
            }
        } elseif(!$cmsContentEntry->enableClientAccess) {
            CmsContentContactLu::model()->deleteAllByAttributes(
                array('cms_content_id' => $cmsContentEntry->id,)
            );
        }

        $this->controller->redirect(
            array("edit" . ucwords($this->templateType),
                  'id' => $cmsContentEntry->id)
        );
    }
}