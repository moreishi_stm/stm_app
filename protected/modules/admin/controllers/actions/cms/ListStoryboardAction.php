<?php

abstract class ListStoryboardAction extends CAction
{

    public $pageType;

    public $templateType;

    protected $_view;

    public function run()
    {

        $this->controller->title       = $this->getDisplayName();
        $this->controller->displayName = $this->getDisplayName();
        $this->controller->render(
            $this->getView(), array(
                'DataProvider' => $this->getDataProvider(),
            )
        );
    }

    abstract protected function getDisplayName();

    abstract protected function getView();

    abstract protected function getDataProvider();
}