<?php

	class AddAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$model = new SmsTemplates;
			
			$this->controller->title = 'Sms Templates List';

			if (isset($_POST['SmsTemplates'])) {
				$model->attributes = $_POST['SmsTemplates'];

				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Successfully added Sms Template.');
					$this->controller->redirect('/'.Yii::app()->controller->module->name.'/smsTemplates/' . $model->id);
				}
			}

			$this->controller->render('form', array(
					'model' => $model
				)
			);
		}
	}
