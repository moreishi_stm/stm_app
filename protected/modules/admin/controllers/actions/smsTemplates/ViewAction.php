<?php

class ViewAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id, $contactId=null) {

		$model = $this->controller->loadModel($id);

	    $this->performAjaxRequest($model, $contactId);

		$this->controller->title = 'Sms Template - '.$model->name;

		$this->controller->render('view',array(
			'model'=>$model
		));
	}

	protected function performAjaxRequest($model, $contactId=null) {
		if (Yii::app()->request->isAjaxRequest) {
            if ($contactId) {
                $contact = Contacts::model()->findByPk($contactId);
				$componentTypeId = Yii::app()->request->getParam('componentTypeId');
				$componentId = Yii::app()->request->getParam('componentId');
				if(!empty($componentTypeId) || !empty($componentId)){
					$componentModel = ComponentTypes::getComponentModel($componentTypeId, $componentId);
				}else{
					$componentModel = NULL;
				}
				$content = $model->filterBodyByContactData($contact, $componentModel);
            }elseif($_POST["returnSmsModel"] === "1"){
				echo CJSON::encode($model->attributes);
				Yii::app()->end();
			}else {
                $content = $model->clearTemplateTags();
            }

			echo CJSON::encode(array(
				"name" => $model->name,
				"content" => $content,
			));

			Yii::app()->end();
		}
	}
}
