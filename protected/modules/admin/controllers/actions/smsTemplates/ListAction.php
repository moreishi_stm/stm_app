<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {

		$this->controller->title = 'Sms Templates List';

		$model = $this->controller->baseModel;

        if($_COOKIE['SmsTemplates']) {
            $model->attributes = $_COOKIE['SmsTemplates'];
        }

		if (isset($_GET['SmsTemplates'])) {
			$model->attributes=$_GET['SmsTemplates'];
            foreach($_GET['SmsTemplates'] as $field => $value) {
                setcookie("SmsTemplates[$field]", $value, time()+3600*24*30);
            }
		}

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
