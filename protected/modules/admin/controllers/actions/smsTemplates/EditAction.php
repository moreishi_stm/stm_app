<?php

class EditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$model=$this->controller->loadModel($id);
		$this->controller->title = 'Sms Template Edit';
		if (isset($_POST['SmsTemplates'])) {
			$model->attributes = $_POST['SmsTemplates'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully updated Sms Template.');
				$this->controller->redirect(array('view','id'=>$model->id));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}