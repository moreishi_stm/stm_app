<?php

class EditExtensionAction extends CAction
{
    public function run($id)
    {
        $model = IvrExtensions::model()->findByPk($id);

        $this->controller->title = 'Edit IVR Extension';

        if (isset($_POST['IvrExtensions'])) {

            $model->attributes = $_POST['IvrExtensions'];
            $model->updated = new CDbExpression('NOW()');
            $model->updated_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated IVR Extension.');
                $this->controller->redirect('/admin/ivr/listExtension/'.$model->ivr_id);
            }
        }

        // Render view
        $this->controller->render('formExtension', array(
            'model' =>  $model,
            'clientId'  =>  Yii::app()->user->clientId,
            'accountId'  =>  Yii::app()->user->accountId
        ));
    }
}