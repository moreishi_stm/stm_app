<?php

class EditSourceAction extends CAction
{
    public function run($id)
    {
        $model = IvrSources::model()->findByPk($id);

        $this->controller->title = 'Edit IVR Source';

        if (isset($_POST['IvrSources'])) {

            $model->attributes = $_POST['IvrSources'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated IVR Source.');
                $this->controller->redirect(array('ivr/listSource'));
            }
        }

        // Render view
        $this->controller->render('formSource', array(
            'model' =>  $model,
        ));
    }
}