<?php

class ListExtensionAction extends CAction
{
    public function run($id=null)
    {
        $model = new IvrExtensions();
        $model->unsetAttributes();

        if($id) {
            $model->ivr_id = $id;
        }

        // Render the view
        $this->controller->render('listExtension', array(
            'model' => $model
        ));
    }
}