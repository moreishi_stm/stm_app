<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddSourceAction extends CAction
{
    public function run($id = null)
    {
        $this->controller->title = 'Add IVR Source';
//        $model = $this->controller->baseModel;
        $model = new IvrSources();

        if (isset($_POST['IvrSources'])) {

            $model->attributes = $_POST['IvrSources'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added IVR Source!');
                $this->controller->redirect(array('ivr/listSource/'));
            }
            else {
                throw new Exception('Error, unable to save IVR source!');
            }
        }

        // Render view
        $this->controller->render('formSource', array(
            'model'=>$model,
        ));
    }
}