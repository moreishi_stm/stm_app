<?php

class ListAction extends CAction
{
    public function run()
    {
        $model = $this->controller->baseModel;
        $model->unsetAttributes();
        // Render the view
        $this->controller->render('index', array(
            'model' =>  $model
        ));
    }
}