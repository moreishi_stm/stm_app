<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddExtensionAction extends CAction
{
    public function run($id = null)
    {
        $this->controller->title = 'Add IVR';
//        $model = $this->controller->baseModel;
        $model = new IvrExtensions();

        if (isset($_POST['IvrExtensions'])) {

            $model->attributes = $_POST['IvrExtensions'];
            $model->ivr_id = $id;
            $model->added = new CDbExpression('NOW()');
            $model->added_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added IVR Extension!');
//                $this->controller->redirect(array('ivr/record?id=' . $model->id . '&type=extension'));
                $this->controller->redirect(array('ivr/editExtension/' . $model->id));
            }
            else {
                print_r($model->getErrors());exit;
            }
        }

        // Render view
        $this->controller->render('formExtension', array(
            'model'=>$model,
        ));
    }
}