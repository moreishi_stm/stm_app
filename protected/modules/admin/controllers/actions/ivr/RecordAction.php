<?php
require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmAws.S3')."/FileManagement.php");
/**
 * Record Action
 *
 * Used to record a greeting for an IVR
 */
class RecordAction extends CAction
{
    const FROM_NUMBER = '19043301885';

    public function run($id)
    {
        // Get id and type
        $type = Yii::app()->request->getParam('type');
        if(!$type) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') ID and type are required!');
        }

        // Handle each record type
        $greeting = false;
        switch($type) {

            // An IVR
            case 'ivr':

                // Get the IVR by the given ID
                /** @var Ivrs $ivr */
                $record = Ivrs::model()->findByPk($id);
                $label = 'IVR';
                $controller = 'ivr';
                $action = 'edit';
                if(!$record) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Error, no IVR found');
                }

            break;

            // An extension
            case 'extension':

                // Get the IVR by the given ID
                /** @var IvrExtensions $ivr */
                $record = IvrExtensions::model()->findByPk($id);
                $label = 'IVR Extension';
                $controller = 'ivr';
                $action = 'editExtension';
                if(!$record) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Error, no IVR Exception found');
                }

            break;

            // Voicemail Greeting
            case 'greeting':

                // Lookup greeting
                $record = CallRecordings::model()->findByPk($id);
                $label = 'Voicemail';
                $controller = 'voicemail';
                $action = 'editGreeting';
                if(!$record) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Error, no Greeting found! "' . $id . '"');
                }

            break;

            // No valid type found
            default:
                throw new Exception(__CLASS__.' (:'.__LINE__.') Error, unknown type passed in or no type.');
            break;
        }

        // Flag for if the phone entry is valid or not
        $phoneFail = false;

        if (Yii::app()->request->isAjaxRequest) {
            // Attempt to get phone number
            $phone = Yii::app()->format->formatInteger(Yii::app()->request->getPost('phone'));

            // If we had a post back, begin the call
            if ($phone) {

                // Validate phone number
                if(preg_match('/^[0-9]{10}$/', $phone) === 1) {

                    // Get Plivo REST API object
                    /** @var \StmPlivoFunctions\RestAPI $plivo */
                    $plivo = Yii::app()->plivo->getPlivo();

                    // Make call to the phone number listed to begin recording
                    $response = $plivo->make_call(array(
                            'from'          =>  self::FROM_NUMBER,
                            'to'            =>  '1' . Yii::app()->request->getPost('phone'),
                            'answer_url'    =>  StmFunctions::getSiteUrl() . '/plivoivrrecord/answer?recordType=' . $type,
                            'hangup_url'    =>  StmFunctions::getSiteUrl() . '/plivoivrrecord/hangup',
                            'ring_url'      =>  StmFunctions::getSiteUrl() . '/plivoivrrecord/ring',
                            'fallback_url'  =>  StmFunctions::getSiteUrl() . '/plivoivrrecord/fallback',
                            'time_limit'    =>  300,
                            'ring_timeout'  =>  60
                        ));

                    // Add the request UUID to the ivr record for looking up later
                    $record->record_call_request_uuid = $response['response']['request_uuid'];
                    if($record->save()) {
                        echo CJSON::encode(array('status'=>'success'));
                    }
                    else {
                        echo CJSON::encode(array('status'=>'error','errorMessage'=>current($record->getErrors())));
                    }
                    Yii::app()->end();
                }
                else {
                    $phoneFail = true;
                }
            }
        }

        // Render the view
        $this->controller->render('record', array(
            'ivr'       => $record,
            'phoneFail' => $phoneFail,
            'theType'   => $type,
            'label'     => $label,
            'controller'=> $controller,
            'action'    => $action,
        ));
    }
}