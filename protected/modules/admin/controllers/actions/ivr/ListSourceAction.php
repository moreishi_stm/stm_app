<?php

class ListSourceAction extends CAction
{
    public function run()
    {
        $model = new IvrSources();

        // Render the view
        $this->controller->render('listSource', array(
            'model' =>  $model
        ));
    }
}