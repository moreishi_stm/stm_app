<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction
{
    public function run()
    {
        $this->controller->title = 'Add IVR';
        $model = $this->controller->baseModel;

        if (isset($_POST['Ivrs'])) {

            $model->attributes = $_POST['Ivrs'];
            $model->added = new CDbExpression('NOW()');
            $model->added_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added IVR!');
//                $this->controller->redirect(array('ivr/record?type=ivr&id=' . $model->id));
                $this->controller->redirect(array('ivr/edit/' . $model->id));
            }
        }

        // Render view
        $this->controller->render('form', array(
            'model'=>$model
        ));
    }
}