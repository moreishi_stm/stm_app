<?php

class EditAction extends CAction
{
    public function run($id)
    {
        $model = $this->controller->loadModel($id);

        $this->controller->title = 'Edit IVR';

        if (isset($_POST['Ivrs'])) {

            $model->attributes = $_POST['Ivrs'];
            $model->updated = new CDbExpression('NOW()');
            $model->updated_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated IVR.');
//                $this->controller->redirect(array('/admin/ivr/record?id=' . $model->id . '&type=ivr'));
                $this->controller->redirect(array('/admin/ivr'));
            }
        }

        // Render view
        $this->controller->render('form', array(
            'model' =>  $model,
            'clientId'  =>  Yii::app()->user->clientId,
            'accountId'  =>  Yii::app()->user->accountId
        ));
    }
}