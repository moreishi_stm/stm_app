<?php

class PrintAction extends CAction {
	/**
	 * Manages models
	 */
	public function run($id) {
		$this->controller->title = 'Operation Manuals';

		$model = OperationManuals::model()->findByPk($id);
        $this->controller->layout = 'plain';

		$this->controller->renderPartial('print',array(
			'model'=>$model
		));
	}
}
