<?php

class TagsAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Operation Manual Tags';

		$model = new OperationManualTags('search');
		$model->unsetAttributes();  // clear any default values

		// used for gridview search box inputs
        if (isset($_GET['OperationManualTags'])) {
			$model->attributes=$_GET['OperationManualTags'];
        }

		$model->operationManualTagLu = new OperationManualTagLu('search');
		if (isset($_GET['OperationManualTagLu'])) {
			$model->operationManualTagLu->attributes = $_GET['OperationManualTagLu'];
		}

		$this->controller->render('tags',array(
			'model'=>$model
		));
	}
}
