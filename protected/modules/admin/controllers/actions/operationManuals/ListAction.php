<?php

class ListAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Operation Manuals';

		$model = new OperationManuals('search');
		$model->unsetAttributes();  // clear any default values
        $model->status_ma = 1;

		// used for gridview search box inputs
        if (isset($_GET['OperationManuals'])) {
			$model->attributes=$_GET['OperationManuals'];
        }

		$model->operationManualTagLu = new OperationManualTagLu('search');
		if (isset($_GET['OperationManualTagLu'])) {
			$model->operationManualTagLu->attributes = $_GET['OperationManualTagLu'];
		}

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
