<?php

class EditAction extends CAction {
	/**
	 * Manages models
	 */
	public function run($id) {
		$this->controller->title = 'Edit Operation Manual';

		$model = OperationManuals::model()->findByPk($id);

        if (Yii::app()->request->isAjaxRequest) {
            $this->performAjaxRequest($model);
        }

        if ($model->operationManualTagLu) {
            foreach ($model->operationManualTagLu as $OperationManualTagLu) {
                array_push($model->tagCollection, $OperationManualTagLu->tag->id);
            }
        }

		$this->controller->render('form',array(
			'model'=>$model
		));
	}

    protected function performAjaxRequest(OperationManuals $model) {

        if (Yii::app()->request->isAjaxRequest) {

            // used for gridview search box inputs
            if (isset($_POST['OperationManuals'])) {
                $model->attributes=$_POST['OperationManuals'];

                if ($model->save()) {

                    if ($model->tagCollection) {
                        $operationTagCollectionFlipped = array_flip($model->tagCollection);
                        foreach ($model->operationManualTagLu as $OperationManualTagLu) {
                            if (!in_array($OperationManualTagLu->operation_manual_tag_id, $model->tagCollection)) {
                                $OperationManualTagLu->delete();
                            } else {
                                unset($operationTagCollectionFlipped[$OperationManualTagLu->operation_manual_tag_id]);
                            }
                        }

                        $model->tagCollection = array_flip($operationTagCollectionFlipped);

                        // Attach the contact types to the newly contact
                        foreach ($model->tagCollection as $attributeId) {
                            $OperationManualTagLu = new OperationManualTagLu;
                            $OperationManualTagLu->operation_manual_id = $model->id;
                            $OperationManualTagLu->operation_manual_tag_id = $attributeId;
                            $OperationManualTagLu->save();
                        }
                    }
                } else {
                    echo CActiveForm::validate($model);
                }
            }

            Yii::app()->end();
        }
    }
}
