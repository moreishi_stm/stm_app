<?php

class ViewAction extends CAction {
	/**
	 * Manages models
	 */
	public function run($id) {
		$this->controller->title = 'View Operation Manual';

		$model = OperationManuals::model()->findByPk($id);

		$this->controller->render('view',array(
			'model'=>$model
		));
	}
}
