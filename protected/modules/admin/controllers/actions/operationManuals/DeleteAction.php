<?php

class DeleteAction extends CAction
{
    public function run($id)
    {
        $model = OperationManuals::model()->findByPk($id);

        if (isset($_POST['OperationManuals'])) {
            $model->attributes = $_POST['OperationManuals'];
        }

        $this->performAjaxRequest($model);
    }

    protected function performAjaxRequest(OperationManuals $model)
    {
        if (Yii::app()->request->isAjaxRequest) {

            // check to see if it's linked to any other active tasks or action plan items
            $linked = Yii::app()->db->createCommand("SELECT COUNT(t.id) tasks, count(i.id) items FROM operation_manuals o LEFT JOIN tasks t ON t.operation_manual_id=o.id AND t.is_deleted=0 LEFT JOIN action_plan_items i ON i.operation_manual_id=o.id AND i.is_deleted=0  AND i.status_ma=1 WHERE o.id={$model->id}")->queryRow(); //LEFT JOIN action_plans a ON a.operation_manual_id=o.id

            if($linked['tasks']!=0 || 'items'!=0) {
                echo CJSON::encode(array('errorMessage' => 'This operations manual is linked to existing Tasks and/or Action Plan Items. Unlink first to delete.', 'status' => 'error'));
                Yii::app()->end();
            }

            // will always return false due to soft delete
            $model->status_ma = 0;
            if ($model->save()) {
                echo CJSON::encode(array('status' => 'success'));
            }
            else {
                echo CJSON::encode(array('errorMessage' => 'Operation Manual did not save. Error Message: '.$model->getErrors()[0], 'status' => 'error'));
            }

            Yii::app()->end();
        }
    }
}
