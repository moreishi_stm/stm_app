<?php

class AddAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Add Operation Manual';

		$model = new OperationManuals;

		// used for gridview search box inputs
        if (isset($_POST['OperationManuals'])) {
			$model->attributes=$_POST['OperationManuals'];

			if ($model->save()) {
                foreach ($model->tagCollection as $attributeId) {
                    $OperationManualTagLu = new OperationManualTagLu;
                    $OperationManualTagLu->operation_manual_id = $model->id;
                    $OperationManualTagLu->operation_manual_tag_id = $attributeId;
                    $OperationManualTagLu->save();
                }

				Yii::app()->user->setFlash('success', 'Successfully Added Operation Manual.');
				$this->controller->redirect(array(
											'edit',
											'id' => $model->id
											)
				);
			}
        }

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}
