<?php

class AddTagAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Operation Manual Add Tag';

		$model = new OperationManualTags;

		// used for gridview search box inputs
        if (isset($_POST['OperationManualTags'])) {
			$model->attributes=$_POST['OperationManualTags'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully added Tag.');
				$this->controller->redirect(array(
											'tags',
											'id' => $model->id
											)
				);
			}
        }

		$this->controller->render('formTag',array(
			'model'=>$model
		));
	}
}
