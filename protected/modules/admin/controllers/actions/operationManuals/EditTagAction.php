<?php

class EditTagAction extends CAction {
	/**
	 * Manages models
	 */
	public function run($id) {
		$this->controller->title = 'Operation Manual Edit Tag';

		$model = OperationManualTags::model()->findByPk($id);

		// used for gridview search box inputs
        if (isset($_POST['OperationManualTags'])) {
			$model->attributes=$_POST['OperationManualTags'];

			if ($model->save()) {

				if ($model->tagPermissionCollection) {
					$tagPermissionsFlipped = array_flip($model->tagPermissionCollection);
					foreach ($model->operationManualTagPermissionLu as $OperationManualTagPermissionLu) {
						if (!in_array($OperationManualTagPermissionLu->auth_item_name, $model->tagPermissionCollection)) {
							$OperationManualTagPermissionLu->delete();
						} else {
							unset($tagPermissionsFlipped[$OperationManualTagPermissionLu->operation_manual_tag_id]);
						}
					}

					$model->tagPermissionCollection = array_flip($tagPermissionsFlipped);

					foreach ($model->tagPermissionCollection as $attributeId) {
						$OperationManualTagPermissionLu = new OperationManualTagPermissionLu;
						$OperationManualTagPermissionLu->operation_manual_tag_id = $model->id;
						$OperationManualTagPermissionLu->auth_item_name = $attributeId;
						$OperationManualTagPermissionLu->save();
					}
				}


				Yii::app()->user->setFlash('success', 'Successfully update Tag.');
				$this->controller->redirect(array(
											'tags',
											'id' => $model->id
											)
				);
			}
        } else {
			if ($model->operationManualTagPermissionLu) {
				foreach ($model->operationManualTagPermissionLu as $OperationManualTagPermissionLu) {
					array_push($model->tagPermissionCollection, $OperationManualTagPermissionLu->tag->id);
				}
			}
		}

		$this->controller->render('formTag',array(
			'model'=>$model
		));
	}
}
