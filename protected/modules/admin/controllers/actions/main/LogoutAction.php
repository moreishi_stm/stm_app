<?php

class LogoutAction extends CAction {

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function run() {
		Yii::app()->user->logout();
		$this->controller->redirect('/admin');
	}
}
