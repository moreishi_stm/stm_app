<?php

class IndexAction extends CAction {

	public function run() {
		if (!Yii::app()->user->isGuest)
			$this->controller->redirect('/admin/main/dashboard');

		$this->controller->module->layout = 'columnLogin';

		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
            	$defaultUrl = Yii::app()->createUrl('/admin/main/dashboard');
	            $returnUrl = Yii::app()->user->getReturnUrl($defaultUrl);
				$this->controller->redirect($returnUrl);
            }
        }

		// display the login form
		$this->controller->render('login',array('model'=>$model));
	}
}
