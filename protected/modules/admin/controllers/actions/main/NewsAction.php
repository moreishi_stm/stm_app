<?php

class NewsAction extends CAction {

	public function run()
    {
        $this->controller->title = 'News';
        $this->controller->displayName = 'Seize the Market';
        $data  = Yii::app()->stm_hq->createCommand("SELECT date, description, count from news_feed")->queryAll();

        $dataProvider=new CArrayDataProvider($data, array(
            'pagination'=>array(
                'pageSize'=>25,
            ),
        ));
        // display the login form
		$this->controller->render('news',array('dataProvider'=>$dataProvider));
	}
}