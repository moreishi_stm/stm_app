<?php
	Yii::import('admin_widgets.DateRanger.DateRanger');

	class DashboardAction extends CAction {

		/**
		 * Shows the dashboard appropriate for the user group and any personal settings for that user/contact
		 */
		public function run() {
            $this->controller->pageColor = 'blue';
			$this->controller->title = 'My Dashboard';
			$view = Yii::app()->user->settings->dashboard;

			$DateRanger = new DateRanger;
            $DateRanger->defaultSelect = 'up_to_next_1_week';
            $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

			switch ($view) {
				case 'closingManager':
					$this->controller->pageColor = 'orange';
					$model = Closings::getListSearchModel();
					break;

//				case 'listingManager':
//                    $id = ($_GET['contact_id']) ? $_GET['contact_id'] : Yii::app()->user->id;
//					$this->controller->pageColor = 'green';
//					$model = Listings::getListSearchModel();
//                    $taskTypeIds = (!empty($_GET['task_types']))? array($_GET['task_types']) : null;
//					break;

				case 'listingAgent':
					$id = ($_GET['contact_id']) ? $_GET['contact_id'] : Yii::app()->user->id;
					$taskTypeIds = (!empty($_GET['task_types']))? array($_GET['task_types']) : null;

                    // @todo: this criteria needs to be centralized. Also used multiple times in this switch AND in tasks/ListAction
                    $overdueCriteria = Tasks::model()->byAssignedTo($id)->dateRange($dateRange)->overdue()->byTaskType($taskTypeIds)->excludeTaskTypeIds(TaskTypes::AUTO_EMAIL_DRIP)->byComponentTypeIds($componentTypeIds)->notCompleted()->asc()->byPriority()->getDbCriteria();
					$overdueCount = Tasks::model()->count($overdueCriteria);
					break;

				case 'buyerAgent':
					$model = Transactions::newInstance('buyers', 'search');
					$model->contact = new Contacts('search');
					$model->contact->unsetAttributes();
					$model->contact->emails = new Emails('search');
					$model->contact->emails->unsetAttributes();
					$model->contact->phones = new Phones('search');
					break;

                case 'owner':
                    $id = ($_GET['contact_id']) ? $_GET['contact_id'] : Yii::app()->user->id;

                    $taskTypeIds = (!empty($_GET['task_types']))? array($_GET['task_types']) : null;

                    /** Set the default select to "up_to_today" as defined by the dropdown */
                    if (!isset($_GET['DateRanger'])) {
                        $DateRanger->defaultSelect = 'up_to_next_1_week';
                    }

                    // @todo: this criteria needs to be centralized. Also used multiple times in this switch AND in tasks/ListAction
                    $overdueCriteria = Tasks::model()->byAssignedTo($id)->dateRange($dateRange)->overdue()->byTaskType($taskTypeIds)->excludeTaskTypeIds(TaskTypes::AUTO_EMAIL_DRIP)->byComponentTypeIds($componentTypeIds)->notCompleted()->asc()->byPriority()->getDbCriteria();
                    $overdueCount = Tasks::model()->count($overdueCriteria);
                    break;

                case 'recruiting':
                    $this->controller->redirect('/admin/recruits');
                    break;

                case 'storyboard':
                    break;

                default:
                    if(Yii::app()->user->checkAccess(AuthItem::ROLE_RECRUIT_REFERRING_AGENT)) {
                        $this->controller->redirect('/admin/recruits');
                    }

					$view = 'default';
					$id = ($_GET['contact_id']) ? $_GET['contact_id'] : Yii::app()->user->id;
					$taskTypeIds = (!empty($_GET['task_types']))? array($_GET['task_types']) : null;

					/** Set the default select to "up_to_today" as defined by the dropdown */
					if (!isset($_GET['DateRanger'])) {
						$DateRanger->defaultSelect = 'up_to_next_1_week';
					}

                    // @todo: this criteria needs to be centralized. Also used multiple times in this switch AND in tasks/ListAction
                    $overdueCriteria = Tasks::model()->byAssignedTo($id)->dateRange($dateRange)->overdue()->byTaskType($taskTypeIds)->excludeTaskTypeIds(TaskTypes::AUTO_EMAIL_DRIP)->byComponentTypeIds($componentTypeIds)->notCompleted()->asc()->byPriority()->getDbCriteria();
					$overdueCount = Tasks::model()->count($overdueCriteria);
					break;
			}


			$tasksData = new CActiveDataProvider(Tasks::model()->byAssignedTo($id)->dateRange($dateRange)->byTaskType($taskTypeIds)->excludeTaskTypeIds(TaskTypes::AUTO_EMAIL_DRIP)->notCompleted()->asc()->byPriority(), array(
				'pagination' => array(
					'pageSize' => 100,
				),
			));

			$this->controller->render('dashboard/' . $view, array(
					'model' => $model,
					'tasksData' => $tasksData,
					'overdueCount' => $overdueCount,
				)
			);
		}
	}
