<?php

class ErrorAction extends CAction {

	/**
	 * This is the action to handle external exceptions.
	 */
	public function run() {

		$this->controller->title='Error';
        $this->controller->layout='plain';

	    if ($error=Yii::app()->errorHandler->error) {

	    	// Process the message for ajax requests
            if (Yii::app()->request->isAjaxRequest) {

                  echo $error['message'];
            } else {

            	if ($error['message'])
	            	$this->writeErrorMsgCookie($error['message']);

	        	$this->controller->render('error', array(
	        		'error'=>$error,
	        	));
            }
	    }
	}

	protected function writeErrorMsgCookie($errorMessage) {

		$errorMessageCookieName = 'STMCookieError';
		unset($_COOKIE[$errorMessageCookieName]);
		setcookie($errorMessageCookieName, '['.Yii::app()->user->fullName.']'.$errorMessage, time()+3600);
	}
}
