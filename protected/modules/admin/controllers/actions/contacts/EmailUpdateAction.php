<?php
    class EmailUpdateAction extends CAction
    {
        public function run()
        {
            switch($_REQUEST['componentTypeId']) {
                case ComponentTypes::SELLERS:
                    $model = Sellers::model()->findByPk($_REQUEST['componentId']);
                    break;
                case ComponentTypes::BUYERS:
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Buyers client update is not setup.');
                    break;
                case ComponentTypes::CLOSINGS:
                    $model = Closings::model()->findByPk($_REQUEST['componentId']);
                    break;
                default:
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Client update is not setup.');
                    break;
            }

            $this->performAjaxRequest($model);
        }

        protected function performAjaxRequest($model)
        {
            if (Yii::app()->request->isAjaxRequest) {
                Yii::import('seizethemarket.protected.commands.*');
                $commandRunner = new CConsoleCommandRunner();

                if($_REQUEST['mode']=="preview") {
                    $command = new EmailClientUpdatesCommand('actionGetHtml', $commandRunner);
                    ob_start();
                    // return 0 = successful, 1 means does not exist automatically
                    $runResult = $command->run(array('getHtml', '--componentTypeId='.$_REQUEST['componentTypeId'], '--componentId='.$_REQUEST['componentId'], '--return=false'));

                    $html = ob_get_contents();
                    ob_end_clean();

                    echo CJSON::encode(array('html'=>$html));
                    Yii::app()->end();
                }
                elseif($_POST['mode']=="sendEmail") {
                    $command = new EmailClientUpdatesCommand('actionSendEmails', $commandRunner);
                    // return 0 = successful, 1 means does not exist automatically
                    $runResult = $command->run(array('sendEmails', '--clientId='.Yii::app()->user->clientId, '--accountId='.Yii::app()->user->accountId, '--componentTypeId='.$_POST['componentTypeId'], '--componentId='.$_POST['componentId']));

                    $result = (empty($runResult))? 'success' : 'error';

                    echo CJSON::encode(array('result'=>$result));
                    Yii::app()->end();
                }
            }
        }
	}
