<?php
    // Include things we need to get the job done
    Yii::import('admin_module.components.StmSlybroadcast.SlyBroadcastException', true);
    Yii::import('admin_module.components.StmSlybroadcast.Slybroadcast', true);
    Yii::import('admin_module.components.StmSlybroadcast.Campaign', true);

	class LeadsAction extends CAction
    {
        public $taskTypeIds;
        public $dateSearchTypes = array(
                'added_desc' => 'Added: newest first',
                'added_asc' => 'Added: oldest first',
                'last_login_desc' => 'Last Login: newest first',
                'last_login_asc' => 'Last Login: oldest first',
                'status_asc' => 'Status: Hottest first',
            );

		protected static $_nowTime;

        public function run()
        {
            $this->taskTypeIds = TaskTypes::PHONE.','.TaskTypes::TODO.','.TaskTypes::NOTE.','.TaskTypes::EMAIL_MANUAL;

            $this->controller->title = 'Leads';

            switch(1) {
                case ($_GET['transactionTypeId'] == ComponentTypes::BUYERS):
                case ($_GET['type'] == ComponentTypes::BUYERS):
                    $transactionType = ComponentTypes::BUYERS;
                    break;

                case ($_GET['transactionTypeId'] == ComponentTypes::SELLERS):
                case ($_GET['type'] == ComponentTypes::SELLERS):
                    $transactionType = ComponentTypes::SELLERS;
                    break;
            }

            switch($transactionType) {
                case ComponentTypes::BUYERS:
                    if(Yii::app()->user->checkAccess('noBuyerAccess')) {
                        $this->controller->render('/main/accessDenied', array());
                        Yii::app()->end();
                    }
                    break;

                case ComponentTypes::SELLERS:
                    if(Yii::app()->user->checkAccess('noSellerAccess')) {
                        $this->controller->render('/main/accessDenied', array());
                        Yii::app()->end();
                    }
                    break;
            }

            if(isset($_GET['transactionStatus'])) {
                $transactionStatusSelected = $_GET['transactionStatus'];
            } else {
                $transactionStatusSelected = array(TransactionStatus::NEW_LEAD_ID, TransactionStatus::HOT_A_LEAD_ID,TransactionStatus::B_LEAD_ID, TransactionStatus::C_LEAD_ID, TransactionStatus::D_NURTURING_SPOKE_TO_ID, TransactionStatus::NURTURING_ID,TransactionStatus::COMING_SOON_ID, TransactionStatus::ACTIVE_LISTING_ID, TransactionStatus::UNDER_CONTRACT_ID);
            }

            $leadsData = $this->getLeadsData();

			if(!empty($_GET['massActionProcess']) || !empty($_GET["MassAction"])) {
				$this->_massUpdateActions($leadsData['leads']);
			}


            if(isset($_GET['export']) && $_GET['export']==1){

                $contactIds = $leadsData['contactIds'];

                $criteria = new CDbCriteria();
                $criteria->addInCondition('id', $contactIds);
                $contacts = Contacts::model()->findAll($criteria);
                unset($dataProvider);
                $exportData = Contacts::model()->getExportData($contacts);
                $date = date('Y-m-d');
                header( "Content-Type: application/vnd.ms-excel; charset=utf-8" );
                header( "Content-Disposition: inline; filename=\"leads_export_$date.xls\"" );
                echo $exportData;
                Yii::app()->end();
            }
            else {
                $dataProvider=new CArrayDataProvider($leadsData['leads'], array(
                    'pagination'=>array(
                        'pageSize'=>25,
                    ),
                ));
                $dataProvider->setTotalItemCount($leadsData['count']);
                $dataProvider->pagination->setItemCount($leadsData['count']);
                $dataProvider->setData($leadsData['leads']);

                $sort = new CSort();
                $sort->attributes = array(
                    'Added',
                    'Last Login',
					'Homes Viewed',// => array('desc'=>''),
					'Saved Homes',
					'Saved Searches',
					"Last Activity",
					"Status"
                );
                $sort->defaultOrder = array(
                    'Added' => false,
                    'Homes Viewed' => false,
                    'Saved Homes' => false,
                    'Saved Searches' => false,
                    "Last Activity" => false,
                );

                $dataProvider->sort = $sort;
            }


            $dialerLists[ComponentTypes::SELLERS] = CallLists::model()->findAllByAttributes(array('preset_ma'=>CallLists::CUSTOM_SELLERS));

            $dialerLists[ComponentTypes::BUYERS] = CallLists::model()->findAllByAttributes(array('preset_ma'=>CallLists::CUSTOM_BUYERS));

            if($_GET['dialerProcess'] && $_GET['dialerAction'] && $_GET['dialerListId'] && Yii::app()->request->isAjaxRequest) {

                $this->_dialerListActions($leadsData['leads'], $_GET['dialerAction'], $_GET['dialerListId']);
            }


            // Handle slybroadcast, if we wanted that
            if($_GET['broadcastProcess']) {
                try {
                    $this->_broadcastProcessAction($leadsData['leads']);
                    $status = 'success';
                    $slyBroadcastMessage = 'Sly Broadcast campaign has been created successfully.';
                }
                catch(\StmSlybroadcast\SlyBroadcastException $e) {
                    $status = 'error';
                    $slyBroadcastMessage = 'Error: Sly Broadcast had an error. '.$e->getMessage().' Please check your username/password. Contact us for additional support.';
                    Yii::log(__CLASS__.' (:'.__LINE__.') Sly Broadcast had an error.', CLogger::LEVEL_ERROR);
                }
                catch(Exception $e) {
                    $status = 'error';
                    $slyBroadcastMessage = 'Error: Sly Broadcast had an unknown error. '.$e->getMessage().' Try again later or Contact us for additional support.';
                    Yii::log(__CLASS__.' (:'.__LINE__.') Sly Broadcast had an error.', CLogger::LEVEL_ERROR);
                }
                finally {
                    // Send response
                    echo CJSON::encode(array('status'=> $status, 'message'=>$slyBroadcastMessage));
                    Yii::app()->end();
                }
            }

            $view = (Yii::app()->request->isAjaxRequest)? '_leads': 'leads';
            $renderType = (Yii::app()->request->isAjaxRequest)? 'renderPartial': 'render';
            $this->controller->{$renderType}($view, array(
                    'dataProvider' => $dataProvider,
                    'transactionStatusSelected'=> $transactionStatusSelected,
                    'count'=>$leadsData['count'],
                    'start'=>($_GET['page'])?$_GET['page']*25-24: 1,
                    'end'=>($_GET['page'])?$_GET['page']*25: ($leadsData['count'] < 25)? $leadsData['count']: 25,
                    'dialerLists' => $dialerLists,
                )
            );
        }

        protected function _dialerListActions($leads, $dialerAction, $dialerListId)
        {
            $errorLog = array();

            $dialerListPhoneIds = Yii::app()->db->createCommand()
                ->select('phone_id')
                ->from('call_list_phones')
                ->where('call_list_id=:callListId AND hard_deleted IS NULL', array(':callListId'=> $dialerListId))
                ->queryColumn();

            foreach($leads as $lead) {

                switch($dialerAction) {
                    case 'add':
                        CallListPhones::addToCallListByContactId($dialerListId, $lead['contact_id'], null, $errorLog);
                        break;

                    case 'remove':
                        CallListPhones::removeFromCallListByContactId($dialerListId, $lead['contact_id'], $errorLog);
                        break;
                }
            }
        }

        protected function _massUpdateActions($leads)
        {
			if(empty($_GET['massActionType'])){return;}
			$massUpdateType = $_GET['massActionType'];
			$massAction = $_GET["MassAction"];
			$status = 404;
			$message = "";
			$count = 0;
			if(!empty($leads)){
				foreach($leads as $lead) {

					switch($massUpdateType) {

						case "unapplyActionPlans":
						case "applyActionPlans":
							$status = 200;
           					 if (
							 	!($ActionPlan = ActionPlans::model()->findByPk($_GET['massActionComponentId']))
							 ||
								!($Parent = ComponentTypes::getComponentModel($lead['component_type_id'], $lead['id']))
							 ) {
                // no action plan found
             				   return;
           					 }
							if($ActionPlan->component_type_id === $lead["component_type_id"]){
								if($massUpdateType === "unapplyActionPlans"){
									$ActionPlan->unapply($Parent);
								} elseif($massUpdateType === "applyActionPlans"){
									$ActionPlan->apply($Parent);
								}
								$count++;
							}

                    break;

						case "addTag":
						case "removeTag" :
							$status = 200;
							foreach ($massAction["tags"] as $oId){
								if(empty($oId)) {
									continue;
                }
								$dataArray = array(
									"transaction_id" => $lead["id"],
									"transaction_tag_id" => $oId
								);
								$massActionModel = new TransactionTagLu();
								$massActionModel = $massActionModel->findByAttributes($dataArray);
								//only edit the record if we need to
								if(empty($massActionModel->transaction_id) && $_GET["massActionType"] === "addTag" && !empty($dataArray)){
									$massActionModel = new TransactionTagLu();
									$massActionModel->attributes = $dataArray;
									$massActionModel->save(1);
								}elseif (!empty($massActionModel->transaction_id) && $_GET["massActionType"] === "removeTag"){
									$massActionModel->deleteAllByAttributes($dataArray);
								}
								$count++;

							}
							break;

						case "updateLeadStatus" :
							$status = 200;
							if(!empty($massAction["status_options"])){
								if($transaction = Transactions::model()->findByPk($lead["id"])){
									$transaction->transaction_status_id = $massAction["status_options"];
									$transaction->save();
									$count++;
								}
							}else{
								$message = "A status was not selected";
							}
							break;

						default : $status = "404"; $message = "I dont know what you are trying to do";break;
					}
				}
				$message .= "({$count})";
				header("Content-Type: application/json");
				echo CJSON::encode(array("status"=> $status, 'message'=>$message));
				return Yii::app()->end();
            }
        }

        /**
         * Broadcast Process Action
         *
         * Creates a new slybroadcast from given data
         * @param $leads
         */
        protected function _broadcastProcessAction($leads)
        {
            // New SlyBroadcast campaign
            $campaign = new StmSlybroadcast\Campaign();
            $campaign->audioUrl = 'http://sites.seizethemarket.com/site-files/' . Yii::app()->user->clientId . '/broadcast-messages/' . $_GET['broadcastAudioFile'] . '.wav';
            $campaign->callerId = $_GET['broadcastCallerId'];
            $campaign->date = date('Y-m-d H:i:s', strtotime($_GET['broadcastDeliveryDate']));
            $campaign->mobileOnly = $_GET['broadcastMobileOnly'] ? true : false;

            // Add phones to campaign
            foreach($leads as $lead) {
                if(!empty($lead['phone'])) {
                    $campaign->addPhone($lead['phone']);
                }
            }

            // Create the campaign
            $slyBroadcast = new StmSlybroadcast\Slybroadcast();
            //$response = "OK\nsession_id=7062677994\nnumber of phone=25"
            $response = $slyBroadcast->createCampaign($campaign);
        }

        protected  function getLeadsData()
        {
            $dataCommand = $this->_getLeadDataQuery();
            $countCommand = $this->_getLeadDataQuery($count=true);

            // (select min(due_date) from tasks where component_type_id=t.component_type_id AND component_id=t.id AND due_date>='".date('Y-m-d')."' AND due_date<='".date("Y-m-d", strtotime('30 days'))."' AND is_deleted=0) as nextTaskDate,
            // (select max(activity_date) from activity_log where component_type_id=t.component_type_id AND component_id=t.id AND task_type_id IN ($this->taskTypeIds) AND is_deleted=0) as lastActivityDate,

            // check the count and if less than 25, remove offset.

            if(isset($_GET['export']) && $_GET['export']==1) {
                $dataCommand->selectDistinct('t.contact_id');
                // this is done this way because there is no way to unset the limit once it is set. It is set from the getLeadDataQuery() method. Chose to keep all export related activity in 1 area. There is the option of having an if statement in the method to not put the limit in there at all.
                $dataCommand->limit = 9999999999999;
                $leadsData['contactIds'] = $dataCommand->queryColumn();
            }
            elseif($_GET['broadcastProcess']) {
                $dataCommand->selectDistinct('p.phone as phone');
                $dataCommand->limit = 9999999999999;
                $leadsData['leads'] = $dataCommand->queryAll();
            }
            else {
                if($_GET['page'] >1) {
                    $dataCommand->offset(($_GET['page']-1)*25);
                }
                $dataCommand->group('t.id');

                // accomodate dialer action

				if(!empty($_GET["MassAction"])){
					$dataCommand->limit = 250;
				}elseif(($_GET['dialerProcess'] && $_GET['dialerAction'] && $_GET['dialerListId'])) {
					$dataCommand->limit = 9999999999999;
                }
                $leadsData['count'] = $countCommand->queryScalar();
                $leadsData['leads'] = $dataCommand->queryAll();
            }


            return $leadsData;
        }

        protected function _getLeadDataQuery($count=false)
        {
            $command = Yii::app()->db->createCommand();

			$countQuery = "";
			$order = "ASC";
			if(isset($_GET["sort"]) && !empty($_GET["sort"])){
				switch ($_GET["sort"]){
					case "Saved Searches" :
					case "Saved Searches.desc" :
					$countQuery = ",(SELECT count(saved_search.id) FROM saved_home_searches as saved_search WHERE saved_search.contact_id = c.id AND saved_search.is_deleted = 0) as saved_search_count";
					if($_GET["sort"] === "Saved Searches.desc"){
						$order = "DESC";
					}
					$orderCommand = "saved_search_count ".$order;

					break;
					case "Saved Homes" :
					case "Saved Homes.desc" :
						$countQuery =
							",(SELECT count(saved_homes.id) FROM saved_homes WHERE saved_homes.contact_id = c.id AND saved_homes.is_deleted=0 ) as saved_home_count";
						if($_GET["sort"] === "Saved Homes.desc"){
							$order = "DESC";
						}
						$orderCommand = "saved_home_count ".$order;


					break;
					case "Homes Viewed" :
					case "Homes Viewed.desc" :
						$countQuery = ",(SELECT count(vp.id) FROM viewed_pages vp WHERE vp.contact_id=c.id AND vp.listing_id IS NOT NULL) as viewed_home_count";
						if($_GET["sort"] === "Homes Viewed.desc"){
							$order = "DESC";
						}
						$orderCommand = "viewed_home_count ".$order;
						break;
					case 'Last Login':
						$orderCommand = "c.last_login";
						break;

					case 'Last Login.desc':
						$orderCommand = "c.last_login DESC";
						break;

					case 'Added':
						$orderCommand = "t.added";
						break;

					case 'Added.desc':
						$orderCommand = "t.added DESC";
						break;
					case "Status" : $orderCommand ="transactionStatusId ASC";break;
					case "Status.desc" : $orderCommand ="transactionStatusId DESC";break;
					case "Last Activity":
					case "Last Activity.desc":
						$countQuery = ",(SELECT max(activity_date) from activity_log where component_type_id= t.component_type_id AND component_id= t.id AND task_type_id IN ($this->taskTypeIds) AND is_deleted=0) as last_activity";
						if($_GET["sort"] === "Last Activity.desc"){
							$order = "DESC";
						}
						$orderCommand = "last_activity ".$order;
						break;
					default: break;
				}
			}



            if($count) {
                $command->select('count(DISTINCT t.id)');
            } else {
                $command
                    ->select("
                    t.id as id,
                    t.contact_id,
                    t.component_type_id,
                    ct.name componentTypeName,
                    t.transaction_status_id as transactionStatusId,
                    t.component_type_id as componentTypeId,
                    c.first_name firstName,
                    c.last_name lastName,
                    c.spouse_first_name spouseFirstName,
                    c.spouse_last_name spouseLastName,
                    a.address as address,
                    a.city as city,
                    a.state_id as stateId,
                    a.zip as zip,
                    p.phone as phone,
                    c.last_login as lastLogin,
                    t.added as added".$countQuery);
            }
			/**
			 *count(distinct(viewed_pages.id)) as viewed_home_count,
			 */
            $command
                ->from('transactions t')
                ->leftJoin('contacts c', 't.contact_id = c.id')
                ->leftJoin('addresses a', 't.address_id = a.id')
                ->leftJoin('component_types ct', 't.component_type_id = ct.id')
                ->leftJoin('auth_assignment a2', 'a2.userid = c.id')
                ->leftJoin('phones p', 'p.contact_id = c.id AND p.is_deleted=0')
                ->andWhere('a2.userid IS NULL || (a2.userid IS NOT NULL AND t.transaction_status_id='.TransactionStatus::ACTIVE_LISTING_ID.')')
                ->andWhere('t.account_id=:accountId',array(':accountId'=>Yii::app()->user->accountId))
                ->order('c.last_login DESC')
                ->limit(25);

            if($_GET['type'] || $_GET['transactionTypeId']) {
                if ($_GET['transactionTypeId'] >1) {
                    $command->andWhere('t.component_type_id ='.$_GET['transactionTypeId']);
                    $componentType = $_GET['transactionTypeId'];

                } elseif($_GET['type']>1 && $_GET['transactionTypeId'] != 1) {
                    $command->andWhere('t.component_type_id ='.$_GET['type']);
                    $componentType = $_GET['type'];
                }

                switch($componentType) {
                    case ComponentTypes::BUYERS:
                        $this->controller->pageColor = 'yellow';
                        break;
                    case ComponentTypes::SELLERS:
                        $this->controller->pageColor = 'green';
                        break;
                    default:
                        $this->controller->pageColor = 'red';
                        break;
                }
            }

            if($_GET['firstName']) {
                $command->andWhere('c.first_name like "%'.$_GET['firstName'].'%" OR c.spouse_first_name like "%'.$_GET['firstName'].'%"');
            }

            if($_GET['lastName']) {
                $command->andWhere('c.last_name like "%'.$_GET['lastName'].'%" OR c.spouse_last_name like "%'.$_GET['lastName'].'%"');
            }

            if($_GET['email']) {
                $command
                    ->leftJoin('emails e', 'e.contact_id = c.id')
                    ->andWhere('e.email like "%'.$_GET['email'].'%" AND e.is_deleted=0');
            }

            if($_GET['address']) {
                $command
                    ->andWhere('a.address like "%'.$_GET['address'].'%" AND a.is_deleted=0');
            }

            if($_GET['sourceId']) {
                $command
                    ->andWhere('t.source_id ='.$_GET['sourceId']);
            }

            if($_GET['phone'] || $_GET['hasPhone']==1) {

//                $command
//                    ->leftJoin('phones p', 'p.contact_id = c.id')
//                    ->andWhere('p.is_deleted=0');

                if($_GET['phone']) {
                    $scrubbedPhoneNumber = Yii::app()->format->formatInteger($_GET['phone']);
                    $command
                        ->andWhere('p.phone like "%'.$scrubbedPhoneNumber.'%"');
                }

                if($_GET['hasPhone']==1) {
                    $command
                        ->andWhere('p.phone > 0');
                }
            }

            if($_GET['callListId']) {
                $command->andWhere("(SELECT count(*) FROM call_list_phones clp JOIN phones p2 ON p2.id=clp.phone_id WHERE clp.hard_deleted IS NULL AND c.id=p2.contact_id AND clp.call_list_id=".intval($_GET['callListId']).") >= 1");
            }

            // narrows the result down
            if($_GET['massActionType'] && $_GET['massActionComponentId']) {

                switch($_GET['massActionType']) {
                    case 'unapplyActionPlans':
                        $command->andWhere("(SELECT count(*) FROM tasks tsk3 LEFT JOIN action_plan_applied_lu lu2 ON tsk3.id=lu2.task_id LEFT JOIN action_plan_items i2 ON i2.id=lu2.action_plan_item_id LEFT JOIN action_plans ap2 ON ap2.id=i2.action_plan_id WHERE ap2.id=".intval($_GET['massActionComponentId'])." AND tsk3.component_type_id=t.component_type_id AND tsk3.component_id=t.id AND tsk3.complete_date IS NULL AND tsk3.is_deleted=0) > 0");
                        break;
                }
            }

            if($_GET['actionPlanId']) {
                $command->andWhere("(SELECT count(*) FROM tasks tsk4 LEFT JOIN action_plan_applied_lu lu3 ON tsk4.id=lu3.task_id LEFT JOIN action_plan_items i3 ON i3.id=lu3.action_plan_item_id LEFT JOIN action_plans ap3 ON ap3.id=i3.action_plan_id WHERE ap3.id=".intval($_GET['actionPlanId'])." AND tsk4.component_type_id=t.component_type_id AND tsk4.component_id=t.id AND tsk4.complete_date IS NULL AND tsk4.is_deleted=0) > 0");
            }

            if($_GET['tags']) {
                $command->andWhere("(SELECT count(*) FROM transaction_tag_lu gu WHERE t.id=gu.transaction_id AND gu.transaction_tag_id IN(:tagIds)) > 0", array(':tagIds'=> implode(',', $_GET['tags'])));
            }

            if($_GET['noActionPlan']==1) {
                $command->andWhere("(SELECT count(*) FROM tasks tsk LEFT JOIN action_plan_applied_lu lu ON tsk.id=lu.task_id LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id LEFT JOIN action_plans ap ON ap.id=i.action_plan_id WHERE tsk.component_type_id=t.component_type_id AND tsk.component_id=t.id AND tsk.complete_date IS NULL AND tsk.is_deleted=0 ORDER BY ap.name ASC) < 1");
            }

            if($_GET['noTasks']==1) {
                $command->andWhere("(SELECT count(*) FROM tasks tsk2 WHERE tsk2.component_type_id=t.component_type_id AND tsk2.component_id=t.id AND tsk2.complete_date IS NULL AND tsk2.is_deleted=0) < 1");
            }

            if($_GET['noAssignment']==1) {
                $command->andWhere("(SELECT count(*) FROM assignments asmt WHERE asmt.component_type_id=t.component_type_id AND asmt.component_id=t.id AND asmt.is_deleted=0) < 1");
            }

            if($_GET['optOutBadEmails']==1) {
                if(!$_GET['email']) {
                    $command
                        ->leftJoin('emails e', 'e.contact_id = c.id');
                }
                $command->andWhere("e.email_status_id IN(".EmailStatus::OPT_OUT.")");
            }

            if($_GET['liveNow']==1) {
                $command->andWhere("c.last_login >= DATE_SUB(NOW(),INTERVAL 30 MINUTE)");
            }

            if($_GET['transactionStatusIds'] && !empty($_GET['transactionStatusIds'])) {

                if(!is_array($_GET['transactionStatusIds'])) {
                    $_GET['transactionStatusIds'] = (array) $_GET['transactionStatusIds'];
                }

                // remove any empty entries
                if(($emptyKey = array_search('', $_GET['transactionStatusIds'])) !== false) {
                    unset($_GET['transactionStatusIds'][$emptyKey]);
                }

                if($statusIds = implode(',', $_GET['transactionStatusIds'])) {
                    $command->andWhere('t.transaction_status_id IN ('.$statusIds.')');
                }

            } elseif(isset($_GET['transactionStatus'])) {
                if(!is_array($_GET['transactionStatus'])) {
                    $_GET['transactionStatus'] = (array) $_GET['transactionStatus'];
                }

                $statusIds = implode(',', $_GET['transactionStatus']);
                $command->andWhere('t.transaction_status_id IN ('.$statusIds.')');

            }elseif (!isset($_GET['transactionStatus']) && !isset($_GET['transactionStatusIds'])) { // && !isset($_GET['transactionTypeId'])
                $statusIds = array(TransactionStatus::NEW_LEAD_ID, TransactionStatus::HOT_A_LEAD_ID,TransactionStatus::B_LEAD_ID, TransactionStatus::C_LEAD_ID, TransactionStatus::D_NURTURING_SPOKE_TO_ID, TransactionStatus::NURTURING_ID, TransactionStatus::COMING_SOON_ID, TransactionStatus::ACTIVE_LISTING_ID, TransactionStatus::UNDER_CONTRACT_ID);
                $statusIds = implode(',', $statusIds);
                $command->andWhere('t.transaction_status_id IN ('.$statusIds.')');
            }

            if((!Yii::app()->user->checkAccess('owner') && Yii::app()->user->settings->show_assigned_leads_only)) {
                $_GET['assignmentId'] = Yii::app()->user->id;
                $_GET['myLeads'] = Yii::app()->user->id;
            }

            if($_GET['assignmentId'] || $_GET['myLeads']) {
                if($_GET['assignmentId']) {
                    $assignmentContactId = $_GET['assignmentId'];
                } elseif ($_GET['myLeads']) {
                    $assignmentContactId = $_GET['myLeads'];
                }

                $command
                    ->leftJoin('assignments a3', 't.id = a3.component_id AND t.component_type_id=a3.component_type_id')
                    ->andWhere('a3.assignee_contact_id = '.$assignmentContactId.' AND a3.is_deleted=0');
            }

            if($_GET['lastActivity'] && !empty($_GET['lastActivity'])) {
                $command
                    ->andWhere(' (select max(activity_date) from activity_log where component_type_id=t.component_type_id AND component_id=t.id AND task_type_id IN ('.$this->taskTypeIds.') AND is_deleted=0) <= "'.date("Y-m-d", strtotime('-'.$_GET['lastActivity'].' days')).'"');
            }

            if($_GET['noPhoneCall']==1) {
                $command
                    ->andWhere('(select count(*) from activity_log a4 WHERE (t.id=a4.component_id AND t.component_type_id=a4.component_type_id) AND (a4.task_type_id  IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.'))) < 1')
                    ->andWhere('(select count(*) from tasks t2 WHERE t2.task_type_id IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.') AND t2.component_id=t.id AND t2.component_type_id=t.component_type_id AND t2.complete_date IS NOT NULL) < 1');
            }

            if($_GET['noSavedSearch']==1) {
                $command
                    ->andWhere('(select count(*) from saved_home_searches s WHERE s.contact_id=t.contact_id and s.is_deleted=0) < 1');
            }

            if($_GET['noActivity']==1) {
                $command
                    ->andWhere(' (select count(*) from activity_log a5 where a5.component_type_id=t.component_type_id AND a5.component_id=t.id AND a5.task_type_id IN ('.$this->taskTypeIds.') AND a5.is_deleted=0) < 1');
            }

            if($_GET['notSpokeTo']==1) {
                $command
                    ->andWhere(' (select count(*) from activity_log a5 where a5.component_type_id=t.component_type_id AND a5.component_id=t.id AND a5.task_type_id IN ('.TaskTypes::PHONE.') AND a5.is_spoke_to=1 AND a5.is_deleted=0) < 1');
            }

            if($_GET['hasAppointment']==1) {
                $command
                    ->andWhere(' (select count(*) from appointments apt where apt.component_type_id=t.component_type_id AND apt.component_id=t.id AND apt.is_deleted=0) > 0');
            }

            if ($_GET['dateSearchFilter'] and $this->dateSearchTypes[$_GET['dateSearchFilter']]) {
                $fromSearchDate = Yii::app()->format->formatDate($_GET['fromSearchDate'], StmFormatter::MYSQL_DATE_FORMAT);
                $toSearchDate = Yii::app()->format->formatDate($_GET['toSearchDate'], StmFormatter::MYSQL_DATE_FORMAT);

                // for the time being just use a switch statement
                switch($_GET['dateSearchFilter']) {
                    case 'last_login_asc':
                    case 'last_login_desc':
                        if ($fromSearchDate) {
                            $command->andWhere('DATE(c.last_login) >= "'.$fromSearchDate.'"');
                        }

                        if ($toSearchDate) {
                            $command->andWhere('DATE(c.last_login) <= "'.$toSearchDate.'"');
                        }

                        $sortOrder = ($_GET['dateSearchFilter'] == 'last_login_asc')? 'asc' : 'desc';
                        $command->order("c.last_login $sortOrder");
                        break;

                    case 'added_asc':
                    case 'added_desc':
                        if ($fromSearchDate) {
                            $command->andWhere('DATE(t.added) >= "'.$fromSearchDate.'"');
                        }

                    if ($toSearchDate) {
                        $command->andWhere('DATE(t.added) <= "'.$toSearchDate.'"');
                    }
                        $sortOrder = ($_GET['dateSearchFilter'] == 'added_asc')? 'asc' : 'desc';
                        $command->order("t.added $sortOrder");
                        break;

                    case 'status_asc':
                        $transactionStatusOrder = implode(',', array(TransactionStatus::HOT_A_LEAD_ID, TransactionStatus::B_LEAD_ID, TransactionStatus::C_LEAD_ID, TransactionStatus::D_NURTURING_SPOKE_TO_ID, TransactionStatus::NURTURING_ID));
                        $command->order("FIELD(t.transaction_status_id, $transactionStatusOrder)");
                        break;

                }
            }

            if(isset($orderCommand) && !$count){
				$command->order($orderCommand);
			}

            /**
             * Reverse Prospecting Block
             */
            if(trim($_GET['reverseZip']) || trim($_GET['reversePriceMin']) || trim($_GET['reversePriceMax']) || trim($_GET['reverseDays'])  || trim($_GET['reverseCity'])) {

                // get the board Id and the mls table name //@todo: this will pull from viewed_homes table once the mls_board_id is fully plugged in
                $mlsTableName = Yii::app()->user->board->prefixedName.'_properties';

                $command
                    ->join('viewed_pages v', 'v.contact_id=c.id')
                    ->join("stm_mls.{$mlsTableName} m", 'm.listing_id=v.listing_id');

                if(trim($_GET['reverseZip'])) {
                    $reverseZips = explode(',', $_GET['reverseZip']);
                    if(!empty($reverseZips)) {
                        $command
                            ->andWhere(array('IN','m.zip', $reverseZips));
                    }
                }

                if(trim($_GET['reverseCity'])) {
                    $reverseCities = explode(',', $_GET['reverseCity']);
                    if(!empty($reverseCities)) {
                        $command
                            ->andWhere(array('IN','m.city', $reverseCities));
                    }
                }

                if(trim($_GET['reversePriceMin'])) {
                    $command
                        ->andWhere('m.price >=:reversePriceMin', array(':reversePriceMin'=>trim($_GET['reversePriceMin'])));
                }

                if(trim($_GET['reversePriceMax'])) {
                    $command
                        ->andWhere('m.price <=:reversePriceMax', array(':reversePriceMax'=>trim($_GET['reversePriceMax'])));
                }

                $reverseDays = (trim($_GET['reverseDays'])) ? trim($_GET['reverseDays']) : 120;

                $command
                    ->andWhere('DATE(v.added) >=:reverseDays', array(':reverseDays'=>date('Y-m-d', strtotime("-{$reverseDays} days"))));
            }

            return $command;
        }

        public function printGridIdTypeInfo($data)
        {
            $backgroundColor = ($data['componentTypeId']==2)? 'yellow': 'rgb(131, 239, 115)';
            $color = ($data['componentTypeId']==2)? 'black': 'black';
            echo '<div style="text-align:center; background-color: '.$backgroundColor.'; color: '.$color.'; text-shadow: 1px 1px 0 white; padding: 5px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;">';
            echo ($data['componentTypeId']==2)? 'Buyer': 'Seller';
            echo '</div><br />';
            echo $data[id];
        }

        public function printGridStatus($data)
        {
//            echo TransactionStatus::getTransactionStatusNameById($data['transactionStatusId']);
//            if($price = TransactionFieldValues::model()->findByAttributes(array('transaction_id'=>$data['id'], 'transaction_field_id'=>TransactionFields::CURRENT_PRICE_SELLER))) {
//                echo ' &nbsp;<span class="list-view-price">'.Yii::app()->format->formatDollars($price->value).'</span>';
//            }

            $classSuffix = '';
            $description = '';
            $text = '';
            switch($data['transactionStatusId']) {
                case TransactionStatus::NEW_LEAD_ID:
                    $classSuffix = 'New';
                    $text = 'N';
                    $description = 'Just Arrived';
                    break;

                case TransactionStatus::HOT_A_LEAD_ID:
                    $classSuffix = 'A';
                    $text = 'A';
                    $description = '30 Days';
                    break;

                case TransactionStatus::B_LEAD_ID:
                    $classSuffix = 'B';
                    $text = 'B';
                    $description = '60 Days';
                    break;

                case TransactionStatus::C_LEAD_ID:
                    $classSuffix = 'C';
                    $text = 'C';
                    $description = '90 Days';
                    break;

                case TransactionStatus::D_NURTURING_SPOKE_TO_ID:
                    $classSuffix = 'D';
                    $text = 'D';
                    $description = '120 Days';
                    break;

                case TransactionStatus::NURTURING_ID:
                    $classSuffix = 'E';
                    $text = 'E';
                    $description = 'No Spoke to';
                    break;

                case TransactionStatus::ACTIVE_LISTING_ID:
                    break;

                case TransactionStatus::UNDER_CONTRACT_ID:
                    break;

                case TransactionStatus::CLOSED_ID:
                    break;
            }

            echo ($text) ? "<span class='status status$text'>$text</span>" : null;
            echo ($description) ? "<br>$description" : null;
//            echo '<br>'.$data->componentType->singularName.'<br>';
//
//            echo Yii::app()->format->formatDate($data->target_date, "n/j/y");
        }

        public function printGridAssignments($data)
        {
            if($data['assignments'] = Yii::app()->db->createCommand("select assignment_type_id, assignee_contact_id from assignments where component_type_id=".$data['component_type_id']." AND component_id=".$data['id']." AND is_deleted=0")->queryAll()) {
                foreach($data['assignments'] as $assignment) {
                    $assignmentString .= (empty($assignmentString))? '' : '<br />';
                    $assignmentString .= Contacts::getAdminNameById($assignment['assignee_contact_id']).'<br><span style="font-size: 10px;">('.AssignmentTypes::getAssignmentTypeNameById($assignment['assignment_type_id']).')</span>';
                }
                echo $assignmentString;
            }
        }

        public function printGridActivityInfo($data)
        {
            echo Yii::app()->format->formatDate($data['added']);
        }

        public function printGridLastLogin($data)
        {
            self::$_nowTime = (self::$_nowTime) ? self::$_nowTime : Yii::app()->db->createCommand("SELECT DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL -30 MINUTE)")->queryScalar();
            $liveNowLabel = '<span style="background-color: #16dd21; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; top: 2px; margin-left: 4px; display: inline-block;">LIVE NOW</span>';
            echo Yii::app()->format->formatDays($data['lastLogin']).' ago'.(($data['lastLogin'] > date('Y-m-d H:i:s', strtotime('-30 minutes'))) ? $liveNowLabel : '');
        }

        protected function printActionPlan($data)
        {
            if($data['actionPlans']) {//
                echo '<i class="fa fa-check action-plan-status exists" data-tooltip="'.htmlspecialchars($data['actionPlans']).'">
                </i>';
//                <div>
//                '.$data['actionPlans'].'
//                </div>
                //$data['actionPlans'];
            }
            else {
                echo '<i class="fa fa-times action-plan-status"></i>';
//                $warningSpanStyle = ' style="background-color: #D20000; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; top: 5px; margin-bottom: 16px;"';
//                echo '<span'.$warningSpanStyle.'>NONE</span>';
            }
        }

//        public function printGridButton($data) {
//            $componentLinkName = ($data[componentTypeId]==ComponentTypes::BUYERS)? 'buyers': 'sellers';
//            $componentButtonName = ($data[componentTypeId]==ComponentTypes::BUYERS)? 'Buyer': 'Seller';
//
//            echo '<div><a href="/admin/'.$componentLinkName.'/'.$data['id'].'" class="button gray icon i_stm_search grey-button" target="_blank">View '.$componentButtonName.'</a></div>';
//        }

        public function printGridNextTask($data) {
            $data['nextTaskDate'] = Yii::app()->db->createCommand("select min(due_date) from tasks where component_type_id=".$data['component_type_id']." AND component_id=".$data['id']." AND due_date>='".date('Y-m-d')."' AND due_date<='".date("Y-m-d", strtotime('30 days'))."' AND is_deleted=0")->queryScalar();

//            $warningSpanStyle = ' style="background-color: #D20000; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; top: 5px; display: inline-block;"';
            $days = null;
            if(empty($data['nextTaskDate'])) {
                echo '<i class="fa fa-times action-plan-status"></i>';

//                echo '<span'.$warningSpanStyle.'>NONE</span>';
            } else {
                $days = null;
                switch($data['transactionStatusId']) {
                    case TransactionStatus::NEW_LEAD_ID:
                        $days = 1;
                        break;
                    case TransactionStatus::HOT_A_LEAD_ID:
                        $days = 7;
                        break;
                    case TransactionStatus::B_LEAD_ID:
                        $days = 14;
                        break;
                    case TransactionStatus::C_LEAD_ID:
                        $days = 30;
                        break;
                    case TransactionStatus::ACTIVE_LISTING_ID:
                        $days = 7;
                        break;
                    case TransactionStatus::NURTURING_ID:
                        $days = null;
                        break;
                }
                $nextTaskFormattedDate = Yii::app()->format->formatDays($data['nextTaskDate'], $opts=array('isFuture'=>true)).'<br>'.Yii::app()->format->formatDate($data['nextTaskDate']);

                if($days && (date('Y-m-d', strtotime($data['nextTaskDate'])) > date('Y-m-d', strtotime($days.' days')))) {
                    echo '<i class="fa fa-times action-plan-status"></i>';
//                    echo '<span'.$warningSpanStyle.'>'.$nextTaskFormattedDate.'</span>';
                } else {
                    echo $nextTaskFormattedDate;
                }
            }
        }

		public function contactViewedListingCount($data){
            $color = "#C80";
			return ($count = Yii::app()->db->createCommand("select count(id) from viewed_pages where contact_id=".$data['contact_id']." AND listing_id IS NOT NULL")->queryScalar()) ? "<div style='color: $color; font-weight: bold;'>".$count."<br>Viewed</div>" : "-";
		}

		public function contactSavedHomesCount($data){
            $color = "#00BD00";
			return ($count = Yii::app()->db->createCommand("select count(id) from saved_homes where contact_id=".$data['contact_id']." AND is_deleted=0")->queryScalar()) ? "<div style='color: $color; font-weight: bold;'>".$count."<br>Saved</div>" : "-";
		}

		public function contactSavedSearchesCount($data){
            $color = "#C80";
			return ($count = Yii::app()->db->createCommand("select count(id) from saved_home_searches where contact_id=".$data['contact_id']." AND is_deleted=0")->queryScalar()) ? $count."<br>Searches" : "-";
		}

//        public function printGridEngagement($data)
//        {
//			$data['viewedHomes'] = $this->contactViewedListingCount($data);
//			$data['savedHomes'] = $this->contactSavedHomesCount($data);
//			$data['searches'] = $this->contactSavedSearchesCount($data);
//
//            $viewedSpan = (empty($data['viewedHomes']))? ' style="width:33%; display: inline-block; float: left; text-align: center;"' : ' style="color: #C80; font-weight: bold; width:33%; display: inline-block; float: left; text-align: center;"';
//            $savedSpan = (empty($data['saved_home_count']))? ' style="width:33%; display: inline-block; float: left; text-align: center;"' : ' style="color: #00BD00; font-weight: bold; width:33%; display: inline-block; float: left; text-align: center;"';
//
//            echo '<div  '.$viewedSpan.'>'.$data['viewedHomes'].'<br>Viewed</div><div '.$savedSpan.'>'.$data['saved_home_count'].'<br>Saved</div><div style="width:33%; display: inline-block; float: left; text-align: center;">'.$data['saved_search_count'].'<br>Searches</div>';
//        }

        public function printGridLastActivity($data)
        {
            $data['lastActivityDate'] = Yii::app()->db->createCommand("select max(activity_date) from activity_log where component_type_id=".$data['component_type_id']." AND component_id=".$data['id']." AND task_type_id IN ($this->taskTypeIds) AND is_deleted=0")->queryScalar();

            $daysAgo = Yii::app()->format->formatDays($data['lastActivityDate']);
            echo ((empty($data['lastActivityDate']))? 'None' : (($daysAgo == 'Today') ? 'Today' : $daysAgo.' ago'));
        }

        public function printGridActionPlan($data)
        {
            // get list of active action plans. Need to distinguish between applied action plans that are already complete
            $data['actionPlans'] = Yii::app()->db->createCommand("select distinct ap.name from tasks t LEFT JOIN action_plan_applied_lu lu ON t.id=lu.task_id LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id LEFT JOIN action_plans ap ON ap.id=i.action_plan_id WHERE t.component_type_id=".$data['component_type_id']." AND t.component_id=".$data['id']." AND t.complete_date IS NULL AND t.is_deleted=0 ORDER BY ap.name ASC")->queryColumn();
            $data['actionPlans'] = implode(',', $data['actionPlans']);

            //            GROUP_CONCAT(distinct a2.assignment_type_id, ',', a2.assignee_contact_id SEPARATOR '|') as assignments

            $this->printActionPlan($data);
        }
    }