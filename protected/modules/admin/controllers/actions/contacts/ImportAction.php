<?php
/**
 * Import Action
 *
 * Used to import contacts
 */
class ImportAction extends CAction
{
    protected $_importLog = array();
    protected $_startTime;
    protected $_duplicateCount = 0;
    protected $_contactMissingDataCount = 0;

    protected $_sourceCollection;
    protected $_contactTypeCollection;

    /**
     * Send JSON
     *
     * Test function that should be moved up the chain of hierarchy later if found useful
     * @return void
     */
    protected function _sendJson($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    /**
     * Run
     *
     * This method is called when the page is loaded
     * @return void
     */
    public function run()
    {
        // Handle postback vs rendering page content
        if(Yii::app()->request->isPostRequest) {

            // Handle each type of post
            switch(Yii::app()->request->getPost('method')) {
                case 'import':
                    $this->_handleImport();
                break;
                case 'process-file':
                    $this->_handlePreviewFile();
                break;
            }
        }

        // Set page title
        $this->controller->title = 'Import Contacts';

        // Pass data to view
        $this->controller->render('import', array(
            // Data can be passed in to view here
        ));
    }

    /**
     * Handle Import
     *
     * Helper function to handle the actual import process
     * @return void
     */
    protected function _handleImport()
    {
        // clear the log
        $this->_importLog = array();
        $this->_startTime = microtime(true);

        // Retrieve and decode JSON for contacts to import
        $newContacts = CJSON::decode(Yii::app()->request->getPost('contacts'));
        if(!$newContacts) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'No contacts have been selected'
            ));
        }

        // Retrieve all states and build dictionaries for lookups of state IDs
        $stateFullNames = array();
        $stateAbbreviations = array();
        $stateResults = AddressStates::model()->findAll();
        foreach($stateResults as $stateResult) {
            $stateFullNames[strtolower($stateResult['name'])] = $stateResult['id'];
            $stateAbbreviations[strtolower($stateResult['short_name'])] = $stateResult['id'];
        }

        // Create a flat array of existing emails indexed by email address for lookups //@todo: pulling all email records doesn't seem scalable for large db's - need to optimize
        $emailsExisting = Yii::app()->db->createCommand("select LOWER(email) from emails WHERE email IS NOT NULL ORDER BY email ASC")->queryColumn();
        $emailsExisting = array_flip($emailsExisting);

        // Create a flat array of existing first and last names
        $namesExisting = Yii::app()->db->createCommand('select LOWER(CONCAT(TRIM(first_name)," ",TRIM(last_name))) fullName from contacts WHERE first_name != "" AND last_name !="" GROUP BY LOWER(CONCAT(TRIM(first_name)," ",TRIM(last_name))) ORDER BY TRIM(first_name)')->queryColumn();
        $namesExisting = array_flip($namesExisting);

        // Store all new contacts
        foreach($newContacts as $newContact) {

            // If we don't have an email or any phone numbers, skip this
            if(empty($newContact['email']) && empty($newContact['email_2']) && empty($newContact['email_3']) && empty($newContact['email_4']) && empty($newContact['phone_1']) && empty($newContact['phone_2']) && empty($newContact['phone_3']) & empty($newContact['phone_4'])) {
                $this->_importLog[] = 'Missing Data Alert! This contact has no phone or email and has not been imported. Contact Data: '.print_r($newContact, true);
                continue;
            }

            // Check for any of the 4 emails to import already exist
            if($this->_checkExist($newContact, 'email', $emailsExisting) || $this->_checkExist($newContact, 'email_2', $emailsExisting) || $this->_checkExist($newContact, 'email_3', $emailsExisting) || $this->_checkExist($newContact, 'email_4', $emailsExisting)) {

                // log error and skip this contact and move to next
                $this->_importLog[] = 'Duplicate alert! Email already exists for this contact. Contact Data: '.print_r($newContact, true);
                $this->_duplicateCount++;
                continue;
            }

            // Check first name and last name (not sure how to do this reliably yet) ???
            if(array_key_exists(strtolower($newContact['first_name'] . ' ' . $newContact['last_name']), $namesExisting)) {

                // exact name match found, log and skip record
                $this->_importLog[] = 'Duplicate alert! Exact name match found. Contact Data: '.print_r($newContact, true);
                $this->_duplicateCount++;
                continue;
            }

            // put existing phone in contactPhoneAdd property so it clears validation. A phone # may be in later position while first is blank.
            $contactPhoneAdd = trim($newContact['phone_1']);
            if(!$contactPhoneAdd) {
                $contactPhoneAdd = trim($newContact['phone_2']);
            }
            if(!$contactPhoneAdd) {
                $contactPhoneAdd = trim($newContact['phone_3']);
            }
            if(!$contactPhoneAdd) {
                $contactPhoneAdd = trim($newContact['phone_4']);
            }

            // put existing email in contactEmailAdd property so it clears validation. An email # may be in later position while first is blank.
            $contactEmailAdd = trim($newContact['email']);
            if(!$contactEmailAdd) {
                $contactEmailAdd = trim($newContact['email_2']);
            }
            if(!$contactEmailAdd) {
                $contactEmailAdd = trim($newContact['email_3']);
            }
            if(!$contactEmailAdd) {
                $contactEmailAdd = trim($newContact['email_4']);
            }

            // Create and save new contact record
            $contact = new Contacts();
            $contact->setAttributes(array(
                'account_id'        => Yii::app()->user->accountId,
                'source_id'         => 1,
                'contact_status_ma' => Contacts::STATUS_ACTIVE,
                'first_name'        => ucwords(strtolower(trim($newContact['first_name']))),
                'last_name'         => ucwords(strtolower(trim($newContact['last_name']))),
                'contactEmailAdd'   => $contactEmailAdd,
                'contactPhoneAdd'   => $contactPhoneAdd,
                'notes'             => trim($newContact['notes']),
            ));
            if(trim($newContact['added'])) {
                $contact->added = date('Y-m-d H:i:s', strtotime(trim($newContact['added'])));
            }

            // If we were unable to save this item, skip it
            if($contact->save()) {
                $namesExisting[strtolower($contact->first_name . ' ' . $contact->last_name)] = $contact;
            }
            else {
                $this->_contactMissingDataCount++;
                $this->_importLog[] = 'Error: Contact did not save. Error Message: '.print_r($contact->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                continue;
            }

            // link source name to contact, create source if not exist
            if($newContact['source']) {

                if(!isset($this->_sourceCollection[$newContact['source']])) {
                    if(($source = Sources::model()->findByAttributes(array('name'=>$newContact['source'])))) {
                        $this->_sourceCollection[$newContact['source']] = $source;
                    }
                    else {

                        $source = new Sources();
                        $source->account_id = Yii::app()->user->accountId;
                        $source->name = $newContact['source'];
                        if($source->save()) {
                            $this->_sourceCollection[$newContact['source']] = $source;
                        }
                        else {
                            $this->_importLog[] = 'Error: New Source did not save. Error Message: '.print_r($source->getErrors(), true).PHP_EOL.'Source Data: '.print_r($source->attributes, true);
                        }
                    }
                }

                $contact->source_id = $this->_sourceCollection[$newContact['source']]->id;
                if(!$contact->save()) {
                    $this->_importLog[] = 'Error: Contact did not save Source: '.print_r($this->_sourceCollection[$newContact['source']]->attributes, true).'. Error Message: '.print_r($contact->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($contact->attributes, true);
                }
            }

            // link contact tag to contact, create tag if not exist
            if($newContact['contactTags']) {

                $contactTags = explode(',', $newContact['contactTags']);
                foreach($contactTags as $contactTagName) {

                    // if the tag is not in collection, create and add.
                    if(!isset($this->_contactTypeCollection[$contactTagName])) {

                        // see if it already exists, if not create a new one
                        if(($contactTag = ContactTypes::model()->findByAttributes(array('name'=>$contactTagName)))) {
                            $this->_sourceCollection[$contactTagName] = $contactTag;
                        }
                        else {
                            $contactTag = new ContactTypes();
                            $contactTag->name = $contactTagName;
                            $contactTag->account_id = Yii::app()->user->accountId;
                            if(!$contactTag->save()) {
                                $this->_sourceCollection[$contactTagName] = $contactTag;
                            }
                            else {
                                $this->_importLog[] = 'Error: Contact Tag did not save. Error Message: '.print_r($contactTag->getErrors(), true).PHP_EOL.'Contact Tag Data: '.print_r($contactTag->attributes, true);
                            }
                        }
                    }

                    // double check to see that tag exists in collection before saving the lookup
                    if(isset($this->_contactTypeCollection[$contactTagName])) {
                        // create a new contact tag lookup
                        $contactTagLu = new ContactTypeLu();
                        $contactTagLu->contact_id = $contact->id;
                        $contactTagLu->contact_type_id = $this->_contactTypeCollection[$contactTagName]->id;
                        if(!$contactTagLu->save()) {
                            $this->_importLog[] = 'Error: Contact Tag Lookup did not save Data: '.print_r($contactTagLu->attributes, true).'. Error Message: '.print_r($contactTagLu->getErrors(), true);
                        }
                    }
                }
            }

            // Create and save new email record if we have one
            $newContact['email'] = trim($newContact['email']);
            if(!empty($newContact['email'])) {
                $email = new Emails();
                $email->setAttributes(array(
                        'account_id'        =>  Yii::app()->user->accountId,
                        'contact_id'        =>  $contact->id,
                        'email_status_id'   =>  1,
                        'email'             =>  $newContact['email'],
                        'email_type_ma'     =>  1,
                        'owner_ma'          =>  0,
                        'is_primary'        =>  1
                    ));

                if(!$email->save()) {
                    $this->_importLog[] = 'Error: Email #1 did not save. Error Message: '.print_r($email->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
                else {
                    $emailsExisting[$email->email] = $email;
                }
            }

            // Add second email if we have one
            $newContact['email_2'] = trim($newContact['email_2']);
            if(!empty($newContact['email_2'])) {
                $email = new Emails();
                $email->setAttributes(array(
                        'account_id'        =>  Yii::app()->user->accountId,
                        'contact_id'        =>  $contact->id,
                        'email_status_id'   =>  1,
                        'email'             =>  $newContact['email_2'],
                        'email_type_ma'     =>  1,
                        'owner_ma'          =>  0,
                        'is_primary'        =>  0
                    ));

                if(!$email->save()) {
                    $this->_importLog[] = 'Error: Email #2 did not save. Error Message: '.print_r($email->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
                else {
                    $emailsExisting[$email->email] = $email;
                }
            }

            // Add third email if we have one
            $newContact['email_3'] = trim($newContact['email_3']);
            if(!empty($newContact['email_3'])) {
                $email = new Emails();
                $email->setAttributes(array(
                        'account_id'        =>  Yii::app()->user->accountId,
                        'contact_id'        =>  $contact->id,
                        'email_status_id'   =>  1,
                        'email'             =>  $newContact['email_3'],
                        'email_type_ma'     =>  1,
                        'owner_ma'          =>  0,
                        'is_primary'        =>  0
                    ));

                if(!$email->save()) {
                    $this->_importLog[] = 'Error: Email #3 did not save. Error Message: '.print_r($email->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
                else {
                    $emailsExisting[$email->email] = $email;
                }
            }

            // Add fourth email if we have one
            $newContact['email_4'] = trim($newContact['email_4']);
            if(!empty($newContact['email_4'])) {
                $email = new Emails();
                $email->setAttributes(array(
                        'account_id'        =>  Yii::app()->user->accountId,
                        'contact_id'        =>  $contact->id,
                        'email_status_id'   =>  1,
                        'email'             =>  $newContact['email_4'],
                        'email_type_ma'     =>  1,
                        'owner_ma'          =>  0,
                        'is_primary'        =>  0
                    ));

                if(!$email->save()) {
                    $this->_importLog[] = 'Error: Email #4 did not save. Error Message: '.print_r($email->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
                else {
                    $emailsExisting[$email->email] = $email;
                }
            }

            // remove non-numeric
            $newContact['phone_1'] = Yii::app()->format->formatInteger($newContact['phone_1']);
            // Add first phone if we have one
            if(!empty($newContact['phone_1'])) {
                $phone = new Phones();
                $phone->setAttributes(array(
                    'account_id'        =>  Yii::app()->user->accountId,
                    'contact_id'        =>  $contact->id,
                    'phone_type_ma'     =>  1,
                    'phone'             =>  $newContact['phone_1'],
                    'owner_ma'          =>  0,
                    'is_primary'        =>  1
                ));

                if(!$phone->save()) {
                    $this->_importLog[] = 'Error: Phone #1 did not save. Error Message: '.print_r($phone->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
            }

            // remove non-numeric
            $newContact['phone_2'] = Yii::app()->format->formatInteger($newContact['phone_2']);
            // Add second phone if we have one
            if(!empty($newContact['phone_2'])) {
                $phone = new Phones();
                $phone->setAttributes(array(
                    'account_id'        =>  Yii::app()->user->accountId,
                    'contact_id'        =>  $contact->id,
                    'phone_type_ma'     =>  1,
                    'phone'             =>  $newContact['phone_2'],
                    'owner_ma'          =>  0,
                    'is_primary'        =>  0
                ));

                if(!$phone->save()) {
                    $this->_importLog[] = 'Error: Phone #2 did not save. Error Message: '.print_r($phone->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
            }

            // remove non-numeric
            $newContact['phone_3'] = Yii::app()->format->formatInteger($newContact['phone_3']);
            // Add third phone if we have one
            if(!empty($newContact['phone_3'])) {
                $phone = new Phones();
                $phone->setAttributes(array(
                    'account_id'        =>  Yii::app()->user->accountId,
                    'contact_id'        =>  $contact->id,
                    'phone_type_ma'     =>  1,
                    'phone'             =>  $newContact['phone_3'],
                    'owner_ma'          =>  0,
                    'is_primary'        =>  0
                ));

                if(!$phone->save()) {
                    $this->_importLog[] = 'Error: Phone #3 did not save. Error Message: '.print_r($phone->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
            }

            // remove non-numeric
            $newContact['phone_4'] = Yii::app()->format->formatInteger($newContact['phone_4']);
            // Add fourth phone if we have one
            if(!empty($newContact['phone_4'])) {
                $phone = new Phones();
                $phone->setAttributes(array(
                    'account_id'        =>  Yii::app()->user->accountId,
                    'contact_id'        =>  $contact->id,
                    'phone_type_ma'     =>  1,
                    'phone'             =>  $newContact['phone_4'],
                    'owner_ma'          =>  0,
                    'is_primary'        =>  0
                ));

                if(!$phone->save()) {
                    $this->_importLog[] = 'Error: Phone #4 did not save. Error Message: '.print_r($phone->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
            }

            // Determine state ID, if we have one
            $stateId = array_key_exists(strtolower(trim($newContact['state'])), $stateFullNames) ? $stateFullNames[strtolower(trim($newContact['state']))] : (array_key_exists(strtolower(trim($newContact['state'])), $stateAbbreviations) ? $stateAbbreviations[strtolower(trim($newContact['state']))] : null);

            $newContact['address'] = trim($newContact['address']);
            $newContact['city'] = trim($newContact['city']);
            $newContact['zip'] = trim($newContact['zip']);
            // If we have any address parts
            if(!empty($newContact['address']) && !empty($newContact['city']) && (!empty($stateId)) && !empty($newContact['zip']) && is_numeric($newContact['zip'])) {

                // Create and save new address record
                $address = new Addresses();
                $address->setAttributes(array(
                    'account_id'    =>  Yii::app()->user->accountId,
                    'address'       =>  $newContact['address'],
                    'city'          =>  $newContact['city'],
                    'state_id'      =>  $stateId,
                    'zip'           =>  $newContact['zip']
                ));

                if(!$address->save()) {
                    $this->_importLog[] = 'Error: Address did not save. Error Message: '.print_r($address->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }

                // Link address to contact
                $addressContact = new AddressContactLu();
                $addressContact->setAttributes(array(
                    'address_id'    =>  $address->id,
                    'contact_id'    =>  $contact->id,
                    'is_primary'    =>  1
                ));

                if(!$addressContact->save()) {
                    $this->_importLog[] = 'Error: Address Contact Lookup did not save. Error Message: '.print_r($addressContact->getErrors(), true).PHP_EOL.'Contact Data: '.print_r($newContact, true);
                }
            }
            elseif($newContact['address'] || $newContact['city'] || $newContact['state'] || $newContact['zip']) {
                $this->_importLog[] = 'Error: Address has missing data.'.PHP_EOL.'Contact Data: '.print_r($newContact, true);
            }
        }

        // send log results
        $this->_emailLog();

        // End
        $this->_sendJson(array(
            'status'    =>  'success'
        ));
    }

    /**
     * Checks to see if value exists
     *
     * return boolean
     */
    protected function _checkExist($newContact, $fieldName, $existingData)
    {
        if(!$newContact[$fieldName]) {
            return false;
        }
        return array_key_exists(strtolower($newContact[$fieldName]), $existingData);
    }

    protected function _emailLog()
    {
        $now = microtime(true);
        $runtimeSeconds = round($now - $this->_startTime, 2);


        $body = '';
        $body .= 'Please see below for import error logs. '.$this->_contactMissingDataCount.' Contacts had missing data and were not saved. Please review log and re-import after updating contact data or manually enter. '.$this->_duplicateCount.' duplicates have been found and not imported. The duplicate contacts have been logged below with full contact data.';

        $body .= '<br><br>This import was performed by '.Yii::app()->user->contact->fullName.' and completed on '.date('Y-m-d H:i:s').'<br><br>';

        if(is_array($this->_importLog)) {
            foreach($this->_importLog as $log) {
                $body .= '<br>---------------------------------------------------------------------------------------------------------------<br>';
                $body .= $log.'<br>';
            }

            $body .= '<br><br>---------------------------------------------------------------------------------------------------------------';
            $body .= '<br>Import Complete.<br>Duration: ' . $runtimeSeconds . ' seconds';
            $body .= '<br><br>This is the end of your Import Log.';
            $body .= '<br><br>Thank you.<br><br>Seize the Market';

            StmZendMail::easyMail(array('donotreply@seizethemarket.com'=>'STM Notification'), Yii::app()->user->contact->primaryEmail, 'Import Completion & Log - '.Yii::app()->user->domain->name, $body, 'debug@seizethemarket.com');
        }
    }

    /**
     * Handle Process File
     *
     * Handles processing a file
     * @throws Exception When issues have come up with a file upload
     */
    protected function _handlePreviewFile()
    {
        // Make sure we had a file upload
            if(!$_FILES['contacts-file']) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'No file upload detected'
            ));
        }

        // this setting is required for Mac environment
        ini_set("auto_detect_line_endings", true);

        // Make sure it is actually an uploaded file and not some hack
        if(!is_uploaded_file($_FILES['contacts-file']['tmp_name'])) {
            throw new Exception('File upload is not really a file upload!');
        }

        // Use file info extension to make sure the file is a proper csv file and not some weird file, we can't process excel files
        $info = new finfo(FILEINFO_MIME_TYPE);
        if(!in_array($info->file($_FILES['contacts-file']['tmp_name']), array('text/plain','text/csv','text/tsv'))) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'The selected file is not a proper csv file, please make sure it is a plain text file.'
            ));
        }
        $csvData = array();
        $row = 1;
        $maxNum = 99999;
        $charset = null;
        if (($handle = fopen($_FILES['contacts-file']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",")) !== FALSE && ($row < $maxNum)) {
                if(!$charset) {
                }

                // prevents yii grid erroring out due to charset issues
                array_walk($data, function(&$value, $key) {
//                    $charset = mb_detect_encoding($value, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
//                    $value = mb_convert_encoding($value,'UTF-8',$charset);
                    // removes any weird encoding
                    $charset = mb_detect_encoding($value, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                    if($charset !== 'UTF-8') {
                        $value = iconv($charset,'UTF-8//IGNORE', $value);
                    }
                });

                if($row == 1 && $_POST['ignoreFirstRow']) {
                    $row++;
                    continue;
                }
                // $num = count($data);
                $row++;
                if(count($data) !== 18) {
                    $this->_sendJson(array(
                            'status'    =>  'error',
                            'message'   =>  'Column do not match the required format.'
                        ));
                    Yii::app()->end();
                }

                $csvData[] = $data;
            }
            fclose($handle);
        }

        // Simply return the contents for the view to process
        $this->_sendJson(array(
            'status'    =>  'success',
            'data'      =>  $csvData,
        ));
    }
}