<?php

class ViewedPagesAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		$viewedPagesProvider = ViewedPages::getAll($id);
		$this->controller->renderPartial('_viewedPages',array(
			'dataProvider'=>$viewedPagesProvider,
		));
	}
}