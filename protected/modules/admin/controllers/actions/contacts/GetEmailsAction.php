<?php
    class GetEmailsAction extends CAction
    {
        public function run($id)
        {
            $emails = array();
            $results = Yii::app()->db->createCommand()
                        ->select('id, email')
                        ->from('emails')
                        ->where('contact_id='.intval($id))
                        ->andWhere('is_deleted=0')
                        ->queryAll();

            foreach($results as $row) {
                $emails[$row['id']] = $row['email'];
            }
            echo CJSON::encode(array('status'=>'success','emails'=>$emails));
            Yii::app()->end();
        }
	}
