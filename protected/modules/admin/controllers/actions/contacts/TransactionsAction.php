<?php

class TransactionsAction extends CAction {
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
    {
        // columns of status, details, assigned_to, submit_date, button
        $data = array();

        // get buyers/sellers
        if($transactions = Yii::app()->db->createCommand("select t.id, t.component_type_id, IF(t.component_type_id = 2, 'buyers', 'sellers') as componentName, IF(t.component_type_id = 2, 'Buyer', 'Seller') as componentLabel, ts.name statusName, t.added, a.address, a.city, s.short_name, a.zip from transactions t LEFT JOIN transaction_status ts ON ts.id=t.transaction_status_id LEFT JOIN addresses a ON a.id=t.address_id LEFT JOIN stm_hq.address_states s ON s.id=a.state_id WHERE t.contact_id=".$id." AND t.is_deleted=0")->queryAll()) {
            foreach($transactions as $transaction) {
                $tData = array();
                $tData['status'] = $transaction['componentLabel'].'<br />'.$transaction['statusName'];
                if($transaction['address']) {
                    $tData['details'] = $transaction['address'].'<br />'.$transaction['city'].', '.$transaction['short_name'].' '.$transaction['zip'];
                }
                if($assignments = Yii::app()->db->createCommand('select CONCAT(c.first_name," ",c.last_name) fullName from assignments a LEFT JOIN contacts c ON c.id=a.assignee_contact_id WHERE a.component_type_id='.$transaction['component_type_id'].' AND a.component_id='.$transaction['id'])->queryScalar()) {
                    $tData['assigned_to'] = $assignments;
                }
                $tData['submit_date'] = Yii::app()->format->formatDateDays($transaction['added']);
                $tData['button'] = $this->printButton($transaction['componentName'], $transaction['id']);
                $data[] = $tData;
            }
        }

        // get recruits
//        $recruits = Yii::app()->db->createCommand("select id, ".ComponentTypes::RECRUITS." component_type_id from recruits where contact_id=".$id)->queryColumn();

        // get referrals
        if($referrals = Yii::app()->db->createCommand('select r.id, CONCAT(c.first_name," ",c.last_name) fullName, r.added from referrals r LEFT JOIN contacts c ON c.id=r.assigned_to_id WHERE r.contact_id='.$id)->queryAll()) {
            foreach($referrals as $referral) {
                $rData = array();
                $rData['status'] = 'Referral<br />';
                $rData['assigned_to'] = $referral['fullName'];
                $rData['submit_date'] = Yii::app()->format->formatDateDays($referral['added']);
                $rData['button'] = $this->printButton('referrals', $referral['id']);
                $data[] = $rData;
            }
        }

		$dataProvider = new CArrayDataProvider($data, array());

		$this->controller->renderPartial('_transactions',array(
			'dataProvider'=>$dataProvider,
		));
	}

    protected function printButton($componentName, $componentId)
    {
        return '<a class="btn btn-primary button default" target="_blank" title="View" href="/admin/'.$componentName.'/'.$componentId.'">View</a>';
    }
}
