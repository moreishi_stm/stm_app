<?php

	class EmailCheckAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {
			if (Yii::app()->request->isAjaxRequest) {
				if(isset($_POST['email'])) {
					if(Emails::isBounced($_POST['email'])) {
						echo 'false';
					} else {
						echo 'true';
					}
				}
				Yii::app()->end();
			}
		}
	}
