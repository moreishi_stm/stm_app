<?php
    class SendPasswordAction extends CAction {

        public function run() {
            $model = new Contacts;
            $this->performAjaxRequest($model);
        }

        protected function performAjaxRequest(Contacts $model) {
            if (Yii::app()->request->isAjaxRequest)
            {
                $model = $model->findByPk($_POST['contactId']);

                if (isset($_POST['contactId']))
                {
                    $model->attributes = $_POST["Contacts"];

                    $domainName = Yii::app()->user->domain->name;

                    $EmailMessage = new EmailMessage;
                    $EmailMessage->subject = 'Password Recovery';
                    $EmailMessage->component_type_id = ComponentTypes::CONTACTS;
                    $EmailMessage->component_id = $model->id;
                    $EmailMessage->to_email_id = $model->getPrimaryEmailObj()->id;
                    $EmailMessage->from_email_id = Yii::app()->user->contact->getPrimaryEmailObj()->id;
                    if(!$EmailMessage->save()) {
                        echo CJSON::encode(array('result'=>'error'));
                        Yii::app()->end();
                    }

                    $EmailMessage->content = $this->controller->renderFile(YiiBase::getPathOfAlias('admin_module.views.emails.sendPasswordEmail').'.php', array(
                            'contact' => $model,
                            'domainName' => $domainName,
                            'officeName' => Yii::app()->getUser()->getSettings()->office_name,
                            'officePhone' => Yii::app()->getUser()->getSettings()->office_phone,
                        ), $returnContent=true);

                    // Create email message
                    $zendMessage = new StmZendMail();
                    $zendMessage->addTo($EmailMessage->toEmail->email, Yii::app()->format->formatProperCase($EmailMessage->toEmail->contact->getFullName()));
                    $replyTo = $zendMessage->getReplyToEmail($EmailMessage, $domainName);
                    $zendMessage->setFrom($replyTo, Yii::app()->format->formatProperCase($EmailMessage->fromEmail->contact->getFullName()));
                    $zendMessage->setReplyTo($replyTo);
                    $zendMessage->setSubject($EmailMessage->subject);
                    $zendMessage->setBodyHtmlTracking($EmailMessage, $domainName);

                    try {
                        $zendMessage->checkBounceUndeliverable = false;
                        $zendMessage->send(StmZendMail::getAwsSesTransport());
                        $mailSent =true;
                    }
                    catch(Exception $e) {
                        echo CJSON::encode(array('result'=>'error'));
                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') Send Password Error sending email (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage(), CLogger::LEVEL_ERROR);
                        Yii::app()->end();
                    }

                }

                echo CJSON::encode(array('result'=>'success'));
                Yii::app()->end();
            }
        }
	}
