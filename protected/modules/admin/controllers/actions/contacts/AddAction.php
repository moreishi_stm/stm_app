<?php

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class AddAction extends CAction {

		public function run($componentName = 'contacts') {
            Yii::app()->user->setReturnUrl(null);
            $componentNameTitle = ucwords(Yii::app()->format->formatSingular($componentName));
			$componentTypeId = ComponentTypes::getByName($componentName)->id;

			$this->controller->title = 'Add New ' . $componentNameTitle;
			switch ($componentTypeId) {
				case ComponentTypes::BUYERS:
					$this->controller->pageColor = 'yellow';
					break;

				case ComponentTypes::SELLERS:
					$this->controller->pageColor = 'green';
					break;

				case ComponentTypes::RECRUITS:
					$this->controller->pageColor = 'blue';
					break;
			}

			$model = new Contacts; // @todo: why two instantiated copies of contacts ... - per CLee => to avoid data dup/collision in forms, had to manually change id & name, is that enough
            $model->addressToAdd = new Addresses();
			$ContactToAdd = new Contacts;
            $ContactToAdd->addressToAdd = new Addresses();

			if (Yii::app()->request->isAjaxRequest) {
				$model = $this->processAjaxRequest($model);
			}

			if (isset($_POST['ContactToAdd'])) {
				$ContactToAdd->attributes = $_POST['ContactToAdd'];
                $ContactToAdd->addressToAdd->attributes = $_POST['Addresses'];
				$ContactToAdd->account_id = Yii::app()->user->accountId;
				$ContactToAdd->contact_status_ma = Contacts::STATUS_ACTIVE;
				if ($ContactToAdd->save()) {

					$emailSaved = false;
					if ($ContactToAdd->contactEmailAdd) {

                        // if email exists as deleted in another scenario
                        $existingEmailCriteria = new CDbCriteria();
                        $existingEmailCriteria->compare('is_deleted', 1);
                        $existingEmailCriteria->compare('email', trim($ContactToAdd->contactEmailAdd));

                        // check to see if this email exists as deleted. If so move it over and save it
                        if($Email = Emails::model()->skipSoftDeleteCheck()->find($existingEmailCriteria)) {
                            $Email->contact_id = $ContactToAdd->id;
                            $Email->is_deleted=0;
                        }
                        else {
                            $Email = new Emails;
                            $Email->email = $ContactToAdd->contactEmailAdd;
                            $Email->contact_id = $ContactToAdd->id;
                            $Email->account_id = Yii::app()->user->accountId;
                        }
                        $emailSaved = $Email->save();
					}

					$phoneSaved = false;
					if ($ContactToAdd->contactPhoneAdd) {
						$Phone = new Phones;
						$Phone->phone = $ContactToAdd->contactPhoneAdd;
						$Phone->contact_id = $ContactToAdd->id;
						$Phone->account_id = Yii::app()->user->accountId;
						$phoneSaved = $Phone->save();
					}

                    $addressSaved = false;
                    if ($ContactToAdd->addressToAdd) {
                        if($addressSaved = $ContactToAdd->addressToAdd->save()) {
                            // link address to contact and transaction
                            $addressLookup = new AddressContactLu();
                            $addressLookup->setAttributes(array(
                                    'address_id' => $ContactToAdd->addressToAdd->id,
                                    'contact_id' => $ContactToAdd->id,
                                ));
                            $addressLookup->save();
                        }
                    }
                }

				// after contact info updated to pass validation, forward to correct component
				// Can have one or the other so, better to validate that either or are set
				if ($phoneSaved || $emailSaved) { //@todo: What is phone or email failed when they were SUPPOSED to save?
					if ($componentName != 'contacts') {
						$this->controller->redirect(array(
								"/".$this->controller->module->name."/" . $componentName . "/add",
								'contactId' => $ContactToAdd->id,
								'source' => $ContactToAdd->source_id,
                                'addressId' => (!$addressLookup->isNewRecord) ? $addressLookup->address_id : '',
							)
						);
					} else {
						$this->controller->redirect(array(
								"/".$this->controller->module->name."/" . $componentName . "/edit",
								'id' => $ContactToAdd->id,
							)
						);
					}
				} else {
					$model = $ContactToAdd;
				}
			}

			$ContactData = $this->getContactsDataProvider($model); //@todo: this is somehow resetting $model acting like its passed by reference.
			$DataProvider = $ContactData['DataProvider'];
			$exactEmailMatch = $ContactData['exactEmailMatch'];
            $exactLastNamePhoneMatch = $ContactData['exactLastNamePhoneMatch'];

            //@todo: Address match also - CLee 4/4/16

			$this->controller->render('add', array(
					'model' => $model,
					'ContactToAdd' => $ContactToAdd,
					'componentTitle' => $componentNameTitle,
					'componentName' => $componentName,
					'componentTypeId' => $componentTypeId,
					'exactEmailMatch' => $exactEmailMatch,
                    'exactLastNamePhoneMatch' => $exactLastNamePhoneMatch,
					'DataProvider' => $ContactData['DataProvider'],
				)
			);
		}

		protected function processAjaxRequest(Contacts $model)
        {
			if (Yii::app()->request->isAjaxRequest) {

				if ($_POST['Contacts']) {

					$model->attributes = $_POST['Contacts'];
                    $model->addressToAdd->attributes = $_POST['Addresses'];

                    // validates contacts model
					$v1 = CJSON::decode(CActiveForm::validate($model));
                    $v2 = array();
                    // validates address model if exists
                    if($model->addressToAdd->address || $model->addressToAdd->city || $model->addressToAdd->state_id || $model->addressToAdd->zip) {
                        $v2 = CJSON::decode(CActiveForm::validate($model->addressToAdd));

                    }
                    // merges the results from the 2 models together
                    $v3 = CMap::mergeArray($v1, $v2);
                    echo CJSON::encode($v3);

					Yii::app()->end();
				} elseif ($_GET['Contacts']) {
					return $this->loadContactGridRequest($model);
				}
			}
		}

		protected function loadContactGridRequest($model) {
			if (isset($_GET['Contacts'])) {
				$model->attributes = $_GET['Contacts'];
			}

			if (isset($_GET['Emails'])) {
				$model->emails->attributes = $_GET['Emails'];
			}

			if (isset($_GET['Phones'])) {
				$model->phones->attributes = $_GET['Phones'];
			}

			return $model;
		}

		protected function getContactsDataProvider($Contact) {

			if (Yii::app()->request->isAjaxRequest && isset($_GET['Contacts'])) {
				$exactEmailMatch = $this->isExactEmailMatch($Contact->contactEmailAdd);
				$exactPhoneMatch = $this->isExactPhoneMatch($Contact->contactPhoneAdd);
                $addressMatch = $this->isAddressMatch($Contact->addressToAdd);
                $exactLastNamePhoneMatch = $this->isExactLastNamePhoneMatch($Contact->last_name, $Contact->contactPhoneAdd);
                $exactMatches = array_merge($exactEmailMatch, $exactPhoneMatch);

				if (!empty($exactMatches)) {
					$Criteria = new CDbCriteria;
					$Criteria->addInCondition('id', $exactMatches);
					$DataProvider = new CActiveDataProvider($Contact, array('criteria' => $Criteria));
				} else {
					$Criteria = new CDbCriteria;
					$Criteria->compare('first_name', $_GET['Contacts']['first_name'], true);
					$Criteria->compare('last_name', $_GET['Contacts']['last_name'], true);
					$DataProvider = new CActiveDataProvider($Contact, array('criteria' => $Criteria));
				}
			}
			if (!isset($DataProvider)) {
				$Contact->unsetAttributes();
				$Contact->id = 0;
				$DataProvider = $Contact->searchBeforeAdd();
			}

            $data['exactLastNamePhoneMatch'] = $exactLastNamePhoneMatch;
			$data['exactEmailMatch'] = $exactEmailMatch;
			$data['DataProvider'] = $DataProvider;

			return $data;
		}

        protected function isExactLastNamePhoneMatch($lastName, $phone) {
            $ids = array();
            $phone = Yii::app()->format->formatInteger($phone);
            if ($lastName) {
                $ids = Yii::app()->db->createCommand() //"select c.id FROM contacts c JOIN phones p ON c.id=p.contact_id WHERE "
                    ->select('c.id')
                    ->from('contacts c')
                    ->join('phones p','c.id=p.contact_id')
                    ->where("p.is_deleted=0 AND p.phone=:phone", array(':phone' => $phone))
                    ->andWhere("TRIM(LOWER(c.last_name))=:lastName",array(':lastName' => trim(strtolower($lastName))))
                    ->queryColumn();
            }

            return $ids;
        }

		protected function isExactEmailMatch($email) {
			$ids = array();
			if ($email) {
				$results = Emails::model()->byEmail($email)->findAll();
				if (count($results)) {
					foreach ($results as $model) {
						array_push($ids, $model->contact_id);
					}
				}
			}

			return $ids;
		}

		protected function isExactPhoneMatch($phone) {
			$ids = array();
			if ($phone) {
				$results = Phones::model()->byPhoneNumber($phone)->findAll();
				if (count($results)) {
					foreach ($results as $model) {
						array_push($ids, $model->contact_id);
					}
				}
			}

			return $ids;
		}

        protected function isAddressMatch($address) {
            $ids = array();
            if ($address) {
                $ids = Yii::app()->db->createCommand() //"select c.id FROM contacts c JOIN phones p ON c.id=p.contact_id WHERE "
                    ->select('c.id')
                    ->from('contacts c')
                    ->join('address_contact_lu lu','c.id=lu.contact_id')
                    ->join('addresses a','a.id=lu.address_id')
                    ->where("a.is_deleted=0 AND lu.is_deleted AND a.address like TRIM(:address)", array(':address' => "%".trim(substr($address->address, 0, Addresses::COMPARE_LENGTH))."%"))
                    ->queryColumn();
            }

            return $ids;
        }

        public function printGridColumnAddButton($componentTypeId, $componentName, $componentTitle, $data)
        {
            // check to see if has any other components
            switch($componentTypeId) {
                case ComponentTypes::SELLERS:
                case ComponentTypes::BUYERS:
                    $model = Transactions::model()->countByAttributes(array('contact_id'=>$data->id));
                    break;

                case ComponentTypes::RECRUITS:
                    $model = Recruits::model()->countByAttributes(array('contact_id'=>$data->id));
                    break;

                case ComponentTypes::CONTACTS:
                    // contacts doesn't have button?? need to verify
                    return;
                    break;
            }

            if(!$model) {
                $string = "<div><a href=\"/".Yii::app()->controller->module->id."/{$componentName}/add/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_add grey-button\">Add New {$componentTitle} </a></div>";
            }
            else {
                $string = "<div class='overdue-task-flag' style='margin-bottom: 10px;'>Review existing records to the left to prevent duplication.</div>";
                //$string .= "<div style='margin-bottom: 10px;'>NOTE: Review and click on the existing records to the left to prevent duplicate records.</div>";
                $string .= "<div><a href=\"/".Yii::app()->controller->module->id."/{$componentName}/add/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_add grey-button\">Add New {$componentTitle} </a></div>";
            }

            return $string;
        }
	}