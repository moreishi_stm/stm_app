<?php

class HomesViewedAction extends CAction {
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
    {
//        $mlsTable = Yii::app()->user->board->prefixedName.'_properties';
//        $linkState = strtolower(AddressStates::getShortNameById(Yii::app()->user->board->state_id));
//
//        $data = Yii::app()->db->createCommand()
//            ->select('v.added, m.listing_id, m.price, m.sq_feet, m.bedrooms, m.baths_full, m.common_subdivision, CONCAT(IFNULL(m.street_number,"")," ", IFNULL(m.street_direction_prefix,""), " ", IFNULL(m.street_name,""), " ", IFNULL(m.street_suffix,""), " ", IFNULL(m.street_direction_suffix,""), " ", IFNULL(m.unit_number,"")) streetAddress, m.city, "'.$linkState.'" as state, SUBSTRING(m.zip,1,5) zip')
//            ->from('viewed_pages v')
//            ->leftJoin('stm_mls.'.$mlsTable.' m', 'v.listing_id=m.listing_id')
//            ->where("v.contact_id=".$id)
//            ->andWhere("v.listing_id IS NOT NULL")
//            ->andWhere("v.page_type_ma=".ViewedPages::HOME_PAGES)
//            ->order('added desc')
//            ->limit(30)
//            ->queryAll();

        $data = ViewedPages::model()->getViewedHomesData($id);

        $dataProvider = new CArrayDataProvider($data, array(
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));

		$this->controller->renderPartial('_homesViewed',array(
			'dataProvider'=>$dataProvider,
		));
	}
}
