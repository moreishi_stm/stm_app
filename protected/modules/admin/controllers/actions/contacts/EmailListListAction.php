<?php
    class EmailListAction extends CAction {

        public function run()
        {
            $this->controller->title = 'Email List';
            $model = new Emails;
            $model->notAdmin = true;

            if (isset($_GET['Emails'])) {
                $model->attributes=$_GET['Emails'];
            }

            $this->controller->render('emailList', array('model'=>$model));
        }
	}
