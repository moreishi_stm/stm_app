<?php

class EmailAutocompleteAction extends CAction {

	public function run() {
		$model = new Emails;
		$this->processAjaxRequest($model);
	}

	protected function processAjaxRequest(Emails $model)
	{
		if (Yii::app()->request->isAjaxRequest) {

			if (isset($_POST['term'])) {
				$collection = array();

				$Criteria = new CDbCriteria;
				$Criteria->compare('email', trim(Yii::app()->request->getPost('term')), true);

				foreach(Emails::model()->findAll($Criteria) as $email)
					array_push($collection, array(
						'value'=>$email->id,
						'text'=>$email->contact->fullName,
						'email'=>$email->email,
					));

				echo CJSON::encode($collection);
			}
			Yii::app()->end();
		}
	}
}
