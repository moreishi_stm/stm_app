<?php

class AutocompleteAction extends CAction {

	public function run() {
		$model = new Contacts;
		$this->processAjaxRequest($model);
	}

	protected function processAjaxRequest(Contacts $model)
	{
		if (Yii::app()->request->isAjaxRequest) {

			if (isset($_POST['term'])) {
				$collection = array();

                $emailSubselect = "(SELECT email from emails e WHERE c.id=e.contact_id ORDER BY is_primary DESC, id DESC LIMIT 1)";
                $contacts = Yii::app()->db->createCommand()
                    ->select("c.id, {$emailSubselect} email, TRIM(CONCAT(TRIM(first_name), ' ', TRIM(last_name))) name")
                    ->from('contacts c')
                    ->where('TRIM(CONCAT(TRIM(first_name), " ", TRIM(last_name))) like :name', array(':name'=> '%'.trim(Yii::app()->request->getPost('term').'%')))
                    ->andWhere('contact_status_ma <> 0')
                    ->queryAll();

                foreach($contacts as $contact) {

                    array_push($collection, array(
                            'value'=>$contact['id'],
                            'text'=>$contact['name'],
                            'email'=>$contact['email'],
                        ));
                }

				echo CJSON::encode($collection);
			}
			Yii::app()->end();
		}
	}
}
