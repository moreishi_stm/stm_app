<?php

/**
 * Creates a new task.
 *
 */
class PhoneStatusAction extends CAction {

    public function run() {
        $model = new Phones;
        $this->performAjaxRequest($model);
    }

    protected function performAjaxRequest(Phones $model)
    {
        if (1 || Yii::app()->request->isAjaxRequest) {

            if(!$_POST['phoneOriginComponentId'] || !$_POST['phoneOriginComponentTypeId'])  {
                echo CJSON::encode(array('status'=>'error', 'message'=> 'Missing Component information. Please refresh the page and try again. Contact support if issue persists.'));
                Yii::app()->end();
            }


            if (isset($_POST['Phones'])) {

                $model->attributes = $_POST["Phones"];
                if($model = $model->findByPk($model->id)) {

                    switch($_POST['actionType']) {
                        case 'deleteBadNumber':
                            $model->delete();
                            if ($model->is_deleted == true) {
                                $activityLog = new ActivityLog;
                                $activityLog->account_id = Yii::app()->user->accountId;
                                $activityLog->component_type_id = ComponentTypes::CONTACTS;
                                $activityLog->component_id = $model->contact->id;
                                $activityLog->task_type_id = TaskTypes::BAD_PHONE;
                                $activityLog->activity_date = new CDbExpression('NOW()');
                                $activityLog->note = 'Deleted bad phone #: '.Yii::app()->format->formatPhone($model->phone);

                                if(!$activityLog->save()) {
                                    $errors = $activityLog->getErrors();
                                    Yii::log('Could not log activity to contact for deleting phone #. Model Error: '.print_r($errors,true), CLogger::LEVEL_ERROR, 'ContactPortlet');
                                }

                                $activityLog = new ActivityLog;
                                $activityLog->account_id = Yii::app()->user->accountId;
                                $activityLog->component_type_id = $_POST['phoneOriginComponentTypeId'];
                                $activityLog->component_id = $_POST['phoneOriginComponentId'];
                                $activityLog->task_type_id = TaskTypes::BAD_PHONE;
                                $activityLog->activity_date = new CDbExpression('NOW()');
                                $activityLog->note = 'Deleted bad phone #: '.Yii::app()->format->formatPhone($model->phone);

                                if(!$activityLog->save()) {
                                    $errors = $activityLog->getErrors();
                                    Yii::log(__CLASS__.' (:'.__LINE__.') Could not log activity to transaction for deleting phone #. Model Error: '.print_r($errors,true), CLogger::LEVEL_ERROR, 'ContactPortlet');
                                }

                                echo CJSON::encode(array('status'=>'success', 'message'=>'Bad Phone # deleted and logged successfully.'));
                                Yii::app()->end();

                            } else {
                                Yii::log(__CLASS__.' ('.__LINE__.') Could not delete phone #. Model Error: '.print_r($model->getErrors(),true), CLogger::LEVEL_ERROR, 'ContactPortlet');

                                echo CJSON::encode(array('status'=>'error', 'message'=> 'Phone # did not deleted. Error Message: ' . current($model->getErrors())[0]));
                                Yii::app()->end();
                            }
                            break;

                        case 'addDoNotCall':
                            $componentTypeId = $_POST['phoneOriginComponentTypeId'];
                            $componentId = $_POST['phoneOriginComponentId'];
                            if(!($days = $_POST['doNotCallDays'])) {
                                echo CJSON::encode(array('status'=>'error', 'message'=> 'Missing # of days.'));
                                Yii::app()->end();
                            }

                            $expireDate = date('Y-m-d', strtotime('+'.$days.' days'));

                            if(!is_numeric($componentTypeId)) {
                                throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Component Type Id is not numeric.');
                            }

                            if(!is_numeric($componentId)) {
                                throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Component Id is not numeric.');
                            }

                            // if record already exists, ignore if this date is less than existing. If after, update record.
                            if($doNotCall = CallListDoNotCall::model()->findByAttributes(array('call_list_phone_id'=>null,'phone'=>$model->phone,'contact_id'=>$model->contact_id))) {
                                if($expireDate > $doNotCall->expire_date) {
                                    $doNotCall->expire_date = $expireDate;
                                }
                                else {
                                    echo CJSON::encode(array('status'=>'notice', 'message'=> 'Phone # is already on the Internal Do Not Call List.'));
                                    Yii::app()->end();
                                }
                            }
                            else {
                                // Add to do not call list & add activity log using specific task_type_id
                                $doNotCall = new CallListDoNotCall();
                                $doNotCall->setAttributes(array(
                                        'call_list_phone_id' => null,
                                        'contact_id' => $model->contact_id,
                                        'phone' => $model->phone,
                                        'expire_date' => $expireDate,
                                    ));
                            }

                            // Attempt to save the record
                            if(!$doNotCall->save()) {
                                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Do Not Call. Errors: ' . print_r($doNotCall->getErrors(), true) . ' Attributes: ' . print_r($doNotCall->attributes, true), CLogger::LEVEL_ERROR);
                                echo CJSON::encode(array('status'=>'error', 'message'=> 'Internal Do Not Call did not save. Error Message: '.current($doNotCall->getErrors())[0]));
                                Yii::app()->end();
                            }
                            else {
                                // save activity level on contact level
                                $activityLog = new ActivityLog;
                                $activityLog->account_id = Yii::app()->user->accountId;
                                $activityLog->component_type_id = ComponentTypes::CONTACTS;
                                $activityLog->component_id = $model->contact_id;
                                $activityLog->task_type_id = TaskTypes::MANUAL_ADD_DO_NOT_CALL;
                                $activityLog->activity_date = new CDbExpression('NOW()');
                                $activityLog->note = 'Added phone number to Internal Do Not Call List - ' . Yii::app()->format->formatPhone($model->phone) . ' until '.date('m/d/Y', strtotime('+'.$days.'days'));
                                if(!$activityLog->save()) {
                                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating activity log for do not call. Did not save properly. Error Message: ' . print_r($activityLog->getErrors(), true) . ' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
                                }

                                // save activity level on component level
                                $activityLog = new ActivityLog;
                                $activityLog->account_id = Yii::app()->user->accountId;
                                $activityLog->component_type_id = $_POST['phoneOriginComponentTypeId'];
                                $activityLog->component_id = $_POST['phoneOriginComponentId'];
                                $activityLog->task_type_id = TaskTypes::MANUAL_ADD_DO_NOT_CALL;
                                $activityLog->activity_date = new CDbExpression('NOW()');
                                $activityLog->note = 'Added phone number to Internal Do Not Call List - ' . Yii::app()->format->formatPhone($model->phone) . ' until '.date('m/d/Y', strtotime('+'.$days.'days'));
                                if(!$activityLog->save()) {
                                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating activity log for do not call. Did not save properly. Error Message: ' . print_r($activityLog->getErrors(), true) . ' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
                                }
                            }
                            break;
                    }
                }
            }

            Yii::app()->end();
        }
    }

}
