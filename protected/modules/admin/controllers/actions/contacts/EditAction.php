<?php

	class EditAction extends CAction {

		/**
		 * Updates a particular model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 *
		 * @param integer $id the ID of the model to be updated
		 */
		//@todo: There's got to be a way to make this run function more modular (220 lines at the time of this writing)
		//@todo: Revisit this and refactor it to use a FactoryMethod which accepts the POST data array and returns a collection of model instances.
		public function run($id) {
			$model = $this->controller->loadModel($id);
			$model->setScenario('update');
			$this->controller->title = $model->fullName;

            if (isset($_POST['returnUrl']) && !empty($_POST['returnUrl'])) {
                $returnUrl = $_POST['returnUrl'];
            } elseif (Yii::app()->user->hasState('returnContactIdVerify')) {//
                $returnPieces = explode('?returnContactIdVerify=', Yii::app()->user->getState('returnContactIdVerify'));
                if(count($returnPieces)==2) {
                    $returnUrl = ($id == $returnPieces[1])? $returnPieces[0] : null;
                }
            }
            Yii::app()->user->setState('returnContactIdVerify',null);

            $this->processAjaxRequest($model);

			// Process additional contact attributes
			$contactAttributesValidated = true;

			if (isset($_POST['ContactAttributeValues'])) {
				foreach ($_POST['ContactAttributeValues'] as $attributeId => $valueData) {
					if (isset($valueData['value']) && (!empty($valueData['value']) || $valueData['value'] == '0')) {
						$ContactAttributeValues = new ContactAttributeValues;
						$ContactAttributeValues->attributes = $valueData;
						$ContactAttributeValues->contact_attribute_id = $attributeId;

						// Mark that the contact attributes section had a failure
						if (!$ContactAttributeValues->validate() && $contactAttributesValidated) {
							$contactAttributesValidated = false;
						}

						$model->contactAttributeValues[$attributeId] = $ContactAttributeValues;
					}
				}
			}

			// Process additional e-mails for the contact if any
			$contactEmailsValidated = true;
			if (isset($_POST['Emails'])) {
				foreach ($_POST['Emails'] as $i => $emailData) {
                    $emailModel = new Emails;
                    $existingEmail = $emailModel->skipSoftDeleteCheck()->byEmail($emailData['email'])->find(array(
//                        'condition' => 'is_deleted = 1',
                    ));
                    if($existingEmail) {
                        $existingEmailIsDeleted = $existingEmail->is_deleted;
                    }

                    // NOT new email record
					if (isset($emailData['id']) and is_numeric($emailData['id'])) {
                        // The email address already exists within the system, so lets undelete and assign it to this contact
                        if($existingEmail && $existingEmailIsDeleted) {
                            if ($existingEmail instanceof Emails) {
                                $existingEmail->contact_id = $id;
                                $existingEmail->is_deleted = 0;
                                // purposely saving now because since the soft delete of current email happens right after, need to have this happen. If something goes wrong, can be left with no data showing up in gui. May need to think through logic some more. This approach is not ideal cleanliness.
                                $existingEmail->save();
                                ${'ContactEmail'.$i} = $existingEmail;

                                $EmailToDelete = Emails::model()->findByPk($emailData['id']);
                                $EmailToDelete->delete();
                                $emailData['id'] = $existingEmail->id;
                            }

                        } else {
                            ${'ContactEmail'.$i} = new Emails;
                            ${'ContactEmail'.$i} = ${'ContactEmail'.$i}->findByPk($emailData['id']);
                        }
					} else { // new email
						${'ContactEmail'.$i} = new Emails;

                        if($existingEmail instanceof Emails) {
                            if($existingEmailIsDeleted == 1) {
                                ${'ContactEmail'.$i} = $existingEmail;
                                $emailData['id'] = $existingEmail->id;
                                ${'ContactEmail'.$i}->is_deleted = 0;
                            }
                        }
					}

                    ${'ContactEmail'.$i}->attributes = $emailData;

					// Check to see if the email needs to be soft-deleted
					if (${'ContactEmail'.$i}->remove) {
                        ${'ContactEmail'.$i}->delete();
						continue;
					}

					//  Mark that the contact e-mails section had a failure
					if (!${'ContactEmail'.$i}->validate() && $contactEmailsValidated) {
						$contactEmailsValidated = false;
					}

					array_push($model->contactEmails, ${'ContactEmail'.$i});
				}
			}

			// Process additional phone numbers for the contact if any
			$contactPhonesValidated = true;
			if (isset($_POST['Phones'])) {
				foreach ($_POST['Phones'] as $i => $phoneData) {
					if ($phoneData['id']) {
						$ContactPhone = Phones::model()->findByPk($phoneData['id']);
//                        if(!$ContactPhone) {
//                            continue;
//                        }
					} else {
						$ContactPhone = new Phones;
					}

					$ContactPhone->attributes = $phoneData;

					if ($ContactPhone->remove) {
						$ContactPhone->delete();
						continue;
					}

					// Mark that the contact e-mails section had a failure
					if (!$ContactPhone->validate() && $contactPhonesValidated) {
						$contactPhonesValidated = false;
					}

					array_push($model->contactPhones, $ContactPhone);
				}
			}

			// Process additional addresses for the contact if any
			$contactAddressesValidated = true;
			if (isset($_POST['Addresses'])) {
				foreach ($_POST['Addresses'] as $i => $addressData) {
					if ($addressData['id']) {
						$Criteria = new CDbCriteria;
						$Criteria->condition = 'id = :id';
						$Criteria->params = array(
							':id' => $addressData['id'],
						);
						$ContactAddress = Addresses::model()->find($Criteria);
					} else {
						$ContactAddress = new Addresses;
					}

					$ContactAddress->attributes = $addressData;

					if ($ContactAddress->remove) {
						$ContactAddress->delete();
						continue;
					}

					// Mark that the contact address section had a failure
					if (!$ContactAddress->validate() && $contactAddressesValidated) {
						$contactAddressesValidated = false;
					}

					array_push($model->contactAddresses, $ContactAddress);
				}
			}

			// Return a boolean if each sub section validated
			$contactSubSectionsValidated = ($contactAttributesValidated && $contactEmailsValidated && $contactAddressesValidated && $contactPhonesValidated) ? true : false;

			if (isset($_POST['Contacts'])) {
				$model->attributes = $_POST['Contacts'];

				if ($model->save() && $contactSubSectionsValidated) {
					// Save the contact attributes
					if ($model->contactAttributeValues) {
						$CurrentAttributeValues = array();

						foreach ($model->attributeValues as $currentAttribute) {
							$CurrentAttributeValues[$currentAttribute->contact_attribute_id] = $currentAttribute;
						}

						foreach ($CurrentAttributeValues as $attributeId => $attributeValue) {
							$isDeletedAttribute = !array_key_exists($attributeId, $model->contactAttributeValues);

							if ($isDeletedAttribute) {
								$attributeValue->delete();
							}
						}

						foreach ($model->contactAttributeValues as $attributeId => &$ContactAttributeValue) {
							$isAnUpdate = array_key_exists($attributeId, $CurrentAttributeValues);

							if ($isAnUpdate) {
								$updatedValue = $ContactAttributeValue->value;
								$ContactAttributeValue = $CurrentAttributeValues[$attributeId];
								$ContactAttributeValue->value = $updatedValue;
							} else {
								$ContactAttributeValue->contact_id = $model->id;
							}

							$ContactAttributeValue->save(false);
						}
					}
                    else {
                        // if no contactAttributeValues posted, then check to see if any exists that need to be deleted
                        ContactAttributeValues::model()->updateAll(array('is_deleted'=>1), 'contact_id='.$model->id);
                    }

					// Process Emails
					if ($model->contactEmails) {
						foreach ($model->contactEmails as $ContactEmail) {
							$ContactEmail->contact_id = $model->id;
							$ContactEmail->account_id = Yii::app()->user->accountId;
							if (count($model->contactEmails) == 1) {
								$ContactEmail->is_primary = 1;
							}

							$ContactEmail->save(false); // already ran validation
						}
					}

					// Process Phone Numbers
					if ($model->contactPhones) {
						foreach ($model->contactPhones as $ContactPhone) {
							if ($ContactPhone->isNewRecord) {
								$ContactPhone->contact_id = $model->id;
								$ContactPhone->account_id = Yii::app()->user->accountId;
							}

							$ContactPhone->save(false); // already ran validation
						}
					}

					// Process the addresses for this contact
					if ($model->contactAddresses) {
						foreach ($model->contactAddresses as $ContactAddress) {
							if ($ContactAddress->isNewRecord) {
								$ContactAddress->contactId = $model->id;
								$ContactAddress->account_id = Yii::app()->user->accountId;
							}

							if($ContactAddress->save(false)) {  // already ran validation
                                $AddressContactLu = AddressContactLu::model()->findByAttributes(array('contact_id'=>$model->id,'address_id'=>$ContactAddress->id));
                                $AddressContactLu->is_primary = $ContactAddress->isPrimary;
                                $AddressContactLu->save();
                            }
						}
					}

                    // We only will add new ones when one is not assigned to the contact or when one is deleted
					if ($model->contactTypesToAdd) {
						$contactTypesToAddFlipped = array_flip($model->contactTypesToAdd);
						foreach ($model->contactTypeLu as $ContactTypeLu) {
							if (!in_array($ContactTypeLu->contact_type_id, $model->contactTypesToAdd)) {
								$ContactTypeLu->delete();
							} else {
								unset($contactTypesToAddFlipped[$ContactTypeLu->contact_type_id]);
							}
						}

						$model->contactTypesToAdd = array_flip($contactTypesToAddFlipped);

						// Attach the contact types to the newly contact
						foreach ($model->contactTypesToAdd as $attributeId) {
							$ContactTypeLu = new ContactTypeLu;
							$ContactTypeLu->contact_id = $model->id;
							$ContactTypeLu->contact_type_id = $attributeId;
							$ContactTypeLu->save();
						}
					}
                    else {
                        // if there is no contactTypesToAdd delete all
                        foreach ($model->contactTypeLu as $ContactTypeLu) {
                            $ContactTypeLu->delete();
                        }
                    }

					Yii::app()->user->setFlash('success', 'Successfully updated Contact!');
                    if(!$returnUrl) {
                        $returnUrl = array('view', 'id' => $model->id);
                    }
                    $this->controller->redirect($returnUrl);

                } else {
					Yii::app()->user->setFlash('warning', 'Contact did not save. Please update fields as described.');
				}
			} else { // Pull existing values
				// Additional Emails
				$model->contactEmails = $model->emails;

				// Additional Addresses
				$model->contactAddresses = $model->addresses;

				// Additional Phones
				$model->contactPhones = $model->phones;

				// Process the contact attribute list
				if ($model->attributeValues) {
					foreach ($model->attributeValues as $ContactAttributeValues) {
						$model->contactAttributeValues[$ContactAttributeValues->contact_attribute_id] = $ContactAttributeValues;
					}
				}

				// Process the contact types
				if ($model->contactTypeLu) {
					foreach ($model->contactTypeLu as $ContactTypeLu) {
						array_push($model->contactTypesToAdd, $ContactTypeLu->type->id);
					}
				}
			}

			$this->controller->breadcrumbs = array(
				$model->fullName => array(
					'view',
					'id' => $model->id
				),
				'Edit' => array(
					'/'.$this->controller->module->name.'/contacts/edit/',
					'id' => $model->id
				),
			);

			$this->controller->pageTitle = $model->fullName;
			Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_AddressPhoneEmailGui.js');

			$this->controller->render('form', array(
					'model' => $model,
                    'returnUrl' =>$returnUrl,
				)
			);
		}

		public function processAjaxRequest(Contacts $model) {
			if (Yii::app()->getRequest()->getIsAjaxRequest()) {
				if (isset($_POST['Emails'])) {
					$emailEntriesToValidate = array();
					foreach ($_POST['Emails'] as $emailData) {
						$emailEntry = new Emails('bounceUniqueCheck');
						$emailEntry->attributes = $emailData;
						array_push($emailEntriesToValidate, $emailEntry);
					}

					echo CActiveForm::validateTabular($emailEntriesToValidate);
				}

				Yii::app()->end();
			}
		}
	}
