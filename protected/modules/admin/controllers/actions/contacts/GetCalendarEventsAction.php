<?php

class GetCalendarEventsAction extends CAction
{
	public function run()
    {
        if($calendarId = Yii::app()->request->getParam('calendarId')) {
            $this->processAjaxRequest($calendarId);
        }
	}

	protected function processAjaxRequest($calendarId)
    {
		if (Yii::app()->request->isAjaxRequest) {

            Yii::import('admin_module.components.StmOAuth2.Google', true);
            $stmGoogle = new \StmOAuth2\Google();

            $start = ($_GET['start'])? date('Y-m-d\T00:00:00P', $_GET['start']) : date('Y-m-01\T00:00:00P');
            $end = ($_GET['end']) ? date('Y-m-d\T00:00:00P', $_GET['end']) : date('Y-m-t\T00:00:00P');

            $events = $stmGoogle->viewCalendarEvents($calendarId, $start, $end, $singleEvents=true);

			if ($events) {

                $data = array();
				foreach($events as $event) {

					array_push($data, array(
						'title'   => $event->summary,
						'start'   => $event->getStart()->dateTime,
						'end'     => $event->getEnd()->dateTime,
						'allDay'  => false,
					));
				}

				echo CJSON::encode($data);
			}

			Yii::app()->end();
		}
	}
}