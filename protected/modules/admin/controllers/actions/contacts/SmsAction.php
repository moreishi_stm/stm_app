<?php

	class SmsAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'SMS';

			if (Yii::app()->request->isAjaxRequest) {
				$this->performAjaxRequest();
			}

			$this->controller->render('sms', array(
//					'model' => $model
				)
			);
		}

		protected function performAjaxRequest() {
			if (Yii::app()->request->isAjaxRequest) {

				//Plivo request
				echo $result = Yii::app()->plivo->call();

//				echo $result = Yii::app()->sms->sendSms($toNumber='9042803800', $fromNumber='9047704151'); //5005550000 (test)
//				echo 'success';
//				echo 'error';

				Yii::app()->end();
			}
		}
	}