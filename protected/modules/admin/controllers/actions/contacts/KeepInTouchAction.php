<?php
    Yii::import('admin_widgets.DateRanger.DateRanger');

    class KeepInTouchAction extends CAction {

        public function run() {
            $this->controller->title = 'Keep In Touch';

            $model = new Contacts;
            $criteria = new CDbCriteria;

            $model->contactTypeLu = new ContactTypeLu('search');
            if (isset($_GET['ContactTypeLu'])) {
                $model->contactTypeLu->attributes = $_GET['ContactTypeLu'];
                $criteria->together = true;

//                SELECT COUNT(DISTINCT `t`.`id`) FROM `contacts` `t`  LEFT OUTER JOIN `contact_type_lu` `contactTypeLu` ON (`contactTypeLu`.`contact_id`=`t`.`id`)  LEFT OUTER JOIN `contacts` `contact` ON (`contactTypeLu`.`contact_id`=`contact`.`id`) AND ((`contact`.is_deleted = 0 or `contact`.is_deleted IS NULL))  WHERE (((`t`.is_deleted = 0 or `t`.is_deleted IS NULL)) AND ((() <2014-08-28) AND (contactTypeLu.contact_type_id=:ycp0)))
                if($model->contactTypeLu->idCollection) {
                    $contactType = ContactTypes::model()->findByPk($model->contactTypeLu->idCollection);
                    $followUpDays = $contactType->follow_up_days;
                    $maxSpokeToDate = new DateTime(date('Y-m-d'));
                    $maxSpokeToDate->sub(new DateInterval('P'.$followUpDays.'D'));
                    $maxSpokeToDate = $maxSpokeToDate->format('Y-m-d');

                    $criteria->addCondition('(select DATE(activity_date) from activity_log where component_id=t.id AND component_type_id='.ComponentTypes::CONTACTS.' AND task_type_id='.TaskTypes::PHONE.' AND is_spoke_to=1 AND is_deleted=0 ORDER BY activity_date DESC LIMIT 1) < "'.$maxSpokeToDate.'" OR (select DATE(activity_date) from activity_log where component_id=t.id AND component_type_id='.ComponentTypes::CONTACTS.' AND task_type_id='.TaskTypes::PHONE.' AND is_spoke_to=1 AND is_deleted=0 ORDER BY activity_date DESC LIMIT 1) IS NULL');
                    $criteria->with = CMap::mergeArray($criteria->with, array('contactTypeLu','contactTypeLu.contact'));
                    $criteria->addInCondition('contactTypeLu.contact_type_id', (array) $model->contactTypeLu->idCollection);
                }
            }
            // forces an empty list before a contact type is selected
            else {
                $criteria->compare('id',0);
            }

            $dataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$criteria,
                                                                      'pagination'=>array(
                                                                         'pageSize'=> 50,
                                                                      ),
                                                                    ));
            $count = $model->count($criteria);
            $dataProvider->setTotalItemCount($count);
            $dataProvider->pagination->setItemCount($count);
            $renderType = (Yii::app()->request->isAjaxRequest)? 'render' : 'render'; //renderPartial - this doesn't work in HQ

            $this->controller->$renderType('keepInTouch', array('model'=>$model, 'dataProvider'=>$dataProvider));
        }

        public function printClosings(Contacts $contact)
        {
            $closingsInfo = '';
            if($transactions = $contact->transactions) {
                foreach($transactions as $transaction) {
                    if($transaction->transaction_status_id==TransactionStatus::CLOSED_ID) {
                        $closingsInfo .= ($closingsInfo)? '<br>' : '';
                        // closed date
                        $closingsInfo .= '[<strong>'.$transaction->componentType->singularName.'</strong>] '.Yii::app()->format->formatDate($transaction->closing->actual_closing_datetime);
                        if($assignments = $transaction->buyerSellerAssignments) {
                            foreach($assignments as $assignment) {
                                $closingsInfo .= '<br><span style="margin-left: 50px;"></span>'.$assignment->contact->fullName.' ('.$assignment->assignmentType->display_name.')';
                            }
                        }

                    }
                }
            }
            return $closingsInfo;
        }
	}
