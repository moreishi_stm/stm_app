<?php

class HomesSavedAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		$homesSavedProvider = SavedHomes::getAll($id);
		$this->controller->renderPartial('_homesSaved',array(
			'dataProvider'=>$homesSavedProvider,
		));
	}
}