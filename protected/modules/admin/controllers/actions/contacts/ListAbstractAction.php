<?php

    // Include things we need to get the job done
    Yii::import('admin_module.components.StmSlybroadcast.SlyBroadcastException', true);
    Yii::import('admin_module.components.StmSlybroadcast.Slybroadcast', true);
    Yii::import('admin_module.components.StmSlybroadcast.Campaign', true);

    abstract class ListAbstractAction extends CAction {

        public $view = 'list';
		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Contacts List';

			$model = new Contacts('search');
			$model->unsetAttributes(); // clear any default values
			//		$model->contact_status_ma = 1;

            $model->contact_status_ma = Contacts::STATUS_ACTIVE;

			if (isset($_GET['Contacts'])) {
				$model->attributes = $_GET['Contacts'];
			}

			$model->emails = new Emails('search');
			if (isset($_GET['Emails'])) {
				$model->emails->attributes = $_GET['Emails'];
			}

            $model->addresses = new Addresses('search');
            if (isset($_GET['Addresses'])) {
                $model->addresses->attributes = $_GET['Addresses'];
            }

			$model->phones = new Phones('search');
			if (isset($_GET['Phones'])) {
				$model->phones->attributes = $_GET['Phones'];
			}

			$model->contactTypeLu = new ContactTypeLu('search');
			if (isset($_GET['ContactTypeLu'])) {
				$model->contactTypeLu->attributes = $_GET['ContactTypeLu'];
			}

			$model->source = new Sources('search');
			if (isset($_GET['Sources'])) {
				$model->source->attributes = $_GET['Sources'];
			}

			if(!empty($_GET["noTasksCheckbox"])){
				$model->noTask = 1;
			}
			if(!empty($_GET["noActionPlanCheckbox"])){
				$model->noActionPlan = 1;
			}

            $DataProvider = $this->getDataProvider($model);

            if(isset($_GET['export']) && $_GET['export']==1){
                $DataProvider->pagination = False;
                $contacts = $DataProvider->data;
                unset($DataProvider);
                $exportData = $model->getExportData($contacts);
                $date = date('Y-m-d');
                header( "Content-Type: application/vnd.ms-excel; charset=utf-8" );
                header( "Content-Disposition: inline; filename=\"contacts_export_$date.xls\"" );
                echo $exportData;
                Yii::app()->end();
            }

            if($_GET['dialerProcess'] && $_GET['dialerAction'] && $_GET['dialerListId'] && Yii::app()->request->isAjaxRequest) {

                $this->_dialerListActions($model, $_GET['dialerAction'], $_GET['dialerListId']);
            }

            $this->beforeRender($model);

            // check for multi-account owner access
            $isMultiAccountOwner = (AuthAssignment::model()->countByAttributes(array('itemname' => 'owner', 'userid'=> Yii::app()->user->contact->id)) > 1) ? true : false;

            $dialerLists[ComponentTypes::CONTACTS] = (array) CallLists::model()->findAllByAttributes(array('preset_ma'=>CallLists::CUSTOM_CONTACTS));

            // Handle slybroadcast, if we wanted that
            $slyBroadcastErrorMessage = '';
            if($_GET['broadcastProcess']) {
                try {
                    $this->_broadcastProcessAction($DataProvider->data);
                    $status = 'success';
                    $slyBroadcastMessage = 'Sly Broadcast campaign has been created successfully.';
                }
                catch(\StmSlybroadcast\SlyBroadcastException $e) {
                    $status = 'error';
                    $slyBroadcastMessage = 'Error: Sly Broadcast had an error. Please check your username/password. Contact us for additional support.';
                    Yii::log(__CLASS__.' (:'.__LINE__.') Sly Broadcast had an error.', CLogger::LEVEL_ERROR);
                }
                catch(Exception $e) {
                    $status = 'error';
                    $slyBroadcastMessage = 'Error: Sly Broadcast had an unknown error. Try again later or Contact us for additional support.';
                    Yii::log(__CLASS__.' (:'.__LINE__.') Sly Broadcast had an error.', CLogger::LEVEL_ERROR);
                }
                finally {
                    // Send response
                    echo CJSON::encode(array('status'=> $status, 'message'=>$slyBroadcastMessage));
                    Yii::app()->end();
                }
            }




			if(!empty($_GET["MassAction"])){
				$this->_doMassAction($model);
			}
            // Render view
            $this->controller->render($this->view, array(
					'model' => $model,
                    'DataProvider' => $DataProvider,
                    'isMultiAccountOwner' => $isMultiAccountOwner,
                    'dialerLists' => $dialerLists
				)
			);
		}

		/**
		 * Broadcast Process Action
		 *
		 * Creates a new slybroadcast from given data
		 * @param $leads
		 */
		protected function _broadcastProcessAction($leads)
		{
			// New SlyBroadcast campaign
			$campaign = new StmSlybroadcast\Campaign();
			$campaign->audioUrl = 'http://sites.seizethemarket.com/site-files/' . Yii::app()->user->clientId . '/broadcast-messages/' . $_GET['broadcastAudioFile'] . '.wav';
			$campaign->callerId = $_GET['broadcastCallerId'];
			$campaign->date = date('Y-m-d H:i:s', strtotime($_GET['broadcastDeliveryDate']));
			$campaign->mobileOnly = $_GET['broadcastMobileOnly'] ? true : false;

			// Add phones to campaign
			foreach($leads as $lead) {

				// Get phone and check to make sure it's valid, then add it to the campaign
				$phone = $lead->getPrimaryPhone();
				if(strlen($phone) == 10) {
					$campaign->addPhone($lead->getPrimaryPhone());
				}
			}

			// Create the campaign
			$slyBroadcast = new StmSlybroadcast\Slybroadcast();
			$slyBroadcast->createCampaign($campaign);
		}

		protected function _doMassAction($model){
			$massAction = $_GET["MassAction"];
			if(!empty($massAction) && !empty($massAction["action"])){
				$dataProvider = $model->search(250);
				$contacts = $dataProvider->getData();
				$contactCount = count($contacts);
				if($contactCount > 0){
					foreach ($contacts as $r){
						if($massAction["action"] === "addTag" || $massAction["action"] === "removeTag"){
							foreach ($massAction["options"] as $oId){
								if(empty($oId)) {
									continue;
								}
								$dataArray = array( "contact_id" => $r->id, "contact_type_id" => $oId);
								$massActionModel = new ContactTypeLu();
								$massActionModel = $massActionModel->findByAttributes($dataArray);
								//only edit the record if we need to
								if(empty($massActionModel->contact_id) && $massAction["action"] === "addTag" && !empty($dataArray)){
									$massActionModel = new ContactTypeLu();
									$massActionModel->attributes = $dataArray;
									$massActionModel->save(1);
								}elseif (!empty($massActionModel->contact_id) && $massAction["action"] === "removeTag"){
									$massActionModel->delete();
								}
							}
						}elseif($massAction["action"] === "applyActionPlan"){
							if(!empty($massAction["plan_options"])){
								if($ActionPlan = ActionPlans::model()->findByPk($massAction["plan_options"])){
									if(!($parentModel = ComponentTypes::getComponentModel(
										ComponentTypes::CONTACTS,
										$r->id
									))) {
										echo CJSON::encode(array('status'=>'error','message'=>'Component not found.'));
										Yii::app()->end();
									}
									$ActionPlan->apply($parentModel);
								}
							}
						}

					}
					header("Content-Type: application/json");
					echo CJSON::encode(array("status"=> 200));
					return Yii::app()->end();
				}
			}
		}

        protected function _dialerListActions($model, $dialerAction, $dialerListId)
        {
            $errorLog = array();

            $dialerListPhoneIds = Yii::app()->db->createCommand()
                ->select('phone_id')
                ->from('call_list_phones')
                ->where('call_list_id=:callListId AND hard_deleted IS NULL', array(':callListId'=> $dialerListId))
                ->queryColumn();

            $criteria = $this->getDataProvider($model)->getCriteria();
            $contacts = Contacts::model()->findAll($criteria);

            foreach($contacts as $contact) {

                switch($dialerAction) {
                    case 'add':
                        CallListPhones::addToCallListByContactId($dialerListId, $contact->id, null, $errorLog);
                        break;

                    case 'remove':
                        CallListPhones::removeFromCallListByContactId($dialerListId, $contact->id, $errorLog);
                        break;
                }
            }
        }



        public function printContactInfo($contact)
        {
            $string = '';
            if($contact->emails) {

                // separate search because when doing a $_GET search on email it doesn't show other emails for the contact
                $contactEmails = Emails::model()->findAllByAttributes(array('contact_id'=>$contact->id));

                // print emails
                foreach($contactEmails as $i => $Email) {
                    $statusAlert = '';
                    $string .= (empty($string))? '' :'<br />';
                    $string .= $Email->email;

                    if($Email->email_status_id == EmailStatus::OPT_OUT) {
                        $statusAlert = 'opted-out';
                    } elseif($Email->email_status_id == EmailStatus::HARD_BOUNCE) {
                        $statusAlert = 'bounced';
                    }
                    if($statusAlert) {
                        $string .= CHtml::tag('span',$htmlOptions=array('class'=>'errorMessage p-pl5'),'('.$statusAlert.')');
                    }
                }
            }

            // print phones
            if($contact->phones) {
                // separate search because when doing a $_GET search on email it doesn't show other emails for the contact
                $contactPhones = Phones::model()->findAllByAttributes(array('contact_id'=>$contact->id));

                foreach($contactPhones as $phone) {
                    $string .= (empty($string))? '' :'<br />';
                    $string .= Yii::app()->format->formatPhone($phone->phone);
                }
            }

            if($contact->addresses) {
                // print addresses
                $string .= (empty($string))? '' :'<br /><br />';
                $string .= Yii::app()->format->formatPrintAddresses($contact->addresses);
            }

            echo $string;
        }

        abstract protected function beforeRender(Contacts &$Contact);

        abstract protected function getDataProvider(Contacts $Contact);
	}