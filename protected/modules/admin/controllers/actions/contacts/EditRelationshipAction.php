<?php

/**
 * Creates a new task.
 *
 */
class EditRelationshipAction extends CAction
{
	public function run($id)
	{
        $model = ContactRelationshipLu::model()->findByPk($id);

        $this->processAjaxRequest($model);
	}

    protected function processAjaxRequest(ContactRelationshipLu $model) {

        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['ContactRelationshipLu'])) {

                $model->attributes = $_POST['ContactRelationshipLu'];
                if (!$model->save()) {
                    echo CActiveForm::validate($model);
                }
            }

            Yii::app()->end();
        }
    }
}