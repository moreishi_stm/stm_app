<?php
    Yii::import('admin_module.controllers.actions.contacts.ListAbstractAction');

    class ListAction extends ListAbstractAction {

		/**
		 * Manages models
		 */

        protected function getDataProvider(Contacts $Contact) {

            $DataProvider = $Contact->search();
            $sort = new CSort();
            $sort->defaultOrder = 't.added DESC';
            $sort->attributes = array(
                'added'=>array(
                    'asc'=>'t.added ASC',
                    'desc'=>'t.added DESC',
                ),
                'source.name',
                'first_name',
                'last_name',
                'spouse_first_name',
                'spouse_last_name',
//                'emails'=>array(
//                    'asc'=>'emails.email ASC',
//                    'desc'=>'emails.email DESC',
//                ),
            );

            $DataProvider->sort = $sort;
            return $DataProvider;
        }

        protected function beforeRender(Contacts &$Contact) {

        }

        public function printSpecialDatesRelationships(Contacts $Contact) {
            $specialDates = '';
            $contactAttributesValues = ContactAttributeValues::model()->byContactIdAttributesIds($Contact->id, array(ContactAttributes::BIRTHDAY, ContactAttributes::CLOSING_ANNIVERSARY, ContactAttributes::WEDDING_ANNIVERSARY))->findAll();

            foreach($contactAttributesValues as $key => $value) {
                switch ($value->contact_attribute_id) {
                    case ContactAttributes::BIRTHDAY:
                        $birthday = 'Birthdays: '.$value->value;
                        break;
                    case ContactAttributes::WEDDING_ANNIVERSARY:
                        $wedding =  'Wedding Anniversary: '.$value->value;;
                        break;
                    case ContactAttributes::CLOSING_ANNIVERSARY:
                        $closing =  'Closing Anniversary: '.$value->value;;
                        break;
                }
            }
            $specialDates .= ($birthday)? $birthday: '';
            $specialDates .= ($wedding)? (($specialDates) ? '<br />'.$wedding: $wedding) : '';
            $specialDates .= ($closing)? (($specialDates) ? '<br />'.$closing: $closing) : '';

            $relationships = ($specialDates) ? '<br />' : '';
            foreach(ContactRelationshipLu::model()->byEitherContactId($Contact->id)->findAll() as $relationship) {
                $relationships .= ($relationships) ? '<br />' : '';

                if($relationship->origin_contact_id == $Contact->id) {
                    $relationships .= '('.$relationship->originRelationshipType->name.') ';
                    $relationships .= '<a href="/'.Yii::app()->controller->module->id.'/contacts/'.$relationship->relatedContact->id.'" target="_blank">'.$relationship->relatedContact->fullName.'</a>';
                } elseif($relationship->related_to_contact_id == $Contact->id) {
                    $relationships .= '('.$relationship->relatedRelationshipType->name.') ';
                    $relationships .= '<a href="/'.Yii::app()->controller->module->id.'/contacts/'.$relationship->originContact->id.'" target="_blank">'.$relationship->originContact->fullName.'</a>';
                }
            }

            return $specialDates.$relationships;
        }

        public function printContactLeads(Contacts $Contact)
        {
            echo $Contact->existingComponentRecordButton(ComponentTypes::CONTACTS);
//            if(!($tnxs = $Contact->transactions)) {
//                return;
//            }
//
//            foreach($tnxs as $tnx) {
//
//            }
        }
    }
