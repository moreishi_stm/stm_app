<?php
/**
 * Import Action
 *
 * Used to import contacts
 */
class InitialImportAction extends CAction
{
    /**
     * Run
     *
     * This method is called when the page is loaded
     * @return void
     */
    public function run()
    {
        // Handle postback vs rendering page content
        if(Yii::app()->request->isPostRequest) {
            $pw = Yii::app()->request->getPost('pw');
            if($pw !== 'stmstm123') {
                throw new Exception('Invalid Password!');
            }
            $csvData = $this->_getCsvData();
            $this->_processContactsImport($csvData);
        }

        // Set page title
        $this->controller->title = 'Initial Import Contacts';

        // Pass data to view
        $this->controller->render('initialImport', array());
    }

    /**
     * Handle Process File
     *
     * Handles processing a file
     * @throws Exception When issues have come up with a file upload
     */
    protected function _getCsvData()
    {
        ini_set("auto_detect_line_endings", "1");  //needed for mac environment
        // Make sure we had a file upload
        if(!$_FILES['contacts-file']) {
            throw new Exception('Upload file missing!');

        }

        // Make sure it is actually an uploaded file and not some hack
        if(!is_uploaded_file($_FILES['contacts-file']['tmp_name'])) {
            throw new Exception('File upload is not really a file upload!');
        }

        // Use file info extension to make sure the file is a proper csv file and not some weird file, we can't process excel files
        $info = new finfo(FILEINFO_MIME_TYPE);
        if(!in_array($info->file($_FILES['contacts-file']['tmp_name']), array('text/plain','text/csv','text/tsv'))) {
            throw new Exception('The selected file is not a proper csv file, please make sure it is a plain text file.');
        }

        $csvData = array();
        $row = 1;
        if (($handle = fopen($_FILES['contacts-file']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                for ($c=0; $c < $num; $c++) {
                    $csvData[] = $data;
                }
            }
            fclose($handle);
        }

        return $csvData;
    }

    protected function _saveContact($firstName, $lastName, $email, $phones = array())
    {
    }

    /**
     * Handle Import
     *
     * Helper function to handle the actual import process
     * @return void
     */
    protected function _processContactsImport($data)
    {
        // Retrieve and decode JSON for contacts to import
        if(!$data) {
            throw new Exception('Upload data missing!');
        }

        // Retrieve all states and build dictionaries for lookups of state IDs
//        $stateFullNames = array();
//        $stateAbbreviations = array();
//        $stateResults = AddressStates::model()->findAll();
//        foreach($stateResults as $stateResult) {
//            $stateFullNames[strtolower($stateResult['name'])] = $stateResult['id'];
//            $stateAbbreviations[strtolower($stateResult['short_name'])] = $stateResult['id'];
//        }
//
//        // Create a flat array of existing emails indexed by email address for lookups
//        $emailsExisting = array();
//        $emailsExistingResults = Emails::model()->skipSoftDeleteCheck()->findAll();
//        foreach($emailsExistingResults as $emailsExistingResult) {
//            $emailsExisting[$emailsExistingResult->email] = $emailsExistingResult;
//        }
//
//        // Create a flat array of existing first and last names
//        $namesExisting = array();
//        $namesExistingResults = Contacts::model()->findAll();
//        foreach($namesExistingResults as $namesExistingResult) {
//            $namesExisting[$namesExistingResult->first_name . ' ' . $namesExistingResult->last_name] = $namesExistingResult;
//        }
//
//        // Store all new contacts
//        foreach($newContacts as $newContact) {
//
//            // If we don't have an email or any phone numbers, skip this
//            if(empty($newContact['email']) && empty($newContact['email_2']) && empty($newContact['email_3']) && empty($newContact['email_4']) && empty($newContact['phone_1']) && empty($newContact['phone_2']) && empty($newContact['phone_3']) & empty($newContact['phone_4'])) {
//                continue;
//            }
//
//            // Check existing email
//            if(array_key_exists($newContact['email'], $emailsExisting)) {
//
//                // If the email has not been deleted, skip.
//                if($emailsExisting[$newContact['email']]['is_deleted'] == '1') {
//                    $emailsExisting[$newContact['email']]->delete();
//                }
//                else {
//                    continue;
//                }
//            }
//
//            // Check first name and last name (not sure how to do this reliably yet) ???
//            if(array_key_exists($newContact['first_name'] . ' ' . $newContact['last_name'], $namesExisting)) {
//                continue;
//            }
//
//            // Create and save new contact record
//            $contact = new Contacts();
//            $contact->setAttributes(array(
//                'account_id'        =>  Yii::app()->user->accountId,
//                'source_id'         =>  1,
//                'contact_status_ma' =>  Contacts::STATUS_ACTIVE,
//                'first_name'        =>  $newContact['first_name'],
//                'last_name'         =>  $newContact['last_name'],
//                'contactEmailAdd'   =>  $newContact['email'] ? $newContact['email'] : null,
//                'contactPhoneAdd'   =>  $newContact['phone_1'] ? $newContact['phone_1'] : null
//            ));
//
//            // If we were unable to save this item, skip it
//            if(!$contact->save()) {
//                continue;
//            }
//
//            // Add first phone if we have one
//            if(!empty($newContact['phone_1'])) {
//                $phone = new Phones();
//                $phone->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'phone_type_ma'     =>  1,
//                    'phone'             =>  $newContact['phone_1'],
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  1
//                ));
//
//                $phone->save();
//            }
//
//            // Add second phone if we have one
//            if(!empty($newContact['phone_2'])) {
//                $phone = new Phones();
//                $phone->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'phone_type_ma'     =>  1,
//                    'phone'             =>  $newContact['phone_2'],
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  0
//                ));
//
//                $phone->save();
//            }
//
//            // Add third phone if we have one
//            if(!empty($newContact['phone_3'])) {
//                $phone = new Phones();
//                $phone->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'phone_type_ma'     =>  1,
//                    'phone'             =>  $newContact['phone_3'],
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  0
//                ));
//
//                $phone->save();
//            }
//
//            // Add third phone if we have one
//            if(!empty($newContact['phone_4'])) {
//                $phone = new Phones();
//                $phone->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'phone_type_ma'     =>  1,
//                    'phone'             =>  $newContact['phone_4'],
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  0
//                ));
//
//                $phone->save();
//            }
//
//            // Create and save new email record if we have one
//            if(!empty($newContact['email'])) {
//                $email = new Emails();
//                $email->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'email_status_id'   =>  1,
//                    'email'             =>  $newContact['email'],
//                    'email_type_ma'     =>  1,
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  1
//                ));
//
//                $email->save();
//            }
//
//            // Add second email if we have one
//            if(!empty($newContact['email_2'])) {
//                $email = new Emails();
//                $email->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'email_status_id'   =>  1,
//                    'email'             =>  $newContact['email_2'],
//                    'email_type_ma'     =>  1,
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  0
//                ));
//
//                $email->save();
//            }
//
//            // Add third email if we have one
//            if(!empty($newContact['email_3'])) {
//                $email = new Emails();
//                $email->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'email_status_id'   =>  1,
//                    'email'             =>  $newContact['email_3'],
//                    'email_type_ma'     =>  1,
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  0
//                ));
//
//                $email->save();
//            }
//
//            // Add fourth email if we have one
//            if(!empty($newContact['email_4'])) {
//                $email = new Emails();
//                $email->setAttributes(array(
//                    'account_id'        =>  Yii::app()->user->accountId,
//                    'contact_id'        =>  $contact->id,
//                    'email_status_id'   =>  1,
//                    'email'             =>  $newContact['email_4'],
//                    'email_type_ma'     =>  1,
//                    'owner_ma'          =>  0,
//                    'is_primary'        =>  0
//                ));
//
//                $email->save();
//            }
//
//            // Determine state ID, if we have one
//            $stateId = array_key_exists(strtolower(trim($newContact['state'])), $stateFullNames) ? $stateFullNames[strtolower(trim($newContact['state']))] : (array_key_exists(strtolower(trim($newContact['state'])), $stateAbbreviations) ? $stateAbbreviations[strtolower(trim($newContact['state']))] : null);
//
//            // If we have any address parts
//            if(!empty($newContact['address']) || !empty($newContact['city']) || (!empty($stateId)) || empty($newContact['zip'])) {
//
//                // Create and save new address record
//                $address = new Addresses();
//                $address->setAttributes(array(
//                    'account_id'    =>  Yii::app()->user->accountId,
//                    'address'       =>  $newContact['address'],
//                    'city'          =>  $newContact['city'],
//                    'state_id'      =>  $stateId,
//                    'zip'           =>  $newContact['zip']
//                ));
//
//                $address->save();
//
//                // Link address to contact
//                $addressContact = new AddressContactLu();
//                $addressContact->setAttributes(array(
//                    'address_id'    =>  $address->id,
//                    'contact_id'    =>  $contact->id,
//                    'is_primary'    =>  1
//                ));
//
//                $addressContact->save();
//            }
//        }

        // End
//        $this->_sendJson(array(
//            'status'    =>  'success'
//        ));
    }
}