<?php

	class AddToDialerListAction extends CAction
    {
		public function run()
        {
			if (Yii::app()->request->isAjaxRequest) {
				$this->performAjaxRequest();
			}
		}

		protected function performAjaxRequest()
        {
			if (Yii::app()->request->isAjaxRequest) {

                $status = 'success';
                $callList = CallLists::model()->findByPk($_POST['callListId']);
                $contact = Contacts::model()->findByPk($_POST['contactId']);
                $phone = Phones::model()->findByPk($_POST['phoneId']);
                $componentId = $_POST['componentId'];

                if(!$phone && !$contact) {
                    echo CJSON::encode(array('status'=>'error', 'message' => 'Phone or Contact not found.'));
                    Yii::app()->end();
                }
                elseif(!$contact) {
                    $contact = $phone->contact;
                }

                if(!$callList) {
                    echo CJSON::encode(array('status'=>'error', 'message' => 'Dialer List not found.'));
                    Yii::app()->end();
                }

                if(!$componentId) {
                    echo CJSON::encode(array('status'=>'error', 'message' => 'Component ID missing.'));
                    Yii::app()->end();
                }

                // see if this contact is already on the list

                // get component type of call list
                $componentTypeId = CallLists::getComponentTypeByPreset($callList->preset_ma);

                // make sure it doesn't already exist - or undelete of soft deleted
                $phonesToAdd = Yii::app()->db->createCommand("SELECT p.id phone_id, p.phone FROM phones p LEFT JOIN call_list_phones clp ON clp.phone_id=p.id AND clp.call_list_id=".$callList->id." WHERE clp.id IS NULL AND p.is_deleted=0 AND p.contact_id=".$contact->id)->queryAll();

                // need to log ????
                $phonesSoftUndeleted = Yii::app()->db->createCommand("UPDATE phones p LEFT JOIN call_list_phones clp ON clp.phone_id=p.id AND clp.call_list_id=".$callList->id." AND (clp.is_deleted=1 OR clp.hard_deleted IS NOT NULL) SET clp.is_deleted=0, clp.updated_by=".Yii::app()->user->id.", clp.hard_deleted=NULL, clp.hard_deleted_by=NULL WHERE p.is_deleted=0 AND p.contact_id=".$contact->id)->execute();

                // see if transaction or component record exists
//                $transactionExists = Yii::app()->db->createCommand("SELECT count(*) FROM transactions t WHERE t.contact_id=".$contact->id.' AND t.component_type_id='.$componentTypeId.' AND t.is_deleted=0 AND transaction_status_id IN(????)')->queryColumn();
//
//                if(!$transactionExists) {
//                    $componentSingularName = ComponentTypes::model()->findByPk($componentTypeId)->getSingularName();
//                    echo CJSON::encode(array('status'=>'error', 'message' => 'Lead record does not exist. Please add a '.$componentSingularName.' and try again.'));
//                    Yii::app()->end();
//                }

                if(empty($phonesToAdd) && !$phonesSoftUndeleted) {
                    echo CJSON::encode(array('status'=>'success', )); //'message' => 'Already added into list.'
                    Yii::app()->end();
                }
                else {

                    // add call_list_phones
                    foreach($phonesToAdd as $phone) {
                        $callListPhone = new CallListPhones();
                        $callListPhone->setAttributes(array(
                                'call_list_id' => $callList->id,
                                'phone_id' => $phone['phone_id'],
                                'status' => CallListPhones::QUEUED,
                                'call_count' => 0,
                                'updated_by' => Yii::app()->user->id,
                            ));

                        if(!$callListPhone->save()) {
                            echo CJSON::encode(array('status'=>'error', 'message' => current($callListPhone->getErrors())[0]));
                            Yii::app()->end();
                        }
                    }

                    // log activity in the regarding addition to dialer in appropriate component
                    $activityLog = new ActivityLog();
                    $activityLog->setAttributes(array(
                            'component_type_id' => $componentTypeId,
                            'component_id' => $componentId,
                            'task_type_id' => TaskTypes::DIALER_LIST_ADD,
                            'activity_date' =>  new CDbExpression('NOW()'),
                            'note' => 'Added to Dialer Call List - '.$callList->name,
                            'lead_gen_type_ma' => 0,
                            'is_spoke_to' => 0,
                            'is_public' => 0,
                            'added' => new CDbExpression('NOW()')
                        ));
                    if(!$activityLog->save()) {
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Activity Log for Adding to Call List did not save. Error Message: '.print_r($activityLog->getErrors(), true).PHP_EOL.'Attributes: '.print_r($activityLog->attributes, true).PHP_EOL.'Call List Data: '.print_r($callList->attributes, true), CLogger::LEVEL_ERROR);
                    }
                }

                $added = array();
                $added = array('call_list_id'=> $callList->id,
                               'name'=>$callList->name,
                               'contactId' => $_POST['contactId'],
                               'phoneId' => $_POST['phoneId'],
                               'componentTypeId' => $componentTypeId,
                               'componentId' => $componentId
                );
                echo CJSON::encode(array('status'=>$status, 'added' => $added));
                Yii::app()->end();
			}
		}
	}