<?php
    Yii::import('admin_widgets.DateRanger.DateRanger');

    class SpecialDatesAction extends CAction {

        public function run() {
            $this->controller->title = 'Special Dates';

            $model = new Contacts;

            $DateRanger = new DateRanger();
            $DateRanger->datePresetList = array();
            $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

            $specialDateAttributeIds = array(ContactAttributes::BIRTHDAY, ContactAttributes::CLOSING_ANNIVERSARY, ContactAttributes::SPOUSE_BIRTHDAY, ContactAttributes::WEDDING_ANNIVERSARY);
            $criteria = new CDbCriteria;
            $criteria->with = array('attributeValues'=>array('alias'=>'attributeValues'));
            $criteria->together = true;

            $criteria->addInCondition('attributeValues.contact_attribute_id', $specialDateAttributeIds);
            $criteria->addCondition('attributeValues.value != "" AND attributeValues.value IS NOT NULL');
            $criteria->addCondition('DATE_FORMAT(attributeValues.value, "%m-%d") >=  DATE_FORMAT("'.$dateRange['from_date'].'", "%m-%d")');
            $criteria->addCondition('DATE_FORMAT(attributeValues.value, "%m-%d") <= DATE_FORMAT("'.$dateRange['to_date'].'", "%m-%d")');


            $dataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$criteria,
                                                                      'pagination'=>array(
                                                                         'pageSize'=> 100,
                                                                      ),
                                                                    ));
            $count = $model->count($criteria);
            $dataProvider->setTotalItemCount($count);
            $dataProvider->pagination->setItemCount($count);

            // creating a separate data provider for work anniversaries
            $specialDateAttributeIds = array(ContactAttributes::BIRTHDAY, ContactAttributes::CLOSING_ANNIVERSARY, ContactAttributes::SPOUSE_BIRTHDAY, ContactAttributes::WEDDING_ANNIVERSARY);
            $specialDateSettingIds = array(Settings::SETTING_ANNIVERSARY_ID);
            $criteria2 = new CDbCriteria;
            $criteria2->with = array('settingValues'=>array('alias'=>'settingValues'));
            $criteria2->together = true;
            $criteria2->addInCondition('settingValues.setting_id', $specialDateSettingIds,'OR');
            $criteria2->addCondition('settingValues.value != "" AND settingValues.value IS NOT NULL');
            $criteria2->addCondition('DATE_FORMAT(settingValues.value, "%m-%d") >= DATE_FORMAT("'.$dateRange['from_date'].'", "%m-%d")');
            $criteria2->addCondition('DATE_FORMAT(settingValues.value, "%m-%d") <= DATE_FORMAT("'.$dateRange['to_date'].'", "%m-%d")');


            $dataProvider2 = new CActiveDataProvider('Contacts', array('criteria'=>$criteria2,
                                                                      'pagination'=>array(
                                                                          'pageSize'=> 100,
                                                                      ),
            ));
            $count2 = $model->count($criteria2);
            $dataProvider2->setTotalItemCount($count2);
            $dataProvider2->pagination->setItemCount($count2);



            $this->controller->render('specialDates', array(
                    'model'=>$model,
                    'dataProvider'=>$dataProvider,
                    'dataProvider2'=>$dataProvider2
                ));
        }


        public function printSpecialDatesRelationships($contact) {
            $specialDates = '';
            $contactAttributesValues = $contact->attributeValues;

            foreach($contactAttributesValues as $key => $value) {
                switch ($value->contact_attribute_id) {
                    case ContactAttributes::BIRTHDAY:
                        $birthday = 'Birthday: '.$value->value;
                        break;
                    case ContactAttributes::WEDDING_ANNIVERSARY:
                        $wedding =  'Wedding Anniversary: '.$value->value;;
                        break;
                    case ContactAttributes::CLOSING_ANNIVERSARY:
                        $closing =  'Closing Anniversary: '.$value->value;;
                        break;
                    case ContactAttributes::SPOUSE_BIRTHDAY:
                        $spouseBirthday =  'Spouse Birthday: '.$value->value;;
                        break;
                }
            }
            $specialDates .= ($birthday)? $birthday: '';
            $specialDates .= ($spouseBirthday)? (($specialDates) ? '<br />'.$spouseBirthday: $spouseBirthday) : '';
            $specialDates .= ($wedding)? (($specialDates) ? '<br />'.$wedding: $wedding) : '';
            $specialDates .= ($closing)? (($specialDates) ? '<br />'.$closing: $closing) : '';

            $relationships = ($specialDates) ? '<br />' : '';
            foreach(ContactRelationshipLu::model()->byEitherContactId($this->id)->findAll() as $relationship) {
                $relationships .= ($relationships) ? '<br />' : '';

                if($relationship->origin_contact_id == $contact->id) {
                    $relationships .= '('.$relationship->originRelationshipType->name.') ';
                    $relationships .= '<a href="/'.Yii::app()->controller->module->id.'/contacts/'.$relationship->relatedContact->id.'" target="_blank">'.$relationship->relatedContact->fullName.'</a>';
                } elseif($relationship->related_to_contact_id == $contact->id) {
                    $relationships .= '('.$relationship->relatedRelationshipType->name.') ';
                    $relationships .= '<a href="/'.Yii::app()->controller->module->id.'/contacts/'.$relationship->originContact->id.'" target="_blank">'.$relationship->originContact->fullName.'</a>';
                }
            }

            return $specialDates.$relationships;
        }

        public function printWorkAnniversary($contact)
        {
            $workAnniversary = Yii::app()->db->createCommand("select value from setting_contact_values where setting_id=".Settings::SETTING_ANNIVERSARY_ID." AND contact_id={$contact->id}")->queryScalar();
            if($workAnniversary) {
                return Yii::app()->format->formatDate($workAnniversary);
            }
        }
	}
