<?php

class VoicemailBroadcastsAction extends CAction
{
    /**
     * Run
     *
     * @retun void
     */
    public function run()
    {
        // Set page title
        $this->controller->title = 'Voicemail Broadcasts';

        // Handle postback if we have one
        if(count($_POST)) {

            // Check to make sure our file upload is actually a file upload
            if(!is_uploaded_file($_FILES['audio_file']['tmp_name'])) {
                $this->_renderView(false, 'Please upload choose a valid wav format audio file to upload');
            }

            // Check MIME type to make sure file is a valid audio file
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if(!in_array($finfo->file($_FILES['audio_file']['tmp_name']), array('audio/wav', 'audio/x-wav'))) {
                 $this->_renderView(false, 'Please upload choose a valid wav format audio file to upload, [' . $finfo->file($_FILES['audio_file']['tmp_name']) . '] is not a valid format');
            }

            // Check name entry
            if(empty($_POST['name'])) {
                $this->_renderView(false, 'Please enter a valid name');
            }

            // Create new record
            $voicemailBroadcastMesssage = new VoicemailBroadcastMessages();
            $voicemailBroadcastMesssage->account_id = Yii::app()->user->accountId;
            $voicemailBroadcastMesssage->name = $_POST['name'];
            $voicemailBroadcastMesssage->added_by = Yii::app()->user->id;
            $voicemailBroadcastMesssage->added = new CDbExpression('NOW()');

            // Save record
            if(!$voicemailBroadcastMesssage->save()) {
                throw new Exception('Error! Unable to save new voicemail broadcast message record! ' . print_r($voicemailBroadcastMesssage->getErrors(), true));
            }

            // Create new S3 client and store file in S3
            $s3 = \Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());
            $result = $s3->putObject(array(
                'Bucket'        =>  (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com",
                'Key'           =>  'site-files/' . Yii::app()->user->clientId . '/broadcast-messages/' . $voicemailBroadcastMesssage->id . '.wav',
                'SourceFile'    =>  $_FILES['audio_file']['tmp_name'],
                'ContentType'   =>  'audio/wav'
            ));

            if($result['ObjectURL']) {
                $message = 'Your new message has been added successfully!';
            }
        }

        // Render the view
        $this->_renderView(true, $message);
    }

    /**
     * Render View
     *
     * Helper function to render the view w/ some messages
     * @param bool $status When True, a green message, when false, a red one
     * @param string $message
     */
    protected function _renderView($success = true, $message = '')
    {
        // Render the view
        $this->controller->render('voicemailBroadcasts', array(
            'model'     =>  (new VoicemailBroadcastMessages()),
            'success'   =>  $success,
            'message'   =>  $message
        ));

        // Terminate app
        Yii::app()->end();
    }
}