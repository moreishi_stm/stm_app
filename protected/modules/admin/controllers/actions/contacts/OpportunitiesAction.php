<?php
    Yii::import('admin_module.controllers.actions.contacts.ListAbstractAction');

    class OpportunitiesAction extends ListAbstractAction {

        public $view = 'opportunities';
		/**
		 * Manages models
		 */

        protected function getDataProvider(Contacts $Contact) {
            $this->controller->title = 'Opportunities';

            $DataProvider = $Contact->search();
            $sort = new CSort();
            $sort->defaultOrder = 't.added DESC';
            $sort->attributes = array(
                'added_date'=>array(
                    'asc'=>'t.added ASC',
                    'desc'=>'t.added DESC',
                ),
                'source.name'=>array(
                    'asc'=>'source.name ASC',
                    'desc'=>'source.name DESC',
                ),
                'first_name',
                'last_name',
//                'emails'=>array(
//                    'asc'=>'emails.email ASC',
//                    'desc'=>'emails.email DESC',
//                ),
            );

            $DataProvider->sort = $sort;
            return $DataProvider;
        }

        protected function beforeRender(Contacts &$Contact) {

        }

        public function printContactInfo(Contacts $Contact) {
            $contactInfo = '<strong><u>'.$Contact->fullName.'</u></strong><br />';
            $contactInfo .= ($phones = $Contact->phones) ? Yii::app()->format->formatPrintPhones($phones).'<br />': '';
            $contactInfo .= ($emails = $Contact->emails) ? Yii::app()->format->formatPrintEmails($emails).'<br />': '';

            return $contactInfo;
        }

        public function printActivityInfo(Contacts $Contact) {
            $activityInfo  = '<div>'.Yii::app()->format->formatGridValue('Last Login:', Yii::app()->format->formatDays($Contact->last_login)).'</div>';
            $activityInfo .= '<div>'.Yii::app()->format->formatHomeViewSaved($Contact->id).'</div>';

//            $activityInfo .= '<div>'.Yii::app()->format->formatGridValue('Last Activity: ', Yii::app()->format->formatDays($Contact->lastActivityLog->activity_date, array('defaultValue'=>'None', 'daysLabel'=>'day', 'daysLabelConcat'=>'ago'))).'</div>';
//            $activityInfo .= '<div>'.Yii::app()->format->formatGridValue('Next Task: ', Yii::app()->format->formatDays($Contact->nextTask->due_date, array('defaultValue'=>'None','isFuture'=>true))).'</div>';

            $lastSpokeDate = $Contact->lastSpokeDate();
            $activityInfo .= ($lastSpokeDate) ? Yii::app()->format->formatDateTimeDays($Contact->lastSpokeDate(), array("break"=>true)): '';

//            $contactInfo = '<strong><u>'.$Contact->fullName.'</u></strong><br />';
//            $contactInfo .= ($phones = $Contact->phones) ? Yii::app()->format->formatPrintPhones($phones).'<br />': '';
//            $contactInfo .= ($emails = $Contact->emails) ? Yii::app()->format->formatPrintEmails($emails).'<br />': '';
//            $contactInfo .=	"<div>".Yii::app()->format->formatHomeViewSaved($Contact->id)."</div>";

            return $activityInfo;
        }

        public function printSpecialDatesRelationships(Contacts $Contact) {
            $specialDates = '';
            $contactAttributesValues = ContactAttributeValues::model()->byContactIdAttributesIds($Contact->id, array(ContactAttributes::BIRTHDAY, ContactAttributes::CLOSING_ANNIVERSARY, ContactAttributes::WEDDING_ANNIVERSARY))->findAll();

            foreach($contactAttributesValues as $key => $value) {
                switch ($value->contact_attribute_id) {
                    case ContactAttributes::BIRTHDAY:
                        $birthday = 'Birthdays: '.$value->value;
                        break;
                    case ContactAttributes::WEDDING_ANNIVERSARY:
                        $wedding =  'Wedding Anniversary: '.$value->value;;
                        break;
                    case ContactAttributes::CLOSING_ANNIVERSARY:
                        $closing =  'Closing Anniversary: '.$value->value;;
                        break;
                }
            }
            $specialDates .= ($birthday)? $birthday: '';
            $specialDates .= ($wedding)? (($specialDates) ? '<br />'.$wedding: $wedding) : '';
            $specialDates .= ($closing)? (($specialDates) ? '<br />'.$closing: $closing) : '';

            $relationships = ($specialDates) ? '<br />' : '';
            foreach(ContactRelationshipLu::model()->byEitherContactId($Contact->id)->findAll() as $relationship) {
                $relationships .= ($relationships) ? '<br />' : '';

                if($relationship->origin_contact_id == $Contact->id) {
                    $relationships .= '('.$relationship->originRelationshipType->name.') ';
                    $relationships .= '<a href="/'.Yii::app()->controller->module->id.'/contacts/'.$relationship->relatedContact->id.'" target="_blank">'.$relationship->relatedContact->fullName.'</a>';
                } elseif($relationship->related_to_contact_id == $Contact->id) {
                    $relationships .= '('.$relationship->relatedRelationshipType->name.') ';
                    $relationships .= '<a href="/'.Yii::app()->controller->module->id.'/contacts/'.$relationship->originContact->id.'" target="_blank">'.$relationship->originContact->fullName.'</a>';
                }
            }

            return $specialDates.$relationships;
        }
	}
