<?php

/**
 * Views a ActivityLog.
 *
 */
class ViewRelationshipAction extends CAction
{
    public function run($id) {

        $model = ContactRelationshipLu::model()->findByPk($id);

        $this->performAjaxRequest($model);
    }

    protected function performAjaxRequest(ContactRelationshipLu $model) {

        if (Yii::app()->request->isAjaxRequest) {

            echo CJSON::encode(array(
                'id'                              => $model->id,
                'origin_relationship_type_id'     =>$model->origin_relationship_type_id,
                'origin_relationship_name'        =>$model->originRelationshipType->name,
                'origin_contact_id'               =>$model->origin_contact_id,
                'origin_contact_fullName'         =>$model->originContact->fullName,
                'related_to_contact_id'           =>$model->related_to_contact_id,
                'related_to_contact_fullName'     =>$model->relatedContact->fullName,
                'related_to_relationship_type_id' =>$model->related_to_relationship_type_id,
                'related_relationship_name'       =>$model->relatedRelationshipType->name,
                'is_referral'                     =>$model->is_referral,
                'is_past_referral'                =>$model->is_past_referral,
                'notes'                           =>$model->notes,
            ));

            Yii::app()->end();
        }
    }
}