<?php

/**
 * Creates a new task.
 *
 */
class TransferAction extends CAction {

	public function run($id=null)
    {
        $this->controller->title = 'Assignments Leads';

        $model = new Contacts;
        $Criteria = $model->byActiveAdmins(true)->getDbCriteria();
        $Criteria->order = 'first_name ASC';

        $contactsList = CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName');

        if (Yii::app()->request->isAjaxRequest && !isset($_GET['ajax']) && isset($_POST['fromId']) && !empty($_POST['fromId']) && isset($_POST['toId']) && !empty($_POST['toId'])) {
            $this->performAjaxRequest($_POST['fromId'], $_POST['toId'], $_POST['transactionStatusId']);
        }

        $dataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>100)));
        $this->controller->render('transfer', array(
                'dataProvider' => $dataProvider,
                'contactsList' => $contactsList,
                'nurtureListData' => CHtml::listData(TransactionStatus::getNurturingStatusList(ComponentTypes::SELLERS), "transaction_status_id", "transactionStatus.name"),
            )
        );
	}

    public function getLeadData($contactId, $transactionStatusId, $mode='count')
    {
        $command = Yii::app()->db->createCommand()
            ->from('assignments a')
            ->where('a.assignee_contact_id=:contactId', array(':contactId'=>$contactId))
            ->andWhere('t.transaction_status_id=:transactionStatusId', array(':transactionStatusId'=>$transactionStatusId))
            ->andWhere(array('IN','t.component_type_id', array(ComponentTypes::SELLERS, ComponentTypes::BUYERS)))
            ->andWhere('a.is_deleted=0 AND a.is_protected=0')
            ->join('transactions t','a.component_id=t.id AND a.component_type_id=t.component_type_id');

        switch($mode)  {
            case 'count':
                return $command->select('count(a.id)')->queryScalar();
                break;

            case 'assignmentIds':
                return $command->select('a.id')->queryColumn();
                break;
        }
    }

    protected function performAjaxRequest($fromId, $toId, $transactionStatusId)
    {
        if (Yii::app()->request->isAjaxRequest) {
            // get all assignmentIds for that contact
            $assignmentIds = $this->getLeadData($fromId, $transactionStatusId, $mode='assignmentIds');
            if(empty($assignmentIds)) {
                return;
            }

            $dbTransaction = Yii::app()->db->beginTransaction();

            $errors = array();
            foreach($assignmentIds as $assignmentId) {

                //@todo: see if they set appointment or met appointment???

                //@todo: check for duplicates

                //@todo: any flagged for non-transfer???? - how to indicate

                $assignment = Assignments::model()->findByPk($assignmentId);
                $assignment->assignee_contact_id = $toId;

                // don't send emails since administrative mass update
                $assignment->alertChange = false;

                // log in activity log the reason for transfer
                $assignment->setScenario('massUpdate');

                if(!$assignment->save()) {
                    $errors[] = array('assignment_id'=> $assignment->id, 'errors'=>$assignment->getErrors());
                }
            }

            if(empty($errors)) {
                $oldValue = array('taskIds'=>$assignmentIds,'assignee_contact_id'=>$fromId);
                $newValue = array('assignee_contact_id'=>$toId);
                $historyLog = new HistoryLog('transfer');
                $historyLog->setAttributes(array('model_name'=>'Assignments',
                                                 'primary_key'=>$fromId,
                                                 'action_type_ma'=>HistoryLog::ACTION_TYPE_TRANSFER,
                                                 'old_value'=>CJSON::encode($oldValue),
                                                 'new_value'=>CJSON::encode($newValue),
                                                 'added_by'=>Yii::app()->user->id,
                                                 'added'=>new CDbExpression('NOW()')
                    ));
                if(!$historyLog->save()) {
                    echo CActiveForm::validate($historyLog);
                    Yii::log('History Log could not save for assignment transfer.', CLogger::LEVEL_ERROR);
                    $dbTransaction->rollback();
                }
                else {
                    $dbTransaction->commit();
                }
            }
            else {
                $dbTransaction->rollback();
                Yii::log(__CLASS__.' (:'.__LINE__.') Errors found with Mass Assignment Transfer.', CLogger::LEVEL_ERROR);
            }

//            $task = new Tasks;
//            if($task->updateAll(array('assigned_to_id'=>$toId, 'updated_by'=>Yii::app()->user->id),
//                                $condition='complete_date IS NULL AND assigned_to_id=:assigned_to_id',
//                                $params=array(':assigned_to_id'=>$fromId)) > 0) {
//
//            }

            Yii::app()->end();
        }
    }
}
