<?php

/**
 * Creates a Activity Log
 */
class AddRelationshipAction extends CAction {

	public function run() {
		$model = new ContactRelationshipLu('insert');
		$this->processAjaxRequest($model);
	}

	protected function processAjaxRequest(ContactRelationshipLu $model) {
		if (Yii::app()->request->isAjaxRequest) {
			if (isset($_POST['ContactRelationshipLu'])) {
                $model->attributes = $_POST["ContactRelationshipLu"];

                if (!$model->save()) {
                    echo CActiveForm::validate($model);
                }
			}

			Yii::app()->end();
		}
	}
}
