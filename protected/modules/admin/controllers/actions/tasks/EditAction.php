<?php

/**
 * Edits an existing task.
 *
 */
class EditAction extends CAction {

	public function run($id) {
        $model = $this->controller->loadModel($id);
        $this->performAjaxRequest($model);
	}

    protected function performAjaxRequest(Tasks $model) {

        if (Yii::app()->request->isAjaxRequest) {

            $taskRecursion = $model->taskRecursion;
            $taskRecursionAttributes = ($taskRecursion)? (object) $taskRecursion->attributes : null;

            // find out if task recursion attributes have changed and need updating, if so delete all recurring tasks and recreate
            if($this->isDiffTaskRecursion($taskRecursionAttributes, (object) $_POST['TaskRecursions'])) {

                // delete all tasks and recreate
                if($taskRecursion && ($primaryTask = $this->deleteAllRecursionTasks($taskRecursion->id))) {
                    // Remove the recursion record
                    $taskRecursion->delete();
                    unset($taskRecursion);
                } else {
                    $primaryTask = $model;
                }

                // recreate recursion tasks
                if(isset($_POST['TaskRecursions']['recur_type']) && ($_POST['TaskRecursions']['recur_type'] != 'One time only')) {
                    $taskRecursion = new TaskRecursions;
                    $taskRecursion->attributes = $_POST['TaskRecursions'];
                    $validateTaskRecursion = CActiveForm::validate($taskRecursion);
                    if($validateTaskRecursion !== '[]') {
                        echo $validateTaskRecursion;
                        Yii::app()->end();
                    }
                    if(!$taskRecursion->save()) {
                        Yii::log(__CLASS__.' ('.__LINE__.'): Task recursion did not save properly. Attributes values: ' . print_r($taskRecursion->attributes, true), CLogger::LEVEL_ERROR);
                    }

                    // assign post values to task so it saves and applies to all recursion tasks - ex. change description
                    $primaryTask->attributes = $_POST['Tasks'];

                    $primaryTask->task_recursion_id = $taskRecursion->id;

                    if(!$primaryTask->save()) {
                        echo CActiveForm::validate($primaryTask);
                        Yii::app()->end();
                    }
                    $taskRecursion->createRecursionTasks($primaryTask);
                }
                $status = 'success';
            }
            // task attributes changed so update all tasks in recursion such as assigned to, priority, task_type, description, due_date
            elseif($taskRecursion && ($this->isDiffModelProperty($model->task_type_id, $_POST['TaskRecursions']['task_type_id']) || $this->isDiffModelProperty($model->description, $_POST['TaskRecursions']['description']) || $this->isDiffModelProperty($model->assigned_to_id, $_POST['TaskRecursions']['assigned_to_id']) || $this->isDiffModelProperty($model->is_priority, $_POST['TaskRecursions']['is_priority']))) {

                // update tasks attributes task_type_id, description, assigned_to_id EXCLUDING due_date and task_recursion_id
                $criteria = new CDbCriteria();
                $criteria->condition = 'task_recursion_id=:id AND complete_date IS NULL';
                $criteria->params = array(':id' => $model->taskRecursion->id);
                $criteria->order = 'id ASC';

                $existingTasks = Tasks::model()->findAll($criteria);

                // update each task
                foreach($existingTasks as $existingTask) {
                    if(isset($_POST['Tasks']) && $existingTask->is_deleted !== 1) {
                        $existingTask->task_type_id = $_POST['Tasks']['task_type_id'];
                        $existingTask->assigned_to_id = $_POST['Tasks']['assigned_to_id'];
                        $existingTask->description = $_POST['Tasks']['description'];
                        $existingTask->operation_manual_id = $_POST['Tasks']['operation_manual_id'];
                        $existingTask->is_priority = $_POST['Tasks']['is_priority'];
                        if(!$existingTask->save()) {
                            echo CActiveForm::validate($model);
                            Yii::log(__CLASS__.' ('.__LINE__.'): Updating existing recursion Task did not save properly. Attributes values: ' . print_r($existingTask->attributes, true), CLogger::LEVEL_ERROR);
                            Yii::app()->end();
                        }
                    }
                }
                $status = 'success';
            }

            //scenario for edit for normal "One time only"
            elseif(!isset($_POST['TaskRecursions']['recur_type']) || !$_POST['TaskRecursions']['recur_type'] || $_POST['TaskRecursions']['recur_type'] == 'One time only') {
                $model->attributes = $_POST['Tasks'];
                if(!$model->save()) {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
                $status = 'success';
            }

            echo CJSON::encode(array('status'=>$status));
            Yii::app()->end();
        }
    }

    protected function isDiffTaskRecursion($oldRecursion, $newRecursion)
    {
        $isDiff = false;
        switch(1) {
            case $oldRecursion==null && ($newRecursion->recur_type == 'One time only' || $newRecursion->recur_type === null):
                $isDiff = false;
                break;
            case $oldRecursion==null && $newRecursion->recur_type !== 'One time only':
                $isDiff = true;
                break;
            case $oldRecursion->recur_type !== $newRecursion->recur_type:
                $isDiff = true;
                break;
            case $oldRecursion->recur_type == $newRecursion->recur_type:
                // format date for comparision
                $newRecurEndAfter = (property_exists($newRecursion, 'recur_end_after_occurrences'))? $newRecursion->recur_end_after_occurrences : null;
                $oldStartDate = ($oldRecursion->recur_start_date)? date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($oldRecursion->recur_start_date)) : null;
                $newStartDate = ($newRecursion->recur_start_date)? date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($newRecursion->recur_start_date)) : null;
                $oldEndDate = ($oldRecursion->recur_end_date)? date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($oldRecursion->recur_end_date)) : null;
                $newEndDate = ($newRecursion->recur_end_date)? date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($newRecursion->recur_end_date)) : null;

                // compare old and new values to see if it's different
                // NOTE: recur_repeat_every and recur_end_after_occurrences is not a editable value yet
                if($this->isDiffModelProperty($oldRecursion->recur_end_after_occurrences, $newRecurEndAfter) || $this->isDiffModelProperty($oldStartDate, $newStartDate) || $this->isDiffModelProperty($oldEndDate, $newEndDate)) {
                    $isDiff = true;
                }

                break;
        }
        return $isDiff;
    }

    protected function isDiffModelProperty($oldValue, $newValue)
    {
        if(($oldValue === $newValue) || ($oldValue==null && $newValue=='')) {
            return false;
        } else {
            return true;
        }
    }

    protected function deleteAllRecursionTasks($taskRecursionId)
    {
        // Get all existing task occurrences
        $criteria = new CDbCriteria();
        $criteria->condition = 'task_recursion_id=:id';
        $criteria->params = array(':id' => $taskRecursionId);
        $criteria->order = 'id ASC';
        $tasks = Tasks::model()->findAll($criteria);

        // Remove reference and recursion from original task
        $primaryTask = $tasks[0];
        $primaryTask->task_recursion_id = null;
        $primaryTask->save();

        // Remove the first record from our result set we will iterate for deletion
        unset($tasks[0]);

        if(count($tasks) > Tasks::MAX_TASK_DELETE) {
            Yii::log(__CLASS__.' ('.__LINE__.') **** URGENT Error ****: '.count($tasks).' is exceeds the delete tasks limit of '.Tasks::MAX_TASK_DELETE.'. This process was terminated. User did not get intended result. May be a bug. Please verify with user.', CLogger::LEVEL_ERROR, __CLASS__);
            Yii::app()->plivo->sendMessage('9043433200', 'STM Task Delete ERROR: '.count($tasks).' task delete attempted. See error log ASAP.');
            Yii::app()->end();
        }

        // Remove all recursion tasks
        foreach($tasks as $task) {

            if(!$task->delete()) {
                // Yii::log(__CLASS__.' ('.__LINE__.') Error in deleting task ID# '.$task->id.' for recursion. Check all '.count($tasks).' tasks with task_recursion_id '.$taskRecursionId.' for the is_deleted status. Task attributes: '.print_r($task->attributes, true), CLogger::LEVEL_ERROR);
            }
        }
        return $primaryTask;
    }
}
