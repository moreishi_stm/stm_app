<?php
	Yii::import('admin_widgets.DateRanger.DateRanger');

	/**
	 * Lists all outstanding tasks
	 *
	 */
	class ListAction extends CAction {

		public function run() {
			if($_GET["massUpdate"]==="1" && !empty($_GET["taskIds"])){
				$this->createPdf();
			}

		//die("<pre>".print_r($_GET,1));

            $this->controller->pageColor = 'blue';
			$this->controller->title = 'My Tasks';

			$DateRanger = new DateRanger();
            $DateRanger->defaultSelect = 'up_to_next_1_week';
			$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

            // if it's not an ajax call and there is a cookie for the preset, use it
            if(!isset($_GET['DateRanger']) && isset($_COOKIE['TaskDateRanger']['date_preset'])) {

                $dateRange['date_preset'] = $_COOKIE['TaskDateRanger']['date_preset'];
                $dateRange['custom_from_date'] = $_COOKIE['TaskDateRanger']['custom_from_date'];
                $dateRange['custom_to_date'] = $_COOKIE['TaskDateRanger']['custom_to_date'];
                $dateRange = $DateRanger->getDateRange($_COOKIE['TaskDateRanger']);

            } elseif($_GET['DateRanger']['date_preset']) {

                // set cookie for preset from ajax call
                setcookie("TaskDateRanger[date_preset]", $_GET['DateRanger']['date_preset'], time()+3600*24*30);
                if($_GET['DateRanger']['custom_from_date']) {
                    setcookie("TaskDateRanger[custom_from_date]", $_GET['DateRanger']['custom_from_date'], time()+3600*24*30);
                }
                if($_GET['DateRanger']['custom_to_date']) {
                    setcookie("TaskDateRanger[custom_to_date]", $_GET['DateRanger']['custom_to_date'], time()+3600*24*30);
                }
            }


            $id = ($_GET['contact_id']) ? $_GET['contact_id'] : Yii::app()->user->id;
            $addedByContactId = ($_GET['added_by']) ? $_GET['added_by'] : null;

			if (!empty($_GET['task_types'])) {
				$taskTypeIds = array($_GET['task_types']);
			} else {
				$taskTypeIds = null;
			}

            if (!empty($_GET['component_type_id'])) {
                $componentTypeIds = array($_GET['component_type_id']);
            } else {
                $componentTypeIds = null;
            }

			// Retrieve a count of overdue items for the selected period
			$overdueCriteria = Tasks::model()->byAssignedTo($id)->byAddedBy($addedByContactId)->dateRange($dateRange)->overdue()->byTaskType($taskTypeIds)->excludeTaskTypeIds(TaskTypes::AUTO_EMAIL_DRIP)->byComponentTypeIds($componentTypeIds)->notCompleted()->asc()->byPriority()->getDbCriteria();
			$overdueCount = Tasks::model()->count($overdueCriteria);

			// Retrieve task records (the first ones setup) for those that use recursion
			$criteria = new CDbCriteria();
			$criteria->condition = 'task_recursion_id IS NOT NULL';
            $criteria->compare('assigned_to_id', $id);
			$criteria->group = 'task_recursion_id';

			// Get all tasks in result set
			$tasks = Tasks::model()->findAll($criteria);

            // Period end date used for recursion
            if(isset($dateRange['to_date']) && $dateRange['to_date'] !== '0000-00-00') {
                $periodEnd = new DateTime($dateRange['to_date']);
            }
            // if no end period (All time) use the Task Recursion constant for # of months to pre-populate in the future
            else {
                $periodEnd = new DateTime(date('Y-m-d'));
                $periodEnd->add(new DateInterval('P'.TaskRecursions::PRE_POPULATE_MONTHS.'M'));
            }

			// Process recursion tasks for each task
			foreach($tasks as $task) {
                if($task->taskRecursion) {
                    if($task->taskRecursion->recur_type == 'One time only') {
                        // check to see if there are any other tasks associated with this recursion just in case. If unexpected result of finding any tasks, alert as error.
                        //Yii::log(__CLASS__.' ('.__LINE__.') **** URGENT Error ****: '.count($tasks).' is exceeds the limit to delete tasks. This process was terminated. User did not get intended result. May be a bug. Please verify with user.', CLogger::LEVEL_ERROR, __CLASS__);

                        $taskOneTimeOnly = Tasks::model()->countByAttributes(array('task_recursion_id'=> $task->taskRecursion->id));

                        // not supposed to have recursion record if one time only, delete it as clean up
                        if(count($taskOneTimeOnly)==1) {
                            $task->taskRecursion->delete();
                        }
                    }
                    elseif($task->taskRecursion->recur_start_date == null) {
                        Yii::log(__CLASS__.' ('.__LINE__.') Task Recursion start_date missing. Attributes: '.print_r($task->taskRecursion->attributes, true), CLogger::LEVEL_ERROR);

                    }
                    elseif(($task->taskRecursion->recur_start_date !== null) && ($task->taskRecursion->recur_end_date !== null && $task->taskRecursion->recur_end_date > date('Y-m-d'))) { // &&

                        // see if the max date available for this task is beyond the timeframe scope, if so create more unless timeframe has no ending then just go to max scope
                        $maxDate = $task->getRecurringMaxDate();
                        $maxDate = new DateTime($maxDate);

                        if($maxDate->format('Y-m-d') < $periodEnd) {
                            $task->taskRecursion->createRecursionTasks($task, $periodEnd->format('Y-m-d'));
                        }
                    }
                }
			}

			// Criteria for task list based on filters
			$Criteria = Tasks::model()->byAssignedTo($id)
				->byAddedBy($addedByContactId)
				->dateRange($dateRange)
				->byTaskType($taskTypeIds)
				->excludeTaskTypeIds(TaskTypes::AUTO_EMAIL_DRIP)
				->byComponentTypeIds($componentTypeIds)
				->notCompleted()->byPriority()->asc()->getDbCriteria();


            if (!empty($_GET['order_by'])) {

                switch($_GET['order_by']) {
                    case 'due_date_asc':
                        break;

                    case 'component':
                        $Criteria->order = 'CONCAT(component_type_id, component_id), due_date asc, id ASC';
                        break;
                }
            }

			// Data provider for grid



			$dataProvider = new CActiveDataProvider('Tasks', array(
				'criteria' => $Criteria,
				'pagination' => array(
					'pageSize' => 100,
				),
			));

			// Render the list view and pass it the data for the grid
			$this->controller->render('list', array(
					'dataProvider' => $dataProvider,
					'overdueCount' => $overdueCount,
                    'contactId'=> $id,
                    'hideTaskListCompleteButton' => Yii::app()->user->checkAccess('hideTaskListCompleteButton'),
                    'hideTaskListDeleteButton' => Yii::app()->user->checkAccess('hideTaskListDeleteButton'),
				)
			);
		}

		private function createPdf(){
			$pdfPages = array();
			foreach ($_GET["taskIds"] as $id){
				$task = Tasks::model()->findByPk($id);

				if(!$task->getContactId() || !($contact = $task->getContact())){
					continue;
				}
				$ActionPlanLu = new ActionPlanAppliedLu();

				$planItem = $ActionPlanLu->findByAttributes(array("task_id"=>$task->id));
				$ActionPlanItem = new ActionPlanItems();
				$plan = $ActionPlanItem->findByPk($planItem->action_plan_item_id);
				if(!empty($plan->letter_template_id)){
					$letter = new LetterTemplates();
					$letter = $letter->findByPk($plan->letter_template_id);
					if(!empty($letter)){
						$pdfPages[] = $letter->filterBodyByContactData($contact,$task->getComponentModel());
					}

				}else{
					//die("no id {$task->id}");
				}
			}
			if(empty($pdfPages)){
				return;
			}

			header("Content-type:application/pdf");
            // It will be called downloaded.pdf
			header("Content-Disposition:attachment;filename='downloaded.pdf'");

            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator("Seize the Market");
            $pdf->SetAuthor('seizethemarket.com');
            $pdf->SetTitle('Print Letters');
            $pdf->SetSubject('Letters to print');
            $pdf->SetKeywords('pdf letters');

            // remove default header/footer
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set font
			$pdf->SetFont('times', '', 11);

            // add a page
			foreach ($pdfPages as $p){
				$pdf->AddPage();
				$pdf->writeHTML($p, true, false, true, false, '');
			}

            //Close and output PDF document
			$pdf->Output("seizethemarket_letters_".date("Ymdhis").".pdf", 'I');
			Yii::app()->end();
		}
	}