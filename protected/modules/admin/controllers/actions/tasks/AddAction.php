<?php

/**
 * Creates a new task.
 *
 */
class AddAction extends CAction {

	public function run() {
        $model = new Tasks;
        $this->performAjaxRequest($model);
	}

	protected function performAjaxRequest(Tasks $model)
    {
		if (Yii::app()->request->isAjaxRequest) {

            if(isset($_POST['TaskRecursions']['recur_type']) && $_POST['TaskRecursions']['recur_type'] != 'One time only') {

                $taskRecursion = new TaskRecursions;
                $taskRecursion->attributes = $_POST['TaskRecursions'];
                $validateTaskRecursion = CActiveForm::validate($taskRecursion);
                if($validateTaskRecursion !== '[]') {
                    echo $validateTaskRecursion;
                    Yii::app()->end();
                }
            }
			// If we have task info, update it
			if (isset($_POST['Tasks'])) {

				// Add task info to model
                $model->attributes = $_POST["Tasks"];

                // validate maxDueDate @todo: this is a manual as we Beta this on CLT - 11/26/2015
                if(Yii::app()->user->clientId==1 && in_array($model->component_type_id, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS))) {

                    if($model->task_type_id == TaskTypes::PHONE && date("Y-m-d", strtotime($model->due_date)) > date("Y-m-d", strtotime("+1 month"))) {

                        $model->addError('due_date','Phone follow-up date cannot be more than 1 month in the future. This is Beta feature so please contact support for any suggestions.');

                        $result[CHtml::activeId($model,'due_date')][]=$model->getError('due_date');

                        echo CJSON::encode($result);
                        Yii::app()->end();
                    }
                }

                // Save the task
				if (!$model->save()) {
					echo CActiveForm::validate($model);
                    Yii::app()->end();
				}
				else {
					// Process task recursion things (needs to run after save method due to IDs)
                    if(isset($_POST['TaskRecursions']['recur_type']) && $_POST['TaskRecursions']['recur_type'] != 'One time only' && isset($taskRecursion)) {
                        $taskRecursion->attributes = $_POST['TaskRecursions'];
                        if(!$taskRecursion->save()) {
                            Yii::log(__CLASS__.' ('.__LINE__.'): Task recursion did not save properly. Attributes values: ' . print_r($taskRecursion->attributes, true), CLogger::LEVEL_ERROR);
                        }
                        else {
                            $model->task_recursion_id = $taskRecursion->id;
                            $model->save();
                            $taskRecursion->createRecursionTasks($model);
                        }
                    }

					if(!$model->save()) {
                        echo CJSON::encode($model->getErrors());
                        Yii::app()->end();
                    }
				}
            }

            $status = 'success';
            echo CJSON::encode(array('status'=>$status));
            Yii::app()->end();
		}
	}
}
