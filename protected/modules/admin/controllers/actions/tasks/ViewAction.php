<?php

	/**
	 * Views a task.
	 *
	 */
	class ViewAction extends CAction {

		public function run($id) {

			$model = $this->controller->loadModel($id);

			$this->performAjaxRequest($model);
		}

		protected function performAjaxRequest(Tasks $model) {

			if (Yii::app()->request->isAjaxRequest) {

                $addressString = '';
                if($model->component_type_id == ComponentTypes::SELLERS) {
                    $seller = Sellers::model()->findByPk($model->component_id);
                    if($address = $seller->address) {
                        $addressString = $address->address.' '.$address->city.' '.$address->state->short_name.' '.$address->zip;
                        $addressString = str_replace(' ','+', $addressString);
                    }
                }

                // if google calendar event exists, see if user has permission.
                $hasCalendarPermission = null;
                if($model->google_calendar_id && $model->google_calendar_event_id) {
                    $hasCalendarPermission = false;
                    Yii::import('admin_module.components.StmOAuth2.Google', true);
                    $stmGoogle = new \StmOAuth2\Google();

                    $success = false;
                    try {
                        $listCalendars = $stmGoogle->listCalendars();
                        $success = true;
                    }
                    catch (\Google_Auth_Exception $e) {
                        echo "Your Google Calendar token is invalid. Please try again or re-do your Integration under Tools => Integration.";
                    }
                    catch (\Exception $e) {
                        \Yii::log(__CLASS__.' (:'.__LINE__.") Google Calendar List error: " . $e->getMessage(), \CLogger::LEVEL_ERROR);
                    }

                    if($success) {
                        foreach($listCalendars as $calendar) {
                            if($model->google_calendar_id == $calendar->id) {
                                $hasCalendarPermission = true;
                            }
                        }
                    }
                }

                $data = array(
					"id" => $model->id,
					"fullName" => $model->contact->fullName,
					"due_date" => Yii::app()->format->formatDate($model->due_date, 'm/d/Y h:i a'),
					"assigned_to_id" => $model->assigned_to_id,
					"task_type_id" => $model->task_type_id,
					"component_id" => $model->component_id,
					"component_type_id" => $model->component_type_id,
					"is_priority" => $model->is_priority,
					"description" => $model->description,
                    "operation_manual_id" => $model->operation_manual_id,
                    "complete_date" => (boolean) $model->complete_date,
                    "address"          => $addressString,
                    "google_calendar_id" => $model->google_calendar_id,
                    "google_calendar_event_id" => $model->google_calendar_event_id,
                    "has_calendar_permission" => $hasCalendarPermission,
				);

				// Get the task recursion, if we had one
				$taskRecursion = $model->taskRecursion;

				// Get the task recursion record, if we have one
				if($taskRecursion) {
                    $data['task_recursion_id'] = $taskRecursion->id;
					$data['recur_type'] = $taskRecursion->recur_type;
					$data['recur_repeat_every'] = $taskRecursion->recur_repeat_every;
					$data['recur_end_after_occurrences'] = $taskRecursion->recur_end_after_occurrences;
					$data['recur_start_date'] = ($taskRecursion->recur_start_date)? Yii::app()->format->formatDate($taskRecursion->recur_start_date) : '';
					$data['recur_end_date'] = ($taskRecursion->recur_end_date)? Yii::app()->format->formatDate($taskRecursion->recur_end_date): '';
//					$data['is_deleted'] = $taskRecursion->is_deleted;
				}

				echo CJSON::encode($data);

				Yii::app()->end();
			}
		}
	}
