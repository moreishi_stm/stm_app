<?php

class ListMiniAction extends CAction {

	/**
	 * Displays a particular model.
	 * @param $componentTypeId
	 * @param $componentId
	 * @return void
	 * @internal param int $id the ID of the model to be displayed
	 */
	public function run($componentTypeId, $componentId) {
		$tasksProvider = Tasks::getAll($componentId, $componentTypeId);

		$ParentModel = $this->retrieveParentModel($componentTypeId, $componentId);

		$this->controller->renderPartial('_listMini', array(
			'parentModel'  => $ParentModel,
			'dataProvider' => $tasksProvider,
			'componentTypeId' => $componentTypeId,
			'componentId' => $componentId,
		));
	}

	/**
	 * Utility method for properly determining and invoking the proper lookup based on the component type id needed here.
	 * TODO: Think about maybe creating a Component factory which can optionally retrieve stubs or an actual record.
	 *
	 * @param $componentTypeId the component type we desire.
	 * @param $componentId the specific instance of the given component type that is needed.
	 * @return Component a component corresponding to the given metadata regarding type and primary key.
	 */
	private function retrieveParentModel($componentTypeId, $componentId) {
		$Component = null;

		switch ($componentTypeId) {
			case ComponentTypes::BUYERS:
			case ComponentTypes::SELLERS:
			case ComponentTypes::LISTINGS:
				$Component = Transactions::model()->findByPk($componentId);
				break;
			case ComponentTypes::CLOSINGS:
				$Component = Closings::model()->findByPk($componentId);
				break;
			case ComponentTypes::CONTACTS:
				$Component = Contacts::model()->findByPk($componentId);
				break;
		}

		return $Component;
	}
}
