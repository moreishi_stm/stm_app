<?php

/**
 * Creates a new task.
 *
 */
class CompleteAction extends CAction
{
	public function run()
	{
        $model = $this->controller->baseModel;
        $this->performAjaxRequest($model);
	}

    protected function performAjaxRequest(Tasks $model) {

        if (Yii::app()->request->isAjaxRequest) {

            $taskId = Yii::app()->request->getQuery('id');
            $model=$this->controller->loadModel($taskId);

            $model->complete_date = date("Y-m-d H:i:s",time());
            if($model->save()) {
                $status = 'success';
            }
            else {
                $status = 'error';
            }

            echo CJSON::encode(array('status'=>$status));
            Yii::app()->end();
        }
    }
}