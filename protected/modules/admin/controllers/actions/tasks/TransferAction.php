<?php

/**
 * Creates a new task.
 *
 */
class TransferAction extends CAction {

	public function run($id=null) {
        if(Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('tasksTransfer')) {

        }

        $this->controller->title = 'Transfer Tasks';

        $model = new Contacts;
        $Criteria = $model->byActiveAdmins(true)->getDbCriteria();
        $Criteria->order = 'first_name ASC';

        $contactsList = CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName');

        if (Yii::app()->request->isAjaxRequest && !isset($_GET['ajax']) && isset($_POST['fromId']) && !empty($_POST['fromId']) && isset($_POST['toId']) && !empty($_POST['toId'])) {
            $this->performAjaxRequest($model, $_POST['fromId'], $_POST['toId']);
        }

        $dataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>100)));
        $this->controller->render('transfer', array(
                'dataProvider' => $dataProvider,
                'contactsList' => $contactsList,
            )
        );
	}

    protected function performAjaxRequest(Contacts $model, $fromId, $toId) {
        if (Yii::app()->request->isAjaxRequest) {

            // get all incomplete tasks from original person
            if($tasksToTransfer = Tasks::model()->notCompleted()->byAssignedTo($fromId)->findAll()) {
                $taskIds = array();
                foreach($tasksToTransfer as $taskToTransfer) {
                    array_push($taskIds, $taskToTransfer->id);
                }
            }

            $task = new Tasks;
            if($task->updateAll(array('assigned_to_id'=>$toId, 'updated_by'=>Yii::app()->user->id),
                                $condition='complete_date IS NULL AND assigned_to_id=:assigned_to_id',
                                $params=array(':assigned_to_id'=>$fromId)) > 0) {

                $oldValue = array('taskIds'=>$taskIds,'assigned_to'=>$fromId);
                $newValue = array('assigned_to'=>$toId);
                $historyLog = new HistoryLog('transfer');
                $historyLog->setAttributes(array('model_name'=>'Tasks',
                                                 'primary_key'=>$fromId,
                                                 'action_type_ma'=>HistoryLog::ACTION_TYPE_TRANSFER,
                                                 'old_value'=>CJSON::encode($oldValue),
                                                 'new_value'=>CJSON::encode($newValue),
                                                 'added_by'=>Yii::app()->user->id,
                                                 'added'=>new CDbExpression('NOW()')
                    ));
                if(!$historyLog->save()) {
                    echo CActiveForm::validate($model);
                    Yii::log('History Log could not save for task transfer.', CLogger::LEVEL_ERROR, 'taskTransfer');
                }
            }

            Yii::app()->end();
        }
    }
}
