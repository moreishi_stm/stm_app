<?php

/**
 * Performs the AJAX validation.
 * @param CModel the model to be validated
 */
class ValidateAction extends CAction
{
	public function run()
	{
        $model = new Tasks;

        if (isset($_POST['ajax']) && $_POST['ajax']==='user-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
	}
}