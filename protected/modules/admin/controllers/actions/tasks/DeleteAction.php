<?php

//
// This is where the delete things go for the tasks, handle one time only AND recurring (different options chosen on UI)
//

/**
 * Deletes a Task
 *
 */
class DeleteAction extends CAction {

	public function run($id)
	{
		// we only allow deletion via POST request
		if (Yii::app()->request->isPostRequest || true) {

			// Retrieve and delete task
			$taskId = $id;

			// If we want to delete all occurrences
			if($_POST['delete_type'] == 'All') {

				// Retrieve task recursion record, if we have one, retrieve all tasks for it
				$taskRecursion = $this->controller->loadModel($taskId)->taskRecursion;
				if($taskRecursion) {

					// Retrieve all tasks associated with this recursion and delete them
					$tasks = $taskRecursion->tasks;
                    if(count($tasks) > Tasks::MAX_TASK_DELETE) {
                        Yii::log(__CLASS__.' ('.__LINE__.') **** URGENT Error ****: '.count($tasks).' is exceeds the limit of '.Tasks::MAX_TASK_DELETE.' to delete tasks. This process was terminated. User did not get intended result. May be a bug. Please verify with user.', CLogger::LEVEL_ERROR, __CLASS__);
                        Yii::app()->plivo->sendMessage('9043433200', 'STM Task Delete ERROR: '.count($tasks).' task delete attempted. See error log ASAP.');
                        Yii::app()->end();
                    }

					foreach($tasks as $task) {
						$task->delete();
					}
				}
			}
			else {
                $this->controller->loadModel($taskId)->delete();
			}


            $status = 'success';
		}
		else {
            $status = 'error';
		}

        echo CJSON::encode(array('status'=>$status));
		Yii::app()->end();
	}
}