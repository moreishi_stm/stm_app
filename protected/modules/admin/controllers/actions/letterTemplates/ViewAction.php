<?php

class ViewAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id, $contactId=null) {

		$model = LetterTemplates::model()->findByPk($id);

	    $this->performAjaxRequest($model, $contactId);

		$this->controller->title = 'Letter Template - ';

		$this->controller->render('view',array(
			'model'=>$model
		));
	}

	protected function performAjaxRequest($model, $contactId=null) {

		if (Yii::app()->request->isAjaxRequest) {

            if ($contactId) {
                $contact = Contacts::model()->findByPk($contactId);

                $componentTypeId = Yii::app()->request->getParam('componentTypeId');
                $componentId = Yii::app()->request->getParam('componentId');

                $componentModel = ComponentTypes::getComponentModel($componentTypeId, $componentId);

                $content = $model->filterBodyByContactData($contact, $componentModel);
            } else {
                $content = $model->clearTemplateTags();
            }

			echo CJSON::encode(array(
                "id" => $model->id,
				"subject" => $model->subject,
				"content" => $content,
                "description" => $model->description,
                "signature" => Yii::app()->user->settings->email_signature,
			));

			Yii::app()->end();
		}
	}
}
