<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Letter Templates List';
		$model = new LetterTemplates();
        if($_COOKIE['EmailTemplates']) {
            $model->attributes = $_COOKIE['EmailTemplates'];
        }

		if (isset($_GET['LetterTemplates'])) {
			$model->attributes=$_GET['LetterTemplates'];
            foreach($_GET['LetterTemplates'] as $field => $value) {
                setcookie("LetterTemplates[{$field}]", $value, time()+3600*24*30);
            }
		}

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
