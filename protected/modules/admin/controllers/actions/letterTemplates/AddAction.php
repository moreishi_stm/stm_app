<?php
include_once(Yii::getPathOfAlias('admin_module.extensions.') . DS . 'SimpleHtmlDom.php');

class AddAction extends CAction {

    /**
     * Manages models
     */
    public function run() {

        $this->controller->title = 'Letter Templates List';
        $model = new LetterTemplates();
        if (isset($_POST['LetterTemplates'])) {

            // Set attributes
            $model->attributes = $_POST['LetterTemplates'];
			$model->added_by = Yii::app()->user->id;
			$model->added = date("Y-m-d H:i:s");

            // Attempt to save
            if ($model->save()) {
                // Report success
                Yii::app()->user->setFlash('success', 'Successfully added Email Template.');
                $this->controller->redirect('/'.Yii::app()->controller->module->name.'/letterTemplates/' . $model->id);
            }else{
				Yii::app()->user->setFlash('error', 'There was a problem saving your letter. Please fix any errors and try again.');
			}
        }
        // Render view
        $this->controller->render('form', array(
                'model' => $model
            )
        );
    }
}
