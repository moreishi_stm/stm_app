<?php

class EditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id = NULL)
	{
        // Load up model
		if(!empty($id)){
			$model = LetterTemplates::model()->findByPk($id);
			if(empty($model->id)){
				Yii::app()->user->setFlash('error', 'The letter you are looking for does not exist. Lets create a new one');
				$this->controller->redirect(array("add"));
			}
		} else {
			$this->controller->redirect(array("add"));
		}
        // Do this only if we have POST data
		if (Yii::app()->request->getPost('LetterTemplates')) {
			$model->attributes = Yii::app()->request->getPost('LetterTemplates');
			$model->updated_by = yii::app()->user->id;
			$model->added = date("Y-m-d H:i:s");
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully updated Letter Template.');
				$this->controller->redirect(array('view','id'=>$model->id));
			} else {
				Yii::app()->user->setFlash('error', 'There was a problem saving your changes. Please fix any errors and try again.');
			}
		}
		$this->controller->title = 'Letter Template Edit';
		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}