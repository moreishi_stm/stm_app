<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class DeleteAction extends CAction {

    public function run($id)
    {
        $model = Documents::model()->findByPk($id);
        if (isset($_POST['Documents'])) {
            $model->attributes = $_POST['Documents'];
        }

        $this->performAjaxRequest($model);
    }

    protected function performAjaxRequest(Documents $model) {

        if (Yii::app()->request->isAjaxRequest) {
            $model->is_deleted = 1;
            if (!$model->save()) {

                echo CJSON::encode(array('status'=> 'error', 'message'=> 'Document did not delete. Error Message: '.$model->getErrors()[0]));
            }
            else {
                echo CJSON::encode(array('status'=> 'success'));
            }

            Yii::app()->end();
        }
    }
}