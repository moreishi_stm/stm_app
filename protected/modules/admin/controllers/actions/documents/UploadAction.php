<?php

/**
 * Uploads a new document.
 *
 */
class UploadAction extends CAction {

	public function run() {

        $model = new Documents;

		if (Yii::app()->request->isAjaxRequest)
			$this->performAjaxRequest($model);


        if (isset($_POST['Documents'])) {

			if (isset($_POST['ajax'])) {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			$model->attributes = $_POST['Documents'];

	        if ($model->save()) {

	        	// Set the component type
				$ComponentType    = ComponentTypes::model()->findByPk($model->component_type_id);
				$componentTypeStr = strtolower(str_replace(' ', '', $ComponentType->name));

				// Set common directories
				$docDirectoryRoot = Yii::app()->getBasePath().DS.'..'.DS.'docs';
				$docDirectory     = $docDirectoryRoot.DS.$componentTypeStr.DS.$model->record_id;

				//Check Directory Structure
				//Check and Create RecordID Folder
                if (!is_dir($docDirectory)) {
					mkdir($docDirectory, $mode=0777, $recursive=true);
                }

	            $model->document  = CUploadedFile::getInstance($model, 'file_name');
	            $model->file_name = $model->id.'.'.$model->document->extensionName;

	            // Save the file
	            $path = $docDirectory.DS.$model->file_name;
	            $model->document->saveAs($path);

	            // Save the rest of the model attributes
	            $model->file_extension = $model->document->extensionName;
	            $model->file_size = $model->document->size;
	            $model->file_path = $path;
	            $model->save(false); // no need to validate this data
			}
        }

        //Take us back to where we were.
        $redirectUrl = (Yii::app()->user->returnUrl) ? Yii::app()->user->returnUrl : Yii::app()->request->requestUri;
        Yii::app()->controller->redirect($redirectUrl);
	}

	protected function performAjaxRequest(Tasks $model) {
		if (Yii::app()->request->isAjaxRequest) {

			if (isset($_POST['Documents'])) {
				$model->attributes = $_POST['Documents'];

				if (!$model->save()) {
					echo CActiveForm::validate($model);
				}
			}

			Yii::app()->end();
		}
	}
}
