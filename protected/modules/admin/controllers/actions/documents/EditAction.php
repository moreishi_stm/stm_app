<?php
Yii::import('admin_widgets.ActivityDetailsPortlet.ActivityDetailsPortlet');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class EditAction extends CAction {

	public function run($id) {
		$model = $this->controller->loadModel($id);

		if (Yii::app()->request->isAjaxRequest)
			$this->performAjaxRequest($model);

		if (isset($_POST['Documents'])) {
            $fileName = $model->file_name;
			$model->attributes = $_POST['Documents'];
            if(!$model->fileReplaceFlag) {
                $model->file_name = $fileName;
            }

			if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Updated Document!');
            }
			else {
                Yii::app()->user->setFlash('error', 'Error: Document did not save.');
            }
		}
		Yii::app()->user->setState('activityDetailPortletLeftTabView',ActivityDetailsPortlet::LEFT_ACTIVE_TAB_DOCS);
		$redirectUrl = (Yii::app()->user->getState('documentReferrer')) ? Yii::app()->user->getState('documentReferrer') : Yii::app()->request->requestUri;
		Yii::app()->controller->redirect($redirectUrl);
	}

	protected function performAjaxRequest(Documents $model) {
		if (Yii::app()->request->isAjaxRequest) {
			if (isset($_POST['Documents'])) {
				$fileName = $model->file_name;
				$model->attributes = $_POST['Documents'];
				if(!$model->fileReplaceFlag)
					$model->file_name = $fileName;

				echo CActiveForm::validate($model);
			}

			Yii::app()->end();
		}
	}
}
