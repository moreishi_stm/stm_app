<?php

class ListAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Documents List';
		$model=new Documents('search');
		$model->unsetAttributes();  // clear any default values
		$model->visibility_ma = Documents::VISIBILITY_TEAM;
		$model->document_type_id = DocumentTypes::TYPE_MISC;

		// used for gridview search box inputs
        if (isset($_GET['Documents'])) {
			$model->attributes=$_GET['Documents'];
        }

		$this->controller->render('list',array(
			'model'=>$model,
            'dataProvider' => $model->search(),
			'componentType' => ComponentTypes::model()->findByPk(ComponentTypes::CONTACTS),
		));
	}
}
