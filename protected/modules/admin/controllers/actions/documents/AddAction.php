<?php
Yii::import('admin_widgets.ActivityDetailsPortlet.ActivityDetailsPortlet');

	/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction {

	public function run() {
		$model = $this->controller->baseModel;

//		if (Yii::app()->request->isAjaxRequest)
//			$this->performAjaxRequest($model);

		if (isset($_POST['Documents'])) {
			$model->attributes = $_POST['Documents'];
			if (!$model->save()) {
				echo CActiveForm::validate($model);
//				Yii::app()->user->setFlash('error', 'Error Adding Document.');
				Yii::app()->end();
			} else {
				$DocumentPermission = new DocumentPermissions;
				$DocumentPermission->document_id = $model->id;
				$DocumentPermission->component_type_id = $model->componentTypeId;
				$DocumentPermission->component_id = $model->componentId;
				$DocumentPermission->permission_level_ma = DocumentPermissions::PERMISSION_LEVEL_COMPONENT;

				if(!$DocumentPermission->save()) {
					Yii::app()->user->setFlash('error', 'Error: Document Permission did not save.');
					//@todo: Log Error
				}
			}

			Yii::app()->user->setFlash('success', 'Successfully Added Document!');

		} else {
			// File did not save correctly, handle error...
			Yii::app()->user->setFlash('error', 'Error: Document did not save.');
		}

		Yii::app()->user->setState('activityDetailPortletLeftTabView',ActivityDetailsPortlet::LEFT_ACTIVE_TAB_DOCS);
		$redirectUrl = (Yii::app()->user->getState('documentReferrer')) ? Yii::app()->user->getState('documentReferrer') : Yii::app()->request->requestUri;
		Yii::app()->controller->redirect($redirectUrl);
	}

	protected function performAjaxRequest(Documents $model) {
		if (Yii::app()->request->isAjaxRequest) {
			if (isset($_POST['Documents'])) {
				$model->attributes = $_POST['Documents'];
				echo CActiveForm::validate($model);
			}

			Yii::app()->end();
		}
	}
}
