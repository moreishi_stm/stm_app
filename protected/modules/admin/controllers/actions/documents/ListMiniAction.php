<?php

class ListMiniAction extends CAction {

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($componentTypeId, $componentId) {

		$documentsProvider = Documents::getAll($componentTypeId, $componentId);
		$this->controller->renderPartial('_listMini', array(
			'dataProvider'    => $documentsProvider,
			'componentTypeId' => $componentTypeId,
			'componentId'     => $componentId,
		));
	}
}
