<?php
/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class ViewAction extends CAction {

	public function run($id) {

		$model = $this->controller->loadModel($id);

		if (Yii::app()->request->isAjaxRequest) {
			$this->performAjaxRequest($model);
		}

        // New Document method, only do this for select people
        //if(0) {
//        if(strpos($_SERVER['HTTP_HOST'], 'christineleeteam') !== false) {

            // Get component name
            $componentTypeEntry = ComponentTypes::model()->findByPk($model->documentPermissions[0]->component_type_id);
            $componentName = $componentTypeEntry->getComponentName();

            // Determine the full file path
            $url = strtr('https://'.((YII_DEBUG) ? 'd7p4c6zeo248w' : 'd3r4abrvltzvc').'.cloudfront.net/:clientId/:componentName/:componentId/:id', array(
                ':clientId'         =>  Yii::app()->user->clientId,
                ':componentName'    =>  $componentName,
                ':componentId'      =>  $model->documentPermissions[0]->component_id,
                ':id'               =>  $model->id
            ));

            // New CloudFront client
            $cfc = Aws\CloudFront\CloudFrontClient::factory(array(
                'key_pair_id'   =>  'APKAJTZQI3R5N5JR5LVA',
                'private_key'   =>  Yii::getPathOfAlias('admin_module') . '/certs/pk-APKAJTZQI3R5N5JR5LVA.pem'
            ));

            // Generate custom policy for CloudFront URL signing
            $policy = array(
                "Statement"	=>	array(
                    array(
                        'Resource'	=>	$url,
                        'Condition'	=>	array(
                            'DateLessThan'	=>	array(
                                'AWS:EpochTime'	=>	time() + (60 * 5)
                            )
                        )
                    )
                )
            );

            // If we are not in development, add an IP restriction.
            if(!YII_DEBUG) {
                $ip = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                $policy['Statement'][0]['Condition']['IpAddress'] = array(
                    'AWS:SourceIp'	=>	$ip . '/32'
                );
            }

            // Generate signed URL for access
            $url = $cfc->getSignedUrl(array(
                'url'			=>	$url,
                'policy'		=>	Zend_Json_Encoder::encode($policy),
                'key_pair_id'	=>	'APKAJTZQI3R5N5JR5LVA',
                'private_key'	=>	Yii::getPathOfAlias('admin_module') . '/certs/pk-APKAJTZQI3R5N5JR5LVA.pem'
            ));

            // Redirect the user to CloudFront using the generated Signed URL
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: ' . $url);

//        }
//
//        else {
//
//            // Retrieve file, if it exists, output it
//            $file = $model->getFilePath().DS.$model->id.'.'.$model->file_extension;
//            if (file_exists($file)) {
//                header('Content-Description: File Transfer');
//                header('Content-Type: '.$this->getContentType($model->file_extension)); //application/octet-stream
//                header('Content-Disposition: inline; filename='.basename($model->file_name)); //attachment makes is download
//                header('Content-Transfer-Encoding: binary');
//                header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() - 3600));
//                header('Cache-Control: must-revalidate');
//                header('Pragma: public');
//                header('Content-Length: ' . filesize($file));
//                ob_clean();
//                flush();
//                readfile($file);
//		    }
//        }

        // Exit
		Yii::app()->end();
	}

	public function getContentType($fileType) {
		switch (strtolower($fileType)) {
			case "pdf": $ctype="application/pdf"; break;
			case "exe": $ctype="application/octet-stream"; break;
			case "zip": $ctype="application/zip"; break;
            case "docx": case "doc": $ctype="application/msword"; break;
            case "xlsx": case "xls": $ctype="application/vnd.ms-excel"; break;
            case "pptx": case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "jpeg":
			case "jpg": $ctype="image/jpg"; break;
			default: $ctype="application/force-download";
		}

		return $ctype;
	}

	protected function performAjaxRequest(Documents $model) {

		if (Yii::app()->request->isAjaxRequest) {

			echo CJSON::encode(array(
							   "id"                => $model->id,
							   "component_id"      => $model->documentPermissions[0]->component_id,
							   "component_type_id" => $model->documentPermissions[0]->component_type_id,
							   "document_type_id"  => $model->document_type_id,
							   "description"       => $model->description,
							   "visibility_ma"     => $model->visibility_ma,
							   ));

			Yii::app()->end();
		}
	}
}
