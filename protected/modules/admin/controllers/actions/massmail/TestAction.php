<?php
/**
 * Test Action
 * 
 * @author Nicole Xu
 */
class TestAction extends CAction {
	
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'Test Action';

		// Retrieve references so we can get API client IDs
		$constantContact = StmMassMailFactory::factory(StmMassMailFactory::MASS_MAIL_ADAPTER_CONSTANT_CONTACT);
		$mailChimp = StmMassMailFactory::factory(StmMassMailFactory::MASS_MAIL_ADAPTER_MAIL_CHIMP);
		
//		$ccResponse = $constantContact->getListContacts('1124493556');
//		$mcResponse = $mailChimp->getListContacts('63ac4ee104');
		
		$ccResponse = $constantContact->addContactsToList('1124493556');
		$mcResponse = $mailChimp->addContactsToList('63ac4ee104');		
		
		
		print_r($ccResponse);
		print_r($mcResponse);
		exit;
	}
}
