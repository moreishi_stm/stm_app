<?php
/**
 * Authorize Action
 *
 * @author Nicole Xu
 */
class AuthorizeAction extends CAction {

	/**
	 * Manages models
	 */
	public function run()
	{
		// Types we allow
		$types = array(
			'ConstantContact'	=> StmMassMailFactory::MASS_MAIL_ADAPTER_CONSTANT_CONTACT,
			'MailChimp'			=> StmMassMailFactory::MASS_MAIL_ADAPTER_MAIL_CHIMP
		);

		// Make sure we have a valid type
		$type = $_GET['type'];
		if(!in_array($type, $types)) {
			throw new Exception('Invalid value for type');
		}

		// Our mass mail object
		$massMail = StmMassMailFactory::factory($types[$type]);

		// If we don't have a code, stop right here
		$code = $_GET['code'];
		if(!$code) {
			throw new Exception('No authorization code has been found in the current request parameters');
		}

		// Retrieve the token information, this may include endpoint information as well
		$response = $massMail->getOAuthAccessToken($code);

		var_dump($response);exit;

		//
		// Store access token
		//


		// Check to see if we already have a setting for mass mail type
//		$massMailType = Settings::model()->findByAttributes(array(
//			'name'	=>	'massmail_type'
//		));

		// Create the new mass mail type if it doesn't already exist
//		if(!$massMailType) {
//			$massMailType = new Settings();
//			$massMailType->name = 'massmail_type';
//			$massMailType->applyAccountScope = Settings::SETTING_TYPE_ACCOUNT_ID;
//		}

		// Check to see if we already have a setting for mass mail authorization
//		$massMailAuthorization = Settings::model()->findByAttributes(array(
//			'name'	=>	'massmail_authorization'
//		));

		// Create the new mass mail type if it doesn't already exist
//		if(!$massMailAuthorization) {
//			$massMailAuthorization = new Settings();
//			$massMailAuthorization->name = 'massmail_type';
//			$massMailAuthorization->applyAccountScope = Settings::SETTING_TYPE_ACCOUNT_ID;
//		}
	}
}
