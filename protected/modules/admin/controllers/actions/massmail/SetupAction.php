<?php
/**
 * Setup Action
 * 
 * @author Nicole Xu
 */
class SetupAction extends CAction {
	
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'Mass Mail Setup';

		// Retrieve references so we can get API client IDs
		$constantContact = StmMassMailFactory::factory(StmMassMailFactory::MASS_MAIL_ADAPTER_CONSTANT_CONTACT);
		$mailChimp = StmMassMailFactory::factory(StmMassMailFactory::MASS_MAIL_ADAPTER_MAIL_CHIMP);
		
		// Render the view
		$this->controller->render('setup',array(
			'authorizeUrlConstantContact'	=>	$constantContact->getOAuthAuthorizeUrl(),
			'authorizeUrlMailChimp'			=>	$mailChimp->getOAuthAuthorizeUrl()
		));
	}
}
