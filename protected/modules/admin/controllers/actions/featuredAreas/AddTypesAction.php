<?php

class AddTypesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$model = new FeaturedAreaTypes;
        $this->controller->title = 'Add Featured Area Type';

		if (isset($_POST['FeaturedAreaTypes'])) {
			$model->attributes=$_POST['FeaturedAreaTypes'];

			if ($model->save()) {

				Yii::app()->user->setFlash('success', 'Successfully Added Featured Area Type.');
				$this->controller->redirect(array('types'));
			}
		}

		$this->controller->render('formTypes',array(
			'model'=>$model
		));
	}
}