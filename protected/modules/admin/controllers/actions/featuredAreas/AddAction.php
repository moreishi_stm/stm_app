<?php
include_once(Yii::getPathOfAlias('admin_module.components.') . '/StmAws/S3/FileManagement.php');

class AddAction extends CAction {
	/**
	 * Manages models
	 */
	
	public function run($id) {

        $featuredAreasModel = new FeaturedAreasCriteriaForm();
//        var_dump($featuredAreasModel->rules());exit;

		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/jquery.mjs.nestedSortable.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/dropzone.js?V=1', CClientScript::POS_END);
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/standard2/assets/css/font-awesome.css');
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/dropzone.basic.min.css');
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/dropzone.min.css');
		
		$model=$this->controller->baseModel;
        if(!Yii::app()->user->hasMultipleBoards()) {
            $model->mls_board_id = Yii::app()->user->board->id;
        }
        $this->controller->title = 'Add Featured Area';

        $termComponentLu[] = new TermComponentLu;

		$messages = array();
		
		$imagesData = NULL;
		
		if (isset($_POST['FeaturedAreas'])) {
			
			$model->attributes=$_POST['FeaturedAreas'];
            $model->status_ma = 1;

			if(isset($_POST['imageData']) && !empty($_POST['imageData'])) {
				$imagesData = json_decode($_POST['imageData']);
			}
			
			if ($model->save()) {

                $errorFlag = false;
//                if (isset($_POST['TermComponentLu'])) {
//                    $termComponentLu[0]->attributes=$_POST['TermComponentLu'];
//                    $termComponentLu[0]->component_type_id = ComponentTypes::FEATURED_AREAS;
//                    $termComponentLu[0]->component_id = $model->id;
//                    if (!$termComponentLu[0]->save()) {
//                        $errors = $termComponentLu[0]->getErrors();
//                        foreach($errors as $attribute => $messages) {
//                            $message = $messages[0];
//                            break;
//                        }
//                        Yii::app()->user->setFlash('error', 'Criteria Error: '.$message);
//                        $errorFlag = true;
//                    }
//                }

                // Handle saving criteria
                if (isset($_POST['FeaturedAreasCriteriaForm'])) {

                    // New saved home search form
                    $facf = new FeaturedAreasCriteriaForm();
                    $facf->attributes = $_POST['FeaturedAreasCriteriaForm'];

//                    // In case of errors
//                    if (!$facf->save()) {
//                        echo CActiveForm::validate($facf);
//                        Yii::app()->end();
//                    }

                    // Iterate through fields
                    $TermValues = array();
                    foreach($facf->fields as $field) {
                        if(is_array($facf->$field)) {
                            $groupId = null;
                            foreach($facf->$field as $submitValueSingle) {
                                if(!empty($submitValueSingle) || $submitValueSingle == '0') {
                                    $TermValue = new TermComponentLu;
                                    $TermValue->component_type_id = ComponentTypes::FEATURED_AREAS;
                                    $TermValue->component_id = $model->id;
                                    $TermValue->term_id = Terms::model()->getIdByName($field);
                                    $TermValue->value = $submitValueSingle;
                                    $TermValue->group_id = $groupId;

                                    if($groupId) {
                                        $TermValue->term_conjunctor = 'OR';
                                    }

                                    if($TermValue->value != null) {
                                        if(!$TermValue->save()) {
                                            echo CJSON::encode($TermValue->getErrors());
                                        } elseif(!$groupId) {
                                            $groupId = $TermValue->id;
                                        }
                                    }
                                }
                            }
                        } else {
                            $TermValue = new TermComponentLu;
                            $TermValue->component_type_id = ComponentTypes::FEATURED_AREAS;
                            $TermValue->component_id = $model->id;
                            $TermValue->term_id = Terms::model()->getIdByName($field);
                            $TermValue->value = $facf->$field;

                            if($TermValue->value != null) {
                                if(!$TermValue->save()) {
                                    echo CJSON::encode($TermValue->getErrors());
                                }
                            }
                        }
                        array_push($TermValues, $TermValue);
                    }

                }

				// We only will add new ones when one is not assigned to the contact or when one is deleted
				if ($model->typesCollection) {
                    $options = array(
						'collectionField'=>'typesCollection',
						'lookupRelation'=>'typeLu',
						'lookupModelName'=>'FeaturedAreaTypeLu',
						'foreignKeyColumn'=>'featured_area_id',
						'relationPKColumn'=>'featured_area_type_id',
					);
                    if(!StmFormHelper::multiSelectSaver($model,$options)) {
                        $errorFlag = true;
                    }
				}

                $model->calculateStats();
                //if(!$errorFlag) {
					if(!empty($imagesData)) {
						$s3Client = new \StmAws\S3\FileManagement('sites.seizethemarket.com');
						
						foreach($imagesData as $order => $imageData) {
							
							$imageFullPath = explode("/",$imageData->image_location);
							
							$target = "site-files/".rtrim(ltrim($_POST['directory'],"/"),"/")."/".$model->id."/".$imageFullPath[count($imageFullPath)-1];
							$thumbTarget = "site-files/".rtrim(ltrim($_POST['directory'],"/"),"/")."/".$model->id."/thumb_".$imageFullPath[count($imageFullPath)-1];
							
							$imageFullPath[count($imageFullPath)-1] = "thumb_".$imageFullPath[count($imageFullPath)-1];
							$thumbSrc = implode("/",$imageFullPath);
							
							$tmpid = '/tmp/featured-area-image-' . uniqid();
							
							$open = fopen($tmpid, "w");
							$wrote = fwrite($open, file_get_contents($imageData->image_location));
							fclose($open);
							
							$s3Client->addFile($tmpid, $target);
							$s3Client->deleteItem(str_replace($this->controller->baseCdnPath,'',$imageData->image_location));
							
							@unlink($tmpid);
							
							$open = fopen($tmpid, "w");
							$wrote = fwrite($open, file_get_contents($thumbSrc));
							fclose($open);
							
							$s3Client->addFile($tmpid, $thumbTarget);
							$s3Client->deleteItem(str_replace($this->controller->baseCdnPath,'',$thumbSrc));
							/*
							* @property string $id
							* @property integer $featured_area_id
							* @property string $file_location
							* @property string $alt_tag
							* @property string $name_tag
							* @property integer $sort_order
							* @property integer $updated_by
							* @property string $updated
							* @property integer $added_by
							* @property string $added
							* @property integer $is_deleted
							 */
					
							$featuredAreasImage = new FeaturedAreaImages();
							$featuredAreasImage->setAttribute('featured_area_id',$model->id);
							$featuredAreasImage->setAttribute('image_location', $this->controller->baseCdnPath.$target);
							$featuredAreasImage->setAttribute('alt_tag', '');
							$featuredAreasImage->setAttribute('name_tag', '');
							$featuredAreasImage->setAttribute('is_primary', $imageData->is_primary);
							$featuredAreasImage->setAttribute('sort_order', $order);
							$featuredAreasImage->setAttribute('added_by', Yii::app()->user->id);
							$featuredAreasImage->setAttribute('added', date("Y-m-d G:i:s", time()));
							
							$saved = $featuredAreasImage->save();
							
							// @TODO: figure out how to handle s3 or model
							// errors on the images
							if(!$saved) {
								$messages[] = $featuredAreasImage->model()->getErrors();
							}
						}
					}
					
					Yii::app()->user->setFlash('success', 'Successfully Added Featured Area.');
                    $this->controller->redirect(array('edit','id'=>$model->id), true);
				//}
			}
		}
		
		$this->controller->render('form',array(
			'baseCdnPath' => $this->controller->baseCdnPath,
			'error_messages' => $messages,
			'imageData' => $imagesData,
			'model'=>$model,
            'featuredAreasModel'=>$featuredAreasModel,
			'componentId' => (!empty($model->id) ? : 0),
			'componentName' => ComponentTypes::model()->findByPk(ComponentTypes::FEATURED_AREAS)->name,
			'termComponentLu'=>$termComponentLu
		));
	}
}