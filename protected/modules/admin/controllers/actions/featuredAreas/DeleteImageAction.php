<?php
include_once(Yii::getPathOfAlias('admin_module.components.') . '/StmAws/S3/FileManagement.php');

class DeleteImageAction extends CAction
{
	/**
	 * Manages models
	 */
    public function run() {

        $model = FeaturedAreaImages::model()->findByPk($_POST['imageId']);
        $this->performAjaxRequest($model);
    }
	
	protected function performAjaxRequest(FeaturedAreaImages $model) {

        if (!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array('results'=>'error','message'=>'Invalid Request.'));
            Yii::app()->end();
        }
		
		if(!YII_DEBUG) {
			$s3Client = new \StmAws\S3\FileManagement('sites.seizethemarket.com');
		} else {
			$s3Client = new \StmAws\S3\FileManagement('dev.sites.seizethemarket.com');
		}
		
		try {
			$s3Client->deleteItem(str_replace($this->controller->baseCdnPath,'',$model->image_location));
		} catch (\Exception $e) {
			echo CJSON::encode(array('results'=>false, 'message' => $e->getMessage()));
			Yii::app()->end();
		}
		
		$thumbLocation = explode("/",$model->image_location);
		$thumb = "thumb_".$thumbLocation[count($thumbLocation)-1];
		unset($thumbLocation[count($thumbLocation)-1]);
		
		$thumbLocation = rtrim(implode("/",$thumbLocation),"/")."/".$thumb;
		$s3Client->deleteItem(str_replace($this->controller->baseCdnPath,'',$thumbLocation));
		
		if ($model->delete()) {
			// TODO: we can't test this it will always return false...
			// this is a soft delete remove is_deleted to force hard delete
		}
		
		echo CJSON::encode(array('results'=>true));
		Yii::app()->end();
    }
}