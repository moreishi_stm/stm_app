<?php

class AssignImagesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id) {
		if (!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array('results'=>'error','message'=>'Invalid Request.'));
			Yii::app()->end();
		}
		
		$model = $this->controller->loadModel($id);
		
		if(empty($model)) {
			echo CJSON::encode(array('results'=>'error','message'=>'Could not find Featured Area.'));
			Yii::app()->end();
		}
		
		if(!isset($_POST['imageData']) || empty($_POST['imageData'])) {
			echo CJSON::encode(array('results'=>'error','message'=>'No File url found.'));
			Yii::app()->end();
		}
		
		$featuredAreasImages = FeaturedAreaImages::model()->findByAttributes(array('featured_area_id' => $model->id, 'image_location' => $_POST['imageData']['image_location']));
		if(!empty($FeaturedAreaImages)) {
			echo CJSON::encode(array('results'=>'error','message'=>'This image already exists for thie Featured Area.'));
			Yii::app()->end();
		}
		
		$sql = "SELECT COUNT(*) as `images_count` FROM `featured_area_images` WHERE `featured_area_id` = '".$model->id."'";
		$command = Yii::app()->db->createCommand($sql);
		$results = $command->queryAll();
		$numImages = (int)$results[0]["images_count"];
		
		$newImage = new FeaturedAreaImages();
		$newImage->setAttribute('featured_area_id',$model->id);
		$newImage->setAttribute('image_location', $this->controller->baseCdnPath.str_replace($this->controller->baseCdnPath,"",$_POST['imageData']['image']));
		$newImage->setAttribute('alt_tag', '');
		$newImage->setAttribute('name_tag', '');
		$newImage->setAttribute('is_primary', 0);
		$newImage->setAttribute('sort_order', $numImages);
		$newImage->setAttribute('added_by', Yii::app()->user->id);
		$newImage->setAttribute('added', date("Y-m-d G:i:s", time()));
		
		$newImage->save();
		
		$results = $newImage->attributes;
		$results['results'] = true;
		
		echo CJSON::encode($results);
		Yii::app()->end();
	}
}