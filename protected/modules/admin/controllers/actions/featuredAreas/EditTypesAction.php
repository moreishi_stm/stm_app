<?php

class EditTypesAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$model = FeaturedAreaTypes::model()->findByPk($id);
        $this->controller->title = 'Edit Featured Area Type';

		if (isset($_POST['FeaturedAreaTypes'])) {
			$model->attributes=$_POST['FeaturedAreaTypes'];

			if ($model->save()) {

				Yii::app()->user->setFlash('success', 'Successfully Updated Featured Area Type.');
				$this->controller->redirect(array('types'));
			}
		}

		$this->controller->render('formTypes',array(
			'model'=>$model
		));
	}
}