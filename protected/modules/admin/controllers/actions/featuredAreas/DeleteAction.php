<?php
include_once(Yii::getPathOfAlias('admin_module.components.') . '/StmAws/S3/FileManagement.php');

class DeleteAction extends CAction
{
	/**
	 * Manages models
	 */
    public function run($id) {

        $model = FeaturedAreas::model()->findByPk($id);

        $this->performAjaxRequest($model);
    }

    protected function performAjaxRequest(FeaturedAreas $model) {

        if (Yii::app()->request->isAjaxRequest) {

			$featuredAreasImages = FeaturedAreaImages::model()->findAllByAttributes(array('featured_areas_id' => $model->id));
			
			if(!empty($featuredAreasImages)) {
				if(!YII_DEBUG) {
					$s3Client = new \StmAws\S3\FileManagement('sites.seizethemarket.com');
				} else {
					$s3Client = new \StmAws\S3\FileManagement('dev.sites.seizethemarket.com');
				}
				
				foreach($featuredAreasImages as $image) {
					$s3Client->deleteItem(str_replace($this->controller->baseCdnPath,'',$image->image_location));
					$image->delete();
				}
			}
			
            if (!$model->delete()) {
                echo CActiveForm::validate($model);
            }

            Yii::app()->end();
        }
    }
}