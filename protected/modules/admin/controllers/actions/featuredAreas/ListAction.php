<?php

	class ListAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {
			$this->controller->title = 'Featured Areas';
			$model = new FeaturedAreas('search');
			$model->unsetAttributes(); // clear any default values
            $model->typesCollection = null;

			// used for gridview search box inputs
			if (isset($_GET['FeaturedAreas'])) {
				$model->attributes = $_GET['FeaturedAreas'];
			}

			$this->controller->render('list', array(
					'model' => $model
				)
			);
		}
	}