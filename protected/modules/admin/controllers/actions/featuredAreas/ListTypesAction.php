<?php

	class ListTypesAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {
			$this->controller->title = 'Featured Areas Types';
			$model = new FeaturedAreaTypes('search');
			$model->unsetAttributes(); // clear any default values

			// used for gridview search box inputs
			if (isset($_GET['FeaturedAreaTypes'])) {
				$model->attributes = $_GET['FeaturedAreaTypes'];
			}

			$this->controller->render('listTypes', array(
					'model' => $model
				)
			);
		}
	}