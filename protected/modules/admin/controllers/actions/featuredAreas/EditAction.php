<?php

class EditAction extends CAction
{
    public $multiSelectFields = array('area','zip','city','neighborhood','county','school_district');

	/**
	 * Manages models
	 */
	public function run($id)
	{
        $featuredAreasModel = new FeaturedAreasCriteriaForm();

		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/jquery.mjs.nestedSortable.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/dropzone.js?V=1', CClientScript::POS_END);
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/standard2/assets/css/font-awesome.css');
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/dropzone.basic.min.css');
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/dropzone.min.css');

		$model=$this->controller->loadModel($id);
        $this->controller->title = $model->name.' Featured Area';
        //@todo: this is a temp safety fill in as we add the mls_board_id column
        if(!Yii::app()->user->hasMultipleBoards() && !$model->mls_board_id) {
            $model->mls_board_id = Yii::app()->user->board->id;
        }

        // Get featured areas values
        $featuredAreaValues = TermComponentLu::model()->getFeaturedAreasValues($model->id);

        // Model featured area values
        $ajaxData = array();
        foreach($featuredAreaValues as $featuredAreaValue) {

            $termName = $featuredAreaValue->term->name;
            if(in_array($termName,$this->multiSelectFields)) {

                if(!isset($ajaxData[$termName]) || !is_array($ajaxData[$termName])) {
                    $ajaxData[$termName] = array();
                    array_push($ajaxData[$termName], $featuredAreaValue->value);
                }
                else
                    array_push($ajaxData[$termName], $featuredAreaValue->value);
            } else
                $ajaxData[$termName]=$featuredAreaValue->value;
        }

        // Assign values to model?
        foreach($ajaxData as $key => $value) {
            $featuredAreasModel->$key = $value;
        }

//        var_dump($ajaxData);exit;

        $termComponentLu = $model->getTermComponentLu();
        if(empty($termComponentLu)) {
            $termComponentLu[] = new TermComponentLu;
        }

		$Criteria = new CDbCriteria();
		$Criteria->condition = "featured_area_id = :featured_area_id";
		$Criteria->order = "sort_order ASC";
		$Criteria->params = array(
			':featured_area_id' => $model->id
		);

		$featuredAreasImages = FeaturedAreaImages::model()->findAll($Criteria);

		if (isset($_POST['FeaturedAreas'])) {
			$model->attributes=$_POST['FeaturedAreas'];

			if ($model->save()) {

                $errorFlag = false;
//                if (isset($_POST['TermComponentLu'])) {
//                    $termComponentLu[0]->attributes=$_POST['TermComponentLu'];
//                    $termComponentLu[0]->component_type_id = ComponentTypes::FEATURED_AREAS;
//                    $termComponentLu[0]->component_id = $model->id;
//                    if ($termComponentLu[0]->save()) {
//                    } else {
//                        $errors = $termComponentLu[0]->getErrors();
//                        foreach($errors as $attribute => $messages) {
//                            $message = $messages[0];
//                            break;
//                        }
//                        Yii::app()->user->setFlash('error', 'Criteria Error: '.$message);
//                        $errorFlag = true;
//                    }
//                }

                // Handle featured area stuff
                if (isset($_POST['FeaturedAreasCriteriaForm'])) {

                    // Set data
                    $featuredAreasModel->attributes = $_POST['FeaturedAreasCriteriaForm'];

                    // Handle featured areas
                    $formFields = $featuredAreasModel->fields;
                    foreach($formFields as $field) { // go through each possible submitted field
                        $termId = Terms::model()->getIdByName($field);
                        $submitValue = $featuredAreasModel->$field;

                        $TermValues = TermComponentLu::model()->getFeaturedAreaValueByTermId($termId, $model->id);
                        $count = count($TermValues);
                        if($count) {
                            if($count==1) {
                                $TermValue = $TermValues[0];
                                if(empty($submitValue) && $submitValue !='0'){ // delete if model exists and empty value submitted
                                    if(!$TermValue->delete())
                                        echo CJSON::encode($TermValue->getErrors());

                                } else { // if submit value not empty
                                    $isArray = is_array($submitValue);
                                    if($isArray && !empty($submitValue)) { //array indicates multiple values, need to have group_id implementation
                                        $key = array_search($TermValue->value,$submitValue);
                                        if($key != null || $key == 0) {
                                            unset($submitValue[$key]);
                                            if($submitValue != null) {
                                                $groupId = $TermValue->id;
                                                foreach($submitValue as $submitValueSingle) {
                                                    if(!empty($submitValueSingle) || $submitValueSingle == 0) {
                                                        $TermValue = new TermComponentLu;
                                                        $TermValue->component_type_id = ComponentTypes::FEATURED_AREAS;
                                                        $TermValue->component_id = $model->id;
                                                        $TermValue->term_id = $termId;
                                                        $TermValue->value = $submitValueSingle;
                                                        $TermValue->group_id = $groupId;

                                                        if($groupId) {
                                                            $TermValue->term_conjunctor = 'OR';
                                                        }

                                                        if(!$TermValue->save())
                                                            echo CJSON::encode($TermValue->getErrors());
                                                        elseif(!$groupId)
                                                            $groupId = $TermValue->id;
                                                    }
                                                }
                                            }
                                        } else {
                                            if(!$TermValue->delete())
                                                echo CJSON::encode($TermValue->getErrors());
                                            else {
                                                $groupId = TermComponentLu::model()->getParentIdFromFeaturedAreaValueByTermId($termId, $model->id);
                                                foreach($submitValue as $submitValueSingle) {
                                                    if(!empty($submitValueSingle) || $submitValueSingle == 0) {
                                                        $TermValue = new TermComponentLu;
                                                        $TermValue->component_type_id = ComponentTypes::FEATURED_AREAS;
                                                        $TermValue->component_id = $model->id;
                                                        $TermValue->term_id = $termId;
                                                        $TermValue->value = $submitValueSingle;
                                                        $TermValue->group_id = $groupId;

                                                        if($groupId) {
                                                            $TermValue->term_conjunctor = 'OR';
                                                        }

                                                        if(!$TermValue->save())
                                                            echo CJSON::encode($TermValue->getErrors());
                                                        elseif(!$groupId)
                                                            $groupId = $TermValue->id;
                                                    }
                                                }
                                            }
                                        }

                                    } else {
                                        $TermValue->value = $submitValue;
                                        if(!$TermValue->save())
                                            echo CJSON::encode($TermValue->getErrors());
                                    }
                                }
                            } elseif($count>1) {
                                // run through the models first, unsetting each submit value, then process what is left over in submitValue
                                foreach($TermValues as $TermValue) {
                                    if($submitValue == null)
                                        $key = null;
                                    else
                                        $key = array_search($TermValue->value,$submitValue);

                                    if($key || $key===0) {
                                        $TermValue->value = $submitValue[$key];
                                        if(!$TermValue->save())
                                            echo CJSON::encode($TermValue->getErrors());
                                        else
                                            unset($submitValue[$key]);
                                    } else {
                                        if(!$TermValue->delete())
                                            echo CJSON::encode($TermValue->getErrors());
                                    }
                                }

                                // run through whatever is left in submitValue
                                if(count($submitValue)>0)
                                    foreach($submitValue as $submitValueSingle) { // adds these values
                                        // check to see if other parents exist, if not record as parent, else record as child
                                        $groupId = 	TermComponentLu::model()->getParentIdFromFeaturedAreaValueByTermId($termId, $model->id);

                                        $TermValue = new TermComponentLu;
                                        $TermValue->component_type_id = ComponentTypes::FEATURED_AREAS;
                                        $TermValue->component_id = $model->id;
                                        $TermValue->term_id = $termId;
                                        $TermValue->value = $submitValueSingle;
                                        $TermValue->group_id = $groupId;

                                        if($groupId)
                                            $TermValue->term_conjunctor = 'OR';

                                        if(!$TermValue->save())
                                            echo CJSON::encode($TermValue->getErrors());
                                    }
                            }

                        } else { // isNewRecord
                            if($submitValue || $submitValue == '0') {
                                if(is_array($submitValue)) {
                                    $groupId = TermComponentLu::model()->getParentIdFromFeaturedAreaValueByTermId($termId, $model->id);
                                    foreach($submitValue as $submitValueSingle) {
                                        if(!empty($submitValueSingle) || $submitValueSingle == '0') {
                                            $TermValue = new TermComponentLu;
                                            $TermValue->component_type_id = ComponentTypes::FEATURED_AREAS;
                                            $TermValue->component_id = $model->id;
                                            $TermValue->term_id = $termId;
                                            $TermValue->value = $submitValueSingle;
                                            $TermValue->group_id = $groupId;

                                            if($groupId) {
                                                $TermValue->term_conjunctor = 'OR';
                                            }

                                            if(!$TermValue->save())
                                                echo CJSON::encode($TermValue->getErrors());
                                            elseif(!$groupId)
                                                $groupId = $TermValue->id;
                                        }
                                    }
                                } else {
                                    $TermValue = new TermComponentLu;
                                    $TermValue->component_type_id = ComponentTypes::FEATURED_AREAS;
                                    $TermValue->component_id = $model->id;
                                    $TermValue->term_id = $termId;
                                    $TermValue->value = $submitValue;

                                    if(!$TermValue->save())
                                        echo CJSON::encode($TermValue->getErrors());
                                }
                            }
                        }
                    }
                }


				// We only will add new ones when one is not assigned to the contact or when one is deleted
				if ($model->typesCollection) {
                    $options = array(
                                    'collectionField'=>'typesCollection',
                                    'lookupRelation'=>'typeLu',
                                    'lookupModelName'=>'FeaturedAreaTypeLu',
                                    'foreignKeyColumn'=>'featured_area_id',
                                    'relationPKColumn'=>'featured_area_type_id',
                                );
                    if(!StmFormHelper::multiSelectSaver($model,$options)) {
                        $errorFlag = true;
                    }
				}

                $model->calculateStats();
                //if(!$errorFlag) {

				#die("<pre>".print_r($_POST,1));

					if(isset($_POST['imageData']) && !empty($_POST['imageData'])) {

						$imageData = json_decode($_POST['imageData']);

						#die("<pre>".print_r($featuredAreasImages,1));

						foreach($imageData as $order => $image) {
							foreach($featuredAreasImages as $featuredAreasImage) {
								if(isset($image->image_id) && $image->image_id == $featuredAreasImage->id) {
									$featuredAreasImage->sort_order = $order;
									$featuredAreasImage->is_primary = $image->is_primary;
									$featuredAreasImage->save();
									break;
								}
							}
						}
					}

                    Yii::app()->user->setFlash('success', 'Successfully updated Featured Area.');
                    $this->controller->redirect(array('edit','id'=>$model->id), true);
                //}
			}
		}else{
			$model->typesCollection = StmFormHelper::multiSelectLoader($model, $options=array('relationName'=>'typeLu','relationPKColumn'=>'featured_area_type_id'));
		}

		$this->controller->render('form',array(
			'baseCdnPath' => $this->controller->baseCdnPath,
			'imageData' => $featuredAreasImages,
			'model'=>$model,
            'featuredAreasModel'=>$featuredAreasModel,
			'componentId' => $model->id,
			'componentName' => ComponentTypes::model()->findByPk(ComponentTypes::FEATURED_AREAS)->name,
			'termComponentLu'=>$termComponentLu
		));
	}
}