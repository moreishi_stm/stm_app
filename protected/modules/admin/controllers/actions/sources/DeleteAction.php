<?php

class DeleteAction extends CAction
{
	/**
	 * Manages models
	 */
    public function run($id) {

        $Source = Sources::model()->findByPk($id);

        $this->performAjaxRequest($Source);
    }
    protected function performAjaxRequest(Sources $model) {

        if (Yii::app()->request->isAjaxRequest) {
            $deleted = !$model->delete();
            echo CJSON::encode(array('deleted' => $deleted));

            Yii::app()->end();
        }
    }
}
