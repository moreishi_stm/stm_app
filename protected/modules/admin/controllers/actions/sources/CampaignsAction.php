<?php

class CampaignsAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id=null) {
		$view = ($id)? 'formCampaigns' : 'listCampaigns';
		$model= new SourceCampaigns;
		$model->unsetAttributes();  // clear any default values

		if ($id) {
			$model = $model->findByPk($id);

			if (isset($_POST['SourceCampaigns'])) {
				$model->attributes = $_POST['SourceCampaigns'];

				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Successfully Updated Campaign.');
					$this->controller->redirect(array('campaigns','id'=>$model->id));
				}
			}
		}

        // used for gridview search box inputs
        if (isset($_GET['SourceCampaigns']))
            $model->attributes=$_GET['SourceCampaigns'];

		$this->controller->render($view, array(
			'model'=>$model
		));
	}
}
