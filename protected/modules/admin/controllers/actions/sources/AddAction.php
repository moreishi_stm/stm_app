<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Add Source';
		$model = $this->controller->baseModel;

		if (isset($_POST['Sources'])) {
			$model->attributes = $_POST['Sources'];
			$model->account_id = Yii::app()->user->accountId;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Added Source.');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form',array(
			'model'=>$model,
		));
	}
}