<?php

class UndeleteAction extends CAction
{
	/**
	 * Manages models
	 */
    public function run($id) {

        $Source = Sources::model()->skipSoftDeleteCheck()->findByPk($id);

        $this->performAjaxRequest($Source);
    }
    protected function performAjaxRequest(Sources $model) {

        if (Yii::app()->request->isAjaxRequest) {
            $model->is_deleted = 0;
            $result = $model->save();
            echo CJSON::encode(array('undeleted' => $result));

            Yii::app()->end();
        }
    }
}
