<?php

class EditAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'Edit Source';
		$model=$this->controller->loadModel($id);

		if (isset($_POST['Sources'])) {
			$model->attributes = $_POST['Sources'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Updated Source.');
				$this->controller->redirect(array('/'.Yii::app()->controller->module->name.'/sources'));
			}
            else {
                Yii::app()->user->setFlash('error', 'Source did not save. '.current($model->getErrors())[0]);
            }
		}

		$this->registerScript($model);
		$this->controller->render('form',array(
			'model'=>$model,
			'subSourceChecked'=>($model->parent_id)? 'checked':null,
			'subSourceDisabled'=>($model->parent_id)? null:'disabled',
		));
	}

	public function registerScript($model) {
		$isSubSource = ($model->parent_id)? 1 : 0;
		$js = <<<JS
		$(document).ready(function () {
			if($isSubSource)
				$('#isSubsource').attr('checked','checked');

			$('#isSubsource').click(function() {
				if ($(this).is(':checked')) {
					$('#Sources_parent_id').removeAttr('disabled');
					$('.chzn-select').trigger("liszt:updated");
				} else {
					$('#Sources_parent_id').val(0);
					$('#Sources_parent_id').attr('disabled','disabled');
					$('.chzn-select').trigger("liszt:updated");
					$('#Sources_parent_id').removeAttr('disabled');
				}
			});
		});
JS;

		Yii::app()->clientScript->registerScript('sourceForm', $js);

	}
}