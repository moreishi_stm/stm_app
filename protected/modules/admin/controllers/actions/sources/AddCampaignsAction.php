<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddCampaignsAction extends CAction
{
	public function run()
	{
		$model = new SourceCampaigns;

		if (isset($_POST['SourceCampaigns'])) {
			$model->attributes = $_POST['SourceCampaigns'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully Added Campaign.');
				$this->controller->redirect(array('viewCampaigns','id'=>$model->id));
			}
		}

		$this->controller->render('formCampaigns',array(
			'model'=>$model,
		));
	}
}