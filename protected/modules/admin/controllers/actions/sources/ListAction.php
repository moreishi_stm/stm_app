<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Sources List';
		$model=$this->controller->baseModel;
		$model->unsetAttributes();  // clear any default values

        // used for gridview search box inputs
        if (isset($_GET['Sources']))
            $model->attributes=$_GET['Sources'];

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
