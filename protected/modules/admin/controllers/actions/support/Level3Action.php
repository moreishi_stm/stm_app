<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class Level3Action extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Level 3 Certification';
            $tutorials = array(
                array(
                    'videoCode' => 'eff5f14350fe445a83eabaacaee905f9',
                    'title' => 'Level 3 Intro',
                ),
                array(
                    'videoCode' => '3438090c69644a35b488e5d30ccb2ad6',
                    'title' => 'Understanding the Layout for Buyers, Sellers, Closings',
                ),
                array(
                    'videoCode' => '2cbf7895cd6b4ff3a410e6d0e3893e06',
                    'title' => 'A, B, C Buyers List',
                ),
                array(
                    'videoCode' => 'aa2b93e596864f52b15b315de3069945',
                    'title' => 'Understanding of Sources for Contacts vs. Transaction',
                ),
                array(
                    'videoCode' => '02612e323e29406ba89659e79b4dfdcc',
                    'title' => 'How Activity Log Ties into the Reports',
                ),
                array(
                    'videoCode' => '5d538de21f1142c5b9fe9dc3ce03edae',
                    'title' => 'How Timeblocking Ties into the Reports',
                ),
                array(
                    'videoCode' => '144368904db54945927b95e4b173e178',
                    'title' => 'How Appointments Tie into the Reports',
                ),
                array(
                    'videoCode' => 'ac47d1667e2e4039ae84d16f089a61eb',
                    'title' => 'Timeblocking Report',
                ),
                array(
                    'videoCode' => '96bd2fd92bf74d40909c63819a6863cd',
                    'title' => 'Setting Appointments',
                ),
                array(
                    'videoCode' => 'a9a4b9d658194345a6279a7672607824',
                    'title' => 'Appointments Report',
                ),
                array(
                    'videoCode' => 'b034710dfd014fa6b19acd1b02494f5c',
                    'title' => 'Leads Report',
                ),
            );

			$this->controller->render('list', array(
					'tutorials' => $tutorials,
				)
			);
		}
	}
