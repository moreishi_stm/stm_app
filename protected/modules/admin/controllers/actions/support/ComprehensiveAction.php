<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class ComprehensiveAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Comprehensive Systems Training';

			$this->controller->render('comprehensive', array(
				)
			);
		}
	}
