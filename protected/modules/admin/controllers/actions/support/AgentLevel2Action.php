<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class AgentLevel2Action extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Agents Training Level 2 Certification';

            $tutorials = array( //sending and email
                array(
                    'videoCode' => '8eb6d96e45864297818efbb1fb4cf2fe',
                    'title' => 'Agent Level 2 Intro',
                ),
                array(
                    'videoCode' => 'a48fd6a29f004aaeb96cb82cd52e16c5',
                    'title' => 'Task List Mastery',
                ),
                array(
                    'videoCode' => 'd7b648dcd8454edf8953c79e8e717fbb',
                    'title' => 'Timeblocking Basics',
                ),
                array(
                    'videoCode' => '558fdd14ad4942bf83fb3d2cf41cb538',
                    'title' => 'Goals In-Depth',
                ),
                array(
                    'videoCode' => '6edd184f82ed4658886abb3d2e3d9d02',
                    'title' => 'Reverse Prospecting',
                ),
                array(
                    'videoCode' => '4b30ffcdfb6a4d1d87d20122bcea3005',
                    'title' => 'Listing Storyboard',
                ),
                array(
                    'videoCode' => 'cbddd0312258478295374ac2e8283621',
                    'title' => 'Lead Routing (Shift)',
                ),
                array(
                    'videoCode' => 'ed1960600c05487a8773fdf1286a8f6f',
                    'title' => 'Email Templates',
                ),
                array(
                    'videoCode' => '1b2405c6323745bf8fad07f46a290ffc',
                    'title' => 'Saved Search (Market Update / MLS Drips)',
                ),
                array(
                    'videoCode' => '6a1af579b227437db367db24f040a54e',
                    'title' => 'Badges',
                ),
            );

			$this->controller->render('list', array(
					'tutorials' => $tutorials,
				)
			);
		}
	}
