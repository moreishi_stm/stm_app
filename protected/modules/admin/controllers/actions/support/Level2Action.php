<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class Level2Action extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

            $this->controller->title = 'Comprehensive Systems Training Level 2';

            $tutorials = array(
                array(
                    'videoCode' => 'cef00afd8dfc401b857676a61d29be66',
                    'title' => 'Level 2 Introduction',
                ),
                array(
                    'videoCode' => '0387e9cf64624fd7b5c1d9d19e555604',
                    'title' => 'Account Settings',
                ),
                array(
                    'videoCode' => '9d9a266c1af04ad88de03ff058f6fa3f',
                    'title' => 'Project Management',
                ),
                array(
                    'videoCode' => 'a48fd6a29f004aaeb96cb82cd52e16c5',
                    'title' => 'Task List Mastery',
                ),
                array(
                    'videoCode' => '6edd184f82ed4658886abb3d2e3d9d02',
                    'title' => 'Reverse Prospecting',
                ),
                array(
                    'videoCode' => 'b9f5ee630f864fd186531c7bec6d1751',
                    'title' => 'Goals',
                ),
                array(
                    'videoCode' => '28d441bf30dd49d381da4dc7b962ea6f',
                    'title' => 'Recruits',
                ),
                array(
                    'videoCode' => '61efd482eb694440a4fb7e19869e7c95',
                    'title' => 'Edit Web Page - Content Management System',
                ),
                array(
                    'videoCode' => '6a1af579b227437db367db24f040a54e',
                    'title' => 'Badges',
                ),
                array(
                    'videoCode' => '175d4566487c404f8d6914a6bd23ad2a',
                    'title' => 'Activity Insights Report',
                ),
                array(
                    'videoCode' => 'e39b7d07c98344cfbba1a63c592e77d9',
                    'title' => 'Conversion Report',
                ),
            );

			$this->controller->render('list', array(
					'tutorials' => $tutorials,
				)
			);
		}
	}
