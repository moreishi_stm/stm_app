<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class LenderAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

            $this->controller->title = 'Lender Quick Start Training';

            $tutorials = array(
                array(
                    'videoCode' => '2fb2698b54874d4ab8e2509a0e9561c7',
                    'title' => 'Lender Tutorials Intro',
                ),
                array(
                    'videoCode' => 'e1baf82238204a5fad5abed7d11cfeff',
                    'title' => 'User Settings',
                ),
                array(
                    'videoCode' => '3438090c69644a35b488e5d30ccb2ad6',
                    'title' => 'Understanding the Layout for Buyers, Sellers, Closings',
                ),
                array(
                    'videoCode' => 'ef8ce42f3e0a42ab80e18867d4b23fdf',
                    'title' => 'Adding & Completing Tasks',
                ),
                array(
                    'videoCode' => 'e870d8a5c2b34ed5b638f1bae8aa5e37',
                    'title' => 'Adding an Activity Log',
                ),
                array(
                    'videoCode' => 'a48fd6a29f004aaeb96cb82cd52e16c5',
                    'title' => 'Task List Mastery',
                ),
                array(
                    'videoCode' => 'fb149c616ec642ba9660e033b0002e59',
                    'title' => 'How Lenders Can Use the Appointment Report',
                ),
                array(
                    'videoCode' => '6a1af579b227437db367db24f040a54e',
                    'title' => 'Badges',
                ),
                array(
                    'videoCode' => 'c338b0cf74ed4d9bbfb8727d1181251c',
                    'title' => 'Prevent Safari Autocomplete unwanted fields',
                ),
            );

            $this->controller->render('list', array(
                    'tutorials' => $tutorials,
                )
            );

		}
	}
