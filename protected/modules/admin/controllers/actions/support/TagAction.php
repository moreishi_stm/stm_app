<?php
/**
 * Tag Action
 *
 * Used to view videos in a tag
 */
class TagAction extends CAction
{
    /**
     * Run
     *
     * This method is called when the page is loaded
     * @param $id The tag ID to videos for
     */
    public function run($id)
    {
        // Retrieve tag
        $tag = SupportVideoTags::model()->findByPk($id);
        if(!$tag) {
            throw new Exception('No such tag exists');
        }

        // Set page title
        $this->controller->title = $tag->name;

        // Pass data to view
        $this->controller->render('tag', array(
                'tag'   =>  $tag
            )
        );
    }
}