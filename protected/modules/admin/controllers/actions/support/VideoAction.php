<?php
/**
 * Video Action
 *
 * Used to view a video
 */
class VideoAction extends CAction
{
    /**
     * Run
     *
     * This method is called when the page is loaded
     * @param $id The video ID to load a video by
     */
    public function run($id)
    {
        // Retrieve video
        $video = SupportVideos::model()->findByPk($id);
        if(!$video) {
            throw new Exception('No such video exists');
        }

        // Set page title
        $this->controller->title = $video->title;

        // Pass data to view
        $this->controller->render('video', array(
                'video' => $video
            )
        );
    }
}