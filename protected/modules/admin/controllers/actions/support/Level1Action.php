<?php

	class Level1Action extends CAction {

		public function run() {

			$this->controller->title = 'Comprehensive Systems Training Level 1';

            $tutorials = array(
                array(
                    'videoCode' => '7cc44ac713574404a3c60f58ecaa4bf9',
                    'title' => 'Instructions & Intro',
                ),
                array(
                    'videoCode' => 'e1baf82238204a5fad5abed7d11cfeff',
                    'title' => 'User Settings Setup',
                ),
                array(
                    'videoCode' => '2d327ecb888d44d6a1f9f5f9e03ab1b7',
                    'title' => 'Understand Structure of Contact, Transaction and Closing',
                ),
                array(
                    'videoCode' => '2c5989ccc32e419e99ba8b5d17f65289',
                    'title' => 'Add a New Contact',
                ),
                array(
                    'videoCode' => '42a5dec3ebbb4067bc2825cc41e69320',
                    'title' => 'Adding a New Seller or Buyer',
                ),
                array(
                    'videoCode' => '3093feae6567406185103a0b1de20a95',
                    'title' => 'Add Closing',
                ),
                array(
                    'videoCode' => 'e870d8a5c2b34ed5b638f1bae8aa5e37',
                    'title' => 'Adding an Activity Log',
                ),
                array(
                    'videoCode' => '27dbf33168964d3fb3b3125df315a2b2',
                    'title' => 'Source of Business',
                ),
                array(
                    'videoCode' => 'ed1960600c05487a8773fdf1286a8f6f',
                    'title' => 'Email Templates',
                ),
                array(
                    'videoCode' => 'd1aad6185d694b4d950f7e6611da9b7c',
                    'title' => 'Creating an Action Plan',
                ),
                array(
                    'videoCode' => '149912cb6685408c8489f084aa4325ef',
                    'title' => 'Applying an Action Plan',
                ),
                array(
                    'videoCode' => '4b30ffcdfb6a4d1d87d20122bcea3005',
                    'title' => 'Listing Storyboard',
                ),
                array(
                    'videoCode' => 'cbddd0312258478295374ac2e8283621', //a72c42cbc6004ae4bf0befaa220d5cae (new shift & round robin)
                    'title' => 'Lead Routing (Shift)',
                ),
                array(
                    'videoCode' => '1b2405c6323745bf8fad07f46a290ffc',
                    'title' => 'Saved Search (Market Update / MLS Drips)',
                ),
                array(
                    'videoCode' => 'd7b648dcd8454edf8953c79e8e717fbb',
                    'title' => 'Timeblocking Basics',
                ),
                array(
                    'videoCode' => 'c338b0cf74ed4d9bbfb8727d1181251c',
                    'title' => 'Prevent Safari Autocomplete unwanted fields',
                ),
            );

			$this->controller->render('list', array(
					'tutorials' => $tutorials,
				)
			);
		}
	}
