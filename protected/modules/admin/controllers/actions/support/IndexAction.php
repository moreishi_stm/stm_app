<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class IndexAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Training & Support Center';

            $model = new SupportVideos();

            if (isset($_GET['SupportVideos']) && $_GET['SupportVideos']['title']) {
                $model->attributes = $_GET['SupportVideos'];
            }
            else {
                //$model->id=0;
            }

			$this->controller->render('index', array('model'=>$model));
		}
	}