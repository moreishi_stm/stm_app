<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class TeamleaderAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Team Leader Quick Start Training';

			$this->controller->render('teamleader', array(
				)
			);
		}
	}
