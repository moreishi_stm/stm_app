<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class AgentLevel1Action extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Agents Training Level 1 Certification';

            $tutorials = array(
                array(
                    'videoCode' => '555fb7c9ba1b42d2b9828e18c4ef4ef5',
                    'title' => 'Agent Tutorials Intro',
                ),
                array(
                    'videoCode' => 'e1baf82238204a5fad5abed7d11cfeff',
                    'title' => 'User Settings',
                ),
                array(
                    'videoCode' => 'b9f5ee630f864fd186531c7bec6d1751',
                    'title' => 'Goals',
                ),
                array(
                    'videoCode' => '2d327ecb888d44d6a1f9f5f9e03ab1b7',
                    'title' => 'Understand Structure of Contact, Transaction and Closing',
                ),
                array(
                    'videoCode' => '3438090c69644a35b488e5d30ccb2ad6',
                    'title' => 'Understanding the Layout for Buyers, Sellers, Closings',
                ),
                array(
                    'videoCode' => '2c5989ccc32e419e99ba8b5d17f65289',
                    'title' => 'Add a New Contact',
                ),
                array(
                    'videoCode' => '42a5dec3ebbb4067bc2825cc41e69320',
                    'title' => 'Adding a New Seller or Buyer',
                ),
                array(
                    'videoCode' => '62e81466d7654c9e8431574c0df31fbf',
                    'title' => 'Lead Processing Tips',
                ),
                array(
                    'videoCode' => '149912cb6685408c8489f084aa4325ef',
                    'title' => 'Applying an Action Plan',
                ),
                array(
                    'videoCode' => 'ef8ce42f3e0a42ab80e18867d4b23fdf',
                    'title' => 'Adding & Completing Tasks',
                ),
                array(
                    'videoCode' => 'e870d8a5c2b34ed5b638f1bae8aa5e37',
                    'title' => 'Adding an Activity Log',
                ),
                array(
                    'videoCode' => 'c338b0cf74ed4d9bbfb8727d1181251c',
                    'title' => 'Prevent Safari Autocomplete unwanted fields',
                ),
            );

			$this->controller->render('list', array(
					'tutorials' => $tutorials,
				)
			);
		}
	}
