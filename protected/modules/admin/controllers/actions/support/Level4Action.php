<?php
//	Yii::import('admin_widgets.DateRanger.DateRanger');

	class Level4Action extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Level 4 Certification';
            $tutorials = array(
                array(
                    'videoCode' => '148b5eb667414ec19713ee62bceadaa5',
                    'title' => 'Level 4 Intro',
                ),
                array(
                    'videoCode' => 'd0b042beea824442a19705bce804c26c',
                    'title' => 'Seller Interview Sheet (Greensheet) In-Depth + Scripts',
                ),
                array(
                    'videoCode' => 'c7137bfbedc04e6cbf31588972bd8f54',
                    'title' => 'Buyer Interview Sheet (Yellowsheet) In-Depth + Scripts',
                ),
                array(
                    'videoCode' => '392f34ae063c4362ba02e8e2fc713f63',
                    'title' => 'Activities Report',
                ),
                array(
                    'videoCode' => 'f052706d30ac41f79dc0217b6b84af58',
                    'title' => 'Saved Search Report',
                ),
                array(
                    'videoCode' => '558fdd14ad4942bf83fb3d2cf41cb538',
                    'title' => 'Goals In-Depth',
                ),
                array(
                    'videoCode' => 'e95309ed47834776a471c75d5c9d97d5',
                    'title' => 'Content Management: Creating a New Web Page',
                ),
                array(
                    'videoCode' => '267d8cd1adeb45088396c05cab06958f',
                    'title' => 'Content Management: Adding Links in a Page',
                ),
//                array(
//                    'videoCode' => '',
//                    'title' => 'Content Management: Created a New Video using a Template',
//                ),
                array(
                    'videoCode' => '52da41ff538241c3aed8a2fcdedfd64a',
                    'title' => 'Best Practice Tip: Power Hour' , //#1
                ),
                array(
                    'videoCode' => 'e0e11ff922b0476884739c2a5a0f04b6',
                    'title' => 'Best Practice Tip:  Daily Tracking Habit', //#2
                ),
                array(
                    'videoCode' => 'd40ec305f6ba491791f611d37a34a69c',
                    'title' => 'Best Practice Tip: Power Dialing w/Tasks', //#3
                ),
            );


            //			$DateRanger = new DateRanger();
//			$dateRange = $DateRanger->getDateRange($_GET['DateRanger']);
//
//			$model = new BadgePointsEarned;
//			$model->byDateRange($dateRange,'added');
//			$model->unsetAttributes(); // clear any default values

			$this->controller->render('list', array(
					'tutorials' => $tutorials,
//					'model' => $model,
//					'dateRange'=>$dateRange,
				)
			);
		}
	}
