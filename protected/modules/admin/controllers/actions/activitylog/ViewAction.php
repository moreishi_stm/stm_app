<?php

/**
 * Views a ActivityLog.
 *
 */
class ViewAction extends CAction
{
    public function run($id) {

        $model = $this->controller->loadModel($id);

        $this->performAjaxRequest($model);
    }

    protected function performAjaxRequest($model) {

        if (Yii::app()->request->isAjaxRequest) {

            $addressString = '';
            if($model->component_type_id == ComponentTypes::SELLERS) {
                $seller = Sellers::model()->findByPk($model->component_id);
                if($address = $seller->address) {
                    $addressString = $address->address.' '.$address->city.' '.$address->state->short_name.' '.$address->zip;
                    $addressString = str_replace(' ','+', $addressString);
                }
            }

            echo CJSON::encode(array(
                "id"                  => $model->id,
                "component_id"        =>$model->component_id,
                "component_type_id"   =>$model->component_type_id,
                "activity_date"       =>date("m/d/Y h:i A",strtotime($model->activity_date)),
                "task_type_id"        =>$model->task_type_id,
                "lead_gen_type_ma"    =>($model->lead_gen_type_ma) ? ActivityLog::LEAD_GEN_YES_ID : ActivityLog::LEAD_GEN_NONE_ID,
                "is_spoke_to"         =>$model->is_spoke_to,
                "asked_for_referral"  =>$model->asked_for_referral,
                "is_public"           =>$model->is_public,
                "note"                =>$model->note,
                "private_note"        =>$model->private_note,
                "address"             => $addressString,
                "url"                 => $model->url,
                'task_id'             =>  $model->task_id,
            ));

            Yii::app()->end();
        }
    }
}