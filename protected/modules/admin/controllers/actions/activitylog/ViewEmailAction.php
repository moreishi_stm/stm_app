<?php

class ViewEmailAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
        if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(500, 'This view must be called via an ajax request.');
        }

		$model = $this->controller->loadModel($id);
        $contentJson = CJSON::encode(array('result' => $model->note,
                                           'fromName'=>$model->emailMessageLu[0]->emailMessage->fromEmail->contact->fullName,
                                           'fromEmail'=>$model->emailMessageLu[0]->emailMessage->fromEmail->email,
                                           'toName'=>$model->emailMessageLu[0]->emailMessage->toEmail->contact->fullName,
                                           'toEmail'=>$model->emailMessageLu[0]->emailMessage->toEmail->email,
                                           'cc'=>$model->emailMessageLu[0]->emailMessage->to_email_cc,
            ));
        echo $contentJson;
	}
}