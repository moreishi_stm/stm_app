<?php

	class EmailAction extends CAction {

		public function run() {

			$emailMessage = new EmailMessage('outbound');

			$this->processAjaxRequest($emailMessage);
		}

		protected function processAjaxRequest(EmailMessage $emailMessage) {
            set_time_limit(1000);
            ini_set('post_max_size', '20M');
            ini_set('upload_max_filesize', '20M');

//            if (Yii::app()->request->isAjaxRequest || strtolower($_POST['X-Requested-With']) == 'iframe') {
                $mailSent = false;

				if (isset($_POST['EmailMessage'])) {

					$emailMessage->attributes = $_POST['EmailMessage'];
					$emailMessage->from_email_id = Yii::app()->user->contact->getPrimaryEmailObj()->id;

                    if($emailMessage->content) {
                        $charset = mb_detect_encoding($emailMessage->content, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                        //                    $emailMessage->content = mb_convert_encoding($emailMessage->content,'UTF-8',$charset);
                        // ignore any weird encoding
                        if($charset !== 'UTF-8') {
                            $emailMessage->content = iconv($charset, 'UTF-8//IGNORE', $emailMessage->content);
                        }

                        if(!$emailMessage->content) {
                            throw new Exception(__CLASS__.' (:'.__LINE__.') Email body content is blank. Check iconv() output.');
                        }
                    }

                    // applies to the initial beforeValidate in the email dialog form, this is called again in the afterValidate as iframe so it's not ajax
                    if(Yii::app()->request->isAjaxRequest) {

                        if(!$emailMessage->validate()) {
                            echo CActiveForm::validate($emailMessage);
                        }
                        Yii::app()->end();
                    }

                    $transaction = Yii::app()->db->beginTransaction();

                    if (!$emailMessage->save()) {
						echo CActiveForm::validate($emailMessage);
						Yii::app()->end();
					}

                    // We're gonna need this later
                    $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

                    // Copy all attachments to S3
                    //@todo: Finish and test this
                    if(count($_FILES['EmailMessage']['name']['attachments'])) {

                        for($i = 0; $i < count($_FILES['EmailMessage']['name']['attachments']); $i++) {

                            if(!$_FILES['EmailMessage']['tmp_name']['attachments'][$i]) {
                                continue;
                            }

                            // Determine Content Type (MIME type)
                            $finfo = finfo_open(FILEINFO_MIME_TYPE);
                            $contentType = finfo_file($finfo, $_FILES['EmailMessage']['tmp_name']['attachments'][$i]);
                            finfo_close($finfo);

                            // Store attachment in database
                            $emailMessageAttachment = new EmailMessageAttachments();
                            $emailMessageAttachment->email_message_id = $emailMessage->id;
                            $emailMessageAttachment->name = $_FILES['EmailMessage']['name']['attachments'][$i];
                            $emailMessageAttachment->type = $contentType;
                            $emailMessageAttachment->size = $_FILES['EmailMessage']['size']['attachments'][$i];
                            $emailMessageAttachment->added = new CDbExpression('NOW()');
                            if (!$emailMessageAttachment->save()) {
                                echo CActiveForm::validate($emailMessageAttachment);
                                Yii::log(__CLASS__.' (:'.__LINE__.') URGENT: Email attachment error. Attributes: '.print_r($emailMessageAttachment->attributes, true).PHP_EOL.'Error Message: '.print_r($emailMessageAttachment->getErrors(), true), CLogger::LEVEL_ERROR);

                                //@todo: ************** check file size - if too big send via Zimbra **************

                                $transaction->rollback();
                                Yii::app()->end();
                            }


                            // Store the file in Amazon S3
                            $response = $s3client->putObject(array(
                                'Bucket'                =>  ((YII_DEBUG)?'dev.':'') .'client-private.seizethemarket.com',
                                'Key'                   =>  Yii::app()->user->clientId. '/' . Yii::app()->user->accountId . '/email-attachments/' . $emailMessageAttachment->id,
                                'SourceFile'            =>  $_FILES['EmailMessage']['tmp_name']['attachments'][$i],
                                'ContentDisposition'    =>  'attachment; filename="' . $_FILES['EmailMessage']['name']['attachments'][$i],
                                'ContentType'           =>  $contentType,
                                'StorageClass'          =>  'REDUCED_REDUNDANCY'
                            )); //$response['data']['ObjectURL']
                        }
                    }


                    // Do SES for CLee instead of the typical method
                    $domainName = Yii::app()->user->domain->name;
                    if(in_array($domainName, array('christineleeteam.com', 'christineleerealestate.com'))) { $domainName='jax-homefinder.com'; }

                    // Create email message
                    $zendMessage = new StmZendMail();
                    $zendMessage->addTo($emailMessage->toEmail->email, Yii::app()->format->formatProperCase($emailMessage->toEmail->contact->getFullName()));
                    $replyTo = $zendMessage->getReplyToEmail($emailMessage, $domainName);

                    //@todo: temp exception for Vlad - see how to code for this
                    if(strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') !== false && Yii::app()->user->id == 1) {
                        $zendMessage->setFrom(Yii::app()->user->contact->getPrimaryEmail(), Yii::app()->user->contact->fullName);
                    }
                    else {
                        $zendMessage->setFrom($replyTo, Yii::app()->format->formatProperCase($emailMessage->fromEmail->contact->getFullName()));
                    }

                    $zendMessage->setReplyTo($replyTo);
                    $zendMessage->setSubject($emailMessage->subject);
                    $zendMessage->setBodyHtmlTracking($emailMessage, $domainName);

                    if($emailMessage->to_email_cc) {

                        $emailMessage->to_email_cc = str_replace(' ', ',', $emailMessage->to_email_cc);

                        foreach (explode(',',$emailMessage->to_email_cc) as $ccRecipient) {

                            //@todo: need to sanitize more for improper characters, email address validity check

                            if(!empty($ccRecipient)) {
                                $zendMessage->addCc(trim($ccRecipient));
                            }
                        }
                    }

                    if($emailMessage->to_email_bcc) {

                        $emailMessage->to_email_bcc = str_replace(' ', ',', $emailMessage->to_email_bcc);

                        foreach (explode(',',$emailMessage->to_email_bcc) as $bccRecipient) {

                            //@todo: need to sanitize more for improper characters, email address validity check

                            if(!empty($bccRecipient)) {
                                $zendMessage->addBcc(trim($bccRecipient));
                            }
                        }
                    }
//                    $zendMessage->addBcc('adhoccopy@seizethemarket.net');
                    $zendMessage->addBcc('stmadhoccopy@gmail.com');

                    // Add all attachments
                    $attachmentsTotalSize = 0;
                    for($i = 0; $i < count($_FILES['EmailMessage']['name']['attachments']); $i++) {
                        if(!$_FILES['EmailMessage']['tmp_name']['attachments'][$i]) {
                            continue;
                        }

                        $attachment = $zendMessage->createAttachment(file_get_contents($_FILES['EmailMessage']['tmp_name']['attachments'][$i]));
                        $attachment->filename = $_FILES['EmailMessage']['name']['attachments'][$i];
                        $attachment->encoding = Zend_Mime::ENCODING_BASE64;
                        $attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                        $attachmentsTotalSize += $_FILES['EmailMessage']['size']['attachments'][$i];
                    }

                    //reject if filesize too big.
                    if($attachmentsTotalSize >= StmZendMail::SES_ATTACHMENT_MAX_BYTES) {
                        $transaction->rollback();
                        echo CJSON::encode(array('status' => 'error', 'emailErrorMessage'=> 'Max attachment size is 10Mb.'));

                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Email Processing attachment has size greater than 10 Mb. Sent user error message that email did not send. Remove logging after done monitoring or log in db.', CLogger::LEVEL_ERROR);

                        Yii::app()->end();
                    }


                    // Check to see if we selected a template with attachments
                    if(!empty($_POST['template_id'])) {

                        // Retrieve the template
                        $emailTemplate = EmailTemplates::model()->findByAttributes(array(
                            'id'    =>  $_POST['template_id']
                        ));

                        // Retrieve attachments
                        foreach($emailTemplate->attachments as $attachment) {

                            // Retrieve the previously stored object from S3 storage
                            $response = $s3client->getObject(array(
                                'Bucket'    =>  ((YII_DEBUG)?'dev.':'') .'client-private.seizethemarket.com',
                                'Key'       =>  Yii::app()->user->clientId. '/' . Yii::app()->user->accountId . '/email-template-attachments/' . $attachment->id,
                            ));

                            // Add the attachment to the email message
                            $zendMessage->createAttachment($response['Body'], $attachment->type, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, $attachment->name);
                        }
                    }

                    // Attempt to send message
                    try {
                        $zendMessage->checkBounceUndeliverable = false;

                        //@todo: total attachment size is 10Mb
//                        if($attachmentsTotalSize >= self::SES_ATTACHMENT_MAX_BYTES) {
//                            Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Email Processing attachment has size greater than 10 Mb. Sending via Zimbra due to SES limit. Remove logging after done monitoring or log in db.', CLogger::LEVEL_ERROR);
//                        }
//                        $transport = ($attachmentsTotalSize < self::SES_ATTACHMENT_MAX_BYTES) ? StmZendMail::getAwsSesTransport() : StmZendMail::getZimbraTransport();

                        $mailTransport = StmZendMail::getAwsSesTransport();

                        //@todo: temp for cleeteam figuring out deliverability options
//                        if(strpos($_SERVER['SERVER_NAME'], 'christineleeteam') !== false) {
//                            $mailTransport = StmZendMail::getCLeeGmailTransport();
//                        }

                        $zendMessage->send($mailTransport, $emailMessage);
                        $mailSent = true;
                    }
                    catch(Exception $e) {

                        $transaction->rollback();
                        Yii::log(__CLASS__ . ' (:'. __LINE__ .') Ad hoc Email Sending Error via STM! (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL . 'Exception Message:' . $e->getMessage().PHP_EOL.print_r($emailMessage->attributes, true).PHP_EOL.print_r($zendMessage, true), CLogger::LEVEL_ERROR);
                        echo CJSON::encode(array('status' => 'error'));
                        Yii::app()->end();
                    }

                    // Email sent successfully
                    //
                    try
                    {
                        $transaction->commit();
                        echo CJSON::encode(array('status' => 'success'));
                    }
                    catch(Exception $e)
                    {
                        Yii::log(__CLASS__ . ' (:'. __LINE__ .') Failed to send email. Error Message: '.$e->getMessage(), CLogger::LEVEL_ERROR);
                        $transaction->rollback();
                        echo CJSON::encode(array('status' => 'error'));
                    }
				}

                Yii::app()->end();
//			}
		}
	}