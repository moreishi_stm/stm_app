<?php

/**
 * Creates a new task.
 *
 */
class EditAction extends CAction
{
	public function run($id)
	{
        $model = $this->controller->loadModel($id);

        $this->processAjaxRequest($model);
	}

    protected function processAjaxRequest(ActivityLog $model) {

        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['ActivityLog'])) {

                $model->attributes = $_POST['ActivityLog'];
                if(!$model->save()) {
                    // error message goes here
                    $error = current($model->getErrors());
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
            }

            $status = 'success';
            echo CJSON::encode(array('status'=>$status));
            Yii::app()->end();
        }
    }
}