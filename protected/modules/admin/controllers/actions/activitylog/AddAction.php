<?php

/**
 * Creates a Activity Log
 */
class AddAction extends CAction {

	public function run() {
		$model = new ActivityLog;
		$this->processAjaxRequest($model);
	}

	protected function processAjaxRequest(ActivityLog $model) {
		if (Yii::app()->request->isAjaxRequest) {

			if (isset($_POST['ActivityLog'])) {

				$model->attributes = $_POST["ActivityLog"];
				$model->account_id = Yii::app()->user->accountId;

                if($_POST["ActivityLog"]['scenario']) {
                    $model->scenario = $_POST["ActivityLog"]['scenario'];
                }

                if (($errors = CActiveForm::validate($model)) !== '[]') {
                    // need to do this again for some reason, it gets cleared after validate
                    echo $errors;
                    Yii::app()->end();
                }

                if(!$model->component_id || !$model->component_type_id) {
                    //@todo: Temp logging. Delete when finished. - CLee 2/11/2015
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Activity Log is missing ComponentId or ComponentTypeId. Investigate immediately. Activity Log Attributes: ' . print_r($model->attributes, true), CLogger::LEVEL_ERROR);
                }

				if (isset($_POST['task_id']) && !empty($_POST['task_id'])) {

					if($Task = Tasks::model()->findByPk($_POST['task_id'])) {
                        $Task->complete_date = new CDbExpression('NOW()');
                        if($Task->save()) {
                            $model->task_id = $Task->id;

                            $model->component_type_id = $Task->component_type_id;
                            $model->component_id = $Task->component_id;
                        }
                    }
                    // find the soft deleted task and do not save a new complete date.
                    else {
                        $Task2 = Tasks::model()->skipSoftDeleteCheck()->findByPk($_POST['task_id']);

                        // Unexpected result if we still can't find the task. Throw error and log
                        if(!$Task2) {
                            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Missing tasks info even with ignoring soft delete. Investigate immediately. Activity Log Attributes: ' . print_r($model->attributes, true).PHP_EOL, CLogger::LEVEL_ERROR);

                            // last attempt to find task using raw query. Log with either result.
                            $rawData = Yii::app()->db->createCommand("select * from tasks where id=".$_POST['task_id'])->queryRow();
                            if(empty($rawData)) {
                                throw new Exception(__CLASS__ . ' (' . __LINE__ . ') Error: Could not find task even when ignoring soft delete. This is a last attempt using raw query and still empty. Investigate immediately.');
                            }
                            else {
                                // found task using raw query. Send log info
                                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Missing tasks info even with ignoring soft delete. However, found it using raw query. Investigate immediately. Activity Log Attributes: ' . print_r($model->attributes, true).PHP_EOL, CLogger::LEVEL_ERROR);
                            }
                        }
                    }
				}

                //@todo: Remove this conditional when fully transitioned to the new way of completing tasks with the activity log
                // Do this if we don't have the dialer posting (testing new stuff out)
                elseif($_POST['is_dialer']) {
                    if($_POST['ActivityLog']['is_spoke_to']) {

                        if($model->task_id) {
                            // Mark the dialer task as completed
                            $Task = Tasks::model()->skipSoftDeleteCheck()->findByPk($model->task_id);
                            $Task->complete_date = new CDbExpression('NOW()');
                            if(!$Task->save()) {
                                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Task did not save as complete. Error Message: ' . print_r($Task->getErrors(), true).' Attributes: '.print_r($Task->attributes, true), CLogger::LEVEL_ERROR);
                            }
                            else {
                                // Delete all other calls from the call list with this task ID
                                CallListPhones::model()->updateAll(array(
                                        'is_deleted'    =>  '1'
                                    ), 'id != :id AND task_id = :task_id', array(
                                        ':id'       =>  $_POST['call_id'],
                                        ':task_id'  =>  $Task->id
                                    ));
                            }
                        }
                    }
                }

                if(!$model->component_id || !$model->component_type_id) {
                    //@todo: Temp logging. Delete when finished. - CLee 2/11/2015
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Activity Log is missing ComponentId or ComponentTypeId. This means it was never found during the AddAction. This was probably missing from the beginning also. You should have gotten another error log right before this one. Investigate immediately. Activity Log Attributes: ' . print_r($model->attributes, true), CLogger::LEVEL_ERROR);
                }

                // Save and handle errors, if we have them
				if (!$model->save()) {
					echo CActiveForm::validate($model);
                    Yii::app()->end();
				}
                elseif($model->addFollowupTask) {
                    $followupTask = new Tasks();
                    $followupTask->attributes = $model->followupTask;
                    if(!$followupTask->save()) {
                        $status = 'error';
                        $message = current($followupTask->getErrors())[0];
                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Ajax request made to Activity log did not save the follow-up task. See Data. Remove this log if it proves to be annoying or irrelevant. Activity Log Data: '.print_r($model->attributes, true), CLogger::LEVEL_ERROR); // 2015-10-07 - CLee

                        Yii::app()->end();
                    }
                }

                $status = 'success';
			}
            else {
                // made ajax post without data. Most likely an unintended action and must notify as error
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Ajax request made to Activity log with no post information. Notifying to investigate. Remove this log if it proves to be annoying or irrelevant.', CLogger::LEVEL_ERROR); // 2015-10-07 - CLee
                $status = 'error';
            }

            echo CJSON::encode(array('status'=>$status, 'message'=>$message));
			Yii::app()->end();
		}
	}
}
