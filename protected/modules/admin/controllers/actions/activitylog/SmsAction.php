<?php

	class SmsAction extends CAction {

		public function run() {

			$smsMessage = new SmsMessages('outbound');

			$id = ($id) ? $id : Yii::app()->user->id;
			$Contact = Contacts::model()->findByPk($id);

            $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id' => Yii::app()->user->clientId, 'account_id' => Yii::app()->user->accountId));

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id = :clientAccountId';
			$Criteria->params = array(
                ':clientAccountId' => $clientAccount->id,
				':contactId'    => $Contact->id,
				':active'		=> 1
			);

			$checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);

			if(empty($checkForSmsNumberHQ)) {
				echo CJSON::encode(array('status' => 'error', 'message' => 'You must have a STM Provided SMS #.'));
				Yii::app()->end();
			}

			$this->processAjaxRequest($smsMessage, $Contact, $checkForSmsNumberHQ);
		}

		protected function processAjaxRequest(SmsMessages $smsMessage, Contacts $Contact, TelephonyPhones $smsProvidedNumber)
        {
			if(Yii::app()->request->isAjaxRequest) {
				if(!isset($_POST['SmsMessages']) || empty($_POST['SmsMessages'])) {
					echo CJSON::encode(array('status' => 'error', 'message' => 'No data sent.'));
					Yii::app()->end();
				}

				$_POST['SmsMessages']['content'] = trim(strip_tags($_POST['SmsMessages']['content']));

				$smsMessage->attributes = $_POST['SmsMessages'];
				$smsMessage->sent_by_contact_id = $Contact->id;
				$smsMessage->direction = SmsMessages::DIRECTION_OUTBOUND;
				$smsMessage->processed_datetime = new \CDbExpression('NOW()');
				$smsMessage->from_phone_number = '1'.$smsProvidedNumber->phone;

				if(!$smsMessage->validate()) {
                    echo CActiveForm::validate($smsMessage);
					Yii::app()->end();
				}

				$phone = Phones::model()->findByPk($smsMessage->to_phone_id);

                // stores the text value of phone as the value for phone can change by user.
                $smsMessage->to_phone_number = '1'.$phone->phone;

				if(empty($phone)) {
					Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error sending SMS.'.PHP_EOL.' could not load phone by pk: '.$smsMessage->to_phone_id, CLogger::LEVEL_ERROR);
					echo CJSON::encode(array('status' => 'error', 'message' => 'The phone number selected could not be found. Please try again.'));
					Yii::app()->end();
				}

                if(!YII_DEBUG) {
                    $response = Yii::app()->plivo->sendMessage($phone->phone, $smsMessage->content, "1".$smsProvidedNumber->phone);

                    if(isset($response['response']['error'])) {
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error sending SMS.'.PHP_EOL.'response: : '.print_r($response, true), CLogger::LEVEL_ERROR);
                        echo CJSON::encode(array('error' => 'There was an error sending the message. Please try again.'));
                        Yii::app()->end();
                    }
                    $smsMessage->vendor_response = json_encode($response);
                    $smsMessage->message_uuid = $response['response']['message_uuid'][0];
                }

				if (!$smsMessage->save()) {
					Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving SMS Message.'.PHP_EOL.'smsMessage->attributes: '.print_r($smsMessage->attributes, true).' POST: '.print_r($_POST,1), CLogger::LEVEL_ERROR);
					echo CJSON::encode(array('status' => 'error', 'message' => 'There was an error saving the sms message. Please try again.'));
					Yii::app()->end();
				}

				$activityLog = new ActivityLog();
				$activityLog->setAttributes(array(
					'account_id' => Yii::app()->user->accountId,
					'component_type_id' => $_POST['SmsMessages']['component_type_id'],
					'task_type_id' => TaskTypes::SMS_OUTBOUND,
					'component_id' => $_POST['SmsMessages']['component_id'],
					'activity_date' => new CDbExpression('NOW()'),
					'lead_gen_type_ma' => 0,
					'is_spoke_to' => 0,
					'call_id' => NULL,
					'note' => $smsMessage->content. '<br><br>Sent to: '.Yii::app()->format->formatPhone($phone->phone),
					'added_by' => $Contact->id,
				));

				if(!$activityLog->save()) {
					Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Outbound SMS Activiy Log.'.PHP_EOL.'activityLog->attributes: '.print_r($activityLog->attributes, true).' POST: '.print_r($_POST,1), CLogger::LEVEL_ERROR);
				}
                else {
                    $smsMessage->activity_log_id = $activityLog->id;
                    $smsMessage->save();
                }

				echo CJSON::encode(array('status' => 'success'));

				Yii::app()->end();
			}
		}
	}