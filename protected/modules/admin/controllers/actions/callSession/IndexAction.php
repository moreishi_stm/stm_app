<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class IndexAction extends CAction
{

    public function run()
    {

        $this->getController()->breadcrumbs = array(
            'Manage' => '#',
        );
        $this->getController()->pageTitle   = 'Start a New Call Session';
        $this->getController()->render('index');
    }
}