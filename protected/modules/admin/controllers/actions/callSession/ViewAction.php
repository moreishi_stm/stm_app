<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class ViewAction extends CAction
{

    public function run($id)
    {

        $callSession = CallSession::model()->findByPk($id);
        if (is_null($callSession)) {
            throw new CHttpException(500, 'Could not locate call session.');
        }

        $callSessionQueueProvider = new CActiveDataProvider('CallSessionQueue', array(
            'criteria' => array(
                'condition' => 'call_session_id = :call_session_id',
                'params'    => array(':call_session_id' => $callSession->id),
            )
        ));
        if (is_null($callSessionQueueProvider)) {
            throw new CHttpException(500, 'Could not locate call session queue.');
        }

        $this->getController()->setPageTitle('Viewing Call Session: ' . $callSession->name);
        $this->getController()->render('view', array('callSession' => $callSession, 'callSessionQueueProvider' => $callSessionQueueProvider));
    }
}