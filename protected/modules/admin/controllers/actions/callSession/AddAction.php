<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
Abstract class AddAction extends CAction
{

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     */
    public function run()
    {

        $contactSearchProvider = $this->getContactProvider();
        $contactSearchModel    = $this->getSearchModel();

        $phoneEntriesToAdd = $this->getPhoneEntriesToAdd();
        if (!empty($phoneEntriesToAdd)) {
            $newCallSession = $this->createCallSession($phoneEntriesToAdd);

            if (!$newCallSession->getIsNewRecord()) {
                Yii::app()->user->setFlash('success', 'Successfully created call session!');
                $this->getController()->redirect(array('/admin/callSession/name', 'id' => $newCallSession->id));
            } else {
                Yii::app()->user->setFlash(
                    'error', 'Sorry, we encountered an error trying to create your call session.'
                );
            }
        }

        $this->getController()->setPageTitle($this->getPageTitle());
        $this->getController()->render(
            $this->getView(), array(
                'contactSearchProvider' => $contactSearchProvider,
                'contactSearchModel'    => $contactSearchModel,
            )
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    abstract protected function getContactProvider();

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    abstract protected function getSearchModel();

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    abstract protected function getPhoneEntriesToAdd();

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $phoneIdsToAdd
     *
     * @return CallSession
     */
    private function createCallSession($phoneEntriesToAdd)
    {

        if (empty($phoneEntriesToAdd)) {
            return false;
        }
        $callSessionEntry = new CallSession;
        foreach ($phoneEntriesToAdd as $phoneEntry) {
            $callSessionEntry->addPhone($phoneEntry);
        }

        $callSessionEntry->save();

        return $callSessionEntry;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    abstract protected function getPageTitle();

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    abstract protected function getView();
}