<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class NameAction extends CAction
{

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function run($id)
    {

        $callSession = CallSession::model()->findByPk($id);
        if (is_null($callSession) or !empty($callSession->name)) {
            throw new CHttpException(500, 'Could not locate your call session.');
        }

        $callSession->setScenario('name');

        if ($_POST['CallSession']) {
            $callSession->name = $_POST['CallSession']['name'];
            if ($callSession->save()) {
                Yii::app()->user->setFlash('success', 'Saved your Call Session!');
                $this->getController()->redirect(array('/admin/callSession/view', 'id' => $callSession->id));
            }
        }

        $this->getController()->setPageTitle('Name your New Call Session');
        $this->getController()->render(
            'name', array('callSession' => $callSession)
        );
    }
}