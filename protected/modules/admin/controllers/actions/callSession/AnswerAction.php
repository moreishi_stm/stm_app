<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class AnswerAction extends CAction
{

    const DIAL_STATUS_COMPLETED = 'completed';

    const DIAL_STATUS_BUSY = 'busy';

    const DIAL_STATUS_FAILED = 'failed';

    const DIAL_STATUS_TIMEOUT = 'timeout';

    const DIAL_STATUS_NOANSWER = 'no-answer';

    private $_validDialStatuses
        = array(
            self::DIAL_STATUS_COMPLETED,
            self::DIAL_STATUS_BUSY,
            self::DIAL_STATUS_FAILED,
            self::DIAL_STATUS_TIMEOUT,
            self::DIAL_STATUS_NOANSWER
        );

    public function run()
    {

        $dialStatus = $this->getDialStatus();
        if (is_null($dialStatus)) {
            throw new CHttpException(500, 'Could not get dial status from post');
        }

        if ($dialStatus == self::DIAL_STATUS_COMPLETED) {
            $phoneNumber = $this->getConnectedCallerPhoneNumber();
        }
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return null
     */
    private function getDialStatus()
    {

        $validDialStatuses = array_combine($this->_validDialStatuses);

        $postDialStatus = $_POST['DialStatus'];
        if (in_array($postDialStatus, $validDialStatuses)) {
            return $validDialStatus;
        }

        return null;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    private function getConnectedCallerPhoneNumber() {

        $connectedCallerFromPost = $_POST['DialBLegUUID'];
        return $connectedCallerFromPost;
    }
}