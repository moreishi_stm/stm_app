<?php
Yii::import('admin_module.controllers.actions.callSession.AddAction');

/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class AddFromManualQueueAction extends AddAction
{

    /** @var string $_view */
    private $_view = 'manualqueue';

    /** @var string $_pageTitle */
    private $_pageTitle = 'Add From a Manual List';

    /** @var CActiveDataProvider $_searchProvider */
    private $_searchProvider;

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return CActiveDataProvider|mixed
     */
    protected function getContactProvider()
    {

        if (empty($this->_searchProvider)) {
            $this->_searchProvider = $this->getSearchModel()->search();
            $this->_searchProvider->getPagination()->setPageSize(100);

            $requirePhoneJoinCriteria            = new CDbCriteria;
            $requirePhoneJoinCriteria->with      = 'phones';
            $requirePhoneJoinCriteria->together  = true;
            $requirePhoneJoinCriteria->condition = 'phones.is_deleted <> 1';
            $this->_searchProvider->getCriteria()->mergeWith($requirePhoneJoinCriteria);
        }

        return $this->_searchProvider;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return Contacts|mixed
     */
    protected function getSearchModel()
    {

        $searchModel = new Contacts;
        $searchModel->unsetAttributes();

        if (isset($_GET['Contacts'])) {
            $searchModel->attributes = $_GET['Contacts'];
        }

        $searchModel->emails = new Emails('search');
        if (isset($_GET['Emails'])) {
            $searchModel->emails->attributes = $_GET['Emails'];
        }

        $searchModel->phones = new Phones('search');
        if (isset($_GET['Phones'])) {
            $searchModel->phones->attributes = $_GET['Phones'];
        }

        $searchModel->contactTypeLu = new ContactTypeLu('search');
        if (isset($_GET['ContactTypeLu'])) {
            $searchModel->contactTypeLu->attributes = $_GET['ContactTypeLu'];
        }

        $searchModel->source = new Sources('search');
        if (isset($_GET['Sources'])) {
            $searchModel->source->attributes = $_GET['Sources'];
        }

        return $searchModel;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return array|mixed
     */
    protected function getPhoneEntriesToAdd()
    {

        $phoneEntries = array();
        if (isset($_POST['contactIds'])) {
            foreach ($_POST['contactIds'] as $contactId) {
                /** @var Contacts $contact */
                $contact = Contacts::model()->findByPk($contactId);
                array_push($phoneEntries, $contact->getPrimaryPhoneObj());
            }
        }

        return $phoneEntries;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed|string
     */
    protected function getView()
    {

        return $this->_view;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed|string
     */
    protected function getPageTitle()
    {

        return $this->_pageTitle;
    }
}