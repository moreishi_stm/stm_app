<?php

class IndexAction extends CAction {

	public function run() {

        $this->controller->pageColor = 'orange';
        $this->controller->title = 'BombBomb';

        if(Yii::app()->user->settings->bombbomb_api_key) {
            $BombBomb = Yii::app()->bombbomb;
            if(Yii::app()->request->isAjaxRequest && isset($_POST['type']))
                $this->performAjaxRequest($BombBomb);

            switch($_GET['type']) {
                case 'lists':
                    $data = $BombBomb->getLists();
                    $DataProvider = new CArrayDataProvider($data['info']);
                    $view = 'Lists';
                    break;
                case 'drips':
                    $view = 'Drips';
                    $data = $BombBomb->getDrips();
                    $DataProvider = new CArrayDataProvider($data['info']);
                    break;
                case 'videos':
                    $view = 'Videos';
                    $data = $BombBomb->getVideos();
                    $DataProvider = new CArrayDataProvider($data['info']);
                    break;
                case 'listContacts':
                    $view = 'ListContacts';
                    $data = $BombBomb->getListContacts($_GET['id']);
                    $DataProvider = new CArrayDataProvider($data['info']);
                    break;
                case 'createList':
                    $view = 'Lists';
                    $result = $BombBomb->createList($_GET['name']);
                    $data = $BombBomb->getLists();
                    $DataProvider = new CArrayDataProvider($data['info']);
                    break;
                default:
                    $view = 'Emails';
                    $data = $BombBomb->getEmails();
                    $DataProvider = new CArrayDataProvider($data['info']);
                    break;
            }
        }

        $existingBbIds = Yii::app()->db->createCommand('SELECT bombbomb_id FROM email_templates WHERE bombbomb_id IS NOT NULL')->queryColumn();

		$this->controller->render('index',array(
                'view'=>$view,
                'DataProvider'=>$DataProvider,
                'existingBbIds' => $existingBbIds,
            ));
	}

	protected function performAjaxRequest(StmBombBomb $BombBomb) {
		if (Yii::app()->request->isAjaxRequest) {

			switch($_POST['type']) {
				case 'addEmailToList':
					$data = $BombBomb->addEmailToList($_POST['email'], $_POST['listId']);
					break;
				default:
					break;
			}

			echo $data['result'] = $data['status'];

			echo CJSON::encode($data);

			Yii::app()->end();
		}
	}

    public function printButton($data, $existingBbIds)
    {
        if(in_array($data['id'], $existingBbIds)) {
            $string = "<div><a href=\"/".Yii::app()->controller->module->name."/emailTemplates/add?bbid=".$data["id"]."&subject=".urlencode($data["subject"])."&desc=".urlencode($data["name"])."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View Template</a></div>";
        }
        else {
            $string = "<div><a href=\"/".Yii::app()->controller->module->name."/bombBomb/importEmailTemplate?bbid=".$data["id"]."&subject=".urlencode($data["subject"])."&desc=".urlencode($data["name"])."\" class=\"button gray icon i_stm_add grey-button\" target=\"_blank\">Import</a></div>";
        }
        return $string;
    }
}
