<?php
/**
 * Views a task.
 */
class ViewAction extends CAction {

	public $multiSelectFields = array('area','zip','neighborhood','county','city','school_district');

	public function run($id) {

		$model = $this->controller->loadModel($id);

		$this->performAjaxRequest($model);
	}

	protected function performAjaxRequest(SavedHomeSearches $model) {

		if (Yii::app()->request->isAjaxRequest) {

			$TermValues = TermComponentLu::model()->getSavedSearchValues($model->id);
			foreach($TermValues as $TermValue) {

				$termName = $TermValue->term->name;
				if(in_array($termName,$this->multiSelectFields)) {

					if(!isset($ajaxData[$termName]) || !is_array($ajaxData[$termName])) {
						$ajaxData[$termName] = array();
						array_push($ajaxData[$termName], $TermValue->value);
					}
					else
						array_push($ajaxData[$termName], $TermValue->value);
				} else
					$ajaxData[$termName]=$TermValue->value;
			}

			$ajaxData['id'] = $model->id;
            $ajaxData['mls_board_id'] = $model->mls_board_id;
			$ajaxData['agent_id'] = $model->agent_id;
			$ajaxData['contact_id'] = $model->contact_id;
            $ajaxData['component_type_id'] = $model->component_type_id;
            $ajaxData['component_id'] = $model->component_id;
			$ajaxData['name'] = $model->name;
			$ajaxData['description'] = $model->description;
            $ajaxData['frequency'] = $model->frequency;

            $ajaxData['sunday'] = $model->sunday;
            $ajaxData['monday'] = $model->monday;
            $ajaxData['tuesday'] = $model->tuesday;
            $ajaxData['wednesday'] = $model->wednesday;
            $ajaxData['thursday'] = $model->thursday;
            $ajaxData['friday'] = $model->friday;
            $ajaxData['saturday'] = $model->saturday;

			echo CJSON::encode($ajaxData);

			Yii::app()->end();
		}
	}

}
