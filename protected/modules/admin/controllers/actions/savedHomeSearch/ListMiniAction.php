<?php

class ListMiniAction extends CAction {

	public function run($id) {
		$model = $this->controller->baseModel;
		$model->unsetAttributes();
		$model->contact_id = $id;

		$this->controller->renderPartial('_listMini', array(
			'model' => $model
		));
	}
}
