<?php
/**
 * Creates a new task.
 *
 */
class EditAction extends CAction {

	public function run($id) {

		$model = $this->controller->loadModel($id);
        if(!Yii::app()->user->hasMultipleBoards()) {
            $model->mls_board_id = Yii::app()->user->board->id;
        }

        $this->performAjaxRequest($model);
	}

	protected function performAjaxRequest($model) {

		if (Yii::app()->request->isAjaxRequest) {
			if (isset($_POST['SavedHomeSearchForm'])) {
				$form = new SavedHomeSearchForm;
				$form->attributes = $_POST['SavedHomeSearchForm'];

				$model->attributes = $form->attributes;
                $model->mls_board_id = $form->mls_board_id;
				$model->name       = $form->name;
                $model->contact_id = $form->contact_id;
                $model->component_type_id = $form->component_type_id;
				$model->component_id = $form->component_id;
                $model->agent_id   = $form->agent_id;
				$model->frequency  = $form->frequency;
//                $model->sunday      = $form->sunday;
//                $model->monday      = $form->monday;
//                $model->tuesday     = $form->tuesday;
//                $model->wednesday   = $form->wednesday;
//                $model->thursday    = $form->thursday;
//                $model->friday      = $form->friday;
//                $model->saturday    = $form->saturday;
				$model->updated    = date('Y-m-d H:i:s');
				$model->updated_by = Yii::app()->user->id;

				if (!$model->save())
					echo CActiveForm::validate($model);
				else {
					$formFields = $form->fields;
					foreach($formFields as $field) { // go through each possible submitted field
						$termId = Terms::model()->getIdByName($field);
						$submitValue = $form->$field;

						$TermValues = TermComponentLu::model()->getSavedSearchValueByTermId($termId, $model->id);
						$count = count($TermValues);
						if($count) {
							if($count==1) {
								$TermValue = $TermValues[0];
								if(empty($submitValue) && $submitValue !='0'){ // delete if model exists and empty value submitted
									if(!$TermValue->delete())
										echo CJSON::encode($TermValue->getErrors());

								} else { // if submit value not empty
									$isArray = is_array($submitValue);
									if($isArray && !empty($submitValue)) { //array indicates multiple values, need to have group_id implementation
										$key = array_search($TermValue->value,$submitValue);
										if($key != null || $key == 0) {
											unset($submitValue[$key]);
											if($submitValue != null) {
												$groupId = $TermValue->id;
												foreach($submitValue as $submitValueSingle) {
													if(!empty($submitValueSingle) || $submitValueSingle === 0 || $submitValueSingle === "0") {
														$TermValue = new TermComponentLu;
														$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
														$TermValue->component_id = $model->id;
														$TermValue->term_id = $termId;
														$TermValue->value = trim($submitValueSingle);
														$TermValue->group_id = $groupId;

														if($groupId) {
															$TermValue->term_conjunctor = 'OR';
														}

														if(!$TermValue->save()) {
                                                            echo CJSON::encode($TermValue->getErrors());
                                                        }
														elseif(!$groupId) {
                                                            $groupId = $TermValue->id;
                                                        }
													}
												}
											}
										} else {
											if(!$TermValue->delete()) {
                                                echo CJSON::encode($TermValue->getErrors());
                                            }
											else {
												$groupId = TermComponentLu::model()->getParentIdFromSavedSearchValueByTermId($termId, $model->id);
												foreach($submitValue as $submitValueSingle) {
													if(!empty($submitValueSingle) || $submitValueSingle == 0) {
														$TermValue = new TermComponentLu;
														$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
														$TermValue->component_id = $model->id;
														$TermValue->term_id = $termId;
														$TermValue->value = trim($submitValueSingle);
														$TermValue->group_id = $groupId;

														if($groupId) {
															$TermValue->term_conjunctor = 'OR';
														}

														if(!$TermValue->save()) {
                                                            echo CJSON::encode($TermValue->getErrors());
                                                        }
														elseif(!$groupId)
															$groupId = $TermValue->id;
													}
												}
											}
										}

									} else {
										$TermValue->value = $submitValue;
										if(!$TermValue->save()){
                                            echo CJSON::encode($TermValue->getErrors());
                                        }
									}
								}
							} elseif($count>1) {
								// run through the models first, unsetting each submit value, then process what is left over in submitValue
								foreach($TermValues as $TermValue) {
									if($submitValue == null)
										$key = null;
									else
										$key = array_search($TermValue->value,$submitValue);

									if($key || $key===0) {
										$TermValue->value = $submitValue[$key];
										if(!$TermValue->save()) {
											echo CJSON::encode($TermValue->getErrors());
                                        }
										else
											unset($submitValue[$key]);
									} else {
										if(!$TermValue->delete())
											echo CJSON::encode($TermValue->getErrors());
									}
								}

								// run through whatever is left in submitValue
								if(count($submitValue)>0)
									foreach($submitValue as $submitValueSingle) { // adds these values
                                        if($submitValueSingle == "") {
                                            continue;
                                        }
										// check to see if other parents exist, if not record as parent, else record as child
										$groupId = 	TermComponentLu::model()->getParentIdFromSavedSearchValueByTermId($termId, $model->id);

										$TermValue = new TermComponentLu;
										$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
										$TermValue->component_id = $model->id;
										$TermValue->term_id = $termId;
										$TermValue->value = trim($submitValueSingle);
										$TermValue->group_id = $groupId;

										if($groupId)
											$TermValue->term_conjunctor = 'OR';

										if(!$TermValue->save()) {
											echo CJSON::encode($TermValue->getErrors());
                                        }
                                    }
							}

						} else { // isNewRecord
							if($submitValue || $submitValue == '0') {
								if(is_array($submitValue)) {
									$groupId = TermComponentLu::model()->getParentIdFromSavedSearchValueByTermId($termId, $model->id);
									foreach($submitValue as $submitValueSingle) {
										if(!empty($submitValueSingle) || $submitValueSingle == '0') {
											$TermValue = new TermComponentLu;
											$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
											$TermValue->component_id = $model->id;
											$TermValue->term_id = $termId;
											$TermValue->value = trim($submitValueSingle);
											$TermValue->group_id = $groupId;

											if($groupId) {
												$TermValue->term_conjunctor = 'OR';
											}

											if(!$TermValue->save())
												echo CJSON::encode($TermValue->getErrors());
											elseif(!$groupId)
												$groupId = $TermValue->id;
										}
									}
								} else {
									$TermValue = new TermComponentLu;
									$TermValue->component_type_id = ComponentTypes::SAVED_SEARCHES;
									$TermValue->component_id = $model->id;
									$TermValue->term_id = $termId;
									$TermValue->value = trim($submitValue);

									if(!$TermValue->save()) {
                                        echo CJSON::encode($TermValue->getErrors());
                                    }
								}
							}
						}
					}
				}
			}

			Yii::app()->end();
		}
	}
}
