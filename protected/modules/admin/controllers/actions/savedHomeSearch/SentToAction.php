<?php

class SentToAction extends CAction {

    public function run($id=null) {

        $this->controller->title = "Market Update Emails";

        $model = new EmailMessage;
        $model->component_type_id = ComponentTypes::SAVED_SEARCHES;
        if (!isset($_GET['EmailMessage']['contactId'])) {
            $model->toContact = ($id)? $id : Yii::app()->user->id;
        }

        if (isset($_GET['EmailMessage'])) {
            $model->attributes = $_GET['EmailMessage'];
        }

        $model->toEmail = new Emails('search');
        if (isset($_GET['Emails'])) {
            $model->toEmail->attributes = $_GET['Emails'];
        }

        $model->fromEmail = new Emails('search');
        $model->fromEmail->contact = new Contacts('search');

        $DataProvider = $model->search();
        $DataProvider->sort = array('defaultOrder'=>'processed_datetime DESC');
        $DataProvider->pagination = array('pageSize'=>50);

        $this->controller->render('sentTo', array(
                                            'model' => $model,
                                            'dataProvider' => $DataProvider,
                                            )
        );
    }
}
