<?php
/**
 * Views a task.
 */
Yii::import('stm_app_root.protected.modules.front.FrontModule');
class PreviewAction extends CAction {

	public function run($id)
    {
		$model = $this->controller->loadModel($id);
//		$this->performAjaxRequest($model);
		$this->controller->title = 'Home Search Preview';
		$Criteria = $model->getCriteria(ComponentTypes::SAVED_SEARCHES, $model->id);
		$ActiveCriteria = $model->getCriteriaByTermName('status', 'Active');
//        $Criteria->addCondition('status like "%Active%" AND status != "Inactive"');
		$Criteria->mergeWith($ActiveCriteria);

        $count = $model->findAllPropertiesCount($Criteria);

//		$DataProvider = $model->getDataProvider($Criteria);
		Yii::app()->clientScript->registerCssFile($this->getController()->getModule()->getCssAssetsUrl() . DS . 'style.homes.css');

		$this->controller->render('preview', array(
//										  'DataProvider' => $DataProvider,
										  'model' => $model,
                                          'count' => $count,
										  'htmlString' => $model->createEmailHtml(),
										  )
		);
	}
}