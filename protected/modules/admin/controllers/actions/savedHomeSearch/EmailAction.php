<?php

	class EmailAction extends CAction {

		public function run($id) {

			if ($EmailMessage = EmailMessage::model()->findByPk($id)) {
				if (Yii::app()->request->isAjaxRequest) {
					$this->performAjaxRequest($EmailMessage);
				}
			}
		}

		protected function performAjaxRequest(EmailMessage $EmailMessage) {

			if (Yii::app()->request->isAjaxRequest) {
				$data['subject'] = $EmailMessage->subject;
				$data['body'] = $EmailMessage->content;
				$data['sent'] = Yii::app()->format->formatDateTime($EmailMessage->processed_datetime);
				$data['to'] = $EmailMessage->toEmail->email;
				$data['cc'] = $EmailMessage->to_email_cc;
				$data['bcc'] = $EmailMessage->to_email_bcc;
				$data['from'] = $EmailMessage->fromEmail->email;

				echo CJSON::encode($data);

				Yii::app()->end();
			}
		}
	}
