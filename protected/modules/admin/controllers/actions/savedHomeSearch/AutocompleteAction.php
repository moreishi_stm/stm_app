<?php
	class AutocompleteAction extends CAction {

		public function run() {

			$tableName = Yii::app()->user->board->prefixedName;
			$model = MlsProperties::model($tableName);

			$this->processAjaxRequest($model);
		}

		protected function processAjaxRequest(MlsProperties $model)
		{
			if (Yii::app()->request->isAjaxRequest) {

				if (isset($_POST['term'])) {
					$collection = array();

					// legal subdivision
					$Criteria = new CDbCriteria;
					$Criteria->select = 'legal_subdivision';
					$Criteria->condition = 'legal_subdivision like :legal_subdivision AND status="Active"';
					$Criteria->params = array(':legal_subdivision'=>'%'.trim(Yii::app()->request->getPost('term')).'%');
					$Criteria->distinct = true;
					$Properties = $model->findAll($Criteria);

					$allSubdivisions = array();
					foreach($Properties as $Property)
						array_push($allSubdivisions, $Property->legal_subdivision);

					// common subdivision
					$Criteria = new CDbCriteria;
					$Criteria->select = 'common_subdivision';
					$Criteria->condition = 'common_subdivision like :common_subdivision AND status="Active"';
					$Criteria->params = array(':common_subdivision'=>'%'.trim(Yii::app()->request->getPost('term')).'%');
					$Criteria->distinct = true;
					$Properties = $model->findAll($Criteria);

					foreach($Properties as $Property) {
						if(!in_array($Property->common_subdivision, $allSubdivisions))
							array_push($allSubdivisions, $Property->common_subdivision);
					}
					//select distinct legal subdivision, then select distinct common sub where it doesn't exist in legal
					//put both in array an the merge and get unique... or check to see if it exists each time?

					foreach($allSubdivisions as $singleSubdivision)
						array_push($collection, array(
												'value'=>$singleSubdivision,
												'text'=>ucwords(strtolower($singleSubdivision)),
												));

					echo CJSON::encode($collection);
				}
				Yii::app()->end();
			}
		}
	}
