<?php
/**
 * Class DeleteAction
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class DeleteAction extends CAction {

	public function run($id) {
		$savedSearchEntry = SavedHomeSearches::model()->findByPk($id);
        if (empty($savedSearchEntry)) {
            Yii::log('Could not locate saved home search to remove.', CLogger::LEVEL_ERROR);
            echo CJSON::encode(array(
                'success' => 0,
            )) ;
            Yii::app()->end();
        }

		$this->performAjaxRequest($savedSearchEntry);
	}

	protected function performAjaxRequest(SavedHomeSearches $savedSearchEntry) {
		if (Yii::app()->request->isAjaxRequest) {
            $savedSearchEntry->delete();
			echo CJSON::encode(array(
				'success' => $savedSearchEntry->is_deleted,
			)) ;

			Yii::app()->end();
		}
	}
}