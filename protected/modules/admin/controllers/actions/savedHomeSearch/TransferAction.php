<?php

/**
 * Creates a new task.
 *
 */
class TransferAction extends CAction {

	public function run($id=null) {
        if(Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('tasksTransfer')) {

        }

        $this->controller->title = 'Transfer Saved Home Searches';

        $contactIds = Yii::app()->db->createCommand("select distinct agent_id from saved_home_searches")->queryColumn();

        $model = new Contacts;
        $Criteria = $model->getDbCriteria()->addInCondition('id', $contactIds);
        $Criteria->order = 'first_name ASC';

        $contactsList = CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName');

        if (Yii::app()->request->isAjaxRequest && !isset($_GET['ajax']) && isset($_POST['fromId']) && !empty($_POST['fromId']) && isset($_POST['toId']) && !empty($_POST['toId'])) {
            $this->performAjaxRequest($model, $_POST['fromId'], $_POST['toId']);
        }

        $dataProvider = new CActiveDataProvider('Contacts', array('criteria'=>$Criteria,'pagination'=>array('pageSize'=>100)));

        $this->controller->render('transfer', array(
                'dataProvider' => $dataProvider,
                'contactsList' => $contactsList,
            )
        );
	}

    protected function performAjaxRequest(Contacts $model, $fromId, $toId) {
        if (Yii::app()->request->isAjaxRequest) {

            if($fromId == $toId) {
                Yii::app()->end();
            }
            $savedHomeSearchIds = Yii::app()->db->createCommand()
                ->select('id')
                ->from('saved_home_searches')
                ->where('agent_id=:agent_id AND (is_deleted=0 OR is_deleted IS NULL)', array(':agent_id' => $fromId))
                ->queryColumn();

            $savedHomeSearch = new SavedHomeSearches;
            if($savedHomeSearch->updateAll(array('agent_id'=>$toId, 'updated_by'=>Yii::app()->user->id, 'updated'=>new CDbExpression('NOW()')),
                                $condition='agent_id=:agent_id',
                                $params=array(':agent_id'=>$fromId)) > 0) {

                $oldValue = array('savedHomeSearchIds'=>$savedHomeSearchIds,'agent_id'=>$fromId);
                $newValue = array('agent_id'=>$toId);
                $historyLog = new HistoryLog('transfer');
                $historyLog->setAttributes(array('model_name'=>'SavedHomeSearches',
                                                 'primary_key'=>$fromId,
                                                 'action_type_ma'=>HistoryLog::ACTION_TYPE_TRANSFER,
                                                 'old_value'=>CJSON::encode($oldValue),
                                                 'new_value'=>CJSON::encode($newValue),
                                                 'added_by'=>Yii::app()->user->id,
                                                 'added'=>new CDbExpression('NOW()')
                    ));
                if(!$historyLog->save()) {
                    echo CActiveForm::validate($model);
                    Yii::log('History Log could not save for task transfer.', CLogger::LEVEL_ERROR, 'savedHomeSearchTransfer');
                }
            }

            Yii::app()->end();
        }
    }
}
