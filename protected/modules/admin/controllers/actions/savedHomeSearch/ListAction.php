<?php

	class ListAction extends CAction {

		public function run($id = null) {

			$this->controller->title = "Saved Home Search";

			$model = $this->controller->baseModel;

			if ($id) {
				$model->contact_id = $id;
			}
            if (isset($_GET['agent_id'])) {
                $_GET['SavedHomeSearches']['agent_id']=$_GET['agent_id'];
            }

			if (isset($_GET['SavedHomeSearches'])) {
				$model->attributes = $_GET['SavedHomeSearches'];
			}

			$this->controller->render('list', array(
					'model' => $model
				)
			);
		}
	}
