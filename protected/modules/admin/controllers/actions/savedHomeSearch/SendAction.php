<?php

	class SendAction extends CAction
    {
		public function run($id)
        {
			if ($model = SavedHomeSearches::model()->findByPk($id)) {
				if (Yii::app()->request->isAjaxRequest) {
					$this->performAjaxRequest($model);
				}
			}
		}

		protected function performAjaxRequest(SavedHomeSearches $model) {

			if (Yii::app()->request->isAjaxRequest)
            {
                $Criteria = $model->getCriteria(ComponentTypes::SAVED_SEARCHES, $model->id);
                $ActiveCriteria = $model->getCriteriaByTermName('status', 'Active');
                $Criteria->mergeWith($ActiveCriteria);

                //$Properties = $model->findAllProperties($Criteria);

                $htmlString = $model->createEmailHtml();

                // Construct mail message
                $EmailMessage = new EmailMessage;
                $EmailMessage->subject = $_POST['subject'];
                $EmailMessage->component_type_id = $model->component_type_id;
                $EmailMessage->component_id = $model->component_id;
                $EmailMessage->to_email_id = $_POST['emailId'];
                $EmailMessage->from_email_id = Yii::app()->user->contact->getPrimaryEmailObj()->id;

                $marketUpdateViewPath = YiiBase::getPathOfAlias('admin_module.views.emails.marketUpdate').'.php';

                //$phoneNumberDisplay = $this->getDisplayPhoneNumber(Yii::app()->user->contact);

                $EmailMessage->content = $_POST['message']."<br><br>".Yii::app()->user->settings->email_signature."<br><br>".$htmlString;

//                $EmailMessage->content = $this->renderFile($marketUpdateViewPath, array(
//                        'savedSearchId' => $model->id,
//                        'updatedProperties' => $UpdatedProperties,
//                        'domain' => $this->domain,
//                        'mlsBoard' => $this->mlsBoard,
//                        'agent' => Yii::app()->user->contact,
//                        'contact' => $model->contact,
//                        'phoneNumberDisplay' => $phoneNumberDisplay,
//                        'propertyCount' => $propertyCount,
//                        'propertyLimit' => ($propertyCount <= $propertyLimit) ? $propertyCount : $propertyLimit,
//                        'emailId' => Yii::app()->user->contact->getPrimaryEmailObj()->id,
//                        'address'=>$this->getOfficeAddress(),
//                        'officeName' => $this->getOfficeName(),
//                    ), $returnContent=true);

                $transaction = Yii::app()->db->beginTransaction();

                if(!$EmailMessage->save()) {
                    Yii::log("Saved Search ID# {$model->id} / Contact ID# {$model->contact->id}: Email Message did not save. Email will not be sent. (Line: ".__LINE__.") | " . print_r($EmailMessage->getErrors(), true), CLogger::LEVEL_ERROR);
                    $status = 'error';
                    $message = current($EmailMessage->getErrors())[0];

                    // only send email if email message saved properly. Watch error logs for reason why didn't save. For now, we know primary reason is due to no email. Also even if did sent through StmMail, it can't be logged.
                }
                else {
                    $message = new StmMailMessage;
                    $message->setReplyToDomain(Yii::app()->user->primaryDomain);
                    $message->init($EmailMessage);

                    // Create email message
                    $zendMessage = new StmZendMail();
                    $zendMessage->addTo($EmailMessage->toEmail->email, Yii::app()->format->formatProperCase($EmailMessage->toEmail->contact->getFullName()));
                    $replyTo = $zendMessage->getReplyToEmail($EmailMessage, Yii::app()->user->domain->name);
                    $zendMessage->setFrom($replyTo, Yii::app()->format->formatProperCase($EmailMessage->fromEmail->contact->getFullName()));
                    $zendMessage->setSubject($EmailMessage->subject);
                    $zendMessage->setBodyHtmlTracking($EmailMessage, Yii::app()->user->primaryDomain->name);

                    $mailSent = false;
                    // Attempt to send message
                    try {
                        $zendMessage->checkBounceUndeliverable = false;
                        $zendMessage->send(StmZendMail::getAwsSesTransport());
                        $mailSent =true;
                        $transaction->commit();
                        $status = 'success';
                    }
                    catch(Exception $e) {
                        $transaction->rollback();
                        $status = 'error';
                        $message = $e->getMessage();

                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL
                            .'Saved Search #: '.$model->id.' | Email ID: '.$EmailMessage->toEmail->id.' | Sender: '.$zendMessage->getFrom().' | Recipients'.print_r($zendMessage->getRecipients(), true), CLogger::LEVEL_ERROR);
                    }
                }

				echo CJSON::encode(array('status'=>$status, 'message'=>$message));
				Yii::app()->end();
			}
		}
	}
