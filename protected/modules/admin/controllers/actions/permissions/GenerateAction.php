<?php

/**
 * Class GenerateActionOperations
 *
 * @author Chris Willard <chriswillard.dev@gmail.com>
 * @since  07/01/2013
 */
class GenerateAction extends CAction
{

    public function run()
    {

        /**
         * Get a list of action names to store in auth item
         */
        $actions = $this->getActionList();
        if (!$actions) {
            throw new CHttpException(500, 'Could not generate list of actions to process.');
        }

        /**
         * Need to check for existing auth items, if found skip checking logic
         */
        foreach ($actions as $controllerDir => $controllerActionList) {
            $existingControllerTaskItem = AuthItem::model()->byTypes(array(CAuthItem::TYPE_TASK))->findAll(
                array(
                    'condition' => 'name = :name',
                    'params' => array(':name' => $controllerDir,),
                )
            );

            /**
             * If we could not find a auth item with the given controller name, create one as a task
             */
            if (!$existingControllerTaskItem) {
                $taskAuthItem = new AuthItem;
                $taskAuthItem->name = $controllerDir;
                $taskAuthItem->type = CAuthItem::TYPE_TASK;
                $taskAuthItem->label = $this->formatLabelFromName($controllerDir);
                $taskAuthItem->save();
            }

            foreach ($controllerActionList as $actionName) {
                $existingActionAuthItem = AuthItem::model()->byTypes(array(CAuthItem::TYPE_OPERATION))->findAll(
                    array(
                        'condition' => 'name = :name',
                        'params' => array(':name' => $actionName,),
                    )
                );

                /**
                 * If we could not find a auth item with the given action name, create one as a operation
                 */
                if (!$existingActionAuthItem) {
                    $actionAuthItem = new AuthItem;
                    $actionAuthItem->name = $actionName;
                    $actionAuthItem->type = CAuthItem::TYPE_OPERATION;
                    $actionAuthItem->label = $this->formatLabelFromName($actionAuthItem->name);
                    $actionAuthItem->save();

                    /**
                     * Create the relationship to the controller task
                     */
                    $actionItemChild = new AuthItemChild;
                    $actionItemChild->parent = $controllerDir;
                    $actionItemChild->child = $actionAuthItem->name;
                    $actionItemChild->save();
                }
            }
        }

        $isSuccessful = true;
        $this->processAjaxRequest($isSuccessful);
    }

    /**
     * Retrieve a list of action names to store in auth_items
     *
     * @return mixed
     */
    protected function getActionList()
    {

        $actionList = array();

        $controllerList = $this->getControllerFileList();
        if ($controllerList) {
            Yii::import(Yii::app()->controller->module->name . '_module.controllers.*');
            foreach ($controllerList as $controllerName) {
                $className = ucwords("{$controllerName}Controller");
                $controller = new $className($controllerName);
                $controllerActions = $controller->actions();
                foreach ($controllerActions as $actionName => $actionAliasPath) {
                    $actionName = $controllerName . ucwords("{$actionName}Action");
                    $actionList[$controllerName][] = $actionName;
                }
            }
        }

        return $actionList;
    }

    /**
     * Retrieve a list of files from a given directory path
     *
     * @param $directoryPath
     *
     * @return array|bool
     */
    private function getFilesFromDirectoryPath($directoryPath)
    {

        if (!is_dir($directoryPath)) {
            return false;
        }

        $fileList = array();

        $files = scandir($directoryPath);
        if ($files) {
            foreach ($files as $file) {
                if (!in_array(
                    $file, array(
                        '.',
                        '..'
                    )
                )
                ) {
                    array_push($fileList, $file);
                }
            }
        }

        return $fileList;
    }


    /**
     * @return array
     */
    protected function getControllerFileList()
    {

        $controllerList = array();

        $controllerDirPath = Yii::getPathOfAlias(Yii::app()->controller->module->name . '_module.controllers');
        $controllerFilesList = $this->getFilesFromDirectoryPath($controllerDirPath);
        foreach ($controllerFilesList as $controllerFile) {
            $controllerFilePath = $controllerDirPath . DS . $controllerFile;
            if (is_file($controllerFilePath) && substr($controllerFile, -3) == 'php') {
                $controllerName = $this->formatControllerNameFromFile($controllerFile);
                $controllerList[] = $controllerName;
            }
        }

        return $controllerList;
    }

    /**
     * Handle the action if it is processed via ajax
     */
    protected function processAjaxRequest($isSuccessful)
    {

        if (!Yii::app()->getRequest()->isAjaxRequest) {
            return false;
        }

        echo CJSON::encode(
            array('isSuccessful' => $isSuccessful,)
        );

        Yii::app()->end();
    }

    /**
     * Formats a action file name in the following way, to prep it for the auth_items table:
     * AddAction.php > addAction
     *
     * @param $actionFileName
     *
     * @return string
     */
    private function formatActionNameFromFile($actionFileName)
    {

        $actionName = current(explode('.', $actionFileName));

        return strtolower($actionName{0}) . substr($actionName, 1);
    }

    /**
     * @param $controllerFileName
     * @return string
     */
    private function formatControllerNameFromFile($controllerFileName)
    {

        $controllerName = current(explode('Controller.php', $controllerFileName));

        return strtolower($controllerName{0}) . substr($controllerName, 1);
    }

    /**
     * Formats the label name from the auth item name, example:
     * addAction => Add Action
     *
     * @param $authItemName
     *
     * @return mixed
     */
    private function formatLabelFromName($authItemName)
    {

        $regexForCapitalLetters = '/(?<!^)((?<![[:upper:]])[[:upper:]]|[[:upper:]](?![[:upper:]]))/';
        $label = preg_replace($regexForCapitalLetters, ' $1', $authItemName);
        $label = strtoupper($label{0}) . substr($label, 1);

        return $label;
    }
}