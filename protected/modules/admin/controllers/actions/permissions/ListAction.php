<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/25/13
 */

class ListAction extends CAction {

	public function run() {

		$this->controller->title = 'Permissions List';

//		$location = Yii::app()->geoip->lookupLocation('209.85.135.104');
        $model = new AuthItem;

		if(isset($_GET['AuthItem'])) {
            $model->attributes = $_GET['AuthItem'];
		}

		$this->getController()->render('list', array(
			'authItemProvider' => $model->search(),
		));
	}
}