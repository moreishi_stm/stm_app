<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>, Christine Lee
 * @since: 6/27/13, 8/24/13
 */

class AssignAction extends CAction {

	// Used in the url to determine if we are assigning or revoking permissions
	const FLAG_APPLY = 'apply';
	const FLAG_REVOKE = 'revoke';

	// Identifier to the user saved state of the role selected
	const STATE_ROLE = 'AssignAction_RoleSelected';

	public function run() {

		$this->controller->title = 'Assign Permissions';
		// Process the role selection changes and get the current state of a row that may be selected
		$roleSelected = Yii::app()->user->getState(self::STATE_ROLE);

		// Get all of the roles, tasks, operations from the system
        if(Yii::app()->controller->module->id == 'hq') {
            $roles = AuthItem::model()->byTypes(array(CAuthItem::TYPE_ROLE))->byIn('name',array('owner','sales','jrdeveloper'))->findAll();
        } else {
            $roles = AuthItem::model()->byTypes(array(CAuthItem::TYPE_ROLE))->byIn('name',array('owner','agent','lender', AuthItem::ROLE_RECRUIT_REFERRING_AGENT))->findAll();
        }


		$tasks = AuthItem::model()->byTypes(array(CAuthItem::TYPE_TASK))->findAll();
		$operations = AuthItem::model()->byTypes(array(CAuthItem::TYPE_OPERATION))->findAll();

		if (Yii::app()->request->isAjaxRequest) {

			if(isset($_GET['updatePermissionsFlag']) && $_GET['updatePermissionsFlag']) {
				$this->performAjaxUpdateRequest($operations, $tasks, $roleSelected);
			}
			$Criteria = $this->performAjaxFilterRequest();
		}
		if(!$Criteria)
			$Criteria = AuthItem::model()->byTypes(array(CAuthItem::TYPE_TASK))->getDbCriteria();

		$authItemProvider = new CActiveDataProvider('AuthItem', array(
			'criteria' => $Criteria,
			'pagination' => array(
				'pageSize' => 1000,
			),
			'sort' => array(
				'defaultOrder' => array(
					'type' => CSort::SORT_DESC,
					'label' => CSort::SORT_ASC,
				),
			),
		));

		$this->getController()->render('assign', array(
			'roles' => $roles,
			'tasks' => $tasks,
			'operations' => $operations,
			'roleSelected' => $roleSelected,
			'authItemProvider' => $authItemProvider,
		));
	}

	protected function performAjaxFilterRequest() {


		if (Yii::app()->request->isAjaxRequest) {
			$Criteria = new CDbCriteria();

			if (isset($_GET['roleSelector'])) {
				$roleSelected = $_GET['roleSelector'];
				Yii::app()->user->setState(self::STATE_ROLE, $roleSelected);
			}
			if(isset($_GET['assignStatus']) && $_GET['assignStatus'] !='') {
				if($_GET['assignStatus'] == 1) {
					$Criteria->mergeWith(AuthItem::model()->byAssigned(Yii::app()->user->getState(self::STATE_ROLE))->getDbCriteria());
				} else {
					$Criteria->mergeWith(AuthItem::model()->byNotAssigned(Yii::app()->user->getState(self::STATE_ROLE))->getDbCriteria());
				}
			}

			if(isset($_GET['authItemType']) && $_GET['authItemType'] !='') {
				$Criteria->mergeWith(AuthItem::model()->byTypes(array($_GET['authItemType']))->getDbCriteria());
			} else {
				$Criteria->mergeWith(AuthItem::model()->byTypes(array(CAuthItem::TYPE_TASK, CAuthItem::TYPE_OPERATION))->getDbCriteria());
			}

			if(isset($_GET['authOperation']) && !empty($_GET['authOperation'])) {
				$Criteria->mergeWith(AuthItem::model()->byChild($_GET['authOperation'])->getDbCriteria());
			}
		}

		if($Criteria && !empty($Criteria->condition))
			return $Criteria;
		else
			return null;
	}

	protected function performAjaxUpdateRequest($operations, $tasks, $role) {

		if (Yii::app()->request->isAjaxRequest) {
			if(isset($_GET['updatePermissionsFlag']) && $_GET['updatePermissionsFlag']) {

				if(!$Criteria = $this->performAjaxFilterRequest())
					$Criteria = AuthItem::model()->byTypes(array(CAuthItem::TYPE_TASK, CAuthItem::TYPE_OPERATION))->getDbCriteria();

				$AuthItems = AuthItem::model()->findAll($Criteria);
				$filteredPermissionList = array();
				foreach($AuthItems as $AuthItem) {
					array_push($filteredPermissionList, $AuthItem->name);
				}

				$CurrentPermissions = AuthItemChild::model()->byParent($role)->byChild($filteredPermissionList)->findAll();

				$postAuthNames = explode(',', $_GET['authNames']);
				$postAuthNamesFlipped = array_flip($postAuthNames);

				$taskAndOperations = array_merge($operations,$tasks);

				foreach($CurrentPermissions as $Permission) {
					if(isset($postAuthNamesFlipped[$Permission->child])) {
						unset($postAuthNamesFlipped[$Permission->child]);
					} else {
						//delete items that should not be there anymore
						if(!$Permission->delete())
							Yii::app()->user->setFlash('error', $Permission->name.' did not delete.');
					}
				}
				$postAuthNames = array_flip($postAuthNamesFlipped);

				foreach($postAuthNames as $authName) {
					//then add any that are left to add in post vars
					$AuthItemChild = new AuthItemChild;
					$AuthItemChild->parent = $role;
					$AuthItemChild->child = $authName;
					$AuthItemChild->save();

				}
			}
			unset($_GET['updatePermissionsFlag']);
		}
	}

}