<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/26/13
 */

class DeleteParentChildAction extends CAction {

	public function run() {

		// Find the auth item that we need to delete
		if (Yii::app()->getRequest()->isAjaxRequest) {

            $Criteria = new CDbCriteria;

            $otherType = ($_GET['type'] == 'child') ? 'parent' : 'child';

            $Criteria->compare($_GET['type'], $_GET['targetname']);
            $Criteria->compare($otherType, $_GET['subjectname']);
            $model = AuthItemChild::model()->find($Criteria);
            if($model->delete()) {
                echo CJSON::encode(array('status' => 'success'));
            } else {
                echo CJSON::encode(array('status' => 'error'));
            }

			Yii::app()->end();
		}
	}
}