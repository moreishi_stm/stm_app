<?php
	/**
	 * @author: Chris Willard <chriswillard.dev@gmail.com>
	 * @since : 6/26/13
	 */

	class EditAction extends CAction {

		public function run($name) {
			$this->controller->title = 'Edit Role';
			// Find the auth item that we need to edit
			$authItem = AuthItem::model()->findByPk($name);
			if (!$authItem) {
				throw new Exception('Invalid Role type.'); //@todo: need to figure out a more graceful approach
			}

			// If the form has been submitted process the new auth item
			$postedAuthItemData = $_POST[get_class($authItem)];
			if (isset($postedAuthItemData)) {
				$authItem->attributes = $postedAuthItemData;
				$authItemSaved = $authItem->save();
				if ($authItemSaved) {
					$properCaseName = Yii::app()->format->formatProperCase($authItem->label);
					Yii::app()->user->setFlash('success', 'Successfully edited ' . $properCaseName);
					$this->getController()->redirect(array(
							'/'.Yii::app()->controller->module->id.'/permissions/list',
							'type' => strtolower($authItem->getTypeName())
						)
					);
				}
			}

			if (isset($_POST['tasksAndOperations'])) {
				$authItemChild = AuthItemChild::model()->findByPk(array(
						'parent' => $authItem->name,
						'child' => $_POST['tasksAndOperations']
					)
				);
				if (!$authItemChild) {
					$authItemChild = new AuthItemChild;
					$authItemChild->parent = $authItem->name;
					$authItemChild->child = $_POST['tasksAndOperations'];
					if ($authItemChild->save()) {
						Yii::app()->user->setFlash('success', 'Successfully added Child Permission.');
					} else {
						Yii::app()->user->setFlash('error', 'Error in adding Child Permission.');
					}
				}
			}

			$this->getController()->render('edititem', array(
					'authItemType' => $authItem->type,
					'authItem' => $authItem,
				)
			);
		}
	}