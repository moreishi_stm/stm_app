<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/26/13
 */

class DeleteAction extends CAction {

	public function run($name) {

		// Find the auth item that we need to delete
		if (Yii::app()->getRequest()->isAjaxRequest) {

            $Criteria = new CDbCriteria;
            $Criteria->condition = 'parent=:parent OR child=:child';
            $Criteria->params = array(':parent'=>$name,':child'=>$name);

            $FoundChild = AuthItemChild::model()->count($Criteria);
            if($FoundChild) {
                echo CJSON::encode(array(
                                   'isSuccessful' => false,
                                   ));
            } else {
                $isSuccessful = AuthItem::model()->deleteByPk($name);
                echo CJSON::encode(array(
                                   'isSuccessful' => $isSuccessful,
                                   ));
            }

			Yii::app()->end();
		}
	}
}