<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/26/13
 */

class AddAction extends CAction {

	public function run($type) {

		// Get the auth item type based on the value from the CAuthItem component
		$authItemType = AuthItem::getAuthItemTypeFromString($type);
		$properCaseType = Yii::app()->format->formatProperCase($type);

		// Init the auth item and set the type before processing
		$authItem = new AuthItem;
		$authItem->type = $authItemType;

		// If the form has been submitted process the new auth item
		$postedAuthItemData = $_POST[get_class($authItem)];
		if (isset($postedAuthItemData)) {
			$authItem->attributes = $postedAuthItemData;
			$authItemSaved = $authItem->save();
			if ($authItemSaved) {
				Yii::app()->user->setFlash('success', 'Successfully added '.$type);
				$this->getController()->redirect(array('/' . $this->controller->module->name . '/permissions/list', 'type'=>$type));
			}
		}

		$this->getController()->render('additem', array(
			'authItemType' => $authItemType,
			'authItemTypeStr' => $properCaseType,
			'authItem' => $authItem,
		));
	}
}