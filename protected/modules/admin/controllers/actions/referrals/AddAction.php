<?php

/**
 * Creates a new task.
 *
 */
class AddAction extends CAction {

	public function run($id=null) {
		$this->controller->title = 'Add Referrals';
		if($id && $Contact = Contacts::model()->findByPk($id)) {
			$model = new Referrals;
			$model->contact_id = $Contact->id;
			if (isset($_POST['Referrals'])) {
				$model->attributes = $_POST['Referrals'];

				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Successfully added a Referral.');
					$this->controller->redirect('/'.Yii::app()->controller->module->name.'/referrals/'.$model->id);
				}
			}
			$this->controller->render('form', array('model'=>$model, 'Contact'=>$Contact));
		} else {
			$Contact = new Contacts;

			if (isset($_GET['Contacts'])) {
				$Contact->attributes = $_GET['Contacts'];
			}

			$Contact->emails = new Emails('search');
			if (isset($_GET['Emails'])) {
				$Contact->emails->attributes = $_GET['Emails'];
			}

			$Contact->phones = new Phones('search');
			if (isset($_GET['Phones'])) {
				$Contact->phones->attributes = $_GET['Phones'];
			}

            $Contact->addresses = new Addresses('search');
            if (isset($_GET['Addresses'])) {
                $Contact->addresses->attributes = $_GET['Addresses'];
            }

            $Contact->contactTypeLu = new ContactTypeLu('search');
			if (isset($_GET['ContactTypeLu'])) {
				$Contact->contactTypeLu->attributes = $_GET['ContactTypeLu'];
			}

			$Contact->source = new Sources('search');
			if (isset($_GET['Sources'])) {
				$Contact->source->attributes = $_GET['Sources'];
			}

			$this->controller->render('add', array('Contact'=>$Contact));
		}
	}
}
