<?php

class ListMiniAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		// $model=new ActionPlans('search');
		// $model->unsetAttributes();  // clear any default values
		// $model->transaction_type_id = $transactionType;

		// $result = ActionPlans::getAppliedPlans($transactionType, $recordId);

		// $dataProvider=ActionPlans::getAppliedPlans($transactionType, $recordId);

		// used for gridview search box inputs
		// if (isset($_GET['ActionPlans']))
		// 	$model->attributes=$_GET['ActionPlans'];

		$this->controller->renderPartial('_listMini',array(
			'dataProvider'=>$dataProvider
		));
	}
}