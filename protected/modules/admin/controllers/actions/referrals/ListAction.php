<?php
class ListAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Referrals List';

        $model = new Referrals('search');
		$model->unsetAttributes();  // clear any default values

		// used for gridview search box inputs
        if (isset($_GET['Referrals'])) {
			$model->attributes=$_GET['Referrals'];
        }

        if(isset($_GET['Referrals']['fromDate'])) {
            $fromDate = $_GET['Referrals']['fromDate'];
        }

        if(isset($_GET['Referrals']['toDate'])) {
            $toDate = $_GET['Referrals']['toDate'];
        }

        // @todo: need these numbers to be filtered by the other filters in listSearchBox like assigned_to, direction, etc.
		$data['inboundQuantity'] = $model->byReferralDate($fromDate, $toDate)->byInbound()->count();
		$data['inboundVolume'] = $model->byReferralDate($fromDate, $toDate)->byInbound()->bySumVolume()->find()->priceSum;
		$data['inboundPotentialIncome'] = $model->byReferralDate($fromDate, $toDate)->byInbound()->bySumIncome(Referrals::REFERRAL_DIRECTION_IN)->byStatusIds($model->getActiveStatusIds())->find()->incomeSum*.03;
		$data['inboundClosedIncome'] = $model->byReferralDate($fromDate, $toDate)->byInbound()->bySumIncome(Referrals::REFERRAL_DIRECTION_IN)->byStatusIds(array(TransactionStatus::CLOSED_ID))->find()->incomeSum*.03;

		$data['outboundQuantity'] = $model->byReferralDate($fromDate, $toDate)->byOutbound()->count();
		$data['outboundVolume'] = $model->byReferralDate($fromDate, $toDate)->byOutbound()->bySumVolume()->find()->priceSum;
		$data['outboundPotentialIncome'] = $model->byReferralDate($fromDate, $toDate)->byOutbound()->bySumIncome(Referrals::REFERRAL_DIRECTION_OUT)->byStatusIds($model->getActiveStatusIds())->find()->incomeSum*.03;
		$data['outboundClosedIncome'] = $model->byReferralDate($fromDate, $toDate)->byOutbound()->bySumIncome(Referrals::REFERRAL_DIRECTION_OUT)->byStatusIds(array(TransactionStatus::CLOSED_ID))->find()->incomeSum*.03;

		$this->controller->render('list',array(
			'model'=>$model,
			'data'=>$data,
		));
	}
}
