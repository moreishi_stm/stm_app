<?php

/**
 * Creates a new task.
 *
 */
class EditAction extends CAction {

	public function run($id) {
		$this->controller->title = 'Edit Referrals';

		$model = $this->controller->loadModel($id);
		if (isset($_POST['Referrals'])) {
			$model->attributes = $_POST["Referrals"];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully added a Referral.');
				$this->controller->redirect('/'.Yii::app()->controller->module->name.'/referrals/'.$model->id);
			}
		}
		$this->controller->render('form', array('model'=>$model, 'Contact'=>$model->contact));
	}
}
