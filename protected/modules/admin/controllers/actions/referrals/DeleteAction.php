<?php

class DeleteAction extends CAction {
	/**
	 * Manages models
	 */

    public function run($id) {

        $referral = Referrals::model()->findByPk($id);
        if (isset($_POST['Referrals'])) {
            $referral->attributes = $_POST['Referrals'];
        }

        $this->performAjaxRequest($referral);
    }

    protected function performAjaxRequest(Referrals $model) {

        if (Yii::app()->request->isAjaxRequest) {
            if (!$model->delete()) {
                echo CActiveForm::validate($model);
            }

            Yii::app()->end();
        }
    }
}
