<?php

class AddAction extends CAction {

	public function run() {
		$this->controller->title = 'Add Company';
		$model = new Companies;

		// Process additional phone for the company if any
		$companyPhonesValidated = true;
		if (isset($_POST['Phones'])) {
			foreach ($_POST['Phones'] as $i => $phoneData) {
				$CompanyPhone = new Phones;
				$CompanyPhone->attributes = $phoneData;

				// Mark that the company e-mails section had a failure
				if (!$CompanyPhone->validate() && $companyPhonesValidated) {
					$companyPhonesValidated = false;
				}
				array_push($model->companyPhones, $CompanyPhone);
			}
		}

		// Process additional addresses for the company if any
		$companyAddressesValidated = true;
		if (isset($_POST['Addresses'])) {
			foreach ($_POST['Addresses'] as $i => $addressData) {
				$CompanyAddress = new Addresses;
				$CompanyAddress->attributes = $addressData;

				// Mark that the company address section had a failure
				if (!$CompanyAddress->validate() && $companyAddressesValidated) {
					$companyAddressesValidated = false;
				}
				array_push($model->companyAddresses, $CompanyAddress);
			}
		}

		// Return a boolean if each sub section validated
		$companySubSectionsValidated = ($companyAddressesValidated && $companyPhonesValidated) ? true : false;

		if (isset($_POST['Companies'])) {
			$model->attributes = $_POST['Companies'];
			$model->validate();

			if ($companySubSectionsValidated && $model->save()) {
				// Process the phones for this company
				foreach ($model->companyPhones as $CompanyPhone) {
					$CompanyPhone->account_id = Yii::app()->user->accountId;
					$CompanyPhone->company_id = $model->id;
					$CompanyPhone->save(false); // already ran validation
				}

				// Process the addresses for this company
				foreach ($model->companyAddresses as $CompanyAddress) {
					$CompanyAddress->companyId = $model->id;
					$CompanyAddress->isPrimary = $CompanyAddress->isPrimary;
					$CompanyAddress->account_id = Yii::app()->user->accountId;
					$CompanyAddress->save(false); // already ran validation
				}

				// Attach the company types to the newly contact
				foreach ($model->companyTypesToAdd as $companyTypeId) {
					$CompanyTypeLu = new CompanyTypeLu;
					$CompanyTypeLu->company_id = $model->id;
					$CompanyTypeLu->company_type_id = $companyTypeId;
					if ($CompanyTypeLu->save())
						Yii::app()->user->setFlash('success', 'Successfully Updated Type!');

				}

				Yii::app()->user->setFlash('success', 'Successfully Added Company!');
				$this->controller->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->controller->breadcrumbs = array(
			'Add' => array('/'.Yii::app()->controller->module->id.'/companies/add'),
		);
		$this->controller->pageTitle = 'Add New Company';

		Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_AddressPhoneEmailGui.js');
		$this->controller->render('form', array(
			'model' => $model,
		));
	}
}