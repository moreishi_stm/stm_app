<?php

class ViewAction extends CAction {

	public function run($id) {
		$model = $this->controller->loadModel($id);
		$this->controller->title = $model->name;

		if (!$model) {
			throw new CHttpException(500, 'Could not load company.');
		}

		$this->controller->pageTitle = $model->name;
		// $this->controller->subTitle = $model->name;
		$this->controller->breadcrumbs=array(
			$model->name=>array('/admin/companies/view', 'id'=>$model->id),
		);

		$this->controller->render('view', array(
			'model'=>$model,
		));
	}
}
