<?php

/**
 * Creates a new task.
 *
 */
class RemoveContactAction extends CAction {

	public function run() {
        $model = new CompanyContactLu;
        $this->performAjaxRequest($model);
	}

	protected function performAjaxRequest(CompanyContactLu $model) {
		if (Yii::app()->request->isAjaxRequest) {

			if (isset($_GET['CompanyContactLu'])) {

                $model->attributes = $_GET['CompanyContactLu'];

				$criteria=new CDbCriteria;

				$result = CompanyContactLu::model()->deleteAll(array(
						'condition' => 'company_id = :company_id and contact_id = :contact_id',
						'params' => array(
						':company_id' => $model->company_id,
						':contact_id' => $model->contact_id,
					),
				));

				if($result)
					echo 'true';
				else
					echo 'false';
            }

            Yii::app()->end();
		}
	}

}
