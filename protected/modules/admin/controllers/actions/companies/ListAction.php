<?php
class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'All Companies';
		$model=new Companies('search');
		$model->unsetAttributes();  // clear any default values
        $model->status_ma = 1;

		// used for gridview search box inputs
		if (isset($_GET['Companies']))
			$model->attributes=$_GET['Companies'];

		$model->companyTypeLu = new CompanyTypeLu('search');
		if (isset($_GET['CompanyTypeLu'])) {
			$model->companyTypeLu->attributes = $_GET['CompanyTypeLu'];
		}

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}