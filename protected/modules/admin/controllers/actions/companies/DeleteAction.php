<?php
/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class DeleteAction extends CAction {

    public function run($id) {

        $companies = Companies::model()->findByPk($id);
        if (isset($_POST['Companies'])) {
            $companies->attributes = $_POST['Companies'];
        }

        $this->performAjaxRequest($companies);
    }

    protected function performAjaxRequest(Companies $model) {

        if (Yii::app()->request->isAjaxRequest) {
            $model->status_ma=0;
            $model->save();
            echo CActiveForm::validate($model);

            Yii::app()->end();
        }
    }
}