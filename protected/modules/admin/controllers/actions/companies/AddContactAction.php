<?php

/**
 * Add a contact to a specific company
 */
class AddContactAction extends CAction {

	public function run() {
        $model = new CompanyContactLu;
        $this->performAjaxRequest($model);
	}

	protected function performAjaxRequest(CompanyContactLu $model) {
		if (Yii::app()->request->isAjaxRequest) {

			if (isset($_POST['CompanyContactLu'])) {
                $model->attributes = $_POST['CompanyContactLu'];
                if (!$model->save()) {
                    echo CActiveForm::validate($model);
                }
            }

            Yii::app()->end();
		}
	}

}
