<?php

class EditAction extends CAction
{
	public function run($id)
	{
		$this->controller->title = 'Edit Company';
		$model = $this->controller->loadModel($id);

		if (!$model)
			throw new CHttpException(500, 'Could not load company.');

		// Process additional e-mails for the company if any
		$companyPhonesValidated = true;
		if (isset($_POST['Phones']))
		{
			foreach($_POST['Phones'] as $i => $phoneData)
			{
				if ($phoneData['id'])
				{
					$Criteria = new CDbCriteria;
					$Criteria->condition = 'id = :id';
					$Criteria->params = array(
						':id'=>$phoneData['id'],
					);
					$CompanyPhone = Phones::model()->find($Criteria);
				}
				else
					$CompanyPhone = new Phones;

				$CompanyPhone->attributes = $phoneData;

				if ($CompanyPhone->remove)
				{
					$CompanyPhone->delete();
					continue;
				}

				// Mark that the company e-mails section had a failure
				if (!$CompanyPhone->validate() && $companyPhonesValidated)
					$companyPhonesValidated = false;
				array_push($model->companyPhones, $CompanyPhone);
			}
		}

		// Process additional addresses for the company if any
		$companyAddressesValidated = true;
		if (isset($_POST['Addresses']))
		{
			foreach($_POST['Addresses'] as $i => $addressData)
			{
				$CompanyAddress = new Addresses;
				$CompanyAddress->attributes = $addressData;

				// Mark that the company address section had a failure
				if (!$CompanyAddress->validate() && $companyAddressesValidated)
					$companyAddressesValidated = false;
				array_push($model->companyAddresses, $CompanyAddress);
			}
		}


		// Return a boolean if each sub section validated
		$companySubSectionsValidated = ($companyAddressesValidated && $companyPhonesValidated) ? true : false;

		if (isset($_POST['Companies']))
		{
			$model->attributes = $_POST['Companies'];
			$model->validate();

			if ($companySubSectionsValidated && $model->save())
			{
				// Process Phone Numbers
				if ($model->companyPhones)
				{
					foreach($model->companyPhones as $CompanyPhone)
					{
						if ($CompanyPhone->isNewRecord)
						{
							$CompanyPhone->company_id = $model->id;
							$CompanyPhone->account_id = Yii::app()->user->accountId;
						}

						$CompanyPhone->save(false); // already ran validation
					}
				}

				// Remove all addresses for a company and then re-add
				if ($model->addresses)
				{
					foreach($model->addresses as $Address)
						$Address->delete();
				}

				// Process the addresses for this company
				if ($model->companyAddresses)
				{
					foreach($model->companyAddresses as $CompanyAddress)
					{
						$CompanyAddress->companyId = $model->id;
						$CompanyAddress->isPrimary = $CompanyAddress->isPrimary;
						$CompanyAddress->account_id = Yii::app()->user->accountId;
						$CompanyAddress->save(false); // already ran validation
					}
				}

				// We only will add new ones when one is not assigned to the contact or when one is deleted
				if ($model->companyTypesToAdd) {
					$companyTypesToAddFlipped = array_flip($model->companyTypesToAdd);
					foreach ($model->companyTypeLu as $CompanyTypeLu) {
						if (!in_array($CompanyTypeLu->company_type_id, $model->companyTypesToAdd)) {
							$CompanyTypeLu->delete();
						} else {
							unset($companyTypesToAddFlipped[$CompanyTypeLu->company_type_id]);
						}
					}

					$model->companyTypesToAdd = array_flip($companyTypesToAddFlipped);

					// Attach the company types to the company
					foreach ($model->companyTypesToAdd as $companyTypeId) {
						$CompanyTypeLu = new CompanyTypeLu;
						$CompanyTypeLu->company_id = $model->id;
						$CompanyTypeLu->company_type_id = $companyTypeId;
						$CompanyTypeLu->save();
					}
				}

				Yii::app()->user->setFlash('success', 'Successfully Edited Company!');
				$this->controller->redirect(array('view','id'=>$model->id));
			}
		}
		else
		{
			$model->companyPhones    = $model->phones;
			$model->companyAddresses = $model->addresses;

			// Process the contact types
			if ($model->companyTypeLu) {
				foreach ($model->companyTypeLu as $CompanyTypeLu) {
					array_push($model->companyTypesToAdd, $CompanyTypeLu->type->id);
				}
			}
		}

		$this->controller->breadcrumbs=array(
			 $model->name=>array('/'.Yii::app()->controller->module->id.'/companies/view', 'id'=>$model->id),
			'Edit'=>array('/'.Yii::app()->controller->module->id.'/companies/edit', 'id'=>$model->id),
		);
		$this->controller->pageTitle = 'Edit Company';
		$this->controller->subTitle = $model->name;

		Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl.DS.'stm_AddressPhoneEmailGui.js');
		$this->controller->render('form',array(
			'model'=>$model,
		));
	}
}