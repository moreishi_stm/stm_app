<?php

class CalculatorAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'Income Calculator';

		$this->controller->render('calculator');
	}
}