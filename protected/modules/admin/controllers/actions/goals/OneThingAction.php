<?php

class OneThingAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id=null)
	{
		$this->controller->title = 'One Thing';
		$model=new GoalsOneThing;
        $model->unsetAttributes();
        $model->contact_id = ($id)? $id: Yii::app()->user->id;

		// used for gridview search box inputs
		if (isset($_GET['GoalsOneThing'])) {

            $model->attributes=$_GET['GoalsOneThing'];
        }

        // see if there is a goal for this year
        $thisYearGoalExists = Goals::model()->find(array('condition'=>'year='.date('Y').' AND contact_id='.$model->contact_id));

        if($thisYearGoalExists->hasWeeklyGoals) {
            // get the one thing unit
            $oneThingUnit = GoalsWeekly::model()->getOneThingUnit($thisYearGoalExists->id);
            $oneThingQuantity = GoalsWeekly::model()->getOneThingQuanity($thisYearGoalExists->id);

            // get YTD goal # & percentages
            $mtdGoalNumber = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-m-01'), date('Y-m-d'), 'one_thing_quantity');
            $mtdGoalResults = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-m-01'), date('Y-m-d'), $oneThingUnit);
            $mtdGoalPercent = GoalsWeekly::model()->getGoalPercent($thisYearGoalExists->id, date('Y-m-01'), date('Y-m-d'));

            $ytdGoalNumber = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'one_thing_quantity');
            $ytdGoalResults = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), $oneThingUnit);
            $ytdGoalPercent = GoalsWeekly::model()->getGoalPercent($thisYearGoalExists->id, date('Y-01-01'), date('Y-m-d'));

            $kpiData = array('closings'=>array(
                                'ytdGoal' => $kpiClosingsGoal = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'closings'),
                                'ytdActual' => $kpiClosingsActual = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'closings'),
                                'ytdPercent'=> ($kpiClosingsGoal) ? round($kpiClosingsActual / $kpiClosingsGoal * 100) : 0,
                                ),
                             'contracts' => array(
                                 'ytdGoal' => $kpiContractGoal = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'contracts'),
                                 'ytdActual' => $kpiContractActual = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'contracts'),
                                 'ytdPercent'=> ($kpiContractGoal) ? round($kpiContractActual / $kpiContractGoal * 100) : 0,
                             ),
                             'signed' => array(
                                 'ytdGoal' => $kpiSignedGoal = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'signed'),
                                 'ytdActual' => $kpiSignedActual = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), GoalsWeekly::ONE_THING_TYPE_SIGNED),
                                 'ytdPercent'=> ($kpiSignedGoal) ? round($kpiSignedActual / $kpiSignedGoal * 100) : 0,
                             ),
                             'met' => array(
                                 'ytdGoal' => $kpiMetGoal = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'appointments_met'),
                                 'ytdActual' => $kpiMetActual = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), GoalsWeekly::ONE_THING_TYPE_APPOINTMENT_MET),
                                 'ytdPercent'=> ($kpiMetGoal) ? round($kpiMetActual / $kpiMetGoal * 100) : 0,
                             ),
                             'set' => array(
                                 'ytdGoal' => $kpiSetGoal = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'appointments_set'),
                                 'ytdActual' => $kpiSetActual = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), GoalsWeekly::ONE_THING_TYPE_APPOINTMENT_SET),
                                 'ytdPercent'=> ($kpiSetGoal) ? round($kpiSetActual / $kpiSetGoal * 100) : 0,
                             ),
                             'contacts' => array(
                                 'ytdGoal' => $kpiContactGoal = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'contacts'),
                                 'ytdActual' => $kpiContactActual = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'contacts'),
                                 'ytdPercent'=> ($kpiContactGoal) ? round($kpiContactActual / $kpiContactGoal * 100) : 0,
                             ),
                             'leadgen' => array(
                                 'ytdGoal' => $kpiLeadgenGoal = GoalsWeekly::getGoalNumber($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'lead_gen_hours'),
                                 'ytdActual' => $kpiLeadgenActual = GoalsWeekly::model()->getGoalResults($thisYearGoalExists->id, $thisYearGoalExists->contact_id, date('Y-01-01'), date('Y-m-d'), 'leadgen'),
                                 'ytdPercent'=> ($kpiLeadgenGoal) ? round($kpiLeadgenActual / $kpiLeadgenGoal * 100) : 0,
                             ),
                        );

            $goalClosings = $thisYearGoalExists->annualClosings;
            $goalContracts =  $thisYearGoalExists->annualContracts;
            $goalAgreements =  $thisYearGoalExists->annualAgreements;
            $goalMetAppointments =  $thisYearGoalExists->annualAppointments;
            $goalSetAppointments =  round($thisYearGoalExists->annualAppointments/$thisYearGoalExists->appointments_set_met_conversion);
            $goalContacts =  $thisYearGoalExists->annualContacts;
            $goalLeadGen =  $thisYearGoalExists->annualLeadGen;
            $goalContractCloseConversion = $thisYearGoalExists->contracts_closing_conversion * 100;
            $goalSignedContractConversion = $thisYearGoalExists->agreements_contract_conversion * 100;
            $goalMetSignedConversion = $thisYearGoalExists->appointments_agreement_conversion * 100;
            $goalSetMetConversion = $thisYearGoalExists->appointments_set_met_conversion * 100;
            $goalContactsApptConversion = $thisYearGoalExists->contacts_per_appointment;

            $weeklySumClosings = GoalsWeekly::model()->annualGoalByField($thisYearGoalExists->id, 'closings');
            $weeklySumContracts =  GoalsWeekly::model()->annualGoalByField($thisYearGoalExists->id, 'contracts');
            $weeklySumAgreements =  GoalsWeekly::model()->annualGoalByField($thisYearGoalExists->id, 'signed');
            $weeklySumMetAppointments =  GoalsWeekly::model()->annualGoalByField($thisYearGoalExists->id, 'appointments_met');
            $weeklySumSetAppointments =  GoalsWeekly::model()->annualGoalByField($thisYearGoalExists->id, 'appointments_set');
            $weeklySumContacts =  GoalsWeekly::model()->annualGoalByField($thisYearGoalExists->id, 'contacts');
            $weeklySumLeadGen =  GoalsWeekly::model()->annualGoalByField($thisYearGoalExists->id, 'lead_gen_hours');

            $actualContractCloseConversion = ($kpiData['contracts']['ytdActual'])? round(($kpiData['closings']['ytdActual'] / $kpiData['contracts']['ytdActual']) *100) : 0;
            $actualSignedContractConversion = ($kpiData['signed']['ytdActual'])? round(($kpiData['contracts']['ytdActual'] / $kpiData['signed']['ytdActual']) *100) : 0;
            $actualMetSignedConversion = ($kpiData['met']['ytdActual'])? round(($kpiData['signed']['ytdActual'] / $kpiData['met']['ytdActual']) *100) : 0;
            $actualSetMetConversion = ($kpiData['set']['ytdActual'])? round(($kpiData['met']['ytdActual'] / $kpiData['set']['ytdActual']) *100) : 0;
            $actualContactsApptConversion = ($kpiData['set']['ytdActual'])? round(($kpiData['contacts']['ytdActual'] / $kpiData['set']['ytdActual'])) : 0;
        }

        $chartData = $this->populateChartData($model, $thisYearGoalExists, $ytdGoalPercent);

        $this->controller->render('listOneThing',array(
			'model'=>$model,
            'thisYearGoalExists'=>$thisYearGoalExists,
            'thisYearWeeklyGoalExists'=>$chartData['thisYearWeeklyGoalExists'],
            'xLabels' => $chartData['xLabels'],
            'goalData' => $chartData['goalData'],
            'dailyPerformanceData' => $chartData['dailyPerformanceData'],
            'oneThingDailyMissingDaysString' => $chartData['oneThingDailyMissingDaysString'],
            'monthlyPerformanceData' => $chartData['monthlyPerformanceData'],
            'monthlyOneThingPerformanceData' => $chartData['monthlyOneThingPerformanceData'],
            'yearlyOneThingPerformanceData' => $chartData['yearlyOneThingPerformanceData'],
            'oneThingType' => $chartData['oneThingType'],
            'mtdGoalResults' => $mtdGoalResults,
            'mtdGoalNumber' => $mtdGoalNumber,
            'mtdGoalPercent' => $mtdGoalPercent,
            'ytdGoalResults' => $ytdGoalResults,
            'ytdGoalNumber' => $ytdGoalNumber,
            'ytdGoalPercent' => $ytdGoalPercent,
            'oneThingUnit' => $oneThingUnit,
            'oneThingQuantity' => $oneThingQuantity,
            'kpiData' => $kpiData,
            'goal' =>$thisYearGoalExists,

            'goalClosings' => $goalClosings,
            'goalContracts' => $goalContracts,
            'goalAgreements' => $goalAgreements,
            'goalMetAppointments' => $goalMetAppointments,
            'goalSetAppointments' => $goalSetAppointments,
            'goalContacts' => $goalContacts,
            'goalLeadGen' => $goalLeadGen,
            'goalContractCloseConversion' => $goalContractCloseConversion,
            'goalSignedContractConversion' => $goalSignedContractConversion,
            'goalMetSignedConversion' => $goalMetSignedConversion,
            'goalSetMetConversion' => $goalSetMetConversion,
            'goalContactsApptConversion' => $goalContactsApptConversion,

            'weeklySumClosings' => $weeklySumClosings,
            'weeklySumContracts' => $weeklySumContracts,
            'weeklySumAgreements' => $weeklySumAgreements,
            'weeklySumMetAppointments' => $weeklySumMetAppointments,
            'weeklySumSetAppointments' => $weeklySumSetAppointments,
            'weeklySumContacts' => $weeklySumContacts,
            'weeklySumLeadGen' => $weeklySumLeadGen,

            'actualContractCloseConversion' => $actualContractCloseConversion,
            'actualSignedContractConversion' => $actualSignedContractConversion,
            'actualMetSignedConversion' => $actualMetSignedConversion,
            'actualSetMetConversion' => $actualSetMetConversion,
            'actualContactsApptConversion' => $actualContactsApptConversion,
		));
	}

    protected function populateChartData(GoalsOneThing $model, $goal, $ytdGoalPercent)
    {
        // check to see if this year's goal has one thing data weekly data
        if($goal && $thisYearWeeklyGoalExists = (boolean) GoalsWeekly::model()->count(array('condition'=>'goal_id='.$goal->id))) {

            $monthlyGoalCount = GoalsWeekly::getGoalNumber($goal->id, $goal->contact_id, date('Y-m-01'), date('Y-m-d'));
            $oneThingType = GoalsWeekly::getOneThingType($goal->id);
        }

        //sanity check
        if($monthlyGoalCount) {
            // x-axis labels
            $numDays = date('t');
            $xLabels = '';
            for($i=1; $i<=$numDays; $i++) {
                $xLabels .= ($xLabels)? ','.$i : $i;
            }
            $xLabels = '['.$xLabels.']';

            $goalData = '';
            for($i=0; $i<$numDays; $i++) {
                $goalData .= ($goalData)? ',' : '';
                $goalData .= '['.$i.',100]';
            }
            $goalData = '['.$goalData.']';


            $criteria = new CDbCriteria();
            $criteria->addCondition('contact_id='.$model->contact_id);
            $criteria->addCondition('date >="'.date('Y-m-01').'"');
            $criteria->addCondition('date <="'.date('Y-m-d').'"');
            $oneThingResults = $model->findAll($criteria);

            // build array of data one thing results: date => array(daily=>percentValue, weekly=>percentValue)
            $resultsData = array();

            //@todo: need to replace this will auto-populated from appointments records and into restulsData array for daily/weekly/monthly cumulatives then need to calculate to percentage somehow or in later for loop?
            foreach($oneThingResults as $oneThing) {
                $date = substr($oneThing->date, 8, 2);
                $resultsData[number_format($date)] = array('daily'=>$oneThing->percent_daily_goal_accomplished); //'weekly'=>$oneThing->percent_weekly_goal_accomplished, 'monthly'=>$oneThing->percent_monthly_goal_accomplished
            }

            // calculates appointments set (as it is the fixed unit for goals right now)

            // appointments set data
            $appointmentsCountbyDate = Yii::app()->db->createCommand()
                //->select('*') //'set_on_datetime as `date`, count(id) as count'
                ->select('DATE(set_on_datetime) as `date`, count(id) as count')
                ->from('appointments')
                ->where('set_by_id='.$model->contact_id)
                ->andWhere('set_on_datetime >="'.date('Y-m-01').'"')
                ->andWhere('set_on_datetime <="'.date('Y-m-d').'"')
                ->group('DATE(set_on_datetime)')
                ->queryAll();

            foreach($appointmentsCountbyDate as $appointmentsCount) {
                $date = substr($appointmentsCount['date'], 8, 2);
                $monthlyGoalCountPercent = ceil(($appointmentsCount['count'] / $monthlyGoalCount) * 100);
                $resultsData[number_format($date)]['monthly'] = $monthlyGoalCountPercent;
            }

            switch($oneThingType) {
                case GoalsWeekly::ONE_THING_TYPE_SIGNED:
                    $oneThingTable = 'appointments';
                    $dateField = 'signed_date';
                    $contactIdField = 'met_by_id';
                    $goalsWeeklyUnit = 'signed';
                    break;

                case GoalsWeekly::ONE_THING_TYPE_SELLER_SIGNED:
                    $oneThingTable = 'appointments';
                    $dateField = 'signed_date';
                    $contactIdField = 'met_by_id';
                    $goalsWeeklyUnit = 'signed';
                    $queryCondition = 'component_type_id='.ComponentTypes::SELLERS;
                    break;

                case GoalsWeekly::ONE_THING_TYPE_BUYER_SIGNED:
                    $oneThingTable = 'appointments';
                    $dateField = 'signed_date';
                    $contactIdField = 'met_by_id';
                    $goalsWeeklyUnit = 'signed';
                    $queryCondition = 'component_type_id='.ComponentTypes::BUYERS;
                    break;

                case GoalsWeekly::ONE_THING_TYPE_APPOINTMENT_SET:
                    $oneThingTable = 'appointments';
                    $dateField = 'set_on_datetime';
                    $contactIdField = 'set_by_id';
                    $goalsWeeklyUnit = 'appointments_set';
                    break;

                case GoalsWeekly::ONE_THING_TYPE_SELLER_APPOINTMENT_SET:
                    $oneThingTable = 'appointments';
                    $dateField = 'set_on_datetime';
                    $contactIdField = 'set_by_id';
                    $goalsWeeklyUnit = 'appointments_set';
                    $queryCondition = 'component_type_id='.ComponentTypes::SELLERS;
                    break;

                case GoalsWeekly::ONE_THING_TYPE_BUYER_APPOINTMENT_SET:
                    $oneThingTable = 'appointments';
                    $dateField = 'set_on_datetime';
                    $contactIdField = 'set_by_id';
                    $goalsWeeklyUnit = 'appointments_set';
                    $queryCondition = 'component_type_id='.ComponentTypes::BUYERS;
                    break;

                case GoalsWeekly::ONE_THING_TYPE_APPOINTMENT_MET:
                    $oneThingTable = 'appointments';
                    $dateField = 'set_for_datetime';
                    $contactIdField = 'met_by_id';
                    $goalsWeeklyUnit = 'appointments_met';
                    $queryCondition = 'met_status_ma='.Appointments::MET_STATUS_SET;
                    break;

                case GoalsWeekly::ONE_THING_TYPE_SELLER_APPOINTMENT_MET:
                    $oneThingTable = 'appointments';
                    $dateField = 'set_for_datetime';
                    $contactIdField = 'met_by_id';
                    $goalsWeeklyUnit = 'appointments_met';
                    $queryCondition = 'component_type_id='.ComponentTypes::SELLERS.' AND met_status_ma='.Appointments::MET_STATUS_SET;
                    break;

                case GoalsWeekly::ONE_THING_TYPE_BUYER_APPOINTMENT_MET:
                    $oneThingTable = 'appointments';
                    $dateField = 'set_for_datetime';
                    $contactIdField = 'met_by_id';
                    $goalsWeeklyUnit = 'appointments_met';
                    $queryCondition = 'component_type_id='.ComponentTypes::BUYERS.' AND met_status_ma='.Appointments::MET_STATUS_SET;
                    break;

                default:
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Goals Weekly type invalid/unknown.');
                    break;
            }

            // appointments SIGNED data
            //@todo: need to consolidate the 2 queries
            $oneThingCountQuery = Yii::app()->db->createCommand()
                //->select('*') //'set_on_datetime as `date`, count(id) as count'
                ->select('DATE('.$dateField.') as `date`, count(id) as count')
                ->from($oneThingTable)
                ->where($contactIdField.'='.$model->contact_id)
                ->andWhere($dateField.' >="'.date('Y-m-01').'"')
                ->andWhere($dateField.' <="'.date('Y-m-d').'"')
                ->group('DATE('.$dateField.')');

            if($queryCondition) {
                $oneThingCountQuery->andWhere($queryCondition);
            }

            $oneThingCountbyDate = $oneThingCountQuery->queryAll();

            foreach($oneThingCountbyDate as $oneThingCount) {
                $date = substr($oneThingCount['date'], 8, 2);
                $monthlyOneThingGoalCount = GoalsWeekly::getGoalNumber($goal->id, $model->contact_id, date('Y-m-01'),  date('Y-m-d'), $goalsWeeklyUnit);
                $monthlyOneThingGoalCountPercent = ceil(($oneThingCount['count'] / $monthlyOneThingGoalCount) * 100);
                $resultsData[number_format($date)]['monthlyOneThing'] = $monthlyOneThingGoalCountPercent;
            }

            $performanceData = '';
            $cumulativeMonthlyPerformance = 0;
            $cumulativeOneThingMonthlyPerformance = 0;
            $todayDate = date('d');
            for($i=0; $i<$numDays; $i++) {

                // add comma delimiter to string if not blank
                $dailyPerformanceData .= ($dailyPerformanceData)? ',' : '';
                $monthlyPerformanceData .= ($monthlyPerformanceData)? ',' : '';
                $monthlyOneThingPerformanceData .= ($monthlyOneThingPerformanceData)? ',' : '';
                $yearlyOneThingPerformanceData .= ($yearlyOneThingPerformanceData)? ',' : '';

                $dayData = ($i>=$todayDate)? '' : ((isset($resultsData[$i+1]))? $resultsData[$i+1]['daily']: 0);
                $monthData = ($i>=$todayDate)? '' : ((isset($resultsData[$i+1]))? $resultsData[$i+1]['monthly']: 0);
                $monthOneThingData = ($i>=$todayDate)? '' : ((isset($resultsData[$i+1]))? $resultsData[$i+1]['monthlyOneThing']: 0);

                // store this # for monthly so that on a "blank" day the monthly progress continues and not drop to zero.
                $cumulativeMonthlyPerformance += (($monthData !== '') &&  $monthData > 0) ? $monthData : 0;
                $cumulativeOneThingMonthlyPerformance += (($monthOneThingData !== '') &&  $monthOneThingData > 0) ? $monthOneThingData : 0;

                $monthData = ($i<$todayDate) ? $cumulativeMonthlyPerformance : '';
                $monthOneThingData = ($i<$todayDate) ? $cumulativeOneThingMonthlyPerformance : '';

                $dailyPerformanceData .= '['.$i.','.$dayData.']';
                $monthlyPerformanceData .= '['.$i.','.$monthData.']';
                $monthlyOneThingPerformanceData .= '['.$i.','.$monthOneThingData.']';
                $yearlyOneThingPerformanceData .= '['.$i.','.$ytdGoalPercent.']';
            }
        }
        else {
            $thisYearWeeklyGoalExists = false;
        }

        // wrap the json string data in appropriate brackets
        $dailyPerformanceData = '['.$dailyPerformanceData.']';
        $monthlyPerformanceData = '['.$monthlyPerformanceData.']';
        $monthlyOneThingPerformanceData = '['.$monthlyOneThingPerformanceData.']';
        $yearlyOneThingPerformanceData = '['.$yearlyOneThingPerformanceData.']';


        // get array of days missing
        $oneThingDailyMissingDates = GoalsOneThing::getMissingDays($goal->contact_id, date('Y-m-01'), date('Y-m-d'));
        if($oneThingDailyMissingDates !== false) {
            // processing missing days string and format for print
            $currentMonthName = '';
            $oneThingDailyMissingDaysString = '';

            foreach($oneThingDailyMissingDates as $oneThingDailyMissingDate) {

                $monthName = date('F', strtotime($oneThingDailyMissingDate));

                // if the month changes, append day number only with comma
                if($currentMonthName == $monthName) {
                    $oneThingDailyMissingDaysString .= ', '.date('j', strtotime($oneThingDailyMissingDate));
                }
                // if the month changes, append the month name and line break
                else {
                    $currentMonthName = $monthName;
                    $oneThingDailyMissingDaysString .= '<br />'.date('F', strtotime($oneThingDailyMissingDate)).': '.date('j', strtotime($oneThingDailyMissingDate));
                }
            }
        }

        $chartData = array(
            'thisYearWeeklyGoalExists'=>$thisYearWeeklyGoalExists,
            'xLabels' => $xLabels,
            'goalData' => $goalData,
            'dailyPerformanceData' => $dailyPerformanceData,
            'oneThingDailyMissingDaysString' => $oneThingDailyMissingDaysString,
            'monthlyPerformanceData' => $monthlyPerformanceData,
            'monthlyOneThingPerformanceData' => $monthlyOneThingPerformanceData,
            'yearlyOneThingPerformanceData' => $yearlyOneThingPerformanceData,
            'oneThingType' => $oneThingType,
        );

        return $chartData;
    }
}