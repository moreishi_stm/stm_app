<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class DashboardAction extends CAction
{
	public function run()
	{
		$this->controller->renderPartial('_dashboard');
	}
}