<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction
{
	public function run()
	{
		$this->controller->title = 'Add Goals';
		$model = $this->controller->baseModel;
        $model->contact_id = Yii::app()->user->id;

		if (isset($_POST['Goals'])) {
			$model->attributes = $_POST['Goals'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully added goals!');
				$this->controller->redirect(array('index'));
			}
		}

		$this->controller->render('form', array(
			'model'=>$model,
		));
	}
}