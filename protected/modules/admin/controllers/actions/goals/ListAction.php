<?php

class ListAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'Goals';
		$model=new Goals('search');
		$model->unsetAttributes();  // clear any default values
		$model->contact_id = Yii::app()->user->id;

		// used for gridview search box inputs
		if ((isset($_GET['Goals']) || $_GET['id']) && Yii::app()->user->checkAccess('owner')) {

            // populate contact_id from get url
            if($_GET['id']) {
                $model->contact_id = $_GET['id'];
            }

            // override url with model get data from list search box form
            $model->attributes=$_GET['Goals'];
        }

		$this->controller->render('list',array(
			'model'=>$model,
		));
	}
}