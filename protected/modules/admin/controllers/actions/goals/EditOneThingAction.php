<?php

class EditOneThingAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run($id)
	{
		$this->controller->title = 'Edit One Thing';
		$model= GoalsOneThing::model()->findByPk($id);

		// used for gridview search box inputs
		if (isset($_POST['GoalsOneThing'])) {

            $model->attributes=$_POST['GoalsOneThing'];
            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated One Thing.');
                $this->controller->redirect('/'.Yii::app()->controller->module->name.'/goals/oneThing');
            }
        }

		$this->controller->render('formOneThing',array(
			'model'=>$model,
		));
	}
}