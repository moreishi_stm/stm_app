<?php

class AddOneThingAction extends CAction
{
	/**
	 * Manages models
	 */
	public function run()
	{
		$this->controller->title = 'Add One Thing';
		$model=new GoalsOneThing;
//		$model->unsetAttributes();  // clear any default values
//		$model->contact_id = Yii::app()->user->id;

		// used for gridview search box inputs
		if (isset($_POST['GoalsOneThing'])) {

            $model->attributes=$_POST['GoalsOneThing'];
            $model->contact_id = Yii::app()->user->id;

            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added One Thing.');
                $this->controller->redirect('/'.Yii::app()->controller->module->name.'/goals/oneThing');
            }
        }

		$this->controller->render('formOneThing',array(
			'model'=>$model,
		));
	}
}