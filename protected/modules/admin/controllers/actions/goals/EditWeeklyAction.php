<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class EditWeeklyAction extends CAction
{
    public function run($id)
    {
        $this->controller->title = 'Edit Weekly Goal';
        $model = new GoalsWeekly();
        $goal = Goals::model()->findByPk($id);
        $leadGenWeeks = 52-$goal->weeks_time_off;

        $goalClosings = $goal->annualClosings;
        $goalContracts =  $goal->annualContracts;
        $goalAgreements =  $goal->annualAgreements;
        $goalMetAppointments =  $goal->annualAppointments;
        $goalSetAppointments =  round($goal->annualAppointments/$goal->appointments_set_met_conversion);
        $goalContacts =  $goal->annualContacts;
        $goalLeadGen =  $goal->annualLeadGen;
        $goalContractCloseConversion = $goal->contracts_closing_conversion * 100;
        $goalSignedContractConversion = $goal->agreements_contract_conversion * 100;
        $goalMetSignedConversion = $goal->appointments_agreement_conversion * 100;
        $goalSetMetConversion = $goal->appointments_set_met_conversion * 100;
        $goalContactsApptConversion = $goal->contacts_per_appointment * 100;

        $collection = $model->findAll(array('condition'=>'goal_id='.$id,'order'=>'week_number ASC'));

        foreach($collection as $i => $singleModel) {
            $model->collection[$i] = $singleModel;
        }

        if (isset($_POST['GoalsWeekly'])) {
            $model->attributes = $_POST['GoalsWeekly'];
            $error = false;
            foreach($model->collection as $i => $weekData) {

                $weeklyGoal = $model->find(array('condition'=>'id='.$weekData['id']));;
                $weeklyGoal->attributes = $weekData;
                if (!$weeklyGoal->save()) {
                    // @todo: error message

                    $error = true;
                }
                $model->collection[$i] = $weeklyGoal;
            }

            if($error) {
                Yii::app()->user->setFlash('error', 'Error updating Weekly Goal.');
            }
            else {
                Yii::app()->user->setFlash('success', 'Successfully update Weekly Goals.');
//                $this->controller->redirect('/admin/goals');
            }
        }

        $weeklySumClosings = $model->annualGoalByField($goal->id, 'closings');
        $weeklySumContracts =  $model->annualGoalByField($goal->id, 'contracts');
        $weeklySumAgreements =  $model->annualGoalByField($goal->id, 'signed');
        $weeklySumMetAppointments =  $model->annualGoalByField($goal->id, 'appointments_met');
        $weeklySumSetAppointments =  $model->annualGoalByField($goal->id, 'appointments_set');
        $weeklySumContacts =  $model->annualGoalByField($goal->id, 'contacts');
        $weeklySumLeadGen =  $model->annualGoalByField($goal->id, 'lead_gen_hours');

        $weeklyContractCloseConversion = ($weeklySumContracts > 0)? round(($weeklySumClosings / $weeklySumContracts) *100) : 0;
        $weeklySignedContractConversion = ($weeklySumAgreements > 0)? round(($weeklySumContracts / $weeklySumAgreements) *100) : 0;
        $weeklyMetSignedConversion = ($weeklySumMetAppointments > 0)? round(($weeklySumAgreements / $weeklySumMetAppointments) *100) : 0;
        $weeklySetMetConversion = ($weeklySumSetAppointments > 0)? round(($weeklySumMetAppointments / $weeklySumSetAppointments) *100) : 0;
        $weeklyContactsApptConversion = ($weeklySumContacts > 0)? round(($weeklySumSetAppointments / $weeklySumContacts) *100) : 0;

        $this->controller->render('formWeekly', array(
            'model'=>$model,
            'goal' => $goal,
            'leadGenWeeks' => $leadGenWeeks,

            'goalClosings' => $goalClosings,
            'goalContracts' => $goalContracts,
            'goalAgreements' => $goalAgreements,
            'goalMetAppointments' => $goalMetAppointments,
            'goalSetAppointments' => $goalSetAppointments,
            'goalContacts' => $goalContacts,
            'goalLeadGen' => $goalLeadGen,

            'goalContractCloseConversion' => $goalContractCloseConversion,
            'goalSignedContractConversion' => $goalSignedContractConversion,
            'goalMetSignedConversion' => $goalMetSignedConversion,
            'goalSetMetConversion' => $goalSetMetConversion,
            'goalContactsApptConversion' => $goalContactsApptConversion,

            'weeklySumClosings' => $weeklySumClosings,
            'weeklySumContracts' => $weeklySumContracts,
            'weeklySumAgreements' => $weeklySumAgreements,
            'weeklySumMetAppointments' => $weeklySumMetAppointments,
            'weeklySumSetAppointments' => $weeklySumSetAppointments,
            'weeklySumContacts' => $weeklySumContacts,
            'weeklySumLeadGen' => $weeklySumLeadGen,

            'weeklyContractCloseConversion' => $weeklyContractCloseConversion,
            'weeklySignedContractConversion' => $weeklySignedContractConversion,
            'weeklyMetSignedConversion' => $weeklyMetSignedConversion,
            'weeklySetMetConversion' => $weeklySetMetConversion,
            'weeklyContactsApptConversion' => $weeklyContactsApptConversion,
        ));
    }
}