<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class EditAction extends CAction
{
	public function run($id)
	{
		$model = $this->controller->loadModel($id);

		if($model->start_date)
			$model->start_date = Yii::app()->format->formatDate($model->start_date, StmFormatter::APP_DATE_PICKER_FORMAT);

		if($model->end_date)
			$model->end_date = Yii::app()->format->formatDate($model->end_date, StmFormatter::APP_DATE_PICKER_FORMAT);

		$this->controller->title = 'Edit Goals';

		if (isset($_POST['Goals'])) {
			$model->attributes = $_POST['Goals'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully updated goals.');
			}
            else {
                $model->start_date = $_POST['Goals']['start_date'];
                $model->end_date = $_POST['Goals']['end_date'];
            }
		}

		$this->controller->render('form', array(
			'model'=>$model,
		));
	}
}