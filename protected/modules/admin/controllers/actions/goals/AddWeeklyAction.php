<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddWeeklyAction extends CAction
{
    public function run($id)
    {
        $this->controller->title = 'Add Weekly Goal';
        $model = new GoalsWeekly();
        $goal = Goals::model()->findByPk($id);

        if($goal->hasWeeklyGoals) {
            Yii::app()->user->setFlash('warning', 'Goal already has One Things. Please see below.');
            $this->controller->redirect('/admin/goals');
        }

        if (isset($_POST['GoalsWeekly'])) {
            $model->attributes = $_POST['GoalsWeekly'];
            $error = false;
            foreach($model->collection as $i => $singleWeekGoal) {

                $weeklyGoal = new GoalsWeekly;
                $weeklyGoal->attributes = $singleWeekGoal;
                $weeklyGoal->goal_id = $id;
                if (!$weeklyGoal->save()) {
                    // @todo: error message

                    $error = true;
                }
                $model->collection[$i] = $weeklyGoal;
            }

            if($error) {
                Yii::app()->user->setFlash('error', 'Error updating Weekly Goal.');
            }
            else {
                 Yii::app()->user->setFlash('success', 'Successfully added Weekly Goal.');
                $this->controller->redirect('/admin/goals');
            }
        }
        else {
            for($i=1; $i<=date("W", strtotime(date($goal->year.'-12-31'))); $i++) {

                $weeklyGoal = new GoalsWeekly();
                $weeklyGoal->goal_id = $id;
                $weeklyGoal->week_number = $i;
                $weeklyGoal->working_days = 5;

                //add default figures based on goals
                //$weeklyGoal->w

                $model->collection[$i] = $weeklyGoal;
            }
        }

        $this->controller->render('formWeekly', array(
                'model'=>$model,
                'goal' => $goal,
            ));
    }
}