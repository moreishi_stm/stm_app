<?php
/**
 * Verify SMS Action
 */
class VerifySmsAction extends CAction
{
    /**
     * Manages models
     */
    public function run() {
        if(!isset($_POST['contactId']) || empty($_POST['contactId'])) {
            echo CJSON::encode(array('error' => 'Invalid Contact ID.'));
            Yii::app()->end();
        }

        $contact = Contacts::model()->findByPk($_POST['contactId']);
        return $this->performAjaxRequest($contact);
    }

    /**
     * Perform AJAX Request
     *
     * @param Contacts $contact
     */
    protected function performAjaxRequest(Contacts $contact) {

        if (Yii::app()->request->isAjaxRequest) {

            // Send test text message
            //if (!YII_DEBUG) {
            $id = $contact->id;
            if(!$id) {
                echo CJSON::encode(array('error' => 'Verification could not be sent. Contact ID not found.'));
                Yii::app()->end();
            }

            $model = $this->controller->baseModel;
            $model->type = 'contact';
            $models = $model->contactType()->findAll();

            if(!isset($_POST['cell_carrier']) || empty($_POST['cell_carrier'])) {
                echo CJSON::encode(array('error' => 'Verification could not be sent. Could not save new Carrier. Please try again.'));
                Yii::app()->end();
            }

            if(!isset($_POST['cell_number']) || empty($_POST['cell_number'])) {
                echo CJSON::encode(array('error' => 'Verification could not be sent. Could not save new Cell Number. Please try again.'));
                Yii::app()->end();
            }

            $SettingContactValues = new SettingContactValues;
            foreach ($models as $Setting) {
                $SettingContactValue = $Setting->getSettingContactValue($id);
                $SettingContactValues->data[$SettingContactValue->setting_id] = $SettingContactValue->value;
            }

            $cell_carrier = (int)$_POST['cell_carrier'];
            if(isset($SettingContactValues->data[$model->getField('cell_carrier_id')->id])) {

                $Criteria = new CDbCriteria;
                $Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
                $Criteria->params = array(
                    ':contact_id' => $contact->id,
                    ':setting_id' => $model->getField('cell_carrier_id')->id,
                );

                $SettingContactValue = SettingContactValues::model()->find($Criteria);
            } else {
                $SettingContactValue = new SettingContactValues;
            }

            $SettingContactValue->contact_id = $contact->id;
            $SettingContactValue->setting_id = $model->getField('cell_carrier_id')->id;
            $SettingContactValue->value = $cell_carrier;

            if(!$SettingContactValue->save()) {
                echo CJSON::encode(array('error' => 'Verification could not be sent. Could not save new Carrier. Please try again.'));
                Yii::app()->end();
            }

            $cell_number = preg_replace('/[^0-9]/', '', $_POST['cell_number']);
            if(isset($SettingContactValues->data[$model->getField('cell_number')->id])) {

                $Criteria = new CDbCriteria;
                $Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
                $Criteria->params = array(
                    ':contact_id' => $contact->id,
                    ':setting_id' => $model->getField('cell_number')->id,
                );

                $SettingContactValue = SettingContactValues::model()->find($Criteria);
            } else {
                $SettingContactValue = new SettingContactValues;
            }

            $SettingContactValue->contact_id = $contact->id;
            $SettingContactValue->setting_id = $model->getField('cell_number')->id;
            $SettingContactValue->value = $cell_number;

            if(!$SettingContactValue->save()) {
                echo CJSON::encode(array('error' => 'Verification could not be sent. Could not save new Call Number. Please try again.'));
                Yii::app()->end();
            }

            if(isset($SettingContactValues->data[$model->getField('sms_verification_code')->id])) {

                $Criteria = new CDbCriteria;
                $Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
                $Criteria->params = array(
                    ':contact_id' => $contact->id,
                    ':setting_id' => $model->getField('sms_verification_code')->id,
                );

                if(!($SettingContactValue = SettingContactValues::model()->find($Criteria))) {
                    $SettingContactValue = new SettingContactValues;
                }
            } else {
                $SettingContactValue = new SettingContactValues;
            }

            $newVerificationCode = rand(100000, 999999);
            $SettingContactValue->contact_id = $contact->id;
            $SettingContactValue->setting_id = $model->getField('sms_verification_code')->id;
            $SettingContactValue->value = $newVerificationCode;

            if(!$SettingContactValue->save()) {
                echo CJSON::encode(array('error' => 'Verification could not be sent. Please try again.'));
                Yii::app()->end();
            }

            if(isset($SettingContactValues->data[$model->getField('sms_verified')->id])) {
                $Criteria = new CDbCriteria;
                $Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
                $Criteria->params = array(
                    ':contact_id' => $contact->id,
                    ':setting_id' => $model->getField('sms_verified')->id,
                );

                if(!($SettingContactValue = SettingContactValues::model()->find($Criteria))) {
                    $SettingContactValue = new SettingContactValues;
                }
            } else {
                $SettingContactValue = new SettingContactValues;
            }

            $SettingContactValue->contact_id = $contact->id;
            $SettingContactValue->setting_id = $model->getField('sms_verified')->id;
            $SettingContactValue->value = 0;
            if(!$SettingContactValue->save()) {
                echo CJSON::encode(array('error' => 'Verification could not be sent. Please try again.'));
                Yii::app()->end();
            }

            $cellNumber = $contact->settings->cell_number;
            $stmOutboundSmsNumber = '19048531990';
//            $response = Yii::app()->plivo->sendMessage($cellNumber, 'test', '1'.DialerController::FROM_NUMBER);
            $response = Yii::app()->plivo->sendMessage($cellNumber, 'Seize The Market SMS Verification Code: '.$newVerificationCode.'. Please enter this code into the SMS Verification Code field.', $stmOutboundSmsNumber); //, StmFunctions::getSiteUrl().'/plivo/handleSmsSentCallback'
//            if(Yii::app()->user->clientId==1) {
//                $response = Yii::app()->plivo->sendMessage($cellNumber, 'Seize The Market SMS Verification Code: '.$newVerificationCode.'. Please enter this code into the SMS Verification Code field.','19046384718'); //, StmFunctions::getSiteUrl().'/plivo/handleSmsSentCallback'
                $response2 = Yii::app()->plivo->sendMessage('9043433200', 'CLee Copy for '.$cellNumber.': Seize The Market SMS Verification Code: '.$newVerificationCode.'. Please enter this code into the SMS Verification Code field.');
//            }

			#die("<pre>".print_r($response, 1));

			if(isset($response['response']['error'])) {
				Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error sending SMS.'.PHP_EOL.'response: : '.print_r($response, true), CLogger::LEVEL_ERROR);
				echo CJSON::encode(array('error' => 'There was an error sending the message. Please try again.'));
			}
			Yii::app()->end();
        }
    }
}