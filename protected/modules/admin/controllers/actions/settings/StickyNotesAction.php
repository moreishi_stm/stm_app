<?php

class StickyNotesAction extends CAction
{
	public function run()
	{
		$Criteria = new CDbCriteria;
		$Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
		$Criteria->params    = array(
			':contact_id' => Yii::app()->user->id,
			':setting_id' => Settings::SETTING_STICKY_NOTES_ID,
		);
		$SettingContactValue = SettingContactValues::model()->find($Criteria);

		if (!$SettingContactValue) {
			$SettingContactValue = new SettingContactValues;
			$SettingContactValue->contact_id = Yii::app()->user->id;
			$SettingContactValue->setting_id = Settings::SETTING_STICKY_NOTES_ID;
		}

		$SettingContactValue->value = Yii::app()->request->getPost('stickyNotes');;

		if ($SettingContactValue->save())
			echo true;
		else
			echo false;
	}
}
