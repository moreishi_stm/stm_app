<?php

	class UsersAddAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Add New User';


			if(Yii::app()->user->checkAccess('owner')) {
                $model = new Contacts('search');
                $model->unsetAttributes(); // clear any default values

                if (isset($_GET['Contacts'])) {
                    $model->attributes = $_GET['Contacts'];
                } else {
                    $model->id = -1;
                }

                $model->emails = new Emails('search');
                if (isset($_GET['Emails'])) {
                    $model->emails->attributes = $_GET['Emails'];
                }

                $DataProvider = $this->getDataProvider($model);

                if(isset($_POST['Contacts'])) {
					$model->attributes = $_POST['Contacts'];

                    if(AuthAssignment::model()->byIn('userid', $_POST['Contacts']['id'])->find()) {
                        Yii::app()->user->setFlash('error', 'This contact is already a user.');
                        $this->controller->redirect('/'.Yii::app()->controller->module->name.'/settings/'.$model->id);

                    } else {
                        $authAssignment = new AuthAssignment;
                        $authAssignment->account_id = Yii::app()->user->accountId;
                        $authAssignment->userid = $model->id;

                        if(Yii::app()->controller->module->id == 'admin') {
                            $authAssignment->itemname = 'agent';
                        } elseif(Yii::app()->controller->module->id == 'hq') {
                            $authAssignment->itemname = 'sales';
                        }

                        if($authAssignment->save()) {
                            Yii::app()->user->setFlash('success', 'Successfully added New User.');
                            $this->controller->redirect('/'.Yii::app()->controller->module->name.'/settings/'.$authAssignment->userid);
                        }
					}
				}
                $isMaxUsers = Yii::app()->user->isMaxUsers();
				$this->controller->render('formUsersAdd', array('model' => $model, 'DataProvider'=>$DataProvider,'isMaxUsers'=>$isMaxUsers));
			} else {
				$this->controller->render('/main/accessDenied', array());
				return false;
			}
		}

        protected function getDataProvider(Contacts $Contact) {
            if(Yii::app()->user->isMaxUsers()) {
                $Criteria = $Contact->byAdmins(true, null, 'allActive')->dbCriteria;
                $DataProvider = new CActiveDataProvider($Contact, array('criteria' => $Criteria));
            } else {
                $DataProvider = $Contact->search();
            }
            $sort = new CSort();
            $sort->defaultOrder = 't.added DESC';
            $sort->attributes = array(
                'first_name',
                'last_name',
                'primaryEmail'=>array(
                    'asc'=>'emails.email ASC',
                    'desc'=>'emails.email DESC',
                ),
            );

            $DataProvider->sort = $sort;
            return $DataProvider;
        }

        protected function printUserButton(Contacts $Contact) {
            if($authAssignment = AuthAssignment::model()->byIn('userid', $Contact->id)->find()) {
                $button = CHtml::link('Edit User', '/'.Yii::app()->controller->module->id.'/settings/'.$Contact->id, $htmlOptions=array('target'=>'_blank','class'=>'button gray icon i_stm_edit grey-button'));
            } else {
                $button = '<form method="post" action="/'.Yii::app()->controller->module->id.'/settings/usersAdd" id="addUserForm-'.$Contact->id.'" target="_blank">';
                $button .= CHtml::hiddenField('Contacts[id]', $Contact->id, $htmlOptions=array());
                $button .= CHtml::link('Add User', 'javascript:void(0)', $htmlOptions=array('class'=>'button gray icon i_stm_add grey-button addNewUser','data-id'=>$Contact->id, 'data-name'=>$Contact->fullName));
                $button .= '</form>';
            }
            return $button;
        }
	}
