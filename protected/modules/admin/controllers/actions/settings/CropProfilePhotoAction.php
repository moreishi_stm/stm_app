<?php

class CropProfilePhotoAction extends CAction {

    public function run()
    {
        $contactId = $_POST['contactId'];
        $contact = Contacts::model()->findByPk($contactId);

        Yii::import('admin_exts.jcrop.EJCropper');
        $jcropper = new EJCropper();
        $jcropper->thumbPath = Yii::app()->user->getProfileImageBaseFilepath().DS.$contact->id;

        $jcropper->jpeg_quality = 95;
        $jcropper->png_compression = 8;

        // get the image cropping coordinates
        $coords = $jcropper->getCoordsFromPost('profilePhoto');

        // returns the path of the cropped image, source must be an absolute path.
        $thumbnail = $jcropper->crop($contact->getProfilePhotoPath(), $coords);

        echo CJSON::encode(array(
            'success' => true,
        ));
    }
}