<?php

class SaveAccountLogoAction extends CAction
{
    public function run()
    {
        // If we don't have a file upload
        if(!isset($_FILES['photo_file'])) {
            throw new Exception('Error, no file upload found!');
        }

        // Make sure we don't have any funny business here
        if(!is_uploaded_file($_FILES['photo_file']['tmp_name'])) {
            throw new Exception('Error, invalid file upload!');
        }

        // Make sure it uploaded without an error
        if($_FILES['photo_file']['error'] !== UPLOAD_ERR_OK) {
            throw new Exception('Error handling file upload!');
        }

        // Load up the setting account value
        $criteria = new CDbCriteria();
        $criteria->condition = 'account_id = :account_id AND setting_id = :setting_id';
        $criteria->params = array(
            ':account_id' => Yii::app()->user->accountId,
            ':setting_id' => Settings::SETTING_PROFILE_PHOTO_ID,
        );
        $settingAccountValue = SettingAccountValues::model()->find($criteria);

        // The account value did not previously exist create a model for it
        if(!$settingAccountValue) {
            $settingAccountValue = new SettingAccountValues();
            $settingAccountValue->account_id = Yii::app()->user->accountId;
            $settingAccountValue->setting_id = Settings::SETTING_PROFILE_PHOTO_ID;
        }

        // Update value
        $settingAccountValue->value = 'logo.jpg';
        if(!$settingAccountValue->save()) {
            throw new Exception('Unable to save profile setting account value!');
        }

        // Load up ImageMagick
        $im = new Imagick($_FILES['photo_file']['tmp_name']);

        // Crop image
        $im->cropImage((int)$_POST['w'], (int)$_POST['h'], (int)$_POST['x'], (int)$_POST['y']);

        // Ensure background is white and convert to jpg
//        $im->setImageBackgroundColor(new ImagickPixel('white'));
//        $im = $im->flattenImages();
//        $im->setImageFormat('jpeg');

        // Store the file in S3
        $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());
        $s3client->putObject(array(
            'Bucket'        =>  (YII_DEBUG ? 'dev.' : '') . 'sites.seizethemarket.com',
            'Key'           =>  'site-files/' . Yii::app()->user->clientId . '/account-logos/' . Yii::app()->user->accountId . '/' . $settingAccountValue->value,
            'Body'          =>  $im->getImageBlob(),
            'CacheControl'  =>  'max-age=10'
        ));
    }
}