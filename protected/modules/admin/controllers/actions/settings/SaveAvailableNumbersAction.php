<?php
/**
 * Verify SMS Code Action
 */
class SaveAvailableNumbersAction extends CAction
{
    protected static $PLIVO_APP_ID = NULL;

    /**
     * Manages models
     */
    public function run()
    {
		self::$PLIVO_APP_ID = (YII_DEBUG ? '33198224245961981' : '14819223960028654');

        if(!self::$PLIVO_APP_ID) {
            echo CJSON::encode(array('error' => 'Missing App ID. Contact Support Team.'));
            Yii::app()->end();
        }

        if(!isset($_POST['phone_to_use']) || empty($_POST['phone_to_use'])) {
            echo CJSON::encode(array('error' => 'Invalid Number Selected.'));
            Yii::app()->end();
        }

		if(!isset($_POST['contact_id']) || empty($_POST['contact_id'])) {
            echo CJSON::encode(array('error' => 'Invalid Contact ID.'));
            Yii::app()->end();
        }

        $contact = Contacts::model()->findByPk($_POST['contact_id']);

        return $this->performAjaxRequest($contact);
    }

    /**
     * Perform AJAX Request
     */
    protected function performAjaxRequest(Contacts $contact)
    {
        if (!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array('status' => 'error', 'message' => 'Invalid request.'));
			Yii::app()->end();
		}

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=> Yii::app()->user->accountId));

        $Criteria = new CDbCriteria;
        $Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id=:clientAccountId';
        $Criteria->params = array(
            ':clientAccountId'     => $clientAccount->id,
            ':contactId'    => $contact->id,
            ':active'		=> 1
        );

		$checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);

		if (!empty($checkForSmsNumberHQ)) {
			echo CJSON::encode(array('status' => 'error', 'message' => 'There is already a number assigned to this user', 'code' => 1));
			Yii::app()->end();
		}

		$phone_to_use = str_replace('/[^0-9]/', '', $_POST['phone_to_use']);

		if(strlen($phone_to_use) != 11) {
			$phone_to_use = "1".$phone_to_use;
		}

		$response = Yii::app()->plivo->buyNumber(array(
			'number' => $phone_to_use,
			'app_id' => self::$PLIVO_APP_ID
		));

		if(isset($response['response']['error'])) {
			Yii::log(__CLASS__ . ' (' . __LINE__ . ') ERROR: Could not save new SMS Numer, Plivo reurtned and error: '.$response['response']['error'].PHP_EOL.'response: '.print_r($response, true).PHP_EOL.'POST: '.print_r($_POST, true));
			echo CJSON::encode(array('status' => 'error', 'message' => 'There is already a number assigned to this user', 'code' => 1));
			Yii::app()->end();
		}

		if(!isset($response['response']['status'])) {
			Yii::log(__CLASS__ . ' (' . __LINE__ . ') ERROR: Could not save new SMS Numer, Plivo reurtned and error. Response: '.print_r($response, true).PHP_EOL.'POST: '.print_r($_POST, true));
			echo CJSON::encode(array('status' => 'error', 'message' => 'There is already a number assigned to this user', 'code' => 1));
			Yii::app()->end();
		}

		// update a phone number alias property and the
		$response = Yii::app()->plivo->editNumber(array(
				'number' => $phone_to_use,
				'alias' => 'Client# '.Yii::app()->user->clientId.', Acct# '.Yii::app()->user->accountId.', Contact# '.$contact->id.' - '.$contact->fullName
			));

		// FYI
		Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: updated purchased number. Response: '.print_r($response, true));

		if(strlen($phone_to_use) > 10 && $phone_to_use[0] == 1) {
			$phone_to_use = substr($phone_to_use, 1);
		}

		$clientAccountId = \ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=> Yii::app()->user->accountId));

		if (empty($clientAccountId)) {
			echo CJSON::encode(array('status' => 'error', 'message' => 'Could not find client account id.', 'code' => 1));
			Yii::app()->end();
		}

		$saveSMSNumber = new \TelephonyPhones();
		$saveSMSNumber->attributes = array(
			'client_id' => Yii::app()->user->clientId,
			'contact_id' => $contact->id,
			'client_account_id' => $clientAccountId->id,
			'phone' => $phone_to_use,
			'provider' => 'Plivo',
			'provider_response_to_rent' => json_encode($response['response']),
			'provider_response_to_rent_message' => $response['response']['message'],
			'provider_response_to_rent_status' => $response['response']['status'],
			'status' => 1,
            'user_description' => $contact->fullName.' - STM Dedicated SMS/Phone Number',
			'added' => new CDbExpression('NOW()')
		);

		if(!$saveSMSNumber->validate()) {
			echo CJSON::encode(array('status' => 'error', 'message' => CActiveForm::validate($saveSMSNumber)));
			Yii::app()->end();
		}

		if(!$saveSMSNumber->save()) {
			Yii::log(__CLASS__ . ' (' . __LINE__ . ') ERROR: Could not save new SMS Numer in HQ. Attributes: ' . print_r($saveSMSNumber->attributes, true).PHP_EOL.'POST: '.print_r($_POST, true), CLogger::LEVEL_ERROR);
			echo CJSON::encode(array('status' => 'error', 'message' => 'The SMS Number could not be saved. Please try again.', 'smsObj' => $saveSMSNumber));
			Yii::app()->end();
		}

		echo CJSON::encode(array('status' => 'success'));
		Yii::app()->end();
    }
}