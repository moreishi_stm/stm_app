<?php

class SaveProfilePhotoAction extends CAction
{
    public function run()
    {
//        print_r($_FILES);
//        print_r($_POST);
//        exit;

        // If we don't have a file upload
        if(!isset($_FILES['video_file'])) {
            throw new Exception('Error, no file upload found!');
        }

        // Make sure we don't have any funny business here
        if(!is_uploaded_file($_FILES['video_file']['tmp_name'])) {
            throw new Exception('Error, invalid file upload!');
        }

        // Make sure it uploaded without an error
        if($_FILES['video_file']['error'] !== UPLOAD_ERR_OK) {
            throw new Exception('Error handling file upload!');
        }

        // Make sure we have a valid contact ID
        $contact = Contacts::model()->findByPk($_POST['contact_id']);
        if(!$contact) {
            throw new Exception('Error, invalid contact ID!');
        }

        // Load up the setting contact value
        $criteria = new CDbCriteria;
        $criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
        $criteria->params = array(
            ':contact_id' => $contact->id,
            ':setting_id' => Settings::SETTING_PROFILE_PHOTO_ID,
        );
        $profileSettingContactValue = SettingContactValues::model()->find($criteria);

        // The contact value did not previously exist create a model for it
        if(!$profileSettingContactValue) {
            $profileSettingContactValue = new SettingContactValues;
            $profileSettingContactValue->contact_id = $contact->id;
            $profileSettingContactValue->setting_id = Settings::SETTING_PROFILE_PHOTO_ID;
        }

        // Update contact value
//        $profileSettingContactValue->value = $profileSettingContactValue->saveProfileImage();
        $profileSettingContactValue->value = 'profile_photo.jpg';
        if(!$profileSettingContactValue->save()) {
            throw new Exception('Unable to save profile setting contact value!');
        }

        // Load up ImageMagick
        $im = new Imagick($_FILES['video_file']['tmp_name']);

        // Crop image
        $im->cropImage((int)$_POST['w'], (int)$_POST['h'], (int)$_POST['x'], (int)$_POST['y']);

        // Ensure background is white and convert to jpg
//        $im->setImageBackgroundColor(new ImagickPixel('white'));
//        $im = $im->flattenImages();
//        $im->setImageFormat('jpeg');

        // Store the file in S3
        $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());
        $s3client->putObject(array(
            'Bucket'        =>  (YII_DEBUG ? 'dev.' : '') . 'sites.seizethemarket.com',
            'Key'           =>  'site-files/' . Yii::app()->user->clientId . '/profile/' . $contact->id . '/' . $profileSettingContactValue->value,
            'Body'          =>  $im->getImageBlob(),
            'CacheControl'  =>  'max-age=10'
        ));
    }
}