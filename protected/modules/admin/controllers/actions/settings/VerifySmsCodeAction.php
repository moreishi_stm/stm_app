<?php
/**
 * Verify SMS Code Action
 */
class VerifySmsCodeAction extends CAction
{
    /**
     * Manages models
     */
    public function run() {
        if(!isset($_POST['contactId']) || empty($_POST['contactId'])) {
            echo CJSON::encode(array('error' => 'Invalid Contact ID.'));
            Yii::app()->end();
        }

        $contact = Contacts::model()->findByPk($_POST['contactId']);
        return $this->performAjaxRequest($contact);
    }

    /**
     * Perform AJAX Request
     *
     * @param Contacts $contact
     */
    protected function performAjaxRequest(Contacts $contact) {

        if (Yii::app()->request->isAjaxRequest) {

            if(!isset($_POST['smsVerificationCode']) || empty($_POST['smsVerificationCode'])) {
                echo CJSON::encode(array('error' => 'Invalid Verification Code.'));
                Yii::app()->end();
            }

            $id = $contact->id;

            $model = $this->controller->baseModel;
            $model->type = 'contact';
            $models = $model->contactType()->findAll();

            $SettingContactValues = new SettingContactValues;
            foreach ($models as $Setting) {
                $SettingContactValue = $Setting->getSettingContactValue($id);
                $SettingContactValues->data[$SettingContactValue->setting_id] = $SettingContactValue->value;
            }

            if(!isset($SettingContactValues->data[$model->getField('sms_verification_code')->id]) || empty($SettingContactValues->data[$model->getField('sms_verification_code')->id])) {
                echo CJSON::encode(array('error' => 'No verification code was sent. Please click the "Send Verification Code" button.'));
                Yii::app()->end();
            }

            $systemCodeValue = $SettingContactValues->data[$model->getField('sms_verification_code')->id];

            #die("<pre>".print_r(array($systemCodeValue, $_POST, $SettingContactValues), true));

            if($systemCodeValue != $_POST['smsVerificationCode']) {
                echo CJSON::encode(array('error' => 'Invalid verification code. Please try again.'));
                Yii::app()->end();
            }

            if(isset($SettingContactValues->data[$model->getField('sms_verified')->id])) {
                $Criteria = new CDbCriteria;
                $Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
                $Criteria->params = array(
                    ':contact_id' => $contact->id,
                    ':setting_id' => $model->getField('sms_verified')->id,
                );

                $SettingContactValue = SettingContactValues::model()->find($Criteria);
            } else {
                $SettingContactValue = new SettingContactValues;
            }

            $SettingContactValue->contact_id = $contact->id;
            $SettingContactValue->setting_id = $model->getField('sms_verified')->id;
            $SettingContactValue->value = 1;

            if(!$SettingContactValue->save()) {
                echo CJSON::encode(array('error' => 'Verification could not be completed. Please try again.'));
                Yii::app()->end();
            }

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id = :contactId AND status = :status';
			$Criteria->params = array(
				':contactId'    => $contact->id,
				':status'		=> 1
			);

			/*$checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);

			if (empty($checkForSmsNumberHQ)) {
				echo CJSON::encode(array('status' => 'success', 'needs_number' => true));
				Yii::app()->end();
			}*/

			echo CJSON::encode(array('status' => 'success'));
            Yii::app()->end();
        }
    }
}