<?php
	class EditAction extends CAction {

		public function run($id = null) {
			Yii::app()->user->setState('settingsReferrer', '/'.Yii::app()->controller->module->id.'/settings/' . $id);
			$id = ($id) ? $id : Yii::app()->user->id;

			if (Yii::app()->request->isAjaxRequest) {
				if ($_POST['photoCropData']) {
					$this->performAjaxPhotoCropRequest($id);
				} else {
					$this->performAjaxRequest($id);
				}
			}
			$this->registerScript($id);
			$this->controller->title = 'Settings';

			$model = $this->controller->baseModel;
			$model->type = 'contact';
			$models = $model->contactType()->findAll();

			$Contact = Contacts::model()->findByPk($id);


            /** @todo This needs to be refactored into a validation rule for the model, byIn() causes an error due to inappropriate access to a StmFormatter method. */
            if(Yii::app()->user->checkAccess('owner')) {
                $countOwners = AuthAssignment::model()->byIn('itemname','owner')->count();
                if($countOwners < 2) {
                    $isOnlyOwner = true;
                }
            }

			// Create a new instance of the contactAttributes fields to use for labeling fields
			$ContactAttributes = new ContactAttributes;

			// Create a collection of contact attribute values from what may already be existing
			$ContactAttributeValues = new ContactAttributeValues;
			if ($Contact->getProfileAttributeList()) {
				foreach ($Contact->getProfileAttributeList() as $ContactAttributeValue) {
					$ContactAttributeValues->data[$ContactAttributeValue->contact_attribute_id] = $ContactAttributeValue->value;
				}
			}

			// Create a collection of contact setting values
			$SettingContactValues = new SettingContactValues;
			foreach ($models as $Setting) {
				$SettingContactValue = $Setting->getSettingContactValue($id);
				$SettingContactValues->data[$SettingContactValue->setting_id] = $SettingContactValue->value;
			}
			if (empty($SettingContactValues->data[Settings::SETTING_PROFILE_ON_WEBSITE_ID])) {
				$SettingContactValues->data[Settings::SETTING_PROFILE_ON_WEBSITE_ID] = StmFormHelper::INACTIVE;
			}

			if (isset($_POST['Contacts'])) {
				$Contact->attributes = $_POST['Contacts'];
				$contactValidated = $Contact->save(false); // no need to validate these changes
			}

			$authAssignmentValidated = true;
            $AuthAssignment = new AuthAssignment();
            $AuthAssignment->itemnameCollection = $Contact->getItemNames();
			if (isset($_POST['AuthAssignment']) && !empty($_POST['AuthAssignment']['itemnameCollection'])) {

                $addingNewUser = false;
                // checks to see if adding a new user. This includes re-activating an old user.
                if((empty($AuthAssignments) || Yii::app()->user->checkAccess('inactive') ) && in_array($_POST['AuthAssignment']['itemnameCollection'],array('owner','agent'))) {
                    $addingNewUser = true;
                }
                if($addingNewUser && Yii::app()->user->isMaxUsers()) {
                        Yii::app()->user->setFlash('error', 'Max user limit is reached. Please contact your administrator for more information.');
                } else {

                    $userId = $Contact->id;
                    $AuthAssignments = $Contact->userGroup;
                    foreach($AuthAssignments as $AuthAssignment) {

                        if(($AuthAssignment->account_id !== Yii::app()->user->accountId || in_array($AuthAssignment->itemname, array('dialer')))) {

                            continue;
                        }

                        // check to see if exists in form submit, if not delete it
                        if(in_array($AuthAssignment->itemname, $_POST['AuthAssignment']['itemnameCollection'])) {
                            // if exists in collection, unset the post data so it doesn't create another record for it later.
                            $key = array_search($AuthAssignment->itemname, $_POST['AuthAssignment']['itemnameCollection']);
                            unset($_POST['AuthAssignment']['itemnameCollection'][$key]);
                        }
                        else {
                            $AuthAssignment->delete();
                        }

                    }

                    // any remaining items in post need to have a new records created.
                    foreach($_POST['AuthAssignment']['itemnameCollection'] as $itemName) {

                        $AuthAssignment = new AuthAssignment();
                        $AuthAssignment->account_id = Yii::app()->user->accountId;
                        $AuthAssignment->itemname = $itemName;
                        $AuthAssignment->userid = $Contact->id;
                        if(!$AuthAssignment->save()) {
                            $authAssignmentValidated = false;
                        }
                    }
                }
			}

			if (isset($_POST['ContactAttributeValues'])) {
				$contactAttributeValuesValidated = true;
				foreach ($_POST['ContactAttributeValues']['data'] as $attributeId => $value) {

					unset($ContactAttribute);
					if (isset($ContactAttributeValues->data[$attributeId])) {
						// If the value was previously set then pull a instance of the value's record and modify it.
						$Criteria = new CDbCriteria;
						$Criteria->condition = 'contact_id = :contact_id AND contact_attribute_id = :contact_attribute_id';
						$Criteria->params = array(
							':contact_id' => $Contact->id,
							':contact_attribute_id' => $attributeId,
						);
						$ContactAttribute = ContactAttributeValues::model()->find($Criteria);
					}

					if ($value) {
						if (!isset($ContactAttribute)) {
							$ContactAttribute = new ContactAttributeValues;
						}

						$ContactAttribute->contact_id = $Contact->id;
						$ContactAttribute->contact_attribute_id = $attributeId;
					}

					if ($value || $ContactAttribute) {
						$ContactAttribute->value = $value;
						if (!$ContactAttribute->save() && $contactAttributeValuesValidated) {
							$contactAttributeValuesValidated = false;
						}
					}
				}
			}
			$settingsSubSectionsValidated = ($contactValidated && $contactAttributeValuesValidated) ? true : false;

			$settingAttributesValidSave = true;

			// Processes the file profile upload field separately
			if(isset($_GET['qqfile'])) {
//			if (isset($_FILES['SettingContactValues']) && $_FILES['SettingContactValues']['name']['data'][Settings::SETTING_PROFILE_PHOTO_ID]) {

				if ($SettingContactValues->data[Settings::SETTING_PROFILE_PHOTO_ID]) {
					$Criteria = new CDbCriteria;
					$Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
					$Criteria->params = array(
						':contact_id' => $Contact->id,
						':setting_id' => Settings::SETTING_PROFILE_PHOTO_ID,
					);
					$ProfileSettingContactValue = SettingContactValues::model()->find($Criteria);
				}

				// The transaction value did not previously exist create a model for it
				if (!$ProfileSettingContactValue) {
					$ProfileSettingContactValue = new SettingContactValues;
					$ProfileSettingContactValue->contact_id = $Contact->id;
					$ProfileSettingContactValue->setting_id = Settings::SETTING_PROFILE_PHOTO_ID;
				}
				$ProfileSettingContactValue->value = $ProfileSettingContactValue->saveProfileImage();

				if (!$ProfileSettingContactValue->save() && $settingAttributesValidSave) {
					$settingAttributesValidSave = false;
				}
			}

			if (isset($_POST['SettingContactValues'])) {

				// Process the Contact Setting Values
				foreach ($_POST['SettingContactValues']['data'] as $settingId => $value) {
					unset($SettingContactValue);
					if (isset($SettingContactValues->data[$settingId])) {
                        // If the value was previously set then pull a instance of the value's record and modify it.
                        $Criteria = new CDbCriteria;
                        $Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
                        $Criteria->params = array(
                            ':contact_id' => $Contact->id,
                            ':setting_id' => $settingId,
                        );
                        if (empty($value) && $value !=='0') {
                            SettingContactValues::model()->deleteAll($Criteria);
                        } else {
                            $SettingContactValue = SettingContactValues::model()->find($Criteria);
                        }
					}

					if ($value || $value =='0') {
						if (!isset($SettingContactValue)) {
							$SettingContactValue = new SettingContactValues;
						}

						$SettingContactValue->contact_id = $Contact->id;
						$SettingContactValue->setting_id = $settingId;
					}

					if ($value || $SettingContactValue) { //$value =='0' ||
						$SettingContactValue->value = $value;
						if (!$SettingContactValue->save()) {
							$settingAttributesValidSave = false;
							Yii::app()->user->setFlash('error', 'Error: ' . $SettingContactValue->getErrors());
						}
					}
				}
				if ($settingAttributesValidSave && $settingsSubSectionsValidated && $authAssignmentValidated) {
					Yii::app()->user->setFlash('success', 'Successfully Updated Settings!');
				} else {
					Yii::app()->user->setFlash('error', 'Error: All the settings info did not save properly. Please contact support.');
				}

				$redirect = Yii::app()->user->getState('settingsReferrer');
				Yii::app()->user->setState('settingsReferrer', null);

				$this->controller->redirect($redirect);
			}

            $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=> Yii::app()->user->accountId));

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id=:clientAccountId';
			$Criteria->params = array(
                ':clientAccountId'     => $clientAccount->id,
				':contactId'    => $Contact->id,
				':active'		=> 1
			);

            $checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);
			$smsProvidedNumber = "";

			if(!empty($checkForSmsNumberHQ)) {
				$smsProvidedNumber =  Yii::app()->format->formatPhone($checkForSmsNumberHQ->phone);
			}

			$this->controller->render('form', array(
					'smsProvidedNumber' => $smsProvidedNumber,
					'model' => $model,
                    'isOnlyOwner' => $isOnlyOwner,
					'Contact' => $Contact,
					'AuthAssignment' => $AuthAssignment,
					'ContactAttributes' => $ContactAttributes,
					'ContactAttributeValues' => $ContactAttributeValues,
					'SettingContactValues' => $SettingContactValues,
				)
			);
		}

		private function performAjaxRequest($contactId) {
			if (Yii::app()->request->isAjaxRequest) {
				$ProfileSettingContactValue = new SettingContactValues;
				$ProfileSettingContactValue->contact_id = $contactId;
				$ProfileSettingContactValue->setting_id = Settings::SETTING_PROFILE_PHOTO_ID;

				$folder = $ProfileSettingContactValue->getProfileImageBaseFilepath() . '/';
				$allowedExtensions = explode(',', Settings::ALLOWED_PROFILE_PHOTO_FILE_EXTENSIONS);
				$sizeLimit = Settings::PROFILE_PHOTO_SIZE_MAX_MB * 1024 * 1024;

				$result = $ProfileSettingContactValue->uploadTempProfilePhoto();
				$result['baseUrlPath'] = $ProfileSettingContactValue->getProfileImageBaseUrlPath();

                $maxWidth = 400;
                $maxHeight = 430;
                if($result['width'] > $maxWidth) {
                    // resize dimensions proportionately
                    $shrinkRatio = $maxWidth/$result['width'];
                    $result['width'] = $maxWidth;
                    $result['height'] = round($result['height'] * $shrinkRatio);
                    $result['ratio'] = $shrinkRatio;
                } elseif($result['height'] > $maxHeight) {

                    $shrinkRatio = $maxHeight/$result['height'];
                    $result['width'] = round($result['width'] * $shrinkRatio);
                    $result['height'] = $maxHeight;
                    $result['ratio'] = $shrinkRatio;
                }
                if(YII_DEBUG) {
                    $result['baseUrlPath'] = str_replace('.com','.local',$result[baseUrlPath]);
                }
				echo CJSON::encode($result);

				Yii::app()->end();
			}
		}

		private function performAjaxPhotoCropRequest($contactId) {

			$ProfileSettingContactValue = SettingContactValues::model()->find(array(
					'condition' => 'contact_id=:contactId AND setting_id=:settingId',
					'params' => array(
						'contactId' => $contactId,
						'settingId' => Settings::SETTING_PROFILE_PHOTO_ID
					)
				)
			);

			if (!$ProfileSettingContactValue) {
				$ProfileSettingContactValue = new SettingContactValues;
				$ProfileSettingContactValue->contact_id = $contactId;
				$ProfileSettingContactValue->setting_id = Settings::SETTING_PROFILE_PHOTO_ID;
			}

			$path = $ProfileSettingContactValue->getProfileImageBaseFilepath();
			$files = glob($path . DS . 'profile_photo_temp*');

			if (empty($files) && count($files) != 1) {
                $this->log('Error on uploading profile photo. There are no files or multiple files', CLogger::LEVEL_ERROR);
				echo 'Error on uploading profile photo. There are no files or multiple files';
				Yii::app()->end();
			}

			$result = $ProfileSettingContactValue->cropProfileImage($files[0], $_POST['photoCropData']);

			echo CJSON::encode(array(
					'filename' => $result['filename'],
					'urlPath' => $result['urlPath']
				)
			);
			Yii::app()->end();
		}

		private function registerScript() {
			$assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_exts.jcrop.assets'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
			Yii::app()->clientScript->registerScriptFile($assetsUrl . DS . 'js' . DS . 'jquery.Jcrop.min.js');
			Yii::app()->clientScript->registerCssFile($assetsUrl . DS . 'css' . DS . 'jquery.Jcrop.min.css');
		}
	}