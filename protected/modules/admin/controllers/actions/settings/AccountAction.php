<?php

	class AccountAction extends CAction {

		public function run() {
			$this->controller->title = 'Account Settings';

            $assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_exts.jcrop.assets'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
            Yii::app()->clientScript->registerScriptFile($assetsUrl . DS . 'js' . DS . 'jquery.Jcrop.min.js');
            Yii::app()->clientScript->registerCssFile($assetsUrl . DS . 'css' . DS . 'jquery.Jcrop.min.css');

			$model = $this->controller->baseModel;
			$model->type = 'account';
			$models = $model->accountType()->findAll();

			// Create a collection of account setting values
			$SettingAccountValues = new SettingAccountValues;
			foreach ($models as $Setting) { //options go where?
				$SettingAccountValue = $Setting->settingAccountValue;
                switch($SettingAccountValue->setting_id) {
                    case Settings::DAILY_TASK_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                    case Settings::CLOSING_PIPELINE_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                    case Settings::APPOINTMENTS_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                    case Settings::GOALS_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                    case Settings::APPOINTMENTS_EXECUTIVE_SUMMARY_AGENTS_ID:
                    $SettingAccountValue->value = CJSON::decode($SettingAccountValue->value);
                        break;
                }

				$SettingAccountValues->data[$SettingAccountValue->setting_id] = $SettingAccountValue->value;
			}
			// $SettingOptions  = new SettingOptions;

			if (isset($_POST['SettingAccountValues']) /* && isset($_POST['Settings'])*/) {
				// Process the website contact setting first as it doesn't come through as a model, need to convert
				if ($_POST['website_flyer_contact']) {
					$websiteContact['type'] = $_POST['website_flyer_contact']['type'];
					$websiteContact['value'] = $_POST['website_flyer_contact']['value'];
					$_POST['SettingAccountValues']['data'][$model->getField('website_flyer_contact')->id] = CJSON::encode($websiteContact);
				}

				// Process the topLinks first as it doesn't come through as a model, need to convert
				if ($_POST['topLink']) {
                    $_POST['SettingAccountValues']['data'][$model->getField('top_links')->id] = $this->formatTopLinks();
				}

				// Process the Account Setting Values
				foreach ($_POST['SettingAccountValues']['data'] as $settingId => $value) {
                    switch($settingId) {
                        case Settings::DAILY_TASK_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                        case Settings::CLOSING_PIPELINE_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                        case Settings::APPOINTMENTS_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                        case Settings::GOALS_EXECUTIVE_SUMMARY_RECIPIENT_ID:
                        case Settings::APPOINTMENTS_EXECUTIVE_SUMMARY_AGENTS_ID:
                            $value = CJSON::encode($value);
                            break;
                    }

                    $SettingAccountValue = null;
					if (isset($value)) {
						if (isset($SettingAccountValues->data[$settingId]) || empty($SettingAccountValues->data[$settingId])) {
							// If the value was previously set then pull a instance of the value's record and modify it.
							$Criteria = new CDbCriteria;
							$Criteria->condition = 'account_id = :account_id AND setting_id = :setting_id';
							$Criteria->params = array(
								':account_id' => Yii::app()->user->accountId,
								':setting_id' => $settingId,
							);
							$SettingAccountValue = SettingAccountValues::model()->find($Criteria);
						}
                        if (empty($SettingAccountValue)) {
                            // The transaction value did not previously exist create it
                            $SettingAccountValue = new SettingAccountValues;
                            $SettingAccountValue->account_id = Yii::app()->user->accountId;
                            $SettingAccountValue->setting_id = $settingId;
                        }
						$SettingAccountValue->value = $value;
						if(!$SettingAccountValue->save()) {
                            //@todo: error log
                        }
					}
				}

				Yii::app()->user->setFlash('success', 'Successfully Updated Settings!');

				Yii::app()->controller->redirect(Yii::app()->request->requestUri);
			}

			Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_TopLinkGui.js');

            // Load up the setting account value
            $logoSettingValue = SettingAccountValues::model()->findByAttributes(array(
                'account_id'    =>  Yii::app()->user->accountId,
                'setting_id'    =>  Settings::SETTING_PROFILE_PHOTO_ID
            ));

			$this->controller->render('formAccount', array(
					'model' => $model,
					'SettingAccountValues'  =>  $SettingAccountValues,
                    'logoUrl'      =>  $logoSettingValue ? 'http://' . (YII_DEBUG ? 'dev.' : '') . 'sites.seizethemarket.com/site-files/' . Yii::app()->user->clientId . '/account-logos/' . Yii::app()->user->accountId . '/' . $logoSettingValue->value : null
				)
			);
		}

        public function formatTopLinks() {
            $topLinks = array();
            $unorderedTopLinks = array();

            foreach ($_POST['topLink'] as $key => $value) {
                $singleTopLink = array(
                    'label' => $value['label'],
                    'url' => $value['url'],
                    'window' => $value['window'],
                    'sort_order' => $value['sort_order']
                );

                if(isset($value['label']) || isset($value['url'])) {
                    if($value['sort_order'] && !isset($topLinks[$value['sort_order']])) {
                        $topLinks[$value['sort_order']] = $singleTopLink;
                    } else {
                        $unorderedTopLinks[] = $singleTopLink;
                    }
                }
            }

            ksort($topLinks);
            $maxKey = (count($topLinks)>0)? max(array_keys($topLinks)) + 1 : 1;
            foreach($unorderedTopLinks as $linkData) {
                $linkData['sort_order'] = $maxKey++;
                $topLinks[] = $linkData;
            }
            return CJSON::encode($topLinks);
        }
	}
