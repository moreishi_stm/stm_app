<?php

	class UsersAction extends CAction {

		/**
		 * Manages models
		 */
		public function run() {

			$this->controller->title = 'Users List';

			$model = new Contacts;
            $model->applyAccountScope = false;
            $model->unsetAttributes();
			$model->userGroups = new AuthAssignment;

			if (isset($_GET['Contacts'])) {
                $model->attributes = $_GET['Contacts'];
                $DataProvider = $model->search();

				if(isset($_GET['authAssignmentIsActive'])) {
					$authAssignmentIsActive = $_GET['authAssignmentIsActive'];
					switch($authAssignmentIsActive) {
						case 0:
							$types = 'inactive';
							break;
						case 1:
							$types = 'allActive';
							break;
						case 2:
							$types = '*';
							break;
					}
					$Criteria = $model->byAdmins(true, null, $types)->dbCriteria;
                    $Criteria->order = 'first_name ASC';
					$DataProvider->criteria->mergeWith($Criteria);
				}
                if(isset($_GET['role'])) {
                    $Criteria = $model->byAdmins(true, null, $_GET['role'], null)->dbCriteria;
                    $Criteria->order = 'first_name ASC';
                    $DataProvider->criteria->mergeWith($Criteria);
                }

			} else {
				$authAssignmentIsActive = 1;
				$Criteria = $model->byAdmins(true, null, 'allActive', Yii::app()->user->accountId)->dbCriteria;
                $Criteria->order = 'first_name ASC';
				$DataProvider = new CActiveDataProvider($model, array('criteria' => $Criteria,'pagination'=>array('pageSize'=>50)));
			}


			$this->controller->render('users', array(
					'model' => $model,
					'authAssignmentIsActive' => $authAssignmentIsActive,
					'DataProvider' => $DataProvider,
				)
			);
		}
	}
