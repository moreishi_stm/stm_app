<?php
/**
 * Verify SMS Code Action
 */
class SearchAvailableNumbersAction extends CAction
{
    /**
     * Manages models
     */
    public function run() {
        if(!isset($_POST['area_code']) || empty($_POST['area_code']) || strlen($_POST['area_code']) != 3) {
            echo CJSON::encode(array('error' => 'Invalid Area Code.'));
            Yii::app()->end();
        }

        return $this->performAjaxRequest();
    }

    /**
     * Perform AJAX Request
     */
    protected function performAjaxRequest() {

        if (Yii::app()->request->isAjaxRequest) {

			$response = Yii::app()->plivo->searchNumbers(array(
				'country_iso' => "US",
				//'type' => 'fixed' (Default) | 'mobile' | 'tollfree' | 'any'
				'pattern' => $_POST['area_code'], // Since country_iso is US it will search numbers satarting with 1415
				//'region' => '', //ONLY FOR FIXED TYPE 'fixed' EX:  region=Frankfurt
				//'services' => 'voice', //| 'sms' | 'voice,sms'
				//'lata' => '458', // is Orlando  see: http://en.wikipedia.org/wiki/Local_access_and_transport_area
				'limit' => 20, // Max
				'offset' => 0 // Pagination
			));

			$dataProvider = NULL;
			$count = 0;

			if(isset($response['response']['objects']) && !empty($response['response']['objects'])) {

				$formattedData = array();
				/*{
        "country": "UNITED STATES",
        "lata": 722,
        "monthly_rental_rate": "0.80000",
        "number": "14154009187",
        "type": "fixed",
        "prefix": "415",
        "rate_center": "SNFC CNTRL",
        "region": "United States",
        "resource_uri": "/v1/Account/MANWVLYTK4ZWU1YTY4ZT/PhoneNumber/14154009187/",
        "restriction": null,
        "restriction_text": null,
        "setup_rate": "0.00000",
        "sms_enabled": true,
        "sms_rate": "0.00800",
        "voice_enabled": true,
        "voice_rate": "0.00500"
        }*/

				$count = count($response['response']['objects']);
				$dataProvider = new CArrayDataProvider($response['response']['objects'], array(
					'pagination'=>array(
						'pageSize'=>25,
					),
				));
			}
			$this->controller->renderPartial('_availableNumbersGird', array(
				'dataProvider' => $dataProvider,
				'count' => $count
			));
        }
    }

	/** Added Bkozak 04/02/2015 - Grid View to select sms number */

	public function makeAvailableNumbersText($data) {
		if(strlen($data['number']) > 10) {
			$data['number'] = substr($data['number'], 1);
		}
		return Yii::app()->format->formatPhone($data['number']);
	}

	public function makeAvailableNumbersRadio($data) {
		if(empty($data)) {
			return "";
		}

		return '<input type="radio" name="phone_to_use" id="phone_to_use_'.$data['number'].'" value="'.$data['number'].'" />';
	}

	/** End Added Bkozak 04/02/2015 - Grid View to select sms number */
}