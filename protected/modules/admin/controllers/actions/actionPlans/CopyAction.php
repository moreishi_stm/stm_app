<?php

class CopyAction extends CAction
{

    /**
     * Map of the oldActionPlanItemId => newActionPlanItemId for saving dependent items properly.
     * @var array
     */
    protected $_map = array();
    protected $_newActionPlanId;

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
        // only allow ajax request
        if(!Yii::app()->request->isAjaxRequest) {
            Yii::app()->end();
        }

		$model = $this->controller->loadModel($id);

        // this only gets the PARENTS (no dependent tasks)
        $actionPlanItems = $model->actionPlanItemParents;
        if(!$actionPlanItems) {
            echo CJSON::encode(array('status'=>'error','message'=>'No Action Plan Items found.'));
            Yii::app()->end();
        }

        // put everything in the transaction so no partial copies are made
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $newActionPlan = new ActionPlans();
            $newActionPlan->attributes = $model->attributes;
            $newActionPlan->name .= ' (COPY '.date('m/d/Y').')';

            if(!$newActionPlan->save()) {
                throw new Exception(__CLASS__.' (:'.__LINE__.') Error during copy action plan. Action Plan save error. Error Message: '.print_r($newActionPlan->getErrors().PHP_EOL.' Attribute Data:'.print_r($newActionPlan->attributes, true)));
            }

            $this->_newActionPlanId = $newActionPlan->id;
            // if triggers self, update that
            if($newActionPlan->on_complete_plan_id == $model->id) {
                $newActionPlan->on_complete_plan_id = $newActionPlan->id;
                $newActionPlan->save();
            }

            foreach($actionPlanItems as $actionPlanItem) {

                $this->_copyActionPlanItem($actionPlanItem);
            }

            $transaction->commit();
            $status = 'success';
            $message = $this->_newActionPlanId;
        }
        catch(Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            $status = 'error';
        }

        echo CJSON::encode(array('status'=>$status,'message'=>$message));
        Yii::app()->end();
	}

    protected function _copyActionPlanItem(ActionPlanItems $oldActionPlanItem)
    {
        // make the copy and add to the map

        $newActionPlanItem = new ActionPlanItems();
        $newActionPlanItem->attributes = $oldActionPlanItem->attributes;
        $newActionPlanItem->action_plan_id = $this->_newActionPlanId;

        if($newActionPlanItem->depends_on_id) {
            if(empty($this->_map[$newActionPlanItem->depends_on_id])) {
                throw new Exception(__CLASS__.' (:'.__LINE__.') Error during copy action plan. Dependent Action Plan Item does not exist in map.'.PHP_EOL.' Attribute Data:'.print_r($newActionPlanItem->attributes, true));
            }

            $newActionPlanItem->depends_on_id = $this->_map[$newActionPlanItem->depends_on_id];
        }
        if(!$newActionPlanItem->save()) {

            // this will cause a transaction rollback to occur.
            throw new Exception(__CLASS__.' (:'.__LINE__.') Error during copy action plan. Action Plan Item save error. Error Message: '.print_r($newActionPlanItem->getErrors().PHP_EOL.' Attribute Data:'.print_r($newActionPlanItem->attributes, true)));
        }
        $this->_map[$oldActionPlanItem->id] = $newActionPlanItem->id;

        $dependentItems = $oldActionPlanItem->dependentItems;
        if($dependentItems) {

            foreach($dependentItems as $dependentItem) {

                $this->_copyActionPlanItem($dependentItem);
            }
        }
        return;
    }
}