<?php

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class DeleteAction extends CAction {

		public function run($id) {

			$ActionPlanItem = ActionPlans::model()->findByPk($id);
			if (isset($_POST['ActionPlans'])) {
				$ActionPlanItem->attributes = $_POST['ActionPlans'];
			}

			$this->performAjaxRequest($ActionPlanItem);
		}

		protected function performAjaxRequest(ActionPlans $model) {

			if (Yii::app()->request->isAjaxRequest) {
				if (!$model->delete()) {
					echo CActiveForm::validate($model);
				}

				Yii::app()->end();
			}
		}
	}