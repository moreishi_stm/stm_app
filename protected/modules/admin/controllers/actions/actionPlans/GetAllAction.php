<?php
class GetAllAction extends CAction {

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id) {
		$dataProvider = ActionPlans::getAll($id);
		$this->controller->renderPartial('_getAll',array(
			'dataProvider'=>$dataProvider,
		));
	}
}
