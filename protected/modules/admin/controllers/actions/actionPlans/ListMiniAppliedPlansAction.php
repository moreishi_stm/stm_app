<?php

	class ListMiniAppliedPlansAction extends CAction {

		/**
		 * Manages models
		 */
		public function run($componentTypeId, $componentId) {
			$dataProvider = ActionPlans::getAppliedPlans($componentTypeId, $componentId);

			$this->controller->renderPartial('_listMiniAppliedPlans', array(
					'dataProvider' => $dataProvider,
					'parentComponentId' => $componentId,
					'parentComponentTypeId' => $componentTypeId
				)
			);
		}
	}
