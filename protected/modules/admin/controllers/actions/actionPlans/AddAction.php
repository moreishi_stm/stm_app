<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class AddAction extends CAction {

	public function run()
    {
        $this->controller->title = 'Add Action Plan';
		$model = new ActionPlans('create');

		if (isset($_POST['ActionPlans'])) {
			$model->attributes = $_POST['ActionPlans'];

			$model->account_id = Yii::app()->user->accountId;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully added Action Plan.');
				$this->controller->redirect('/'.Yii::app()->controller->module->name.'/actionPlans/'.$model->id);
			}
		}

		$this->controller->render('form', array(
			'model' => $model,
		));
	}
}
