<?php
	/**
	 * Standalone Action for the Asynchronous Application of Action Plans.
	 *
	 * @author Will Madison (will@willmadison.com)
	 */

	class ApplyAction extends CAction {

		public function run() {
			$ActionPlan = new ActionPlans('apply');

			if (isset($_POST['ActionPlans'])) {
				$ActionPlan->attributes = $_POST['ActionPlans'];
                if($ActionPlan->id) {
                    $ActionPlan = $ActionPlan->findByPk($ActionPlan->id);
                }
                else {
                    echo CActiveForm::validate($ActionPlan);
                    Yii::app()->end();
                }
			}

			$this->performAjaxRequest($ActionPlan);
		}

		private function performAjaxRequest(ActionPlans $ActionPlan)
        {
			if (Yii::app()->request->isAjaxRequest) {

                if(!($parentModel = ComponentTypes::getComponentModel($_POST['ActionPlans']['component_type_id'], $_POST['ActionPlans']['componentId']))) {
                    echo CJSON::encode(array('status'=>'error','message'=>'Component not found.'));
                    Yii::app()->end();
                }

				$ActionPlan->apply($parentModel);

				if(($errors = StmActiveForm::validate($ActionPlan, null, null, $clearErrors = false)) ==  '[]') {
                    echo CJSON::encode(array('status'=>'success'));
                }
                else {
                    echo $errors;
                }

				Yii::app()->end();
			}
		}
	}