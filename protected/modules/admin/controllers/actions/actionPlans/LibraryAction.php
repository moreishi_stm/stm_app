<?php

class LibraryAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 *
	 * @important all data that is run in a seperate database must be called
	 * prior to rendering any views or any actions that require another or the
	 * original database database, databses may be switched during partial
	 * and rendering so be careful
	 */
	public function run($id)
	{

		$clientId = Yii::app()->user->clientId;
		$accountId = Yii::app()->user->accountId;

		// Swap DBs
		StmFunctions::frontConnectDbByClientIdAccountId(30, 1);

		$this->controller->title = 'Shared Action Plan Library';

		$model = $this->controller->loadModel($id);
		//if(!$model->id){die("borked");}
		$this->controller->title = $model->name;
        $model->operation_manuals = CJSON::decode($model->operation_manuals);

		$actionPlanItemsModel = new ActionPlanItems;
        $actionPlanItemsModel->unsetAttributes();
        $actionPlanItemsModel->is_deleted=0;
		$actionPlanItemsModel->action_plan_id = $id;
		// we need to search before changing the database
		$actionPlanItemsModel = $actionPlanItemsModel->search();
		StmFunctions::frontConnectDbByClientIdAccountId($clientId, $accountId);

		$this->controller->render('viewLibraryItem',array(
			'model'=>$model,
			'actionPlanItemsModel'=>$actionPlanItemsModel,
		));

	}

    public function printReferenceDateGrid($actionPlanItem) {
        $string = $actionPlanItem->getModelAttribute("getDueReferences", $actionPlanItem->due_reference_ma);
        if($actionPlanItem->due_reference_ma == ActionPlanItems::DUE_REFERENCE_UPON_COMPLETION_OF_TASK) {
            $string .= ' (#'.$actionPlanItem->depends_on_id.'):<br />'.$actionPlanItem->dependsOn->description;
        }
        return $string;
    }
}