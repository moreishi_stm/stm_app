<?php

class ViewAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		$model = $this->controller->loadModel($id);
		$this->controller->title = $model->name;
        $model->operation_manuals = CJSON::decode($model->operation_manuals);

		$actionPlanItemsModel = new ActionPlanItems;
        $actionPlanItemsModel->unsetAttributes();
        $actionPlanItemsModel->is_deleted=0;
		$actionPlanItemsModel->action_plan_id = $id;

		$this->controller->render('view',array(
			'model'=>$model,
			'actionPlanItemsModel'=>$actionPlanItemsModel,
		));
	}

    public function printReferenceDateGrid($actionPlanItem) {
        $string = $actionPlanItem->getModelAttribute("getDueReferences", $actionPlanItem->due_reference_ma);
        if($actionPlanItem->due_reference_ma == ActionPlanItems::DUE_REFERENCE_UPON_COMPLETION_OF_TASK) {
            $string .= ' (#'.$actionPlanItem->depends_on_id.'):<br />'.$actionPlanItem->dependsOn->description;
        }
        return $string;
    }
}