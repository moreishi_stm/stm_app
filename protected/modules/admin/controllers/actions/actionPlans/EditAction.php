<?php

	class EditAction extends CAction {

		/**
		 * Manages models
		 */
		public function run($id)
        {
			$model = $this->controller->loadModel($id);
            $model->operation_manuals = CJSON::decode($model->operation_manuals);
			$this->controller->title = 'Edit Action Plan';

			if (isset($_POST['ActionPlans'])) {

                $model->attributes = $_POST['ActionPlans'];
                if($_POST['ActionPlans']['operation_manuals']) {
                    $model->operation_manuals = CJSON::encode($_POST['ActionPlans']['operation_manuals']);
                }
				if(empty($_POST['ActionPlans']['on_complete_plan_id'])) {
					$model->on_complete_plan_id = null;
				}

				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Successfully updated Action Plan.');
					$this->controller->redirect(array(
							'view',
							'id' => $model->id
						)
					);
				}
			}

            // check to see if there are any action plan items with any non-compatible action plan types OR has already applied action plans.
            $isActionPlanApplied = Yii::app()->db->createCommand('SELECT count(*) FROM action_plan_applied_lu lu JOIN action_plan_items i ON i.id=lu.action_plan_item_id WHERE i.action_plan_id='.$model->id)->queryScalar();
            $hasNonCompatibleItems = Yii::app()->db->createCommand()
                ->select('count(*)')
                ->from('action_plan_items')
                ->orWhere(array('NOT IN','task_type_id', array(TaskTypes::TODO, TaskTypes::PHONE, TaskTypes::EMAIL_MANUAL, TaskTypes::TEXT_MESSAGE, TaskTypes::HANDWRITTEN_NOTE, TaskTypes::MAIL)))
                ->orWhere(array('NOT IN','due_reference_ma', array(ActionPlanItems::DUE_REFERENCE_APPLIED_DATE_ID, ActionPlanItems::DUE_REFERENCE_UPON_COMPLETION_OF_TASK)))
                ->orWhere(array('NOT IN','assign_to_type_ma', array(AssignmentTypes::CURRENT_USER, AssignmentTypes::SPECIFIC_USER)))
                ->orWhere('email_template_id IS NOT NULL')
                ->andWhere('action_plan_id='.$model->id)
                ->queryScalar();

            $crossComponentSafe = ($isActionPlanApplied || $hasNonCompatibleItems)? false : true;

            $originalComponentType = $model->component_type_id;

			$this->controller->render('form', array(
					'model' => $model,
                    'crossComponentSafe' => $crossComponentSafe,
				)
			);
		}
	}