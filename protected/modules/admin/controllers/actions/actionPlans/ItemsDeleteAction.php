<?php

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class ItemsDeleteAction extends CAction {

		public function run($id) {

			$ActionPlanItem = ActionPlanItems::model()->findByPk($id);
			if (isset($_POST['ActionPlanItems'])) {
				$ActionPlanItem->attributes = $_POST['ActionPlanItems'];
			}

			$this->performAjaxRequest($ActionPlanItem);
		}
		protected function performAjaxRequest(ActionPlanItems $model) {

			if (Yii::app()->request->isAjaxRequest) {

				if (!$model->delete()) {
					echo CActiveForm::validate($model);
				}

				Yii::app()->end();
			}
		}
	}
