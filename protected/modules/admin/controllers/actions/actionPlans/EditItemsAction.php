<?php

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class EditItemsAction extends CAction {

		public function run($id) {
			$this->controller->title = 'Edit Action Plan Item';

			$ActionPlanItem = ActionPlanItems::model()->findByPk($id);

			if (isset($_POST['ActionPlanItems'])) {
				$ActionPlanItem->attributes = $_POST['ActionPlanItems'];
                if($ActionPlanItem->task_type_id != TaskTypes::AUTO_EMAIL_DRIP) {
                    $ActionPlanItem->email_template_id = null;
                }

				$save = true;
				if($ActionPlanItem->specific_user_id) {
					$contact = Contacts::model()->findByPk($ActionPlanItem->specific_user_id);
				} else {
					$contact = Yii::app()->user->getContact();
				}

				if(empty($contact)) {
					Yii::app()->user->setFlash('error', 'The user selected could not be found. Please try again.');
					$save = false;
				}

				if($ActionPlanItem->due_days_type_ma == ActionPlanItems::DUE_REFERENCE_BIRTHDAY_DATE_ID) {
					$birthDay = $contact->getAttributeValue(ContactAttributes::BIRTHDAY);
					if(empty($birthDay) || empty($birthDay->value)) {
						Yii::app()->user->setFlash('error', 'The user selected does not have a birthday.');
						$save = false;
					}
				}

				if($ActionPlanItem->due_days_type_ma == ActionPlanItems::DUE_REFERENCE_SPOUSE_BIRTHDAY_DATE_ID) {
					$birthDay = $contact->getAttributeValue(ContactAttributes::SPOUSE_BIRTHDAY);
					if(empty($birthDay) || empty($birthDay->value)) {
						Yii::app()->user->setFlash('error', 'The user selected does not have a birthday listed for spouse.');
						$save = false;
					}
				}

				if($ActionPlanItem->due_days_type_ma == ActionPlanItems::DUE_REFERENCE_ANNIVERSARY_DATE_ID) {
					$anniversary = $contact->getAttributeValue(ContactAttributes::WEDDING_ANNIVERSARY);
					if(empty($anniversary) || empty($anniversary->value)) {
						Yii::app()->user->setFlash('error', 'The user selected does not have a birthday listed for wedding anniversery.');
						$save = false;
					}
				}

				if ($save && $ActionPlanItem->save()) {
					Yii::app()->user->setFlash('success', 'Successfully saved Action Plan Item!');

					$this->controller->redirect(array(
							'view',
							'id' => $ActionPlanItem->action_plan_id
						)
					);
				}
			}

			$this->controller->render('formItem', array(
					'model' => $ActionPlanItem
				)
			);
		}
	}
