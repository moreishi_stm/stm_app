<?php

Yii::import('application.components.RepairableDbConnection');

class CopySharedAction extends CAction
{
    /**
     * Action Plan Items
     *
     * Complete list of action plan items that we need to copy, includes dependencies
     * @var array
     */
    protected $_aActionPlanItems = array();

    /**
     * Email Templates
     *
     * Complete list of required email templates
     * @var array
     */
    protected $_aEmailTemplates = array();

    /**
     * Email Template Id Map
     *
     * Mapping of old email template IDs to new email template IDs
     * @var array
     */
    protected $_emailTemplateIdMap = array();

    /**
     * Run
     *
     * Action point of entry
     * @return void
     */
    public function run($id)
    {
        // only allow ajax requests
        if (!Yii::app()->request->isAjaxRequest) {
            Yii::app()->end();
        }

        // Retrieve client and account IDs
        $clientId = Yii::app()->user->clientId;
        $accountId = Yii::app()->user->accountId;

        // Swap to library database
        StmFunctions::frontConnectDbByClientIdAccountId(30, 1);

        // Retrieve action plan
        /** @var ActionPlans $actionPlan */
        $oActionPlan = ActionPlans::model()->findByPk($id);
        if(!$oActionPlan) {
            throw new Exception('Error, unable to locate action plan!');
        }

        // Process action plan items and dependencies
        foreach($oActionPlan->actionPlanItems as $oActionPlanItem) {
            $this->_processActionPlanItems($oActionPlanItem);
        }

        // Change back to original database
        StmFunctions::frontConnectDbByClientIdAccountId($clientId, $accountId);

        // Add email templates
        foreach($this->_aEmailTemplates as $key => $aEmailTemplate) {

            // Strip off old email template ID for insertion
            $oldTemplateId = $aEmailTemplate['attributes']['id'];
            unset($aEmailTemplate['attributes']['id']);

            // Create new email template
            $oEmailTemplate = new EmailTemplates();
            $oEmailTemplate->attributes = $aEmailTemplate['attributes'];

            // Attempt to save email template
            if(!$oEmailTemplate->save()) {
                throw new Exception('Error, unable to save email template!');
            }

            // Store the mapping
            $this->_emailTemplateIdMap[$oldTemplateId] = $oEmailTemplate->id;
            $this->_aEmailTemplates[$key]['newId'] = $oEmailTemplate->id;
        }

        // Add action plan
        $newActionPlan = new ActionPlans();
        $newActionPlan->attributes = $oActionPlan->attributes;
        $newActionPlan->account_id = $accountId;
        $newActionPlan->name .= ' (COPY ' . date('m/d/Y') . ')';  //@todo: At least one pre-existing name was found to be too long for record insertion when coupled with this concatination, commenting out until we find a better solution.

        // Attempt to save action plan
        if(!$newActionPlan->save()) {
            echo CJSON::encode(array('status'=> 'error', 'message'=>current($newActionPlan->getErrors()[0])));
            Yii::app()->end();
        }
        // the only type of trigger should be for self
        elseif($newActionPlan->on_complete_plan_id) {
            $newActionPlan->on_complete_plan_id = $newActionPlan->id;
            $newActionPlan->save();
        }

        // Add action plan items
        foreach($this->_aActionPlanItems as $key => $aActionPlanItem) {

            // Strip off old action plan ID
            unset($aActionPlanItem['attributes']['id']);

            // Create new action plan item
            $oActionPlanItem = new ActionPlanItems();
            $oActionPlanItem->attributes = $aActionPlanItem['attributes'];
            $oActionPlanItem->action_plan_id = $newActionPlan->id;

            if($oActionPlanItem->depends_on_id) {

                $oActionPlanItem->depends_on_id = $this->_aActionPlanItems[ $aActionPlanItem['attributes']['depends_on_id']]['newId'];
            }

            // If we have an email template, link it in
            if(array_key_exists($aActionPlanItem['attributes']['email_template_id'], $this->_emailTemplateIdMap)) {
                $oActionPlanItem->email_template_id = $this->_emailTemplateIdMap[$aActionPlanItem['attributes']['email_template_id']];
            }
            // Attempt to save action plan item
            if(!$oActionPlanItem->save()) {
                throw new Exception('Error, unable to save action plan item! Error Message: '.print_r($oActionPlanItem->getErrors(), 1));
            }

            // Store the mapping
            $this->_aActionPlanItems[$key]['newId'] = $oActionPlanItem->id;
        }

        if(1) {
            echo CJSON::encode(array('status'=> 'success', 'id'=>$newActionPlan->id));
        }
        else {
            echo CJSON::encode(array('status'=> 'error', 'message'=>current($newActionPlan->getErrors()[0])));
        }
        Yii::app()->end();
    }

    /**
     * Process Action Plan Items
     *
     * @param ActionPlanItems $oActionPlanItem
     * @throws Exception
     */
    protected function _processActionPlanItems(ActionPlanItems $oActionPlanItem)
    {
        // If we have a dependency
        if($oActionPlanItem->depends_on_id) {

            // Retrieve dependency and add if it isn't already in the list
            $dependency = ActionPlanItems::model()->findByPk($oActionPlanItem->depends_on_id);
            if(!$dependency) {
                throw new Exception('Error, unable to locate dependency, action plan id ' . $oActionPlanItem->id);
            }

            // Process dependency
            $this->_processActionPlanItems($dependency);
        }

        // Add action plan item if we haven't already
        if(!array_key_exists($oActionPlanItem->id, $this->_aActionPlanItems)) {
            $this->_aActionPlanItems[$oActionPlanItem->id] = array(
                'attributes'    =>  $oActionPlanItem->attributes,
                'newId'         =>  null
            );
        }

        // Check for email template
        if($oActionPlanItem->email_template_id) {

            /** @var EmailTemplates $oEmailTemplate */
            $oEmailTemplate = EmailTemplates::model()->findByPk($oActionPlanItem->email_template_id);
            if(!$oEmailTemplate) {
                throw new Exception('Error, unable to locate email template id ' . $oActionPlanItem->email_template_id);
            }

            // Store email template data
            $this->_aEmailTemplates[$oEmailTemplate->id] = array(
                'attributes'    =>  $oEmailTemplate->attributes,
                'newId'         =>  null
            );
        }
    }
}