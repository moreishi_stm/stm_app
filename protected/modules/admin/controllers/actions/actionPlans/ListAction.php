<?php

class ListAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {

		$this->controller->title = 'Action Plans List';

		$model = new ActionPlans('search');
		$model->unsetAttributes();  // clear any default values
        $model->status_ma = StmFormHelper::ACTIVE;
        $model->account_id = Yii::app()->user->accountId;

        if($_COOKIE['ActionPlans']) {
            $model->attributes = $_COOKIE['ActionPlans'];
        }

        if (isset($_GET['ActionPlans'])) {
			$model->attributes=$_GET['ActionPlans'];

            foreach($_GET['ActionPlans'] as $field => $value) {
                setcookie("ActionPlans[$field]", $value, time()+3600*24*30);
            }
        }

		$this->controller->render('list',array(
			'model'=>$model
		));
	}

    public function printComponentTypeName(ActionPlans $model)
    {
        switch($model->componentType->id) {
            case ComponentTypes::CONTACTS:
                $label = '<span style="font-weight: bold; color: #D20000;">'.$model->componentType->display_name.'</span>';
                break;
            case ComponentTypes::SELLERS:
                $label = '<span style="font-weight: bold; color: rgb(0, 200, 0);">'.$model->componentType->display_name.'</span>';
                break;
            case ComponentTypes::BUYERS:
                $label = '<span style="font-weight: bold; color: rgb(245, 245, 0); text-shadow: 1px 1px 0 #666;">'.$model->componentType->display_name.'</span>';
                break;
            case ComponentTypes::RECRUITS:
                $label = '<span style="font-weight: bold; color: blue;">'.$model->componentType->display_name.'</span>';
                break;
            case ComponentTypes::CLOSINGS:
                $label = '<span style="font-weight: bold; color: orange;">'.$model->componentType->display_name.'</span>';
                break;
            case ComponentTypes::PROJECTS:
                $label = '<span style="font-weight: bold; color: pink;">'.$model->componentType->display_name.'</span>';
                break;
            default:
                $label = $model->componentType->display_name;

                break;
        }
        return $label;
    }
}
