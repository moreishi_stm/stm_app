<?php
	/**
	 * Standalone Action for the Asynchronous Un-Application of Action Plans.
	 *
	 * @author Will Madison (will@willmadison.com)
	 */

	class UnapplyAction extends CAction {

		public function run($id, $componentId, $componentTypeId) {
			if($ActionPlan = ActionPlans::model()->skipSoftDeleteCheck()->findByPk($id)) {
                $this->performAjaxRequest($ActionPlan, $componentId, $componentTypeId);
            }
            else {
                Yii::log(__CLASS__.' (:'.__LINE__.') Error in un-applying an action plan. Action Plan ID:'.$id, CLogger::LEVEL_ERROR, __CLASS__);
            }
		}

		private function performAjaxRequest(ActionPlans $ActionPlan, $componentId, $componentTypeId) {
			if (Yii::app()->request->isAjaxRequest) {

				if (isset($ActionPlan)) {
					$result = $ActionPlan->unapply(ComponentTypes::getComponentModel($componentTypeId, $componentId));
				}

                echo CJSON::encode($result);

				Yii::app()->end();
			}
		}
	}