<?php
Yii::import('front_module.controllers.actions.forms.AbstractFormSubmitAction', true);
//include_once("AbstractFormSubmitAction.php");

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class SendReferralAction extends AbstractFormSubmitAction
    {
        public $autoLogin = false;

//        protected function beforeValidateWebForm($formId)
        protected function afterValidateWebForm($formId, $validationResult)
        {
            // do parent first so any errors gets handled before processing further
            parent::afterValidateWebForm($formId, $validationResult);

            $sourceName = 'Agent Recruit Referral';

            // if setting not exist, create source and set the setting
            $accountSetting = SettingAccountValues::model()->findByAttributes(array('setting_id'=>\Settings::SETTING_AGENT_RECRUIT_REFERRAL_SOURCE_ID, 'account_id'=>Yii::app()->user->accountId));
            if(!($accountSetting)) {

                // see if source exists, if not create it.
                if(!($source = Sources::model()->findByAttributes(array('name'=>$sourceName)))) {
                    $source = new Sources();
                    $source->setAttributes(array('name'=>$sourceName, 'account_id'=>Yii::app()->user->accountId));
                    if(!$source->save()) {
                        // error message
                        throw new Exception(__CLASS__.' (:'.__LINE__.') Source did not save for sending Recruit Referral. Error Messages: '.print_r($source->getErrors(), true));
                    }
                }

                $accountSetting = new SettingAccountValues();
                $accountSetting->setAttributes(array('setting_id'=>\Settings::SETTING_AGENT_RECRUIT_REFERRAL_SOURCE_ID, 'account_id'=>Yii::app()->user->accountId, 'value'=>$source->id));
                if(!$accountSetting->save()) {
                    // error message
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Account Setting did not save for sending Recruit Referral. Error Messages: '.print_r($accountSetting->getErrors(), true));
                }
            }
            //setting exists but is empty. find or create source and save settings.
            elseif(empty($accountSetting->value)) {
                // see if source exists, if not create it.
                if(!($source = Sources::model()->findByAttributes(array('name'=>$sourceName)))) {
                    $source = new Sources();
                    $source->setAttributes(array('name'=>$sourceName, 'account_id'=>Yii::app()->user->accountId));
                    if(!$source->save()) {
                        // error message
                        throw new Exception(__CLASS__.' (:'.__LINE__.') Source did not save for sending Recruit Referral. Error Messages: '.print_r($source->getErrors(), true));
                    }
                }
                $accountSetting->value = $source->id;
                if(!$accountSetting->save()) {
                    // error message
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Account Setting did not save for sending Recruit Referral. Error Messages: '.print_r($accountSetting->getErrors(), true));
                }
            }
            elseif($accountSetting->value) {
                // value doesn't exist
                $source = Sources::model()->findByAttributes(array('name'=>$sourceName));
            }

            if(!$source) {
                throw new Exception(__CLASS__.' (:'.__LINE__.') Source is missing for Agent Referral submission. Please investigate.');
            }

            // set the source
            $this->_webAdapter->data()->source_id = $source->id;
            $this->_webAdapter->data()->added_by = Yii::app()->user->id;
        }

        protected function performRequest($formId) {
            //Additional Actions to take place here

            // add assignment if not exist
            if(!($existingAssignment = Assignments::model()->findByAttributes(array('component_type_id'=> ComponentTypes::RECRUITS,'component_id'=> $this->_webAdapter->getComponentId(),'assignment_type_id'=> AssignmentTypes::REFERRER, 'assignee_contact_id' => Yii::app()->user->id)))) {
                $assignment = new Assignments();
                $assignment->setAttributes(array(
                    'component_type_id'=> ComponentTypes::RECRUITS,
                    'component_id'=> $this->_webAdapter->getComponentId(),
                    'assignment_type_id'=> AssignmentTypes::REFERRER,
                    'assignee_contact_id' => Yii::app()->user->id
                ));

                if(!$assignment->save()) {

                    throw new Exception(__CLASS__.' (:'.__LINE__.') Assignment did not save. Error Messages: '.print_r($assignment->getErrors(), true));
                }
            }

            echo CJSON::encode(array('status'=>'success','id'=>$this->_webAdapter->getComponentId()));
        }
	}