<?php

/**
 * Finds the contact to add a transaction to
 */
class AddFindAction extends CAction {

	public function run() {

		$this->controller->title = 'Add New Recruit';

		$model = new Contacts('search');
		$model->unsetAttributes();  // clear any default values
//		$model->contact_status_ma = 1;

		if (isset($_GET['Contacts'])) {
			$model->attributes=$_GET['Contacts'];
		}

		$model->emails = new Emails('search');

		if (isset($_GET['Emails'])) {
			$model->emails->attributes = $_GET['Emails'];
		}

		$model->phones = new Phones('search');

		if (isset($_GET['Phones'])) {
			$model->phones->attributes = $_GET['Phones'];
		}

		$this->controller->render('addFind', array(
			'model' => $model,
		));
	}
}