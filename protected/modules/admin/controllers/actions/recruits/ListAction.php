<?php

// Include things we need to get the job done
Yii::import('admin_module.components.StmSlybroadcast.SlyBroadcastException', true);
Yii::import('admin_module.components.StmSlybroadcast.Slybroadcast', true);
Yii::import('admin_module.components.StmSlybroadcast.Campaign', true);

class ListAction extends CAction
{
    protected $_bounceDetectedEmails = array();
    protected $_notSentEmails = array();
    protected $_dupeNotSentEmails = array();
    protected $_missingEmails = array();

	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Recruits List';

		$model = new Recruits('search');

		$model->unsetAttributes();  // clear any default values
		$model->status_id = array(TransactionStatus::NEW_LEAD_ID, TransactionStatus::HOT_A_LEAD_ID, TransactionStatus::B_LEAD_ID, TransactionStatus::C_LEAD_ID, TransactionStatus::D_NURTURING_SPOKE_TO_ID, TransactionStatus::E_NOT_YET_INTERESTED, TransactionStatus::NURTURING_ID, TransactionStatus::ALLIED_RESOURCE);

        if (isset($_GET['Recruits'])) {
		    $model->attributes=$_GET['Recruits'];
            if(!isset($_GET['Recruits']['status_id'])) {
                $model->status_id = null;
            }
		}
		if(!empty($_GET["noTasks"])){
			$model->noTasks = $_GET["noTasks"];
		}
		if(!empty($_GET["noActionPlan"])){
			$model->noActionPlan = $_GET["noActionPlan"];
		}
        if(Yii::app()->user->checkAccess(AuthItem::ROLE_RECRUIT_REFERRING_AGENT)) {
            $model->assignmentId = Yii::app()->user->id;
        }

        $model->contact = new Contacts('search');
		$model->contact->unsetAttributes();
		if (isset($_GET['Contacts'])) {
			$model->contact->attributes = $_GET['Contacts'];
		}

		$model->contact->emails = new Emails('search');
		$model->contact->emails->unsetAttributes();
		if (isset($_GET['Emails'])) {
		   $model->contact->emails->attributes = $_GET['Emails'];
		}

		$model->contact->phones = new Phones('search');
		$model->contact->phones->unsetAttributes();
		if (isset($_GET['Phones'])) {
		   $model->contact->phones->attributes = $_GET['Phones'];
		}

        $emailBlast = new EmailBlasts();
		$emailBlast->sender_name = Yii::app()->user->contact->fullName;
		if (isset($_GET['EmailBlasts']) && !empty($_GET['EmailBlasts']['email_template_id'])) {
			$emailTemplates = new EmailTemplates();
			$emailTemplates = $emailTemplates->findByPk($_GET['EmailBlasts']['email_template_id']);
		}

        if($_GET['blastSend'] == 1 && Yii::app()->user->checkAccess('emails') && Yii::app()->request->isAjaxRequest) {
            $this->_blastSend($model);
        }

		if(!empty($_GET["export"]) && $_GET["export"] === "csv"){
			$this->_exportToCsv($model);
		}

        $smsBlasts = new SmsBlasts();
        $smsBlasts->sender_contact_id = Yii::app()->user->id;
        $smsBlastLimit = ($smsBlastLimit = Yii::app()->user->settings->sms_blast_limit) ? $smsBlastLimit : 250;
        /**
         * @todo: NEED TO CLEAN UP AND CONSOLIDATE - just getting it working for now.
         */
        if(isset($_GET["SmsBlasts"]) && !empty($_GET["SmsBlasts"])){
            $smsBlasts->attributes = $_GET["SmsBlasts"];
			if(!$smsBlasts->sms_body || !$smsBlasts->description){
				$smsTemplate = new SmsTemplates();
				$smsTemplate = $smsTemplate->findByPk($_GET["SmsBlasts"]["sms_template_id"]);
				if(!$smsBlasts->sms_body){
					$smsBlasts->sms_body = $smsTemplate->body;
				}
				if(!$smsBlasts->description){
					$smsBlasts->description = $smsTemplate->description;
				}
			}

            $smsBlasts->send_datetime = new CDbExpression('NOW()');
            if($_GET["saveSmsBlast"] === "1"){
                // check to see if a blast already went out today. Limit it to 1 per day :)
                $alreadySentToday = Yii::app()->db->createCommand("select count(*) from sms_blasts where DATE(send_datetime) = '".date('Y-m-d')."'")->queryScalar();
                if($alreadySentToday && !YII_DEBUG) {
                    echo CJSON::encode(array('status' => 'error', 'message'=>'Already sent an SMS Blast today. Limit 1 per day.'));
                    Yii::app()->end();
                }

                $smsBlastCriteria = array();
                //array of model attributes to build the recipient criteria array
                $modelCriteriaMap = array(
                    'Recruits' => $_GET['Recruits'],
                    'Contacts' => $_GET['Contacts'],
                    'Phones' => $_GET['Phones'],
                    'Emails' => $_GET['Emails']
                );

                foreach($modelCriteriaMap as $modelName => $attributes) {
                    //build recipient criteria array to store
                    foreach($attributes as $key => $value) {
                        if($value != "" && $value != null) {
                            $smsBlastCriteria[$modelName][$key] = $value;
                        }
                    }
                }
                $smsBlasts->recipient_criteria = json_encode($smsBlastCriteria);

                $dataProvider = $model->search();
                $dataProvider->pagination->pageSize = $smsBlastLimit + 1;
                if($recruitsToEmail = $dataProvider->getData()) {

                    if(count($recruitsToEmail) > $smsBlastLimit) {
                        echo CJSON::encode(array('status' => 'error', 'message'=>'Your max recipient limit is '.$smsBlastLimit.'. Please refine your search or contact support to increase your limit.'));
                        Yii::app()->end();
                    }
                }
                $smsBlasts->recipient_count = count($recruitsToEmail);

				if(!$smsBlasts->save()){
					echo CJSON::encode(array('status' => 'error', 'message'=>'Error saving blast'));
					Yii::app()->end();
				}
				echo CJSON::encode(array('status' => 'success', 'message'=>'SMS Blast has been saved.'));
				Yii::app()->end();
			}
		}


        if($_GET['dialerProcess'] && $_GET['dialerAction'] && $_GET['dialerListId'] && Yii::app()->request->isAjaxRequest) {
            $this->_dialerListActions($model, $_GET['dialerAction'], $_GET['dialerListId']);
        }

        // Handle slybroadcast, if we wanted that
        if($_GET['broadcastProcess']) {
            try {
                $this->_broadcastProcessAction($model);
                $status = 'success';
                $slyBroadcastMessage = 'Sly Broadcast campaign has been created successfully.';
            }
            catch(\StmSlybroadcast\SlyBroadcastException $e) {
                $status = 'error';
                $slyBroadcastMessage = 'Error: Sly Broadcast had an error. Please check your username/password. Contact us for additional support.';
                Yii::log(__CLASS__.' (:'.__LINE__.') Sly Broadcast had an error: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            }
            catch(Exception $e) {
                $status = 'error';
                $slyBroadcastMessage = 'Error: Sly Broadcast had an unknown error. Try again later or Contact us for additional support.';
                Yii::log(__CLASS__.' (:'.__LINE__.') Sly Broadcast had an error: '.$e->getMessage(), CLogger::LEVEL_ERROR);
            }
            finally {
                // Send response
                echo CJSON::encode(array('status'=> $status, 'message'=>$slyBroadcastMessage));
                Yii::app()->end();
            }
        }



        // find the STM sms # for user
        $clientAccountId = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=>Yii::app()->user->accountId))->id;
        $toCriteria = new CDbCriteria;
        $toCriteria->condition = 'contact_id = :contact_id AND status = :status AND client_account_id=:clientAccountId';
        $toCriteria->params = array(
            ':clientAccountId' => $clientAccountId,
            ':contact_id'    => Yii::app()->user->id,
            ':status'		=> 1
        );
        $stmSmsNumber = TelephonyPhones::model()->find($toCriteria);

        // mass action
		if(!empty($_GET["MassAction"])){
			$massAction = $_GET["MassAction"];
			if(!empty($massAction) && !empty($massAction["action"]) && !(empty($massAction["options"]) && empty($massAction["plan_options"]) && empty($massAction["status_options"]))){
				$dataProvider = $model->search($returnAll = true);
				$recruits = $dataProvider->getData();
				$recruitCount = count($recruits);
				if($recruitCount > 0){
					$massActionLimit = 250;
					$currentIteration = 0;
					foreach ($recruits as $i => $r){
						if(in_array($massAction["action"] , array('applyActionPlan','unapplyActionPlan')) && $currentIteration > $massActionLimit){
                            break;
                        }
						$currentIteration++;
						if($massAction["action"] === "addTag" || $massAction["action"] === "removeTag"){
							foreach ($massAction["options"] as $recruitTypeId){
								$dataArray = array("recruit_id" => $r->id, "recruit_type_id" => $recruitTypeId);
								$massActionModel = new RecruitTypeLu();
								$massActionModel = $massActionModel->findByAttributes($dataArray);
								if(!$massActionModel->recruit_id && $massAction["action"] === "addTag" && !empty($dataArray)){
									$massActionModel = new RecruitTypeLu();
									$massActionModel->attributes = $dataArray;
									$massActionModel->save(1);
								}elseif ($massAction["action"] === "removeTag" && !empty($massActionModel->recruit_id)){
									$massActionModel->delete();
								}
							}
						}elseif($massAction["action"] === "applyActionPlan" || $massAction["action"] === "unapplyActionPlan"){
							if(!empty($massAction["plan_options"])){
								if($ActionPlan = ActionPlans::model()->findByPk($massAction["plan_options"])){
									if(!($parentModel = ComponentTypes::getComponentModel(
										ComponentTypes::RECRUITS,
										$r->id
									))) {
										echo CJSON::encode(array('status'=>'error','message'=>'Component not found.'));
										Yii::app()->end();
									}

                                    if($massAction["action"] === "applyActionPlan" ) {
                                        $ActionPlan->apply($parentModel);
                                    }
                                    elseif($massAction["action"] === "unapplyActionPlan") {
                                        $ActionPlan->unapply($parentModel, 'via Mass Unapply');
                                    }
								}
							}
                        }elseif($massAction["action"] === "status"){
                            $r->status_id = $massAction["status_options"];
                            if(!$r->save()) {
                                echo CJSON::encode(array("status"=> 'error'));
                                return Yii::app()->end();
                            }
                        }

					}

					header("Content-Type: application/json");
					echo CJSON::encode(array("status"=> 200));
					return Yii::app()->end();
				}
			}
		}


        $this->controller->render("list",array(
			"model"=>$model,
            "emailBlast" => $emailBlast,
            "emailTemplates" => $emailTemplates,
			"smsBlast" => $smsBlasts,
			"smsTemplate" => $smsTemplate,
            "stmSmsNumber" => $stmSmsNumber,
            "smsBlastLimit" => $smsBlastLimit
		));
	}

    protected function _dialerListActions($model, $dialerAction, $dialerListId)
    {
        $errorLog = array();

        $dialerListPhoneIds = Yii::app()->db->createCommand()
            ->select('phone_id')
            ->from('call_list_phones')
            ->where('call_list_id=:callListId AND hard_deleted IS NULL', array(':callListId'=> $dialerListId))
            ->queryColumn();

        $criteria = $model->search()->getCriteria();
        $recruits = Recruits::model()->findAll($criteria);

        foreach($recruits as $recruit) {

            switch($dialerAction) {
                case 'add':
                    CallListPhones::addToCallListByContactId($dialerListId, $recruit->contact->id, null, $errorLog);
                    break;

                case 'remove':
                    CallListPhones::removeFromCallListByContactId($dialerListId, $recruit->contact->id, $errorLog);
                    break;
            }
        }
    }

	private function _exportToCsv($model){
		ini_set('memory_limit', '-1');
		$dataProvider = $model->search();
		$dataProvider->pagination->pageSize = 3000;
		if($recruitsToEmail = $dataProvider->getData()) {
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=recruits-export-'.date('Y-m-d_h-i-s').'-'.str_replace('www.','',$_SERVER['SERVER_NAME']).'-'.count($recruitsToEmail).'.csv');
			$csvRows = array();

            // create a file pointer connected to the output stream
			$output = fopen('php://output', 'w');

            // output the column headings
			$header =  array(
                "ID",
				"First Name",
				"Last Name",
				"Email 1",
				"Email 2",
				"Email 3",
				"Email 4",
				"Pnone 1",
				"Phone 2",
				"Phone 3",
				"Phone 4",
				"Primary Address",
				"City",
				"State",
				"Zip",
				"Status",
				"$ Volume",
				"% Volume Change",
				"$ Volume Change",
				"# Units",
				"Brokerage Name",
                "Brokerage Address",
                "Brokerage City",
                "Brokerage Zip",
			);

			fputcsv($output,$header);
			$rowCount = 0;
			$maxRowCount = 250;
			foreach($recruitsToEmail as $data){
				if($rowCount <= $maxRowCount){
					$row = $this->getContactDataforCsv($data);
					$recruitRow = array(
						$data->statusName,
						($data->annual_volume) ? Yii::app()->format->formatDollars($data->annual_volume) : "",
						($data->annual_volume_change_percent <> 0) ? $data->annual_volume_change_percent."%" : "",
						($data->annual_volume_change <> 0) ? Yii::app()->format->formatDollars($data->annual_volume_change) : "",
						$data->annual_units,
                        $data->current_brokerage,
                        $data->office_address,
                        $data->office_city,
                        $data->office_zip,
					);
					/**
					 * I could be wrong but array merge displayed some funky results
					 * when to many fields were empty, this is the safest way
					 */
					foreach ($recruitRow as $x){
						$row[] = $x;
					}
					fputcsv($output,$row);
				}else{
					continue;
				}
			}
			return Yii::app()->end();

		}
	}

    public function printGridContact($data)
    {
        $string = "<strong><a href='/".Yii::app()->controller->module->id."/recruits/".$data->id."'>".$data->contact->fullName."</a></strong><br />".Yii::app()->format->formatPrintEmails($data->contact->emails);

        foreach($data->contact->phones as $phone) {

            //$string .= '<br />'.(($hidePhones)? '***-***-**'.substr($phone->phone, -2, 2): Yii::app()->format->formatPhone($phone->phone));
            // this is for the phone pad in STM
            if(Yii::app()->user->clientId == 0) {
                $string .= ' <button class="text click2callHelper" type="button" data-cid="'.$data->id.'" ctid="'.ComponentTypes::RECRUITS.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'" style="margin-left: 0;"><em class="icon i_stm_phone" style="width: 20px;"></em>CLICK-2-CALL</button>';
            }
            // this is for the dialog popup
            elseif(Yii::app()->user->checkAccess('clickToCall')) {
                $string .= '<br>'.Yii::app()->format->formatPhone($phone->phone).'<a class="action-button green  fa fa-phone recruit-button-tip click-to-call-button" data-tooltip="Click to Call" data-cid="'.$data->id.'" ctid="'.ComponentTypes::RECRUITS.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'"></a>';
                //                            $string .= ' <button class="text click-to-call-button" type="button" data-cid="'.$this->component_id.'" ctid="'.$this->componentType->id.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'" style="margin-left: 0;"><em class="icon i_stm_phone" style="width: 20px;"></em>CLICK-2-CALL</button>';
            } else {
                $string .= '<br>'.Yii::app()->format->formatPhone($phone->phone);
            }
        }
        return $string;
    }
	public function printContactCsv($data)
	{
		$string = $data->contact->fullName;

		return $string;
	}

	public function getContactDataforCsv($model){
		$con = $model->contact;
		$data = array($con->recruit->id, $con->first_name,$con->last_name);
		$maxEmails = 4;
		$emailCount = 0;
		if($emails = $con->emails) {
			foreach($emails as $email) {
				if($emailCount < $maxEmails) {
					$data[] = $email->email;
					$emailCount++;
				}
			}
			if($emailCount < $maxEmails) {
				while ($emailCount < $maxEmails){
					$data[] = " ";
					$emailCount++;
				}
			}
		} else {
			while ($emailCount < $maxEmails){
				$data[] = " ";
				$emailCount++;
			}
		}

		$maxPhones = 4;
		$phoneCount = 0;
		if($phones = $con->phones) {
			foreach($phones as $key => $phone) {
				if($phoneCount < $maxPhones) {
					$data[] = $phone->phone;
					$phoneCount++;
				}
			}
			if($phoneCount < $maxPhones) {
				while ($phoneCount < $maxPhones){
					$data[] = " ";
					$phoneCount++;
				}
			}
		} else {
			while ($phoneCount < $maxPhones){
				$data[] = " ";
				$phoneCount++;
			}
		}
		if($con->primaryAddress) {
			$data[] = $con->primaryAddress->address;
			$data[] = $con->primaryAddress->city;
			$data[] = AddressStates::getShortNameById($con->primaryAddress->state_id);
			$data[] = $con->primaryAddress->zip;

		}else{
			$data[] = " ";
			$data[] = " ";
			$data[] = " ";
			$data[] = " ";
		}
		return $data;
	}

    /**
     * Broadcast Process Action
     *
     * Creates a new slybroadcast from given data
     * @param $leads
     */
    protected function _broadcastProcessAction($model)
    {
        $criteria = $model->search()->getCriteria();
        $leads = Recruits::model()->findAll($criteria);

        // New SlyBroadcast campaign
        $campaign = new StmSlybroadcast\Campaign();
        $campaign->audioUrl = 'http://sites.seizethemarket.com/site-files/' . Yii::app()->user->clientId . '/broadcast-messages/' . $_GET['broadcastAudioFile'] . '.wav';
        $campaign->callerId = $_GET['broadcastCallerId'];
        $campaign->date = date('Y-m-d H:i:s', strtotime($_GET['broadcastDeliveryDate']));
        $campaign->mobileOnly = $_GET['broadcastMobileOnly'] ? true : false;

        // Add phones to campaign
        foreach($leads as $lead) {

            // Get phone and check to make sure it's valid, then add it to the campaign
            $phone = $lead->contact->getPrimaryPhone();
            if(strlen($phone) == 10) {
                $campaign->addPhone($lead->contact->getPrimaryPhone());
            }
        }

        // Create the campaign
        $slyBroadcast = new StmSlybroadcast\Slybroadcast();
        $slyBroadcast->createCampaign($campaign);
    }


    protected function _blastSend($model)
    {
         //2016-08-17-email-blast-offload
        if(!isset($_GET['EmailBlasts']['sender_name']) || !isset($_GET['EmailBlasts']['email_subject']) || !isset($_GET['EmailBlasts']['description'])) {
            echo CJSON::encode(array('status' => 'error', 'message'=>'Email Blast information missing.'));
            Yii::app()->end();
        }

        // check to see if a blast already went out today. Limit it to 1 per day :)
//        $alreadySentToday = Yii::app()->db->createCommand("select count(*) from email_blasts where DATE(complete_datetime) = '".date('Y-m-d')."'")->queryScalar();
//        if($alreadySentToday && !YII_DEBUG) {
//            echo CJSON::encode(array('status' => 'error', 'message'=>'Already sent an Email Blast today. Limit 1 per day.'));
//            Yii::app()->end();
//        }

        //@todo: check duplicate sends? - same template, subject, and filter

        //verify office name and address exists for can spam compliance data
        if(!Yii::app()->user->settings->office_name) {
            echo CJSON::encode(array('status' => 'error', 'message'=>'Missing office name in settings.'));
            Yii::app()->end();
        }

        if(!Yii::app()->user->settings->office_address) {
            echo CJSON::encode(array('status' => 'error', 'message'=>'Missing office address in Account Settings.'));
            Yii::app()->end();
        }

        if(!Yii::app()->user->settings->office_city) {
            echo CJSON::encode(array('status' => 'error', 'message'=>'Missing office city in Account Settings.'));
            Yii::app()->end();
        }

        if(!Yii::app()->user->settings->office_state) {
            echo CJSON::encode(array('status' => 'error', 'message'=>'Missing office state in Account Settings.'));
            Yii::app()->end();
        }

        if(!Yii::app()->user->settings->office_zip) {
            echo CJSON::encode(array('status' => 'error', 'message'=>'Missing office zip in Account Settings.'));
            Yii::app()->end();
        }

        $dataProvider = $model->search();
        $dataProvider->pagination->pageSize = 2001;
        if($recruitsToEmail = $dataProvider->getData()) {

            if(count($recruitsToEmail) > 2000) {
                echo CJSON::encode(array('status' => 'error', 'message'=>'Your max recipient limit is 2000. Please refine your search or contact support to increase your limit.'));
                Yii::app()->end();
            }

            // build $recipientCriteria
            $recipientCriteria = array();
            //array of model attributes to build the recipient criteria array
            $modelCriteriaMap = array(
                'Recruits' => $_GET['Recruits'],
                'Contacts' => $_GET['Contacts'],
                'Phones' => $_GET['Phones'],
                'Emails' => $_GET['Emails']
            );

            foreach($modelCriteriaMap as $modelName => $attributes) {
                //build recipient criteria array to store
                foreach($attributes as $key => $value) {
                    if($value != "" && $value != null) {
                        $recipientCriteria[$modelName][$key] = $value;
                    }
                }
            }

            $recipientCriteria['emailBlastType'] = 'recruits';

            // save initial data for email blast
            $emailBlast = new EmailBlasts();
            $emailBlast->attributes = $_GET['EmailBlasts'];
            $emailTemplate = EmailTemplates::model()->findByPk($emailBlast->email_template_id);
            $emailBlast->send_datetime = new CDbExpression('NOW()');
            $emailBlast->email_body = $emailTemplate->body;
            if(!$emailBlast->save()) {
                // error handling stop process
                throw new Exception(__CLASS__.' (:'.__LINE__.') Email Blast model did not save. Errors: '.print_r($emailBlast->getErrors(), true));
            }
            $emailBlast->status = 'Draft';
            $emailBlast->recipient_count = count($recruitsToEmail);
            $emailBlast->start_send_datetime = null;
            $emailBlast->recipient_criteria = CJSON::encode($recipientCriteria);
            if(!$emailBlast->save()) {
                throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Email Blast initial start data logging save had error. Attributes: ' . print_r($emailBlast->attributes, true) . PHP_EOL.'Error: ' . print_r($emailBlast->getErrors(), true));
            }
            else {

//                if(isset($_GET['draft']) && $_GET['draft'] == 1) {
//                    Yii::app()->end();
//                }

                // send email to sender that email blast has started
                // $subject = 'Starting Email Blast #: '.$emailBlast->id.'.';
                // $body = 'Starting Email Blast #: '.$emailBlast->id.'.';
                // StmZendMail::easyMail(array('do-not-reply@seizethemarket.net' => 'STM Notification'), 'christine@seizethemarket.com', $subject, $body);
                echo CJSON::encode(array('status'=>'success','message'=>'Email blast scheduled successfully.'));
            }

            //$this->_sendEmailToContacts($emailBlast, $recruitsToEmail, $emailTemplate);
        }

        // send email to sender that email has been sent
        /*$subject = 'Email Blast #: '.$emailBlast->id.'. Complete Confirmation';
        $body = 'This email is to confirm that the Email Blast #: '.$emailBlast->id.' send is complete.'
            .'<br>Emails that were opt-outs or bounced detected and prevented from being sent:<br>'.print_r($this->_bounceDetectedEmails, true);
        StmZendMail::easyMail(array('do-not-reply@seizethemarket.net' => 'STM Notification'), 'christine@seizethemarket.com', $subject, $body);*/

        Yii::app()->end();
    }

    /*protected function _sendEmailToContacts($emailBlast, $contactsToEmail, $emailTemplate)
    {
        // prevents script from timing out
        set_time_limit(1800);
        ini_set('max_execution_time', 1800);

        $domainName = Yii::app()->user->domain->name;
        $officeAddress =  Yii::app()->user->settings->office_address.', '.Yii::app()->user->settings->office_city.', '.AddressStates::getShortNameById(Yii::app()->user->settings->office_state).' '.Yii::app()->user->settings->office_zip;
        $successfulSent = 0;

        $signature = Yii::app()->db->createCommand("select value from setting_contact_values where contact_id=".$emailBlast->senderEmail->contact->id." AND setting_id=".Settings::SETTING_EMAIL_SIGNATURE)->queryScalar();

        $emailMessage = new EmailMessage();
        $emailMessage->id = StmZendMail::NO_EMAIL_ID;
        $emailMessage->setAttributes(array(
                //'id' => 0, // @todo: email message id - put in a fake one??? need to have the right reply to format
                'component_type_id' => ComponentTypes::RECRUITS,
                'from_email_id' => $emailBlast->sender_email_id,
            ));

        //sets variables to make work, hack way into $emailMessage->fromEmail->contact->getFullName()) - only doing this cuz we are moving to zend and no need to rewrite
        $emailMessage->fromEmail = $emailBlast->senderEmail;
        $emailMessage->fromEmail->contact = $emailBlast->senderEmail->contact;

        $alreadySentContactIds = Yii::app()->db->createCommand("SELECT contact_id FROM email_blast_contacts where email_blast_id=".$emailBlast->id)->queryColumn();

        // pre-filter bounced emails - for some reason, zend class not doing it properly?
        $bouncedEmails = Yii::app()->stm_hq->createCommand("SELECT TRIM(LOWER(email)) FROM emails_undeliverable WHERE client_id IS NULL OR (client_id=".Yii::app()->user->clientId." AND account_id=".Yii::app()->user->accountId.") ORDER BY email")->queryColumn();

        $optOutEmails = Yii::app()->db->createCommand("SELECT TRIM(LOWER(email)) FROM emails WHERE email_status_id IN(".EmailStatus::OPT_OUT.",".EmailStatus::HARD_BOUNCE.") ORDER BY email")->queryColumn();

        $doNotDeliverEmails = CMap::mergeArray(array_flip($bouncedEmails), array_flip($optOutEmails));
        $doNotDeliverEmails = array_flip($doNotDeliverEmails);

        // We're gonna need this later
        $s3client = Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

        // pre-process attachments
        // Add all attachments
        $attachmentsTotalSize = 0;
        if($attachments = $emailTemplate->attachments) {

            foreach($attachments as $i => $attachment) {

                $attachmentsTotalSize += $attachment->size;

                //reject if filesize too big.
                if($attachmentsTotalSize >= StmZendMail::SES_ATTACHMENT_MAX_BYTES) {
                    echo CJSON::encode(array('emailStatus' => 'error', 'emailErrorMessage'=> 'Max attachment size is 10Mb.'));

                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Email Processing attachment has size greater than 10 Mb. Sent user error message that email did not send. Remove logging after done monitoring or log in db.', CLogger::LEVEL_ERROR);

                    Yii::app()->end();
                }

                // Attempt to grab and attach attachments for email template from S3
                try {

                    // Retrieve the previously stored object from S3 storage
                    $attachmentsToProcess[$i] = $s3client->getObject(array(
                            'Bucket'    =>  ((YII_DEBUG)?'dev.':'') .'client-private.seizethemarket.com',
                            'Key'       =>  Yii::app()->user->clientId . '/' . Yii::app()->user->accountId . '/email-template-attachments/' . $attachment->id,
                        ));
                    $attachmentsToProcess[$i]['type'] = $attachment->type;
                    $attachmentsToProcess[$i]['name'] = $attachment->name;
                }
                catch(Exception $e) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Error getting S3 email attachment file in Recruit Blast. (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL .'Email Template ID: '.$emailTemplate->id, CLogger::LEVEL_ERROR);
                }
            }
        }

        $status = 'success';
        foreach($contactsToEmail as $recruitToEmail) {

            // prevents from dupe sending
            if(in_array($recruitToEmail->contact_id, $alreadySentContactIds)) {
                $this->_dupeNotSentEmails[] = $recruitToEmail->contact_id . ': '.$recruitToEmail->contact->fullName .' - ' . $recruitToEmail->contact->getPrimaryEmail();
                continue;
            }

            // update component_id to adjust reply-to email result
            $emailMessage->component_id = $recruitToEmail->id;

            //sets variables to make work, hack way into $emailMessage->toEmail->contact->id - only doing this cuz we are moving to zend and no need to rewrite
            $emailMessage->toEmail = $recruitToEmail->contact->getPrimaryEmailObj();

            if(!$emailMessage->toEmail) {
                // if the contact does not have an email skip this contact
                $this->_missingEmails[] = $recruitToEmail->contact_id . ': '.$recruitToEmail->contact->fullName;
                continue;
            }

            // if bounce detected, add to the collection to report on later
            if(in_array(trim(strtolower($emailMessage->toEmail->email)), $doNotDeliverEmails)) {
                $this->_bounceDetectedEmails[] = $emailMessage->toEmail->email;
                continue;
            }

            $emailMessage->toEmail->contact = $recruitToEmail->contact;

            $emailMessage->content = $emailTemplate->filterBodyByContactData($recruitToEmail->contact, $recruitToEmail);
            $emailMessage->content .= $signature;
            $emailMessage->content .= StmFunctions::getCanSpamFooter($domainName, Yii::app()->user->settings->office_name, $officeAddress, $emailMessage->toEmail->id);

            $zendMessage = new StmZendMail();
            $zendMessage->addTo($recruitToEmail->contact->getPrimaryEmail(), Yii::app()->format->formatProperCase($recruitToEmail->contact->getFullName()));
            $replyTo = $zendMessage->getReplyToEmail($emailMessage, $domainName); //@todo: verify it worked without a saved email message ... NEED emailMessageId ***** create new componentType for message Id???

            // Vlad specific tweaks - need to make it a setting - just getting it to work for now
            if(strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') !== false && intval(Yii::app()->user->id) === 1) {
                $zendMessage->setFrom($emailBlast->senderEmail->email, Yii::app()->format->formatProperCase($emailBlast->senderEmail->contact->getFullName()));
            }
            else {
                $zendMessage->setFrom($replyTo, Yii::app()->format->formatProperCase($emailBlast->senderEmail->contact->getFullName()));
            }

            $zendMessage->setSubject($emailBlast->email_subject);

            $zendMessage->setBodyHtml(StmZendMail::replaceLinksForTracking($emailMessage->content, $emailMessage->toEmail->contact->id, $emailMessage->id, $domainName, $trackEmailMessages=false, $emailTemplate->id));

            // Attachments for email template from S3 already downloaded above
            try {
                if(!empty($attachmentsToProcess)) {

                    foreach($attachmentsToProcess as $attachmentToProcess) {

                        // Add the attachment to the email message
                        $zendMessage->createAttachment($attachmentToProcess['Body'], $attachmentToProcess['type'], Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, $attachmentToProcess['name']);
                    }
                }
            }
            catch(Exception $e) {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Error attaching file in Recruit Blast. (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL .'Email Template ID: '.$emailTemplate->id.' | Sender: '.$zendMessage->getFrom().' | Recipients'.print_r($zendMessage->getRecipients(), true), CLogger::LEVEL_ERROR);
            }

            // Attempt to send message
            try {

                if(strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') !== false && Yii::app()->user->id == 1) {
                    $transport = StmZendMail::getVladSparkpostTransport();
                }
                else {
                    $transport = StmZendMail::getAwsSesTransport();
                }

                if($zendMessage->send($transport)) {
                    $successfulSent++;
                    $alreadySentContactIds[] = $recruitToEmail->contact_id;

                    //                    if($successfulSent > 10) {
                    //                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI Bounce Emails: '.print_r($this->_bounceDetectedEmails, true), CLogger::LEVEL_ERROR);
                    //                        Yii::app()->end();
                    //                    }
                }

                // add to emailBlastSent table
                $emailBlastContact = new EmailBlastContacts();
                $emailBlastContact->setAttributes(array('contact_id'=>$recruitToEmail->contact->id, 'email_blast_id'=>$emailBlast->id, 'sent'=> new CDbExpression('NOW()')));
                if(!$emailBlastContact->save()) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Email Blast contact did not save.');
                }
            }
            catch(Exception $e) {

                $status = 'error';
                $message .= ' '.$emailMessage->toEmail->email.' did not send.';
//                    //@todo: need better logging in case we need to see it all in one list
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL
                    .'Email Blast #: '.$emailBlast->id.' | Contact Attributes: '.print_r($recruitToEmail->attributes, true), CLogger::LEVEL_ERROR);

                // let the system rest for a couple seconds
                sleep(2);
                continue;
            }

            if(YII_DEBUG) {
                // only run once for debug
                break;
            }
        }

        // record when email blast went out complete
        $emailBlast->status = 'Sent';
        $emailBlast->sent_count = $successfulSent;
        $emailBlast->complete_datetime = new CDbExpression('NOW()');
        if(!$emailBlast->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Email Blast complete save had error. Attributes: "' . print_r($emailBlast->attributes, true) . '" '.PHP_EOL.'Error: ' . print_r($emailBlast->getErrors(), true), CLogger::LEVEL_ERROR);
        }

        echo CJSON::encode(array('status'=>$status, 'message'=>$message));
    }*/
}
