<?php

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class EditAction extends CAction {

	public function run($id)
	{
		$model=$this->controller->loadModel($id);
        $model->contactEmails = $model->contact->emails;
        $model->contactPhones = $model->contact->phones;
        $model->contactAddresses = $model->contact->addresses;
        $model->hire_date = Yii::app()->format->formatDate($model->hire_date, StmFormatter::APP_DATE_PICKER_FORMAT);
        $model->target_date = Yii::app()->format->formatDate($model->target_date, StmFormatter::APP_DATE_PICKER_FORMAT);

		$this->controller->title = $model->contact->fullName;

        $Appointment = Appointments::model()->byComponentTuple(ComponentTypes::RECRUITS, $id)->find();
        if (!$Appointment) {
            $Appointment = new Appointments;
            $Appointment->isEmpty = true;
            $Appointment->component_type_id = ComponentTypes::RECRUITS;
            $Appointment->component_id = $id;
        }

        // Process the Appointments model
        $appointmentValidated = true;
        if (isset($_POST['Appointments'])) {

            $Appointment->isEmpty = true;
            foreach ($_POST['Appointments'] as $key => $value) {
                if (!empty($value)) {
                    if(in_array($key, array('met_by_id','set_by_id','signed_date','met_status_ma','met_status_reason','set_activity_type_id','is_deleted'))) {
                        $Appointment->isEmpty = false;
                        break;
                    }
                }
            }

            $Appointment->attributes = $_POST['Appointments'];
            if ($Appointment->isEmpty == false && $_POST['Appointments']['toDelete'] != 1) {
                $appointmentValidated = $Appointment->validate();
            }
            if($Appointment->toDelete) {
                $appointmentValidated = true;
            }
        }

        $contactValidated = true;
        if (isset($_POST['Contacts'])) {
            $model->contact->attributes = $_POST['Contacts'];
            $contactValidated = $model->contact->validate();
        }

        $contactEmailsValidated = true;
        if (isset($_POST['Emails'])) {
            $model->contactEmails = array();
            foreach ($_POST['Emails'] as $i => $emailData) {
                $emailModel = new Emails;
                $existingEmail = $emailModel->skipSoftDeleteCheck()->byEmail($emailData['email'])->find();
                if($existingEmail) {
                    $existingEmailIsDeleted = $existingEmail->is_deleted;
                }

                // NOT new email record
                if (isset($emailData['id']) and is_numeric($emailData['id'])) {
                    // The email address already exists within the system, so lets undelete and assign it to this contact
                    if($existingEmail && $existingEmailIsDeleted) {
                        if ($existingEmail instanceof Emails) {
                            $existingEmail->contact_id = $model->contact_id;
                            $existingEmail->is_deleted = 0;
                            // purposely saving now because since the soft delete of current email happens right after, need to have this happen. If something goes wrong, can be left with no data showing up in gui. May need to think through logic some more. This approach is not ideal cleanliness.
                            $existingEmail->save();
                            ${'ContactEmail'.$i} = $existingEmail;

                            $EmailToDelete = Emails::model()->findByPk($emailData['id']);
                            $EmailToDelete->delete();
                            $emailData['id'] = $existingEmail->id;
                        }

                    } else {
                        ${'ContactEmail'.$i} = new Emails;
                        ${'ContactEmail'.$i} = ${'ContactEmail'.$i}->findByPk($emailData['id']);
                    }
                } else { // new email
                    ${'ContactEmail'.$i} = new Emails;

                    if($existingEmail instanceof Emails) {
                        if($existingEmailIsDeleted == 1) {
                            ${'ContactEmail'.$i} = $existingEmail;
                            $emailData['id'] = $existingEmail->id;
                            ${'ContactEmail'.$i}->is_deleted = 0;
                        }
                    }
                }

                ${'ContactEmail'.$i}->attributes = $emailData;

                // Check to see if the email needs to be soft-deleted
                if (${'ContactEmail'.$i}->remove) {
                    ${'ContactEmail'.$i}->delete();
                    continue;
                }

                //  Mark that the contact e-mails section had a failure
                if (!${'ContactEmail'.$i}->validate() && $contactEmailsValidated) {
                    $contactEmailsValidated = false;
                }

                array_push($model->contactEmails, ${'ContactEmail'.$i});
            }
        }

        // Process additional phone numbers for the contact if any
        $contactPhonesValidated = true;
        if (isset($_POST['Phones'])) {
            foreach ($_POST['Phones'] as $i => $phoneData) {
                if ($phoneData['id']) {
                    $ContactPhone = Phones::model()->byIgnoreAccountScope()->findByPk($phoneData['id']);
                } else {
                    $ContactPhone = new Phones;
                }

                $ContactPhone->attributes = $phoneData;

                if ($ContactPhone->remove) {
                    $ContactPhone->delete();
                    continue;
                }

                // Mark that the contact e-mails section had a failure
                if (!$ContactPhone->validate() && $contactPhonesValidated) {
                    $contactPhonesValidated = false;
                }

                array_push($model->contactPhones, $ContactPhone);
            }
        }

        // Process additional addresses for the contact if any
        $contactAddressesValidated = true;
        if (isset($_POST['Addresses'])) {
            foreach ($_POST['Addresses'] as $i => $addressData) {
                if ($addressData['id']) {
                    $Criteria = new CDbCriteria;
                    $Criteria->condition = 'id = :id';
                    $Criteria->params = array(
                        ':id' => $addressData['id'],
                    );
                    $ContactAddress = Addresses::model()->find($Criteria);
                } else {
                    $ContactAddress = new Addresses;
                }

                $ContactAddress->attributes = $addressData;

                if ($ContactAddress->remove) {
                    $ContactAddress->delete();
                    continue;
                }

                // Mark that the contact address section had a failure
                if (!$ContactAddress->validate() && $contactAddressesValidated) {
                    $contactAddressesValidated = false;
                }

                array_push($model->contactAddresses, $ContactAddress);
            }
        }

        $typesValues = array();
		if($RecruitTypes = $model->types) {
			foreach($RecruitTypes as $RecruitType)
				array_push($typesValues, $RecruitType->id);
		}
		$typesValues = CJSON::encode($typesValues);

        // pre-Process assignments for the transactions if any
        $assignmentValidated = true;
        if (isset($_POST['Assignments'])) {
            $assignmentsToDelete = array();
            $assignmentsIdsToDelete = array();
            foreach ($_POST['Assignments'] as $i => $assignmentData) {

                if ($assignmentData['id']) {
                    $Criteria = new CDbCriteria;
                    $Criteria->condition = 'id = :id';
                    $Criteria->params = array(
                        ':id' => $assignmentData['id'],
                    );
                    $RecruitAssignment = Assignments::model()->find($Criteria);
                } else {
                    $RecruitAssignment = new Assignments;
                    $assignmentData['component_type_id'] = ComponentTypes::RECRUITS;
                    $assignmentData['component_id'] = $model->id;
                }

                $RecruitAssignment->attributes = $assignmentData; // @todo: on test, Check to see component_type_id is correct for new record

                if ($RecruitAssignment->remove) {
                    array_push($assignmentsToDelete, $RecruitAssignment);
                    $assignmentsIdsToDelete[] = $RecruitAssignment->assignee_contact_id;
                    continue;
                }

                //  Mark that the contact e-mails section had a failure
                if (!$RecruitAssignment->validate() && $assignmentValidated) {
                    $assignmentValidated = false;
                    $error = current($RecruitAssignment->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
                array_push($model->assignmentCollection, $RecruitAssignment);
            }
        }

		if (isset($_POST['Recruits'])) {
			$model->attributes = $_POST['Recruits'];

			if ($appointmentValidated && $assignmentValidated && $contactValidated && $contactEmailsValidated && $contactAddressesValidated && $contactPhonesValidated && $model->save()) {

                $model->contact->save();
                // Process Emails
                if ($model->contactEmails) {
                    foreach ($model->contactEmails as $ContactEmail) {
                        $ContactEmail->contact_id = $model->contact_id;
                        $ContactEmail->account_id = Yii::app()->user->accountId;
                        if (count($model->contactEmails) == 1) {
                            $ContactEmail->is_primary = 1;
                        }

                        $ContactEmail->save(false); // already ran validation
                    }
                }

                // Process Phone Numbers
                if ($model->contactPhones) {
                    foreach ($model->contactPhones as $ContactPhone) {
                        if ($ContactPhone->isNewRecord) {
                            $ContactPhone->contact_id = $model->contact_id;
                            $ContactPhone->account_id = Yii::app()->user->accountId;
                        }

                        $ContactPhone->save(false); // already ran validation
                    }
                }

                // Process the addresses for this contact
                if ($model->contactAddresses) {
                    foreach ($model->contactAddresses as $ContactAddress) {
                        if ($ContactAddress->isNewRecord) {
                            $ContactAddress->contactId = $model->contact_id;
                            $ContactAddress->account_id = Yii::app()->user->accountId;
                        }

                        if($ContactAddress->save(false)) {  // already ran validation
                            $AddressContactLu = AddressContactLu::model()->findByAttributes(array('contact_id'=>$model->contact_id,'address_id'=>$ContactAddress->id));
                            $AddressContactLu->is_primary = $ContactAddress->isPrimary;
                            $AddressContactLu->save();
                        }
                    }
                }

                if ($model->typesCollection) {
					$typesFlipped = array_flip($model->typesCollection);
					foreach ($model->recruitTypeLu as $RecruitTypeLu) {
						if (!in_array($RecruitTypeLu->recruit_type_id, $model->typesCollection)) {
							$RecruitTypeLu->delete();
						} else {
							unset($typesFlipped[$RecruitTypeLu->recruit_type_id]);
						}
					}

					$model->typesCollection = array_flip($typesFlipped);

					// Attach the contact types to the newly contact
					foreach ($model->typesCollection as $attributeId) {
						$RecruitTypeLu = new RecruitTypeLu;
						$RecruitTypeLu->recruit_id = $model->id;
						$RecruitTypeLu->recruit_type_id = $attributeId;
						$RecruitTypeLu->save();
					}
				}
                else {
                    RecruitTypeLu::model()->deleteAllByAttributes(array('recruit_id'=>$model->id));
                }

                // Assign a contact to this transaction
                if ($model->assignmentCollection) {
                    foreach ($model->assignmentCollection as $RecruitAssignment) {
                        if ($RecruitAssignment->isNewRecord) {
                            $RecruitAssignment->component_id = $model->id;
                        }

                        $RecruitAssignment->logChange = true;
                        $RecruitAssignment->save(false); // already ran validation
                    }
                }

                if(!empty($assignmentsToDelete) && is_array($assignmentsToDelete)) {
                    foreach($assignmentsToDelete as $assignmentToDelete) {
                        // delete will always return false due to soft delete, therefore removed the error message
                        $assignmentToDelete->delete();
                    }
                }

                // already ran validation
                if (!$Appointment->isEmpty && $Appointment->toDelete!=1) {
                    $Appointment->save(false);
                } elseif (!$Appointment->isNewRecord && $Appointment->toDelete==1) {
                    if(!$Appointment->delete()) {
                        Yii::log(__CLASS__ . '(' . __LINE__ . ') Appointment did not delete. Error Message: '.print_r($Appointment->getErrors()).' Appointment Attributes: '.print_r($Appointment->attributes), CLogger::LEVEL_ERROR);
                    }
                }
				if(!empty($_POST["Recruits"]["referred_by_ids"])){
					$deleteIds = array();
					$Criteria = new CDbCriteria;
					$Criteria->select = "*";
					$Criteria->addColumnCondition(array(
						"component_type_id" => ComponentTypes::RECRUITS,
						"component_id" => $model->id,
						"assignment_type_id" => Recruits::REFERRER,
					));
					$current = Assignments::model()->findAll($Criteria);
					foreach ($current as $a){
						$deleteIds[$a->id] = $a->id;
					}


					foreach ($_POST["Recruits"]["referred_by_ids"] as $refferredId){
						if(!empty($refferredId)){
							$Criteria = new CDbCriteria;
							$Criteria->select = "*";
							$Criteria->addColumnCondition(array(
								"component_type_id" => ComponentTypes::RECRUITS,
								"component_id" => $model->id,
								"assignment_type_id" => Recruits::REFERRER,
								"assignee_contact_id" => $refferredId
							));

							$referModel  = Assignments::model()->find($Criteria);
							if(!$referModel){
								$referModel = new Assignments();
							}
							$referModel->attributes = array(
								"component_type_id" => ComponentTypes::RECRUITS,
								"component_id" => $model->id,
								"assignment_type_id" => Recruits::REFERRER,
								"assignee_contact_id" => $refferredId,
								"updated_by" => Yii::app()->user->id
							);

							$referModel->save();
							unset($deleteIds[$referModel->id]);
						}

					}
					/**
					 * @TODO : test delete, write function?
					 */
					if(!empty($deleteIds)){
						foreach ($deleteIds as $id){
							$referModel = Assignments::model()->findByPk($id);
							if(!empty($referModel->id)){
								$referModel->delete();
							}
						}
					}
				}

                Yii::app()->user->setFlash('success', 'Successfully updated Recruit.');
				$this->controller->redirect(array('view','id'=>$model->id));
			}
            // has post but not validated
            else {

                if(!empty($model->assignmentCollection) && is_array($model->assignmentCollection)) {
                    if(empty($model->assignments)) {
                        $model->assignments = $model->assignmentCollection;
                    }
                    else {
                        $existingAssignmentIds = array();
                        if(is_array($model->assignments)) {
                            foreach($model->assignments as $i => $existingAssignment) {
                                $existingAssignmentIds[] = $existingAssignment->assignee_contact_id;
                                if(in_array($existingAssignment->assignee_contact_id, $assignmentsIdsToDelete)) {
                                    $model->assignments[$i]->remove = 1;
                                }
                            }
                        }

                        $allAssignments = $model->assignments;
                        foreach($model->assignmentCollection as $newAssignment) {
                            if(!in_array($newAssignment->assignee_contact_id, $existingAssignmentIds)) {
                                array_push($allAssignments, $newAssignment);
                            }
                        }

                        $model->assignments = $allAssignments;
                    }
                }
            }



		}
		$refferedByDataArray = array();
		if($model->id){
			$Criteria = new CDbCriteria;
			$Criteria->select = "*";
			$Criteria->addColumnCondition(array(
				"component_type_id" => ComponentTypes::RECRUITS,
				"component_id" => $model->id,
				"assignment_type_id" => Recruits::REFERRER,
			));
			$current = Assignments::model()->findAll($Criteria);
			foreach ($current as $a){
				$contact = Contacts::model()->findByPk($a->assignee_contact_id);
				if(!empty($contact->id)){
					$refferedByDataArray[$contact->id] = $contact->fullName;
					$model->referred_by_ids[] = $contact->id;
				}

			}
		}

		$this->registerScript($typesValues);
        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'stm_AddressPhoneEmailGui.js');

		$this->controller->render('form',array(
			'model'=>$model,
            'Appointment'=>$Appointment,
			"refferedByDataArray" => $refferedByDataArray
		));
	}

	public function registerScript($typesValues) {
		$js = <<<JS
		var typesValues = jQuery.parseJSON('$typesValues');
		if(typesValues)
			$.each(typesValues, function( key, value ) {
				$('select#Recruits_typesCollection>option[value="'+value+'"]').attr('selected', true);
			});
		$('select#Recruits_typesCollection').trigger("liszt:updated");
JS;
		Yii::app()->clientScript->registerScript('recruit-form-script', $js);
	}
}