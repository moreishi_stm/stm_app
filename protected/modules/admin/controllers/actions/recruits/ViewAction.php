<?php
Yii::import('admin_widgets.ActivityLogPortlet.ActivityLogPortlet', true);

class ViewAction extends CAction
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function run($id)
	{
		$model = $this->controller->loadModel($id);

        $returnUrl = $this->getController()->createUrl("/admin/recruits/".$id).'?returnContactIdVerify='.$model->contact->id;
        Yii::app()->user->setState('returnContactIdVerify',$returnUrl);

        $tasksProvider = Tasks::getAll($model->id, $model->componentType->id);
        $tasksContent = $this->controller->renderPartial('../tasks/_listMini', array(
                'parentModel' => $model,
                'dataProvider' => $tasksProvider,
                'componentTypeId' => $model->componentType->id,
                'componentId' => $model->id,
            ), true
        );

        $actionPlanProvider = ActionPlans::getAppliedPlans($model->componentType->id, $model->id);
        $actionPlansContent = $this->controller->renderPartial('../actionPlans/_listMiniAppliedPlans', array(
                'dataProvider' => $actionPlanProvider,
                'parentComponentId' => $model->id,
                'parentComponentTypeId' => $model->componentType->id
            ), true
        );
        $actionPlansActiveCount = ActionPlans::getAppliedPlans($model->componentType->id, $model->id, false)->itemCount;

        $this->controller->render('view2', array(
			'model'=>$model,
            'componentTypeId' => ComponentTypes::RECRUITS,
            'Appointment' => $model->appointment,
            'actionPlansContent' => $actionPlansContent,
            'tasksContent' => $tasksContent,
            'tasksCount' => $tasksProvider->totalItemCount,
		));
	}
}