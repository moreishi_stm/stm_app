<?php

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class AddAction extends CAction {

		public function run($contactId)
        {
			$this->controller->title = 'Add Recruit';
			$model = $this->controller->baseModel;
			$model->source_id = ($_GET['source']) ? $_GET['source'] : '';
			$model->contact_id = $contactId;

            //see if recruit already exists for the contact
            if($existingRecruit = Recruits::model()->findByAttributes(array('contact_id'=>$contactId))) {
                Yii::app()->user->setFlash('success', 'Recruit already exists.');
                $this->controller->redirect('/'.Yii::app()->controller->module->id.'/recruits/' . $existingRecruit->id);
            }

            $assignmentValidated = true;
            // Process the Assignments model
            if (isset($_POST['Assignments'])) {
                $assignmentValidated = true;

                $recruitAssignments = array();
                foreach ($_POST['Assignments'] as $i => $assignmentData) {

                    $Assignment = new Assignments('addTransaction');
                    $Assignment->attributes = $assignmentData;

                    if(!$Assignment->validate(array('assignee_contact_id','assignment_type_id'))) {
                        Yii::app()->user->setFlash('error', current($Assignment->getErrors())[0]);
                        $assignmentValidated = false;
                    }
                    array_push($recruitAssignments,$Assignment);
                    $model->assignments = $recruitAssignments;
                }
            }


            $Appointment = new Appointments;
            $Appointment->component_type_id = ComponentTypes::RECRUITS;

            if (isset($_POST['Recruits'])) {
				$model->attributes = $_POST['Recruits'];

                // Process the Appointments model
                $appointmentValidated = false;
                if (isset($_POST['Appointments'])) {
                    $appointmentIsEmpty = true;
                    foreach ($_POST['Appointments'] as $key => $value) {
                        if (!empty($value) && !in_array($key, array('googleCalendarData', 'google_calendar_id', 'google_calendar_event_id','toDelete'))) {

                            $appointmentIsEmpty = false;
                            break;
                        }
                    }

                    if ($appointmentIsEmpty == false) {
                        $Appointment->attributes = $_POST['Appointments'];
                        $appointmentValidated = $Appointment->validate(array(
                                'component_type_id',
                                'set_by_id',
                                'met_by_id',
                                'met_status_ma',
                                'set_on_datetime',
                                'set_for_datetime',
                                'location_ma'
                            )
                        );
                    } else {
                        $appointmentValidated = true;
                    }
                }

				if ($appointmentValidated && $assignmentValidated && $model->save()) {
                    if ($model->typesCollection) {
                        $typesFlipped = array_flip($model->typesCollection);
                        foreach ($model->recruitTypeLu as $RecruitTypeLu) {
                            if (!in_array($RecruitTypeLu->recruit_type_id, $model->typesCollection)) {
                                $RecruitTypeLu->delete();
                            } else {
                                unset($typesFlipped[$RecruitTypeLu->recruit_type_id]);
                            }
                        }

                        $model->typesCollection = array_flip($typesFlipped);

                        // Attach the contact types to the newly contact
                        foreach ($model->typesCollection as $attributeId) {
                            $RecruitTypeLu = new RecruitTypeLu;
                            $RecruitTypeLu->recruit_id = $model->id;
                            $RecruitTypeLu->recruit_type_id = $attributeId;
                            $RecruitTypeLu->save();
                        }
                    }

                    if (!$appointmentIsEmpty) {
                        $Appointment->component_id = $model->id;
                        $Appointment->save(false); // Already validated
                    }

                    if (isset($_POST['Assignments'])) {
                        foreach ($_POST['Assignments'] as $i => $assignmentData) {
                            $assignment = new Assignments;
                            $assignment->attributes = $assignmentData;
                            $assignment->component_type_id = ComponentTypes::RECRUITS;
                            $assignment->component_id = $model->id;
                            if (!$assignment->save(false)) {
                                Yii::app()->user->setFlash('error', 'Error: Assignment did not save properly.');
                            }
                        }
                    }
					if(!empty($model->id) && !empty($_POST["Recruits"]["referred_by_ids"])){
						foreach ($_POST["Recruits"]["referred_by_ids"] as $refferredId){
							if(!empty($refferredId)){
								$Criteria = new CDbCriteria;
								$Criteria->select = "*";
								$Criteria->addColumnCondition(array(
									"component_type_id" => ComponentTypes::RECRUITS,
									"component_id" => $model->id,
									"assignment_type_id" => Recruits::REFERRER,
									"assignee_contact_id" => $refferredId
								));

								$referModel  = Assignments::model()->find($Criteria);
								if(!$referModel){
									$referModel = new Assignments();
								}
								$referModel->attributes = array(
									"component_type_id" => ComponentTypes::RECRUITS,
									"component_id" => $model->id,
									"assignment_type_id" => Recruits::REFERRER,
									"assignee_contact_id" => $refferredId,
									"updated_by" => Yii::app()->user->id
								);

								$referModel->save();
							}
						}
					}

					Yii::app()->user->setFlash('success', 'Successfully added New Recruit.');
					$this->controller->redirect('/'.Yii::app()->controller->module->id.'/recruits/' . $model->id);
				}
			}

			$this->controller->render('form', array(
					'model' => $model,
                    'Appointment'=>$Appointment,
                )
			);
		}
	}