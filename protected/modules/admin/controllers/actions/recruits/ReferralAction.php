<?php
//Yii::import('front_module.controllers.actions.form.AbstractFormSubmitAction', true);

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	class ReferralAction extends CAction
    {
		public function run()
        {
			$this->controller->title = 'Send Recruit Referral';

            $FormFields       = new FormFields;
            $SubmissionValues = new FormSubmissionValues(Forms::RECRUIT_REFERRAL);

			$this->controller->render('formReferral', array(
                    'SubmissionValues' =>$SubmissionValues,
                    'FormFields' =>$FormFields,
                )
			);
		}
	}