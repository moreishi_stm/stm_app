<?php

class AddAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Meeting Notes';

		$model = new MeetingNotes();

        $model->met_by_id = Yii::app()->user->id;
        $model->attributes=$_POST['MeetingNotes'];

        if (isset($_POST['MeetingNotes'])) {

            $model->attributes=$_POST['MeetingNotes'];
            if($model->save()) {

                Yii::app()->user->setFlash('success', 'Successfully added Meeting Notes.');
                $this->controller->redirect('/'.Yii::app()->controller->module->name.'/meetingNotes');
            }
        }

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}
