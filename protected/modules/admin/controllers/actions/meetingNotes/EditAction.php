<?php

class EditAction extends CAction {
	/**
	 * Manages models
	 */
	public function run($id) {
		$this->controller->title = 'Meeting Notes';

		$model = MeetingNotes::model()->findByPk($id);

        if (isset($_POST['MeetingNotes'])) {

			$model->attributes=$_POST['MeetingNotes'];
            if($model->save()) {

                Yii::app()->user->setFlash('success', 'Successfully updated Meeting Notes.');
                $this->controller->redirect('/'.Yii::app()->controller->module->name.'/meetingNotes');
            }
        }

		$this->controller->render('form',array(
			'model'=>$model
		));
	}
}
