<?php

class ListAction extends CAction {
	/**
	 * Manages models
	 */
	public function run() {
		$this->controller->title = 'Meeting Notes';

		$model = new MeetingNotes();
		$model->unsetAttributes();  // clear any default values

        if (isset($_GET['MeetingNotes'])) {
			$model->attributes=$_GET['MeetingNotes'];
        }

		$this->controller->render('list',array(
			'model'=>$model
		));
	}
}
