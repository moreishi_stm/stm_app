<?php
include_once(Yii::getPathOfAlias('admin_module.extensions.') . DS . 'SimpleHtmlDom.php');
class CraigslistController extends AdminController
{
    const CRAIGSLIST_URL = 'http://post.craigslist.com/c/:area/H/:category';
//    const CRAIGSLIST_URL = 'http://post.craigslist.com/c/:area/H/:category/?/:subarea';

	public $pageColor = 'blue';
    private $_cgAction;
    private $_cgCrypt;

    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
            'index' =>'stm_app.modules.admin.controllers.actions.craigslist.ListAction',
            'post'  =>'stm_app.modules.admin.controllers.actions.craigslist.PostAction',
            'view'  =>'stm_app.modules.admin.controllers.actions.craigslist.ViewAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
	}

    /**
     * Returns the property model for searching purposes
     * @param null $tableName
     * @return MlsProperties
     */
    public function getPropertyModel($tableName=null) {

        $tableName = ($tableName === null) ? Yii::app()->user->board->prefixedName : $tableName;
        $property = MlsProperties::model($tableName);

        return $property;
    }

    /**
     * Returns the posting form content from craigslist based on the area and category parameters
     * @return mixed
     */
    public function getCraigslistPostContent($url=null, $postString = null) {
        if($url) {
            $craigslistPostUrl = $url;

        } else {
            $url = self::CRAIGSLIST_URL;
            $variables = array(
                ':area' => Yii::app()->getUser()->getAccount()->craigslist_area,
                ':category' => Yii::app()->getUser()->getAccount()->craigslist_category,
                //            ':subarea' => Yii::app()->getUser()->getAccount()->craigslist_subarea,
            );
            $craigslistPostUrl = strtr($url, $variables);
        }


//        $curl = Yii::app()->curl;
//        if(!YII_DEBUG) {
//            $curl->setProxy('http://cgp.seizethemarket.net', 8080);
//            $curl->setProxyLogin('stm', 'Kaqufr3y');
//        }
//
//        try{
//            $craigslistContent = $curl->run($craigslistPostUrl, false, $postString);
//        }
//        catch(Exception $e) {
//            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Craigslist controller had error: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
//        }

        $ch = curl_init();
        if(!YII_DEBUG) {
            curl_setopt($ch, CURLOPT_PROXY, 'http://cgp.seizethemarket.net');
            curl_setopt($ch, CURLOPT_PROXYPORT, '8080');
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'stm:Kaqufr3y');
        }
        curl_setopt($ch, CURLOPT_URL, $craigslistPostUrl);
//        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if($postString) {
            $postString = 'go=I will abide by these guidelines&cryptedStepCheck=';
        }
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);

        // Perform the request, and save content to $result
        $craigslistContent = curl_exec($ch); //@not working properly... need to be able to pass post variables correctly

        $craigslistContent = $this->prepCraigslistContent($craigslistContent);
        return $craigslistContent;
    }

    /**
     * Craigslist has made some modifications so we need to remove two of their faulty scripts in order for ours to work.
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param $craigslistContent
     *
     * @return mixed
     */
    private function prepCraigslistContent($craigslistContent) {

        $scriptsToRemove = array(
            'formats.js',
            'general.min.js',
            'suggestions.min.js',
            'postingForm.min.js',
//            'compatibility.min.js',
        );
        $excludeStripTagRegexTpl = '/<script[^>]*src="[^\"]*(:scriptsToExcludeList)[^\"]*".*>(.*)<\/script>/i';
        $excludeStripTagRegex = strtr($excludeStripTagRegexTpl, array(
                                                                ':scriptsToExcludeList' => implode('|', $scriptsToRemove),
                                                                ));
        $craigslistContent = preg_replace($excludeStripTagRegex, '', $craigslistContent);

        $replaceCount = 1;
        //sometimes craiglist jquery goes down and it breaks this script
        $craigslistContent = str_replace('</script>','</script><script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>',$craigslistContent, $replaceCount);
        return $craigslistContent;
    }

    public function printCraigslistButton($property) {
        if(!$this->_cgAction || !$this->_cgCrypt) {
            $craigslistPostContent = $this->getCraigslistPostContent();
            preg_match('/post.craigslist.org\/([^\n\"]*)/s', $craigslistPostContent, $matchAction);
            $cgAction = $matchAction[1];
            $this->_cgAction = 'https://post.craigslist.org/'.$cgAction;

            $cryptHtml = str_get_html($craigslistPostContent);
            $cryptField = $cryptHtml->find('input[name=cryptedStepCheck]');
            if(count($cryptField)) {
                $this->_cgCrypt = $cryptField[0]->attr[value];
            }
        }
        $url = Yii::app()->controller->createUrl("/admin/craigslist/post", array("propertyTypeName" => strtolower($property->propertyType->name), "listingId" => $property->listing_id));
        echo '<form method="POST" action="'.$url.'" target="_blank">';
        echo '<input type="hidden" name="cgAction" value="'.$this->_cgAction.'">';
        echo '<input type="hidden" name="cgCrypt" value="'.$this->_cgCrypt.'">';

        echo '<input type="submit" value="Post to Craigslist" class="button gray icon i_stm_add grey-button">';
        echo '</form>';
    }
}