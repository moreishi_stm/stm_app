<?php

/**
 * Lead Route Options Controller
 *
 * Angular based app to manage lead route options
 */
class LeadRouteOptionsController extends AdminController
{
    /**
     * Send JSON
     *
     * Outputs JSON data and terminates application
     * @param array $data Array of data
     */
    protected function _sendJson($data)
    {
        // Set header and output JSON data
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }

    /**
     * Action Index
     *
     * Default page load action
     * @param int $id The lead route ID to load stuff for
     * @return void
     */
    public function actionIndex($id)
    {
        // Render view
        $this->render('index', array(
            'lead_route_id' =>  $id
        ));
    }

    /**
     * Load Option values
     *
     * Used to load in lead route option values and all available options, used on initial load pretty much
     * @param int $id The lead route to load by
     * @return void
     */
    public function actionLoadOptionValues($id)
    {
        // Get lead route options
        $leadRouteOptions = LeadRouteOptions::model()->findAll();

        // Get lead route option values
        $leadRouteOptionValues = LeadRouteOptionValues::model()->findAllByAttributes(array(
            'lead_route_id' =>  $id
        ));

        //
        $optionValues = array();
        foreach($leadRouteOptionValues as $leadRouteOptionValue) {



        }

        // Model response data
        $options = array();
        foreach($leadRouteOptions as $leadRouteOption) {
            $options[] = $leadRouteOption->attributes;
        }

        // Send JSON response
        $this->_sendJson(array(
            'leadRouteId'   =>  $id,
            'leadRouteOptions'       =>  $options,
            'leadRouteOptionValues'  =>  $leadRouteOptionValues
        ));
    }

    /**
     * Action Load Values For Option
     *
     * Returns the values for a chosen option
     * @return void
     */
    public function actionLoadValuesForOption()
    {
        // Get POST variables
        $leadRouteId = Yii::app()->request->getParam('leadRouteId');
        $optionId = Yii::app()->request->getParam('optionId');

        // Get lead route option values
        $leadRouteOptionValues = LeadRouteOptionValues::model()->findAllByAttributes(array(
            'lead_route_id'         =>  $leadRouteId,
            'lead_route_option_id'  =>  $optionId
        ));

        // Model response data
        $results = array();
        foreach($leadRouteOptionValues as $leadRouteOptionValue) {
            $results[] = $leadRouteOptionValue->attributes;
        }

        // Send JSON response
        $this->_sendJson(array(
            'leadRouteId'       =>  $leadRouteId,
            'optionId'          =>  $optionId,
            'results'           =>  $results
        ));
    }

    /**
     * Action Save
     *
     * Performs saving of option values
     * @return void
     */
    public function actionSave()
    {
        // Parse JSON post data (this is how angular likes to post data...
        $postData = json_decode(file_get_contents('php://input'), true);

        // Get POST variables
        $leadRouteId = Yii::app()->request->getParam('leadRouteId');
        $optionId = Yii::app()->request->getParam('optionId');

        // Get lead route option values
        $leadRouteOptionValues = LeadRouteOptionValues::model()->findAllByAttributes(array(
            'lead_route_id'         =>  $leadRouteId,
            'lead_route_option_id'  =>  $optionId
        ));

        // Organize db records as a dictionary lookup by ID
        $records = array();
        foreach($leadRouteOptionValues as $leadRouteOptionValue) {
            $records[$leadRouteOptionValue->id] = $leadRouteOptionValue;
        }

        // Process new and existing records
        foreach($postData['optionValues'] as $optionValue) {

            // Get the new or existing record for the current item
            /** @var LeadRouteOptionValues $record */
            $record = array_key_exists($optionValue['id'], $records) ? $records[$optionValue['id']] : new LeadRouteOptionValues();

            // Set values
            $record->lead_route_option_id = $postData['option']['id'];
            $record->lead_route_id = Yii::app()->request->getParam('leadRouteId');
            $record->value = $optionValue['value'];
            $record->added = $record->added ? $record->added : new CDbExpression('NOW()');

            // Attempt to save the record
            if(!$record->save()) {
                throw new Exception('Error! Unable to save record! ' . print_r($record->getErrors(), true));
            }
        }

        // Delete records where values are empty (no reason to keep them around)
        foreach($postData['deletes'] as $delete) {

            // Make sure we don't have any funny business
            if(!array_key_exists($delete, $records)) {
                throw new Exception('Error, unable to delete non existent record!');
            }

            // Delete the item
            if(!$records[$delete]->delete()) {
                throw new Exception('Error, unable to delete item ID: "' . $delete . '"!');
            }
        }

        // Send JSON response
        $this->_sendJson(array(
            'status'       =>  'success'
        ));
    }
}