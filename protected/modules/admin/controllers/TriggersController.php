<?php
class TriggersController extends AdminController
{
	public $pageColor = 'blue';

	/**
	 * @return array action filters
	 */
	public function filters()
    {
		return CMap::mergeArray(parent::filters(), array());
	}

    public function actionIndex()
    {
        $this->title = 'Triggers List';

        $model = new Triggers();
        $model->unsetAttributes();
        $model->status_ma=1;

        if (isset($_GET['Triggers'])) {
            $model->attributes=$_GET['Triggers'];
        }

        $this->render('list',array(
                'model'=>$model
            ));
    }


    public function actionAdd()
    {
        $this->title = 'Add Trigger';
        $model = new Triggers();
        $model->status_ma=1;

        $currentActionPlanList = array();

        if (isset($_POST['Triggers'])) {
            $model->attributes = $_POST['Triggers'];
            $model->account_id = Yii::app()->user->accountId;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Added Trigger.');
                $this->redirect(array('index'));
            }
            else {
                $currentActionPlanList = CHtml::listData(ActionPlans::model()->findAllByAttributes(array('component_type_id'=>$model->component_type_id)), 'id','name');
            }
        }

        $this->render('form',array(
                'model'=>$model,
                'currentActionPlanList' => $currentActionPlanList,
            ));
    }

    public function actionEdit($id)
    {
        $this->title = 'Edit Trigger';
        $model = Triggers::model()->findByPk($id);

        $currentActionPlanList = CHtml::listData(ActionPlans::model()->findAllByAttributes(array('component_type_id'=>$model->component_type_id)), 'id','name');

        if (isset($_POST['Triggers'])) {
            $model->attributes = $_POST['Triggers'];
            $model->account_id = Yii::app()->user->accountId;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Updated Trigger.');
                $this->redirect(array('index'));
            }
        }

        $this->render('form',array(
                'model'=>$model,
                'currentActionPlanList' => $currentActionPlanList,
            ));
    }

    public function actionDelete($id)
    {
        $model = Triggers::model()->findByPk($id);
        if (isset($_POST['Triggers'])) {
            $model->attributes = $_POST['Triggers'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            if (!$model->delete()) {
                $errors = $model->getErrors();
                // returns the first error message
                echo CJSON::encode(array('status'=>'error', 'message'=> current($errors)[0]));
            }
            else {
                echo CJSON::encode(array('status'=>'success'));
            }

            Yii::app()->end();
        }
    }
}
