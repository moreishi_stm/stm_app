<?php
class SmsTemplatesController extends AdminController {
    public $pageColor = 'blue';
	public $CachedTransactionType;

    public function actions() {
    	return array (
			'index' => 'stm_app.modules.admin.controllers.actions.smsTemplates.ListAction',
			'view' => 'stm_app.modules.admin.controllers.actions.smsTemplates.ViewAction',
			'edit' => 'stm_app.modules.admin.controllers.actions.smsTemplates.EditAction',
			'add' => 'stm_app.modules.admin.controllers.actions.smsTemplates.AddAction',
    	);
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return CMap::mergeArray(parent::filters(), array(
                'stmAccessControl' => array(
                    'class' => 'stm_app.modules.admin.components.filters.StmAccessControl',
                    'guestAllowedActions' => array(
                        'view',
                    ),
                ), // perform access control
            ));
    }

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new SmsTemplates;
	}
}
