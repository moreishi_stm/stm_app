<?php
	class TransactionsController extends AdminController {

		// Holds the TransactionTypes model from getTransactionType so we do not need to make multiple DB calls
		public $CachedComponentType;
		public $formPartialView;

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
			$transactionType = Yii::app()->request->getQuery('transactionType');

			$this->baseModel = Transactions::newInstance($transactionType);

			switch ($this->baseModel->transactionType) {
				case Transactions::BUYERS:
					$this->pageColor = 'yellow';
					$this->formPartialView = '_formBuyers';
					break;
				case Transactions::SELLERS:
					$this->pageColor = 'green';
					$this->formPartialView = '_formSellers';
					break;
				case Transactions::LISTINGS:
					$this->pageColor = 'green';
					$this->formPartialView = '_formListings';
					break;
			}
		}

		public function actions() {
			// return external action classes, e.g.:
			return array(
				'expiredCancel' => 'stm_app.modules.admin.controllers.actions.transactions.ExpiredCancelAction',
                'list' => 'stm_app.modules.admin.controllers.actions.transactions.ListAction',
                'leads' => 'stm_app.modules.admin.controllers.actions.transactions.LeadsAction',
				'index' => 'stm_app.modules.admin.controllers.actions.transactions.ListAction',
                'my' => 'stm_app.modules.admin.controllers.actions.transactions.ListAction',
				'addAddress' => 'stm_app.modules.admin.controllers.actions.transactions.AddAddressAction',
				'addFind' => 'stm_app.modules.admin.controllers.actions.transactions.AddFindAction',
				'abc' => 'stm_app.modules.admin.controllers.actions.transactions.ABCListAction',
                'reverseProspect' => 'stm_app.modules.admin.controllers.actions.transactions.ReverseProspectAction',
				'reverseProspectCount' => 'stm_app.modules.admin.controllers.actions.transactions.ReverseProspectCountAction',
				'view' => 'stm_app.modules.admin.controllers.actions.transactions.ViewAction',
				'add' => 'stm_app.modules.admin.controllers.actions.transactions.AddAction',
				'edit' => 'stm_app.modules.admin.controllers.actions.transactions.EditAction',
				'merge' => 'stm_app.modules.admin.controllers.actions.transactions.MergeAction',
				'dashboard' => 'stm_app.modules.admin.controllers.actions.transactions.DashboardAction',
				'pipeline' => 'stm_app.modules.admin.controllers.actions.transactions.PipelineAction',
				'comingsoon' => 'stm_app.modules.admin.controllers.actions.transactions.ComingSoonAction',
				'listings' => 'stm_app.modules.admin.controllers.actions.transactions.ListingsAction',
			);
		}
		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array(
				array(
					'admin_module.components.filters.GridViewHandler',
				),
			));
		}

		/**
		 * Return a instance of the transaction type model and validate the request
		 *
		 * @return Instance of the TransactionTypes model.
		 */
		public function getComponentType() {
			if ($this->CachedComponentType) {
				return $this->CachedComponentType;
			}

			$componentTypeName = Yii::app()->request->getQuery('transactionType');
			if (!$componentTypeName || ($ComponentType = ComponentTypes::getByName($componentTypeName)) === null) {
				throw new CHttpException(404, 'The ' . __CLASS__ . ' must specify a valid transaction type.');
			}

			$this->CachedComponentType = $ComponentType;

			return $this->CachedComponentType;
		}

		/**
		 * Performs the AJAX validation.
		 *
		 * @param CModel the model to be validated
		 */
		protected function performAjaxValidation($model) {
			if (isset($_POST['ajax']) && $_POST['ajax'] === 'task-form') {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}

		public function getSearchModel($transactionType) {
			$model = Transactions::newInstance($transactionType, 'search');
			$modelClass = get_class($model);

            if(isset($_REQUEST['searchNoSavedSearches'])) {
                $model->searchNoSavedSearches = $_GET['searchNoSavedSearches'];
            }

            if(isset($_REQUEST['searchNoActivity'])) {
                $model->searchNoActivity = $_GET['searchNoActivity'];
            }

            if(isset($_REQUEST['searchNoEmails'])) {
                $model->searchNoEmails = $_GET['searchNoEmails'];
            }

            if(isset($_REQUEST['searchNoCalls'])) {
                $model->searchNoCalls = $_GET['searchNoCalls'];
            }

            if(isset($_REQUEST['dateSearchFilter'])) {
                $model->dateSearchFilter = $_GET['dateSearchFilter'];
            }

            if(isset($_REQUEST['fromSearchDate'])) {
                $model->fromSearchDate = $_GET['fromSearchDate'];
            }

            if(isset($_REQUEST['toSearchDate'])) {
                $model->toSearchDate = $_GET['toSearchDate'];
            }

            if(isset($_REQUEST['includeTrash'])) {
                $model->includeTrash = $_GET['includeTrash'];
            }

            if(isset($_REQUEST['assignmentId'])) {
                $model->assignmentId = $_GET['assignmentId'];
            }
                        // used for gridview search box inputs
			if (isset($_REQUEST[$modelClass])) {
				$model->attributes = $_REQUEST[$modelClass];
			}

			$model->contact = new Contacts('search');
			$model->contact->unsetAttributes();

			if (isset($_REQUEST['Contacts'])) {
				$model->contact->attributes = $_REQUEST['Contacts'];
			}

			$model->contact->emails = new Emails('search');
			$model->contact->emails->unsetAttributes();

			if (isset($_REQUEST['Emails'])) {
				$model->contact->emails->attributes = $_REQUEST['Emails'];
			}

			$model->contact->phones = new Phones('search');

			if (isset($_REQUEST['Phones'])) {
				$model->contact->phones->attributes = $_REQUEST['Phones'];
			}

			return $model;
		}

		public function getTransactionSearchProvider() {
			$transactionType = Yii::app()->request->getQuery('transactionType');
			$model = $this->getSearchModel($transactionType);
			$DataProvider = $model->search();


			return $DataProvider;
		}

		public function _getGridViewTransactionGrid() {
			$DataProvider = $this->getTransactionSearchProvider();
			if($sort = Yii::app()->request->getQuery('sort')) {
				if($sort == 'added') {
					$DataProvider->criteria->order = 't.added desc';
				}
			}

			$transactionType = Yii::app()->request->getQuery('transactionType');
			$partialView = ($transactionType == Transactions::BUYERS) ? '_listBuyers' : '_listSellers';
			$this->renderPartial($partialView, array('DataProvider' => $DataProvider));
		}

		public function _getGridExpiredCancel() {
			$DataProvider = $this->getTransactionSearchProvider();
			$Criteria = new CDbCriteria;
			$Criteria->addInCondition('transaction_status_id', array(TransactionStatus::EXPIRED_ID, TransactionStatus::CANCELLED_ID));
			$DataProvider->criteria->mergeWith($Criteria);

			$this->renderPartial('_listSellersExpiredCancel', array('DataProvider' => $DataProvider));
		}

		public function _getGridViewReverseProspect()
        {
			$DataProvider = $this->getTransactionSearchProvider();

            if (!isset($_GET['MlsProperties']) || empty($_GET['MlsProperties'])) {
                // this forces no results for when the page first loads
                $DataProvider->criteria->mergeWith(array('condition'=>'contact_id=0'));
            }
			else{
				// Create a instance of mls properties and set variables
				$tableName = Yii::app()->user->board->prefixedName;
				$MlsProperties = MlsProperties::model($tableName);
				$MlsProperties->attributes = $_GET['MlsProperties'];

                // Store a collection of listings to check the viewed page data for matches
                $listingIds = array();

                // Add the listing id we already know about
				if ($MlsProperties->listing_id) {
					array_push($listingIds, $MlsProperties->listing_id);
				}
                elseif(!(empty($MlsProperties->zip) && empty($MlsProperties->street_name) && empty($MlsProperties->min_price) && empty($MlsProperties->max_price))) {
                    $conditions = '';
                    if($MlsProperties->zip) {
                        $conditions .= ($conditions)? ' AND ' : '';
                        $conditions .= ' zip="'.$MlsProperties->zip.'"';
                    }
                    if($MlsProperties->street_name) {
                        $conditions .= ($conditions)? ' AND ' : '';
                        $conditions .= 'street_name="'.$MlsProperties->street_name.'"';
                    }
                    if($MlsProperties->price_min) {
                        $conditions .= ($conditions)? ' AND ' : '';
                        $conditions .= 'price>='.Yii::app()->format->formatInteger($MlsProperties->price_min);
                    }
                    if($MlsProperties->price_max) {
                        $conditions .= ($conditions)? ' AND ' : '';
                        $conditions .= 'price<='.Yii::app()->format->formatInteger($MlsProperties->price_max);
                    }

                    $listingIds = Yii::app()->stm_mls->createCommand("SELECT DISTINCT listing_id FROM {$tableName}_properties WHERE {$conditions}")->queryColumn();
				}

                if($MlsProperties->reverseProspectDays) {
                    $dateCondition = 'added >="'.date("Y-m-d", strtotime("-{$MlsProperties->reverseProspectDays} month")).'"';
                }

                if(!empty($listingIds)) {

                    $listingIds = implode('","',$listingIds);
                    $listingIds = '"'.$listingIds.'"';

                    $contactIds = Yii::app()->db->createCommand("SELECT distinct contact_id as contact_id FROM `viewed_pages` WHERE listing_id IN({$listingIds}) AND {$dateCondition}")->queryColumn();

					// Restrict the contacts to ones that have viewed the selected pages
					$contactIdCriteria = new CDbCriteria;
					$contactIdCriteria->addInCondition('t.contact_id', $contactIds);
					$DataProvider->criteria->mergeWith($contactIdCriteria);

                    $count = Transactions::model()->count($DataProvider->criteria);
                    $DataProvider->setTotalItemCount($count);
                    $DataProvider->pagination->setItemCount($count);
				} else {
					$DataProvider->criteria->mergeWith(array('condition'=>'contact_id=0'));
				}

			}
            $DataProvider->criteria->mergeWith(array('condition'=>'t.is_deleted<>1'));

			$this->renderPartial('_listBuyersReverse', array('DataProvider' => $DataProvider));
		}
	}
