<?php
class TransactionTagsController extends AdminController
{
	public $pageColor = 'purple';

    public function actions()
    {
    	return array(
			'index'       =>'stm_app.modules.admin.controllers.actions.transactionTags.ListAction',
			'edit'        =>'stm_app.modules.admin.controllers.actions.transactionTags.EditAction',
			'add'         =>'stm_app.modules.admin.controllers.actions.transactionTags.AddAction',
            'delete'      =>'stm_app.modules.admin.controllers.actions.transactionTags.DeleteAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
        $this->baseModel = new TransactionTags;
		$this->displayName = 'Transaction Tags';
	}
}