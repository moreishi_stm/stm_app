<?php
class WidgetsController extends AdminController
{
	public $pageColor = 'teal';
    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
			'index'  =>'stm_app.modules.admin.controllers.actions.widgets.ListAction',
			'add'    =>'stm_app.modules.admin.controllers.actions.widgets.AddAction',
            'view'   =>'stm_app.modules.admin.controllers.actions.widgets.ViewAction',
            'addGroup' =>'stm_app.modules.admin.controllers.actions.widgets.AddGroupAction',
			'editGroup' =>'stm_app.modules.admin.controllers.actions.widgets.EditGroupAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new CmsWidgets;
	}
}