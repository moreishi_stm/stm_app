<?php

/**
 * Class PermissionsController
 */
class PermissionsController extends AdminController
{

    public $pageColor = 'purple';

    /**
     * @return array
     */
    public function actions()
    {

        return array(
            'index'    => 'admin_module.controllers.actions.permissions.ListAction',
            'list'     => 'admin_module.controllers.actions.permissions.ListAction',
            'add'      => 'admin_module.controllers.actions.permissions.AddAction',
            'edit'     => 'admin_module.controllers.actions.permissions.EditAction',
            'delete'   => 'admin_module.controllers.actions.permissions.DeleteAction',
            'assign'   => 'admin_module.controllers.actions.permissions.AssignAction',
            'generate' => 'admin_module.controllers.actions.permissions.GenerateAction',
            'deleteParentChild' => 'admin_module.controllers.actions.permissions.DeleteParentChildAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return CMap::mergeArray(parent::filters(), array());
    }

//    public function actionIndex()
//    {
//        $this->title = 'Builders';

//        $model = new Builders;
//        $model->unsetAttributes(); // clear any default values
//
//        if (isset($_GET['Builders'])) {
//            $model->attributes = $_GET['Builders'];
//        }
//
//        $this->render('index', array(
//                'model' => $model,
//            )
//        );
//    }
}