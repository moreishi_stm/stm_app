<?php
class YardSignsController extends AdminController
{
	public $pageColor = 'blue';

    public function actionIndex()
    {
        $this->title = 'Yard Sign Inventory';

        $model = new YardSigns('search');
        $model->unsetAttributes();  // clear any default values

        if($_COOKIE['YardSigns']) {
            $model->attributes = $_COOKIE['YardSigns'];
        }

        if (isset($_GET['YardSigns'])) {
            $model->attributes=$_GET['YardSigns'];

//            foreach($_GET['ActionPlans'] as $field => $value) {
//                setcookie("ActionPlans[$field]", $value, time()+3600*24*30);
//            }
        }

        $this->render('list',array(
                'model'=>$model
            ));
    }


    public function actionAdd()
    {
        $this->title = 'Add Yard Signs';

        $model = new YardSigns();

        if (isset($_POST['YardSigns'])) {
            $model->attributes = $_POST['YardSigns'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added Yard Sign.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/yardSigns');
            }
        }

        $this->render('form', array(
                'model' => $model,
            ));
    }

    public function actionEdit($id)
    {
        $this->title = 'Edit Yard Signs';

        $model = YardSigns::model()->findByPk($id);

        if (isset($_POST['YardSigns'])) {
            $model->attributes = $_POST['YardSigns'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated Yard Sign.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/yardSigns');
            }
        }

        $this->render('form', array(
                'model' => $model,
            ));
    }
}