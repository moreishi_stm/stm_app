<?php
// Includes we need
include_once(Yii::getPathOfAlias('admin_module.components.') . '/StmAws/S3/FileManagement.php');

/**
 * Image ManagerController
 */
class ImageManagerController extends AdminController
{
    /**
     * S3 Client
     *
     * Holds an instance to the AWS s3 client
     * @var Brilliant_Global_Amazon_S3_FileManagement_Site
     */
    protected $_s3Client;

    /**
     * Allowed Formats
     *
     * Formats that a user is allowed to upload with file extensions to map to.
     * @var array (Format Constant => file extension)
     */
    protected $allowedFormats = array(
        'GIF'	=>	'.gif',
        'JPEG'	=>	'.jpg',
        'PNG'	=>	'.png',
        'BMP'	=>	'.bmp'
    );

	protected $_baseUrl = 'http://cdn.seizethemarket.com/';
	protected $_baseBucket = 'cdn.seizethemarket.com';
	
    /**
     * Domain
     *
     * @var Domains reference to domain record
     */
    protected $_domain;

    /**
     * Init
     */
    public function init()
    {
        // Set the layout for image manager
        $this->layout = 'imageManager';

		$cdnUrl = Yii::app()->request->getParam('cdnUrl', null);
		if(!empty($cdnUrl)) {
			$this->_baseUrl = $cdnUrl;
		}
		
		$cdnBucket = Yii::app()->request->getParam('cdnBucket', null);
		if(!empty($cdnBucket)) {
			$this->_baseBucket = $cdnBucket;
		}
		
        // Use the file manager object for S3
        $this->_s3Client = new \StmAws\S3\FileManagement($this->_baseBucket);

        // Get UUID
        $this->_domain = Domains::model()->findByAttributes(array(
            'account_id'    =>  Yii::app()->user->account_id,
            'is_primary'    =>  '1'
        ));
    }

    /**
     * Get Requested Path
     *
     * Securely creates a full path from user inputted directory
     * @return type
     */
    protected function getRequestedPath($dirAppend = '')
    {
        // Construct actual directory
        $directory = ltrim(Yii::app()->request->getParam('directory'), '/');
        //$directory = sprintf($this->_domain->name . '/content/cms%s' . ($directory == '/' ? '' : '/' . $directory), $dirAppend);
        //$directory = preg_replace('/\/{2,}/u', '\/', $directory);
        return $directory;
    }

    /**
     * Generate Safe Filename
     *
     * Generates a safe file name, removes things we don't allow.
     * @param string $file The filename to filter
     * @return string Filtered filename
     */
    protected function generateSafeFilename($file)
    {
        return preg_replace('/\-+/', '-', preg_replace('/[^a-zA-Z0-9\-_]/', '-', preg_replace('/\.[^\.]*$/', '', $file)));
    }

    /**
     * Send JSON
     *
     * Outputs JSON data and terminates application
     * @param array $data Array of data
     */
    protected function _sendJson($data)
    {
        // Set header and output JSON data
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }

    /**
     * Action Index
     *
     * Default page load action
     * @return void
     */
    public function actionIndex()
    {
        // Render the view
        $this->render('index', array(
            'config'    =>  array(
                'outputImageLocation'   =>  $this->_baseUrl . 'site-files/'.Yii::app()->user->clientId,
                'imageLocation'         =>  $this->_baseUrl . 'site-files/'.Yii::app()->user->clientId,
                'mce'                   =>  false,
                'eleId'                 =>  false
            )
        ));
    }

    /**
     * List Action
     *
     * Returns directory contents
     * @return void
     */
    public function listAction()
    {
        // Get full requested path
        $fullPath = $this->getRequestedPath();

        // If we are using S3 as our CDN, retrieve files from there instead
        $results = array();

        // Get directory listing
        $directories = $this->_s3Client->listDirectoryContents($fullPath);
        foreach($directories as $directory) {
            $results[] = array(
                'path'			=>	'/' . $directory['Name'],
                'isDirectory'	=>	$directory['IsDirectory']
            );
        }

        // Output results
        $this->_helper->json(array(
            'status'	=>	'success',
            'results'	=>	$results
        ));
    }

    /**
     * List Action
     *
     * Returns directory contents
     * @return void
     */
    public function actionList()
    {
        // Get full requested path
        $fullPath = $this->getRequestedPath();

        // If we are using S3 as our CDN, retrieve files from there instead
        $results = array();

        // Get directory listing
        $directories = $this->_s3Client->listDirectoryContents($fullPath);
        foreach($directories as $directory) {
            $results[] = array(
                'path'			=>	'/' . $directory['Name'],
                'isDirectory'	=>	$directory['IsDirectory']
            );
        }

        // Output results
        $this->_sendJson(array(
            'status'	=>	'success',
            'results'	=>	$results
        ));
    }

    /**
     * Create Directory Action
     *
     * Used to create a directory
     * @return void
     */
    public function actionCreatedirectory()
    {
        // Source photos directory
        $newPath = $this->getRequestedPath() . '/' . basename($this->generateSafeFilename(Yii::app()->request->getParam('name')));
        if(!$this->_s3Client->createDirectory($newPath)) {
            $this->_helper->json(array(
                'status'		=>	'error',
                'errors'		=>	array('An error occurred loading parent working directory, please try again later or contact support.')
            ));
        }

        // Output results
        $this->_sendJson(array(
            'status'		=>	'success'
        ));
    }

    /**
     * Delete Action
     *
     * Used to delete a file or directory
     * @return void
     */
    public function actionDelete()
    {
        // Process deletion for composite photo directories
//        foreach($this->compositeImageDimensions as $dimension) {
//            $this->_s3Client->deleteItem($this->getRequestedPath('-' . $dimension['width'] . 'x' . $dimension['height']));
//        }

        // Process deletion for width photo directories
//        foreach($this->imageWidths as $width) {
//            $this->_s3Client->deleteItem($this->getRequestedPath('-' . $width));
//        }

        // Delete the source photo
		$requestedPath = $this->getRequestedPath();
        $this->_s3Client->deleteItem($requestedPath);

        // Output results
        $this->_sendJson(array(
			'requestedPath' => $requestedPath,
            'results'		=>	true
        ));
		Yii:app()->end();
    }

    /**
     * Upload Action
     *
     * Used to upload a new file
     * @return void
     */
    public function actionUpload()
    {
        // If we don't have a file upload and we tried loading this page...
        if(!array_key_exists('Filedata', $_FILES)) {
            throw new Exception('No file upload detected <pre>'.print_r($_FILES, 1));
        }
		
        // If we had an error, stop right here.
        if($_FILES['Filedata']['error'] !== UPLOAD_ERR_OK) {
            $this->_sendJson(array(
                'status'		=>	'error',
                'errors'		=>	array('Error uploading file with name "' .  $_FILES['Filedata']['name'] . '"')
            ));
        }
        // Verify that the file is a file upload (security measure)
        if(!is_uploaded_file($_FILES['Filedata']['tmp_name'])) {
            throw new Exception('WARNING! Invalid file upload detected!');
        }
        // Attempt to load up ImageMagick
        try {
            $i = new Imagick($_FILES['Filedata']['tmp_name']);

            // Verify image format
            if(!in_array($i->getimageformat(), array_keys($this->allowedFormats))) {
                $this->_sendJson(array(
                    'status'		=>	'error',
                    'errors'		=>	array('Error, invalid image format detected with name "' .  $_FILES['Filedata']['name'] . '"')
                ));
            }
        }
        catch(ImagickException $e) {
            $this->_sendJson(array(
                'status'		=>	'error',
                'errors'		=>	array('Error, invalid image detected with name "' .  $_FILES['Filedata']['name'] . '"')
            ));
        }
        // Retrieve sanitized version of the filename to use
        $target = "site-files".DIRECTORY_SEPARATOR.rtrim($this->getRequestedPath(), '/') . '/' . $this->generateSafeFilename($_FILES['Filedata']['name']) . $this->allowedFormats[$i->getimageformat()];
		
        // Get geometry of input image
        $imageGeometry = $i->getimagegeometry();
		
		$generateThumb = Yii::app()->request->getParam('generatethumb', TRUE);
		$thumbName = "";
		if($generateThumb) {
			$thumbName = "site-files".DIRECTORY_SEPARATOR.rtrim($this->getRequestedPath(), '/') . '/' . "thumb_".$this->generateSafeFilename($_FILES['Filedata']['name']) . $this->allowedFormats[$i->getimageformat()];
			$resizeHeight = Yii::app()->request->getParam('thumbmaxheight', 50);
			$resizeWidth = Yii::app()->request->getParam('thumbmaxwidth', 50);
            $i->resizeimage($resizeWidth, $resizeHeight, imagick::FILTER_LANCZOS, 1);

            // Save new image
            $outFile = '/tmp/image-resizer-' . uniqid();
            $i->writeimage($outFile);
			$this->_s3Client->addFile($outFile, $thumbName);
		}
		
		 // The width we want to constrain to
        $width = null;
		$postedWidth = Yii::app()->request->getParam('maxwidth');
		if(!empty($postedWidth)) {
			$width = (float)$postedWidth;
		}
		
        // Shrink down the image if we need to
        if(!empty($width) && $imageGeometry['width'] > $width) {

            // Perform resize
            $resizeHeight = round(($imageGeometry['height'] * $width) / $imageGeometry['width']);
            $i->resizeimage($width, $resizeHeight, imagick::FILTER_LANCZOS, 1);

            // Save new image
            $outFile = '/tmp/image-resizer-' . uniqid();
            $i->writeimage($outFile);

            // Upload to S3
            $this->_s3Client->addFile($outFile, $target);
        }
        else {
//            $outFile = '/tmp/image-resizer-' . uniqid();
//            $i->writeimage($outFile);

            // Upload to S3
            $this->_s3Client->addFile($_FILES['Filedata']['tmp_name'], $target);
        }
		
        // Output results
        $this->_sendJson(array(
			'basePath' => $this->_baseUrl,
			'image'   => $target,
			'thumb'   => $thumbName,
            'results' => true
        ));
    }
}