<?php
class FeaturedAreasController extends AdminController
{
	public $baseCdnPath;
    public $pageColor = 'purple';
    public function actions()
    {
    	return array(
			'addTypes'    =>'stm_app.modules.admin.controllers.actions.featuredAreas.AddTypesAction',
			'delete' =>'stm_app.modules.admin.controllers.actions.featuredAreas.DeleteAction',
            'add'   =>'stm_app.modules.admin.controllers.actions.featuredAreas.AddAction',
			'edit'  =>'stm_app.modules.admin.controllers.actions.featuredAreas.EditAction',
            'editTypes'  =>'stm_app.modules.admin.controllers.actions.featuredAreas.EditTypesAction',
			'index' =>'stm_app.modules.admin.controllers.actions.featuredAreas.ListAction',
            'types' =>'stm_app.modules.admin.controllers.actions.featuredAreas.ListTypesAction',
			'assignimage' => 'stm_app.modules.admin.controllers.actions.featuredAreas.AssignImagesAction',
			'deleteImage' => 'stm_app.modules.admin.controllers.actions.featuredAreas.DeleteImageAction'
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new FeaturedAreas;
		$this->baseModel->applyAccountScope = false;
		$this->displayName = 'Featured Areas';
        $this->baseCdnPath = (YII_DEBUG) ? 'http://dev.sites.seizethemarket.com/' : "http://sites.seizethemarket.com/";
	}
}