<?php
class BombBombController extends AdminController {

    public $pageColor = 'blue';
    public $CachedTransactionType;

    public function actions() {
        return array(
            'index'        => 'stm_app.modules.admin.controllers.actions.bombBomb.IndexAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return CMap::mergeArray(parent::filters(), array());
    }

    public function actionImportEmailTemplate()
    {
        if(!isset($_GET['bbid'])) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') BombBomb email ID is required.');
        }
        $bbid = $_GET['bbid'];
        $subject = $_GET['subject'];
        $description = $_GET['desc'];

        if(!($exitingTemplate = EmailTemplates::model()->findByAttributes(array('bombbomb_id'=>$bbid)))) {
            $this->redirect('/'.Yii::app()->controller->module->name.'/emailTemplates/add?bbid='.$bbid.'&subject='.$subject.'&desc='.$description);
        }

        $this->render('importEmailTemplate',array('exitingTemplate' => $exitingTemplate));
    }
}
