<?php
class EmailTemplatesController extends AdminController {
    public $pageColor = 'blue';
	public $CachedTransactionType;

    public function actions() {
    	return array (
            'index'            => 'stm_app.modules.admin.controllers.actions.emailTemplates.ListAction',
            'view'             => 'stm_app.modules.admin.controllers.actions.emailTemplates.ViewAction',
            'edit'             => 'stm_app.modules.admin.controllers.actions.emailTemplates.EditAction',
            'add'              => 'stm_app.modules.admin.controllers.actions.emailTemplates.AddAction',
            'delete'           => 'stm_app.modules.admin.controllers.actions.emailTemplates.DeleteAction',
            'deleteAttachment' => 'stm_app.modules.admin.controllers.actions.emailTemplates.DeleteAttachmentAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return CMap::mergeArray(parent::filters(), array(
                'stmAccessControl' => array(
                    'class' => 'stm_app.modules.admin.components.filters.StmAccessControl',
                    'guestAllowedActions' => array(
                        'view',
                    ),
                ), // perform access control
            ));
    }

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new EmailTemplates;
	}
}