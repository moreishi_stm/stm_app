<?php
class MarketTrendsController extends AdminController
{
	public $pageColor = 'blue';
    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
            'index'    =>'stm_app.modules.admin.controllers.actions.marketTrends.ListAction',
            'add'      =>'stm_app.modules.admin.controllers.actions.marketTrends.AddAction',
            'data'     =>'stm_app.modules.admin.controllers.actions.marketTrends.DataAction',
            'edit'     =>'stm_app.modules.admin.controllers.actions.marketTrends.EditAction',
            'view'     =>'stm_app.modules.admin.controllers.actions.marketTrends.ViewAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->displayName = 'Market Trends';
		$this->baseModel = new MarketTrends;
	}
}