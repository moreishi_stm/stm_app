<?php
	class ProjectsController extends AdminController {

		public $pageColor = 'magenta';

		public function actions() {
			return array(
				'add' => 'stm_app.modules.admin.controllers.actions.projects.AddAction',
				'addItem' => 'stm_app.modules.admin.controllers.actions.projects.AddItemAction',
				'items' => 'stm_app.modules.admin.controllers.actions.projects.ItemsAction',
				'view' => 'stm_app.modules.admin.controllers.actions.projects.ViewAction',
				'edit' => 'stm_app.modules.admin.controllers.actions.projects.EditAction',
				'index' => 'stm_app.modules.admin.controllers.actions.projects.ListAction',
			);
		}

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array(

			));
		}

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
			$this->baseModel = new Projects;
		}
	}
