<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

	class TrackingController extends AdminController {

		const GOALS = 'goals';
		const TIME_BLOCKING = 'timeblocking';

        protected $_recruit = NULL;

        protected $_columnLabels = array();

        public $thisMonthStart;
        public $thisMonthEnd;

        protected function _sendJson($data)
        {
            header('Content-type: application/json');
            echo CJSON::encode($data);
            Yii::app()->end();
        }

        protected function _getPipelineData() {
            $this->_recruit->sortByCapStatus = "DESC";
            $this->_recruit->excludeStatusIds = TransactionStatus::ACTIVE_LISTING_ID;

            if(isset($_GET['ajax'])) {
                if(isset($_GET['set_by_id']) && !empty($_GET['set_by_id'])) {

                    $ids = array();
                    foreach($_GET['set_by_id'] as $k => $v) {

                        $iv = intval($v);

                        if(empty($iv)) {
                            continue;
                        }

                        $ids[] = $iv;
                    }

                    if(!empty($ids)) {

                        $this->_recruit->setByIds = $ids;
                    }
                }

                if(isset($_GET['production_status']) && !empty($_GET['production_status'])) {
                    $this->_recruit->sortByCapStatus = ($_GET['production_status'] == 1) ? "ASC" : "DESC";
                }

                if(isset($_GET['rating']) && !empty($_GET['rating'])) {
                    $this->_recruit->sortByRating = ($_GET['rating'] == 1) ? "ASC" : "DESC";
                }
            }

            return;
        }

        protected function _pipelineGetTotals($data) {
            reset($data);

            foreach ($this->_columnLabels as $id => $label) {
                $totals[$label] = array(
                    'leads' => 0,
                    'appt' => 0
                );
            }

            foreach($data as $recruit) {

                $recruit->appointmentStartDate = $this->thisMonthStart;
                $recruit->appointmentEndDate = $this->thisMonthEnd;

                $appointmentsCount = $recruit->appointment;

                if(!isset($totals[$this->_columnLabels[$recruit->status_id]])) {
                    $totals[$this->_columnLabels[$recruit->status_id]]['leads'] = 0;
                    $totals[$this->_columnLabels[$recruit->status_id]]['appt'] = 0;
                }

                $totals[$this->_columnLabels[$recruit->status_id]]['leads']++;
                $totals[$this->_columnLabels[$recruit->status_id]]['appt'] += count($appointmentsCount);
            }

            return $totals;
        }

		/**
		 * Specifies the action rules.
		 *
		 * @return array actions
		 */
		public function actions() {
			return array(
                'accountability'    =>'stm_app.modules.admin.controllers.actions.tracking.AccountabilityAction',
                'activities' => 'stm_app.modules.admin.controllers.actions.tracking.ActivitiesAction',
                'addTimeblocking' => 'stm_app.modules.admin.controllers.actions.tracking.AddTimeblockingAction',
				'appointments' => 'stm_app.modules.admin.controllers.actions.tracking.AppointmentsAction',
                'appointmentTimes'    =>'stm_app.modules.admin.controllers.actions.tracking.AppointmentTimesAction',
                'appointmentsPastDue' => 'stm_app.modules.admin.controllers.actions.tracking.AppointmentsPastDueAction',
                'callsInboundTimes'    =>'stm_app.modules.admin.controllers.actions.tracking.CallsInboundTimesAction',
                'callPerformanceTimes'    =>'stm_app.modules.admin.controllers.actions.tracking.CallPerformanceTimesAction',
                'closings' => 'stm_app.modules.admin.controllers.actions.tracking.ClosingsAction',
                'closingsExecutiveSummary' => 'stm_app.modules.admin.controllers.actions.tracking.ClosingsExecutiveSummaryAction',
                'closingsByAgent' => 'stm_app.modules.admin.controllers.actions.tracking.ClosingsByAgentAction',
                'conversion' => 'stm_app.modules.admin.controllers.actions.tracking.ConversionAction',
                'conversionTrends' => 'stm_app.modules.admin.controllers.actions.tracking.ConversionTrendsAction',
                'createLink'    =>'stm_app.modules.admin.controllers.actions.tracking.CreateLinkAction',
                'editTimeblocking' => 'stm_app.modules.admin.controllers.actions.tracking.EditTimeblockingAction',
                'emailClicks' => 'stm_app.modules.admin.controllers.actions.tracking.EmailClicksAction',
                'emailClickStatusUpdate' => 'stm_app.modules.admin.controllers.actions.tracking.EmailClickStatusUpdateAction',
                'formViews' => 'stm_app.modules.admin.controllers.actions.tracking.FormViewsAction',
                'deleteTimeblock' => 'stm_app.modules.admin.controllers.actions.tracking.DeleteTimeblockAction',
                'dialerListRemoval' => 'stm_app.modules.admin.controllers.actions.tracking.DialerListRemovalAction',
                'goals' => 'stm_app.modules.admin.controllers.actions.tracking.GoalsAction',
                'isa' => 'stm_app.modules.admin.controllers.actions.tracking.IsaAction',
                'internalDoNotCall' => 'stm_app.modules.admin.controllers.actions.tracking.InternalDoNotCallAction',
                'leads' => 'stm_app.modules.admin.controllers.actions.tracking.LeadsAction',
                'leadAssignments' => 'stm_app.modules.admin.controllers.actions.tracking.LeadAssignmentsAction',
                'listings' => 'stm_app.modules.admin.controllers.actions.tracking.ListingsAction',
                'marketUpdates' => 'stm_app.modules.admin.controllers.actions.tracking.MarketUpdatesAction',
                'noActivity'    =>'stm_app.modules.admin.controllers.actions.tracking.NoActivityAction',
                'opportunities'    =>'stm_app.modules.admin.controllers.actions.tracking.OpportunitiesAction',
                'session' => 'stm_app.modules.admin.controllers.actions.tracking.SessionAction',
                'sources' => 'stm_app.modules.admin.controllers.actions.tracking.SourcesAction',
                'tasks' => 'stm_app.modules.admin.controllers.actions.tracking.TasksAction',
                'timeblocking' => 'stm_app.modules.admin.controllers.actions.tracking.TimeblockingAction',
                'traffic' => 'stm_app.modules.admin.controllers.actions.tracking.TrafficAction',
                'trafficDetails' => 'stm_app.modules.admin.controllers.actions.tracking.TrafficDetailsAction',
                'viewTimeblock' => 'stm_app.modules.admin.controllers.actions.tracking.ViewTimeblockAction',
                'formSubmissions' => 'stm_app.modules.admin.controllers.actions.tracking.FormSubmissionsAction',
                'formSubmission' => 'stm_app.modules.admin.controllers.actions.tracking.FormSubmissionAction',
			);
		}

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
            $this->thisMonthStart = date("Y-m-01 00:00:00", time());

            $day=new DateTime('last day of this month');
            $this->thisMonthEnd = $day->format('Y-m-d')." 23:59:59";

			switch ($this->action->id) {
				case self::GOALS:
					break;
				case self::TIME_BLOCKING:
					$this->pageColor = 'rose';
					break;
			}

			$this->_columnLabels = array(
                TransactionStatus::HOT_A_LEAD_ID => '',
                TransactionStatus::B_LEAD_ID => '',
                TransactionStatus::C_LEAD_ID => '',
                TransactionStatus::D_NURTURING_SPOKE_TO_ID => '',
                TransactionStatus::E_NOT_YET_INTERESTED => '',
                TransactionStatus::NURTURING_ID => ''
            );

            $criteria = new CDbCriteria();
            $criteria->addInCondition('transaction_status_id', array_keys($this->_columnLabels));
            $criteria->addCondition('account_id='. Yii::app()->user->accountId);
            $criteria->addCondition('component_type_id='. ComponentTypes::RECRUITS);

            $ratingOverrides = TransactionStatusAccountLu::model()->findAll($criteria);

            // return (!empty($rating) && !empty($rating->override_name)) ? $rating->override_name : $recruit->status->name;
            foreach($ratingOverrides as $ratingOverride) {
                $this->_columnLabels[$ratingOverride->transaction_status_id] = $ratingOverride->override_name;
            }

            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', array_keys($this->_columnLabels));

            $statusDefaultLabel = TransactionStatus::model()->findAll($criteria);

            foreach($statusDefaultLabel as $label) {
                if (empty($this->_columnLabels[$label->id])) {
                    $this->_columnLabels[$label->id] = $label->name;
                }
            }

            $this->_recruit = new Recruits();
            $this->_recruit->unsetAttributes();
            $this->_recruit->status_id = array_keys($this->_columnLabels);
		}

		public function getRating($recruit) {
            $rating = TransactionStatusAccountLu::model()->findByAttributes(array(
                'transaction_status_id' => $recruit->status_id,
                'account_id' => Yii::app()->user->accountId,
                'component_type_id' => ComponentTypes::RECRUITS
            ));

            return (!empty($rating) && !empty($rating->override_name)) ? $rating->override_name : $recruit->status->name;
        }

        public function getLeadRecruiter($data) {
            $recruit = $this->_recruit->findByPk($data->id);

            $criteria = new CDbCriteria();
            $criteria->addCondition('assignment_type_id='. AssignmentTypes::RECRUITER);
            $criteria->addCondition('component_type_id='. ComponentTypes::RECRUITS);
            $criteria->addCondition('component_id='. $recruit->id);

            $assignment = new Assignments();
            $assignment = $assignment->find($criteria);

            if(!empty($assignment)) {
                return $assignment->contact->first_name." ".$assignment->contact->last_name;
            }

            //echo "Not Found";
        }

        public function getCapStatus($recruit) {
            if(!empty($recruit->capStatus) && !empty($recruit->capStatus->override_name)) {

                return $recruit->capStatus->override_name;
            }

            if(!empty($recruit->capStatus) && !empty($recruit->capStatus->name)) {

                return $recruit->capStatus->name;

            }
        }

        public function getNextStep($recruit) {

            $componentTuple = $recruit->getComponentChain();

            if(isset($componentTuple[0])) {
                $criteria = new CDbCriteria();
                $criteria->compare('component_id', $componentTuple[0]['componentId']);
                $criteria->compare('component_type_id', $componentTuple[0]['componentTypeId']);
                $criteria->addCondition('due_date IS NOT NULL AND complete_date IS NULL');
                $criteria->order = 'due_date ASC';

                $task = Tasks::model()->find($criteria);

                if (!empty($task)) {
                    return Yii::app()->format->formatDate($task->due_date)." - ".$task->description;
                }
            }

            return "";
        }

        public function formatRecruitName($recruit, $row) {
            return '<a href="/admin/recruits/'.$recruit->id.'">'.$recruit->contact->first_name." ".$recruit->contact->last_name."</a>";
        }

        public function formatNotes($recruit) {
            $results = "";
            if(!empty($recruit->motivation)) {
                $results .= "Motivation: ".$recruit->motivation."\r\n";
            }

            if(!empty($recruit->notes)) {
                $results .= "Notes: ".$recruit->notes."\r\n";
            }

            return $results;
        }

		public function actionPipeline() {

            $this->title = 'Pipeline Report';
            $this->pageColor = 'orange';

            $this->_getPipelineData();

            $dp = $this->_recruit->search();
            $tmpData = $dp->getData();

            $totals = $this->_pipelineGetTotals($tmpData);

            unset($tmpData);

            $this->render('pipeline', array(
                'dataProvider' => $dp,
                'totals' => $totals
            ));
        }

        public function actionPipelinePrint() {
            $this->layout = false;

            $title = "Pipeline Report";

            if(isset($_GET['title']) && !empty($_GET['title'])) {
                $title = strip_tags($_GET['title']);
            }

            $this->_getPipelineData();

            $dp = $this->_recruit->search();
            $tmpData = $dp->getData();

            $totals = $this->_pipelineGetTotals($tmpData);

            $this->render('pipelinePrint', array(
                'title' => $title,
                'dataProvider' => $this->_recruit->search(),
                'totals' => $totals
            ));
        }

        public function actionPipelineEmail() {
            $this->_getPipelineData();

            $dp = $this->_recruit->search();
            $tmpData = $dp->getData();
            $out = fopen('php://temp', 'w');

            $emails = array();
            if(!isset($_POST['emails']) || empty($_POSt['emails'])) {
                $emails[] = Yii::app()->user->contact->primaryEmail;
            } else {
                $emails = array_map(function($value) {
                    return strip_tags(trim($value));
                },explode(',',trim($_POST['emails'])));
            }

            $headers = array(
                'Full Name',
                'First Appointment',
                'Lead Recruiter',
                'Rating',
                'Production Status',
                'Lead Source',
                'Join By Date',
                'Phone',
                'Email',
                'Next Step',
                'Notes'
            );

            fputcsv($out, $headers);
            foreach($tmpData as $data) {
                fputcsv($out, array(
                    $data->contact->first_name." ".$data->contact->last_name,
                    Yii::app()->format->formatDate($data->appointment->set_for_datetime),
                    $this->getLeadRecruiter($data),
                    $this->getRating($data),
                    $this->getCapStatus($data),
                    $data->source->name,
                    Yii::app()->format->formatDate($data->target_date),
                    Yii::app()->format->formatPrintPhones($data->contact->phones),
                    Yii::app()->format->formatPrintEmails($data->contact->emails),
                    $this->getNextStep($data),
                    $this->formatNotes($data)
                ));
            }

            $totals = $this->_pipelineGetTotals($tmpData);
            fputcsv($out, array());
            fputcsv($out, array_keys($totals));
            fputcsv($out, array_values($totals));

            rewind($out);

            $csv = fgets($out);

            fclose($out);

            $mail = new StmZendMail();
            $mail->checkBounceUndeliverable = false;
            $mail->setSubject('Pipeline Report - ' . Yii::app()->user->domain->name . ' @ '.date('Y-m-d H:i:s'));
            $mail->setBodyHtml('Please see the attached file of your pipeline report.');
            $mail->setFrom('do-not-reply@seizethemarket.net', 'STM Notification');
            $mail->addBcc('debug@seizethemarket.com');

            if (YII_DEBUG) {
                $mail->addTo("brandon@w3evolutions.com", 'STM Debug'); //StmMailMessage::DEBUG_EMAIL_ADDRESS
            } else {
                $mail->addTo($emails);
            }

            $mail->setType(Zend_Mime::MULTIPART_RELATED);

            $at = $mail->createAttachment($csv);
            $at->type = 'text/csv';
            $at->disposition = Zend_Mime::DISPOSITION_INLINE;
            $at->encoding = Zend_Mime::ENCODING_BASE64;
            $at->filename = 'pipeline-report-'. Yii::app()->user->domain->name . '-' . date('Y-m-d H:i:s').".csv";

            // Attempt to send message
            try {
                $mail->send(StmZendMail::getAwsSesTransport());
            } catch (Exception $e) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Mass Update Error! (Zend Mail). Code: "' . $e->getCode() . '" ' . PHP_EOL . 'Exception Message:' . $e->getMessage(), CLogger::LEVEL_ERROR);
                $this->_sendJson(array(0=>'Email could not be sent. Please try again.'));
            }


            $this->_sendJson(array('status'=>'success'));
        }

    }
