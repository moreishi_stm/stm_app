<?php
class LeadRoutingController extends AdminController
{
    public $pageColor = 'orange';
    public function actions()
    {
    	return array(
			'index'     =>'stm_app.modules.admin.controllers.actions.leadRouting.ListAction',
			'routes'    =>'stm_app.modules.admin.controllers.actions.leadRouting.RoutesAction',
			'rules'     =>'stm_app.modules.admin.controllers.actions.leadRouting.RulesAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

    public function actionListZipCodes()
    {
        $this->title = 'Lead Route Zip Codes';
        $model=new ZipCodes('search');

        $this->render('listZipCodes',array(
                'model'=>$model,
            ));
    }


    public function actionEditZipCodes($id)
    {
        $this->title = 'Lead Route Zip Codes';
        $model= ZipCodes::model()->findByPk($id);

		if (isset($_POST['ZipCodes'])) {
            $model->attributes=$_POST['ZipCodes'];

            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated Zip Code.');
                $this->redirect(array('listZipCodes'));
            }
        }

        $this->render('formZipCodes',array(
                'model'=>$model,
            ));
    }

    public function actionAddZipCodes()
    {
        $this->title = 'Lead Route Zip Codes';
        $model=new ZipCodes();

        if (isset($_POST['ZipCodes'])) {
            $model->attributes=$_POST['ZipCodes'];

            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added Zip Code.');
                $this->redirect(array('listZipCodes'));
            }
        }

        $this->render('formZipCodes',array(
                'model'=>$model,
            ));
    }

    public function actionListAgentZipCodes()
    {
        $this->title = 'Lead Route Zip Codes';
        $model=new Contacts();

        $criteria = $model->byAdmins(true, null, 'allActive')->dbCriteria;
        $criteria->order = 'first_name ASC';
        $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria,'pagination'=>array('pageSize'=>50)));

        $this->render('listAgentZipCodes',array(
                'dataProvider'=>$dataProvider,
            ));
    }

    public function actionIncludeAgentZipCodes($id)
    {
        $this->renderAgentZipCodes($id, $type='Include');
    }

    public function actionExcludeAgentZipCodes($id)
    {
        $this->renderAgentZipCodes($id, $type='Exclude');
    }

    protected function renderAgentZipCodes($contactId, $type)
    {
        $this->title = 'Agent '.$type.' Zip Codes';
        $oppositeType = ($type=='Include')? 'Exclude' : 'Include';
        $leadRouteZipLu = new LeadRouteZipCodeLu();
        $contact = Contacts::model()->findByPk($contactId);

        if($_POST['LeadRouteZipCodeLu'] || $_POST['postTrigger']) {
            $leadRouteZipLu->attributes = $_POST['LeadRouteZipCodeLu'];

            // check to see the zip exists in the other "type" already
            $duplicateExists = false;
            if(!empty($leadRouteZipLu->data)) {
                $zipIds = implode(',', $leadRouteZipLu->data);
                $existingZips = Yii::app()->db->createCommand("select zip_code_id from lead_route_zip_code_lu where contact_id={$contactId} AND type='{$oppositeType}' AND zip_code_id IN({$zipIds})")->queryColumn();

                if($existingZips) {
                    Yii::app()->user->setFlash('error', 'The following zip code already assigned to this Agent in the "'.$oppositeType.'" list: '.implode(',', $existingZips));
                    $duplicateExists = true;
                }
            }

            if(!$duplicateExists) {
                $leadRouteZipLu->deleteAllByAttributes(array('contact_id'=>$contactId, 'type'=>$type));
                $errorFlag = false;
                foreach($leadRouteZipLu->data as $zipCodeId) {

                    $newLeadRouteZipLu = new LeadRouteZipCodeLu();
                    $newLeadRouteZipLu->setAttributes(array('contact_id'=>$contactId, 'type'=>$type, 'zip_code_id'=>$zipCodeId));
                    if(!$newLeadRouteZipLu->save()) {
                        $errorFlag = true;
                        //@todo: error message
                    }
                }
                if(!$errorFlag) {
                    Yii::app()->user->setFlash('success', 'Successfully updated Agent Zip Code.');
                    $this->redirect(array('listAgentZipCodes'));
                }
            }
        }

        $leadRouteZips = $leadRouteZipLu->findAllByAttributes(array('contact_id'=>$contactId, 'type'=>$type));

        if($leadRouteZips) {
            $leadRouteZipLu->data = array();
            foreach($leadRouteZips as $leadRouteZip) {
                $leadRouteZipLu->data[] = $leadRouteZip->zip_code_id;
            }
        }

        $this->render('formAgentZipCodes',array(
                'contact' => $contact,
                'leadRouteZipLu' => $leadRouteZipLu,
                'type'=>$type
            ));
    }

    protected function printZips($contactId, $type)
    {
        $zips = Yii::app()->db->createCommand("select z.zip from lead_route_zip_code_lu l LEFT JOIN zip_codes z ON z.id=l.zip_code_id where contact_id={$contactId} AND type='{$type}' ORDER BY z.zip ASC")->queryColumn();
        return implode(', ', $zips);
    }
    /**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new LeadRoutes;
		$this->displayName = 'Lead Routes';
	}
}