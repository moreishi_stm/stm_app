<?php
/**
 * Mass Mail Controller
 * 
 * @author Nicole Xu
 */
class MassmailController extends AdminController
{
	// Register controller actions for this controller
	public function actions() {
			return array(
				'index'			=>	'stm_app.modules.admin.controllers.actions.massmail.SetupAction',
				'authorize'		=>	'stm_app.modules.admin.controllers.actions.massmail.AuthorizeAction',
				'test'			=>	'stm_app.modules.admin.controllers.actions.massmail.TestAction',
			);
	}
}