<?php
class RecruitTypesController extends AdminController
{
	public $pageColor = 'purple';

//    public function actions()
//    {
//    	return array(
//			'index'       =>'stm_app.modules.admin.controllers.actions.contactTypes.ListAction',
//			'edit'        =>'stm_app.modules.admin.controllers.actions.contactTypes.EditAction',
//			'view'        =>'stm_app.modules.admin.controllers.actions.contactTypes.EditAction',
//			'add'         =>'stm_app.modules.admin.controllers.actions.contactTypes.AddAction',
//            'delete'      =>'stm_app.modules.admin.controllers.actions.contactTypes.DeleteAction',
//    	);
//    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init()
    {
        $this->baseModel = new RecruitTypes();
		$this->displayName = 'Recruit Tags';
	}

    public function actionIndex()
    {
        $this->title = 'Recruit Types';
        $model=$this->baseModel;
        $model->unsetAttributes();  // clear any default values

        // used for gridview search box inputs
        if (isset($_GET['RecruitTypes'])) {
            $model->attributes=$_GET['RecruitTypes'];
        }

        $this->render('list',array(
                'model'=>$model
            ));
    }

    public function actionEdit($id)
    {
        $this->title = 'Edit Recruit Type';
        $model=$this->loadModel($id);

        if (isset($_POST['RecruitTypes'])) {
            $model->attributes = $_POST['RecruitTypes'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Updated Recruit Type.');
                $this->redirect(array('index'));
            }
        }

        $this->render('form',array(
                'model'=>$model
            ));
    }

    public function actionAdd()
    {
        $this->title = 'Add Recruit Type';
        $model=$this->baseModel;

        if (isset($_POST['RecruitTypes'])) {
            $model->attributes = $_POST['RecruitTypes'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Added Recruit Type.');
                $this->redirect(array('index'));
            }
        }

        $this->render('form',array(
                'model'=>$model
            ));
    }

    public function actionDelete($id)
    {
        $model=$this->loadModel($id);

        if(count($model->recruitTypeLus)>0) {
            Yii::app()->user->setFlash('error', 'Recruit Type has associated contacts. Please remove associations before deleting.');
            $this->redirect(array('index'));
        } else {
            $model->delete();
            if ($model->is_deleted) {
                Yii::app()->user->setFlash('success', 'Successfully deleted Recruit Type.');
                $this->redirect(array('index'));
            }
        }

        $this->controller->render('form',array(
                'model'=>$model
            ));
    }
}