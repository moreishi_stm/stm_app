<?php
class MainController extends AdminController {

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'index'        =>'stm_app.modules.admin.controllers.actions.main.IndexAction',
            'news'         =>'stm_app.modules.admin.controllers.actions.main.NewsAction',
			'dashboard'    =>'stm_app.modules.admin.controllers.actions.main.DashboardAction',
			'login'        =>'stm_app.modules.admin.controllers.actions.main.LoginAction',
			'logout'       =>'stm_app.modules.admin.controllers.actions.main.LogoutAction',
			'error'        =>'stm_app.modules.admin.controllers.actions.main.ErrorAction',
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'    =>array(
				'class'      =>'CCaptchaAction',
				'backColor'  =>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(
			'stmAccessControl' => array(
				'class' => 'stm_app.modules.admin.components.filters.StmAccessControl',
				'guestAllowedActions' => array(
					'login',
					'index',
					'logout',
					'error',
				),
			), // perform access control
		));
	}
}
