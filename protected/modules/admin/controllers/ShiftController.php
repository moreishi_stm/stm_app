<?php

class ShiftController extends AdminController {

    /**
     * Specifies the action rules.
     *
     * @return array actions
     */
    public function actions()
    {
        return array(
			'add'    => 'stm_app.modules.admin.controllers.actions.shift.AddAction',
			'edit'   => 'stm_app.modules.admin.controllers.actions.shift.EditAction',
			'view'   => 'stm_app.modules.admin.controllers.actions.shift.ViewAction',
			'get'    => 'stm_app.modules.admin.controllers.actions.shift.GetAction',
	        'getShift' => 'stm_app.modules.admin.controllers.actions.shift.GetShiftAction',
			'delete' => 'stm_app.modules.admin.controllers.actions.shift.DeleteAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}
}