<?php
class RecruitsController extends AdminController {
	public $pageColor = 'blue';
    protected $_bombBombLists;

    public function actions() {
    	return array(
			'add'                  =>'stm_app.modules.admin.controllers.actions.recruits.AddAction',
			'addFind'              =>'stm_app.modules.admin.controllers.actions.recruits.AddFindAction',
			'view'                 =>'stm_app.modules.admin.controllers.actions.recruits.ViewAction',
			'edit'                 =>'stm_app.modules.admin.controllers.actions.recruits.EditAction',
			'index'                =>'stm_app.modules.admin.controllers.actions.recruits.ListAction',
            'referral'             =>'stm_app.modules.admin.controllers.actions.recruits.ReferralAction',
            'sendReferral'         =>'stm_app.modules.admin.controllers.actions.recruits.SendReferralAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new Recruits;
		$this->displayName = 'Recruiting';
	}

    public function printDescription($model)
    {
        $string = '';
        $string .= ($model->rating != '0.0')? "Rating: ".$model->rating." Star" : '';

        $types = Yii::app()->format->formatCommaDelimited($model->types, "name");
        $string .= ($string && $types) ? '<br>' : '';
        $string .= ($types) ? "Tags: ".$types : '';

        $sourceName = $model->source->name;
        $string .= ($string && $sourceName) ? '<br>' : '';
        $string .= ($model->source_id) ? "Source: ".$sourceName : '';

        $string .= ($string && $model->getDISCProfile()) ? '<br>' : '';
        $string .= ($model->getDISCProfile()) ? "DISC: <strong class=\"disc\">".$model->getDISCProfile().'</strong><i>'.$model->printDISCDetails($options=array("parentheses"=>true))."</i>" : '';

        $string .= ($string && $model->printAVAProfile()) ? '<br>' : '';
        $string .= ($model->printAVAProfile()) ? "AVA: ".$model->printAVAProfile() : '';

        $string .= ($string && $model->application_status_ma) ? '<br>' : '';
        $string .= ($model->application_status_ma) ? "Application Sent: ".StmFormHelper::getYesNoName($model->application_status_ma) : '';

        $string .= ($string && $model->met_status_ma) ? '<br>' : '';
        $string .= ($model->met_status_ma) ? "Met: ".StmFormHelper::getYesNoName($model->met_status_ma) : '';

        $actionPlans = Yii::app()->db->createCommand("select distinct ap.name from tasks t LEFT JOIN action_plan_applied_lu lu ON t.id=lu.task_id LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id LEFT JOIN action_plans ap ON ap.id=i.action_plan_id WHERE t.component_type_id=".ComponentTypes::RECRUITS." AND t.component_id=".$model->id." AND t.complete_date IS NULL AND t.is_deleted=0 ORDER BY ap.name ASC")->queryColumn();
        $actionPlans = implode(',', $actionPlans);

        $string .= '<br /><strong style="float: left;">Action Plan:</strong>  ';
        if($actionPlans) {
            $string .= $actionPlans;
        }
        else {
            $warningSpanStyle = ' style="float: left; background-color: #D20000; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative; top: -1px;"';
            $string .= '<span'.$warningSpanStyle.'>NONE</span>';
        }

        $string .= '<br /><strong style="float: left; clear: both;">Next Task:</strong>  ';
        // next task
        if($nextTaskDate = Yii::app()->db->createCommand("select min(due_date) from tasks where component_type_id=".ComponentTypes::RECRUITS." AND component_id=".$model->id." AND due_date>='".date('Y-m-d')."' AND due_date<='".date("Y-m-d", strtotime('30 days'))."' AND is_deleted=0")->queryScalar()) {
            $string .= date('m/d/y', strtotime($nextTaskDate));
        }
        else {
            $warningSpanStyle = ' style="float: left; background-color: #D20000; color: white; text-shadow: none; font-weight: bold; padding: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; position: relative;"';
            $string .= '<span'.$warningSpanStyle.'>NONE</span>';
        }

        if($model->assignments) {
            $string .= '<div style="clear: both;"><strong style="float: left; clear: both;">Assigned to:</strong>  ';
            foreach($model->assignments as $i => $assignment) {
                $string .= ($i) ? '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '';
                $string .= $assignment->contact->fullName.' ('.$assignment->assignmentType->display_name.')';
            }
            $string .= '</div>';
        }


        return $string;
    }

    public function printAgentInfo($model)
    {
        $string = '';
        $string .= (($model->met_status_ma) ? 'Met: '.StmFormHelper::getBooleanImage($model->met_status_ma, null) : 'Not Met');

        if($model->is_licensed || $model->licensed_date) {

            $string .= ($string && $model->is_licensed) ? '<br>' : '';
            $string .= ($model->is_licensed) ? "Licensed: ".StmFormHelper::getYesNoName($model->is_licensed) : '';

            $string .= ($model->is_licensed && $model->licensed_date > 0) ? ' - ' : '';
            $string .= ($model->licensed_date) ? Yii::app()->format->formatDate($model->licensed_date) : '';

//            $string .= ($string && $model->annual_volume && $model->annual_units) ? '<br>' : '';
//            $string .= ($model->annual_volume && $model->annual_units) ? "Avg Sales Price: ".Yii::app()->format->formatDollars($model->annual_volume / $model->annual_units) : '';
        }


        $string .= ($string && $model->current_brokerage) ? '<br>' : '';
        $string .= ($model->current_brokerage) ? "Brokerage: ".$model->current_brokerage : '';
        $string .= ($model->office_city) ? "<br>Office Location: ".$model->office_city : '';
        $string .= ($model->office_zip) ? (($model->office_city) ? ", " : "Zip: ").$model->office_zip : '';

        if($model->annual_volume || $model->annual_units || $model->current_brokerage) {

//            $string .= ($string) ? '<br>Last 12 mo: ' : '';
//            $string .= ($model->annual_volume) ? Yii::app()->format->formatDollars($model->annual_volume) : '';
//            $string .= ($model->annual_units) ? ' ('.$model->annual_units.' Units)' : '';
//            $string .= ($model->annual_volume_change_percent) ? ' | '.$model->annual_volume_change_percent.'%' : '';

//            $string .= ($string && $model->annual_volume && $model->annual_units) ? '<br>' : '';
//            $string .= ($model->annual_volume && $model->annual_units) ? "Avg Sales Price: ".Yii::app()->format->formatDollars($model->annual_volume / $model->annual_units) : '';
        }

        return $string;
    }

    //@todo: quick copy/paste for hotfix - need to centralize
    public function bombBombLists() {
        if(!$this->_bombBombLists) {
            $BombBomb = Yii::app()->bombbomb;
            $data = $BombBomb->getLists();

            $listArray = array();
            if($data && is_array($data['info'])) {

                foreach($data['info'] as $list) {
                    $listArray[$list['id']] = $list['name'];
                }
            }
            $this->_bombBombLists = CHtml::dropDownList('bombBombList',null, $listArray, $htmlOptions=array('style'=>'display:none;','class'=>'bombBombLists g8','id'=>'bombBombList-'.$list['id']));
        }

        return $this->_bombBombLists;
    }
}
