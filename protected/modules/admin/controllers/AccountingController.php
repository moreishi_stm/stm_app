<?php
Yii::import('admin_widgets.DateRanger.DateRanger');
class AccountingController extends AdminController
{
	public $pageColor = 'teal';

    //@todo: this is a temporary solution
    public static $leadsCount;
    public static $closingsCount;


    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
            'index'   =>'stm_app.modules.admin.controllers.actions.accounting.ListAction',
            'add'     =>'stm_app.modules.admin.controllers.actions.accounting.AddAction',
            // 'data' =>'stm_app.modules.admin.controllers.actions.accounting.DataAction',
            'edit'    =>'stm_app.modules.admin.controllers.actions.accounting.EditAction',
            // 'view' =>'stm_app.modules.admin.controllers.actions.accounting.ViewAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

    public function actionRoi()
    {
        $this->title = 'ROI Tracking';
        $this->pageColor = 'teal';

        $model= $this->baseModel;
        $model->unsetAttributes();  // clear any default values

        // used for gridview search box inputs
        if (isset($_GET['AccountingTransactions']))
            $model->attributes=$_GET['AccountingTransactions'];

        $this->render('list',array(
                'model'=>$model,
            ));
    }

    public function actionAddExpense()
    {
        $this->title = 'Add Expense';
        $this->pageColor = 'teal';

        $model= $this->baseModel;
        $model->scenario = 'roi';
        $model->unsetAttributes();  // clear any default values

        $model->accounting_account_id = AccountingAccounts::MARKETING_LEAD_GEN;

        // used for gridview search box inputs
        if (isset($_POST['AccountingTransactions'])) {
            $model->attributes = $_POST['AccountingTransactions'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added a Referral.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/accounting/roi');
            }
        }

        $this->render('form',array(
                'model'=>$model,
            ));
    }

    public function actionEditExpense($id)
    {
        $this->title = 'Edit Expense';
        $this->pageColor = 'teal';

        $model= $this->loadModel($id);
        $model->scenario = 'roi';

        // used for gridview search box inputs
        if (isset($_POST['AccountingTransactions'])) {
            $model->attributes = $_POST['AccountingTransactions'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added a Referral.');
                $this->redirect('/'.Yii::app()->controller->module->name.'/accounting/roi');
            }
        }

        $this->render('form',array(
                'model'=>$model,
            ));
    }

    public function actionRoiReport()
    {
        $this->title = 'ROI Report';
        $this->pageColor = 'teal';

        $DateRanger = new DateRanger();
        if($_GET['DateRanger']) {
            $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);
        }
        // by default do the last month as date range
        else {
            $month_ini = new DateTime("first day of last month");
            $month_end = new DateTime("last day of last month");

            $dateRange['from_date'] = $month_ini->format('Y-m-d');
            $dateRange['to_date'] = $month_end->format('Y-m-d');
        }
        $sellerLeadCountQuery = "select count(*) from transactions where component_type_id=".ComponentTypes::SELLERS." AND source_id=s.id AND DATE(added) >= '".$dateRange['from_date']."' AND DATE(added) <= '".$dateRange['to_date']."'";

        $buyerLeadCountQuery = "select count(*) from transactions where component_type_id=".ComponentTypes::BUYERS." AND source_id=s.id AND DATE(added) >= '".$dateRange['from_date']."' AND DATE(added) <= '".$dateRange['to_date']."'";

//        $leadCountQuery = "select count(*) from transactions where source_id=s.id AND DATE(added) >= '".$dateRange['from_date']."' AND DATE(added) <= '".$dateRange['to_date']."'";


        $sellerClosingCountQuery = "select count(*) from closings c LEFT JOIN transactions t ON t.id=c.transaction_id where t.source_id=s.id AND DATE(c.actual_closing_datetime) >= '".$dateRange['from_date']."' AND DATE(c.actual_closing_datetime) <= '".$dateRange['to_date']."'";

        $buyerClosingCountQuery = "select count(*) from closings c LEFT JOIN transactions t ON t.id=c.transaction_id where t.source_id=s.id AND DATE(c.actual_closing_datetime) >= '".$dateRange['from_date']."' AND DATE(c.actual_closing_datetime) <= '".$dateRange['to_date']."'";

//        $closingCountQuery = "select count(*) from closings c LEFT JOIN transactions t ON t.id=c.transaction_id where t.source_id=s.id AND DATE(c.actual_closing_datetime) >= '".$dateRange['from_date']."' AND DATE(c.actual_closing_datetime) <= '".$dateRange['to_date']."'";

        $data = Yii::app()->db->createCommand()
            ->select("SUM(amount) amount, s.name name, ($sellerLeadCountQuery) sellerLeadCount, ($buyerLeadCountQuery) buyerLeadCount, ($sellerClosingCountQuery) sellerClosingCount, ($buyerClosingCountQuery) buyerClosingCount")
            ->from("accounting_transactions a")
            ->leftJoin("sources s","s.id=a.source_id")
            ->where("DATE(`date`) >= '".$dateRange['from_date']."' AND DATE(`date`) <= '".$dateRange['to_date']."'")
            ->group("source_id")
            ->queryAll();

        $dataProvider = new CArrayDataProvider($data, array(
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));

        $this->render('reportRoi',array(
                'dataProvider'=>$dataProvider,
            ));
    }

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->displayName = 'ROI Tracker';
		$this->baseModel = new AccountingTransactions;
	}
}