<?php
/**
 * Hunt Group Controller
 */
class IvrController extends AdminController
{
    public $title = 'IVR';

    /**
     * Initializes the controller
     * @return none
     */
    public function init() {
        $this->displayName = 'IVR';
        $this->baseModel = new Ivrs();
    }

    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
            'index'         =>'stm_app.modules.admin.controllers.actions.ivr.ListAction',
            'list'          =>'stm_app.modules.admin.controllers.actions.ivr.ListAction',
            'add'           =>'stm_app.modules.admin.controllers.actions.ivr.AddAction',
            'record'           =>'stm_app.modules.admin.controllers.actions.ivr.RecordAction',
            'edit'          =>'stm_app.modules.admin.controllers.actions.ivr.EditAction',
            'listExtension' =>'stm_app.modules.admin.controllers.actions.ivr.ListExtensionAction',
            'addExtension'  =>'stm_app.modules.admin.controllers.actions.ivr.AddExtensionAction',
            'editExtension' =>'stm_app.modules.admin.controllers.actions.ivr.EditExtensionAction',
            'listSource' =>'stm_app.modules.admin.controllers.actions.ivr.ListSourceAction',
            'addSource'  =>'stm_app.modules.admin.controllers.actions.ivr.AddSourceAction',
            'editSource' =>'stm_app.modules.admin.controllers.actions.ivr.EditSourceAction',
        );
    }
}