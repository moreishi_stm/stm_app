<?php
class MeetingNotesController extends AdminController
{
	public $pageColor = 'blue';
    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
			'index'       =>'stm_app.modules.admin.controllers.actions.meetingNotes.ListAction',
            'add'         =>'stm_app.modules.admin.controllers.actions.meetingNotes.AddAction',
			'edit'        =>'stm_app.modules.admin.controllers.actions.meetingNotes.EditAction',
			// 'view'     =>'stm_app.modules.admin.controllers.actions.goals.ViewAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}
}