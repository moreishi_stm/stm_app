<?php
// Import stuff we need
Yii::import('admin_module.components.StmDialer.Lists.Factory', true);

/**
 * Dialer Controller
 */
class DialerController extends AdminController
{
    public $title = 'Dialer';

    const FROM_NUMBER = '19043301885';
    const FILTER_DEFAULT_MAX_CALL_COUNT = 15;
    const FILTER_DEFAULT_MAX_CALL_DAYS = 30;
    const TASK_QUEUE_LIMIT = 250;
    const TASK_QUEUE_LIMIT_SMALL = 2500;

    /**
     * Response
     *
     * Response object for Plivo
     * @var \StmPlivoFunctions\Response
     */
    protected $_response;

    /**
     * Node URL
     *
     * The URL for the Admin Socket Server
     * @var string Admin Socket server URL
     */
    protected $_nodeUrl;

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        // Determine what URL we will be using for the Admin Socket Server
        $this->_nodeUrl = YII_DEBUG ? 'http://dev-nicole.christineleeteam.com:8080' : 'http://node-admin.seizethemarket.net';

        // Chain parent init
        return parent::beforeAction($action);
    }

    public static function getDialTaskTypes($includeSkip=false)
    {
        $skipTaskTypeId = ($includeSkip) ? \TaskTypes::DIALER_SKIP.',' : '';
        return $skipTaskTypeId.\TaskTypes::DIALER_ANSWER.','.\TaskTypes::DIALER_NO_ANSWER.','.\TaskTypes::PHONE.','.\TaskTypes::DIALER_LEFT_VOICEMAIL.','.\TaskTypes::BAD_PHONE;
    }

    public function actionIndex()
    {
        $this->title = 'Dialer Start-up Wizard';
        $dialerTaskOverdueLimit = Yii::app()->user->settings->dialer_task_overdue_maxed_hide_all_shared_list;
        $isTaskOverdueMaxed = Tasks::isTaskOverdueMax(Yii::app()->user->id, Yii::app()->user->accountId);

        $listFilter = array(CallLists::MY_SELLERS_TASKS, CallLists::MY_BUYERS_TASKS, CallLists::MY_CONTACTS_TASKS, CallLists::MY_RECRUITS_TASKS, CallLists::LEAD_POOL_SELLERS_TASKS, CallLists::LEAD_POOL_BUYERS_TASKS, CallLists::LEAD_POOL_CONTACTS_TASKS, CallLists::ALL_NEW_SELLER_PROSPECTS, CallLists::ALL_NURTURING_SELLERS, CallLists::ALL_D_SELLER_PROSPECTS, CallLists::ALL_NEW_BUYER_PROSPECTS, CallLists::ALL_NURTURING_BUYERS, CallLists::ALL_D_BUYER_PROSPECTS,
                            CallLists::LEAD_POOL_A_SELLERS, CallLists::LEAD_POOL_B_SELLERS, CallLists::LEAD_POOL_C_SELLERS,
                            CallLists::LEAD_POOL_A_BUYERS, CallLists::LEAD_POOL_B_BUYERS, CallLists::LEAD_POOL_C_BUYERS,
                            CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_RECRUITS, CallLists::CUSTOM_CONTACTS);

        if($callUuid = CallSessions::model()->hasActiveSession(Yii::app()->user->id)) {
            $response = Yii::app()->plivo->getPlivo()->get_live_calls(array(
                    'call_uuid'            => $callUuid,
                ));
        }

        // if lead pool contact not set, remove the call lists related to it
        $leadPoolContactId = Yii::app()->db->createCommand()
            ->select('value')
            ->from('setting_account_values')
            ->where('setting_id='.Settings::SETTING_LEAD_POOL_CONTACT_ID)
            ->andWhere('account_id='.Yii::app()->user->accountId)
            ->queryScalar();
        if(empty($leadPoolContactId)) {
            $listFilter = array_diff($listFilter, array(CallLists::LEAD_POOL_SELLERS_TASKS, CallLists::LEAD_POOL_BUYERS_TASKS, CallLists::LEAD_POOL_CONTACTS_TASKS));
        }

        $customListQuery = 'SELECT l.id call_list_id, REPLACE(REPLACE(l.name, "\'", ""), "\\"","") name, (select count(clp.id) from call_list_phones clp WHERE clp.call_list_id=l.id AND clp.is_deleted=0 AND clp.hard_deleted IS NULL) count FROM call_lists l where l.preset_ma={CUSTOM_LIST_PRESET_ID} AND l.account_id='.Yii::app()->user->accountId.' AND l.is_deleted=0 ORDER BY name ASC';

        $customContactLists = Yii::app()->db->createCommand(str_replace('{CUSTOM_LIST_PRESET_ID}', CallLists::CUSTOM_CONTACTS, $customListQuery))->queryAll();
        $customSellerLists = Yii::app()->db->createCommand(str_replace('{CUSTOM_LIST_PRESET_ID}', CallLists::CUSTOM_SELLERS, $customListQuery))->queryAll();
        $customBuyerLists = Yii::app()->db->createCommand(str_replace('{CUSTOM_LIST_PRESET_ID}', CallLists::CUSTOM_BUYERS, $customListQuery))->queryAll();
        $customRecruitLists = Yii::app()->db->createCommand(str_replace('{CUSTOM_LIST_PRESET_ID}', CallLists::CUSTOM_RECRUITS, $customListQuery))->queryAll();
        $listCount = 17 + count($customContactLists) + count($customSellerLists) + count($customBuyerLists) + count($customRecruitLists);
        //==========================================================================================================================================================================

        $voicemailList = Yii::app()->db->createCommand("SELECT id, title from call_recordings WHERE type='".\CallRecordings::TYPE_VOICEMAIL_GREETING."' AND contact_id=".Yii::app()->user->id." AND recording_url != '' AND recording_url IS NOT NULL AND is_deleted=0 ORDER BY title ASC")->queryAll();

        $this->render('index', array(
                'userContactStatus' => Yii::app()->user->contact->contact_status_ma,
                'callerIdName' => $_COOKIE['callerIdName'],
                'callerIdNumber' => $_COOKIE['callerIdNumber'],

                'listCount' => $listCount,
                'listFilter' => $listFilter, //array of presetIds that will determine which lists get shown in dropdown
                'voicemailList' => $voicemailList,
//            'callListIdsFilter' => $callListIdsFilter, //array of callListIds that will determine which lists get shown in dropdown @todo: may not need this, idea placeholder for now
                'customContactLists' => $customContactLists,
                'customSellerLists' => $customSellerLists,
                'customBuyerLists' => $customBuyerLists,
                'customRecruitLists' => $customRecruitLists,
                'hideSharedLists' => ($dialerTaskOverdueLimit && $isTaskOverdueMaxed)? true : false,
            ));
    }

    /**
     * Initial Queue List //@todo: This is New Queue Logic Refactor - 4/2/16
     *
     * This is called from the dialer index page. This is the step 2 of the dialer once a list is selected and the "next" button is clicked. Queues a Call List and updates sort order according to filter
     * @param $id
     */
    public function actionInitialQueueList($id)
    {
        $callListId = $id;
        $filters = array();

        parse_str($_POST['data'], $params);
        // populate filters
        $filters = $params['filters'];

        if(!($callList = CallLists::model()->findByPk($id))) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid Call List ID: '.$id);
        }

        $o = \StmDialer\Lists\Factory::getInstance($callList->preset_ma, $callList->id);

        // only reqeueue now based on request
        if(($callList->type->table_type == 'task' || $callList->type->table_type == 'time') && $filters['requeue_now']) {
            $o->reQueueBySortOrder($callList->id, Yii::app()->user->id, $filters);
        }

        // one time clean up at initial list queue
        $o->cleanQueueBySortOrder($callList->id);

        echo CJSON::encode(array('status'=>'success'));
        Yii::app()->end();
    }

    /**
     * Initialize Call List
     * Primarily used for task list for contacts. They are not created until the first usage.
     * This initializing process creates the list if it does not exist via ajax call. Responds with a success status and the Call List ID.
     *
     * @param $id
     */
    public function actionInitializeCallList($id)
    {
        $o = \StmDialer\Lists\Factory::getInstance($id, null);
        echo CJSON::encode(array('status'=>'success', 'callListId'=>$o->getCallList()->id));
        Yii::app()->end();
    }

    /**
     * Action Start
     *
     * Default page load action
     * @return void
     */
    public function actionIndexSort($id)
    {
        if(!($callList = CallLists::model()->findByPk($id))) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid Call List ID: '.$id);
        }

        if(empty($_POST) || empty($_POST['callerInitialNumber']) || empty($_POST['callerIdName']) || empty($_POST['callerIdNumber']) || empty($_POST['num_to_call'])) {
            $this->redirect(array('index'));
        }

        // set cookies for current caller ID to be re-used later, good for 6 months
        Yii::app()->request->setCookie('callerInitialNumber', $_POST['callerInitialNumber'], 60*60*24*180);
        Yii::app()->request->setCookie('callerIdName', $_POST['callerIdName'], 60*60*24*180);
        Yii::app()->request->setCookie('callerIdNumber', $_POST['callerIdNumber'], 60*60*24*180);

//        $o = \StmDialer\Lists\Factory::getInstance($callList, $callList->id);
        $checkForSmsNumberHQ = false;
        if(Yii::app()->user->checkAccess('sms')) { //in_array(strtolower($_SERVER['SERVER_NAME']), array('www.christineleeteam.com','www.christineleeteam.local'))
            $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=> Yii::app()->user->accountId));

            $Criteria = new CDbCriteria;
            $Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id=:clientAccountId';
            $Criteria->params = array(
                ':clientAccountId'     => $clientAccount->id,
                ':contactId'    => Yii::app()->user->id,
                ':active'		=> 1
            );
            $checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);
        }
        $hasSmsProvidedNumber = false;

        if(!empty($checkForSmsNumberHQ)) {
            $hasSmsProvidedNumber =  true;
        }

        // Render the view
        $this->render('indexSort', array(
                'callList' => $callList,
                'hasSmsProvidedNumber' => $hasSmsProvidedNumber,
                'from' => $_POST['callerInitialNumber'],
                'callIdName' => $_POST['callerIdName'],
                'callerIdNumber' => $_POST['callerIdNumber'],
                'num_to_call' => $_POST['num_to_call'],
                'voicemailId' => $_POST['default_voicemail'],
                'nodeUrl' => YII_DEBUG ? 'dev-nicole.christineleeteam.com:8080' : 'node-admin.seizethemarket.net'
            ));
    }

    public function actionCallLists()
    {
        $this->title = 'Dialer Call Lists';

        $queuedCountSubquery = '(SELECT count(clp.id) from call_list_phones clp WHERE clp.status="'.CallListPhones::QUEUED.'" AND clp.call_list_id=l.id AND is_deleted=0 AND hard_deleted IS NULL)';
        $totalCountSubquery = '(SELECT count(clp.id) from call_list_phones clp WHERE clp.call_list_id=l.id AND hard_deleted IS NULL)';

        $rawData = Yii::app()->db->createCommand()
            ->select('l.*, clt.component_type_id, '.$queuedCountSubquery.' queuedCount, '.$totalCountSubquery.' totalCount')
            ->from('call_lists l')
            ->join('call_list_types clt','clt.id=l.preset_ma')
            ->where(array('in','l.preset_ma', array(CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_CONTACTS, CallLists::CUSTOM_RECRUITS)))
            ->andWhere('l.account_id='.Yii::app()->user->accountId)
            ->andWhere('l.is_deleted=0')
            ->order('l.name')
            ->queryAll();

//        $sort = new CSort();
//        $sort->defaultOrder = 'fullName';
//        $sort->attributes = array(
//            'fullName'=>array(
//                'asc'=> 'fullName ASC',
//                'desc'=>'fullName DESC',
//                'label' => 'Full Name',
//            ),
//        );

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
//        $dataProvider->sort = $sort;

        $this->render('callLists', array(
                'dataProvider'=> $dataProvider
            ));
    }

    public function actionCallListDetails($id)
    {
        $this->title = 'Dialer Call List Details';

        if(!($calLList = CallLists::model()->findByPk($id))) {
            echo "Call List Not Found. Please try again or contact support.";
            Yii::app()->end();
        }

        $sellerSubquery = ($calLList->type->component_type_id == ComponentTypes::SELLERS) ? '(SELECT t.id FROM transactions t WHERE c.id=t.contact_id and t.component_type_id='.ComponentTypes::SELLERS.' ORDER BY t.id DESC LIMIT 1)' : "(null)";
//        $totalCountSubquery = '(SELECT count(clp.id) from call_list_phones clp WHERE clp.call_list_id=l.id AND hard_deleted IS NULL)';

        $command = Yii::app()->db->createCommand()
//            ->select('l.*, '.$queuedCountSubquery.' queuedCount, '.$totalCountSubquery.' totalCount')
            ->select('clp.id call_list_phone_id, c.id contactId, c.first_name, c.last_name, s.name source, '.$sellerSubquery.' sellerId, p.phone')
            ->from('call_list_phones clp')
            ->join('call_lists l','l.id=clp.call_list_id')
            ->join('phones p','p.id=clp.phone_id')
            ->join('contacts c','c.id=p.contact_id');


        if(in_array($calLList->type->component_type_id, array( ComponentTypes::SELLERS,  ComponentTypes::BUYERS) )) {
            $command->join('transactions t2','t2.id='.$sellerSubquery)
                ->join('sources s','s.id=t2.source_id');
        }
        if(in_array($calLList->type->component_type_id, array( ComponentTypes::RECRUITS) )) {
            $command->join('recruits r','r.contact_id=c.id')
                ->join('sources s','s.id=r.source_id');
        }


        $command
            ->where(array('in','l.preset_ma', array(CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_CONTACTS, CallLists::CUSTOM_RECRUITS)))
            ->andWhere('l.account_id='.Yii::app()->user->accountId)
            ->andWhere('l.is_deleted=0')
            ->andWhere('clp.hard_deleted IS NULL')
            ->andWhere('l.id='.intval($id))
            ->group('p.phone')
            ->order('clp.sort_order, p.contact_id');

        $rawData = $command->queryAll();




//        //        $sort = new CSort();
//        //        $sort->defaultOrder = 'fullName';
//        //        $sort->attributes = array(
//        //            'fullName'=>array(
//        //                'asc'=> 'fullName ASC',
//        //                'desc'=>'fullName DESC',
//        //                'label' => 'Full Name',
//        //            ),
//        //        );
//
        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        //        $dataProvider->sort = $sort;

        $this->render('callListDetails', array(
                'dataProvider'=> $dataProvider,
                'calLList' => $calLList,
            ));
    }

    public function actionAddCallLists()
    {

        $this->title = 'Add Dialer Call List';
        $model = new CallLists('insert');

        if($_POST['CallLists']) {
            $model->attributes = $_POST['CallLists'];
            $model->account_id = Yii::app()->user->accountId;
            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Updated Call List.');
                $this->redirect(array('callLists'));
            }
            else {
                Yii::app()->user->setFlash('error', 'Error Updating Call List.');
            }
        }

        $this->render('formCallLists', array(
                'model'=> $model
            ));
    }

    public function actionEditCallLists($id)
    {
        $model = CallLists::model()->findByPk($id);

        if(!in_array($model->preset_ma, array(CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_CONTACTS, CallLists::CUSTOM_RECRUITS))) {
            Yii::app()->user->setFlash('error', 'Invalid List type to edit.');
            $this->redirect(array('callLists'));
        }

        $this->title = 'Dialer Call List - '.$model->name;

        if($_POST['CallLists']) {
            $model->attributes = $_POST['CallLists'];
            $model->account_id = Yii::app()->user->accountId;
            if($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully Updated Call List.');
                $this->redirect(array('callLists'));
            }
            else {
                Yii::app()->user->setFlash('error', 'Error Updating Call List.');
            }
        }

        $this->render('formCallLists', array(
                'model'=> $model
            ));
    }

    public function actionDeleteCallLists($id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = CallLists::model()->findByPk($id);
            $model->delete();
            echo CJSON::encode(array('status'=>'success'));
        }
    }

    public function actionCallListQueue($id)
    {
        $model = CallLists::model()->findByPk($id);

        $criteria = new CDbCriteria();
        $criteria->compare('call_list_id', $id);
        $criteria->order = 'id DESC';

        $dataProvider = new CActiveDataProvider('CallListPhones', array('criteria' => $criteria, 'pagination'=>array('pageSize'=> 100)));

        $this->render('callListQueue', array(
                'dataProvider'=> $dataProvider,
                'model' => $model,
            ));
    }

    public function actionSessions()
    {
        $this->title = 'Dialer Sessions';

        Yii::import('admin_widgets.DateRanger.DateRanger');
        if(isset($_GET['contact_id'])) {
            $contactId = $_GET['contact_id'];
        }
        else {
            $contactId = Yii::app()->user->id;
        }

        //get active sessions
        $criteria = new CDbCriteria();
        $criteria->addCondition('end IS NULL');
        $criteria->compare('DATE(start)', ' >= '.date('Y-m-d'));
        //$activeSessions = CallSessions::model()->findAll($criteria);
        $activeSessionsDataProvider = new CActiveDataProvider('CallSessions', array('criteria' => $criteria, 'pagination'=>array('pageSize'=> 100)));

        $DateRanger = new DateRanger();
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $dialSubQuery = '(SELECT COUNT(DISTINCT CONCAT(d.component_type_id,d.component_id,DATE_FORMAT(d.activity_date, "%Y-%m-%d %h:%i"))) from activity_log d WHERE d.added_by=s.contact_id AND DATE(d.activity_date) >= "'.$dateRange['from_date'].'" AND DATE(d.activity_date) <= "'.$dateRange['to_date'].'" AND d.component_type_id IN ('.ComponentTypes::BUYERS.','.ComponentTypes::SELLERS.','.ComponentTypes::CONTACTS.','.ComponentTypes::RECRUITS.') AND d.task_type_id IN ('.TaskTypes::BAD_PHONE.','.TaskTypes::DIALER_ANSWER.','.TaskTypes::DIALER_LEFT_VOICEMAIL.','.TaskTypes::DIALER_NO_ANSWER.'))'; //COUNT(DISTINCT CONCAT(d.component_type_id,d.component_id,DATE_FORMAT(d.activity_date, "%Y-%m-%d %h:%i")))

        $spokeToSubQuery = '(SELECT count(distinct DATE(st.activity_date), st.component_type_id, st.component_id) from activity_log st WHERE st.added_by=s.contact_id AND DATE(st.activity_date) >= "'.$dateRange['from_date'].'" AND DATE(st.activity_date) <= "'.$dateRange['to_date'].'" AND st.component_type_id IN ('.ComponentTypes::BUYERS.','.ComponentTypes::SELLERS.','.ComponentTypes::CONTACTS.','.ComponentTypes::RECRUITS.') AND st.task_type_id IN ('.TaskTypes::BAD_PHONE.','.TaskTypes::DIALER_ANSWER.','.TaskTypes::DIALER_LEFT_VOICEMAIL.','.TaskTypes::DIALER_NO_ANSWER.','.TaskTypes::DIALER_ADD_DO_NOT_CALL.') AND st.is_spoke_to=1)';

        $sellerApptSubQuery = '(SELECT count(sa.id) from appointments sa WHERE sa.set_by_id=s.contact_id AND DATE(sa.set_on_datetime) >= "'.$dateRange['from_date'].'" AND DATE(sa.set_on_datetime) <= "'.$dateRange['to_date'].'" AND sa.component_type_id='.ComponentTypes::SELLERS.')';

        $buyerApptSubQuery = '(SELECT count(ba.id) from appointments ba WHERE ba.set_by_id=s.contact_id AND DATE(ba.set_on_datetime) >= "'.$dateRange['from_date'].'" AND DATE(ba.set_on_datetime) <= "'.$dateRange['to_date'].'" AND ba.component_type_id='.ComponentTypes::BUYERS.')';

        $recruitApptSubQuery = '(SELECT count(ba.id) from appointments ba WHERE ba.set_by_id=s.contact_id AND DATE(ba.set_on_datetime) >= "'.$dateRange['from_date'].'" AND DATE(ba.set_on_datetime) <= "'.$dateRange['to_date'].'" AND ba.component_type_id='.ComponentTypes::RECRUITS.')';

        $assignmentSubQuery = '(SELECT at.display_name FROM setting_contact_values cv JOIN assignment_types at ON at.id=cv.value WHERE c.id=cv.contact_id AND cv.setting_id='.Settings::SETTING_ASSIGNMENT_TYPE_ID.')';

        $rawDataCommand = Yii::app()->db->createCommand()
            ->select('s.contact_id, SUM(s.bill_duration) total_time, CONCAT(c.first_name, " ", c.last_name) fullName, '.$dialSubQuery.' dials, '.$spokeToSubQuery.' spokeTo, '.$sellerApptSubQuery.' sellerAppts, '.$buyerApptSubQuery.' buyerAppts, '.$assignmentSubQuery.' assignmentType')
            ->from("call_sessions s")
            ->leftJoin("contacts c", "c.id=s.contact_id")
            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'")
            ->group("s.contact_id");

        $totalSumHoursCommand = Yii::app()->db->createCommand()
            ->select('SUM(bill_duration)')
            ->from("call_sessions s")
            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'");

        if(isset($_GET['componentTypeId']) && !empty($_GET['componentTypeId'])) {
            $rawDataCommand
                ->join('call_lists l','l.id=s.call_list_id')
                ->join('call_list_types clt','clt.id=l.preset_ma')
                ->andWhere("clt.component_type_id=:component_type_id", array(':component_type_id'=>$_GET['componentTypeId']));
            $totalSumHoursCommand
                ->join('call_lists l','l.id=s.call_list_id')
                ->join('call_list_types clt','clt.id=l.preset_ma')
                ->andWhere("clt.component_type_id=:component_type_id", array(':component_type_id'=>$_GET['componentTypeId']));
        }

        if(isset($_GET['assignmentTypes']) && !empty($_GET['assignmentTypes'])) {
            $rawDataCommand
                ->leftJoin('setting_contact_values cv2','cv2.contact_id=c.id AND cv2.setting_id='.Settings::SETTING_ASSIGNMENT_TYPE_ID)
                ->leftJoin('assignment_types at2','at2.id=cv2.value')
                ->andWhere(array('in', 'at2.id', (array) $_GET['assignmentTypes']));

            $totalSumHoursCommand
                ->leftJoin("contacts c", "c.id=s.contact_id")
                ->leftJoin('setting_contact_values cv2','cv2.contact_id=c.id AND cv2.setting_id='.Settings::SETTING_ASSIGNMENT_TYPE_ID)
                ->leftJoin('assignment_types at2','at2.id=cv2.value')
                ->andWhere(array('in', 'at2.id', (array) $_GET['assignmentTypes']));
        }

        $rawData = $rawDataCommand->queryAll();
        $totalSumHours = $totalSumHoursCommand->queryScalar();

        $totalSumDials = 0;
        $totalSumSpokeTo = 0;
        foreach($rawData as $row) {
            $totalSumDials += $row['dials'];
            $totalSumSpokeTo += $row['spokeTo'];
            $totalSellerAppts += $row['sellerAppts'];
            $totalBuyerAppts += $row['buyerAppts'];
            $totalRecruitAppts += $row['recruitAppts'];
        }

        $sort = new CSort();
        $sort->defaultOrder = 'fullName';
        $sort->attributes = array(
            'fullName'=>array(
                'asc'=> 'fullName ASC',
                'desc'=>'fullName DESC',
                'label' => 'Full Name',
            ),
            'hours'=>array(
                'asc'=> 'total_time ASC',
                'desc'=>'total_time DESC',
                'label' => 'Hours',
            ),
            'dials'=>array(
                'asc'=> 'dials ASC',
                'desc'=>'dials DESC',
                'label' => '# Dials',
            ),
            'spoke_to'=>array(
                'asc'=> 'spoke_to ASC',
                'desc'=>'spoke_to DESC',
                'label' => '# Spoke to',
            ),
            'sellerAppts'=>array(
                'asc'=> 'sellerAppts ASC',
                'desc'=>'sellerAppts DESC',
                'label' => '# Seller Appt Set',
            ),
            'buyerAppts'=>array(
                'asc'=> 'buyerAppts ASC',
                'desc'=>'buyerAppts DESC',
                'label' => '# Buyer Appt Set',
            ),
        );

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        $dataProvider->sort = $sort;

        // Render the view
        $this->render('session', array(
                'activeSessionsDataProvider' => $activeSessionsDataProvider,
                'dataProvider'=>$dataProvider,
                'totalSumHours' => $totalSumHours,
                'totalSumDials' => $totalSumDials,
                'totalSumSpokeTo' => $totalSumSpokeTo,
                'totalSellerAppts' => $totalSellerAppts,
                'totalBuyerAppts' => $totalBuyerAppts,
                'totalRecruitAppts' => $totalRecruitAppts,
                'fromDate' => date('m/d/Y', strtotime($dateRange['from_date'])),
                'toDate' => date('m/d/Y', strtotime($dateRange['to_date'])),
            ));
    }

    public function actionSessionDetails($id)
    {
        Yii::import('admin_widgets.DateRanger.DateRanger');

        $DateRanger = new DateRanger();
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $rawDataCommand = Yii::app()->db->createCommand()
            ->select('l.name, s.number_lines, s.start, s.end, DATE_FORMAT(s.start, "%W") day_name, s.bill_duration total_time')
            ->from("call_sessions s")
            ->leftJoin("call_lists l", "l.id=s.call_list_id")
            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'")
            ->andWhere("s.contact_id=$id");

        $totalSumCommand = Yii::app()->db->createCommand()
            ->select('SUM(bill_duration)')
            ->from("call_sessions s")
            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'")
            ->andWhere("s.contact_id=$id");

        if(isset($_GET['componentTypeId']) && !empty($_GET['componentTypeId'])) {
            $rawDataCommand
                ->join('call_list_types clt','clt.id=l.preset_ma')
                ->andWhere("clt.component_type_id=:component_type_id", array(':component_type_id'=>$_GET['componentTypeId']));
            $totalSumCommand
                ->join('call_lists l','l.id=s.call_list_id')
                ->join('call_list_types clt','clt.id=l.preset_ma')
                ->andWhere("clt.component_type_id=:component_type_id", array(':component_type_id'=>$_GET['componentTypeId']));
        }

        $rawData = $rawDataCommand->queryAll();
        $totalSum = $totalSumCommand->queryScalar();

        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));

        $this->render('sessionDetails', array(
                'dataProvider'=>$dataProvider,
                'totalSum' => $totalSum,
                'fullName' => Contacts::model()->findByPk($id)->fullName,
            ));
    }

    public function actionCallfinder()
    {
        $model = new Calls();
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Calls'])) {
            $model->attributes=$_GET['Calls'];
            $model->dial_b_leg_status = 'Hangup';
        }
        else {
            $model->id = 0;
        }
        $this->render('callfinder', array('model'=>$model));
    }

    public function actionLoadSort($id)
    {
        if(!($callList = CallLists::model()->findByPk($id))) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid Call List ID: '.$id);
        }

        // Get and check ID, which is the type_ma, for some reason the string null gets passed, not sure if it's due to
        $sessionId  = (Yii::app()->request->getParam('callsession') === 'null' ) ? null : Yii::app()->request->getParam('callsession');

        // Use new adapter pattern to get a list instance
        $o = \StmDialer\Lists\Factory::getInstance($callList->preset_ma, $callList->id);

        // In this initialize, checks to see if current user has any sessions active today. If not any "Ringing" call_list_phones records that belong to this user are set to "No Answer"
        if(Yii::app()->request->getParam('initialize') == 1) {
            $currentUser = Yii::app()->user->id;
        }

        // Retrieve current call session
        $inProgress = false;
        if($callSession = CallSessions::model()->hasActiveSession(Yii::app()->user->id, true)) {
            $inProgress = true;
            $sessionId = (is_numeric($sessionId)) ? $sessionId : $callSession->id ;
        }

        // Get phone numbers to call
        $callListPhones = $o->getQueueBySortOrder($id, Yii::app()->user->id, $filters, CallListPhones::MIN_LIST_COUNT);

        // Get in progress call, if we have one
        $active = null;
        foreach($callListPhones as $callListPhone) {
            if($callListPhone['status'] == 'Answered') {
                // if "Answered call does not have a active session, set the call as "No Answer" & email error log it.
                if(!$sessionId) {
                    // get data for call_list_phone before updating it to "No Answer"
                    $callListPhoneMissingSession = Yii::app()->db->createCommand("select * from  call_list_phones WHERE id={$callListPhone['call_list_phone_id']}")->queryRow();
                    Yii::app()->db->createCommand("UPDATE call_list_phones SET `status`='".CallListPhones::NO_ANSWER."' WHERE id={$callListPhone['call_list_phone_id']}")->execute();

                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') **** URGENT Error****'.PHP_EOL.'A call_list_phone record is marked as Answered without a call session ID. Marking this call as No Answer. Please investigate. Call List Phone ID: '.$callListPhone['call_list_phone_id'].' | Call List ID#: '.$id.' | Phone#: '.$callListPhone['phone'].' | Call List Phone Record: '.print_r($callListPhoneMissingSession, true).' | Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                $callId = Calls::getIdByAnsweredCallListPhoneId($callListPhone['call_list_phone_id'], $sessionId);

                if(!$callId) { // && !YII_DEBUG
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find CallID.  Please investigate. '.PHP_EOL
                        .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                        .'Session ID: '.$sessionId.PHP_EOL
                        .'Call List ID#: '.$id.PHP_EOL
                        .'Phone#: '.$callListPhone['phone'].PHP_EOL
                        .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                // Get phone record
                $phone = Phones::model()->skipSoftDeleteCheck()->findByPk($callListPhone['phone_id']);

                if(!$phone) { // && !YII_DEBUG
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find Phone model.  Please investigate. '.PHP_EOL
                        .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                        .'Session ID: '.$sessionId.PHP_EOL
                        .'Call List ID#: '.$id.PHP_EOL
                        .'Phone#: '.$callListPhone['phone'].PHP_EOL
                        .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                // Get the associated contact
                $contact = $phone->contact;

                $command = Yii::app()->db->createCommand()
                    ->select('a.id')
                    ->from('activity_log a')
                    ->where('a.call_id='.$callId)
                    ->order('a.id DESC');

                $answeredActivityLogId = $command->queryRow();

                // get the activity log that is marked as Answered for this call to pass to GUI as it may be a voicemail
                // @todo: one can not be found between the lag of being answered and marking as voicemail??? - keep an eye on this, watch error log emails
                if(!$answeredActivityLogId) {

                    // log error, no activity log id was found
                    // Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: No activity log record was found for an answered call. Call ID#: '.$callId.' Call List ID#: '.$id.' Call List Phone ID#: '.$callListPhone['call_list_phone_id'].' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
                    throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Error: No activity log record was found for an answered call. Call ID#: '.$callId.' Call List ID#: '.$id.' Call List Phone ID#: '.$callListPhone['call_list_phone_id'].' Query: '.$command->getText());
                }

                $componentType = ComponentTypes::model()->findByPk($callListPhone['component_type_id']);

                $callListIds = Yii::app()->db->createCommand()
                    ->select('clp.call_list_id id, l.name name, CONCAT("'.$phone->id.'") as phone_id, '.$callListPhone['component_type_id'].' component_type_id, '.$callListPhone['component_id'].' component_id')
                    ->from('call_list_phones clp')
                    ->join('call_lists l', 'l.id=clp.call_list_id')
                    ->join('call_list_types t', 't.id=l.preset_ma')
                    ->where('clp.phone_id='.$phone->id)
                    ->andWhere('clp.is_deleted=0')
                    ->andWhere(array('NOT IN','t.table_type', array('task')))
                    ->group('clp.call_list_id')
                    ->queryAll();

                $activityLogs = Yii::app()->db->createCommand()
                    ->select('DATE_FORMAT(a.activity_date, "%c/%e/%Y") date, a.note')
                    ->from('activity_log a')
                    ->where('a.component_type_id='.$callListPhone['component_type_id'])
                    ->andWhere('a.component_id='.$callListPhone['component_id'])
                    ->order('a.activity_date DESC, a.id DESC')
                    ->limit(40)
                    ->queryAll();

                foreach($activityLogs as $i => $activityLog) {
                    $activityLogs[$i]['note'] = strip_tags($activityLog['note']);
                }

                // Mode data for active content
                $active = array(
                    'session_id'        =>  $sessionId,
                    'contact_id'        =>  $contact->id,
                    'call_id'           =>  $callId,
                    'call_list_ids'     =>  $callListIds,
                    'call_list_phone_id'=>  $callListPhone['call_list_phone_id'],
                    //                    'source'            =>  $callListPhone['source'], //@todo: test on task based first to make sure the blank doesn't error out
                    'activity_log_id'   =>  $answeredActivityLogId['id'], // this enables the activity log to get updated from dialed to voicemail or other status
                    'phone_id'          =>  $phone->id,
                    'first_name'        =>  $contact->first_name,
                    'last_name'         =>  $contact->last_name,
                    'spouse_first_name' =>  ($contact->spouse_first_name)? ' & '.$contact->spouse_first_name : '',
                    'spouse_last_name'  =>  $contact->spouse_last_name,
                    'address'           =>  $contact->primaryAddress->address,
                    'city'              =>  $contact->primaryAddress->city,
                    'state'             =>  AddressStates::getShortNameById($contact->primaryAddress->state_id),
                    'zipcode'           =>  $contact->primaryAddress->zip,
                    'phone'             =>  $phone->phone,
                    'task_id'           =>  $callListPhone['task_id'],
                    'description'       =>  $callListPhone['description'],
                    'activity_logs'     =>  $activityLogs,
                    'component_id'      =>  $callListPhone['component_id'],
                    'component_type_id' =>  $callListPhone['component_type_id'],
                    'component_name'    =>  $componentType->name,
                    'component_label'   =>  $componentType->getSingularName(),
                );
            }
        }

        // Retrieve a list of phone numbers to call for the current session
        $this->_sendJson(array(
                'status'    =>  'success',
                'results'   =>  $callListPhones,
                'meta'	=>	array(
                    'in_progress'               =>  $inProgress,
                    'session_id'                =>  $sessionId,
                    'active'                    =>  $active,
                )
            ));
    }

    /**
     * Action Start
     *
     * Stars the dialer session
     * @return string
     */
    public function actionStart()
    {
        // see if there is another session active first for this contact. If so, get the b-leg-uuid and stop the call
        if($existingSession = CallSessions::model()->hasActiveSession(Yii::app()->user->id, true)) {
            //@todo: see if there are any answered call_sessions that are active and hang those up first. Send log note when this happens and let user determine how to handle it.
            //            $this->_sendJson(array(
            //                    'status'            =>  'sessionExists',
            //                ));

            // Sometimes a new session is request exactly after it's hungup. Wait a little and try again to verify it's state
            sleep(1);
            if($existingSession = CallSessions::model()->hasActiveSession(Yii::app()->user->id, true)) {

                $callString = '';
                if($existingSession->call_uuid) {
                    $calls = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('calls c')
                        ->join('call_list_phones l','l.id=c.call_list_phone_id')
                        ->where('c.dial_a_leg_uuid="'.$existingSession->call_uuid.'"')
                        ->andWhere(array('in', 'l.status',array(CallListPhones::ANSWERED, CallListPhones::RINGING)))
                        ->queryAll();
                }
                else {
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT! Existing session does not have call_uuid. Session Data: '.PHP_EOL.print_r($existingSession->attributes, true), CLogger::LEVEL_ERROR);
                }

                if(empty($calls) && $existingSession->call_uuid) {
                    $callId = Yii::app()->db->createCommand("SELECT id FROM calls where dial_a_leg_uuid='".$existingSession->call_uuid."'")->queryScalar();
                }
                elseif($calls) {
                    $callString = PHP_EOL.'Calls Data: '.print_r($calls, true);
                    $callId = $calls[0]['dial_a_leg_uuid'];
                }

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Session existed upon start dialer. Session Call UUID: '.$existingSession->call_uuid.' Call ID: '.$callId.PHP_EOL.'Session Attributes'.print_r($existingSession->attributes, true).$callString, CLogger::LEVEL_ERROR);

                if($callId) {
                    $this->actionHangupAllCalls($callId);
                }

                sleep(1);
                // update any sessions from today that do not have an end time to equal the current time. //@todo: enable this if the next fix doesn't do it for existing sessions. 4/2/7/15 - CLee
                if($existingSessionIds = Yii::app()->db->createCommand("SELECT s.id, s.start, (SELECT MAX(end_time) from calls WHERE call_session_id=s.id) newEndTime FROM call_sessions s WHERE DATE(s.start)=CURRENT_DATE AND s.contact_id=".Yii::app()->user->id." AND s.end IS NULL")->queryAll()) {

                    foreach($existingSessionIds as $existingSessionId) {

                        $existingSessionId['newEndTime'] = (empty($existingSessionId['newEndTime'])) ? '"'.$existingSessionId['start'].'"' : 'start';

                        // check on the max time for a call with this session and make that the last end call
                        if($countExistingSessionUpdated = Yii::app()->db->createCommand("UPDATE call_sessions SET end=".$existingSessionId['newEndTime']." WHERE contact_id=".Yii::app()->user->id." AND end IS NULL")->execute()) {
                            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: '.$countExistingSessionUpdated.' Call Sessions updated that had no end date. Session Call UUID: '.$existingSession->call_uuid.' Call ID: '.$callId.PHP_EOL.'Session Attributes'.print_r($existingSession->attributes, true).$callString, CLogger::LEVEL_ERROR);
                        }
                    }
                }
            }
        }

        // Make sure we have a call list ID
        $callListId = Yii::app()->request->getPost('call_list_id');
        $presetId = Yii::app()->request->getPost('preset_id');
        $callerIdName = Yii::app()->request->getPost('callerIdName');
        $callerIdNumber = Yii::app()->request->getPost('callerIdNumber');
        $numToCall = Yii::app()->request->getPost('num_to_call');
        $defaultVoicemailId = Yii::app()->request->getPost('default_voicemail');
        $phoneNumber = '1' . preg_replace('/[^0-9,.]/', '', Yii::app()->request->getPost('phone_number'));

        if(!($callList = CallLists::model()->findByPk($callListId))) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid Call List ID: '.$callListId);
        }

        // this can be reused in the actual update and error reporting if set to true. Prevents condition from going out of sync.
        $ringingCallListCondition = "l.id={$callList->id} AND c.status IN ('Ringing','Answered') AND c.updated_by=".Yii::app()->user->id." AND (l.contact_id IS NULL OR l.contact_id=".Yii::app()->user->id.")";

        // query so "Ringing" can be reported
        if($ringingIds = Yii::app()->db->createCommand("select c.*, l.id call_list_id, l.preset_ma, l.name, p.phone FROM call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id LEFT JOIN phones p ON p.id=c.phone_id WHERE {$ringingCallListCondition}")->queryAll()) {

            // set the ringing Id's to No Answer
            Yii::app()->db->createCommand("UPDATE call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id SET status='No Answer' WHERE {$ringingCallListCondition}")->execute();

            // send email of list of erroneously "Ringing" call_list_phone Id's
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Error: Dialer Start found Ringing or Answered phone # without an active Call Session. Changing to "No Answer".'.PHP_EOL.'You can update the flag above this line to disable this reporting.'.PHP_EOL.'Call List Phone Id #: '.print_r($ringingIds, true), CLogger::LEVEL_ERROR);
        }

        // Determine what URL we will be using for the Admin Socket Server, Special URL for testing Node things
        $nodeUrl = YII_DEBUG ? 'http://dev-nicole.christineleeteam.com:8080' : 'http://node-admin.seizethemarket.net';

        // Make the call
        $plivoCallData = array(
            'to'            => $phoneNumber,
            'from'          => self::FROM_NUMBER ,
            'ring_url'      =>  StmFunctions::getSiteUrl() . '/plivo/ring/id/' . $callListId,
            'fallback_url'  =>  StmFunctions::getSiteUrl() . '/plivo/fallback/id/' . $callListId,
            'answer_url'    =>  $nodeUrl . '/plivo/answerConfirmStart?id=' . $callListId. '&callerIdNumber='.$callerIdNumber.'&callerIdName='.$callerIdName.'&num_to_call=' . $numToCall.'&user_id='.Yii::app()->user->id.'&site='.urlencode(StmFunctions::getSiteUrl()),
            'hangup_url'    =>  $nodeUrl . '/plivo/hangup?id=' . $callListId .'&user_id='.Yii::app()->user->id . '&site='.urlencode(StmFunctions::getSiteUrl()),
            //                'ring_url'      =>  $nodeUrl . '/plivo/ring?id=' . $callListId,
            //                'fallback_url'  =>  $devUrl . '/plivo/fallback?id=' . $callListId

        );

        $response = Yii::app()->plivo->getPlivo()->make_call($plivoCallData);

        if($response['status'] >= 200 && $response['status'] <= 299) {
            $status = 'success';
        }
        else {

            // 2nd attempt to make the call before giving up
            $response = Yii::app()->plivo->getPlivo()->make_call($plivoCallData);
            if($response['status'] >= 200 && $response['status'] <= 299) {

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error making initial call. Plivo Response: '.print_r($response, true), CLogger::LEVEL_ERROR);
                $status = 'error';
            }
            else {
                $status = 'success';
            }
        }

        // Start call session
        $callSession = new CallSessions();
        $callSession->setAttributes(array(
            'call_list_id' => $callList->id,
            'number_lines' => $numToCall,
            'contact_id' => Yii::app()->user->id,
            'default_voicemail_id' => $defaultVoicemailId,
            'request_uuid' => $response['response']['request_uuid'], //this is available most actions except callback. Callbacks have call_uuid.
            'start' => new CDbExpression('NOW()'),
            'caller_id_name' => $callerIdName,
            'caller_id_number' => StmFormatter::formatInteger($callerIdNumber),
        ));

        if(!$callSession->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Call session did not save. Call List ID: '.$callList->id.PHP_EOL
                .'Error message: '.print_r($callSession->getErrors(), true).PHP_EOL
                .'Call Response: '.print_r($response, true), CLogger::LEVEL_ERROR);
            throw new Exception('Unable to create new call session');
        }

        // Return JSON response
        $this->_sendJson(array(
            'status'            =>  $status,
            'call_session_id'   =>  $callSession->id,
            'call_list_id'      =>  $callListId,
        ));
    }

    /**
     * Logs Voicemail from dialer GUI when answered and discovers it is a voicemail and not a real answered call.
     *
     * @param $id integer activity_log_id
     * return void
     */
    public function actionGotVoicemail($id)
    {
        if($activityLog = ActivityLog::model()->findByPk($id)) {
//            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI: Voicemail Data Check. Pls review and make sure this SHOULD be changed to voicemail. Possibility of someone pressing inadvertently. Current Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);

            $phone = Yii::app()->db->createCommand("select phone from calls c LEFT JOIN call_list_phones c2 ON c.call_list_phone_id=c2.id LEFT JOIN phones p ON c2.phone_id=p.id where c.id=".$activityLog->call_id)->queryScalar();

            $activityLog->is_spoke_to = 0;
            $activityLog->note = 'Voicemail - '.Yii::app()->format->formatPhone($phone);
            $activityLog->task_type_id = TaskTypes::DIALER_NO_ANSWER;

            if(!$activityLog->save()) { //undo command: update activity_log set task_type_id=42 WHERE id=226623
                // activity log did not save, send error message
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating  activity log for voicemail. Did not save properly. Error Message: '.print_r($activityLog->getErrors(), true).' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
            }

            // disconnect the leg for the voicemail call
            $this->actionHangupSpecificCall($activityLog->call_id);

        }
        else {
            // did not find activity log based on id,  send error message
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Did not find activity log to log voicemail. ActivityLogId# '.$id, CLogger::LEVEL_ERROR);
            $this->_sendJson(array('status' => 'error'));
        }
    }

    /**
     * Leave Voicemail
     *
     * @param $id integer callId
     */
    public function actionLeaveVoicemail($id)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('call_id', $id);
        $criteria->order = 'is_spoke_to DESC';

        // THIS IS DONE IN VOICEMAIL TRANSFER
//        if($activityLog = ActivityLog::model()->find($criteria)) {
//
//            $phone = Yii::app()->db->createCommand("select phone from calls c LEFT JOIN call_list_phones c2 ON c.call_list_phone_id=c2.id LEFT JOIN phones p ON c2.phone_id=p.id where c.id=".$activityLog->call_id)->queryScalar();
//
//            $activityLog->is_spoke_to = 0;
//            $activityLog->note = 'Left Voicemail - '.Yii::app()->format->formatPhone($phone);
//            $activityLog->task_type_id = TaskTypes::DIALER_LEFT_VOICEMAIL;
//
//            if(!$activityLog->save()) { //undo command: update activity_log set task_type_id=42 WHERE id=226623
//                // activity log did not save, send error message
//                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating activity log for left voicemail. Did not save properly. Error Message: '.print_r($activityLog->getErrors(), true).' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
//            }
//        }

        $voicemailId = Yii::app()->request->getParam('voicemailId');
        if(!$voicemailId) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Voicemail ID was not found.');
        }

        // get active call session for user and grab call uuid from that ... there is no uuid when it's ringing and no one has picked up yet.
        $call = Yii::app()->db->createCommand()
            ->select('c.dial_b_leg_uuid, c.call_uuid, c.call_list_phone_id, c.call_session_id, s.call_list_id')
            ->from('calls c')
            ->leftJoin('call_sessions s','s.id=c.call_session_id')
            ->where('c.id="'.$id.'"')
            ->queryRow();

        if($call['call_uuid']) {

            // mark call as no answer
            $result = Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."', updated=NOW(), updated_by=".Yii::app()->user->id." WHERE id=".$call['call_list_phone_id'])->execute();

            $response = Yii::app()->plivo->getPlivo()->transfer_call(array(
                    'call_uuid' =>  $call['call_uuid'],
                    'aleg_url' => $this->_nodeUrl . '/plivo/transferReturnToAnswer?id=' . $call['call_list_id'] . '&user_id='.Yii::app()->user->id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                    'bleg_url' => StmFunctions::getSiteUrl()  . '/plivo/transferLeaveVoicemail?id=' . $id . '&voicemailId=' . $voicemailId . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                    'legs'      => 'both',
                ));

            if($response['status'] == 202) {

                $status = 'success';
            }
            else {
                $status = 'error';
            }
        }

        $this->_sendJson(array('status' => $status));
    }

    /**
     * Delete Bad Number
     *
     * @param $id integer callId
     */
    public function actionDeleteBadNumber($id)
    {
        if(!$id || $id == 'undefined') {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Call ID was not found.');
        }

        // get active call session for user and grab call uuid from that ... there is no uuid when it's ringing and no one has picked up yet.
        $call = Yii::app()->db->createCommand()
            ->select('c.dial_b_leg_uuid, c.call_uuid, c.call_list_phone_id, c.call_session_id, s.call_list_id')
            ->from('calls c')
            ->leftJoin('call_sessions s','s.id=c.call_session_id')
            ->where('c.id="'.$id.'"')
            ->queryRow();

        if($call['call_uuid']) {

            $response = Yii::app()->plivo->getPlivo()->transfer_call(array(
                    'call_uuid' =>  $call['call_uuid'],
                    'aleg_url' => StmFunctions::getSiteUrl() . '/plivo/transferReturnToAnswer?id=' . $call['call_list_id'] . '&user_id='.Yii::app()->user->id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                    'bleg_url' => StmFunctions::getSiteUrl()  . '/plivo/transferHangup?id=' . $id . '&callListPhoneId=' . $call['call_list_phone_id'],
                    'legs'      => 'both',
                ));

            if($response['status'] == 202) {

                $status = 'success';
            }
            else {
                $status = 'error';
            }
        }

        $this->_sendJson(array('status' => $status));
    }

    /**
     * Adds a Call List Phone back to Queue by updating status
     * @param $id integer call_list_phone_id
     */
    public function actionAddToQueue($id)
    {
        if(Yii::app()->db->createCommand('UPDATE call_list_phones set status="'.CallListPhones::QUEUED.'", updated=NOW(), updated_by='.Yii::app()->user->id.' WHERE id='.$id)->execute()) {
            $this->_sendJson(array('status'=>'success'));
        }
    }


    /**
     * Action Do Not Call List
     *
     * Displays a Call List Phone to our internal Do Not Call List
     */
    public function actionDonotcalllist()
    {
//        Yii::import('admin_widgets.DateRanger.DateRanger');
//
//        $DateRanger = new DateRanger();
//        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);
//
//        $rawDataCommand = Yii::app()->db->createCommand()
//            ->select('l.name, s.number_lines, s.start, s.end, DATE_FORMAT(s.start, "%W") day_name, s.bill_duration total_time')
//            ->from("call_sessions s")
//            ->leftJoin("call_lists l", "l.id=s.call_list_id")
//            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
//            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'")
//            ->andWhere("s.contact_id=$id");
//
//        $totalSumCommand = Yii::app()->db->createCommand()
//            ->select('SUM(bill_duration)')
//            ->from("call_sessions s")
//            ->andWhere("DATE(s.start) >= '".$dateRange['from_date']."'")
//            ->andWhere("DATE(s.start) <= '".$dateRange['to_date']."'")
//            ->andWhere("s.contact_id=$id");
//
//        if(isset($_GET['componentTypeId']) && !empty($_GET['componentTypeId'])) {
//            $rawDataCommand
//                ->join('call_list_types clt','clt.id=l.preset_ma')
//                ->andWhere("clt.component_type_id=:component_type_id", array(':component_type_id'=>$_GET['componentTypeId']));
//            $totalSumCommand
//                ->join('call_lists l','l.id=s.call_list_id')
//                ->join('call_list_types clt','clt.id=l.preset_ma')
//                ->andWhere("clt.component_type_id=:component_type_id", array(':component_type_id'=>$_GET['componentTypeId']));
//        }
//
//        $rawData = $rawDataCommand->queryAll();
//        $totalSum = $totalSumCommand->queryScalar();
//
//        $dataProvider=new CArrayDataProvider($rawData, array(
//            'pagination'=>array(
//                'pageSize'=>100,
//            ),
//        ));

        $model = new CallListDoNotCall();

        $this->render('donotcalllist', array(
                'dataProvider'=>$model->search(),
            ));
    }

    /**
     * Action Do Not Call
     *
     * Adds a Call List Phone to our internal Do Not Call List
     * @param $id integer call_list_phone_id
     */
    public function actionAddDoNotCall($id)
    {
        $componentTypeId = Yii::app()->request->getParam('ctid');
        $componentId = Yii::app()->request->getParam('cid');
        $days = Yii::app()->request->getParam('days');

        $expireDate = date('Y-m-d', strtotime('+'.$days.' days'));

        if(!is_numeric($componentTypeId)) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Component Type Id is not numeric.');
        }

        if(!is_numeric($componentId)) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Component Id is not numeric.');
        }

        $contactId = CallListPhones::model()->skipSoftDeleteCheck()->findByPk($id)->phone->contact->id;

        // if record already exists, ignore if this date is less than existing. If after, update record.
        if($doNotCall = CallListDoNotCall::model()->findByAttributes(array('phone'=>Yii::app()->request->getParam('phoneNumber')))) {
            if($expireDate > $doNotCall->expire_date) {
                $doNotCall->expire_date = $expireDate;
            }
        }
        else {
            // Add to do not call list & add activity log using specific task_type_id
            $doNotCall = new CallListDoNotCall('dialer');
            $doNotCall->setAttributes(array(
                    'call_list_phone_id' => $id,
                    'contact_id' => $contactId,
                    'phone' => Yii::app()->request->getParam('phoneNumber'),
                    'expire_date' => $expireDate,
                ));
        }

        // Attempt to save the record
        if(!$doNotCall->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Do Not Call. Errors: ' . print_r($doNotCall->getErrors(), true) . ' Attributes: ' . print_r($doNotCall->attributes, true), CLogger::LEVEL_ERROR);
            //throw new Exception('Error saving do not call record!' . print_r($doNotCall->getErrors(), true));
        }

        // Create a new activity log
        $activityLog = new ActivityLog();
        $activityLog->activity_date = new CDbExpression('NOW()');
        $activityLog->component_type_id = $componentTypeId;
        $activityLog->component_id = $componentId;
        $activityLog->is_spoke_to = 1;
        $activityLog->note = 'Added phone number to our internal Do Not Call List - ' . Yii::app()->format->formatPhone(Yii::app()->request->getParam('phoneNumber')) . ' until '.date('m/d/Y', strtotime('+'.$days.'days'));
        $activityLog->task_type_id = TaskTypes::DIALER_ADD_DO_NOT_CALL;

        // Make sure activity log saves
        if(!$activityLog->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error updating activity log for do not call. Did not save properly. Error Message: ' . print_r($activityLog->getErrors(), true) . ' Activity Log Attributes:'.print_r($activityLog->attributes, true), CLogger::LEVEL_ERROR);
            //throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Error saving do not call activity log record!' . print_r($activityLog->getErrors(), true));
        }

        // Send JSON response of success
        $this->_sendJson(array('status'=>'success'));
    }

    /**
     * Skips the call list phone item, until the next timeframe (1 day, 3 hrs, etc) based on upaaated date
     * @param $id integer call_list_phone_id
     */
    public function actionSkipQueue($id)
    {
        $callListPhone = CallListPhones::model()->skipSoftDeleteCheck()->findByPk($id);
        $callListType = $callListPhone->callList->type;

//        $callList = Yii::app()->db->createCommand('select c2.preset_ma preset_ma FROM call_list_phones c LEFT JOIN call_lists c2 ON c2.id=c.call_list_id WHERE c.id='.$id)->queryRow();
        if(Yii::app()->db->createCommand('UPDATE call_list_phones set status="'.CallListPhones::SKIPPED.'", updated_by='.Yii::app()->user->id.', updated=NOW() WHERE id='.$id)->execute()) {

            //switch($callList['preset_ma']) {
            switch($callListType->table_type) {

                // task based list
//                case CallLists::MY_SELLERS_TASKS:
//                case CallLists::MY_BUYERS_TASKS:
//                case CallLists::MY_CONTACTS_TASKS:
//                case CallLists::MY_RECRUITS_TASKS:
//                case CallLists::LEAD_POOL_BUYERS_TASKS:
//                case CallLists::LEAD_POOL_SELLERS_TASKS:
//                case CallLists::LEAD_POOL_CONTACTS_TASKS:
//                case CallLists::LEAD_POOL_RECRUITS_TASKS:
                case CallListTypes::TASK_TABLE_TYPE:
                    $command = Yii::app()->db->createCommand()
                        ->select('t.component_type_id component_type_id, t.component_id component_id, p.phone')
                        ->from('call_list_phones c')
                        ->leftJoin('phones p','c.phone_id=p.id')
                        ->leftJoin('tasks t','c.task_id=t.id')
                        ->where('c.id='.$id);

                    // keep the $command variable so it can be access later if error
                    $callListPhoneData = $command->queryRow();

                    $componentId = $callListPhoneData['component_id'];

                    break;

                // time based list
//                case CallLists::ALL_NEW_SELLER_PROSPECTS:
//                case CallLists::ALL_NURTURING_SELLERS:
//                case CallLists::ALL_NEW_BUYER_PROSPECTS:
//                case CallLists::ALL_NURTURING_BUYERS:
//                case CallLists::ALL_D_SELLER_PROSPECTS:
//                case CallLists::ALL_D_BUYER_PROSPECTS:
//                case CallLists::CUSTOM_CONTACTS:
//                case CallLists::CUSTOM_SELLERS:
//                case CallLists::CUSTOM_BUYERS:
//                case CallLists::CUSTOM_RECRUITS:
                case CallListTypes::CUSTOM_TABLE_TYPE:
                case CallListTypes::TIME_TABLE_TYPE:

//                    $componentTypeId = CallLists::getComponentTypeByPreset($callList['preset_ma']);
                        //($callList['preset_ma'] == CallLists::ALL_NEW_SELLER_PROSPECTS || $callList['preset_ma'] == CallLists::ALL_NURTURING_SELLERS) ? ComponentTypes::SELLERS : ComponentTypes::BUYERS;

//                    if(in_array($callList['preset_ma'], array(CallLists::ALL_NEW_SELLER_PROSPECTS, CallLists::ALL_NURTURING_SELLERS, CallLists::CUSTOM_SELLERS, CallLists::ALL_D_SELLER_PROSPECTS))) {
//                        $componentTypeId = ComponentTypes::SELLERS;
//
//                    } elseif(in_array($callList['preset_ma'], array(CallLists::ALL_NEW_BUYER_PROSPECTS, CallLists::ALL_NURTURING_BUYERS, CallLists::CUSTOM_BUYERS, CallLists::ALL_D_BUYER_PROSPECTS))) {
//                        $componentTypeId = ComponentTypes::BUYERS;
//
//                    } elseif($callList['preset_ma'] == CallLists::CUSTOM_CONTACTS) {
//                        $componentTypeId = ComponentTypes::CONTACTS;
//
//                    } elseif($callList['preset_ma'] == CallLists::CUSTOM_RECRUITS) {
//                        $componentTypeId = ComponentTypes::RECRUITS;
//                    }

                    $command = Yii::app()->db->createCommand()
                        ->select('t.id component_id, p.phone phone')
                        ->from('call_list_phones c')
                        ->leftJoin('phones p','c.phone_id=p.id')
                        ->leftJoin('contacts c2','p.contact_id=c2.id')
                        //->leftJoin('transactions t','t.contact_id=c2.id AND t.component_type_id='.$componentTypeId)
                        ->where('c.id='.$id);

                    // joins based on componentTypeId
                    switch($callListType->component_type_id) {

                        case ComponentTypes::CONTACTS:
                            $command->leftJoin('contacts t','t.id=c2.id');
                            break;

                        case ComponentTypes::SELLERS:
                        case ComponentTypes::BUYERS:
                        $command->leftJoin('transactions t','t.contact_id=c2.id AND t.component_type_id='.$callListType->component_type_id);
                            break;

                        case ComponentTypes::RECRUITS:
                            $command->leftJoin('recruits t','t.contact_id=c2.id');
                            break;

                        default:
                            //error log
                            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Invalid Component Type Id: '.$callListType->component_type_id, CLogger::LEVEL_ERROR);
                            break;
                    }

                    // keep the $command variable so it can be access later if error
                    $callListPhoneData = $command->queryRow();

                    if(!$callListPhoneData) {
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: Did not find call_list_phones record when attempting to skip. Query: '.$command->getText(), CLogger::LEVEL_ERROR);
                    }

                    $componentId = $callListPhoneData['component_id'];

                    break;

                default:
                    throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Unknown type of table for getting calls by list Preset ID. '.$callListType->id);
                    break;
            }

            // add this note to activity log
            $activityLog = new ActivityLog();
            $activityLog->setAttributes(array(
                'component_type_id' => $callListType->component_type_id,
                'component_id' => $componentId,
                'task_type_id' => TaskTypes::DIALER_SKIP,
                'activity_date' =>  new CDbExpression('NOW()'),
                'note' => CallListPhones::SKIPPED.' - '.Yii::app()->format->formatPhone($callListPhoneData['phone']),
                'lead_gen_type_ma' => 0,
                'is_spoke_to' => 0,
                'is_public' => 0,
                'added' => new CDbExpression('NOW()')
            ));
            if(!$activityLog->save()) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Activity Log for Skip Queue did not save. Error Message: '.print_r($activityLog->getErrors(), true).' Attributes: '.print_r($activityLog->attributes, true).' Call List Phone Data: '.print_r($callListPhoneData, true).' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
            }
            $this->_sendJson(array('status'=>'success'));
        }
    }

    /**
     * Hangup specific call
     * @param $id integer call_id
     */
    public function actionHangupSpecificCall($id)
    {
        // get active call session for user and grab call uuid from that ... there is no uuid when it's ringing and no one has picked up yet.
        $call = Yii::app()->db->createCommand()
            ->select('c.dial_b_leg_uuid, c.call_uuid, c.call_list_phone_id, c.call_session_id, s.call_list_id')
            ->from('calls c')
            ->leftJoin('call_sessions s','s.id=c.call_session_id')
            ->where('c.id="'.$id.'"')
            ->queryRow();

        if($call['call_uuid']) {

            $nodeUrl = YII_DEBUG ? 'http://dev-nicole.christineleeteam.com:8080' : 'http://node-admin.seizethemarket.net';

            $response = Yii::app()->plivo->getPlivo()->transfer_call(array(
                    'call_uuid' =>  $call['call_uuid'],
                    'aleg_url' => $nodeUrl . '/plivo/transferReturnToAnswer?id=' . $call['call_list_id'] . '&user_id='.Yii::app()->user->id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                    'bleg_url' => StmFunctions::getSiteUrl() . '/plivo/transferHangup?id=' . $call['call_list_id'] . '&callListPhoneId=' .$id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                    'legs'      => 'both', //bleg
                ));
            if($response['status'] == 202) {

                //@todo: ACTIVITY LOG needs to get logged here if desired. Right now there is no logging so no attempt is visible.
                $status = 'success';
            }
            else {

                //@todo: If we keep getting errors, you can query to see if a call record with same session and call_list_phone id and hangup status exists and this may indicate double-clicking error. See about disabling once clicked.

//                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error transfer/hanging up call. Plivo Response: '.print_r($response, true).PHP_EOL.'UUID: '.$call['call_uuid'].PHP_EOL.' call_list_phone_id: '.$call['call_list_phone_id'], CLogger::LEVEL_ERROR);
                //$status = 'error';
            }

            // either way mark as No Answer call_list_phone_id
            //@todo: see if there is a hangup callback that will increment the call_count or not. ... that is supposed to mark these as No Answer but may be having issues.
            Yii::app()->db->createCommand('UPDATE call_list_phones set status="'.CallListPhones::NO_ANSWER.'", last_called=NOW(), updated=NOW(), updated_by='.Yii::app()->user->id.' WHERE id='.$call['call_list_phone_id'])->execute();

            $this->_sendJson(array('status' => $status));
        }
        else {
            if(!YII_DEBUG) {
                $backtrace = debug_backtrace();
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not find call to hangup specific call.'.PHP_EOL.'UUID: '.$call['dial_b_leg_uuid'].PHP_EOL.' call_id: '.$id.PHP_EOL.'Bracktrace: '.print_r($backtrace, true), CLogger::LEVEL_ERROR);
            }
        }
        $this->_sendJson(array('status'=>'success'));
    }

    /**
     * Hangup all calls related to that callId
     * @param $id integer call_id
     */
    public function actionHangupAllCalls($id, $sendJson=false)
    {
        // get active call session for user and grab call uuid from that ... there is no uuid when it's ringing and no one has picked up yet.
        $call = Yii::app()->db->createCommand()
            ->select('c.dial_b_leg_uuid, c.call_uuid, c.call_list_phone_id, c.call_session_id, s.call_list_id')
            ->from('calls c')
            ->leftJoin('call_sessions s','s.id=c.call_session_id')
            ->where('c.id="'.$id.'"')
            ->queryRow();

        if($call['call_uuid']) {
            // hangup specific call by call_id
            $response = Yii::app()->plivo->getPlivo()->hangup_call(array(
                'call_uuid' =>  $call['dial_b_leg_uuid'],
            ));

            if($response['status'] == 204) {
                if($sendJson) {
                    $this->_sendJson(array('status'=>'success'));
                }
            }
            else {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not find call to hangup all call.'.PHP_EOL.'UUID: '.$call['dial_b_leg_uuid'].PHP_EOL.' call_id: '.$id.PHP_EOL.'Response: '.print_r($response, true), CLogger::LEVEL_ERROR);
            }
        }
        else {
            if(!YII_DEBUG) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not find call to hangup specific call.'.PHP_EOL.'UUID: '.$call['dial_b_leg_uuid'].PHP_EOL.' call_id: '.$id, CLogger::LEVEL_ERROR);
            }
        }
    }

    /**
     * Hangup Call List Phone
     *
     * @param $id integer call_list_phone_id
     */
    public function actionHangupCallListPhone($id)
    {
        // get active call session for user and grab call uuid from that ... there is no uuid when it's ringing and no one has picked up yet.
        // this checks for  just the session first. If it's the first call, then the other data doesn't exist and causes errors.
        $command = Yii::app()->db->createCommand()
            ->select('id, call_uuid, call_list_id')
            ->from('call_sessions s')
            ->where('s.contact_id='.Yii::app()->user->id)
            ->andWhere('s.end IS NULL')
            ->order('s.id DESC');

        $callSession = $command->queryRow();

        if($callSession['call_uuid']) {
            $nodeUrl = YII_DEBUG ? 'http://dev-nicole.christineleeteam.com:8080' : 'http://node-admin.seizethemarket.net';

            //Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::NO_ANSWER."' WHERE id={$id}")->execute();

            $response = Yii::app()->plivo->getPlivo()->transfer_call(array(
                'call_uuid' =>  $callSession['call_uuid'],
                'aleg_url' => $nodeUrl . '/plivo/transferReturnToAnswer?id=' . $callSession['call_list_id'] . '&user_id='.Yii::app()->user->id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                'bleg_url' => StmFunctions::getSiteUrl() . '/plivo/transferHangup?id=' . $callSession['call_list_id'] . '&callListPhoneId=' . $id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                'legs'      => 'both', //bleg
            ));
            if($response['status'] == 202) {

                //@todo: ACTIVITY LOG needs to get logged here if desired. Right now there is no logging so no attempt is visible.

                $this->_sendJson(array('status'=>'success'));
            }
            else {

                // try again if get error
                $response = Yii::app()->plivo->getPlivo()->transfer_call(array(
                        'call_uuid' =>  $callSession['call_uuid'],
                        'aleg_url' => $nodeUrl . '/plivo/transferReturnToAnswer?id=' . $callSession['call_list_id'] . '&user_id='.Yii::app()->user->id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                        'bleg_url' => StmFunctions::getSiteUrl() . '/plivo/transferHangup?id=' . $callSession['call_list_id'] . '&callListPhoneId=' . $id . '&site=' . urlencode(StmFunctions::getSiteUrl()),
                        'legs'      => 'both', //bleg
                    ));

                //@todo: If we keep getting errors, you can query to see if a call record with same session and call_list_phone id and hangup status exists and this may indicate double-clicking error. See about disabling once clicked.

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error hanging up call both attempts. Plivo Response: '.print_r($response, true).PHP_EOL.'UUID: '.$callSession['call_uuid'].PHP_EOL.' call_list_phone_id: '.$id.' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
                $this->_sendJson(array('status'=>'error'));
            }
        }
        else {

            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not find call to hangup a call.'.PHP_EOL.'UUID: '.$callSession['call_uuid'].' This may be from a session that already ended and the status "Ringing" is stuck for some reason.'.PHP_EOL.' call_list_phone_id: '.$id.' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
            $this->_sendJson(array('status'=>'success'));
        }
    }

    /**
     * Dials the next numbers in the Queue
     *
     * Sends a request to the front end
     * @param $id integer call_session_id
     */
    public function actionDialNext($id)
    {
        $callSession = CallSessions::model()->findByPk($id);

        if(!$callSession) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Call session not found. Critical error. See Post & Header Data below.');
        }

//        $callDisposition = Yii::app()->request->getParam('callDisposition');
//        if(!$callDisposition) {
//            throw new Exception(__CLASS__.' (:'.__LINE__.') Call Disposition required.');
//        }

        $bLegCallListPhoneId = Yii::app()->request->getParam('callListPhoneId');
        if(!$bLegCallListPhoneId) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') B Leg Call List Phone required.');
        }

        //@todo: hangup all calls first?

        $response = Yii::app()->plivo->getPlivo()->transfer_call(array(
                'call_uuid' =>  $callSession->call_uuid,

                'aleg_url'        =>  StmFunctions::getSiteUrl() . '/plivo/dialNext?id='.$callSession->id . '&site='.urlencode(StmFunctions::getSiteUrl()),
//                'bleg_url' => StmFunctions::getSiteUrl()  . '/plivo/transferHangup?callListPhoneId=' . $bLegCallListPhoneId,
//                'legs'      => 'both',
            ));

        if($response['status'] == 202) {

            $status = 'success';
        }
        else {
            $status = 'error';
        }
        $this->_sendJson(array('status'=>$status));
    }

    /**
     * Send JSON
     *
     * Outputs JSON data and terminates application
     * @param array $data Array of data
     */
    protected function _sendJson($data)
    {
        // Set header and output JSON data
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }

    /**
     * Send Response
     *
     * Send the XML response
     * @return void
     */
    protected function _sendResponse()
    {
        // Set output to XML
        header('Content-type: text/xml');

        // Output XML body
        echo $this->_response->toXML();

        // Terminate execution
        Yii::app()->end();
    }

    public function actionRecordings()
    {
        $this->title = 'Dialer Calls';

        $model = new Calls();

        if(isset($_GET['contactId'])) {
            $model->contactId = $_GET['contactId'];
        }

        if(isset($_GET['callListId'])) {
            $model->callListId = $_GET['callListId'];
        }

        if(isset($_GET['phone'])) {
            $model->phone = Yii::app()->format->formatInteger($_GET['phone']);
        }

        if(isset($_GET['order'])) {
            $model->order = $_GET['order'];
        }

        $this->render('recordings', array(
            'model' => $model
        ));

    }

    public function actionRecordingDownload($id)
    {
        $model = Calls::model()->findByPk($id);
        header( "Content-Type: application/vnd.ms-excel; charset=utf-8" );
        $filename = str_replace(' ','',$model->callSession->contact->fullName).'-'.$model->id.'.mp3';
        header( "Content-Disposition: inline; filename=\"".$filename."\"" );
        readfile($model->record_url);
        Yii::app()->end();
    }

    //special temporary action to remove dupes in call_list_phones... to be deleted!
    public function actionRemoveDupes()
    {
        echo 'Starting Dupe Removal.<br>';

        $dbName = Yii::app()->db->createCommand("select DATABASE()")->queryScalar();
        $date = date('y-m-d-H-i-s');
        $dbHost = (YII_DEBUG)? 'localhost' : STM_DB_BACKUP_HOST;
        exec('mkdir -p /var/log/stm/dialer');
        $filename = "$dbName-call_list_phones-$date";

        echo 'Dumping table clp.<br>';
        exec(((YII_DEBUG) ? 'mysqldump' : '/usr/bin/mysqldump')." -h$dbHost $dbName call_list_phones > /var/log/stm/dialer/$filename.sql");

        // finds all dupe time-based call_list_phones records
        $dupes = Yii::app()->db->createCommand("SELECT count(id) count, sum(call_count) dialed,id, phone_id, call_list_id FROM `call_list_phones` WHERE task_id IS NULL GROUP BY phone_id, call_list_id HAVING count(id) >1")->queryAll();

        echo count($dupes).' dupes found.<br>';

        foreach($dupes as $i => $dupe) {

            // get all dupe records order by id ASC
            $callListPhoneDupes = Yii::app()->db->createCommand("SELECT id FROM call_list_phones WHERE call_list_id=".$dupe['call_list_id']." AND phone_id=".$dupe['phone_id']." ORDER BY id ASC")->queryColumn();

            $dupeData = Yii::app()->db->createCommand("SELECT SUM(call_count) totalCallCount, MIN(is_deleted) isAllDeleted FROM call_list_phones WHERE call_list_id=".$dupe['call_list_id']." AND phone_id=".$dupe['phone_id']." ORDER BY id ASC")->queryRow();

            // grabs record from the last updated record to save that data for update, update_by, last_called, status
            $lastUpdatedRecord = Yii::app()->db->createCommand("SELECT status, last_called, updated, updated_by FROM call_list_phones WHERE call_list_id=".$dupe['call_list_id']." AND phone_id=".$dupe['phone_id']." ORDER BY updated DESC LIMIT 1")->queryRow();

            $mergeToId = $callListPhoneDupes[0];
            $mergeFromIdsString = implode(',',array_slice($callListPhoneDupes, 1));

            // update call_list_phone_id in calls
            $dbTransaction = Yii::app()->db->beginTransaction();
            try {
                // merge all into the first/earliest record - add the count into the 1st one and set the rest to zero, pick the highest last_updated and select that as the last_called, updated_by, updated value... can have done for you by using MAX() in sql query... check to see if they are all deleted?
                Yii::app()->db->createCommand("UPDATE call_list_phones clp SET status='".$lastUpdatedRecord['status']."', call_count=".$dupeData['totalCallCount'].", last_called='".$lastUpdatedRecord['last_called']."', updated='".$lastUpdatedRecord['updated']."', updated_by=".$lastUpdatedRecord['updated_by'].", is_deleted=".$dupeData['isAllDeleted']." WHERE clp.id={$mergeToId}")->execute();

                // updates all call_list_phone_id column in calls records to $mergeToId
                Yii::app()->db->createCommand("UPDATE calls c LEFT JOIN call_list_phones clp ON clp.id=c.call_list_phone_id LEFT JOIN call_lists l ON l.id=clp.call_list_id SET c.call_list_phone_id={$mergeToId} WHERE call_list_phone_id IN({$mergeFromIdsString})")->execute();

                // permanently delete ... OR option #2 is set to zero on the counts and soft delete
                Yii::app()->db->createCommand("DELETE FROM call_list_phones where id IN({$mergeFromIdsString})")->execute();

                $dbTransaction->commit();
            }
            catch (Exception $e) {
                $dbTransaction->rollback();
                echo 'Error in removing dupe. Error Message: '.$e->getMessage().'<br>Dupe Data: '.print_r($dupe, true).'<br>';
            }

            if($i%100 == 0) {
                echo 'Progress - '.$i.' of '.count($dupes).'.<br>';
            }
        }
        echo '<br>Completed Removing Dupes.';
    }

	/** Added: Bkozak - 3/26/2015 - Vicemail Greetings */
	public function actionViewVoicemailGreetings()
    {
        $this->title = 'Dialer Voicemail Greetings';

		$model = new CallRecordings();
        $model->contact_id = Yii::app()->user->id;

		$this->render('viewvoicemailgreetings', array(
			'DataProvider' => $model->search()
		));
	}

	public function actionRecordVoicemailGreeting()
    {
		$numberToCall = Yii::app()->request->getPost('number_to_call');
		$numberToCall = "1".preg_replace('/[^0-9,.]/', '',$numberToCall);
		$title = Yii::app()->request->getPost('title');

		$response = Yii::app()->plivo->getPlivo()->make_call(array(
		   'to'            => $numberToCall,
		   'from'          => self::FROM_NUMBER ,
		   'ring_url'      =>  StmFunctions::getSiteUrl() . '/plivo/ring/',
		   'fallback_url'  =>  StmFunctions::getSiteUrl() . '/plivo/fallback/',
		   'answer_url'    =>  StmFunctions::getSiteUrl() . '/plivo/answerConfirmStartVm?contact_id=' . Yii::app()->user->id."&title=".  urlencode($title),
		   'hangup_url'    =>  StmFunctions::getSiteUrl() . '/plivo/hangupVm'
		));

		$this->_sendJson(array('status'=>'success'));
	}

	/** End Added: Bkozak - 3/26/2015 - Voicemail Greetings */

	public function actionDeleteVoicemailGreeting($id)
    {
        if($model = CallRecordings::model()->findByPk($id)) {

            // this delete will always be false due to soft deletes
            $model->delete();

            if(empty($model->getErrors())) {
                echo CJSON::encode(array('status'=>'success'));
            }
            else {
                echo CJSON::encode(array('status'=>'error','errorMessage'=>current($model->getErrors())[0]));
            }
        }

        Yii::app()->end();
	}

    /**
     * Answer Check
     *
     * Used to log current answered data when the answer box doesn't show up.
     */
    public function actionAnswerCheck()
    {
        $command = Yii::app()->db->createCommand()
            ->select('c.id callId, c.call_uuid, c.call_session_id, l.name listName, c.call_list_phone_id, clp.phone_id, p.phone, p.is_deleted phoneIsDeleted, a.id activity_log_id, a.component_type_id, a.component_id, a.note, c.event, c.dial_b_leg_to, c.added, c.recording_duration duration, clp.task_id, t.description task_description, clp.status callListStatus, clp.last_called, clp.added callListAdded, clp.updated callListUpdated, clp.updated_by callListUpdatedBy, clp.is_deleted callListDeleted')
            ->from('calls c')
            ->join('call_list_phones clp','c.call_list_phone_id=clp.id')
            ->join('call_sessions s','c.call_session_id=s.id')
            ->join('call_lists l','s.call_list_id=l.id')
            ->join('phones p','clp.phone_id=p.id')
            ->join('activity_log a','a.call_id=c.id')
            ->leftJoin('tasks t','clp.task_id=t.id')
            ->where('c.dial_b_leg_status = "answer"')
            ->andWhere((YII_DEBUG) ? '' : 'c.added > DATE_SUB(NOW(), INTERVAL 30 MINUTE)')
            ->andWhere('s.contact_id='.Yii::app()->user->id)
            ->order('c.id DESC')
            ->limit(3);

        $results = $command->queryAll();
        $query = $command->getText();

        $command2 = Yii::app()->db->createCommand()
            ->select('clp.id callListPhoneId, l.name listName, clp.phone_id, clp.task_id, clp.status callListStatus, clp.last_called, clp.added callListAdded, clp.updated callListUpdated, clp.updated_by callListUpdatedBy, clp.is_deleted callListDeleted')
            ->from('call_list_phones clp')
            ->join('call_lists l','clp.call_list_id=l.id')
            ->where('clp.status = "'.CallListPhones::ANSWERED.'"')
            ->andWhere((YII_DEBUG) ? '' : 'clp.updated > DATE_SUB(NOW(), INTERVAL 30 MINUTE)')
            ->andWhere('clp.updated_by='.Yii::app()->user->id)
            ->order('clp.updated DESC')
            ->limit(3);

        $results2 = $command2->queryAll();
        $query2 = $command2->getText();

        if(!YII_DEBUG) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Dialer user clicked the Answer Check button because answered call did not show. See data below.'.PHP_EOL.PHP_EOL.'Past 3 CALLS with "dial_b_leg_status" answer status in DESC order.'.PHP_EOL.'Data: '.print_r($results, true).PHP_EOL.PHP_EOL.'Query: '.$query.PHP_EOL.PHP_EOL.'Past 3 CALL LIST PHONES with Answered Status by user updated DESC order: '.print_r($results2, true).PHP_EOL.PHP_EOL.'Query 2: '.$query2, CLogger::LEVEL_ERROR);
        }

        //see if the 1st row from first query matches the 1st row from second query
        if(count($results[0]) > 0 && count($results2[0]) > 0 && $results[0]['call_list_phone_id'] == $results2[0]['callListPhoneId']) {

            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Answer Check Found: Both queries resulted in the same CallListPhoneId: '.$results2[0]['callListPhoneId'].'. See adjacent logs for details.', CLogger::LEVEL_ERROR);
        }

        if(!empty($results) && $component = ComponentTypes::getComponentModel($results[0]['component_type_id'], $results[0]['component_id'])) {

            // get a list of all the call lists this phone is in and format them in callListId => callListName
            $callListIds = Yii::app()->db->createCommand()
                ->select('clp.call_list_id id, l.name name, CONCAT("'.$results[0]['phone_id'].'") as phone_id, '.$results[0]['component_type_id'].' component_type_id, '.$results[0]['component_id'].' component_id')
                ->from('call_list_phones clp')
                ->join('call_lists l', 'l.id=clp.call_list_id')
                ->join('call_list_types t', 't.id=l.preset_ma')
                ->where('clp.phone_id='.$results[0]['phone_id'])
                ->andWhere('clp.is_deleted=0')
                ->andWhere(array('IN','t.table_type', array('custom')))
                ->group('clp.call_list_id')
                ->queryAll();

            $activityLogs = Yii::app()->db->createCommand()
                ->select('DATE_FORMAT(a.activity_date, "%c/%e/%Y") date, a.note')
                ->from('activity_log a')
                ->where('a.component_type_id='.$results[0]['component_type_id'])
                ->andWhere('a.component_id='.$results[0]['component_id'])
                ->order('a.activity_date DESC, a.id DESC')
                ->limit(40)
                ->queryAll();

                foreach($activityLogs as $i => $activityLog) {
                    $activityLogs[$i]['note'] = strip_tags($activityLog['note']);
                }

            // Mode data for active content
            $active = array(
                'call_id'           =>  $results[0]['callId'],
                'call_list_phone_id'=>  $results[0]['call_list_phone_id'],
                'call_list_ids'     =>  $callListIds,
                //                    'source'            =>  $callListPhone['source'], //@todo: test on task based first to make sure the blank doesn't error out
                'activity_log_id'   =>  $results[0]['activity_log_id'],
                'phone_id'          =>  $results[0]['phone_id'],
                'contact_id'        =>  $component->contact->id,
                'first_name'        =>  $component->contact->first_name,
                'last_name'         =>  $component->contact->last_name,
                'spouse_first_name' =>  ($component->contact->spouse_first_name)? ' & '.$component->contact->spouse_first_name : '',
                'spouse_last_name'  =>  $component->contact->spouse_last_name,
                'address'           =>  $component->contact->primaryAddress->address,
                'city'              =>  $component->contact->primaryAddress->city,
                'state'             =>  AddressStates::getShortNameById($component->contact->primaryAddress->state_id),
                'zipcode'           =>  $component->contact->primaryAddress->zip,
                'phone'             =>  $results[0]['phone'],
                'task_id'           =>  $results[0]['task_id'],
                'description'       =>  (YII_DEBUG) ? "" : $results[0]['task_description'],
                'activity_logs'     =>  $activityLogs,
                'component_id'      =>  $results[0]['component_id'],
                'component_type_id' =>  $results[0]['component_type_id'],
                'component_name'    =>  $component->componentType->name,
                'component_label'   =>  $component->componentType->getSingularName(),
            );
        }

        // Retrieve a list of phone numbers to call for the current session
        $this->_sendJson(array(
            'status'    =>  'success',
            'meta'	=>	array(
                'active' =>  $active,
            )
        ));
    }

    public function actionAnswerTracker()
    {
        $contactId = (isset($_GET['contactId']) && !empty($_GET['contactId'])) ? $_GET['contactId'] : Yii::app()->user->id;
        $timeframe = (isset($_GET['timeframe']) && (!empty($_GET['timeframe']) || $_GET['timeframe'] == 0)) ? $_GET['timeframe'] : null;
        $datetime = (isset($_GET['datetime']) && !empty($_GET['datetime'])) ? date('Y-m-d H:i:s', strtotime($_GET['datetime'])) : null;

        $data = array();
        if($_GET['contactId'] && ($_GET['timeframe'] || $_GET['timeframe'] == 0) && $datetime) {
            //select * from calls c JOIN call_list_phones l ON c.call_list_phone_id=l.id where c.dial_b_leg_status = 'answer' and c.added > '2015-05-07 10:30:00' AND l.updated_by=28642
            $command = Yii::app()->db->createCommand()
                ->select('c.id, c.call_session_id, l.name listName, c.call_list_phone_id, c.event, c.dial_b_leg_to, c.added, c.recording_duration duration, clp.task_id, clp.status callListStatus, clp.last_called, clp.added callListAdded, clp.updated callListUpdated, clp.is_deleted callListDeleted')
                ->from('calls c')
                ->join('call_list_phones clp','c.call_list_phone_id=clp.id')
                ->join('call_sessions s','c.call_session_id=s.id')
                ->join('call_lists l','s.call_list_id=l.id')
                ->where('c.dial_b_leg_status = "answer" and c.added > DATE_SUB(:datetime, INTERVAL :timeframe HOUR) AND clp.updated_by=:contactId', array(':timeframe'=>$timeframe, ':contactId'=>$contactId, ':datetime'=>$datetime))
                ->order('c.id');

            $data = $command->queryAll();
        }

        $dataProvider = new CArrayDataProvider($data, array(
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));

        $this->render('answerTracker', array(
                'dataProvider' => $dataProvider
            ));
    }

    public function actionRemoveFromCallList()
    {
        $callListId = Yii::app()->request->getPost('callListId');
        $componentTypeId = Yii::app()->request->getPost('componentTypeId');
        $componentId = Yii::app()->request->getPost('componentId');

        if($contactId = Yii::app()->request->getPost('contactId')) {
            $contact = Contacts::model()->findByPk($contactId);
        }

        if($phoneId = Yii::app()->request->getPost('phoneId')) {
            $phone = Phones::model()->skipSoftDeleteCheck()->findByPk($phoneId);
        }

        if(!is_numeric($callListId) || (!is_numeric($contactId) && !is_numeric($phoneId)) || !$componentTypeId || !is_numeric($componentTypeId) || !$componentId || !is_numeric($componentId)) {
            return;
        }
        $callList = CallLists::model()->skipSoftDeleteCheck()->findByPk($callListId);

        //@todo: find all instances of where this call_list type with all it's filter versions sit

        if($contact && !$phoneId) {
            $result = Yii::app()->db->createCommand("UPDATE call_list_phones clp JOIN phones p ON p.id=clp.phone_id SET clp.is_deleted=1, clp.hard_deleted=NOW(), clp.hard_deleted_by=".Yii::app()->user->id.", clp.updated=NOW(), clp.updated_by=".Yii::app()->user->id." WHERE clp.call_list_id={$callListId} AND p.contact_id={$contactId}")->execute();

            $activityLogNote = 'Deleted all contact phone numbers from Dialer Call List - '.$callList->name;
        }
        else {
            $result = Yii::app()->db->createCommand("UPDATE call_list_phones SET is_deleted=1, hard_deleted=NOW(), hard_deleted_by=".Yii::app()->user->id.", updated=NOW(), updated_by=".Yii::app()->user->id." WHERE call_list_id IN({$callListId}) AND phone_id={$phoneId}")->execute();

            $phoneNumber = Yii::app()->format->formatPhone($phone->phone);
            $activityLogNote = 'Deleted single phone number '.$phoneNumber.' from Dialer Call List - '.$callList->name;
        }

        if($result) {
            // add activity log
            $activityLog = new ActivityLog();
            $activityLog->setAttributes(array(
                    'component_type_id' => $componentTypeId,
                    'component_id' => $componentId,
                    'task_type_id' => TaskTypes::DIALER_LIST_DELETE,
                    'activity_date' =>  new CDbExpression('NOW()'),
                    'note' => $activityLogNote,
                    'lead_gen_type_ma' => 0,
                    'is_spoke_to' => 0,
                    'is_public' => 0,
                    'added' => new CDbExpression('NOW()')
                ));
            if(!$activityLog->save()) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Activity Log for Deleting from Call List did not save. Error Message: '.print_r($activityLog->getErrors(), true).PHP_EOL.'Attributes: '.print_r($activityLog->attributes, true).PHP_EOL.'Call List Data: '.print_r($callList->attributes, true), CLogger::LEVEL_ERROR);
            }
        }

        $this->_sendJson(array(
                'status'    =>  ($result) ? 'success' : 'error',
                'callListId' => "$callListId"
            ));
    }

    /**
     * Action Delete Call List Phones
     *
     * @throws Exception When invalid paramaters are present
     */
    public function actionDeletecalllistphone()
    {
        // Retrieve params
        $callListPhoneId = Yii::app()->request->getPost('call_list_phone_id');
        $type = Yii::app()->request->getPost('type');
        if(empty($callListPhoneId) || empty($type)) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Call List Phone ID and Type are both required parameters.', CLogger::LEVEL_ERROR);
            $this->_sendJson(array('status'=>'error'));
        }

        // Load up call list phone record
        $sourceCallListPhone = CallListPhones::model()->skipSoftDeleteCheck()->findByPk($callListPhoneId);
        if(!$sourceCallListPhone) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Call List Phone ID not found.', CLogger::LEVEL_ERROR);
            $this->_sendJson(array('status'=>'error'));
        }

        // Handle different types
        switch($type) {
            case 'one':

                // Delete the one and only record
                $sourceCallListPhone->setAttributes(array(
                        'is_deleted' => 1,
                        'hard_deleted' => new CDbExpression('NOW()'),
                        'hard_deleted_by' => Yii::app()->user->id,
                        'updated' => new CDbExpression('NOW()'),
                        'update_by' => Yii::app()->user->id,
                    ));
                $sourceCallListPhone->save();
                break;
            case 'all':

                // Get source contact //$sourceCallListPhone->phone->contact->id;
                $sourceContactId = Yii::app()->db->createCommand("SELECT p.contact_id FROM call_list_phones clp JOIN phones p ON p.id=clp.phone_id WHERE clp.id=".$sourceCallListPhone->id)->queryScalar();
                $phoneIds = Yii::app()->db->createCommand("SELECT id FROM phones where contact_id=".$sourceContactId)->queryColumn();

                // Retrieve all call list phones for call list
                $callListPhones = CallListPhones::model()->findAllByAttributes(array('phone_id' =>$phoneIds, 'call_list_id' => $sourceCallListPhone->call_list_id));
                foreach($callListPhones as $callListPhone) {

                    // Delete the entry if it is the same contact
                    if($callListPhone->phone->contact->id == $sourceContactId) {
                        $callListPhone->setAttributes(array(
                                'is_deleted' => 1,
                                'hard_deleted' => new CDbExpression('NOW()'),
                                'hard_deleted_by' => Yii::app()->user->id,
                                'updated' => new CDbExpression('NOW()'),
                                'update_by' => Yii::app()->user->id,
                            ));
                        $callListPhone->save();
                    }
                }

                break;
            default:
                throw new Exception('Invalid option for type');
                break;
        }

        // Report success
        $this->_sendJson(array('status'=>'success'));
    }

    public function actionRequeue()
    {
        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') FYI Requeue.', CLogger::LEVEL_ERROR);

        $callListPost = Yii::app()->request->getPost('CallLists');
        $type = Yii::app()->request->getPost('requeueType');
        $requeueDateFrom = Yii::app()->format->formatDate(Yii::app()->request->getPost('requeueDateFrom'), StmFormatter::MYSQL_DATE_FORMAT);
        $requeueDateTo = Yii::app()->format->formatDate(Yii::app()->request->getPost('requeueDateTo'), StmFormatter::MYSQL_DATE_FORMAT);

        $callList = CallLists::model()->findByPk($callListPost['id']);

        $requeueCount = 0;

        if(!is_numeric($callListPost['id'])) {
            echo CJSON::encode(array('status' => 'error', 'message' => 'Call List Id is invalid.'));
            Yii::app()->end();
        }

        $callListRequeueLog = new CallListRequeueLog();
        $callListRequeueLog->call_list_id = $callListPost['id'];
        $callListRequeueLog->added_by = Yii::app()->user->id;
        $callListRequeueLog->start_datetime = new CDbExpression('NOW()');

        if($type == 'all') {

            //@todo: some dupe in code - also similar 4 lines in type=all condition
            $callListRequeueLog->description = 'Requeue All';
            if(!$callListRequeueLog->save()) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') CallListRequeueLog did not save: Error Message: .'.print_r($callListRequeueLog->getErrors(), true), CLogger::LEVEL_ERROR);
            }
            // this will remove the CdbExpression from the start time.
            $callListRequeueLog = $callListRequeueLog->findByPk($callListRequeueLog->id);

            // queue up list based on filter settings and order them
            $presetId = Yii::app()->db->createCommand("SELECT preset_ma from call_lists where id=".$callListPost['id'])->queryScalar();
            $o = \StmDialer\Lists\Factory::getInstance($presetId, $callListPost['id']);

            $sortOrder = 1;
            $requeueCount = 0;
            $updateScript = '';

            // this function exists in custom list only
            $requeueData = $o->getReQueueData(Yii::app()->user->id);

            foreach($requeueData as $callListPhoneId) {

                // update call_list_phone sort order & queue status to "Queued"
                //$updateScript .= "UPDATE call_list_phones SET sort_order={$sortOrder}, is_deleted=0, status='".CallListPhones::QUEUED."', updated=NOW(), updated_by=".Yii::app()->user->id." WHERE id={$callListPhoneId} AND status NOT IN('".\CallListPhones::ANSWERED."','".\CallListPhones::RINGING."'); ";
                $updateScript .= "UPDATE call_list_phones SET sort_order={$sortOrder} WHERE id={$callListPhoneId}; ";

                $requeueCount++;
                $sortOrder++;
            }

            if($updateScript) {

                $callListRequeueLog->count = $requeueCount;

                Yii::app()->db->createCommand("UPDATE call_list_phones SET is_deleted=0, status='".CallListPhones::QUEUED."', updated=NOW(), updated_by=".Yii::app()->user->id." WHERE status NOT IN('".\CallListPhones::ANSWERED."','".\CallListPhones::RINGING."')")->execute();
                Yii::app()->db->createCommand($updateScript)->execute();
            }
            else {
                echo CJSON::encode(array('status' => 'success', 'message' => 'There were no phones in the list to requeue. Please check your list and try again.'));
            }
        }
        elseif($type == 'spot') {

            if(strpos($requeueDateFrom, '1969-12-31') !== false) {
                echo CJSON::encode(array('status' => 'error', 'message' => ''));
            }

            if(strpos($requeueDateTo, '1969-12-31') !== false) {
                echo CJSON::encode(array('status' => 'error', 'message' => ''));
            }

            //@todo: determine COMPONENT TYPE ID for joins to query
            //
            //
            //
            switch($callList->type->component_type_id) {

                case ComponentTypes::CONTACTS:
                    //
                    // @todo:
                    //
                    break;

                case ComponentTypes::SELLERS:
                    //
                    // @todo:
                    //
                    break;

                case ComponentTypes::BUYERS:
                    //
                    // @todo:
                    //
                    break;

                case ComponentTypes::RECRUITS:
                    //
                    // @todo:
                    //
                    break;
            }

            if(!($dateRangeType = Yii::app()->request->getPost('dateRangeType'))) {
                echo CJSON::encode(array('status' => 'error', 'message' => 'Date Range Type has must be selected from the drop down list. (ex. Added Date or Last Login)'));
                Yii::app()->end();
            }
            else {
                switch($dateRangeType) {
                    case 'added':
                        $requeueColumn = 'cnts.added';
                        break;

                    case 'last_login':
                        $requeueColumn = 'cnts.last_login';
                        break;

//                    case 'expired_date':
//                        $requeueColumn = 'expired'; //@todo: will need subquery to pull this data?
//                        break;

                    default:
                        echo CJSON::encode(array('status' => 'error', 'message' => 'Date Range Type "'.$dateRangeType.'" invalid (ex. Added Date or Last Login). Please verify your selection and try again or contact support.'));
                        Yii::app()->end();
                }
            }

            if(!($orderBy = Yii::app()->request->getPost('orderBy'))) {
                echo CJSON::encode(array('status' => 'error', 'message' => 'Order by has must be selected from the drop down list. (ex. # Dialed, Added Date or Last Login)'));
                Yii::app()->end();
            }
            else {
                // NOTE: order reverse as the queue is last in first out ****
                $phoneRequeueSubquery = '';
                switch($orderBy) {
                    case 'least_dialed':
                        $orderByString = 'dialer_count_30_days DESC';
                        $phoneRequeueSubquery = ", (select count(c2.id) from calls c2 WHERE clp.id=c2.call_list_phone_id AND DATE(c2.added) > DATE_SUB(NOW(), INTERVAL 30 DAY)) dialer_count_30_days";
                        break;

                    case 'login_desc':
                        $orderByString = 'cnts.last_login ASC';
                        break;

                    case 'component_added_desc':
                        $orderByString = 'cnts.added ASC'; //@todo: next iteration will reference componentModel
                        break;

                    case 'component_added_asc':
                        $orderByString = 'cnts.added DESC'; //@todo: next iteration will reference componentModel
                        break;
                }
            }

            //@todo: some dupe in code - also similar 4 lines in type=all condition
            $callListRequeueLog->description = 'Spot Requeue';
            $callListRequeueLog->from_date = $requeueDateFrom;
            $callListRequeueLog->to_date = $requeueDateTo;
            if(!$callListRequeueLog->save()) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') CallListRequeueLog did not save: Error Message: .'.print_r($callListRequeueLog->getErrors(), true), CLogger::LEVEL_ERROR);
            }
            // this will remove the CdbExpression from the start time.
            $callListRequeueLog = $callListRequeueLog->findByPk($callListRequeueLog->id);

            $minSortOrder = Yii::app()->db->createCommand("SELECT MIN(sort_order) FROM call_list_phones WHERE call_list_id=".$callListPost['id'])->queryScalar();

            // NOTE: order reverse as the queue is last in first out
//            $phonesToRequeue = Yii::app()->db->createCommand("SELECT c.id call_list_phone_id {$phoneRequeueSubquery} FROM call_list_phones c WHERE c.call_list_id=".$callListPost['id']." AND DATE({$reququeColumn}) >='".$requeueDateFrom."' AND DATE({$reququeColumn}) <= '".$requeueDateTo."' ORDER BY dialer_count_30_days DESC")->queryAll();

            $phonesToRequeueCommand = Yii::app()->db->createCommand()
                ->select("clp.id call_list_phone_id {$phoneRequeueSubquery}")
                ->from("call_list_phones clp")
                ->join("phones p","p.id=clp.phone_id")
                ->join("contacts cnts","cnts.id=p.contact_id")
                ->where("clp.call_list_id=".$callListPost['id'])
                ->andWhere("DATE({$requeueColumn}) >='".$requeueDateFrom."' AND DATE({$requeueColumn}) <= '".$requeueDateTo."'")
                ->order($orderByString);

            // if the order by needs contact info, add the joins to the command
//            if(in_array($dateRangeType, array('login_desc','component_added_desc','component_added_asc'))) {
//                $phonesToRequeueCommand
//                    ->join("phones p","p.id=clp.phone_id")
//                    ->join("contacts cnts","cnts.id=p.contact_id");
//            }

            $phonesToRequeue = $phonesToRequeueCommand->queryAll();

            $requeueCount = count($phonesToRequeue);
            $callListRequeueLog->count = $requeueCount;

            if(!empty($phonesToRequeue)) {

                $currSortOrder = $minSortOrder;
                $updateScript = '';
                foreach($phonesToRequeue as $i => $phoneToRequeue) {
                    $currSortOrder--;

                    $updateScript .= "UPDATE call_list_phones SET sort_order=".$currSortOrder.", status='".CallListPhones::QUEUED."', updated=NOW(), updated_by=".Yii::app()->user->id.", is_deleted=0 WHERE id=".$phoneToRequeue['call_list_phone_id']." AND status NOT IN('".CallListPhones::ANSWERED."','".CallListPhones::RINGING."'); ";
                }
                Yii::app()->db->createCommand($updateScript)->execute();
            }
        }

        $callListRequeueLog->complete_datetime = new CDbExpression('NOW()');
        if(!$callListRequeueLog->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') CallListRequeueLog did not save: Error Message: .'.print_r($callListRequeueLog->getErrors(), true), CLogger::LEVEL_ERROR);
        }

        echo CJSON::encode(array('status' => 'success', 'message' => $requeueCount.' records Re-queued.'));
    }

    /**
     * IO
     *
     * IO Failure from the dialer gui calls this method to send it to error log to be debugged.
     */
    public function actionIo()
    {
        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT: Dialer GUI IO Failure. See data below.'.PHP_EOL
            .'ID: '.Yii::app()->request->getPost('id').PHP_EOL
            .'Status: '.Yii::app()->request->getPost('status').PHP_EOL
            .'Response: '.Yii::app()->request->getPost('response').PHP_EOL
            .'Message: '.Yii::app()->request->getPost('message').PHP_EOL
            .'Note: '.Yii::app()->request->getPost('note').PHP_EOL
            .'Url: '.Yii::app()->request->getPost('url').PHP_EOL
            .(Yii::app()->request->getPost('message')) ? '' : 'All Data: '.Yii::app()->request->getPost('data').PHP_EOL
            , CLogger::LEVEL_ERROR);

        $this->_sendJson(array('status'=>'success'));
    }

    private function _generateDataAndWrite($fh, $tmpFile, $row, $maxPhones = 4, $maxEmails = 4) {
        $contact = NULL;

        $contact = Contacts::model()->findByPk($row['id']);
        if(empty($contact)) {
            return false;
        }

        $exportData = array(
            $row['id'],
            $row['first_name'],
            $row['last_name'],
            $row['spouse_first_name'],
            $row['spouse_last_name'],
        );

        $emailCount = 0;

        if($emails = Emails::model()->findAllByAttributes(array('contact_id' => $contact->id))) {
            foreach($emails as $key => $email) {
                if($key >= $maxEmails) {
                    break;
                }
                $exportData[] = $email->email;
                $emailCount++;
            }

            if($emailCount < $maxEmails) {
                for($i = ($maxEmails-$emailCount); $i > 0; $i--) {
                    $exportData[] = " ";
                }
            }
            unset($emails);
        } else {
            $exportData[] = " ";
            $exportData[] = " ";
            $exportData[] = " ";
            $exportData[] = " ";
        }

        $phonesCount = 0;
        if($phones = Phones::model()->findAllByAttributes(array('contact_id' => $contact->id))) {
            foreach($phones as $key => $phone) {
                if($key >= $maxPhones) {
                    break;
                }
                $exportData[] = $phone->phone;
                $phonesCount++;
            }

            if($phonesCount < $maxEmails) {
                for($i = ($maxPhones-$phonesCount); $i > 0; $i--) {
                    $exportData[] = " ";
                }
            }
            unset($phones);
        } else {
            $exportData[] = " ";
            $exportData[] = " ";
            $exportData[] = " ";
            $exportData[] = " ";
        }

        $addressLUAddresIds = CHtml::listData(AddressContactLu::model()->findAllByAttributes(array('contact_id' => $contact->id, 'is_deleted' => 0)), 'address_id', 'address_id');


        if($addressLUAddresIds) {
            $address = Addresses::model()->findByPk(key($addressLUAddresIds));

            $exportData[] = (!empty($address->address) ? $address->address : " ");
            $exportData[] = (!empty($address->city) ? $address->city : " ");
            $state = AddressStates::getShortNameById($address->state_id);

            $exportData[] = (!empty($state) ? $state : " ");
            $exportData[] = (!empty($address->zip) ? $address->zip : " ");

            unset($address);
        } else {
            $exportData[] = " ";
            $exportData[] = " ";
            $exportData[] = " ";
            $exportData[] = " ";
        }

        unset($addressLUAddresIds);
        unset($contact);

        $exportData[] = $row['call_list_name'];

        $put = @fputcsv($fh, $exportData);

        unset($exportData);


        if($put === FALSE) {
            @fclose($fh);

            if(file_exists($tmpFile)) {
                unlink($tmpFile);
            }

            Yii::app()->end();
        }
    }

    public function actionCallListExport($id)
    {
        ini_set('memory_limit', '700M'); //-1

        gc_enable();

        require_once(Yii::getPathOfAlias('admin_module.components.StmAws.S3')."/FileManagement.php");


        $clientId = Yii::app()->user->clientId;
        $accountId = Yii::app()->user->accountId;

        StmFunctions::frontConnectDbByClientIdAccountId($clientId, $accountId);

        $rows = Yii::app()->db->createCommand()
            ->select("c.id, c.first_name, c.last_name, c.spouse_first_name, c.spouse_last_name, l.name as call_list_name")
            ->from('call_lists l')
            ->leftJoin('call_list_phones clp','l.id = clp.call_list_id')
            ->leftJoin('phones p','p.id = clp.phone_id')
            ->leftJoin('contacts c','p.contact_id = c.id')
            ->where("clp.hard_deleted IS NULL AND l.id = '{$id}'")
            ->group("c.id")
            ->queryAll();

        $totalCount = count($rows);
        if($totalCount < 1) {
            header("HTTP/1.0 404 Not Found");
            echo("No rows found. Ending Export.");
            Yii::app()->end();
        } else {
            //$this->out($totalCount." rows to process...");
        }

        $tmpFile = "/tmp/manual_dailer_export_".time().".csv";
        $fh = fopen($tmpFile, "w");
        if($fh === FALSE) {
            header("HTTP/1.0 404 Not Found");
            echo("Could not open file for writing: ".$tmpFile);
            Yii::app()->end();
        }

        $exportDataHeaders = array("ID","First name","Last Name","Spouse First Name","Spouse Last Name","Email 1","Email 2","Email 3","Email 4","Phone 1","Phone 2","Phone 3","Phone 4","Address","City","State","Zip","Call List Name");
        $put = @fputcsv($fh, $exportDataHeaders);

        if($put === FALSE) {
            header("HTTP/1.0 404 Not Found");
            echo("Could not write headers to: ".$tmpFile);
            Yii::app()->end();
        }

        $rowsAdded = 0;
        $callListName = $rows[0]['call_list_name'];

        while($row = $rows[$rowsAdded]) {
            ob_end_clean();

            gc_collect_cycles();

            $this->_generateDataAndWrite($fh, $tmpFile, $row);

            unset($rows[$rowsAdded]);
            $rowsAdded++;

            sleep((1/5));
            gc_collect_cycles();
        }
        @fclose($fh);

        $s3bucket = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";

        $s3 = new \StmAws\S3\FileManagement($s3bucket);
        $s3Folder = '/site-files/'.$clientId.'/'.$accountId."/export/";
        $s3FileName = "dialer-export-".str_replace(array(' ','/'),'-', $callListName)."-".$rowsAdded."-".time().".csv";

        $fileAdded = $s3->addFile($tmpFile, $s3Folder.$s3FileName);

        if(!isset($fileAdded['ObjectURL']) || empty($fileAdded['ObjectURL'])) {
            header("HTTP/1.0 404 Not Found");
            echo('Failed to sync file: '.$tmpFile." to ".$s3Folder.$s3FileName);
            Yii::app()->end();
        }

        header("Content-Description: File Transfer");
        header('Content-Type: application/octet-stream');
        header('Expires: 0');
        header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Pragma: public');
        header("Content-disposition: attachment; filename=\"" . basename($tmpFile) . "\"");
        header("Set-Cookie: fileDownload=true; path=/");
        readfile($tmpFile);
        Yii::app()->end();
    }
}