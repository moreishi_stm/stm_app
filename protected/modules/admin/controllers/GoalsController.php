<?php
class GoalsController extends AdminController
{
	public $pageColor = 'blue';
    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
			'index'       =>'stm_app.modules.admin.controllers.actions.goals.ListAction',
            'list'        =>'stm_app.modules.admin.controllers.actions.goals.ListAction',
            'add'         =>'stm_app.modules.admin.controllers.actions.goals.AddAction',
			'addOneThing' =>'stm_app.modules.admin.controllers.actions.goals.AddOneThingAction',
            'addWeekly'  =>'stm_app.modules.admin.controllers.actions.goals.AddWeeklyAction',
			// 'data'     =>'stm_app.modules.admin.controllers.actions.goals.DataAction',
			'edit'        =>'stm_app.modules.admin.controllers.actions.goals.EditAction',
            'editOneThing' =>'stm_app.modules.admin.controllers.actions.goals.EditOneThingAction',
            'editWeekly'  =>'stm_app.modules.admin.controllers.actions.goals.EditWeeklyAction',
			'dashboard'   =>'stm_app.modules.admin.controllers.actions.goals.DashboardAction',
			// 'view'     =>'stm_app.modules.admin.controllers.actions.goals.ViewAction',
            'calculator'  =>'stm_app.modules.admin.controllers.actions.goals.CalculatorAction',
            'oneThing'    =>'stm_app.modules.admin.controllers.actions.goals.OneThingAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

// appts-set-met
    /**
     * Initializes the controller
     * @return none
     */
    public function init() {
        $this->displayName = 'Goals';
        $this->baseModel = new Goals;
    }

    public function zeroClass($model, $attribute)
    {
        if($model->{$attribute} == 0) {
            return ' zero';
        }
    }
}