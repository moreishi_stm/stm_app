<?php
class ReferralsController extends AdminController
{
    public $pageColor = 'rose';
    public function actions()
    {
    	return array(
			'add'    =>'stm_app.modules.admin.controllers.actions.referrals.AddAction',
			'view'   =>'stm_app.modules.admin.controllers.actions.referrals.ViewAction',
			'delete' =>'stm_app.modules.admin.controllers.actions.referrals.DeleteAction',
			'edit'  =>'stm_app.modules.admin.controllers.actions.referrals.EditAction',
			'index' =>'stm_app.modules.admin.controllers.actions.referrals.ListAction',
			'listMini' =>'stm_app.modules.admin.controllers.actions.referrals.ListMiniAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new Referrals;
		$this->displayName = 'Referrals';
	}
}