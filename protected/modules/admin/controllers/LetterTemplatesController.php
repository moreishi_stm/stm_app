<?php
class LetterTemplatesController extends AdminController {
    public $pageColor = 'blue';
	public $CachedTransactionType;

    public function actions() {
    	return array (
			'index' => 'stm_app.modules.admin.controllers.actions.letterTemplates.ListAction',
			'view' => 'stm_app.modules.admin.controllers.actions.letterTemplates.ViewAction',
			'edit' => 'stm_app.modules.admin.controllers.actions.letterTemplates.EditAction',
			'add' => 'stm_app.modules.admin.controllers.actions.letterTemplates.AddAction'
    	);
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return CMap::mergeArray(parent::filters(), array(
                'stmAccessControl' => array(
                    'class' => 'stm_app.modules.admin.components.filters.StmAccessControl',
                    'guestAllowedActions' => array(
                        'view'
                    )
                )
            ));
    }
}