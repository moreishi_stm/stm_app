<?php
class SourcesController extends AdminController
{
	public $pageColor = 'orange';

    public function actions()
    {
    	return array(
			'index'         =>'stm_app.modules.admin.controllers.actions.sources.ListAction',
			'view'          =>'stm_app.modules.admin.controllers.actions.sources.EditAction',
			'add'           =>'stm_app.modules.admin.controllers.actions.sources.AddAction',
            'delete'        =>'stm_app.modules.admin.controllers.actions.sources.DeleteAction',
            'undelete'        =>'stm_app.modules.admin.controllers.actions.sources.UndeleteAction',
			'campaigns'     =>'stm_app.modules.admin.controllers.actions.sources.CampaignsAction',
			'addCampaigns'  =>'stm_app.modules.admin.controllers.actions.sources.AddCampaignsAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new Sources;
	}
}