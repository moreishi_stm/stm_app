<?php
	class TelephonyController extends AdminController {

		public $pageColor = 'red';
		public $CachedTransactionType;

		public function actions() {
			return array(
				'add'          => 'stm_app.modules.admin.controllers.actions.pbx.AddAction',
				'view'         => 'stm_app.modules.admin.controllers.actions.pbx.ViewAction',
				'delete'       => 'stm_app.modules.admin.controllers.actions.pbx.DeleteAction',
				'edit'         => 'stm_app.modules.admin.controllers.actions.pbx.EditAction',
				'index'        => 'stm_app.modules.admin.controllers.actions.pbx.ListAction',
			);
		}

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
			$this->baseModel = new Contacts;
		}
	}
