<?php

/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class CallSessionController extends AdminController
{

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return array
     */
    public function actions()
    {

        $callListActionDirAlias = 'admin_module.controllers.actions.callSession';

        return array(
            'index'       => $callListActionDirAlias . '.IndexAction',
            'name'        => $callListActionDirAlias . '.NameAction',
            'view'        => $callListActionDirAlias . '.ViewAction',
            'answer'      => $callListActionDirAlias . '.AnswerAction',

            // add actions
            'task'        => $callListActionDirAlias . '.AddFromTaskAction',
            'bucket'      => $callListActionDirAlias . '.AddFromBucketAction',
            'manualqueue' => $callListActionDirAlias . '.AddFromManualQueueAction',
        );
    }
}