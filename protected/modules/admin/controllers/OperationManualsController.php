<?php
	class OperationManualsController extends AdminController {

		public $pageColor = 'red';
		public $CachedTransactionType;

		public function actions() {
			return array(
				'add'      => 'stm_app.modules.admin.controllers.actions.operationManuals.AddAction',
				'addTag'   => 'stm_app.modules.admin.controllers.actions.operationManuals.AddTagAction',
				'edit'     => 'stm_app.modules.admin.controllers.actions.operationManuals.EditAction',
				'editTag'  => 'stm_app.modules.admin.controllers.actions.operationManuals.EditTagAction',
				'index'    => 'stm_app.modules.admin.controllers.actions.operationManuals.ListAction',
				'tags'     => 'stm_app.modules.admin.controllers.actions.operationManuals.TagsAction',
				'view'     => 'stm_app.modules.admin.controllers.actions.operationManuals.ViewAction',
                'print'    => 'stm_app.modules.admin.controllers.actions.operationManuals.PrintAction',
                'delete'   => 'stm_app.modules.admin.controllers.actions.operationManuals.DeleteAction',
			);
		}

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
			$this->baseModel = new OperationManuals;
            $this->displayName = 'Operations Manual';
		}
	}
