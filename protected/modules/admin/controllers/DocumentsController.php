<?php
class DocumentsController extends AdminController {
	public $pageColor = 'teal';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

    /**
     * Specifies the action rules.
     *
     * @return array actions
     */
    public function actions() {
        return array(
			'upload'    =>'stm_app.modules.admin.controllers.actions.documents.UploadAction',
			'index'     =>'stm_app.modules.admin.controllers.actions.documents.ListAction',
			'listMini'  =>'stm_app.modules.admin.controllers.actions.documents.ListMiniAction',
			'add'       =>'stm_app.modules.admin.controllers.actions.documents.AddAction',
//			'dashboard' =>'stm_app.modules.admin.controllers.actions.documents.DashboardAction',
			'view'      =>'stm_app.modules.admin.controllers.actions.documents.ViewAction',
			'delete'    =>'stm_app.modules.admin.controllers.actions.documents.DeleteAction',
			'edit'      =>'stm_app.modules.admin.controllers.actions.documents.EditAction',
        );
    }

    public function actionDotLoop() {
        $this->title = 'Dot Loop';

        $dotLoopResponse = $this->dotLoopRequest('loop');
        $dataProvider = new CArrayDataProvider($dotLoopResponse, array('pagination'=>array('pageSize'=>50)));

        $this->render('dotLoopList', array('dataProvider'=>$dataProvider,'loopData'=>$dotLoopResponse));
    }

    public function actionDotLoopDetails($id)
    {
        $this->title = 'Dot Loop Details';
        $loopInfo = $this->dotLoopRequest('loop/'.$id);              // gets loop basic info
        $loopDetails = $this->dotLoopRequest('loop/'.$id.'/detail');   // gets loop details ...$dotLoopResponse2[sections][Financials][purchasePrice]
//        $activities = $this->dotLoopRequest('loop/'.$id.'/activity'); // gets loop activities ... message, activityDate
        $documents = $this->dotLoopRequest('loop/'.$id.'/document'); // gets list of docs for loop ... folderName, documentId, documentName, signatureVerificationLink, lastModifiedDate, createdDate, sharedWith, createdBy

//        $activitiesDataProvider = new CArrayDataProvider($activities, array('pagination'=>array('pageSize'=>50))); //activity
        $documentDataProvider = new CArrayDataProvider($documents, array('pagination'=>array('pageSize'=>50))); //documents

        $this->render('dotLoopDetails', array(
                'loopInfo' => $loopInfo,
                'loopDetails' => $loopDetails,
//                'activitiesDataProvider'=>$activitiesDataProvider,
                'documentDataProvider'=>$documentDataProvider,
            ));
    }

    public function actionDotLoopViewDocument($id)
    {
        $this->title = 'Dot Loop View Document';
        $documentId = Yii::app()->request->getQuery('documentId');
//        $documentName = urldecode(Yii::app()->request->getQuery('documentName'));
        $file = $this->dotLoopRequest('loop/'.$id.'/document/'.$documentId.'/'.$documentId.'.pdf', false);
        $profileId = 17952;
        $location = '/var/tmp/'.$documentId.'.pdf';
        file_put_contents($location, $file);

        if (file_exists($location)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf'); //application/octet-stream
            header('Content-Disposition: inline; filename='.$documentId.'.pdf'); //attachment makes is download
            header('Content-Transfer-Encoding: binary');
            header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() - 3600));
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($location));
            ob_clean();
            flush();
            readfile($location);
        }

        Yii::app()->end();
    }

    protected function dotLoopRequest($endPointUrl, $json=true)
    {
        $ch = curl_init();
        $accessToken = '64045c5c-d642-4200-a93b-cc27e1c31d0c';
        //curl -H "Authorization: Bearer XXXX" https://www.dotloop.com/my/api/v1_0/profile
        // must use proxy, current server is block, just like craigslist
        if(!YII_DEBUG) {
            curl_setopt($ch, CURLOPT_PROXY, 'http://cgp.seizethemarket.net');
            curl_setopt($ch, CURLOPT_PROXYPORT, '8080');
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'stm:Kaqufr3y');
        }

        // Gets google maps api data based on address
        $curlUrl = 'https://www.dotloop.com/my/api/v1_0/profile';

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$accessToken));

        curl_setopt($ch, CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //        $dotLoopResponse = curl_exec($ch);
        //
        //        if($dotLoopResponse) {
        //            $dotLoopResponse = json_decode($dotLoopResponse, true);
        //            foreach($dotLoopResponse as $dotLoopInfo) {
        //                if($dotLoopInfo['profileType'] == 'COMPANY') {
        //                    $profileId = $dotLoopInfo['profileId'];
        //                    break;
        //                }
        //            }
        //          // Need to store this somewhere as it will be reused with EVERY call. Most likely as key2
        //        }
        $profileId = 17952;
        if($profileId) {
            $curlUrl = 'https://www.dotloop.com/my/api/v1_0/profile/'.$profileId.'/'.$endPointUrl; //$endPointUrl
            curl_setopt($ch, CURLOPT_URL, $curlUrl);
            $dotLoopResponse = curl_exec($ch);
            if($json) {
                $dotLoopResponse = json_decode($dotLoopResponse, true);
            }
        }
        return $dotLoopResponse;
    }
	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new Documents;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax']==='documents-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
