<?php
class AuthccdController extends AdminController {

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new Tasks;
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

    public function actionAuth($id)
    {
        if (Yii::app()->user->isGuest) {
            $this->redirect(array('/admin/logout'));
        }

        if(empty($id)) {
            $this->redirect(array('/admin/logout'));
        }

        $Account = Accounts::model()->findByPk($id);

        if(empty($Account)) {
            Yii::app()->user->setFlash('error', 'Account not found.');
            $this->redirect(array('/admin'));
        }

        $Domain = Domains::model()->byIgnoreAccountScope()->findByAttributes(array('is_primary' => 1, 'account_id' => $Account->id));

        if(empty($Domain)) {
            Yii::app()->user->setFlash('error', 'Domains not found.');
            $this->redirect(array('/admin'));
        }

        $userEmail = Emails::model()->byIgnoreAccountScope()->findByAttributes(array('is_primary' => '1', 'contact_id' => Yii::app()->user->id));

        if(empty($userEmail)) {
            Yii::app()->user->setFlash('error', 'Email not found.');
            $this->redirect(array('/admin'));
        }

        /*Yii::app()->user->logout(true);
        Yii::app()->getRequest()->getCookies()->clear();*/

        $this->render('post', array(
                'domain' => (YII_DEBUG) ? str_replace('.com','.local',$Domain->name) : $Domain->name,
                'username' => $userEmail->email,
                'credentials' => Yii::app()->user->password
            ));
    }
}
