<?php
Yii::setPathOfAlias('api_resources', Yii::getPathOfalias('admin_module.controllers.resources'));

/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 8/12/13
 */

class ApiController extends CController {

	/**
	 * Used for http authentication
	 */
	const APPLICATION_ID = 'SEIZETHEMARKET';

	/**
	 * @var ApiAccess the api access ar instance
	 */
	protected $apiAccess;

	/**
	 * @var array $statusCodes provides list of all valid status codes
	 */
	protected $statusCodes = array(
		200 => 'OK',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
	);

	/**
	 * Currently json is the only available format
	 * The formats should be put into separate components, and have their own content types associated with them
	 * as well as formatting capabilities
	 */
	private $format = 'json';
	private $contentType = 'application/json';

	/**
	 * @param      $version
	 * @param      $resource
	 * @param null $resourceId
	 * @param null $association
	 */
	public function actionList($version, $resource, $resourceId = null, $association = null) {
		$this->checkAuth();

		$resourceController = $this->getResource($version, $resource);
		$resourceModel = $resourceController->getResourceModel();

		// Get the searchable criteria from the resource controller
		$responseSearchCriteria = $resourceController->getSearchCriteria();
		$responseSearchCriteria->mergeWith(array(
			'limit' => $this->getResponseLimit(),
		));

		$dataCollection = array();

		if ($resourceId) {
			$responseData = $resourceModel->findByPk($resourceId, $responseSearchCriteria);
			$responseModelRelations = $responseData->relations();
			if (!isset($responseModelRelations[$association])) {
				$this->sendResponse(500, 'association does not exist.');
			}

			if (is_array($responseData->$association)) {
				foreach ($responseData->$association as $associationModel) {
					array_push($dataCollection, $associationModel->attributes);
				}
			} else {
				array_push($dataCollection, $responseData->$association->attributes);
			}
		}
		else {
			$responseData = $resourceModel->findAll($responseSearchCriteria);

			foreach ($responseData as $responseDataModel) {
				array_push($dataCollection, $responseDataModel->attributes);
			}
		}


		$this->sendResponse(200, $dataCollection);
	}

	public function actionCreate($version, $resource, $resourceId = null, $association = null) {

	}

	public function actionView($version, $resource, $resourceId, $association = null, $assocationId = null) {
		$resourceController = $this->getResource($version, $resource);
		$resourceModel = $resourceController->getResourceModel();

		$responseData = $resourceModel->findByPk($resourceId);

		$dataCollection = array();
		array_push($dataCollection, $responseData->attributes);

		$this->sendResponse(200, $dataCollection);
	}

	public function actionUpdate($version, $resource, $resourceId = null, $association = null, $assocationId = null) {

	}

	public function actionDelete($version, $resource, $resourceId = null, $association = null, $assocationId = null) {

	}

	/**
	 * Used to limit the number of responses by default
	 * @return int
	 */
	private function getResponseLimit() {
		$responseLimit = Yii::app()->getRequest()->getQuery('limit');
		return ($responseLimit) ? $responseLimit : 25;
	}

	/**
	 * @param $response
	 */
	private function formatResponse($response) {
		$formatMethod = 'format'.strtoupper($this->format{0}).substr($this->format, 1);
		if (!method_exists($this, $formatMethod)) {
			$this->sendResponse(501, $this->format.' not yet supported.');
		}

		return $this->{$formatMethod}($response);
	}

	/**
	 * @param $response
	 * @return mixed
	 */
	private function formatJson($response) {
		return CJSON::encode($response);
	}

	private function getAssociationCriteria() {

	}

	/**
	 * @param $resource
	 * @return ApiController
	 */
	private function getResource($version, $resource) {
		// Get the formatted controller name ex. ContactsResourceController
		$resourceControllerNameFmt = strtoupper($resource{0}).substr($resource, 1).'Resource';

		// Import the resources as needed
		$resourceControllerAlias = 'api_resources.v'.$version.'.'.$resourceControllerNameFmt;
		$resourceActionsAlias = 'api_resources.v.'.$version.'.actions.'.$resource.'.*';
		$resourceFilePath = Yii::getPathOfAlias($resourceControllerAlias).'.php';
		$resourceActionsFolderPath = Yii::getPathOfAlias($resourceActionsAlias);

		// Sanity check the resources
		$resourceActionsPathExists = file_exists($resourceActionsFolderPath);
		$resourcePathExists = file_exists($resourceFilePath);
		if (!$resourcePathExists) {
			$this->sendResponse(501); // Not yet implemented (could not locate the resource controller)
		}

		// Import the base api resource controller, and then the actual resource and any external actions
		Yii::import('api_resources.v'.$version.'.ApiResource');
		Yii::import($resourceControllerAlias);
		Yii::import($resourceActionsAlias);
		$resourceController = new $resourceControllerNameFmt('api');
		if ($resourceController instanceof ApiResource) {
			return $resourceController;
		}
	}

	/**
	 * Checks access against the ApiAccess AR
	 * @return bool
	 */
	private function checkAuth() {
		$appAccessKey = 'HTTP_X_'.self::APPLICATION_ID.'_KEY';
		if(!isset($_SERVER[$appAccessKey])) {
			// Error: Unauthorized
			$this->sendResponse(401);
		}

		$apiAccessKey = $_SERVER[$appAccessKey];
		$apiAccess = ApiAccess::model()->find(array(
			'condition' => 'api_key = :api_key',
			'params' => array(
				':api_key' => $apiAccessKey,
			),
		));

		if (!$apiAccess) {
			$this->sendResponse(401, 'The api key used is invalid.');
		}

		// Mark that a request has been made in our system
		$apiRequest = new ApiRequest;
		$apiRequest->attributes = array(
			'api_access_id' => $apiAccess->id,
			'added' => new CDbExpression('NOW()'),
		);
		$apiRequest->save();

		return true;
	}

	protected function sendResponse($statusCode, $response = null) {
		$statusCodeMessage = $this->getStatusCodeMessage($statusCode);
		$statusHeaderPattern = 'HTTP/1.1 :statusCode :statusCodeMessage';
		$statusHeader = strtr($statusHeaderPattern, array(
			':statusCode' => $statusCode,
			':statusCodeMessage' => $statusCodeMessage,
		));
		header($statusHeader);
		header('Content-type: '.$this->contentType);

		if (!$response) {
			// Need to create the body message to send
			switch ($statusCode) {
				case 401:
					$response = 'You must be authorized to view this resource.';
				break;

				case 404:
					$response = 'The resource you attempted to retrieve does not exist.';
				break;

				case 500:
					$response = 'We are unable to process your resource at this time.';
				break;

				case 501:
					$response = 'This resource is not yet implement.';
				break;
			}
		}

		// Get a count of matched rows if the response is a collection
		$matchCount = (is_array($response)) ? count($response) : null;

		// @todo: Implement this feature
		$requestsCount = 'unlimited';

		// The actual returned response collection
		$responseReturn = array(
			'matchCount' => $matchCount,
			'statusCode' => $statusCode,
			'response' => $response,
		);

		echo $this->formatResponse($responseReturn);
		Yii::app()->end();
	}

	protected function getStatusCodeMessage($statusCode) {
		return (isset($this->statusCodes[$statusCode])) ? $this->statusCodes[$statusCode] : '';
	}
}