<?php
/**
 * Search Controller
 *
 * Provides general search functionality for things
 */
class SearchController extends AdminController
{
    /**
     * Action Index
     *
     * Default page load action
     * @return void
     */
    public function actionIndex()
    {
        // Register JS libs we need
        Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/lib/handlebars/handlebars-4.0.5.js', CClientScript::POS_END);

        // Render the view
        $this->render('index', array());
    }

    /**
     * Action Results
     *
     * Handles returning search results via AJAX
     * @return void
     */
    public function actionResults()
    {
        // Retrieve search query from POST variables
        $query = trim(Yii::app()->request->getPost('query'));
        $queryInteger = Yii::app()->format->formatInteger($query);

        header('Content-type: application/json');

        if(in_array($query, array('', null))) {

            echo CJSON::encode(array());
            Yii::app()->end();
        }

        // Leave the below stuff for reference
//        "(
//                    IF(
//                        @aSearchVal:=(
//                            SELECT CONCAT(a.address, ' ', a.city, ' ', a.zip)
//                            FROM addresses AS a
//
//                                LEFT JOIN address_contact_lu AS acl ON acl.contact_id = c.id AND acl.is_deleted = 0 AND acl.is_primary = 1
//                            WHERE a.address LIKE :likeQuery
//                            LIMIT 1
//                        ),
//                        @aSearchVal,
//                        '123'
//                    )
//                ) AS full_address",


//        "(
//                    IF(
//                        @eSearchVal:=(SELECT eSearch.email FROM emails AS eSearch WHERE c.id = eSearch.contact_id AND eSearch.is_deleted = 0 AND eSearch.email = :query LIMIT 1),
//                        @eSearchVal,
//                        IF(
//                            @ePrimaryVal:=(SELECT ePrimary.email FROM emails AS ePrimary WHERE c.id = ePrimary.contact_id AND ePrimary.is_deleted = 0 AND ePrimary.email = :query LIMIT 1),
//                            @ePrimaryVal,
//                            (SELECT eFirst.email FROM emails AS eFirst WHERE c.id = eFirst.contact_id LIMIT 1)
//                        )
//                    )
//                ) AS email",


        // Create a raw command to pull data
        /** @var CDbCommand $command */
        $selectData = array(

                // Get IDs
                'c.id AS contact_id',
                't.id AS transaction_id',
                'r.id AS recruit_id',
                'cl.id AS closing_id',

                // Basic contact info
                'c.first_name AS first_name',
                'c.last_name AS last_name',
                'c.spouse_first_name AS spouse_first_name',
                'c.spouse_last_name AS spouse_last_name',
                "CONCAT(c.last_name, ', ', c.first_name) AS combined_name",
                "CONCAT(c.spouse_last_name, ', ', c.spouse_first_name) AS spouse_combined_name",

                // Determine what component type this result represents, also get a link id for it
                "IF(r.id, 'recruits', IF(cl.id, 'closings', IF(ct.id, ct.name, 'contacts'))) AS component_type",
                "IF(r.id, 'Recruit', IF(cl.id, 'Closing', IF(ct.id, CONCAT(UPPER(SUBSTRING(ct.name, 1, 1)),SUBSTRING(ct.name, 2, CHAR_LENGTH(ct.name)-2)), 'Contact'))) AS component_type_label",
                "IF(r.id, r.id, IF(cl.id, cl.id, IF(t.id, t.id, c.id))) AS link_id",

                // Primary, search, and first-in-line emails
                'e.email AS primary_email',
                'eSearch.email AS search_email',
                'eFirst.email AS first_email',

//                // Primary Address
//                'a.address AS primary_address',
//                'a.city AS primary_city',
//                'as.short_name AS primary_state_short',
//                'a.zip AS primary_zip',
//
//                // Search Address
//                'aSearch.address AS search_address',
//                'aSearch.city AS search_city',
//                'asSearch.short_name AS search_state_short',
//                'aSearch.zip AS search_zip',
//
//                // First Address
//                'aFirst.address AS first_address',
//                'aFirst.city AS first_city',
//                'asFirst.short_name AS first_state_short',
//                'aFirst.zip AS first_zip',

                // Primary, search, and first-in-line phones
                'p.phone AS primary_phone',
                'pFirst.phone AS first_phone',
            );

        if($queryInteger) {
            $selectData[] = 'pSearch.phone AS search_phone';
        }


        $command = Yii::app()->db->createCommand()
            ->select($selectData)
            // Pull from contacts table
            ->from('contacts AS c')

            // Primary, search, and first-in-line emails
            ->leftJoin('emails AS e', 'c.id = e.contact_id AND e.is_deleted = 0 AND e.is_primary = 1')
            ->leftJoin('emails AS eSearch', 'c.id = eSearch.contact_id AND eSearch.is_deleted = 0 AND eSearch.email LIKE :likeQuery')
            ->leftJoin('emails AS eFirst', 'c.id = eFirst.contact_id AND eFirst.is_deleted = 0')

            // Primary, search, and first-in-line phones
            ->leftJoin('phones AS p', 'c.id = p.contact_id AND p.is_deleted = 0 AND p.is_primary = 1')
            ->leftJoin('phones AS pFirst', 'c.id = pFirst.contact_id AND pFirst.is_deleted = 0')

            // Recruits data
            ->leftJoin('recruits AS r', 'c.id = r.contact_id AND r.is_deleted = 0')

            // Transaction record and associated component type
            ->leftJoin('transactions AS t', 'c.id = t.contact_id AND t.transaction_status_id <> '.TransactionStatus::TRASH_ID.' AND t.is_deleted = 0')
            ->leftJoin('component_types AS ct', 't.component_type_id = ct.id')

            // Closing
            ->leftJoin('closings AS cl', 'cl.transaction_id = t.id AND cl.is_deleted = 0')

//            // Primary Address
//            ->leftJoin('address_contact_lu AS acl', 'acl.contact_id = c.id AND acl.is_deleted = 0 AND acl.is_primary = 1')
//            ->leftJoin('addresses AS a', 'a.id = acl.address_id AND a.is_deleted = 0')
//            ->leftJoin('stm_hq.address_states AS as', 'as.id = a.state_id')
//
//            // Search Address
//            ->leftJoin('address_contact_lu AS aclSearch', 'aclSearch.contact_id = c.id AND aclSearch.is_deleted = 0 AND aclSearch.is_primary = 1')
//            ->leftJoin('addresses AS aSearch', 'aSearch.id = aclSearch.address_id AND aSearch.is_deleted = 0 AND aSearch.address LIKE :likeQuery')
//            ->leftJoin('stm_hq.address_states AS asSearch', 'asSearch.id = aSearch.state_id')
//
//            // First in line Address
//            ->leftJoin('address_contact_lu AS aclFirst', 'aclFirst.contact_id = c.id AND aclFirst.is_deleted = 0')
//            ->leftJoin('addresses AS aFirst', 'aFirst.id = aclFirst.address_id AND aFirst.is_deleted = 0')
//            ->leftJoin('stm_hq.address_states AS asFirst', 'asFirst.id = aFirst.state_id')

            // Top level filters
            ->where('c.account_id = :account_id')
            ->andWhere('c.is_deleted = 0')
            ->andWhere("
                eSearch.email LIKE :likeQuery OR
                c.last_name LIKE :likeQuery OR
                c.spouse_last_name LIKE :likeQuery OR
                CONCAT(c.first_name, ' ', c.last_name) LIKE :likeQuery OR
                CONCAT(c.spouse_first_name, ' ', c.spouse_last_name) LIKE :likeQuery"
                .(($queryInteger) ? " OR pSearch.phone = :queryInteger" : "") // if integer exists to compare to phone number
            )
            // OR aSearch.address LIKE :likeQuery //@todo: CLee removed - 3/28/16 when disabling address for now

            // Group items to avoid duplicates
            ->group("CONCAT(component_type, '-', link_id)")

            // Arbitrary limit just in case
            ->limit(20);

        // only do this extra join if need to search by phone which is indicated by whether the query string has integers.
        if($queryInteger) {
            $command
                ->leftJoin('phones AS pSearch', 'c.id = pSearch.contact_id AND pSearch.is_deleted = 0 AND pSearch.phone = :queryInteger')
                ->bindParam(':queryInteger', $queryInteger);
        }

        // Bind params
        $likeQuery = '%' . $query . '%';
        $command->bindParam(':query', $query);
        $command->bindParam(':likeQuery', $likeQuery);
        $command->bindParam(':account_id', Yii::app()->user->account_id);

        // Retrieve records
        $records = $command->queryAll(true);

        // Fix up results for handlebars
        foreach($records as $key => $record) {

            // Email address
            if($record['search_email']) {
                $records[$key]['has_email'] = true;
                $records[$key]['email'] = $record['search_email'];
            }
            elseif($record['primary_email']) {
                $records[$key]['has_email'] = true;
                $records[$key]['email'] = $record['primary_email'];
            }
            elseif($record['first_email']) {
                $records[$key]['has_email'] = true;
                $records[$key]['email'] = $record['first_email'];
            }

            unset($records[$key]['search_email']);
            unset($records[$key]['primary_email']);
            unset($records[$key]['first_email']);

            // Determine phone we want to use
            if($record['search_phone']) {
                $records[$key]['has_phone'] = true;
                $records[$key]['phone'] = Yii::app()->format->formatPhone($record['search_phone']);
            }
            elseif($record['primary_phone']) {
                $records[$key]['has_phone'] = true;
                $records[$key]['phone'] = Yii::app()->format->formatPhone($record['primary_phone']);
            }
            elseif($record['first_phone']) {
                $records[$key]['has_phone'] = true;
                $records[$key]['phone'] = Yii::app()->format->formatPhone($record['first_phone']);
            }

            unset($records[$key]['search_phone']);
            unset($records[$key]['primary_phone']);
            unset($records[$key]['first_phone']);

            // Address
//            if($record['search_address']) {
//                $records[$key]['has_address'] = true;
//                $records[$key]['address'] = $record['search_address'];
//                $records[$key]['city'] = $record['search_city'];
//                $records[$key]['state_short'] = $record['search_state_short'];
//                $records[$key]['zip'] = $record['search_zip'];
//            }
//            elseif($record['primary_address']) {
//                $records[$key]['has_address'] = true;
//                $records[$key]['address'] = $record['primary_address'];
//                $records[$key]['city'] = $record['primary_city'];
//                $records[$key]['state_short'] = $record['primary_state_short'];
//                $records[$key]['zip'] = $record['primary_zip'];
//            }
//            elseif($record['first_address']) {
//                $records[$key]['has_address'] = true;
//                $records[$key]['address'] = $record['first_address'];
//                $records[$key]['city'] = $record['first_city'];
//                $records[$key]['state_short'] = $record['first_state_short'];
//                $records[$key]['zip'] = $record['first_zip'];
//            }

            // Remove junk we don't need after this iteration
            unset($records[$key]['search_address']);
            unset($records[$key]['search_city']);
            unset($records[$key]['search_state_short']);
            unset($records[$key]['search_zip']);
            unset($records[$key]['primary_address']);
            unset($records[$key]['primary_city']);
            unset($records[$key]['primary_state_short']);
            unset($records[$key]['primary_zip']);
            unset($records[$key]['first_address']);
            unset($records[$key]['first_city']);
            unset($records[$key]['first_state_short']);
            unset($records[$key]['first_zip']);
        }

        // Set header and output JSON data
        echo CJSON::encode($records);

        // Terminate application
        Yii::app()->end();
    }
}