<?php
class ContactTypesController extends AdminController
{
	public $pageColor = 'purple';

    public function actions()
    {
    	return array(
			'index'       =>'stm_app.modules.admin.controllers.actions.contactTypes.ListAction',
			'edit'        =>'stm_app.modules.admin.controllers.actions.contactTypes.EditAction',
			'view'        =>'stm_app.modules.admin.controllers.actions.contactTypes.EditAction',
			'add'         =>'stm_app.modules.admin.controllers.actions.contactTypes.AddAction',
            'delete'      =>'stm_app.modules.admin.controllers.actions.contactTypes.DeleteAction',
    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new ContactTypes;
		$this->displayName = 'Contact Types';
	}
}