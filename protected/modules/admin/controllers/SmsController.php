<?php
Yii::import('admin_widgets.DateRanger.DateRanger');

class SmsController extends AdminController {

	/**
	 * Initializes the controller
	 *
	 * @return void
	 */
	public function init() {
		$this->baseModel = new SmsMessages;
	}

    public function actionIndex()
    {
        $this->title = 'SMS Activity';
        $this->pageColor = 'blue';
        $this->baseModel = new SmsMessages();

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'month_to_date';
        $dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

        $smsActivity = array();
        $smsMessages = new SmsMessages();

        $clientAccountId = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=>Yii::app()->user->accountId))->id;

        $toCriteria = new CDbCriteria;
        $toCriteria->condition = 'contact_id = :contact_id AND status = :status AND client_account_id=:clientAccountId';
        $toCriteria->params = array(
            ':clientAccountId' => $clientAccountId,
            ':contact_id'    => Yii::app()->user->id,
            ':status'		=> 1
        );
        $checkForSmsNumberHQ = TelephonyPhones::model()->find($toCriteria);

        $Criteria = new CDbCriteria;
        $Criteria->join = 'JOIN stm_hq.telephony_phones tp ON CONCAT("1",tp.phone)=t.from_phone_number OR CONCAT("1",tp.phone)=t.to_phone_number';
        $Criteria->addCondition('contact_id = :contact_id AND status = :status AND client_account_id=:clientAccountId');
        $Criteria->params = array(
            ':clientAccountId' => $clientAccountId,
            ':contact_id'    => Yii::app()->user->id,
            ':status'		=> 1
        );

        if($checkForSmsNumberHQ) {
            if(isset($_POST['to_phone_id'])) {
                $Criteria->condition = 'to_phone_id = :toPhoneId';
                $Criteria->params = array(
                    ':toPhoneId'    => $_POST['to_phone_id'],
                );

                $foundSmsActivity = $smsMessages->findAll($Criteria);
            } else {
				if(isset($_GET["SmsListFilterForm"]) || isset($_GET["SmsListFilterForm"]["q"])){

					$q =  (int)preg_replace('/\D/', '', $_POST["SmsListFilterForm"]["q"]);
					if(!empty($q)){
						$Criteria->condition = 'to_phone_number like :q OR from_phone_number like :q';
						$Criteria->params = array(
							":q"    => "%".$q."%"
						);
						$Criteria->limit = 100;
					}

				}else{
					$Criteria->condition = 'to_phone_number like :toPhoneId OR from_phone_number like :fromPhoneNumber';
					$Criteria->group = 'to_phone_number, from_phone_number';
					$Criteria->params = array(
						':toPhoneId'    => '%'.$checkForSmsNumberHQ->phone,
						':fromPhoneNumber' => '%'.$checkForSmsNumberHQ->phone,
					);
				}
                $sortOrder = (isset($_GET["SmsListFilterForm"]["sort"]) && $_GET["SmsListFilterForm"]["sort"] === "old")? "ASC" : "DESC";

                $Criteria->order = 'processed_datetime '.$sortOrder;

                $foundSmsActivity = $smsMessages->findAll($Criteria);
            }
        }

        $numbersFound = array();

        if(!empty($foundSmsActivity)) {
            foreach($foundSmsActivity as $activity) {
                if(!in_array($activity->to_phone_number, $numbersFound) && $activity->to_phone_number != '1'.$checkForSmsNumberHQ->phone) {
                    $smsActivity[] = array(
                        "id" => $activity->to_phone_number,
                        "phone" => $activity->to_phone_number,
                        'processed_datetime' => $activity->processed_datetime
                    );
                    $numbersFound[] = $activity->to_phone_number;
                }

                if(!in_array($activity->from_phone_number, $numbersFound) && $activity->from_phone_number != '1'.$checkForSmsNumberHQ->phone) {
                    $smsActivity[] = array(
                        "id" => $activity->from_phone_number,
                        "phone" => $activity->from_phone_number,
                        'processed_datetime' => $activity->processed_datetime
                    );
                    $numbersFound[] = $activity->from_phone_number;
                }
            }
        }

        $DataProvider = new CArrayDataProvider($smsActivity, array('pagination' => false));

        $this->render("sms", array(
                'model' => $this->baseModel,
                'DataProvider' => $DataProvider,
                'dateRange' => $dateRange,
                'stmNumber' => $checkForSmsNumberHQ
            ));
    }

    public function printContactMatches($phone, $break=true)
    {
        if(strlen($phone) == 11) {
            $phone = substr($phone, 1);
        }
        $contactIds = Yii::app()->db->createCommand()
            ->selectDistinct('contact_id')
            ->from('phones')
            ->where('phone=:phone AND contact_id >0', array(':phone'=>$phone))
            ->queryColumn();

        $contacts = Contacts::model()->findAllByPk($contactIds);
        foreach($contacts as $i => $contact) {

            echo ($i > 0) ? "<hr>" : "";
            echo '<a href="/'.Yii::app()->controller->module->name.'/contacts/'.$contact->id.'" style="font-size: 15px;">'.$contact->fullName.'</a><br>';
            echo $contact->existingComponentRecordButton(ComponentTypes::CONTACTS, $break);
        }
    }

    public function actionSellers($id)
    {
        $this->pageColor = 'blue';
        $this->baseModel = new SmsMessages();

        $transactionsModel = Transactions::newInstance(Transactions::SELLERS);
        $transaction = $transactionsModel->findByPk($id);

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'month_to_date';
        $dateRange = $DateRanger->getDateRange($_POST['DateRanger']);

        $smsActivity = array();
        if(isset($_POST['to_phone_id'])) {
            $smsMessages = new SmsMessages();
            $Criteria = new CDbCriteria;
            $Criteria->condition = 'to_phone_id = :toPhoneId';
            $Criteria->params = array(
                ':toPhoneId'    => $_POST['to_phone_id'],
            );

            $foundSmsActivity = $smsMessages->findAll($Criteria);

            if(!empty($foundSmsActivity)) {
                foreach($foundSmsActivity as $activity) {
                    $smsActivity[] = $activity->getAttributes();
                }
            }
        }

        $DataProvider = new CArrayDataProvider($smsActivity, array('pagination' => false));

        $this->render("sms", array(
                'model' => $this->baseModel,
                'contact' => $transaction->contact,
                'DataProvider' => $DataProvider,
                'dateRange' => $dateRange
            ));
    }

    public function actionBuyers($id)
    {
        $this->pageColor = 'blue';
        $this->baseModel = new SmsMessages();

        $Contact = Contacts::model()->findByPk(Yii::app()->user->id);

        $transactionsModel = Transactions::newInstance(Transactions::BUYERS);
        $transaction = $transactionsModel->findByPk($id);

        $DateRanger = new DateRanger();
        $DateRanger->defaultSelect = 'month_to_date';
        $dateRange = $DateRanger->getDateRange($_GET['DateRanger']);

        $smsActivity = array();
        if(isset($_GET['to_phone_id'])) {

            $StmSmsNumberCriteria = new CDbCriteria;
            $StmSmsNumberCriteria->condition = 'contact_id = :contactId AND status = :status';
            $StmSmsNumberCriteria->params = array(
                ':contactId'    => $Contact->id,
                ':status'		=> 1
            );

            $checkForSmsNumberHQ = TelephonyPhones::model()->find($StmSmsNumberCriteria);

            $Criteria = new CDbCriteria;
            $Criteria->condition = 'to_phone_id = :toPhoneId OR inbound_to_sms_number_id = :inboundToSmsNumberId';
            $Criteria->params = array(
                ':toPhoneId'    => $_GET['to_phone_id'],
                ':inboundToSmsNumberId' => $checkForSmsNumberHQ->id
            );

            $foundSmsActivity = $this->baseModel->dateRange($dateRange, 'processed_datetime')->findAll($Criteria);

            if(!empty($foundSmsActivity)) {
                foreach($foundSmsActivity as $activity) {
                    $smsActivity[] = $activity->getAttributes();
                }
            }
        }

        $DataProvider = new CArrayDataProvider($smsActivity, array('pagination' => false));

        $this->render("sms", array(
                'model' => $this->baseModel,
                'contact' => $transaction->contact,
                'DataProvider' => $DataProvider,
                'dateRange' => $dateRange
            ));
    }

	public function actionChat($id)
    {

        $this->title = 'SMS Conversation';
		$smsActivity = array();
		$smsMessages = new SmsMessages();

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id' => Yii::app()->user->clientId, 'account_id' => Yii::app()->user->accountId));

		$toCriteria = new CDbCriteria;
		$toCriteria->condition = 'contact_id = :contact_id AND status = :status AND client_account_id = :clientAccountId';
		$toCriteria->params = array(
            ':clientAccountId' => $clientAccount->id,
			':contact_id'    => Yii::app()->user->id,
			':status'		=> 1
		);

		$checkForSmsNumberHQ = TelephonyPhones::model()->find($toCriteria);

		$Criteria = new CDbCriteria;

		if($checkForSmsNumberHQ) {
			$Criteria->condition = '((to_phone_number = :myStmNumber AND from_phone_number = :contactNumber) OR (to_phone_number = :contactNumber AND from_phone_number = :myStmNumber))';
			$Criteria->params = array(
				':myStmNumber' => '1'.$checkForSmsNumberHQ->phone,
				':contactNumber' => $id,
			);
			$sortOrder = "DESC";
			if(isset($_GET["SmsMessageSearch"])){
				if(isset($_GET["SmsMessageSearch"]["q"]) && !empty($_GET["SmsMessageSearch"]["q"])){
					$Criteria->condition .= " AND content LIKE :q ";
					$q = str_ireplace(" ","%",trim($_GET["SmsMessageSearch"]["q"]));
					$Criteria->params[":q"] = "%".str_ireplace("%%","%",$q)."%";
				}
				if(isset($_GET["SmsMessageSearch"]["sort"])){
					if($_GET["SmsMessageSearch"]["sort"] === "new"){
						$sortOrder = "DESC";
					}
				}
			}

			$Criteria->order="processed_datetime ".$sortOrder;
			$smsActivity = $smsMessages->findAll($Criteria);
		}

		$DataProvider = new CArrayDataProvider($smsActivity, array('pagination' => false));

		$this->render("chat", array(
			'model' => $this->baseModel,
			'DataProvider' => $DataProvider,
            'id' => $id,
		));
	}

    public function actionReply($id)
    {
        $this->title = 'SMS Conversation';
        $smsActivity = array();
        $smsMessages = new SmsMessages();

        $currentSms = $smsMessages->findByPk($id);
        $leadPhoneNumber = $currentSms->from_phone_number;

        $clientAccountId = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=>Yii::app()->user->accountId))->id;

        $toCriteria = new CDbCriteria;
        $toCriteria->condition = 'contact_id = :contact_id AND status = :status AND client_account_id=:clientAccountId';
        $toCriteria->params = array(
            ':clientAccountId' => $clientAccountId,
            ':contact_id'    => Yii::app()->user->id,
            ':status'		=> 1
        );
        $checkForSmsNumberHQ = TelephonyPhones::model()->find($toCriteria);

        $Criteria = new CDbCriteria;

        if($checkForSmsNumberHQ) {
            $Criteria->condition = '(to_phone_number = :myStmNumber AND from_phone_number = :contactNumber) OR (to_phone_number = :contactNumber AND from_phone_number = :myStmNumber)';
            $Criteria->params = array(
                ':myStmNumber' => '1'.$checkForSmsNumberHQ->phone,
                ':contactNumber' => $leadPhoneNumber,
            );

            $smsActivity = $smsMessages->findAll($Criteria);
        }

        $DataProvider = new CArrayDataProvider($smsActivity, array('pagination' => false));

        $this->render("chat", array(
                'model' => $this->baseModel,
                'DataProvider' => $DataProvider,
                'id' => $leadPhoneNumber,
            ));
    }

    public function actionSend($id)
    {
        if(empty($_POST['smsMessage'])) {
            echo CJSON::encode(array('status' => 'error', 'message' => 'Message is required.'));
            Yii::app()->end();
        }
        $smsMessage = new SmsMessages;

        $Contact = Contacts::model()->findByPk(Yii::app()->user->id);

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id' => Yii::app()->user->clientId, 'account_id' => Yii::app()->user->accountId));

        $Criteria = new CDbCriteria;
        $Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id = :clientAccountId';
        $Criteria->params = array(
            ':clientAccountId' => $clientAccount->id,
            ':contactId'    => $Contact->id,
            ':active'		=> 1
        );

        $checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);

        if(empty($checkForSmsNumberHQ)) {
            echo CJSON::encode(array('status' => 'error', 'message' => 'You must have a STM Provided SMS #.'));
            Yii::app()->end();
        }

        $smsMessage->attributes = $_POST['SmsMessages'];
        $smsMessage->sent_by_contact_id = $Contact->id;
        $smsMessage->direction = SmsMessages::DIRECTION_OUTBOUND;
        $smsMessage->processed_datetime = new \CDbExpression('NOW()');
        $smsMessage->from_phone_number = '1'.$checkForSmsNumberHQ->phone;
        $smsMessage->to_phone_number = $id;
        $smsMessage->content = $_POST['smsMessage'];

        if(YII_DEBUG) {
            // use an STM # to send to
			$id = '8133300522';
        }

        $response = Yii::app()->plivo->sendMessage(substr($id, 1), $smsMessage->content, "1".$checkForSmsNumberHQ->phone);

        if(isset($response['response']['error'])) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error sending SMS.'.PHP_EOL.'response: : '.print_r($response, true), CLogger::LEVEL_ERROR);
            echo CJSON::encode(array('error' => 'There was an error sending the message. Please try again.'));
            Yii::app()->end();
        }

        $smsMessage->vendor_response = json_encode($response);
        $smsMessage->message_uuid = $response['response']['message_uuid'][0];

        if (!$smsMessage->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving SMS Message.'.PHP_EOL.'smsMessage->attributes: '.print_r($smsMessage->attributes, true).' POST: '.print_r($_POST,1), CLogger::LEVEL_ERROR);
            echo CJSON::encode(array('status' => 'error', 'message' => 'There was an error saving the sms message. Please try again.'));
            Yii::app()->end();
        }

        echo CJSON::encode(array('status' => 'success'));
        Yii::app()->end();
    }

    public function printChatBubble($data, $leadPhone)
    {
        $class = ($data['from_phone_number'] == $leadPhone) ? "fromLead" : "";
        ?>
            <div class="chat-bubble <?=$class?>">
                <div class="chat-datetime"><?=Yii::app()->format->formatDateTime($data['processed_datetime'])?></div>
                <div class="chat-message"><?=$data['content']?></div>
            </div>
        <?
    }
}
