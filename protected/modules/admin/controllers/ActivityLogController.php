<?php
class ActivityLogController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return CMap::mergeArray(parent::filters(), array(
                'stmAccessControl' => array(
                    'class' => 'stm_app.modules.admin.components.filters.StmAccessControl',
                    'guestAllowedActions' => array(
                        'viewEmail',
                    ),
                ), // perform access control
            ));
    }

    /**
     * Specifies the action rules.
     *
     * @return array actions
     */
    public function actions()
    {
        return array(
			'add'      =>'stm_app.modules.admin.controllers.actions.activitylog.AddAction',
			'view'     =>'stm_app.modules.admin.controllers.actions.activitylog.ViewAction',
			'edit'     =>'stm_app.modules.admin.controllers.actions.activitylog.EditAction',
			'validate' =>'stm_app.modules.admin.controllers.actions.activitylog.ValidateAction',
			'email'    =>'stm_app.modules.admin.controllers.actions.activitylog.EmailAction',
			'sms'	   =>'stm_app.modules.admin.controllers.actions.activitylog.SmsAction',
            'viewEmail'=>'stm_app.modules.admin.controllers.actions.activitylog.ViewEmailAction',
        );
    }

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new ActivityLog;
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ActivityLog');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Task('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['ActivityLog']))
			$model->attributes=$_GET['ActivityLog'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

}
