<?php
Yii::import('admin_module.components.StmOAuth2.MailChimp', true);
Yii::import('admin_module.components.StmOAuth2.Google', true);
/**
 * oAuth 2 Controller
 *
 * Workflow for oAuth 2
 * Example Url: http://www.christineleeteam.local/admin/oauth2/redirect?type=mailchimp
 */
class Oauth2Controller extends AdminController
{

    /**
     * Oauth2 Adapter Class
     *
     * @var \StmOAuth2\Base
     */
    protected $_auth;

    /**
     * Init
     */
    public function init()
    {
        switch(\Yii::app()->request->getParam('id')) {
            case 'mailchimp':
                $this->_auth = new \StmOAuth2\MailChimp();
                break;

            case 'google':
                $this->_auth = new \StmOAuth2\Google();
                break;

            case 'facebook':
                $this->_auth = new \StmOAuth2\Facebook();
                break;

            case 'dropbox':
                $this->_auth = new \StmOAuth2\Dropbox();
                break;

            case 'constantcontact':
                $this->_auth = new \StmOAuth2\ConstantContact();
                break;

            default:
                throw new \Exception('Unknown type');
                break;
        }
    }

    /**
     * Authorize Action
     *
     * oAuth first step of getting auth code starts here
     * @return void
     */
    public function actionAuthorize($id)
    {
        $this->_auth->setContactId(\Yii::app()->user->id);

        $dbName = substr(\Yii::app()->db->connectionString, strpos(\Yii::app()->db->connectionString,'dbname=')+7);
        if(!($client = \Clients::model()->findByAttributes(array('db_name'=>$dbName)))) {
            throw new \Exception('Client Not Found.');
        }

        if(!($clientAccount = \ClientAccounts::model()->findByAttributes(array('client_id'=>$client->id, 'account_id'=> \Yii::app()->user->accountId)))) {
            throw new \Exception('Client Account Not Found.');
        }
        $this->_auth->setClientAccount($clientAccount);

        header('Location: ' . $this->_auth->getAuthorizeUrl());
    }

    /**
     * Authorize Success
     *
     * oAuth first step of getting auth code starts here
     * @return void
     */
    public function actionSuccess($id)
    {
        Yii::app()->user->setFlash('success', 'Successfully Integrated with '.$this->_auth->getName().'!');
        $this->render('success', array('oAuth2Client' => $this->_auth));
    }
}