<?php
	class ContactsController extends AdminController {

		public $tmpLocation = "/tmp";
		public $pageColor = 'red';
		public $CachedTransactionType;

		public function actions() {
			return array(
				'add'                   => 'stm_app.modules.admin.controllers.actions.contacts.AddAction',
				'view'                  => 'stm_app.modules.admin.controllers.actions.contacts.ViewAction',
				'delete'                => 'stm_app.modules.admin.controllers.actions.contacts.DeleteAction',
				'documents'             => 'stm_app.modules.admin.controllers.actions.contacts.DocumentsAction',
				'edit'                  => 'stm_app.modules.admin.controllers.actions.contacts.EditAction',
				'index'                 => 'stm_app.modules.admin.controllers.actions.contacts.ListAction',
                'opportunities'         => 'stm_app.modules.admin.controllers.actions.contacts.OpportunitiesAction',
				'emailCheck'            => 'stm_app.modules.admin.controllers.actions.contacts.EmailCheckAction',
				'sms'                   => 'stm_app.modules.admin.controllers.actions.contacts.SmsAction',
				'homesSaved'            => 'stm_app.modules.admin.controllers.actions.contacts.HomesSavedAction',
				'homesViewed'           => 'stm_app.modules.admin.controllers.actions.contacts.HomesViewedAction',
				'transactions'          => 'stm_app.modules.admin.controllers.actions.contacts.TransactionsAction',
				'viewedPages'           => 'stm_app.modules.admin.controllers.actions.contacts.ViewedPagesAction',
				'autocomplete'          => 'stm_app.modules.admin.controllers.actions.contacts.AutocompleteAction',
                'emailAutocomplete'     => 'stm_app.modules.admin.controllers.actions.contacts.EmailAutocompleteAction',
                'addRelationship'       => 'stm_app.modules.admin.controllers.actions.contacts.AddRelationshipAction',
                'editRelationship'      => 'stm_app.modules.admin.controllers.actions.contacts.EditRelationshipAction',
                'viewRelationship'      => 'stm_app.modules.admin.controllers.actions.contacts.ViewRelationshipAction',
                'phoneStatus'           => 'stm_app.modules.admin.controllers.actions.contacts.PhoneStatusAction',
                'emails'                => 'stm_app.modules.admin.controllers.actions.contacts.EmailListAction',
                'getEmails'             => 'stm_app.modules.admin.controllers.actions.contacts.GetEmailsAction',
                'sendPassword'          => 'stm_app.modules.admin.controllers.actions.contacts.SendPasswordAction',
                'specialDates'          => 'stm_app.modules.admin.controllers.actions.contacts.SpecialDatesAction',
                'keepInTouch'           => 'stm_app.modules.admin.controllers.actions.contacts.KeepInTouchAction',
                'import'                => 'stm_app.modules.admin.controllers.actions.contacts.ImportAction',
                'initialImport'         => 'stm_app.modules.admin.controllers.actions.contacts.InitialImportAction',
                'leads'                 => 'stm_app.modules.admin.controllers.actions.contacts.LeadsAction',
                'emailUpdate'           => 'stm_app.modules.admin.controllers.actions.contacts.EmailUpdateAction',
                'addToDialerList'       => 'stm_app.modules.admin.controllers.actions.contacts.AddToDialerListAction',
                'getCalendarEvents'     => 'stm_app.modules.admin.controllers.actions.contacts.GetCalendarEventsAction',
                'transfer'              => 'stm_app.modules.admin.controllers.actions.contacts.TransferAction',
                'voicemailBroadcasts'   =>  'stm_app.modules.admin.controllers.actions.contacts.VoicemailBroadcastsAction',
			);
		}

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
			$this->baseModel = new Contacts;
		}

        /**
         * Referral Touch
         *
         * Generates calls that need to be made for referral touches based on the source setting for touch frequency. From special dates to X days per source.
         */
        public function actionReferralTouch()
        {
            $this->title = 'Referral Calls';
            $this->render('listReferralTouch');
        }
/*
lee@yahoo.com
test@yahoo.com
shawntepayne@hotmail.com
*/
        public function actionMassUpdateByEmail()
		{
            if(!Yii::app()->user->checkAccess('owner')) {
                $this->render('/main/accessDenied', array());
                Yii::app()->end();
            }

			$this->title = 'Mass Update by Email';
			$this->pageColor = 'red';

            if(Yii::app()->user->checkAccess('transactions')) {
                $TransactionStatus = CHtml::listData(TransactionStatus::model()->byNurtureType()->defaultSort()->findAll(), 'id', 'name');
            }
            else {
                $TransactionStatus = CHtml::listData(TransactionStatusAccountLu::model()->byNurtureType()->byComponentTypeIds(ComponentTypes::RECRUITS)->findAll(array('order'=>'transactionStatus.sort_order', 'with'=>array('transactionStatus'))), 'transaction_status_id', 'displayName');
            }
			$log = array();

			if (isset($_POST['email_list'])) {

				// Check the first dropdown options
				if(empty($_POST['email_list'])) {
					Yii::app()->user->setFlash('error', 'You must enter at least 1 email.');
					$this->render('reconcileContacts', array(
						'transactionStatus' => $TransactionStatus,
						'model' => $this->baseModel
					));
					return;
				}

				if(!isset($_POST['action']) || $_POST['action'] == "-1") {
					Yii::app()->user->setFlash('error', 'You must select an Action');
					$this->render('reconcileContacts', array(
						'transactionStatus' => $TransactionStatus,
						'model' => $this->baseModel
					));
					return;
				}

				if(empty($_POST['componentType'])) {
					Yii::app()->user->setFlash('error', 'You must select a Lead Type');
					$this->render('reconcileContacts', array(
						'transactionStatus' => $TransactionStatus,
						'model' => $this->baseModel
					));
					return;
				}
				// END Check the first dropdown options

				// If you select Assign User Action
				if($_POST['action'] == 0 && empty($_POST['assign_to_user'])) {
					Yii::app()->user->setFlash('error', 'You must select a User to assign to');
					$this->render('reconcileContacts', array(
						'transactionStatus' => $TransactionStatus,
						'model' => $this->baseModel
					));
					return;
				}

				if($_POST['action'] == 0 && $_POST['componentType'] == ComponentTypes::BUYERS && empty($_POST['buyer_assignment_type_id'])) {
					Yii::app()->user->setFlash('error', 'You must select an assignment type');
					$this->render('reconcileContacts', array(
						'transactionStatus' => $TransactionStatus,
						'model' => $this->baseModel
					));
					return;
				}

				if($_POST['action'] == 0 && $_POST['componentType'] == ComponentTypes::SELLERS && empty($_POST['seller_assignment_type_id'])) {
					Yii::app()->user->setFlash('error', 'You must select an assignment type');
					$this->render('reconcileContacts', array(
						'transactionStatus' => $TransactionStatus,
						'model' => $this->baseModel
					));
					return;
				}

				if($_POST['action'] == 0 && !empty($_POST['assign_to_user'])) {
					$assignToUserContact = Contacts::model()->findByPk($_POST['assign_to_user']);
					if(empty($assignToUserContact)) {
						Yii::app()->user->setFlash('error', 'Could not find contact for selected Assign To User. Please try again.');
						$this->render('reconcileContacts', array(
							'transactionStatus' => $TransactionStatus,
							'model' => $this->baseModel
						));
						return;
					}
				}
				// END If you select Assign User Action

				// If you select Update Lead Status
				if($_POST['action'] == 1 && empty($_POST['assign_to_lead_status'])) {
					Yii::app()->user->setFlash('error', 'You must select a Leasd Status to assign to');
					$this->render('reconcileContacts', array(
						'transactionStatus' => $TransactionStatus,
						'model' => $this->baseModel
					));
				}

				// Get all the emails entered into the textarea
				$emails = explode("\n", $_POST['email_list']);

				foreach($emails as $email) {
					$email = trim($email);

                    if(empty($email)) {
                        continue;
                    }

					$Contact = Contacts::model()->findByEmail($email);
					if(empty($Contact)) {
						$log[$email] = "Skipped, no contact found.";
						continue;
					}
					if($_POST['action'] === "2" || $_POST['action'] === "3") {
						$this->_editTags($Contact,$_POST["componentType"], $_POST['action'], $_POST["MassAction"]["options"]);
					}

					if($_POST['componentType'] == ComponentTypes::BUYERS) {
						$transactions = Transactions::model()->findAllByAttributes(array('contact_id' => $Contact->id, 'component_type_id' => ComponentTypes::BUYERS));
					}

					if($_POST['componentType'] == ComponentTypes::SELLERS) {
						$transactions = Transactions::model()->findAllByAttributes(array('contact_id' => $Contact->id, 'component_type_id' => ComponentTypes::SELLERS));
					}

                    if($_POST['componentType'] == ComponentTypes::RECRUITS) {
                        $transactions = Recruits::model()->findAllByAttributes(array('contact_id' => $Contact->id));
                    }

                    if(empty($transactions)) {
						Yii::app()->user->setFlash('error', 'Contact with email: '.$email.' has 0 records.');
						$log[$email] = "Skipped, 0 records found.";
						continue;
					}

					if(count($transactions) > 1) {
						Yii::app()->user->setFlash('error', 'Contact with email: '.$email.' has more than 1 records. Please review and update manually.');
						$log[$email] = "Skipped, more than 1 records found.";
						continue;
					}

					if($_POST['action'] == 0) {

						if($_POST['componentType'] == ComponentTypes::BUYERS) {
							$component_type_id = ComponentTypes::BUYERS;
							$assignment_type_id = $_POST['buyer_assignment_type_id'];
						}

						if($_POST['componentType'] == ComponentTypes::SELLERS) {
							$component_type_id = ComponentTypes::SELLERS;
							$assignment_type_id = $_POST['seller_assignment_type_id'];
						}

                        if($_POST['componentType'] == ComponentTypes::RECRUITS) {
                            $component_type_id = ComponentTypes::RECRUITS;
                            $assignment_type_id = $_POST['recruit_assignment_type_id'];
                        }

                        $Assignments = Assignments::model()->findAllByAttributes(array('assignee_contact_id' => intval($_POST['assign_to_user']), 'component_type_id' => $component_type_id, 'assignment_type_id' => intval($assignment_type_id), 'component_id' => $transactions[0]->id));

						if(empty($Assignments)) {
							$Assignments = new Assignments();
							$Assignments->component_id = $transactions[0]->id;
							$Assignments->component_type_id = $component_type_id;
							$Assignments->assignment_type_id = $assignment_type_id;
							$Assignments->assignee_contact_id = $_POST['assign_to_user'];
							$Assignments->is_protected = 0;
                            $Assignments->alertChange = false;

							if(!$Assignments->save()) {
                                $log[$email] = "Error updating Assignment: ".print_r($Assignments->getErrors(), 1);
                            }
							$log[$email] = "Successfully added new Assignment.";
						} else {
							$log[$email] = "Skipped, assignment already exists.";
							continue;
						}
					}

					if($_POST['action'] == 1) {

						if(!isset($TransactionStatus[$_POST['assign_to_lead_status']])) {
							Yii::app()->user->setFlash('error', 'Contact with email: '.$email.' has more than 1 transaction. Please update manually.');
							$log[$email] = "Skipped, could not find proper lead status";
							continue;
						}

                        // @todo: or recruits

                        if($_POST['componentType'] == ComponentTypes::BUYERS || $_POST['componentType'] == ComponentTypes::SELLERS) {
                            $transactions[0]->transaction_status_id = $_POST['assign_to_lead_status'];
                        }
                        elseif($_POST['componentType'] == ComponentTypes::RECRUITS) {
                            $transactions[0]->status_id = $_POST['assign_to_lead_status'];
                        }


						if(!$transactions[0]->save()) {
                            $log[$email] = "Error updating status: ".print_r($transactions[0]->getErrors(), 1);
                            continue;
                        }

						$log[$email] = "Successfully updated lead status to: ".$TransactionStatus[$_POST['assign_to_lead_status']];
					}
				}

				$fileName = "mass-update-log-".time().'.csv';

				$filePath = $this->tmpLocation.DIRECTORY_SEPARATOR.$fileName;

				$fp = fopen($filePath, 'w');

				if($fp) {
					// first line headers
					$wrote = @fputcsv($fp, array("Email", "Message"));

					foreach ($log as $email => $message) {
						$csvRow = array();
						$csvRow['Email'] = $email;
						$csvRow['Message'] = $message;
						@fputcsv($fp, $csvRow);
					}

					@fclose($fp);

					$zendMessage = new StmZendMail();
					$zendMessage->setSubject('Mass Update by Email Log - ' . Yii::app()->user->domain->name . ' @ '.date('Y-m-d H:i:s'));
					$zendMessage->setBodyHtml('Please see the attached file from your mass update.');
					$zendMessage->setFrom('do-not-reply@seizethemarket.net', 'STM Notification');
					$zendMessage->addBcc('debug@seizethemarket.com');

					if (YII_DEBUG) {
						$zendMessage->addTo(StmMailMessage::DEBUG_EMAIL_ADDRESS, 'STM Debug'); //"brandon@w3evolutions.com"
					} else {
						$zendMessage->addTo(Yii::app()->user->contact->primaryEmail);
					}

					$zendMessage->setType(Zend_Mime::MULTIPART_RELATED);

					$at = $zendMessage->createAttachment(file_get_contents($filePath));
					$at->type = 'text/csv';
					$at->disposition = Zend_Mime::DISPOSITION_INLINE;
					$at->encoding = Zend_Mime::ENCODING_BASE64;
					$at->filename = $fileName;

					// Attempt to send message
					try {
						$zendMessage->send(StmZendMail::getAwsSesTransport());
					} catch (Exception $e) {
						Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Mass Update Error! (Zend Mail). Code: "' . $e->getCode() . '" ' . PHP_EOL . 'Exception Message:' . $e->getMessage(), CLogger::LEVEL_ERROR);
					}
				} else {
					Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Mass Update Error! (Zend Mail). Could not write tmp file for email.', CLogger::LEVEL_ERROR);
				}
			}

			$tagOptions = CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name');
			$tagOptions = CHtml::listData(TransactionTags::model()->findAll(), 'id', 'name');
			//die("<pre>".print_r($tagOptions,1));
			$this->render('massUpdateByEmail', array(
				'log' => $log,
				'model' => $this->baseModel,
				'transactionStatus' => $TransactionStatus,
				"tagOptions" => $tagOptions
			));
		}

		public function actionMoveContact() {
			if (Yii::app()->request->isAjaxRequest) {

				if(!isset($_POST['contactId']) || empty($_POST['contactId'])) {
					echo CJSON::encode(array(
						'success' => false,
						'message' => 'Missing Contact ID (clientId).'
					));
					Yii::app()->end();
				}

				$contactToMove = Contacts::model()->findByPk($_POST['contactId']);

				if(empty($contactToMove)) {
					echo CJSON::encode(array(
						'success' => flase,
						'message' => 'Could not find Contact from ID passed.'
					));
					Yii::app()->end();
				}

				if(!isset($_POST['moveToAccountId']) || empty($_POST['moveToAccountId'])) {
					echo CJSON::encode(array(
						'success' => flase,
						'message' => 'Missing Move To Account ID (moveToAccountId).'
					));
					Yii::app()->end();
				}

				if($contactToMove->account_id == $_POST['moveToAccountId']) {
					echo CJSON::encode(array(
						'success' => flase,
						'message' => 'The selected Contact is already in the account to move to.'
					));
					Yii::app()->end();
				}

				$moveToAccountId = (int)$_POST['moveToAccountId'];

				$q = "UPDATE transactions t
						JOIN contacts c ON c.id=t.contact_id
						LEFT JOIN emails e ON e.contact_id=c.id
						LEFT JOIN phones p ON p.contact_id=c.id
						LEFT JOIN address_contact_lu alu ON alu.contact_id=c.id
						LEFT JOIN addresses d ON d.id=alu.address_id
						LEFT JOIN activity_log l ON l.component_type_id=t.component_type_id AND l.component_id=t.id
						LEFT JOIN tasks x ON x.component_type_id=t.component_type_id AND x.component_id=t.id
					SET c.account_id={$moveToAccountId}, d.account_id={$moveToAccountId}, e.account_id={$moveToAccountId}, p.account_id={$moveToAccountId}, l.account_id={$moveToAccountId}, x.account_id={$moveToAccountId}, t.account_id={$moveToAccountId}
					WHERE c.id = ".$contactToMove->id;

				$result = Yii::app()->db->createCommand($q)->execute();

				echo CJSON::encode(array(
					'success' => true,
				));
				Yii::app()->end();
			}

			echo CJSON::encode(array(
				'success' => false,
				'message' => 'Invalid request.'
			));
			Yii::app()->end();
		}

		public function actionGetTags(){
			$tagOptions = array();
			if(!empty($_POST["componentId"])){
				if($_POST["componentId"] == ComponentTypes::BUYERS || $_POST["componentId"] == ComponentTypes::SELLERS){
					$tagOptions = CHtml::listData(TransactionTags::model()->findAll(), 'id', 'name');
				}elseif($_POST["componentId"] == ComponentTypes::RECRUITS) {
					$tagOptions = CHtml::listData(RecruitTypes::model()->findAll(), 'id', 'name');
				}elseif($_POST["componentId"] == ComponentTypes::CONTACTS){
					//not sure we need this one
					$tagOptions = CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name');
				}
			}
			if(!empty($tagOptions)){$status = "success";}
			else{$status = "empty";}
			echo CJSON::encode(array('status'=>$status,'tags'=>$tagOptions));
			Yii::app()->end();
		}

		/**
		 * Name _editTags
		 * @param object $Contact (Contacts data model)
		 * @param int $componentId //contact type identifer
		 * @param int $action (2 = add tags)
		 * @param array $tagIds
		 * @return array
		 *
		 * @author RapidMod.com
		 * @author 813.330.0522
		 *
		 * @todo buyers / sellers saving is not fully implemeneted, we must ensure and test we are getting the proper model
		 * @todo implement remove tags
		 *
		 */
		private function _editTags($Contact, $componentId, $action, $tagIds) {

			if($componentId == ComponentTypes::BUYERS || $componentId == ComponentTypes::SELLERS) {
				foreach ($tagIds as $tagId) {

					if(empty($tagId)) { continue; }

					$dataArray = array(
						"transaction_id" => $Contact->id,
						"transaction_tag_id" => $tagId
					);
					$massActionModel = new TransactionTagLu();
					$massActionModel = $massActionModel->findByAttributes($dataArray);

					//only edit the record if we need to
					if(empty($massActionModel->transaction_id) && $action === "2" && !empty($dataArray)){
						$massActionModel = new TransactionTagLu();
						$massActionModel->attributes = $dataArray;
						$massActionModel->save(1);
					}elseif (!empty($massActionModel->transaction_id) && " " === "removeTag"){
						$massActionModel->deleteAllByAttributes($dataArray);
					}
				}

			}elseif($componentId == ComponentTypes::RECRUITS) {
				$Recruits = Recruits::model()->findAllByAttributes(array("contact_id"=>$Contact->id));
				if(empty($Recruits)){return;}
				foreach ($Recruits as $recruit){
					foreach ($tagIds as $tagId){

						if(empty($tagId)){continue;}

						$dataArray = array("recruit_id" => $recruit->id, "recruit_type_id" => $tagId);
						$massActionModel = new RecruitTypeLu();
						$massActionModel = $massActionModel->findByAttributes($dataArray);

						if(!$massActionModel && $action == "2" && !empty($dataArray)){
							$massActionModel = new RecruitTypeLu();
							$massActionModel->attributes = $dataArray;
							$massActionModel->save(1);
						}elseif ($action === "3" && !empty($massActionModel->recruit_id)){
							$massActionModel->delete();
						}
					}
				}
				return;
			}elseif($componentId == ComponentTypes::CONTACTS){
				foreach ($tagIds as $tagId){
					if(empty($tagId)) { continue; }

					$dataArray = array( "contact_id" => $Contact->id, "contact_type_id" => $tagId);
					$massActionModel = new ContactTypeLu();
					$massActionModel = $massActionModel->findByAttributes($dataArray);

					//only edit the record if we need to
					if(empty($massActionModel->contact_id) && $action === "2"  && !empty($dataArray)){
						$massActionModel = new ContactTypeLu();
						$massActionModel->attributes = $dataArray;
						$massActionModel->save(1);
					}elseif (!empty($massActionModel->contact_id)){
						$massActionModel->delete();
					}
				}
			}

		}

        public function actionGetPhones($id)
        {
            $phones = array();
            $results = Yii::app()->db->createCommand()
                ->select('id, phone')
                ->from('phones')
                ->where('contact_id='.intval($id))
                ->andWhere('is_deleted=0')
                ->queryAll();

            foreach($results as $row) {
                $phones[$row['id']] = Yii::app()->format->formatPhone($row['phone']);
            }
            echo CJSON::encode(array('status'=>'success','phones'=>$phones));
            Yii::app()->end();
        }

		/**
		 * HTML 5 Audio
		 *
		 * Helper function to avoid the disaster of eval()'d code in the view
		 * @param $data Data for the current row
		 * @param $row I have no idea
		 * @return string Compiled HTML string, as a side note HTML should NEVER appear in a controller, but here it is...
		 */
		public function html5Audio($data, $row)
		{
			return "
				<audio controls preload='none' style='display: block; width: 100%;'>
					<source src='http://sites.seizethemarket.com/site-files/" . Yii::app()->user->clientId . "/broadcast-messages/" . $data->id . ".wav' type='audio/wav'>
				</audio>
			";
		}
	}