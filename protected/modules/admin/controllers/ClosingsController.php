<?php
class ClosingsController extends AdminController
{

    public $pageColor = 'orange';

    public $CachedComponentType;

    public function actions()
    {

        return array(
            'index'           => 'stm_app.modules.admin.controllers.actions.closings.ListAction',
            'view'            => 'stm_app.modules.admin.controllers.actions.closings.ViewAction',
            'delete'          => 'stm_app.modules.admin.controllers.actions.closings.DeleteAction',
            'add'             => 'stm_app.modules.admin.controllers.actions.closings.AddAction',
            'edit'            => 'stm_app.modules.admin.controllers.actions.closings.EditAction',
            'pending'         => 'stm_app.modules.admin.controllers.actions.closings.PendingAction',
            'closed'          => 'stm_app.modules.admin.controllers.actions.closings.ClosedAction',
            'documents'       => 'stm_app.modules.admin.controllers.actions.closings.DocumentsAction',
            'companies'       => 'stm_app.modules.admin.controllers.actions.closings.CompaniesAction',
            'agents'          => 'stm_app.modules.admin.controllers.actions.closings.AgentsAction',
            'info'            => 'stm_app.modules.admin.controllers.actions.closings.InfoAction',
            'inspectors'      => 'stm_app.modules.admin.controllers.actions.closings.InspectorsAction',
            'companyContacts' => 'stm_app.modules.admin.controllers.actions.closings.CompanyContactsAction',
            'import'          => 'stm_app.modules.admin.controllers.actions.closings.ImportAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {

        return CMap::mergeArray(
            parent::filters(), array()
        );
    }

    /**
     * Return a instance of the transaction type model and validate the request
     *
     * @return Instance of the TransactionTypes model.
     */
    public function getComponentType()
    {

        if ($this->CachedComponentType) {
            return $this->CachedComponentType;
        }

        if (($ComponentType = ComponentTypes::model()->findByPk(ComponentTypes::CLOSINGS)) === null
        ) {
            throw new CHttpException(404, 'The ' . __CLASS__ . ' must specify a valid transaction type.');
        }

        $this->CachedComponentType = $ComponentType;

        return $this->CachedComponentType;
    }

    /**
     * Process an assignment group
     * this method will handle the removal of the title companies, co-brokers, lenders, and inspectors
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param AssignmentGroup $assignmentGroup
     *
     * @return AssignmentGroup
     */
    public function processAssignmentGroup(AssignmentGroup $assignmentGroup)
    {

        $assignmentGroupName = get_class($assignmentGroup);

        if (isset($_POST[$assignmentGroupName])) {
            /** @var AssignmentGroup $assignmentGroup */
            $assignmentGroup->attributes = $_POST[$assignmentGroupName];
        } else {
            $previousAssignmentTuples = $assignmentGroup->getPreviouslyAssignedAsTupleCollection();
            if (!empty($previousAssignmentTuples) and empty($_POST)) {
                if (!empty($previousAssignmentTuples)) {
                    foreach ($previousAssignmentTuples as $previousAssignmentTuple) {
                        array_push(
                            $assignmentGroup->assigneeGroup, AssignmentGroupHtmlHelper::getOptionIdByAssignmentGroup(
                                $assignmentGroup, $previousAssignmentTuple
                            )
                        );
                    }
                }
            }
        }

        $assignmentGroup->update();

        return $assignmentGroup;
    }

    /**
     * Processes a collection of assignments from the closing form
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param Closings $closing
     *
     * @return bool
     */
    public function processAssignments(Closings &$closing) {

        $assignmentsSaved = true;
        $assignmentsProcessed = array();

        if (isset($_POST['Assignments'])) {
            foreach ($_POST['Assignments'] as $assignmentData) {
                $assignment = new Assignments;
                $assignment->attributes = $assignmentData;
                $assignment->component_type_id = ComponentTypes::CLOSINGS;
                $assignment->component_id = $closing->id;
                array_push($assignmentsProcessed, $assignment);
                if (!$this->processAssignment($assignment)) {
                    $assignmentsSaved = !$assignmentsSaved;
                }
            }
        }

        $closing->assignments = $assignmentsProcessed;

        return $assignmentsSaved;
    }

    /**
     * Processes an assignment from the closing form
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param Assignments $assignmentFromPost
     *
     * @return bool
     */
    private function processAssignment(Assignments $assignmentFromPost) {

        $assignmentProcessed = true;
        if (empty($assignmentFromPost->assignment_type_id) or empty($assignmentFromPost->assignee_contact_id)) {
            return !$assignmentProcessed;
        }

        // need to pull the original record and make changes
        if (!empty($assignmentFromPost->id)) {

            // retrieve the previously saved assignment record
            $modifiedAssignment = Assignments::model()->findByPk($assignmentFromPost->id);

            // if the gui marked the assignment from removal, remove it
            if ($assignmentFromPost->remove) {
                $modifiedAssignment->delete(); // soft deletes don't return a boolean
                return $assignmentProcessed;
            }
            $modifiedAssignment->attributes = $assignmentFromPost->attributes;
            return $modifiedAssignment->save();
        }

        return $assignmentFromPost->save();
    }

    /**
     * Initializes the controller
     *
     * @return none
     */
    public function init()
    {

        $this->baseModel = new Closings;
    }
}
