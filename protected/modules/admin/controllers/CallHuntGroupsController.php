<?php
/**
 * Hunt Group Controller
 */
class CallHuntGroupsController extends AdminController
{
    public $title = 'Hunt Groups';

    /**
     * Initializes the controller
     * @return none
     */
    public function init() {
        $this->displayName = 'Call Hunt Groups';
        $this->baseModel = new CallHuntGroups();
    }

    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
            'index'         =>'stm_app.modules.admin.controllers.actions.callHuntGroup.ListAction',
            'list'          =>'stm_app.modules.admin.controllers.actions.callHuntGroup.ListAction',
            'add'           =>'stm_app.modules.admin.controllers.actions.callHuntGroup.AddAction',
            'edit'          =>'stm_app.modules.admin.controllers.actions.callHuntGroup.EditAction',
            'listPhone'    =>'stm_app.modules.admin.controllers.actions.callHuntGroup.ListPhoneAction',
            'addPhone'     =>'stm_app.modules.admin.controllers.actions.callHuntGroup.AddPhoneAction',
            'editPhone'    =>'stm_app.modules.admin.controllers.actions.callHuntGroup.EditPhoneAction',
        );
    }

    public function printHuntGroupPhones(CallHuntGroups $model)
    {
        $string = '';
        foreach($model->callHuntGroupPhones as $callHuntGroupPhone){

            if($callHuntGroupPhone->status_ma) {

                $string .= ($string) ? '<br>' : '';
                $string .= Yii::app()->format->formatPhone($callHuntGroupPhone->phone).' - '.$callHuntGroupPhone->description;
            }
        }
        return $string;
    }

    public function actionAddCallBlock()
    {
        $this->title = 'Add Call Blocks';
        $model = new CallBlocks();

        if (isset($_POST['CallBlocks'])) {

            $model->attributes = $_POST['CallBlocks'];
            $model->added = new CDbExpression('NOW()');
            $model->added_by = Yii::app()->user->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully added Call Block!');
                $this->redirect(array('listCallBlock'));
            }
        }

        $this->render('formCallBlock', array(
                'model'=>$model,
            ));
    }

    public function actionEditCallBlock($id)
    {
        $this->title = 'Edit Call Blocks';
        $model = CallBlocks::model()->findByPk($id);

        if (isset($_POST['CallBlocks'])) {

            $model->attributes = $_POST['CallBlocks'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated Call Blocks.');
                $this->redirect(array('listCallBlock'));
            }
        }

        $this->render('formCallBlock', array(
                'model' =>  $model,
            ));
    }

    public function actionListCallBlock()
    {
        $this->title = 'Call Blocks';
        $model = new CallBlocks();

        if (isset($_GET['CallBlocks'])) {

            $model->attributes = $_POST['CallBlocks'];
        }

        $this->render('listCallBlocks', array(
                'model'=>$model,
            ));
    }
}