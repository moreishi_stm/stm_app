<?php
class ClickToCallController extends AdminController
{

    const FROM_NUMBER = '19043301885';
    public $pageColor = 'blue';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    /**
     * Initializes the controller
     *
     * @return void
     */
    public function init()
    {
        $this->displayName = 'Click to Call';
//        $this->baseModel = new Domains;
    }

    public function actionGetPhoneNumbers()
    {
        echo CJSON::encode(array('componentTypeId' => $_POST['componentTypeId'], 'componentId' => $_POST['componentId']));
    }

    public function actionCall()
    {
        $clickToCallInitialPhone = Yii::app()->format->formatInteger($_POST['clickToCallInitialPhone']);
        $clickToCallToPhoneNumber = Yii::app()->format->formatInteger($_POST['clickToCallToPhoneNumber']);
        $clickToCallCallerIdNumber = Yii::app()->format->formatInteger($_POST['clickToCallCallerIdNumber']);
        $clickToCallSettingSkipConfirmation = (Yii::app()->user->settings->click_to_call_skip_confirmation) ? 1 : 0;
        $componentTypeId = $_POST['componentTypeId'];
        $componentId = $_POST['componentId'];
        $userId = $_POST['userId'];

        if(!$_POST['clickToCallInitialPhone']) {
            echo CJSON::encode(array('status'=>'error', 'message' => 'Missing Your Phone #.'));
            Yii::app()->end();
        }

        if(strlen($clickToCallInitialPhone) != 10) {
            echo CJSON::encode(array('status'=>'error', 'message' => 'Invalid Phone #.'));
            Yii::app()->end();
        }

        if(strlen($clickToCallToPhoneNumber) != 10) {
            echo CJSON::encode(array('status'=>'error', 'message' => 'Invalid TO Phone #.'));
            Yii::app()->end();
        }

        if(!$_POST['clickToCallCallerIdName']) {
            echo CJSON::encode(array('status'=>'error', 'message' => 'Missing Call ID Name.'));
            Yii::app()->end();
        }

        if(!$_POST['clickToCallCallerIdNumber']) {
            echo CJSON::encode(array('status'=>'error', 'message' => 'Missing Caller ID #.'));
            Yii::app()->end();
        }

        $clickToCallCallerIdNumber = ($clickToCallCallerIdNumber) ? '1'.$clickToCallCallerIdNumber : $clickToCallCallerIdNumber;

        // Make the call
        $plivoCallData = array(
            'to'            => '1'.$_POST['clickToCallInitialPhone'],
            'from'          => self::FROM_NUMBER ,
//            'ring_url'      =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/ring/id/' . $id,
            'fallback_url'  =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/fallback/id/',
            'answer_url'    =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/answer?toPhoneNumber=' . $clickToCallToPhoneNumber . '&callerIdNumber=' . $clickToCallCallerIdNumber . '&clickToCallSettingSkipConfirmation=' . $clickToCallSettingSkipConfirmation . '&componentTypeId=' . $componentTypeId . '&componentId=' . $componentId . '&userId=' . $userId,
            'hangup_url'    =>  StmFunctions::getSiteUrl() . '/plivoclicktocall/hangup'

        );

        $response = Yii::app()->plivo->getPlivo()->make_call($plivoCallData);

        $status='success';
        echo CJSON::encode(array('status'=>$status, 'message' => $message));
        Yii::app()->end();
    }
}