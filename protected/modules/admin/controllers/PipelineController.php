<?php

/**
 * Pipeline Controller
 *
 */
class PipelineController extends AdminController {

    protected function _sendJson($data) {
        // Set header and output JSON data
        //header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }

    protected function _checkPipeline($id) {
        $pipeline = $this->baseModel->findByPk($id);

        if(empty($pipeline)) {
            return false;
        }

        return $pipeline;
    }

    protected function _isAjax() {
        if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
            Yii::app()->user->setFlash('error', 'Permissions Denied.');
            $this->redirect(array('/'.Yii::app()->controller->module->id.'/pipeline'));
            Yii::app()->end();
        }

        return true;
    }

    public function init() {
        $this->baseModel = new Pipelines();

        return parent::init();
    }

    public function printActions($data) {
        return '<a href="/'.Yii::app()->controller->module->id.'/pipeline/edit/'.$data->id.'"><i class="fa fa-2x fa-pencil"></i></a> <a href="/'.Yii::app()->controller->module->id.'/pipeline/view/'.$data->id.'"><i class="fa fa-2x fa-eye"></i></a>';
    }

    public function actionGetAssignments($id) {
        $stage = PipelineStages::model()->findByPk($id);

        if(empty($stage)) {
            $this->_sendJson(array(
                'status' => 'error',
                'messages' => array(
                    'Could not load stage for ID: '.$id
                )
            ));
        }

        /*$criteria = new CDbCriteria();
        $criteria->with = array('contact');
        $criteria->compare('pipeline_stage_id',$id);
        $criteria->order = 'component_id ASC';
        $criteria->together = true;

        $dataProvider = new CActiveDataProvider('PipelineComponentLu', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));*/

        $results = Yii::app()->db->createCommand(
            "SELECT c.*, pcl.* FROM pipeline_component_lu pcl INNER JOIN contacts c ON c.id = pcl.component_id WHERE (c.is_deleted = 0 OR c.is_deleted IS NULL) AND (pcl.pipeline_stage_id = ".$id.")"
        )->queryAll();

        $this->_sendJson(array(
            'status' => 'success',
            //'items' => $dataProvider->getData(),
            'items' => $results,
            'pipeline_stage_id' => $id
        ));
    }

    public function actionIndex() {
        $this->render('list', array(
            'pipelines' => $this->baseModel
        ));
    }

    public function actionView($id) {
        $pipeline = $this->_checkPipeline($id);

        if(empty($pipeline)) {
            Yii::app()->user->setFlash('error', 'Pipeline not found.');
            $this->redirect(array('/'.Yii::app()->controller->module->id.'/pipeline'));
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('pipeline_id = '.$pipeline->id);
        $criteria->order = 'sort_order ASC';

        $stages = PipelineStages::model()->findAll($criteria);

        $this->render('view', array(
            'pipeline' => $pipeline,
            'stages' => $stages,
        ));
    }

    public function actionEdit($id) {
        $pipeline = $this->_checkPipeline($id);

        if(empty($pipeline)) {
            Yii::app()->user->setFlash('error', 'Pipeline not found.');
            $this->redirect(array('/'.Yii::app()->controller->module->id.'/pipeline'));
        }

        $this->render('edit', array(
            'model' => $pipeline
        ));
    }

    public function actionStages($id) {
        $pipeline = $this->_checkPipeline($id);

        if(empty($pipeline)) {
            Yii::app()->user->setFlash('error', 'Pipeline not found.');
            $this->redirect(array('/'.Yii::app()->controller->module->id.'/pipeline'));
        }

        $stages = PipelineStages::model()->findAllByAttributes(array('pipeline_id' => $pipeline->id));

        $assignments = new PipelineComponentLu();

        $this->render('stages', array(
            'pipeline' => $pipeline,
            'stages' => $stages,
            'assignments' => $assignments
        ));
    }

    public function actionAjaxAdd() {
        $this->_isAjax();

        $name = Yii::app()->request->getParam('name', NULL);

        if(empty($name)) {
            $this->_sendJson(array(
                'status' => 'error',
                'messages' => array(
                    'Missing parameter: name'
                )
            ));
        }

        $this->baseModel->unsetAttributes();

        $this->baseModel->attributes = $_POST['pipeline'];

        if($this->baseModel->save()) {
            $this->_sendJson(array(
                'status' => 'error',
                'messages' => $this->baseModel->getErrors()
            ));
        }

        $this->_sendJson(array(
            'status' => 'success',
        ));
    }

    public function actionAjaxStages($id) {
        $this->_isAjax();

        $pipeline = $this->_checkPipeline($id);

        if(empty($pipeline)) {
            $this->_sendJson(array(
                'status' => 'error',
                'messages' => array(
                    'Pipeline Not Found.'
                )
            ));
        }

    }

    public function actionAjaxAssign() {
        $this->_isAjax();

        $stage = PipelineStages::model()->findByPk($_POST['pipeline_stages']);

        if(empty($stage)) {
            $this->_sendJson(array(
                'Could not find Pipeline Stage. Please try again.'
            ));
        }

        $pipeline = Pipelines::model()->findByPk($stage->pipeline_id);

        if(empty($pipeline)) {
            $this->_sendJson(array(
                'Could not find Pipeline. Please try again.'
            ));
        }

        $pcl = PipelineComponentLu::model()->findByAttributes(array('pipeline_id' => $pipeline->id, 'component_id' => $_POST['componentId']));

        if(empty($pcl)) {
            $pcl = new PipelineComponentLu();
            $pcl->added = date("Y-m-d H:i:s", time());
            $pcl->added_by = Yii::app()->user->id;
        }

        $pcl->pipeline_id = $pipeline->id;
        $pcl->pipeline_stage_id = $_POST['pipeline_stages'];
        $pcl->component_id = $_POST['componentId'];
        $pcl->updated = date("Y-m-d H:i:s", time());
        $pcl->updated_by = Yii::app()->user->id;

        if(!$pcl->save()) {
            $this->_sendJson($pcl->getErrors());
        }

        $pipelinesLus = PipelineComponentLu::model()->findAllByAttributes(array('component_id' => $_POST['componentId']));

        $pipelines = array();
        foreach($pipelinesLus as $pipelineLu) {
            $pipelines[$pipelineLu->pipeline_id] = array(
                'pipeline' => $pipelineLu->pipeline->name,
                'stage' => $pipelineLu->pipelineStage->name
            );
        }

        $this->_sendJson(array(
            'status' => 'success',
            'message' => 'Pipeline Assignment Saved.',
            'pipelines' => $pipelines
        ));
    }
}