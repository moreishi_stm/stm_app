<?php
	class TrainingController extends AdminController {

		/**
		 * Specifies the action rules.
		 *
		 * @return array actions
		 */
		public function actions() {
			return array(
                //'viewTimeblock' => 'stm_app.modules.admin.controllers.actions.tracking.ViewTimeblockAction',
			);
		}

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

        public function actionIndex()
        {
            $this->title = 'Training List';

            $model = new Training('search');
            $model->unsetAttributes();  // clear any default values
            $model->status_ma = StmFormHelper::ACTIVE;

            if($_COOKIE['Training']) {
                $model->attributes = $_COOKIE['Training'];
            }

            if (isset($_GET['Training'])) {
                $model->attributes=$_GET['Training'];
            }

            $this->controller->render('list',array(
                    'model'=>$model
                ));
        }
    }
