<?php
class DirectMailController extends AdminController
{
    const TEST_API_KEY = 'test_7946f241b91777e54daa05518ce9d9cc21a';
    public $pageColor = 'blue';

    public function actionIndex()
    {exit;
        $apiKey = (YII_DEBUG) ? self::TEST_API_KEY : self::TEST_API_KEY;

        $frontCardUrl = 'https://lob.com/postcard_4x6_front.html';
        $backCardUrl = 'https://lob.com/postcard_4x6_back.html';

        $frontPostcard = file_get_contents($frontCardUrl);
        $backPostcard = file_get_contents($backCardUrl);
        $lob = new \Lob\Lob($apiKey);

        $to_address = $lob->addresses()->create(array(
                'name'          => 'Lob.com',
                'address_line1' => '185 Berry Street',
                'address_line2' => 'Suite 1510',
                'address_city'  => 'San Francisco',
                'address_state' => 'CA',
                'address_zip'   => '94107',
                'email'         => 'support@lob.com',
                'phone'         => '555-555-5555'
            ));

        $from_address = $lob->addresses()->create(array(
                'name'          => 'The Big House',
                'address_line1' => '1201 S Main St',
                'address_line2' => '',
                'address_city'  => 'Ann Arbor',
                'address_state' => 'MI',
                'address_zip'   => '48104',
                'email'         => 'goblue@umich.edu',
                'phone'         => '734-647-2583'
            ));

        $postcard = $lob->postcards()->create(array(
                'to'          => $to_address['id'],
                'from'        => $from_address['id'],
                'front'       => $frontPostcard,
                'back'        => $backPostcard,
                //'data[variable_name]'  => 'Harry'
                'data'        => array('variable_name'=> 'Harry')
            ));
        print_r($postcard);
   }
}