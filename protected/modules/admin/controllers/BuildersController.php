<?php
	class BuildersController extends AdminController {

		public $pageColor = 'blue';

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

        public function actionIndex()
        {
            $this->title = 'Builders List';

            $model = new Builders;
            $model->unsetAttributes(); // clear any default values

            if (isset($_GET['Builders'])) {
                $model->attributes = $_GET['Builders'];
            }

            $this->render('list', array(
                    'model' => $model,
                )
            );
        }

        public function actionAdd()
        {
            $this->title = 'Add New Builder';

            $model = new Builders;
            $model->unsetAttributes(); // clear any default values

            if (isset($_POST['Builders'])) {
                $model->attributes = $_POST['Builders'];
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully added Builder.');
                    $this->redirect('/'.Yii::app()->controller->module->name.'/builders');
                }
                else {
                    $error = current($model->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
            }

            $this->render('form', array(
                    'model' => $model,
                )
            );
        }

        public function actionEdit($id)
        {
            $this->title = 'Edit Builder';

            $model = new Builders;
            $model = $model->findByPk($id);

            if (isset($_POST['Builders'])) {
                $model->attributes = $_POST['Builders'];
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully updated Builder.');
                    $this->redirect('/'.Yii::app()->controller->module->name.'/builders');
                }
                else {
                    $error = current($model->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
            }

            $this->render('form', array(
                    'model' => $model,
                )
            );
        }

        public function actionFloorPlans($id=null)
        {
            $this->title = 'Floor Plans List';

            $model = new FloorPlans;
            $model->unsetAttributes(); // clear any default values

            if($builder = ($id) ? Builders::model()->findByPk($id) : null) {
                $model->builder_id = $builder->id;
            }

            if (isset($_GET['FloorPlans'])) {
                $model->attributes = $_GET['FloorPlans'];
            }

            $this->render('listFloorPlans', array(
                    'model' => $model,
                    'builder' => $builder,
                    'builderName' => $builder->name,
                )
            );
        }

        public function actionEditFloorPlan($id)
        {
            $this->title = 'Edit Floor Plan';

            $model = FloorPlans::model()->findByPk($id);
            $builder = Builders::model()->findByPk($model->builder_id);

            if (isset($_POST['FloorPlans'])) {
                $model->attributes = $_POST['FloorPlans'];
                if($model->save()) {

                    FloorPlanCommunityLu::model()->deleteAllByAttributes(array('floor_plan_id'=>$model->id));
                    if($model->communitiesCollection) {
                        foreach($model->communitiesCollection as $featuredAreaId) {
                            $floorPlanCommunityLu = new FloorPlanCommunityLu();
                            $floorPlanCommunityLu->floor_plan_id = $model->id;
                            $floorPlanCommunityLu->featured_area_id = $featuredAreaId;
                            if($floorPlanCommunityLu->save()) {

                            }
                        }
                    }

                    Yii::app()->user->setFlash('success', 'Successfully updated Floor Plan.');
                    $this->redirect('/'.Yii::app()->controller->module->name.'/builders/floorPlans');
                }
                else {
                    $error = current($model->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
            }
            else {
                if($model->floorPlanCommunityLu) {
                    foreach($model->floorPlanCommunityLu as $floorPlanCommunityLu) {
                        $communitiesLu[] = $floorPlanCommunityLu->featured_area_id;
                    }
                    $model->communitiesCollection = $communitiesLu;
                }
            }

            $this->render('formFloorPlan', array(
                    'model' => $model,
                    'builder' => $builder,
                )
            );
        }

        public function actionEditCommunity($id)
        {
            $this->title = 'Edit Builder Community';

            $model = new BuilderCommunityLu;
            $model = $model->findByPk($id);

            $builder = Builders::model()->findByPk($model->builder_id);

            if (isset($_POST['BuilderCommunityLu']) && $builder) {
                $model->attributes = $_POST['BuilderCommunityLu'];
                $model->builder_id = $builder->id;
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully updated Builder Community.');
                    $this->redirect('/'.Yii::app()->controller->module->name.'/builders/communities/'.$builder->id);
                }
                else {
                    $error = current($model->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
            }

            $this->render('formCommunity', array(
                    'model' => $model,
                    'builder' => $builder,
                )
            );
        }

        public function actionAddCommunity($id)
        {
            $this->title = 'Add New Builder Community';

            $model = new BuilderCommunityLu;
//            $model->state_id = Yii::app()->user->settings->office_state;
            $builder = Builders::model()->findByPk($id);

            if (isset($_POST['BuilderCommunityLu']) && $builder) {
                $model->attributes = $_POST['BuilderCommunityLu'];
                $model->builder_id = $builder->id;
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully added Builder Community.');
                    $this->redirect('/'.Yii::app()->controller->module->name.'/builders');
                }
                else {
                    $error = current($model->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
            }

            $this->render('formCommunity', array(
                    'model' => $model,
                    'builder' => $builder,
                )
            );
        }

        public function actionAddFloorPlan($id)
        {
            $this->title = 'Add New Floor Plan';

            $model = new FloorPlans;
            $builder = Builders::model()->findByPk($id);

            if (isset($_POST['FloorPlans']) && $builder) {
                $model->attributes = $_POST['FloorPlans'];
                $model->builder_id = $builder->id;
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully added Floor Plan.');
                    $this->redirect('/'.Yii::app()->controller->module->name.'/builders/floorPlans/'.$builder->id);
                }
                else {
                    $error = current($model->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
            }

            $this->render('formFloorPlan', array(
                    'model' => $model,
                    'builder' => $builder,
                )
            );
        }

        public function actionModelHomes()
        {
            $this->title = 'Model Homes List';

            $model = new ModelHomes();
            $model->unsetAttributes(); // clear any default values

            if (isset($_GET['ModelHomes'])) {
                $model->attributes = $_GET['ModelHomes'];
            }

            $this->render('listModelHomes', array(
                    'model' => $model,
                )
            );
        }

        public function actionAddModelHome($id)
        {
            $this->title = 'Add Model Home';

            $model = new ModelHomes;
            $builder = Builders::model()->findByPk($id);

            if (isset($_POST['ModelHomes'])) {
                $model->attributes = $_POST['ModelHomes'];
                $model->builder_id = $builder->id;
                if($model->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully added Model Home.');
                    $this->redirect('/'.Yii::app()->controller->module->name.'/builders');
                }
                else {
                    $error = current($model->getErrors());
                    Yii::app()->user->setFlash('error', $error[0]);
                }
            }

            $this->render('formModelHome', array(
                    'model' => $model,
                    'builder' => $builder,
                )
            );
        }

        public function actionCommunities($id=null)
        {
            $this->title = 'Builder Community';

            $model = new BuilderCommunityLu;
            if($builder = ($id) ? Builders::model()->findByPk($id) : null) {
                $model->builder_id = $builder->id;
            }

            $this->render('listCommunities', array(
                    'model' => $model,
                    'builder' => $builder,
                )
            );
        }

    }