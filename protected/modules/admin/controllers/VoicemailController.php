<?php
/**
 * Hunt Group Controller
 */
class VoicemailController extends AdminController
{
    public $title = 'Voicemails';

    /**
     * Initializes the controller
     * @return none
     */
    public function init() {
        $this->displayName = 'Voicemails';
        $this->baseModel = new Ivrs();
    }

    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
            'index'         =>'stm_app.modules.admin.controllers.actions.voicemail.GreetingsAction',
            'list'          =>'stm_app.modules.admin.controllers.actions.voicemail.ListAction',
            'view'          =>'stm_app.modules.admin.controllers.actions.voicemail.ListAction',
            'greetings'     =>'stm_app.modules.admin.controllers.actions.voicemail.GreetingsAction',
            'addGreeting'   =>'stm_app.modules.admin.controllers.actions.voicemail.AddGreetingAction',
            'editGreeting'  =>'stm_app.modules.admin.controllers.actions.voicemail.EditGreetingAction',
            'delete'        =>'stm_app.modules.admin.controllers.actions.voicemail.DeleteAction',
        );
    }
}