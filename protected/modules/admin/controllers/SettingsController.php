<?php
class SettingsController extends AdminController
{
    public $pageColor = 'blue';

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function actions()
    {
        return array(
            'account' => 'stm_app.modules.admin.controllers.actions.settings.AccountAction',
            'view' => 'stm_app.modules.admin.controllers.actions.settings.EditAction',
            'cropProfilePhoto' => 'stm_app.modules.admin.controllers.actions.settings.CropProfilePhotoAction',
            // 'delete'   =>'stm_app.modules.admin.controllers.actions.contacts.DeleteAction',
            'users' => 'stm_app.modules.admin.controllers.actions.settings.UsersAction',
            'usersAdd' => 'stm_app.modules.admin.controllers.actions.settings.UsersAddAction',
            'usersLenderAdd' => 'stm_app.modules.admin.controllers.actions.settings.UsersLenderAddAction',
            'index' => 'stm_app.modules.admin.controllers.actions.settings.EditAction',
            'stickyNotes' => 'stm_app.modules.admin.controllers.actions.settings.StickyNotesAction',
            'verifySms' => 'stm_app.modules.admin.controllers.actions.settings.VerifySmsAction',
			'verifySmsCode' => 'stm_app.modules.admin.controllers.actions.settings.VerifySmsCodeAction',
			'searchAvailableNumbersAction' => 'stm_app.modules.admin.controllers.actions.settings.SearchAvailableNumbersAction',
			'saveAvailableNumbersAction' => 'stm_app.modules.admin.controllers.actions.settings.SaveAvailableNumbersAction',
            'saveProfilePhoto' => 'stm_app.modules.admin.controllers.actions.settings.SaveProfilePhotoAction',
            'saveAccountLogo' => 'stm_app.modules.admin.controllers.actions.settings.SaveAccountLogoAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return CMap::mergeArray(parent::filters(), array());
    }

    /**
     * Initializes the controller
     * @return none
     */
    public function init()
    {
        $this->baseModel = new Settings;
    }
}