<?php
	class SavedHomeSearchController extends AdminController {

		public $pageColor = 'teal';

		public function actions() {
			return array(
				'add' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.AddAction',
				'edit' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.EditAction',
				'delete' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.DeleteAction',
				'email' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.EmailAction',
				'view' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.ViewAction',
				'index' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.ListAction',
                'list' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.ListAction',
				'listMini' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.ListMiniAction',
				'preview' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.PreviewAction',
				'sent' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.SentAction',
				'sentBy' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.SentByAction',
                'sentBySearch' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.SentBySearchAction',
                'sentTo' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.SentToAction',
				'autocomplete' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.AutocompleteAction',
                'transfer' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.TransferAction',
                'send' => 'stm_app.modules.admin.controllers.actions.savedHomeSearch.SendAction',
			);
		}

		/**
		 * @return array action filters
		 */
		public function filters() {
			return CMap::mergeArray(parent::filters(), array());
		}

		/**
		 * Initializes the controller
		 *
		 * @return none
		 */
		public function init() {
			$this->baseModel = new SavedHomeSearches;
		}
	}