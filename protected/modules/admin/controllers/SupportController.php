<?php
class SupportController extends AdminController
{
	public $pageColor = 'orange';

    public function actions()
    {
    	return array(
			'index'         =>'stm_app.modules.admin.controllers.actions.support.IndexAction',
            'comprehensive' =>'stm_app.modules.admin.controllers.actions.support.ComprehensiveAction',
            'level1'        =>'stm_app.modules.admin.controllers.actions.support.Level1Action',
//            'intermediate'  =>'stm_app.modules.admin.controllers.actions.support.IntermediateAction', //to deprecate/delete after March 1 2014
            'level2'        =>'stm_app.modules.admin.controllers.actions.support.Level2Action',
            'level3'        =>'stm_app.modules.admin.controllers.actions.support.Level3Action',
            'level4'        =>'stm_app.modules.admin.controllers.actions.support.Level4Action',
            'lender'         =>'stm_app.modules.admin.controllers.actions.support.LenderAction',
            'lenderLevel1'   =>'stm_app.modules.admin.controllers.actions.support.LenderLevel1Action',
//            'lenderLevel2'   =>'stm_app.modules.admin.controllers.actions.support.LenderLevel2Action',
            'agent'         =>'stm_app.modules.admin.controllers.actions.support.AgentAction',
            'agentLevel1'   =>'stm_app.modules.admin.controllers.actions.support.AgentLevel1Action',
//            'agentlevel1'   =>'stm_app.modules.admin.controllers.actions.support.AgentLevel1Action', //to deprecate/delete after March 1 2014
            'agentLevel2'   =>'stm_app.modules.admin.controllers.actions.support.AgentLevel2Action',

            'teamleader'         =>'stm_app.modules.admin.controllers.actions.support.TeamleaderAction',
            'video'         =>'stm_app.modules.admin.controllers.actions.support.VideoAction',
            'tag'         =>'stm_app.modules.admin.controllers.actions.support.TagAction',

    	);
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
//		$this->baseModel = new Sources;
	}

    public function getBaseCssScript() {
        return <<<CSS
    div#support-dashboard {
        margin-bottom: 20px;
        max-width: 1000px;
        margin-left: auto;
        margin-right: auto;
    }
    #support-dashboard ul{
        list-style-type: none;
        font-size: 20px;
    }
    #support-dashboard ul li{
        float: left;
        width: 20%;
        margin-bottom: 20px;;
        margin-left: 5%;
        min-width: 172px;
    }
    #support-dashboard ul li a{
        display: block;
        height: 150px;
        text-align: center;
        border: 3px #84a1dd solid;
        padding: 20px;
        background: #d6e5f8;
        -webkit-border-radius: 200px;
        -moz-border-radius: 200px;
        border-radius: 200px;
        line-height: 1.2;
        width: 155px;
    }
    #support-dashboard ul li a:hover{
        border-color: #C20000;
        background: #fbecef;
    }
    #support-dashboard ul li a span{
        top: 45px;
        position: relative;
    }
CSS;

    }
}