<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 8/12/13
 */
abstract class ApiResource extends CModel {

	/**
	 * @return StmActiveRecord base active record instance for a given api resource
	 */
	abstract public function getResourceModel();

	/**
	 * Can be extended to provide specific searching functionality for a resource
	 * Currently utilizes a resource controllers's searchable attribute names collection to filter the
	 * resources automatically
	 * @return CDbCriteria
	 */
	public function getSearchCriteria() {
		$searchCriteria = new CDbCriteria;

		// If there are no valid searchable attributes, just return the base CDbCriteria
		$searchableAttributeNames = $this->attributeNames();
		if (!$searchableAttributeNames) {
			return $searchCriteria;
		}

		$responseModel = $this->getResourceModel();
		foreach ($searchableAttributeNames as $attributeName) {

			$attributeRequestValue = Yii::app()->getRequest()->getQuery($attributeName);
			if ($attributeRequestValue) {
				$searchCriteria->compare($attributeName, $attributeRequestValue);
			}
		}

		return $searchCriteria;
	}
}