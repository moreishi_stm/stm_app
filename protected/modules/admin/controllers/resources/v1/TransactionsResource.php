<?php

/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 8/12/13
 */
class TransactionsResource extends ApiResource {

	public function attributeNames() {
		return array(
		);
	}

	public function getResourceModel() {
		return new Transactions;
	}
}