<?php

/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 8/12/13
 */
class EmailsResource extends ApiResource {

	/**
	 * Searchable attribute names for this resource
	 */
	public function attributeNames() {
		return array(
			'email',
		);
	}

	/**
	 * The base model used for this resource
	 */
	public function getResourceModel() {
		return new Emails;
	}
}