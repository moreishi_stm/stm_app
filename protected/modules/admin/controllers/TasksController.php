<?php
class TasksController extends AdminController {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

	/**
	 * Specifies the action rules.
	 *
	 * @return array actions
	 */
	public function actions() {
		return array(
			'add' => 'stm_app.modules.admin.controllers.actions.tasks.AddAction',
			'validate' => 'stm_app.modules.admin.controllers.actions.tasks.ValidateAction',
			'complete' => 'stm_app.modules.admin.controllers.actions.tasks.CompleteAction',
			'dashboard' => 'stm_app.modules.admin.controllers.actions.tasks.DashboardAction',
			'view' => 'stm_app.modules.admin.controllers.actions.tasks.ViewAction',
			'delete' => 'stm_app.modules.admin.controllers.actions.tasks.DeleteAction',
			'edit' => 'stm_app.modules.admin.controllers.actions.tasks.EditAction',
			'index' => 'stm_app.modules.admin.controllers.actions.tasks.ListAction',
			'listMini' => 'stm_app.modules.admin.controllers.actions.tasks.ListMiniAction',
            'transfer' => 'stm_app.modules.admin.controllers.actions.tasks.TransferAction',
		);
	}

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new Tasks;
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$model = new Task('search');
		$model->unsetAttributes(); // clear any default values

		if (isset($_GET['Task'])) {
			$model->attributes = $_GET['Task'];
		}

		$this->render('admin', array(
			'model' => $model,
		));
	}
}
