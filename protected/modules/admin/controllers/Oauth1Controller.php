<?php
/**
 * oAuth 1 Controller
 *
 * Workflow for oAuth 1
 */
class Oauth1Controller extends AdminController
{
    /**
     * Type
     *
     * The type of service
     * @var string
     */
    protected $_type;

    /**
     * Type Map
     *
     * @var array
     */
    protected $_typeMap;

    /**
     * Consumer
     *
     * @var Zend_Oauth_Consumer
     */
    protected $_consumer;

    /**
     * Init
     */
    public function init()
    {
        // Configure type map
        $this->_typeMap = array(
            'twitter'	=>	array(
                'name'			=>	'Twitter',
                'url'			=>	'https://api.twitter.com/oauth',
                'key'			=>	'XXXXXXXX',
                'secret'		=>	'XXXXXXXX'
            ),
            'tumblr'	=>	array(
                'name'		=>	'Tumblr',
                'url'		=>	'http://www.tumblr.com/oauth',
                'key'		=>	'XXXXXXXXXX',
                'secret'	=>	'XXXXXXXXXX'
            )
        );

        // Make sure we have a valid type
        $this->_type = \Yii::app()->request->getParam('type');
        if(!in_array($this->_type, array_keys($this->_typeMap))) {
            throw new \Exception('Invalid type passed, or no type passed.');
        }

        // Use oAuth
        $url = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . (in_array($_SERVER['SERVER_PORT'], array('80', '443')) ? '' : ':' . $_SERVER['SERVER_PORT']);
        $this->_consumer = new \Zend_Oauth_Consumer(array(
            'callbackUrl'		=> $url . '/oauth/oauth1/callback/type/' . $this->_type,
            'siteUrl'			=> $this->_typeMap[$this->_type]['url'],
            'consumerKey'		=> $this->_typeMap[$this->_type]['key'],
            'consumerSecret'	=> $this->_typeMap[$this->_type]['secret']
        ));
    }

    /**
     * Index Action
     *
     * Default page load action
     * @return void
     */
    public function actionIndex()
    {
        // If we didn't have success
        if(\Yii::app()->request->getParam('status') != 'success') {
            throw new \Exception('Invalid status');
        }

        // Render the view
        $this->render('index', array());
    }

    /**
     * Redirect Action
     *
     * Sends the user to authenticate an authorization request
     * @return void
     */
    public function actionRedirect()
    {
        // Fetch a request token
        $token = $this->_consumer->getRequestToken();

        // Persist the token to storage
        $_SESSION['requestToken'] = serialize($token);

        // Redirect the user
        $this->_consumer->redirect();
    }

    /**
     * Callback Action
     *
     * Retrieves and stores authorization token
     * @return void
     */
    public function callbackAction()
    {
        // Make sure we had the proper request variables
        if((!isset($_GET['oauth_token']) || !isset($_GET['oauth_verifier'])) || isset($_GET['denied'])) {
            header('Location: /oauth/oauth1/index/status/fail/type/' . $this->_type);
            \Yii::app()->end();
        }

        // Retrieve the token from the session
        $requestToken = unserialize($_SESSION['requestToken']);

        // Verify session token matches given token
        if($_GET['oauth_token'] != $requestToken->getToken()) {
            header('Location: /oauth/oauth1/index/status/fail/type/' . $this->_type);
            \Yii::app()->end();
        }

        // Get access token
        $accessToken = $this->_consumer->getAccessToken($_GET, $requestToken);


        //
        // Store key and secret in database
        //

//        $accessToken->getToken();
//        $accessToken->getTokenSecret();

        // Redirect
        header('Location: /admin/oauth1/index?status=success&type=' . $this->_type);
        \Yii::app()->end();
    }
}