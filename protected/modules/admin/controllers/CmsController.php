<?php
class CmsController extends AdminController
{

    public $pageColor = 'teal';

    /**
     * Specifies the action rules.
     *
     * @return array actions
     */
    public function actions()
    {

        return array(
            'index'                 => 'admin_module.controllers.actions.cms.DomainListAction',
            'addComment'            => array('class' => 'admin_module.controllers.actions.cms.AddComment'),
            'deleteComment'         => array('class' => 'admin_module.controllers.actions.cms.DeleteCommentAction'),
            'comments'              => array('class' => 'admin_module.controllers.actions.cms.ListCommentsAction'),
            'addPage'               => array('class' => 'admin_module.controllers.actions.cms.AddPageAction', 'pageType' => CmsContents::TYPE_PAGE, 'templateType' => CmsTemplates::TEMPLATE_TYPE_PAGE),
            'addBlog'               => array('class' => 'admin_module.controllers.actions.cms.AddPageAction', 'pageType' => CmsContents::TYPE_BLOG, 'templateType' => CmsTemplates::TEMPLATE_TYPE_BLOG),
            'addListingStoryboard'  => array('class' => 'admin_module.controllers.actions.cms.AddStoryboardAction', 'pageType' => CmsContents::TYPE_LISTING_SB, 'templateType' => CmsTemplates::TEMPLATE_TYPE_LISTING_SB),
            'addBuyerStoryboard'    => array('class' => 'admin_module.controllers.actions.cms.AddStoryboardAction', 'pageType' => CmsContents::TYPE_BUYER_SB, 'templateType' => CmsTemplates::TEMPLATE_TYPE_BUYER_SB),
            'addHouseValues'        => array('class' => 'admin_module.controllers.actions.cms.AddPageAction', 'pageType' => CmsContents::TYPE_HOUSE_VALUES, 'templateType' => CmsTemplates::TEMPLATE_TYPE_HOUSE_VALUES),
            'addLandingPages'       => array('class' => 'admin_module.controllers.actions.cms.AddPageAction', 'pageType' => CmsContents::TYPE_LANDING_PAGES, 'templateType' => CmsTemplates::TEMPLATE_TYPE_LANDING_PAGES),
            'editPage'              => array('class' => 'admin_module.controllers.actions.cms.EditPageAction', 'pageType' => CmsContents::TYPE_PAGE, 'templateType' => CmsTemplates::TEMPLATE_TYPE_PAGE),
            'editBlog'              => array('class' => 'admin_module.controllers.actions.cms.EditPageAction', 'pageType' => CmsContents::TYPE_BLOG, 'templateType' => CmsTemplates::TEMPLATE_TYPE_BLOG),
            'editListingStoryboard' => array('class' => 'admin_module.controllers.actions.cms.EditStoryboardAction', 'pageType' => CmsContents::TYPE_LISTING_SB, 'templateType' => CmsTemplates::TEMPLATE_TYPE_LISTING_SB),
            'editBuyerStoryboard'   => array('class' => 'admin_module.controllers.actions.cms.EditStoryboardAction', 'pageType' => CmsContents::TYPE_BUYER_SB, 'templateType' => CmsTemplates::TEMPLATE_TYPE_BUYER_SB),
            'editHouseValues'       => array('class' => 'admin_module.controllers.actions.cms.EditHouseValuesAction', 'pageType' => CmsContents::TYPE_HOUSE_VALUES, 'templateType' => CmsTemplates::TEMPLATE_TYPE_HOUSE_VALUES),
            'editLandingPages'       => array('class' => 'admin_module.controllers.actions.cms.EditLandingPagesAction', 'pageType' => CmsContents::TYPE_LANDING_PAGES, 'templateType' => CmsTemplates::TEMPLATE_TYPE_LANDING_PAGES),
            'listListingStoryboard' => array('class' => 'admin_module.controllers.actions.cms.ListListingStoryboardAction', 'pageType' => CmsContents::TYPE_LISTING_SB, 'templateType' => CmsTemplates::TEMPLATE_TYPE_LISTING_SB),
            'listBuyerStoryboard'   => array('class' => 'admin_module.controllers.actions.cms.ListBuyerStoryboardAction', 'pageType' => CmsContents::TYPE_BUYER_SB, 'templateType' => CmsTemplates::TEMPLATE_TYPE_BUYER_SB),
            'landingPages'          => array('class' => 'admin_module.controllers.actions.cms.ListLandingPagesAction'),
            'houseValues'           => array('class' => 'admin_module.controllers.actions.cms.ListHouseValuesAction'),
            'domain'                => array('class' => 'admin_module.controllers.actions.cms.DomainPagesAction'),
            'tags'                  => 'admin_module.controllers.actions.cms.TagsListAction',
            'tagsEdit'              => 'admin_module.controllers.actions.cms.TagsEditAction',
            'tagsAdd'               => 'admin_module.controllers.actions.cms.TagsAddAction',
			'menus'					=> 'admin_module.controllers.actions.cms.MenusAction',
			'checkMenuNameAndSaveAction'			=> 'admin_module.controllers.actions.cms.CheckMenuNameAndSaveAction',
			'assignMenuPosition'	=> 'admin_module.controllers.actions.cms.AssignMenuPositionAction',
			'saveMenuLinks'			=> 'admin_module.controllers.actions.cms.SaveMenuLinksAction',
			'removeMenuLink'		=> 'admin_module.controllers.actions.cms.RemoveMenuLinkAction',
			'deleteCmsImage'		=> 'admin_module.controllers.actions.cms.DeleteCmsImageAction',
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return CMap::mergeArray(parent::filters(), array(
            'stmAccessControl' => array(
                'class' => 'admin_module.components.filters.StmAccessControl',
                'guestAllowedActions' => array(
                    'addComment'
                ),
            ), // perform access control
        ));
    }

    /**
     * Initializes the controller
     *
     * @return none
     */
    public function init()
    {
        $this->displayName = 'CMS';
        $this->baseModel = new Domains;

        Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/style.cms.css');
    }

	public function actionMainImageManager()
	{
		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/dropzone.js?V=1', CClientScript::POS_END);
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/standard2/assets/css/font-awesome.css');
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/dropzone.basic.min.css');
		Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.com/assets/css/dropzone.min.css');

		$mainImageSetting = Settings::model()->findByAttributes(array('name' => 'homePageMainImage'));

		$mainImageAccountValue = SettingAccountValues::model()->findByAttributes(array('account_id' => Yii::app()->user->accountId, 'setting_id' => $mainImageSetting->id));

		$this->render('mainImageManager',array(
			'currentImageId' => $mainImageAccountValue->id,
			'currentImage' => $mainImageAccountValue->value
		));
	}

	public function actionAssignMainImage() {
		if (!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array('results'=>'error','message'=>'Invalid Request.'));
			Yii::app()->end();
		}

		$mainImageSetting = Settings::model()->findByAttributes(array('name' => 'homePageMainImage'));
		$mainImageAccountValue = SettingAccountValues::model()->findByAttributes(array('account_id' => Yii::app()->user->accountId, 'setting_id' => $mainImageSetting->id));

		if(empty($mainImageAccountValue)) {
			$mainImageAccountValue = new SettingAccountValues();
		}

		if(!isset($_POST['imageData']) || empty($_POST['imageData'])) {
			echo CJSON::encode(array('results'=>'error','message'=>'No File url found.'));
			Yii::app()->end();
		}

		$mainImageAccountValue->setting_id = $mainImageSetting->id;
		$mainImageAccountValue->value = $_POST['imageData']['basePath'].$_POST['imageData']['image'];

		$mainImageAccountValue->save();

		$results = $mainImageAccountValue->attributes;
		$results['results'] = true;

		echo CJSON::encode($results);
		Yii::app()->end();
	}

	public function actionUpdateCustomLink() {
		if (!Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array('results'=>'error','message'=>'Invalid Request.'));
			Yii::app()->end();
		}

		if(!isset($_POST['link_id']) || empty($_POST['link_id'])) {
			echo CJSON::encode(array('results'=>'error','message'=>'No Link ID found.'));
			Yii::app()->end();
		}

		$cmsMenuLinks = CmsMenuLinks::model()->findByPk($_POST['link_id']);

		if(empty($cmsMenuLinks)) {
			echo CJSON::encode(array('results'=>'error','message'=>'No Link found.'));
			Yii::app()->end();
		}

		if(!isset($_POST['title']) || empty($_POST['title'])) {
			echo CJSON::encode(array('results'=>'error','message'=>'No Title found.'));
			Yii::app()->end();
		}

		if(!isset($_POST['url']) || empty($_POST['url'])) {
			echo CJSON::encode(array('results'=>'error','message'=>'No Url found.'));
			Yii::app()->end();
		}

		$cmsMenuLinks->name = $_POST['title'];
		$cmsMenuLinks->url = $_POST['url'];

		$cmsMenuLinks->save();

		echo CJSON::encode(array(
			'results' => true,
			'link_id' => $_POST['link_id'],
			'title' => $_POST['title'],
			'url' => $_POST['url']
		));
		Yii::app()->end();
	}
}