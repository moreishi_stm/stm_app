<?php

class CompaniesController extends AdminController
{
	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}

    public function actions()
    {
    	return array(
			'add'           =>'stm_app.modules.admin.controllers.actions.companies.AddAction',
			'addContact'    =>'stm_app.modules.admin.controllers.actions.companies.AddContactAction',
			'removeContact' =>'stm_app.modules.admin.controllers.actions.companies.RemoveContactAction',
			'view'          =>'stm_app.modules.admin.controllers.actions.companies.ViewAction',
			'delete'        =>'stm_app.modules.admin.controllers.actions.companies.DeleteAction',
			'edit'          =>'stm_app.modules.admin.controllers.actions.companies.EditAction',
			'index'         =>'stm_app.modules.admin.controllers.actions.companies.ListAction',
    	);
    }

	/**
	 * Initializes the controller
	 * @return none
	 */
	public function init() {
		$this->baseModel = new Companies;
	}
}