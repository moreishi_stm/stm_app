<?php
class LockboxesController extends AdminController
{
	public $pageColor = 'blue';
    /**
     * Specifies the action rules.
     * @return array actions
     */
    public function actions()
    {
        return array(
			'index'       =>'stm_app.modules.admin.controllers.actions.lockboxes.ListAction',
            'add'         =>'stm_app.modules.admin.controllers.actions.lockboxes.AddAction',
			'edit'        =>'stm_app.modules.admin.controllers.actions.lockboxes.EditAction',
            'view'        =>'stm_app.modules.admin.controllers.actions.lockboxes.EditAction',
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters() {
		return CMap::mergeArray(parent::filters(), array(

		));
	}
}