<?php
Yii::setPathOfAlias('stm_app_root', SITE_ROOT . DS . 'stm_app');

class AdminModule extends CWebModule
{

    const REFRESH_CSS_ASSETS = null;

    const REFRESH_IMAGE_ASSETS = null;

    const REFRESH_JS_ASSETS = null;

    public $defaultController = 'main';

    public $layoutPath;

    public $layout = 'column1';

    public $cssAssets
        = array(
            'style.css?v=18',
            'style.sidebar.css',
            'style.login.css?v=3',
            'style.colors.css?v=2',
            'style.breadcrumb.css?v=2',
            'style.gallery.css',
            'style.messageBox.css',
            'style.alertBox.css',
            'style.liveSearch.css',
            'style.wizard.css',
            'style.errorPage.css',
            'style.widgets.css?v=4',
            'style.grid.css',
            'style.jui.css?v=2',
            'style.pageOptions.css?v=3',
            'style.ddMenu.css?v=4',
            'style.form.css?v=2',
            'style.comboSelect.css',
            'style.buttons.css?v=3',
            'style.icons.css?v=2',
            'style.pager.css',
            'jquery.datatables.css',
            'bootstrap-grid-only.css'
        );
    public $cdnCssAssets = array();

    public $coreScripts
        = array(
            'jquery',
			'jqueryMigrate',
            'jquery.ui',
        );

    public $javaScriptAssets
        = array(
            'handlebars/handlebars-4.0.5.js',
            'jquery/plugins/jquery.form.min.js',
            'jquery/plugins/jquery.populate.min.js',
            'jquery.ui.touch-punch.0.2.3.min.js',
            'ajaxSetup.js?v=8',
            'wl_Multiselect.js',
            'wl_Dialog.js',
            'plugins.js',
            'stm_Message.js',
            'stm_PlaceholderFix.js',
            'stm_BrokenImageFix.js',
            'click2callHelper.js'
        );
    public $cdnJsAssets = array();

    protected $_cssAssetsUrl = null;

    protected $_imageAssetsUrl = null;

    protected $_jsAssetsUrl = null;

    public function preventScriptReload()
    {

        $scripts = CMap::mergeArray($this->cssAssets, $this->coreScripts, $this->javaScriptAssets);
        foreach ($scripts as $script) {
            Yii::app()->clientScript->scriptMap[$script] = false;
        }
    }

    public function init()
    {

        $this->layoutPath = Yii::getPathOfAlias('admin_module') . DS . 'views' . DS . 'layouts';
        $this->setImport(
            array(
                'admin_module.models.*',
                'admin_module.components.*',
            )
        );

        // Define error handler for this module
        Yii::app()->setComponents(
            array(
                'errorHandler' => array(
                    'errorAction' => '/admin/main/error',
                ),
            )
        );

        parent::init();
    }

    public function registerScripts()
    {

        $this->imageAssetsUrl; // registers images assets

        foreach ($this->coreScripts as $coreScript) {
            Yii::app()->clientScript->registerCoreScript($coreScript, CClientScript::POS_END);
        }

        foreach ($this->cssAssets as $cssAsset) {
            if ($this->themeAssetExists('css', $cssAsset)) {
                $cssAssetPath = $this->getThemeAssetUrl() . '/css/' . $cssAsset;
            } else {
                $cssAssetPath = $this->getCdnAssetUrl() . '/css/' . $cssAsset;
            }

            Yii::app()->clientScript->registerCssFile($cssAssetPath);
        }

        foreach ($this->javaScriptAssets as $jsAsset) {
            if ($this->themeAssetExists('js', $jsAsset)) {
                $jsAssetPath = $this->getThemeAssetUrl() . '/js/' . $jsAsset;
            } else {
                $jsAssetPath = $this->getCdnAssetUrl() . '/js/' . $jsAsset;
            }

            Yii::app()->clientScript->registerScriptFile($jsAssetPath, CClientScript::POS_END);
        }

        Yii::app()->getClientScript()->scriptMap = array(
            'jquery.maskedinput.js'     => $this->jsAssetsUrl . '/inputmask/jquery.inputmask.js',
            'jquery.maskedinput.min.js' => $this->jsAssetsUrl . '/inputmask/jquery.inputmask.js',
        );
		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/inputmask/inputmask.js?v=1', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/assets/js/inputmask/jquery.inputmask.js?v=1', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/bbq.jquery.min.js', CClientScript::POS_END);
    }

    private function themeAssetExists($assetType, $assetName)
    {

        $themeAssestExists = true;

        $validTypes = array(
            'js',
            'css',
            'image'
        );
        if (!in_array($assetType, $validTypes)) {
            return !$themeAssetExists;
        }

        if (!file_exists($this->getThemeAssetPath() . DS . $assetType . DS . $assetName)) {
            return !$themeAssestExists;
        }

        return $themeAssestExists;
    }

    public function getThemeAssetPath()
    {

        return Yii::app()->getTheme()->basePath . DS . 'admin' . DS . 'assets';
    }

    public function getThemeAssetUrl()
    {

        return Yii::app()->getTheme()->baseUrl . DS . 'admin' . DS . 'assets';
    }

    public function getCssAssetsUrl()
    {

        /*if ($this->_cssAssetsUrl === null) {
            $this->_cssAssetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('stm_app_root.assets.css'), true, -1, self::REFRESH_CSS_ASSETS
            );

            if(YII_DEBUG) {
                $debugDomain = Yii::app()->user->domain->getVerboseName();//str_replace('.com', '.local',Yii::app()->user->domain->getVerboseName());
                $this->_cssAssetsUrl = $this->getHttpProtocol() . $debugDomain . $this->_cssAssetsUrl;
            } else {
                $this->_cssAssetsUrl = $this->getHttpProtocol() . Yii::app()->user->domain->getVerboseName() . $this->_cssAssetsUrl;
            }
        }*/

		 if ($this->_cssAssetsUrl === null) {
            $this->_cssAssetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('stm_app_root.assets.css'), true, -1, self::REFRESH_CSS_ASSETS
            );
		 }

        return $this->_cssAssetsUrl;
    }

    /**
     * Returns the appropriate http protocol based on if ssl is enabled
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @todo   : should consider moving this along with other server related things into a separate object
     * @return bool
     */
    public function getHttpProtocol()
    {

        $httpProtocol = (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';

        return $httpProtocol;
    }

    public function getJsAssetsUrl()
    {
        /*if ($this->_jsAssetsUrl === null) {
            $this->_jsAssetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('stm_app_root.assets.js'), true, -1, self::REFRESH_JS_ASSETS
            );

            if(YII_DEBUG) {
                $debugDomain = Yii::app()->user->domain->getVerboseName();//str_replace('.com', '.local',Yii::app()->user->domain->getVerboseName());
                $this->_jsAssetsUrl = $this->getHttpProtocol() . $debugDomain . $this->_jsAssetsUrl;
            } else {
                $this->_jsAssetsUrl = $this->getHttpProtocol() . Yii::app()->user->domain->getVerboseName() . $this->_jsAssetsUrl;
            }
        }

        return $this->_jsAssetsUrl;*/

		if ($this->_jsAssetsUrl === null) {
			$this->_jsAssetsUrl = Yii::app()->getAssetManager()->publish(
				Yii::getPathOfAlias('stm_app_root.assets.js'), true, -1, self::REFRESH_JS_ASSETS
			);
		}

		return $this->_jsAssetsUrl;
    }

    public function getImageAssetsUrl()
    {

        if ($this->_imageAssetsUrl === null) {
            $this->_imageAssetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('stm_app_root.assets.images'), true, -1, self::REFRESH_IMAGE_ASSETS
            );

            if(YII_DEBUG) {
                $debugDomain = str_replace('.com', '.local',Yii::app()->user->domain->getVerboseName());
                $this->_imageAssetsUrl = $this->getHttpProtocol() . $debugDomain . $this->_imageAssetsUrl;
            } else {
                $this->_imageAssetsUrl = $this->getHttpProtocol() . Yii::app()->user->domain->getVerboseName() . $this->_imageAssetsUrl;
            }
        }

        return $this->_imageAssetsUrl;
    }

    public function getIconAssetsUrl($category = 'dark')
    {

        return $this->getCdnImageAssetsUrl() . 'icons/' . $category . '/';
    }

    public function getCdnAssetUrl()
    {
        $domain = (YII_DEBUG) ? 'local' : 'com';
        return "http://cdn.seizethemarket.{$domain}/assets";
    }

    public function getCdnImageAssetsUrl()
    {
        return 'http://cdn.seizethemarket.com/assets/images/';
    }
}
