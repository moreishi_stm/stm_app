<?php
/**
 * StmWebApplication
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 4/20/13
 *
 */
class StmWebApplication extends CWebApplication {

    protected $_domain;
    protected $_initialized = false;
    protected $_themeSettings = array();

	public function __construct($config=null)
    {
        // temp fix fo HQ
        if(strpos($_SERVER['SERVER_NAME'], 'seizethemarket') !== false && strpos($_SERVER['SERVER_NAME'], 'demo') === false) {
            parent::__construct($config);

            register_shutdown_function(array($this, 'shutdown'));
            return;
        }

        $this->setImport(array(
            'admin_module.models.*',
            'admin_module.components.*',
            'front_module.models.*',
            'front_module.components.*'
        ));

        if (isset($config['enable_theme']) && !empty($config['enable_theme']) && $config['enable_theme'] === false) {
           $config['theme'] = 'standard1';
            $config['params'] = CMap::mergeArray(
                $config['params'],  array(
                    'layoutDirectory'=>'standard1',
                )
            );

            parent::__construct($config);

            register_shutdown_function(array($this, 'shutdown'));

            return;
        }
        // put in by CLee - have Brandon check 2/8/16
        else {
            $config['theme'] = 'standard2';
            $config['params'] = CMap::mergeArray(
                $config['params'],  array(
                    'layoutDirectory'=>'standard2',
                )
            );
        }

        if(isset($config['enable_theme'])) {
            unset($config['enable_theme']);
        }

        Yii::setApplication($this);

        if (isset($config['basePath'])) {
            $this->setBasePath($config['basePath']);
            unset($config['basePath']);
        } else
            $this->setBasePath('protected');

        Yii::setPathOfAlias('application', $this->getBasePath());
        Yii::setPathOfAlias('webroot', dirname($_SERVER['SCRIPT_FILENAME']));
        Yii::setPathOfAlias('ext', $this->getBasePath() . DIRECTORY_SEPARATOR . 'extensions');
        $this->initSystemHandlers();
        $this->registerCoreComponents();
        $this->configure($config);

        $currentWWWDomain = rtrim(str_replace(array('http://', 'https://'), '', Yii::app()->getBaseUrl(true)), '/');

        if (YII_DEBUG) {
            $currentWWWDomain = str_replace('.local', '.com', $currentWWWDomain);
        }

        $currentDomain = str_replace(array('www.'), '', $currentWWWDomain);

        $Criteria = new CDbCriteria();
        $Criteria->addInCondition("name", array($currentWWWDomain, $currentDomain));

        $this->_domain = Domains::model()->byIgnoreAccountScope()->find($Criteria);

        // this will happen if domain is not .com
        if(YII_DEBUG && !$this->_domain) {
            $d = rtrim(str_replace(array('http://', 'https://'), '', Yii::app()->getBaseUrl(true)), '/');
            $currentWWWDomain = str_replace('.local', '.', $d);

            $Criteria = new CDbCriteria();
            $Criteria->addCondition("name like :name");
            $Criteria->params[':name'] = $currentWWWDomain.'%';
            $this->_domain = Domains::model()->find($Criteria);

        }

        $theme = CmsThemes::model()->findByPk($this->_domain->cms_theme_id);

        $themeConfigs['theme'] = $theme->name;
        $themeConfigs['components'] = require(Yii::getPathOfAlias('stm_app.themes') . DS . $theme->name . DS . "config.php");
        $themeConfigs['params']['layoutDirectory'] = $theme->name;

        $themeSettings = CmsThemeSettings::model()->findAllByAttributes(array('cms_theme_id' => $this->_domain->cms_theme_id));
        $themeSettingsValues = CHtml::listData(CmsThemeSettingDomainValues::model()->findAllByAttributes(array('domain_id' => $this->_domain->id)), 'cms_theme_setting_id', 'value');

        foreach($themeSettings as $themeSetting) {
            $this->_themeSettings[$themeSetting->id] = $this->_themeSettings[$themeSetting->name] = (isset($themeSettingsValues[$themeSetting->id])) ? $themeSettingsValues[$themeSetting->id] : $themeSetting->default;
        }
        unset($themeSettings);
        unset($themeSettingsValues);

        $this->configure($themeConfigs);

        $this->attachBehaviors($this->behaviors);
        $this->preloadComponents();
		register_shutdown_function(array($this, 'shutdown'));
        $this->init();
	}

	public function shutdown() {
		if (YII_ENABLE_ERROR_HANDLER && ($error = error_get_last())) {
			$this->handleError($error['type'], $error['message'], $error['file'], $error['line']);
			exit;
		}
	}

    public function setDomain($domain) {
        $this->_domain = $domain;
    }

    public function getDomain() {
        return $this->_domain;
    }

    public function getThemeSettings() {
        return $this->_themeSettings;
    }
}