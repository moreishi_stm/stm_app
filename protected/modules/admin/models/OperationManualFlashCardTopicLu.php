<?php

/**
 * This is the model class for table "operation_manual_flash_card_topic_lu".
 *
 * The followings are the available columns in table 'operation_manual_flash_card_topic_lu':
 * @property integer $operation_manual_id
 * @property string $flash_card_topic_id
 */
class OperationManualFlashCardTopicLu extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OperationManualFlashCardTopicLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'operation_manual_flash_card_topic_lu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('operation_manual_id, flash_card_topic_id', 'required'),
            array('operation_manual_id', 'numerical', 'integerOnly'=>true),
            array('flash_card_topic_id', 'length', 'max'=>11),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('operation_manual_id, flash_card_topic_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'operation_manual_id' => 'Operation Manual',
            'flash_card_topic_id' => 'Flash Card Topic',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('operation_manual_id',$this->operation_manual_id);
        $criteria->compare('flash_card_topic_id',$this->flash_card_topic_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}