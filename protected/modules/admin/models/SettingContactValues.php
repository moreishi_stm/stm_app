<?php

	/**
	 * This is the model class for table "setting_contact".
	 *
	 * The followings are the available columns in table 'setting_contact':
	 *
	 * @property integer  $id
	 * @property integer  $contact_id
	 * @property integer  $setting_id
	 * @property string   $value
	 * @property integer  $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Settings $setting
	 * @property Contacts $contact
	 */
	class SettingContactValues extends StmBaseActiveRecord
    {
		const SETTING_CELL_NUMBER_ID = 28;
        const SETTING_TEAM_DIRECT_NUMBER_ID = 49;
		const SETTING_PROFILE_ON_WEBSITE = 43;

		const PROFILE_PHOTO_WIDTH = 167;
		const PROFILE_PHOTO_HEIGHT = 220;
        const PROFILE_ASPECT_RATIO = .759;
		public $data = array();

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return SettingContact the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'setting_contact_values';
		}

		public function primaryKey() {
			// For composite primary key, return an array like the following
			return array(
				'setting_id',
				'contact_id'
			);
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id, setting_id, value, updated, updated_by',
					'required'
				),
				array(
					'contact_id, setting_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'contact_id, setting_id, value',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'setting' => array(
					self::BELONGS_TO,
					'Settings',
					'setting_id'
				),
				// 'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'contact_id' => 'Contact',
				'setting_id' => 'Setting',
				'value' => 'Value',
			);
		}

		/**
		 * __get magic getter wrapper for getValue
		 *
		 * @param  string $name setting name
		 *
		 * @return string
		 */
		// public function __get($name) {
		// 	$setting = $this->getValue($name);

		// 	if ($setting) {
		//  		return $setting->value;
		// 	}

		// 	return parent::__get($name);
		// }

		/**
		 * @return string result of account setting value
		 */
		public function getValue($fieldName, $contactId = null) {
			$contactId = ($contactId) ? $contactId : Yii::app()->user->id;

			$settingId = Settings::model()->getField($fieldName, 'contact')->id;

			return $this->find(array(
					'condition' => 'contact_id=:contact_id AND setting_id=:setting_id',
					'params' => array(
						':contact_id' => $contactId,
						':setting_id' => $settingId
					),
				)
			);
		}

		public function uploadTempProfilePhoto() {
			if (!$this->contact_id) {
				return null;
			}

			$fileInfo = pathinfo($_GET['qqfile']);
			$fileExtension = $fileInfo['extension'];

			if (!$_GET['qqfile']) {
				return array('error' => 'No files were uploaded.');
			}

			if (isset($_SERVER["CONTENT_LENGTH"])) {
				$fileSize = (int)$_SERVER["CONTENT_LENGTH"];
			} else {
				throw new Exception('Getting content length is not supported.');
			}

			if ($fileSize == 0) {
				return array('error' => 'File is empty');
			}

			if ($fileSize > (Settings::PROFILE_PHOTO_SIZE_MAX_MB * 1024 * 1024)) {
				return array('error' => 'File is too large');
			}

			$allowedExtensions = explode(',', Settings::ALLOWED_PROFILE_PHOTO_FILE_EXTENSIONS);
			$sizeLimit = Settings::PROFILE_PHOTO_SIZE_MAX_MB * 1024 * 1024;
			$contactPath = $this->getProfileImageBaseFilepath();
			$imageName = 'profile_photo_temp'.date('YmdHis').'.' . $fileExtension;
			$imageSavePath = $contactPath . DS . $imageName;

            // remove any other files with the name profile_photo_temp.* as they may be left in there from other incomplete uploads
            exec('rm -f '.$contactPath.'/profile_photo_temp*');

			if ($this->saveTempProfileImage($imageSavePath)) {
				list($width, $height) = getimagesize($imageSavePath);

				return array(
					'success' => true,
					'filename' => $imageName,
					'height' => $height,
					'width' => $width
				);
			} else {
				false;
			}
		}

		public function saveTempProfileImage($targetPath) {
			$input = fopen("php://input", "r");
			$temp = tmpfile();
			$realSize = stream_copy_to_stream($input, $temp);
			fclose($input);

			//gets size of file
			if (isset($_SERVER["CONTENT_LENGTH"])) {
				$fileSize = (int)$_SERVER["CONTENT_LENGTH"];
			} else {
				throw new Exception('Getting content length is not supported.');
			}

			if ($realSize != $fileSize) {
				return false;
			}

			$target = fopen($targetPath, "w");
			fseek($temp, 0, SEEK_SET);
			$bytesCopied = stream_copy_to_stream($temp, $target);
			fclose($target);

			return ($bytesCopied > 0) ? true : false;
		}

		/**
		 * Save the image if one is added in a template
		 *
		 * @return image path
		 */
		public function saveProfileImage() {

			$image = CUploadedFile::getInstanceByName('SettingContactValues[data][' . Settings::SETTING_PROFILE_PHOTO_ID . ']');

			// Don't attempt to save a null image
			if (!$image) {
				return false;
			}

			$contactPath = $this->getProfileImageBaseFilepath();
			$imageName = 'profile_photo.' . $image->extensionName;
			$imageSavePath = $contactPath . DS . $imageName;

			$thumb = Yii::app()->phpThumb->create($image->tempName);
			$thumb->resize(180, 220); //width x height
			$thumb->save($imageSavePath);

			// $image->saveAs($imageSavePath);

			return $imageName;
		}

		public function cropProfileImage($file, $coords) {

			$fileType = strtolower(pathinfo($file, PATHINFO_EXTENSION));

			if ($fileType == 'jpg' || $fileType == 'jpeg') {
				$fileResource = imagecreatefromjpeg($file);
			} elseif ($fileType == 'png') {
				$fileResource = imagecreatefrompng($file);
			} elseif ($fileType == 'gif') {
				$fileResource = imagecreatefromgif($file);
			} else {
				return false;
			}

			$destinationResource = imagecreatetruecolor(self::PROFILE_PHOTO_WIDTH, self::PROFILE_PHOTO_HEIGHT);
			if (!imagecopyresampled($destinationResource, $fileResource, 0, 0, $coords['x'], $coords['y'], self::PROFILE_PHOTO_WIDTH, self::PROFILE_PHOTO_HEIGHT, $coords['w'], $coords['h'])) {
				return false;
			}

            $profilePhotoFileName = $this->value = 'profile_photo.png';
			$path = $this->getProfileImageBaseFilepath() . DS . $profilePhotoFileName;
		    imagepng($destinationResource, $path, 9);
			if ($this->save()) {
				unlink($file);
			}

			$data['filename'] = $this->value;
			$data['urlPath'] = $this->getProfileImageBaseUrlPath();

			return $data;
		}

        protected function imagickProfileTBD() {
//            Yii::import('admin_module.extensions.CArray');
//            Yii::import('admin_module.extensions.image.*');
//            Yii::import('admin_module.extensions.image.drivers.*');
//            Yii::import('admin_module.extensions.image.Image_Driver');
//            $image = new Imagick($file);
//            $image = new Image($file);
//            $image->setImageFormat('png');
//            $image->resize(400, 100, Image::NONE)->quality(100);
//            $image->crop(self::PROFILE_PHOTO_WIDTH, self::PROFILE_PHOTO_HEIGHT); //
//            $image->resize(self::PROFILE_PHOTO_WIDTH, self::PROFILE_PHOTO_HEIGHT, Image::NONE)->quality(100);
//            $picture = Yii::app()->image->load($file);
//            $picture = new Imagick($file);
//            $picture->cropImage(self::PROFILE_PHOTO_WIDTH, self::PROFILE_PHOTO_HEIGHT, $coords['x'], $coords['y']);


//            $image->save($path.'png');
        }

        public function getProfileImageBaseUrlPath() {
			if (!$this->contact_id) {
				return null;
			}

			return Yii::app()->user->getProfileImageBaseUrl() . '/' . $this->contact_id . '/';
		}

		public function getProfileImageBaseFilepath() {
			if (!$this->contact_id) {
				return null;
			}

			$imageBasePath = Yii::app()->user->getProfileImageBaseFilepath();

			$contactPath = $imageBasePath . DS . $this->contact_id;
			if (!is_dir($contactPath)) {
                try {
                    $resultMkDir = mkdir($contactPath, 0777, true);
                }
                catch (ErrorException $e) {
                    $this->log(__CLASS__." (:".__LINE__.") ERROR: Could not create profile image directory. " . $e->getMessage(), CLogger::LEVEL_ERROR);
                    Yii::app()->end();
                }
			}

			return $contactPath;
		}

        protected function afterFind()
        {
            if (Settings::isDateField($this->setting_id)) {
                $this->value = Yii::app()->format->formatDate($this->value, StmFormatter::APP_DATE_FORMAT);
            }

            parent::afterFind();
        }

        protected function beforeValidate()
        {
			$this->updated = new CDbExpression('NOW()');
			$this->updated_by = Yii::app()->user->id;

			return parent::beforeValidate();
		}

		protected function beforeSave() {
			if ($this->setting_id == self::SETTING_CELL_NUMBER_ID || $this->setting_id == self::SETTING_TEAM_DIRECT_NUMBER_ID) {
				$this->value = str_replace(array(
						'(',
						')',
						'-',
						' '
					), '', $this->value
				);
			}

            if (Settings::isDateField($this->setting_id)) {
                $this->value = Yii::app()->format->formatDate($this->value, StmFormatter::MYSQL_DATE_FORMAT);
            }

            return parent::beforeSave();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('setting_id', $this->setting_id);
			$criteria->compare('value', $this->value, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
