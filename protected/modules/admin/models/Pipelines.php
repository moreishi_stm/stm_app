<?php

/**
 * This is the model class for table "pipelines".
 *
 * The followings are the available columns in table 'pipelines':
 * @property integer $id
 * @property integer $component_type_id
 * @property string $name
 * @property string $added
 * @property integer $added_by
 * @property string $updated
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property PipelineStages[] $pipelineStages
 */
class Pipelines extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pipelines the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pipelines';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('component_type_id, name, added, added_by', 'required'),
			array('component_type_id, added_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>200),
			array('updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, component_type_id, name, added, added_by, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pipelineStages' => array(self::HAS_MANY, 'PipelineStages', 'pipeline_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'component_type_id' => 'Component Type',
			'name' => 'Name',
			'added' => 'Added',
			'added_by' => 'Added By',
			'updated' => 'Updated',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('component_type_id',$this->component_type_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}