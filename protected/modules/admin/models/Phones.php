<?php

/**
 * This is the model class for table "phones".
 *
 * The followings are the available columns in table 'phones':
 *
 * @property integer                $id
 * @property integer                $account_id
 * @property integer                $contact_id
 * @property integer                $phone_type_ma
 * @property string                 $phone
 * @property integer                $owner_ma
 * @property string                 $extension
 * @property integer                $is_primary
 * @property integer                $is_deleted
 *
 * The followings are the available model relations:
 * @property MobileGatewayPhoneLu[] $mobileGatewayPhoneLus
 * @property PhoneMobileGatewayLu[] $phoneMobileGatewayLus
 * @property Contacts               $contact
 * @property Accounts               $account
 */
class Phones extends StmBaseActiveRecord
{

    const PHONE_TYPE_UNKNOWN = null;

    const PHONE_TYPE_CELL = 1;

    const PHONE_TYPE_WORK = 2;

    const PHONE_TYPE_HOME = 3;

    const PHONE_TYPE_FAX = 4;

    const PHONE_TYPE_OTHER = 5;

    const PHONE_OWNER_TYPE_SELF = null;

    const PHONE_OWNER_TYPE_HUSBAND = 1;

    const PHONE_OWNER_TYPE_WIFE = 2;

    const PHONE_OWNER_TYPE_OTHER = 3;

    public $remove = false;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Phones the static model class
     */
    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {

        return 'phones';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'phone, is_primary',
                'required'
            ),
            array(
                'account_id, contact_id, phone, is_primary',
                'required',
                'on' => 'import'
            ),
            array(
                'phone',
                'required',
                'on' => 'addFind'
            ),
            array(
                'phone',
                'validateDuplicate',
            ),
            array(
                'id, account_id, phone, contact_id, phone_type_ma, owner_ma, is_primary, is_deleted',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'phone',
                'length',
                'min' => 10
            ),
//            array(
//                'phone',
//                'length',
//                'max' => 10
//            ),
            array(
                'extension',
                'length',
                'max' => 50
            ),
            array(
                'contact_id, remove, phone',
                'safe'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, phone_id, account_id, contact_id, phone_type_ma, phone, owner_ma, extension, is_primary, is_deleted',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'mobileGatewayPhoneLus' => array(
                self::HAS_MANY,
                'MobileGatewayPhoneLu',
                'phone_id'
            ),
            'phoneMobileGatewayLus' => array(
                self::HAS_MANY,
                'PhoneMobileGatewayLu',
                'phone_id'
            ),
            'contact'               => array(
                self::BELONGS_TO,
                'Contacts',
                'contact_id'
            ),
            'company'               => array(
                self::BELONGS_TO,
                'Companies',
                'company_id'
            ),
            'callListPhones' => array(
                self::HAS_MANY,
                'CallListPhones',
                'phone_id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'phone_id'      => 'Phone',
            'account_id'    => 'Account',
            'contact_id'    => 'Contact',
            'phone_type_ma' => 'Phone Type Ma',
            'phone'         => 'Phone',
            'owner_ma'      => 'Owner Ma',
            'extension'     => 'Extension',
            'is_primary'    => 'Is Primary',
            'is_deleted'    => 'Is Deleted',
        );
    }

    /**
     * @return array behaviors for model attributes.
     */
    public function behaviors()
    {

        return array(
            'modelAttribute' => array(
                'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
            ),
        );
    }

    /**
     * Validate Duplicate Phone
     * NOTE: This is used in normal validation rules AND called again from beforeSave as the Contacts Edit action pre-validates before a new contact is added and this handles that scenario as well.
     * @param $attribute
     * @param $param
     *
     * @return bool
     */
    public function validateDuplicate($attribute, $param)
    {
        if($this->contact_id && !$this->company_id) {
            $idFieldName = 'contact_id';
        }
        elseif(!$this->contact_id && $this->company_id) {
            $idFieldName = 'company_id';
        }

        if ($this->isNewRecord) {

            if($this->contact_id || $this->company_id) {
                $phone = Phones::model()->byIgnoreAccountScope()->findByAttributes(array(
                        'phone'         =>  $this->phone,
                        $idFieldName    =>  $this->{$idFieldName},
                    ));

                if($phone) {
                    $this->addError('phone', 'Phone # '.$this->phone.' already exists.');
                    return false;
                }
            }
        }
        else {
            $criteria = new CDbCriteria();
            $criteria->addNotInCondition('id',array($this->id));
            $criteria->compare('phone', $this->phone);
            $criteria->compare($idFieldName, $this->$idFieldName);
            if($phone = Phones::model()->byIgnoreAccountScope()->find($criteria)) {
                $this->addError('phone', 'Phone # '.$this->phone.' already exists.');
                return false;
            }
        }

        return true;
    }

    /**
     * Beautifies this Phone instance's collection of phone number.
     *
     * @return string the beautified phone number.
     */
    public function getFriendlyPhone($phone = NULL)
    {
		if(is_null($phone)){
			if(empty($this->phone)){
				return NULL;
			}
			$phone = $this->phone;
		}

        return Yii::app()->format->formatPhone($phone);
    }

    public function scopes()
    {

        return array(
            'primary' => array(
                'condition' => 'is_primary = 1',
            ),
        );
    }

    public function getPhoneTypes()
    {

        return array(
            self::PHONE_TYPE_UNKNOWN => '',
            self::PHONE_TYPE_CELL    => 'Cell',
            self::PHONE_TYPE_WORK    => 'Work',
            self::PHONE_TYPE_HOME    => 'Home',
            self::PHONE_TYPE_FAX     => 'Fax',
            self::PHONE_TYPE_OTHER   => 'Other',
        );
    }

    public function getPhoneOwnerTypes()
    {

        return array(
            self::PHONE_OWNER_TYPE_SELF    => '',
            self::PHONE_OWNER_TYPE_HUSBAND => 'Husband',
            self::PHONE_OWNER_TYPE_WIFE    => 'Wife',
            self::PHONE_OWNER_TYPE_OTHER   => 'Other',
        );
    }

    /**
     * @param $phoneNumber
     *
     * @return $this
     */
    public function byPhoneNumber($phoneNumber)
    {

        $phoneNumber = Yii::app()->format->formatStripPhone($phoneNumber);

        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'phone = :phone',
                'params'    => array(':phone' => $phoneNumber),
            )
        );

        return $this;
    }

    public function byContactId($contactId)
    {

        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'contact_id = :contact_id',
                'params'    => array(':contact_id' => $contactId),
            )
        );

        return $this;
    }

    public function getAreaCodeInfo()
    {

        $areaCode = substr($this->phone, 0, 3);

        return AreaCodes::model()->byAreaCode($areaCode)->find();
    }

    /**
     * Get Phone By Id List And Phone Number
     *
     * Commonly used for the Dialer to determine what instance of the phone record we are referring to when calling outbound
     * @param int $phoneNumber
     * @param array $callListPhoneIds call_list_phone_ids
     * @param mixed $callSession model
     * @return array Database results
     */
    public function getPhoneFromDialerCallback($phoneNumber, $callListPhoneIds = array(), CallSessions $callSession)
    {
        // Format the phone number
        $phoneNumber = Yii::app()->format->formatStripPhone($phoneNumber);

        // find phone number match from the ids - Create 2 queries
        $query = Yii::app()->db->createCommand()
            ->select("p.*, c.id call_list_phone_id, c.task_id, c.status, c.updated, c.updated_by")
            ->from('phones p')
            ->join('call_list_phones c', 'c.phone_id = p.id')
            ->where("CONCAT('1', p.phone) = :phone", array(':phone' => $phoneNumber))
            ->andWhere(array('in', 'c.id', $callListPhoneIds))
            ->andWhere('c.call_list_id='.$callSession->call_list_id)
            ->andWhere("c.updated_by = :updated_by", array(':updated_by' => $callSession->contact_id))
            ->order('c.updated DESC');

        $result =  $query->queryAll();

        if(empty($result)) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Urgent: No results found for both queries. This will affect Answered showing up. Phone #: '.$phoneNumber.'. Ids: '.print_r($callListPhoneIds, true).' This is unexpected result. Needs immediate attention. Query Ignoring Soft Delete: '.PHP_EOL.$query->getText().PHP_EOL.'Params: '.print_r($query->params, true), CLogger::LEVEL_ERROR); //.PHP_EOL.'Query for Including Soft Deletes: '.$queryIncludeSoftDeleted->getText()

            // last ditch effort to find call list phone
            $lastDitchQuery = Yii::app()->db->createCommand()
                ->select("p.*, c.id call_list_phone_id, c.task_id, c.status, c.updated, c.updated_by")
                ->from('phones p')
                ->join('call_list_phones c', 'c.phone_id = p.id')
                ->where("CONCAT('1', p.phone) = :phone", array(':phone' => $phoneNumber))
                ->andWhere('c.call_list_id='.$callSession->call_list_id)
                ->order('c.updated DESC');

            $result = $lastDitchQuery->queryAll();

            // last ditch effort #2 to find call list phone, without updated params
            if(empty($result)) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Urgent: Last ditch query did NOT find any results. Phone #: '.$phoneNumber.'. Ids: '.print_r($callListPhoneIds, true).' This is unexpected result. Needs immediate attention. Query Ignoring Soft Delete: '.PHP_EOL.$lastDitchQuery->getText().PHP_EOL.'Params: '.print_r($lastDitchQuery->params, true), CLogger::LEVEL_ERROR);
            }
            else {

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Urgent: Last ditch query found '.count($result).' result(s). Phone #: '.$phoneNumber.'. Ids: '.print_r($callListPhoneIds, true).' This is unexpected result. Needs immediate attention. Query Ignoring Soft Delete: '.PHP_EOL.$lastDitchQuery->getText().PHP_EOL.'Params: '.print_r($lastDitchQuery->params, true).PHP_EOL.'Result Data: '.print_r($result, true).PHP_EOL.'If 1 result found and status is not Answered, updating now...', CLogger::LEVEL_ERROR);

                // if status not Answered, update it.
                if(count($result) == 1 && $result[0]['status'] !== 'Answered') {
                    Yii::app()->db->createCommand("UPDATE call_list_phones SET status='".CallListPhones::ANSWERED."', updated=NOW(), updated_by=".$callSession->contact_id.", is_deleted=0 WHERE id=".$result[0]['call_list_phone_id'])->execute();
                }
            }

        }
        if(count($result)>1) {
            // error!! should only be one. Need to investigate.
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: '.count($result).' matches found for Phones::model()->getPhoneFromDialerCallback. '.PHP_EOL.'Result Set: '.print_r($result, true).PHP_EOL.'Phone #: '.$phoneNumber.PHP_EOL.'Phone IDs#: '.print_r($callListPhoneIds, true).PHP_EOL.'First Query Exclude Soft Deleted: '.$query->getText() , CLogger::LEVEL_ERROR);

            // add logic for checking for which one is Ringing or Answered
            foreach($result as $callListPhone) {

                if(in_array($callListPhone['status'], array(CallListPhones::ANSWERED,CallListPhones::RINGING))) {
                    //@todo: if this continues to solve the problem, then add it to the query??? or make it a secondary???? or not...
                    // log that a Ringing/Answered was round and send all data.
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Answered or Ringing was found in Duplicate Call List Phones. See Data: '.print_r($result, true) , CLogger::LEVEL_ERROR);

                    $result[] = $callListPhone;

                    break;
                }
            }

            // see if the updated_by can be narrowed down to a single user per $callSession->contact_id.
            if(count($result) > 1) {
                $matchesKeyUpdatedBy = array();
                // loop through each result to see if any matches the current session user. Array key stored in $matchesKeyUpdatedBy
                foreach($result as $key => $row) {

                    if($row['updated_by'] == $callSession->contact_id) {
                        $matchesKeyUpdatedBy[] = $key;
                    }
                }
                if(count($matchesKeyUpdatedBy) == 1) {
                    $result = array($result[$matchesKeyUpdatedBy[0]]);
                }
            }
        }

        return $result;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('phone_id', $this->phone_id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('contact_id', $this->contact_id);
        $criteria->compare('phone_type_ma', $this->phone_type_ma);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('owner_ma', $this->owner_ma);
        $criteria->compare('extension', $this->extension, true);
        $criteria->compare('is_primary', $this->is_primary);
        $criteria->compare('is_deleted', $this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    protected function beforeValidate()
    {
        //Remove all non-numeric characters so we can verify we have at least 10 digits at validation time.
        $this->phone = Yii::app()->format->formatInteger($this->phone);

        return parent::beforeValidate();
    }

    protected function beforeSave()
    {

        $this->phone = Yii::app()->format->formatStripPhone($this->phone);
        if(!$this->validateDuplicate('phone', array())) {
            return false;
        }

        return parent::beforeSave();
    }

    protected function beforeDelete()
    {
        // log in the activity log that this number was deleted
        $activityLog = new ActivityLog();
        $activityLog->setAttributes(array(
                'component_type_id' => ComponentTypes::CONTACTS,
                'component_id' => $this->contact_id,
                'task_type_id' => TaskTypes::PHONE_RECORD_LOG,
                'activity_date' => new CDbExpression('NOW()'),
                'note' => 'Deleted Phone#: '.Yii::app()->format->formatPhone($this->phone),
            ));
        if(!$activityLog->save()) {
            Yii::log(__FILE__ . '(' . __LINE__ . '): Error saving Activity log for delete phone. Activity Log attributes:'.print_r($activityLog->attributes, true).' Phone Record: ' . print_r($this->attributes, true), CLogger::LEVEL_ERROR);
        }

        parent::beforeDelete();
    }

    public function delete() {
        // also delete any call_list_phones tied to this phone. This happens after the parent delete
        if(!$this->getIsNewRecord()) {
            if($callListPhones = $this->callListPhones) {
                foreach($callListPhones as $callListPhone) {
                    $callListPhone->delete();
                }
            }

            parent::delete();
        }
    }
}
