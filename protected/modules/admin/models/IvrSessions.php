<?php

/**
 * This is the model class for table "ivr_sessions".
 *
 * The followings are the available columns in table 'ivr_sessions':
 * @property string $id
 * @property string $ivr_extension_id
 * @property string $call_hunt_group_session_id
 * @property string $call_uuid
 * @property string $extension
 * @property string $status
 * @property string $added
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property IvrSessionVoicemailMessages[] $ivrSessionVoicemailMessages
 * @property IvrExtensions $ivrExtension
 */
class IvrSessions extends StmBaseActiveRecord
{
    const STATUS_ANSWERED = 'Answered';
    const STATUS_VALID_EXTENSION_ENTERED = 'Valid Extension Entered';
    const STATUS_INVALID_EXTENSION_ENTERED = 'Invalid Extension Entered';
    const STATUS_TRANSFER_IN_PROGRESS = 'Transfer In Progress';
    const STATUS_TRANSFERRED_TO_HUNT_GROUP = 'Transferred to Hunt Group';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return IvrSessions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ivr_sessions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_uuid, status, added', 'required'),
            array('ivr_extension_id, call_hunt_group_session_id, extension', 'length', 'max'=>10),
            array('call_uuid', 'length', 'max'=>36),
            array('status', 'length', 'max'=>25),
            array('updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, ivr_extension_id, call_hunt_group_session_id, call_uuid, extension, status, added, updated', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ivrSessionVoicemailMessages' => array(self::HAS_MANY, 'IvrSessionVoicemailMessages', 'ivr_session_id'),
            'ivrExtension' => array(self::BELONGS_TO, 'IvrExtensions', 'ivr_extension_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'ivr_extension_id' => 'Ivr Extension',
            'call_hunt_group_session_id' => 'Call Hunt Group Session',
            'call_uuid' => 'Call Uuid',
            'extension' => 'Extension',
            'status' => 'Status',
            'added' => 'Added',
            'updated' => 'Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('ivr_extension_id',$this->ivr_extension_id,true);
        $criteria->compare('call_hunt_group_session_id',$this->call_hunt_group_session_id,true);
        $criteria->compare('call_uuid',$this->call_uuid,true);
        $criteria->compare('extension',$this->extension,true);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}