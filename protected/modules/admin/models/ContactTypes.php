<?php

	/**
	 * This is the model class for table "contact_types".
	 *
	 * The followings are the available columns in table 'contact_types':
	 *
	 * @property integer         $contact_type_id
	 * @property integer         $account_id
	 * @property string          $name
     * @property integer         $follow_up_days
     * @property integer         $parent_id
	 * @property integer         $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property ContactTypeLu[] $contactTypeLus
	 * @property Accounts        $account
	 */
	class ContactTypes extends StmBaseActiveRecord {

		const COACH = 1;
		const SPHERE = 4;
		const CLIENT = 5;
		const PROSPECT = 6;
		const LENDER = 7;
		const VENDOR = 8;
		const TITLE_COMPANY = 9;
		const HOME_INSPECTOR = 10;
		const WDO_INSPECTOR = 11;

		// collection of Contact Types models
		public $data = array();
		// array of contact_type_id to pass to the multiSelect widget
		public $multiSelectData = array();

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ContactTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'contact_types';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
                array(
                    'account_id, name',
                    'required',
                ),
				array(
					'account_id, parent_id, follow_up_days, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'contact_type_id, account_id, name, parent_id, follow_up_days, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contactTypeLus' => array(
					self::HAS_MANY,
					'ContactTypeLu',
					'contact_type_id'
				),
				'account' => array(
					self::BELONGS_TO,
					'Accounts',
					'account_id'
				),
                'parent' => array(
                    self::BELONGS_TO,
                    'ContactTypes',
                    'parent_id'
                ),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'account_id' => 'Account',
				'name' => 'Name',
                'parent_id' => 'Parent Id',
				'is_deleted' => 'Is Deleted',
			);
		}

        public function scopes() {
            return array(
                'orderByName'=>array('order'=>'name ASC')
            );
        }

        public function exclude($ids) {
            if(!empty($ids)) {
                $this->getDbCriteria()->addNotInCondition('id',(array) $ids);
            }
            return $this;
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('name', $this->name, true);
            $criteria->compare('parent_id', $this->parent_id, true);
			$criteria->compare('is_deleted', $this->is_deleted);
            $criteria->order = 'name ASC';

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination' => array('pageSize'=>50),
			));
		}
	}
