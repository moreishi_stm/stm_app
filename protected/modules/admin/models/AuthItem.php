<?php

	/**
	 * This is the model class for table "AuthItem".
	 *
	 * The followings are the available columns in table 'AuthItem':
	 *
	 * @property string           $name
	 * @property integer          $type
	 * @property string           $description
	 * @property string           $bizrule
	 * @property string           $data
	 *
	 * The followings are the available model relations:
	 * @property AuthAssignment[] $authAssignments
	 * @property AuthItemChild[]  $authItemchildren
	 * @property AuthItemChild[]  $authItemchildren1
	 */
	class AuthItem extends StmBaseActiveRecord {

		const TYPE_ROLE_STRING = 'role';
		const TYPE_OPERATION_STRING = 'operation';
		const TYPE_TASK_STRING = 'task';

        const ROLE_OWNER = 'owner';
        const ROLE_AGENT = 'agent';
        const ROLE_LENDER = 'lender';

        const ROLE_RECRUIT_REFERRING_AGENT = 'recruitReferringAgent';
//        const ROLE_PRODUCTIVITY_AGENT = 'productivityAgent';

        /**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AuthItem the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'auth_item';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'name, type, label',
					'required'
				),
				array(
					'type',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 64
				),
				array(
					'description, bizrule, data',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'name, type, description, bizrule, data',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'authAssignments' => array(
					self::HAS_MANY,
					'AuthAssignment',
					'itemname'
				),
				'itemParent' => array(
					self::HAS_MANY,
					'AuthItemChild',
					'parent'
				),
				'itemChild' => array(
					self::HAS_MANY,
					'AuthItemChild',
					'child'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'name' => 'Name',
				'label' => 'Name',
				'type' => 'Type',
				'description' => 'Description',
				'bizrule' => 'Business Rule Logic',
				'data' => 'Data',
			);
		}

		/**
		 * Return the string equivalent of the auth item type
		 *
		 * @return null|string
		 */
		public function getTypeName() {

			$typeName = "";
			switch ($this->type) {
				case CAuthItem::TYPE_ROLE:
					$typeName = self::TYPE_ROLE_STRING;
					break;

				case CAuthItem::TYPE_TASK:
					$typeName = self::TYPE_TASK_STRING;
					break;

				case CAuthItem::TYPE_OPERATION:
					$typeName = self::TYPE_OPERATION_STRING;
					break;

				default:
					break;
			}

			return Yii::app()->format->formatProperCase($typeName);
		}

		/**
		 * Get plural formatted type name
		 *
		 * @return mixed
		 */
		public function getTypeNamePlural() {
			return Yii::app()->format->formatPluralize($this->getTypeName());
		}

		/**
		 * Returns the CAuthItem type id by examining the authItemString as defined by AuthItem::TYPE_ROLE_STRING
		 *
		 * @author Chris Willard <chriswillard.dev@gmail.com>
		 *
		 * @param $authItemString
		 *
		 * @return int|null
		 */
		public static function getAuthItemTypeFromString($authItemString) {
			$authItemType = null;
			switch ($authItemString) {
				case AuthItem::TYPE_ROLE_STRING:
					$authItemType = CAuthItem::TYPE_ROLE;
					break;

				case AuthItem::TYPE_OPERATION_STRING:
					$authItemType = CAuthItem::TYPE_OPERATION;
					break;

				case AuthItem::TYPE_TASK_STRING:
					$authItemType = CAuthItem::TYPE_TASK;
					break;
			}

			return $authItemType;
		}

		public function byParent($parent) {
			if (is_array($parent)) {
				$this->getDbCriteria()->addInCondition('name', $parent);
			} else {
				$this->getDbCriteria()->mergeWith(array(
						'with' => array('itemParent'),
						'together' => true,
						'condition' => 'parent = :parent',
						'params' => array(
							':parent' => $parent,
						),
					)
				);
			}

			return $this;
		}

		public function byChild($child) {
			$this->getDbCriteria()->mergeWith(array(
					'with' => array('itemChild'),
					'together' => true,
					'condition' => 'child = :child',
					'params' => array(
						':child' => $child,
					),
				)
			);

			return $this;
		}

		/**
		 * Filter auth items by one or more types
		 *
		 * @param $listCAuthItemTypes
		 *
		 * @return $this
		 */
		public function byTypes($listCAuthItemTypes) {
			if (!empty($listCAuthItemTypes) && $listCAuthItemTypes == "") {
				return $this;
			}

			if(!is_array($listCAuthItemTypes)) {
				$listCAuthItemTypes = array($listCAuthItemTypes);
			}

			$addInCriteria = new CDbCriteria;
			$addInCriteria->addInCondition('type', $listCAuthItemTypes);
			$this->getDbCriteria()->mergeWith($addInCriteria);

			return $this;
		}

		/**
		 * Filter auth items by one or more types of the auth_item_child relationship
		 *
		 * @param $listCAuthItemTypes
		 *
		 * @return $this
		 */
		public function byItemChildType($relationName, $childRelationName, $listCAuthItemTypes) {
			if (!is_array($listCAuthItemTypes)) {
				return $this;
			}

			$addInCriteria = new CDbCriteria;
			$addInCriteria->addInCondition('authItem.type', $listCAuthItemTypes);
			$addInCriteria->with = array(
				$relationName . '.' . $childRelationName => array(
					'alias' => 'authItem',
				)
			);
			$this->getDbCriteria()->mergeWith($addInCriteria);

			return $this;
		}

		/**
		 * Checks against the auth_item_child relationship and uses the second argument as the relationship for the auth_item_child
		 *
		 * @param $listCAuthItemTypes
		 *
		 * @return $this
		 */
		public function byChildTypes($listCAuthItemTypes) {
			return $this->byItemChildType('itemChild', 'itemParent', $listCAuthItemTypes);
		}

		/**
		 * Checks against the auth_item_child relationship and uses the second argument as the relationship for the auth_item_child
		 *
		 * @param $listCAuthItemTypes
		 *
		 * @return $this
		 */
		public function byParentTypes($listCAuthItemTypes) {
			return $this->byItemChildType('itemParent', 'itemChild', $listCAuthItemTypes);
		}

		// @todo: this is 99% identidal to byNotAssigned() ... see if this should be modified for reuse
		public function byAssigned($name) {
			$parents = AuthItemChild::model()->byParent($name)->findAll();

			$this->getDbCriteria()->mergeWith(array(
											  'with' => array('itemChild'),
											  'together' => true,
											  )
			);
			if ($parents) {
				$parentItems = array();
				foreach ($parents as $Parent) {
					array_push($parentItems, $Parent->child);
				}
				$this->getDbCriteria()->addInCondition('name', $parentItems);
			}

			return $this;
		}

		public function byNotAssigned($name) {
			$parents = AuthItemChild::model()->byParent($name)->findAll();

			$this->getDbCriteria()->mergeWith(array(
					'with' => array('itemChild'),
					'together' => true,
				)
			);
			if ($parents) {
				$parentItems = array();
				foreach ($parents as $Parent) {
					array_push($parentItems, $Parent->child);
				}
				$this->getDbCriteria()->addNotInCondition('name', $parentItems);

				$children = AuthItemChild::model()->byParent($parentItems)->findAll();

				if ($children) {
					$childItems = array();
					foreach ($children as $Child) {
						array_push($childItems, $Child->child);
					}
					$this->getDbCriteria()->addNotInCondition('name', $childItems);
				}
			}

			return $this;
		}

        public function byIn($field, $values) {
            $values = StmFormatter::formatToArray($values);

            $this->getDbCriteria()->addInCondition($field, $values);

            return $this;
        }

        public function byNotIn($field, $values) {
            $values = StmFormatter::formatToArray($values);

            $this->getDbCriteria()->addNotInCondition($field, $values);

            return $this;
        }

        public function findChild($role) {
			return AuthItemChild::model()->byParentAndChild($role, $this->name)->find();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('name', $this->name, true);
			$criteria->compare('label', $this->label, true);
            if($this->type || $this->type ==0) {
                $criteria->compare('type', $this->type);
            }
            else {
                $criteria->addInCondition('type', array(CAuthItem::TYPE_OPERATION, CAuthItem::TYPE_TASK));
            }
			$criteria->compare('description', $this->description, true);
			$criteria->compare('bizrule', $this->bizrule, true);
			$criteria->compare('data', $this->data, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=> 50,
                ),
			));
		}
	}