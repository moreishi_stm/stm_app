<?php

/**
 * This is the model class for table "cms_comments".
 *
 * The followings are the available columns in table 'cms_comments':
 * @property integer $id
 * @property integer $cms_content_id
 * @property integer $contact_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $comment
 * @property integer $parent_id
 * @property integer $approved
 * @property string $added
 * @property string $updated
 */
class CmsComment extends StmBaseActiveRecord
{
    const COMMENT_SOURCE_BLOG = 1;
    const COMMENT_SOURCE_LISTING_STORYBOARD = 2;
    const COMMENT_SOURCE_BUYER_STORYBOARD = 3;

    public $added = null;

    private $_name;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CmsComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cms_content_id, first_name, last_name, email, comment', 'required'),
            array('name', 'required', 'on'=>'addFromWidget'),
            array('name', 'validateFullName', 'on' => 'addFromWidget'),
            array('comment', 'validateSpamCheck'),
            array('cms_content_id, contact_id, parent_id, approved', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name', 'length', 'max'=>85),
			array('email', 'length', 'max'=>255),
			array('added, name', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cms_content_id, contact_id, first_name, last_name, email, comment, parent_id, approved, added, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'contact' => array(
                self::BELONGS_TO,
                'Contacts',
                'contact_id',
            ),
            'cmsContent' => array(
                self::BELONGS_TO,
                'CmsContents',
                'cms_content_id',
            ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cms_content_id' => 'Cms Content',
			'contact_id' => 'Contact',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'comment' => 'Comment',
			'parent_id' => 'Parent',
			'approved' => 'Approved',
			'added' => 'Added',
			'updated' => 'Updated',
		);
	}

    /**
     * @return array
     */
    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'added',
                'updateAttribute' => 'updated',
            )
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cms_content_id',$this->cms_content_id);
		$criteria->compare('contact_id',$this->contact_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function validateFullName($attribute, $params)
    {
        if (strpos($this->_name, ' ') == false) {
            $this->addError($attribute, 'Please provide First & Last Name.');
        }
    }

    public function validateSpamCheck($attribute, $params)
    {
        $spamTerms = array('href=', 'http://', '<a ', '</a>');
        foreach ($spamTerms as $spamTerm) {
            if (stripos($this->$attribute, $spamTerm) !== false) {
                $this->addError($attribute, 'Comment is invalid.');
                return;
            }
        }
    }

    public function setName($name) {
        $this->_name = trim($name);

        $nameParts = explode(' ', $this->_name);
        $this->first_name = $nameParts[0];
        $this->last_name = $nameParts[1];
    }

    public function getName() {
        $this->_name = "{$this->first_name}";
        $this->_name .= ($this->last_name)? " " : "";
        $this->_name .= "{$this->last_name}";
        return $this->_name;
    }

    protected function afterSave() {

//        if(in_array($this->cmsContent->type_ma, array(CmsContents::TYPE_BUYER_SB, CmsContents::TYPE_LISTING_SB, CmsContents::TYPE_BLOG))) {
//        }

        //@todo: notify listing storyboard assignments
        $this->emailAlertBlogComment();

        $ActivityLog = new ActivityLog;
        $ActivityLog->setAttributes(array('account_id'=>Yii::app()->user->account->id,
                                          'component_type_id'=>ComponentTypes::CONTACTS,
                                          'component_id'=>$this->contact_id,
                                          'task_type_id'=>TaskTypes::BLOG_COMMENT,
                                          'activity_date'=>new CDbExpression('NOW()'),
                                          'note'=>'Blog Comment was made: '.$this->comment,
                                          'lead_gen_type_ma'=>ActivityLog::LEAD_GEN_NONE_ID,
                                          'is_spoke_to'=>0,
                                          'asked_for_referral'=>0,
                                          'is_public'=>0,
                                          'is_action_plan'=>0,
            ));

        if(!$ActivityLog->save()) {
            //@todo: error message about saving model
        }

        return parent::afterSave();
    }

    protected function emailAlertBlogComment() {
        $message = new YiiMailMessage;
        $logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Our Logo">';

        if($Transaction = $this->cmsContent->getTransaction()) {
            $Assignments = $Transaction->assignments;
            $toEmail = array();
            if(!empty($Assignments)) {
                foreach($Assignments as $Assignment) {
                    $toEmail[$Assignment->contact->primaryEmail] = $Assignment->contact->fullName;
                }
            }
        } else {
            if($blogModeratorEmail = Yii::app()->user->settings->blog_comment_moderator_email) {
                $toEmail = $blogModeratorEmail;
            } else {
                if($ownerContacts = Contacts::model()->byAdmins(null,null,'owner')->findAll()) {
                    foreach($ownerContacts as $ownerContact) {
                        $toEmail[$ownerContact->primaryEmail] = $ownerContact->fullName;
                    }
                }
            }
        }

        $pageType = $this->cmsContent->getPageTypeName();

        $content = '<span style="font-size:16px;">Comment Added to '.$pageType;
        $content .= '<br /><b>Please review and touch base with contact as needed.</b>';
//        $content .= '<br /><br />Client Name: '.$Transaction->contact->fullName;
//        if($Transaction->component_type_id == ComponentTypes::SELLERS) {
//            $content .= '<br />Address: '.Yii::app()->format->formatAddress($Transaction->address, array('lineBreak'=>false));
//        }
        $contactUrl = 'http://www.'.Yii::app()->user->domain->name.'/admin/contacts/'.$this->contact->id;
        $content .= '<br /><br /><a href="'.$contactUrl.'">Click here to view Contact details</a>';
        $content .= '</span><br />'.$logoImage;

        $message->view = 'plain';
        $message->setBody(array(
                'toEmail' => $toEmail,
                'content' => $content,
            ), 'text/html'
        );
        $message->setSubject('New Comment Added to '.$pageType);

        $toEmail = (YII_DEBUG)? array(StmMailMessage::DEBUG_EMAIL_ADDRESS) : $toEmail ;
        foreach($toEmail as $email => $name) {
            $message->addTo($email, $name);
        }

        $message->addFrom('Do-Not-Reply@SeizeTheMarket.net', 'Storyboard Notification');
//        Yii::app()->mail->send($message);
        StmZendMail::sendYiiMailMessage($message);
        return;
    }
}