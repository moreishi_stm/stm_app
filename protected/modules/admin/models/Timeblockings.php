<?php

	/**
	 * This is the model class for table "timeblocking".
	 *
	 * The followings are the available columns in table 'timeblocking':
	 *
	 * @property integer $id
	 * @property integer $contact_id
	 * @property integer $timeblock_type_ma
	 * @property string  $start_time
	 * @property string  $end_time
	 * @property integer $is_deleted
	 */
	class Timeblockings extends StmBaseActiveRecord {

		const LEAD_GEN_TYPE_PHONE_ID = 1;
		const FOLLOW_UP_TYPE_ID = 2;
		const SHOWING_TYPE_ID = 3;
		const CONSULT_TYPE_ID = 4;
		const CLOSING_TYPE_ID = 5;
		const HOME_INSPECTION_TYPE_ID = 6;
		const OFFICE_WORK_ID = 7;
		const TRAINING_ID = 8;
		const TEAM_MEETING_ID = 9;
		const PERSONAL_ID = 10;
		const EAT_ID = 11;
		const DRIVE_TIME_ID = 12;
		const CMA_ID = 13;
		const LEAD_GEN_TYPE_EMAIL_ID = 14;
        const BUILDER_VISIT_ID = 15;
        const PLANNED_TIME_OFF = 16;
        const LEAD_GEN_PHONE_RECRUITING_ID = 17;
        const SPECIAL_PROJECTS_ID = 18;
        const CUSTOMER_SUPPORT_ID = 19;

		public $queryCount;
		public $startHour;
		public $startMinute;
		public $endHour;
		public $endMinute;
		public $minutes;
		public $timeRange;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Timeblockings the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'timeblocking';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id, timeblock_type_ma, assignment_type_id, startHour, startMinute, endHour, endMinute, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'contact_id, assignment_type_id, start_time, end_time, timeblock_type_ma, date',
					'required'
				),
				array(
					'minutes',
					'safe'
				),
				array(
					'start_time',
					'validateTimeRange'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, assignment_type_id, timeblock_type_ma, start_time, end_time, startHour, startMinute, endHour, endMinute, minutes, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array behaviors for model attributes.
		 */
		public function behaviors() {
			return array(
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
			);
		}

		public static function getTypes() {

			return (Yii::app()->controller->module->id == 'hq')?
                // HQ timeblock types
                array(
                    self::CUSTOMER_SUPPORT_ID => 'Customer Support',
                    self::LEAD_GEN_TYPE_PHONE_ID => 'Lead Gen (Phone)',
                    self::LEAD_GEN_TYPE_EMAIL_ID => 'Lead Gen (Email)',
                    self::FOLLOW_UP_TYPE_ID => 'Follow Up',
                    self::CONSULT_TYPE_ID => 'Live Demo',
                    self::OFFICE_WORK_ID => 'Office Work',
                    self::TRAINING_ID => 'Training',
                    self::TEAM_MEETING_ID => 'Team Meeting',
                    self::PERSONAL_ID => 'Personal',
                    self::EAT_ID => 'Lunch/Dinner',
                    self::DRIVE_TIME_ID => 'Drive Time',
                    self::PLANNED_TIME_OFF => 'Planned Time Off',
                    self::SPECIAL_PROJECTS_ID => 'Special Projects',
                )
                // normal application timeblock types
                : array(
				self::LEAD_GEN_TYPE_PHONE_ID => 'Lead Gen (Phone)',
				self::LEAD_GEN_TYPE_EMAIL_ID => 'Lead Gen (Email)',
				self::FOLLOW_UP_TYPE_ID => 'Follow Up',
				self::SHOWING_TYPE_ID => 'Showing',
				self::CONSULT_TYPE_ID => 'Initial Consultation',
				self::CLOSING_TYPE_ID => 'Closing',
				self::HOME_INSPECTION_TYPE_ID => 'Home Inspection',
				self::OFFICE_WORK_ID => 'Office Work',
				self::TRAINING_ID => 'Training',
				self::TEAM_MEETING_ID => 'Team Meeting',
				self::PERSONAL_ID => 'Personal',
				self::EAT_ID => 'Lunch/Dinner',
				self::DRIVE_TIME_ID => 'Drive Time',
				self::CMA_ID => 'CMA',
                self::BUILDER_VISIT_ID => 'Builder Visit',
                self::PLANNED_TIME_OFF => 'Planned Time Off',
                self::SPECIAL_PROJECTS_ID => 'Special Projects',
                self::LEAD_GEN_PHONE_RECRUITING_ID => 'Lead Gen Recruiting (Phone)',
			);
		}

		public function dateRange($opts = null) {

			if ($opts == null) {
				$this->getDbCriteria()->mergeWith(array(
						'condition' => 'date <= :date',
						'params' => array(':date' => date('Y-m-d')),
					)
				);
			} else {
				if ($opts['to_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => 'date <= :to_date',
							'params' => array(':to_date' => $opts['to_date']),
						)
					);
				}

				if ($opts['from_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => 'date >= :from_date',
							'params' => array(':from_date' => $opts['from_date']),
						)
					);
				}
			}

			return $this;
		}

		public function byTypes($types) {

			if(!is_array($types))
				$types = array($types);

			$this->getDbCriteria()->addInCondition('timeblock_type_ma', $types);

			return $this;
		}

		public function calculateMinutes() {
			$this->getDbCriteria()->mergeWith(array('select' => '*, sum(time_to_sec(timediff(end_time,start_time)) / 3600)  as minutes'));

			return $this;
		}

		public function getType() {
			$types = self::getTypes();
			if (isset($this->timeblock_type_ma) && isset($types[$this->timeblock_type_ma])) {
				return $types[$this->timeblock_type_ma];
			}

			return null;
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array();
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'timeblock_type_ma' => 'Timeblock Type',
				'date' => 'Date',
				'start_time' => 'Start Time',
				'end_time' => 'End Time',
				'assignment_type_id' => 'Assignment Type',
				'is_deleted' => 'Is Deleted',
			);
		}

		public function scopes() {
			return array(
				'byCurrentUser' => array(
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => Yii::app()->user->contact->id),
				),
				'byCurrentMonth' => array(
					'condition' => 'MONTH(date) = MONTH(CURRENT_DATE) AND YEAR(date) = YEAR(CURRENT_DATE)',
				),
				'asc' => array(
					'order' => 'date asc',
				),
				'desc' => array(
					'order' => 'date desc',
				),
			);
		}

		public function byContactId($contactId) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => $contactId),
				)
			);

			return $this;
		}

		public function byDate($opts) {
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$criteria = new CDbCriteria;
			$criteria->condition = 'date >=:added_from AND date <=:added_to';
			$criteria->params = array(
				":added_from" => $opts['from_date'],
				":added_to" => $opts['to_date'],
			);

			$this->getDbCriteria()->mergeWith($criteria);

			return $this;
		}

		public function getHours() {
			return (strtotime($this->end_time) - strtotime($this->start_time)) / 60 / 60;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('timeblock_type_ma', $this->timeblock_type_ma);
			$criteria->compare('start_time', $this->start_time, true);
			$criteria->compare('end_time', $this->end_time, true);
			$criteria->compare('assignment_type_id', $this->timeblock_type_ma);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		protected function beforeValidate() {

			if (!$this->contact_id) {
				$this->contact_id = Yii::app()->user->contact->id;
			}

			//@todo: can't use dateformat cuz it checks for zero and so midnight time fails, dunnow what sanity current formatter checks for the zero so need to make options to pass to bypass zero.
			$startTime = $this->getActualStartTime();
			$endTime = $this->getActualEndTime();
			if (($this->startHour != '') && ($this->startMinute != '')) {
				$this->start_time = date(StmFormatter::MYSQL_TIME_FORMAT, $startTime);
			}

			if ($this->endHour && !empty($this->endMinute)) {
				$this->end_time = date(StmFormatter::MYSQL_TIME_FORMAT, $endTime);
			}

			if($this->start_time && $this->end_time) {
				$this->timeRange['start_time'] = $this->start_time;
				$this->timeRange['end_time'] = $this->end_time;
			}

			return parent::beforeValidate();
		}

		public function getActualStartTime() {
			return $this->getActualTime($this->startHour, $this->startMinute);
		}

		public function getActualEndTime() {
			return $this->getActualTime($this->endHour, $this->endMinute);
		}

		private function getActualTime($strHour, $strMinute, $strSeconds='00') {
			$strHour = sprintf("%02d", $strHour);
			$strMinute = sprintf("%02d", $strMinute);
			$formattedTime = strtotime($strHour.':'.$strMinute.':'.$strSeconds);
			return $formattedTime;
		}

		protected function beforeSave() {

			// Date Conversions
			$this->date = Yii::app()->format->formatDate($this->date, StmFormatter::MYSQL_DATE_FORMAT);
			$this->start_time = Yii::app()->format->formatDate($this->start_time, StmFormatter::MYSQL_TIME_FORMAT);
			$this->end_time = Yii::app()->format->formatDate($this->end_time, StmFormatter::MYSQL_TIME_FORMAT);

			return parent::beforeSave();
		}

		public function validateTimeRange($attribute, $params) {
			return true;
			$startTime = $this->getActualStartTime();
			$endTime = $this->getActualEndTime();
			if ($startTime > $endTime) {
				$this->addError($attribute, 'Start Time cannot be greater than End Time.');
			}
			return true;
		}

        public static function getMissingDays($contactId, $startDate=null, $endDate=null)
        {
            if(!$contactId) {
                 throw new Exception(__CLASS__.'(:'.__LINE__.') ContactId is missing. Stop everything.');
            }

            if(!$startDate) {
                // check settings
                if(Yii::app()->user->settings->timeblocks_past_due_redirect && Yii::app()->user->settings->timeblocks_past_due_redirect !=='0') {
                    $timeblockStartDateSetting = Yii::app()->user->settings->timeblocks_past_due_redirect_start_date;
                }
                $startDate = (!empty($timeblockStartDateSetting)) ? date('Y-m-d', strtotime($timeblockStartDateSetting)) : date('Y-01-01');
            }

            if(!$endDate) {
                $endDate = date('Y-m-d');
            }

            $yearMonthWeekdays = StmFunctions::getWeekdayDates($startDate, $endDate);

            $exitingRecordsDates = Yii::app()->db->createCommand("SELECT DISTINCT date from timeblocking WHERE contact_id={$contactId} AND date >='".$startDate."' AND date <='".$endDate."' AND is_deleted=0 ORDER BY date ASC")->queryColumn();

            $missingDates = array_diff($yearMonthWeekdays, $exitingRecordsDates);

            // get all weekdays for the month and see which days are missing
            return $missingDates;
        }
	}