<?php

/**
 * This is the model class for table "audio_operation_manual_tag_lu".
 *
 * The followings are the available columns in table 'audio_operation_manual_tag_lu':
 * @property string $audio_file_id
 * @property integer $operation_manual_tag_id
 * @property integer $added_by
 * @property string $added
 */
class AudioOperationManualTagLu extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AudioOperationManualTagLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'audio_operation_manual_tag_lu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('audio_file_id, operation_manual_tag_id', 'required'),
            array('operation_manual_tag_id', 'numerical', 'integerOnly'=>true),
            array('audio_file_id', 'length', 'max'=>11),
            array('added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('audio_file_id, operation_manual_tag_id, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'audio_file_id' => 'Audio File',
            'operation_manual_tag_id' => 'Operation Manual Tag',
            'added' => 'Added',
        );
    }

    protected function beforeValidate()
    {
        if($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        return parent::beforeValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('audio_file_id',$this->audio_file_id,true);
        $criteria->compare('operation_manual_tag_id',$this->operation_manual_tag_id);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}