<?php

/**
 * This is the model class for table "recruit_production_status".
 *
 * The followings are the available columns in table 'recruit_production_status':
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $override_name
 * @property integer $sort_order
 * @property string $added
 * @property string $updated
 * @property integer $added_by
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Kwlegacypikesville.contacts $addedBy
 * @property Kwlegacypikesville.contacts $updatedBy
 */
class RecruitProductionStatuses extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RecruitProductionStatuses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recruit_production_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, added, added_by', 'required'),
			array('sort_order, added_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name, override_name', 'length', 'max'=>255),
			array('short_name', 'length', 'max'=>100),
			array('updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, short_name, override_name, sort_order, added, updated, added_by, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addedBy' => array(self::BELONGS_TO, 'Kwlegacypikesville.contacts', 'added_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Kwlegacypikesville.contacts', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'short_name' => 'Short Name',
			'override_name' => 'Override Name',
			'sort_order' => 'Sort Order',
			'added' => 'Added',
			'updated' => 'Updated',
			'added_by' => 'Added By',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('short_name',$this->short_name,true);
		$criteria->compare('override_name',$this->override_name,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}