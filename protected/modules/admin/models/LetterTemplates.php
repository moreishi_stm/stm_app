<?php

/**
 * This is the model class for table "letter_templates".
 *
 * The following are the available columns in table 'letter_templates':
 * @property string $id
 * @property integer $status_ma
 * @property integer $component_type_id
 * @property string $description
 * @property string $name
 * @property string $body
 * @property integer $is_deleted
 * @property string $updated
 * @property integer $updated_by
 * @property string $added
 * @property integer $added_by
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 * @property ComponentTypes $componentType
 * @property Contacts $updatedBy
 */
class LetterTemplates extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LetterTemplates the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'letter_templates';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('component_type_id, status_ma, is_deleted, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('description', 'length', 'max'=>250),
            array('name', 'length', 'max'=>128),
            array('body, updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, status_ma, component_type_id, description, name, body, is_deleted, updated, updated_by, added, added_by', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'componentType' => array(self::BELONGS_TO, 'ComponentTypes', 'component_type_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'component_type_id' => 'Component Type',
            'description' => 'Description',
            'name' => 'Name',
            'body' => 'Body',
            'is_deleted' => 'Is Deleted',
            'updated' => 'Updated',
            'updated_by' => 'Updated By',
            'added' => 'Added',
            'added_by' => 'Added By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('component_type_id',$this->component_type_id);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('body',$this->body,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('added_by',$this->added_by);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }

	public static function getByComponentTypeId($id) {
		return LetterTemplates::model()->findAll(array(
				'condition' => 'component_type_id=:component_type_id',
				'params' => array(':component_type_id' => $id),
				'order'=>'name ASC',
			)
		);
	}

	public function getDropDownDisplay() {
		return "$this->name | Desc: $this->description (ID# $this->id)";
	}

	/**
	 * filterBodyByContactData
	 *
	 * @param Contacts $contact
	 *
	 * @return mixed|null|string Body content filtered by the map provided
	 */
	public function filterBodyByContactData(Contacts $contact, $componentModel = null) {

		// If we do not have any content then we can't filter it
		if (!$this->body) {
			return null;
		}

		$currentUser = Contacts::model()->findByPk(Yii::app()->user->id);
		//die("<pre>".print_r($currentUser->attributes,1));
		// Maps a replacement name to the contact public property
		$contactTplMap = array(
			"{{first_name}}" => $contact->first_name,
			"{{last_name}}" => $contact->last_name,
			"{{appointment_date}}" => "",
			"{{seller_address}}"    => "",
			"{{seller_city}}" => "",
			"{{seller_state}}" => "",
			"{{seller_zip}}" => "",
			"{{sender_first_name}}" => $currentUser->first_name,
			"{{sender_last_name}}" => $currentUser->last_name,
			"{{sender_email}}" => $currentUser->primaryEmail,
			"{{sender_phone}}" => Phones::model()->getFriendlyPhone($currentUser->getPrimaryPhone()),
			"{{recruit_units}}" => "",
			"{{recruit_volume}}" => "",
			"{{recruit_volume_change}}" => "",
			"{{recruit_volume_change_percent}}" => ""
		);

		if($componentModel && get_class($componentModel) == 'Recruits') {
			$contactTplMap['{{recruit_units}}'] = $componentModel->annual_units;
			$contactTplMap['{{recruit_volume}}'] = Yii::app()->format->formatDollars($componentModel->annual_volume);
			$contactTplMap['{{recruit_volume_change}}'] = Yii::app()->format->formatDollars($componentModel->annual_volume_change);
			$contactTplMap['{{recruit_volume_change_percent}}'] = $componentModel->annual_volume_change_percent.'%';
		}

		// Add in appointment data if we have any
		/** @var Appointments $appointment */
		try {
			$appointment = $componentModel->appointment;
			if($appointment) {
				$contactTplMap['{{appointment_date}}'] = date("m/d/Y g:i a", strtotime($appointment->set_for_datetime));
			}
		}
		catch(Exception $e) {

		}

		// If we have a seller address, add values for that
		try {
			$address = $componentModel->address;
			if($address) {
				$contactTplMap['{{seller_address}}'] = $address->address;
				$contactTplMap['{{seller_city}}'] = $address->city;
				$contactTplMap['{{seller_state}}'] = $address->state->name;
				$contactTplMap['{{seller_zip}}'] = $address->zip;
			}
		}
		catch(Exception $e) {

		}

		// Build out the arrays the str_replace function needs based off a easier to read/understand map
		$tplSearchMap = $replacementMap = array();
		foreach ($contactTplMap as $tplVarName => $contactAttr) {
			array_push($tplSearchMap, $tplVarName);
			array_push($replacementMap, $contactAttr);
		}

		// Replace the template data with the contact data
		$replacementContent = str_replace($tplSearchMap, $replacementMap, $this->body, $count);

		// Sanity check and make sure replacements actually occurred
		if (!$count) {
			$replacementContent = $this->clearTemplateTags();
		}

		return $replacementContent;
	}
}