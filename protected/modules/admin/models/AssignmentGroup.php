<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
abstract class AssignmentGroup extends CModel
{

    /** @var array $assigneeGroup contains all of the assignments that will need to be processed */
    public $assigneeGroup = array();

    /** @var Component $_assignedToComponent */
    private $_assignedToComponent;

    /** @var string $_assignmentAssigneeComponentColumn the name of the assignee column */
    private $_assignmentAssigneeComponentColumn;

    /**
     * @param Component $assignedToComponent
     */
    public function __construct(Component $assignedToComponent)
    {

        $this->setAssignmentAssigneeComponentColumn($this->assignmentAssigneeComponentColumn());
        $this->setAssignedToComponent($assignedToComponent);
    }

    /**
     * Child groups must provide the component column name for the assignee
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    abstract protected function assignmentAssigneeComponentColumn();

    public function attributeNames()
    {

        return array('assigneeGroup');
    }

    public function rules()
    {

        return array(
            array('assigneeGroup', 'safe'),
        );
    }

    /**
     * Processes the assignments within the child group
     *
     * @throws CHttpException
     */
    public function update()
    {

        $previouslyAssigned   = $this->getPreviouslyAssigned();
        $assignmentsProcessed = array();

        foreach ($this->getAssignmentCollection() as $assignment) {
            if (!$assignment instanceof Assignments) {
                throw new CHttpException(500, 'Cannot process an assignment group without the proper interface.');
            }

            array_push(
                $assignmentsProcessed, $assignment
            );

            // we do not need to add a assignment if the record already exists
            if ($this->assignmentAlreadyExists($assignment)) {
                continue;
            }

            $assignedToComponentTuple = $this->_assignedToComponent->asComponentTuple();
            $assignment->component_type_id = $assignedToComponentTuple['componentTypeId'];
            $assignment->component_id      = $assignedToComponentTuple['componentId'];
            $assignment->save();
        }

        // cleanup any assignments that have been removed from the gui
        if (is_array($previouslyAssigned)) {

            /** @var Assignments $prevAssignment */
            foreach ($previouslyAssigned as $previousAssignment) {
                if ($this->checkForRemoval($previousAssignment, $assignmentsProcessed)) {
                    $previousAssignment->delete();
                }
            }
        }
    }

    /**
     * Retrieve the assignment collection for processing
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return array
     */
    protected function getAssignmentCollection()
    {

        $assignmentCollection = array();
        foreach ($this->assigneeGroup as $assigneeId) {
            $assignee                                                    = new Assignee($this, $assigneeId);
            $assignment                                                  = new Assignments;
            $assignment->assignment_type_id                              = $assignee->getAssignmentTypeId();
            $assignment->{$this->getAssignmentAssigneeComponentColumn()} = $assignee->getAssigneeId();
            array_push($assignmentCollection, $assignment);
        }

        return $assignmentCollection;
    }

    /**
     * Checks if the assignment already exists in the database (prevents multiple additions)
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param Assignments $assignmentToValidate
     *
     * @return bool
     */
    protected function assignmentAlreadyExists(Assignments $assignmentToValidate)
    {

        $assignmentExists = true;

        $previousAssignments = $this->getPreviouslyAssigned();
        if (empty($previousAssignments)) {
            return !$assignmentExists;
        }

        foreach ($previousAssignments as $previousAssignment) {
            if ($previousAssignment->assignment_type_id == $assignmentToValidate->assignment_type_id and
                $previousAssignment->{$this->getAssignmentAssigneeComponentColumn()}
                    == $assignmentToValidate->{$this->getAssignmentAssigneeComponentColumn()}
            ) {
                return $assignmentExists;
            }
        }

        return !$assignmentExists;
    }

    /**
     * Checks to see if a entry has been removed from a previously saved assignment
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param Assignments $savedAssignment
     * @param             $assignmentsProcessed
     *
     * @return bool
     */
    private function checkForRemoval(Assignments $savedAssignment, $assignmentsProcessed)
    {

        $removeAssignment = true;
        if (empty($assignmentsProcessed)) {
            return $removeAssignment;
        }

        /** @var Assignee $assignee */
        foreach ($assignmentsProcessed as $assignment) {
            if (!$assignment instanceof Assignments) {
                continue;
            }

            // the record was processed and still exists, so do not remove
            if ($savedAssignment->assignment_type_id == $assignment->assignment_type_id and
                $savedAssignment->{$this->getAssignmentAssigneeComponentColumn()}
                    == $assignment->{$this->getAssignmentAssigneeComponentColumn()}
            ) {
                $removeAssignment = !$removeAssignment;
                break;
            }
        }

        return $removeAssignment;
    }

    /**
     * Provides a assignment assignee tuple, used to normalize the assignee component column
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return array
     */
    public function getPreviouslyAssignedAsTupleCollection()
    {

        $previousAssignmentTupleCollection = array();

        $previousAssignments = $this->getPreviouslyAssigned();
        if (empty($previousAssignments)) {
            return $previousAssignmentTupleCollection;
        }

        foreach ($previousAssignments as $previousAssignment) {
            array_push(
                $previousAssignmentTupleCollection, array(
                    'assigneeComponentId' => $previousAssignment->{$this->getAssignmentAssigneeComponentColumn()},
                    'assignmentTypeId'    => $previousAssignment->assignment_type_id,
                )
            );
        }

        return $previousAssignmentTupleCollection;
    }

    /**
     * Retrieves a list of all previous assignments based on the assignment type
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return array
     */
    protected function getPreviouslyAssigned()
    {

        $assignedToComponentTuple = $this->getAssignedToComponent()->asComponentTuple();
        $previouslyAssigned = Assignments::model()->findAllByAttributes(
            array(
                'component_type_id'  => $assignedToComponentTuple['componentTypeId'],
                'component_id'       => $assignedToComponentTuple['componentId'],
                'assignment_type_id' => $this->getAssignmentTypeIds(),
            )
        );

        return $previouslyAssigned;
    }

    /**
     * @return string
     * @throws CHttpException
     */
    protected function getAssignmentAssigneeComponentColumn()
    {

        if (empty($this->_assignmentAssigneeComponentColumn)) {
            throw new CHttpException(500, 'Cannot access the assignee component column.');
        }

        return $this->_assignmentAssigneeComponentColumn;
    }

    /**
     * Sets and validates the assignee component column
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $columnName
     *
     * @throws CHttpException
     */
    protected function setAssignmentAssigneeComponentColumn($columnName)
    {

        $validColumnNames = array('assignee_company_id', 'assignee_contact_id');

        if (in_array($columnName, $validColumnNames)) {
            $this->_assignmentAssigneeComponentColumn = $columnName;
        } else {
            throw new CHttpException(500, "Invalid assignment assignee component column: $columnName");
        }
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return Component
     */
    public function getAssignedToComponent()
    {

        return $this->_assignedToComponent;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param Component $component
     */
    public function setAssignedToComponent(Component $component)
    {

        $this->_assignedToComponent = $component;
    }

    /**
     * Return a collection of valid assignment types for a child group
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    abstract public function getAssignmentTypeIds();
}


/**
 * Standarizes the way we interpret assignee parameters
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class Assignee
{

    /** @var int the assignment type id processed */
    private $_assignment_type_id;

    /** @var int the assignee id processed */
    private $_assignee_id;

    /** @var AssignmentGroup the group attempted to utilize the tuple */
    private $_assignmentGroup;

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param AssignmentGroup $assignmentGroup
     * @param                 $assigneeId
     */
    public function __construct(AssignmentGroup $assignmentGroup, $assigneeId)
    {

        $this->_assignmentGroup = $assignmentGroup;
        $this->processAssigneeId($assigneeId);
    }

    /**
     * @todo   this method exists to parse the format of "{assignment_type_id}-{assignee_id}" that the CompanyContactsAction is using
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $assigneeId
     */
    private function processAssigneeId($assigneeId)
    {

        $assignmentTypeId = null;
        if (strpos($assigneeId, '-') !== false) {
            $assignmentParts = explode('-', $assigneeId);
            list($assignmentTypeId, $assigneeId) = $assignmentParts;
        } else if (count($this->_assignmentGroup->getAssignmentTypeIds()) == 1) {
            $assignmentTypeId = current($this->_assignmentGroup->getAssignmentTypeIds());
        }

        $this->_assignment_type_id = $assignmentTypeId;
        $this->_assignee_id        = $assigneeId;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return int
     */
    public function getAssignmentTypeId()
    {

        return $this->_assignment_type_id;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return int
     */
    public function getAssigneeId()
    {

        return $this->_assignee_id;
    }
}


/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class AssignmentGroupHtmlHelper
{

    /**
     * Retrieves the option value of an assignment group based on the number of assignment types
     *
     * @todo this method exists to parse the format of "{assignment_type_id}-{assignee_id}" that the CompanyContactsAction is using
     *
     * @param AssignmentGroup $assignmentGroup
     * @param                 $assignmentTuple
     *
     * @return string
     */
    public static function getOptionIdByAssignmentGroup(AssignmentGroup $assignmentGroup, $assignmentTuple)
    {

        $optionId = $assignmentTuple['assigneeComponentId'];

        if (count($assignmentGroup->getAssignmentTypeIds()) > 1) {
            $optionId = $assignmentTuple['assignmentTypeId'] . '-' . $optionId;
        }

        return $optionId;
    }
}
