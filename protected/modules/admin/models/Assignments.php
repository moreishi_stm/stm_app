<?php

	/**
	 * This is the model class for table "assignments".
	 *
	 * The followings are the available columns in table 'assignments':
	 *
	 * @property integer          $id
	 * @property integer          $component_type_id
	 * @property integer          $component_id
	 * @property integer          $assignment_type_id
	 * @property integer          $assignee_contact_id
	 * @property integer          $assignee_company_id
     * @property integer          $is_protected
	 *
	 * The followings are the available model relations:
	 * @property TransactionTypes $transactionType
	 * @property Contacts         $contact
	 * @property AssignmentTypes  $assignmentType
	 */
	class Assignments extends StmBaseActiveRecord {

		// Flag to remove the record during processing
		public $remove = false;
		public $logChange = false;
        public $alertChange = true;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Assignments the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'assignments';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'assignee_contact_id',
					'required',
					'on' => 'addTransaction'
				),
				array(
					'component_type_id, component_id, assignment_type_id',
					'required'
				),
				array(
					'component_type_id, component_id, assignment_type_id, assignee_company_id, assignee_contact_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'remove, id, is_protected',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, component_type_id, component_id, assignment_type_id, assignee_company_id, assignee_contact_id, is_deleted, is_protected',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'assignee_contact_id',
					'alias' => 't'
				),
				'company' => array(
					self::BELONGS_TO,
					'Companies',
					'assignee_company_id'
				),
				'assignmentType' => array(
					self::BELONGS_TO,
					'AssignmentTypes',
					'assignment_type_id'
				),
			);
		}

		protected function beforeSave()
        {
            //@todo: what if 2 assigments are flipping roles, one saving before the other will flag this ... grrr
            // prevent duplicate saves
//            if ($this->isNewRecord && ($this->assignee_contact_id ||  $this->assignee_company_id)) {
//                $criteria = new CDbCriteria();
//                $criteria->addCondition('id != '.$this->id);
//                $criteria->compare('component_type_id', $this->component_type_id);
//                $criteria->compare('component_id', $this->component_id);
//                $criteria->compare('assignment_type_id', $this->assignment_type_id);
//                if($this->assignee_contact_id) {
//                    $criteria->compare('assignee_contact_id', $this->assignee_contact_id);
//                }
//                elseif($this->assignee_company_id) {
//                    $criteria->compare('assignee_company_id', $this->assignee_company_id);
//                }
//
//                // check for existing duplicate
//                if(Assignments::model()->count($criteria)) {
//                    // don't save and
//                    return true;
//                }
//            }

            $changeData = $this->isChangedRecord();
			if (!empty($changeData)) {

                foreach($changeData['fields'] as $field) {
                    $this->logChange($changeData['originalModel'], $field);
                }
			}
			$this->updated = date(StmFormatter::MYSQL_DATETIME_FORMAT);
            if(!$this->updated_by) {
                $this->updated_by = Yii::app()->user->id;
            }

			return parent::beforeSave();
		}

		public function isChangedRecord() {
			if (!$this->isNewRecord) {
                // elements are "originalModel", and "type"
                $changedFields = array();

				$originalData = Assignments::model()->findByPk($this->id);

				if ($originalData->assignee_contact_id != $this->assignee_contact_id) {
                    $changedFields['originalModel'] = $originalData;
                    $changedFields['fields'][] = 'assignee_contact_id';
				}

                if ($originalData->is_protected != $this->is_protected) {
                    $changedFields['originalModel'] = $originalData;
                    $changedFields['fields'][] = 'is_protected';
                }
			}

			return $changedFields;
		}

        /**
         * Handles logging of multiple columns
         * @param $OriginalAssignment
         * @param $field
         */
        public function logChange($OriginalAssignment, $field) {

			$ActivityLog = new ActivityLog;

            switch($field) {
                case 'assignee_contact_id':
                    $ActivityLog->note = 'Assignment change from <strong><u>:assigned_from_contact</u></strong> to <strong><u>:assigned_to_contact</u></strong>';
                    // Translate the note string
                    $ActivityLog->note = strtr($ActivityLog->note, array(
                            ':assigned_to_contact' => $this->contact->fullName,
                            ':assigned_from_contact' => $OriginalAssignment->contact->fullName,
                        )
                    );
                    $ActivityLog->task_type_id = TaskTypes::TRANSFER;
                    if($this->scenario == 'massUpdate') {
                        $ActivityLog->note .= ' via mass administrative update.';
                        $ActivityLog->task_type_id = TaskTypes::MASS_ASSIGNMENT_TRANSFER;
                    }
                    break;

                case 'is_protected':
                    $ActivityLog->note = 'Assignment protection updated to '.(($this->is_protected)? 'Protected' : 'Normal');
                    $ActivityLog->task_type_id = TaskTypes::PROTECTED_ASSIGNMENT;
                    break;

                default:
                    return;
            }



			$ActivityLog->account_id = Yii::app()->user->accountId;
			$ActivityLog->component_type_id = $this->component_type_id;
			$ActivityLog->component_id = $this->component_id;
			$ActivityLog->added_by = Yii::app()->user->id;
			$ActivityLog->activity_date = date('Y-m-d H:i:s');
            $ActivityLog->save();

            $AssignmentLog = new AssignmentLog;
			$AssignmentLog->component_type_id = $this->component_type_id;
			$AssignmentLog->component_id = $this->component_id;
			$AssignmentLog->old_assignment_type_id = $OriginalAssignment->assignment_type_id;
			$AssignmentLog->new_assignment_type_id = $this->assignment_type_id;
			$AssignmentLog->old_assignee_contact_id = $OriginalAssignment->assignee_contact_id;
			$AssignmentLog->new_assignee_contact_id = $this->assignee_contact_id;
			$AssignmentLog->added_by = Yii::app()->user->id;
			$AssignmentLog->added = date('Y-m-d H:i:s');
			$AssignmentLog->save();

            if($this->alertChange) {
                $this->alertAssignmentChanges($OriginalAssignment, $ActivityLog->note);
            }

			return;
		}

		public function alertAssignmentChanges($AssignmentOld, $note) {

            if(AuthAssignment::model()->findByAttributes(array('itemname'=>'inactive','userid'=>$AssignmentOld->contact->id))) {
                return;
            }

            $content = '<h2>Seize the Market Assignment Change Notification</h2>';
            $nowDateTime = Yii::app()->db->createCommand("SELECT NOW()")->queryScalar();
            $content .= 'Date/Time: ' . date('m/d/Y g:ia', strtotime($nowDateTime)).'<br>';
			$content .= $note . '<br />';

            $content .= $this->componentType->singularName . ' Name: ' . $this->componentType->getContact($this->component_id)->fullName . ' (ID: '.$this->component_id.')<br /><br />';
            $content .= '<a href="http://www.'.Yii::app()->user->getPrimaryDomain()->name.'/admin/'.$this->componentType->name.'/'.$this->component_id.'">Click here to View the '.$this->componentType->singularName.'</a><br>';
			if ($toEmail = $AssignmentOld->contact->primaryEmail) {
				if (!YII_DEBUG) {
                    StmZendMail::easyMail('do-not-reply@seizethemarket.net', $toEmail, 'Assignment Update Notification', $content, $bcc=null, $type='text/html', true, true, null, false);
				}
			}

            if (($contact = Contacts::model()->findByPk($this->assignee_contact_id)) && ($toEmail = $contact->primaryEmail)) {
                if (!YII_DEBUG) {
                    StmZendMail::easyMail('do-not-reply@seizethemarket.net', $toEmail, 'Assignment Update Notification', $content, $bcc=null, $type='text/html', true, true, null, false);
                }
            }

            return;
		}

		public function getAssignee() {

			$assignee = $this->getAssigneeByType();

			return $assignee;
		}

		private function getAssigneeByType() {
			$assignee = null;

			if ($this->assignee_company_id) {
				$assignee = Companies::model()->findByPk($this->assignee_company_id);
			} else {
				$assignee = Contacts::model()->findByPk($this->assignee_contact_id);
			}

			return $assignee;
		}

		// @todo: When company aspect gets implemented
		public function getContact() {
			if ($this->assignment_type_id == ComponentTypes::CONTACTS) {
				return Contacts::model()->findByPk($this->contact_id);
			}
		}

		/**
		 * Return the contact's full name
		 *
		 * @return string
		 */
		public function getContactName() {
			return StmHtml::value($this, 'contact.fullName');
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'component_type_id' => 'Component Type',
				'component_id' => 'Component',
				'assignment_type_id' => 'Assignment Type',
				'assignee_contact_id' => 'Assigned To',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('component_id', $this->component_id);
			$criteria->compare('assignment_type_id', $this->assignment_type_id);
			$criteria->compare('assignee_contact_id', $this->assignee_contact_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		public function byComponentTuple($componentTypeId, $componentId) {
			if (isset($componentTypeId) && isset($componentId)) { // @todo: is this necessary? is argument params enough? - CLee
				$Criteria = new CDBCriteria();
				$Criteria->condition = 'component_id = :component_id AND ' . 'component_type_id = :component_type_id';
				$Criteria->params = array(
					':component_type_id' => $componentTypeId,
					':component_id' => $componentId,
				);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}

		public function byAssignmentType($assignmentTypeId) {
			$Criteria = new CDBCriteria();
			$Criteria->condition = 'assignment_type_id = :assignment_type_id';
			$Criteria->params = array(
				':assignment_type_id' => $assignmentTypeId
			);
			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}
	}
