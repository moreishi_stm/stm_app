<?php

	/**
	 * This is the model class for table "operators".
	 *
	 * The followings are the available columns in table 'operators':
	 *
	 * @property integer        $id
	 * @property string         $name
	 * @property string         $symbol
	 *
	 * The followings are the available model relations:
	 * @property TermCriteria[] $termCriterias
	 */
	class Operators extends StmBaseActiveRecord {

		const EQUALS = '=';
		const NOT_EQUALS = '<>';
		const IN = 'in';
		const NOT_IN = 'notin';
		const GT = '>';
		const LT = '<';
		const GTE = '>=';
		const LTE = '<=';
		const LIKE = 'like';
		const LIKE_WILDCARD = 'like%%';
		const BETWEEN = 'between';

		const CONJUNCTOR_AND = 'and';
		const CONJUNCTOR_OR = 'or';

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Operators the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'operators';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'name, symbol',
					'required'
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'symbol',
					'length',
					'max' => 7
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, symbol',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'termCriterias' => array(
					self::HAS_MANY,
					'TermCriteria',
					'operator_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
				'symbol' => 'Symbol',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('symbol', $this->symbol, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}