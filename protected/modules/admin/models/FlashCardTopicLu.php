<?php

/**
 * This is the model class for table "flash_card_topic_lu".
 *
 * The followings are the available columns in table 'flash_card_topic_lu':
 * @property string $flash_card_id
 * @property string $flash_card_topic_id
 * @property integer $sort_order
 */
class FlashCardTopicLu extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FlashCardTopicLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'flash_card_topic_lu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('flash_card_id, flash_card_topic_id', 'required'),
            array('sort_order', 'numerical', 'integerOnly'=>true),
            array('flash_card_id, flash_card_topic_id', 'length', 'max'=>11),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('flash_card_id, flash_card_topic_id, sort_order', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'flash_card_id' => 'Flash Card',
            'flash_card_topic_id' => 'Flash Card Topic',
            'sort_order' => 'Sort Order',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('flash_card_id',$this->flash_card_id,true);
        $criteria->compare('flash_card_topic_id',$this->flash_card_topic_id,true);
        $criteria->compare('sort_order',$this->sort_order);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}