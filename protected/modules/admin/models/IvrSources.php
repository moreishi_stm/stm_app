<?php

/**
 * This is the model class for table "ivr_sources".
 *
 * The followings are the available columns in table 'ivr_sources':
 * @property string $id
 * @property string $extension_suffix
 * @property integer $source_id
 *
 * The followings are the available model relations:
 * @property Sources $source
 */
class IvrSources extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return IvrSources the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ivr_sources';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('source_id', 'required'),
            array('source_id', 'numerical', 'integerOnly'=>true),
            array('extension_suffix', 'length', 'max'=>2),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, extension_suffix, source_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'source' => array(self::BELONGS_TO, 'Sources', 'source_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'extension_suffix' => 'Extension Suffix',
            'source_id' => 'Source',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('extension_suffix',$this->extension_suffix,true);
        $criteria->compare('source_id',$this->source_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}