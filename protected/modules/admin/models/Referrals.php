<?php
	Yii::import('admin_module.models.interfaces.Component');

	/**
	 * This is the model class for table "referrals".
	 *
	 * The followings are the available columns in table 'referrals':
	 * @property integer $id
	 * @property integer $contact_id
	 * @property integer $assigned_to_id
	 * @property integer $outside_referral_agent
	 * @property integer $source_id
	 * @property string $referral_date
	 * @property integer $transaction_type_id
	 * @property integer $transaction_id
	 * @property integer $referral_direction_ma
	 * @property integer $referral_agreement_signed
	 * @property integer $status_ma
	 * @property string $referral_percent
	 * @property string $referral_paid
	 * @property string $price
	 * @property string $closed_date
	 * @property string $commission_gross
	 * @property string $commission_net
	 * @property string $notes
	 * @property integer $updated_by
	 * @property string $updated
	 * @property integer $added_by
	 * @property string $added
	 * @property integer $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts $assignedTo
	 * @property Contacts $outsideReferralAgent
	 * @property Sources $source
	 * @property Contacts $contact
	 * @property Contacts $updatedBy
	 * @property Contacts $addedBy
	 * @property ComponentTypes $componentType
	 */
	class Referrals extends StmBaseActiveRecord implements Component
	{
		const REFERRAL_DIRECTION_IN = 1;
		const REFERRAL_DIRECTION_OUT = 2;
        const REFERRAL_DIRECTION_INTERNAL = 3;

		public $priceSum;
		public $incomeSum;

        public $fromDate;
        public $toDate;

		private $_componentType;

		/**
		 * Returns the static model of the specified AR class.
		 * @param string $className active record class name.
		 * @return Referrals the static model class
		 */
		public static function model($className=__CLASS__)
		{
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName()
		{
			return 'referrals';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules()
		{
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array('contact_id, assigned_to_id, outside_referral_agent_id, source_id, transaction_type_id, referral_direction_ma, status_ma, referral_percent, price, referral_date', 'required'),
				array('contact_id, assigned_to_id, outside_referral_agent_id, source_id, transaction_type_id, transaction_id, referral_direction_ma, referral_agreement_signed, status_ma, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
				array('referral_percent', 'length', 'max'=>4),
				array('referral_paid, price, commission_gross, commission_net', 'length', 'max'=>11),
				array('notes', 'length', 'max'=>750),
				array('referral_date, closed_date, updated, added, fromDate, toDate', 'safe'),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('id, contact_id, assigned_to_id, outside_referral_agent_id, source_id, referral_date, transaction_type_id, transaction_id, referral_direction_ma, referral_agreement_signed, status_ma, referral_percent, referral_paid, price, closed_date, commission_gross, commission_net, notes, updated_by, updated, added_by, added, is_deleted, fromDate, toDate', 'safe', 'on'=>'search'),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations()
		{
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'assignedTo' => array(self::BELONGS_TO, 'Contacts', 'assigned_to_id'),
				'outsideReferralAgent' => array(self::BELONGS_TO, 'Contacts', 'outside_referral_agent_id'),
				'source' => array(self::BELONGS_TO, 'Sources', 'source_id'),
				'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
				'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
				'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
                'transaction' => array(self::BELONGS_TO, 'Transactions', 'transaction_id'),
				'transactionStatus' => array(self::BELONGS_TO, 'TransactionStatus', 'status_ma'),
				'transactionType' => array(self::BELONGS_TO, 'ComponentTypes', 'transaction_type_id'),
			);
		}

		public function behaviors() {
			return array(
				'getTasks' => array(
					'class' => 'admin_module.components.behaviors.GetTasksBehavior',
				),
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels()
		{
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'assigned_to_id' => 'Agent Assigned To',
				'outside_referral_agent_id' => 'Referred by/to',
				'source_id' => 'Source',
				'referral_date' => 'Referral Date',
				'transaction_type_id' => 'Transaction Type',
				'transaction_id' => 'Transaction',
				'referral_direction_ma' => 'Referral Direction',
				'referral_agreement_signed' => 'Referral Agreement Signed',
				'status_ma' => 'Status',
				'referral_percent' => 'Referral Fee',
				'referral_paid' => 'Referral Paid',
				'price' => 'Price',
				'closed_date' => 'Closed Date',
				'commission_gross' => 'Commission Gross',
				'commission_net' => 'Commission Net',
				'notes' => 'Notes',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'added_by' => 'Added By',
				'added' => 'Added',
				'is_deleted' => 'Is Deleted',
			);
		}

        public function byReferralDate($fromDate=null, $toDate=null) {

            if($fromDate) {
                $this->getDbCriteria()->mergeWith(array(
                        'condition' => 'referral_date >= :referral_date_from',
                        'params' => array(':referral_date_from' => Yii::app()->format->formatDate($fromDate, StmFormatter::MYSQL_DATE_FORMAT)),
                    )
                );
            }

            if($toDate) {
                $this->getDbCriteria()->mergeWith(array(
                        'condition' => 'referral_date <= :referral_date_to',
                        'params' => array(':referral_date_to' => Yii::app()->format->formatDate($toDate, StmFormatter::MYSQL_DATE_FORMAT)),
                    )
                );
            }

            return $this;
        }

        public function byInbound() {
			$this->getDbCriteria()->mergeWith(array(
											  'condition' => 'referral_direction_ma=:referral_direction_ma',
											  'params' => array(':referral_direction_ma' => self::REFERRAL_DIRECTION_IN),
											  )
			);

			return $this;
		}

		public function bySumVolume() {
			$this->getDbCriteria()->mergeWith(array(
											  'select' => 'sum(price) AS priceSum',
											  )
			);

			return $this;
		}

		public function byOutbound() {
			$this->getDbCriteria()->mergeWith(array(
											  'condition' => 'referral_direction_ma=:referral_direction_ma',
											  'params' => array(':referral_direction_ma' => self::REFERRAL_DIRECTION_OUT),
											  )
			);

			return $this;
		}

		public function bySumIncome($directionId) {
			if($directionId == self::REFERRAL_DIRECTION_IN) {
				$this->getDbCriteria()->mergeWith(array(
												  'select' => 'sum(price*(1-referral_percent)) AS incomeSum',
												  )
				);

			} elseif ($directionId == self::REFERRAL_DIRECTION_OUT) {
				$this->getDbCriteria()->mergeWith(array(
												  'select' => 'sum(price*referral_percent) AS incomeSum',
												  )
				);
			}

			return $this;
		}

		public function byStatusIds($statusIds) {
			$this->getDbCriteria()->addInCondition('status_ma',$statusIds);

			return $this;
		}

		public function getPotentialIncome() {
			if($this->referral_direction_ma == self::REFERRAL_DIRECTION_IN) {
				$potentialIncome =  (1-$this->referral_percent) * $this->price *.03;

			} else {
				$potentialIncome = $this->referral_percent * $this->price *.03;
			}
			return $potentialIncome;
		}

		public function getReferralDirectionTypes() {
			return array(
				self::REFERRAL_DIRECTION_IN => 'Inbound',
				self::REFERRAL_DIRECTION_OUT => 'Outbound',
                self::REFERRAL_DIRECTION_INTERNAL => 'Internal',
			);
		}

		public function getTransactionTypes() {
			return array(
				ComponentTypes::BUYERS=>'Buyer',
				ComponentTypes::SELLERS=>'Seller',
			);
		}

		public function getStatusTypes() {
			return array(
				TransactionStatus::NURTURING_ID=>'Nurturing',
				TransactionStatus::ACTIVE_LISTING_ID=>'Listed',
				TransactionStatus::UNDER_CONTRACT_ID=>'Pending',
				TransactionStatus::CLOSED_ID=>'Closed',
				TransactionStatus::CANCELLED_ID=>'Cancelled'
			);
		}

		public function getActiveStatusIds() {
			return array(
				TransactionStatus::NURTURING_ID,
				TransactionStatus::ACTIVE_LISTING_ID,
				TransactionStatus::UNDER_CONTRACT_ID,
			);
		}

		public function getComponentType() {
			if (!isset($this->_componentType)) {
				$this->_componentType = ComponentTypes::model()->findByPk(ComponentTypes::REFERRALS);
			}

			return $this->_componentType;
		}

		public function getContactId() {
			return $this->contact_id;
		}


		public function getComponentChain() {
			$chain = array($this->asComponentTuple());

			$includeCompleted = true;
			$Tasks = $this->getTasks($this, $includeCompleted);

			if (count($Tasks) > 0) {
				foreach ($Tasks as $Task) {
					array_push($chain, $Task->asComponentTuple());
				}
			}

			return $chain;
		}

		public function asComponentTuple() {
			return array(
				'componentTypeId' => ComponentTypes::REFERRALS,
				'componentId' => $this->id
			);
		}

		protected function beforeValidate() {
			// If the value is a number based field strip commas or decimals from it.
			$this->price = Yii::app()->format->formatDecimal($this->price);
			$this->referral_paid = Yii::app()->format->formatDecimal($this->referral_paid);
			$this->commission_gross = Yii::app()->format->formatDecimal($this->commission_gross);
			$this->commission_net = Yii::app()->format->formatDecimal($this->commission_net);

			return parent::beforeValidate();
		}

		protected function beforeSave() {
			if ($this->isNewRecord) {
				$this->added_by = Yii::app()->user->id;
				$this->added = new CDbExpression('NOW()');
			}

			$this->updated_by = Yii::app()->user->id;
			$this->updated = new CDbExpression('NOW()');

			if ($this->referral_date > 0) {
				$this->referral_date = Yii::app()->format->formatDate($this->referral_date, StmFormatter::MYSQL_DATETIME_FORMAT);
			}

			if ($this->closed_date > 0) {
				$this->closed_date = Yii::app()->format->formatDate($this->closed_date, StmFormatter::MYSQL_DATETIME_FORMAT);
			}

			return parent::beforeSave();
		}

		protected function afterFind() {
			$this->referral_date = Yii::app()->format->formatDate($this->referral_date, StmFormatter::APP_DATE_FORMAT);
			$this->closed_date = Yii::app()->format->formatDate($this->closed_date, StmFormatter::APP_DATE_FORMAT);

			return parent::afterFind();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search()
		{
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria=new CDbCriteria;

			$criteria->compare('id',$this->id);
			$criteria->compare('contact_id',$this->contact_id);
			$criteria->compare('assigned_to_id',$this->assigned_to_id);
			$criteria->compare('outside_referral_agent_id',$this->outside_referral_agent_id);
			$criteria->compare('source_id',$this->source_id);
			$criteria->compare('referral_date',$this->referral_date,true);

            if($this->fromDate) {
                $criteria->compare('referral_date','>='.Yii::app()->format->formatDate($this->fromDate, StmFormatter::MYSQL_DATE_FORMAT));
            }

            if($this->toDate) {
                $criteria->compare('referral_date', '<='.Yii::app()->format->formatDate($this->toDate, StmFormatter::MYSQL_DATE_FORMAT));
            }

			$criteria->compare('transaction_type_id',$this->transaction_type_id);
			$criteria->compare('transaction_id',$this->transaction_id);
			$criteria->compare('referral_direction_ma',$this->referral_direction_ma);
			$criteria->compare('referral_agreement_signed',$this->referral_agreement_signed);
			$criteria->compare('status_ma',$this->status_ma);
			$criteria->compare('referral_percent',$this->referral_percent,true);
			$criteria->compare('referral_paid',$this->referral_paid,true);
			$criteria->compare('price',$this->price,true);
			$criteria->compare('closed_date',$this->closed_date,true);
			$criteria->compare('commission_gross',$this->commission_gross,true);
			$criteria->compare('commission_net',$this->commission_net,true);
			$criteria->compare('notes',$this->notes,true);
			$criteria->compare('updated_by',$this->updated_by);
			$criteria->compare('updated',$this->updated,true);
			$criteria->compare('added_by',$this->added_by);
			$criteria->compare('added',$this->added,true);
			$criteria->compare('is_deleted',$this->is_deleted);

			return new CActiveDataProvider($this, array(
												  'criteria'=>$criteria,
                                                  'pagination'=>array(
                                                      'pageSize'=> 50,
                                                  ),
												  ));
		}
	}