<?php

/**
 * This is the model class for table "pipeline_stages".
 *
 * The followings are the available columns in table 'pipeline_stages':
 * @property integer $id
 * @property integer $pipeline_id
 * @property string $name
 * @property string $description
 * @property string $added
 * @property integer $added_by
 * @property string $updated
 * @property integer $updated_by
 * @property integer $probability
 * @property integer $sort_order
 * @property integer $status_id
 *
 * The followings are the available model relations:
 * @property PipelineComponentLu[] $pipelineComponentLus
 * @property Pipelines $pipeline
 */
class PipelineStages extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PipelineStages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pipeline_stages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pipeline_id, name, added, added_by', 'required'),
			array('pipeline_id, added_by, updated_by, probability, sort_order, status_id, status_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>200),
			array('description, updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, pipeline_id, name, description, added, added_by, updated, updated_by, probability, sort_order, status_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pipelineComponentLus' => array(self::HAS_MANY, 'PipelineComponentLu', 'pipeline_stage_id'),
			'pipeline' => array(self::BELONGS_TO, 'Pipelines', 'pipeline_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pipeline_id' => 'Pipeline',
			'name' => 'Name',
			'description' => 'Description',
			'added' => 'Added',
			'added_by' => 'Added By',
			'updated' => 'Updated',
			'updated_by' => 'Updated By',
			'probability' => 'Probability',
			'sort_order' => 'Sort Order',
			'status_id' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pipeline_id',$this->pipeline_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('probability',$this->probability);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('status_id',$this->status_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}