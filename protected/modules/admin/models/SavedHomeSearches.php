<?php

	/**
	 * This is the model class for table "saved_home_searches".
	 *
	 * The followings are the available columns in table 'saved_home_searches':
	 *
	 * @property integer  $id
     * @property integer  $mls_board_id
     * @property integer  $component_type_id
     * @property integer  $component_id
	 * @property integer  $contact_id
	 * @property string   $name
	 * @property string   $description
	 * @property string   $criteria
	 * @property string   $frequency
     * @property string   $sunday
     * @property string   $monday
     * @property string   $tuesday
     * @property string   $wednesday
     * @property string   $thursday
     * @property string   $friday
     * @property string   $saturday
     * @property integer  $property_count
     * @property integer  $is_auto_criteria
	 * @property integer  $added_by
	 * @property string   $added
	 * @property integer  $updated_by
	 * @property string   $updated
	 * @property integer  $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts $contact
	 * @property Contacts $updatedBy
	 * @property Contacts $addedBy
	 * @property Contacts $agent
	 */
	class SavedHomeSearches extends StmBaseActiveRecord {

		const FREQUENCY_DAILY_ID = 1;
		const FREQUENCY_WEEKLY_ID = 2;
		const FREQUENCY_HOURLY = 3;
		const FREQUENCY_HALF_HOURLY = 4;

		public $data = array();
		public $_term;
		public $_criteriaArray = array();

		public $addedFromDate;
		public $addedToDate;
		public $updatedToDate;
		public $updatedFromDate;
		public $contactFirstName;
		public $contactLastName;
        public $resultCountMax;

        private $_zipString;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return SavedHomeSearches the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'saved_home_searches';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'mls_board_id, component_type_id, component_id, contact_id, name, frequency, updated, updated_by, agent_id',
					'required'
				),
				array(
					'mls_board_id, component_type_id, component_id, contact_id, agent_id, sunday, monday, tuesday, wednesday, thursday, friday, saturday, is_auto_criteria, added_by, updated_by, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'description',
					'length',
					'max' => 255
				),
				array(
					'contactFirstName, contactLastName, frequency, added, addedFromDate, addedToDate, updated, updatedFromDate, updatedToDate, resultCountMax',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, mls_board_id, component_type_id, component_id, contact_id, agent_id, contactFirstName, contactLastName, name, description, frequency, is_auto_criteria, added_by, added, addedFromDate, addedToDate, updatedFromDate, updatedToDate, updated_by, updated, is_deleted, resultCountMax',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				// 'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
				'termValues' => array(
					self::HAS_MANY,
					'TermComponentLu',
					'component_id',
					'condition' => 'component_type_id = :component_type_id',
					'params' => array(':component_type_id' => ComponentTypes::SAVED_SEARCHES),
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by',
					'alias' => 't'
				),
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by',
					'alias' => 't'
				),
				'agent' => array(
					self::BELONGS_TO,
					'Contacts',
					'agent_id'
				)
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'name' => 'Name',
				'description' => 'Description',
				'criteria' => 'Criteria',
				'frequency' => 'Frequency',
                'sunday' => 'Sunday',
                'monday' => 'Monday',
                'tuesday' => 'Tuesday',
                'wednesday' => 'Wednesday',
                'thursday' => 'Thursday',
                'friday' => 'Friday',
                'saturday' => 'Saturday',
                'is_auto_criteria' => 'Is Auto Criteria',
				'added_by' => 'Added By',
				'added' => 'Added',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'is_deleted' => 'Is Deleted',
				'area'		=> "Area"
			);
		}

		/**
		 * @return array behaviors for model attributes.
		 */
		public function behaviors() {
			return array(
				'modelAttribute' => array('class' => 'admin_module.components.behaviors.ModelAttributeBehavior'),
				'TermCriteriaBuilder' => array('class' => 'admin_module.components.behaviors.TermCriteriaBuilderBehavior'),
			);
		}

		public function byDistinct($field) {
			$Criteria = new CDbCriteria;
			$Criteria->select = $field;
			$Criteria->distinct = true;
			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public static function getFrequencyTypes() {
			return array(
				self::FREQUENCY_DAILY_ID => 'Daily',
				self::FREQUENCY_WEEKLY_ID => 'Checked Days',
			);
		}

		public function byContactId($contactId) {

			 $this->getDbCriteria()->mergeWith(array(
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => $contactId)
			));

			return $this;
		}


		/**
		 * getCount
		 * Returns the # of Saved Home Searches for a contact. Used for diplayed number in JUI Tab
		 *
		 * @param int     $contactId
		 * @param boolean $tabFormat true returns in "(10)" to be used in tabs
		 *
		 * @return int $count
		 */
		public static function getCount($contactId, $tabFormat = false) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);
			$count = SavedHomeSearches::model()->count($Criteria);
			if ($tabFormat) {
				$count = '(' . $count . ')';
			}

			return $count;
		}

		public function getPriceRange() {
			return Yii::app()->format->formatDollars($this->getCriteriaValueByField('price_min')) . '-' . Yii::app()->format->formatDollars($this->getCriteriaValueByField('price_max'));
		}

		public function getCriteriaValueByField($fieldName) {
			return $this->criteriaArray[$fieldName];
		}

		public function printBedsBathSqFeetRow() {
			$row = '';
			if ($this->getCriteriaValueByField('bedrooms')) {
				$row .= $this->getCriteriaValueByField('bedrooms') . ' BR';
			}

			if ($this->getCriteriaValueByField('baths_total')) {
				$row .= ($row) ? ' / ' : '';
				$row .= $this->getCriteriaValueByField('baths_total') . ' Baths';
			}
			if ($this->getCriteriaValueByField('sq_feet')) {
				$row .= ($row) ? ' / ' : '';
				$row .= number_format($this->getCriteriaValueByField('sq_feet')) . ' SF';
			}
			$row = ($row) ? '<br />' . $row : $row;

			return $row;
		}

		public function printBooleanFieldsRow() {
			$row = '';
			$fields = array(
				'waterfront' => 'Waterfront',
				'pool' => 'Pool',
				'foreclosure' => 'Foreclosure',
				'short_sale' => 'Short Sale',
			);

			foreach ($fields as $field => $label) {
				$temp = $this->getCriteriaValueByField($field);
				if (isset($temp)) {
					$row .= ($row) ? ', ' : '';
					$row .= ($this->getCriteriaValueByField($field) == 1) ? $label : 'Not ' . $label;
				}
			}
			$row = ($row) ? '<br />' . $row : $row;

			return $row;
		}

		public function getCriteriaArray() {
			if (!empty($this->_criteriaArray)) {
				return $this->_criteriaArray;
			}

			return $this->_criteriaArray = CJSON::decode($this->criteria);
		}

		public function getTermIdByName($name) {
			if (!$this->_term) {
				$this->_term = new Terms;
			}

			return $this->_term->getIdByName($name);
		}

		public function printSearchCriteria() {
            $string = '';
            $this->_zipString = '';
            $cityString = '';
			$TermValues = $this->termValues;
			foreach ($TermValues as $TermValue) {
				$termName = $TermValue->term->name;
				$termDisplayName = $TermValue->term->display_name;

				if (in_array($termName, array(
						'price_min',
						'price_max',
						'sq_feet',
                        'sq_feet_max',
						'bedrooms',
                        'bedrooms_max',
						'baths_total',
                        'baths_total_max',
                        'year_built',
                        'year_built_max',
						'property_type',
                        'city',
                        'zip'
					)
				)) {
					switch ($termName) {
                        case 'property_type':
                            $TermValue->value = MlsPropertyTypes::model()->findByPk($TermValue->value)->name;
                            $string .= (empty($string))? '':'<br />';
                            $string .= $termDisplayName . ': ' . $TermValue->value;
                            break;

						case 'price_min':
							$priceArray['price_min'] = $TermValue->value;
							break;

						case 'price_max':
							$priceArray['price_max'] = $TermValue->value;
							break;

						case 'sq_feet':
//							$bedsBathSf['sq_feet'] = number_format($TermValue->value) . '+ SF';
                            $bedsBathSf['sq_feet'] = ($bedsBathSf['sq_feet']) ? $TermValue->value . " ". $bedsBathSf['sq_feet'] : $TermValue->value . ' SF';
							break;

                        case 'sq_feet_max':
                            $bedsBathSf['sq_feet'] = str_replace('SF', '', $bedsBathSf['sq_feet']). "to " . $TermValue->value ." SF";
                            break;

                        case 'bedrooms':
							$bedsBathSf['bedrooms'] = ($bedsBathSf['bedrooms']) ? $TermValue->value . " ". $bedsBathSf['bedrooms'] : $TermValue->value . ' BR';
							break;

                        case 'bedrooms_max':
                            $bedsBathSf['bedrooms'] = str_replace('BR', '', $bedsBathSf['bedrooms']). "to " . $TermValue->value ." BR";
                            break;

                        case 'baths_total':
                            $bedsBathSf['baths_total'] = ($bedsBathSf['baths_total']) ? $TermValue->value . " ". $bedsBathSf['baths_total'] : $TermValue->value . ' Baths';
							break;

                        case 'baths_total_max':
                            $bedsBathSf['baths_total'] = str_replace('Baths', '', $bedsBathSf['baths_total']). "to " . $TermValue->value ." Baths";
                            break;

                        case 'year_built':
                            $bedsBathSf['year_built'] = ($bedsBathSf['year_built']) ? str_replace(' up ', $TermValue->value, $bedsBathSf['year_built']) : 'Yr Built: '.$TermValue->value .' or newer';
//                            $bedsBathSf['year_built'] = str_replace(' up ', '', $bedsBathSf['year_built']). " to " . $TermValue->value ." Yr Built";
                            break;

                        case 'year_built_max':
                            $bedsBathSf['year_built'] = ($bedsBathSf['year_built']) ? str_replace(' or newer', ' to '.$TermValue->value, $bedsBathSf['year_built']): 'Yr Built: up to '.$TermValue->value;

                            break;

                        case 'city':
                            $cityString .= (empty($cityString))? $TermValue->value : ', '.$TermValue->value;
                            break;

                        case 'zip':
                            $this->addToZipString($TermValue->value);
                            break;

					}
				} else {
                    $string .= (empty($string))? '':'<br />';
					$string .= $termDisplayName . ': ' . $TermValue->value;
				}
			}

            if (isset($priceArray['price_min']) && isset($priceArray['price_max'])) {
                $priceString = '<br />Price: $' . number_format($priceArray['price_min']) . ' - $' . number_format($priceArray['price_max']);
            } elseif (isset($priceArray['price_min'])) {
                $priceString = '<br />Price: $' . number_format($priceArray['price_min']) . '+';
            } elseif (isset($priceArray['price_max'])) {
                $priceString = '<br />Price: $0 - $' . number_format($priceArray['price_max']);
            }

            $bedsBathSfString = '';
			if (isset($bedsBathSf['bedrooms']) && $bedsBathSf['bedrooms']) {
				$bedsBathSfString = $bedsBathSf['bedrooms'];
			}

			if (isset($bedsBathSf['baths_total']) && $bedsBathSf['baths_total']) {
				$bedsBathSfString .= ($bedsBathSfString) ? ' / ' : '';
				$bedsBathSfString .= '' . $bedsBathSf['baths_total'];
			}

			if (isset($bedsBathSf['sq_feet']) && $bedsBathSf['sq_feet']) {
				$bedsBathSfString .= ($bedsBathSfString) ? ' / ' : '';
				$bedsBathSfString .= '' . $bedsBathSf['sq_feet'];
			}
            if (isset($bedsBathSf['year_built']) && $bedsBathSf['year_built']) {
                $bedsBathSfString = $bedsBathSf['year_built'];
            }
			if ($bedsBathSfString) {
                $string = (empty($string))? $bedsBathSfString : $bedsBathSfString . '<br />'.$string;;
			}

            if($priceString && $string) {
                $priceString = $priceString.'<br />';
            }
            $string = $priceString.$string;

            if(!empty($cityString)) {
                $string .= '<br />City: '.$cityString;
            }
            if(!empty($this->_zipString)) {
                $string .= '<br />Zip: '.$this->_zipString;
            }
            return $string;
		}

        public function addToZipString($zip) {
            $this->_zipString .= (empty($this->_zipString))? $zip : ', '.$zip;
            return;
        }

        public function createEmailHtml() {
			$Criteria = $this->getCriteria(ComponentTypes::SAVED_SEARCHES, $this->id);
			$ActiveCriteria = $this->getCriteriaByTermName('status', 'Active');
			$Criteria->mergeWith($ActiveCriteria);

			$Properties = $this->findAllProperties($Criteria);

			$oddEven = 'even';
			$htmlString = '<div style="width:670px;">';
			ob_start();
			foreach ($Properties as $Property) {
				$bg = ($oddEven == 'odd') ? '#E1E8FF' : '#FFFFFF';

                $propertyUrl = "/home/".strtolower(AddressStates::getShortNameById(Yii::app()->user->board->state_id))."/".$Property->addressLink."/".$Property->listing_id;
                $photoUrl = $Property->getPhotoUrl(1);
                if(strpos($photoUrl, 'default-placeholder') !== false) {
                    // some properties missing photo #1 but have #2 - weird
                    $photoUrl = $Property->getPhotoUrl(2);
                }
				?>
				<div style="background-color: <?php echo $bg; ?>;padding:6px;">
					<div style="width:260px;display:inline-block;vertical-align:top;padding-right:8px;"><a href="<?php echo $propertyUrl; ?>"><img
								src="<?php echo $photoUrl ?>" width="260"></a></div>
					<div style="width:120px;display:inline-block;vertical-align:top;position:relative;">
						<span style="color:#444;font-size:17px;font-weight:bold;line-height: 15px;"><?php echo Yii::app()->format->formatDollars($Property->price) ?></span>
                                        <span style="color:#222;font-size:12px;display:inline-block;padding-top:6px;">
                                                ($<?php echo $Property->pricePerSf ?>/SF)<br/>
                                                MLS#: <?php echo $Property->listing_id; ?><br/>
                                        </span>
						<span style="margin-top: 20px;display:block;"><a style="color:#D20000;font-weight:bold;font-size:13px;" href="<?php echo $propertyUrl; ?>">View Details</a></span>
					</div>
					<div style="width:130px;color:#222;font-size:12px;vertical-align:top;display:inline-block;position:relative;">
						<?php echo $Property->bedrooms; ?> Bedrooms<br/>
						<?php echo $Property->baths_full; ?> Baths<br/>
						<?php echo $Property->sq_feet; ?> Sq. Feet<br/>
						<?php echo $Property->year_built ?> Yr Built<br/>
						<?php echo ($Property->pool_yn) ? '<span style="color:#36F;font-weight:bold;font-size:13px;">Pool</span><br />' : ''; ?>
						<?php echo ($Property->waterfront_yn) ? '<span style="color:#36F;font-weight:bold;font-size:13px;">Waterfront</span><br />' : ''; ?>
						<span style="margin-top: 20px;display:block;"><a style="color:#30BF00;font-weight:bold;font-size:13px;" href="<?php echo $propertyUrl; ?>">View <?php echo $Property->photo_count; ?> Photos</a></span>
					</div>
					<div style="width:120px;color:#222;font-size:12px;vertical-align:top;display:inline-block;position:relative;">
						<?php echo Yii::app()->format->formatProperCase($Property->common_subdivision) ?><br/>
						<?php echo Yii::app()->format->formatProperCase($Property->city) ?>, <br/><?php echo substr($Property->zip, 0, 5) ?><br/>
                        <?php echo Yii::app()->format->formatDays($Property->status_change_date); ?><br/>

						<span style="margin-top: 20px;display:block;"><a style="color:blue;font-weight:bold;font-size:13px;" href="<?php echo $propertyUrl; ?>">View Map</a></span>
					</div>
				</div>


				<?php
				$oddEven = ($oddEven == 'even') ? 'odd' : 'even';
			}
			$htmlString .= ob_get_contents();
			ob_end_clean();
			$htmlString .= '</div>';

			return $htmlString;
		}

		/**
		 * @return CDbCriteria this saved home search instance as a CDbCriteria instance.
		 */
		public function toCriteria() {
			$Criteria = $this->getCriteria(ComponentTypes::SAVED_SEARCHES, $this->id);
			$ActiveCriteria = $this->getCriteriaByTermName('status', 'Active');
			$Criteria->mergeWith($ActiveCriteria);

			return $Criteria;
		}

		public function sincePropertyLastUpdated($date, CDbCriteria $Criteria = null) {

			$LastUpdateCriteria = new CDbCriteria;
			// @todo: create this into term "property_last_updated" as it will be used frequently on front side.
			$LastUpdateCriteria->condition = 'property_last_updated>=:date';
			$LastUpdateCriteria->params = array(':date' => $date);

			if ($Criteria) {
				$Criteria->mergeWith($LastUpdateCriteria);
			} else {
				$Criteria = $LastUpdateCriteria;
			}

			return $Criteria;
		}

		public function getCountPropertyResults() {
			$Criteria = $this->getCriteria(ComponentTypes::SAVED_SEARCHES, $this->id);
			//$ActiveCriteria = $this->getCriteriaByTermName('status', 'Active');
            $Criteria->addCondition('status like "%Active%" AND status != "Inactive"');
//			$Criteria->mergeWith($ActiveCriteria);

			return $this->findAllPropertiesCount($Criteria);
		}

//        public static function createSavedHomeSearches($componentTypeIds)
//        {
//            $componentTypeIds = (array) $componentTypeIds;
//            $contactIds = Yii::app()->createCommand()
//                ->select('*')
//                ->from('contacts c')
//                ->leftJoin('saved_home_searches s', 'c.id=s.contact_id')
//                ->join('transactions t', 'c.id=s.contact_id')
//                ->join('email e', 'c.id=e.contact_id')
//                ->where('s.id IS NULL')
//                ->andWhere(array('in', 't.component_type_id', $componentTypeIds))
//                ->group('c.id')
//                ->queryColumn();
//        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('t.id', $this->id);
			$criteria->compare('t.contact_id', $this->contact_id);
            $criteria->compare('t.agent_id', $this->agent_id);
			$criteria->compare('t.name', $this->name, true);
			$criteria->compare('t.description', $this->description, true);
			$criteria->compare('t.criteria', $this->criteria, true);
			$criteria->compare('t.frequency', $this->frequency, true);
			$criteria->compare('t.added_by', $this->added_by);
			$criteria->compare('t.added', $this->added, true);

            if($this->resultCountMax) {
//    			$criteria->compare('t.resultCountMax', $this->countPropertyResults);
            }

			if ($this->contactFirstName || $this->contactLastName) {
				$criteria->with = CMap::mergeArray($criteria->with, array('contact'));

				if ($this->contactFirstName) {
					$criteria->compare('contact.first_name', $this->contactFirstName, true);
				}

				if ($this->contactLastName) {
					$criteria->compare('contact.last_name', $this->contactLastName, true);
				}
			}

			if ($this->addedFromDate) {
				$this->addedFromDate = Yii::app()->format->formatDate($this->addedFromDate, StmFormatter::MYSQL_DATETIME_FORMAT);
				$criteria->compare("t.added", '>=' . $this->addedFromDate, true);
			}
			if ($this->addedToDate) {
				$this->addedToDate = date('Y-m-d H:i:s', strtotime(Yii::app()->format->formatDate($this->addedToDate, StmFormatter::MYSQL_DATETIME_FORMAT) . ' +1 day'));
				$this->addedToDate = Yii::app()->format->formatDate($this->addedToDate, StmFormatter::MYSQL_DATETIME_FORMAT);
				$criteria->compare("t.added", '<' . $this->addedToDate, true);
			}

			if ($this->updatedFromDate) {
				$this->updatedFromDate = Yii::app()->format->formatDate($this->updatedFromDate, StmFormatter::MYSQL_DATETIME_FORMAT);
				$criteria->compare("t.added", '>=' . $this->updatedFromDate, true);
			}
			if ($this->updatedToDate) {
				$this->updatedToDate = date('Y-m-d H:i:s', strtotime(Yii::app()->format->formatDate($this->updatedToDate, StmFormatter::MYSQL_DATETIME_FORMAT) . ' +1 day'));
				$this->updatedToDate = Yii::app()->format->formatDate($this->updatedToDate, StmFormatter::MYSQL_DATETIME_FORMAT);
				$criteria->compare("t.added", '<' . $this->addedToDate, true);
			}

            $criteria->compare('t.is_auto_criteria', $this->is_auto_criteria);
			$criteria->compare('t.updated_by', $this->updated_by);
			$criteria->compare('t.updated', $this->updated, true);
			$criteria->compare('t.is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}