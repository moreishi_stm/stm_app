<?php

	/**
	 * This is the model class for table "operation_manual_tag_permission_lu".
	 *
	 * The followings are the available columns in table 'operation_manual_tag_permission_lu':
	 * @property integer $operation_manual_tag_id
	 * @property string $auth_item_name
	 */
	class OperationManualTagPermissionLu extends StmBaseActiveRecord
	{
		/**
		 * Returns the static model of the specified AR class.
		 * @param string $className active record class name.
		 * @return OperationManualTagPermissionLu the static model class
		 */
		public static function model($className=__CLASS__)
		{
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName()
		{
			return 'operation_manual_tag_permission_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules()
		{
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array('operation_manual_tag_id, auth_item_name', 'required'),
				array('operation_manual_tag_id', 'numerical', 'integerOnly'=>true),
				array('auth_item_name', 'length', 'max'=>64),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('operation_manual_tag_id, auth_item_name', 'safe', 'on'=>'search'),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations()
		{
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'tag' => array(self::BELONGS_TO, 'OperationManualTags', 'operation_manual_tag_id'),
				'authItem' => array(self::BELONGS_TO, 'AuthItem', 'auth_item_name'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels()
		{
			return array(
				'operation_manual_tag_id' => 'Operation Manual Tag',
				'auth_item_name' => 'Auth Item Name',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search()
		{
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria=new CDbCriteria;

			$criteria->compare('operation_manual_tag_id',$this->operation_manual_tag_id);
			$criteria->compare('auth_item_name',$this->auth_item_name,true);

			return new CActiveDataProvider($this, array(
												  'criteria'=>$criteria,
												  ));
		}
	}