<?php

	/**
	 * This is the model class for table "login_log".
	 *
	 * The followings are the available columns in table 'login_log':
	 *
	 * @property integer  $id
	 * @property integer  $contact_id
	 * @property string   $ip
	 * @property string   $datetime
	 *
	 * The followings are the available model relations:
	 * @property Contacts $contact
	 */
	class LoginLog extends StmBaseActiveRecord {

		const LOGIN_BUFFER_TIME = '300 seconds'; //must be 'X seconds'
		public $maxDatetime;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return LoginLog the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'login_log';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'ip',
					'length',
					'max' => 25
				),
				array(
					'datetime',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, ip, datetime',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'ip' => 'Ip',
				'datetime' => 'Datetime',
			);
		}

		public static function processLogin() {
			$model = new LoginLog;

			if (!$model->isDupLogin()) {
				$model->ip = Yii::app()->stmFunctions->getVisitorIp();
				$model->datetime = date('Y-m-d H:i:s');
				$model->save();
			}
		}

		/**
		 * isDupLogin Check to see if there is a login in the past timeframe of LOGIN_BUFFER_TIME
		 *
		 * @return boolean
		 */
		public function isDupLogin() {

			$now = new DateTime;
			$model = $this->find(array(
					'select' => 'max(datetime) as maxDatetime',
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => Yii::app()->user->id),
				)
			);

			if (!$model->maxDatetime) {
				return false;
			}

			$added = new DateTime($model->maxDatetime);
			$time_limit = $added->add(DateInterval::createFromDateString(self::LOGIN_BUFFER_TIME));

			return ($now < $time_limit) ? true : false;
		}

		/**
		 * @return
		 */
		public function mostRecent($contact_id, $limit) {
			$logins = $this->find(array(
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => $contact_id),
					'order' => 'datetime desc',
					'limit' => $limit,
				)
			);

			return $logins;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('ip', $this->ip, true);
			$criteria->compare('datetime', $this->datetime, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}