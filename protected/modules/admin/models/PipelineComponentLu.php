<?php

/**
 * This is the model class for table "pipeline_component_lu".
 *
 * The followings are the available columns in table 'pipeline_component_lu':
 * @property integer $id
 * @property integer $pipeline_stage_id
 * @property integer $component_id
 * @property string $added
 * @property integer $added_by
 * @property string $updated
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property PipelineStages $pipelineStage
 */
class PipelineComponentLu extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PipelineComponentLu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pipeline_component_lu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pipeline_stage_id, component_id, added, added_by', 'required'),
			array('pipeline_stage_id, component_id, added_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, pipeline_stage_id, component_id, added, added_by, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'pipeline' => array(self::BELONGS_TO, 'Pipelines', 'pipeline_id'),
			'pipelineStage' => array(self::BELONGS_TO, 'PipelineStages', 'pipeline_stage_id'),
            'contact' => array(self::BELONGS_TO, 'Contacts', 'component_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pipeline_stage_id' => 'Pipeline Stage',
			'component_id' => 'Component',
			'added' => 'Added',
			'added_by' => 'Added By',
			'updated' => 'Updated',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pipeline_stage_id',$this->pipeline_stage_id);
		$criteria->compare('component_id',$this->component_id);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}