<?php

/**
 * This is the model class for table "history_log".
 *
 * The followings are the available columns in table 'history_log':
 * @property integer $id
 * @property string $model_name
 * @property integer $action_type_ma
 * @property string $old_value
 * @property string $new_value
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 */
class HistoryLog extends StmBaseActiveRecord
{
    const ACTION_TYPE_ADD = 1;
    const ACTION_TYPE_UPDATE = 2;
    const ACTION_TYPE_DELETE = 3;
    const ACTION_TYPE_TRANSFER = 4;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HistoryLog the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'history_log';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('primary_key, action_type_ma, added_by', 'numerical', 'integerOnly'=>true),
            array('model_name, primary_key, old_value, new_value', 'required'),
            array('model_name', 'length', 'max'=>127),
            array('old_value, new_value, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, model_name, primary_key, action_type_ma, old_value, new_value, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    public function behaviors() {
        return array(
            'modelAttribute' => array(
                'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'model_name' => 'Model Name',
            'primary_key' => 'Primary Key',
            'action_type_ma' => 'Action Type',
            'old_value' => 'Old Value',
            'new_value' => 'New Value',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    /**
     * getMetStatusTypes
     * Returns the Action Types
     */
    public function getActionTypes() {
        return array(
            self::ACTION_TYPE_ADD       => 'Add',
            self::ACTION_TYPE_UPDATE    => 'Update',
            self::ACTION_TYPE_DELETE    => 'Delete',
            self::ACTION_TYPE_TRANSFER  => 'Transfer',
        );
    }

    protected function beforeSave() {

        if($this->isNewRecord) {
            $this->added_by = Yii::app()->user->id;
            $this->added = new CDbExpression('NOW()');
        }
        return parent::beforeSave();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('model_name',$this->model_name,true);
        $criteria->compare('primary_key',$this->primary_key);
        $criteria->compare('action_type_ma',$this->action_type_ma);
        $criteria->compare('old_value',$this->old_value,true);
        $criteria->compare('new_value',$this->new_value,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
                                              'criteria'=>$criteria,
                                              ));
    }
}