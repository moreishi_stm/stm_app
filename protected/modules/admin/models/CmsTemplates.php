<?php
// Include library necessary to do our html lookups
// @todo: Convert to class
include_once(Yii::getPathOfAlias('admin_module.extensions.') . DS . 'SimpleHtmlDom.php');

/**
 *
 */
class CmsTemplates extends CModel
{

    const TEMPLATE_TYPE_PAGE = 'page';
    const TEMPLATE_TYPE_BLOG = 'blog';
    const TEMPLATE_TYPE_LISTING_SB = 'listingStoryboard';
    const TEMPLATE_TYPE_BUYER_SB = 'buyerStoryboard';
    const TEMPLATE_TYPE_HOUSE_VALUES = 'houseValues';
    const TEMPLATE_TYPE_LANDING_PAGES = 'landingPages';

    // Set a common file extension for the templates
    const TEMPLATE_FILE_EXT = '.php';

    // The css selector used to determine which fields in the template are editable
    const TEMPLATE_FIELD_SELECTOR = '.stmcms';

    // Collection of a template's field models (CmsTemplateField)

    public $fields = array();

    public $cmsContentId;

    public $content;

    public $templateName;

    public $loadedFields = array();

    protected $templateFileName;

    public $templatePath;

    public $templateType;

    private $_invalidFiles
        = array('.', '..');

    private $_appTemplates = array();

    private $_themeTemplates = array();

    private $_appTemplatePath;

    private $_loadOnly = false;

    private $_themeTemplatePath;

    private $_appTemplateTypes
        = array(
            CmsContents::TYPE_PAGE => 'page',
            CmsContents::TYPE_BLOG => 'blog',
            CmsContents::TYPE_LISTING_SB => 'listingStoryboard',
            CmsContents::TYPE_BUYER_SB => 'buyerStoryboard',
            CmsContents::TYPE_HOUSE_VALUES => 'houseValues',
            CmsContents::TYPE_LANDING_PAGES => 'landingPages',
        );

    public function __construct($templateName = '', $templateType = null, $cmsContentId = null) {
        $this->templateType = $this->_appTemplateTypes[$templateType];

        if ($cmsContentId) {
            $this->cmsContentId = $cmsContentId;
        }

        if ($templateName) {
            $this->loadTemplate($templateName);
        }
    }

    /**
     * Initializes the template, and sets the default values for the cms fields found
     *
     * @param      $templateName
     *
     * @return bool|null
     */
    public function loadTemplate($templateName)
    {
        $this->templateName = $templateName;
        $this->templateFileName = $this->templateName . self::TEMPLATE_FILE_EXT;

        if (!$this->content) {
            //check to see if theme exists for this template, then go to base app as secondary
            $themeBasePath = $this->getThemeTemplatePath();
            $themeBasePath .= DS . $this->templateType;
            $this->templatePath = $themeBasePath . DS . $this->templateName . DS . $this->templateFileName;

            if (!file_exists($this->templatePath)) {

                // check for stm_app template path
                $this->templatePath = Yii::getPathOfAlias('stm_app.themes.'.Yii::app()->theme->name.'.views.front.cms.templates') . DS . $this->templateType . DS . $this->templateName . DS . $this->templateFileName;

                if (!file_exists($this->templatePath)) {

                    $basePath = (in_array($templateName, $this->appTemplates[$this->templateType])) ? Yii::getPathOfAlias('stm_app.themes.'.Yii::app()->theme->name.'.views.front.cms.templates')
                        : $this->getThemeTemplatePath();
                    $basePath .= DS . $this->templateType;
                    $this->templatePath = $basePath . DS . $this->templateName . DS . $this->templateFileName;

                    if (!file_exists($this->templatePath)) {

                        $basePath = (in_array($templateName, $this->appTemplates[$this->templateType])) ? $this->_appTemplatePath
                            : $this->getThemeTemplatePath();
                        $basePath .= DS . $this->templateType;
                        $this->templatePath = $basePath . DS . $this->templateName . DS . $this->templateFileName;

                        if (!file_exists($this->templatePath)) {
                            return null;
                        }
                    }
                }
            }


            $this->content = file_get_contents($this->templatePath);
        } else {
            $this->fields = array();
        }

        $templateHtml = str_get_html($this->content);
        $stmFields = $templateHtml->find(self::TEMPLATE_FIELD_SELECTOR);

        // If there are no stm fields to process, no need to go any further
        if (!$stmFields) {
            return null;
        }

        // Init the .stmcms found fields
        foreach ($stmFields as $Field) {
            $templateField = new CmsTemplateField;
            $templateField->element = $Field;
            $templateField->content = $Field->innertext;
            $templateField->contentRaw = $Field->plaintext;

            if (!empty($Field->id)) {
                $this->fields[$Field->id] = $templateField;
            }
        }

        // Load up data from table instead of JSON
        $cmsContentValues = CmsContentValues::model()->findAllByAttributes(array(
            'cms_content_id'    =>  $this->cmsContentId
        ));

        // Model data
        $loadedData = array();
        foreach($cmsContentValues as $cmsContentValue) {
            $loadedData[$cmsContentValue->element_id] = $cmsContentValue->value;
        }

        $this->loadedFields = $this->loadFields($loadedData);

        return true;
    }

    /**
     * Loads a numerically indexed field array into the .stmcms fields dynamically
     *
     * @param $fieldsToLoad
     *
     * @return string
     */
    public function loadFields($fieldsToLoad, $saveMedia=false)
    {

        $templateHtml = str_get_html($this->content);
        $stmFields = $templateHtml->find(self::TEMPLATE_FIELD_SELECTOR);

        // Create a container for the values to be returned
        $loadedFields = array();

        $numericLegacyFields = array_values($this->fields);

        $fieldLegacyCounter = 0;
        foreach ($this->fields as $fieldId => $field) {
            $storedField =& $this->fields[$fieldId];

            /** We are dealing with legacy cms data */
            if (is_numeric(key($fieldsToLoad))) {
                $fieldValue = $fieldsToLoad[$fieldLegacyCounter];
                $stmField =& $stmFields[$fieldLegacyCounter];
                $fieldLegacyCounter++;
            } else {
                $fieldValue = $fieldsToLoad[$fieldId];
                foreach ($stmFields as $fieldIdx => $field) {
                    if ($field->id == $fieldId) {
                        $stmField =& $stmFields[$fieldIdx];
                    }
                }
            }

            // Stores the plain value to be stored
            $plainContentValue = null;

            if ($storedField->getIsImage()) {
//                $image = $storedField->saveImage($this->cmsContentId);
                $image = $fieldValue;
                $storedField->src = $stmField->src = $fieldValue = $plainContentValue = $image;

            } elseif ($storedField->isLink) {

                $storedField->href = $stmField->href = $plainContentValue = $fieldValue;

            } elseif ($storedField->isSourceDropDownList) {

                $storedField->{"data-id"} = $stmField->{"data-id"} = $plainContentValue = $fieldValue;

            } elseif ($storedField->isVideo) {
                $fieldValue = (isset($fieldValue['src'])) ? $fieldValue['src'] : $fieldValue;
                $videoHtml = $storedField->getVideo($fieldValue);
//                $plainContentValue = $stmField->src = $storedField->src = $fieldValue['src'];
                $plainContentValue = $stmField->src = $storedField->src = $fieldValue;
                $storedField->outertext = $stmField->outertext = $videoHtml->outertext;

            } elseif ($storedField->isAudio) {
                //@todo: is this saving every time even when just loading?
                $audio = $storedField->saveAudio($this->cmsContentId);
                $audio = ($audio) ? $audio : $fieldValue;
                $storedField->{"data-src"} = $stmField->{"data-src"} = $fieldValue = $plainContentValue = $audio;

            } else {

                if ($storedField->isInnerText) {
                    $storedField->innertext
                        =
                    $storedField->content = $stmField->innertext = $plainContentValue = trim($fieldValue);
                } else {
                    $storedField->outertext
                        =
                    $storedField->content = $stmField->outertext = $plainContentValue = trim($fieldValue);
                }
            }

            // Update the content (updates the outertext attributes so they reflect the src and href modifications)
            $this->content = $templateHtml->save();

            // Used to store in the data provider
//            $loadedFields[$fieldId]['content'] = $fieldValue;
//            $loadedFields[$fieldId]['plaintext'] = $plainContentValue;
//            $loadedFields[$fieldId]['raw'] = $fieldValue;
            $loadedFields[$fieldId] = $fieldValue;
        }

        return $loadedFields;
    }

    public function attributeNames()
    {
        return array('name', 'description', 'author', 'added');
    }

    /**
     * Return a path to the theme's template directory
     *
     * @return string
     */
    public function getThemeTemplatePath()
    {

        // We cannot get the domain's theme so we cannot build a path to the template
        if (!Yii::app()->theme->name) {
            return null;
        }

        if (!$this->_themeTemplatePath) {
            $this->_themeTemplatePath = Yii::getPathOfAlias(
                'webroot.themes.' . Yii::app()->theme->name . '.views.front.cms.templates'
            );
        }

        return $this->_themeTemplatePath;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array();
    }

    /**
     * Returns an array of the application's templates
     *
     * @return array template directory names
     */
    public function getAppTemplates()
    {

        if (!$this->_appTemplates) {
            $templates = array();
            foreach ($this->_appTemplateTypes as $templateType) {

                //@todo: INSERT HQ Modificaitons here!! For getting template files from the theme directory
                if(STM_HQ) {
                    //go to the theme directory to pull up the templates
                } else {

                }

                $templatePath = $this->getAppTemplatePath() . DS . $templateType;
                if (is_dir($templatePath)) {
                    $templateDirHandle = opendir($templatePath);
                    $templates[$templateType] = array();
                    while (($file = readdir($templateDirHandle)) !== false) {
                        if (!in_array($file, $this->_invalidFiles)) {
                            // If the file is a folder within templates then it is assumed to be a template
                            if (is_dir($templatePath . DS . $file)) {
                                array_push($templates[$templateType], $file);
                            }
                        }
                    }

                    $this->_appTemplates = $templates;

                    closedir($templateDirHandle);
                }
            }
        }

        return $this->_appTemplates;
    }

    /**
     * Return a path to the applications's template directory
     *
     * @return string
     */
    public function getAppTemplatePath()
    {
        if (!$this->_appTemplatePath) {
            $this->_appTemplatePath = Yii::getPathOfAlias('admin_module.views.cms.templates');
        }

        return $this->_appTemplatePath;
    }

    public function setAppTemplatePath($path)
    {
        $this->_appTemplatePath = $path;
    }

    public function getThemeTemplates()
    {
        $templates = array();
        foreach ($this->_appTemplateTypes as $templateType) {

            $templatePath = $this->getThemeTemplatePath() . DS . $templateType;
            if (is_dir($templatePath)) {
                $templateDirHandle = opendir($templatePath);
                $templates[$templateType] = array();
                while (($file = readdir($templateDirHandle)) !== false) {
                    if (!in_array($file, $this->_invalidFiles)) {
                        // If the file is a folder within templates then it is assumed to be a template
                        if (is_dir($templatePath . DS . $file)) {
                            array_push($templates[$templateType], $file);
                        }
                    }
                }

                $this->_themeTemplates = $templates;

                closedir($templateDirHandle);
            }
        }

        return $this->_themeTemplates;
    }

    public function getTemplates()
    {

        $templates = CMap::mergeArray($this->appTemplates, $this->themeTemplates);

        if (!$templates) {
            return null;
        }

        foreach ($templates as $template) {
            // Cleanup the template names
            $template = array_map(
                array($this, 'formatTemplateNames'), $template
            );
        }

        return $templates;
    }

    /**
     * Used as a mapping function to clean up the template names
     *
     * @param  string $templateName The Template Name
     *
     * @return string
     */
    protected function formatTemplateNames($templateName)
    {

        return ucwords(str_replace('_', ' ', $templateName));
    }
}
