<?php

class CmsTemplateField extends CFormModel {

	const TAG_TYPE_HEADER = 'header';
	const TAG_TYPE_CONTENT = 'content';
	const TAG_TYPE_LINK = 'link';
	const TAG_TYPE_IMAGE = 'image';
	const TAG_TYPE_VIDEO = 'video';
    const TAG_TYPE_BUTTON = 'button';
    const TAG_TYPE_AUDIO = 'audio';
    const TAG_TYPE_SOURCE_DROPDOWNLIST = 'sourceDropDownList';

	const TYPE_YOUTUBE = 'youtube';

	const YOUTUBE_EMBED_STRING           = '<iframe id=":id" class="stmcms" title=":title" data-thumbnail=":thumbnail" data-type=":type" width=":width" height=":height" src="http://www.youtube.com/embed/:key?wmode=opaque&amp;modestbranding=1&amp;autohide=1&amp;color=white&amp;hd=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
	const YOUTUBE_EMBED_THUMBNAIL_STRING = '<img class="stmcms" title=":title" data-type=":type" data-thumbnail=":thumbnail" src="http://i1.ytimg.com/vi/:key/:thumbnailImage.jpg" width=":width" height=":height" />';

	//Adding in a Youtube Video Placeholder so that video templates which may not be completely filled with Videos
	//in every available slot may still elect to include a video at a later time. This is to resolve an issue where
	//video templates were having their video iframe containers removed from the template's DOM whenever there was
	//no source URL present. (see also STM-275: http://goo.gl/DpHQP) As we add other video providers we'll need
	//other placeholders for each provider (perhaps we can key these placeholders and embed string by video provider,
	//ex: Vimeo, Youtube, TwitVid, Videojug, etc.)
	const YOUTUBE_VIDEO_PLACEHOLDER_KEY = 'OCWj5xgu5Ng';

	public $element; // instance of a element from simplehtmldom

	public $contentRaw;
	public $content;

	public $headerTags  = array('h1', 'h2', 'h3', 'h4', 'h5');
	public $inlineTags  = array();
	public $contentTags = array('div', 'span', 'p');
	public $linkTags    = array('a');
    public $buttonTags    = array('button');
	public $rowClass;

	// Holder for data-(\w+) style html attributes
	protected $dataAttrs = array();

	/**
	 * Allows us to get any attribute from the element found through this model
	 * @param  string $name
	 * @return mixed
	 */
	public function __get($name) {

		if (property_exists($this->element, $name)) {
			return $this->element->$name;
		}

		$magicGetterName = 'get' . ucwords($name);
		if (method_exists($this, $magicGetterName)) {
			return $this->{$magicGetterName}();
		}

		if (isset($this->element->{$name})) {
			return $this->element->{$name};
		}

		return parent::__get($name);
	}

    public function __set($name, $value) {

        $magicSetterName = 'set' . ucwords($name);
        if (method_exists($this->element, $magicSetterName)) {
            return $this->element->{$magicSetterName}($name, $value);
        }

        if (isset($this->element->{$name})) {
            return $this->element->$name = $value;
        }

        return parent::__set($name, $value);
    }

	public function getTitle() {

		$title = ($this->element->attr['stmtemplate-title']) ? $this->element->attr['stmtemplate-title'] : $this->label;
		return $title . ':';
	}

	public function getDataAttr($dataAttrName) {

		if (!$this->dataAttrs) {

			preg_match_all('/data-(\w+)="(\w+)"+/', $this->element->outertext, $matches);
			if ($matches) {

				$attrNames = $matches[1];
				$attrVals  = $matches[2];

				foreach($attrNames as $attrIndex => $attrName) {

					$this->dataAttrs[$attrName] = $attrVals[$attrIndex];
				}
			}
		}

		if ($this->dataAttrs[$dataAttrName])
			return $this->dataAttrs[$dataAttrName];

		return null;
	}

	public function getLabel() {

		switch ($this->type) {

			case self::TAG_TYPE_HEADER:
				return 'Header';
			break;

			case self::TAG_TYPE_CONTENT:
				return 'Content';
			break;

			case self::TAG_TYPE_IMAGE:
				return 'Image';
			break;

			case self::TAG_TYPE_LINK:
				return 'Link';
			break;

			case self::TAG_TYPE_VIDEO:
				return 'Video URL';
			break;

            case self::TAG_TYPE_BUTTON:
                return 'Button Text';
                break;

            case self::TAG_TYPE_AUDIO:
                return 'Audio';
                break;

            case self::TAG_TYPE_SOURCE_DROPDOWNLIST:
                return 'Source Drop Down List';
                break;

			default:
				return 'Section';
			break;
		}
	}

	public function getType() {

		$tag = $this->element->tag;

		if (in_array($tag, $this->headerTags)) {
			return self::TAG_TYPE_HEADER;
		}

		if (in_array($tag, $this->contentTags)) {
			return self::TAG_TYPE_CONTENT;
		}

		if ($tag === 'iframe' || ($tag === 'img' && $this->getDataAttr('thumbnail'))) {
			return self::TAG_TYPE_VIDEO;
		}

		if ($tag === 'img' && !$this->getDataAttr('thumbnail')) {
			return self::TAG_TYPE_IMAGE;
		}

		if ($tag === 'a') {
			return self::TAG_TYPE_LINK;
		}

        if ($tag === 'button') {
            return self::TAG_TYPE_BUTTON;
        }

        if ($tag === 'audio') {
            return self::TAG_TYPE_AUDIO;
        }

        if ($tag === 'select' && $this->element->id == 'source-id') {
            return self::TAG_TYPE_SOURCE_DROPDOWNLIST;
        }

        return null;
	}

	/**
	 * Save the image if one is added in a template
	 * @return image path
	 */
	public function saveImage($cmsContentId) {

		$image = CUploadedFile::getInstanceByName($this->name);
//        preg_match('\[(.*)\]$', $this->name, $matches);
        $filename = str_replace(array('CmsTemplateField','[',']'), '', $this->name);
        $filename = $filename.'.'.$image->extensionName;

		// Don't attempt to save a null image
		if (!$image) {
			return false;
		}

        // 1MB
        if ($image->size >= 2097152) {
            Yii::app()->getUser()->setFlash('error', 'Could not upload image, '.$image->name.' please upload a image less than 2Mb.');
            return false;
        }

        $cmsRelativePath = $this->getImageRelativeRootPath().DS.$cmsContentId;
		$cmsPath = dirname(Yii::app()->getBasePath()).$cmsRelativePath;

		if (!is_dir($cmsPath)) {
			mkdir($cmsPath, 0755, true);
		}

		$saved = $image->saveAs($cmsPath . DS . $filename);

		return $cmsRelativePath.DS.$filename;
	}

    /**
     * Save the audio if one is added in a template
     * @return audio path
     */
    public function saveAudio($cmsContentId) {

        $audio = CUploadedFile::getInstanceByName($this->name);
        $filename = str_replace(array('CmsTemplateField','[',']'), '', $this->name);
        $filename = $filename.'.'.$audio->extensionName;

        // Don't attempt to save a null image
        if (!$audio) {
            return false;
        }

        // 100MB
        if ($audio->size >= 104857600) {//  (100 Mb) //52428800
            $phpMaxFilesize = ini_get('upload_max_filesize');
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Cms Template audio file too large: '.round($audio->size/1024/1024).' Mb', CLogger::LEVEL_ERROR);
            Yii::app()->getUser()->setFlash('error', 'Could not upload audio, '.$audio->name.' please upload an audio less than 100Mb. PHP upload_max_filesize is '.$phpMaxFilesize.'.');
            return false;
        }

        $cmsRelativePath = $this->getImageRelativeRootPath().DS.$cmsContentId;
        $cmsPath = dirname(Yii::app()->getBasePath()).$cmsRelativePath;

        if (!is_dir($cmsPath)) {
            mkdir($cmsPath, 0777, true);
        }

        //@todo: need error handling for if save not successful
        //@todo: KNOWN BUG: this gets called twice times and deletes temp file, put in temp fix for that.
        if(file_exists($audio->tempName)) {
            if (file_exists($cmsPath . DS . $filename)) {
                unlink($cmsPath . DS . $filename);
            }

            $saved = $audio->saveAs($cmsPath . DS . $filename);
        }

        return $cmsRelativePath.DS.$filename;
    }

    public function getImageRelativeRootPath() {
        return DS.'images'.DS.'cms';
    }
	/**
	 * Takes an array of video data and returns the video's html string
	 * @param  array $videoData (src, width, height)
	 * @return string
	 */
	public function getVideo($videoData) {

		// Save video data to a namespace
//		if(!is_array($videoData)) ////@todo: was here temp to fix breaks in cms contents
//			$videoData = array('src'=>$videoData);

		$videoSrcUrl = isset($videoData)? $videoData : '';
		$videoWidth  = $videoHeight = 0;

		// Check to see the form has sent us any dimensions
//		if (isset($videoData['width']) && isset($videoData['height'])) {
//			$videoWidth  = $videoData['width'];
//			$videoHeight = $videoData['height'];
//		}

		// If the width and height attributes are not available, then use the element defaults
		if (!$videoWidth && !$videoHeight) {
			$videoWidth  = $this->width;
			$videoHeight = $this->height;
		}

		// Only video type supported currently is youtube so go with that in mind
		if ($this->getDataAttr('type') == self::TYPE_YOUTUBE) {

			$youtubeKey = $this->getYoutubeKey($videoSrcUrl);
			if (!$youtubeKey) {
				$youtubeKey = self::YOUTUBE_VIDEO_PLACEHOLDER_KEY;
			}

			if ($this->getDataAttr('thumbnail'))
				$youtubeHtml = $this->getYoutubeVideoThumbnail($youtubeKey, $videoWidth, $videoHeight);
			else
				$youtubeHtml = $this->getYoutubeVideoHTML($this->element->id, $youtubeKey, $videoWidth, $videoHeight);

			return $youtubeHtml;
		}

		return null;
	}

	protected function getYoutubeKey($videoSrcUrl) {

		$keyPatternQueue = array(
			'/.*v=([-[:word:]]*)/is',
			'/.*embed\/([-[:word:]]*)/is',
			'/.*vi\/([-[:word:]]*)/is',
            '/.*youtu.be\/([-[:word:]]*)/is'
		);

		foreach($keyPatternQueue as $keyPattern) {
			preg_match($keyPattern, $videoSrcUrl, $keyMatches);

			// Return the first offset (the youtube key)
			if ($keyMatches[1])
				return $keyMatches[1];
		}

		return null;
	}

	protected function getYoutubeVideoHTML($id, $youtubeKey, $width=560, $height=315) {

		$youtubeHtml = strtr(self::YOUTUBE_EMBED_STRING, array(
            ':id'        => $id,
			':width'     => $width,
			':height'    => $height,
			':key'       => $youtubeKey,
			':thumbnail' => $this->getDataAttr('thumbnail'),
			':type'      => $this->getDataAttr('type'),
			':title'	 => str_replace(':', '', $this->title),
		));

		$html = str_get_html($youtubeHtml);
		$iframe = $html->find('iframe', 0);
		$iframe->class = $this->element->class;

		return $iframe;
	}

	protected function getYoutubeVideoThumbnail($youtubeKey, $width=120, $height=90) {

		$youtubeThumbnailHtml = strtr(self::YOUTUBE_EMBED_THUMBNAIL_STRING, array(
			':width'          => $width,
			':height'         => $height,
			':key'            => $youtubeKey,
			':thumbnailImage' => $defaultYoutubeThumbnail='mqdefault',
			':thumbnail'      => $this->getDataAttr('thumbnail'),
			':type'           => $this->getDataAttr('type'),
			':title'		  => str_replace(':', '', $this->title),
		));

		$html = str_get_html($youtubeThumbnailHtml);
		$image = $html->find('img', 0);
		$image->class = $this->element->class;

		return $html;
	}

	public function getName() {
		return __CLASS__ .'['.$this->id.']';
	}

	public function getIsHeaderTag() {
		return $this->getType() === self::TAG_TYPE_HEADER;
	}

	public function getIsImage() {
		return $this->getType() === self::TAG_TYPE_IMAGE;
	}

	public function getIsLink() {
		return $this->getType() === self::TAG_TYPE_LINK;
	}

	public function getIsVideo() {
		return $this->getType() === self::TAG_TYPE_VIDEO;
	}

    public function getIsAudio() {
        return $this->getType() === self::TAG_TYPE_AUDIO;
    }

    public function getIsSourceDropDownList() {
        return $this->getType() === self::TAG_TYPE_SOURCE_DROPDOWNLIST;
    }

    /**
	 * Returns whether or not we should inject the content in the inner text
	 * @return boolean
	 */
	public function getIsInnerText() {
		return in_array($this->element->tag, CMap::mergeArray($this->headerTags, $this->contentTags, $this->inlineTags, $this->buttonTags));
	}
}