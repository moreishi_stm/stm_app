<?php

/**
 * CallReportingStatusForm class.
 * CallReportingStatusForm is the data structure for updating call status.
 */
class CallReportingStatusForm extends CFormModel
{
    public $call_hunt_group_session_id;
    public $task_type_id;
    public $exclude_from_stats;
    public $exclude_reason;
    public $exclude_reason_other;
    public $notes;

    /**
     * Declares the validation rules.
     * The rules state that email and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array(
                'call_hunt_group_session_id, notes, task_type_id, exclude_from_stats, exclude_reason',
                'required'
            ),
            array(
                'exclude_from_stats',
                'boolean'
            ),
            array(
                'call_hunt_group_session_id, notes, task_type_id, exclude_from_stats, exclude_reason, exclude_reason_other',
                'safe'
            ),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'exclude_from_stats' => 'Exclude from Status',
            'exclude_reason' => 'Reason',
            'exclude_reason_other' => 'Other Reason',
        );
    }
}
