<?php

	/**
	 * This is the model class for table "badge_requirements".
	 *
	 * The followings are the available columns in table 'badge_requirements':
	 *
	 * @property integer         $id
	 * @property integer         $badge_id
	 * @property integer         $badge_point_type_id
	 * @property integer         $points_required
	 * @property integer         $updated_by
	 * @property string          $updated
	 * @property integer         $added_by
	 * @property string          $added
	 *
	 * The followings are the available model relations:
	 * @property Contacts        $addedBy
	 * @property Badges          $badge
	 * @property BadgePointTypes $badgePointType
	 * @property Contacts        $updatedBy
	 */
	class BadgeRequirements extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return BadgeRequirements the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'badge_requirements';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'badge_id, badge_point_type_id, points_required, updated_by, updated, added_by, added',
					'required'
				),
				array(
					'badge_id, badge_point_type_id, points_required, updated_by, added_by',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, badge_id, badge_point_type_id, points_required, updated_by, updated, added_by, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
				'badge' => array(
					self::BELONGS_TO,
					'Badges',
					'badge_id'
				),
				'badgePointType' => array(
					self::BELONGS_TO,
					'BadgePointTypes',
					'badge_point_type_id'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'badge_id' => 'Badge',
				'badge_point_type_id' => 'Badge Point Type',
				'points_required' => 'Points Required',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'added_by' => 'Added By',
				'added' => 'Added',
			);
		}

		public static function getBadgesIdsByRequiedPointType($pointTypeId) {
			$BadgeRequirments = BadgeRequirements::model()->byPointType($pointTypeId)->distinctBadgeIdOnly()->findAll();

			if ($BadgeRequirments) {
				$badgeIds = array();
				foreach ($BadgeRequirments as $BadgeRequirment) {
					array_push($badgeIds, $BadgeRequirment->badge_id);
				}
			}

			return $badgeIds;
		}

		public function byPointType($pointTypeId) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'badge_point_type_id=:badge_point_type_id',
					'params' => array(':badge_point_type_id' => $pointTypeId)
				)
			);

			return $this;
		}

		public function distinctBadgeIdOnly() {
			$this->getDbCriteria()->mergeWith(array(
					'select' => 'badge_id',
					'distinct' => true
				)
			);

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('badge_id', $this->badge_id);
			$criteria->compare('badge_point_type_id', $this->badge_point_type_id);
			$criteria->compare('points_required', $this->points_required);
			$criteria->compare('updated_by', $this->updated_by);
			$criteria->compare('updated', $this->updated, true);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}