<?php

	/**
	 * This is the model class for table "saved_homes".
	 *
	 * The followings are the available columns in table 'saved_homes':
	 *
	 * @property integer      $id
     * @property integer      $mls_board_id
	 * @property integer      $contact_id
	 * @property integer      $viewed_home_id
	 * @property integer      $status_ma
	 * @property string       $notes
	 * @property string       $ip
	 * @property string       $added
	 * @property integer      $added_by
	 * @property integer      $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts     $contact
	 * @property ViewedHomes  $viewedHome
	 * @property HomesShown[] $homesShowns
	 */
	class SavedHomes extends StmBaseActiveRecord {

		const COOKIE_KEY = 'saved_homes';

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return HomesSaved the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'saved_homes';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'mls_board_id, contact_id, status_ma, added_by, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'ip',
					'length',
					'max' => 50
				),
				array(
					'notes',
					'length',
					'max' => 255
				),
				array(
					'added',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, listing_id, status_ma, notes, ip, added, added_by, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( // 'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
				// 'listing' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
				// 'homesShowns' => array(self::HAS_MANY, 'HomesShown', 'saved_homes_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'listing_id' => 'Listing ID',
				'status_ma' => 'Status Ma',
				'notes' => 'Notes',
				'ip' => 'IP',
				'added' => 'Added',
				'added_by' => 'Added By',
				'is_deleted' => 'Is Deleted',
			);
		}

        public function beforeSave()
        {
            if ($this->isNewRecord && $this->listing_id) {
                // check to see if already exists for saved listing
                if($this->countByAttributes(array('contact_id'=>$this->contact_id, 'listing_id'=>$this->listing_id))) {
                    Yii::log(__CLASS__.' (:'.__FILE__.') Existing saved home was attempted to dupe save. Review this log and prevent future attempts via GUI and validation.');
                    return false;
                }
            }
            return parent::beforeSave();
        }

		/**
		 * getAll
		 * Loads activeDataProvider with conditions and returns ready for use (ex. CGridview)
		 * Called in: Activities Details Widget (bottom area for contacts, tnx, closings view)
		 *
		 * @param int $contactId
		 *
		 * @return mixed activeDataProvider
		 */
		public static function getAll($contactId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);
			$dataProvider = new CActiveDataProvider('SavedHomes', array(
				'criteria' => $Criteria
			));

			return $dataProvider;
		}

		public static function getListingIds() {
			$listingIds = array();
			if (Yii::app()->user->isGuest) {
				if (isset(Yii::app()->request->cookies[self::COOKIE_KEY]) && Yii::app()->request->cookies[self::COOKIE_KEY]->value != 'null') {
					$cookieJSON = Yii::app()->request->cookies[self::COOKIE_KEY]->value;
					$homesSavedData = CJSON::decode($cookieJSON);

					foreach ($homesSavedData as $key => $listingId) {
						array_push($listingIds, $listingId);
					}
				}
			} else {
                $listingIds = Yii::app()->db->createCommand("select listing_id from saved_homes where contact_id=".Yii::app()->user->id)->queryColumn();
			}

			return $listingIds;
		}

		public function getProperty()
        {
            if(Yii::app()->user->hasMultipleBoards() && $this->mls_board_id) {
                $mlsBoard = MlsBoards::model()->findByPk($this->mls_board_id);
                $property = MlsProperties::model($mlsBoard->prefixedName)->findByPk($this->listing_id);
            }
            else {
                $boardName = Yii::app()->user->getBoard()->getPrefixedName();
                $property = MlsProperties::model($boardName)->findByPk($this->listing_id);
            }

            return $property;
		}

		/**
		 * getCount
		 * Returns the # of Homes Saved for a contact. Used for diplayed number in JUI Tab
		 *
		 * @param int     $contactId
		 * @param boolean $tabFormat if true, formats in "(1)" for tab display
		 *
		 * @return int $count
		 */
		public static function getCount($contactId = null, $tabFormat = false) {
			if (Yii::app()->user->isGuest) {
				if (!isset(Yii::app()->request->cookies[self::COOKIE_KEY]) || Yii::app()->request->cookies[self::COOKIE_KEY]->value == 'null') {
					$count = 0;
				} else {
					$cookieJSON = Yii::app()->request->cookies[self::COOKIE_KEY]->value;
					$homesSavedData = CJSON::decode($cookieJSON);
					$count = count($homesSavedData);
				}
			} else {
				if ($contactId == null) {
					$contactId = Yii::app()->user->id;
				}

				$Criteria = new CDbCriteria;
				$Criteria->condition = 'contact_id=:contact_id';
				$Criteria->params = array(':contact_id' => $contactId);
				$count = SavedHomes::model()->count($Criteria);
			}

			return $count = ($tabFormat) ? '(' . $count . ')' : $count;
		}

		/**
		 * isHomeSaved handles checking if a home is saved for both user and guest(cookie)
		 *
		 * @param  int $listingId
		 * @param  int $contactId
		 *
		 * @param bool $includeDeletedRecord
		 *
		 * @return boolean
		 */
		public static function isHomeSaved($listingId, $contactId = null, $mlsBoardId=null) {
			if (Yii::app()->user->isGuest) {
				if (!isset(Yii::app()->request->cookies[self::COOKIE_KEY]) || Yii::app()->request->cookies[self::COOKIE_KEY]->value == 'null') {
					return false;
				}

				$cookieJSON = Yii::app()->request->cookies[self::COOKIE_KEY]->value;
				$homesSavedData = CJSON::decode($cookieJSON);

				if (array_key_exists($listingId, $homesSavedData)) {
					return true;
				} else {
					return false;
				}
			} else {
                $condition = 'contact_id=:contact_id AND listing_id=:listing_id AND is_deleted=0';
                $savedHomeParams = array(
                    ':contact_id' => $contactId,
                    ':listing_id' => $listingId,
                );

                if($mlsBoardId) {
                    $condition .= ' AND (mls_board_id=:mls_board_id OR mls_board_id IS NULL)';
                    $savedHomeParams[':mls_board_id'] = $mlsBoardId;
                }
                return SavedHomes::model()->skipSoftDeleteCheck()->exists($condition, $savedHomeParams);
			}
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
            $criteria->compare('mls_board_id', $this->mls_board_id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('listing_id', $this->listing_id);
			$criteria->compare('status_ma', $this->status_ma);
			$criteria->compare('notes', $this->notes, true);
			$criteria->compare('ip', $this->ip, true);
			$criteria->compare('added', $this->added, true);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('is_deleted', $this->is_deleted);

            $boardName = Yii::app()->getUser()->getBoard()->getPrefixedName();
            $criteria->join = 'LEFT JOIN stm_mls.'.$boardName.'_properties ON t.listing_id='.$boardName.'_properties.listing_id';
            $criteria->addCondition($boardName.'_properties.listing_id IS NOT NULL');

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}