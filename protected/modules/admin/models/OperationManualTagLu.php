<?php

/**
 * This is the model class for table "operation_manual_tag_lu".
 *
 * The followings are the available columns in table 'operation_manual_tag_lu':
 * @property integer $operation_manual_id
 * @property integer $operation_manual_tag_id
 */
class OperationManualTagLu extends StmBaseActiveRecord
{
	public $idCollection;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OperationManualTagLu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'operation_manual_tag_lu';
	}

	public function primaryKey(){
		return array('operation_manual_id', 'operation_manual_tag_id');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('operation_manual_id, operation_manual_tag_id', 'required'),
			array('operation_manual_id, operation_manual_tag_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('operation_manual_id, operation_manual_tag_id, idCollection', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'operationManual' => array(self::BELONGS_TO,'OperationManuals','operation_manual_id'),
			'tag' => array(self::BELONGS_TO,'OperationManualTags','operation_manual_tag_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'operation_manual_id' => 'Operation Manual',
			'operation_manual_tag_id' => 'Operation Manual Tag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('operation_manual_id',$this->operation_manual_id);
		$criteria->compare('operation_manual_tag_id',$this->operation_manual_tag_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}