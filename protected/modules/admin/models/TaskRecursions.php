<?php

/**
 * This is the model class for table "task_recursions".
 *
 * The followings are the available columns in table 'task_recursions':
 * @property string $id
 * @property string $recur_type
 * @property integer $recur_repeat_every
 * @property integer $recur_end_after_occurrences
 * @property string $recur_start_date
 * @property string $recur_end_date
 * @property string $added
 * @property integer $added_by
 * @property string $updated
 * @property integer $updated_by
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Tasks[] $tasks
 */
class TaskRecursions extends StmBaseActiveRecord
{
    const PRE_POPULATE_MONTHS = 2;
    const MAX_TASK_CREATION = 30;

    const RECUR_TYPE_ONE_TIME_ONLY = 'One time only';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TaskRecursions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'task_recursions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('recur_start_date, recur_type','required'),
            array('recur_repeat_every, recur_end_after_occurrences, added_by, updated_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('recur_type', 'length', 'max'=>32),
            array('recur_start_date, recur_end_date, added, updated', 'safe'),
            array('recur_end_date', 'validateEndDate'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, recur_type, recur_repeat_every, recur_end_after_occurrences, recur_start_date, recur_end_date, added, added_by, updated, updated_by, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tasks' => array(self::HAS_MANY, 'Tasks', 'task_recursion_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
		return array(
			'id'							=>	'Task Recursion',
			'recur_type'					=>	'Recurring Type',
			'recur_repeat_every'			=>	'Repeat Every',
			'recur_end_after_occurrences'	=>	'End After Occurrences',
			'recur_start_date'				=>	'Start Date',
			'recur_end_date'				=>	'End Date',
			'added'					        =>	'Date Added',
			'added_by'				        =>	'Added By',
			'updated'						=>	'Date Updated',
			'updated_by'					=>	'Updated By',
			'is_deleted'					=>	'Deleted'
		);
    }

    public function validateEndDate($attribute, $params)
    {
        if (!empty($this->recur_start_date) && !empty($this->recur_end_date) && date('Y-m-d',strtotime($this->recur_start_date)) > date('Y-m-d',strtotime($this->recur_end_date))) {
            $this->addError($attribute, 'End Date cannot be less than start date.');
        }
        if (!empty($this->recur_end_date) && date('Y-m-d',strtotime($this->recur_end_date)) < date('Y-m-d')) {
            $this->addError($attribute, 'End Date cannot be less than today.');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function createRecursionTasks(Tasks &$originalTask, $periodEnd=null)
    {
        // if end_date is less than today or the period end date, it doesn't need anymore handling, it's old ... ignore!
        if(($this->recur_end_date !== null) && ($this->recur_end_date < date('Y-m-d') || $this->recur_end_date < $periodEnd)) {
            return;
        }

        // If we don't have a recur start date, use the due date of the original task record
        if(!empty($this->recur_start_date) && !in_array($this->recur_start_date, array('1969-12-31 00:00:00', '1969-12-31'))) {
            $startDate = new DateTime($this->recur_start_date);
//            $startDate->sub(new DateInterval('P1D')); //@todo: why subtract 1 day?
        }
        else {
            $startDate = new DateTime($originalTask->due_date);

            //@todo: $this->recur_start_date can be null, do anything? let beforesave handle it?

        }

        // If we have a period, then we set the end date to the last day in that period, also get things if we need them for checking
        if(is_null($periodEnd)) {

            // If we don't have a recur end date, generate the date based on the max allowable date
            if($this->recur_end_date && !in_array($this->recur_end_date, array('1969-12-31 00:00:00', '1969-12-31'))) {
                $endDate = new DateTime($this->recur_end_date);
            }
            else {
                $endDate = new DateTime(date('Y-m-d'));
                $endDate->add(new DateInterval($this->getPrepopulateDateIntervalByFrequency()));
            }
        }
        else {

            // End date is the period end, unless we have a recur end date that comes before the period end date
            $endDate = new DateTime($periodEnd);

            // Use the inputted period end date, if they specified one
            if($this->recur_end_date && !in_array($this->recur_end_date, array('1969-12-31 00:00:00', '1969-12-31'))) {

                // If the user has chosen a date older than the end date for the period, we want to use that as the end date
                $recurEndDate = new DateTime($this->recur_end_date);
                if($recurEndDate < $endDate) {
                    $endDate = $recurEndDate;
                }
            }
        }

        // Remove time from dates to avoid calculation issues
        $startDate = new DateTime($startDate->format('Y-m-d'));
        $endDate = new DateTime($endDate->format('Y-m-d'));

        // compare to today's date, used for all recur types
        $todayDate = new DateTime(date('Y-m-d'));
        $diffInterval = $endDate->diff($startDate, true);

        if(in_array($startDate, array('1969-12-31 00:00:00', '1969-12-31')) || in_array($endDate, array('1969-12-31 00:00:00', '1969-12-31'))) {
            Yii::log(__CLASS__.' ('.__LINE__.'): Task recursion ERROR: start/end date is invalid! Data: ' . print_r($this->attributes, true).' Start Date:  '.$startDate->format('Y-m-d').'| End Date: '.$endDate->format('Y-m-d'), CLogger::LEVEL_ERROR);
            Yii::app()->plivo->sendMessage('9043433200', 'Task Recursion ERROR: Invalid start/end date. User: '.Yii::app()->user->contact->fullName.'. See error log ASAP.');
            throw new Exception("This shouldn't be happening");
        }

        // Get difference in days between start and end date
        switch($this->recur_type) {
            case 'Daily':
            case 'Every weekday (Monday to Friday)':
                $diff = ceil($diffInterval->format('%a')) + (365 * intval($diffInterval->format('%y')));
                break;

            case 'Weekly':
                $diff = ceil($diffInterval->format('%a') / 7) + (52 * intval($diffInterval->format('%y')));
                break;

            case 'Bi-Weekly':
                $diff = ceil($diffInterval->format('%a') / 14) + (26 * intval($diffInterval->format('%y')));
                break;

            case 'Monthly':
                $diff = intval($diffInterval->format('%m')) + (12 * intval($diffInterval->format('%y')));
                break;

            case 'Bi-Monthly':
                $diff = intval($diffInterval->format('%m') / 2) + (6 * intval($diffInterval->format('%y')));
                break;

            case 'Quarterly':
                $diff = intval($diffInterval->format('%m') / 3) + (3 * intval($diffInterval->format('%y')));
                break;

            case 'Semi-Annually':
                $diff = intval($diffInterval->format('%m') / 6) + (2 * intval($diffInterval->format('%y')));
                break;

            case 'Yearly':
                $diff = ceil($diffInterval->format('%y'));
                break;
        }

        // Recur types to datetime intervals
        $intervalMap = array(
            'Daily'								=>	'P1D',
            'Every weekday (Monday to Friday)'	=>	'P1D',
            'Weekly'							=>	'P1W',
            'Bi-Weekly'							=>	'P2W',
            'Monthly'							=>	'P1M',
            'Bi-Monthly'						=>	'P2M',
            'Quarterly'							=>	'P3M',
            'Semi-Annually'						=>	'P6M',
            'Yearly'							=>	'P1Y'
        );

        // Don't add new things if this is one time only
        if($this->recur_type == 'One time only') { //@todo: delete this task recursion and any other task associated with it
            return;
        }

        $existingTaskDates = $originalTask->getExistingTaskDates($this->id);
        // Iterate through each day
        $currentDate = $startDate;
        $createNewTaskCount = 0;
        for($i = 1; $i <= $diff; $i++) {

            // Increment date, we skip the first occurrence (it's the opening task)
            $currentDate->add(new DateInterval($intervalMap[$this->recur_type]));

            // Do checking here for if the task already exists on this date
            if(in_array($currentDate->format('Y-m-d'), $existingTaskDates)) {
                continue;
            }

            // Special handling for weekdays (mon-fri)
            if($this->recur_type == 'Every weekday (Monday to Friday)') {

                // Make sure this is monday - friday, skip saturday and sunday iterations.
                if(in_array($currentDate->format('w'), array(0, 6))) {
                    continue;
                }
            }

            // ignore any tasks before today
            if($currentDate->format('Y-m-d') < $todayDate->format('Y-m-d')) {
                continue;
            }

            // Add the iteration
            $task = new Tasks();
            $task->setAttributes(array(
                    'account_id'		    =>	$originalTask->account_id,
                    'status_ma'			    =>	$originalTask->status_ma,
                    'component_type_id'	    =>	$originalTask->component_type_id,
                    'component_id'		    =>	$originalTask->component_id,
                    'is_priority'		    =>	$originalTask->is_priority,
                    'task_type_id'		    =>	$originalTask->task_type_id,
                    'assigned_to_id'	    =>	$originalTask->assigned_to_id,
                    'assigned_by_id'	    =>	$originalTask->assigned_by_id,
                    'description'		    =>	$originalTask->description,
                    'operation_manual_id'   =>	$originalTask->operation_manual_id,
                    'due_date'			    =>	$currentDate->format('Y-m-d H:i:s'),
                    'task_recursion_id'	    =>	$this->id
                ));

            // Attempt to save
            if(!$task->save()) {
                Yii::log(__FILE__.'(Line: '.__LINE__.') Unable to save task recursion! Error: '.print_r($task->getErrors(), true).' Tasks Attributes'.print_r($task->attributes, true), CLogger::LEVEL_ERROR);
                throw new Exception(print_r($task->getErrors(), true));
            }
            else {
                $createNewTaskCount++;
                // check to see if new task creation is greater than limit
                if($createNewTaskCount > self::MAX_TASK_CREATION) {
                    Yii::app()->plivo->sendMessage('9043433200', 'Task Recursion ERROR: '.$diff.' recursion task attempted by '.Yii::app()->user->contact->fullName.'. See error log ASAP.');

                    throw new Exception(__CLASS__." (:".__LINE__.") Task Recursion ERROR: Exceeding new task creation limit of ".self::MAX_TASK_CREATION.". $diff attempted by ".Yii::app()->user->contact->fullName.'. Start Date:  '.$startDate->format('Y-m-d').'| End Date: '.$endDate->format('Y-m-d').'Task Recursion Attributes: '.print_r($this->attributes, true));
                }
            }
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('recur_type',$this->recur_type,true);
        $criteria->compare('recur_repeat_every',$this->recur_repeat_every);
        $criteria->compare('recur_end_after_occurrences',$this->recur_end_after_occurrences);
        $criteria->compare('recur_start_date',$this->recur_start_date,true);
        $criteria->compare('recur_end_date',$this->recur_end_date,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function getPrepopulateDateIntervalByFrequency()
    {
        switch($this->recur_type)
        {
            case 'Daily':
            case 'Every weekday (Monday to Friday)':
                $dateInterval = 'P1W';
                break;

            case 'Weekly':
                $dateInterval = 'P4W';
                break;

            case 'Bi-Weekly':
                $dateInterval = 'P8W';
                break;

            case 'Monthly':
                $dateInterval = 'P4M';
                break;

            case 'Bi-Monthly':
                $dateInterval = 'P8M';
                break;

            case 'Quarterly':
                $dateInterval = 'P1Y';
                break;

            case 'Semi-Annually':
                $dateInterval = 'P2Y';
                break;

            case 'Yearly':
                $dateInterval = 'P3Y';
                break;

            default:
                throw new Exception(__CLASS__." (:".__LINE__.") Invalid Task Recursion Frequency. Task Recursion Attributes: ".print_r($this->attributes, true));
        }

        return $dateInterval;
    }

    /**
     * Before Save
     *
     * Performs various operations on data right before save is called
     * @return bool !?
     */
    protected function beforeSave()
    {
		// Date Conversions
        $this->recur_start_date = in_array(date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($this->recur_start_date)), array('1969-12-31', null)) ? null : date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($this->recur_start_date));
		$this->recur_end_date = in_array(date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($this->recur_end_date)), array('1969-12-31', null)) ? null : date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($this->recur_end_date));

        if($this->isNewRecord) {
            //Attach User who Created Task
            $this->added_by = Yii::app()->user->id;
            $this->added = new CDbExpression('NOW()');
        }

		//Attach User who Created Task
		$this->updated_by = Yii::app()->user->id;
		$this->updated = new CDbExpression('NOW()');

        // Chain parent method
		return parent::beforeSave();
	}
}