<?php

	/**
	 * This is the model class for table "leads".
	 *
	 * The followings are the available columns in table 'leads':
	 *
	 * @property integer $id
	 * @property integer $lead_route_id
	 * @property integer $component_id
	 * @property integer $assigned_to_id
	 */
	class Lead extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Lead the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'leads';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'lead_route_id, component_id, assigned_to_id',
					'required'
				),
				array(
					'id, lead_route_id, component_id, assigned_to_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, lead_route_id, component_id, assigned_to_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'transaction' => array(
					self::BELONGS_TO,
					'Transactions',
					'component_id'
				),
			);
		}

		/**
		 * byLeadRouteId
		 * Filters the model by the lead route id
		 *
		 * @param  int $leadRouteId Lead Route ID
		 *
		 * @return Lead
		 */
		public function byLeadRouteId($leadRouteId) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'lead_route_id = :lead_route_id',
					'params' => array(':lead_route_id' => $leadRouteId),
				)
			);

			return $this;
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'lead_route_id' => 'Lead Route',
				'component_id' => 'Component',
				'assigned_to_id' => 'Assigned To',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('lead_route_id', $this->lead_route_id);
			$criteria->compare('component_id', $this->component_id);
			$criteria->compare('assigned_to_id', $this->assigned_to_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		/**
		 * Setup the observers for a new instance of a lead
		 */
		public function init() {

			/**
			 * Attach the lead observers, that will create the lead based on different lead route types
			 * Allows for unique route interaction with a newly created lead
			 */
			$this->attachEventHandler('onInitLead', array(LeadShiftRouteObserver, 'initLead'));
			$this->attachEventHandler('onInitLead', array(LeadRoundRobinObserver, 'initLead'));
			$this->attachEventHandler('onInitLead', array(LeadPoolObserver, 'initLead'));
		}

		protected function beforeValidate() {

			if ($this->getIsNewRecord()) {
				// Sets up the lead to be saved
				$this->initLead();

				/**
				 * Attach observers to process any modifications to a lead once they are saved by route
				 * This way routes can respond differently to a lead being modified (update or removal)
				 */
				$this->attachEventHandler('onAfterSave', array(LeadShiftRouteObserver, 'afterLeadCreated'));
				$this->attachEventHandler('onAfterSave', array(LeadRoundRobinObserver, 'afterLeadCreated'));
				$this->attachEventHandler('onAfterSave', array(LeadPoolObserver, 'afterLeadCreated'));
			}
		}

		protected function beforeSave() {

			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');

			}

			return parent::beforeSave();
		}

		protected function initLead() {

			if ($this->hasEventHandler('onInitLead')) {
				$this->onInitLead(new CModelEvent($this));
			}
		}

		public function onInitLead($event) {
			$this->raiseEvent('onInitLead', $event);
		}
	}