<?php

	/**
	 * This is the model class for table "lead_shifts".
	 *
	 * The followings are the available columns in table 'lead_shifts':
	 *
	 * @property integer $id
	 * @property integer $lead_route_id
	 * @property string  $date
	 * @property string  $start_time
	 * @property string  $end_time
	 */
	class LeadShift extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return LeadShift the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'lead_shifts';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'lead_route_id, date, start_time, end_time, contact_id',
					'required'
				),
				array(
					'lead_route_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, lead_route_id, date, start_time, end_time',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id',
					'alias' => 't'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'lead_route_id' => 'Lead Route',
				'date' => 'Date',
				'start_time' => 'Start Time',
				'end_time' => 'End Time',
				'contact_id' => 'Agent',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('lead_route_id', $this->lead_route_id);
			$criteria->compare('date', $this->date, true);
			$criteria->compare('start_time', $this->start_time, true);
			$criteria->compare('end_time', $this->end_time, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		public function getCalendarStartDateTime() {

			return $this->date . ' ' . $this->start_time;
		}

		public function getCalendarEndDateTime() {

			return $this->date . ' ' . $this->end_time;
		}

		public function scopes() {

			return array(
				'byCurrentDate' => array(
					'condition' => 'date = CURRENT_DATE',
				),
				'withinCurrentTime' => array(
					'condition' => 'CURRENT_TIME >= start_time AND CURRENT_TIME <= end_time',
				),
				'priorDay' => array(
					'condition' => 'date = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY)',
				),
				'nextDay' => array(
					'condition' => 'date = DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)',
				),
                'laterToday' => array(
                    'condition' => 'date = CURRENT_DATE AND CURRENT_TIME <= end_time',
                ),
				'latestShift' => array(
					'order' => 'end_time DESC',
				),
				'earliestShift' => array(
					'order' => 'start_time ASC',
				),
			);
		}

		protected function beforeSave() {

			$this->date = Yii::app()->format->formatDate($this->date, StmFormatter::MYSQL_DATE_FORMAT);
			$this->start_time = Yii::app()->format->formatDate($this->start_time, StmFormatter::MYSQL_TIME_FORMAT);
			$this->end_time = Yii::app()->format->formatDate($this->end_time, StmFormatter::MYSQL_TIME_FORMAT);

			$this->updated = new CDbExpression('NOW()');
			$this->updated_by = Yii::app()->user->id;

			return parent::beforeSave();
		}

		public function byLeadRouteId($leadRouteId) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'lead_route_id = :lead_route_id',
					'params' => array(':lead_route_id' => $leadRouteId),
				)
			);

			return $this;
		}
	}