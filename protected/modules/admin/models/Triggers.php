
<?php

/**
 * This is the model class for table "triggers".
 *
 * The followings are the available columns in table 'triggers':
 * @property string $id
 * @property integer $account_id
 * @property integer $component_type_id
 * @property integer $status_ma
 * @property string $event
 * @property string $event_data
 * @property string $action
 * @property integer $action_plan_id
 * @property string $description
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 * @property Accounts $account
 * @property ActionPlans $actionPlan
 * @property ComponentTypes $componentType
 */
class Triggers extends StmBaseActiveRecord
{

    const ACTION_APPLY_ACTION_PLAN = 'Apply Action Plan';
    const ACTION_UNAPPLY_ACTION_PLAN = 'Unapply Action Plan';

    const EVENT_SELLER_STATUS_UPDATE = 'Seller Status Updates to';
    const EVENT_BUYER_STATUS_UPDATE = 'Buyer Lead Updates to';

    const EVENT_SELLER_APPOINTMENT_MET_STATUS = 'Seller Appointment Met Status';
    const EVENT_BUYER_APPOINTMENT_MET_STATUS = 'Buyer Appointment Met Status';

    const EVENT_CLOSING_STATUS = 'Closing Status Updates to';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Triggers the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'triggers';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, component_type_id, event, event_data, updated_by, updated, added_by, added', 'required'),
            array('account_id, component_type_id, status_ma, action_plan_id, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('action', 'validateDuplicate'),
            array('event, action, description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, account_id, component_type_id, status_ma, event, event_data, action, action_plan_id, description, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'actionPlan' => array(self::BELONGS_TO, 'ActionPlans', 'action_plan_id'),
            'componentType' => array(self::BELONGS_TO, 'ComponentTypes', 'component_type_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'account_id' => 'Account',
            'event' => 'Event',
            'event_data' => 'Event Data',
            'action' => 'Action',
            'action_plan_id' => 'Action Plan',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
            if(empty($this->account_id)) {
                $this->account_id = Yii::app()->user->getAccountId();
            }
        }

        switch($this->event) {
            case self::EVENT_SELLER_STATUS_UPDATE:
            case self::EVENT_SELLER_APPOINTMENT_MET_STATUS:
                $this->component_type_id = ComponentTypes::SELLERS;
                break;

            case self::EVENT_BUYER_STATUS_UPDATE:
            case self::EVENT_BUYER_APPOINTMENT_MET_STATUS:
                $this->component_type_id = ComponentTypes::BUYERS;
                break;
            case self::EVENT_CLOSING_STATUS:
                $this->component_type_id = ComponentTypes::CLOSINGS;
                break;
        }

        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    public function validateDuplicate($attribute, $params)
    {
        $duplicate = Triggers::model()->findByAttributes(array(
                'account_id'=> $this->account_id,
                'component_type_id'=> $this->component_type_id,
                'event'=> $this->event,
                'event_data'=> $this->event_data,
                'action'=> $this->action,
                'action_plan_id'=> $this->action_plan_id,
            ));

        if($duplicate && ($this->isNewRecord || $duplicate->id != $this->id)) {

            $this->addError($attribute, 'This trigger setting already exists.');
        }
    }

    public function getEventDataLabel($type)
    {
        switch($type) {
            case self::EVENT_BUYER_STATUS_UPDATE:
            case self::EVENT_SELLER_STATUS_UPDATE:
                $label = TransactionStatus::model()->findByAttributes(array('id' => $this->event_data))->name;
                break;

            case self::EVENT_SELLER_APPOINTMENT_MET_STATUS:
            case self::EVENT_BUYER_APPOINTMENT_MET_STATUS:
                $label = Appointments::model()->getMetStatusTypes()[$this->event_data];
                break;

            default:
                //@todo: error message, throw exception???
                break;
        }

        return $label;
    }

    public function getEventDataList($type)
    {
        switch($type) {
            case self::EVENT_SELLER_STATUS_UPDATE:
                $list = CHtml::listData(TransactionStatus::model()->findAll(array('condition'=>'id<='.TransactionStatus::ACTIVE_LISTING_ID.' OR id IN('.TransactionStatus::E_NOT_YET_INTERESTED.','.TransactionStatus::WITH_ANOTHER_AGENT.','.TransactionStatus::COMING_SOON_ID.','.TransactionStatus::ACTIVE_LISTING_ID.','.TransactionStatus::REFERRAL_OUTBOUND_ID.','.TransactionStatus::UNDER_CONTRACT_ID.','.TransactionStatus::CLOSED_ID.')','order'=>'sort_order')),'id','name');
                break;

            case self::EVENT_BUYER_STATUS_UPDATE:
                $list = CHtml::listData(TransactionStatus::model()->findAll(array('condition'=>'id<='.TransactionStatus::D_NURTURING_SPOKE_TO_ID.' OR id IN('.TransactionStatus::E_NOT_YET_INTERESTED.','.TransactionStatus::WITH_ANOTHER_AGENT.','.TransactionStatus::ACTIVE_LISTING_ID.','.TransactionStatus::REFERRAL_OUTBOUND_ID.','.TransactionStatus::UNDER_CONTRACT_ID.','.TransactionStatus::CLOSED_ID.')','order'=>'sort_order')),'id','name');
                break;

            case self::EVENT_SELLER_APPOINTMENT_MET_STATUS:
            case self::EVENT_BUYER_APPOINTMENT_MET_STATUS:
//                $list = array(
//                            Appointments::MET_STATUS_SET => Appointments::model()->getMetStatusTypes()[Appointments::MET_STATUS_SET],
//                        );
                $list = Appointments::model()->getMetStatusTypes();
                break;
            case self::EVENT_CLOSING_STATUS:
                $list = array(Closings::STATUS_TYPE_CLOSED_ID => 'Closed');
                break;
            default:
                //@todo: error message, throw exception???
                break;
        }

        return $list;
    }

    public function getEventsList()
    {
        return array(
            self::EVENT_SELLER_STATUS_UPDATE => self::EVENT_SELLER_STATUS_UPDATE,
            self::EVENT_BUYER_STATUS_UPDATE => self::EVENT_BUYER_STATUS_UPDATE,
//            self::EVENT_SELLER_NEW_APPOINTMENT_SET => self::EVENT_SELLER_NEW_APPOINTMENT_SET,
//            self::EVENT_BUYER_NEW_APPOINTMENT_SET => self::EVENT_BUYER_NEW_APPOINTMENT_SET,
            self::EVENT_SELLER_APPOINTMENT_MET_STATUS => self::EVENT_SELLER_APPOINTMENT_MET_STATUS,
            self::EVENT_BUYER_APPOINTMENT_MET_STATUS => self::EVENT_BUYER_APPOINTMENT_MET_STATUS,
            self::EVENT_CLOSING_STATUS => self::EVENT_CLOSING_STATUS
        );
    }

    public function getActionsList()
    {
        return array(
            self::ACTION_APPLY_ACTION_PLAN => self::ACTION_APPLY_ACTION_PLAN,
            self::ACTION_UNAPPLY_ACTION_PLAN => self::ACTION_UNAPPLY_ACTION_PLAN
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('account_id',$this->account_id);
        $criteria->compare('event',$this->event,true);
        $criteria->compare('event_data',$this->event_data,true);
        $criteria->compare('action',$this->action,true);
        $criteria->compare('action_plan_id',$this->action_plan_id);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}