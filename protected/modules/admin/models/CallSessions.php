<?php

/**
 * This is the model class for table "call_sessions".
 *
 * The followings are the available columns in table 'call_sessions':
 * @property integer $id
 * @property integer $call_list_id
 * @property integer $contact_id
 * @property integer $filter_max_call_count
 * @property integer $filter_max_call_days
 * @property integer $number_lines
 * @property string $call_uuid
 * @property string $caller_id_name
 * @property string $caller_id_number
 * @property integer $default_voicemail_id
 * @property string $start
 * @property string $end
 * @property string $total_cost
 * @property string $direction
 * @property string $hang_up_cause
 * @property string $from_phone
 * @property integer $duration
 * @property string $a_leg_uuid
 * @property integer $bill_duration
 * @property string $bill_rate
 * @property string $to_phone
 * @property string $answer_time
 * @property string $start_time
 * @property string $a_leg_request_uuid
 * @property string $request_uuid
 * @property string $end_time
 * @property string $call_status
 * @property string $event
 * @property string $availability
 *
 * The followings are the available model relations:
 * @property CallLists $callList
 * @property Contacts $contact
 * @property Calls[] $calls
 * @property CallListPhones[] $callListPhones
 * @property CallRecordings $callRecording
 */
class CallSessions extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallSessions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_sessions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_list_id, contact_id, request_uuid', 'required'),
            array('call_list_id, contact_id, number_lines, filter_max_call_count, filter_max_call_days, duration, bill_duration, default_voicemail_id', 'numerical', 'integerOnly'=>true),
            array('call_uuid, a_leg_uuid, a_leg_request_uuid, request_uuid', 'length', 'max'=>36),
            array('total_cost, bill_rate, availability', 'length', 'max'=>9),
            array('direction, call_status, event', 'length', 'max'=>20),
            array('hang_up_cause', 'length', 'max'=>50),
            array('from_phone, to_phone', 'length', 'max'=>15),
            array('caller_id_name, caller_id_number, start, end', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_list_id, contact_id, number_lines, caller_id_name, caller_id_number, filter_max_call_count, filter_max_call_days, call_uuid, start, end, total_cost, direction, hang_up_cause, from_phone, duration, a_leg_uuid, bill_duration, bill_rate, to_phone, answer_time, start_time, a_leg_request_uuid, request_uuid, end_time, call_status, event, availability, default_voicemail_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callList' => array(self::BELONGS_TO, 'CallLists', 'call_list_id'),
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'calls' => array(self::HAS_MANY, 'Calls', 'call_session_id'),
            'callListPhones' => array(self::HAS_MANY, 'CallListPhones', 'last_call_session_id'),
            'callRecording' => array(self::BELONGS_TO, 'CallRecordings', 'default_voicemail_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_list_id' => 'Call List',
            'contact_id' => 'Contact',
            'filter_max_call_count' => 'Filter Max Call Count',
            'filter_max_call_days' => 'Filter Max Call Days',
            'number_lines' => 'Number Lines',
            'call_uuid' => 'Call Uuid',
            'start' => 'Start',
            'end' => 'End',
            'total_cost' => 'Total Cost',
            'direction' => 'Direction',
            'hang_up_cause' => 'Hang Up Cause',
            'from_phone' => 'From Phone',
            'duration' => 'Duration',
            'a_leg_uuid' => 'A Leg Uuid',
            'bill_duration' => 'Bill Duration',
            'bill_rate' => 'Bill Rate',
            'to_phone' => 'To Phone',
            'answer_time' => 'Answer Time',
            'start_time' => 'Start Time',
            'a_leg_request_uuid' => 'A Leg Request Uuid',
            'request_uuid' => 'Request Uuid',
            'end_time' => 'End Time',
            'call_status' => 'Call Status',
            'event' => 'Event',
            'availability' => 'Availability',
            'default_voicemail_id' => 'Default Voicemail'
        );
    }

    /**
     * Has Active Session
     * @param $contactId
     * return mixed string/model
     */
    public function hasActiveSession($contactId, $returnModel=false) //, $verifyActive=true
    {
        if($returnModel) {
            $criteria = new CDbCriteria();
            $criteria->condition = "contact_id={$contactId} AND end IS NULL"; // AND DATE(start)=CURRENT_DATE
            $criteria->order = 'id DESC';
            $callSession = CallSessions::model()->find($criteria);

            return $callSession;
        }
        else {
            // check for a session today that is still active, get the oldest one first and hang up on them.
            return Yii::app()->db->createCommand("select call_uuid from call_sessions where contact_id={$contactId} AND DATE(start)=CURRENT_DATE AND end IS NULL ORDER BY id ASC LIMIT 1")->queryScalar();
        }
    }

    /**
     * Get Active Sessions By Call List ID
     *
     * Retrieves active sessions for a given call list id
     * @param int $id Call List ID to lookup active sessions for
     * @return array Result set
     */
    public function getActiveSessionsByCallListId($id)
    {
        return CallSessions::model()->findAll(array('condition'=>"call_list_id={$id} AND DATE(start)=CURRENT_DATE AND end IS NULL",'order'=>'id ASC',));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('call_list_id',$this->call_list_id);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('number_lines',$this->number_lines);
        $criteria->compare('call_uuid',$this->call_uuid,true);
        $criteria->compare('start',$this->start,true);
        $criteria->compare('end',$this->end,true);
        $criteria->compare('total_cost',$this->total_cost,true);
        $criteria->compare('direction',$this->direction,true);
        $criteria->compare('hang_up_cause',$this->hang_up_cause,true);
        $criteria->compare('from_phone',$this->from_phone,true);
        $criteria->compare('duration',$this->duration);
        $criteria->compare('a_leg_uuid',$this->a_leg_uuid,true);
        $criteria->compare('bill_duration',$this->bill_duration);
        $criteria->compare('bill_rate',$this->bill_rate,true);
        $criteria->compare('to_phone',$this->to_phone,true);
        $criteria->compare('answer_time',$this->answer_time,true);
        $criteria->compare('start_time',$this->start_time,true);
        $criteria->compare('a_leg_request_uuid',$this->a_leg_request_uuid,true);
        $criteria->compare('request_uuid',$this->request_uuid,true);
        $criteria->compare('end_time',$this->end_time,true);
        $criteria->compare('call_status',$this->call_status,true);
        $criteria->compare('event',$this->event,true);
        $criteria->compare('availability',$this->availability,true);
        $criteria->compare('default_voicemail_id',$this->default_voicemail_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}