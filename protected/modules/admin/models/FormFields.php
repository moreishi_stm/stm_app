<?php

	/**
	 * This is the model class for table "form_fields".
	 *
	 * The followings are the available columns in table 'form_fields':
	 *
	 * @property integer       $id
	 * @property string        $name
	 * @property integer       $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property FormFieldLu[] $formFieldLus
	 */
	class FormFields extends StmBaseActiveRecord {

		/**
		 * Mapping of Account Lu Id to a Mapping of Form Fields by Field Name.
		 */
		private $fieldsByAccountLu = array();

        /**
         * Field Label Maps
         *
         * A static cache for mappings of field IDs to labels
         * @var array Mapping id => label
         */
        protected static $_fieldIdLabelMaps = array();

        /**
         * Field Name Label Maps
         *
         * A static cache for mappings of field names to labels
         * @var array Mapping name => label
         */
        protected static $_fieldNameLabelMaps = array();


        /**
         * Field Id Name Maps
         *
         * A static cache for mappings of field id to names
         * @var array Mapping id => name
         */
        protected static $_fieldIdNameMaps = array();

		const FIRST_NAME_ID = 1;
		const LAST_NAME_ID = 2;
		const EMAIL_ID = 3;
		const PHONE_ID = 4;
		const COMMENTS_ID = 7;
		const TO_EMAIL_ID = 11;
        const ADDRESS_ID = 14;
        const CITY_ID = 16;
        const STATE_ID = 17;
        const ZIP_ID = 18;
        const SOURCE_ID = 24;
        const SOURCE_DESCRIPTION_ID = 25;
        const BEDROOMS_ID = 26;
        const BATHS_ID = 27;
        const SQ_FEET_ID = 28;
        const CONDITION_ID = 29;
        const TIME_FRAME_ID = 31;
        const REFERRER_PHONE_ID = 51;
        const FULL_NAME_ID = 52;
		const FORM_SUBMISSION_ID = 53;

		const LOG_CATEGORY = 'leadgen';

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FormFields the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'form_fields';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'label',
					'length',
					'max' => 31
				),
				array(
					'name',
					'length',
					'max' => 50
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, label is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'formFieldLus' => array(
					self::HAS_MANY,
					'FormFieldLu',
					'form_field_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
				'label' => 'Label',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('label', $this->label, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

        public static function getFieldIdByName($name) {

            // Populate field maps
            self::_populateFieldMaps();

            if(!($key = array_search($name, self::$_fieldIdNameMaps))) {
                // send error log about missing/invalid field name
                Yii::log(__FILE__.'(:'.__LINE__.') Unknown Field Name: '.$name.'. Investigate what form/page is requesting this and verify that it is not missing in client table.', \CLogger::LEVEL_ERROR);
            }

            // Return the map
            return ($key) ? $key : null;
        }

        /**
         * Get Field Name
         *
         * Retrieves an input elements name attribute value for a given ID
         * @param $id int Form Element ID (not HTML, from the database)
         * @return null
         */
        public static function getFieldNameById($id)
        {
            // Populate field maps
            self::_populateFieldMaps();

            if(!($fieldNameExists = array_key_exists($id, self::$_fieldIdNameMaps))) {
                Yii::log(__FILE__.'(:'.__LINE__.') Unknown Field ID: '.$id.'. Investigate what form/page is requesting this and verify that it is not missing in client table.', \CLogger::LEVEL_ERROR);
            }

            // Return the map
            return $fieldNameExists ? self::$_fieldIdNameMaps[$id] : null;
        }

        /**
         * Populate Field Maps
         *
         * Helper function that populates our field maps
         * @return void
         */
        protected static function _populateFieldMaps()
        {
            // If we haven't retrieved these values at least once "per page load", do it now
            if(!count(self::$_fieldIdLabelMaps)) {

                // Retrieve all records
                $fields = self::model()->findAll();

                // Model data
                foreach($fields as $field) {
                    self::$_fieldIdLabelMaps[$field->id] = $field->label;
                    self::$_fieldNameLabelMaps[$field->name] = $field->label;
                    self::$_fieldIdNameMaps[$field->id] = $field->name;
                }
            }
        }

        /**
         * Get Field Label By Id
         *
         * Returns field labels by ID
         * @param $id int Field ID
         * @return string The mapping result
         */
        public static function getFieldLabelById($id)
        {
            // Populate field maps
            self::_populateFieldMaps();

            // Return the map
            return array_key_exists($id, self::$_fieldIdLabelMaps) ? self::$_fieldIdLabelMaps[$id] : null;
        }

        /**
         * Get Field Label By Name
         *
         * Returns field labels by name
         * @param $name String the name to retrieve a label for
         * @return string The mapping result
         */
        public static function getFieldLabelByName($name)
        {
            // Populate field maps
            self::_populateFieldMaps();

            // Return the map
            return array_key_exists($name, self::$_fieldNameLabelMaps) ? self::$_fieldNameLabelMaps[$name] : null;
        }

		public function getField($fieldName) {
            $formField = self::model()->findByAttributes(array('name' => $fieldName));
            if (empty($formField)) {
                return null;
            }

            return $formField;
		}

        public function getMultiComponentList()
        {
            return array(ComponentTypes::SELLERS => 'Selling a House', ComponentTypes::BUYERS => 'Buying a House', 'seller_and_buyer' => 'Both: Selling and Buying a House');
        }

		private function cacheFields($formId, $formAccountLuId) {
			$FormFieldLu = new FormFieldLu('search');
			$FormFieldLu->form_account_lu_id = $formAccountLuId;
			$FormFieldLu->is_deleted = 0;
			$FormFieldLus = $FormFieldLu->search()->getData();

			$FormFieldIds = array();

			foreach ($FormFieldLus as $FormFieldLu) {
				array_push($FormFieldIds, strval($FormFieldLu->form_field_id));
			}

			$FormFieldIds = array_unique($FormFieldIds);

			$FormFields = FormFields::model()->multiSelect($FormFieldIds)->findAll();

			$fieldsByName = array();

			foreach ($FormFields as $FormField) {
				$fieldsByName[$FormField->name] = (object)$FormField->attributes;
			}

			$this->fieldsByAccountLu[$formAccountLuId] = $fieldsByName;
		}

		public function multiSelect($formFieldIds = array()) {
			if (count($formFieldIds) > 0) {
				$this->getDbCriteria()->addInCondition('id', $formFieldIds);
			}

			return $this;
		}
	}
