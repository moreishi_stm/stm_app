<?php

/**
 * This is the model class for table "call_list_requeue_log".
 *
 * The followings are the available columns in table 'call_list_requeue_log':
 * @property integer $id
 * @property integer $call_list_id
 * @property string $description
 * @property integer $count
 * @property string $from_date
 * @property string $to_date
 * @property integer $added_by
 * @property string $start_datetime
 * @property string $complete_datetime
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 * @property CallLists $callList
 */
class CallListRequeueLog extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallListRequeueLog the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_list_requeue_log';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_list_id,', 'required', 'on'=>'insert'),
            array('call_list_id, description, count', 'required', 'on'=>'update'),
            array('call_list_id, count, added_by', 'numerical', 'integerOnly'=>true),
            array('description', 'length', 'max'=>100),
            array('start_datetime, complete_datetime, from_date, to_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_list_id, description, count, added_by, start_datetime, complete_datetime, from_date, to_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'callList' => array(self::BELONGS_TO, 'CallLists', 'call_list_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_list_id' => 'Call List',
            'description' => 'Description',
            'added_by' => 'Added By',
            'count' => 'Count',
            'start_datetime' => 'Start Datetime',
            'complete_datetime' => 'Complete Datetime',
        );
    }

    /**
     * @param $callListId
     *
     * @return array|mixed|null
     */
    public static function getLastRequeueData($callListId)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('call_list_id', $callListId);
        $criteria->order = 'id DESC';
        $criteria->limit = 1;

        return CallListRequeueLog::model()->find($criteria);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('call_list_id',$this->call_list_id);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('count',$this->count);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('start_datetime',$this->start_datetime,true);
        $criteria->compare('complete_datetime',$this->complete_datetime,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}