<?php

	/**
	 * This is the model class for table "form_field_lu".
	 *
	 * The followings are the available columns in table 'form_field_lu':
	 *
	 * @property integer       $id
	 * @property integer       $form_account_lu_id
	 * @property integer       $form_field_id
	 * @property integer       $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property FormFields    $formField
	 * @property FormAccountLu $formAccountLu
	 */
	class FormFieldLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FormFieldLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'form_field_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'form_account_lu_id, form_field_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, form_account_lu_id, form_field_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'formField' => array(
					self::BELONGS_TO,
					'FormFields',
					'form_field_id'
				),
				'formAccountLu' => array(
					self::BELONGS_TO,
					'FormAccountLu',
					'form_account_lu_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'form_account_lu_id' => 'Form Account Lu',
				'form_field_id' => 'Form Field',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('form_account_lu_id', $this->form_account_lu_id);
			$criteria->compare('form_field_id', $this->form_field_id);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
