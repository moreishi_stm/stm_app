<?php

	class Sellers extends Transactions {

        public $transactionType = Transactions::SELLERS;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Transactions the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function __construct($scenario = 'insert') {
			parent::__construct($scenario);
			$this->transactionType = Transactions::SELLERS;
		}

		public static function countListingsTaken($opts) {
			$opts['default_preset'] = 'this_month';
            $opts = Yii::app()->format->formatDateRangeArray($opts);
            //@todo: when changing to the appointment sign date need to apply throughout the report
//            return Yii::app()->db->createCommand("SELECT count(*) FROM appointments WHERE component_type_id=".ComponentTypes::SELLERS." AND signed_date >= '".$opts['from_date']."' AND signed_date <= '".$opts['to_date']."' AND is_deleted=0")->queryScalar();
			return TransactionFieldValues::model()->byFieldId(TransactionFields::SELLER_LISTING_DATE)->dateRange($opts)->count();
		}

		public function byListingDate($opts) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$TransactionFieldsValues = TransactionFieldValues::model()->byFieldId(TransactionFields::SELLER_LISTING_DATE)->dateRange($opts)->desc()->findAll();
			if ($TransactionFieldsValues) {
				$transactionIds = array();
				foreach ($TransactionFieldsValues as $TransactionValue) {
					array_push($transactionIds, $TransactionValue->transaction_id);
				}
			}
			$this->getDbCriteria()->addInCondition('id',$transactionIds);

			return $this;
		}

		public static function listingsTakenVolume($opts) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$TransactionFieldsValues = TransactionFieldValues::model()->byFieldId(TransactionFields::SELLER_LISTING_DATE)->dateRange($opts)->findAll();
			if ($TransactionFieldsValues) {
				$transactionIds = array();
				foreach ($TransactionFieldsValues as $TransactionValue) {
					array_push($transactionIds, $TransactionValue->transaction_id);
				}
				$transactionIds = implode(',', $transactionIds);
				$query = 'SELECT sum(value) as volume FROM `transaction_field_values` WHERE transaction_field_id=' . TransactionFields::CURRENT_PRICE_SELLER . ' AND transaction_id in (' . $transactionIds . ')';
				$volumeTotal = Yii::app()->db->createCommand($query)->queryScalar();
			}

			return ($volumeTotal) ? $volumeTotal : 0;
		}

		public static function expiredWithdrawnVolume($opts) {
			//@todo: need to add daterange functionality

			$Sellers = Sellers::model()->byStatusIds(array(TransactionStatus::EXPIRED_ID,TransactionStatus::CANCELLED_ID))->findAll();
            if (empty($Sellers)) {
                return 0;
            }
			$sellerIds = array();
			foreach($Sellers as $Seller) {
				array_push($sellerIds, $Seller->id);
			}
			$sellerIds = implode(',', $sellerIds);

			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$query = 'SELECT sum(value) FROM `transaction_field_values` WHERE transaction_field_id=' . TransactionFields::CURRENT_PRICE_SELLER . ' AND transaction_id in (' . $sellerIds . ')';
			$volumeTotal = Yii::app()->db->createCommand($query)->queryScalar();

			return ($volumeTotal) ? $volumeTotal : 0;
		}
	}
