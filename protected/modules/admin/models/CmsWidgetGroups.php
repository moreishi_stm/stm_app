<?php

	/**
	 * This is the model class for table "cms_widget_groups".
	 *
	 * The followings are the available columns in table 'cms_widget_groups':
	 *
	 * @property integer            $id
	 * @property integer            $account_id
	 * @property integer            $status_ma
	 * @property string             $name
	 * @property integer            $is_default
	 * @property integer            $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property CmsContent[]       $cmsContents
	 * @property CmsWidgetGroupLu[] $cmsWidgetGroupLus
	 * @property Accounts           $account
	 */
	class CmsWidgetGroups extends StmBaseActiveRecord {

		public $widgetsToAdd = array(); // Collection of CmsWidgets models

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return CmsWidgetGroups the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'cms_widget_groups';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, status_ma, is_default, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'widgetsToAdd, description',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, widgetsToAdd, description, status_ma, name, is_default, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'cmsContents' => array(
					self::HAS_MANY,
					'CmsContent',
					'widget_group_id'
				),
				'cmsWidgets' => array(
					self::MANY_MANY,
					'CmsWidgets',
					'cms_widget_group_lu(cms_widget_id, cms_widget_group_id)'
				),
				'cmsWidgetGroupLu' => array(
					self::HAS_MANY,
					'CmsWidgetGroupLu',
					'cms_widget_group_id'
				),
				'account' => array(
					self::BELONGS_TO,
					'Accounts',
					'account_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'status_ma' => 'Status',
				'name' => 'Name',
				'widgetsToAdd' => 'Widgets',
				'description' => 'Description',
				'is_default' => 'Is Default',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('status_ma', $this->status_ma);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('is_default', $this->is_default);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}