<?php

	/**
	 * This is the model class for table "action_plan_items".
	 *
	 * The followings are the available columns in table 'action_plan_items':
     *
     * @property integer     $id
	 * @property integer     $action_plan_id
     * @property integer     $status_ma
     * @property integer     $is_priority
     * @property integer     $task_type_id
	 * @property integer     $email_template_id
	 * @property string      $description
	 * @property integer     $due_days
	 * @property datetime	 $specific_due_date
	 * @property integer     $due_days_type_ma
	 * @property integer     $due_reference_ma
	 * @property integer     $assign_to_type_ma
	 * @property integer     $if_weekend
     * @property integer     $specific_user_id
     * @property integer     $depends_on_id
	 * @property integer     $sort_order
     * @property integer     $operation_manual_id
	 * @property integer     $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property ActionPlans $actionPlan
	 */
	class ActionPlanItems extends StmBaseActiveRecord {

		const DUE_DAYS_TYPE_BEFORE_ID = 1;
		const DUE_DAYS_TYPE_AFTER_ID = 2;

		const DUE_REFERENCE_APPLIED_DATE_ID = 1;
		const DUE_REFERENCE_ADDED_DATE_ID = 2;
		const DUE_REFERENCE_LISTING_DATE_ID = 3;
		const DUE_REFERENCE_CONTRACT_DATE_ID = 4;
		const DUE_REFERENCE_CLOSING_DATE_ID = 5;
		const DUE_REFERENCE_UPON_COMPLETION_OF_TASK = 6;
        const DUE_REFERENCE_APPOINTMENT_SET_DATE_ID = 7;
        const DUE_REFERENCE_APPOINTMENT_MET_DATE_ID = 8;
        const DUE_REFERENCE_LISTING_EXPIRE_ID = 9;
        const DUE_REFERENCE_HOME_INSPECTION_ID = 10;
        const DUE_REFERENCE_SPECIFIC_DATE_ID = 11;
        const DUE_REFERENCE_BIRTHDAY_DATE_ID = 12;
        const DUE_REFERENCE_SPOUSE_BIRTHDAY_DATE_ID = 13;
        const DUE_REFERENCE_ANNIVERSARY_DATE_ID = 14;

		const IF_WEEKEND_KEEP_SAME_ID = 1;
		const IF_WEEKEND_BEFORE_ID = 2;
		const IF_WEEKEND_AFTER_ID = 3;

        public $orderNumber;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ActionPlanItems the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'action_plan_items';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'status_ma, description, task_type_id, due_reference_ma, assign_to_type_ma, if_weekend', //due_days, due_days_type_ma,
					'required'
				),
				array(
					'action_plan_id, email_template_id,letter_template_id, status_ma, task_type_id, due_days, due_days_type_ma, due_reference_ma, assign_to_type_ma, if_weekend, sort_order, operation_manual_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
                array(
                    'specific_user_id',
                    'validateSpecificUser',
                ),
                array(
                    'email_template_id',
                    'validateEmailTemplateId',
                ),
                array(
                    'due_days',
                    'validateDaysValue',
                ),
				array(
					'description',
					'length',
					'max' => 750
				),
                array(
                    'due_reference_ma',
                    'validateDependsOnId'
                ),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, action_plan_id, status_ma, task_type_id, description, due_days, specific_due_date, due_days_type_ma, due_reference_ma, assign_to_type_ma, if_weekend, sort_order, operation_manual_id, is_deleted, specific_user_id',
					'safe',
					'on' => 'search'
				),
				array(
					'specific_user_id, depends_on_id, operation_manual_id, specific_due_date',
					'safe'
				)
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'actionPlan' => array(
					self::BELONGS_TO,
					'ActionPlans',
					'action_plan_id'
				),
				'taskType' => array(
					self::BELONGS_TO,
					'TaskTypes',
					'task_type_id'
				),
				'emailTemplate' => array(
					self::BELONGS_TO,
					'EmailTemplates',
					'email_template_id'
				),
                'specificUser' => array(
                    self::BELONGS_TO,
                    'Contacts',
                    'specific_user_id'
                ),
				'dependentItems' => array(
					self::HAS_MANY,
					'ActionPlanItems',
					'depends_on_id',
                    'order'=>'due_days ASC',
				),
                'dependsOn' => array(
                    self::BELONGS_TO,
                    'ActionPlanItems',
                    'depends_on_id'
                ),
                'operationManual' => array(
                    self::BELONGS_TO,
                    'OperationManuals',
                    'operation_manual_id'
                ),
			);
		}

		/**
		 * Creates a task commensurate with the metadata associated with this Action Plan Item as well as the component
		 * to ultimately be associated with the resultant task.
		 *
		 * @param mixed   $component       the component to create a task for. (Typically a Contact, Buyer, Seller, Closing, etc.)
		 *
		 * @param integer $dependsOnTaskId the id of the task which the task to be created here depends on.
		 *
		 * @return array a collection of the plan items which have successfully created their respective tasks.
		 */
		public function createTask($component, $dependsOnTaskId = null)
        {
			$planItemsApplied = array();

			$Task = new Tasks;

            $accountId=Yii::app()->user->accountId;

            if(!$accountId) {

                //@todo: In production for Zillow lead posts, Yii::app()->user->accountId is empty, send log as to why...
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Yii::app()->user->accountId is missing. Investigate immediately. Pulling account_id from $component->account_id: '.$component->account_id.'. Remove log when done debugging. Component Model Attributes: '.print_r($component->attributes, 1).PHP_EOL.'Yii::app()->user data: ' . print_r(Yii::app()->user, 1).PHP_EOL.'Action Plan Item Data: '.print_r($this->attributes, 1), CLogger::LEVEL_ERROR);
                if(isset($component->account_id) && !empty($component->account_id)) {
                    $accountId=$component->account_id;
                }
            }

			$Task->account_id = $accountId;
			$Task->status_ma = 0;
			$Task->component_type_id = $component->componentType->id;
			$Task->task_type_id = $this->task_type_id;
			$Task->component_id = $component->id;
			$Task->depends_on_id = $dependsOnTaskId;
			$Task->assigned_to_id = $this->determineAssignee($component);
			$Task->assigned_by_id = Yii::app()->user->contact->id;
			$Task->description = $this->description;

            // dependent tasks do not have due date. They are triggered when the parent task is complete.
            if(intval($this->due_reference_ma) !== self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK) {
                $dueDate = $this->calculateDueDate($component);
            } else {
                $Task->due_date = null;
                $Task->scenario = 'dependentTask';
            }

			if (isset($dueDate)) {
				$Task->due_date = $dueDate->format(StmFormatter::MYSQL_DATETIME_FORMAT);
			}

            $taskSaved = $Task->save();
			if(!$taskSaved) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving Task while applying Action Plan. Error Messages:' . print_r($Task->getErrors(), true).PHP_EOL.'Task Attributes: '.print_r($Task->attributes, true).PHP_EOL.'Component Class Name: '.get_class($component).PHP_EOL.'Component Attributes: '.print_r($component->attributes, true), CLogger::LEVEL_ERROR);
            }

			if ($taskSaved) {
				$AppliedLu = new ActionPlanAppliedLu;
				$AppliedLu->action_plan_item_id = $this->id;
				$AppliedLu->task_id = $Task->id;
				$AppliedLu->is_deleted = 0;
				$AppliedLu->save();

				$planItemsApplied[] = $this->id;

				if (!empty($this->dependentItems)) {
					foreach ($this->dependentItems as $Item) {
						$planItemsApplied = array_merge($planItemsApplied, $Item->createTask($component, $Task->id));
					}
				}
			} else {
				Yii::log('Action Plan Item Id: ' . $this->id, 'info', __CLASS__);
				Yii::log('Unable to create task due to error: ' . print_r($Task->getErrors(), true), 'warning', __CLASS__);
			}

			return $planItemsApplied;
		}

		public function isApplicable(ActionPlans $actionPlan, $component) {
			if($this->hasValidAssignee($actionPlan, $component) && $this->hasValidReferenceDate($actionPlan, $component)) {
                return true;
            } else {
                return false;
            }
		}

		private function hasValidAssignee(ActionPlans $actionPlan, $component) {
			$validAssignee = true;

			$assigneeId = $this->determineAssignee($component);

			if (empty($assigneeId)) {
				$validAssignee = false;

                $assignTos = ActionPlanItems::getAssignTos();
				$errorMessage = 'SETTING MISSING in Action Plan Item # '.$this->id.': This action plan requires a '.$assignTos[$this->assign_to_type_ma] .'. Please go to Action Plan Item and input the proper assignee one and re-apply this plan or update the action plan items so it does not assign a task to the '.$assignTos[$this->assign_to_type_ma].'.';
                    //" for " . Yii::app()->format->formatSingular($component->componentType->display_name) . " " . $component->id . "! (". $this->description . ")";

				$actionPlan->addError('applicabilityCheck', $errorMessage);
			} else {
				$ContactAssigned = Contacts::model()->findByPk($assigneeId);

				if (empty($ContactAssigned)) {
					$validAssignee = false;

					$contactName = (empty($ContactAssigned)) ? "Contact# $assigneeId" : $ContactAssigned->getFullName();

					$errorMessage =
						"ERROR: " . $this->assignTos[$this->assign_to_type_ma] . " $contactName is not a valid user in the system. Please review the Action Plan Item: '"
						. $this->description ."'";

					$actionPlan->addError('applicabilityCheck', $errorMessage);
				}
			}

			return $validAssignee;
		}

        private function hasValidReferenceDate(ActionPlans $actionPlan, $component) {
            $validReferenceDate = true;

            switch($this->due_reference_ma) {

                case self::DUE_REFERENCE_APPOINTMENT_SET_DATE_ID:
                    if(!($appointment = $component->appointment)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: An appointment is required to apply this action plan.';
                    }

                    break;

                case self::DUE_REFERENCE_APPOINTMENT_MET_DATE_ID:
                    if(!($appointment = $component->appointment)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: An appointment is required to apply this action plan.';
                    }
                    break;

                case self::DUE_REFERENCE_LISTING_DATE_ID:
                    $listingDate = $component->getFieldValue('listing_date');
                    if(empty($listingDate)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: A Listing Date is required to apply this action plan.';
                    }
                    break;

                case self::DUE_REFERENCE_LISTING_EXPIRE_ID;
                    $expireDate = $component->getFieldValue('listing_expire_date');
                    if(empty($expireDate)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: Current Listing Expiration Date is required to apply this action plan.';
                    }
                    break;

                case self::DUE_REFERENCE_CONTRACT_DATE_ID:
                    if(empty($component->contract_execute_date)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: A Contract Execute Date is required to apply this action plan';
                    }
                    break;
                case self::DUE_REFERENCE_CLOSING_DATE_ID:
                    if(empty($component->contract_closing_date)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: A Closing Date is required to apply this action plan.';
                    }
                    break;

                case self::DUE_REFERENCE_HOME_INSPECTION_ID:
                    if(empty($component->inspection_contingency_date)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: A Home Inspection Contingency Date is required to apply this action plan.';
                    }
                    break;

                case self::DUE_REFERENCE_BIRTHDAY_DATE_ID:
					$birthDay = $component->getAttributeValue(ContactAttributes::BIRTHDAY);
                    if(empty($birthDay) || empty($birthDay->value)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: Birthday Date for contact is required to apply this action plan.';
                    }
                    break;

                case self::DUE_REFERENCE_SPOUSE_BIRTHDAY_DATE_ID:
                    $birthDay = $component->getAttributeValue(ContactAttributes::SPOUSE_BIRTHDAY);
                    if(empty($birthDay) || empty($birthDay->value)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: Spouse Birthday Date for contact is required to apply this action plan.';
                    }
                    break;

                case self::DUE_REFERENCE_ANNIVERSARY_DATE_ID:
                    $anniversaryDate = $component->getAttributeValue(ContactAttributes::WEDDING_ANNIVERSARY);
                    if(empty($anniversaryDate) || empty($anniversaryDate->value)) {
                        $validReferenceDate = false;
                        $errorMessage = 'ERROR: Wedding Anniversary Date for contact is required to apply this action plan.';
                    }
                    break;
            }

            if(!$validReferenceDate) {
                $actionPlan->addError('applicabilityCheck', $errorMessage);
            }

            return $validReferenceDate;
        }

        /**
		 * Determines a Task's Assignee based on component and Plan Item metadata.
		 *
		 * @param $component mixed the component whose type and id will be utilized when necessary to find a proper assignment.
		 *
		 * @return integer|null the assignee Id if found, null otherwise.
		 */
		private function determineAssignee($component) {
			$assigneeId = null;

			switch ($this->assign_to_type_ma) {
				case AssignmentTypes::SPECIFIC_USER:
					$assigneeId = $this->specific_user_id;
					break;
				case AssignmentTypes::CURRENT_USER:
					$assigneeId = Yii::app()->user->contact->id;
					break;
				default:
					$Assignment = $this->findAssignment($component);

					if (isset($Assignment)) {
						$assigneeId = $Assignment->assignee_contact_id;
					}
					break;
			}

			return $assigneeId;
		}

		/**
		 * This wrapper function to find an Assignment based on a component.
		 *
		 * @param $component mixed the component whose Assignment is to be found.
		 *
		 * @return Assignments the Assignment associated with this component, null otherwise if not found.
		 */
		private function findAssignment($component) {
			$Assignment = null;

			$Assignment = Assignments::model()->byComponentTuple($component->componentType->id, $component->id)->byAssignmentType($this->assign_to_type_ma)->find();

			//		if (empty($Assignment) && !empty($component->assignment_single)) {
			//			$Assignment = $component->assignment_single;
			//		}

			//TODO: We need to add additional logic here for what happens when multiple assignments come back for a component.

			//Developer's Note: Models in the Transaction family have a primary assignment column (currently called assignment_single)
			//                  can be used reliably. We need a strategy such that it works universally for all Action Plan Applicable
			//                  models. For now the first Assignment wins. I would like for there to only be a a single assignment
			//                  possible for a given component and assignment type seems to make more sense to me. -- Will M.

			return $Assignment;
		}

		/**
		 * Utility method for instructing this Action Plan Item to calculate the proper due date for a task it has or will create. Exposed
		 * publicly so that Tasks which have a dependency on other tasks can have their due date recalculated on the fly.
		 *
		 * @param mixed $component          the component for which this action plan item is to create a task.
		 * @param null  $givenReferenceDate an optional reference date to utilize to calculate the resultant due date based on this action plan's configuration.
		 *
		 * @return DateTime|null the reference date if successfully calculated, null otherwise.
		 */
		public function calculateDueDate($component, $givenReferenceDate = null) {
			$dueDate = null;

			$referenceDate = $givenReferenceDate;

			if (empty($referenceDate)) {
				$referenceDate = $this->retrieveReferenceDate($component);
			}

            if (empty($referenceDate)) {

                Yii::log(__CLASS__ . ' (' . __LINE__ . ') Reference Date is empty. Action Plan Item Attributes:' . print_r($this->attributes, true), CLogger::LEVEL_ERROR);
            }

			if (isset($referenceDate)) { // && ($this->due_reference_ma != self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK)
				$timeInterval = new DateInterval($this->toIntervalSpecification(abs($this->due_days)));

				switch ($this->due_days_type_ma) {
					case self::DUE_DAYS_TYPE_BEFORE_ID:
						$referenceDate->sub($timeInterval);
						break;
					case self::DUE_DAYS_TYPE_AFTER_ID:
						$referenceDate->add($timeInterval);
						break;
					case self::DUE_REFERENCE_SPECIFIC_DATE_ID:

                        if(strpos($this->specific_due_date, "-") !== false) {
                            $savedDate = explode("-", $this->specific_due_date);
                        }
                        elseif(strpos($this->specific_due_date, "/") !== false) {
                            $savedDate = explode("/", $this->specific_due_date);
                        }

						$currMonth = date("m", time());
						$currDay = date("d", time());
						$curYear = date("Y", time());

						$yearAdd = 0;
						if($savedDate[0] < $currMonth) {
							$yearAdd++;
						}

						if($savedDate[0] == $currMonth) {
							if($savedDate[1] < $currDay) {
								$yearAdd++;
							}
						}

						$finalDate = ($curYear+$yearAdd)."-".$savedDate[0]."-".$savedDate[1];

						$referenceDate = new DateTime($finalDate);
						break;
				}

				$dueDate = $referenceDate;
			}

			if (isset($dueDate) && $this->isWeekend($dueDate)) {
				$dueDate = $this->adjustForWeekend($dueDate);
			}

			return $dueDate;
		}

		private function retrieveReferenceDate($component) {
			$referenceDate = null;

			switch ($this->due_reference_ma) {
				case self::DUE_REFERENCE_APPLIED_DATE_ID:
				case self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK:
					//If this is a Task Completion Based Action Plan Item then we'll default to
					//today's since the completion date of the parent task is unknown we'll have to
					//update it on the fly to the day when the task it's dependent upon is completed.
					$referenceDate = new DateTime();
					break;
				case self::DUE_REFERENCE_ADDED_DATE_ID:
					$referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATETIME_FORMAT, $component->added);
					break;

                case self::DUE_REFERENCE_APPOINTMENT_SET_DATE_ID:
                    if(!($appointment = $component->appointment)) {
                        throw new Exception(__CLASS__.' (:'.__LINE__.') Component is missing an Appointment. Sanity Checking should have caught this. Investigate immediately!');
                    }
                    $referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, Yii::app()->format->formatDate($appointment->set_on_datetime,StmFormatter::MYSQL_DATE_FORMAT));
                    break;

                case self::DUE_REFERENCE_APPOINTMENT_MET_DATE_ID:
                    if(!($appointment = $component->appointment)) {
                        throw new Exception(__CLASS__.' (:'.__LINE__.') Component is missing an Appointment. Sanity Checking should have caught this. Investigate immediately!');
                    }
                    $referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, Yii::app()->format->formatDate($appointment->set_for_datetime,StmFormatter::MYSQL_DATE_FORMAT));
                    break;

				case self::DUE_REFERENCE_LISTING_DATE_ID:
					$referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, Yii::app()->format->formatDate($component->getFieldValue('listing_date'),StmFormatter::MYSQL_DATE_FORMAT));
					break;

                case self::DUE_REFERENCE_LISTING_EXPIRE_ID;
                    $referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, Yii::app()->format->formatDate($component->getFieldValue('listing_expire_date'),StmFormatter::MYSQL_DATE_FORMAT));
                    break;

                case self::DUE_REFERENCE_CONTRACT_DATE_ID:
					$referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, Yii::app()->format->formatDate($component->contract_execute_date,StmFormatter::MYSQL_DATE_FORMAT));
					break;

                case self::DUE_REFERENCE_CLOSING_DATE_ID:
					$referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, Yii::app()->format->formatDate($component->contract_closing_date,StmFormatter::MYSQL_DATE_FORMAT));
					break;

                case self::DUE_REFERENCE_HOME_INSPECTION_ID:
                    $referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, Yii::app()->format->formatDate($component->inspection_contingency_date,StmFormatter::MYSQL_DATE_FORMAT));
                    break;

				case self::DUE_REFERENCE_SPECIFIC_DATE_ID:
					$this->due_days_type_ma = self::DUE_REFERENCE_SPECIFIC_DATE_ID;
					$referenceDate = date("Y-m-d", time());
					break;

				case self::DUE_REFERENCE_BIRTHDAY_DATE_ID:
					$birthdayAttribute = $component->getAttributeValue(ContactAttributes::BIRTHDAY);

					$birthDayValue = $birthdayAttribute->value;

					$birthdayParts = explode("/", $birthDayValue);

					$currYear = date("Y", time());
					$currMonth = date("m", time());
					$currDay = date("d", time());

					$newYear = $currYear;
					if($birthdayParts[2] < $currYear) {
						if($birthdayParts[0] < $currMonth) {
							$newYear = $newYear + 1;
						}

						if($birthdayParts[0] == $currMonth) {
							if($birthdayParts[1] > $currDay) {
								$newYear = $newYear + 1;
							}
						}
					}

					$date = $newYear."-".$birthdayParts[0]."-".$birthdayParts[1];

					$referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, $date);
					break;
				case self::DUE_REFERENCE_SPOUSE_BIRTHDAY_DATE_ID:

					$birthdayAttribute = $component->getAttributeValue(ContactAttributes::SPOUSE_BIRTHDAY);

					$birthDayValue = $birthdayAttribute->value;

					$birthdayParts = explode("/", $birthDayValue);

					$currYear = date("Y", time());
					$currMonth = date("m", time());
					$currDay = date("d", time());

					$newYear = $currYear;
					if($birthdayParts[2] < $currYear) {
						if($birthdayParts[0] < $currMonth) {
							$newYear = $newYear + 1;
						}

						if($birthdayParts[0] == $currMonth) {
							if($birthdayParts[1] > $currDay) {
								$newYear = $newYear + 1;
							}
						}
					}

					$date = $newYear."-".$birthdayParts[0]."-".$birthdayParts[1];

					$referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, $date);
					break;
                case self::DUE_REFERENCE_ANNIVERSARY_DATE_ID:
					$weddingAnniversaryAttribute = $component->getAttributeValue(ContactAttributes::WEDDING_ANNIVERSARY);

					$wedding_anniversary = $weddingAnniversaryAttribute->value;

					$dateParts = explode("/", $wedding_anniversary);

					$currYear = date("Y", time());
					$currMonth = date("m", time());
					$currDay = date("d", time());

					$newYear = $currYear;
					if($dateParts[2] < $currYear) {
						if($dateParts[0] < $currMonth) {
							$newYear = $newYear + 1;
						}

						if($dateParts[0] == $currMonth) {
							if($dateParts[1] > $currDay) {
								$newYear = $newYear + 1;
							}
						}
					}

					$date = $newYear."-".$dateParts[0]."-".$dateParts[1];

					$referenceDate = DateTime::createFromFormat(StmFormatter::MYSQL_DATE_FORMAT, $date);
					break;

                default:
                    throw new Exception(__CLASS__.'(:'.__LINE__.') Unknown Reference Date type.');
                    break;
			}

            if(empty($referenceDate)) {
                throw new Exception(__CLASS__.'(:'.__LINE__.') Reference Date is empty. Attributes: '.print_r($this->attributes, true));
            }

			return $referenceDate;
		}

		private function toIntervalSpecification($numDays) {
			//The context of this function is such that we're only supporting an integer
			//offset for the number of days away that we should set due date of the Task
			//created by this Plan Item.

			//More information on this DateInterval Specification can be found here: http://goo.gl/iK8a9
			return 'P' . $numDays . 'D';
		}

		/**
		 * @param DateTime $date the date which we want to determine is or is not a weekend day.
		 *
		 * @return bool true if the given date falls on a weekend, false otherwise.
		 */
		private function isWeekend(DateTime $date) {
			$SATURDAY = "6";
			$SUNDAY = "0";

			$weekendDays = array(
				$SUNDAY,
				$SATURDAY
			);

			return in_array($date->format("w"), $weekendDays);
		}

		/**
		 * @param DateTime $dueDate the date to be adjusted based on this action plan item's weekend handling presets.
		 *
		 * @return DateTime the date time adjusted according to the rules of this action plan item's weekend handling presets.
		 */
		private function adjustForWeekend(DateTime $dueDate) {
			$adjustedDueDate = null;

			$SATURDAY = "6";
			$SUNDAY = "0";

			$timeIntervalsByWeekendDay = array(
				$SUNDAY => array(
					"after" => 1,
					"before" => 2
				),
				$SATURDAY => array(
					"after" => 2,
					"before" => 1
				),
			);

			switch ($this->if_weekend) {
				case self::IF_WEEKEND_KEEP_SAME_ID:
					$adjustedDueDate = $dueDate;
					break;
				case self::IF_WEEKEND_AFTER_ID:
					$offsetInDays = $timeIntervalsByWeekendDay[$dueDate->format("w")]["after"];
					$timeInterval = new DateInterval($this->toIntervalSpecification($offsetInDays));
					$adjustedDueDate = $dueDate->add($timeInterval);
					break;
				case self::IF_WEEKEND_BEFORE_ID:
					$offsetInDays = $timeIntervalsByWeekendDay[$dueDate->format("w")]["before"];
					$timeInterval = new DateInterval($this->toIntervalSpecification($offsetInDays));
					$adjustedDueDate = $dueDate->sub($timeInterval);
					break;
			}

			return $adjustedDueDate;
		}

		/**
		 * @return array behaviors for model attributes.
		 */
		public function behaviors() {
			return array(
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'action_plan_id' => 'Action Plan',
				'status_ma' => 'Status',
				'task_type_id' => 'Task Type',
				'description' => 'Description',
				'due_days' => 'Due Days',
				'due_days_type_ma' => 'Due Type',
				'due_reference_ma' => 'Due Reference',
				'assign_to_type_ma' => 'Assign To',
				'if_weekend' => 'If Weekend',
				'sort_order' => 'Sort Order',
				'is_deleted' => 'Is Deleted',
				'depends_on_id' => 'Depends on Item',
			);
		}

        public function validateDependsOnId($attribute, $params)
        {
            if ($this->due_reference_ma == self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK && empty($this->depends_on_id)) {
                $this->addError('depends_on_id', 'Please select a dependent Task.');
            }
        }

        public function getDueDaysTypes() {
			return array(
				self::DUE_DAYS_TYPE_BEFORE_ID => 'Before',
				self::DUE_DAYS_TYPE_AFTER_ID => 'After'
			);
		}

		public function getDueReferences() {
			$list = array();
			if(!($componentTypeId = $this->actionPlan->component_type_id)) {
				if($this->action_plan_id && ($ActionPlan = ActionPlans::model()->findByPk($this->action_plan_id))) {
					$componentTypeId = $ActionPlan->component_type_id;
				}
			}
			switch($componentTypeId) {
				case ComponentTypes::SELLERS:
					$list = array(
						self::DUE_REFERENCE_APPLIED_DATE_ID => 'Applied Date',
//						self::DUE_REFERENCE_ADDED_DATE_ID => 'Added Date',
						self::DUE_REFERENCE_LISTING_DATE_ID => 'Listing Date',
                        self::DUE_REFERENCE_LISTING_EXPIRE_ID => 'Listing Expire Date',
                        self::DUE_REFERENCE_APPOINTMENT_SET_DATE_ID => 'Appointment Set Date',
                        self::DUE_REFERENCE_APPOINTMENT_MET_DATE_ID => 'Appointment Met Date',
						self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK => 'Upon Completion of Task',
					);
					break;
				case ComponentTypes::CLOSINGS:
					$list = array(
						self::DUE_REFERENCE_APPLIED_DATE_ID => 'Applied Date',
//						self::DUE_REFERENCE_ADDED_DATE_ID => 'Added Date',
						self::DUE_REFERENCE_CONTRACT_DATE_ID => 'Contract Date',
						self::DUE_REFERENCE_CLOSING_DATE_ID => 'Closing Date',
                        self::DUE_REFERENCE_HOME_INSPECTION_ID => 'Home Inspection Contingency Date',
						self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK => 'Upon Completion of Task',
					);
					break;

                case ComponentTypes::BUYERS:
                case ComponentTypes::RECRUITS:
                    $list = array(
                        self::DUE_REFERENCE_APPLIED_DATE_ID => 'Applied Date',
                        self::DUE_REFERENCE_APPOINTMENT_SET_DATE_ID => 'Appointment Set Date',
                        self::DUE_REFERENCE_APPOINTMENT_MET_DATE_ID => 'Appointment Met Date',
                        self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK => 'Upon Completion of Task',
                    );
                    break;

				case ComponentTypes::CONTACTS:
				default:
					$list = array(
						self::DUE_REFERENCE_APPLIED_DATE_ID => 'Applied Date',
						self::DUE_REFERENCE_ADDED_DATE_ID => 'Added Date',
						self::DUE_REFERENCE_BIRTHDAY_DATE_ID => 'Birthday',
                        self::DUE_REFERENCE_SPOUSE_BIRTHDAY_DATE_ID => 'Spouse Birthday',
                        self::DUE_REFERENCE_ANNIVERSARY_DATE_ID => 'Anniversary',
                        self::DUE_REFERENCE_SPECIFIC_DATE_ID => 'Specific Date',
						self::DUE_REFERENCE_UPON_COMPLETION_OF_TASK => 'Upon Completion of Task',
					);
					break;
			}
			return $list;
		}

		public static function getAssignTos() {
			return array(
				AssignmentTypes::ASSIGNED_TO => 'Primary Assignment',
				AssignmentTypes::LISTING_AGENT => 'Listing Agent',
                AssignmentTypes::LISTING_MANAGER => 'Listing Manager',
                AssignmentTypes::SELLER_ISA => 'Seller ISA',
				AssignmentTypes::CO_LISTING_AGENT => 'Co-Listing Agent',
				AssignmentTypes::BUYER_AGENT => 'Buyer Agent',
                AssignmentTypes::BUYER_ISA => 'Buyer ISA',
				AssignmentTypes::CLOSING_MANAGER => 'Closer',
				AssignmentTypes::SPECIFIC_USER => 'Specific User',
				AssignmentTypes::CURRENT_USER => 'Current User'
			);
		}

		public static function getAssignmentTypes($componentTypeId) {
			$globalAssignmentTypes = AssignmentTypes::byComponentType('all');
			$componentSpecificTypes = AssignmentTypes::byComponentType($componentTypeId);

			return CMap::mergeArray($componentSpecificTypes, $globalAssignmentTypes);
		}

		public static function getIfWeekends() {
			return array(
				self::IF_WEEKEND_KEEP_SAME_ID => 'Keep the Same',
				self::IF_WEEKEND_BEFORE_ID => 'Move to Previous Weekday',
				self::IF_WEEKEND_AFTER_ID => 'Move to Next Weekday'
			);
		}

		public function byActionPlan($actionPlanId) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'action_plan_id = :action_plan_id',
					'params' => array(':action_plan_id' => $actionPlanId),
				)
			);

			return $this;
		}

        public function byExcludeId($id) {
            if(!empty($id)) {
                $this->getDbCriteria()->mergeWith(array(
                        'condition' => 't.id <> :id',
                        'params' => array(':id' => $id),
                    )
                );
            }

            return $this;
        }

        public function getDropDownDisplay() {
            $emailSubject = '';
            if($this->task_type_id == TaskTypes::AUTO_EMAIL_DRIP) {
                $emailSubject = ' Subject: '.$this->emailTemplate->subject;
            }
            return "#$this->id: $this->description | $emailSubject";
        }

        public function validateSpecificUser($attribute, $params)
        {
            if ($this->assign_to_type_ma == AssignmentTypes::SPECIFIC_USER && empty($this->specific_user_id)) {
                $this->addError($attribute, 'Please select a Specific User.');
            }
        }

        public function validateEmailTemplateId($attribute, $params)
        {
            if ($this->task_type_id == TaskTypes::AUTO_EMAIL_DRIP && empty($this->email_template_id)) {
                $this->addError($attribute, 'Please select an Email Template to go out automatically.');
            }
        }

        public function validateDaysValue($attribute, $params)
        {
            if ($this->due_days < 0 || ($this->due_days != (integer)$this->due_days)) {
                $this->addError($attribute, 'Days must be an integer zero or greater.');
            }
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('action_plan_id', $this->action_plan_id);
			$criteria->compare('status_ma', $this->status_ma, true);
			$criteria->compare('task_type_id', $this->task_type_id, true);
            $criteria->compare('email_template_id', $this->email_template_id);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('due_days', $this->due_days);
			$criteria->compare('specific_due_date', $this->specific_due_date);
			$criteria->compare('due_days_type_ma', $this->due_days_type_ma);
			$criteria->compare('due_reference_ma', $this->due_reference_ma);
			$criteria->compare('assign_to_type_ma', $this->assign_to_type_ma);
			$criteria->compare('if_weekend', $this->if_weekend);
            $criteria->addCondition('depends_on_id IS NULL OR due_reference_ma <>'.ActionPlanItems::DUE_REFERENCE_UPON_COMPLETION_OF_TASK);
            $criteria->compare('sort_order', $this->sort_order);
			$criteria->compare('is_deleted', $this->is_deleted);

			$criteria->order = 'due_reference_ma ASC, due_days ASC';

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=> 100,
                ),
			));
		}

		public function beforeSave()
		{
			if($this->specific_due_date) {
				$this->due_days_type_ma = self::DUE_REFERENCE_SPECIFIC_DATE_ID;
			}

			return parent::beforeSave();
		}
	}
