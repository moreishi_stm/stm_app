 <?php
 /**
 * This is the model class for table "cms_menu_links".
 *
 * The followings are the available columns in table 'cms_menu_links':  * @property string $id
  * @property string $parent_id
  * @property string $cms_menu_id
  * @property string $name
  * @property string $title
  * @property string $url
  * @property integer $append_to_parent_url
  * @property string $css_classes
  * @property integer $order
  * @property string $added
  * @property integer $added_by
  * @property string $updated
  * @property integer $updated_by
   *
 * The followings are the available model relations:  * @property   * @property   * @property    */
 class CmsMenuLinks extends StmBaseActiveRecord
 {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CmsMenuLinks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_menu_links';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 			array('cms_menu_id, name, title, url, added, added_by', 'required'),
 			array('append_to_parent_url, order, added_by, updated_by', 'numerical', 'integerOnly'=>true),
 			array('parent_id, cms_menu_id', 'length', 'max'=>10),
 			array('name, title', 'length', 'max'=>100),
 			array('css_classes, updated', 'safe'),
 			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, cms_menu_id, name, title, url, append_to_parent_url, css_classes, order, added, added_by, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'cmsMenu' => array(self::BELONGS_TO, 'CmsMenus', 'cms_menu_id'),
 			'parent' => array(self::BELONGS_TO, 'CmsMenuLinks', 'parent_id'),
 			'cmsMenuLinks' => array(self::HAS_MANY, 'CmsMenuLinks', 'parent_id'),
 		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array( 			'id' => 'ID',
 			'parent_id' => 'Parent',
 			'cms_menu_id' => 'Cms Menu',
 			'name' => 'Name',
 			'title' => 'Title',
 			'url' => 'Url',
 			'append_to_parent_url' => 'Append To Parent Url',
 			'css_classes' => 'Css Classes',
 			'order' => 'Order',
 			'added' => 'Added',
 			'added_by' => 'Added By',
 			'updated' => 'Updated',
 			'updated_by' => 'Updated By',
 		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter
conditions.
	 * @return CActiveDataProvider the data provider that can return
the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria; 		$criteria->compare('id',$this->id,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('cms_menu_id',$this->cms_menu_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('append_to_parent_url',$this->append_to_parent_url);
		$criteria->compare('css_classes',$this->css_classes,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
