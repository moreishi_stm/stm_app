<?php

/**
 * This is the model class for table "sms_blasts".
 *
 * The followings are the available columns in table 'sms_blasts':
 * @property integer $id
 * @property string $status
 * @property string $description
 * @property integer $sender_contact_id
 * @property string $recipient_criteria
 * @property integer $recipient_count
 * @property string $send_datetime
 * @property integer $sent_count
 * @property string $start_send_datetime
 * @property string $complete_datetime
 * @property integer $sms_template_id
 * @property string $sms_body
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property SmsTemplates $smsTemplate
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 * @property Contacts $senderContact
 */
class SmsBlasts extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SmsBlasts the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sms_blasts';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sender_contact_id, sms_template_id, sms_body, added_by', 'required'),
            array('sender_contact_id, recipient_count, sent_count, sms_template_id, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('status', 'length', 'max'=>5),
            array('description', 'length', 'max'=>500),
            array('recipient_criteria, send_datetime, start_send_datetime, complete_datetime, updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, status, description, sender_contact_id, recipient_criteria, recipient_count, send_datetime, sent_count, start_send_datetime, complete_datetime, sms_template_id, sms_body, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'smsTemplate' => array(self::BELONGS_TO, 'SmsTemplates', 'sms_template_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'senderContact' => array(self::BELONGS_TO, 'Contacts', 'sender_contact_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'description' => 'Description',
            'sender_contact_id' => 'Sender Contact',
            'recipient_criteria' => 'Recipient Criteria',
            'recipient_count' => 'Recipient Count',
            'send_datetime' => 'Send Datetime',
            'sent_count' => 'Sent Count',
            'start_send_datetime' => 'Start Send Datetime',
            'complete_datetime' => 'Complete Datetime',
            'sms_template_id' => 'Sms Template',
            'sms_body' => 'Sms Body',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->status = 'Draft';
            $this->added_by = Yii::app()->user->id;
            $this->added = new CDbExpression('NOW()');
        }

        $this->updated_by = Yii::app()->user->id;
        $this->updated = new CDbExpression('NOW()');

        if($this->send_datetime) {
            if(is_string($this->send_datetime)) { // && (get_class($this->send_datetime) != "CDbExpression")
                $this->send_datetime = date(StmFormatter::MYSQL_DATETIME_FORMAT, strtotime($this->send_datetime));
            }
        }

        if(is_string($this->send_datetime) && date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($this->send_datetime)) < date("Y-m-d")) {
            $this->addError("send_datetime", "Send date cannot be in the past.");
        }

        return parent::beforeValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('sender_contact_id',$this->sender_contact_id);
        $criteria->compare('recipient_criteria',$this->recipient_criteria,true);
        $criteria->compare('recipient_count',$this->recipient_count);
        $criteria->compare('send_datetime',$this->send_datetime,true);
        $criteria->compare('sent_count',$this->sent_count);
        $criteria->compare('start_send_datetime',$this->start_send_datetime,true);
        $criteria->compare('complete_datetime',$this->complete_datetime,true);
        $criteria->compare('sms_template_id',$this->sms_template_id);
        $criteria->compare('sms_body',$this->sms_body,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}