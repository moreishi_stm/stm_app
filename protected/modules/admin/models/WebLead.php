<?php
Yii::import('admin_module.components.observers.leads.*');
/**
 * Class WebLead
 * A weblead instance is generated whenever a user submits their personal data via the front module
 * a 'lead' is business terminology for a transaction
 */
class WebLead extends Transactions {

    private $_alertLabel;
    public $formSubmission;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Transactions the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @param string $scenario
	 */
	public function __construct($scenario = 'insert') {
		parent::__construct($scenario);
	}

	public function setTransactionType($componentTypeId) {
		switch($componentTypeId) {
			case ComponentTypes::BUYERS:
				$transactionType = Transactions::BUYERS;
			break;

			case ComponentTypes::SELLERS:
				$transactionType = Transactions::SELLERS;
			break;
		}

        $utmData = StmTrafficTracker::getUtmTrackingData();
        // see if source exists, if not create one.
        if(!empty($utmData['campaign_source'])) {
            if($source = Sources::model()->findByAttributes(array('name'=>$utmData['campaign_source']))) {
                $trafficSource = $source->id;
            } else {
                $source = new Sources;
                $source->account_id = Yii::app()->user->accountId;
                $source->name = $utmData['campaign_source'];
//                if ((strpos($utmData['campaign_name'], '(organic)')!==false or strpos($utmData['campaign_name'], '(direct)')!== false) AND
//                    ($utmData['campaign_source'] !== '(organic)' && $utmData['campaign_source'] !== '(direct)')) {
//                    $source->name .= ' '.$utmData['campaign_name'];
//                }

                if(!$source->save()) {
                    $trafficSource = Sources::ID_WEBSITE;
                }
                $trafficSource = $source->id;
            }
        } else {
            $trafficSource = Sources::ID_WEBSITE;
        }

        $this->source_id = $trafficSource;
		$this->transactionType = $transactionType;
		$this->component_type_id = $componentTypeId;
		$this->transaction_status_id = TransactionStatus::NEW_LEAD_ID;
	}

	public function init() {

		/**
		 * Attach the lead observers, that will create the lead based on different lead route types
		 * Allows for unique route interaction with a newly created lead
		 */
		$this->attachEventHandler('onInitLead', array(new AgentRouteObserver, 'initLead'));
	}

	protected function beforeValidate() {
		if ($this->getIsNewRecord()) {
			// Sets up the lead to be saved
			$event = $this->initLead();
            if($event->sender->getIsResubmit()) {
                return false;
            }

			/**
			 * Attach observers to process any modifications to a lead once they are saved by route
			 * This way routes can respond differently to a lead being modified (update or removal)
			 */
			$this->attachEventHandler('onAfterSave', array(new AgentRouteObserver, 'afterLeadCreated'));

		}
		return $event->isValid;
	}

	protected function initLead() {
		if ($this->hasEventHandler('onInitLead')) {
			$event = new CModelEvent($this);
			$this->onInitLead($event);

			return $event;
		}
	}

	public function onInitLead($event) {
		$this->raiseEvent('onInitLead', $event);
	}
}
