<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class CoBrokerAssignmentGroup extends AssignmentGroup
{

    protected function assignmentAssigneeComponentColumn() {

        return 'assignee_contact_id';
    }


    public function getAssignmentTypeIds()
    {

        return array(AssignmentTypes::CO_BROKER_AGENT);
    }
}