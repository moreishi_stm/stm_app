<?php
/**
 * This is the model class for table "lead_round_robin_contact_lu".
 *
 * The followings are the available columns in table 'lead_round_robin_contact_lu':
 * @property integer $lead_route_id
 * @property integer $contact_id
 * @property integer $leads_per_round
 * @property integer $sort_order
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property LeadRoutes $leadRoute
 * @property Contacts $updatedBy
 * @property Contacts $contact
 * @property Contacts $addedBy
 */
class LeadRoundRobinContactLu extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeadRoundRobinContactLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lead_round_robin_contact_lu';
    }

    public function primaryKey() {
        return array(
            'lead_route_id',
            'contact_id'
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lead_route_id, contact_id, sort_order', 'required'),
            array('lead_route_id, contact_id, leads_per_round, sort_order, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('lead_route_id, contact_id, leads_per_round, sort_order, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'leadRoute' => array(self::BELONGS_TO, 'LeadRoutes', 'lead_route_id'),
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'lead_route_id' => 'Lead Route',
            'contact_id' => 'Contact',
            'leads_per_round' => 'Leads Per Round',
            'sort_order' => 'Sort Order',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    public function scopes()
    {
        return array(
            'orderBySort' => array(
                'order' => 'sort_order ASC'
            ),
        );
    }

    protected function beforeValidate() {
        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        if($this->isNewRecord) {
            $this->added = $this->updated;
            $this->added_by = $this->updated_by;
        }

        return parent::beforeValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('lead_route_id',$this->lead_route_id);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('leads_per_round',$this->leads_per_round);
        $criteria->compare('sort_order',$this->sort_order);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}