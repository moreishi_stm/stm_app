<?php

/**
 * This is the model class for table "lead_route_autoresponders".
 *
 * The followings are the available columns in table 'lead_route_autoresponders':
 * @property integer $id
 * @property integer $lead_route_id
 * @property integer $type_ma
 * @property integer $component_id
 * @property string $added
 * @property integer $added_by
 * @property string $updated
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 * @property LeadRoutes $leadRoute
 */
class LeadRouteAutoresponder extends StmBaseActiveRecord
{
	const TYPE_EMAIL = 1;
	const TYPE_ACTION_PLAN = 2;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LeadRouteAutoresponder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lead_route_autoresponders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lead_route_id, type_ma, component_id, added, added_by, updated, updated_by', 'required'),
			array('lead_route_id, type_ma, component_id, added_by, updated_by', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, lead_route_id, type_ma, component_id, added, added_by, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
			'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
			'leadRoute' => array(self::BELONGS_TO, 'LeadRoutes', 'lead_route_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lead_route_id' => 'Lead Route',
			'type_ma' => 'Type Ma',
			'component_id' => 'Component',
			'added' => 'Added',
			'added_by' => 'Added By',
			'updated' => 'Updated',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @param Contacts $agentToNotify
	 * @param Contacts $applicableContact
	 * @return bool|void
	 */
	public function apply(Contacts $agentToNotify, $componentModel) {
		if (!$this->getIsValidType()) {
			return false;
		}

		$isApplied = false;
		switch ($this->type_ma) {
			case self::TYPE_EMAIL:
				$responderComponent = EmailTemplates::model()->findByPk($this->component_id);
				$isApplied = $this->applyAutoEmailResponder($responderComponent, $agentToNotify, $componentModel);
			break;

			case self::TYPE_ACTION_PLAN:
				$responderComponent = ActionPlans::model()->findByPk($this->component_id);

                if(!$responderComponent) {

                    break;
                }

                // check to see if action plan is already applied and if so skip
                if($responderComponent->isApplied($componentModel)===false) {
                    $isApplied = $responderComponent->apply($componentModel); //@todo: right now the returns on this model is not right, override in next line for now
                    $isApplied = true;
                }
                else {
                    $isApplied = true;
                }

			break;
		}

		return $isApplied;
	}

	/**
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @param EmailTemplates $emailTemplateToSend
	 * @return boolean
	 */
	private function applyAutoEmailResponder(EmailTemplates $emailTemplateToSend, Contacts $agentToNotify, $componentModel)
    {
		$emailMessage = new EmailMessage;

        $signature = '';
        if($signatureSetting = SettingContactValues::model()->findByAttributes(array('contact_id' => $agentToNotify->id, 'setting_id'=>Settings::SETTING_EMAIL_SIGNATURE))) {
            $signature .= $signatureSetting->value;
        }

		$emailMessage->setAttributes(array(
			'subject' => $emailTemplateToSend->subject,
			'content' => $emailTemplateToSend->filterBodyByContactData($componentModel->contact, $componentModel).$signature,
			'component_type_id' => $componentModel->componentType->id,
			'component_id' => $componentModel->id,
			'from_email_id' => $agentToNotify->getPrimaryEmailObj()->id,
			'to_email_id' => $componentModel->contact->getPrimaryEmailObj()->id,
		));

		$isApplied = false;
		if ($emailMessage->save())
        {
            if(!YII_DEBUG)
            {
                // Create email message
                $zendMessage = new StmZendMail();
                $zendMessage->addTo($emailMessage->toEmail->email, Yii::app()->format->formatProperCase($emailMessage->toEmail->contact->getFullName()));
                $replyTo = $zendMessage->getReplyToEmail($emailMessage, Yii::app()->user->replyToDomain->name);
                $zendMessage->setFrom($replyTo, Yii::app()->format->formatProperCase($emailMessage->fromEmail->contact->getFullName()));
                $zendMessage->setSubject($emailMessage->subject);
                $zendMessage->setBodyHtmlTracking($emailMessage, Yii::app()->user->replyToDomain->name);

                // Attempt to send message
                try {
                    $zendMessage->send(StmZendMail::getAwsSesTransport());
                    $isApplied = 1; //@todo temp hard code in 1 cuz new mail class doesn't return anything
                }
                catch(Exception $e) {
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '"' . PHP_EOL . ' Exception Message:' . $e->getMessage().PHP_EOL
                        .'To Email ID: '.$emailMessage->toEmail->id.' | Sender: '.$zendMessage->getFrom().' | Recipients'.print_r($zendMessage->getRecipients(), true),CLogger::LEVEL_ERROR);
                }

            } else {
                $isApplied = 1;
            }
		}

		return $isApplied;
	}

	/**
	 * Returns if the type is valid
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @return array
	 */
	public function getIsValidType() {
		$validTypes = array(self::TYPE_EMAIL, self::TYPE_ACTION_PLAN);
		return in_array($this->type_ma, $validTypes);
	}

	/**
	 * @return array behaviors for model attributes.
	 */
	public function behaviors() {
		return array(
			'modelAttribute' => array(
				'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
			),
		);
	}

	public static function getTypes() {
		return array(
			self::TYPE_EMAIL => 'Email',
			self::TYPE_ACTION_PLAN => 'Action Plan',
		);
	}

	protected function beforeValidate() {
		if ($this->isNewRecord) {
			$this->added = date('Y-m-d H:i:s');
			$this->added_by = Yii::app()->user->id;
		}

		$this->updated_by = Yii::app()->user->id;
		$this->updated = date('Y-m-d H:i:s');

		return parent::beforeSave();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lead_route_id',$this->lead_route_id);
		$criteria->compare('type_ma',$this->type_ma);
		$criteria->compare('component_id',$this->component_id);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}