<?php

/**
 * This is the model class for table "accounting_accounts".
 *
 * The followings are the available columns in table 'accounting_accounts':
 * @property integer $id
 * @property integer $account_type
 * @property integer $status_ma
 * @property string $code
 * @property integer $parent_id
 * @property string $name
 * @property string $notes
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property AccountingAccounts $parent
 * @property AccountingAccounts[] $accountingAccounts
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */

class AccountingAccounts extends StmBaseActiveRecord
{
	const MARKETING_LEAD_GEN = 1;
    const SALES_COMMISSION = 2;
    const REFERRAL_FEE_INTERNAL = 3;
    const CLOSING_BONUS = 4;

    // account type
    // income
    // expense

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccountingAccounts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'accounting_accounts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_type, name, updated_by, updated, added_by, added', 'required'),
			array('account_type, status_ma, parent_id, updated_by, added_by', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>15),
			array('name', 'length', 'max'=>127),
			array('notes', 'length', 'max'=>255),
			array('updated, added', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_type, status_ma, code, parent_id, name, notes, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'AccountingAccounts', 'parent_id'),
			'accountingAccounts' => array(self::HAS_MANY, 'AccountingAccounts', 'parent_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
			'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_type' => 'Account Type',
			'status_ma' => 'Status Ma',
			'code' => 'Code',
			'parent_id' => 'Parent',
			'name' => 'Name',
			'notes' => 'Notes',
			'updated_by' => 'Updated By',
			'updated' => 'Updated',
			'added_by' => 'Added By',
			'added' => 'Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_type',$this->account_type);
		$criteria->compare('status_ma',$this->status_ma);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('added',$this->added,true);

		return new CActiveDataProvider($this, array(
											  'criteria'=>$criteria,
											  ));
	}
}