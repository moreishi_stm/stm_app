<?php

	/**
	 * This is the model class for table "cms_content_tag_lu".
	 *
	 * The followings are the available columns in table 'cms_content_tag_lu':
	 *
	 * @property integer    $cms_content_id
	 * @property integer    $cms_tag_id
	 *
	 * The followings are the available model relations:
	 * @property CmsContent $cmsContent
	 * @property CmsTags    $cmsTag
	 */
	class CmsContentTagLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return CmsContentTagLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'cms_content_tag_lu';
		}

		public function getPrimaryKey() {
			return array(
				'cms_content_id',
				'cms_tag_id'
			);
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'cms_content_id, cms_tag_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'cms_content_id, cms_tag_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'cmsContent' => array(
					self::BELONGS_TO,
					'CmsContent',
					'cms_content_id'
				),
				'cmsTag' => array(
					self::BELONGS_TO,
					'CmsTags',
					'cms_tag_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'cms_content_id' => 'Cms Content',
				'cms_tag_id' => 'Cms Tag',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('cms_content_id', $this->cms_content_id);
			$criteria->compare('cms_tag_id', $this->cms_tag_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}