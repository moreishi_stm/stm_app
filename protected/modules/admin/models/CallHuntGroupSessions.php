<?php

/**
 * This is the model class for table "call_hunt_group_sessions".
 *
 * The followings are the available columns in table 'call_hunt_group_sessions':
 * @property string $id
 * @property integer $call_hunt_group_id
 * @property string $call_hunt_group_session_call_id
 * @property string $call_uuid
 * @property string $from_number
 * @property string $from_caller_name
 * @property string $to_number
 * @property integer $component_type_id
 * @property integer $component_id
 * @property string $source_type
 * @property integer $exclude_from_stats
 * @property string $exclude_reason_other
 * @property string $exclude_reason
 * @property string $total_cost
 * @property integer $bill_duration
 * @property string $hangup_cause
 * @property string $bill_rate
 * @property string $answer_time
 * @property string $start_time
 * @property integer $duration
 * @property string $end_time
 * @property string $call_status
 * @property string $event
 * @property string $added
 * @property string $updated
 * @property string $recording_id
 * @property string $recording_call_uuid
 * @property string $recording_url
 * @property integer $recording_duration
 * @property integer $recording_duration_ms
 * @property string $recording_start_ms
 * @property string $recording_end_ms
 * @property string $recording_added
 *
 * The followings are the available model relations:
 * @property CallHuntGroupSessionCalls[] $callHuntGroupSessionCalls
 * @property CallHuntGroupSessionVoicemailMessages[] $callHuntGroupSessionVoicemailMessages
 * @property CallHuntGroups $callHuntGroup
 * @property CallHuntGroupSessionCalls $callHuntGroupSessionCall
 */
class CallHuntGroupSessions extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallHuntGroupSessions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_hunt_group_sessions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_hunt_group_id, call_uuid, from_number, added', 'required'),
            array('call_hunt_group_id, component_type_id, component_id, exclude_from_stats, bill_duration, duration, recording_duration, recording_duration_ms', 'numerical', 'integerOnly'=>true),
            array('call_hunt_group_session_call_id', 'length', 'max'=>11),
            array('call_uuid, recording_id, recording_call_uuid', 'length', 'max'=>36),
            array('from_number, to_number', 'length', 'max'=>32),
            array('from_caller_name', 'length', 'max'=>20),
            array('source_type, recording_start_ms, recording_end_ms', 'length', 'max'=>16),
            array('exclude_reason_other, recording_url', 'length', 'max'=>100),
            array('exclude_reason', 'length', 'max'=>18),
            array('total_cost, bill_rate', 'length', 'max'=>9),
            array('hangup_cause, call_status, event', 'length', 'max'=>50),
            array('answer_time, start_time, end_time, updated, recording_added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_hunt_group_id, call_hunt_group_session_call_id, call_uuid, from_number, from_caller_name, to_number, component_type_id, component_id, source_type, exclude_from_stats, exclude_reason_other, exclude_reason, total_cost, bill_duration, hangup_cause, bill_rate, answer_time, start_time, duration, end_time, call_status, event, added, updated, recording_id, recording_call_uuid, recording_url, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, recording_added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callHuntGroupSessionCalls' => array(self::HAS_MANY, 'CallHuntGroupSessionCalls', 'call_hunt_group_session_id'),
            'callHuntGroupSessionVoicemailMessages' => array(self::HAS_MANY, 'CallHuntGroupSessionVoicemailMessages', 'call_hunt_group_session_id'),
            'callHuntGroup' => array(self::BELONGS_TO, 'CallHuntGroups', 'call_hunt_group_id'),
            'callHuntGroupSessionCall' => array(self::BELONGS_TO, 'CallHuntGroupSessionCalls', 'call_hunt_group_session_call_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_hunt_group_id' => 'Call Hunt Group',
            'call_hunt_group_session_call_id' => 'Call Hunt Group Session Call',
            'call_uuid' => 'Call Uuid',
            'from_number' => 'From Number',
            'from_caller_name' => 'From Caller Name',
            'to_number' => 'To Number',
            'component_type_id' => 'Component Type',
            'component_id' => 'Component',
            'source_type' => 'Source Type',
            'exclude_from_stats' => 'Exclude From Stats',
            'exclude_reason_other' => 'Exclude Reason Other',
            'exclude_reason' => 'Exclude Reason',
            'total_cost' => 'Total Cost',
            'bill_duration' => 'Bill Duration',
            'hangup_cause' => 'Hangup Cause',
            'bill_rate' => 'Bill Rate',
            'answer_time' => 'Answer Time',
            'start_time' => 'Start Time',
            'duration' => 'Duration',
            'end_time' => 'End Time',
            'call_status' => 'Call Status',
            'event' => 'Event',
            'added' => 'Added',
            'updated' => 'Updated',
            'recording_id' => 'Recording',
            'recording_call_uuid' => 'Recording Call Uuid',
            'recording_url' => 'Recording Url',
            'recording_duration' => 'Recording Duration',
            'recording_duration_ms' => 'Recording Duration Ms',
            'recording_start_ms' => 'Recording Start Ms',
            'recording_end_ms' => 'Recording End Ms',
            'recording_added' => 'Recording Added',
        );
    }

    protected function beforeSave()
    {
        $this->updated = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    public static function getExcludeReasonList()
    {
        return array(
            'Duplicate' => 'Duplicate',
            'From Office / Team' => 'From Office / Team',
            'Solicitation' => 'Solicitation',
            'Bad Number' => 'Bad Number',
            'Other' => 'Other',
        );
    }

//    public function getTelephonyPhone()
//    {
//        if(!$this->to_number) {
//            return null;
//        }
//
//        return TelephonyPhones::model()->findByAttributes(array('phone' => substr($this->to_number, 1)));
//    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('call_hunt_group_id',$this->call_hunt_group_id);
        $criteria->compare('call_hunt_group_session_call_id',$this->call_hunt_group_session_call_id,true);
        $criteria->compare('call_uuid',$this->call_uuid,true);
        $criteria->compare('from_number',$this->from_number,true);
        $criteria->compare('from_caller_name',$this->from_caller_name,true);
        $criteria->compare('to_number',$this->to_number,true);
        $criteria->compare('component_type_id',$this->component_type_id);
        $criteria->compare('component_id',$this->component_id);
        $criteria->compare('source_type',$this->source_type,true);
        $criteria->compare('exclude_from_stats',$this->exclude_from_stats);
        $criteria->compare('exclude_reason_other',$this->exclude_reason_other,true);
        $criteria->compare('exclude_reason',$this->exclude_reason,true);
        $criteria->compare('total_cost',$this->total_cost,true);
        $criteria->compare('bill_duration',$this->bill_duration);
        $criteria->compare('hangup_cause',$this->hangup_cause,true);
        $criteria->compare('bill_rate',$this->bill_rate,true);
        $criteria->compare('answer_time',$this->answer_time,true);
        $criteria->compare('start_time',$this->start_time,true);
        $criteria->compare('duration',$this->duration);
        $criteria->compare('end_time',$this->end_time,true);
        $criteria->compare('call_status',$this->call_status,true);
        $criteria->compare('event',$this->event,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('recording_id',$this->recording_id,true);
        $criteria->compare('recording_call_uuid',$this->recording_call_uuid,true);
        $criteria->compare('recording_url',$this->recording_url,true);
        $criteria->compare('recording_duration',$this->recording_duration);
        $criteria->compare('recording_duration_ms',$this->recording_duration_ms);
        $criteria->compare('recording_start_ms',$this->recording_start_ms,true);
        $criteria->compare('recording_end_ms',$this->recording_end_ms,true);
        $criteria->compare('recording_added',$this->recording_added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}