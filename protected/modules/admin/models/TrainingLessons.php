<?php

/**
 * This is the model class for table "training_lessons".
 *
 * The followings are the available columns in table 'training_lessons':
 * @property integer $id
 * @property string $training_id
 * @property string $name
 * @property string $description
 * @property integer $duration
 * @property string $start_datetime
 * @property string $due_date
 * @property string $content
 * @property integer $sort_order
 * @property integer $is_deleted
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Trainings $training
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */
class TrainingLessons extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TrainingLessons the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'training_lessons';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('training_id, name, description, duration, start_datetime, due_date, content, updated_by, updated, added_by, added', 'required'),
            array('duration, sort_order, is_deleted, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('training_id', 'length', 'max'=>11),
            array('name', 'length', 'max'=>100),
            array('description', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, training_id, name, description, duration, start_datetime, due_date, content, sort_order, is_deleted, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'training' => array(self::BELONGS_TO, 'Trainings', 'training_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'training_id' => 'Training',
            'name' => 'Name',
            'description' => 'Description',
            'duration' => 'Duration',
            'start_datetime' => 'Start Datetime',
            'due_date' => 'Due Date',
            'content' => 'Content',
            'sort_order' => 'Sort Order',
            'is_deleted' => 'Is Deleted',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('training_id',$this->training_id,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('duration',$this->duration);
        $criteria->compare('start_datetime',$this->start_datetime,true);
        $criteria->compare('due_date',$this->due_date,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('sort_order',$this->sort_order);
        $criteria->compare('is_deleted',$this->is_deleted);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}