<?php

/**
 * This is the model class for table "email_message_attachments".
 *
 * The followings are the available columns in table 'email_message_attachments':
 * @property integer $id
 * @property integer $email_message_id
 * @property string $name
 * @property string $type
 * @property string $size
 * @property string $added
 *
 * The followings are the available model relations:
 * @property EmailMessage $emailMessage
 */
class EmailMessageAttachments extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EmailMessageAttachments the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'email_message_attachments';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email_message_id, name, type, size, added', 'required'),
            array('email_message_id', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>255),
            array('type', 'length', 'max'=>100),
            array('size', 'length', 'max'=>10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, email_message_id, name, type, size, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'emailMessage' => array(self::BELONGS_TO, 'EmailMessages', 'email_message_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'email_message_id' => 'Email Message',
            'name' => 'Name',
            'type' => 'Type',
            'size' => 'Size',
            'added' => 'Added',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('email_message_id',$this->email_message_id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('size',$this->size,true);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }

    /**
     * Before Save
     *
     * @return bool
     */
    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->added = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }
}