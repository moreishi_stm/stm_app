<?php

	/**
	 * This is the model class for table "goals".
	 *
	 * The followings are the available columns in table 'goals':
	 *
	 * @property integer  $id
	 * @property integer  $contact_id
	 * @property integer  $status_ma
	 * @property string   $name
	 * @property integer  $year
	 * @property string   $start_date
	 * @property string   $end_date
	 * @property integer  $average_sales_price
	 * @property integer  $lead_gen_min_per_day
	 * @property integer  $income_per_year
     * @property integer  $fees
	 * @property integer  $average_commission_percent
	 * @property string   $contracts_closing_conversion
	 * @property string   $agreements_contract_conversion
	 * @property string   $appointments_agreement_conversion
     * @property string   $appointments_set_met_conversion
	 * @property integer  $contacts_per_appointment
     * @property integer  $contacts_per_hour
	 * @property integer  $contacts_per_day
	 * @property string   $referral_percentage
	 * @property integer  $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts $contact
	 */
	class Goals extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Goals the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'goals';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id, status_ma, name, year, start_date, end_date, average_sales_price, lead_gen_minutes_per_day, income_per_year, average_commission_percent, commission_split, contracts_closing_conversion, agreements_contract_conversion, appointments_agreement_conversion, appointments_set_met_conversion, contacts_per_appointment, contacts_per_hour, contacts_per_day',
					'required'
				),
				array(
					'contact_id, status_ma, year, average_sales_price, lead_gen_minutes_per_day, weeks_time_off, income_per_year, fees, contacts_per_appointment, contacts_per_hour, contacts_per_day, is_deleted',
					'numerical',
					'integerOnly' => true
				),
                array(
                    'year_id',
                    'validateYear'
                ),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'contracts_closing_conversion, agreements_contract_conversion, appointments_agreement_conversion, referral_percentage',
					'length',
					'max' => 5
				),
                array(
                    'start_date',
                    'validateStartEndDate'
                ),
				array(
					'start_date, end_date, commission_split',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, status_ma, name, year, start_date, end_date, average_sales_price, lead_gen_minutes_per_day, weeks_time_off, income_per_year, fees, average_commission_percent, commission_split, contracts_closing_conversion, agreements_contract_conversion, appointments_agreement_conversion, appointments_set_met_conversion, contacts_per_appointment, contacts_per_hour, contacts_per_day, referral_percentage, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
                'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
                'weeklyGoals' => array(self::HAS_MANY, 'GoalsWeekly', 'goal_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'status_ma' => 'Status Ma',
				'name' => 'Goal Name',
				'year' => 'Year',
				'start_date' => 'Start Date',
				'end_date' => 'End Date',
				'average_sales_price' => 'Average Sales Price',
				'average_commission_percent' => 'Avg Gross Commission %',
				'commission_split' => 'Commission Split',
				'lead_gen_minutes_per_day' => 'Daily Lead Gen Minutes',
				'weeks_time_off' => 'Weeks Time Off',
				'income_per_year' => 'Annual Income',
                'fees' => 'Fees',
				'contracts_closing_conversion' => 'Contracts-to-Closing',
				'agreements_contract_conversion' => 'Agreements-to-Contract',
				'appointments_agreement_conversion' => 'Appts-to-Agreement',
				'contacts_per_appointment' => 'Contacts Per Appt',
                'contacts_per_hour' => 'Contacts Per Hour',
				'contacts_per_day' => 'Contacts Per Day',
				// leave in for now...
				'referral_percentage' => 'Referral Percentage',
				'is_deleted' => 'Is Deleted',
				'incomePerClosing' => 'Income per Closing',
			);
		}

		public function getMonthlyContacts() {
			if (isset($this->contacts_per_day)) {
				return $this->contacts_per_day * 20;
			} else {
				return 0;
			}
		}

		public function getIncomePerClosing() {
			if (!empty($this->average_sales_price) && !empty($this->average_commission_percent) && !empty($this->commission_split)) {
				return ceil($this->average_sales_price * $this->average_commission_percent * $this->commission_split
				);
			} else {
				return 0;
			}
		}

		public function getMonthlyLeadGen() {
			return ceil($this->lead_gen_minutes_per_day * 16 / 60);
		}

		public function getMonthlyAppointments() {
			return ceil($this->annualAppointments / 12);
		}

		public function getMonthlyAgreements() {
			return ceil($this->annualAgreements / 12);
		}

		public function getMonthlyContracts() {
			return ceil($this->annualContracts / 12);
		}

		public function getMonthlyClosings() {
			return ceil($this->annualClosings / 12);
		}

		public function getMonthlyIncome() {
			return ceil($this->income_per_year / 12);
		}

		public function getAnnualClosings() {
			if (isset($this->income_per_year) && !empty($this->income_per_year) && $this->incomePerClosing) {
				return ceil($this->income_per_year / $this->incomePerClosing);
			} else {
				return 0;
			}
		}

		public function getAnnualContracts() {
			if (isset($this->annualClosings) && $this->annualClosings && !empty($this->contracts_closing_conversion)) {
				return ceil($this->annualClosings / $this->contracts_closing_conversion);
			} else {
				return 0;
			}
		}

		public function getAnnualAgreements() {
			if (isset($this->annualContracts) && $this->annualContracts && !empty($this->agreements_contract_conversion)) {
				return ceil($this->annualContracts / $this->agreements_contract_conversion
				);
			} else {
				return 0;
			}
		}

		public function getAnnualAppointments() {
			if (isset($this->annualAgreements) && $this->annualAgreements && !empty($this->appointments_agreement_conversion)) {
				return ceil($this->annualAgreements / $this->appointments_agreement_conversion
				);
			} else {
				return 0;
			}
		}

		public function getAnnualContacts() {
			return ceil($this->annualAppointments * $this->contacts_per_appointment);
		}

		public function getWeeklyContacts() {
			return ceil($this->annualContacts / (52 - $this->weeks_time_off));
		}

		public function getDailyContacts() {
			return ceil($this->getWeeklyContacts() / 4);
		}

        public function getAnnualLeadGen() {
            return ceil($this->lead_gen_minutes_per_day / 60 * 5 * (52 - $this->weeks_time_off));
        }

        public function getActualLeadGen($opts = null) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$actualLeadGen = 0;
			$Timeblock = Timeblockings::model()->byCurrentUser()->dateRange($opts)->byTypes(Timeblockings::LEAD_GEN_TYPE_PHONE_ID)->calculateMinutes()->find();

			return number_format($Timeblock->minutes);
		}

		public function getDueLeadGen() {
			return number_format($this->monthlyLeadGen * $this->percent);
		}

		public function getActualDials() {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			return ActivityLog::countLeadGenDials($opts);
		}

		public function getDueDials() {
			return number_format($this->dueContacts * 3);
		}

		public function getActualContacts($opts = null) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			return ActivityLog::countLeadGenContacts($opts);
		}

		public function getDueContacts() {
			return number_format($this->monthlyContacts * $this->percent);
		}

		public function getActualAppts() {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			return Appointments::countMetAppointments($opts);
		}

		public function getDueAppts() {
			return number_format($this->monthlyAppointments * $this->percent);
		}

		public function getActualSigned() {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			return Appointments::countSignedAppointments($opts);
		}

		public function getDueSigned() {
			return number_format($this->monthlyAgreements * $this->percent);
		}

		public function getActualContracts() {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);
			$opts['contactId'] = Yii::app()->user->id;

			return Closings::getCountExecutedByAssigneeId($opts);
		}

		public function getDueContracts() {
			return number_format($this->monthlyContracts * $this->percent);
		}

		public function getActualClosings() {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);
			$opts['contactId'] = Yii::app()->user->id;

			return Closings::countClosings($opts);
		}

		public function getDueClosings() {
			return number_format($this->monthlyClosings * $this->percent);
		}

		public function getActualIncome() {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);
			$opts['contactId'] = Yii::app()->user->id;

			return $this->getNetIncome(Closings::countIncome($opts));
		}

		public function getNetIncome($income) {
			return $income * $this->commission_split;
		}


		public function getDueIncome() {
			return $this->monthlyIncome * $this->percent;
		}

		public function getPercent() {
			return (int)date('d') / cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
		}

		public function getCommissionSplitDisplay() {
			return sprintf('%02s', ($this->commission_split * 100));
		}

		/**
		 * Return the provider used for the monthly goals
		 *
		 * @param null $opts
		 *
		 * @return CArrayDataProvider
		 */
		public function getMonthlyGoalProvider($opts = null) {
			//		if(empty($opts) || !isset($opts['from_date']) || !isset($opts['to_date']) || !empty($opts['from_date']) || !empty($opts['to_date'])) {
			//			if(!isset($opts['from_date']) || empty($opts['from_date'])) {
			//				$opts['from_date'] = date('Y-m-d');
			//			}
			//		}

			$goalsArray = array(
				array(
					'type' => 'Lead Gen',
					'actual' => $this->getActualLeadGen($opts),
					'due' => $this->dueLeadGen,
					'goal' => $this->monthlyLeadGen,
					'track' => StmHtml::booleanImage(($this->actualLeadGen >= $this->dueLeadGen) ? true : false),
				),
				array(
					'type' => 'Dials',
					'actual' => $this->actualDials,
					'due' => $this->dueDials,
					'goal' => $this->monthlyContacts * 3,
					'track' => StmHtml::booleanImage(($this->actualDials >= $this->dueDials) ? true : false),
				),
				array(
					'type' => 'Contacts',
					'actual' => $this->actualContacts,
					'due' => $this->dueContacts,
					'goal' => $this->monthlyContacts,
					'track' => StmHtml::booleanImage(($this->actualContacts >= $this->dueContacts) ? true : false),
				),
				array(
					'type' => 'Appts',
					'actual' => $this->actualAppts,
					'due' => $this->dueAppts,
					'goal' => $this->monthlyAppointments,
					'track' => StmHtml::booleanImage(($this->actualAppts >= $this->dueAppts) ? true : false),
				),
				array(
					'type' => 'Signed',
					'actual' => $this->actualSigned,
					'due' => $this->dueSigned,
					'goal' => $this->monthlyAgreements,
					'track' => StmHtml::booleanImage(($this->actualSigned >= $this->dueSigned) ? true : false),
				),
				array(
					'type' => 'Contracts',
					'actual' => $this->actualContracts,
					'due' => $this->dueContracts,
					'goal' => $this->monthlyContracts,
					'track' => StmHtml::booleanImage(($this->actualContracts >= $this->dueContracts) ? true : false),
				),
				array(
					'type' => 'Closings',
					'actual' => $this->actualClosings,
					'due' => $this->dueClosings,
					'goal' => $this->monthlyClosings,
					'track' => StmHtml::booleanImage(($this->actualClosings >= $this->dueClosings) ? true : false),
				),
				array(
					'type' => 'Income',
					'actual' => Yii::app()->format->formatDollars($this->actualIncome),
					'due' => Yii::app()->format->formatDollars($this->dueIncome),
					'goal' => Yii::app()->format->formatDollars($this->monthlyIncome),
					'track' => StmHtml::booleanImage(($this->actualIncome >= $this->dueIncome) ? true : false),
				),
			);

			return new CArrayDataProvider($goalsArray);
		}

		public function findByContactId($contactId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);

			return $this->find($Criteria);
		}

		public static function getListByContactId($contactId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);

			return Goals::model()->findAll($Criteria);
		}

		public function scopes() {
			return array(
				'byCurrentUser' => array(
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => Yii::app()->user->contact->id),
				),
				'asc' => array(
					'order' => 'id asc',
				),
				'desc' => array(
					'order' => 'id desc',
				),
			);
		}

		protected function beforeValidate() {
			$fields = array(
				'average_sales_price',
				'income_per_year'
			);
			// If the value is a number based field strip commas or decimals from it.
			foreach ($fields as $field) {
				if ($this->$field) {
					$this->$field = Yii::app()->format->formatInteger($this->$field);
				}
			}

            if ($this->fees) {
                $this->fees = Yii::app()->format->formatInteger($this->fees);
            }

			//contacts_per_day
			$this->contacts_per_day = $this->getDailyContacts();

			if ($this->start_date) {
				$this->start_date = Yii::app()->format->formatDate($this->start_date, StmFormatter::MYSQL_DATE_FORMAT);
			}

			if ($this->end_date) {
				$this->end_date = Yii::app()->format->formatDate($this->end_date, StmFormatter::MYSQL_DATE_FORMAT);
			}

			return parent::beforeValidate();
		}

        public function getHasWeeklyGoals()
        {
            return (boolean) GoalsWeekly::model()->count(array('condition'=>'goal_id='.$this->id));
        }

        public static function getThisYearGoal($contactId)
        {
            return Goals::model()->find(array('condition'=>'contact_id='.$contactId.' AND year='.date('Y')));
        }

        public function validateYear($attribute, $params)
        {
            if ($this->isNewRecord) {
                if(Goals::model()->findByAttributes(array('year'=>$this->year, 'contact_id'=>$this->contact_id))) {
                    $this->addError('year', 'Goal already exists for this year.');
                }
            }
        }

        public function validateStartEndDate($attribute, $params)
        {
            if ($this->year !== date('Y', strtotime($this->start_date))) {
                $this->addError('start_date', 'Start Date must be the same year as the Goal Year.');
            }

            if ($this->year !== date('Y', strtotime($this->end_date))) {
                $this->addError('end_date', 'End Date must be the same year as the Goal Year.');
            }
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('status_ma', $this->status_ma);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('year', $this->year);
			$criteria->compare('start_date', $this->start_date, true);
			$criteria->compare('end_date', $this->end_date, true);
			$criteria->compare('average_sales_price', $this->average_sales_price);
			$criteria->compare('average_commission_percent', $this->average_commission_percent);
			$criteria->compare('commission_split', $this->commission_split);
			$criteria->compare('lead_gen_minutes_per_day', $this->lead_gen_minutes_per_day);
			$criteria->compare('income_per_year', $this->income_per_year);
			$criteria->compare('contracts_closing_conversion', $this->contracts_closing_conversion, true);
			$criteria->compare('agreements_contract_conversion', $this->agreements_contract_conversion, true);
			$criteria->compare('appointments_agreement_conversion', $this->appointments_agreement_conversion, true);
			$criteria->compare('contacts_per_appointment', $this->contacts_per_appointment);
			$criteria->compare('contacts_per_day', $this->contacts_per_day);
			$criteria->compare('referral_percentage', $this->referral_percentage, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
