<?php

	/**
	 * This is the model class for table "component_types".
	 *
	 * The followings are the available columns in table 'component_types':
	 *
	 * @property integer $id
	 * @property string  $name
	 */
	class ComponentTypes extends StmBaseActiveRecord {

		const CONTACTS = 1;
		const BUYERS = 2;
		const SELLERS = 3;
		const LISTINGS = 4;
		const CLOSINGS = 5;
		const ACTION_PLANS = 6;
		const ACTION_PLANS_ITEMS = 7;
		const TASKS = 8;
		const EMAILS = 9;
		const PROJECTS = 10;
		const PROJECT_ITEMS = 11;
		const ASSIGNMENTS = 12;
		const COMPANIES = 13;
		const RECRUITS = 14;
		const APPOINTMENTS = 15;
		const FEATURED_AREAS = 16;
		const SAVED_SEARCHES = 17;
		const REFERRALS = 19;
        const DIRECT_INCOMING_CALLS = 20;
        const RENTALS = 21;
		const IMPORT_NOTES = 22;


		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ComponentTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'component_types';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'name, display_name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, display_name',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array();
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
				'display_name' => 'Type',
			);
		}

		public function scopes() {
			return array(
				'actionPlan' => array(
					'condition' => '(id<:id_max AND id !=:notListing) OR id=:recruits OR id=:referrals OR id=:projects',
					'params' => array(':id_max' => 6,':notListing'=>ComponentTypes::LISTINGS, ':recruits'=>ComponentTypes::RECRUITS, ':referrals'=>ComponentTypes::REFERRALS, ':projects'=>ComponentTypes::PROJECTS),
				),
			);
		}

		public function getContact($componentId)
        {
			switch ($this->id) {
				case ComponentTypes::CONTACTS:
					$Contact = Contacts::model()->findByPk($componentId);
					break;
				case ComponentTypes::CLOSINGS:
					$Contact = Closings::model()->findByPk($componentId)->transaction->contact;
					break;
				case ComponentTypes::BUYERS:
				case ComponentTypes::SELLERS:
				case ComponentTypes::LISTINGS:
					// The component chain stuff is slowing down the system, need to find an alternative solution to this
					$Transaction = Transactions::newInstance($this->name)->findByPk($componentId);
					$Contact = Contacts::model()->findByPk($Transaction->contact_id);
					break;
				case ComponentTypes::TASKS:
					$Task = Tasks::model()->skipSoftDeleteCheck()->findByPk($componentId);
					$Contact = $Task->getContact();
					break;
				case ComponentTypes::ACTION_PLANS_ITEMS:
					$ActionPlanItem = ActionPlanItems::model()->findByPk($componentId);
					$Contact = $ActionPlanItem->getContact();
					break;
                case ComponentTypes::REFERRALS:
                    $Referral = Referrals::model()->findByPk($componentId);
                    $Contact = $Referral->contact;
                    break;
				case ComponentTypes::RECRUITS:
					$Recruit = Recruits::model()->findByPk($componentId);
					$Contact = $Recruit->contact;
					break;

                case ComponentTypes::SAVED_SEARCHES:
                    $SavedSearch = SavedHomeSearches::model()->findByPk($componentId);
                    $Contact = $SavedSearch->contact;
                    break;

                case ComponentTypes::DIRECT_INCOMING_CALLS:
                case ComponentTypes::PROJECTS:
                    break;

                default:
                    //error
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Unknown Component Type ID: '.$this->id.' Component ID: '.$componentId, CLogger::LEVEL_ERROR);
                    break;
			}
			return $Contact;
		}

        public static function getComponentModel($componentTypeId, $componentId)
        {
            $componentModel = null;

            switch ($componentTypeId)
            {
                case ComponentTypes::CONTACTS:
                    $componentModel = Contacts::model()->findByPk($componentId);
                    break;

                case ComponentTypes::CLOSINGS:
                    $componentModel = Closings::model()->findByPk($componentId);
                    break;

                case ComponentTypes::BUYERS:
                case ComponentTypes::SELLERS:
                case ComponentTypes::LISTINGS:
                    // The component chain stuff is slowing down the system, need to find an alternative solution to this
                    $componentModel = Transactions::model()->findByPk($componentId);
                    break;

                case ComponentTypes::TASKS:
                    $componentModel = Tasks::model()->findByPk($componentId);
                    break;

                case ComponentTypes::ACTION_PLANS_ITEMS:
                    $componentModel = ActionPlanItems::model()->findByPk($componentId);
                    break;

                case ComponentTypes::REFERRALS:
                    $componentModel = Referrals::model()->findByPk($componentId);
                    break;

                case ComponentTypes::RECRUITS:
                    $componentModel = Recruits::model()->findByPk($componentId);
                    break;

                case ComponentTypes::PROJECTS:
                    $componentModel = Projects::model()->findByPk($componentId);
                    break;

                case ComponentTypes::SAVED_SEARCHES:
                    $componentModel = SavedHomeSearches::model()->findByPk($componentId);
                    break;

                case ComponentTypes::DIRECT_INCOMING_CALLS:
                    $componentModel = CallHuntGroupSessions::model()->findByPk($componentId);
                    break;

                default:
                    //error
                    Yii::log(__CLASS__ . ' (' . __LINE__ . ') Unknown Component Type ID: '.$componentTypeId.' Component ID: '.$componentId, CLogger::LEVEL_ERROR);
                    break;
            }
            return $componentModel;
        }

		public static function getComponentNameFromID($componentTypeId, $humanReadable = true)
		{
			switch ($componentTypeId)
			{
				case ComponentTypes::CONTACTS:
					return "Contacts";
					break;

				case ComponentTypes::CLOSINGS:
					return "Closings";
					break;

				case ComponentTypes::BUYERS:
					return "Buyers";
					break;
				case ComponentTypes::SELLERS:
					return "Sellers";
					break;
				case ComponentTypes::LISTINGS:
					return "Listings";
					break;

				case ComponentTypes::TASKS:
					return "Tasks";
					break;

				case ComponentTypes::ACTION_PLANS_ITEMS:
					return $humanReadable ? "Action Plan Items" : "Action-Plan-Items";
					break;

				case ComponentTypes::REFERRALS:
					return "Referrals";
					break;

				case ComponentTypes::RECRUITS:
					return "Recruits";
					break;

				case ComponentTypes::PROJECTS:
					return "Projects";
					break;

				case ComponentTypes::SAVED_SEARCHES:
					return "Saved Searches";
					break;

				case ComponentTypes::DIRECT_INCOMING_CALLS:
					return $humanReadable ? "Direct Incoming Calls" : "Direct-Incoming-Calls";
					break;
				case ComponentTypes::IMPORT_NOTES:
					return "Notes";
					break;

				default:
					return $humanReadable ? "Not Found" : "not-Found";
					break;
			}
			return $componentModel;
		}

		/**
		 * getByName
		 *
		 * @param  string $name
		 *
		 * @return mixed  ComponentTypes model
		 */
		public static function getByName($name) {
			return ComponentTypes::model()->find(array(
					'condition' => 'name=:name',
					'params' => array(':name' => $name)
				)
			);
		}

        /**
         * getByName
         *
         * @param  string $name
         *
         * @return mixed  ComponentTypes model
         */
        public static function getNameById($id) {
            $componentType = ComponentTypes::model()->find(array(
                    'condition' => 'id=:id',
                    'params' => array(':id' => $id)
                )
            );
            return ($componentType) ? $componentType->name : null;
        }

        public function byIds($ids) {
            $ids = (array) $ids;
            $criteria = new CDbCriteria;
            $criteria->addInCondition('id', $ids);
            $this->getDbCriteria()->mergeWith($criteria);

            return $this;
        }

        /**
		 * Returns the actual component name by the component type id.  If the component type is related to a transaction then return "transaction"
		 * @return null|string
		 */
		public function getComponentName() {
			if (!$this->id) {
				return null;
			}

			$componentName = null;

			$transactionComponentTypes = array(self::BUYERS, self::SELLERS);
			if (in_array($this->id, $transactionComponentTypes)) {
				$componentName = 'transaction';
			} else {
				$componentName = $this->name;
			}

			return $componentName;
		}

        /**
         * Get Singular Name
         *
         * @return null|string
         */
        public function getSingularName()
        {
			if (!$this->id) {
				return null;
			}

            $singularName = Yii::app()->format->formatSingular($this->name);

			return ucwords($singularName);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('display_name', $this->display_name, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
