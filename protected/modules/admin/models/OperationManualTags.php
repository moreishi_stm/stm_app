<?php

/**
 * This is the model class for table "operation_manual_tags".
 *
 * The followings are the available columns in table 'operation_manual_tags':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property OperationManuals[] $operationManuals
 */
class OperationManualTags extends StmBaseActiveRecord
{
	public $tagPermissionCollection;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OperationManualTags the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'operation_manual_tags';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, tagPermissionCollection', 'required'),
			array('parent_id','numerical', 'integerOnly' => true),
			array('tagPermissionCollection', 'safe'),
			array('name', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::HAS_ONE, 'OperationManualTags', 'parent_id'),
			'operationManuals' => array(self::MANY_MANY, 'OperationManuals', 'operation_manual_tag_lu(operation_manual_tag_id, operation_manual_id)'),
			'operationManualTagLu' => array(self::HAS_MANY, 'OperationManualTagLu', 'operation_manual_tag_id'),
			'operationManualTagPermissionLu'=> array(self::HAS_MANY, 'OperationManualTagPermissionLu', 'operation_manual_tag_id'),
			'operationManualTagPermissions' => array(self::MANY_MANY, 'AuthItem', 'operation_manual_tag_permission_lu(operation_manual_tag_id, auth_item_name)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'parent_id' => 'Subcategory of',
			'tagPermissionCollection' => 'Permission',
		);
	}

	public function exclude($ids) {
		if (!is_array($ids)) {
			$ids = array($ids);
		}

		$this->getDbCriteria()->addNotInCondition('id', $ids);

		return $this;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_id',$this->parent_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}