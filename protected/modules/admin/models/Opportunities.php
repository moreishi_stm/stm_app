<?php

/**
 * This is the model class for table "opportunities".
 *
 * The followings are the available columns in table 'opportunities':
 * @property string $id
 * @property integer $component_type_id
 * @property integer $component_id
 * @property string $target_date
 * @property string $next_followup_date
 * @property string $dig_motivation_rating
 * @property integer $activity_log_id
 * @property integer $task_id
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property ComponentTypes $componentType
 * @property Contacts $addedBy
 */
class Opportunities extends StmBaseActiveRecord
{
    public $fromDate;
    public $toDate;
    public $toDelete;
    public $activityLogNote;
    public $followUpTaskDescription;
    public $followUpTaskAssignedToId;
    public $hasAppointment;
    public $appointmentId;
    public $sortOrder;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Opportunities the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'opportunities';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('activityLogNote, dig_motivation_rating, followUpTaskDescription', 'required', 'on'=>'insert'),
            array('component_type_id, component_id, target_date, next_followup_date, dig_motivation_rating, added_by', 'required'),
            array('component_type_id, component_id, dig_motivation_rating, activity_log_id, task_id, added_by, is_deleted, followUpTaskAssignedToId', 'numerical', 'integerOnly'=>true),
            array('target_date, next_followup_date, added, fromDate, toDate, toDelete, activityLogNote, followUpTaskDescription, followUpTaskAssignedToId, hasAppointment, sortOrder', 'safe'),
            array('target_date','validateTargetDate'),
            array('next_followup_date','validateNextFollowupDate'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, component_type_id, component_id, activity_log_id, task_id, target_date, dig_motivation_rating, next_followup_date, added_by, added, is_deleted, fromDate, toDate, toDelete, activityLogNote, followUpTaskDescription, followUpTaskAssignedToId, hasAppointment, sortOrder', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'task' => array(self::BELONGS_TO, 'Tasks', 'task_id'),
            'componentType' => array(self::BELONGS_TO, 'ComponentTypes', 'component_type_id'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'activityLog' => array(self::BELONGS_TO, 'ActivityLog', 'activity_log_id'),
            'transaction' => array(self::BELONGS_TO, 'Transactions', 'component_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'component_type_id' => 'Component Type',
            'component_id' => 'Component',
            'target_date' => 'Target Date',
            'next_followup_date' => 'Next Followup Date',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    public function getIsEmpty()
    {
        $fields = array('target_date','next_followup_date','activityLogNote','followUpTaskDescription','followUpTaskAssignedToId');
        foreach($fields as $field) {
            if(!empty($this->$field)) {
                return false;
            }
        }
        return true;
    }

    protected function beforeValidate()
    {
        if($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        return parent::beforeValidate();
    }

    public function validateTargetDate($attribute, $params)
    {
        if(Yii::app()->format->formatDate($this->target_date,'Y-m-d') < Yii::app()->format->formatDate($this->added, 'Y-m-d')) {
            $this->addError($attribute, 'Target date cannot be before today.');
        }
    }

    public function validateNextFollowupDate($attribute, $params)
    {
        if(Yii::app()->format->formatDate($this->next_followup_date, 'Y-m-d') > date('Y-m-d', strtotime("+1 month"))) {
            $this->addError($attribute, 'Next Follow-up must be less than 1 month. Recommend 1 week for Hot "A", and 2 weeks for "B" Leads.');
        }
    }

    protected function beforeSave()
    {
        $this->target_date = Yii::app()->format->formatDate($this->target_date, StmFormatter::MYSQL_DATE_FORMAT);
        $this->next_followup_date = Yii::app()->format->formatDate($this->next_followup_date, StmFormatter::MYSQL_DATE_FORMAT);

        // save activity log note activityLogNote value exists
        if($this->activityLogNote) {

            $activityLog = new ActivityLog();
            $activityLog->setAttributes(array(
                    'component_type_id' => $this->component_type_id,
                    'component_id' => $this->component_id,
                    'task_type_id' => TaskTypes::OPPORTUNITY,
                    'activity_date' => $this->added,
                    'is_spoke_to' => 1,
                    'added_by' => Yii::app()->user->id,
                    'note' => $this->activityLogNote,
                ));
            if(!$activityLog->save()) {
                Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: Auto Activity Log from New Opportunity did not save. Error Message '.print_r($activityLog->getErrors(), true), CLogger::LEVEL_ERROR);
            }
            else {
                $this->activity_log_id = $activityLog->id;
            }
        }

        if($this->followUpTaskDescription) {

            $task = new Tasks();
            $task->setAttributes(array(
                    'is_priority' => Tasks::PRIORITY_EOD,
                    'component_type_id' => $this->component_type_id,
                    'component_id' => $this->component_id,
                    'task_type_id' => TaskTypes::PHONE,
                    'assigned_to_id' => $this->followUpTaskAssignedToId,
                    'due_date' => $this->next_followup_date,
                    'description' => 'New Opportunity Follow-up: '.$this->followUpTaskDescription,
                ));
            if(!$task->save()) {
                Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: Auto Task from New Opportunity did not save. Error Message '.print_r($task->getErrors(), true), CLogger::LEVEL_ERROR);
            }
            else {
                $this->task_id = $task->id;
            }
        }
        return parent::beforeSave();
    }

    protected function afterSave()
    {
       // calculate the days difference from now to target date.
        $todayDate = new DateTime(date('Y-m-d'));
        $targetDate = new DateTime($this->target_date);
        $dateDiff = $targetDate->diff($todayDate);
        $daysDiff = $dateDiff->days;

        //@todo: make status to update based on timeframe???? - It's automatic for now
        // Change status of lead for Sellers/Buyers only for now
        if(in_array($this->component_type_id, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS))) {
            // change status of leads based on target date
            if($daysDiff <= 30) {
                // this is a Hot A Lead
                $leadStatus = TransactionStatus::HOT_A_LEAD_ID;
                $leadStatusName = 'Hot A';

            } elseif($daysDiff <= 60) {
                // this is a B Lead
                $leadStatus = TransactionStatus::B_LEAD_ID;
                $leadStatusName = 'B - 60 days';

            } elseif($daysDiff <= 90) {
                // this is a C Lead
                $leadStatus = TransactionStatus::C_LEAD_ID;
                $leadStatusName = 'C - 90 days';

            } elseif($daysDiff > 90) {
                // this is a D Lead
                $leadStatus = TransactionStatus::D_NURTURING_SPOKE_TO_ID;
                $leadStatusName = 'D - Nurture Spoke To';
            } else {
                Yii::log(__CLASS__.' (:'.__FILE__.') Unexpected Result: Lead Status cannot be determined for this new Opportunity. Attributes: '.print_r($this->attributes, true), CLogger::LEVEL_ERROR);
            }

            $tnxModel = Transactions::model()->findByPk($this->component_id);
            if($leadStatus && $tnxModel->transaction_status_id != $leadStatus) {
                $tnxModel->transaction_status_id = $leadStatus;
                if(!$tnxModel->save()) {
                    Yii::log(__CLASS__.' (:'.__FILE__.') Transaction status could not be updated. Error Message: '.print_r($tnxModel->getErrors(), true), CLogger::LEVEL_ERROR);
                }
            }
        }

        if(Yii::app()->user->settings->new_opportunity_alert == 1 && Yii::app()->user->settings->new_opportunity_alert !== '0' && Yii::app()->user->settings->new_opportunity_alert_email !== '') {
            $this->_sendEmailAlert($leadStatusName);
        }

        parent::afterSave();
    }

    public function getTransactionStatusByDate()
    {
        $addedDate = new DateTime($this->added);
        $targetDate = new DateTime($this->target_date);
        $dateDiff = $targetDate->diff($addedDate);
        $daysDiff = $dateDiff->days;

        if($daysDiff <= 30) {
            // this is a Hot A Lead
            $leadStatus = TransactionStatus::HOT_A_LEAD_ID;

        } elseif($daysDiff <= 60) {
            // this is a B Lead
            $leadStatus = TransactionStatus::B_LEAD_ID;

        } elseif($daysDiff <= 90) {
            // this is a C Lead
            $leadStatus = TransactionStatus::C_LEAD_ID;

        } elseif($daysDiff > 90) {
            // this is a D Lead
            $leadStatus = TransactionStatus::D_NURTURING_SPOKE_TO_ID;
        } else {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Unexpected Result: Lead Status cannot be determined for this Opportunity.');
        }

        return $leadStatus;
    }

    protected function _sendEmailAlert($leadStatusName)
    {
        $emailViewPath = YiiBase::getPathOfAlias('admin_module.views.emails.opportunity').'.php';

        $phrases = array(
            'Keep up the awesomeness!!',
            'You rock! Wohooooo!',
            'Rock on!!',
            'Oh yeahhh!!!',
            'Go get \'em!!!',
            'Heck yeaaahhhh!!',
            'Sweeeeet!!!',
            'Cool beans!!!',
            'Fantasticoooo!!! :D',
            'Superrificc!!! =)',
        );
        $randomKey = array_rand($phrases);

        $photos = array('goldpot.jpg','hook.jpg','moneyplant.jpg','rain.jpg','squirrel.jpg');
        $photoKey = array_rand($photos);

        $content = Yii::app()->controller->renderFile($emailViewPath, array(
                'model' => $this,
                'photo' => $photos[$photoKey],
                'phrase' => $phrases[$randomKey],
                'componentSingularName' => $this->componentType->getSingularName(),
                'leadStatusName' => $leadStatusName,
                'domainName' => Yii::app()->user->primaryDomain->name,
            ), $returnContent=true);

        $subject = 'Congrats! ' . $this->addedBy->fullName . ' got a New '.$this->componentType->getSingularName().' Opportunity!';

        $to = (YII_DEBUG) ? array(StmMailMessage::DEBUG_EMAIL_ADDRESS => 'Team Congrats') : array(Yii::app()->user->settings->new_opportunity_alert_email => 'Team Congrats');
        $from = array(Yii::app()->user->settings->new_opportunity_alert_email => 'Team Congrats');
        StmZendMail::easyMail($from, $to, $subject, $content, null, $type='text/html', $awsTransport=true, $ignoreOnDebug = false, Yii::app()->user->settings->new_opportunity_alert_email);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->select = 't.*, appointments.id as appointmentId';
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.component_type_id',$this->component_type_id);
        $criteria->compare('t.component_id',$this->component_id);
        $criteria->compare('t.target_date',$this->target_date,true);
        $criteria->compare('t.next_followup_date',$this->next_followup_date,true);
        $criteria->compare('t.added_by',$this->added_by);
        $criteria->compare('t.added',$this->added,true);

        $sortField = 'target_date';
        switch($this->sortOrder) {

            case 'addedDateAsc':
                $criteria->order = 't.added ASC';
                $sortField = 'added';
                break;

            case 'addedDateDesc':
                $criteria->order = 't.added DESC';
                $sortField = 'added';
                break;

            case 'targetDateAsc':
                $criteria->order = 't.target_date ASC';
                $sortField = 'target_date';
                break;

            default:
                $criteria->order = 't.target_date ASC';
                $sortField = 'target_date';
                break;
        }

        if($this->fromDate) {
            $criteria->addCondition('DATE(t.'.$sortField.') >= :fromDate');
            $criteria->params[':fromDate'] = $this->fromDate;
        }
        if($this->toDate) {
            $criteria->addCondition('DATE(t.'.$sortField.') <= :toDate');
            $criteria->params[':toDate'] = $this->toDate;
        }
        $criteria->compare('t.is_deleted',$this->is_deleted);

        $criteria->together = true;
        $criteria->join = 'LEFT JOIN appointments ON t.component_type_id=appointments.component_type_id AND t.component_id=appointments.component_id AND appointments.is_deleted=0';

        if($this->hasAppointment) {
            $criteria->addCondition('appointments.id IS NOT NULL');
        }
        if($this->hasAppointment === "0") {
            $criteria->addCondition('appointments.id IS NULL');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}