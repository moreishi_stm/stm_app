<?php

	/**
	 * This is the model class for table "transaction_address_lu".
	 *
	 * The followings are the available columns in table 'transaction_address_lu':
	 *
	 * @property integer          $id
	 * @property integer          $transaction_id
	 * @property integer          $address_id
	 * @property integer          $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Transactions     $transaction
	 * @property AddressContactLu $addressContactLu
	 */
	class TransactionAddressLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TransactionAddressLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'transaction_address_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'transaction_id, address_id',
					'required'
				),
				array(
					'transaction_id, address_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, transaction_id, address_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'transaction' => array(
					self::BELONGS_TO,
					'Transactions',
					'transaction_id'
				),
				'address' => array(
					self::BELONGS_TO,
					'Addresses',
					'address_id'
				),
			);
		}

		/**
		 * Used to get the address properties from this model
		 *
		 * @param  string $name
		 *
		 * @return mixed
		 */
		public function __get($name) {
			if ($this->address && isset($this->address->$name)) {
				return $this->address->$name;
			}

			return parent::__get($name);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'transaction_id' => 'Transaction',
				'address_id' => 'Address Contact Lu',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('transaction_id', $this->transaction_id);
			$criteria->compare('address_id', $this->address_id);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}