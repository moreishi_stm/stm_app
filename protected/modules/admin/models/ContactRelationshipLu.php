<?php

/**
 * This is the model class for table "contact_relationship_lu".
 *
 * The followings are the available columns in table 'contact_relationship_lu':
 * @property integer $id
 * @property integer $origin_relationship_type_id
 * @property integer $origin_contact_id
 * @property integer $related_to_contact_id
 * @property integer $is_referral
 * @property integer $is_past_referral
 * @property string $notes
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */
class ContactRelationshipLu extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ContactRelationshipLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contact_relationship_lu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('origin_relationship_type_id, origin_contact_id, related_to_contact_id, related_to_relationship_type_id, updated_by', 'required'),
            array('origin_relationship_type_id, origin_contact_id, related_to_contact_id, related_to_relationship_type_id, is_referral, is_past_referral, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('related_to_contact_id', 'validateUniqueCombo', 'on'=>'insert'),
            array('notes', 'length', 'max'=>255),
            array('updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, origin_relationship_type_id, origin_contact_id, related_to_contact_id, related_to_relationship_type_id, is_referral, is_past_referral, notes, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'originContact' => array(self::BELONGS_TO, 'Contacts', 'origin_contact_id'),
            'relatedContact' => array(self::BELONGS_TO, 'Contacts', 'related_to_contact_id'),
            'originRelationshipType' => array(self::BELONGS_TO, 'ContactRelationshipTypes', 'origin_relationship_type_id'),
            'relatedRelationshipType' => array(self::BELONGS_TO, 'ContactRelationshipTypes', 'related_to_relationship_type_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'origin_relationship_type_id' => 'Relationship',
            'origin_contact_id' => 'Contact Origin',
            'related_to_contact_id' => 'Related Contact',
            'related_to_relationship_type_id' => 'Reverse Relationship',
            'is_referral' => 'Is Referral',
            'is_past_referral' => 'Referred Before Today',
            'notes' => 'Notes',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    public function relationshipExists($contactId1, $contactId2) {

        $this->getDbCriteria()->mergeWith(array(
                                          'condition' => '(origin_contact_id=:origin_contact_id1 AND related_to_contact_id=:related_to_contact_id1) OR (origin_contact_id=:origin_contact_id2 AND related_to_contact_id=:related_to_contact_id2)',
                                          'params' => array(':origin_contact_id1'=> $contactId1, ':related_to_contact_id1'=> $contactId2, ':origin_contact_id2'=> $contactId2, ':related_to_contact_id2'=> $contactId1),
                                          )
        );

        return $this;
    }

    public function byEitherContactId($contactId) {

        $this->getDbCriteria()->mergeWith(array(
                                          'condition' => 'origin_contact_id=:origin_contact_id OR related_to_contact_id=:related_to_contact_id',
                                          'params' => array(':origin_contact_id'=> $contactId, ':related_to_contact_id'=> $contactId),
                                          )
        );

        return $this;
    }

    protected function beforeValidate() {

        if ($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }
        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    public function validateUniqueCombo($attribute, $params)
    {
        if ($this->relationshipExists($this->origin_contact_id, $this->related_to_contact_id)->count()) {
            $this->addError($attribute, 'Relationship already exists with this contact. Please update the existing one.');
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('origin_relationship_type_id',$this->origin_relationship_type_id);
        $criteria->compare('origin_contact_id',$this->origin_contact_id);
        $criteria->compare('related_to_contact_id',$this->related_to_contact_id);
        $criteria->compare('related_to_relationship_type_id',$this->origin_relationship_type_id);
        $criteria->compare('is_referral',$this->is_referral);
        $criteria->compare('is_past_referral',$this->is_past_referral);
        $criteria->compare('notes',$this->notes,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}