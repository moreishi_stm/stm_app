<?php

	/**
	 * This is the model class for table "document_permissions".
	 *
	 * The followings are the available columns in table 'document_permissions':
	 *
	 * @property integer        $account_id
	 * @property integer        $document_type_id
	 * @property integer        $component_type_id
	 * @property integer        $component_id
	 * @property integer        $permission_level_ma
	 *
	 * The followings are the available model relations:
	 * @property Documents      $account
	 * @property Documents      $documentType
	 * @property ComponentTypes $componentType
	 */
	class DocumentPermissions extends StmBaseActiveRecord {

		const PERMISSION_LEVEL_COMPONENT = 1;
		const PERMISSION_LEVEL_OWNER = 2;
		const PERMISSION_LEVEL_EDIT = 3;
		const PERMISSION_LEVEL_READ_ONLY = 4;
		const PERMISSION_LEVEL_DENY = 5;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return DocumentPermissions the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'document_permissions';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, document_id, component_type_id, component_id, permission_level_ma',
					'required'
				),
				array(
					'account_id, document_id, component_type_id, component_id, permission_level_ma',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, document_id, component_type_id, component_id, permission_level_ma',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				//			'account' => array(self::BELONGS_TO, 'Documents', 'account_id'),
				'documentType' => array(
					self::BELONGS_TO,
					'Documents',
					'document_id'
				),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'document_id' => 'Document Type',
				'component_type_id' => 'Component Type',
				'component_id' => 'Component Id',
				'permission_level_ma' => 'Permission Level',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('document_id', $this->document_id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('component_id', $this->component_id);
			$criteria->compare('permission_level_ma', $this->permission_level_ma);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}