<?php

	/**
	 * This is the model class for table "transaction_field_values".
	 *
	 * The followings are the available columns in table 'transaction_field_values':
	 *
	 * @property integer           $id
	 * @property integer           $transaction_field_id
	 * @property integer           $transaction_id
	 * @property string            $value
	 * @property integer           $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property TransactionFields $transactionField
	 * @property Transactions      $transaction
	 */
	class TransactionFieldValues extends StmBaseActiveRecord {

		public $data = array();
        protected $_originalData;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TransactionFieldValues the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'transaction_field_values';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'value, data',
					'safe'
				),
                array(
                    'transaction_field_id',
                    'validateUniqueFieldIdValue'
                ),
				array(
					'transaction_field_id, transaction_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'value',
					'length',
					'max' => 65535
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, transaction_field_id, transaction_id, value, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'transactionField' => array(
					self::BELONGS_TO,
					'TransactionFields',
					'transaction_field_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'transaction_field_id' => 'Transaction Field',
				'transaction_id' => 'Transaction',
				'value' => 'Value',
				'is_deleted' => 'Is Deleted',
			);
		}

		public function scopes() {
			return array(
				'asc'=>array(
					'order' => 'value asc',
				),
				'desc'=>array(
					'order' => 'value desc',
				),
			);
		}

		/**
		 * Get the field name related to this value
		 *
		 * @return mixed field name
		 */
		public function getFieldName() {
			if ($this->transactionField->name) {
				return $this->transactionField->name;
			}

			return null;
		}

		public function byFieldId($fieldId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'transaction_field_id=:transaction_field_id';
			$Criteria->params = array('transaction_field_id' => $fieldId);
			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function dateRange($opts) {

			if ($opts['to_date']) {
				$this->getDbCriteria()->mergeWith(array(
						'condition' => 'IF(LOCATE("-", value) > 0, value, STR_TO_DATE(value,"%m/%d/%Y"))   <= :to_date',
						'params' => array(':to_date' => $opts['to_date'])
					)
				);
			}

			if ($opts['from_date']) {
				$this->getDbCriteria()->mergeWith(array(
                        'condition' => 'IF(LOCATE("-", value) > 0, value, STR_TO_DATE(value,"%m/%d/%Y"))   >= :from_date',
						'params' => array(':from_date' => $opts['from_date']),
					)
				);
			}

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('transaction_field_id', $this->transaction_field_id);
			$criteria->compare('transaction_id', $this->transaction_id);
			$criteria->compare('value', $this->value, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		protected function beforeSave()
        {
            // only do this for elastic action task field reference dates
            if($this->transaction_field_id == TransactionFields::SELLER_LISTING_DATE) {
                $this->_originalData = $this->findByPk($this->id)->attributes;
            }

			// If the value is a number based field strip commas or decimals from it.
			if ($this->transactionField->isNumberField) {
				// $this->value = str_replace(array('.',',','$'), '', $this->value);
				$this->value = Yii::app()->format->formatInteger($this->value);
			}

			if ($this->transactionField->isDateField) {
				$this->value = Yii::app()->format->formatDate($this->value, StmFormatter::MYSQL_DATE_FORMAT);
                if(strpos($this->value, '1969-12-31') !== false || strpos($this->value, '12/31/1969') !== false) {
                    Yii::log(__CLASS__ . '(' . __LINE__ . ') TransactionFieldValues date value is formatting improperly. Review post data. TransactionFieldValues Attributes: '.print_r($this->attributes, true), CLogger::LEVEL_ERROR);
                    return false;
                }
			}

			if ($this->transactionField->isDateTimeField) {
                $this->value = Yii::app()->format->formatDate($this->value, StmFormatter::MYSQL_DATETIME_FORMAT);
                if(strpos($this->value, '1969-12-31') !== false || strpos($this->value, '12/31/1969') !== false) {
                    Yii::log(__CLASS__ . '(' . __LINE__ . ') TransactionFieldValues date value is formatting improperly. Review post data. TransactionFieldValues Attributes: '.print_r($this->attributes, true), CLogger::LEVEL_ERROR);
                    return false;
                }
			}

			return parent::beforeSave();
		}

		protected function afterFind() {
			// Format anything that needs to be changed for the gui after a record is found
			if ($this->transactionField->isDateField) {
				$this->value = Yii::app()->format->formatDate($this->value, 'm/d/Y');
			}

			if ($this->transactionField->isDateTimeField) {
				$this->value = str_replace(' 00:',' 12:',Yii::app()->format->formatDate($this->value, 'm/d/Y h:i a'));

                if(strpos($this->value, '1969-12-31') !== false || strpos($this->value, '12/31/1969') !== false) {
                    $this->value = '';
                }
            }

            parent::afterFind();
		}
        protected function afterSave()
        {
            if(in_array($this->transaction_field_id, array(TransactionFields::SELLER_LISTING_DATE,TransactionFields::SELLER_LISTING_EXPIRED_DATE))) {

                $component = Transactions::model()->findByPk($this->transaction_id);

                switch($this->transaction_field_id) {
                    case TransactionFields::SELLER_LISTING_DATE:
                        $actionPlanReferenceId = ActionPlanItems::DUE_REFERENCE_LISTING_DATE_ID;
                        break;

                    case TransactionFields::SELLER_LISTING_EXPIRED_DATE:
                        $actionPlanReferenceId = ActionPlanItems::DUE_REFERENCE_LISTING_EXPIRE_ID;
                        break;
                }

                Tasks::updateElasticActionTaskDueDate($component, $actionPlanReferenceId);
            }

            parent::afterSave();
        }

        /**
         * Validate the full name for the contact.
         *
         * @param  string $attribute The attribute name
         * @param  array  $params    Parameters for the validation
         *
         * @return null
         */
        public function validateUniqueFieldIdValue($attribute, $params)
        {
            if($this->isNewRecord) {

                if($this->findByAttributes(array('transaction_field_id' => $this->transaction_field_id, 'transaction_id' => $this->transaction_id))) {
                    $this->addError('transaction_field_id', 'Duplicate record. This field and for the transaction already exists.');
                }
            }
        }

        public function getAllTransactionFieldValuesForContact($contactId)
        {
            // Create command
            $command = Yii::app()->db->createCommand()
                ->select('tfv.id')
                ->from('transaction_field_values tfv')
                ->leftJoin('transactions tr', 'tfv.transaction_id = tr.id')
                ->where('contact_id = :contactId', array(':contactId' => $contactId))
                ->andWhere('in', 'tfv.transaction_field_id', array(TransactionFields::PRICE_MAX_BUYER));

            return $command->queryAll();
        }
    }