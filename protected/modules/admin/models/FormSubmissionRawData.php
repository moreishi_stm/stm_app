<?php

	/**
	 * This is the model class for table "form_submission_raw_data".
	 *
	 * The followings are the available columns in table 'form_submission_raw_data':
	 *
	 * @property integer $id
	 * @property integer $is_spam
	 * @property string  $first_name__1
	 * @property string  $last_name__2
	 * @property string  $email__3
	 * @property string  $phone__4
	 * @property string  $contact_date__5
	 * @property string  $contact_time__6
	 * @property string  $comments__7
	 * @property string  $question__8
	 * @property string  $to_first_name__9
	 * @property string  $to_last_name__10
	 * @property string  $to_email__11
	 * @property string  $form_title__12
	 * @property string  $password__13
	 * @property string  $address__14
	 * @property string  $unit_num__15
	 * @property string  $city__16
	 * @property string  $state__17
	 * @property string  $zip__18
	 * @property string  $reason__20
	 * @property string  $misc
	 * @property string  $url
	 * @property string  $ip
	 * @property string  $added
	 */
	class FormSubmissionRawData extends StmBaseActiveRecord {

        public $count;
        public $dateRange;
		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FormSubmissionRawData the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'form_submission_raw_data';
		}

        public function setDbConnection($newDbConnection) {
            self::$db = $newDbConnection;
        }

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'url, ip, added',
					'required'
				),
				array(
					'is_spam, count',
					'numerical',
					'integerOnly' => true
				),
//				array(
//					'first_name__1, last_name__2, email__3, phone__4, contact_date__5, contact_time__6, comments__7, question__8, to_first_name__9, to_last_name__10, to_email__11, form_title__12, password__13, address__14, unit_num__15, city__16, state__17, zip__18, reason__20, misc, url',
//					'length',
//					'max' => 255
//				),
//				array(
//					'ip',
//					'length',
//					'max' => 35
//				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, is_spam, first_name__1, last_name__2, email__3, phone__4, contact_date__5, contact_time__6, comments__7, question__8, to_first_name__9, to_last_name__10, to_email__11, form_title__12, password__13, address__14, unit_num__15, city__16, state__17, zip__18, reason__20, misc, url, ip, added, count, dateRange',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array();
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'is_spam' => 'Is Spam',
				'first_name__1' => 'First Name 1',
				'last_name__2' => 'Last Name 2',
				'email__3' => 'Email 3',
				'phone__4' => 'Phone 4',
				'contact_date__5' => 'Contact Date 5',
				'contact_time__6' => 'Contact Time 6',
				'comments__7' => 'Comments 7',
				'question__8' => 'Question 8',
				'to_first_name__9' => 'To First Name 9',
				'to_last_name__10' => 'To Last Name 10',
				'to_email__11' => 'To Email 11',
				'form_title__12' => 'Form Title 12',
				'password__13' => 'Password 13',
				'address__14' => 'Address 14',
				'unit_num__15' => 'Unit Num 15',
				'city__16' => 'City 16',
				'state__17' => 'State 17',
				'zip__18' => 'Zip 18',
				'reason__20' => 'Reason 20',
				'misc' => 'Misc',
				'url' => 'Url',
				'ip' => 'Ip',
				'added' => 'Added',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('is_spam', $this->is_spam);
			$criteria->compare('first_name__1', $this->first_name__1, true);
			$criteria->compare('last_name__2', $this->last_name__2, true);
			$criteria->compare('email__3', $this->email__3, true);
			$criteria->compare('phone__4', $this->phone__4, true);
			$criteria->compare('contact_date__5', $this->contact_date__5, true);
			$criteria->compare('contact_time__6', $this->contact_time__6, true);
			$criteria->compare('comments__7', $this->comments__7, true);
			$criteria->compare('question__8', $this->question__8, true);
			$criteria->compare('to_first_name__9', $this->to_first_name__9, true);
			$criteria->compare('to_last_name__10', $this->to_last_name__10, true);
			$criteria->compare('to_email__11', $this->to_email__11, true);
			$criteria->compare('form_title__12', $this->form_title__12, true);
			$criteria->compare('password__13', $this->password__13, true);
			$criteria->compare('address__14', $this->address__14, true);
			$criteria->compare('unit_num__15', $this->unit_num__15, true);
			$criteria->compare('city__16', $this->city__16, true);
			$criteria->compare('state__17', $this->state__17, true);
			$criteria->compare('zip__18', $this->zip__18, true);
			$criteria->compare('reason__20', $this->reason__20, true);
			$criteria->compare('misc', $this->misc, true);
			$criteria->compare('url', $this->url, true);
			$criteria->compare('ip', $this->ip, true);
			$criteria->compare('added', $this->added, true);

            if($this->dateRange) {
                $toDateCriteria = new CDbCriteria;
                if($this->dateRange['from_date']) {
                    $criteria->addCondition('DATE(added) >="'.$this->dateRange['from_date'].'"');
                }
                if($this->dateRange['to_date']) {
                    $criteria->addCondition('DATE(added) <="'.$this->dateRange['to_date'].'"');
                }
            }

            $criteria->select = 'count(*) as count, MAX(added) as added, ip';
            $criteria->group = 'ip';

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=> 100,
                ),
			));
		}
	}