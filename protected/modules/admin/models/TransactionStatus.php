<?php

	/**
	 * This is the model class for table "transaction_status".
	 *
	 * The followings are the available columns in table 'transaction_status':
	 *
	 * @property integer                      $id
	 * @property integer                      $transaction_type_id
	 * @property string                       $name
	 * @property integer                      $is_core
	 * @property integer                      $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property TransactionTypes             $transactionType
	 * @property TransactionStatusAccountLu[] $transactionStatusAccountLus
	 * @property Transactions[]               $transactions
	 */
	class TransactionStatus extends StmBaseActiveRecord {

		const NEW_LEAD_ID = 1; //@todo: to be deleted later after refactor
		const LEAD_POOL_ID = 1;
		const HOT_A_LEAD_ID = 2;
		const B_LEAD_ID = 3;
		const C_LEAD_ID = 4;
		const NURTURING_ID = 5; //this is E - Nurturing Not Spoke to
		const D_NURTURING_SPOKE_TO_ID = 6;
		const COMING_SOON_ID = 7;
		const ACTIVE_LISTING_ID = 8;
		const UNDER_CONTRACT_ID = 9;
		const CLOSED_ID = 10;
		const EXPIRED_ID = 11;
		const CANCELLED_ID = 12;
		const TRASH_ID = 13;
        const REFERRAL_OUTBOUND_ID = 14;
        const WITH_ANOTHER_AGENT = 15;
        const ALLIED_RESOURCE = 16;
        const E_NOT_YET_INTERESTED = 17;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TransactionStatus the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'transaction_status';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'transaction_type_id, is_core, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, transaction_type_id, name, is_core, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				// 'transactionType'             => array(self::BELONGS_TO, 'TransactionTypes', 'transaction_type_id'),
				'transactionStatusAccountLu' => array(
					self::HAS_MANY,
					'TransactionStatusAccountLu',
					'transaction_status_id'
				),
				// 'transactions'                => array(self::HAS_MANY, 'Transactions', 'transaction_status_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'transaction_type_id' => 'Transaction Type',
				'name' => 'Name',
				'is_core' => 'Is Core',
				'is_deleted' => 'Is Deleted',
			);
		}

		public function scopes() {

			return array(
				'byNurtureType' => array(
					'condition' => 'id <= ' . self::D_NURTURING_SPOKE_TO_ID . ' || id=' . self::ACTIVE_LISTING_ID.' || id=' . self::TRASH_ID,
				),
                'defaultSort' => array(
                    'order' => 'sort_order ASC, id',
                ),
			);
		}

		public function byComponentType($componentTypeId, $together=false)
        {
            $componentTypeId = (array) $componentTypeId;
			$Criteria = new CDbCriteria;

            $Criteria->addInCondition('transactionStatusAccountLu.component_type_id', $componentTypeId);
			$Criteria->with = 'transactionStatusAccountLu';

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public static function getTransactionStatus($transactionTypeId)
        {
			$Criteria = new CDbCriteria;
			// $Criteria->select = 'TransactionStatus.id, TransactionStatus.name';
			$Criteria->condition = 'transactionStatus.transaction_type_id=:transaction_type_id'; //should be limited by accountId scope
			$Criteria->params = array(':transaction_type_id' => $transactionTypeId);

			return TransactionStatusAccountLu::model()->with('transactionStatus')->findAll($Criteria);
		}

        public static function getTransactionStatusNameById($transactionTypeId) {
            /*$api_cache_id = 'TransactionStatusIdName';
            $cache = Yii::app()->fileCache->get($api_cache_id);
            if(empty($cache)) {
                //@todo: later need to implement the account lu to allow for custom display names overrides, not needed right now
                Yii::app()->fileCache->set($api_cache_id , CHtml::listData(TransactionStatus::model()->findAll(), 'id', 'name'), 60*60*12);
                $cache = Yii::app()->fileCache->get($api_cache_id);
            }
            return $cache[$transactionTypeId];*/

            return CHtml::listData(TransactionStatus::model()->findAll(), 'id', 'name');
        }

        public static function getNurturingStatusList($componentTypeId) {
            $Criteria = new CDbCriteria;
            // $Criteria->select = 'TransactionStatus.id, TransactionStatus.name';
            $Criteria->condition = 'component_type_id=:component_type_id AND transaction_status_id <='.TransactionStatus::D_NURTURING_SPOKE_TO_ID; //should be limited by accountId scope
            $Criteria->params = array(':component_type_id' => $componentTypeId);
            $Criteria->order = 'sort_order';

            return TransactionStatusAccountLu::model()->with('transactionStatus')->findAll($Criteria);
        }

        public function getDisplayName()
        {
            return ($this->transactionStatusAccountLu[0]->override_name) ? $this->transactionStatusAccountLu[0]->override_name : $this->name;
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('transaction_type_id', $this->transaction_type_id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('is_core', $this->is_core);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
