<?php

/**
 * This is the model class for table "call_hunt_group_phones".
 *
 * The followings are the available columns in table 'call_hunt_group_phones':
 * @property integer $id
 * @property integer $call_hunt_group_id
 * @property integer $status_ma
 * @property string $phone
 * @property string $description
 * @property string $sort_order
 * @property integer $level
 * @property string $updated_by
 * @property string $updated
 * @property string $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property CallHuntGroups $callHuntGroup
 */
class CallHuntGroupPhones extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallHuntGroupPhones the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_hunt_group_phones';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_hunt_group_id, phone, description, added_by, added', 'required'),
            array('status_ma, level, is_deleted', 'numerical', 'integerOnly'=>true),
            array('call_hunt_group_id, sort_order, updated_by, added_by', 'length', 'max'=>10),
            array('phone', 'length', 'max'=>10),
            array('phone', 'length', 'min'=>10),
            array('description', 'length', 'max'=>100),
            array('updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_hunt_group_id, phone, description, sort_order, level, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callHuntGroup' => array(self::BELONGS_TO, 'CallHuntGroups', 'call_hunt_group_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status_ma' => 'status',
            'call_hunt_group_id' => 'Call Hunt Group',
            'description' => 'Name / Description',
            'phone' => 'Phone',
            'sort_order' => 'Sort Order',
            'level' => 'Level',
            'updated_by' => 'Updated By',
            'updated' => 'Last Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('call_hunt_group_id',$this->call_hunt_group_id);
        $criteria->compare('phone',$this->phone,true);
        $criteria->compare('sort_order',$this->sort_order);
        $criteria->compare('level',$this->level);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}