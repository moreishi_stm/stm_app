<?php

/**
 * This is the model class for table "import_reconcile_data".
 *
 * The followings are the available columns in table 'import_reconcile_data':
 * @property integer $id
 * @property integer $file_id
 * @property integer $component_type_id
 * @property integer $is_scanned
 * @property string $scanned
 * @property string $data
 * @property string $collisions
 * @property string $action
 * @property string $merge_with
 * @property string $reason
 * @property string $error_type
 * @property string $added
 *
 * The followings are the available model relations:
 * @property ComponentTypes $componentType
 * @property ImportReconcileFiles $file
 */
class ImportReconcileData extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ImportReconcileData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'import_reconcile_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file_id, action, reason', 'required'),
			array('file_id, component_type_id, is_scanned', 'numerical', 'integerOnly'=>true),
//			array('action', 'length', 'max'=>6),
			array('error_type', 'length', 'max'=>9),
			array('data, collisions, added', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, file_id, component_type_id, is_scanned, data, collisions, action, merge_with, reason, error_type, added, scanned', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'componentType' => array(self::BELONGS_TO, 'ComponentTypes', 'component_type_id'),
			'file' => array(self::BELONGS_TO, 'ImportReconcileFiles', 'file_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'file_id' => 'File',
			'component_type_id' => 'Component Type',
			'is_scanned' => 'Is Scanned',
			'scanned' => 'Scanned Date',
			'data' => 'Data',
			'collisions' => 'Collisions',
			'action' => 'Action',
			'reason' => 'Reason',
			'error_type' => 'Error Type',
			'added' => 'Added',
			'merge_with' => 'Merge With'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('file_id',$this->file_id);
		$criteria->compare('component_type_id',$this->component_type_id);
		$criteria->compare('is_scanned',$this->is_scanned);
		$criteria->compare('scanned', $this->scanned, true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('collisions',$this->collisions,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('error_type',$this->error_type,true);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('merge_with', $this->merge_with, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
		));
	}
}