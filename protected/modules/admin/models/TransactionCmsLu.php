<?php

	/**
	 * This is the model class for table "transaction_cms_lu".
	 *
	 * The followings are the available columns in table 'transaction_cms_lu':
	 * @property integer $transaction_id
	 * @property integer $cms_id
	 *
	 * The followings are the available model relations:
	 * @property CmsContents $cms
	 * @property Transactions $transaction
	 */
	class TransactionCmsLu extends StmBaseActiveRecord
	{
		/**
		 * Returns the static model of the specified AR class.
		 * @param string $className active record class name.
		 * @return TransactionCmsLu the static model class
		 */
		public static function model($className=__CLASS__)
		{
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName()
		{
			return 'transaction_cms_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules()
		{
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array('transaction_id, cms_id', 'numerical', 'integerOnly'=>true),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('transaction_id, cms_id', 'safe', 'on'=>'search'),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations()
		{
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'cms' => array(self::BELONGS_TO, 'CmsContents', 'cms_id'),
				'transaction' => array(self::BELONGS_TO, 'Transactions', 'transaction_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels()
		{
			return array(
				'transaction_id' => 'Transaction',
				'cms_id' => 'Cms',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search()
		{
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria=new CDbCriteria;

			$criteria->compare('transaction_id',$this->transaction_id);
			$criteria->compare('cms_id',$this->cms_id);

			return new CActiveDataProvider($this, array(
												  'criteria'=>$criteria,
												  ));
		}
	}