<?php

/**
 * This is the model class for table "audio_files".
 *
 * The followings are the available columns in table 'audio_files':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $file_name
 * @property integer $file_size
 * @property string $file_extension
 * @property string $updated
 * @property integer $updated_by
 * @property string $added
 * @property integer $added_by
 *
 * The followings are the available model relations:
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */
class AudioFiles extends StmBaseActiveRecord
{
    public $tagCollection;
    protected $_skipSaveFile = false;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AudioFiles the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'audio_files';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, added, updated, updated_by, added_by', 'required'),
            array('file_size, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('name, file_name', 'length', 'max'=>100),
            array('description', 'length', 'max'=>500),
            array('file_name', 'file', 'allowEmpty'=>true, 'types'=>'wav, mp3', 'message'=>'Invalid file type. Allowed types are: wav, mp3', 'on'=>'insert, update'),
            array('file_extension', 'length', 'max'=>10),
            array('updated, added, tagCollection', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, description, file_name, file_size, file_extension, updated, updated_by, added, added_by, tagCollection', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'tagLu' => array(self::BELONGS_TO, 'AudioOperationManualTagLu', 'audio_file_id'),
            'tags' => array(self::MANY_MANY, 'OperationManualTags', 'audio_operation_manual_tag_lu(audio_file_id, operation_manual_tag_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'file_name' => 'File to Upload',
            'file_size' => 'File Size',
            'file_extension' => 'File Extension',
            'updated' => 'Updated',
            'updated_by' => 'Updated By',
            'added' => 'Added',
            'added_by' => 'Added By',
        );
    }

    protected function beforeValidate()
    {
        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        if($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        return parent::beforeValidate();
    }

    protected function afterSave()
    {
        if (!$this->_skipSaveFile) {
            $this->setIsNewRecord(false);
            $this->_saveFile();
            // Throw exception
        }

        parent::afterSave();
    }

    protected  function _saveFile()
    {
        $uploadedFile = CUploadedFile::getInstance($this, 'file_name');

        $docDirectory = strtr(':clientId/:componentName', array(
                ':clientId'     =>  Yii::app()->user->clientId,
                ':componentName' => 'audio',
            ));

        if (!$docDirectory || !$uploadedFile instanceof CUploadedFile) {
            return null;
        }

        // New Document method, only do this for select people
        $fullFilePath = strtr(':docDirPath/:fileName', array(
                ':docDirPath' => $docDirectory,
                ':fileName' => $this->id,
            ));

        // Initialize S3
        $s3 = \Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

        // Store the file in S3
        $result = $s3->putObject(array(
                'Bucket'                =>  (YII_DEBUG) ? 'dev.client-private.seizethemarket.com' : 'client-private.seizethemarket.com',
                'Key'                   =>  $fullFilePath,
                'SourceFile'            =>  $uploadedFile->tempName,
                'ContentType'           =>  $uploadedFile->type,
                'CacheControl'          =>  'no-cache, no-store, max-age=0, must-revalidate',
                'ContentDisposition'    =>  'inline; filename="' . $uploadedFile->name . '"'
            ));

        if($result['ObjectURL']) {
            $this->file_extension = $uploadedFile->extensionName;
            $this->file_name = $uploadedFile->name;
            $this->file_size = $uploadedFile->size;
            $this->_skipSaveFile = true;
            $this->save();
        }

        return true;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);

        if($this->name) {
            $criteria->compare('name',$this->name,true);
            $criteria->compare('description',$this->name,true, 'OR');

            //$criteria->compare('description',$this->description,true);
        }
        $criteria->compare('file_name',$this->file_name,true);
        $criteria->compare('file_size',$this->file_size);
        $criteria->compare('file_extension',$this->file_extension,true);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('added_by',$this->added_by);

        $criteria->order = 'name';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}