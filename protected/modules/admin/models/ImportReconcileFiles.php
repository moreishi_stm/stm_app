<?php

/**
 * This is the model class for table "import_reconcile_files".
 *
 * The followings are the available columns in table 'import_reconcile_files':
 * @property integer $id
 * @property integer $account_id
 * @property string $file_name
 * @property string $field_map
 * @property integer $added_by
 * @property integer $total_rows
 * @property integer $rows_scanned
 * @property integer $errors
 * @property integer $collisions
 * @property integer $new
 * @property datetime $added
 * @property string $status
 * @property string $scanned
 * @property string $sourceId
 * @property datetime $scheduled
 * @property string $call_list_id
 * @property datetime $reconciled
 *
 * The followings are the available model relations:
 * @property ImportReconcileData[] $importReconcileData
 * @property Contacts $addedBy
 */
class ImportReconcileFiles extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ImportReconcileFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'import_reconcile_files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file_name, added_by, type, title, account_id', 'required'),
			array('added_by, errors, collisions, total_rows, rows_scanned, new, type, collisions_reconciled, reconcile_errors, ignore_dupe_phones, ignore_dupe_address, account_id', 'numerical', 'integerOnly'=>true),
			array('file_name, title', 'length', 'max'=>255),
			array('status', 'length', 'max'=>50),
			array('added, scanned, sourceId, scheduled, field_map, call_list_id, error_message', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, file_name, field_map, added_by, added, status, scanned, sourceId, scheduled, call_list_id, errors, collisions, total_rows, rows_scanned, new, reconciled, collisions_reconciled, reconciled_scheduled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'importReconcileData' => array(self::HAS_MANY, 'ImportReconcileData', 'file_id'),
			'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'file_name' => 'File Name',
			'field_map' => 'Field Map',
			'added_by' => 'Added By',
			'added' => 'Added',
			'status' => 'Status',
            'error_message' => 'Error Message',
			'scanned' => 'Scanned',
			'sourceId' => 'Source ID',
			'scheduled' => "Scheduled",
			'call_list_id' => "Call List ID",
			'errors' => "Error Count",
			'collisions' => 'Collision Count',
			'total_rows' => 'Total Rows',
			'rows_scanned' => "Rows Scanned",
			'new' => 'New',
			'reconciled' => 'Reconciled On',
			'type' => 'Type',
			'collisions_reconciled' => 'Collisions Reconciled',
			'reconciled_scheduled' => 'Reconciled Scheduled',
			'reconcile_errors' => 'Reconcile Errors',
			'ignore_dupe_address' => 'Ignore Duplicate Addresses',
			'ignore_dupe_phones' => 'Ignore Duplicate Phones',
            'account_id' => 'Account ID'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('account_id',$this->account_id);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('field_map',$this->field_map,true);
		$criteria->compare('collisions',$this->collisions,true);
		$criteria->compare('errors',$this->errors,true);
        $criteria->compare('error_message', $this->error_message, true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('scanned',$this->scanned,true);
		$criteria->compare('sourceId',$this->sourceId,true);
		$criteria->compare('scheduled', $this->scheduled, true);
		$criteria->compare('call_list_id', $this->call_list_id, true);
		$criteria->compare('total_rows', $this->total_rows, true);
		$criteria->compare('rows_scanned', $this->rows_scanned, true);
		$criteria->compare('new', $this->new, true);
		$criteria->compare('reconciled', $this->reconciled, true);
		$criteria->compare('collisions_reconciled', $this->collisions_reconciled, true);
		$criteria->compare('reconciled_scheduled', $this->reconciled_scheduled, true);
		$criteria->compare('reconcile_errors', $this->reconcile_errors, true);
		$criteria->compare('ignore_dupe_address', $this->ignore_dupe_address, true);
		$criteria->compare('ignore_dupe_phones', $this->ignore_dupe_phones, true);
        $criteria->compare('account_id', $this->account_id, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
		));
	}
}