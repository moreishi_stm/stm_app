<?php

/**
 * This is the model class for table "lead_route_assigned".
 *
 * The followings are the available columns in table 'lead_route_assigned':
 * @property integer $id
 * @property integer $lead_route_id
 * @property integer $agent_contact_id
 * @property integer $component_type_id
 * @property integer $component_id
 * @property string $added
 *
 * The followings are the available model relations:
 * @property LeadRoutes $leadRoute
 * @property Contacts $agentContact
 * @property ComponentTypes $componentType
 */
class LeadRouteAssigned extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeadRouteAssigned the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lead_route_assigned';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('component_type_id, component_id', 'required'),
            array('lead_route_id, agent_contact_id, component_type_id, component_id', 'numerical', 'integerOnly'=>true),
            array('added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, lead_route_id, agent_contact_id, component_type_id, component_id, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'leadRoute' => array(self::BELONGS_TO, 'LeadRoutes', 'lead_route_id'),
            'agentContact' => array(self::BELONGS_TO, 'Contacts', 'agent_contact_id'),
            'componentType' => array(self::BELONGS_TO, 'ComponentTypes', 'component_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'lead_route_id' => 'Lead Route',
            'agent_contact_id' => 'Agent Contact',
            'component_type_id' => 'Component Type',
            'component_id' => 'Component',
            'added' => 'Added',
        );
    }

    protected function beforeValidate() {
        if ($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
        }

        return parent::beforeValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('lead_route_id',$this->lead_route_id);
        $criteria->compare('agent_contact_id',$this->agent_contact_id);
        $criteria->compare('component_type_id',$this->component_type_id);
        $criteria->compare('component_id',$this->component_id);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}