<?php

	/**
	 * This is the model class for table "settings".
	 *
	 * The followings are the available columns in table 'settings':
	 *
	 * @property integer            $id
	 * @property string             $name
	 * @property string             $label
	 * @property integer            $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property SettingAccount[]   $settingAccounts
	 * @property SettingAccountLu[] $settingAccountLus
	 * @property SettingContact[]   $settingContacts
	 * @property SettingOptions[]   $settingOptions
	 */
	class Settings extends StmBaseActiveRecord {

		public $applyAccountScope = false;

		const SETTING_TYPE_ACCOUNT_ID = 1;
		const SETTING_TYPE_CONTACT_ID = 2;
		const SETTING_TYPE_BOTH_ID = 3;

        const OFFICE_NAME_ID = 1;
        const OFFICE_PHONE_ID = 2;
        const OFFICE_ADDRESS_ID = 5;
        const OFFICE_CITY_ID = 6;
        const OFFICE_STATE_ID = 7;
        const OFFICE_ZIP_ID = 8;

        const SETTING_ANNIVERSARY_ID = 9;

        const RESPONSE_TIME_NEW_LEAD = 14;
        const SETTING_AGENT_MLS_ID = 19;

        const SETTING_PROFILE_PHOTO_ID = 22;
        const SETTING_EMAIL_SIGNATURE = 27;
        const SETTING_CELL_NUMBER = 28;
		const SETTING_PROFILE_ON_WEBSITE_ID = 43;

		const SETTING_STICKY_NOTES_ID = 38;
		const SETTING_ASSIGNMENT_TYPE_ID = 46;
		const SETTING_WEBSITE_FLYER_CONTACT_ID = 48;
        const SETTING_LEAD_POOL_CONTACT_ID = 50;
        const SETTING_MAX_ADMIN_USERS_ID = 51;
        const GOOGLE_TRACKING_CODE_ID = 53; //@todo: this is duped by #102
//        const SETTING_FIRST_RESPONDER_ALERT_ID = XX;
        const DAILY_TASK_EXECUTIVE_SUMMARY_RECIPIENT_ID = 66;
        const CLOSING_PIPELINE_EXECUTIVE_SUMMARY_RECIPIENT_ID = 68;
        const APPOINTMENTS_EXECUTIVE_SUMMARY_RECIPIENT_ID = 70;
        const GOALS_EXECUTIVE_SUMMARY_RECIPIENT_ID = 72;
        const APPOINTMENTS_EXECUTIVE_SUMMARY_AGENTS_ID = 74;
        const MAX_HOUSE_VALUES_PAGES_ID = 75;
        const MAX_LANDING_PAGES_ID = 76;
        const ZILLOW_SOURCE_ID = 77;
        const TRULIA_SOURCE_ID = 82;
        const REALTORDOTCOM_SOURCE_ID = 83;

        const SETTING_TASK_REDIRECT = 84;

        const SETTING_TIMEBLOCK_REDIRECT_START_DATE = 106;
        const SETTING_EMAIL_CLICK_NOTIFICATION = 107;
        const SETTING_MARKET_SNAPSHOT = 109;
        const SETTING_MARKET_SNAPSHOT_CONTACT_ID = 110;

        const SETTING_SLY_BROADCAST_EMAIL_ID = 111;
        const SETTING_SLY_BROADCAST_PASSWORD_ID = 112;

        const SETTING_AGENT_RECRUIT_REFERRAL_SOURCE_ID = 113;
        const SETTING_TRACKING_SCRIPTS = 114;
        const SETTING_EMAIL_BLAST_LIMIT = 115;
        const SETTING_SMS_BLAST_LIMIT = 116;

        //        const BOOMTOWN_SOURCE_ID = 83;
//        const TIGERLEAD_SOURCE_ID = 83;
//        const KUNVERSION_SOURCE_ID = 83;
//        const COMMISSIONSINC_SOURCE_ID = 83;

        const NEW_LEAD_STATUS_CHANGE_DAYS = 88;
        const NEW_LEAD_STATUS_CHANGE_DAYS_EMAIL = 89;

        const THIRD_PARTY_LEADS_EMAIL_CC = 94;
        const SETTING_LEAD_POOL_RECRUIT_ID = 95;
        const GOOGLE_ANALYTICS_CODE_ID = 102;  //@todo: this is duped by #53
        const EMAIL_BLAST_LIMIT = 115;
        const SMS_BLAST_LIMIT = 116;
        const SHOW_ASSIGNED_LEADS_ONLY = 117;

        const SETTING_CELL_NUMBER_FIELD_NAME = 'cell_number';
        const SETTING_CELL_CARRIER_FIELD_NAME = 'cell_carrier_id';

		const DASHBOARD_TYPE_OWNER_VIEW = 'owner';
		const DASHBOARD_TYPE_GENERAL_ADMIN_VIEW = 'generalAdmin';
		const DASHBOARD_TYPE_LENDER_VIEW = 'lender';
		const DASHBOARD_TYPE_BUYER_AGENT_VIEW = 'buyerAgent';
		const DASHBOARD_TYPE_SHOWING_PARTNER_VIEW = 'showingPartner';
		const DASHBOARD_TYPE_LISTING_AGENT_VIEW = 'listingAgent';
		const DASHBOARD_TYPE_GENERAL_AGENT_VIEW = 'generalAgent';
		const DASHBOARD_TYPE_CLOSING_MANAGER_VIEW = 'closingManager';
		const DASHBOARD_TYPE_LISTING_MANAGER_VIEW = 'listingManager';
		const DASHBOARD_TYPE_VIRTUAL_ASST_VIEW = 'virtualAssistant';
        const DASHBOARD_TYPE_RECRUITING = 'recruiting';

		const GENDER_FEMALE = 1;
		const GENDER_MALE = 2;

		const ALLOWED_PROFILE_PHOTO_FILE_EXTENSIONS = 'jpg,jpeg,png,gif'; //comma delimited, no spaces, will be exploded later
		const PROFILE_PHOTO_SIZE_MIN = null; // maximum file size in bytes
		const PROFILE_PHOTO_SIZE_MAX_MB = 10; //10*1024*1024; //10 Mb

		public $settings = array(); // Collection
		public $contactId;
		public $type = 'account'; // contact or account (default)
		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Settings the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'settings';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'setting_type_ma, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name, label',
					'length',
					'max' => 63
				),
				array(
					'description',
					'length',
					'max' => 127
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, setting_type_ma, name, description, label, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array behaviors for model attributes.
		 */
		public function behaviors() {
			return array(
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( // 'settingAccountValues' => array(self::HAS_MANY, 'SettingAccountValues', 'setting_id'),
				// 'settingContactValues' => array(self::HAS_MANY, 'SettingContactValues', 'setting_id'),
				// 'settingContactValue' => array(self::HAS_MANY, 'SettingContactValues', 'setting_id', 'condition'=>'t.id = setting_contact_values.setting_id AND setting_contact_values.contact_id = '.Yii::app()->user->id),
				// 'settingOptions' => array(self::HAS_MANY, 'SettingOptions', 'setting_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'setting_type_ma' => 'Setting Type Ma',
				'name' => 'Name',
				'label' => 'Label',
				'description' => 'Description',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * getByName - get's field by name only, used for cases such as sticky notes
		 *
		 * @param  string $name fieldName
		 *
		 * @return mixed  Settings model or null
		 */
		public static function getByName($name) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'name=:name';
			$Criteria->params = array(':name' => $name);

			return Settings::model()->find($Criteria);
		}

		/**
		 * gets fields by name, after all the settings for an account or contact has been loaded
		 *
		 * @param  string  $name      Searches the index by the 'name' column in transaction_fields
		 * @param  integer $contactId (optional) This can be set by this method or in the model
		 *
		 * @return object A standard object containing all of the values pertaining to the field
		 */
		public function getField($name, $type = null) {
			if ($type != null) {
				$this->type = $type;
			}

			if (!$this->settings) {
				$Settings = Settings::model()->{$this->type . 'Type'}()->findAll();

				foreach ($Settings as $Setting) {
					$this->settings[$Setting->name] = (object)$Setting->attributes;
				}
			}

			if ($this->settings[$name]) {
				return $this->settings[$name];
			}
		}

		public function getSettingContactValue($id = null) {
			$id = ($id) ? $id : Yii::app()->user->id;

			return SettingContactValues::model()->find(array(
					'condition' => 'setting_id=:settingId AND contact_id=:contactId',
					'params' => array(
						'settingId' => $this->id,
						':contactId' => $id
					)
				)
			);
		}


		public function getSettingAccountValue() {
			return SettingAccountValues::model()->find(array(
					'condition' => 'setting_id=:settingId AND account_id=:accountId',
					'params' => array(
						'settingId' => $this->id,
						':accountId' => Yii::app()->user->accountId
					)
				)
			);
		}

		/**
		 * gets fields by name
		 *
		 * @param  string  $name      Searches the index by the 'name' column in transaction_fields
		 * @param  integer $contactId (optional) This can be set by this method or in the model
		 *
		 * @return object A standard object containing all of the values pertaining to the field
		 */
		// public function getAccountField($name)
		// {
		// 	if (!$this->settings)
		// 	{
		// 		foreach( Settings::model()->account()->findAll() as $Setting )
		// 			$this->settings[$Setting->name] = (object) $Setting->attributes;
		// 	}

		// 	if ($this->settings[$name])
		// 		return $this->settings[$name];
		// 	else
		// 		return null;
		// 	// throw new CHttpException(500, 'Could not find a field related to this form.');
		// }

		/**
		 * scopes
		 *
		 * @return mixed
		 */
		public function scopes() {
			return array(
				'accountType' => array(
					'condition' => 'setting_type_ma=:accountType || setting_type_ma=:bothType',
					'params' => array(
						':accountType' => Settings::SETTING_TYPE_ACCOUNT_ID,
						':bothType' => Settings::SETTING_TYPE_BOTH_ID,
					),
				),
				'contactType' => array(
					'condition' => 'setting_type_ma=:contactType || setting_type_ma=:bothType',
					'params' => array(
						':contactType' => Settings::SETTING_TYPE_CONTACT_ID,
						':bothType' => Settings::SETTING_TYPE_BOTH_ID,
					),
				)
			);
		}

		/**
		 * getSettingTypes - model attribute setting_type_ma
		 *
		 * @return array
		 */
		public function getSettingTypes() {
			return array(
				self::SETTING_TYPE_ACCOUNT_ID => 'Account',
				self::SETTING_TYPE_CONTACT_ID => 'Contact',
				self::SETTING_TYPE_BOTH_ID => 'Both',
			);
		}

		/**
		 * getSettingTypes - model attribute setting_type_ma
		 *
		 * @return array
		 */
		public function getDashboardTypes() {
            return array(
				self::DASHBOARD_TYPE_OWNER_VIEW => 'Owner/Broker',
				self::DASHBOARD_TYPE_GENERAL_ADMIN_VIEW => 'Task List (Default/Recommended)',
				self::DASHBOARD_TYPE_LENDER_VIEW => 'Lender',
				self::DASHBOARD_TYPE_BUYER_AGENT_VIEW => 'Buyer Agent',
				self::DASHBOARD_TYPE_SHOWING_PARTNER_VIEW => 'Showing Partner',
				self::DASHBOARD_TYPE_LISTING_AGENT_VIEW => 'Listing Agent',
				self::DASHBOARD_TYPE_GENERAL_AGENT_VIEW => 'General Agent',
				self::DASHBOARD_TYPE_CLOSING_MANAGER_VIEW => 'Closing Manager',
				self::DASHBOARD_TYPE_LISTING_MANAGER_VIEW => 'Listing Manager',
				self::DASHBOARD_TYPE_VIRTUAL_ASST_VIEW => 'Virtual Asst',
                self::DASHBOARD_TYPE_RECRUITING => 'Recruiting',
			);
		}

        public static function isDateField($settingId)
        {
            $dateSettings = array(
                self::SETTING_ANNIVERSARY_ID,
                self::SETTING_TIMEBLOCK_REDIRECT_START_DATE,
            );
            return (in_array($settingId, $dateSettings))? true : false;
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('label', $this->label, true);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
