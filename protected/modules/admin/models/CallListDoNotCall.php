<?php

/**
 * This is the model class for table "call_list_do_not_call".
 *
 * The followings are the available columns in table 'call_list_do_not_call':
 * @property integer $id
 * @property integer $call_list_phone_id
 * @property integer $phone_number
 * @property string $expire_date
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property CallLists $callList
 * @property Phones $phone
 * @property Contacts $addedBy
 * @property Contacts $updatedBy
 */
class CallListDoNotCall extends StmBaseActiveRecord
{
    public $fromDate;
    public $toDate;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallListDoNotCall the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_list_do_not_call';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('contact_id, phone, expire_date, updated_by, updated, added_by, added', 'required'),
            array('call_list_phone_id, contact_id, phone, expire_date, updated_by, updated, added_by, added', 'required', 'on'=>'dialer'),
            array('call_list_phone_id, contact_id, phone, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('expire_date', 'safe'),
            array('phone', 'validateUniquePhoneCallList'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_list_phone_id, contact_id, phone, expire_date, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callListPhone' => array(self::BELONGS_TO, 'CallListPhones', 'call_list_phone_id'),
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_list_phone_id' => 'Call List',
            'contact_id' => 'Contact',
            'phone' => 'Phone',
            'expire_date' => 'Expire Date',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    public function validateUniquePhoneCallList($attribute, $params)
    {
        $model = CallListDoNotCall::model()->findByAttributes(array('call_list_phone_id'=>$this->call_list_phone_id,'phone'=>$this->phone, 'contact_id'=>$this->contact_id));
        if($model && ($model->id != $this->id)) {
            $this->addError($attribute, 'Already on Do No Call List.');
        }
    }

    public function getContactModel()
    {
        if($this->contact_id) {
            $contact = Contacts::model()->findByPk($this->contact_id);
        }
        elseif($this->call_list_phone_id) {
            if($callListPhone = CallListPhones::model()->findByPk($this->call_list_phone_id)) {
                $contact = $callListPhone->phone->contact;
            }
        }

        return $contact;
    }

    protected function beforeValidate()
    {
        $this->updated = new CDbExpression('NOW()');
        // checks to see if user Id has a value. In console command it does not.
        if($userId = Yii::app()->user->id) {
            $this->updated_by = $userId;
        }

        if($this->isNewRecord) {
            $this->added_by = $userId;
            $this->added = new CDbExpression('NOW()');
        }

        return parent::beforeValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('call_list_phone_id',$this->call_list_phone_id);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('phone',$this->phone);
        $criteria->compare('expire_date',$this->expire_date);

        $sortField = 'added';
//        switch($this->sortOrder) {
//
//            case 'addedDateAsc':
//                $criteria->order = 't.added ASC';
//                break;
//
//            case 'addedDateDesc':
//                $criteria->order = 't.added DESC';
//                break;
//
//            case 'targetDateAsc':
//                $sortField = 'target_date';
//                $criteria->order = 't.target_date ASC';
//                break;
//
//            default:
//                $sortField = 'target_date';
//                $criteria->order = 't.target_date ASC';
//                break;
//        }

        if($this->fromDate) {
            $criteria->addCondition('DATE(t.'.$sortField.') >= :fromDate');
            $criteria->params[':fromDate'] = $this->fromDate;
        }
        if($this->toDate) {
            $criteria->addCondition('DATE(t.'.$sortField.') <= :toDate');
            $criteria->params[':toDate'] = $this->toDate;
        }

        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}