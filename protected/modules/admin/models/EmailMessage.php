<?php

/**
 * This is the model class for table "email_messages".
 *
 * The followings are the available columns in table 'email_messages':
 *
 * @property integer $id
 * @property integer $origin_type_ma
 * @property integer $component_type_id
 * @property integer $component_id
 * @property string  $content
 * @property string  $to_email_id
 * @property string  $to_email_cc
 * @property string  $to_email_bcc
 * @property string  $from_email_id
 * @property string  $processed_datetime
 * @property integer $delivery_status_ma
 * @property integer $needs_response
 * @property string $imap_message_id
 */
class EmailMessage extends StmBaseActiveRecord
{

    const ORIGIN_TYPE_AD_HOC_ID = 1;
    const ORIGIN_TYPE_ACTION_PLAN_ID = 2;
    const ORIGIN_TYPE_MLS_ID = 3;
    const SYSTEM_AUTO_NEW_ACCOUNT_WELCOME = 4;
    const SYSTEM_AUTO_OTHER = 5; //Not Action Plan

    const DELIVERY_STATUS_SENT_ID = 1;
    const DELIVERY_STATUS_SOFT_BOUNCE_ID = 2;
    const DELIVERY_STATUS_HARD_BOUND_ID = 3;
    const DELIVERY_STATUS_OPT_OUT_ID = 4;

    public $contactId;

    public $fromContact;
    public $toContact;
    public $isClicked;

    public $fromDate;
    public $toDate;

    public $include_signature;

    public $subject;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return EmailMessages the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'email_messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'component_type_id, component_id, to_email_id, from_email_id, subject',
                'required'
            ),
            array(
                'origin_type_ma, component_type_id, component_id, to_email_id, delivery_status_ma, contactId, isClicked',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'imap_message_id',
                'length',
                'max' => 127
            ),
            array(
                'component_type_id, component_id, to_email_cc, to_email_bcc, content, processed_datetime, content, imap_message_id',
                'safe'
            ),
            array(
                'to_email_cc, to_email_bcc',
                'validateEmailList'
            ),
            array(
                'to_email_id',
                'validateOptStatus','on' => 'outbound'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, origin_type_ma, component_type_id, component_id, content, to_email_id, to_email_cc, to_email_bcc, from_emails, processed_datetime, delivery_status_ma, fromContact, toContact, isClicked',
                'safe',
                'on' => 'search'
            ),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'toEmail' => array(
                self::BELONGS_TO,
                'Emails',
                'to_email_id'
            ),
            'fromEmail' => array(
                self::BELONGS_TO,
                'Emails',
                'from_email_id'
            ),
            'emailClicks' => array(
                self::HAS_MANY,
                'EmailClickTracking',
                'email_message_id'
            ),
            'attachments'            => array(
                self::HAS_MANY,
                'EmailMessageAttachments',
                'email_message_id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'origin_type_ma' => 'Origin Type Ma',
            'component_type_id' => 'Component Type',
            'component_id' => 'Component',
            'content' => 'Message:',
            'to_email_id' => 'To:',
            'to_email_cc' => 'Cc:',
            'to_email_bcc' => 'Bcc:',
            'from_email_id' => 'From:',
            'processed_datetime' => 'Processed Datetime',
            'delivery_status_ma' => 'Delivery Status Ma',
            'subject' => 'Subject:',
            'imap_message_id' => 'Imap Message ID:',
        );
    }

    public function scopes()
    {
        return array(
            'orderByProcessedDesc' => array(
                'order' => 'processed_datetime DESC'
            ),
            'groupByToEmail' => array(
                'group' => 'to_email_id'
            ),
        );
    }

    public function byDateRange($dateRange)
    {
        if ($dateRange['from_date']) {
            $Criteria = new CDbCriteria;
            $Criteria->condition = 'processed_datetime>=:from_date';
            $Criteria->params = array(':from_date' => $dateRange['from_date']);
            $this->getDbCriteria()->mergeWith($Criteria);
        }

        if ($dateRange['to_date']) {
            $Criteria = new CDbCriteria;
            $Criteria->condition = 'processed_datetime<=:to_date';
            $Criteria->params = array(':to_date' => $dateRange['to_date']);
            $this->getDbCriteria()->mergeWith($Criteria);
        }

        return $this;
    }

    public function byDistinct($field) {
        $Criteria = new CDbCriteria;
        $Criteria->select = $field;
        $Criteria->distinct = true;
        $this->getDbCriteria()->mergeWith($Criteria);

        return $this;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param $id
     * @return $this
     */
    public function byComponentTypeId($id)
    {
        $Criteria = new CDbCriteria;
        $Criteria->condition = 'component_type_id=:component_type_id';
        $Criteria->params = array(':component_type_id' => $id);

        $this->getDbCriteria()->mergeWith($Criteria);

        return $this;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param $id
     * @return $this
     */
    public function byComponentId($id)
    {
        $Criteria = new CDbCriteria;
        $Criteria->condition = 'component_id=:component_id';
        $Criteria->params = array(':component_id' => $id);

        $this->getDbCriteria()->mergeWith($Criteria);

        return $this;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param $contactId
     * @return EmailMessage $this
     */
    public function byFromContactId($contactId)
    {
        $Criteria = new CDbCriteria;
        $Criteria->with = array('fromEmail' => array('alias' => 'fromEmail'), 'fromEmail.contact' => array('alias' => 'contact'));
        $Criteria->together = true;
        $Criteria->condition = 'contact_id=:contact_id';
        $Criteria->params = array(':contact_id' => $contactId);

        $this->getDbCriteria()->mergeWith($Criteria);

        return $this;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param $contactId
     * @return EmailMessage $this
     */
    public function byToContactId($contactId)
    {
        $Criteria = new CDbCriteria;
        $Criteria->with = array('toEmail' => array('alias' => 'toEmail'), 'toEmail.contact' => array('alias' => 'contact'));
        $Criteria->together = true;
        $Criteria->condition = 'contact_id=:contact_id';
        $Criteria->params = array(':contact_id' => $contactId);

        $this->getDbCriteria()->mergeWith($Criteria);

        return $this;
    }

    public function getLastSent() //deprecate as we update SentByAction
    {
        if ($result = $this->byComponentTypeId($this->component_type_id)->byComponentId($this->component_id)->orderByProcessedDesc()->find()) {
            return $result->processed_datetime;
        } else {
            return null;
        }
    }

    public function getCountSent()
    {
        if ($this->toEmail->contact) {
            return $this->byComponentTypeId($this->component_type_id)->byComponentId($this->component_id)->byToContactId($this->toEmail->contact->id)->count();
        }

        return null;
    }

    public function getClickCount()
    {
        return EmailClickTracking::model()->byEmailMessageId($this->id)->count();
    }


    public function getAllClickCountForContact($days = 90) //need to deprecate, old version of savedHomeSearch/sentBy page
    {
        $dateRange = '';
        if($days) {
            $dateRange['from_date'] = date('Y-m-d', strtotime("-$days days"));
        }
        return EmailClickTracking::model()->byContactId($this->toEmail->contact->id)->byDateRange($dateRange)->count();
    }

    public function getLastClicked()
    {
        if ($model = EmailClickTracking::model()->byEmailMessageId($this->id)->find()) {
            return $model->added;
        } else {
            return null;
        }
    }

    /**
     * Runs before this model is saved, rules:
     * 1) Set the processed_datetime to the current time of the model being saved
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return bool
     */
    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->processed_datetime = new CDbExpression('NOW()');
        }
        $this->content = StmFunctions::removeWeirdCharacters($this->content);

        return parent::beforeSave();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('origin_type_ma', $this->origin_type_ma);
        $criteria->compare('component_type_id', $this->component_type_id);
        $criteria->compare('component_id', $this->component_id);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('to_email_id', $this->to_email_id);
        $criteria->compare('to_email_cc', $this->to_email_cc);
        $criteria->compare('to_email_bcc', $this->to_email_bcc);
        $criteria->compare('from_email_id', $this->from_email_id);
        $criteria->compare('processed_datetime', $this->processed_datetime, true);
        $criteria->compare('delivery_status_ma', $this->delivery_status_ma);
        $criteria->compare('imap_message_id', $this->imap_message_id);

        $criteria->together = true;

        if($this->fromDate) {
            $criteria->compare('DATE(processed_datetime)', '>='.$this->fromDate, true);
        }

        if($this->toDate) {
            $criteria->compare('DATE(processed_datetime)', '<='.$this->toDate, true);
        }

        if ($this->toEmail->email) {
            $criteria->with = CMap::mergeArray($criteria->with, array('toEmail'));
            $criteria->compare('toEmail.email', $this->toEmail->email, true);
        }

        if ($this->fromContact) {
            $criteria->with = CMap::mergeArray($criteria->with, array('fromEmail'=>array('alias'=>'fromEmail'),'fromEmail.contact'=>array('alias'=>'contact')));
            $criteria->compare('contact.id', $this->fromContact);
        }

        if ($this->toContact) {
            $criteria->with = CMap::mergeArray($criteria->with, array('toEmail'=>array('alias'=>'toEmail'),'toEmail.contact'=>array('alias'=>'contact')));
            $criteria->compare('contact.id', $this->toContact);
        }

        if($this->isClicked==1) {
            $criteria->with = CMap::mergeArray($criteria->with, array('emailClicks'));
            $criteria->compare('emailClicks.id>', '0');
        } elseif ($this->isClicked==0) {
            $criteria->with = CMap::mergeArray($criteria->with, array('emailClicks'));
            $criteria->compare('emailClicks.id', null);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getCcList() {
        if (empty($this->to_email_cc)) {
            return null;
        }

        $ccList = explode(',', $this->to_email_cc);

        return $ccList;
    }

    public function getBccList() {
        if (empty($this->to_email_bcc)) {
            return null;
        }

        $bccList = explode(',', $this->to_email_bcc);

        return $bccList;
    }

    protected function beforeValidate() {
        if (!empty($this->to_email_cc)) {
            $this->to_email_cc = trim(str_replace(';', ',', $this->to_email_cc));
        }

        if (!empty($this->to_email_bcc)) {
            $this->to_email_bcc = trim(str_replace(';', ',', $this->to_email_bcc));
        }

        return parent::beforeValidate();
    }

    /**
     * Validate Email List. Used for BCC & CC emails that can be separated by spaces or commas
     * @param $attribute
     * @param $params
     */
    public function validateEmailList($attribute, $params)
    {
        $emailList = trim($this->$attribute);
        $emailList = str_replace(' ', ',', $emailList);

        if (!empty($emailList)) {

            // counts @ symbols
            $atCount = substr_count($emailList, '@');
            $emailsParsed = explode(',', $emailList);
            $emailCleanList = array();
            foreach($emailsParsed as $emailParse) {
                if(!empty($emailParse)) {
                    $emailCleanList[] = $emailParse;
                }
            }

            if (count($emailCleanList) !== $atCount) {
                $this->addError($attribute, 'Emails must be valid and separated a commas.');
                return;
            }

            // secondary pattern check... not sure if it's working
//            preg_match_all('/([\w-\._\+%]+@(?:[\w-]+\.)+[\w]{2,6})(,\s*\d+)*/', $emailList, $emailsParsed);
//
//            $emailsToValidate = explode(',', $emailList);
//
//            if (count($emailsToValidate) !== count($emailsParsed)) {
//                $this->addError($attribute, 'Emails must be valid and separated a comma.');
//            }
        }
    }

    public function validateOptStatus($attribute, $params)
    {
        if($this->toEmail->email && ($this->toEmail->getIsOptedOut() || $this->toEmail->getIsUniversallyUndeliverable())) {
            $this->addError($attribute, 'Email is Opt-out or Undeliverable.');
            return;
        }
    }

    /**
     * Return the content if this model is called as a string
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function __toString()
    {
        $content = (!empty($this->content)) ? $this->content : '';
        return $content;
    }
}
