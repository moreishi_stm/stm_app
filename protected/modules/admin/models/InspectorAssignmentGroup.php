<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class InspectorAssignmentGroup extends AssignmentGroup
{

    protected function assignmentAssigneeComponentColumn() {

        return 'assignee_company_id';
    }


    public function getAssignmentTypeIds()
    {

        return array(AssignmentTypes::INSPECTOR);
    }
}