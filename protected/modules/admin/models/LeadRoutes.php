<?php
	/**
	 * This is the model class for table "lead_groups".
	 *
	 * The followings are the available columns in table 'lead_groups':
	 *
     * @property integer $id
     * @property integer $account_id
     * @property integer $context_ma
     * @property integer $type_ma
     * @property integer $is_cascading
     * @property integer $component_type_id
     * @property string $name
     * @property string $description
     * @property integer $default_contact_id
     * @property integer $overflow_action_ma
     * @property string $also_full_alert_contact_ids
     * @property string $cc_contact_ids
     * @property integer $send_sms
     * @property integer $make_calls
     * @property integer $call_hunt_group_id
     * @property integer $do_reassign
     * @property integer $apply_task_overdue_limit
     * @property integer $updated_by
     * @property string $updated
     * @property integer $is_deleted
     * @property integer $sort_order
     *
     * The followings are the available model relations:
     * @property Contacts[] $contacts
     * @property LeadRouteAssigned[] $leadRouteAssigneds
     * @property LeadRouteAutoresponders[] $leadRouteAutoresponders
	 * @property LeadRouteOptionValues[] $leadRouteOptionValues
     * @property LeadRouteRules[] $leadRouteRules
     * @property CallHuntGroups $callHuntGroup
     * @property Accounts $account
     * @property Contacts $defaultContact
     * @property ComponentTypes $componentType
     * @property Contacts $updatedBy
     * @property LeadShifts[] $leadShifts
     * @property Leads[] $leads
	 */
	class LeadRoutes extends StmBaseActiveRecord {

        const CONTEXT_AGENT_ID = 1;
        const CONTEXT_LENDER_ID = 2;

		const ROUTE_TYPE_SHIFT_ID = 1;
		const ROUTE_TYPE_ROUND_ROBIN_ID = 2;

		const OVERFLOW_DEFAULT_CONTACT_ID = 1;
		const OVERFLOW_NEXT_ASSIGNEE_ID = 2;
		const OVERFLOW_PREV_ASSIGNEE_ID = 3;

		public $contactsToAdd = array(); // Collection of Contacts models
        public $triggeredRouteId;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return LeadGroups the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'lead_routes';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, type_ma, overflow_action_ma',
					'required'
				),
				array(
					'account_id, context_ma, type_ma, call_hunt_group_id, do_reassign, is_cascading, send_sms, make_calls, updated_by, is_deleted, triggeredRouteId, apply_task_overdue_limit',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'description',
					'length',
					'max' => 127
				),
				array(
					'contactsToAdd, context_ma, overflow_action_ma, default_contact_id, cc_contact_ids, updated, triggeredRouteId',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, context_ma, contactsToAdd, type_ma, call_hunt_group_id, do_reassign, is_cascading, send_sms, make_calls, name, description, cc_contact_ids, updated, updated_by, is_deleted, triggeredRouteId, apply_task_overdue_limit',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'leadRouteOptionValues' => array(
					self::MANY_MANY,
					'LeadRouteOptionValues',
					'id'
				),
				'contacts' => array(
					self::MANY_MANY,
					'Contacts',
					'leads(component_id, lead_route_id)'
				),
                'defaultContact' => array(
                    self::BELONGS_TO,
                    'Contacts',
                    'default_contact_id'
                ),
				'statContacts' => array(
					self::STAT,
					'Contacts',
					'leads(component_id, lead_route_id)'
				),
				'autoResponders' => array(
					self::HAS_MANY,
					'LeadRouteAutoresponder',
					'lead_route_id'
				),
				'leadRouteOptionValues' => array(
					self::HAS_MANY,
					'LeadRouteOptionValues',
					'lead_route_id'
				),
                'leadRouteRoundRobinContactLu' => array(
                    self::HAS_MANY,
                    'LeadRoundRobinContactLu',
                    'lead_route_id',
                    'order' => 'sort_order ASC',
                ),
                'triggeredRoutes' => array(
                    self::MANY_MANY,
                    'LeadRoutes',
                    'lead_routes_triggered(parent_route_id, triggered_route_id)',
                ),
//                'leadRouteAssigned' => array(
//                    self::HAS_MANY,
//                    'LeadRouteAssigned',
//                    'lead_route_id',
//                ),
                'lastAssigned' => array(
                    self::HAS_MANY,
                    'LeadRouteAssigned',
                    'lead_route_id',
                    'order' => 'added DESC',
                    'limit'=> 1,
                ),
                'updatedBy' => array(
                    self::BELONGS_TO,
                    'Contacts',
                    'updated_by'
                ),
                'callHuntGroup' => array(
                    self::BELONGS_TO,
                    'CallHuntGroups',
                    'call_hunt_group_id'
                ),
            );
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
                'context_ma' => 'Context',
				'type_ma' => 'Type',
                'is_cascading' => 'Cascading',
                'do_reassign'   =>  'Do Reassign',
                'make_calls'    =>  'Make Calls',
                'call_hunt_group_id'    =>  'Call Hunt Group ID',
				'name' => 'Name',
				'contactsToAdd' => 'Contacts',
				'description' => 'Description:',
                'send_sms' => 'Send SMS',
				'is_deleted' => 'Is Deleted',
				'overflow_action_ma' => 'Overflow Action',
				'default_contact_id' => 'Default Assignee',
                'cc_contact_ids' => 'Email CC',
                'apply_task_overdue_limit' => 'Apply Task Overdue Limit',
			);
		}

		public function byComponentTypeId($componentTypeId) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'component_type_id = :component_type_id',
					'params' => array(':component_type_id' => $componentTypeId),
				)
			);

			return $this;
		}

        //@todo: this is also in route observer, but this is needed to be able to access in other ways
		public function getActingAgents() {
			// Storage for any acting agents
			$actingAgents = array();

			// If the current lead route is shift based, handle within this scope
			if ($this->getIsShiftBased()) {

                $contactsToNotify = array();

                $shifts = LeadShift::model()->byLeadRouteId($this->id)->byCurrentDate()->withinCurrentTime()->findAll();
                if ($shifts) {
                    foreach ($shifts as $shift) {
                        if ($shift->contact) {
                            array_push($contactsToNotify, $shift->contact);
                        }
                    }
                } else {
                    $overflowContact = $this->getOverflowAgent();
                    if ($overflowContact) {
                        array_push($contactsToNotify, $overflowContact);
                    }
                }

                $actingAgents = (!empty($contactsToNotify)) ? $contactsToNotify : null;

			} else {
				// Round Robin Based... to be implemented
                // get the last person who got leads, if null, choose 1st person on list by sort_order
                $actingUser = $this->getNextRoundRobinAgent();
                if ($actingUser) {
                    $actingUser = array($actingUser);
                }
                $actingAgents = ($actingUser) ? $actingUser : null;
			}

			return $actingAgents;
		}

		public function getOverflowAgent() {

			if (!$this->overflow_action_ma) {
				return null;
			}

			$Agent = null;

			switch ($this->overflow_action_ma) {

				case self::OVERFLOW_NEXT_ASSIGNEE_ID:

                    // look for next shift today. If not found then look into tomorrow.
                    if (($Shift = LeadShift::model()->byLeadRouteId($this->id)->laterToday()->earliestShift()->find()) && $Shift->id) {
                        $Agent = $Shift->contact;
                        break;
                    }

                    $Shift = new LeadShift();
                    $Shift = $Shift->byLeadRouteId($this->id)->nextDay()->earliestShift()->find();
                    if ($Shift->id) {
                        $Agent = $Shift->contact;
                    }

					break;

				case self::OVERFLOW_PREV_ASSIGNEE_ID:
					$Shift = LeadShift::model()->byLeadRouteId($this->id)->priorDay()->latestShift()->find();
					if ($Shift) {
						$Agent = $Shift->contact;
					}
					break;

				case self::OVERFLOW_DEFAULT_CONTACT_ID AND $this->default_contact_id:
					$Agent = Contacts::model()->findByPk($this->default_contact_id);
					break;
			}

			if (!$Agent) {
				$Agent = Contacts::model()->findByPk($this->default_contact_id);
			}

			return $Agent;
		}

		/**
		 * getIsShiftBased
		 *
		 * @return boolean Whether or not the route is shift based
		 */
		public function getIsShiftBased() {

			return $this->type_ma == self::ROUTE_TYPE_SHIFT_ID;
		}

        public function getNextRoundRobinAgent()
        {
            if($roundRobbinList = $this->leadRouteRoundRobinContactLu) {

                if($previousAssignedAgent = $this->lastAssigned) {

                    $previousAssignedAgent = $previousAssignedAgent[0];

                    foreach($roundRobbinList as $key => $roundRobbinContactLu) {

                        if($previousAssignedAgent->agent_contact_id == $roundRobbinContactLu->contact_id) {

                            //@todo: INSERT LOGIC for apply_task_overdue_limit
                            if($key < (count($roundRobbinList)-1)) {

                                $nextAgentKey = $key +1;

                            } else {

                                $nextAgentKey = 0;
                            }

                            if($this->apply_task_overdue_limit) {

                                // checks to see if user is over the task limit, if so go to the next one.
                                if(!Tasks::isTaskOverdueMax($roundRobbinList[$nextAgentKey]->contact->id, $roundRobbinList[$nextAgentKey]->contact->account_id)) {
                                    $nextAgent = $roundRobbinList[$nextAgentKey]->contact;
                                    break;
                                }
                                else {

                                    $roundRobbinList2 = array_slice($roundRobbinList, $key);

                                    // re-order the array starting from the current user and go through the rest of the list to see if anyone else is available and meets task requirement. Once found, break from loop.
                                    foreach(array_slice($roundRobbinList, 0, $key) as $lookupDta) {
                                        $roundRobbinList2[] = $lookupDta;
                                    }

                                    foreach($roundRobbinList2 as $roundRobbinContactLu2) {
                                        // checks to see if user is over the task limit, if so go to the next one.
                                        if(!Tasks::isTaskOverdueMax($roundRobbinContactLu2->contact->id, $roundRobbinContactLu2->contact->account_id)) {
                                            $nextAgent = $roundRobbinContactLu2->contact;
                                            break 2;
                                        }
                                    }
                                }
                            }
                            else {
                                $nextAgent = $roundRobbinList[$nextAgentKey]->contact;
                                break; // or return $nextAgent
                            }
                        }
                    }
                }
                if(!$nextAgent) {
                    if($this->apply_task_overdue_limit) {

                        foreach($roundRobbinList as $key => $roundRobbinContactLu) {

                            // checks to see if user is over the task limit, if so go to the next one.
                            if(!Tasks::isTaskOverdueMax($roundRobbinContactLu->contact->id, $roundRobbinContactLu->contact->account_id)) {
                                $nextAgent = $roundRobbinContactLu->contact;
                                break;
                            }
                        }
                    }
                    else {
                        $nextAgent = $roundRobbinList[0]->contact;
                    }
                }
            }

            //@todo: need logic for if leads per round > 1

            if(!$nextAgent) {
                $nextAgent = $this->defaultContact;
            }

            return $nextAgent;
        }

        /**
		 * getGroupTypesList
		 * Returns array of Group Types
		 *
		 * @return string
		 */
		public static function getGroupTypesList() {
			return array(
				self::ROUTE_TYPE_SHIFT_ID => 'Shift',
				self::ROUTE_TYPE_ROUND_ROBIN_ID => 'Round Robin',
			);
		}

		public static function getOverflowTypesList() {

			return array(
				self::OVERFLOW_DEFAULT_CONTACT_ID => 'Assign to Default Contact',
				self::OVERFLOW_NEXT_ASSIGNEE_ID => 'Assign to Next Available Agent',
				self::OVERFLOW_PREV_ASSIGNEE_ID => 'Assign to Previous Available Agent',
			);
		}

		/**
		 * getGroupType
		 * Returns string name of Group Type
		 *
		 * @return string
		 */
		public function getGroupType() {
			$groupTypes = LeadRoutes::getGroupTypesList();

			return $groupTypes[$this->type_ma];
		}

        protected function beforeSave()
        {
            if(!empty($this->cc_contact_ids)) {
                $this->cc_contact_ids = CJSON::encode($this->cc_contact_ids);
            }

            $this->updated_by = Yii::app()->user->id;
            $this->updated = new CDbExpression('NOW()');

            return parent::beforeSave();
        }

        protected function afterFind()
        {
            if(!empty($this->cc_contact_ids)) {
                $this->cc_contact_ids = CJSON::decode($this->cc_contact_ids);
            }

            parent::afterFind();
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
            $criteria->compare('context_ma', $this->context_ma);
			$criteria->compare('type_ma', $this->type_ma);
            $criteria->compare('is_cascading', $this->is_cascading);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		public function scopes() {

			return array(
				'bySortAsc' => array(
					'order' => 'sort_order ASC',
				),
				'bySortDesc' => array(
					'order' => 'sort_order DESC'
				),
			);
		}

        /**
         * Get Contacts To Notify
         *
         * Takes a lead route and retrieves contacts to notify
         * @param LeadRoutes $leadRoute Lead route to process
         * @throws Exception When an unhandled route type is encountered
         * @return Array Contacts to notify
         */
        public static function getContactsToNotify(LeadRoutes $leadRoute, $includeCcContacts = false)
        {
            // Handle different route types
            switch($leadRoute->type_ma) {

                // Handle shift type
                case \LeadRoutes::ROUTE_TYPE_SHIFT_ID:

                    // Get shifts and make sure we have some
                    $shifts = \LeadShift::model()->byLeadRouteId($leadRoute->id)->byCurrentDate()->withinCurrentTime()->findAll();

                    // If we had shifts, go throuh them and add the responsible contacts, if we don't, add the overflow agent
                    if ($shifts) {

                        // Iterate through each shift
                        foreach ($shifts as $shift) {
                            if ($shift->contact) {
                                if($leadRoute->apply_task_overdue_limit) {
                                    if(!Tasks::isTaskOverdueMax($shift->contact->id, $shift->contact->account_id)) {
                                        $contactsToNotify[] = $shift->contact;
                                        break;
                                    }
                                }
                                else {
                                    $contactsToNotify[] = $shift->contact;
                                }
                            }
                        }
                        if(empty($contactsToNotify)) {
                            $contactsToNotify[] = $leadRoute->defaultContact;
                        }
                    }
                    else {

                        // Add overflow agent, if we can find one
                        $overflowContact = $leadRoute->getOverflowAgent();
                        if ($overflowContact) {
                            $contactsToNotify[] = $overflowContact;
                        }
                    }

                    break;

                // Handle round robin type
                case \LeadRoutes::ROUTE_TYPE_ROUND_ROBIN_ID:

                    // Get the acting user, whoever that might be...
                    $actingUser = $leadRoute->getNextRoundRobinAgent();
                    if ($actingUser) {
                        $contactsToNotify[] = $actingUser;
                    }

                    break;

                default:
                    Yii::log(__CLASS__ . '(' . __LINE__ . ') Invalid lead route type: ' . print_r($leadRoute, true), CLogger::LEVEL_ERROR);
                    throw new \Exception('Invalid lead route type!');
                    break;
            }

            //@todo: insert here any logic for extra rules such as zip codes
            //@todo: once you find the agents for a zip code is it distributed shift or round robin?

            // If we want to include CC Contacts
            if($includeCcContacts) {

                $ccContactIds = $leadRoute->cc_contact_ids;
                if(!empty($ccContactIds)) {

                    $activeAgentIds = array();
                    if($activeAgents = Contacts::model()->byActiveAdmins()->findAll()) {
                        foreach($activeAgents as $activeAgent) {
                            $activeAgentIds[] = $activeAgent->id;
                        }
                    }
                    $existingAgents = array();

                    // Build list of people that are already set to be notified
                    if(!empty($contactsToNotify)) {
                        foreach($contactsToNotify as $contact) {
                            $existingAgents[] = $contact->id;
                        }
                    }

                    // Skip those that have already been added
                    if($ccContacts = Contacts::model()->findAllByPk($ccContactIds)) {
                        foreach($ccContacts as $ccContact) {
                            //
                            //\Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: All Active Agents: '.print_r($activeAgentIds, true), \CLogger::LEVEL_ERROR);

                            if(!in_array($ccContact->id, $existingAgents) && in_array($ccContact->id, $activeAgentIds)) {
                                array_push($contactsToNotify, $ccContact);
                            }
                        }
                    }
                }
            }

            // Return data
            return $contactsToNotify;
        }

		/**
		 * Populate Lead Route Options Human Readable
		 *
		 * Used to populate data table column with pretty print options
		 * @return string
		 */
		public function populateLeadRouteOptionsHumanReadable()
		{
			// Retrieve option values
			$leadRouteOptionvalues = $this->leadRouteOptionValues;

			// Group data
			$options = array();
			foreach($leadRouteOptionvalues as $leadRouteOptionvalue) {

				// Add sub-array if we don't already have it
				$name = $leadRouteOptionvalue->leadRouteOption->name;
				if(!array_key_exists($name, $options)) {
					$options[$name] = array();
				}

				// Catalog value
				$options[$name][] = $leadRouteOptionvalue->value;
			}

			// Order alphabetically
			ksort($options, SORT_NATURAL);

			// Generate human readable string
			$out = '';
			foreach($options as $name => $values) {
				$out .= '<b>' . $name . '</b>: ' . implode(', ', $values) . "<br>";
			}

			return $out;
		}
	}