<?php

	/**
	 * This is the model class for table "contact_type_lu".
	 *
	 * The followings are the available columns in table 'contact_type_lu':
	 *
	 * @property integer      $contact_id
	 * @property integer      $contact_type_id
	 *
	 * The followings are the available model relations:
	 * @property Contacts     $contact
	 * @property ContactTypes $contactType
	 */
	class ContactTypeLu extends StmBaseActiveRecord {

		public $idCollection;
        public $excludeIds;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ContactTypeLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'contact_type_lu';
		}

		public function primaryKey() {
			return array(
				'contact_id',
				'contact_type_id'
			);
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id, contact_type_id',
					'numerical',
					'integerOnly' => true
				),
                array('excludeIds', 'safe'),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'idCollection, excludeIds, contact_type_lu_id, contact_id, contact_type_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'type' => array(
					self::BELONGS_TO,
					'ContactTypes',
					'contact_type_id'
				),
			);
		}

        // put this in here because in many_many lookups the base active record default scope gets ignored
        public function defaultScopes()
        {
            return array(
                'condition' => "`$this->tableAlias`".'.is_deleted = 0',

            );
        }

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'contact_id' => 'Contact',
				'contact_type_id' => 'Contact Type',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('contact_type_id', $this->contact_type_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}