<?php

	/**
	 * This is the model class for table "assignment_types".
	 *
	 * The followings are the available columns in table 'assignment_types':
	 *
	 * @property integer              $id
	 * @property integer              $account_id
	 * @property string               $name
     * @property string               $display_name
	 * @property integer              $sort_order
	 * @property integer              $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Accounts             $account
	 * @property ContactAssignments[] $contactAssignments
	 */
	class AssignmentTypes extends StmBaseActiveRecord {

		const ASSIGNED_TO = 1;
		const BUYER_AGENT = 2;
		const CO_BUYER_AGENT = 3;
		const LISTING_AGENT = 4;
		const CO_LISTING_AGENT = 5;
		const LISTING_MANAGER = 6;
		const SHOWING_PARTNER = 7;
		const ISA = 8;
		const BUYER_ISA = 9;
		const SELLER_ISA = 10;
		const CLOSING_MANAGER = 11;
		const SPECIFIC_USER = 12;
		const CURRENT_USER = 13;
		const CO_BROKER_AGENT = 14;
		const TITLE_AGENT = 15;
		const TITLE_PROCESSOR = 16;
		const LOAN_OFFICER = 17;
		const LOAN_PROCESSOR = 18;
        const INSPECTOR = 19;
        const CLOSING_MANAGER_ASSISTANT = 21;
        const RECRUITER = 22;
        const REFERRER = 23;

		private static $actionPlanItemTypesByComponentType = array(
			ComponentTypes::BUYERS => array(
				self::BUYER_AGENT => 'Buyer Agent',
				self::CO_BUYER_AGENT => 'Co-Buyer Agent',
                self::BUYER_ISA => 'Buyer ISA',
			),
			ComponentTypes::SELLERS => array(
				self::LISTING_AGENT => 'Listing Agent',
				self::CO_LISTING_AGENT => 'Co-Listing Agent',
				self::LISTING_MANAGER => 'Listing Manager',
                self::SELLER_ISA => 'Seller ISA'
			),
			ComponentTypes::CONTACTS => array(),
			ComponentTypes::CLOSINGS => array(
                self::CLOSING_MANAGER => 'Closing Manager'),
			    'all' => array(
                    self::SPECIFIC_USER => 'Specific User',
                    self::CURRENT_USER => 'Current User'
                )
            );

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AssignmentTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'assignment_types';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, sort_order, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, name, display_name, sort_order, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
		return array(
				// 'contactAssignments' => array(self::HAS_MANY, 'ContactAssignments', 'assignment_type_id'),
			 'assignmentTypeComponentLu' => array(self::HAS_MANY, 'AssignmentTypeComponentLu', 'assignment_type_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
				'display_name' => 'Display Name',
			);
		}

		public function byTypes($assignmentTypes = array()) {

			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $assignmentTypes);
			$this->getDbCriteria()->mergeWith($criteria);

			return $this;
		}

        public function byNames($assignmentTypeNames = array()) {

            $criteria = new CDbCriteria();
            $criteria->addInCondition('name', $assignmentTypeNames);
            $this->getDbCriteria()->mergeWith($criteria);

            return $this;
        }

        public function byTimeblockTypes() {

			$criteria = new CDbCriteria();
			$criteria->condition = 'id<=:max_id AND id>:min_id';
			$criteria->params = array(':max_id'=>self::CLOSING_MANAGER, ':min_id'=>self::ASSIGNED_TO);
			$this->getDbCriteria()->mergeWith($criteria);

			return $this;
		}

		public function byActiveAdmin() {

			//@todo: need the active contact status only... need to put in params
			$this->getDbCriteria()->mergeWith(array(
											  'join'	  => 'JOIN `setting_contact_values` ON t.id=setting_contact_values.value',
											  'condition' => 'setting_id=:setting_id',
											  'params'    => array(':setting_id' => Settings::SETTING_ASSIGNMENT_TYPE_ID),
											  ));

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name);
			$criteria->compare('display_name', $this->display_name);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

        public function findByComponentTypeId($componentTypeId) {
            return $this->findAll(array(
                'condition'=>'assignmentTypeComponentLu.component_type_id=:component_type_id',
                'with'=>'assignmentTypeComponentLu',
                'params'=>array(':component_type_id'=>$componentTypeId),
            ));
        }

        public static function getAssignmentTypeNameById($id) {
            /*$api_cache_id = 'AssignmentTypeIdName';
            $cache = Yii::app()->fileCache->get($api_cache_id);
            if(empty($cache)) {
                Yii::app()->fileCache->set($api_cache_id , CHtml::listData(AssignmentTypes::model()->findAll(), 'id', 'display_name'), 60*60*12);
                $cache = Yii::app()->fileCache->get($api_cache_id);
            }
            return $cache[$id];*/

            return CHtml::listData(AssignmentTypes::model()->findAll(), 'id', 'display_name');
        }

		public static function byComponentType($componentType) {
			$assignmentTypes = self::$actionPlanItemTypesByComponentType[$componentType];

			if (!isset($assignmentTypes)) {
				$assignmentTypes = array();
			}

			return $assignmentTypes;
		}

		public static function titleTypes() {

			return CHtml::listData(AssignmentTypes::model()->findAll(array('condition' => 'id IN (' . self::TITLE_AGENT . ',' . self::TITLE_PROCESSOR . ')')), 'id', 'name');
		}

		public static function lenderTypes() {

			return CHtml::listData(AssignmentTypes::model()->findAll(array('condition' => 'id IN (' . self::LOAN_OFFICER . ',' . self::LOAN_PROCESSOR . ')')), 'id', 'name');
		}

		public static function coBrokerTypes() {
			return CHtml::listData(AssignmentTypes::model()->findAll(array('condition' => 'id IN (' . self::CO_BROKER_AGENT . ',' . self::CLOSING_MANAGER . ')')), 'id', 'name');
		}
	}
