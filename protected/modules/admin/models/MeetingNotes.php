<?php

/**
 * This is the model class for table "meeting_notes".
 *
 * The followings are the available columns in table 'meeting_notes':
 * @property integer $id
 * @property string $date
 * @property integer $contact_id
 * @property integer $met_by_id
 * @property string $accountability
 * @property string $knowledge
 * @property string $share
 * @property string $challenges
 * @property string $action_items
 * @property integer $percent_prev_action_items_accomplished
 * @property integer $percent_goal_accomplished
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 * @property Contacts $contact
 * @property Contacts $updatedBy
 */
class MeetingNotes extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MeetingNotes the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'meeting_notes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date, contact_id, met_by_id, accountability, knowledge, challenges, action_items, updated_by, updated, added_by, added', 'required'),
            array('contact_id, met_by_id, percent_prev_action_items_accomplished, percent_goal_accomplished, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, date, contact_id, met_by_id, accountability, knowledge, share, challenges, action_items, percent_prev_action_items_accomplished, percent_goal_accomplished, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'metBy' => array(self::BELONGS_TO, 'Contacts', 'met_by_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'date' => 'Date',
            'contact_id' => 'Meeting for',
            'met_by_id' => 'Met By / Lead Agent',
            'accountability' => 'Accountability',
            'knowledge' => 'Knowledge',
            'share' => 'Share',
            'challenges' => 'Challenges',
            'action_items' => 'Action Items',
            'percent_prev_action_items_accomplished' => '% Prev Action Items Accomplished',
            'percent_goal_accomplished' => '% Goal Accomplished',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord) {

            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        if($this->date) {
            $this->date = date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($this->date));
        }
        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('contact');

        $criteria->compare('t.id',$this->id);
        $criteria->compare('date',$this->date);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('met_by_id',$this->met_by_id);
        $criteria->compare('accountability',$this->accountability,true);
        $criteria->compare('knowledge',$this->knowledge,true);
        $criteria->compare('share',$this->share,true);
        $criteria->compare('challenges',$this->challenges,true);
        $criteria->compare('action_items',$this->action_items,true);
        $criteria->compare('percent_prev_action_items_accomplished',$this->percent_prev_action_items_accomplished);
        $criteria->compare('percent_goal_accomplished',$this->percent_goal_accomplished);
        $criteria->compare('t.updated_by',$this->updated_by);
        $criteria->compare('t.updated',$this->updated,true);
        $criteria->compare('t.added_by',$this->added_by);
        $criteria->compare('t.added',$this->added,true);

        $criteria->order = 'date DESC, contact.first_name ASC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}