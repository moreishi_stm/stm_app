<?php

/**
 * This is the model class for table "domains".
 *
 * The followings are the available columns in table 'domains':
 *
 * @property integer        $id
 * @property integer        $account_id
 * @property string         $name
 * @property string         $description
 * @property string         $cms_theme_id
 * @property integer        $is_active
 * @property integer        $is_primary
 * @property integer        $for_emails
 * @property integer        $is_deleted
 *
 * The followings are the available model relations:
 * @property ActiveGuests[] $activeGuests
 * @property CmsContent[]   $cmsContents
 * @property CmsTags[]      $cmsTags
 * @property Accounts       $account
 */
class Domains extends StmBaseActiveRecord
{
    // Current requested domain
    public static $_activeDomain;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Domains the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'domains';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'account_id, is_active, is_primary, for_emails, is_deleted, cms_theme_id',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'name, description',
                'length',
                'max' => 63
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, account_id, name, cms_theme_id, is_active, is_primary, for_emails, is_deleted',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activeGuests' => array(
                self::HAS_MANY,
                'ActiveGuests',
                'id'
            ),
            'cmsContents'  => array(
                self::HAS_MANY,
                'CmsContent',
                'id'
            ),
            'cmsTags'      => array(
                self::HAS_MANY,
                'CmsTags',
                'id'
            ),
            'account'      => array(
                self::BELONGS_TO,
                'Accounts',
                'account_id'
            ),
            'theme' => array(
                self::BELONGS_TO,
                'CmsThemes',
                'cms_theme_id'
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Id',
            'account_id' => 'Account',
            'cms_theme_id' => 'Cms Theme ID',
            'name' => 'name',
            'is_active' => 'Status',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * getCount returns the number of domains for the current account
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count(array('condition' => 'account_id=' . Yii::app()->user->accountId));
    }

    /**
     * getVerboseName
     * Returns the full domain name based on whether or not the domain is a subdomain
     *
     * @author Chris Willard
     * @since  01/13/2013
     * @return string
     */
    public function getVerboseName()
    {
        return (self::isSubDomain($this->name)) ? $this->name : 'www.' . $this->name;
    }

    /**
     * Determines if a domain name is a subdomain
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $domainName
     *
     * @return bool
     */
    public static function isSubDomain($domainName)
    {
        $domainParts = explode('.', $domainName);

        // Domain has two parts, this cannot be a subdomain
        if (count($domainParts) == 2) {
            return false;
        }

        // If the domain starts with 'www' then it is not a subdomain
        $domainPrefix = $domainParts[0];
        if ($domainPrefix === 'www') {
            return false;
        }

        return true;
    }

    /**
     * Returns the active requested domain from a http request
     *
     * @author Chris Willard <chriswillard.dev@gmail.com>
     * @since  07/10/2013
     * @throws CException
     * @return CActiveRecord
     */
    public static function getActiveDomain()
    {
        // @todo: handle cli environment better
        // searches through arguments to find accountId in argument passed to command
        if(Yii::app()->user->isCliEnv) {
            if(isset($_SERVER['argv'])) {
                foreach($_SERVER['argv'] as $arg) {
                    if(strpos($arg, '--accountId=') !== false) {
                        $accountId = str_replace('--accountId=', '', $arg);
                        $domain = Domains::model()->findByAttributes(array('account_id'=>$accountId, 'for_emails'=>1, 'is_active'=>1));

                        if($domain) {
                            return $domain;
                        }
                        break;
                    }
                }
            }

                throw new CException(__CLASS__.' ('.__LINE__.') In CLI Environment. Http host not found, cannot determine domain.', 500);

//            if(!$accountId) {
//                throw new CException(__CLASS__.' ('.__LINE__.') In CLI Environment. Http host not found, cannot determine domain.', 500);
//            }
        } elseif (empty($_SERVER['HTTP_HOST'])) {
            throw new CException(__CLASS__.' ('.__LINE__.') Http host not found, cannot determine domain.', 500);
        }

        preg_match('/(.*\.)?(.*)\.(\w{0,8})/', $_SERVER['HTTP_HOST'], $requestedDomainMatches);
        $fullDomainName = current($requestedDomainMatches);

        if (self::isSubDomain($fullDomainName)) {
            $requestedDomainName = $fullDomainName;
        } else {
            $requestedDomainName = $requestedDomainMatches[$domainNameIdx = 2] . '.' . $requestedDomainMatches[$domainExtIdx = 3];
        }

        // This allows for a match when working on .local
        if (YII_DEBUG) {
            $requestedDomainMatchesPart1 = ($requestedDomainMatches[1] != 'www.') ? $requestedDomainMatches[1] : '';
            $requestedDomainName = $requestedDomainMatchesPart1 . $requestedDomainMatches[2];
        }

        // The domain name must be escaped
        $requestedDomainName = addcslashes(strtolower($requestedDomainName), '%_');

        if (YII_DEBUG) {
            $requestedDomainName .= '%';
        }

        //$api_cache_id = 'Domains_'.$fullDomainName;

        // do not file cache for seizethemarket.com. christinleeteam had seizethemarket.com in db and it caused errors in domain caching. Extra entry has been removed from db since then. 11/15/2015 - CLee
        //if(($domainCache = Yii::app()->fileCache->get($api_cache_id)) !==false && !strstr($_SERVER['HTTP_HOST'], 'seizethemarket.com')) {

        //    return $domainCache;

        //} else {

            // No valid cached data was found, so we will generate it.
            $Domain = new Domains;
            $Domain->applyAccountScope = false;
            $Domain = $Domain->find(
                array(
                    'condition' => 'name LIKE :domain',
                    'params'    => array(
                        ':domain' => $requestedDomainName
                    ),
                )
            );

            /*if(!empty($Domain)) {

                // check if domain found and file cache is match?
                if(strstr($_SERVER['HTTP_HOST'], 'seizethemarket.com') && !in_array($domainCache->name, array('seizethemarket.com','demo.seizethemarket.com', 'demotl.seizethemarket.com','library.seizethemarket.com'))) {
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Domain conflict for seizethemarket.com (non-CLI). Domain Attributes: '.print_r($domainCache->attributes, true). 'DB Connection String: '.print_r(Yii::app()->db->connectionString, true), CLogger::LEVEL_ERROR);
                }

                //filecache for 1 day: Note if this is changed, you need to delete the cache file
                //Yii::app()->fileCache->set($api_cache_id , $Domain, 60*60*12);
            }*/
            return $Domain;
        //}

        //@todo: memcache candidate location
        // memcache
//        $detailDependency = new CDbCacheDependency("SELECT UNIX_TIMESTAMP(updated) FROM domains WHERE LIKE = '{$requestedDomainName}'"); //@todo: need to put in an updated column
//        $detailDependency->connectionID = 'db'; //@todo: what does this have to be?? defaults to db, leave alone if db it seems

        // One day cache
//        $Domain = new Domains;
//        $Domain->applyAccountScope = false;
//        return $Domain->cache(86400/*, $detailDependency*/)->find(array('condition' => 'name LIKE :domain', 'params' => array(':domain' => $requestedDomainName)));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('cms_theme_id',$this->cms_theme_id,true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('is_deleted', $this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}