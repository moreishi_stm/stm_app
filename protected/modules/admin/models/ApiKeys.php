<?php

/**
 * This is the model class for table "api_keys".
 *
 * The followings are the available columns in table 'api_keys':
 * @property integer $id
 * @property integer $account_id
 * @property string $level
 * @property string $type
 * @property string $key1 (typically "code")
 * @property string $key2 (typically "access_token")
 * @property string $key3 (misc, ex. google's "refresh_token")
 * @property string $other_data (additional misc data)
 * @property string $added
 */
class ApiKeys extends StmBaseActiveRecord
{
    const LEVEL_ACCOUNT = 'Account';
    const LEVEL_CONTACT = 'Contact';
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ApiKeys the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'api_keys';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, contact_id', 'numerical', 'integerOnly' => true),
            array('account_id, level, type, key1, added', 'required'),
            array('type, key1, key2, key3', 'length', 'max'=>100),
            array('contact_id','validateLevel'),
            array('other_data, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, account_id, level, type, key1, key2, key3, other_data, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * Validate the full name for the contact.
     *
     * @param  string $attribute The attribute name
     * @param  array  $params    Parameters for the validation
     *
     * @return null
     */
    public function validateLevel($attribute, $params)
    {
        if (!$this->level == self::LEVEL_CONTACT && !$this->contact_id) {
            $this->addError('contact_id', 'Contact Level key must have a Contact ID value.');
        }
    }

}