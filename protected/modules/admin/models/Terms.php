<?php

	/**
	 * This is the model class for table "terms".
	 *
	 * The followings are the available columns in table 'terms':
	 *
	 * @property integer $id
	 * @property integer $status_ma
	 * @property string  $name
	 */
	class Terms extends StmBaseActiveRecord {

		const REQUIRED = 1;
		const EXCLUDE = 0;

        const STATUS=1;
        const CITY = 13;
        const NEIGHBORHOOD = 16;
        const COUNTY = 15;
        const ZIP = 14;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Terms the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'terms';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'name',
					'required'
				),
				array(
					'status_ma',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, status_ma, name',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'termComponentLu' => array(
					self::HAS_MANY,
					'TermComponentLu',
					'term_id'
				),
				'termCriteria' => array(
					self::HAS_MANY,
					'TermCriteria',
					'term_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'status_ma' => 'Status Ma',
				'name' => 'Name',
			);
		}

		public function findByName($name) {
			return $this->find(array(
					'condition' => 'name=:name',
					'params' => array(':name' => $name)
				)
			);
		}

		public function findCriteriaByTermName($name) {
			$model = $this->find(array(
					'select' => 'id',
					'condition' => 'name=:name',
					'params' => array(':name' => $name)
				)
			);

			return $this->findCriteriaByTermId($model->id);
		}

		public function findCriteriaByTermId($id) {
			return TermCriteria::model()->findByTermId($id);
		}

		public function getIdByName($name) {
			$model = $this->find(array('condition'=>'name=:name','params'=>array(':name'=>$name)));
			if($model)
				return $model->id;
			else
				return null;
		}

        public function getIdsByNames(array $names = array())
        {
            // Generate criteria
            $criteria = new CDbCriteria();
            $criteria->addInCondition('name', $names);
            $terms = $this->findAll($criteria);

            // Model results
            $results = array();
            if($terms) {
                foreach($terms as $term) {
                    $results[$term['name']] = $term['id'];
                }
            }

            return $results;
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('status_ma', $this->status_ma);
			$criteria->compare('name', $this->name, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}