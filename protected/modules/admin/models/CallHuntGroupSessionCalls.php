<?php

/**
 * This is the model class for table "call_hunt_group_session_calls".
 *
 * The followings are the available columns in table 'call_hunt_group_session_calls':
 * @property integer $id
 * @property string $call_hunt_group_session_id
 * @property integer $component_type_id
 * @property integer $component_id
 * @property string $source_type
 * @property string $status
 * @property string $dial_b_leg_request_uuid
 * @property string $dial_b_leg_uuid
 * @property string $answer_time
 * @property string $dial_b_leg_total_cost
 * @property string $dial_b_leg_status
 * @property string $dial_b_leg_bill_rate
 * @property string $dial_action
 * @property integer $dial_b_leg_bill_duration
 * @property string $event
 * @property string $dial_b_leg_from
 * @property string $dial_a_leg_uuid
 * @property integer $dial_b_leg_position
 * @property string $start_time
 * @property string $dial_b_leg_hangup_cause
 * @property string $call_uuid
 * @property string $end_time
 * @property integer $dial_b_leg_duration
 * @property string $dial_b_leg_to
 * @property string $record_api_id
 * @property string $recording_id
 * @property string $recording_call_uuid
 * @property string $record_url
 * @property string $record_message
 * @property integer $recording_duration
 * @property integer $recording_duration_ms
 * @property string $recording_start_ms
 * @property string $recording_end_ms
 * @property string $recording_added
 * @property string $added
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property CallHuntGroupSessions $callHuntGroupSession
 */
class CallHuntGroupSessionCalls extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallHuntGroupSessionCalls the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_hunt_group_session_calls';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_hunt_group_session_id, call_uuid, added', 'required'),
            array('component_type_id, component_id, dial_b_leg_bill_duration, dial_b_leg_position, dial_b_leg_duration, recording_duration, recording_duration_ms', 'numerical', 'integerOnly'=>true),
            array('call_hunt_group_session_id', 'length', 'max'=>10),
            array('source_type, recording_start_ms, recording_end_ms', 'length', 'max'=>16),
            array('status, dial_b_leg_status, dial_action, event', 'length', 'max'=>20),
            array('dial_b_leg_request_uuid, dial_b_leg_uuid, dial_a_leg_uuid, call_uuid, record_api_id, recording_id, recording_call_uuid', 'length', 'max'=>36),
            array('dial_b_leg_total_cost, dial_b_leg_bill_rate', 'length', 'max'=>9),
            array('dial_b_leg_from, dial_b_leg_to', 'length', 'max'=>15),
            array('dial_b_leg_hangup_cause', 'length', 'max'=>50),
            array('record_url, record_message', 'length', 'max'=>100),
            array('answer_time, start_time, end_time, recording_added, updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_hunt_group_session_id, component_type_id, component_id, source_type, status, dial_b_leg_request_uuid, dial_b_leg_uuid, answer_time, dial_b_leg_total_cost, dial_b_leg_status, dial_b_leg_bill_rate, dial_action, dial_b_leg_bill_duration, event, dial_b_leg_from, dial_a_leg_uuid, dial_b_leg_position, start_time, dial_b_leg_hangup_cause, call_uuid, end_time, dial_b_leg_duration, dial_b_leg_to, record_api_id, recording_id, recording_call_uuid, record_url, record_message, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, recording_added, added, updated', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callHuntGroupSession' => array(self::BELONGS_TO, 'CallHuntGroupSessions', 'call_hunt_group_session_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_hunt_group_session_id' => 'Call Hunt Group Session',
            'component_type_id' => 'Component Type',
            'component_id' => 'Component',
            'source_type' => 'Source Type',
            'status' => 'Status',
            'dial_b_leg_request_uuid'   =>  'Dial B Leg Request Uuid',
            'dial_b_leg_uuid' => 'Dial B Leg Uuid',
            'answer_time' => 'Answer Time',
            'dial_b_leg_total_cost' => 'Dial B Leg Total Cost',
            'dial_b_leg_status' => 'Dial B Leg Status',
            'dial_b_leg_bill_rate' => 'Dial B Leg Bill Rate',
            'dial_action' => 'Dial Action',
            'dial_b_leg_bill_duration' => 'Dial B Leg Bill Duration',
            'event' => 'Event',
            'dial_b_leg_from' => 'Dial B Leg From',
            'dial_a_leg_uuid' => 'Dial A Leg Uuid',
            'dial_b_leg_position' => 'Dial B Leg Position',
            'start_time' => 'Start Time',
            'dial_b_leg_hangup_cause' => 'Dial B Leg Hangup Cause',
            'call_uuid' => 'Call Uuid',
            'end_time' => 'End Time',
            'dial_b_leg_duration' => 'Dial B Leg Duration',
            'dial_b_leg_to' => 'Dial B Leg To',
            'record_api_id' => 'Record Api',
            'recording_id' => 'Recording',
            'recording_call_uuid' => 'Recording Call Uuid',
            'record_url' => 'Record Url',
            'record_message' => 'Record Message',
            'recording_duration' => 'Recording Duration',
            'recording_duration_ms' => 'Recording Duration Ms',
            'recording_start_ms' => 'Recording Start Ms',
            'recording_end_ms' => 'Recording End Ms',
            'recording_added' => 'Recording Added',
            'added' => 'Added',
            'updated' => 'Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('call_hunt_group_session_id',$this->call_hunt_group_session_id,true);
        $criteria->compare('component_type_id',$this->component_type_id);
        $criteria->compare('component_id',$this->component_id);
        $criteria->compare('source_type',$this->source_type,true);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('dial_b_leg_request_uuid',$this->dial_b_leg_request_uuid,true);
        $criteria->compare('dial_b_leg_uuid',$this->dial_b_leg_uuid,true);
        $criteria->compare('answer_time',$this->answer_time,true);
        $criteria->compare('dial_b_leg_total_cost',$this->dial_b_leg_total_cost,true);
        $criteria->compare('dial_b_leg_status',$this->dial_b_leg_status,true);
        $criteria->compare('dial_b_leg_bill_rate',$this->dial_b_leg_bill_rate,true);
        $criteria->compare('dial_action',$this->dial_action,true);
        $criteria->compare('dial_b_leg_bill_duration',$this->dial_b_leg_bill_duration);
        $criteria->compare('event',$this->event,true);
        $criteria->compare('dial_b_leg_from',$this->dial_b_leg_from,true);
        $criteria->compare('dial_a_leg_uuid',$this->dial_a_leg_uuid,true);
        $criteria->compare('dial_b_leg_position',$this->dial_b_leg_position);
        $criteria->compare('start_time',$this->start_time,true);
        $criteria->compare('dial_b_leg_hangup_cause',$this->dial_b_leg_hangup_cause,true);
        $criteria->compare('call_uuid',$this->call_uuid,true);
        $criteria->compare('end_time',$this->end_time,true);
        $criteria->compare('dial_b_leg_duration',$this->dial_b_leg_duration);
        $criteria->compare('dial_b_leg_to',$this->dial_b_leg_to,true);
        $criteria->compare('record_api_id',$this->record_api_id,true);
        $criteria->compare('recording_id',$this->recording_id,true);
        $criteria->compare('recording_call_uuid',$this->recording_call_uuid,true);
        $criteria->compare('record_url',$this->record_url,true);
        $criteria->compare('record_message',$this->record_message,true);
        $criteria->compare('recording_duration',$this->recording_duration);
        $criteria->compare('recording_duration_ms',$this->recording_duration_ms);
        $criteria->compare('recording_start_ms',$this->recording_start_ms,true);
        $criteria->compare('recording_end_ms',$this->recording_end_ms,true);
        $criteria->compare('recording_added',$this->recording_added,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}