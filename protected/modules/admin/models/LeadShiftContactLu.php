<?php

	/**
	 * This is the model class for table "lead_shift_contact_lu".
	 *
	 * The followings are the available columns in table 'lead_shift_contact_lu':
	 *
	 * @property integer $contact_id
	 * @property integer $lead_shift_id
	 */
	class LeadShiftContactLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return LeadShiftContactLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'lead_shift_contact_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id, lead_shift_id',
					'required'
				),
				array(
					'contact_id, lead_shift_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'contact_id, lead_shift_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array();
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'contact_id' => 'Contact',
				'lead_shift_id' => 'Lead Shift',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('lead_shift_id', $this->lead_shift_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}