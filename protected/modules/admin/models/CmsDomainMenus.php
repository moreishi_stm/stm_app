 <?php
 /**
 * This is the model class for table "cms_domain_menus".
 *
 * The followings are the available columns in table 'cms_domain_menus':  * @property string $id
  * @property string $menu_position_id
  * @property integer $domain_id
  * @property string $menu_id
  * @property string $added
  * @property integer $added_by
  * @property string $updated
  * @property integer $updated_by
   *
 * The followings are the available model relations:  * @property   * @property   * @property    */
 class CmsDomainMenus extends StmBaseActiveRecord
 {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CmsDomainMenus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_domain_menus';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('menu_position_id, domain_id, menu_id, added, added_by', 'required'),
 			array('domain_id, added_by, updated_by', 'numerical', 'integerOnly'=>true),
 			array('menu_position_id, menu_id', 'length', 'max'=>10),
 			array('updated', 'safe'),
 			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, menu_position_id, domain_id, menu_id, added, added_by, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'menu' => array(self::BELONGS_TO, 'CmsMenus', 'menu_id'),
 			'menuPosition' => array(self::BELONGS_TO, 'CmsMenuPositions', 'menu_position_id'),
 			'domain' => array(self::BELONGS_TO, 'Domains', 'domain_id'),
 		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
 			'menu_position_id' => 'Menu Position',
 			'domain_id' => 'Domain',
 			'menu_id' => 'Menu',
 			'added' => 'Added',
 			'added_by' => 'Added By',
 			'updated' => 'Updated',
 			'updated_by' => 'Updated By',
 		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;
        $criteria->compare('id',$this->id,true);
		$criteria->compare('menu_position_id',$this->menu_position_id,true);
		$criteria->compare('domain_id',$this->domain_id);
		$criteria->compare('menu_id',$this->menu_id,true);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}