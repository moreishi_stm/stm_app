<?php

/**
 * This is the model class for table "addresses".
 *
 * The followings are the available columns in table 'addresses':
 *
 * @property integer            $id
 * @property integer            $account_id
 * @property integer            $address_type_ma
 * @property string             $address
 * @property string             $city
 * @property integer            $state_id
 * @property string             $zip
 * @property integer            $is_deleted
 *
 * The followings are the available model relations:
 * @property AddressContactLu[] $addressContactLus
 * @property AddressStates      $state
 * @property Closings[]         $closings
 */
class Addresses extends StmBaseActiveRecord
{
    const COMPARE_LENGTH = 9;

    public $applyAccountScope = false;

    // Used to create a relationship between the address and the contact
    public $contactId;

    // Used to create a relationship between the address and the company

    public $companyId;

    public $isPrimary = 0;

    // Flag to remove the record during processing

    public $remove = false;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Addresses the static model class
     */
    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {

        return 'addresses';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'address, city, state_id, zip',
                'required'
            ),
            array(
                'account_id, address_type_ma, state_id,  is_deleted',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'address',
                'length',
                'max' => 127
            ),
            array(
                'city',
                'length',
                'max' => 50
            ),
            array(
                'zip',
                'length',
                'max' => 5
            ),
            array(
                'remove',
                'safe'
            ),
            array(
                'isPrimary',
                'safe'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, account_id, address_type_ma, address, city, state_id, zip, is_deleted',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addressContactLus' => array(
                self::HAS_MANY,
                'AddressContactLu',
                'address_id'
            ),
            'addressCompanyLus' => array(
                self::HAS_MANY,
                'AddressCompanyLu',
                'address_id'
            ),
            'state'             => array(
                self::BELONGS_TO,
                'AddressStates',
                'state_id'
            ),
            'sellers'           => array(
                self::HAS_MANY,
                'Sellers',
                'address_id'
            ),
            'transactions'           => array(
                self::HAS_MANY,
                'Transactions',
                'address_id'
            ),
            'closings'          => array(
                self::HAS_MANY,
                'Closings',
                'address_id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'id'              => 'Address',
            'account_id'      => 'Account',
            'address_type_ma' => 'Address Type Ma',
            'address'         => 'Address',
            'city'            => 'City',
            'state_id'        => 'State',
            'zip'             => 'Zip',
            'is_deleted'      => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('address_type_ma', $this->address_type_ma);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('state_id', $this->state_id);
        $criteria->compare('zip', $this->zip, true);
        $criteria->compare('is_deleted', $this->is_deleted);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    /**
     * Retrieve a concatenated string of the full address
     * If any of the necessary components to form the full address are missing just return the address
     *
     * @return string
     */
    public function getFullAddress()
    {

        if ($this->city && $this->state && $this->zip) {
            return $this->address . ', ' . $this->city . ', ' . AddressStates::getShortNameById($this->state_id) . ' ' . $this->zip;
        }

        return $this->address;
    }

    public function findAllByContactIdAddress($contactId, $streetAddress, $zip)
    {
//        return Yii::app()->db->createCommand()
//            ->select('a.id') //, lu.contact_id
//            ->from('addresses a')
//            ->join('address_contact_lu lu','lu.address_id=a.id')
//            ->where('TRIM(LOWER(SUBSTRING(a.address=:address,1,'.Addresses::COMPARE_LENGTH.')))', array(':address'=>substr(strtolower(trim($streetAddress)), 0, Addresses::COMPARE_LENGTH)))
//            ->andWhere('zip=:zip', array(':zip' => substr($zip,0,5)))
//            ->andWhere('lu.contact_id=:contactId', array(':contactId'=>$contactId))
//            ->andWhere('a.is_deleted=0 AND lu.is_deleted=0')
//            ->queryColumn();

        $criteria = new CDbCriteria();
        $criteria->with = array('addressContactLus');
        $criteria->compare('SUBSTRING(LOWER(TRIM(address)),1,'.Addresses::COMPARE_LENGTH.')', substr(strtolower(trim($streetAddress)), 0, Addresses::COMPARE_LENGTH), true);
        $criteria->compare('addressContactLus.contact_id', $contactId);
        $criteria->compare('zip', substr($zip,0,5), true);
        $criteria->order = 't.id DESC';
        return $this->findAll($criteria);
    }

    public function getStreetNumber()
    {

        return current(explode(' ', $this->address));
    }

    public function getStreet()
    {

        $addressParts = explode(' ', $this->address);
        unset($addressParts[0]);

        return implode(' ', $addressParts);
    }

    protected function afterFind()
    {

        if ($this->addressCompanyLus) {
            foreach ($this->addressCompanyLus as $AddressCompanyLu) {
                if ($AddressCompanyLu->is_primary) {
                    $this->isPrimary = $AddressCompanyLu->is_primary;
                }
            }
        }

        if ($this->addressContactLus) {
            foreach ($this->addressContactLus as $AddressContactLu) {
                if ($AddressContactLu->is_primary) {
                    $this->isPrimary = $AddressContactLu->is_primary;
                }
            }
        }

        return parent::afterFind();
    }

    protected function beforeSave()
    {
        $this->address = trim($this->address);
        $this->city = trim($this->city);
        return parent::beforeSave();
    }

        /**
     * Add a lookup record to connect this e-mail to a contact
     *
     * @return boolean
     */
    protected function afterSave()
    {

        if ($this->isNewRecord) {
            if ($this->contactId) {
                $AddressLookupRef             = new AddressContactLu;
                $AddressLookupRef->contact_id = $this->contactId;
            } else {
                $AddressLookupRef             = new AddressCompanyLu;
                $AddressLookupRef->company_id = $this->companyId;
            }

            $AddressLookupRef->address_id = $this->id;
            $AddressLookupRef->is_primary = $this->isPrimary;
            $AddressLookupRef->save();

            return parent::afterSave();
        }
    }

    public static function existsForContact($address, $city, $stateId, $zip, $contactId)
    {
        return (boolean) Yii::app()->db->createCommand()
            ->select('a.id')
            ->from('addresses a')
            ->join('address_contact_lu lu','lu.address_id=a.id')
            ->where('a.address=:address', array(':address'=>$address))
            ->andWhere('a.state_id=:stateId', array(':stateId'=>$stateId))
            ->andWhere('a.zip=:zip', array(':zip'=>$zip))
            ->andWhere('lu.contact_id=:contactId', array(':contactId'=>$contactId))
            ->andWhere('a.is_deleted=0 AND lu.is_deleted=0')
            ->order('a.id ASC')
            ->queryScalar();
    }

    /**
     * Soft delete the lookup record
     *
     * @return boolean
     */
    protected function beforeDelete()
    {

        if ($this->addressContactLus) {
            foreach ($this->addressContactLus as $AddressContactLu) {
                $AddressContactLu->delete();
            }
        }

        //TODO: Talk to Chris about how we should handle Company Address References when an address is removed from a contact.
        if ($this->addressCompanyLus) {
            foreach ($this->addressCompanyLus as $AddressCompanyLu) {
                $AddressCompanyLu->delete();
            }
        }

        $hasTransactionReference = count(Transactions::model()->byAddress($this->id)->findAll()) > 0;

        if (!$hasTransactionReference) {
            return parent::beforeDelete();
        } else {
            $this->addErrors(array('id' => 'This address is used in a transaction.'));

            return false;
        }
    }
}