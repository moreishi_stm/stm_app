<?php

/**
 * This is the model class for table "call_blocks".
 *
 * The followings are the available columns in table 'call_blocks':
 * @property string $id
 * @property integer $status_ma
 * @property string $phone
 * @property string $reason
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */
class CallBlocks extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallBlocks the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_blocks';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('phone, reason, updated_by, added_by', 'required'),
            array('status_ma, phone, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('phone', 'validatePhone'),
            array('phone', 'unique', 'message'=>'This phone number is already blocked.'),
            array('reason', 'length', 'max'=>255),
            array('updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, status_ma, phone, reason, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callBlockCalls' => array(self::HAS_MANY, 'CallBlockCalls', 'call_block_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status_ma' => 'Status',
            'phone' => 'Phone',
            'reason' => 'Reason',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    protected function beforeValidate()
    {
        $this->phone = Yii::app()->format->formatInteger($this->phone);

        if($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    public function validatePhone($attribute, $params)
    {
        if (strlen($this->phone) < 10) {
            $this->addError($attribute, 'Please enter a valid 10 digit phone.');
        }

        // if edit and the new and old number don't match, see of there is existing call block data linked to this. If don't let the number change.
        if(!$this->isNewRecord) {

            $originalRecord = CallBlocks::model()->findByPk($this->id);
            if($originalRecord->phone != $this->phone) {

                // check to see if any blocked incoming calls are related to this record
                if($this->callBlockCalls) {
                    $this->addError($attribute, 'This phone number has existing blocked incoming call records and cannot be changed. Please add a new number.');
                }
            }
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('status_ma',$this->status_ma);
        $criteria->compare('phone',$this->phone,true);
        $criteria->compare('reason',$this->reason,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}