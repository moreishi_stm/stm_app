<?php

/**
 * This is the model class for table "featured_areas".
 *
 * The followings are the available columns in table 'featured_areas':
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $mls_board_id
 * @property integer $parent_id
 * @property integer $status_ma
 * @property string  $name
 * @property string  $title
 * @property string  $display_description_top
 * @property string  $description
 * @property string  $url
 * @property integer $count_homes
 * @property integer $count_photo
 * @property integer $min_price
 * @property integer $max_price
 * @property integer $avg_price
 * @property string  $avg_bedrooms
 * @property string  $avg_baths
 * @property integer $avg_sf
 * @property integer $avg_price_sf
 * @property integer $avg_br1
 * @property integer $avg_br2
 * @property integer $avg_br3
 * @property integer $avg_br4
 * @property integer $avg_br5
 * @property integer $avg_dom
 * @property string  $updated
 */
class FeaturedAreas extends StmBaseActiveRecord
{

	public $image = NULL;
    public $typesCollection = array();

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return FeaturedAreas the static model class
     */
    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {

        return 'featured_areas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, mls_board_id, name, url', 'required'),
            array(
                'display_description_top, parent_id, mls_board_id, status_ma, count_homes, count_photo, min_price, max_price, avg_price, avg_sf, avg_price_sf, avg_br1, avg_br2, avg_br3, avg_br4, avg_br5, avg_dom',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'name, url',
                'length',
                'max' => 63
            ),
            array(
                'title',
                'length',
                'max' => 50
            ),
            // unique per account
            array(
                'url, name',
                'unique',
                'criteria' => array(
                    'condition' => 'account_id=:account_id',
                    'params'    => array(':account_id' => Yii::app()->user->accountId,),
                ),
            ),
            array(
                'avg_bedrooms, avg_baths',
                'length',
                'max' => 3
            ),
            array(
                'description, updated, typesCollection, stats_last_updated',
                'safe'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, parent_id, mls_board_id, status_ma, name, title, description, display_description_top, url, count_homes, count_photo, min_price, max_price, avg_price, avg_bedrooms, avg_baths, avg_sf, avg_price_sf, avg_br1, avg_br2, avg_br3, avg_br4, avg_br5, avg_dom, updated, stats_last_updated, typesCollection',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'types'  => array(
                self::MANY_MANY,
                'FeaturedAreaTypes',
                'featured_area_type_lu(featured_area_id, featured_area_type_id)'
            ),
            'typeLu' => array(
                self::HAS_MANY,
                'FeaturedAreaTypeLu',
                'featured_area_id'
            ),
            'mlsBoard' => array(
                self::BELONGS_TO,
                'MlsBoards',
                'mls_board_id'
            ),
        );
    }

    public function behaviors()
    {

        return array(
            'TermCriteriaBuilder' => array('class' => 'admin_module.components.behaviors.TermCriteriaBuilderBehavior'),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'updateAttribute' => 'updated',
                'createAttribute' => null,
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'id'           => 'ID',
            'status_ma'    => 'Status Ma',
            'name'         => 'Name',
            'title'        => 'Title',
            'description'  => 'Description',
            'url'          => 'Url',
            'count_homes'  => 'Count Homes',
            'count_photo'  => 'Count Photo',
            'min_price'    => 'Min Price',
            'max_price'    => 'Max Price',
            'avg_price'    => 'Avg Price',
            'avg_bedrooms' => 'Avg Bedrooms',
            'avg_baths'    => 'Avg Baths',
            'avg_sf'       => 'Avg Sf',
            'avg_price_sf' => 'Avg Price Sf',
            'avg_br1'      => 'Avg Br1',
            'avg_br2'      => 'Avg Br2',
            'avg_br3'      => 'Avg Br3',
            'avg_br4'      => 'Avg Br4',
            'avg_br5'      => 'Avg Br5',
            'avg_dom'      => 'Avg Dom',
            'updated'      => 'Updated',
            'stats_last_updated' => 'Status Last Udpated',
        );
    }

    /**
     * getArea finds Featured Area, returns default if not found
     *
     * @param $url
     *
     * @return FeaturedAreas model
     */
    public function findByUrl($url)
    {

        $Criteria = new CDbCriteria;
        if (!$url) {
            $url = 'all';
        }

        $Criteria->condition = 'url=:url';
        $Criteria->params    = array(':url' => $url);

        return $this->find($Criteria);
    }

    /**
     * getArea finds Featured Area, returns default if not found
     *
     * @return string the formatted price range, if applicable.
     */
    public function getPriceRange()
    {

        if (!$this->min_price && !$this->max_price) {
            return StmFormatter::DEFAULT_VALUE;
        } else {
            return Yii::app()->format->formatDollars($this->min_price) . ' - ' . Yii::app()->format->formatDollars(
                $this->max_price
            );
        }
    }


    /**
     * isSimpleCriteria checks to see if this featured area has a simple criteria that can be edit via GUI
     *
     * @return boolean
     */
//    public function isGuiFriendly()
//    {
//        return (boolean) TermComponentLu::model()->count(array('component_type_id'=>ComponentTypes::FEATURED_AREAS,'component_id'=>$this->id));
//    }

    public function getTermComponentLu()
    {
        return TermComponentLu::model()->findAllByAttributes(array('component_type_id'=>ComponentTypes::FEATURED_AREAS,'component_id'=>$this->id));
    }

    /**
     * calculateStats calculates the featured area stats
     *
     * @return mixed true if success or array of error messages
     */
    public function calculateStats()
    {
        if(Yii::app()->user->hasMultipleBoards()) {
            $mlsBoard = MlsBoards::model()->findByPk($this->mls_board_id);
        }
        else {
            $mlsBoard = Yii::app()->user->board;
        }

        $boardPrefixName = $mlsBoard->getPrefixedName();
        $this->board_name = $boardPrefixName;
        $properties = MlsProperties::model($boardPrefixName);

        $criteria = $this->getCriteria(ComponentTypes::FEATURED_AREAS, $this->id);
        $activeCriteria = $this->getCriteriaByTermName('status', 'Active');
        $criteria->mergeWith($activeCriteria);
//        $propertyTypeCriteria = $this->getCriteriaByTermName('property_type', 1);
//        $criteria->mergeWith($propertyTypeCriteria);
        $criteria->select = 'count(*) count_homes, sum(photo_count) count_photo, min(price) min_price, max(price) max_price, round(avg(price)) avg_price, round(avg(bedrooms),1) avg_bedrooms, round(avg(baths_total),1) avg_baths, round(avg(sq_feet)) avg_sf, round((avg(price)) / round(avg(sq_feet))) avg_price_sf';
        $statsResult = $properties->find($criteria);

        if (!empty($statsResult)) {
            $featuredAreaFields = array('count_homes', 'count_photo', 'min_price', 'max_price', 'avg_price', 'avg_bedrooms', 'avg_baths', 'avg_sf', 'avg_price_sf');
            foreach($featuredAreaFields as $featuredAreaField) {
                $this->$featuredAreaField  = $statsResult->$featuredAreaField;
            }

            $bedroomCriteria = new CDbCriteria;
            $bedroomCriteria->mergeWith($criteria);
            $bedroomCriteria->addCondition('bedrooms = :bedrooms');
            for($bedroomCount=1; $bedroomCount <= ($maxBedrooms = 5); $bedroomCount++) {
                $averageBedroomAliasName = "avg_br{$bedroomCount}";
                $bedroomCriteria->select = "avg(price) {$averageBedroomAliasName}";
                $bedroomCriteria->params['bedrooms'] = $bedroomCount;
                $bedroomStatResult = $properties->find($bedroomCriteria);
                $this->$averageBedroomAliasName = round($bedroomStatResult->$averageBedroomAliasName);
            }

            $domCriteria = new CDbCriteria;
            $domCriteria->mergeWith($criteria);
            $domCriteria->select = 'avg(DATEDIFF(now(),status_change_date)) avg_dom';
            $domStatsResult = $properties->find($domCriteria);
            $this->avg_dom = round($domStatsResult->avg_dom);
            $this->stats_last_updated = new CDbExpression('NOW()');

            if(!$this->save()) {
                return $this->getErrors();
            }
        }
        return true;
    }

    	protected function beforeValidate() {

            if(empty($this->account_id)) {
                $this->account_id = Yii::app()->user->accountId;
            }

            if(empty($this->mls_board_id) && !Yii::app()->user->hasMultipleBoards()) {
                $this->mls_board_id = Yii::app()->user->board->id;
            }

    		return parent::beforeValidate();
    	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);

        if(!empty($this->typesCollection)) {
            $criteria->together = true;
            $criteria->with = array('types');
            $criteria->compare('types.name', $this->typesCollection, true);
        }

        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('status_ma', $this->status_ma);
        $criteria->compare('mls_board_id', $this->mls_board_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('count_homes', $this->count_homes);
        $criteria->compare('count_photo', $this->count_photo);
        $criteria->compare('min_price', $this->min_price);
        $criteria->compare('max_price', $this->max_price);
        $criteria->compare('avg_price', $this->avg_price);
        $criteria->compare('avg_bedrooms', $this->avg_bedrooms, true);
        $criteria->compare('avg_baths', $this->avg_baths, true);
        $criteria->compare('avg_sf', $this->avg_sf);
        $criteria->compare('avg_price_sf', $this->avg_price_sf);
        $criteria->compare('avg_br1', $this->avg_br1);
        $criteria->compare('avg_br2', $this->avg_br2);
        $criteria->compare('avg_br3', $this->avg_br3);
        $criteria->compare('avg_br4', $this->avg_br4);
        $criteria->compare('avg_br5', $this->avg_br5);
        $criteria->compare('avg_dom', $this->avg_dom);
        $criteria->compare('updated', $this->updated, true);
        $criteria->compare('stats_last_updated', $this->stats_last_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }

    public function __toString()
    {

        $attributesString = "";
        foreach ($this->getAttributes() as $attributeName => $attributeValue) {
            $attributesString .= "{$attributeName}={$attributeValue}, ";
        }

        return __CLASS__ . ' : (' . substr($attributesString, 0, -2) . ')';
    }
}