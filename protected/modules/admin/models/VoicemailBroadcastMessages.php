<?php

/**
 * This is the model class for table "voicemail_broadcast_messages".
 *
 * The followings are the available columns in table 'voicemail_broadcast_messages':
 * @property string $id
 * @property integer $account_id
 * @property string $name
 * @property integer $added_by
 * @property integer $updated_by
 * @property string $added
 * @property string $updated
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Accounts $account
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */
class VoicemailBroadcastMessages extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VoicemailBroadcastMessages the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'voicemail_broadcast_messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, name, added_by, added', 'required'),
            array('account_id, added_by, updated_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>100),
            array('updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, account_id, name, added_by, updated_by, added, updated, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'account_id' => 'Account',
            'name' => 'Name',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'added' => 'Added',
            'updated' => 'Updated',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('account_id',$this->account_id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
} 