<?php
	Yii::import('admin_module.models.interfaces.Component');

	/**
	 * This is the model class for table "project_items".
	 *
	 * The followings are the available columns in table 'project_items':
	 * @property integer $id
	 * @property integer $project_id
	 * @property string $description
	 * @property integer $is_priority
	 * @property integer $sort_order
	 * @property string $due_date
	 * @property string $updated
	 * @property integer $updated_by
	 * @property string $added
	 * @property integer $added_by
	 * @property integer $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Projects $project
	 * @property Contacts $updatedBy
	 * @property Contacts $addedBy
	 */
	class ProjectItems extends StmBaseActiveRecord
	{
		private $_componentType;

		/**
		 * Returns the static model of the specified AR class.
		 * @param string $className active record class name.
		 * @return ProjectItems the static model class
		 */
		public static function model($className=__CLASS__)
		{
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName()
		{
			return 'project_items';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules()
		{
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array('due_date, updated, updated_by, added, added_by', 'required'),
				array('project_id, is_priority, sort_order, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
				array('description', 'safe'),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('id, project_id, description, is_priority, sort_order, due_date, updated, updated_by, added, added_by, is_deleted', 'safe', 'on'=>'search'),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations()
		{
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
				'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
				'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
			);
		}

		public function scopes() {
			return array(
				'byPriority' => array(
					'order' => 'is_priority desc',
				),
				'byDueDateDesc' => array(
					'order' => 'due_date desc',
				),
			);
		}

		/**
		 * @return array behaviors for getTasks
		 */
		public function behaviors() {
			return array(
				'getTasks' => array(
					'class' => 'admin_module.components.behaviors.GetTasksBehavior',
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels()
		{
			return array(
				'id' => 'ID',
				'project_id' => 'Project',
				'description' => 'Description',
				'is_priority' => 'Is Priority',
				'sort_order' => 'Sort Order',
				'due_date' => 'Due Date',
				'updated' => 'Updated',
				'updated_by' => 'Updated By',
				'added' => 'Added',
				'added_by' => 'Added By',
				'is_deleted' => 'Is Deleted',
			);
		}


		public function getContactId() {
			return $this->project->contact_id;
		}

		public function getComponentType() {
			if (!isset($this->_componentType)) {
				$this->_componentType = ComponentTypes::model()->findByPk(ComponentTypes::PROJECT_ITEMS);
			}

			return $this->_componentType;
		}

		/**
		 * @return array collection of component tuple representations formatted as described in the asComponentTuple function.
		 * @see asComponentTuple()
		 */
		public function getComponentChain() {
			$chain = array($this->asComponentTuple());

			$includeCompleted = true;
			$Tasks = $this->getTasks($this, $includeCompleted);

			if (count($Tasks) > 0) {
				foreach ($Tasks as $Task) {
					array_push($chain, $Task->asComponentTuple());
				}
			}

			return $chain;
		}

		/**
		 * Returns a representation of this component as an associative array containing its
		 * componentTypeId and componentId keyed verbatim.
		 *
		 * @return array associative array representation of this component.
		 */
		public function asComponentTuple() {
			return array(
				'componentTypeId' => $this->componentType->id,
				'componentId' => $this->id
			);
		}

		protected function beforeValidate() {

			if ($this->isNewRecord) {
				$this->added = date(StmFormatter::MYSQL_DATETIME_FORMAT);
				$this->added_by = Yii::app()->user->id;
			}

			$this->updated_by = Yii::app()->user->id;
			$this->updated = date('Y-m-d H:i:s');

			return parent::beforeValidate();
		}

		protected function beforeSave() {
			$this->due_date = date(StmFormatter::MYSQL_DATETIME_FORMAT, strtotime($this->due_date));

			return parent::beforeSave();
		}

		protected function afterFind() {
			$this->due_date = ($this->due_date < 1) ? null : Yii::app()->format->formatDate($this->due_date, StmFormatter::APP_DATE_PICKER_FORMAT);

			return parent::afterFind();
		}

		public function printPriorityFlag() {
			return ($this->is_priority) ? '<span class="priority-task-flag">!</span> ' : '';
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search()
		{
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria=new CDbCriteria;

			$criteria->compare('id',$this->id);
			$criteria->compare('project_id',$this->project_id);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('is_priority',$this->is_priority);
			$criteria->compare('sort_order',$this->sort_order);
			$criteria->compare('due_date',$this->due_date,true);
			$criteria->compare('updated',$this->updated,true);
			$criteria->compare('updated_by',$this->updated_by);
			$criteria->compare('added',$this->added,true);
			$criteria->compare('added_by',$this->added_by);
			$criteria->compare('is_deleted',$this->is_deleted);

			return new CActiveDataProvider($this, array(
												  'criteria'=>$criteria,
												  ));
		}
	}