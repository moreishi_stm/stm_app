<?php

/**
 * This is the model class for table "cms_tags".
 *
 * The followings are the available columns in table 'cms_tags':
 *
 * @property integer    $id
 * @property integer    $parent_id
 * @property integer    $account_id
 * @property integer    $cms_content_id
 * @property string     $name
 *
 * The followings are the available model relations:
 * @property CmsContent $cmsContent
 * @property Domains    $domain
 */
class CmsTags extends StmBaseActiveRecord {

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return CmsTags the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'cms_tags';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('account_id, name, url, is_popular_topic_link', 'required'),
            array('name, url', 'unique','message'   => 'CMS tag "{value}" already exists.'),
			array('parent_id, account_id, is_popular_topic_link', 'numerical', 'integerOnly'=>true),
			array('name, url', 'length', 'max'=>63),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, account_id, name, url, is_popular_topic_link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
            'parent' => array(self::BELONGS_TO, 'CmsTags', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
            'parent_id' => 'Subcategory of',
			'account_id' => 'Account',
			'name' => 'Name',
            'url' => 'Url',
            'is_popular_topic_link' => 'Popular Topic Link',
		);
	}

    public function scopes() {
        return array(
            'orderNameAsc' => array(
                'order' => 'name ASC',
            ),
            'isPopularLink' => array(
                'condition' => 'is_popular_topic_link=:is_popular_topic_link',
                'params' => array(':is_popular_topic_link'=>1),
            ),
            'excludeSelf' => array(
                'condition' => 'id<>:id',
                'params' => array(':id'=>$this->id),
            ),
            'excludeDirectParent' => array(
                'condition' => 'parent_id<>:parent_id OR parent_id is NULL',
                'params' => array(':parent_id'=>$this->id),
            ),
        );
    }
	/**
	 * Create a scope to filter the request by a url
	 *
	 * @param int $domainId identifier of the domain associated with this CmsTag instance.
	 *
	 * @return CmsTags $this
	 */
	public function byAccount($domainId)
	{
		$this->getDbCriteria()->mergeWith(array(
										  'condition'=>'account_id = :account_id',
										  'params'=>array(':account_id'=>$domainId)
										  ));
		return $this;
	}

    public function byUrlOrName($urlName)
    {
        $this->getDbCriteria()->mergeWith(array(
                'condition'=>'url = :url OR name=:name',
                'params'=>array(':url'=>$urlName,':name'=>$urlName)
            ));
        return $this;
    }

    public function parentDropDownList() {
        $list = $this->excludeSelf()->excludeDirectParent()->findAll();
        return CHtml::listData($list,'id','name');
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
        $criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('name', $this->name, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('is_popular_topic_link', $this->is_popular_topic_link, true);
        $criteria->order = 'name ASC';

		return new CActiveDataProvider($this, array(
											  'criteria' => $criteria,
                                              'pagination'=>array(
                                                  'pageSize'=> 100,
                                              ),
											  ));
	}
}