<?php

/**
 * This is the model class for table "yard_signs".
 *
 * The followings are the available columns in table 'yard_signs':
 * @property integer $id
 * @property integer $account_id
 * @property string $internal_sign_id
 * @property integer $type_ma
 * @property integer $status_ma
 * @property integer $transaction_id
 * @property string $photo_url
 * @property string $ivr_extension
 * @property string $notes
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 */
class YardSigns extends StmBaseActiveRecord
{
    const AT_OFFICE_STATUS_ID = 1;
    const IN_TRANSIT_STATUS_ID = 2;
    const INSTALLED_STATUS_ID = 3;
    const MISSING_STATUS_ID = 4;
    const TRASH_STATUS_ID = 5;

    const METAL_TYPE_ID = 1;
    const WOOD_TYPE_ID = 2;
    const OTHER_TYPE_ID = 3;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return YardSigns the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'yard_signs';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, internal_sign_id, added_by', 'required'),
            array('account_id, type_ma, status_ma, transaction_id, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('internal_sign_id', 'length', 'max'=>50),
            array('photo_url', 'length', 'max'=>100),
            array('ivr_extension', 'length', 'max'=>25),
            array('notes, updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, account_id, internal_sign_id, type_ma, status_ma, transaction_id, photo_url, ivr_extension, notes, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'transaction' => array(self::BELONGS_TO, 'Transactions', 'transaction_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'internal_sign_id' => 'Internal Sign ID',
            'type_ma' => 'Type',
            'status_ma' => 'Status',
            'transaction_id' => 'Transaction',
            'photo_url' => 'Photo Url',
            'ivr_extension' => 'Ivr Extension #',
            'notes' => 'Notes',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    public function getStatusList()
    {
        return array(
            self::AT_OFFICE_STATUS_ID => 'At Office',
            self::IN_TRANSIT_STATUS_ID => 'In-transit',
            self::INSTALLED_STATUS_ID => 'Installed',
            self::MISSING_STATUS_ID => 'Missing',
            self::TRASH_STATUS_ID => 'Trash',
        );
    }

    public function getTypesList()
    {
        return array(
            self::METAL_TYPE_ID => 'Metal Frame',
            self::WOOD_TYPE_ID => 'Wood Post',
            self::OTHER_TYPE_ID => 'Other',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('account_id',$this->account_id);
        $criteria->compare('internal_sign_id',$this->internal_sign_id,true);
        $criteria->compare('type_ma',$this->type_ma);
        $criteria->compare('status_ma',$this->status_ma);
        $criteria->compare('transaction_id',$this->transaction_id);
        $criteria->compare('photo_url',$this->photo_url,true);
        $criteria->compare('ivr_extension',$this->ivr_extension,true);
        $criteria->compare('notes',$this->notes,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        $criteria->order = 'ivr_extension';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}