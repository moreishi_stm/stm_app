<?php

	/**
	 * This is the model class for table "task_type_component_lu".
	 *
	 * The followings are the available columns in table 'task_type_component_lu':
	 *
	 * @property integer $id
	 * @property integer $account_id
	 * @property integer $task_type_id
	 * @property integer $component_type_id
	 * @property integer $is_deleted
	 */
	class TaskTypeComponentLu extends StmBaseActiveRecord {
        public $applyAccountScope = false;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TaskTypeComponentLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'task_type_component_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, task_type_id, component_type_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, task_type_id, component_type_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'taskType' => array(
					self::BELONGS_TO,
					'TaskTypes',
					'task_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'task_type_id' => 'Task Type',
				'component_type_id' => 'Component Type',
				'is_deleted' => 'Is Deleted',
			);
		}

		public static function getTaskTypes($componentTypeId) {
			$Criteria = new CDbCriteria;
			$Criteria->select = 'id';
			$Criteria->addCondition('account_id=:account_id AND component_type_id=:component_type_id');
			$Criteria->params = array(
				':account_id' => Yii::app()->user->accountId,
				':component_type_id' => $componentTypeId,
			);
            $Criteria->order = 'name ASC';

			return TaskTypeComponentLu::model()->with('taskType')->findAll($Criteria);
		}

        public static function getDropDownListByComponentTypeId($componentTypeId, $actionPlanItem=false) {
            $list = CHtml::listData(TaskTypeComponentLu::getTaskTypes($componentTypeId), 'taskType.id', 'taskType.name');
            if($actionPlanItem) {
                $list[TaskTypes::AUTO_EMAIL_DRIP] = TaskTypes::model()->findByPk(TaskTypes::AUTO_EMAIL_DRIP)->name;
//                $list[TaskTypes::LETTER] = TaskTypes::model()->findByPk(TaskTypes::LETTER)->name;
            }
            asort($list);
            return $list;
        }

        /**
		 * getDropDownLists builds html for dropdownlist for each componentType, to be used in dialog for dynamic load
		 *
		 * @return mixed array of html code for <options>
		 */
		public static function getDropDownLists() {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'account_id=:account_id';
			$Criteria->order = 'component_type_id ASC, id ASC';
			$Criteria->params = array(
				':account_id' => Yii::app()->user->accountId,
			);
			$result = TaskTypeComponentLu::model()->findAll($Criteria);
			$dropDownLists = array();
			foreach ($result as $key => $model) {
				$dropDownLists[$model->component_type_id] .= '<option value=\"' . $model->taskType->id . '\">' . $model->taskType->name . '</option>';
			}

			return $dropDownLists;
		}


		public function byComponentType($componentType) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'component_type_id=:component_type_id',
					'params' => array(':component_type_id' => $componentType),
				)
			);

			return $this;
		}

        public function byTaskType($taskType) {

            $this->getDbCriteria()->mergeWith(array(
                                              'condition' => 'task_type_id=:task_type_id',
                                              'params' => array(':task_type_id' => $taskType),
                                              )
            );

            return $this;
        }

        // Provide access to the task types
		public function __get($name) {

			if (isset($this->taskType->$name)) {
				return $this->taskType->$name;
			}

			return parent::__get($name);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('task_type_id', $this->task_type_id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
