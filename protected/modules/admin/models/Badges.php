<?php

	/**
	 * This is the model class for table "badges".
	 *
	 * The followings are the available columns in table 'badges':
	 *
	 * @property integer             $id
	 * @property string              $name
	 * @property string              $description
	 * @property string              $image_filename
	 * @property integer             $updated_by
	 * @property string              $updated
	 * @property integer             $added_by
	 * @property string              $added
	 *
	 * The followings are the available model relations:
	 * @property BadgeAwarded[]      $badgeAwardeds
	 * @property BadgeRequirements[] $badgeRequirements
	 * @property Contacts            $addedBy
	 * @property Contacts            $updatedBy
	 */
	class Badges extends StmBaseActiveRecord {

		const THANK_YOU = 1;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Badges the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'badges';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'name, image_filename, updated_by, updated, added_by, added',
					'required'
				),
				array(
					'updated_by, added_by',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'description, image_filename',
					'length',
					'max' => 127
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, description, image_filename, updated_by, updated, added_by, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'badgeAwardeds' => array(
					self::HAS_MANY,
					'BadgeAwarded',
					'badge_id'
				),
				'badgeRequirements' => array(
					self::HAS_MANY,
					'BadgeRequirements',
					'badge_id'
				),
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
				'description' => 'Description',
				'image_filename' => 'Image Filename',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'added_by' => 'Added By',
				'added' => 'Added',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('image_filename', $this->image_filename, true);
			$criteria->compare('updated_by', $this->updated_by);
			$criteria->compare('updated', $this->updated, true);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}