<?php
/**
 * This is the model class for table "lead_call_sessions".
 *
 * The followings are the available columns in table 'lead_call_sessions':
 * @property string $id
 * @property integer $lead_route_id
 * @property string $component_id
 * @property string $component_type_id
 * @property string $b_leg_uuid
 * @property string $source_phone
 * @property string $cascading_data
 * @property string $added
 * @property string $updated
 * @property string $recording_id
 * @property string $recording_call_uuid
 * @property string $recording_url
 * @property string $recording_duration
 * @property string $recording_duration_ms
 * @property string $recording_start_ms
 * @property string $recording_end_ms
 * @property string $recording_added
 *
 * The followings are the available model relations:
 * @property LeadCallSessionPhones[] $leadCallSessionPhones
 * @property LeadRoutes $leadRoute
 */
class LeadCallSessions extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeadCallSessions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lead_call_sessions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lead_route_id, component_id, component_type_id, source_phone, added', 'required'),
            array('lead_route_id, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms', 'numerical', 'integerOnly'=>true),
            array('component_id, component_type_id', 'length', 'max'=>10),
            array('b_leg_uuid, recording_id, recording_call_uuid', 'length', 'max'=>36),
            array('source_phone', 'length', 'max'=>32),
            array('recording_url', 'length', 'max'=>100),
            array('cascading_data, updated, recording_added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, lead_route_id, component_id, component_type_id, b_leg_uuid, source_phone, cascading_data, added, updated', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'leadCallSessionPhones' => array(self::HAS_MANY, 'LeadCallSessionPhones', 'lead_call_session_id'),
            'leadRoute' => array(self::BELONGS_TO, 'LeadRoutes', 'lead_route_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'lead_route_id' => 'Lead Route',
            'component_id' => 'Component',
            'component_type_id' => 'Component Type',
            'b_leg_uuid' => 'B Leg Uuid',
            'source_phone' => 'Source Phone',
            'added' => 'Added',
            'updated' => 'Updated',
        );
    }

    protected function beforeSave()
    {
        $this->updated = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('lead_route_id',$this->lead_route_id);
        $criteria->compare('component_id',$this->component_id,true);
        $criteria->compare('component_type_id',$this->component_type_id,true);
        $criteria->compare('b_leg_uuid',$this->b_leg_uuid,true);
        $criteria->compare('source_phone',$this->source_phone,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}