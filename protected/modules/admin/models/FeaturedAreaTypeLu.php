<?php

	/**
	 * This is the model class for table "featured_area_type_lu".
	 *
	 * The followings are the available columns in table 'featured_area_type_lu':
	 *
	 * @property integer           $featured_area_type_id
	 * @property integer           $featured_area_id
	 *
	 * The followings are the available model relations:
	 * @property FeaturedAreas     $featuredArea
	 * @property FeaturedAreaTypes $featuredAreaType
	 */
	class FeaturedAreaTypeLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FeaturedAreaTypeLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'featured_area_type_lu';
		}

		public function primaryKey() {
			return array(
				'featured_area_id',
				'featured_area_type_id'
			);
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'featured_area_type_id, featured_area_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'featured_area_type_id, featured_area_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'featuredArea' => array(
					self::BELONGS_TO,
					'FeaturedAreas',
					'featured_area_id'
				),
				'type' => array(
					self::BELONGS_TO,
					'FeaturedAreaTypes',
					'featured_area_type_id'
				),
			);
		}
	}