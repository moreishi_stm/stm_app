<?php

/**
 * This is the model class for table "telephony_phone_tags".
 *
 * The followings are the available columns in table 'telephony_phone_tags':
 * @property integer $id
 * @property integer $account_id
 * @property string $name
 * @property string $description
 * @property integer $is_priority
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property TelephonyPhoneTagLu[] $telephonyPhoneTagLus
 */
class TelephonyPhoneTags extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TelephonyPhoneTags the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'telephony_phone_tags';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, name', 'required'),
            array('name', 'unique'),
            array('account_id, is_priority, is_deleted', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>50),
            array('description', 'length', 'max'=>250),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, is_priority, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
            'telephonyPhones' => array(self::MANY_MANY, 'StmHq.telephonyPhones', 'telephony_phone_tag_lu(telephony_phone_tag_id, telephony_phone_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'is_priority' => 'Is Priority',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('is_priority',$this->is_priority);
        $criteria->compare('is_deleted',$this->is_deleted);
        $criteria->order = 'name';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}