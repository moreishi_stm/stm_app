<?php

	/**
	 * This is the model class for table "company_types".
	 *
	 * The followings are the available columns in table 'company_types':
	 *
	 * @property integer     $id
	 * @property integer     $account_id
	 * @property string      $type
	 * @property integer     $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Companies[] $companies
	 */
	class CompanyTypes extends StmBaseActiveRecord {

		/**
		 * "Global" Company Types.
		 */

		const LENDER = 6;
		const TITLE_COMPANY = 8;
		const HOME_INSPECTOR = 9;
		const WDO_INSPECTOR = 10;
		const GENERAL_CONTRACTOR = 11;
		const INSURANCE = 12;
		const POOL_INSPECTOR = 13;
		const ELECTRICIAN = 14;
		const PLUMBER = 15;
		const PAINTER = 16;
		const ROOFER = 17;
		const MOVER = 18;
		const REAL_ESTATE_BROKERAGES = 19;

        const CORE_ID_MAX=999;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return CompanyTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'company_types';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, name, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'companies' => array(
					self::HAS_MANY,
					'Companies',
					'company_type_id'
				),
				'companyTypeLus' => array(
					self::HAS_MANY,
					'CompanyTypeLu',
					'company_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'name' => 'Name',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=> 100,
                ),
			));
		}

		/**
		 * getShortName()
		 * Returns a shortned version of the company type
		 *
		 * @author Chris Willard
		 * @since  3/13/2013
		 */
		public function getShortName() {

			if (!$this->name) {
				return null;
			}

			$shortName = '';

			$nameParts = explode(' ', $this->name);
			foreach ($nameParts as $namePart) {

				if (strtolower($namePart) != 'inspector') {
					$shortName .= $namePart;
				}
			}

			return $shortName;
		}

		/**
		 * Utility function for returning the relevant lender company type identifiers.
		 *
		 * @return array a collection of lender company type ids.
		 */
		public static function lenders() {
			return array(self::LENDER);
		}

		/**
		 * Utility function for returning the relevant title company company type identifiers.
		 *
		 * @return array a collection of title company company type ids.
		 */
		public static function titleCompanies() {
			return array(self::TITLE_COMPANY);
		}

		/**
		 * Utility function for returning the relevant title company company type identifiers.
		 *
		 * @return array a collection of title company company type ids.
		 */
		public static function realEstateBrokerages() {
			return array(self::REAL_ESTATE_BROKERAGES);
		}

		/**
		 * Utility function for returning the relevant inspector company type identifiers.
		 *
		 * @return array a collection of inspector company type ids.
		 */
		public static function inspectors() {
			return array(
				self::HOME_INSPECTOR,
				self::POOL_INSPECTOR,
				self::WDO_INSPECTOR
			);
		}
	}
