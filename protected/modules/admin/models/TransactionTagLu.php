<?php

/**
 * This is the model class for table "transaction_tag_lu".
 *
 * The followings are the available columns in table 'transaction_tag_lu':
 * @property integer $transaction_id
 * @property integer $transaction_tag_id
 *
 * The followings are the available model relations:
 * @property TransactionTags $transactionTag
 * @property Transactions $transaction
 */
class TransactionTagLu extends StmBaseActiveRecord
{
    public $idCollection;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TransactionTagLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'transaction_tag_lu';
    }


    public function getPrimaryKey() {
        return array(
            'transaction_id',
            'transaction_tag_id'
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('transaction_id, transaction_tag_id', 'numerical', 'integerOnly'=>true),
            array('transaction_id', 'validateUnique',),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCollection', 'safe'),
            array('transaction_id, transaction_tag_id, idCollection', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'transactionTag' => array(self::BELONGS_TO, 'TransactionTags', 'transaction_tag_id'),
            'transaction' => array(self::BELONGS_TO, 'Transactions', 'transaction_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'transaction_id' => 'Transaction',
            'transaction_tag_id' => 'Transaction Tag',
        );
    }


    public function validateUnique($attribute, $params)
    {

        if($this->isNewRecord) {
            $criteria = new CDbCriteria();
            $criteria->compare('transaction_id', $this->transaction_id);
            $criteria->compare('transaction_tag_id', $this->transaction_tag_id);
        }
        if ($this->count($criteria)) {
            $this->addError($attribute, 'This tag already exists for the transaction.');
        }
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('transaction_id',$this->transaction_id);
        $criteria->compare('transaction_tag_id',$this->transaction_tag_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}