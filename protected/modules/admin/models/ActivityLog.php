<?php

/**
 * This is the model class for table "activity_log".
 *
 * The followings are the available columns in table 'activity_log':
 *
 * @property integer          $id
 * @property integer          $account_id
 * @property integer          $component_type_id
 * @property integer          $component_id
 * @property integer          $task_type_id
 * @property string           $activity_date
 * @property string           $note
 * @property string           $private_note
 * @property string           $url
 * @property integer          $lead_gen_type_ma
 * @property integer          $is_spoke_to
 * @property integer          $asked_for_referral
 * @property integer          $is_public
 * @property integer          $added_by
 * @property string           $added
 * @property integer          $is_action_plan
 * @property integer          $task_id
 * @property integer          $is_deleted
 *
 * The followings are the available model relations:
 * @property Contacts         $addedBy
 * @property Accounts         $account
 * @property TransactionTypes $transactionType
 */
class ActivityLog extends StmBaseActiveRecord
{

    public $lastActivityDateSearch; //@todo: may not need, for last activity search
    public $notifyLender;
//    public $notifyCompletion;
    public $notify;

    public $addFollowupTask;
    public $followupTask;

    public $referrerUrl;

    const ACTIVITY_TYPE_INITIAL_CONSULT_ID = 8;

    const LEAD_GEN_NONE_ID = 0;
    const LEAD_GEN_NEW_ID = 1;
    const LEAD_GEN_FOLLOW_UP_ID = 2;
    const LEAD_GEN_YES_ID = 3; // this means the system figures out if it's a 1 or 2 at save

    const DEFAULT_NO_ACTIVITY_DAYS = '-30 days';

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return ActivityLog the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'activity_log';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'component_type_id, component_id, activity_date, lead_gen_type_ma, task_type_id, note',
                'required'
            ),
            array(
                'account_id, component_type_id, component_id, task_type_id, lead_gen_type_ma, is_spoke_to, asked_for_referral, is_public, added_by, is_action_plan, task_id, call_id, is_deleted, addFollowupTask',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'note, private_note',
                'length',
                'max' => 16777215
            ),

            array('notifyLender', 'validateNotifyLender'),

            array('addFollowupTask', 'validateFollowupTask'),

            array('activity_date', 'validateActivityDate', 'on' => 'dialer'),

            array(
                'activity_date, url, added, lastActivityDateSearch, notifyLender, notify, notifyCompletion, followupTask',
                'safe'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, account_id, component_type_id, component_id, task_type_id, activity_date, note, private_note, url, lead_gen_type_ma, is_spoke_to, asked_for_referral, is_public, added_by, added, is_action_plan, task_id, call_id, is_deleted, lastActivityDateSearch, notifyLender, notify, notifyCompletion, addFollowupTask, followupTask',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy'        => array(
                self::BELONGS_TO,
                'Contacts',
                'added_by',
                'alias' => 't'
            ),
            'componentType'  => array(
                self::BELONGS_TO,
                'ComponentTypes',
                'component_type_id',
                //					'alias' => 't'
            ),
            // 'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
            'taskType'       => array(
                self::BELONGS_TO,
                'TaskTypes',
                'task_type_id',
            ),
            'tasks'       => array(
                self::BELONGS_TO,
                'Tasks',
                'component_id',
            ),
            'emailMessageLu' => array(
                self::HAS_MANY,
                'ActivityLogEmailMessageLu',
                'activity_log_id'
            ),
            'task'        => array(
                self::BELONGS_TO,
                'Tasks',
                'task_id'
            ),
            'call'        => array(
                self::BELONGS_TO,
                'Calls',
                'call_id'
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                => 'ID',
            'account_id'        => 'Account',
            'component_type_id' => 'Transaction Type',
            'component_id'      => 'Component Id',
            'task_type_id'      => 'Activity Type',
            'activity_date'     => 'Date',
            'note'              => 'Notes',
            'url'               => 'URL',
            'lead_gen_type_ma'  => 'Is Lead Gen',
            'is_spoke_to'       => 'Spoke to Contact',
            'is_public'         => '',
            'added_by'          => 'Added By',
            'added'             => 'Added',
            'is_action_plan'    => 'Is Action Plan',
            'is_deleted'        => 'Is Deleted',
        );
    }

    /**
     * @return array behaviors for model attributes.
     */
    public function behaviors()
    {
        return array(
            'modelAttribute'     => array(
                'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
            ),
            'CTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'added',
                'updateAttribute' => null,
            ),
        );
    }

    public function scopes()
    {
        return array(
            'activityDateDesc' => array(
                'order' => 'activity_date DESC',
            ),
        );
    }

    public function getLeadGenTypes()
    {
        return array(
            self::LEAD_GEN_NONE_ID      => 'No',
            self::LEAD_GEN_NEW_ID       => 'Yes, New Contact',
            self::LEAD_GEN_FOLLOW_UP_ID => 'Yes, Follow-up'
        );
    }

    public function byComponentType($componentTypeId)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'component_type_id=:component_type_id',
                'params'    => array(':component_type_id' => $componentTypeId),
            )
        );

        return $this;
    }

    public function byExcludeTaskTypes($taskTypeIds)
    {
        $taskTypeIds = (array) $taskTypeIds;

        if(!empty($taskTypeIds)) {
            $criteria = new CDbCriteria();
            $criteria->addNotInCondition('task_type_id', $taskTypeIds);
            $this->getDbCriteria()->mergeWith($criteria);
        }

        return $this;
    }

//    public function byIncludeComponentTupleTaskType($componentTuple, $taskTypeId)
//    {
//        $this->getDbCriteria()->mergeWith(
//            array(
//                'condition' => 'component_type_id=:component_type_id AND component_id=:component_id AND task_type_id=:task_type_id',
//                'params'    => array(':component_type_id' => $componentTuple['componentTypeId'], ':component_id'=>$componentTuple['componentId'],':task_type_id'=>$taskTypeId),
//            ),
//            $operator='OR'
//        );
//
//        return $this;
//    }

    public function byComponentId($componentId)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'component_id=:component_id',
                'params'    => array(':component_id' => $componentId),
            )
        );

        return $this;
    }

    public function byTaskType($taskTypeId)
    {
        if($taskTypeId) {
            $this->getDbCriteria()->mergeWith(
                array(
                    'condition' => $this->getTableAlias().'.task_type_id=:task_type_id',
                    'params'    => array(':task_type_id' => $taskTypeId),
                )
            );
        }

        return $this;
    }

    public function byAccount($accountId)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'account_id=:account_id',
                'params'    => array(':account_id' => $accountId),
            )
        );

        return $this;
    }

    public function bySpokeTo($spokeTo = 1)
    {

        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 't.is_spoke_to in( :is_spoke_to )',
                'params'    => array(':is_spoke_to' => $spokeTo),
            )
        );

        return $this;
    }

    public function byIsPublic($isPublic = 1)
    {

        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 't.is_public in( :is_public )',
                'params'    => array(':is_public' => $isPublic),
            )
        );

        return $this;
    }

    public function byTaskAssignedBy($contactId, $together=true)
    {
        if($contactId) {
            $this->getDbCriteria()->mergeWith(
                array(
                    'condition' => 'tasks.assigned_by_id=:assigned_by_id',
                    'params'    => array(':assigned_by_id' => $contactId),
                    'with'      => array('tasks'),
                    'together'  => $together,
                )
            );
        }

        return $this;
    }

    public function byAddedBy($contactId, $opts = null)
    {
        $opts = Yii::app()->format->formatDateRangeArray($opts);
        if (is_array($contactId)) {
            $this->getDbCriteria()->addInCondition('t.added_by', $contactId);
        } else {
            $this->getDbCriteria()->mergeWith(
                array(
                    'condition' => 't.added_by=:contact_id',
                    'params'    => array(
                        ':contact_id' => $contactId,
                    ),
                )
            );
        }

        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 't.activity_date >= :added_from AND t.activity_date <= :added_to',
                'params'    => array(
                    ':added_from' => $opts['from_date'],
                    ':added_to'   => $opts['to_date'] . ' 23:59:59'
                ),
            )
        );

        return $this;
    }

    /**
     * getContact
     *
     * @return model Contact
     */
    public function getContact()
    {
        $Contact = $this->componentType->getContact($this->component_id);

        return $Contact;
    }

    public function getOriginalTaskComponentTypeId()
    {
        if($this->component_type_id==ComponentTypes::TASKS) {
            if($model = Tasks::model()->findByPk($this->component_id)) {
                $componentTypeId = $model->component_type_id;
            }
        }
        return $componentTypeId;
    }

    public function getOriginalComponentType()
    {
        switch ($this->component_type_id) {
            case ComponentTypes::CONTACTS:
            case ComponentTypes::CLOSINGS:
            case ComponentTypes::BUYERS:
            case ComponentTypes::SELLERS:
            case ComponentTypes::LISTINGS:
                $ComponentType = $this->componentType;
                break;
            case ComponentTypes::ACTION_PLANS_ITEMS:
                $model = ActionPlanItems::model()->findByPk($this->component_id);
                $ComponentType = $model->componentType;
                break;

            case ComponentTypes::RECRUITS:
                $model = Recruits::model()->findByPk($this->component_id);
                $ComponentType = $model->componentType;
                break;

            case ComponentTypes::TASKS:
                $model = Tasks::model()->findByPk($this->component_id);
                $ComponentType = $model->componentType;
                break;
        }
        return $ComponentType;
    }

    public function getOriginalComponentId()
    {
        switch ($this->component_type_id) {
            case ComponentTypes::CONTACTS:
            case ComponentTypes::CLOSINGS:
            case ComponentTypes::BUYERS:
            case ComponentTypes::SELLERS:
            case ComponentTypes::LISTINGS:
            case ComponentTypes::RECRUITS:
                $ComponentId = $this->component_id;
                break;
            case ComponentTypes::ACTION_PLANS_ITEMS:
                $model = ActionPlanItems::model()->findByPk($this->component_id);
                $ComponentId = $model->component_id;
                break;
            case ComponentTypes::TASKS:
                $model = Tasks::model()->findByPk($this->component_id);
                $ComponentId = $model->component_id;
                break;
        }
        return $ComponentId;
    }

    /**
     * Finds out whether the current task type is one that is selectable by a user, a non-auto task type
     *
     * @return boolean
     */
    public function getIsShowActivityLogEditButton()
    {
        if (in_array($this->task_type_id, array(TaskTypes::PHONE, TaskTypes::TODO, TaskTypes::NOTE, TaskTypes::MARKETING, TaskTypes::SHOWING, TaskTypes::SHOWING_FEEDBACK, TaskTypes::WAITING, TaskTypes::SELLER_MARKET_UPDATE, TaskTypes::LIVE_DEMO))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Scope which selects Activity Log entries associated with one or more components.
     *
     * @param $componentTuples
     *
     * @internal param $componentTypeTuples
     * @return ActivityLog
     */
    public function byComponentTypes($componentTuples)
    {
        $numComponentTuples = count($componentTuples);

        if ($numComponentTuples > 0) {
            $criterion = array();

            for ($index = 0; $index < $numComponentTuples; ++$index) {
                $tuple = $componentTuples[$index];

                //Adding "_$index" to the placeholders to simply differentiate the condition
                //clauses when the criteria mergeWith() is called.
                $Criteria = new CDBCriteria();
                $Criteria->condition
                    = "component_type_id = :component_type_id_$index AND component_id = :component_id_$index";
                $Criteria->params = array(
                    ":component_type_id_$index" => $tuple['componentTypeId'],
                    ":component_id_$index"      => $tuple['componentId']
                );

                array_push($criterion, $Criteria);
            }

            $MergedCriterion = $criterion[0];
            $numCriterion = count($criterion);

            if ($numCriterion > 1) {
                for ($index = 1; $index < $numCriterion; ++$index) {
                    $criteria = $criterion[$index];

                    //false here forces the criterion to be OR'd together, this isn't very
                    //intuitive and has been fixed in v1.1.13 of the Yii Framework
                    $MergedCriterion->mergeWith($criteria, false);
                }
            }

            $this->getDbCriteria()->mergeWith($MergedCriterion);
        }

        return $this;
    }

    public function dateRange($opts = null)
    {

        if ($opts == null) {
            $this->getDbCriteria()->mergeWith(
                array(
                    'condition' => 'activity_date <= :date',
                    'params'    => array(':date' => date('Y-m-d')),
                )
            );
        } else {
            if ($opts['to_date']) {
                $this->getDbCriteria()->mergeWith(
                    array(
                        'condition' => 'activity_date < :to_date',
                        'params'    => array(':to_date' => date('Y-m-d', strtotime($opts['to_date'] . ' +1 day')))
                    )
                );
            }

            if ($opts['from_date']) {
                $this->getDbCriteria()->mergeWith(
                    array(
                        'condition' => 'activity_date >= :from_date',
                        'params'    => array(':from_date' => $opts['from_date']),
                    )
                );
            }
        }

        return $this;
    }

    public static function countLeadGenDials($opts)
    {
        $opts['default_preset'] = 'this_month';
        $opts = Yii::app()->format->formatDateRangeArray($opts);

        $Criteria = new CDbCriteria;
        $Criteria->condition = 'task_type_id=:taskType AND added_by=:added_by';
        $Criteria->params = array(
            ':taskType' => TaskTypes::PHONE,
            ':added_by' => Yii::app()->user->id
        );
        $Criteria->addInCondition(
            'lead_gen_type_ma', array(
                ActivityLog::LEAD_GEN_NEW_ID,
                ActivityLog::LEAD_GEN_FOLLOW_UP_ID
            )
        );

        return ActivityLog::model()->dateRange($opts)->count($Criteria);
    }

    public static function countLeadGenContacts($opts = null)
    {
        $opts['default_preset'] = 'this_month';
        $opts = Yii::app()->format->formatDateRangeArray($opts);

        $Criteria = new CDbCriteria;
        $Criteria->condition = 'is_spoke_to=:is_spoke_to AND added_by=:added_by';
        $Criteria->params = array(
            ':is_spoke_to' => 1,
            ':added_by'    => Yii::app()->user->id
        );
        $Criteria->addInCondition(
            'lead_gen_type_ma', array(
                ActivityLog::LEAD_GEN_NEW_ID,
                ActivityLog::LEAD_GEN_FOLLOW_UP_ID
            )
        );

        return ActivityLog::model()->dateRange($opts)->count($Criteria);
    }

    public static function hasNoActivity($componentTypeId, $componentId, $sinceDate = null)
    {
        if (!empty($sinceDate)) {
            // @todo: if($days = Yii::app()->user->last_activity_timeframe... if no setting for last activity timeframe for leads since last_login) { $days .= ' days'} else
            $sinceDate = date('Y-m-d H:i:s', strtotime(self::DEFAULT_NO_ACTIVITY_DAYS));
        }

        $Criteria = new CDbCriteria;
        $Criteria->condition
            = 'component_type_id=:component_type_id AND component_id=:component_id AND task_type_id <= :task_type_id AND added>=:added';
        $Criteria->params = array(
            ':component_type_id' => $componentTypeId,
            ':component_id'      => $componentId,
            ':task_type_id'      => TaskTypes::NOTE,
            ':added'             => $sinceDate,
        );

        // $Criteria = new CDbCriteria;
        // $Criteria->condition = 'component_type_id=:component_type_id AND component_id=:component_id';
        // $Criteria->params = array(
        // 	':component_type_id' => $componentTypeId,
        // 	':component_id' => $componentId
        // );

        // if (!empty($sinceDate)) {
        // 	$Criteria->condition .= ' AND added>=:added';
        // 	$Criteria->params = CMap::mergeArray($Criteria->params, array(':added'=>$sinceDate));
        // }
        return (ActivityLog::model()->count($Criteria)) ? false : true;
    }

    protected function beforeValidate()
    {
        if(empty($this->account_id)) {
            $this->account_id = Yii::app()->user->accountId;
        }

        if ($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
        }

        if (!$this->call_id) {
            $this->call_id = null;
        }

        return parent::beforeValidate();
    }


    protected function beforeSave()
    {
        if (parent::beforeSave()) {

            if (!($this->activity_date instanceof CDbExpression)) {
                // @todo: temp solution because the datetime picker treats midnight as "00" vs. 12, it breaks strtotime()
                $this->activity_date = str_replace(' 00:', ' 12:', $this->activity_date);
                // Date Conversions
                $this->activity_date = date(StmFormatter::MYSQL_DATETIME_FORMAT, strtotime($this->activity_date));
            }

            if (!$this->added_by && $this->isNewRecord) {
                //Attach User who Created Activity
                $this->added_by = Yii::app()->user->id;
            }

            if (!$this->account_id) {
                //Attach Account ID to Activity
                $this->account_id = Yii::app()->user->accountId;
            }

            if($this->notifyLender) {
                if(strpos($this->note, '(Lender Notified)')==false) {
                    $this->note .= PHP_EOL.PHP_EOL.'(Lender Notified)';
                }
            }

            // they just marked yes and does not know if it's a new or follow-up contact. We will figure this out here.
            if($this->lead_gen_type_ma == self::LEAD_GEN_YES_ID) {

                $compareActivityDate = ($this->activity_date instanceof CDbExpression) ? Yii::app()->db->createCommand('SELECT NOW()')->queryScalar() : $this->activity_date;

                // find out if spoke to before
                $isSpokeTo = Yii::app()->db->createCommand()
                    ->select('count(*)')
                    ->from('activity_log')
                    ->where('component_type_id=:component_type_id AND component_id=:component_id', array(':component_type_id' => $this->component_type_id,':component_id'=>$this->component_id))
                    ->andWhere('is_spoke_to=1')
                    ->andWhere('activity_date < :activity_date', array(':activity_date' => $compareActivityDate))
                    ->queryScalar();
                $this->lead_gen_type_ma = ($isSpokeTo) ? self::LEAD_GEN_FOLLOW_UP_ID : self::LEAD_GEN_NEW_ID ;
            }

            if (!$this->isNewRecord) {
                $HistoryLog = new HistoryLog;

                $ActivityLog = new ActivityLog;
                $ActivityLog = $ActivityLog->findByPk($this->id);
                $oldValue = CJSON::encode($ActivityLog->attributes);
                $newValue = CJSON::encode($this->attributes);
                $HistoryLog->setAttributes(
                    array(
                        'model_name'     => get_class($this),
                        'primary_key'    => $this->id,
                        'action_type_ma' => HistoryLog::ACTION_TYPE_UPDATE,
                        'old_value'      => $oldValue,
                        'new_value'      => $newValue,
                    )
                );
                if (!$HistoryLog->save()) {
                    $this->addError('id', 'History Log did not save properly');
                }
            }

            return true;
        } else {
            return false;
        }
    }

    protected function afterSave()
    {
        if($this->notifyLender) {
            $this->emailLender();
        }

        if($this->notify && $this->is_public) {
            $this->_emailNotify();
        }

        parent::afterSave();
    }

    public function validateNotifyLender($attribute, $params)
    {
        if ($this->notifyLender == 1)
        {
            $hasAssignment = Assignments::model()->findByAttributes(array('component_type_id'=>$this->component_type_id, 'component_id'=>$this->component_id, 'assignment_type_id'=> AssignmentTypes::LOAN_OFFICER));
            if(!$hasAssignment) {
                $this->addError('notifyLender', 'Lender Assignment is missing. Please assign a lender and try again.');
            }
        }
    }

    public function validateActivityDate($attribute, $params)
    {
        $this->activity_date = new CDbExpression('NOW()');
    }

    public function validateFollowupTask($attribute, $params)
    {
        if(!$this->addFollowupTask) {
            return;
        }

        $followupTask = new Tasks;
        $followupTask->attributes = $this->followupTask;

        if(!$followupTask->validate()) {
            $errors = CJSON::decode(CActiveForm::validate($followupTask));

            // go through each error and map error to proper Task property inside this model
            foreach($errors as $i => $error) {
                $this->addError('followupTask['.str_replace('Tasks_','',$i).']', $error[0]);
            }
        }
    }

    protected function emailLender() {
        $message = new YiiMailMessage;
        $logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Our Logo">';

        if($lender = Assignments::model()->findByAttributes(array('component_type_id'=>$this->component_type_id, 'component_id'=>$this->component_id, 'assignment_type_id'=>AssignmentTypes::LOAN_OFFICER))) {
            $toEmail[$lender->contact->primaryEmail] = $lender->contact->fullName;
        }
        else {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Lender assignment is missing. Cannot send them email.', CLogger::LEVEL_ERROR); //@todo: validate for activity log dialog *********
            return;
        }

        $content = '<span style="font-size:16px;">Agent alerted you on a '.$this->componentType->getSingularName().' Activity';
        $content .= '<br /><b>Please review and touch base as needed.</b>';
                $content .= '<br /><br />Activity Details: '.$this->note;
        //        if($Transaction->component_type_id == ComponentTypes::SELLERS) {
        //            $content .= '<br />Address: '.Yii::app()->format->formatAddress($Transaction->address, array('lineBreak'=>false));
        //        }
        $url = 'http://www.'.Yii::app()->user->domain->name.'/admin/'.$this->componentType->name.'/'.$this->component_id;
        $content .= '<br /><br /><a href="'.$url.'">Click here to view '.$this->componentType->getSingularName().' details</a>';
        $content .= '</span><br />'.$logoImage;

        $message->view = 'plain';
        $message->setBody(array(
                'toEmail' => $toEmail,
                'content' => $content,
            ), 'text/html'
        );
        $message->setSubject('Agent Alerted you on an Activity.');

        $toEmail = (YII_DEBUG)? array(StmMailMessage::DEBUG_EMAIL_ADDRESS) : $toEmail ;
        foreach($toEmail as $email => $name) {
            $message->addTo($email, $name);
        }

        $message->addFrom(Yii::app()->user->contact->getPrimaryEmail(), 'Agent Activity Alert');
        if(!YII_DEBUG) {
            StmZendMail::sendYiiMailMessage($message);
//            Yii::app()->mail->send($message);
        }
        return;
    }

    protected function _emailNotify()
    {
        $subject = 'Quick Update...';

        switch($this->component_type_id) {

            case ComponentTypes::RECRUITS:
                $subject = 'Update for Recruit Referral - ' . $this->contact->fullName;
                break;
        }

//        $message->addFrom(Yii::app()->user->contact->getPrimaryEmail(), ); @todo: should this be a encoded email so they can reply to it?
//        if(!YII_DEBUG) {

            if($contacts = Contacts::model()->findAllByPk($this->notify)) {

                foreach($contacts as $contact) {

                    $to = array($contact->getPrimaryEmail() => $contact->fullName);

                    $body = Yii::app()->controller->renderFile(YiiBase::getPathOfAlias('admin_module.views.emails.activityLogNotify').'.php', array(
                            'toContact' => $contact,
                            'fromContact' => $this->addedBy,
                            'subject' => $subject,
                            'note' => $this->note,
                            'url' => 'http://www.'.Yii::app()->user->domain->name.'/'.Yii::app()->controller->module->name.'/'.$this->componentType->name.'/'.$this->component_id,
                            'componentModel' => ComponentTypes::getComponentModel($this->component_type_id, $this->component_id),
                        ), $returnContent=true);

                    $from = "do-not-reply@seizethemarket.net";
//                    StmZendMail::easyMail($from, $to, $subject, $body, null, $type='text/html', $awsTransport=true, $ignoreOnDebug=false);
                    StmZendMail::easyMail($from, $to, $subject, $body, null, $type='text/html', $awsTransport=true, $ignoreOnDebug=false, $replyTo=null, $checkBounceUndeliverable=false);
                }
            }
//        }
        return;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('component_type_id', $this->component_type_id);
        $criteria->compare('component_id', $this->component_id);
        $criteria->compare('task_type_id', $this->task_type_id);
        $criteria->compare('activity_date', $this->activity_date, true);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('lead_gen_type_ma', $this->lead_gen_type_ma);
        $criteria->compare('is_spoke_to', $this->is_spoke_to);
        $criteria->compare('asked_for_referral', $this->asked_for_referral);
        $criteria->compare('added_by', $this->added_by);
        $criteria->compare('added', $this->added, true);
        $criteria->compare('is_public', $this->is_public);
        $criteria->compare('is_action_plan', $this->is_action_plan);
        $criteria->compare('call_id', $this->call_id);
        $criteria->compare('is_deleted', $this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

    public static function logActivity($component, $taskType, $note)
    {
        try {
            $ActivityLog = new ActivityLog;

            $ActivityLog->account_id = Yii::app()->user->accountId;
            $ActivityLog->task_type_id = $taskType;
            $ActivityLog->component_type_id = $component->componentType->id;
            $ActivityLog->component_id = $component->id;
            $ActivityLog->note = $note;
            $ActivityLog->added_by = Yii::app()->user->id;

            $dateTime = new DateTime();
            $ActivityLog->activity_date = new CDbExpression('NOW()');

            $ActivityLog->save();
        } catch (Exception $e) {
            Yii::log(
                'Encountered exception saving the following Activity Log Entry: ' . print_r(
                    $ActivityLog->attributes, true
                ) . ' - ' . $e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__
            );
        }
    }
}
