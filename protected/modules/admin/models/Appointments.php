<?php

	/**
	 * This is the model class for table "appointments".
	 *
	 * The followings are the available columns in table 'appointments':
	 *
     * @property integer $id
     * @property integer $component_type_id
     * @property integer $component_id
     * @property integer $set_by_id
     * @property integer $met_by_id
     * @property integer $met_status_ma
     * @property string $met_status_reason
     * @property integer $is_signed
     * @property string $signed_date
     * @property integer $not_signed_reason
     * @property string $not_signed_reason_other
     * @property integer $set_activity_type_id
     * @property string $set_on_datetime
     * @property string $set_for_datetime
     * @property integer $dig_motivation_rating
     * @property integer $location_ma
     * @property string $location_other
     * @property string $google_calendar_id
     * @property string $google_calendar_event_id
     * @property integer $updated_by
     * @property string $updated
     * @property integer $added_by
     * @property string $added
     * @property integer $is_deleted
     *
     * The followings are the available model relations:
     * @property Contacts $addedBy
     * @property ComponentTypes $componentType
     * @property Contacts $metBy
     * @property TaskTypes $setActivityType
     * @property Contacts $setBy
     * @property Contacts $updatedBy
	 */
	class Appointments extends StmBaseActiveRecord {

		public $toDelete;
		public $isEmpty;
        public $googleCalendarData = array();
        protected $_originalModel;

		const MET_STATUS_SET        = 1;
		const MET_STATUS_MET        = 2;
		const MET_STATUS_CANCEL     = 3;
        const MET_STATUS_RESCHEDULE = 4;

		const LOCATION_OFFICE = 1;
		const LOCATION_HOUSE  = 2;
		const LOCATION_OTHER  = 3;
        const LOCATION_VIRTUAL = 4;

		const SIGN_STATUS_NO = 0;
		const SIGN_STATUS_YES = 1;

		const NOT_SIGNED_SELLER_VALUE_TOO_LOW = 1;
		const NOT_SIGNED_SELLER_NOT_SELL = 2;
		const NOT_SIGNED_SELLER_ANOTHER_AGENT = 3;
		const NOT_SIGNED_SELLER_OTHER = 4;

		const NOT_SIGNED_BUYER_NOT_QUALIFY = 1;
		const NOT_SIGNED_BUYER_NOT_BUY = 2;
		const NOT_SIGNED_BUYER_ANOTHER_AGENT = 3;
		const NOT_SIGNED_BUYER_OTHER = 4;

        const NOT_SIGNED_RECRUIT_IN_PROGRESS = 1;
        const NOT_SIGNED_RECRUIT_NOT_FIT = 2;
        const NOT_SIGNED_RECRUIT_NOT_OTHER = 4;

        /**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Appointments the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'appointments';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'component_type_id, component_id, set_by_id, met_by_id, met_status_ma, location_ma, set_for_datetime, set_activity_type_id, set_on_datetime, updated_by, updated, added_by, added',
					'required'
				),
				array(
					'component_type_id, component_id, dig_motivation_rating, set_by_id, met_by_id, met_status_ma, is_signed, not_signed_reason, set_activity_type_id, location_ma, updated_by, added_by, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'location_other, not_signed_reason_other',
					'length',
					'max' => 63
				),
                array(
                    'google_calendar_event_id',
                    'length',
                    'max' => 30
                ),
                array(
                    'met_status_ma',
                    'validateMetStatus'
                ),
                array(
                    'set_for_datetime',
                    'validateMetDate'
                ),
                array(
                    'dig_motivation_rating',
                    'validateDigForMotivation'
                ),
                array(
                    'is_signed',
                    'validateSignedDate'
                ),
                array(
                    'not_signed_reason',
                    'validateNotSignedReason'
                ),
                array(
                    'googleCalendarData',
                    'validateGoogleCalendarData'
                ),
				array(
					'signed_date, set_for_datetime, set_on_datetime, met_status_reason, google_calendar_id, google_calendar_event_id, googleCalendarData, updated, added, toDelete',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, component_type_id, component_id, set_by_id, met_by_id, dig_motivation_rating, met_status_ma, met_status_reason, is_signed, signed_date, not_signed_reason, not_signed_reason_other, set_activity_type_id, set_on_datetime, set_for_datetime, location_ma, location_other, google_calendar_id, google_calendar_event_id, googleCalendarData, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
				'taskType' => array(
					self::BELONGS_TO,
					'TaskTypes',
					'set_activity_type_id'
				),
				'metBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'met_by_id'
				),
				'setBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'set_by_id'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
			);
		}

		/**
		 * @return array behaviors for getTasks
		 */
		public function behaviors() {
			return array(
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'component_type_id' => 'Component Type Id',
				'component_id' => 'Component Id',
				'set_by_id' => 'Appointment Set By',
				'met_by_id' => 'Appointment By',
				'met_status_ma' => 'Appointment Status',
				'is_signed' => 'Signed',
				'not_signed_reason' => 'Not Signed Reason',
				'not_signed_reason_other' => 'Other Not Signed Reason',
				'set_activity_type_id' => 'Appointment Set Method',
				'set_for_datetime' => 'Appointment Set for Date/Time',
				'set_on_datetime' => 'Appointment Set on Date/Time',
				'location_ma' => 'Appointment Location',
				'location_other' => 'Location Other',
				'updated_by' => 'Updated by',
				'updated' => 'Updated',
				'added_by' => 'Added by',
				'added' => 'Added',
				'is_deleted' => 'Deleted',
			);
		}

		public function scopes() {
			return array(
				'byOrForCurrentUser' => array(
					'condition' => 'set_by_id=:set_by_id OR met_by_id=:met_by_id',
					'params' => array(':set_by_id' => Yii::app()->user->contact->id,
									  ':met_by_id' => Yii::app()->user->contact->id
									),
				),
				'byCurrentUser' => array(
					'condition' => 'set_by_id=:set_by_id',
					'params' => array(':set_by_id' => Yii::app()->user->contact->id),
				),
				'forCurrentUser' => array(
					'condition' => 'met_by_id=:met_by_id',
					'params' => array(':met_by_id' => Yii::app()->user->contact->id),
				),
				'signed' => array(
					'condition' => 'is_signed=:is_signed',
					'params' => array(':is_signed' => Appointments::SIGN_STATUS_YES),
				),
				'met' => array(
					'condition' => 'met_status_ma=:met_status_ma',
					'params' => array(':met_status_ma' => Appointments::MET_STATUS_MET),
				),
				'notMet' => array(
					'condition' => 'met_status_ma=:met_status_ma',
					'params' => array(':met_status_ma' => Appointments::MET_STATUS_SET),
				),
				'setOnOrForCurrentMonth' => array(
					'condition' => '(MONTH(set_for_datetime) = MONTH(CURRENT_DATE) OR MONTH(set_on_datetime) = MONTH(CURRENT_DATE)) AND (YEAR(set_for_datetime) = YEAR(CURRENT_DATE) OR YEAR(set_on_datetime) = YEAR(CURRENT_DATE))',
				),
				'setForCurrentMonth' => array(
					'condition' => 'MONTH(set_for_datetime) = MONTH(CURRENT_DATE) AND YEAR(set_for_datetime) = YEAR(CURRENT_DATE)',
				),
				'setOnCurrentMonth' => array(
					'condition' => 'MONTH(set_on_datetime) = MONTH(CURRENT_DATE) AND (YEAR(set_on_datetime) = YEAR(CURRENT_DATE))',
				),
                'setForDateAsc' => array(
                    'order' => 'set_for_datetime ASC',
                ),
                'setForDateDesc' => array(
                    'order' => 'set_for_datetime DESC',
                ),
				'addedDesc' => array(
					'order' => 'added DESC',
				),
				'addedAsc' => array(
					'order' => 'added ASC',
				),
			);
		}

        /**
         * Validate the the cancel or reschedule reason
         *
         * @param  string $attribute The attribute name
         * @param  array  $params    Parameters for the validation
         *
         * @return null
         */
        public function validateMetStatus($attribute, $params)
        {
            //check for set_for date and status, cannot be signed and meet date in future
            if($this->met_status_ma == Appointments::MET_STATUS_MET && date('Y-m-d', strtotime($this->set_for_datetime)) > date('Y-m-d')) {
                $this->addError('met_status_ma', 'Appointment Met Date cannot be in the future if status is Met.');
            }

            if($this->met_status_ma == Appointments::MET_STATUS_SET && date('Y-m-d', strtotime($this->set_for_datetime)) < date('Y-m-d')) {
                $this->addError('met_status_ma', 'Appointment Status needs to be updated.');
            }

            //validateRescheduleCancelReason
            if (($this->met_status_ma == Appointments::MET_STATUS_RESCHEDULE || $this->met_status_ma == Appointments::MET_STATUS_CANCEL) && empty($this->met_status_reason)) {
                $this->addError('met_status_reason', 'Please provide the reason for the Reschedule or Cancel.');
            }
        }

        public function validateMetDate($attribute, $params)
        {
            //check to see it met date is greater than set date
            if(date('Y-m-d H:i:s', strtotime($this->set_for_datetime)) < date('Y-m-d H:i:s', strtotime($this->set_on_datetime))) {
                $this->addError('set_for_datetime', 'Appointment Met Date cannot be less than the Set Date.');
            }
        }

        /**
         * Validate the Dig for Motivation baased on setting
         *
         * @param  string $attribute The attribute name
         * @param  array  $params    Parameters for the validation
         *
         * @return null
         */
        public function validateDigForMotivation($attribute, $params)
        {
            if (!empty(Yii::app()->user->settings->appointments_dig_for_motivation_required) && empty($this->dig_motivation_rating)) {
                $this->addError('dig_motivation_rating', 'Dig for Motivation is required.');
            }
        }

        /**
         * Validate the the cancel or reschedule reason
         *
         * @param  string $attribute The attribute name
         * @param  array  $params    Parameters for the validation
         *
         * @return null
         */
        public function validateSignedDate($attribute, $params)
        {
            if (($this->is_signed == 1 && empty($this->signed_date))) {
                $this->addError('signed_date', 'Enter the signed Date.');
            }

            if (($this->is_signed == 1 && date('Y-m-d', strtotime($this->set_for_datetime)) > date('Y-m-d'))) {
                $this->addError('set_for_datetime', 'Date cannot be in the future if already Signed.');
            }

            if (($this->is_signed == 1 && $this->met_status_ma != Appointments::MET_STATUS_MET)) {
                $this->addError('met_status_ma', 'Must be Met if already Signed.');
            }
        }

        /**
         * Validate the the Not signed reason
         *
         * @param  string $attribute The attribute name
         * @param  array  $params    Parameters for the validation
         *
         * @return null
         */
        public function validateNotSignedReason($attribute, $params)
        {
            if ($this->is_signed == 0 && $this->met_status_ma == self::MET_STATUS_MET && empty($this->not_signed_reason)) {
                $this->addError('not_signed_reason', 'Please provide reason for not signing.');
            }

            if ($this->is_signed == 0 && $this->met_status_ma == self::MET_STATUS_MET && in_array($this->not_signed_reason, array(self::NOT_SIGNED_BUYER_OTHER, self::NOT_SIGNED_SELLER_OTHER)) && empty($this->not_signed_reason_other)) {
                $this->addError('not_signed_reason_other', 'Please provide other reason for not signing.');
            }
        }

        /**
         * Validate Google Calender fields
         *
         * @param  string $attribute The attribute name
         * @param  array  $params    Parameters for the validation
         *
         * @return null
         */
        public function validateGoogleCalendarData($attribute, $params)
        {
            if(empty($this->googleCalendarData) || !$this->googleCalendarData['add_event']) {
                return;
            }

            // check event_name, event_start
            if(!$this->googleCalendarData['event_name']) {
                $this->addError('googleCalendarData[event_name]', 'Enter a Google Calendar Event Name.');
            }
            $this->googleCalendarData['event_start'] = $this->set_for_datetime;
        }

        public function dateRange($opts = null, $field) {

			if ($opts == null) {
				$this->getDbCriteria()->mergeWith(array(
						'condition' => $field . ' <= :date',
						'params' => array(':date' => date('Y-m-d')),
					)
				);
			} else {
				if ($opts['to_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => $field . ' < :to_date',
							'params' => array(':to_date' => date('Y-m-d', strtotime($opts['to_date'] . ' +1 day')))
						)
					);
				}

				if ($opts['from_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => $field . ' >= :from_date',
							'params' => array(':from_date' => $opts['from_date']),
						)
					);
				}
			}

			return $this;
		}

//		public function bySetBy??($id, $componentTypeId) {
//			return $this->find(array(
//							   'condition' => 'component_id=:component_id AND component_type_id=:component_type_id',
//							   'params' => array(
//								   ':component_id' => $id,
//								   ':component_type_id' => $componentTypeId
//							   ),
//							   )
//			);
//		}

        public function bySetByIds($contactIds) {
            if(!empty($contactIds)) {
                if(!is_array($contactIds))
                    $contactIds = array($contactIds);

                $this->getDbCriteria()->addInCondition('set_by_id',$contactIds);
            }

            return $this;
        }

        public function byMetByIds($contactIds) {
            if(!empty($contactIds)) {
                if(!is_array($contactIds))
                    $contactIds = array($contactIds);

                $this->getDbCriteria()->addInCondition('met_by_id',$contactIds);
            }

            return $this;
        }

		public function byAppointmentMetStatus($statusIds) {
			if(!is_array($statusIds))
				$statusIds = array($statusIds);

			$this->getDbCriteria()->addInCondition('met_status_ma',$statusIds);

			return $this;
		}

		public function byComponentTypeIds($componentTypeIds) {
			if(!is_array($componentTypeIds))
				$componentTypeIds = array($componentTypeIds);

			$this->getDbCriteria()->addInCondition('component_type_id',$componentTypeIds);

			return $this;
		}

		public function byComponentTuple($componentTypeId, $componentId) {
			if (isset($componentTypeId) && isset($componentId)) { // @todo: is this necessary? is argument params enough? - CLee
				$Criteria = new CDBCriteria();
				$Criteria->condition = 'component_id = :component_id AND component_type_id = :component_type_id';
				$Criteria->params = array(
					':component_type_id' => $componentTypeId,
					':component_id' => $componentId,
				);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}

		public static function countSetAppointments($opts = null, $componentTypeId = null) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$Criteria = new CDbCriteria;
			if ($componentTypeId) {
				$Criteria->compare('component_type_id', $componentTypeId);
			}

			return Appointments::model()->dateRange($opts, 'set_on_datetime')->count($Criteria);
		}

		public static function countMetAppointments($opts = null, $componentTypeId = null) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$Criteria = new CDbCriteria;
			if ($componentTypeId) {
				$Criteria->compare('component_type_id', $componentTypeId);
			}

			$Criteria->compare('met_status_ma', Appointments::MET_STATUS_MET);

			$Criteria->compare('met_by_id', Yii::app()->user->id);

			return Appointments::model()->dateRange($opts, 'set_for_datetime')->count($Criteria);
		}

		public static function countSignedAppointments($opts = null) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'met_status_ma=:met_status AND is_signed=:is_signed AND met_by_id=:met_by_id';
			$Criteria->params = array(
				':met_status' => Appointments::MET_STATUS_MET,
				':is_signed' => Appointments::SIGN_STATUS_YES,
				':met_by_id' => Yii::app()->user->id
			);

			return Appointments::model()->dateRange($opts, 'set_for_datetime')->count($Criteria);
		}

		public function getMetYesNo() {
			if($this->met_status_ma == Appointments::MET_STATUS_MET)
				return 'Yes';
			elseif($this->met_status_ma == Appointments::MET_STATUS_SET)
				return 'No';
			else
				return 'Not Set or Met';
		}

		public function getIsPastMetDate($date=null) {
			if($date==null) {
				$date = date("Y-m-d");
			}
			$setForDate = Yii::app()->format->formatDate($this->set_for_datetime, StmFormatter::MYSQL_DATE_FORMAT);
			return ($setForDate < $date)? true: false;
		}

        public function printMetStatusIcon() {
        if ($this->met_status_ma==Appointments::MET_STATUS_MET) {
                echo StmHtml::booleanImage(true);

        } elseif ($this->getIsPastMetDate()) {
                if($this->met_status_ma==Appointments::MET_STATUS_SET) {
                    echo '<span style="color:#E20000; font-weight: bold">PAST DUE</span>';
                } else if($this->met_status_ma==Appointments::MET_STATUS_CANCEL) {
                    echo StmHtml::booleanImage(false);
                    echo '<br />Cancelled';
                     if($this->met_status_ma==Appointments::MET_STATUS_CANCEL) { // && empty($this->not_signed_reason)
                         //@todo: need to add cancel reason
//                         echo '<br /><span style="color:#E20000; font-weight: bold">Reason Needed</span>';
                     }
                } else if($this->met_status_ma==Appointments::MET_STATUS_RESCHEDULE) {
                    echo 'To Reschedule';
                }
            } else {
                $setForDate = Yii::app()->format->formatDate($this->set_for_datetime, StmFormatter::MYSQL_DATE_FORMAT);
                $days = Yii::app()->format->formatDays($setForDate, array('isFuture'=>true));
                echo 'In '.$days;
            }
        }


        public function printSignedStatusIcon() {
            if($this->is_signed==Appointments::SIGN_STATUS_YES) {
                echo StmHtml::booleanImage(true).' '.(($this->signed_date) ? '<br><br>'.Yii::app()->format->formatDate($this->signed_date) : '');
            } elseif ($this->met_status_ma==Appointments::MET_STATUS_MET) {
                if($this->is_signed==null) {
                    echo '<span style="color:#E20000; font-weight: bold">DID THEY SIGN?</span>';
                } elseif($this->is_signed==Appointments::SIGN_STATUS_NO) {
                    echo StmHtml::booleanImage(false);
                    if($this->not_signed_reason==null) {
                        echo '<span style="color:#E20000; font-weight: bold">REASON MISSING!</span>';
                    } else {
                        if($this->not_signed_reason == self::NOT_SIGNED_SELLER_OTHER || $this->not_signed_reason == self::NOT_SIGNED_BUYER_OTHER) {
                            if(empty($this->not_signed_reason_other)) {
                                echo '<span style="color:#E20000; font-weight: bold">REASON MISSING!</span>';
                            } else {
                                echo '<br />Reason (Other): '.$this->not_signed_reason_other;
                            }
                        } else {
                            echo '<br />Reason: ';
                            if($this->component_type_id == ComponentTypes::SELLERS) {
                                echo $this->getModelAttribute('getNotSignedSellerReasonTypes', $this->not_signed_reason);
                            } elseif ($this->component_type_id == ComponentTypes::BUYERS) {
                                echo $this->getModelAttribute('getNotSignedBuyerReasonTypes', $this->not_signed_reason);
                            }
                        }

                    }

                }
            } elseif($this->met_status_ma!=Appointments::MET_STATUS_MET) {
                echo '-';
            }
        }
        /**
		 * getMetStatusTypes
		 * Returns the Appointment Met Types
		 */
		public function getMetStatusTypes() {
			return array(
				self::MET_STATUS_SET        => 'Set (not Met)',
				self::MET_STATUS_MET        => 'Met',
                self::MET_STATUS_RESCHEDULE => 'To Reschedule',
				self::MET_STATUS_CANCEL     => 'Cancel',
			);
		}

		/**
		 * getMetStatusTypes
		 * Returns the Appointment Met Types
		 */
		public function getLocationTypes() {
			return array(
				self::LOCATION_OFFICE => 'Office',
				self::LOCATION_HOUSE => 'House',
                self::LOCATION_VIRTUAL => 'Virtual',
				self::LOCATION_OTHER => 'Other',
			);
		}


        public function getNotSignedReasonTypes() {
            if($this->component_type_id == ComponentTypes::SELLERS) {
                $list = $this->getNotSignedSellerReasonTypes();
            } elseif ($this->component_type_id == ComponentTypes::BUYERS) {
                $list = $this->getNotSignedBuyerReasonTypes();
            } elseif ($this->component_type_id == ComponentTypes::RECRUITS) {
                $list = $this->getNotSignedRecruitReasonTypes();
            }
            return $list;
        }

        /**
		 * getNotSignedSellerReasonTypes
		 * Returns the Reasons for Not Signing for Sellers
		 */
		public function getNotSignedSellerReasonTypes() {
			return array(
				self::NOT_SIGNED_SELLER_VALUE_TOO_LOW => 'Value Too Low',
				self::NOT_SIGNED_SELLER_NOT_SELL => 'Decided Not to Sell',
				self::NOT_SIGNED_SELLER_ANOTHER_AGENT => 'Went with Another Agent',
				self::NOT_SIGNED_SELLER_OTHER => 'Other',
			);
		}

		/**
		 * getNotSignedBuyerReasonTypes
		 * Returns the Reasons for Not Signing for Buyers
		 */
		public function getNotSignedBuyerReasonTypes() {
			return array(
				self::NOT_SIGNED_BUYER_NOT_QUALIFY => 'Not Qualified',
				self::NOT_SIGNED_BUYER_NOT_BUY => 'Decide Not to Buy',
				self::NOT_SIGNED_BUYER_ANOTHER_AGENT => 'Went with Another Agent',
				self::NOT_SIGNED_BUYER_OTHER => 'Other',
			);
		}

        /**
         * getNotSignedBuyerReasonTypes
         * Returns the Reasons for Not Signing for Buyers
         */
        public function getNotSignedRecruitReasonTypes() {
            return array(
                self::NOT_SIGNED_RECRUIT_IN_PROGRESS => 'In Progress',
                self::NOT_SIGNED_RECRUIT_NOT_FIT => 'Not Fit',
                self::NOT_SIGNED_RECRUIT_NOT_OTHER => 'Other',
            );
        }

        /**
         * getNotSignedBuyerReasonTypes
         * Returns the Reasons for Not Signing for Buyers
         */
        public function getStatusTypes() {
            return array(
                self::MET_STATUS_SET => 'Set (not Met)',
                self::MET_STATUS_MET => 'Met',
                self::MET_STATUS_CANCEL => 'Cancelled',
                self::MET_STATUS_RESCHEDULE => 'To Reschedule',
            );
        }

		protected function afterFind() {
			$this->set_on_datetime = ($this->set_on_datetime < 1) ? null : Yii::app()->format->formatDate($this->set_on_datetime, StmFormatter::APP_DATETIME_PICKER_FORMAT);
			$this->set_for_datetime = ($this->set_for_datetime < 1) ? null : Yii::app()->format->formatDate($this->set_for_datetime, StmFormatter::APP_DATETIME_PICKER_FORMAT);
            $this->signed_date = ($this->signed_date < 1) ? null : Yii::app()->format->formatDate($this->signed_date, StmFormatter::APP_DATE_PICKER_FORMAT);

            // uses api to find google calendar event
            if($this->google_calendar_event_id && $this->google_calendar_id && Yii::app()->controller->id=='transactions' && Yii::app()->controller->action->id=='edit' && ApiKeys::model()->findByAttributes(array('contact_id'=>Yii::app()->user->id, 'type'=>'google'))) {

                Yii::import('admin_module.components.StmOAuth2.Google', true);
                $stmGoogle = new \StmOAuth2\Google();
                $googleCalendarList = array();

                $success = false;
                try {
                    $listCalendars = $stmGoogle->listCalendars();
                    $success = true;
                }
                catch (\Google_Auth_Exception $e) {
                    echo "Your Google Calendar token is invalid. Please try again or re-do your Integration under Tools => Integration.";
                }
                catch (\Exception $e) {
                    \Yii::log(__CLASS__.' (:'.__LINE__.") Google Calendar List error: " . $e->getMessage(), \CLogger::LEVEL_ERROR);
                }

                if($success) {
                    foreach($listCalendars as $calendar) {
                        $googleCalendarList[$calendar->id] = 'Calendar - '.$calendar->id;
                    }

                    // make sure the user has permission to view this calendar
                    if(isset($googleCalendarList[$this->google_calendar_id])) {
                        $this->_getGoogleCalendarEvent();
                    }
                }
            }

			parent::afterFind();
		}

		protected function beforeValidate()
        {
            if($this->isNewRecord) {
                $this->added = new CDbExpression('NOW()');
                $this->added_by = Yii::app()->user->id;
            }
			$this->updated = new CDbExpression('NOW()');
			$this->updated_by = Yii::app()->user->id;

			return parent::beforeValidate();
		}

        protected function beforeSave() {
            //handle signed agreement alert
            if(!$this->isNewRecord) {
                $oldAppointment = new Appointments;
                $this->_originalModel = $oldAppointment->findByPk($this->id);
            }

			if ($this->set_on_datetime > 0) {
				$this->set_on_datetime = Yii::app()->format->formatDate($this->set_on_datetime, StmFormatter::MYSQL_DATETIME_FORMAT);
			} else {
				$this->set_on_datetime = null;
			}

			if ($this->set_for_datetime > 0) {
				$this->set_for_datetime = Yii::app()->format->formatDate($this->set_for_datetime, StmFormatter::MYSQL_DATETIME_FORMAT);
			} else {
				$this->set_for_datetime = null;
			}

            if ($this->signed_date > 0) {
                $this->signed_date = Yii::app()->format->formatDate($this->signed_date, StmFormatter::MYSQL_DATETIME_FORMAT);
            } else {
                $this->signed_date = null;
            }

            // google calendar handling
            if(!YII_DEBUG &&  $this->googleCalendarData['add_event'] && ApiKeys::model()->findByAttributes(array('contact_id'=>Yii::app()->user->id, 'type'=>'google'))) {
                $this->_saveGoogleCalendarEvent();
            }
            else {
                $this->google_calendar_id = null;
                $this->google_calendar_event_id = null;
            }

            return parent::beforeSave();
		}

		protected function afterSave() {
            if($toEmail = Yii::app()->user->settings->email_all) {

                // sends out alert for newly signed agreements whether it's new or existing
                if($this->is_signed == StmFormHelper::YES) {
                    if($this->isNewRecord || (!empty($this->_originalModel) && $this->_originalModel->is_signed == StmFormHelper::NO)) {
                        $this->_sendAppointmentSignedEmail();
                    }
                }

                // sends out alert for new appointments
                if ($this->isNewRecord && $this->is_signed == StmFormHelper::NO) {

                    $this->_sendAppointmentSetEmail();
                }
            }

            // NOTE: need to check for original data as after the save everything is NOT a new record
            if ($this->isNewRecord) {
                $this->_logNewAppointmentData();
            }

            // see if any elastic tasks need to be updated
            if(!$this->isNewRecord && (Yii::app()->format->formatDate($this->_originalModel->set_on_datetime, StmFormatter::MYSQL_DATETIME_FORMAT) !== Yii::app()->format->formatDate($this->set_on_datetime, StmFormatter::MYSQL_DATETIME_FORMAT))) {
                Tasks::updateElasticActionTaskDueDate($this->componentModel, ActionPlanItems::DUE_REFERENCE_APPOINTMENT_SET_DATE_ID);
            }

            if(!$this->isNewRecord && (Yii::app()->format->formatDate($this->_originalModel->set_for_datetime, StmFormatter::MYSQL_DATETIME_FORMAT) !== Yii::app()->format->formatDate($this->set_for_datetime, StmFormatter::MYSQL_DATETIME_FORMAT))) {
                Tasks::updateElasticActionTaskDueDate($this->componentModel, ActionPlanItems::DUE_REFERENCE_APPOINTMENT_MET_DATE_ID);
            }

            // apply triggers related to appointments set/met/signed
            $this->_applyTriggers();

            parent::afterSave();
		}

        protected function _applyTriggers()
        {
            // if new appointment with SET status (not met)
            if ($this->isNewRecord || (!empty($this->_originalModel) && $this->met_status_ma != $this->_originalModel->met_status_ma)) {
                if(!empty($actionPlanIdsToApply = Yii::app()->db->createCommand("SELECT action_plan_id FROM triggers WHERE status_ma=1 AND component_type_id=".$this->component_type_id." AND action='".Triggers::ACTION_APPLY_ACTION_PLAN."' AND event IN('".Triggers::EVENT_SELLER_APPOINTMENT_MET_STATUS."','".Triggers::EVENT_BUYER_APPOINTMENT_MET_STATUS."') AND event_data='".$this->met_status_ma."'")->queryColumn())) {

                    $criteria = new CDbCriteria();
                    $criteria->addInCondition('id', $actionPlanIdsToApply);
                    $actionPlansToApply = ActionPlans::model()->findAll($criteria);

                    foreach($actionPlansToApply as $actionPlanToApply) {

                        $actionPlanToApply->apply($this->componentModel, null, 'Auto-triggered by Appointment Status of "'.$this->getMetStatusTypes()[$this->met_status_ma].'".');
                        //@todo: what to do if could not apply due to some error? - notify user?
    //                    $validation = StmActiveForm::validate($actionPlanToApply, null, null, $clearErrors=false);
                    }
                }

                if(!empty($actionPlanIdsToUnapply = Yii::app()->db->createCommand("SELECT action_plan_id FROM triggers WHERE status_ma=1 AND component_type_id=".$this->component_type_id." AND action='".Triggers::ACTION_UNAPPLY_ACTION_PLAN."' AND event IN('".Triggers::EVENT_SELLER_APPOINTMENT_MET_STATUS."','".Triggers::EVENT_BUYER_APPOINTMENT_MET_STATUS."') AND event_data='".$this->met_status_ma."'")->queryColumn())) {

                    $criteria = new CDbCriteria();
                    $criteria->addInCondition('id', $actionPlanIdsToUnapply);
                    $actionPlansToUnapply = ActionPlans::model()->findAll($criteria);
                    foreach($actionPlansToUnapply as $actionPlanToUnapply) {

                        $actionPlanToUnapply->unapply($this->componentModel, 'Auto-triggered by Appointment Status of "'.$this->getMetStatusTypes()[$this->met_status_ma].'".');
                    }
                }
            }
        }

        protected  function _logNewAppointmentData()
        {
            $signedString = '';
            if($this->is_signed && $this->signed_date) {
                $signedString = 'Signed: '.Yii::app()->format->formatDate($this->signed_date, StmFormatter::APP_DATE_PICKER_FORMAT);
            }


            $note = 'New Appointment Set:'.PHP_EOL
                .'Set by: '.$this->setBy->fullName.' @ '.Yii::app()->format->formatDate($this->set_on_datetime, StmFormatter::APP_DATETIME_PICKER_FORMAT).PHP_EOL
                .'Set for: '.$this->metBy->fullName.' @ '.Yii::app()->format->formatDate($this->set_for_datetime, StmFormatter::APP_DATETIME_PICKER_FORMAT).PHP_EOL
                .'Set Method: '.TaskTypes::model()->findByPk($this->set_activity_type_id)->name.PHP_EOL
                .'Met Status: '.$this->getMetStatusTypes()[$this->met_status_ma].PHP_EOL
                .'Location: '.$this->getLocationTypes()[$this->location_ma].(($this->location_other)? ': '.$this->location_other : '').PHP_EOL
                .$signedString;

            $activityLog = new ActivityLog();
            $activityLog->setAttributes(array(
                    'component_type_id' => $this->component_type_id,
                    'component_id' => $this->component_id,
                    'task_type_id' => TaskTypes::SYSTEM_NOTE,
                    'activity_date' => new CDbExpression('NOW()'),
                    'note' => $note,
                ));

            if(!$activityLog->save()) {
                // error log
                Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: New Appointment log did not save. Error Message '.print_r($activityLog->getErrors(), true), \CLogger::LEVEL_ERROR);
            }
        }

        // @todo: clean up duplicate code within the 2 email functions _sendAppointmentSetEmail() and _sendAppointmentSignedEmail(), make more efficient with the 2 diff email alerts
        protected function _sendAppointmentSetEmail()
        {
            $phrases = array(
                'Keep up the awesomeness!!',
                'You rock! Wohooooo!',
                'Rock on!!',
                'Oh yeahhh!!!',
                'Go get \'em!!!',
                'Heck yeaaahhhh!!',
                'Sweeeeet!!!',
                'Cool beans!!!',
                'Fantasticoooo!!! :D',
                'Superrificc!!! =)',
            );
            $randomKey = array_rand($phrases);

            $appointmentPhotos = array('brain.jpg','drevil.jpg','rainmoney.jpg','superman.jpg');
            $randomPhotoKey = array_rand($appointmentPhotos);


            //get # of appointments set by the agent this month so far
            $countThisMonthSellerCriteria = new CDbCriteria;
            $countThisMonthSellerCriteria->condition = 'set_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::SELLERS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'"';

            $countThisMonthBuyerCriteria = new CDbCriteria;
            $countThisMonthBuyerCriteria->condition = 'set_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::BUYERS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'"';

            $countThisMonthRecruitCriteria = new CDbCriteria;
            $countThisMonthRecruitCriteria->condition = 'set_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::RECRUITS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'"';

            $countThisMonthSellerAppts = Appointments::model()->count($countThisMonthSellerCriteria);
            $countThisMonthBuyerAppts = Appointments::model()->count($countThisMonthBuyerCriteria);
            $countThisMonthRecruitAppts = Appointments::model()->count($countThisMonthRecruitCriteria);

            //get # of appointments set by the agent this month so far
            $countThisMonthSellerSignedCriteria = new CDbCriteria;
            $countThisMonthSellerSignedCriteria->condition = 'met_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::SELLERS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'" AND is_signed='.self::SIGN_STATUS_YES;

            $countThisMonthBuyerSignedCriteria = new CDbCriteria;
            $countThisMonthBuyerSignedCriteria->condition = 'met_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::BUYERS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'" AND is_signed='.self::SIGN_STATUS_YES;

            $countThisMonthRecruitSignedCriteria = new CDbCriteria;
            $countThisMonthRecruitSignedCriteria->condition = 'met_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::RECRUITS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'" AND is_signed='.self::SIGN_STATUS_YES;

            $countThisMonthSellerSigned = Appointments::model()->count($countThisMonthSellerSignedCriteria);
            $countThisMonthBuyerSigned = Appointments::model()->count($countThisMonthBuyerSignedCriteria);
            $countThisMonthRecruitSigned = Appointments::model()->count($countThisMonthRecruitSignedCriteria);



//            $message = new YiiMailMessage;
            $logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Display Team Logo" style="width:200px;">';
            $appointmentFor = (($this->set_by_id == $this->met_by_id)? '' : ' for '.$this->metBy->fullName);
            $content =
                '<div style="font-weight:bold;font-size:25px;">'
                .'Congrats!<br /><br />'
                .'<span style="color: #D20000;">'. $this->setBy->fullName . ' set an appointment'.$appointmentFor.' with '. $this->contact->fullName .' ('.$this->componentType->getSingularName(). ')!<br />'
                .'Appointment is scheduled for '. Yii::app()->format->formatDateTime($this->set_for_datetime).'</span><br /><br />'

                .$this->setBy->fullName.' Set Appointments this Month:<br />'
                .'<table>'
                .'<tr>'
                .'<td></td>'
                .'<td style="text-align: center; border-bottom: 1px solid black;">Set Appts</td>'
                .'<td style="text-align: center; border-bottom: 1px solid black;">Signed Appts</td>'
                .'</tr>';

            if(in_array($this->component_type_id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))) {
                $content .='<tr>'
                    .'<td>Sellers</td>'
                    .'<td style="text-align: center;">'.$countThisMonthSellerAppts.'</td>'
                    .'<td style="text-align: center;">'.$countThisMonthSellerSigned.'</td>'
                    .'</tr>'
                    .'<tr>'
                    .'<td>Buyers</td>'
                    .'<td style="text-align: center;">'.$countThisMonthBuyerAppts.'</td>'
                    .'<td style="text-align: center;">'.$countThisMonthBuyerSigned.'</td>'
                    .'</tr>'
                    .'<tr>'
                    .'<td>Total</td>'
                    .'<td style="text-align: center;">'.($countThisMonthSellerAppts + $countThisMonthBuyerAppts).'</td>'
                    .'<td style="text-align: center;">'.($countThisMonthSellerSigned + $countThisMonthBuyerSigned).'</td>'
                    .'</tr>';
            }
            elseif($this->component_type_id = ComponentTypes::RECRUITS) {
                $content .='<tr>'
                    .'<td>Recruits</td>'
                    .'<td style="text-align: center;">'.$countThisMonthRecruitAppts.'</td>'
                    .'<td style="text-align: center;">'.$countThisMonthRecruitSigned.'</td>'
                    .'</tr>';
            }

            $content .='</table>'

                .'<br /><span style="color:blue;font-style:italic;">' . $phrases[$randomKey].'</span>'.$logoImage.'<br/><br/>'
                .'<img src="' . Yii::app()->controller->getModule()->getCdnImageAssetsUrl().'appointments/' . $appointmentPhotos[$randomPhotoKey].'" alt="Congrats on Appointment Set - '.$phrases[$randomKey].'!">'
                .'</div>';
            //                    $message->view = 'plain';
//            $message->setBody($content, 'text/html');
            $subject = 'Congrats! ' . $this->setBy->fullName . ' set a '.$this->componentType->getSingularName().' Appointment'.$appointmentFor.'!';
//            $message->setSubject($subject);

            if (YII_DEBUG) {
//                $message->addTo(StmMailMessage::DEBUG_EMAIL_ADDRESS, 'Team Congrats');
                $to = array(StmMailMessage::DEBUG_EMAIL_ADDRESS => 'Team Congrats');
            } else {
                $to = array(Yii::app()->user->settings->email_all => 'Team Congrats');
//                $message->addTo(Yii::app()->user->settings->email_all, 'Team Congrats');
            }

//            $message->addFrom('do-not-reply@seizethemarket.net', 'Team Congrats');

            //$viewPath = YiiBase::getPathOfAlias('admin_module.views.emails.appointmentSet').'.php';
//            StmZendMail::sendYiiMailMessage($message);

            //@todo: use Zimbra transport as the from may NOT be AWS approved.
            $from = array(Yii::app()->user->settings->email_all => 'Team Congrats');
            if($from) {
                $bcc = (strpos($_SERVER['SERVER_NAME'], 'findhomesinpdx') !== false) ? "debug@seizethemarket.com" : null;
                // send email to team congrats email
                StmZendMail::easyMail(array('do-not-reply@seizethemarket.net' => 'STM Notification'), $to, $subject, $content, $bcc, $type='text/html', $awsTransport=false, $ignoreOnDebug = false, Yii::app()->user->settings->email_all);
            }

            // send email to appointment recipient
            if($apptRecipientEmail = $this->metBy->getPrimaryEmail()) {
                $bcc = (strpos($_SERVER['SERVER_NAME'], 'findhomesinpdx') !== false) ? "debug@seizethemarket.com" : null;
                StmZendMail::easyMail(array('do-not-reply@seizethemarket.net' => 'STM Notification'), $apptRecipientEmail, $subject, $content, $bcc, $type='text/html', $awsTransport=false, $ignoreOnDebug = false, $apptRecipientEmail);
            }
        }

        protected function _sendAppointmentSignedEmail()
        {
            $phrases = array(
                'Keep up the awesomeness!!',
                'You rock! Wohooooo!',
                'Rock on!!',
                'Oh yeahhh!!!',
                'Go get \'em!!!',
                'Heck yeaaahhhh!!',
                'Sweeeeet!!!',
                'Cool beans!!!',
                'Fantasticoooo!!! :D',
                'Superrificc!!! =)',
            );
            $randomKey = array_rand($phrases);

            $appointmentPhotos = array('brain.jpg','drevil.jpg','rainmoney.jpg','superman.jpg');
            $randomPhotoKey = array_rand($appointmentPhotos);


            //get # of appointments set by the agent this month so far
            $countThisMonthSellerCriteria = new CDbCriteria;
            $countThisMonthSellerCriteria->condition = 'set_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::SELLERS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'"';

            $countThisMonthBuyerCriteria = new CDbCriteria;
            $countThisMonthBuyerCriteria->condition = 'set_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::BUYERS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'"';

            $countThisMonthRecruitCriteria = new CDbCriteria;
            $countThisMonthRecruitCriteria->condition = 'set_by_id='.$this->set_by_id.' AND component_type_id='.ComponentTypes::RECRUITS.' AND DATE(set_on_datetime) >="'.date('Y-m-').'01" AND DATE(set_on_datetime) <="'.date('Y-m-t').'"';

            $countThisMonthSellerAppts = Appointments::model()->count($countThisMonthSellerCriteria);
            $countThisMonthBuyerAppts = Appointments::model()->count($countThisMonthBuyerCriteria);
            $countThisMonthRecruitAppts = Appointments::model()->count($countThisMonthRecruitCriteria);

            $message = new YiiMailMessage;
            $logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Display Team Logo" style="width:200px;">';
            $content =
                '<div style="font-weight:bold;font-size:25px;">'
                .'Congrats!<br /><br />'
                .'<span style="color: #00C300;">'. $this->metBy->fullName . ' got a Signed Agreement with '. $this->contact->fullName .' ('.$this->componentType->getSingularName(). ')!</span><br /><br />'
                . $this->setBy->fullName.' Appointments this Month:<br />';

            if(in_array($this->component_type_id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))) {
                $content .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sellers:'.$countThisMonthSellerAppts.'<br />'
                    .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Buyers: '.$countThisMonthBuyerAppts.'<br />'
                    .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total: '.($countThisMonthSellerAppts + $countThisMonthBuyerAppts).'<br /><br />';
            }
            elseif($this->component_type_id==ComponentTypes::RECRUITS) {
                $content .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Recruits:'.$countThisMonthRecruitAppts.'<br /><br />';
            }

            $content.= '<span style="color:blue;font-style:italic;">' . $phrases[$randomKey].'</span>'.$logoImage.'<br/><br/>'
                .'<a href="http://www.'.Yii::app()->user->primaryDomain->name.'/admin/'.$this->componentType->name.'/'.$this->component_id.'">Click here to View</a>'
                .'<img src="' . Yii::app()->controller->getModule()->getCdnImageAssetsUrl().'appointments/' . $appointmentPhotos[$randomPhotoKey].'" alt="Congrats on Signed Agreement - '.$phrases[$randomKey].'!">'
                .'</div>';
            //                    $message->view = 'plain';
//            $message->setBody($content, 'text/html');
            $subject = 'Congrats! ' . $this->metBy->fullName . ' got a Signed '.$this->componentType->getSingularName().' Agreement!';
            $message->setSubject($subject);

            if (YII_DEBUG) {
//                $message->addTo(StmMailMessage::DEBUG_EMAIL_ADDRESS, 'Team Congrats');
                $to = array(StmMailMessage::DEBUG_EMAIL_ADDRESS => 'Team Congrats');
            } else {
                $to = array(Yii::app()->user->settings->email_all => 'Team Congrats');
//                    $message->addTo(Yii::app()->user->settings->email_all, 'Team Congrats');
            }

//            $message->addFrom('do-not-reply@seizethemarket.net', 'Team Congrats');
//            StmZendMail::sendYiiMailMessage($message);
            $from = array(Yii::app()->user->settings->email_all =>'Team Congrats');
            StmZendMail::easyMail($from, $to, $subject, $content, null, $type='text/html', $awsTransport=false, $ignoreOnDebug = false, Yii::app()->user->settings->email_all);
        }

        protected function _saveGoogleCalendarEvent()
        {
            // sanity check
            if(!$this->googleCalendarData['add_event']) {
                return;
            }

            Yii::import('admin_module.components.StmOAuth2.Google', true);
            $stmGoogle = new \StmOAuth2\Google();
            // start-time for event_duration //@todo: need to streamline more, make efficient & reusable
            $timezone = new \DateTimeZone(\Yii::app()->user->account->timezone);
            if(!$timezone) {
                throw new \Exception('Timezone is missing.');
            }
            $stmGoogle->setTimeZone($timezone);

            // calculates end time for either scenario of add/update
            $endDateTime = new \DateTime($this->googleCalendarData['event_start']);
            // $endDateTime->setTimezone($timezone); //@todo: there is start.timeZone that we should assign $timezone to
            $endDateTime->modify('+'.$this->googleCalendarData['event_duration'].' minutes');

//$gmtTimezone = new DateTimeZone('GMT');
//$myDateTime = new DateTime(date('Y-m-d H:i:s'), $gmtTimezone);
//$myDateTime->format('c');
//$offset = $timezone->getOffset($myDateTime);
            // edit existing event
            if($this->google_calendar_id && $this->google_calendar_event_id) {
                $stmGoogle->editCalendarEvent($this->google_calendar_id, $this->google_calendar_event_id, $this->googleCalendarData['event_start'], $endDateTime->format('Y-m-d H:i:s'), $this->googleCalendarData['event_name'], $this->googleCalendarData['event_location'], $this->googleCalendarData['event_description']);
            }
            // add new event
            else {
                $this->google_calendar_event_id = $stmGoogle->addCalendarEvent($this->google_calendar_id, $this->googleCalendarData['event_start'], $endDateTime->format('c'), $this->googleCalendarData['event_name'], $this->googleCalendarData['event_location'], $this->googleCalendarData['event_description'], $colorId='11');
            }
        }

        /**
         * Finds Google Calendar Event and populates it into the Appointments model
         * @throws Exception
         */
        protected function _getGoogleCalendarEvent()
        {
            Yii::import('admin_module.components.StmOAuth2.Google', true);
            $stmGoogle = new \StmOAuth2\Google();
            $event = $stmGoogle->getCalendarEvent($this->google_calendar_id, $this->google_calendar_event_id);

            if(!$event) {
                return;
            }

            $this->googleCalendarData['add_event'] = true;
            $this->googleCalendarData['event_name'] = $event->summary;
            $this->googleCalendarData['event_location'] = $event->location;
            $this->googleCalendarData['event_description'] = $event->description;

            // start-time for event_duration //@todo: need to streamline more, make efficient & reusable
            $timezone = new \DateTimeZone(\Yii::app()->user->account->timezone);
            if(!$timezone) {
                throw new \Exception('Timezone is missing.');
            }

            $gcalStart = ($event->start->dateTime)? $event->start->dateTime : $event->start->date;
            $startDateTime = new \DateTime($gcalStart);
            $startDateTime->setTimezone($timezone);
//            $this->googleCalendarData['event_start'] = $startDateTime->format(\StmFormatter::APP_DATETIME_PICKER_FORMAT);

            // event_duration //@todo: need to streamline more, make efficient & reusable
            $gcalEnd = ($event->end->dateTime)? $event->end->dateTime : $event->end->date;
            $endDateTime = new \DateTime($gcalEnd);
            $endDateTime->setTimezone($timezone);

            $diff = $startDateTime->diff($endDateTime);
            $diffMinutes = ($diff->format('%h') * 60) + $diff->format('%i');

            switch(1) {
                case ($diffMinutes < 75):
                    $this->googleCalendarData['event_duration'] = 60;
                    break;
                case ($diffMinutes >= 75 & $diffMinutes <= 105):
                    $this->googleCalendarData['event_duration'] = 90;
                    break;
                case ($diffMinutes > 105):
                    $this->googleCalendarData['event_duration'] = 120;
                    break;
            }

            $this->googleCalendarData['calendar_id'] = $this->google_calendar_id;
        }

        public function getContact() {
			$Contact = $this->componentType->getContact($this->component_id);

			return $Contact;
		}

        protected function _getReportQueryCommandBase()
        {
            return Yii::app()->db->createCommand()
                ->select("
                a.id id,
                DATE(a.set_on_datetime) set_on,
                DATE(a.set_for_datetime) set_for,
                a.signed_date,
                a.component_type_id component_type_id,
                a.component_id component_id,
                CONCAT(c.first_name, ' ', c.last_name) name,
                a.set_by_id set_by_id,
                a.met_by_id met_by_id,
                CONCAT(c2.first_name, ' ', c2.last_name) set_by,
                tt.name set_via,
                CONCAT(c3.first_name, ' ', c3.last_name) met_by,
                a.met_status_ma met_status_ma,
                a.not_signed_reason not_signed_reason,
                a.not_signed_reason_other not_signed_reason_other,
                a.is_signed is_signed,
                cl.closing_status_ma closed_status
                ")
                ->from('appointments a')
                ->leftJoin('transactions t', 'a.component_id = t.id')
                ->leftJoin('closings cl', 't.id = cl.transaction_id AND cl.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID)
                ->leftJoin('contacts c', 't.contact_id = c.id')
                ->leftJoin('contacts c2', 'a.set_by_id = c2.id')
                ->leftJoin('contacts c3', 'a.met_by_id = c3.id')
                ->leftJoin('task_types tt', 'a.set_activity_type_id = tt.id')


                //->andWhere('cl.closing_status_ma='.Closings::STATUS_TYPE_CLOSED_ID) //@todo: REMOVE AFTER TESTING!!!!!!!!!!!!!*********************** Clee 4/10/16

                ->andWhere('(a.is_deleted = 0 OR a.is_deleted IS NULL)');
        }

        public function reportBySetDateRange($startDate=null, $endDate=null, $old=false)
        {
            $startDate = ($startDate)? $startDate : date('Y-m-01');
            $endDate = ($endDate)? $endDate : date('Y-m-t');
            $appointments = $this->_getReportQueryCommandBase()->andWhere(
                    '(DATE(a.set_on_datetime) >= :start_date AND DATE(a.set_on_datetime) <= :end_date) OR (DATE(a.set_for_datetime) >= :start_date2 AND DATE(a.set_for_datetime) <= :end_date2) OR (a.signed_date >= :start_date3 AND a.signed_date <= :end_date3)',
                    array(
                        ':start_date' => $startDate,
                        ':end_date' => $endDate,
                        ':start_date2' => $startDate,
                        ':end_date2' => $endDate,
                        ':start_date3' => $startDate,
                        ':end_date3' => $endDate,
                    )
            )->queryAll();

            // the old version is kept until appointments email reporting is updated with new data format.
            return ($old===false)? $this->_sortAppointmentDataForReporting2($appointments, $startDate, $endDate) : $this->_sortAppointmentDataForReporting($appointments, $startDate, $endDate);
        }

        protected function _sortAppointmentDataForReporting2($appointments, $startDate, $endDate)
        {
            $appointmentMetrics = array(
                // You personally set appts
                'youSetForSelf',
                'youSetForSelfFuture',
                'youSetForSelfFuturePrev', // future appt set in previous time frame

                'youSetMet',
                'youSetMetPrev', // met this time frame, set previously
                'youSetMetPercent',

                // Appointments Set for You
                'setForYou',
                'setForYouPresent',
                'setForYouFuture',
                'setForYouFuturePrev',
                'setForYouTotal',
                'setForYouTotalFuture',

                'metPerDateTotal', // appointments you met total according to the date. used for set-met conversion (eliminates future, includes past set appt date)

                'setForYouMet',
                'setForYouMetPrev',
                'setForYouMetPercent',

                // Appointments Set for Others (ex. Inside Sales scenario)
                'setForOthers',
                'setForOthersPresent',
                'setForOthersFuture',
                'setForOthersFuturePrev',

                'setForOthersMet',
                'setForOthersMetPrev',
                'setForOthersMetPercent',

                // Total Appointments Set
                'setTotal',
                'setTotalPresent',
                'setFutureTotal',
                'setFuturePrevTotal',

                'metTotal',
                'metPrevTotal',
                'setMetTotalPercent',

                // Signed
                'youSetMetSigned',
                'youSetMetPrevSigned',
                'setForYouSigned',
                'setForYouPrevSigned',
                'setForOthersSigned',                 // Inside Sales scenario
                'setForOthersPrevSigned',
                'signedCurrentTotal',
                'signedTotal',
                'signedPrevTotal',

                // Met to Signed
                'youSetMetSignedPercent',
                'setForYouSignedPercent',
                'youSetSignedPercent',             // Inside sales scenario of met set percent
                'metSignedTotalPercent',

                // Closings
                'closed',
                'metClosedPercent',
                'signedClosedPercent',

                'pastDue',
            );

            // array of all agent IDs that have appointment activities
            $agentIds = array();
            // will house the buyers, sellers and total reports data for appointments
            $appointmentsReportData = array();

            foreach($appointments as $appointment) {
                // using variables for easy code reading as it gets nested in arrays later
                $setById = $appointment['set_by_id'];
                $metById = $appointment['met_by_id'];

                // check to see if the either the setter or the meeter of the appointment is in the agentIds array. If not, add them for the report.
                if(!in_array($setById, $agentIds)) {
                    array_push($agentIds, $setById);
                }
                if(!in_array($metById, $agentIds)) {
                    array_push($agentIds, $metById);
                }

                switch($appointment['component_type_id']) {
                    case ComponentTypes::BUYERS:
                        $type = 'buyer';
                        break;

                    case ComponentTypes::SELLERS:
                        $type = 'seller';
                        break;

                    case ComponentTypes::RECRUITS:
                        $type = 'recruit';
                        break;

                    default:
                        throw new Exception(__CLASS__.' ('.__LINE__.') Unhandled type detected');
                        break;
                }

                // populated all the tracked metrics - array('sellers'=> array(sellers info),'buyers'=> array(buyer info),'total'=> array(total info))
                foreach($appointmentMetrics as $appointmentMetric) {
                    $countValue = 0;
                    $agentIdFieldName = '';

                    // check to see if the appointment was set prev to request time frame. This keeps track of the building pipeline.
                    $setOnPrev = ($appointment['set_on'] < $startDate)? true : false;
                    // alternative way to reference this to avoid double negative. Represents the appointment was set in the requested time frame.
                    $isCurr = !$setOnPrev;
                    // flag for if the appointment is set in the future, hasn't happened yet and is handled different. Example: signed data is not counted.
                    $isFuture = ($appointment['set_for'] > date('Y-m-d'))? true : false;

                    $setForPrev = ($appointment['set_for'] < $startDate)? true : false;

                    $isMet = ($appointment['met_status_ma'] == self::MET_STATUS_MET) ? true : false;

                    $isSigned = ($appointment['is_signed'] == self::SIGN_STATUS_YES && $appointment['signed_date'] >= $startDate && $appointment['signed_date'] <= $endDate) ? true : false;

                    // uses the setById, metById variables from above to populate the reports array properly
                    switch($appointmentMetric) {

                        // appointments you set for yourself
                        case 'youSetForSelf':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr && ($setById === $metById))? 1 : 0;
                            break;

                        // any and all appointments that is set for the future, hasn't happened yet.
                        case 'youSetForSelfFuture':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr && ($setById === $metById) && $isFuture)? 1 : 0;
                            break;

                        case 'youSetMet':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr && ($setById === $metById) && $isMet)? 1 : 0;
                            break;

                        case 'youSetMetPrev':
                            $agentIdFieldName = 'setById';
                            $countValue = ($setOnPrev && ($setById === $metById) && $isMet)? 1 : 0;
                            break;

                        case 'setTotalPresent':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr && !$isFuture)? 1 : 0;
                            break;

                        case 'setTotal':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr)? 1 : 0;
                            break;

                        case 'setFutureTotal':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr && $isFuture)? 1 : 0;
                            break;

                        case 'setFuturePrevTotal':
                            $agentIdFieldName = 'setById';
                            $countValue = ($setOnPrev && $isFuture)? 1 : 0;
                            break;

                        case 'setForYou':
                            $agentIdFieldName = 'metById';
                            $countValue = ($isCurr & ($setById !== $metById))? 1 : 0;
                            break;

                        case 'setForYouTotal':
                            $agentIdFieldName = 'metById';
                            $countValue = ($isCurr)? 1 : 0;
                            break;

                        case 'metPerDateTotal':
                            $agentIdFieldName = 'metById';
                            //@todo: $isMet should not be part of the statement, validation is put in place now, but some data is funky and put $isMet it so it would not ignore
                            $countValue = ($isMet || ($appointment['set_for'] >= $startDate && $appointment['set_for'] <= date('Y-m-d')))? 1 : 0;
                            break;

                        case 'setForYouTotalFuture': //@todo:?? include prev for set total count?... for set to met
                            $agentIdFieldName = 'metById';
                            $countValue = ($isFuture)? 1 : 0;
                            break;

                        case 'setForYouFuture':
                            $agentIdFieldName = 'metById';
                            $countValue = ($isCurr & ($setById !== $metById) && $isFuture)? 1 : 0;
                            break;

                        case 'setForYouFuturePrev':
                            $agentIdFieldName = 'metById';
                            $countValue = ($setOnPrev & ($setById !== $metById) && $isFuture)? 1 : 0;
                            break;

                        case 'setForYouMet':
                            $agentIdFieldName = 'metById';
                            $countValue = ($isCurr & ($setById !== $metById) && $isMet)? 1 : 0;
                            break;

                        case 'setForYouMetPrev':
                            $agentIdFieldName = 'metById';
                            $countValue = ($setOnPrev & ($setById !== $metById) && $isMet)? 1 : 0;
                            break;

                        case 'setForYouPresent': // set for you met that is not from the past time frame
                            $agentIdFieldName = 'metById';
                            $countValue = ($isCurr  && ($setById !== $metById) && !$isFuture)? 1 : 0;
                            break;

                        case 'setForOthers':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr & ($setById !== $metById))? 1 : 0;
                            break;

                        case 'setForOthersFuture':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr & ($setById !== $metById) && $isFuture)? 1 : 0;
                            break;

                        case 'setForOthersPresent':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr & ($setById !== $metById) && !$isFuture)? 1 : 0;
                            break;

                        case 'setForOthersMet':
                            $agentIdFieldName = 'setById';
                            $countValue = ($isCurr & ($setById !== $metById) && $isMet)? 1 : 0;
                            break;

                        case 'metTotal': //setMetTotalPercent
                            $agentIdFieldName = 'metById';
                            $countValue = ($isMet)? 1 : 0;
                            break;

                        case 'metPrevTotal':
                            $agentIdFieldName = 'metById';
                            $countValue = ($setOnPrev && $isMet)? 1 : 0;
                            break;

                        case 'youSetMetSigned':
                            $agentIdFieldName = 'setById';
                            $countValue = (!$setForPrev && ($setById === $metById) && $isSigned)? 1 : 0;
                            break;

                        case 'youSetMetPrevSigned':
                            $agentIdFieldName = 'setById';
                            $countValue = ($setForPrev & ($setById === $metById) && $isSigned)? 1 : 0;
                            break;

                        case 'setForYouSigned':
                            $agentIdFieldName = 'metById';
                            $countValue = (($setById !== $metById) && $isSigned)? 1 : 0;
                            break;

                        case 'setForYouPrevSigned':
                            $agentIdFieldName = 'metById';
                            $countValue = ($setOnPrev & ($setById !== $metById) && $isSigned)? 1 : 0;
                            break;

                        case 'setForOthersSigned':
                            $agentIdFieldName = 'setById';
                            $countValue = (($setById !== $metById) && $isSigned)? 1 : 0;
                            break;

                        case 'setForOthersPrevSigned':
                            $agentIdFieldName = 'setById';
                            $countValue = ($setOnPrev & ($setById !== $metById) && $isSigned)? 1 : 0;
                            break;

                        // created to calculate met to signed. It was pulling in met from before, signed this month and creating a 200% met to signed figure because it was not pulling in any other mets from previous periods
                        case 'signedCurrentTotal':
                            $agentIdFieldName = 'metById';
                            $countValue = ($isSigned && !$isFuture)? 1 : 0;
                            break;

                        case 'signedTotal':
                            $agentIdFieldName = 'metById';
                            $countValue = ($isSigned)? 1 : 0;
                            break;

                        case 'signedPrevTotal':
                            $agentIdFieldName = 'metById';
                            $countValue = ($setOnPrev && $isSigned)? 1 : 0;
                            break;

                        case 'closed':
                            $agentIdFieldName = 'metById';
                            $countValue = ($appointment['closed_status'])? 1 : 0;
                            break;

                        case 'pastDue':
                            $agentIdFieldName = 'metById';
                            $countValue = (($appointment['set_for']  < date('Y-m-d')) && (intval($appointment['met_status_ma']) == self::MET_STATUS_SET))? 1 : 0;
//                            $agentAppointments[$appointment['met_by_id']]['pastDueBuyer'] += (($appointment['set_for'] < date('Y-m-d')) && ($appointment['met_status_ma'] == self::MET_STATUS_SET)) ? 1 : 0;
                            break;
                        default:
                            continue;
                            break;

                    }
                    // populates the buyer, seller and total data for appointments report array
                    if(!empty($agentIdFieldName)) {
                        $appointmentsReportData[$type][$$agentIdFieldName][$appointmentMetric] += $countValue;
                        $appointmentsReportData[$type.'Total'][$appointmentMetric] += $countValue;
                        $appointmentsReportData['total'][$$agentIdFieldName][$appointmentMetric] += $countValue;
                        $appointmentsReportData['totalTotal'][$appointmentMetric] += $countValue;
                    }
                }
            }

            // calculate all the percentages with populated data
            $percentFields = array('youSetMetPercent','setForYouMetPercent','setForOthersMetPercent','setMetTotalPercent','metSignedTotalPercent','metClosedPercent','signedClosedPercent');
            // go through all agentsIds for buyer, seller, buyerTotal, sellerTotal
            foreach($percentFields as $percentField) {

                $numeratorField = 0;
                $denominatorField = 0;
                switch ($percentField) {
                    case 'youSetMetPercent':
                        $numeratorField = 'youSetMet';
                        $denominatorField = 'setTotalPresent';
                        break;

                    case 'setForYouMetPercent':
                        $numeratorField = 'setForYouMet';
                        $denominatorField = 'setForYouPresent';
                        break;

                    case 'setForOthersMetPercent':
                        $numeratorField = 'setForOthersMet';
                        $denominatorField = 'setForOthersPresent';
                        break;

                    case 'setMetTotalPercent':
                        $numeratorField = 'metTotal';
                        $denominatorField = 'metPerDateTotal';
                        break;

                    case 'metSignedTotalPercent':
                        $numeratorField = 'signedCurrentTotal';
                        $denominatorField = 'metTotal';
                        break;

                    case 'metClosedPercent':
                        $numeratorField = 'closed';
                        $denominatorField = 'metTotal';
                        break;

                    case 'signedClosedPercent':
                        $numeratorField = 'closed';
                        $denominatorField = 'signedTotal';
                        break;

                    default:
                        continue;
                        break;
                }
                if($appointmentsReportData['seller'] && is_array($appointmentsReportData['seller'])) {
                    // calculate all agent percentages
                    foreach($appointmentsReportData['seller'] as $id => $agentData) {
                        if(isset($agentData[$denominatorField]) && $agentData[$denominatorField]) {
                            $appointmentsReportData['seller'][$id][$percentField] = Yii::app()->format->formatPercentages($agentData[$numeratorField]/$agentData[$denominatorField]);
                        }
                    }
                }
                if($appointmentsReportData['buyer'] && is_array($appointmentsReportData['buyer'])) {
                    foreach($appointmentsReportData['buyer'] as $id => $agentData) {
                        if(isset($agentData[$denominatorField]) && $agentData[$denominatorField]) {
                            $appointmentsReportData['buyer'][$id][$percentField] = Yii::app()->format->formatPercentages($agentData[$numeratorField]/$agentData[$denominatorField]);
                        }
                    }
                }
                if($appointmentsReportData['recruit'] && is_array($appointmentsReportData['recruit'])) {
                    foreach($appointmentsReportData['recruit'] as $id => $agentData) {
                        if(isset($agentData[$denominatorField]) && $agentData[$denominatorField]) {
                            $appointmentsReportData['recruit'][$id][$percentField] = Yii::app()->format->formatPercentages($agentData[$numeratorField]/$agentData[$denominatorField]);
                        }
                    }
                }
                if($appointmentsReportData['total'] && is_array($appointmentsReportData['total'])) {
                    foreach($appointmentsReportData['total'] as $id => $agentData) {
                        if(isset($agentData[$denominatorField]) && $agentData[$denominatorField]) {
                            $appointmentsReportData['total'][$id][$percentField] = Yii::app()->format->formatPercentages($agentData[$numeratorField]/$agentData[$denominatorField]);
                        }
                    }
                }
                // loop through the different chunks / categories (indexes) of data to calculate percentage for
                $totalCategories = array('sellerTotal','buyerTotal','recruitTotal','totalTotal');
                foreach($totalCategories as $totalCategory) {
                    if(isset($appointmentsReportData[$totalCategory][$denominatorField]) && $appointmentsReportData[$totalCategory][$denominatorField]) {
                        $appointmentsReportData[$totalCategory][$percentField] = Yii::app()->format->formatPercentages($appointmentsReportData[$totalCategory][$numeratorField]/$appointmentsReportData[$totalCategory][$denominatorField]);
                    } else {
                        $appointmentsReportData[$totalCategory][$percentField] = 0;
                    }
                }
            }


            return array('appointmentsReportData'=>$appointmentsReportData, 'agentIds'=>$agentIds, 'appointments'=>$appointments);
        }

        protected function _sortAppointmentDataForReporting($appointments, $startDate, $endDate) {
            // Make dictionaries for lookups
            $agentTotals = array(
                'setAppointmentsBuyer'      =>  0,
                'metAppointmentsBuyer'      =>  0,
                'signedAgreementsBuyer'     =>  0,
                'setAppointmentsSeller'     =>  0,
                'metAppointmentsSeller'     =>  0,
                'signedAgreementsSeller'    =>  0,
                'setAppointmentsRecruit'     =>  0,
                'metAppointmentsRecruit'     =>  0,
                'signedAgreementsRecruit'    =>  0,
                'setAppointments'           =>  0,
                'metAppointments'           =>  0,
                'signedAgreements'          =>  0
            );
            $agentAppointments = array();
            $agentIds = array();

            foreach($appointments as $appointment) {
                if(!in_array($appointment['set_by_id'], $agentIds)) {
                    array_push($agentIds, $appointment['set_by_id']);
                }
                // Make sure the agent has an array
                if(!array_key_exists($appointment['set_by_id'], $agentAppointments)) {
                    $agentAppointments[$appointment['set_by_id']] = array(
                        'setAppointmentsBuyer'      =>  0,
                        'metAppointmentsBuyer'      =>  0,
                        'signedAgreementsBuyer'     =>  0,
                        'setAppointmentsSeller'     =>  0,
                        'metAppointmentsSeller'     =>  0,
                        'signedAgreementsSeller'    =>  0,
                        'setAppointmentsRecruit'     =>  0,
                        'metAppointmentsRecruit'     =>  0,
                        'signedAgreementsRecruit'    =>  0,
                        'setAppointments'           =>  0,
                        'metAppointments'           =>  0,
                        'signedAgreements'          =>  0
                    );
                }

                // Add up totals for agent
                switch($appointment['component_type_id']) {
                    case ComponentTypes::BUYERS:
                        // if the appt is set before the date range but met/signed within range
                        if($appointment['set_on'] < $startDate) {
                            $agentAppointments[$appointment['met_by_id']]['prevSignedAgreementsBuyer'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetByOnlySignedBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetForOthersAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetForOthersFutureAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevMetButSetByYouAppointmentsBuyer'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['prevSetByMetBySignedBuyer'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;

                            $agentTotals['prevSetForOthersAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['prevSetForOthersFutureAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentTotals['prevMetButSetByYouAppointmentsBuyer'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['prevSignedAgreementsBuyer'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentTotals['prevSetByOnlySignedBuyer'] +=  (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentTotals['prevSetByMetBySignedBuyer'] +=  (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;

                        }
                        // if appt is set and met is within the date range
                        else {
                            $agentAppointments[$appointment['set_by_id']]['setAppointmentsBuyer']++;
                            $agentAppointments[$appointment['met_by_id']]['setForAppointmentsBuyer']++;
                            $agentAppointments[$appointment['met_by_id']]['setForFutureAppointmentsBuyer'] += $appointment['set_for'] > date('Y-m-d') ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setForOthersAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setForOthersFutureAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['metAppointmentsBuyer'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['signedAgreementsBuyer'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['metButSetByYouAppointmentsBuyer'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['setByMetByAppointmentsBuyer'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] == $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['futureAppointmentsBuyer'] += $appointment['set_for'] > date('Y-m-d')? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setByMetBySignedBuyer'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setByOnlySignedBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;



                            $agentTotals['setAppointmentsBuyer']++;
                            $agentTotals['metAppointmentsBuyer'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                            $agentTotals['signedAgreementsBuyer'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentTotals['metButSetByYouAppointmentsBuyer'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['setByMetByAppointmentsBuyer'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] == $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['futureAppointmentsBuyer'] += $appointment['set_for'] > date('Y-m-d')? 1 : 0;
                            $agentTotals['setForOthersAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['setForOthersFutureAppointmentsBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && $appointment['set_for'] > date('Y-m-d'))? 1 : 0;
                            $agentTotals['setByMetBySignedBuyer'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentTotals['setByOnlySignedBuyer'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['pastDueBuyer'] += (($appointment['set_for'] < date('Y-m-d')) && ($appointment['met_status_ma'] == self::MET_STATUS_SET)) ? 1 : 0;
                        }

                        break;

                    case ComponentTypes::SELLERS:
                        if($appointment['set_on'] < $startDate) {
                            $agentAppointments[$appointment['met_by_id']]['prevSignedAgreementsSeller'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetByOnlySignedSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetForOthersAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetForOthersFutureAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevMetButSetByYouAppointmentsSeller'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['prevSetByMetBySignedSeller'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;

                            $agentTotals['prevSetForOthersAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['prevSetForOthersFutureAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentTotals['prevMetButSetByYouAppointmentsSeller'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['prevSignedAgreementsSeller'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentTotals['prevSetByOnlySignedSeller'] +=  (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentTotals['prevSetByMetBySignedSeller'] +=  (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                        }
                        // if appt is set and met is within the date range
                        else {
                            $agentAppointments[$appointment['set_by_id']]['setAppointmentsSeller']++;
                            $agentAppointments[$appointment['met_by_id']]['setForAppointmentsSeller']++;
                            $agentAppointments[$appointment['met_by_id']]['setForFutureAppointmentsSeller'] += $appointment['set_for'] > date('Y-m-d') ? 1 : 0;;
                            $agentAppointments[$appointment['set_by_id']]['setForOthersAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setForOthersFutureAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['metAppointmentsSeller'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['signedAgreementsSeller'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['metButSetByYouAppointmentsSeller'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['setByMetByAppointmentsSeller'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] == $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['futureAppointmentsSeller'] += $appointment['set_for'] > date('Y-m-d') ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setByMetBySignedSeller'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setByOnlySignedSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;

                            $agentTotals['setAppointmentsSeller']++;
                            $agentTotals['metAppointmentsSeller'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                            $agentTotals['signedAgreementsSeller'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentTotals['metButSetByYouAppointmentsSeller'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['setByMetByAppointmentsSeller'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] == $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['futureAppointmentsSeller'] += $appointment['set_for'] > date('Y-m-d')? 1 : 0;
                            $agentTotals['setForOthersAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['setForOthersFutureAppointmentsSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && $appointment['set_for'] > date('Y-m-d'))? 1 : 0;
                            $agentTotals['setByMetBySignedSeller'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentTotals['setByOnlySignedSeller'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['pastDueSeller'] += (($appointment['set_for'] < date('Y-m-d')) && ($appointment['met_status_ma'] == self::MET_STATUS_SET)) ? 1 : 0;
                        }
                        break;

                    case ComponentTypes::RECRUITS:
                        $componentName = 'Recruits';
                        if($appointment['set_on'] < $startDate) {
                            $agentAppointments[$appointment['met_by_id']]['prevSignedAgreementsRecruit'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetByOnlySignedRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetForOthersAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevSetForOthersFutureAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['prevMetButSetByYouAppointmentsRecruit'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['prevSetByMetBySignedRecruit'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;

                            $agentTotals['prevSetForOthersAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['prevSetForOthersFutureAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentTotals['prevMetButSetByYouAppointmentsRecruit'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['prevSignedAgreementsRecruit'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentTotals['prevSetByOnlySignedRecruit'] +=  (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentTotals['prevSetByMetBySignedRecruit'] +=  (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;

                        }
                        // if appt is set and met is within the date range
                        else {
                            $agentAppointments[$appointment['set_by_id']]['setAppointmentsRecruit']++;
                            $agentAppointments[$appointment['met_by_id']]['setForAppointmentsRecruit']++;
                            $agentAppointments[$appointment['met_by_id']]['setForFutureAppointmentsRecruit'] += $appointment['set_for'] > date('Y-m-d') ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setForOthersAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setForOthersFutureAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['metAppointmentsRecruit'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['signedAgreementsRecruit'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['metButSetByYouAppointmentsRecruit'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['setByMetByAppointmentsRecruit'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] == $appointment['met_by_id']))? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['futureAppointmentsRecruit'] += $appointment['set_for'] > date('Y-m-d')? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setByMetBySignedRecruit'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['set_by_id']]['setByOnlySignedRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;



                            $agentTotals['setAppointmentsRecruit']++;
                            $agentTotals['metAppointmentsRecruit'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                            $agentTotals['signedAgreementsRecruit'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                            $agentTotals['metButSetByYouAppointmentsRecruit'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['setByMetByAppointmentsRecruit'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] == $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['futureAppointmentsRecruit'] += $appointment['set_for'] > date('Y-m-d')? 1 : 0;
                            $agentTotals['setForOthersAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                            $agentTotals['setForOthersFutureAppointmentsRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && $appointment['set_for'] > date('Y-m-d'))? 1 : 0;
                            $agentTotals['setByMetBySignedRecruit'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentTotals['setByOnlySignedRecruit'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                            $agentAppointments[$appointment['met_by_id']]['pastDueRecruit'] += (($appointment['set_for'] < date('Y-m-d')) && ($appointment['met_status_ma'] == self::MET_STATUS_SET)) ? 1 : 0;
                        }
                        break;

                    default:
                        throw new Exception('Unhandled type detected');
                        break;
                }

                if($appointment['set_on'] < $startDate) {
                    $agentAppointments[$appointment['set_by_id']]['prevSetForOthersAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['prevSetForOthersFutureAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                    $agentAppointments[$appointment['met_by_id']]['prevSignedAgreements'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['prevSetByOnlySigned'] +=  (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                    $agentAppointments[$appointment['met_by_id']]['prevSetByMetBySigned'] +=  (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;

                    $agentTotals['prevSetForOthersAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                    $agentTotals['prevSetForOthersFutureAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['set_for'] > date('Y-m-d')))? 1 : 0;
                    $agentTotals['prevMetButSetByYouAppointments'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                    $agentTotals['prevSignedAgreements'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                    $agentTotals['prevSetByOnlySigned'] +=  (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                    $agentTotals['prevSetByMetBySigned'] +=  (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                }
                else {
                    // Add defaults
                    $agentAppointments[$appointment['set_by_id']]['setAppointments']++;
                    $agentAppointments[$appointment['met_by_id']]['setForAppointments']++;
                    $agentAppointments[$appointment['met_by_id']]['setForFutureAppointments'] += $appointment['set_for'] > date('Y-m-d') ? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['setForOthersAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['setForOthersFutureAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && $appointment['set_for'] > date('Y-m-d'))? 1 : 0;
                    $agentAppointments[$appointment['met_by_id']]['metAppointments'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['metButSetByYouAppointments'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id'])) ? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['setByMetByAppointments'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] == $appointment['met_by_id']))? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['futureAppointments'] += $appointment['set_for'] > date('Y-m-d') ? 1 : 0;
                    $agentAppointments[$appointment['met_by_id']]['setByMetBySigned'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                    $agentAppointments[$appointment['set_by_id']]['setByOnlySigned'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                    $agentAppointments[$appointment['met_by_id']]['signedAgreements'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;

                    $agentTotals['setAppointments']++;
                    $agentTotals['metAppointments'] += $appointment['met_status_ma'] == self::MET_STATUS_MET ? 1 : 0;
                    $agentTotals['metButSetByYouAppointments'] += (($appointment['met_status_ma'] == self::MET_STATUS_MET) && ($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                    $agentTotals['signedAgreements'] += $appointment['is_signed'] == self::SIGN_STATUS_YES ? 1 : 0;
                    $agentTotals['futureAppointments'] += $appointment['set_for'] > date('Y-m-d') ? 1 : 0;
                    $agentTotals['setForOthersAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']))? 1 : 0;
                    $agentTotals['setForOthersFutureAppointments'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && $appointment['set_for'] > date('Y-m-d'))? 1 : 0;
                    $agentTotals['setByMetBySigned'] += (($appointment['set_by_id'] == $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                    $agentTotals['setByOnlySigned'] += (($appointment['set_by_id'] !== $appointment['met_by_id']) && ($appointment['is_signed'] == self::SIGN_STATUS_YES)) ? 1 : 0;
                }
            }
            return array('agentAppointments'=>$agentAppointments, 'totalAppointments'=>$agentTotals, 'agentIds'=>$agentIds, 'appointments'=>$appointments);
        }

        /*
         * Get component model based on Component Type Id
         *
         * @return mixed Buyer, Seller, Contact, or any Other Component
         */
        public function getComponentModel()
        {
            if($this->component_type_id && $this->component_id) {
                switch($this->component_type_id) {
                    case ComponentTypes::CONTACTS:
                        $componentModel = Contacts::model()->findByPk($this->component_id);
                        break;

                    case ComponentTypes::SELLERS:
                    case ComponentTypes::BUYERS:
                        $componentModel = Buyers::model()->findByPk($this->component_id);
                        break;

                    case ComponentTypes::RECRUITS:
                        $componentModel = Recruits::model()->findByPk($this->component_id);
                        break;
                }
            }

            return $componentModel;
        }

        /**
         *
         * @return bool
         */
        public function byPastDue()
        {
            $this->getDbCriteria()->addCondition('DATE(set_for_datetime) < "'.date('Y-m-d'). '" AND (met_status_ma = '.Appointments::MET_STATUS_SET.')');
            $this->getDbCriteria()->addCondition('met_status_reason="" AND (met_status_ma = '.Appointments::MET_STATUS_RESCHEDULE.' OR met_status_ma = '.Appointments::MET_STATUS_CANCEL.')', 'OR');

            $this->getDbCriteria()->addCondition('met_status_ma = '.Appointments::MET_STATUS_MET.' AND is_signed=0 AND (not_signed_reason IS NULL OR not_signed_reason="")', 'OR');
            $this->getDbCriteria()->addCondition('is_signed=1 AND DATE(set_for_datetime) > "'.date('Y-m-d').'"', 'OR');
            $this->getDbCriteria()->addCondition('is_signed=1 AND met_status_ma != "'.Appointments::MET_STATUS_MET.'"', 'OR');

            return $this;
        }

        /**
         * Has Past Due
         *
         * Returns boolean on if the user id has overdue appointments that need to be inputted correctly.
         * @param $id
         * return boolean
         */
        public static function hasPastDue($id)
        {
            return (boolean) Appointments::model()->byPastDue()->countByAttributes(array('met_by_id'=>Yii::app()->user->id));

        }
        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('component_id', $this->component_id);
			$criteria->compare('set_by_id', $this->set_by_id);
			$criteria->compare('met_by_id', $this->met_by_id);
			$criteria->compare('met_status_ma', $this->met_status_ma);
			$criteria->compare('is_signed', $this->is_signed);
			$criteria->compare('not_signed_reason', $this->not_signed_reason);
			$criteria->compare('not_signed_reason_other', $this->not_signed_reason_other);
			$criteria->compare('set_activity_type_id', $this->set_activity_type_id);
			$criteria->compare('set_on_datetime', $this->set_on_datetime, true);
			$criteria->compare('set_for_datetime', $this->set_for_datetime, true);
			$criteria->compare('location_ma', $this->location_ma);
			$criteria->compare('location_other', $this->location_other, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
            ));
		}
	}