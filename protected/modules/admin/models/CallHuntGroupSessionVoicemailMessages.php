<?php

/**
 * This is the model class for table "call_hunt_group_session_voicemail_messages".
 *
 * The followings are the available columns in table 'call_hunt_group_session_voicemail_messages':
 * @property string $id
 * @property string $call_hunt_group_session_id
 * @property string $voicemail_message_id
 *
 * The followings are the available model relations:
 * @property CallHuntGroupSessions $callHuntGroupSession
 * @property VoicemailMessages $voicemailMessage
 */
class CallHuntGroupSessionVoicemailMessages extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallHuntGroupSessionVoicemailMessages the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_hunt_group_session_voicemail_messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_hunt_group_session_id, voicemail_message_id', 'required'),
            array('call_hunt_group_session_id, voicemail_message_id', 'length', 'max'=>10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_hunt_group_session_id, voicemail_message_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callHuntGroupSession' => array(self::BELONGS_TO, 'CallHuntGroupSessions', 'call_hunt_group_session_id'),
            'voicemailMessage' => array(self::BELONGS_TO, 'VoicemailMessages', 'voicemail_message_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_hunt_group_session_id' => 'Call Hunt Group Session',
            'voicemail_message_id' => 'Voicemail Message',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('call_hunt_group_session_id',$this->call_hunt_group_session_id,true);
        $criteria->compare('voicemail_message_id',$this->voicemail_message_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}