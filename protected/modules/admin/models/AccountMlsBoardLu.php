<?php
Yii::import('seizethemarket.protected.components.phRets.phRets');

/**
 * This is the model class for table "account_mls_board_lu".
 *
 * The followings are the available columns in table 'account_mls_board_lu':
 *
 * @property integer  $account_id
 * @property integer  $mls_board_id
 * @property string   $feed_url
 * @property string   $feed_username
 * @property string   $feed_password
 * @property string   $listing_id_system_field
 * @property string   $office_id_system_field
 * @property string   $agent_id_system_field
 * @property string   $base_dmql_query
 * @property integer  $offset_support
 *
 * The followings are the available model relations:
 * @property Accounts $account
 */
class AccountMlsBoardLu extends StmBaseActiveRecord
{

    /** @var phRets $_mlsFeedClient */
    private $_mlsFeedClient;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return AccountMlsBoardLu the static model class
     */
    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {

        return 'account_mls_board_lu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'feed_url, feed_username, feed_password, listing_id_system_field, office_id_system_field, agent_id_system_field, base_dmql_query, offset_support',
                'required'
            ),
            array(
                'account_id, mls_board_id, offset_support',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'feed_url, base_dmql_query',
                'length',
                'max' => 127
            ),
            array(
                'feed_username, feed_password',
                'length',
                'max' => 63
            ),
            array(
                'listing_id_system_field, office_id_system_field, agent_id_system_field',
                'length',
                'max' => 31
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'account_id, mls_board_id, feed_url, feed_username, feed_password, listing_id_system_field, office_id_system_field, agent_id_system_field, base_dmql_query, offset_support',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'account' => array(
                self::BELONGS_TO,
                'Accounts',
                'account_id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'account_id'              => 'Account',
            'mls_board_id'            => 'Mls Board',
            'feed_url'                => 'Feed Url',
            'feed_username'           => 'Feed Username',
            'feed_password'           => 'Feed Password',
            'listing_id_system_field' => 'Listing Id System Field',
            'office_id_system_field'  => 'Office Id System Field',
            'agent_id_system_field'   => 'Agent Id System Field',
            'base_dmql_query'         => 'Base Dmql Query',
            'offset_support'          => 'Offset Support',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('mls_board_id', $this->mls_board_id);
        $criteria->compare('feed_url', $this->feed_url, true);
        $criteria->compare('feed_username', $this->feed_username, true);
        $criteria->compare('feed_password', $this->feed_password, true);
        $criteria->compare('listing_id_system_field', $this->listing_id_system_field, true);
        $criteria->compare('office_id_system_field', $this->office_id_system_field, true);
        $criteria->compare('agent_id_system_field', $this->agent_id_system_field, true);
        $criteria->compare('base_dmql_query', $this->base_dmql_query, true);
        $criteria->compare('offset_support', $this->offset_support);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getExampleListingData($listingId)
    {
        if(!YII_DEBUG) {
            return false;
        }

        $listingData = array();

        /** @var MlsBoards $mlsBoard */
        $mlsBoard    = Yii::app()->getUser()->getBoard();
        $mlsProperty = MlsProperties::model($mlsBoard->getPrefixedName())->findByPk($listingId);
        if (!empty($mlsProperty)) {
            $listingIdQuery       = "({$this->listing_id_system_field}={$listingId})";
            $propertyTypeLuResult = Yii::app()->stm_hq->createCommand(
                'SELECT name, feed_table_name FROM mls_property_type_board_lu lu INNER JOIN mls_property_types type ON lu.mls_property_type_id = type.id WHERE mls_board_id = :mlsBoardId AND mls_property_type_id = :mlsPropertyTypeId'
            )->queryRow(
                    $fetchAssociative = true, $params = array(
                        ':mlsBoardId'        => $mlsBoard->id,
                        ':mlsPropertyTypeId' => $mlsProperty->mls_property_type_id,
                    )
                );
            if (!empty($propertyTypeLuResult)) {
                list($resource, $class) = $propertyTypeLuResult;
                $resource='Property'; //@todo: probably need a column for this
                $class = $propertyTypeLuResult['feed_table_name'];
                $listingResults = $this->getMlsFeedClient()->SearchQuery($resource, $class, $listingIdQuery, array(), 1);
                $listingData = $this->getMlsFeedClient()->FetchRow($listingResults);
            }
        }

        return $listingData;
    }

    private function getMlsFeedClient()
    {
        if (empty($this->_mlsFeedClient)) {
            Yii::import('seizethemarket.protected.components.phRets.phRETS');
            $this->_mlsFeedClient = new phRETS();
            $this->_mlsFeedClient->SetParam('compression_enabled', true); //for Photos, enables gzip compression
            $this->_mlsFeedClient->Connect($this->feed_url, $this->feed_username, $this->feed_password);
            $this->_mlsFeedClient->offset_support = $this->offset_support;
        }

        return $this->_mlsFeedClient;
    }
}