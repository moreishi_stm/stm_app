<?php

/**
 * This is the model class for table "call_recordings".
 *
 * The followings are the available columns in table 'call_recordings':
 * @property integer $id
 * @property string $type
 * @property integer $contact_id
 * @property string $from_number
 * @property string $to_number
 * @property string $record_call_request_uuid
 * @property string $recording_id
 * @property string $recording_url
 * @property string $call_uuid
 * @property integer $recording_duration
 * @property integer $recording_duration_ms
 * @property integer $recording_start_ms
 * @property integer $recording_end_ms
 * @property string $record_message
 * @property string $title
 * @property integer $is_default
 * @property string $started
 * @property string $stopped
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Contacts $contact
 * @property CallSessions $callSessions
 */
class CallRecordings extends StmBaseActiveRecord
{
    const TYPE_VOICEMAIL_GREETING = 'Voicemail Greeting';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallRecordings the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_recordings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, contact_id, title', 'required'), //from_number, to_number, recording_id, recording_url, call_uuid
            array('id, contact_id, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, is_default, is_deleted', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>100),
            array('record_call_request_uuid', 'length', 'max'=>36),
            array('record_message, recording_url, started, stopped, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, type, contact_id, from_number, to_number, record_call_request_uuid, recording_id, recording_url, call_uuid, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, record_message, title, is_default, started, stopped, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'callSessions' => array(self::HAS_MANY, 'CallSessions', 'call_session_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'contact_id' => 'Contact',
            'record_call_request_uuid' => 'Record Call Request UUID',
            'recording_id' => 'Recording',
            'recording_url' => 'Recording Url',
            'call_uuid' => 'Call Uuid',
            'recording_duration' => 'Recording Duration',
            'recording_duration_ms' => 'Recording Duration Ms',
            'recording_start_ms' => 'Recording Start Ms',
            'recording_end_ms' => 'Recording End Ms',
            'record_message' => 'Record Message',
            'title' => 'Title',
            'is_default' => 'Is Default',
            'started' => 'Started',
            'stopped' => 'Stopped',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    public function getLabel()
    {
        return $this->title . ' (' . $this->contact->fullName.')';
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('type',$this->type);
        $criteria->compare('from_number',$this->from_number);
        $criteria->compare('to_number',$this->to_number);
        $criteria->compare('record_call_request_uuid',$this->record_call_request_uuid,true);
        $criteria->compare('recording_id',$this->recording_id,true);
        $criteria->compare('recording_url',$this->recording_url,true);

        //$criteria->addCondition('recording_url IS NOT NULL AND recording_url != ""');

        $criteria->compare('call_uuid',$this->call_uuid,true);
        $criteria->compare('recording_duration',$this->recording_duration);
        $criteria->compare('recording_duration_ms',$this->recording_duration_ms);
        $criteria->compare('recording_start_ms',$this->recording_start_ms);
        $criteria->compare('recording_end_ms',$this->recording_end_ms);
        $criteria->compare('record_message',$this->record_message,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('is_default',$this->is_default);
        $criteria->compare('started',$this->started,true);
        $criteria->compare('stopped',$this->stopped,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        $criteria->order = 'title ASC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}