<?php

/**
 * This is the model class for table "sms_messages".
 *
 * The followings are the available columns in table 'sms_messages':
 *
 * @property integer $id
 * @property integer $origin_type_ma
 * @property integer $component_type_id
 * @property integer $component_id
 * @property integer $sent_by_contact_id
 * @property string  $content
 * @property integer $to_phone_id
 * @property string  $from_phone_number
 * @property string  $to_phone_number
 * @property integer $direction 0 = out, 1 = in
 * @property string	 $vendor_response
 * @property string	 $message_uuid
 * @property string  $processed_datetime
 * @property integer $delivery_status_ma
 * @property integer $activity_log_id
 * @property integer $needs_response
 */

class SmsMessages extends StmBaseActiveRecord
{
    const DIRECTION_OUTBOUND = 1;
    const DIRECTION_INBOUND = 2;

	public $contactId;

	/**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return SmsMessages the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sms_messages';
    }

	/**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'component_type_id, component_id, from_phone_number, to_phone_id, content',
                'required','on' => 'outbound'
            ),
            array(
                'component_type_id, component_id, sent_by_contact_id, sent_to_contact_id, inbound_to_sms_number_id, activity_log_id',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'content',
                'length',
                'max' => 1550
            ),
            array(
                'component_type_id, component_id, to_phone_id, content, processed_datetime, content, from_phone_number, to_phone_number, activity_log_id, message_uuid',
                'safe'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, component_type_id, component_id, content, to_phone_id, from_phone_number, to_phone_number, processed_datetime, activity_log_id, message_uuid',
                'safe',
                'on' => 'search'
            ),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'toPhone' => array(
                self::BELONGS_TO,
                'Phones',
                'to_phone_id'
            ),
            'activityLog' => array(
                self::BELONGS_TO,
                'ActivityLog',
                'activity_log_id'
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'component_type_id' => 'Component Type',
            'component_id' => 'Component',
            'content' => 'Message:',
            'to_phone_id' => 'To:',
            'processed_datetime' => 'Processed Datetime',
        );
    }

	public function dateRange($opts = null, $field) {

        if ($opts == null) {
            $this->getDbCriteria()->mergeWith(array(
                    'condition' => $field . ' <= :date',
                    'params' => array(':date' => date('Y-m-d')),
                )
            );
        } else {
            if ($opts['to_date']) {
                $this->getDbCriteria()->mergeWith(array(
                        'condition' => $field . ' < :to_date',
                        'params' => array(':to_date' => date('Y-m-d', strtotime($opts['to_date'] . ' +1 day')))
                    )
                );
            }

            if ($opts['from_date']) {
                $this->getDbCriteria()->mergeWith(array(
                        'condition' => $field . ' >= :from_date',
                        'params' => array(':from_date' => $opts['from_date']),
                    )
                );
            }
        }

        return $this;
    }

    public static function getMessageCount($phone, $stmNumber)
    {
        return Yii::app()->db->createCommand()
            ->select('count(*)')
            ->from('sms_messages')
            ->where('to_phone_number like :toPhoneNumber AND from_phone_number like :fromPhoneNumber', array(':toPhoneNumber' => '1'.$stmNumber, ':fromPhoneNumber' => $phone))
            ->orWhere('to_phone_number like :toPhoneNumber2 AND from_phone_number like :fromPhoneNumber2', array(':toPhoneNumber2' => $phone, ':fromPhoneNumber2' => '1'.$stmNumber))
            ->queryScalar();
    }

    public static function getLastMessage($phone, $stmNumber)
    {
        return Yii::app()->db->createCommand()
            ->select('content')
            ->from('sms_messages')
            ->where('to_phone_number like :toPhoneNumber AND from_phone_number like :fromPhoneNumber', array(':toPhoneNumber' => '1'.$stmNumber, ':fromPhoneNumber' => $phone))
            ->orWhere('to_phone_number like :toPhoneNumber2 AND from_phone_number like :fromPhoneNumber2', array(':toPhoneNumber2' => $phone, ':fromPhoneNumber2' => '1'.$stmNumber))
            ->order('id DESC')
            ->queryScalar();
    }

    public static function getPhoneNameDisplay($phone)
    {
        $results = Yii::app()->db->createCommand()
            ->selectDistinct('TRIM(CONCAT(c.first_name, " ", c.last_name)) fullName')
            ->from('phones p')
            ->join('contacts c','c.id=p.contact_id')
            ->where('p.phone like :phone', array(':phone' => substr($phone,1)))
            ->queryAll();
        $string = '';
        if(count($results) == 0) {
            $string = Yii::app()->format->formatPhone(substr($phone, 1));
        }elseif(count($results) == 1) {
            $string = $results[0]['fullName'];
        }
        else {
            foreach($results as $i => $row) {
                $string .= ($string) ? " OR " : "";
                $string .= $results[$i]['fullName'];
            }
        }
        return $string;
    }
}