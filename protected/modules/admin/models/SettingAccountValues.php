<?php

	/**
	 * This is the model class for table "setting_account".
	 *
	 * The followings are the available columns in table 'setting_account':
	 *
	 * @property integer  $id
	 * @property integer  $account_id
	 * @property integer  $setting_id
	 * @property integer  $value
	 * @property integer  $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Settings $setting
	 * @property Accounts $account
	 */
	class SettingAccountValues extends StmBaseActiveRecord {

		public $data = array();
		const WEBSITE_FLYER_CONTACT_TYPE_SPECIFIC_CONTACT = 1;
		const WEBSITE_FLYER_CONTACT_TYPE_LEAD_ROUTE = 2;

        protected $_accountSettings;

        /**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return SettingAccount the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'setting_account_values';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, setting_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'value',
					'length',
					'max' => 65535
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, setting_id, value, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'setting' => array(
					self::BELONGS_TO,
					'Settings',
					'setting_id'
				),
				// 'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'setting_id' => 'Setting',
				'value' => 'Value',
				'is_deleted' => 'Is Deleted',
			);
		}

        protected function afterFind()
        {
            if (Settings::isDateField($this->setting_id)) {
                $this->value = Yii::app()->format->formatDate($this->value, StmFormatter::APP_DATE_FORMAT);
            }

            parent::afterFind();
        }

        protected function beforeSave()
        {
            if (Settings::isDateField($this->setting_id)) {
                $this->value = Yii::app()->format->formatDate($this->value, StmFormatter::MYSQL_DATE_FORMAT);
            }

            return parent::beforeSave();
        }

		/**
		 * __get magic getter wrapper for getValue
		 *
		 * @param  string $name setting name
		 *
		 * @return string
		 */
		// public function __get($name) {
		// 	$setting = $this->getValue($name);

		// 	if ($setting) {
		//  		return $setting->value;
		// 	}

		// 	return parent::__get($name);
		// }

		/**
		 * @return string result of account setting value
		 */
		public function getValue($name) {
			$settingId = Settings::model()->getField($name)->id;

			return $this->find(array(
					'condition' => 'account_id=:account_id AND setting_id=:setting_id',
					'params' => array(
						':account_id' => Yii::app()->user->accountId,
						':setting_id' => $settingId
					),
				)
			);
		}

        public function getSettingsById($accountId) {
            if (!$this->_accountSettings) {
                $accountSettings = SettingAccountValues::model()->findAllByAttributes(array('account_id'=>$accountId));

                if ($accountSettings) {
                    foreach ($accountSettings as $accountSetting) {
                        $this->_accountSettings[$accountSetting->setting->name] = $accountSetting->value;
                    }
                }
            }

            return (object) $this->_accountSettings;
        }

        /**
         * Get Settings Value by Account Setting ID
         * @param $accountId
         * @param $settingId
         *
         * @return mixed
         */
        public function getSettingsByAccountSettingId($accountId, $settingId)
        {
            if($model = SettingAccountValues::model()->findByAttributes(array('setting_id'=>$settingId, 'account_id'=>$accountId))) {
                return $model->value;
            }
            return false;
        }

		public static function getWebsiteFlyerContact() {

			$websiteFlyerSetting = Yii::app()->user->settings->website_flyer_contact;
			$websiteFlyerSetting = CJSON::decode($websiteFlyerSetting);
			if (true || $websiteFlyerSetting['type'] == SettingAccountValues::WEBSITE_FLYER_CONTACT_TYPE_SPECIFIC_CONTACT) {
				$agentOnDuty = Contacts::model()->findByPk(1);
			} else {
				$LeadRoute = LeadRoutes::model()->findByPk($websiteFlyerSetting['value']);
				$actingAgents = $LeadRoute->getActingAgents();
				$agentOnDuty = ($actingAgents) ? current($actingAgents) : null;
			}

			return $agentOnDuty;
		}

        public static function getOfficeInfo($accountId)
        {
            $officeInfo = array();
            $officeSettingIds = Settings::OFFICE_NAME_ID .','. Settings::OFFICE_ADDRESS_ID .','. Settings::OFFICE_CITY_ID .','. Settings::OFFICE_STATE_ID .','. Settings::OFFICE_ZIP_ID;
            $results = Yii::app()->db->createCommand("select setting_id, value from setting_account_values WHERE setting_id IN({$officeSettingIds}) AND account_id={$accountId} AND is_deleted=0")->queryAll();

            // all fields are not present return false;
            if(count($results) != 5) {
                return false;
            }

            foreach($results as $result) {
                switch($result['setting_id']) {
                    case Settings::OFFICE_NAME_ID:
                        $officeInfo['name'] = $result['value'];
                        break;

                    case Settings::OFFICE_ADDRESS_ID:
                        $officeInfo['address'] = $result['value'];
                        break;

                    case Settings::OFFICE_CITY_ID:
                        $officeInfo['city'] = $result['value'];
                        break;

                    case Settings::OFFICE_STATE_ID:
                        $officeInfo['state'] = $result['value'];
                        break;

                    case Settings::OFFICE_ZIP_ID:
                        $officeInfo['zip'] = $result['value'];
                        break;
                }
            }

            if($officeInfo['address'] && $officeInfo['city'] && $officeInfo['state'] && $officeInfo['zip']) {

                $officeInfo['fullAddress'] = $officeInfo['address'].', '.$officeInfo['city'].', '.AddressStates::getShortNameById($officeInfo['state']).' '.$officeInfo['zip'];
            }

            return $officeInfo;
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('setting_id', $this->setting_id);
			$criteria->compare('value', $this->value);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
