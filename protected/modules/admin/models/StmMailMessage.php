<?php
	include_once(Yii::getPathOfAlias('admin_module.extensions.') . DS . 'SimpleHtmlDom.php');

	/**
	 * YiiMailMessage class file.
	 *
	 * @author  Jonah Turnquist <poppitypop@gmail.com>
	 * @link    https://code.google.com/p/yii-mail/
	 * @package Yii-Mail
	 */

	/**
	 * Any requests to set or get attributes or call methods on this class that are
	 * not found in that class are redirected to the {@link Swift_Mime_Message}
	 * object.
	 *
	 * This means you need to look at the Swift Mailer documentation to see what
	 * methods are available for this class.  There are a <b>lot</b> of methods,
	 * more than I wish to document.  Any methods available in
	 * {@link Swift_Mime_Message} are available here.
	 *
	 * Documentation for the most important methods can be found at
	 * {@link http://swiftmailer.org/docs/messages}
	 *
	 * The YiiMailMessage component also allows using a shorthand for methods in
	 * {@link Swift_Mime_Message} that start with set* or get*
	 * For instance, instead of calling $message->setFrom('...') you can use
	 * $message->from = '...'.
	 *
	 * Here are a few methods to get you started:
	 * <ul>
	 *    <li>setSubject('Your subject')</li>
	 *    <li>setFrom(array('john@doe.com' => 'John Doe'))</li>
	 *    <li>setTo(array('receiver@domain.org', 'other@domain.org' => 'Name'))</li>
	 *    <li>attach(Swift_Attachment::fromPath('my-document.pdf'))</li>
	 * </ul>
	 *
	 * Three possible scenarios exists for StmMailMessages.
	 * 1) E-mail from a agent to a contact (customer). (Reply-to: agent name w/ pattern)
	 * 2) E-mail from a contact to a agent. (Reply-to: contact name w/ imap pattern)
	 * 3) E-mail from a agent to agent.
	 */
	class StmMailMessage extends YiiMailMessage {

		const BOUNCED_EMAIL_ADDRESS = 'bounced@seizethemarket.com';
		const DEBUG_EMAIL_ADDRESS = 'debug@seizethemarket.com';//'debug@seizethemarket.com';

        /** @var $emailLinks Stores a collection of email links */
        public static $emailLinks = array();

		public $view = 'stmEmail';
		public $contactEmail;
		public $agentEmail;
		public $emailMessage;

		// Iff _debug is set to null, then default to the state of YII_DEBUG
		private $_debug = null;

        /** @var Domains $_replyToDomain Used as the base to send emails from */
        private $_replyToDomain;


		/**
		 * @param  EmailMessage $emailMessage Instance of the EmailMessage model, this provides access to the contact and agent needed for the message
		 * @param array         $additionalBodyContextParams
         * @param Domains       $replyToDomain The email to use for the "reply to" domain
		 * @return null
		 */
		public function init(EmailMessage $emailMessage = null, $additionalBodyContextParams = array(), Domains $replyToDomain = null) {

            if ($replyToDomain instanceof Domains) {
                $this->setReplyToDomain($replyToDomain);
            }

			$this->emailMessage = $emailMessage;

			$toEmail = $this->emailMessage->toEmail;
			$fromEmail = $this->emailMessage->fromEmail;
			$toContact = $toEmail->contact;
			$fromContact = $fromEmail->contact;

			// Need a valid contact to continue
            if(empty($toContact)) {
                Yii::log(__FILE__."(".__LINE__.") ERROR: toContact for email is empty. ", CLogger::LEVEL_WARNING);
                return false;
            }


            $toAddress = $this->getToAddress($toEmail->email);
			$this->addTo($toAddress);

            $fromContactNameProperCase = Yii::app()->format->formatProperCase($fromContact->getFullName());
			$replyToEmail = ($emailMessage) ? $this->getReplyToEmail($fromEmail, $fromContact) : $fromEmail;
            $this->setFrom(array($replyToEmail => $fromContactNameProperCase));

			if (($ccList = $this->emailMessage->getCcList())) {
                foreach ($ccList as $ccEmail) {
                    $this->addCc($ccEmail);
                }
			}
			if (($bccList = $this->emailMessage->getBccList())) {
                foreach ($bccList as $bccEmail) {
                    $this->addBcc($bccEmail);
                }
			}

			$this->setSubject($this->emailMessage->subject);
			$this->setBody(CMap::mergeArray(array(
						'content' => $this->emailMessage->content,
						'Contact' => $toContact,
					), $additionalBodyContextParams
				), 'text/html'
			);

			// Set the return path, so if a message is undeliverable for any reason the return address is infallible.
//            $zendMessage->addHeader('Content-Type', 'text/html; charset=UTF-8');
			$this->getHeaders()->addPathHeader('Return-Path', $this->replaceEmailWithClientDomain(self::BOUNCED_EMAIL_ADDRESS));
		}

		/**
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @param Emails    $email
		 * @param  Contacts $contact Contact model instance
		 * @return string E-mail address
		 */
		public function getReplyToEmail(Emails $email, Contacts $contact) {

            $contactName = str_replace(' ', '', strtolower($contact->getFullName()));
			$contactName = preg_replace('/\(.*\)/', '', $contactName); // this left comma on the name ex. andyprokes,md
            $contactName = preg_replace('/[^a-zA-Z0-9]/', '', $contactName); //@todo: clee put in here, chris to checkout
			$componentId = $this->emailMessage->component_id;
			$componentTypeId = $this->emailMessage->component_type_id;
			$fromEmailId = $email->id;
            $replyToEmailPtrn = ':contactName-:messageId-:componentTypeId-:componentId-:fromEmailId@:replyToDomain';
            $replyToEmail = strtr($replyToEmailPtrn, array(
                ':contactName' => $contactName,
                ':messageId' => $this->emailMessage->id,
                ':componentTypeId' => $componentTypeId,
                ':componentId' => $componentId,
                ':fromEmailId' => $fromEmailId,
                ':replyToDomain' => $this->getReplyToDomain()->name,
            ));

            return $replyToEmail;
		}

        public function setReplyToDomain(Domains $replyToDomain) {
            $this->_replyToDomain = $replyToDomain;
        }

        public function getReplyToDomain() {
            if (empty($this->_replyToDomain)) {
                $this->_replyToDomain = Yii::app()->user->getReplyToDomain();
            }

            return $this->_replyToDomain;
        }

		/**
		 * Allows us to send test emails to ourselves
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @param $email
		 * @return string
		 */
		public function getToAddress($email) {
			return ($this->getDebug() === true) ? self::DEBUG_EMAIL_ADDRESS : $email;
		}

		/**
		 * Return the state of the debugger
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @param $debugState
		 */
		public function setDebug($debugState) {
			$this->_debug = $debugState;
		}

		/**
		 * Iff debug is not set before accessing, refer to YII_DEBUG for current state
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @return boo|
		 */
		public function getDebug() {
			if ($this->_debug === null) {
				$this->_debug = YII_DEBUG;
			}

			return $this->_debug;
		}

        /**
         * @author Chris Willard <chris@seizethemarket.com>
         * @since  1.0
         * @param string $body
         * @param null $contentType
         * @param null $charset
         */
        public function setBody($body = '', $contentType = null, $charset = 'utf-8') {
			parent::setBody($body, $contentType, $charset);

			$messageBody = $this->message->getBody();
            if (!$this->emailMessage->content) {
                $this->emailMessage->content = $messageBody;
                $this->emailMessage->save();
            }

            $trackingLinkBody = $this->replaceLinksForTracking($messageBody);
			$this->message->setBody($trackingLinkBody, $contentType, $charset);
		}

        /**
         * Replace Links For Tracking
         *
         * Replaces links in emails so that they can be funnelled through the tracking action
         * @param $content Email message body to perform find/replace on
         * @return string Email message body after find/replace
         */
        public function replaceLinksForTracking($content)
        {
            // Handle errors internally
            libxml_use_internal_errors(true);

            // Load content
            $dom = new DOMDocument('1.0', 'UTF-8');
            $dom->formatOutput = false;
            @$dom->loadHTML($content);

            // Replace all links found in content
            $tags = $dom->getElementsByTagName('a');
            foreach($tags as $tag) {

                // Skip this if for some reason we don't have a href attribute
                if(!$tag->getAttribute('href')) {
                    continue;
                }

                // Update the attribute
                $tag->setAttribute('href', 'http://www.'.$this->getReplyToDomain()->name.'/click2/?data='.EmailClickTracking::encodeLink(array(
                    'contactId'         =>  $this->emailMessage->toEmail->contact->id,
                    'emailMessageId'    =>  $this->emailMessage->id,
                    'url'               =>  $tag->getAttribute('href')
                )));
            }

            // Perform find and replace
            return $dom->saveHTML();
        }

        public static function urlSafeBase64Decode($value) {
            return base64_decode(str_replace(array('-', '_' ,'*'), array('+', '/' ,'='), $value));
        }

        /**
         * Replaces 'test@test.com' with the client's current for_emails domain: 'test@christineleeteam.com'
         * @param $emailAddress
         *
         * @return mixed
         */
        private function replaceEmailWithClientDomain($emailAddress) {

            $replyToDomain = $this->getReplyToDomain();
            return preg_replace('/(.*)@.*/i', "$1@{$replyToDomain->name}", $emailAddress);
        }
	}
