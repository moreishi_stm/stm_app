<?php

	/**
	 * This is the model class for table "transaction_fields".
	 *
	 * The followings are the available columns in table 'transaction_fields':
	 *
	 * @property integer                     $id
	 * @property integer                     $component_type_id
	 * @property string                      $name
	 * @property string                      $label
	 * @property integer                     $data_type_id
	 * @property integer                     $sort_order
	 * @property integer                     $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property TransactionFieldAccountLu[] $transactionFieldAccountLus
	 * @property TransactionFieldValues[]    $transactionFieldValues
	 * @property ComponentTypes              $componentType
	 * @property DataTypes                   $dataType
	 */
	class TransactionFields extends StmBaseActiveRecord {

        const PRICE_MIN_BUYER = 1;
        const PRICE_MAX_BUYER = 2;
        const LOCATION_BUYER = 9;
        const BEDROOMS_BUYER = 10;
        const BATHS_BUYER = 11;
        const SQ_FEET_BUYER = 12;
        const ESTIMATED_VALUE = 27;

		// const PRICE_ORIGINAL_SELLER = 63;
        const AREA_NEIGHBORHOOD_SELLER = 46;
		const BEDROOMS_SELLER = 47;
		const BATHS_SELLER = 48;
		const SQ_FEET_SELLER = 50;
        const FEATURES_SELLER = 45;

        const PREV_EXPIRED_MLS_NUMBER_SELLER = 53;
        const PREV_EXPIRED_DATE_SELLER = 54;
        const PREV_EXPIRED_PRICE_SELLER = 55;
        const CURRENT_PRICE_SELLER = 57;

	    const SELLER_MLS_NUMBER = 59;
		// const IVR_NUM_SELLER = 59;
		const SELLER_IVR_EXTENSION = 61;
        const SELLER_LOCKBOX_NUMBER = 62;
        const SELLER_SHOWING_INSTRUCTIONS = 65;
        const SELLER_LISTING_EXPIRED_DATE = 66;
		const SELLER_LISTING_DATE = 67;
		// const MET_SELLER = 55;
		// const APPT_SELLER = 53;

		const SELLER_CANCEL_DATE = 69;


        const ZIP_CODES_BUYER = 75;

        const YEAR_BUILT_SELLER = 76;
        const EXPIRED_LISTING_OFFICE_SELLER = 77;
        const EXPIRED_LISTING_AGENT_SELLER = 78;
        const EXPIRED_AGENT_REMARKS_SELLER = 79;


        const IMPORT_EXTERNAL_IDENTIFIER_BUYER = 100;
        const IMPORT_EXTERNAL_IDENTIFIER_SELLER = 101;
        const MARKET_SNAPSHOT_FREQUENCY = 102;
        const REAL_ESTATE_TAX_ID = 103;
        const PREVIOUS_SALE_DATE = 104;
        const PREVIOUS_SALE_PRICE = 105;
        const PREVIOUS_SALE_OWNER = 106;

		// const MET_BUYER = 8;
		// const APPT_BUYER = 9;

		public $transactionFields = array(); // Collection

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TransactionFields the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'transaction_fields';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'component_type_id, data_type_id, sort_order, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 100
				),
				array(
					'label',
					'length',
					'max' => 150
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, component_type_id, name, label, data_type_id, sort_order, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				// 'transactionFieldAccountLus' => array(self::HAS_MANY, 'TransactionFieldAccountLu', 'transaction_field_id'),
				'transactionFieldValues' => array(
					self::HAS_MANY,
					'TransactionFieldValues',
					'transaction_field_id'
				),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
				// 'dataType' => array(self::BELONGS_TO, 'DataTypes', 'data_type_id'),
			);
		}

		/**
		 * Caches fields by transaction type id
		 *
		 * @param  string  $name            Searches the index by the 'name' column in transaction_fields
		 * @param  integer $componentTypeId (optional) This can be set by this method or in the model
		 *
		 * @return object A standard object containing all of the values pertaining to the field
		 */
		public function getField($name, $componentTypeId = null) {
			if (!$componentTypeId && !$this->component_type_id) {
				return false;
			}

			if (!$this->transactionFields) {
				$componentTypeId = (!$componentTypeId) ? $this->component_type_id : $componentTypeId; // If the type was stored in the model

				foreach (TransactionFields::model()->componentType($componentTypeId)->findAll() as $TransactionField) {
					$this->transactionFields[$TransactionField->name] = (object)$TransactionField->attributes;
				}
			}

			if ($this->transactionFields[$name]) {
				return $this->transactionFields[$name];
			}

			throw new CHttpException(500, 'Could not find field ' . $name . ' related to this form.');
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'component_type_id' => 'Component Type',
				'name' => 'Name',
				'label' => 'Label',
				'data_type_id' => 'Data Type',
				'sort_order' => 'Sort Order',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Returns if the field type is a number based field
		 *
		 * @return boolean
		 */
		public function getIsNumberField() {
			return in_array($this->data_type_id, array(
					DataTypes::INTEGER_ID,
					DataTypes::DECIMAL_ID,
					DataTypes::DOLLARS_ID
				)
			);
		}

		/**
		 * Returns if the field type is a number based field
		 *
		 * @return boolean
		 */
		public function getIsDateField() {
			return in_array($this->data_type_id, array(DataTypes::DATE_ID));
		}

		public function getIsDateTimeField() {
			return in_array($this->data_type_id, array(DataTypes::DATETIME_ID));
		}
		/**
		 * Scope
		 *
		 * @param  int $componentTypeId Component Type ID to filter the request by
		 *
		 * @return CActiveRecord
		 */
		public function componentType($componentTypeId) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'component_type_id = :component_type_id',
					'params' => array(
						':component_type_id' => $componentTypeId,
					),
				)
			);

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('label', $this->label, true);
			$criteria->compare('data_type_id', $this->data_type_id);
			$criteria->compare('sort_order', $this->sort_order);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
