<?php

/**
 * This is the model class for table "ivrs".
 *
 * The followings are the available columns in table 'ivrs':
 * @property string $id
 * @property integer $account_id
 * @property string $name
 * @property string $description
 * @property integer $call_hunt_group_id
 * @property string $speech
 * @property integer $do_speech
 * @property string $record_call_request_uuid
 * @property string $record_api_id
 * @property string $recording_id
 * @property string $recording_call_uuid
 * @property string $record_url
 * @property string $record_message
 * @property integer $recording_duration
 * @property integer $recording_duration_ms
 * @property string $recording_start_ms
 * @property string $recording_end_ms
 * @property string $recording_added
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property IvrExtensions[] $ivrExtensions
 * @property Accounts $account
 * @property Contacts $addedBy
 * @property Contacts $updatedBy
 */
class Ivrs extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Ivrs the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ivrs';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, name, call_hunt_group_id, added_by, added', 'required'),
            array('account_id, do_speech, recording_duration, recording_duration_ms, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('name, record_url, record_message', 'length', 'max'=>100),
            array('record_call_request_uuid, record_api_id, recording_id, recording_call_uuid', 'length', 'max'=>36),
            array('recording_start_ms, recording_end_ms', 'length', 'max'=>16),
            array('description, speech, recording_added, updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, account_id, name, description, speech, do_speech, record_call_request_uuid, record_api_id, recording_id, recording_call_uuid, record_url, record_message, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, recording_added, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ivrExtensions' => array(self::HAS_MANY, 'IvrExtensions', 'ivr_id'),
            'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'account_id' => 'Account',
            'name' => 'Name',
            'description' => 'Description',
            'call_hunt_group_id' => 'Call Hunt Group Default',
            'speech' => 'Text to Voice Content',
            'do_speech' => 'Text to Voice Enabled',
            'record_call_request_uuid' => 'Record Call Request Uuid',
            'record_api_id' => 'Record Api',
            'recording_id' => 'Recording',
            'recording_call_uuid' => 'Recording Call Uuid',
            'record_url' => 'Record Url',
            'record_message' => 'Record Message',
            'recording_duration' => 'Recording Duration',
            'recording_duration_ms' => 'Recording Duration Ms',
            'recording_start_ms' => 'Recording Start Ms',
            'recording_end_ms' => 'Recording End Ms',
            'recording_added' => 'Recording Added',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('account_id',$this->account_id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('speech',$this->speech,true);
        $criteria->compare('do_speech',$this->do_speech);
        $criteria->compare('record_call_request_uuid',$this->record_call_request_uuid,true);
        $criteria->compare('record_api_id',$this->record_api_id,true);
        $criteria->compare('recording_id',$this->recording_id,true);
        $criteria->compare('recording_call_uuid',$this->recording_call_uuid,true);
        $criteria->compare('record_url',$this->record_url,true);
        $criteria->compare('record_message',$this->record_message,true);
        $criteria->compare('recording_duration',$this->recording_duration);
        $criteria->compare('recording_duration_ms',$this->recording_duration_ms);
        $criteria->compare('recording_start_ms',$this->recording_start_ms,true);
        $criteria->compare('recording_end_ms',$this->recording_end_ms,true);
        $criteria->compare('recording_added',$this->recording_added,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}