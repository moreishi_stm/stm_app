<?php

	/**
	 * This is the model class for table "email_templates".
	 *
	 * The followings are the available columns in table 'email_templates':
	 *
	 * @property integer               $id
	 * @property integer               $component_type_id
	 * @property integer               $status_ma
	 * @property string                $name
	 * @property string                $subject
     * @property string                $description
	 * @property string                $body
     * @property integer               $include_signature
     * @property string                $bombBomb_id
	 * @property integer               $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property ActionPlanItems[]     $actionPlanItems
	 * @property CompanyTypes          $componentType
	 * @property TaskEmailTemplateLu[] $taskEmailTemplateLus
     * @property EmailTemplateAttachments[] $attachments
	 */
	class EmailTemplates extends StmBaseActiveRecord
    {
        public $searchContent;
        public $skipVideoConvert;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return EmailTemplates the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'email_templates';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'component_type_id, subject, include_signature',
					'required'
				),
				array(
					'component_type_id, status_ma, include_signature, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'description, subject',
					'length',
					'max' => 250
				),
                array(
                    'body',
                    'checkMicrosoftTags'
                ),
				array(
					'id, body, searchContent, bombBomb_id',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, component_type_id, status_ma, description, searchContent, subject, body, include_signature, bombBomb_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'actionPlanItems' => array(
					self::HAS_MANY,
					'ActionPlanItems',
					'email_template_id'
				),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
				'taskEmailTemplateLus' => array(
					self::HAS_MANY,
					'TaskEmailTemplateLu',
					'email_template_id'
				),
                'attachments'            => array(
                    self::HAS_MANY,
                    'EmailTemplateAttachments',
                    'email_template_id'
                ),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'component_type_id' => 'Component Type',
				'status_ma' => 'Status',
				'description' => 'Description',
				'subject' => 'Subject',
				'body' => 'Body',
				'is_deleted' => 'Is Deleted',
			);
		}

		public static function getByComponentTypeId($id) {
			return EmailTemplates::model()->findAll(array(
					'condition' => 'component_type_id=:component_type_id',
					'params' => array(':component_type_id' => $id),
                    'order'=>'subject ASC',
				)
			);
		}

        protected function afterSave()
        {
            if(!$this->skipVideoConvert || $this->isNewRecord) {
                $this->convertVideoIframesToImage();
                $this->setIsNewRecord(false);
                $this->skipVideoConvert = true;
                $this->save();
            }
            parent::afterSave();
        }

        /**
		 * filterBodyByContactData
		 *
		 * @param Contacts $contact
		 *
		 * @return mixed|null|string Body content filtered by the map provided
		 */
        public function filterBodyByContactData(Contacts $contact, $componentModel = null) {

			// If we do not have any content then we can't filter it
			if (!$this->body) {
				return null;
			}

			// Maps a replacement name to the contact public property
			$contactTplMap = array(
				'{{first_name}}' => $contact->first_name,
				'{{last_name}}' => $contact->last_name,
                '{{website}}' => Yii::app()->user->primaryDomain->name,
                '{{appointment_date}}' => '',
                '{{seller_address}}'    => '',
                '{{seller_city}}' => '',
                '{{seller_state}}' => '',
                '{{seller_zip}}' => '',
                '{{recruit_units}}' => '',
                '{{recruit_volume}}' => '',
                '{{recruit_volume_change}}' => '',
                '{{recruit_volume_change_percent}}' => '',
            );

            if($componentModel && get_class($componentModel) == 'Recruits') {
                $contactTplMap['{{recruit_units}}'] = $componentModel->annual_units;
                $contactTplMap['{{recruit_volume}}'] = Yii::app()->format->formatDollars($componentModel->annual_volume);
                $contactTplMap['{{recruit_volume_change}}'] = Yii::app()->format->formatDollars($componentModel->annual_volume_change);
                $contactTplMap['{{recruit_volume_change_percent}}'] = $componentModel->annual_volume_change_percent.'%';
            }

            // Add in appointment data if we have any
            /** @var Appointments $appointment */
            try {
                $appointment = $componentModel->appointment;
                if($appointment) {
                    $contactTplMap['{{appointment_date}}'] = date("m/d/Y g:i a", strtotime($appointment->set_for_datetime));
                }
            }
            catch(Exception $e) {
//                Yii::log(__CLASS__.' (:'.__LINE__.') Unknown situation. Address is not available in model!', CLogger::LEVEL_ERROR);
            }

            // If we have a seller address, add values for that
            try {
                $address = $componentModel->address;
                if($address) {
                    $contactTplMap['{{seller_address}}'] = $address->address;
                    $contactTplMap['{{seller_city}}'] = $address->city;
                    $contactTplMap['{{seller_state}}'] = $address->state->name;
                    $contactTplMap['{{seller_zip}}'] = $address->zip;
                }
            }
            catch(Exception $e) {
//                Yii::log(__CLASS__.' (:'.__LINE__.') Unknown situation. Address is not available in model!', CLogger::LEVEL_ERROR);
            }

			// Build out the arrays the str_replace function needs based off a easier to read/understand map
			$tplSearchMap = $replacementMap = array();
			foreach ($contactTplMap as $tplVarName => $contactAttr) {
				array_push($tplSearchMap, $tplVarName);
				array_push($replacementMap, $contactAttr);
			}

			// Replace the template data with the contact data
			$replacementContent = str_replace($tplSearchMap, $replacementMap, $this->body, $count);

			// Sanity check and make sure replacements actually occurred
			if (!$count) {
				$replacementContent = $this->clearTemplateTags();
			}

			return $replacementContent;
		}

        public function checkMicrosoftTags($attribute, $params)
        {
            $msTagCount = substr_count(strtolower($this->$attribute), 'mso');
            $msoNormal = substr_count(strtolower($this->$attribute), 'msonormal');
            $msTagCount2 = substr_count($this->$attribute, '<w:');

            if ($msoNormal || $msTagCount > 10 || $msTagCount2 > 10) {
                $this->addError($attribute, 'Invalid hidden tags have been detected. This is commonly caused by a cut/paste from a Microsoft document. The proper way to do this is a 2 step process. <br>1) Copy/paste to a text editor like Notepad. This eliminates the hidden tags. <br>2) Copy/paste from Notepad to this form.');
            }
        }

        /**
         * Converts any Video Iframes in body to image for youtube and Bombbomb
         * @param $attribute
         * @param $params
         */
        public function convertVideoIframesToImage()
        {
            include_once(Yii::getPathOfAlias('admin_module.extensions.') . DS . 'SimpleHtmlDom.php');

            // replace youtube iframes
            preg_match_all("/<iframe[^>]*><\/iframe>/ismU", $this->body, $matches);
            if(!empty($matches[0])) {

                // if domain is blank, the whole url structure breaks. Most likely due to console environment.
                if(!Yii::app()->user->primaryDomain->name) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Primary domain is blank. Required for video email link creation. May be due to console environment. Check variables. Investigate immediately. Attributes: '.print_r($this->attributes, true));
                }

                foreach($matches[0] as $iframe) {

                    // extract width & height, default can be 300w x 250h
                    preg_match("/width=\"(.*)\"/ismU", $iframe, $matchFrame);
                    $width = trim($matchFrame[1]);

                    preg_match("/height=\"(.*)\"/ismU", $iframe, $matchFrame);
                    $height = trim($matchFrame[1]);

                    preg_match("/class=\"(.*)\"/ismU", $iframe, $matchFrame);
                    $class = trim($matchFrame[1]);

                    //preg_match("/name=\"(.*)\"/ismU", $iframe, $matchFrame);
                    //$name = trim($matchFrame[1]);

                    preg_match("/id=\"(.*)\"/ismU", $iframe, $matchFrame);
                    $id = trim($matchFrame[1]);

                    preg_match("/src=\"(.*)\"/ismU", $iframe, $matchFrame);
                    $src = trim($matchFrame[1]);

                    //use all patterns to see if it's youtube
                    $youtubeKey = StmFunctions::getYoutubeKey($src);

                    switch($class) {

                        case 'youtube_video':
                            $videoType = 'youtube';
                            //<iframe class="youtube_video" src="http://www.youtube.com/embed/ABBsOupIy-E" width=" 300" height=" 250" frameborder="0"></iframe>
                            //<iframe class="youtube_video" type="text/html" class="youtube_video" height="250" width="300" src="http://www.youtube.com/embed/72gi5SAIkTg" frameborder="0"></iframe>
                            $id = $youtubeKey;

                            $newSource = 'http://s3.amazonaws.com/bbemail/youtube_posters/'.$id.'.jpg'; //youtube_posters/aD1xPiSgns8.jpg
                            break;

                        case 'bbVideoIframe':
                            $videoType = 'bombbomb';
                            //<iframe class="bbVideoIframe" width="300" height="225" src="http://bbemaildelivery.com/bbext/?p=vidEmbed&id=cd1c6afd-88b6-940b-279f-ad89e27aa49f" frameborder="0" scrolling="no"></iframe>
                            preg_match("/id=(.*)\"/ismU", $iframe, $matchFrame);
                            $id = trim($matchFrame[1]);

                            $newSource = 'http://s3.amazonaws.com/bbemail/PROD/video_thumbs/'.$id.'.jpg';
                            break;

                        case 'responsive_image':
                            $videoType = 'bombbomb';
                            if(strpos($src, 'http://s3.amazonaws.com/bbemail/') !== false) {
                                $class = 'bbVideoIframe';
                                $newSource = 'http://s3.amazonaws.com/bbemail/PROD/video_thumbs/'.$id.'.jpg'; //PROD/video_thumbs/cd1c6afd-88b6-940b-279f-ad89e27aa49f.jpg
                            }
                            else {
                                Yii::log(__CLASS__.' (:'.__LINE__.') Unknown situation. Pls review IFrame: '.$iframe, CLogger::LEVEL_ERROR);
                            }
                            break;

                        default:
                            // is youtube that is NOT bombbomb
                            if($class=='' && $youtubeKey) {
                                $videoType = 'youtube';
                                $class = 'youtube_video';
                                $youtubeThumbnail = StmFunctions::getYouTubePlayThumbnail($youtubeKey, $width, $height, true);
                                $newSource = $youtubeThumbnail; //'http://i1.ytimg.com/vi/'.$youtubeKey.'/mqdefault.jpg';
                                $id = $youtubeKey;
                                break;
                            }
                            else {
                                Yii::log(__CLASS__.' (:'.__LINE__.') Unknown IFrame class. IFrame text: '.$iframe, CLogger::LEVEL_ERROR);
                                continue;
                            }
                    }

                    //default height & width
                    $width = ($width) ? $width : '600';
                    $height = ($height) ? $height : '400';

                    //http://link2.bblink.co/bbext/?p=land&id=2288116424C44BB3E0530100007FF3D8&vid=cd1c6afd-88b6-940b-279f-ad89e27aa49f
                    /**
                     * NOTE: DO NOT CHANGE THIS without update VideoEmailAction on Front Module. It uses this very specific pattern to pull variables out. It will break without it!
                     */
                    $bbidString = ($bbid) ? '&bbid='.$bbid : '';
                    if($videoType=='youtube' && strpos($newSource, 'http://s3.amazonaws.com/bbemail') === false) {
                        $replaceWith = '<a href="http://www.'.Yii::app()->user->primaryDomain->name.'/videoEmail?vid='.$id.'&vtype='.$videoType.'&eid='.$this->id.'&etid='.StmFunctions::click3Encode($this->id).$bbidString.'">';
                        $replaceWith .= '<img class="'.$class.' stmVideoEmail" id="'.$id.'" src="'.$newSource.'" width="'.$width.'" height="'.$height.'"/></a>';
                    }
                    else {
                        $replaceWith = '<a href="http://www.'.Yii::app()->user->primaryDomain->name.'/videoEmail?vid='.$id.'&vtype='.$videoType.'&eid='.$this->id.'&etid='.StmFunctions::click3Encode($this->id).$bbidString.'"><img class="'.$class.' stmVideoEmail" id="'.$id.'" src="'.$newSource.'" width="'.$width.'" height="'.$height.'"/></a>';
                    }

                    // replace both
                    $this->body = str_replace($iframe, $replaceWith, $this->body);
                }
            }
        }

		/**
		 * clearTemplateTags
		 *
		 * @internal Used to clear the tags when filtering is not possible
		 * @return string Body content without any template tags: {{name}}
		 */
		public function clearTemplateTags() {

			return preg_replace('/{{.*}}/', '', $this->body);
		}

        public function getDropDownDisplay()
        {
            return "$this->subject | ".(($this->description) ? "(Desc: ".$this->description.")" : "")." ID# $this->id";
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

            if($this->searchContent) {
                $criteria->compare('description', $this->searchContent, true, 'OR');
                $criteria->compare('subject', $this->searchContent, true, 'OR');
                $criteria->compare('body', $this->searchContent, true, 'OR');

//                $criteria->addCondition('subject like ":subject" OR description like ":description" OR body like ":body"');
//                $criteria->params = CMap::mergeArray($criteria->params, array(':subject'=>"%{$this->searchContent}%",':description'=>"%{$this->searchContent}%",':body'=>"%{$this->searchContent}%"));
            }

            $criteria->compare('id', $this->id);

			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('status_ma', $this->status_ma);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('subject', $this->subject, true);
			$criteria->compare('body', $this->body, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'sort'=>array('defaultOrder'=>'t.subject ASC'),
                'pagination'=>array(
                    'pageSize'=> 100,
                ),
			));
		}
	}