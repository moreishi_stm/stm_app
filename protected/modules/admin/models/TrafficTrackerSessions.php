<?php

	/**
	 * This is the model class for table "traffic_tracker_sessions".
	 *
	 * The followings are the available columns in table 'traffic_tracker_sessions':
	 *
	 * @property integer          $id
	 * @property string           $session_id
	 *
	 * The followings are the available model relations:
	 * @property TrafficTracker[] $trafficTrackers
	 */
	class TrafficTrackerSessions extends CActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TrafficTrackerSessions the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'traffic_tracker_sessions';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'session_id',
					'required'
				),
				array(
					'session_id',
					'length',
					'max' => 50
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, session_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( //			'trafficTrackers' => array(self::HAS_MANY, 'TrafficTracker', 'session_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'session_id' => 'Session',
			);
		}

		public static function getIdBySessionId($sessionId) {
			if ($model = TrafficTrackerSessions::model()->find(array(
					'condition' => 'session_id=:session_id',
					'params' => array(':session_id' => $sessionId)
				)
			)
			) {
				return $model->id;
			} else {
				$model = new TrafficTrackerSessions;
				$model->session_id = $sessionId;
				$model->save();

				return $model->id;
			}
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('session_id', $this->session_id, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}