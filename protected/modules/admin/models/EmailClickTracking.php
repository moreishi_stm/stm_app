<?php

	/**
	 * This is the model class for table "email_click_tracking".
	 *
	 * The followings are the available columns in table 'email_click_tracking':
	 *
	 * @property integer       $id
	 * @property integer       $email_message_id
	 * @property integer       $contact_id
	 * @property integer       $email_link_id
     * @property integer       $email_template_id
     * @property integer       $status_ma
     * @property integer       $updated_by
     * @property string        $updated
	 * @property string        $added
	 *
	 * The followings are the available model relations:
	 * @property EmailLinks    $emailLink
	 * @property Contacts      $contact
	 * @property EmailMessage $emailMessage
	 */
	class EmailClickTracking extends StmBaseActiveRecord
    {
        const ID_ENCRYPTION_KEY = '505645f6639c4d1a8f16a87866935b60917610fb';

        const STATUS_NO_FOLLOW_UP_ID = 0;
        const STATUS_FOLLOWED_UP_ID = 1;

        /** @var $uniqueClicks can be used as a alias to derive unique contact click throughs */
		public $uniqueClicks;

        public $fromDate;
        public $toDate;

        public $prevUpdated;
        protected $_mostRecentClick;
        public $componentTypeId;

        /**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return EmailClickTracking the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'email_click_tracking';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id, url',
					'required'
				),
				array(
					'email_message_id, contact_id, email_link_id, email_template_id, status_ma, updated_by, componentTypeId',
					'numerical',
					'integerOnly' => true
				),
                array(
                    'url, added, updated',
                    'safe'
                ),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, email_message_id, contact_id, email_link_id, email_template_id, status_ma, url, added, updated, componentTypeId',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'emailLink' => array(
					self::BELONGS_TO,
					'EmailLinks',
					'email_link_id'
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
                'updatedBy' => array(
                    self::BELONGS_TO,
                    'Contacts',
                    'updated_by'
                ),
                'emailTemplate' => array(
                    self::BELONGS_TO,
                    'EmailTemplates',
                    'email_template_id'
                ),
                'emailMessage' => array(
					self::BELONGS_TO,
					'EmailMessage',
					'email_message_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'email_message_id' => 'Email Message',
				'contact_id' => 'Contact',
				'email_link_id' => 'Email Link',
                'email_template_id' => 'Email Template ID',
				'added' => 'Added',
			);
		}

		public function scopes() {
			return array(
				'orderByAddedDesc' => array(
					'order' => 'added DESC'
				),
				'byUniqueContacts' => array(
					'select' => 'COUNT(DISTINCT t.contact_id) AS uniqueClicks',
				),
			);
		}

		public function byEmailMessageId($id) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'email_message_id=:email_message_id';
			$Criteria->params = array(':email_message_id' => $id);

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function byEmailMessageComponentTypeId($id, $together=true) {
			$Criteria = new CDbCriteria;
			$Criteria->with = 'emailMessage';
			$Criteria->together = $together;
			$Criteria->condition = 'emailMessage.component_type_id=:component_type_id';
			$Criteria->params = array(':component_type_id' => $id);

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function byContactId($contactId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function byDateRange($dateRange) {
			if($dateRange['from_date']) {
				$Criteria = new CDbCriteria;
				$Criteria->condition = 'added>=:from_date';
				$Criteria->params = array(':from_date' => $dateRange['from_date']);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			if($dateRange['to_date']) {
				$Criteria = new CDbCriteria;
				$Criteria->condition = 'added<=:to_date';
				$Criteria->params = array(':to_date' => $dateRange['to_date']);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}

		protected function beforeSave() {

			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');
			}

			return parent::beforeSave();
		}

        /**
         * Encode Link
         *
         * Encodes an array of data in to a link
         * @param $data array Data to encode
         * @return string Encoded data
         * @throws Exception When an invalid argument has been passed
         */
        public static function encodeLink($data)
        {
            // Make sure we have a proper data structure to encode
            if(!is_array($data)) {
                throw new Exception('Data must be an array');
            }

            $encodedLink = urlencode(Yii::app()->format->urlSafeBase64Encode(Yii::app()->getSecurityManager()->encrypt(json_encode($data), self::ID_ENCRYPTION_KEY)));
            //@todo: this is temporary. Log everything for 1 month until 2014-10-01. When we see error, we can see where it originated from by looking at the logs.
            Yii::log(__FILE__.'('.__LINE__.'): '.date('Y-m-d H:i:s').': Log of encoded link & data. Data array values: '.print_r($data, true), CLogger::LEVEL_INFO, 'clickTrackLog');

            return $encodedLink;
        }

        /**
         * Decode Link
         *
         * @param $data string Data to decode
         * @return array Decoded data
         * @throws Exception When no data has been passed
         */
        public static function decodeLink($data)
        {
            // Make sure we have data to decode
            if(empty($data)) {
                throw new Exception('Invalid argument for data');
            }
            // if -_, characters exist, it indicates that it uses the new urlSafeBase64Encode, this will soon replace all the other none sense below
            // if +/= do NOT exist, use the new method
            if(preg_match('/-|_|,/', $data) || !preg_match('/\+|\/|\=/', $data)) {
                // $x = Yii::app()->format->urlSafeBase64Encode(CJSON::encode(array('contactId'=>31883, 'emailMessageId'=>123456, 'url'=>'http://www.christineleeteam.com/home/Jacksonville/FL/32257')));
                //@todo: this is the new safe url base64 code as of 08-31-2014. Later we can remove the else statement when it's phased out 2014-10-01
                $jsonString = Yii::app()->getSecurityManager()->decrypt(Yii::app()->format->urlSafeBase64Decode($data), self::ID_ENCRYPTION_KEY); //@todo: to implement 2014-08-31
                $decodedLinkData = json_decode($jsonString);

                switch(json_last_error()) {
                    case JSON_ERROR_DEPTH:
                        // Maximum stack depth exceeded
                        Yii::log(__FILE__.'('.__LINE__.'): JSON max stack depth error. String input: '.$jsonString, CLogger::LEVEL_ERROR);
                        break;
                    case JSON_ERROR_CTRL_CHAR:
                        // Unexpected control character found
                        Yii::log(__FILE__.'('.__LINE__.'): JSON unexpected control character found. String input: '.$jsonString, CLogger::LEVEL_ERROR);
                        break;
                    case JSON_ERROR_SYNTAX:
                        // Syntax error, malformed JSON
                        Yii::log(__FILE__.'('.__LINE__.'): JSON decode syntax, malformed error. String input: '.$jsonString, CLogger::LEVEL_ERROR);
                        break;
                    case JSON_ERROR_UTF8:
                        Yii::log(__FILE__.'('.__LINE__.'): JSON decode error. Malformed UTF-8 characters. String input: '.$jsonString, CLogger::LEVEL_ERROR);

                        if(strpos($jsonString, '"}') !== false) {
                            $modifiedJsonString = substr($jsonString, 0, strpos($jsonString, '"}')+2);
                            $decodedLinkData = json_decode($modifiedJsonString);
                            return (array) $decodedLinkData;
                        }
                        break;
                    case JSON_ERROR_NONE:
                        return (array) $decodedLinkData;
                        break;
                    default:
                        Yii::log(__FILE__.'('.__LINE__.'): Unknown JSON decode error. JSON Error #: '.json_last_error().' String input: '.$jsonString, CLogger::LEVEL_ERROR);
                        break;
                }

            }
            else {
                // log to see if anyone is still using this, check to see if these characters exist +/=
                //@todo: if there is no activity on this for 2 weeks, remove. As of 12/7/14
                Yii::log(__FILE__.'('.__LINE__.'): Old method of decode link is still being used. This was detected because it had the old characters of +/= used in the URL. Verify to see the cause and if the url is legit. Data value: '.$data, CLogger::LEVEL_ERROR);

                // run this both the new and old way and compare results
                switch(1) {
                    // check to see if the last character is } as test for valid json array
                    case strpos($data, '?utm_source=') !== false:
                        $jsonStringOld = Yii::app()->getSecurityManager()->decrypt(base64_decode($data), self::ID_ENCRYPTION_KEY);

                        //@todo: this is a TEMPORARY way of extracting good data from the unsafe base64 coding... it's not perfect but better than nothing.
                        $dataArray = explode('?utm_source=',$data);
                        $jsonString = Yii::app()->getSecurityManager()->decrypt(base64_decode($dataArray[0]), self::ID_ENCRYPTION_KEY);
                        //there are some weird characters at the end so going to cut it off
                        $cleanJsonString = substr($jsonString,0, strpos($jsonString,'}')+1);

                        if(json_decode($cleanJsonString) == null) {
                            preg_match('/"contactId":"([0-9]+)"/', $jsonString, $contactIdMatch);
                            if($contactIdMatch !== null && isset($contactIdMatch[1])) {
                                $contactId = $contactIdMatch[1];
                            }

                            preg_match('/"emailMessageId":"([0-9]+)"/', $jsonString, $emailMessageIdMatch);
                            if($emailMessageIdMatch !== null && isset($emailMessageIdMatch[1])) {
                                $emailMessageId = $emailMessageIdMatch[1];
                            }

                            preg_match('/"url":"(.*)"/', $jsonString, $urlMatch);
                            $urlSubStr = substr($jsonString, strpos($jsonString, '"url":')+6);
                            $urlLong = str_replace('"','',stripcslashes($urlSubStr));
                            $urlTemp = explode('&utm_content', $urlLong);
                            $url = $urlTemp[0];

                            $cleanJsonString = array();
                            $cleanJsonString['contactId'] = $contactId;
                            $cleanJsonString['emailMessageId'] = $emailMessageId;
                            $cleanJsonString['url'] = $url;

                            //returning here is bad practice, but this will be removed soon... after we make a "safe" base64 encoding method
                            if($contactId && $emailMessageId) {
                                return $cleanJsonString;
                            }
                        }

                        break;
                    default:
                        // decode both ways and see which one produces a valid json array which is checked by looking for } as last character. Remove old method after 2014-10-01
                        $jsonStringOld = Yii::app()->getSecurityManager()->decrypt(base64_decode($data), self::ID_ENCRYPTION_KEY);
                        $jsonStringNew = Yii::app()->getSecurityManager()->decrypt(Yii::app()->format->urlSafeBase64Decode($data), self::ID_ENCRYPTION_KEY);

                        if(substr($jsonStringOld, -1) !== '}' && substr($jsonStringNew, -1) !== '}') {
                            Yii::log(__FILE__.'('.__LINE__.'): Email click track error. Decoded string does not have brackets"{" or "}" and does not have "?utm_source=". Unexpected scenario. Json string using old method: '.$jsonStringOld.'. Json string using new method: '.$jsonStringNew, CLogger::LEVEL_ERROR);
                            break;
                        }

                        // check to see if new string has valid start and end json characters
                        if(substr($jsonStringOld, -1) == '}' && substr($jsonStringOld, 0, 1) == '{') {
                            $decodedLinkDataOld = json_decode($jsonStringOld);

                            switch(json_last_error()) {
                                case JSON_ERROR_DEPTH:
                                    // Maximum stack depth exceeded
                                    Yii::log(__FILE__.'('.__LINE__.'): JSON max stack depth error. String input: '.$jsonStringOld, CLogger::LEVEL_ERROR);
                                    break;
                                case JSON_ERROR_CTRL_CHAR:
                                    // Unexpected control character found
                                    Yii::log(__FILE__.'('.__LINE__.'): JSON unexpected control character found. String input: '.$jsonStringOld, CLogger::LEVEL_ERROR);
                                    break;
                                case JSON_ERROR_SYNTAX:
                                    // Syntax error, malformed JSON
                                    Yii::log(__FILE__.'('.__LINE__.'): JSON decode syntax, malformed error. String input: '.$jsonStringOld, CLogger::LEVEL_ERROR);
                                    break;
                                case JSON_ERROR_NONE:
                                    return (array) $decodedLinkDataOld;
                                    break;
                                default:
                                    Yii::log(__FILE__.'('.__LINE__.'): Unknown JSON decode error. JSON Error #: '.json_last_error().' String input: '.$jsonStringNew, CLogger::LEVEL_ERROR);
                                    break;
                            }

                        }

                        // check to see if new string has valid start and end json characters
                        if(substr($jsonStringNew, -1) == '}' && substr($jsonStringNew, 0, 1) == '{') {
                            $decodedLinkDataNew = json_decode($jsonStringNew);

                            switch(json_last_error()) {
                                case JSON_ERROR_DEPTH:
                                    // Maximum stack depth exceeded
                                    Yii::log(__FILE__.'('.__LINE__.'): JSON max stack depth error. String input: '.$jsonStringNew, CLogger::LEVEL_ERROR);
                                    break;
                                case JSON_ERROR_CTRL_CHAR:
                                    // Unexpected control character found
                                    Yii::log(__FILE__.'('.__LINE__.'): JSON unexpected control character found. String input: '.$jsonStringNew, CLogger::LEVEL_ERROR);
                                    break;
                                case JSON_ERROR_SYNTAX:
                                    // Syntax error, malformed JSON
                                    Yii::log(__FILE__.'('.__LINE__.'): JSON decode syntax, malformed error. String input: '.$jsonStringNew, CLogger::LEVEL_ERROR);
                                    break;
                                case JSON_ERROR_NONE:
                                    return (array) $decodedLinkDataNew;
                                    break;
                                case JSON_ERROR_UTF8:
                                    Yii::log(__FILE__.'('.__LINE__.'): JSON decode error. Malformed UTF-8 characters. String input: '.$jsonStringNew, CLogger::LEVEL_ERROR);
                                    // @todo: see if it contains pieces we need
                                    break;
                                default:
                                    Yii::log(__FILE__.'('.__LINE__.'): Unknown JSON decode error. JSON Error #: '.json_last_error().' String input: '.$jsonStringNew, CLogger::LEVEL_ERROR);
                                    break;
                            }

                        }
                        break;
                }
            }
        }

        public static function decodeDataNew($data) {

        }

        public static function decodeDataOld($data) {

        }

        /**
         * Stripped URL
         * Return url without any tracking info on it
         * @return mixed
         */
        public static function printGridUrl($url)
        {
            $urlParts = parse_url($url);
            $urlVars = array();
            $urlDesc = '';
            if($urlParts['query']) {
                parse_str($urlParts['query'], $urlVars);
            }

            $urlString = $urlParts['scheme'].'://'.$urlParts['host'].$urlParts['path'];

            // remove utm tracking links and make a separate line for it so it's not clicked on by user and throws off tracking
            if($urlVars['utm_source']) {
                $urlDesc .= 'Source: '.$urlVars['utm_source'];
                unset($urlVars['utm_source']);
            }

            if($urlVars['utm_medium']) {

                $urlDesc .=  ($urlDesc) ? ', ' : '';
                $urlDesc .= 'Medium: '.$urlVars['utm_medium'];
                unset($urlVars['utm_medium']);
            }

            if($urlVars['utm_content']) {

                $urlDesc .=  ($urlDesc) ? ', ' : '';
                $urlDesc .= 'Content: '.$urlVars['utm_content'];
                unset($urlVars['utm_content']);
            }

            if($urlVars['utm_campaign']) {

                $urlDesc .=  ($urlDesc) ? ', ' : '';
                $urlDesc .= 'Campaign: '.$urlVars['utm_campaign'];
                unset($urlVars['utm_campaign']);
            }

            $urlVarString = '';
            foreach($urlVars as $urlVarName => $urlVarValue) {
                $urlVarString .= ($urlVarString) ? '&' : '';
                $urlVarString .= $urlVarName.'='.$urlVarValue;
            }

            $urlString .=  ($urlVarString) ? $urlVarString : '';
            $urlString .=  ($urlDesc) ? '<br>('.$urlDesc.')' : '';

            return $urlString;
        }

        public function getStatusList()
        {
            return array(
                self::STATUS_NO_FOLLOW_UP_ID => 'Missing',
                self::STATUS_FOLLOWED_UP_ID => 'Done'
            );
        }

        public function getStatusName()
        {
            return $this->getStatusList()[$this->status_ma];
        }

        public function getMostRecentClick()
        {
            if(!$this->_mostRecentClick) {
                $criteria = new CDbCriteria();
                $criteria->compare('contact_id', $this->contact_id);
                $criteria->order = 'id DESC';
                $this->_mostRecentClick = $this->find($criteria);
            }
            return $this->_mostRecentClick;
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

            $componentTypeIdJoin = '';
            $componentTypeIdWhere = '';
            if($this->componentTypeId) {
//                $criteria->together = true;
                $criteria->with = array('emailTemplate');
                $criteria->compare('emailTemplate.component_type_id', $this->componentTypeId);
//                $componentTypeIdJoin = 'JOIN email_templates et ON et.id=ect.email_template_id';
//                $componentTypeIdWhere = ' AND et.component_type_id='.intval($this->componentTypeId);
            }

            $criteria->select = "MAX(t.id) as id, status_ma, contact_id, url, MAX(added) as added, (select updated FROM email_click_tracking ect WHERE ect.status_ma=1 AND ect.contact_id=t.contact_id AND ect.updated > 0 AND ect.updated > NOW() - INTERVAL 2 WEEK ORDER BY updated DESC LIMIT 1) as prevUpdated"; //MAX((select MAX(updated) FROM email_click_tracking WHERE contact_id=t.contact_id AND id <> t.id AND updated > NOW() - INTERVAL 2 WEEK AND updated > 0 ORDER BY id DESC LIMIT 1)) as prevUpdated
//            $criteria->select = "id, status_ma, contact_id, url, MAX(added) as added, (select updated FROM email_click_tracking WHERE contact_id=t.contact_id ORDER BY id DESC LIMIT 1) as updated, updated_by, MAX((select MAX(updated) FROM email_click_tracking WHERE contact_id=t.contact_id AND updated IS NOT NULL ORDER BY id DESC LIMIT 1)) as prevUpdated";  //can't to updated_by cuz the group doesn't pair with the same row as updated, it can mis match
			$criteria->compare('id', $this->id);
			$criteria->compare('email_message_id', $this->email_message_id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('email_link_id', $this->email_link_id);
            $criteria->compare('email_template_id', $this->email_template_id);

            if($this->fromDate) {
                $criteria->compare('DATE(added)', '>='.$this->fromDate, true);
            }
            if($this->toDate) {
                $criteria->compare('DATE(added)', '<='.$this->toDate, true);
            }

            $criteria->order = 'added DESC';
            $criteria->group = 'contact_id';

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=> 100,
                ),
			));
		}
	}