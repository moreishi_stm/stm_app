<?php

	/**
	 * This is the model class for table "AuthAssignment".
	 *
	 * The followings are the available columns in table 'AuthAssignment':
	 *
     * @property integer  $account_id
	 * @property string   $itemname
	 * @property string   $userid
	 * @property string   $bizrule
	 * @property string   $data
	 *
	 * The followings are the available model relations:
	 * @property AuthItem $itemname0
	 */
	class AuthAssignment extends StmBaseActiveRecord {

		public $oldRecord;
        public $itemnameCollection;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AuthAssignment the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'auth_assignment';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, itemname, userid',
					'required'
				),
				array(
					'itemname, userid',
					'length',
					'max' => 64
				),
				array(
					'bizrule, data, oldRecord, itemnameCollection',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'account_id, itemname, userid, bizrule, data, oldRecord, itemnameCollection',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'itemName' => array(self::BELONGS_TO, 'AuthItem', 'itemname'),
				 'contact' => array(self::BELONGS_TO, 'Contacts', 'userid'),
                 'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'itemname' => 'Itemname',
				'userid' => 'Userid',
				'bizrule' => 'Bizrule',
				'data' => 'Data',
			);
		}

		protected function afterFind()
		{
			$this->oldRecord=clone $this;
			parent::afterFind();
		}

        public function byIn($field, $values) {
            $values = StmFormatter::formatToArray($values);
            $this->getDbCriteria()->addInCondition($field, $values);

            return $this;
        }

        public function byAccountId($accountId) {
            $Criteria = new CDbCriteria;
            $Criteria->condition = 'contact.account_id=:account_id';
            $Criteria->params = array(':account_id'=>$accountId);
            $Criteria->with = array('contact');
            $Criteria->order = 'userid ASC';

            $Criteria->together = true;
            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

		protected function beforeSave() {
			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');
				 $this->added_by = Yii::app()->user->id;
                if(!$this->account_id) {
                    $this->account_id = Yii::app()->user->accountId;
                }
			}

			$this->updated = new CDbExpression('NOW()');
			$this->updated_by = Yii::app()->user->id;

			if((!$this->isNewRecord) AND ($this->itemname != $this->oldRecord->itemname)) {
				$this->addAssignmentLog();
			}

			return parent::beforeSave();
		}

		protected function addAssignmentLog() {
			$AuthAssignmentLog = new AuthAssignmentLogs;
			if (!$this->isNewRecord) {
				$AuthAssignmentLog->auth_item_name_old = $this->oldRecord->itemname;
			}

			$AuthAssignmentLog->auth_item_name_new = $this->itemname;
			$AuthAssignmentLog->userid = $this->userid;
			$AuthAssignmentLog->added_by = Yii::app()->user->id;
			$AuthAssignmentLog->added = new CDbExpression('NOW()');
			$AuthAssignmentLog->save();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('itemname', $this->itemname, true);
			$criteria->compare('userid', $this->userid, true);
			$criteria->compare('bizrule', $this->bizrule, true);
			$criteria->compare('data', $this->data, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}