<?php

	/**
	 * This is the model class for table "badge_awarded".
	 *
	 * The followings are the available columns in table 'badge_awarded':
	 *
	 * @property integer  $id
	 * @property integer  $badge_id
	 * @property integer  $contact_id
	 * @property integer  $points_redeemed
	 * @property string   $note
	 * @property integer  $updated_by
	 * @property string   $updated
	 * @property integer  $added_by
	 * @property string   $added
	 *
	 * The followings are the available model relations:
	 * @property Contacts $addedBy
	 * @property Badges   $badge
	 * @property Contacts $contact
	 * @property Contacts $updatedBy
	 */
	class BadgeAwarded extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return BadgeAwarded the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'badge_awarded';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'badge_id, contact_id, points_redeemed, updated_by, updated, added_by, added',
					'required'
				),
				array(
					'badge_id, contact_id, points_redeemed, updated_by, added_by',
					'numerical',
					'integerOnly' => true
				),
				array(
					'note',
					'length',
					'max' => 127
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, badge_id, contact_id, points_redeemed, note, updated_by, updated, added_by, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
				'badge' => array(
					self::BELONGS_TO,
					'Badges',
					'badge_id'
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'badge_id' => 'Badge',
				'contact_id' => 'Contact',
				'points_redeemed' => 'Points Redeemed',
				'note' => 'Note',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'added_by' => 'Added By',
				'added' => 'Added',
			);
		}

		public function isBadgeAwardedWithNewPoint(BadgePointsEarned $PointEarned) {
			// get list of badges that have a requirement with this point type
			$badgeIds = BadgeRequirements::getBadgesIdsByRequiedPointType($PointEarned->badge_point_type_id);

			$BadgesAwarded = BadgeAwarded::model()->byAwardee($PointEarned->contact_id)->byBadgeType($badgeIds)->findAll();
			//@todo: logic is incomplete, right now hard code for Thank You badge
			$BadgeToAward = new BadgeAwarded;
			$BadgeToAward->badge_id = Badges::THANK_YOU;
			$BadgeToAward->contact_id = $PointEarned->contact_id;
			$BadgeToAward->points_redeemed = 1;
			$BadgeToAward->note = $PointEarned->note;
			$BadgeToAward->save();

			return Badges::model()->findByPk(Badges::THANK_YOU); //@todo: this is temporary until any other badges are added
		}

		public function byAwardee($contactId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function byBadgeType($badgeIds) {
			$Criteria = new CDbCriteria;
			$Criteria->addInCondition('badge_id', $badgeIds);

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		protected function beforeValidate() {

			if ($this->isNewRecord) {
				$this->added_by = Yii::app()->user->id;
				$this->added = date('Y-m-d H:i:s');
				$this->updated_by = Yii::app()->user->id;
				$this->updated = Yii::app()->user->id;
			}

			return parent::beforeValidate();
		}

		protected function beforeSave() {

			$this->updated_by = Yii::app()->user->id;
			$this->updated = date('Y-m-d H:i:s');

			return parent::beforeSave();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('badge_id', $this->badge_id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('points_redeemed', $this->points_redeemed);
			$criteria->compare('note', $this->note, true);
			$criteria->compare('updated_by', $this->updated_by);
			$criteria->compare('updated', $this->updated, true);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}