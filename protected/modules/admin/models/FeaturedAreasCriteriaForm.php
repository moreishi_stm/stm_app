<?php

/**
 * FeaturedAreasCriteriaForm class.
 * FeaturedAreasCriteriaForm is the data structure for saved home searches.
 */
class FeaturedAreasCriteriaForm extends SavedHomeSearchForm
{
    /**
     * Required Columns To Remove
     *
     * Things that are required for the Saved Home Search Form but are not required for the Featured Areas Criteria Form
     * @var array
     */
    protected $_requiredColumnsToRemove = array(
//        'component_type_id',
//        'component_id',
        'contact_id',
        'frequency'
    );

    /**
     * Rules
     *
     * Overwrites parent definition in order to remove the required items that are not required for featured areas
     * @return array
     */
    public function rules()
    {
        // Modify things that are required by definition of rules
        $rules = parent::rules();
        foreach($rules as $key => $rule) {

            // If we have required items, remove things we don't need for featured areas criteria
            if($rule[1] == 'required') {

                // Parse out rules from string
                $ruleExplodes = array_map(trim, explode(',', $rule[0]));

                // Compute differences and remove the columns we don't want
                $bads = array_intersect($ruleExplodes, $this->_requiredColumnsToRemove);
                foreach($bads as $key2 => $bad) {
                    unset($ruleExplodes[$key2]);
                }

                // Implode back to string and replace rules value
                $rules[$key][0] = implode(', ', array_values($ruleExplodes));

                // Exit loop, we're done here
                break;
            }
        }

        // Return rules
        return $rules;
    }

}