<?php

	/**
	 * This is the model class for table "sources".
	 *
	 * The followings are the available columns in table 'sources':
	 *
	 * @property integer               $id
	 * @property integer               $account_id
	 * @property string                $name
     * @property integer               $follow_up_days
	 * @property integer               $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Referrals[]           $referrals
	 * @property SourceCampaigns[]     $sourceCampaigns
	 * @property Accounts              $account
	 * @property TransactionSourceLu[] $transactionSourceLus
	 */
	class Sources extends StmBaseActiveRecord {

        public $applyAccountScope = false;
        public $idCollection;
        public $excludeIds;

		// Website sources id
        const UNKNOWN = 1;
		const ID_WEBSITE = 17;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Sources the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'sources';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, parent_id, follow_up_days, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'description',
					'length',
					'max' => 127
				),
                array('excludeIds', 'safe'),

				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, excludeIds, idCollection, account_id, parent_id, name, description, follow_up_days, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contacts' => array(
					self::HAS_MANY,
					'Contacts',
					'source_id'
				),
				'account' => array(
					self::BELONGS_TO,
					'Accounts',
					'account_id'
				),
				'parent' => array(
					self::BELONGS_TO,
					'Sources',
					'parent_id'
				),
				// 'transactionSourceLus' => array(self::HAS_MANY, 'TransactionSourceLu', 'id'),

				// 'referrals' => array(self::HAS_MANY, 'Referrals', 'source_id'),
                'formSubmissions' => array(
                    self::HAS_MANY,
                    'FormSubmissions',
                    'source_id'
                ),
				'transactions' => array(
					self::HAS_MANY,
					'Transactions',
					'source_id'
				),
			);
		}

        public function defaultScope() {
            return CMap::mergeArray(parent::defaultScope(), array(
//                'order' => 'name ASC',
            ));
        }

        /**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'name' => 'Source',
				'description' => 'Description',
				'parent_id' => 'Parent',
				'is_deleted' => 'Is Deleted',
			);
		}

        protected function beforeSave()
        {
            if ($this->isNewRecord) {
                $this->added = new CDbExpression('NOW()');
            }

            return parent::beforeSave();
        }

		// public function getSubgroupList() {
		// 	$Criteria = new CDbCriteria;
		// 	$Criteria->condition = 'source_id=:source_id';
		// 	$Criteria->params = array(':source_id'=>$this->id);
		// 	$SubGroups = SourceCampaigns::model()->findAll($Criteria);
		// 	foreach ($SubGroups as $SubGroup) {
		// 		$list .= ($list) ? ', ' : '';
		// 		$list .= $SubGroup->name;
		// 	}
		// 	return $list;
		// }

		public static function getList() {
			return Sources::model()->findAll();
		}


		// public function scopes() {
		// 	return array(
		// 		'sourcesByTransactionType' => array(
		// 			'with' => 'source',
		// 			'select' => 'source.id, source.name',
		// 			'condition' => 'transaction.component_type_id IN(3,4)',
		// 			// 'params' => array(
		// 			// 	':component_type_id' => $this->component_type_id,
		// 			// ),
		// 		),
		// 	);
		// }

		public function findByName($name) {
			return $this->find(array(
					'condition' => 'name like :name',
					'params' => array(':name' => "%$name%")
				)
			);
		}

		public static function optGroupList($includeId=null)
        {
			$sources = Sources::model()->findAll(array('order' => 'name ASC,parent_id ASC'));

            if($includeId) {
                $includeSource = Sources::model()->skipSoftDeleteCheck()->findByPk($includeId);
                if($includeSource->is_deleted) {
                    array_push($sources, $includeSource);
                }
            }

			$sourcesList = CHtml::listData($sources, 'id', function ($source) { // Anon function for returning the source parent's html content in the dropdown
					if ($source->parent) {
						return $source->parent->name . ' - ' . $source->name;
					}

					return $source->name;
				}, function ($source) { // Determines the grouping
					return $source->parent->name;
				}
			);

			return $sourcesList;
		}

		public function byComponentTypeIds($componentIdList) {
			$ComponentIdCriteria = new CDbCriteria;
			$ComponentIdCriteria->with = array('transactions');
			$ComponentIdCriteria->addInCondition('component_type_id', $componentIdList);
			$ComponentIdCriteria->order = 't.name asc';
			$ComponentIdCriteria->group = 't.id';
			$this->getDbCriteria()->mergeWith($ComponentIdCriteria);

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

            // to ensure that "Unknown" ID #1 is protected and never changed. This can be later modified to be controlled by is_core field or similar flag.
            $criteria->addCondition('id>1');

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('parent_id', $this->parent_id);

			if ($this->name) {
				$criteria->compare('name', $this->name, true);
				$model = Sources::model()->findByName($this->name);
				$criteria->compare('parent_id', $model->id, false, $operator = 'OR');
			}

			$criteria->compare('description', $this->description, true);
			$criteria->compare('is_deleted', $this->is_deleted);
            $criteria->order = 'name ASC';

			return new CActiveDataProvider(Sources::model()->skipSoftDeleteCheck(), array(
				'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 100,
                ),
			));
		}

        public function searchList()
        {
            $command = Yii::app()->db->createCommand()
                ->select('s.id, s.name, s2.name parent_name,s.description, s.follow_up_days, s.is_deleted')
                ->from('sources s')
                ->leftJoin('sources s2','s2.id=s.parent_id')
                ->where('s.id>1')
                ->andWhere('s.account_id='.Yii::app()->user->accountId);

            if ($this->name) {
                $command->andWhere('s.name like :name', array(':name'=>'%'.$this->name.'%'));
            }

            if($this->is_deleted) {
                $command->andWhere('s.is_deleted=1');
            }
            else {
                $command->andWhere('s.is_deleted=0');
            }

            $command->order('s.name ASC');
            $data = $command->queryAll();

            return new CArrayDataProvider($data, array(
                'pagination' => array(
                    'pageSize' => 100,
                ),
            ));
        }

	}
