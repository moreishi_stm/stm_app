<?php

	/**
	 * This is the model class for table "lead_group_contact_lu".
	 *
	 * The followings are the available columns in table 'lead_group_contact_lu':
	 *
	 * @property integer    $id
	 * @property integer    $lead_group_id
	 * @property integer    $contact_id
	 * @property integer    $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property LeadGroups $leadGroup
	 * @property Contacts   $contact
	 */
	class LeadRouteContactLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Lead the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'lead_route_contact_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'lead_route_id, contact_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, lead_route_id, contact_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( // 'leadGroup' => array(self::BELONGS_TO, 'LeadGroups', 'lead_group_id'),
				// 'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'lead_route_id' => 'Lead Route',
				'contact_id' => 'Contact',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('lead_route_id', $this->lead_route_id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}