<?php

	/**
	 * This is the model class for table "form_submit_values".
	 *
	 * The followings are the available columns in table 'form_submit_values':
	 *
	 * @property integer     $id
	 * @property integer     $form_submission_id
	 * @property integer     $form_field_id
	 * @property integer     $value
	 *
	 * The followings are the available model relations:
	 * @property FormSubmits $formSubmit
	 */
	class FormSubmissionValues extends StmBaseActiveRecord {

		public $data = array();
        public $formPart;

		protected $cachedFields;

		public static $_fieldValues = array();

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FormSubmitValues the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'form_submission_values';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'form_submission_id, form_field_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'value',
					'length',
					'max' => 1000
				),
				// Form Rules
				array(
					$this->getDataFieldRules(array(
							'email',
							'first_name',
							'last_name',
							'phone',
						)
					),
					'required',
					'on' => Forms::FORM_ASK_A_QUESTION . ',' . Forms::FORM_SHOWING_REQUEST . ',' . Forms::FORM_PROPERTY_TAX . ',' . Forms::FORM_VIDEO . ',' . Forms::FORM_HOME_DETAILS . ','
					. Forms::FORM_LOGIN_WIDGET_REGISTER . ',' . Forms::FORM_HOUSE_VALUES . ',' . Forms::FORM_MOVE_ONCE_PROGRAM . ',' . Forms::FORM_WE_BUY_HOUSES . ',' . Forms::FORM_GUARANTEED_SALE . ',' . Forms::FORM_FORCED_REG . ',' . Forms::FORM_STORYBOARD_SNEAK_PREVIEW. ','. Forms::FORM_PREQUAL.','.Forms::FORM_MAKE_AN_OFFER_HOME_DETAILS.','.Forms::FORM_CAREERS.','.Forms::FORM_BUYER_DREAM_HOME_SHORT.','.Forms::FORM_REFERRAL_REQUEST.','.Forms::FORM_MARKET_SNAPSHOT_QUESTION.','.Forms::FORM_FUNDRAISER.','.Forms::FORM_MARKET_SNAPSHOT_UPDATE_ESTIMATE.','.Forms::SELLER_INQUIRY.','.Forms::FORM_RECRUIT_FREE_CONSULT.','.Forms::FORM_CONTACT_US
				),
                array(
                    $this->getDataFieldRules(array(
                            'first_name',
                            'last_name',
                            'phone',
                        )
                    ),
                    'required',
                    'on' => Forms::RECRUIT_REFERRAL
                ),
				//            array($this->getDataFieldRules(array('password')), 'required', 'on'=>Forms::FORM_FORCED_REG),

				array(
					$this->getDataFieldRules(array('email')),
					'email',
					'checkMX' => true,
					'on' => (!YII_DEBUG) ? Forms::FORM_FORCED_REG.','.Forms::FORM_SHARE_FRIEND.','.Forms::FORM_HOUSE_VALUES.','.Forms::FORM_HOUSE_VALUES.'_Part2'.','.Forms::FORM_HOUSE_VALUES.'_Part2_details'.','.Forms::FORM_HOUSE_VALUES.'_Part_all_details,'.Forms::FORM_BUYER_DREAM_HOME_SHORT.','.Forms::FORM_FUNDRAISER.','.Forms::FORM_MARKET_SNAPSHOT_QUESTION.','.Forms::FORM_MARKET_SNAPSHOT_UPDATE_ESTIMATE.','.Forms::FORM_CONTACT_US.','.Forms::RECRUIT_REFERRAL : 'debug'
				),

                array(
                    $this->getDataFieldRules(array('multi_component_type')),
                    'required',
                    'on' => Forms::FORM_FUNDRAISER.','.Forms::FORM_CONTACT_US
                ),
				array(
					$this->getDataFieldRules(array('phone')),
					'numerical',
					'integerOnly' => true,
					'on' => Forms::FORM_ASK_A_QUESTION . ',' . Forms::FORM_FORCED_REG
				),
                array(
                    $this->getDataFieldRules(array('phone')),
                    'validatePhone',
                ),
                array(
                    $this->getDataFieldRules(array('first_name')),
                    'validateFullName',
                ),

                array(
                    $this->getDataFieldRules(array('email')),
                    'validateEmail',
                ),
                array(
                    $this->getDataFieldRules(array('zip')),
                    'validateZip',
                ),
//                array(
//                    $this->getDataFieldRules(array('phone')),
//                    'length',
//                    'min' => 10,
//                    'tooShort'=>'Please enter a valid Phone #.',
//                    'on' => Forms::FORM_ASK_A_QUESTION . ',' . Forms::FORM_FORCED_REG
//                ),

                array(
                    $this->getDataFieldRules(array('comments')),
                    'required',
                    'on' => Forms::FORM_REFERRAL_REQUEST.','.Forms::FORM_CONTACT_US
                ),

                array(
                    $this->getDataFieldRules(array('source_description')),
                    'required',
                    'on' => Forms::FORM_BUYER_DREAM_HOME_SHORT
                ),

                array(
					$this->getDataFieldRules(array('question')),
					'required',
					'on' => Forms::FORM_ASK_A_QUESTION.','.Forms::FORM_MARKET_SNAPSHOT_QUESTION.','.Forms::FORM_MARKET_SNAPSHOT_UPDATE_ESTIMATE.','.Forms::SELLER_INQUIRY //.','.Forms::FORM_VIDEO @todo: add video form after everyone is transitioned to standard2
				),

				/** The reason is required on the form_home_details **/
				array(
					$this->getDataFieldRules(array('reason')),
					'required',
					'on' => Forms::FORM_HOME_DETAILS
				),

				// Send to Friend has it's own set cuz it's so different
				array(
					$this->getDataFieldRules(array(
							'email',
							'first_name',
							'last_name',
							'to_first_name',
							'to_last_name',
							'to_email'
						)
					),
					'required',
					'on' => Forms::FORM_SHARE_FRIEND
				),
				array(
					$this->getDataFieldRules(array(
							'to_email'
						)
					),
					'email',
					'checkMX' => true,
					'on' => Forms::FORM_SHARE_FRIEND
				),


                // House Values Form Part 1
                array(
                    $this->getDataFieldRules(array(
                            'address',
                            'city',
                            'state',
                            'zip',
                        )
                    ),
                    'required',
                    'on' => Forms::FORM_HOUSE_VALUES.'_Part1'.','.Forms::FORM_HOUSE_VALUES.'_Part1_details'.','.Forms::FORM_HOUSE_VALUES.'_Part_all_details'.','.Forms::FORM_FUNDRAISER.','.Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part1_details'
                ),
                // House Values Form Part 2
                array(
                    $this->getDataFieldRules(array(
                            'email',
                            'phone',
                            'first_name',
                            'last_name'
                        )
                    ),
                    'required',
                    'on' => Forms::FORM_HOUSE_VALUES.'_Part2'.','.Forms::FORM_HOUSE_VALUES.'_Part2_details'.','.Forms::FORM_HOUSE_VALUES.'_Part_all_details'.','.Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part2'.','.Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part2B'
                ),

                // House Values Form Part 2
                array(
                    $this->getDataFieldRules(array(
                            'email',
                            'phone',
                            'full_name'
                        )
                    ),
                    'required',
                    'on' => Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part2'.','.Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part2B'
                ),

//                array(
//                    $this->getDataFieldRules(array('full_name')),
//                    'validateFullNameSingleField',
//                ),

                array(
                    $this->getDataFieldRules(array(
                            'bedrooms',
                            'baths',
                            'sq_feet',
                            'condition',
                        )
                    ),
                    'required',
                    'on' => Forms::FORM_HOUSE_VALUES.'_Part1_details'.','.Forms::FORM_HOUSE_VALUES.'_Part2_details'.','.Forms::FORM_HOUSE_VALUES.'_Part_all_details'.','.Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part2B'
                ),

                // House Values Form
				array(
					$this->getDataFieldRules(array(
							'address',
							'city',
							'state',
							'zip',
							'email',
							'first_name',
							'last_name'
						)
					),
					'required',
					'on' => Forms::FORM_MOVE_ONCE_PROGRAM . ',' . Forms::FORM_WE_BUY_HOUSES . ',' . Forms::FORM_GUARANTEED_SALE . ',' . Forms::FORM_HOUSE_VALUES . ',' . Forms::FORM_BUYER_STORYBOARD_OFFER_ME
				),

                array(
                    $this->getDataFieldRules(array('sq_feet')),
                    'numerical',
                    'integerOnly' => true,
                    'on' => Forms::FORM_HOUSE_VALUES.'_Part1_details'.','.Forms::FORM_HOUSE_VALUES.'_Part2_details'.','.Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part2B'
                ),

				array(
					$this->getDataFieldRules(array('zip')),
					'numerical',
					'integerOnly' => true,
					'on' => Forms::FORM_MOVE_ONCE_PROGRAM . ',' . Forms::FORM_WE_BUY_HOUSES . ',' . Forms::FORM_GUARANTEED_SALE . ',' . Forms::FORM_HOUSE_VALUES.','.Forms::FORM_HOUSE_VALUES_SIMPLE.'_Part2B'
				),
                array(
                    $this->getDataFieldRules(array('price')),
                    'required',
                    'on' => Forms::FORM_MAKE_AN_OFFER_HOME_DETAILS.','.Forms::FORM_MAKE_AN_OFFER_LISTING_STORYBOARD
                ),
                array(
                    $this->getDataFieldRules(array('is_expecting_call','is_licensed')),
                    'required',
                    'on' => Forms::RECRUIT_REFERRAL
                ),
                // House Values Form
                array(
                    $this->getDataFieldRules(array(
                            'referrer_first_name',
                            'referrer_last_name',
                            'referrer_email',
                            'referrer_phone',
                            'relationship',
                            'comments'
                        )
                    ),
                    'required',
                    'on' => Forms::RECRUIT_REFERRAL
                ),

                // The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, form_submission_id, form_field_id, value',
					'safe',
					'on' => 'search'
				),
			);
		}

		public function getDataFieldRules($ruleFieldList) {
			$rules = array();
			if (!is_array($ruleFieldList) || !$ruleFieldList) {
				return $rules;
			}

			foreach ($ruleFieldList as $field) {
				$field = $this->getDataField($field);
				if ($field) {
					array_push($rules, $field);
				}
			}

			return join(',', $rules);
		}

		/**
		 * Checks to see if the validators are trying to access keys of the public property 'data'.  If so then return the value
		 *
		 * @param  string $name
		 *
		 * @return mixed
		 */
		public function __get($name) {
			preg_match('/data\[(.*)\]/is', $name, $matches);

			if ($matches) {
				$dataKey = $matches[1];

				if (isset($this->data[$dataKey])) { // || array_key_exists($dataKey, $this->data)
					return $this->data[$dataKey];
				}
                else {
                    return null;
                }
			}

			return parent::__get($name);
		}

		/**
		 * Used to get the data key name for use with form rules validation
		 *
		 * @param  string $dataFieldName
		 *
		 * @return string data[data field name's pk]
		 */
		public function getDataField($dataFieldName) {
			if (!$this->cachedFields) {
				$FormFields = FormFields::model()->findAll();

				if (!$FormFields) {
					return;
				}

				foreach ($FormFields as $FormField) {
					$this->cachedFields[$FormField->name] = $FormField->id;
				}
			}

			return str_replace(':fieldKey', $this->cachedFields[$dataFieldName], 'data[:fieldKey]');
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'formSubmission' => array(
					self::BELONGS_TO,
					'FormSubmissions',
					'form_submission_id'
				),
				'formField' => array(
					self::BELONGS_TO,
					'FormFields',
					'form_field_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			$modelAttributes = array(
				'id' => 'ID',
				'form_submission_id' => 'Form Submit',
				'form_field_id' => 'Field',
				'value' => 'Value',
			);

			$dynamicLabels = $this->getDynamicLabels();

			return CMap::mergeArray($modelAttributes, $dynamicLabels);
		}

		public function getDynamicLabels() {
			$FormFields = FormFields::model()->findAll();

			if (!$FormFields) {
				return false;
			}

			$labels = array();
			foreach ($FormFields as $FormField) {
				$labels['data[' . $FormField->id . ']'] = $FormField->label;
			}

			return $labels;
		}

		protected function beforeValidate() {

			if (isset($this->data[FormFields::PHONE_ID])) {
				$this->data[FormFields::PHONE_ID] = str_replace(array(
						'(',
						')',
						'-',
						' ',
						'_'
					), '', $this->data[FormFields::PHONE_ID]
				);
			}

			return parent::beforeValidate();
		}

        public function validateFullName($attribute, $params)
        {
            if($this->isAttributeRequired($attribute)) {
                if($this->data[FormFields::FIRST_NAME_ID] == $this->data[FormFields::LAST_NAME_ID]) {
                    $this->addError($attribute, 'Please enter a valid Name.');
                    $this->addError('data['.FormFields::LAST_NAME_ID.']', 'Please enter a valid Name.');
                    return;
                }

                if(strlen($this->data[FormFields::FIRST_NAME_ID]) < 2) {
                    $this->addError($attribute, 'Please enter a valid Name.');
                    return;
                }
            }
        }

//        public function validateFullNameSingleField($attribute, $params)
//        {
//            if($this->isAttributeRequired($attribute)) {
//                if($this->data[FormFields::FULL_NAME_ID] == $this->data[FormFields::LAST_NAME_ID]) {
//                    $this->addError($attribute, 'Please enter a valid Name.');
//                    $this->addError('data['.FormFields::LAST_NAME_ID.']', 'Please enter a valid Name.');
//                    return;
//                }
//
//                if(strlen($this->data[FormFields::FIRST_NAME_ID]) < 2) {
//                    $this->addError($attribute, 'Please enter a valid Name.');
//                    return;
//                }
//            }
//        }

        public function validateZip($attribute, $params) {
            if($this->isAttributeRequired($attribute) && strlen($this->$attribute) >0) {
                if(strlen($this->$attribute) < 5 || !is_numeric($this->$attribute)) {
                    $this->addError($attribute, 'Please enter a valid Zip.');
                }

            }
        }

        public function validateEmail($attribute, $params) {
            if($this->isAttributeRequired($attribute)) {
                if(strpos($this->data[FormFields::EMAIL_ID],array('@','.')==false) || strlen($this->data[FormFields::EMAIL_ID])<8) {
                    $this->addError($attribute, 'Please enter a valid Email.');
                }

            }
        }

        public function validatePhone($attribute, $params)
        {
            if($this->isAttributeRequired($attribute)) {
                $phone = str_replace(array(
                        '(',
                        ')',
                        '-',
                        ' ',
                        '_'
                    ), '', $this->data[FormFields::PHONE_ID]
                );

                if(strlen($phone) <10 && strlen($phone) >0) {
                    $this->addError($attribute, 'Please enter a valid Phone #.');
                    return;
                }

                $badPhones = array('1111111111','2222222222','3333333333','4444444444','5555555555','6666666666','7777777777','8888888888','9999999999','0000000000',);
                if (in_array($phone, $badPhones)) {
                    $this->addError($attribute, 'Please enter a valid Phone #.');
                    return;
                }

                $badShortPhones = array('1111111','2222222','3333333','4444444','5555555','6666666','7777777','8888888','9999999','0000000','5551212',);
                foreach($badShortPhones as $badShortPhone) {
                    if(strpos($phone, $badShortPhone)!==false) {
                        $this->addError($attribute, 'Please enter a valid Phone #.');
                        return;
                    }
                }

            }
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('form_submission_id', $this->form_submission_id);
			$criteria->compare('form_field_id', $this->form_field_id);
			$criteria->compare('value', $this->value);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		public function getData() {
			return $this->data;
		}

		public function setData($data = array()) {
			$this->data = $data;
		}

	/**
	 * Get Values By Submission Id
	 *
	 * Retrieves form submission values by field id, as a bonus it also caches them as to not be re-retrieved upon the next call for values during the current page execution
	 * @param $formSubmissionId
	 * @return mixed
	 */
	public static function getValuesBySubmissionId($formSubmissionId)
	{
		$fieldValues = array();

		// Do this if we haven't already done so
//		if(!array_key_exists($formSubmissionId, self::$_fieldValues)) {

			// Create this
		$fieldValues[(string)$formSubmissionId] = array();

			// Load up records
			$records = FormSubmissionValues::model()->findAllByAttributes(array(
				'form_submission_id'	=>	$formSubmissionId
			));

			// Catalog form submission values for easy lookup
			foreach($records as $record) {
				$fieldValues[(string)$formSubmissionId][(string)$record->form_field_id] = $record->value;
			}
//		}

		$stuff = $fieldValues[(string)$formSubmissionId];

		// Return the field values
		return $stuff;
	}
}

