<?php

interface LeadGenInterface {
	public function getComponentTypeId();
	public function getComponentId();
	public function retrieveContactInstance();
	public function retrieveEmailInstance();
	public function retrievePhoneInstance();
}