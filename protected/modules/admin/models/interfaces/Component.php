<?php

/**
 * The contract vis a vis a collection of methods which any
 * "Component" in the STM Application must adhere to.
 *
 */
interface Component {

	/**
	 * @return integer the contact id associated with this component, null otherwise.
	 */
	public function getContactId();

	/**
	 * @return array collection of component tuple representations formatted as described in the asComponentTuple function.
	 * @see asComponentTuple()
	 */
	public function getComponentChain();

	/**
	 * Returns a representation of this component as an associative array containing its
	 * componentTypeId and componentId keyed verbatim.
	 *
	 * @return array associative array representation of this component.
	 */
	public function asComponentTuple();
}
