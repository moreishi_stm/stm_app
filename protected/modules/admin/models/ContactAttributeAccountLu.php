<?php

	/**
	 * This is the model class for table "contact_attribute_account_lu".
	 *
	 * The followings are the available columns in table 'contact_attribute_account_lu':
	 *
	 * @property integer               $id
	 * @property integer               $account_id
	 * @property integer               $contact_attribute_id
	 * @property integer               $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Accounts              $account
	 * @property ContactAttributeTypes $contactAttribute
	 */
	class ContactAttributeAccountLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ContactAttributeAccountLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'contact_attribute_account_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, contact_attribute_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, contact_attribute_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'account' => array(
					self::BELONGS_TO,
					'Accounts',
					'account_id'
				),
				'contactAttribute' => array(
					self::BELONGS_TO,
					'ContactAttributes',
					'contact_attribute_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'contact_attribute_id' => 'Contact Attribute',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('contact_attribute_id', $this->contact_attribute_id);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}