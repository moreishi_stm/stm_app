<?php

/**
 * This is the model class for table "click_to_call_sessions".
 *
 * The followings are the available columns in table 'click_to_call_sessions':
 * @property string $id
 * @property string $call_uuid
 * @property integer $component_id
 * @property integer $component_type_id
 * @property string $to_number
 * @property string $from_number
 * @property string $answer_time
 * @property string $total_cost
 * @property string $bill_rate
 * @property integer $bill_duration
 * @property string $event
 * @property string $start_time
 * @property string $hangup_cause
 * @property string $end_time
 * @property integer $duration
 * @property string $recording_id
 * @property string $recording_url
 * @property integer $recording_duration
 * @property integer $recording_duration_ms
 * @property string $recording_start_ms
 * @property string $recording_end_ms
 * @property integer $activity_log_id
 * @property string $added
 * @property string $updated
 */
class ClickToCallSessions extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ClickToCallSessions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'click_to_call_sessions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_uuid, to_number, from_number, added', 'required'),
            array('component_id, component_type_id, bill_duration, duration, recording_duration, recording_duration_ms, activity_log_id', 'numerical', 'integerOnly'=>true),
            array('call_uuid, recording_id', 'length', 'max'=>36),
            array('to_number, from_number', 'length', 'max'=>32),
            array('total_cost, bill_rate', 'length', 'max'=>9),
            array('event', 'length', 'max'=>20),
            array('hangup_cause', 'length', 'max'=>50),
            array('recording_url', 'length', 'max'=>100),
            array('recording_start_ms, recording_end_ms', 'length', 'max'=>16),
            array('answer_time, start_time, end_time, updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, component_id, component_type_id, call_uuid, to_number, from_number, answer_time, total_cost, bill_rate, bill_duration, event, start_time, hangup_cause, end_time, duration, recording_id, recording_url, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, added, updated, activity_log_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activityLog' => array(
                self::BELONGS_TO,
                'ActivityLog',
                'activity_log_id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_uuid' => 'Call Uuid',
            'to_number' => 'To Number',
            'from_number' => 'From Number',
            'answer_time' => 'Answer Time',
            'total_cost' => 'Total Cost',
            'bill_rate' => 'Bill Rate',
            'bill_duration' => 'Bill Duration',
            'event' => 'Event',
            'start_time' => 'Start Time',
            'hangup_cause' => 'Hangup Cause',
            'end_time' => 'End Time',
            'duration' => 'Duration',
            'recording_id' => 'Recording',
            'recording_url' => 'Recording Url',
            'recording_duration' => 'Recording Duration',
            'recording_duration_ms' => 'Recording Duration Ms',
            'recording_start_ms' => 'Recording Start Ms',
            'recording_end_ms' => 'Recording End Ms',
            'added' => 'Added',
            'updated' => 'Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('call_uuid',$this->call_uuid,true);
        $criteria->compare('to_number',$this->to_number,true);
        $criteria->compare('from_number',$this->from_number,true);
        $criteria->compare('answer_time',$this->answer_time,true);
        $criteria->compare('total_cost',$this->total_cost,true);
        $criteria->compare('bill_rate',$this->bill_rate,true);
        $criteria->compare('bill_duration',$this->bill_duration);
        $criteria->compare('event',$this->event,true);
        $criteria->compare('start_time',$this->start_time,true);
        $criteria->compare('hangup_cause',$this->hangup_cause,true);
        $criteria->compare('end_time',$this->end_time,true);
        $criteria->compare('duration',$this->duration);
        $criteria->compare('recording_id',$this->recording_id,true);
        $criteria->compare('recording_url',$this->recording_url,true);
        $criteria->compare('recording_duration',$this->recording_duration);
        $criteria->compare('recording_duration_ms',$this->recording_duration_ms);
        $criteria->compare('recording_start_ms',$this->recording_start_ms,true);
        $criteria->compare('recording_end_ms',$this->recording_end_ms,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}