<?php

	/**
	 * This is the model class for table "forms".
	 *
	 * The followings are the available columns in table 'forms':
	 *
	 * @property integer         $id
	 * @property integer         $account_id
     * @property integer         $component_type_id
	 * @property integer         $form_type_ma
	 * @property string          $name
	 * @property integer         $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property FormAccountLu[] $formAccountLus
	 * @property FormViews[]     $formViews
	 * @property Accounts        $account
	 */
	class Forms extends StmBaseActiveRecord {

		// Define some constants to uniquely identify specific forms
		const FORM_CONTACT_US = 1;
		const FORM_SHOWING_REQUEST = 2;
		const FORM_SHARE_FRIEND = 3;
		const FORM_ASK_A_QUESTION = 4;
		const FORM_PROPERTY_TAX = 5;
		const FORM_HOUSE_VALUES = 6;
		const FORM_FORCED_REG = 7;
		const FORM_VIDEO = 10;
		const FORM_HOME_DETAILS = 11;
		const FORM_LOGIN_WIDGET_REGISTER = 12;
		const FORM_WE_BUY_HOUSES = 13;
		const FORM_MOVE_ONCE_PROGRAM = 14;
		const FORM_GUARANTEED_SALE = 15;
		const FORM_STORYBOARD_SNEAK_PREVIEW = 18;
        const FORM_PREQUAL = 19;
        const FORM_BUYER_STORYBOARD_OFFER_ME = 20;
        const FORM_MAKE_AN_OFFER_LISTING_STORYBOARD = 21;
        const FORM_MAKE_AN_OFFER_HOME_DETAILS = 22;
        const FORM_CAREERS = 23;
        const FORM_BUYER_DREAM_HOME_SHORT = 24;
        const FORM_REFERRAL_REQUEST = 25;
        const FORM_MARKET_SNAPSHOT_QUESTION = 26;
        const FORM_FUNDRAISER = 27;
        const FORM_MARKET_SNAPSHOT_UPDATE_ESTIMATE = 28;
        const SELLER_INQUIRY = 29;
        const RECRUIT_REFERRAL = 30;
        const FORM_RECRUIT_FREE_CONSULT = 31;
        const FORM_HOUSE_VALUES_SIMPLE = 32;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Forms the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'forms';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'form_type_ma, component_type_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name, alert_label',
					'length',
					'max' => 50
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, form_type_ma, component_type_id, name, alert_label, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'formAccountLus' => array(
					self::HAS_MANY,
					'FormAccountLu',
					'form_id'
				),
				'formViews' => array(
					self::HAS_MANY,
					'FormViews',
					'form_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'form_type_ma' => 'Form Type Ma',
				'name' => 'Name',
                'alert_label' => 'Alert Label',
				'is_deleted' => 'Is Deleted',
			);
		}

		public function findByName($name) {
			return $this->find(array(
					'condition' => 'name=:name',
					'params' => array(':name' => $name)
				)
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('form_type_ma', $this->form_type_ma);
			$criteria->compare('name', $this->name, true);
            $criteria->compare('alert_label', $this->alert_label, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
