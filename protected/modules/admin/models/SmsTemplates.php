<?php

	/**
	 * This is the model class for table "sms_templates".
	 *
	 * The followings are the available columns in table 'sms_templates':
	 *
	 * @property integer               $id
	 * @property integer               $component_type_id
	 * @property integer               $status_ma
	 * @property string                $name
     * @property string                $description
	 * @property string                $body
	 * @property integer               $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property ActionPlanItems[]     $actionPlanItems
	 * @property CompanyTypes          $componentType
	 * @property TaskEmailTemplateLu[] $taskEmailTemplateLus
	 */
	class SmsTemplates extends StmBaseActiveRecord {
        public $searchContent;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return EmailTemplates the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return "sms_templates";
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					"component_type_id, name",
					"required"
				),
				array(
					"component_type_id, status_ma, is_deleted",
					"numerical",
					"integerOnly" => true
				),
				array(
					"name",
					"length",
					"max" => 250
				),
				array(
					"description",
					"length",
					"max" => 63
				),
				array(
					"body",
					"length",
					"max" => 1550
				),
				array(
					"id, body, searchContent",
					"safe"
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					"id, component_type_id, status_ma, description, searchContent, body, is_deleted",
					"safe",
					"on" => "search"
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				"actionPlanItems" => array(
					self::HAS_MANY,
					"ActionPlanItems",
					"email_template_id"
				),
				"componentType" => array(
					self::BELONGS_TO,
					"ComponentTypes",
					"component_type_id"
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				"id" => "ID",
				"component_type_id" => "Component Type",
				"status_ma" => "Status",
				"description" => "Description",
				"body" => "Body",
				"is_deleted" => "Is Deleted",
			);
		}

		public static function getByComponentTypeId($id) {
			return SmsTemplates::model()->findAll(array(
					"condition" => "component_type_id=:component_type_id",
					"params" => array(":component_type_id" => $id),
                    "order"=>"name ASC",
				)
			);
		}

		/**
		 *
		 * Name filterBodyByContactData
		 * @param Contacts $contact
		 * @param null $componentModel
		 * @param null $body
		 * @return mixed|null|string
		 *
		 * @author RapidMod.com
		 * @author 813.330.0522
		 * @important, setting a body will set this objects body
		 */
		public function filterBodyByContactData(Contacts $contact, $componentModel = null)
        {
			// If we do not have any content then we can"t filter it
			if (!$this->body) {
				return null;
			}

			// Maps a replacement name to the contact public property
			$contactTplMap = array(
				"{{first_name}}" => $contact->first_name,
				"{{last_name}}" => $contact->last_name,
				"{{appointment_date}}" => "",
				"{{seller_address}}"    => "",
				"{{seller_city}}" => "",
				"{{seller_state}}" => "",
				"{{seller_zip}}" => "",
				"{{recruit_units}}" => "",
				"{{recruit_volume}}" => "",
				"{{recruit_volume_change}}" => "",
				"{{recruit_volume_change_percent}}" => "",
			);
			if($componentModel && get_class($componentModel) == "Recruits") {
				$contactTplMap["{{recruit_units}}"] = $componentModel->annual_units;
				$contactTplMap["{{recruit_volume}}"] = Yii::app()->format->formatDollars($componentModel->annual_volume);
				$contactTplMap["{{recruit_volume_change}}"] = Yii::app()->format->formatDollars($componentModel->annual_volume_change);
				$contactTplMap["{{recruit_volume_change_percent}}"] = $componentModel->annual_volume_change_percent."%";
			}

			// Add in appointment data if we have any
			/** @var Appointments $appointment */
			try {
				$appointment = $componentModel->appointment;
				if($appointment) {
					$contactTplMap["{{appointment_date}}"] = date("m/d/Y g:i a", strtotime($appointment->set_for_datetime));
				}
			}
			catch(Exception $e) {
//                Yii::log(__CLASS__." (:".__LINE__.") Unknown situation. Address is not available in model!", CLogger::LEVEL_ERROR);
			}
			try {
				$address = $componentModel->address;
				if($address) {
					$contactTplMap["{{seller_address}}"] = $address->address;
					$contactTplMap["{{seller_city}}"] = $address->city;
					$contactTplMap["{{seller_state}}"] = $address->state->name;
					$contactTplMap["{{seller_zip}}"] = $address->zip;
				}
			}
			catch(Exception $e) {
//                Yii::log(__CLASS__." (:".__LINE__.") Unknown situation. Address is not available in model!", CLogger::LEVEL_ERROR);
			}

			// Build out the arrays the str_replace function needs based off a easier to read/understand map
			$tplSearchMap = $replacementMap = array();
			foreach ($contactTplMap as $tplVarName => $contactAttr) {
				array_push($tplSearchMap, $tplVarName);
				array_push($replacementMap, $contactAttr);
			}

			// Replace the template data with the contact data
			$replacementContent = str_replace($tplSearchMap, $replacementMap, $this->body, $count);

			// Sanity check and make sure replacements actually occurred
			if (!$count) {
				$replacementContent = $this->clearTemplateTags();
			}

			return $replacementContent;
		}

		/**
		 * clearTemplateTags
		 *
		 * @internal Used to clear the tags when filtering is not possible
		 * @return string Body content without any template tags: {{name}}
		 */
		public function clearTemplateTags() {

			return preg_replace("/{{.*}}/", "", $this->body);
		}

        public function getDropDownDisplay() {

            return "$this->name - ($this->description) [ID# $this->id]";
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

            if($this->searchContent) {
                $criteria->compare("description", $this->searchContent, true, "OR");
                $criteria->compare("body", $this->searchContent, true, "OR");

//                $criteria->addCondition("subject like ":subject" OR description like ":description" OR body like ":body"");
//                $criteria->params = CMap::mergeArray($criteria->params, array(":subject"=>"%{$this->searchContent}%",":description"=>"%{$this->searchContent}%",":body"=>"%{$this->searchContent}%"));
            }

            $criteria->compare("id", $this->id);

			$criteria->compare("component_type_id", $this->component_type_id);
			$criteria->compare("status_ma", $this->status_ma);
			$criteria->compare("description", $this->description, true);
			$criteria->compare("body", $this->body, true);
			$criteria->compare("is_deleted", $this->is_deleted);

			return new CActiveDataProvider($this, array(
				"criteria" => $criteria,
                "sort"=>array("defaultOrder"=>"t.name ASC"),
                "pagination"=>array(
                    "pageSize"=> 100,
                ),
			));
		}
	}