<?php
	/**
	 * @deprecated
	 */
	class Listings extends Transactions {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Transactions the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function __construct($scenario = 'insert') {
			parent::__construct($scenario);
			$this->transactionType = Transactions::LISTINGS;
		}

		/**
		 * getListSearchModel provides reusable logic for preparing for the list view
		 *
		 * @return none
		 */
		public static function getListSearchModel() {
			$model = Transactions::newInstance(Transactions::LISTINGS, 'search');

			$model->unsetAttributes();
			$model->transaction_status_id = TransactionStatus::ACTIVE_LISTING_ID;

			$modelClass = get_class($model);

			// used for gridview search box inputs
			if (isset($_GET[$modelClass])) {
				$model->attributes = $_GET[$modelClass];
			}

			$model->contact = new Contacts('search');
			$model->contact->unsetAttributes();

			if (isset($_GET['Contacts'])) {
				$model->contact->attributes = $_GET['Contacts'];
			}

			$model->contact->emails = new Emails('search');
			$model->contact->emails->unsetAttributes();

			if (isset($_GET['Emails'])) {
				$model->contact->emails->attributes = $_GET['Emails'];
			}

			$model->contact->phones = new Phones('search');

			if (isset($_GET['Phones'])) {
				$model->contact->phones->attributes = $_GET['Phones'];
			}

			return $model;
		}
	}
