<?php

	/**
	 * This is the model class for table "company_assignment_lu".
	 *
	 * The followings are the available columns in table 'company_assignment_lu':
	 *
	 * @property integer          $id
	 * @property integer          $company_id
	 * @property integer          $component_type_id
	 * @property integer          $component_id
	 * @property integer          $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property TransactionTypes $transactionType
	 * @property Contacts         $contact
	 * @property AssignmentTypes  $assignmentType
	 */
	class CompanyAssignmentLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Assignments the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'company_assignment_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'company_id, component_type_id, component_id',
					'required'
				),
				array(
					'company_id, component_type_id, component_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, company_id, component_type_id, component_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'company_id' => 'Assigned To',
				'component_type_id' => 'Component Type',
				'component_id' => 'Component',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('company_id', $this->company_id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('component_id', $this->component_id);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
