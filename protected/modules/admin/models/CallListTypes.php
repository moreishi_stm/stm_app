<?php

/**
 * This is the model class for table "call_list_types".
 *
 * The followings are the available columns in table 'call_list_types':
 * @property integer $id
 * @property string $name
 * @property string $default_requeue_timeframe
 * @property integer $default_ring_seconds
 * @property integer $is_shared
 * @property integer $component_type_id
 * @property string $table_type
 * @property integer $default_max_call_count
 * @property integer $default_max_call_days
 * @property string $updated
 * @property integer $updated_by
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property ComponentTypes $componentType
 * @property Contacts $updatedBy
 * @property Accounts $account
 */
class CallListTypes extends StmBaseActiveRecord
{
    const TASK_TABLE_TYPE = 'task';
    const TIME_TABLE_TYPE = 'time';
    const CUSTOM_TABLE_TYPE = 'custom';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallListTypes the static model class
     */
    public static function model()
    {
        return parent::model(__CLASS__);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_list_types';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, default_ring_seconds, is_shared', 'required'),
            array('id, default_ring_seconds, is_shared, component_type_id, default_max_call_count, default_max_call_days, updated_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>100),
            array('default_requeue_timeframe', 'length', 'max'=>50),
            array('table_type', 'length', 'max'=>6),
            array('updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, default_requeue_timeframe, default_ring_seconds, is_shared, component_type_id, table_type, default_max_call_count, default_max_call_days, updated, updated_by, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'componentType' => array(self::BELONGS_TO, 'ComponentTypes', 'component_type_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'default_requeue_timeframe' => 'Default Requeue Timeframe',
            'default_ring_seconds' => 'Default Ring Seconds',
            'is_shared' => 'Is Shared',
            'component_type_id' => 'Component Type',
            'table_type' => 'Table Type',
            'default_max_call_count' => 'Default Max Call Count',
            'default_max_call_days' => 'Default Max Call Days',
            'updated' => 'Updated',
            'updated_by' => 'Updated By',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('default_requeue_timeframe',$this->default_requeue_timeframe,true);
        $criteria->compare('default_ring_seconds',$this->default_ring_seconds);
        $criteria->compare('is_shared',$this->is_shared);
        $criteria->compare('component_type_id',$this->component_type_id);
        $criteria->compare('table_type',$this->table_type,true);
        $criteria->compare('default_max_call_count',$this->default_max_call_count);
        $criteria->compare('default_max_call_days',$this->default_max_call_days);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}