<?php

	/**
	 * This is the model class for table "action_plan_applied_lu".
	 *
	 * The followings are the available columns in table 'action_plan_applied_lu':
	 *
	 * @property integer $action_plan_item_id
	 * @property integer $task_id
	 * @property integer $is_deleted
	 *
	 * The followings are the available model relations:
	 */
	class ActionPlanAppliedLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ActionPlans the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'action_plan_applied_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'action_plan_item_id, task_id',
					'required'
				),
				array(
					'action_plan_item_id, task_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'action_plan_item_id, task_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'actionPlanItem' => array(
					self::BELONGS_TO,
					'ActionPlanItems',
					'action_plan_item_id'
				),
                'task' => array(
                    self::BELONGS_TO,
                    'Tasks',
                    'task_id'
                ),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'action_plan_item_id' => 'Action Plan Item ID',
				'task_id' => 'Task ID',
				'is_deleted' => 'Task ID',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('action_plan_item_id', $this->action_plan_item_id);
			$criteria->compare('task_id', $this->task_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		public function byActionPlanItems($actionPlanItemIds = array()) {
			if (count($actionPlanItemIds) > 0) {
				$this->getDbCriteria()->addInCondition('action_plan_item_id', $actionPlanItemIds);
			}

			return $this;
		}

        public function byTaskComponentId($componentId, $together=false) {
            $Criteria = new CDbCriteria;
            $Criteria->condition = 'task.component_id = :component_id';
            $Criteria->params = array(':component_id' => $componentId);
            $Criteria->with = array(
                'task',
            );

            if($together) {
                $Criteria->together = true;
            }
            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

		public function getPrimaryKey() {
			return array(
				'action_plan_item_id',
				'task_id'
			);
		}
	}
