<?php

	/**
	 * This is the model class for table "cms_widget_group_lu".
	 *
	 * The followings are the available columns in table 'cms_widget_group_lu':
	 *
	 * @property integer         $id
	 * @property integer         $cms_widget_group_id
	 * @property integer         $cms_widget_id
	 * @property integer         $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property CmsWidgets      $cmsWidget
	 * @property CmsWidgetGroups $cmsWidgetGroup
	 */
	class CmsWidgetGroupLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return CmsWidgetGroupLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'cms_widget_group_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'cms_widget_group_id, cms_widget_id, sort_order, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, cms_widget_group_id, cms_widget_id, sort_order, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( // 'cmsWidget' => array(self::BELONGS_TO, 'CmsWidgets', 'cms_widget_id'),
				// 'cmsWidgetGroup' => array(self::BELONGS_TO, 'CmsWidgetGroups', 'cms_widget_group_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'cms_widget_group_id' => 'Cms Widget Group',
				'cms_widget_id' => 'Cms Widget',
				'sort_order' => 'Sort Order',
				'is_deleted' => 'Is Deleted',
			);
		}

		public function defaultScopes() {
			return array(
				'order' => 'sort_order asc',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('cms_widget_group_id', $this->cms_widget_group_id);
			$criteria->compare('cms_widget_id', $this->cms_widget_id);
			$criteria->compare('sort_order', $this->sort_order);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}