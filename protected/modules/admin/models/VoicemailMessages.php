<?php

/**
 * This is the model class for table "voicemail_messages".
 *
 * The followings are the available columns in table 'voicemail_messages':
 * @property string $id
 * @property string $record_api_id
 * @property string $recording_id
 * @property string $recording_call_uuid
 * @property string $record_url
 * @property string $record_message
 * @property integer $recording_duration
 * @property integer $recording_duration_ms
 * @property string $recording_start_ms
 * @property string $recording_end_ms
 * @property string $recording_added
 * @property integer $updated_by
 * @property string $updated
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property CallHuntGroupSessionVoicemailMessages[] $callHuntGroupSessionVoicemailMessages
 * @property IvrSessionVoicemailMessages[] $ivrSessionVoicemailMessages
 */
class VoicemailMessages extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VoicemailMessages the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'voicemail_messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('added', 'required'),
            array('recording_duration, recording_duration_ms, updated_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('record_api_id, recording_id, recording_call_uuid', 'length', 'max'=>36),
            array('record_url, record_message', 'length', 'max'=>100),
            array('recording_start_ms, recording_end_ms', 'length', 'max'=>16),
            array('recording_added, updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, record_api_id, recording_id, recording_call_uuid, record_url, record_message, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, recording_added, updated_by, updated, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callHuntGroupSessionVoicemailMessages' => array(self::HAS_MANY, 'CallHuntGroupSessionVoicemailMessages', 'voicemail_message_id'),
            'ivrSessionVoicemailMessages' => array(self::HAS_MANY, 'IvrSessionVoicemailMessages', 'voicemail_message_id'),
            'callHuntGroupSession'        => array(
                self::MANY_MANY,
                'CallHuntGroupSessions',
                'call_hunt_group_session_voicemail_messages(voicemail_message_id, call_hunt_group_session_id)',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'record_api_id' => 'Record Api',
            'recording_id' => 'Recording',
            'recording_call_uuid' => 'Recording Call Uuid',
            'record_url' => 'Record Url',
            'record_message' => 'Record Message',
            'recording_duration' => 'Recording Duration',
            'recording_duration_ms' => 'Recording Duration Ms',
            'recording_start_ms' => 'Recording Start Ms',
            'recording_end_ms' => 'Recording End Ms',
            'recording_added' => 'Recording Added',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('record_api_id',$this->record_api_id,true);
        $criteria->compare('recording_id',$this->recording_id,true);
        $criteria->compare('recording_call_uuid',$this->recording_call_uuid,true);
        $criteria->compare('record_url',$this->record_url,true);
        $criteria->compare('record_message',$this->record_message,true);
        $criteria->compare('recording_duration',$this->recording_duration);
        $criteria->compare('recording_duration_ms',$this->recording_duration_ms);
        $criteria->compare('recording_start_ms',$this->recording_start_ms,true);
        $criteria->compare('recording_end_ms',$this->recording_end_ms,true);
        $criteria->compare('recording_added',$this->recording_added,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Search By IVR Extension ID
     *
     * @param int $id
     * @return CActiveDataProvider
     */
    public function searchByIvrExensionId($id)
    {
        $criteria=new CDbCriteria();

        $criteria->together = true;
        $criteria->with = array('ivrSessionVoicemailMessages.ivrSession');

        $criteria->compare('ivrSession.ivr_extension_id', $id);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Search By TelephonyPhone ID
     *
     * @param int $id
     * @return CActiveDataProvider
     */
    public function searchByTelephonyPhoneId($id)
    {
        $telephonyPhone = TelephonyPhones::model()->findByPk($id);

        $criteria=new CDbCriteria();

        $criteria->together = true;
        $criteria->with = array('callHuntGroupSessionVoicemailMessages.callHuntGroupSession');

        $criteria->compare('callHuntGroupSession.to_number', '1'.$telephonyPhone->phone);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Search By Call Hunt Group ID
     *
     * @param int $id
     * @return CActiveDataProvider
     */
//    public function searchByCallHuntGroupId($id)
//    {
//        $criteria=new CDbCriteria();
//
//        $criteria->together = true;
//        $criteria->with = array('callHuntGroupSessionVoicemailMessages.callHuntGroupSession');
//
//        $criteria->compare('callHuntGroupSession.call_hunt_group_id', $id);
//
//        return new CActiveDataProvider($this, array(
//            'criteria'=>$criteria,
//        ));
//    }
}