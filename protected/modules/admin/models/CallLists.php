<?php
/**
 * This is the model class for table "call_lists".
 *
 * The followings are the available columns in table 'call_lists':
 * @property integer $id
 * @property integer $account_id
 * @property integer $preset_ma
 * @property integer $contact_id
 * @property string $requeue_timeframe
 * @property string $name
 * @property integer $filter_max_call_count
 * @property integer $filter_max_call_days
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Contacts $contact
 * @property CallSessions[] $callSessions
 * @property CallListPhones[] $callListPhones
 */
class CallLists extends StmBaseActiveRecord
{
    const TABLE_TYPE_TASK = 'task';
    const TABLE_TYPE_TIME = 'time';
    const TABLE_TYPE_CUSTOM = 'custom';

    const REQUEUE_30MIN = '30 minute';
    const REQUEUE_1HOUR = '1 hour';
    const REQUEUE_3HOURS = '3 hours';
    const REQUEUE_1DAY = '1 day';
    const REQUEUE_2DAYS = '2 days';
    const REQUEUE_5DAYS = '5 days';
    const REQUEUE_10DAYS = '10 days';
    const REQUEUE_14DAYS = '14 days';
    const REQUEUE_30DAYS = '30 days';
    const REQUEUE_45DAYS = '45 days';
    const REQUEUE_60DAYS = '60 days';
    const REQUEUE_90DAYS = '90 days';
    const REQUEUE_120DAYS = '120 days';
    const REQUEUE_180DAYS = '180 days';
    const REQUEUE_360DAYS = '360 days';

    /**
     * TASK-based Lists
     */
    const MY_SELLERS_TASKS                   = 1;
    const MY_BUYERS_TASKS                    = 2;
    const LEAD_POOL_BUYERS_TASKS             = 3;
    const LEAD_POOL_SELLERS_TASKS            = 4;
    const MY_CONTACTS_TASKS                  = 5;
    const LEAD_POOL_CONTACTS_TASKS           = 6;
    const MY_RECRUITS_TASKS                  = 7;
    const LEAD_POOL_RECRUITS_TASKS           = 8;
//    const MY_PAST_CLIENT               = 9;
//    const MY_SPHERE                    = 10;
//    const MY_EXPIREDS                  = ??;
//    const MY_FSBO                      = ??;

    /**
     * TIME-based Lists
     */
    //    const MY_NEW_SELLERS               = 30;
    //    const MY_HOT_A_SELLERS             = 31;
    //    const MY_B_SELLERS                 = 32;
    //    const MY_C_SELLERS                 = 33;
    //    const MY_D_SELLERS                 = 34;
    //    const MY_E_SELLERS                 = 35;

    //    const MY_NEW_BUYERS                = 40;
    //    const MY_HOT_A_BUYERS              = 41;
    //    const MY_B_BUYERS                  = 42;
    //    const MY_C_BUYERS                  = 43;
    //    const MY_D_BUYERS                  = 44;
    //    const MY_E_BUYERS                  = 45;

    const ALL_NEW_SELLER_PROSPECTS      = 50;
//    const ALL_HOT_A_SELLER_PROSPECTS    = 51;
//    const ALL_B_SELLER_PROSPECTS        = 52;
//    const ALL_C_SELLER_PROSPECTS        = 53;
    const ALL_D_SELLER_PROSPECTS        = 55;
    const ALL_NURTURING_SELLERS         = 54;

    const LEAD_POOL_A_SELLERS           = 56;
    const LEAD_POOL_B_SELLERS           = 57;
    const LEAD_POOL_C_SELLERS           = 58;
    const LEAD_POOL_D_SELLERS           = 59;

    const ALL_NEW_BUYER_PROSPECTS       = 60;
//    const ALL_HOT_A_BUYER_PROSPECTS     = 61;
//    const ALL_B_BUYER_PROSPECTS         = 62;
//    const ALL_C_BUYER_PROSPECTS         = 63;
    const ALL_D_BUYER_PROSPECTS         = 65;
    const ALL_NURTURING_BUYERS          = 64;

    const LEAD_POOL_A_BUYERS            = 66;
    const LEAD_POOL_B_BUYERS            = 67;
    const LEAD_POOL_C_BUYERS            = 68;
    const LEAD_POOL_D_BUYERS            = 69;

//    const ALL_PAST_CLIENT               = 58;
//    const ALL_SPHERE                    = 59;
//    const ALL_EXPIREDS                  = 60;
//    const ALL_FSBO                      = 61;

//    const KEEP_IN_TOUCH                = 90;

    const CUSTOM_BUYERS = 90;
    const CUSTOM_SELLERS = 95;
    const CUSTOM_RECRUITS = 96;
    const CUSTOM_CONTACTS = 98;

    /**
     * Model
     *
     * Static method to retrieve an instance of this model
     * @return CallLists Object instance
     */
    public static function model()
    {
        return parent::model(__CLASS__);
    }

    /**
     * Table Name
     *
     * Used to retrieve the table name associated with this model
     * @return string Database table name
     */
    public function tableName()
    {
        return 'call_lists';
    }

    /**
     * Rules
     *
     * Rules for validation
     * @return array validation rules for model attributes
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'preset_ma, name, account_id','required','on'=>'insert'
            ),
            array(
                'id, account_id, contact_id, preset_ma, filter_max_call_count, filter_max_call_days, is_deleted',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'name',
                'unique',
            ),
            array('name', 'validateName'),
//            array(
//                'name',
//                'match',
//                'not' => true,
//                'pattern' => '/["\']/', 'message' => 'Quotes are not allowed.'
//            ),
            array(
                'name',
                'length',
                'max' => 100
            ),
            array(
                'requeue_timeframe, added',
                'safe',
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, name, account_id, contact_id, preset_ma, filter_max_call_count, filter_max_call_days, requeue_timeframe, added',
                'safe',
                'on' => 'search'
            ),
        );
    }
    /**
     * Relations
     *
     * Model relations with other models
     * @return array
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
        return array(
//            'callSession' => array(
//                self::HAS_MANY,
//                'CallSessions',
//                'call_session_id'
//            ),
            'callListPhones' => array(
                self::HAS_MANY,
                'CallListPhones',
                'call_list_id'
            ),
            'lastRequeue' => array(
                self::HAS_ONE,
                'CallListRequeueLog',
                'call_list_id',
                'order'=>'id DESC',
//                'limit'=>'1',
            ),
            'type' => array(
                self::BELONGS_TO,
                'CallListTypes',
                'preset_ma'
            ),

        );
    }

    public function validateName($attribute, $params)
    {
        if(!$this->preset_ma) {
            $this->addError('preset_ma', 'Call List Type is Required.');
            return;
        }

        if (self::getTableTypeByPreset($this->preset_ma) == 'custom' &&  strpos($this->name, '"') !== false || strpos($this->name, "'") !== false) {

            $this->addError($attribute, 'Quotes are Not Allowed.');
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'preset_ma' => 'Type',

        );
    }

    /**
     * Get Preset Types
     *
     * returns array of preset id and name pairs
     */
    public function getPresetTypes()
    {
        $types = array();
        $rows = Yii::app()->db->createCommand("SELECT id, name FROM call_list_types WHERE is_deleted=0 ORDER BY id")->queryAll();
        foreach($rows as $row) {
            $types[$row['id']] = $row['name'];
        }
        return $types;
    }

    /**
     * Get Call back date by Call List Id
     * @param $id integer call list id
     *
     * @return string datetime
     */
    public static function getCallbackDateByListId($id)
    {
        $callList = CallLists::model()->findByPk($id);


        switch($callList->preset_ma) {

            // if a specific call list has been specified. Used for list type that have requeue_timeframe value.
            case(!empty($callList) && !empty($callList->requeue_timeframe)):
                $requeueTimeframe = ($callList->requeue_timeframe)? $callList->requeue_timeframe : self::REQUEUE_1DAY;
                $callBackDate = date('Y-m-d H:i:s', strtotime('-'.$requeueTimeframe));
                break;

            case self::CUSTOM_BUYERS:
            case self::CUSTOM_SELLERS:
            case self::CUSTOM_CONTACTS:
            case self::CUSTOM_RECRUITS:
                $callBackDate = ($callList->requeue_timeframe)? date('Y-m-d 00:00:00', strtotime('-'.$callList->requeue_timeframe)) : date('Y-m-d 00:00:00', strtotime('-1 day'));
                break;

            case self::MY_SELLERS_TASKS:
            case self::MY_BUYERS_TASKS:
            case self::MY_RECRUITS_TASKS:
            case self::MY_CONTACTS_TASKS:
            case self::LEAD_POOL_BUYERS_TASKS:
            case self::LEAD_POOL_SELLERS_TASKS:
            case self::LEAD_POOL_RECRUITS_TASKS:
            case self::LEAD_POOL_CONTACTS_TASKS:
            case self::ALL_NURTURING_SELLERS:
            case self::ALL_NURTURING_BUYERS:
            case self::ALL_D_BUYER_PROSPECTS:
            case self::ALL_D_SELLER_PROSPECTS:
                $callBackDate = date('Y-m-d 00:00:00', strtotime('-1 day'));
                break;

            case self::ALL_NEW_SELLER_PROSPECTS:
            case self::ALL_NEW_BUYER_PROSPECTS:
                $callBackDate = date('Y-m-d H:i:s', strtotime('-2 hours'));
                break;

            default:
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Unrecognized Preset ID#: '.$callList->preset_ma, CLogger::LEVEL_ERROR);
                throw new Exception('Error: Unrecognized Preset ID');
                break;
        }
        //@todo: FUTURE will get date based on setting stored in table somewhere... to discuss with Nicole.
        return $callBackDate;
    }

    public static function isLongRingByListId($id)
    {
        $isLongRing = false;

        if($callList = CallLists::model()->findByPk($id)) {
            switch($callList->preset_ma) {
                case self::MY_SELLERS_TASKS:
                case self::MY_BUYERS_TASKS:
                case self::MY_RECRUITS_TASKS:
                case self::MY_CONTACTS_TASKS:
                    $isLongRing = true;
                    break;
            }
        }
        else {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Unrecognized Preset ID#: '.$callList->preset_ma, CLogger::LEVEL_ERROR);
            throw new Exception('Error: Unrecognized Preset ID');
        }

        return $isLongRing;
    }

    /**
     * Is Preset a Shared List
     *
     * @param $presetId integer
     * returns boolean
     */
    public static function getIsPresetShared($presetId)
    {
        if($callListType = CallListTypes::model()->findByPk(intval($presetId))) {
            return $callListType->is_shared;
        }
        else {
            //Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Dialer Error! Unrecognized Preset ID#: '.$presetId, CLogger::LEVEL_ERROR);
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Urgent Dialer Error: Unrecognized Preset ID #'.$presetId);
        }
    }

    public static function getComponentTypeByPreset($presetId)
    {
        if($callListType = CallListTypes::model()->findByPk($presetId)) {
            return $callListType->component_type_id;
        }
        else {
            //Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Dialer Error! Unrecognized Preset ID#: '.$presetId, CLogger::LEVEL_ERROR);
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Urgent Dialer Error: Unrecognized Preset ID #'.$presetId);
        }
    }

    public static function getComponentNameByPreset($presetId, $singular=false)
    {
        if($callListType = CallListTypes::model()->findByPk($presetId)) {
            return ($singular) ? $callListType->componentType->singularName : ucwords(strtolower($callListType->componentType->name));
        }
        else {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Dialer Error! Unrecognized Preset ID#: '.$presetId, CLogger::LEVEL_ERROR);
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Urgent Dialer Error: Unrecognized Preset ID #'.$presetId);
        }
    }

    public static function getTableTypeById($id)
    {
        if($callList = CallLists::model()->findByPk($id)) {

            if($callListType = CallListTypes::model()->findByPk($callList->preset_ma)) {
                return $callListType->table_type;
            }
            else {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Dialer Error! Unrecognized Preset ID#: '.$callList->preset_ma, CLogger::LEVEL_ERROR);
                throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Urgent Dialer Error: Unrecognized Preset ID #'.$callList->preset_ma);
            }
        }
    }

    /**
     * Get Table Type based on Preset Id
     *
     * @param $presetId integer
     * returns string table type name
     */
    public static function getTableTypeByPreset($presetId)
    {
        if($callListType = CallListTypes::model()->findByPk($presetId)) {
            return $callListType->table_type;
        }
        else {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Dialer Error! Unrecognized Preset ID#: '.$presetId, CLogger::LEVEL_ERROR);
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Urgent Dialer Error: Unrecognized Preset ID #'.$presetId);
        }
    }

    /**
     *  Get Single Call List By Preset Id
     *
     * Finds a single call list based on preset ID. Throws error if find none or multiple
     * @param integer $id
     * @param array $filters
     * @return mixed CallLists model or null
     */
    public static function getCallListByPresetId($presetId, array $filters = array())
    {
        $isShared = CallLists::getIsPresetShared($presetId);

        // If we had these filters, use them
        $criteria = array('preset_ma' => $presetId, 'contact_id' => ($isShared) ? null : Yii::app()->user->id);

        // Retrieve call list results
        $callListResults = CallLists::model()->findAllByAttributes($criteria);
        if($callListResults) {

            // exactly one match found
            if(count($callListResults)==1) {
                $callList = $callListResults[0];
            }
            // no or multiple call lists returned, both of which are errors and need logging and reporting
            else {
                $listIds = array();
                foreach($callListResults as $callListResult) {
                    $listIds[] = $callListResult->id;
                }

                // Error: there should only be one shared list. Throw exception, this will cause confusion.
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') There are '.count($callListResults).' entries of the "'.CallLists::getIsPresetShared($presetId).'" shared list. Investigate immediately. Ids for list are: '.implode(',', $listIds), CLogger::LEVEL_ERROR);
                throw new Exception(__CLASS__.' (:'.__LINE__.') There are '.count($callListResults).' entries of the "'.CallLists::getIsPresetShared($presetId).'" shared list. Investigate immediately. Ids for list are: '.implode(',', $listIds));
            }
        }

        /**
         * NOTE: If the call list is not found, it's ok, the Base initialize function, it will create one. Do not throw exception if not found.
         */

        return $callList;
    }

    public function getRequeueList()
    {
        return array(
            self::REQUEUE_30MIN   => self::REQUEUE_30MIN,
            self::REQUEUE_1HOUR   => self::REQUEUE_1HOUR,
            self::REQUEUE_3HOURS  => self::REQUEUE_3HOURS,
            self::REQUEUE_1DAY    => self::REQUEUE_1DAY,
            self::REQUEUE_2DAYS   => self::REQUEUE_2DAYS,
            self::REQUEUE_5DAYS   => self::REQUEUE_5DAYS,
            self::REQUEUE_10DAYS  => self::REQUEUE_10DAYS,
            self::REQUEUE_14DAYS  => self::REQUEUE_14DAYS,
            self::REQUEUE_30DAYS  => self::REQUEUE_30DAYS,
            self::REQUEUE_45DAYS  => self::REQUEUE_45DAYS,
            self::REQUEUE_60DAYS  => self::REQUEUE_60DAYS,
            self::REQUEUE_90DAYS  => self::REQUEUE_90DAYS,
            self::REQUEUE_120DAYS => self::REQUEUE_120DAYS,
            self::REQUEUE_180DAYS => self::REQUEUE_180DAYS,
            self::REQUEUE_360DAYS => self::REQUEUE_360DAYS,

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        if($this->preset_ma) {
            $criteria->compare('preset_ma',$this->preset_ma);
        }
        else {
            $criteria->addInCondition('preset_ma', array(CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_CONTACTS, CallLists::CUSTOM_RECRUITS));
        }
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}
