<?php

/**
 * This is the model class for table "webrtc_call_sessions".
 *
 * The followings are the available columns in table 'webrtc_call_sessions':
 * @property string $id
 * @property integer $contact_id
 * @property string $call_sid
 * @property string $recording_sid
 * @property string $to
 * @property string $from
 * @property string $direction
 * @property string $call_status
 * @property string $recording_url
 * @property integer $duration
 * @property integer $call_duration
 * @property string $timestamp
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $contact
 */
class WebrtcCallSessions extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return WebrtcCallSessions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'webrtc_call_sessions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('added', 'required'),
            array('contact_id, duration, call_duration', 'numerical', 'integerOnly'=>true),
            array('call_sid, recording_sid, timestamp', 'length', 'max'=>255),
            array('to, from, call_status', 'length', 'max'=>32),
            array('direction', 'length', 'max'=>45),
            array('recording_url', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, contact_id, call_sid, recording_sid, to, from, direction, call_status, recording_url, duration, call_duration, timestamp, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'contact_id' => 'Contact',
            'call_sid' => 'Call Sid',
            'recording_sid' => 'Recording Sid',
            'to' => 'To',
            'from' => 'From',
            'direction' => 'Direction',
            'call_status' => 'Call Status',
            'recording_url' => 'Recording Url',
            'duration' => 'Duration',
            'call_duration' => 'Call Duration',
            'timestamp' => 'Timestamp',
            'added' => 'Added',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('call_sid',$this->call_sid,true);
        $criteria->compare('recording_sid',$this->recording_sid,true);
        $criteria->compare('to',$this->to,true);
        $criteria->compare('from',$this->from,true);
        $criteria->compare('direction',$this->direction,true);
        $criteria->compare('call_status',$this->call_status,true);
        $criteria->compare('recording_url',$this->recording_url,true);
        $criteria->compare('duration',$this->duration);
        $criteria->compare('call_duration',$this->call_duration);
        $criteria->compare('timestamp',$this->timestamp,true);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
} 