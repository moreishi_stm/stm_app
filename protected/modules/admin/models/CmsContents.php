<?php

/**
 * This is the model class for table "cms_content".
 *
 * The followings are the available columns in table 'cms_contents':
 * @property integer $id
 * @property integer $domain_id
 * @property integer $status_ma
 * @property integer $type_ma
 * @property string $template_name
 * @property string $url
 * @property string $title
 * @property string $template_data
 * @property integer $cms_widget_group_id
 * @property string $meta_tags
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $published
 * @property integer $display_tags
 * @property integer $allow_comments
 * @property string $updated
 * @property integer $updated_by
 * @property string $added
 * @property integer $added_by
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Contacts[] $contacts
 * @property CmsContentTagLu[] $cmsContentTagLus
 * @property CmsContentWidgetLu[] $cmsContentWidgetLus
 * @property Domains $domain
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 * @property CmsWidgetGroups $cmsWidgetGroup
 * @property TransactionCmsLu[] $transactionCmsLus
 */
class CmsContents extends StmBaseActiveRecord
{
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_PENDING  = 2;

    const TYPE_PAGE          = 1;
    const TYPE_BLOG          = 2;
    const TYPE_LISTING_SB    = 3;
    const TYPE_BUYER_SB      = 4;
    const TYPE_HOUSE_VALUES  = 5;
    const TYPE_LANDING_PAGES = 6;

    const TEMPLATE_TYPE_PAGE = 'page';
    const TEMPLATE_TYPE_BLOG = 'blog';
    const TEMPLATE_TYPE_LISTING_SB = 'listingStoryboard';
    const TEMPLATE_TYPE_BUYER_SB = 'buyerStoryboard';
    const TEMPLATE_TYPE_HOUSE_VALUES = 'houseValues';
    const TEMPLATE_TYPE_LANDING_PAGES = 'landingPages';

    public $enableClientAccess = 0; // for storyboard access for cms_content_contact_lu
    public $template; // Placeholder for the template associated with the content
    public $accountId;

    public $statusTypes
        = array(
            1 => 'Active',
            2 => 'Pending',
            0 => 'Inactive'
        );

    public $cmsTagsToAdd = array(); // Collection of CmsTags models

    private $_templateTypes
        = array(
            self::TYPE_PAGE            => self::TEMPLATE_TYPE_PAGE,
            self::TYPE_BLOG            => self::TEMPLATE_TYPE_BLOG,
            self::TYPE_LISTING_SB      => self::TEMPLATE_TYPE_LISTING_SB,
            self::TYPE_BUYER_SB        => self::TEMPLATE_TYPE_BUYER_SB,
            self::TYPE_LANDING_PAGES   => self::TEMPLATE_TYPE_LANDING_PAGES,
        );

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return CmsContent the static model class
     */
    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {

        return 'cms_contents';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'status_ma, url, template_name, title, updated_by',
                //cms_widget_group_id ... @todo: get scenarios working right
                'required',
            ),
            array(
                'domain_id, type_ma, cms_widget_group_id, status_ma, display_tags, allow_comments, updated_by, added_by, is_deleted',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'seo_description, seo_keywords',
                'length',
                'max' => 255
            ),

            array(
                'url',
                'length',
                'max' => 127
            ),
            array(
                'title',
                'length',
                'max' => 100
            ),
            array(
                'published, content_public, content_private, updated, enableClientAccess, cmsTagsToAdd, meta_tags',
                'safe'
            ),

            // Unique per domain
            array(
                'url, title',
                'unique',
                'criteria' => array(
                    'condition' => 'domain_id=:domain_id',
                    'params'    => array(':domain_id' => Yii::app()->user->getDomain()->id,),
                )
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, domain_id, type_ma, template_name, cms_widget_group_id, url, title, content_public, content_private, status_ma, enableClientAccess, seo_description, seo_keywords, published, updated, updated_by, added, added_by, is_deleted, meta_tags',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'domain'          => array(
                self::BELONGS_TO,
                'Domains',
                'domain_id'
            ),
            'cmsWidgetGroup'  => array(
                self::BELONGS_TO,
                'CmsWidgetGroups',
                'cms_widget_group_id'
            ),
            'transactions'     => array(
                self::MANY_MANY,
                'Transactions',
                'transaction_cms_lu(cms_id, transaction_id)'
            ),
            'comments'        => array(
                self::HAS_MANY,
                'CmsComment',
                'cms_content_id'
            ),
            'cmsTags'         => array(
                self::MANY_MANY,
                'CmsTags',
                'cms_content_tag_lu(cms_content_id, cms_tag_id)'
            ),
            //, 'condition'=>'types_types.is_deleted <> 1 OR types_types.is_deleted IS NULL'
            'cmsContentTagLu' => array(
                self::HAS_MANY,
                'CmsContentTagLu',
                'cms_content_id'
            ),
            'cmsContentContactLu' => array(
                self::HAS_MANY,
                'CmsContentContactLu',
                'cms_content_id'
            ),
            'contacts'     => array(
                self::MANY_MANY,
                'Transactions',
                'cms_content_contact_lu(cms_content_id, contact_id)'
            ),
            'addedBy'         => array(
                self::BELONGS_TO,
                'Contacts',
                'added_by'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'id'                  => 'ID',
            'domain_id'           => 'Domain',
            'cms_widget_group_id' => 'Widget Group',
            'status_ma'           => 'Status',
            'template_name'       => 'Template',
            'url'                 => 'Url',
            'title'               => 'Title',
            'content_public'      => 'Content',
            'content_private'     => 'Private Content',
            'cmsTagsToAdd'        => 'Tags',
            'meta_tags'           => 'Additional Meta Tags',
            'display_tags'        => 'Display Tags',
            'allow_comments'      => 'Allow Comments',
            'published'           => 'Publish Date',
            'is_deleted'          => 'Is Deleted'
        );
    }

    public function getTemplates()
    {

        $CmsTemplates = new CmsTemplates;

        return $CmsTemplates->templates;
    }

    public function getPageType()
    {

        if ($this->_templateTypes[$this->type_ma]) {
            return $this->_templateTypes[$this->type_ma];
        }

        return null;
    }
    public function getPageTypeName() {

        $pageTypeName = array(
            self::TYPE_PAGE            => 'Web Page',
            self::TYPE_BLOG            => 'Blog',
            self::TYPE_LISTING_SB      => 'Listing Storyboard',
            self::TYPE_BUYER_SB        => 'Buyer Storyboard',
            self::TYPE_LANDING_PAGES   => 'Landing Page',
        );
        return isset($pageTypeName[$this->type_ma])? $pageTypeName[$this->type_ma] : null;
    }
    /**
     * @return array
     */
    public function behaviors()
    {

        return array(
            'CTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'added',
                'updateAttribute' => 'updated',
            )
        );
    }

    /**
     * @return null
     */
    public function getContent()
    {

        if (!$this->template_name) {// or !$this->template_data) {
            return null;
        }

        $template = new CmsTemplates($this->template_name, $this->type_ma, $this->id);

        return $template->content;
    }

    public function getContact()
    {

        if (($transaction = $this->getTransaction())) {
            return $transaction->contact;
        } else {
            return null;
        }
    }

    public function printTransactionAssignments()
    {

        if (($transaction = $this->getTransaction())) {
            return $transaction->printGridviewAssignments();
        } else {
            return null;
        }
    }

    public function getTransaction() {
        if ($this->transactions) {
            return current($this->transactions);
        }

        return null;
    }

    public function isClientEnabled() {
        return !(count($this->cmsContentContactLu) == 0);
    }

    public function isStoryboardType() {
        return in_array($this->type_ma, $this->getStoryboardTypes());
    }

    public function getStoryboardTypes() {
        return array(self::TYPE_BUYER_SB, self::TYPE_LISTING_SB);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);

        if($this->accountId) {
            $criteria->with = CMap::mergeArray($criteria->with, array('domain'));
            $criteria->compare('domain.account_id', $this->accountId, true);
            $criteria->together = true;
        }

        $criteria->compare('domain_id', $this->domain_id);
        $criteria->compare('status_ma', $this->status_ma);
        $criteria->compare('type_ma', $this->type_ma);
        $criteria->compare('template_name', $this->template_name);
        if($this->title) {
            $criteria->compare('title', $this->title, true);
            $criteria->compare('url', $this->title, true, 'OR');
        }
        $criteria->compare('seo_description', $this->seo_description, true);
        $criteria->compare('published', $this->published, true);

        $criteria->compare('display_tags', $this->display_tags, true);
        $criteria->compare('allow_comments', $this->allow_comments, true);

        $criteria->compare('is_deleted', $this->is_deleted);
        $criteria->order = 'title ASC';

        return new CActiveDataProvider($this, array('criteria' => $criteria,
                                                    'pagination'=>array(
                                                        'pageSize'=> 100,
                                                    ),
        ));
    }

    /**
     * Create a scope to filter the request by a url
     *
     * @param  string $url Url string
     *
     * @return CmsContent
     */
    public function byUrl($url)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                 'condition' => 'url = :url',
                 'params'    => array(':url' => $url)
            )
        );

        return $this;
    }

    public function byPageType($pageType)
    {
        if(!is_array($pageType)) {
            $pageType = array($pageType);
        }

        $this->getDbCriteria()->addInCondition('type_ma', $pageType);

        return $this;
    }

    public function byContactId($contactId)
    {
        $Criteria = new CDbCriteria;
        $Criteria->condition = 'cmsContentContactLu.contact_id = :contact_id';
        $Criteria->params = array(':contact_id' => $contactId);
        $Criteria->with = array(
            'cmsContentContactLu',
        );
        $Criteria->together = true;

        $this->getDbCriteria()->mergeWith($Criteria);

        return $this;
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->domain_id = Yii::app()->user->domain->id;
            $this->added_by  = Yii::app()->user->id;
        }

        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    protected function afterFind()
    {
        if($this->published) {
            $this->published = Yii::app()->format->formatDate($this->published, StmFormatter::APP_DATETIME_PICKER_FORMAT);
        }

        if ($this->cmsContentTagLu) {
            foreach ($this->cmsContentTagLu as $CmsContentTagLu) {
                array_push($this->cmsTagsToAdd, $CmsContentTagLu->cms_tag_id);
            }
        }

        $this->enableClientAccess = (int) $this->isClientEnabled();

        parent::afterFind();
    }

    protected function beforeSave()
    {
        if ($this->published) {
            $this->published = Yii::app()->format->formatDate($this->published, StmFormatter::MYSQL_DATE_FORMAT);
        }

        $this->updated = new CDbExpression('NOW()');

        // Make cli compatible since the user component is not accessible from the cli
        $userId           = (php_sapi_name() == 'cli') ? null : Yii::app()->user->id;
        $this->updated_by = $userId;

        return parent::beforeSave();
    }

    protected function afterSave()
    {
        /**
         * Update the tags for a particular entry
         */
        $cmsTagsToAdd = array();
        if (!$this->getIsNewRecord()) {
            $cmsTypesToAddFlipped = array_flip($this->cmsTagsToAdd);
            foreach ($this->cmsContentTagLu as $CmsContentTagLu) {
                if (!in_array($CmsContentTagLu->cms_tag_id, $this->cmsTagsToAdd)) {
                    $CmsContentTagLu->delete();
                } else {
                    unset($cmsTypesToAddFlipped[$CmsContentTagLu->cms_tag_id]);
                }
            }

            $cmsTagsToAdd = array_flip($cmsTypesToAddFlipped);
        }

        if (!empty($this->cmsTagsToAdd)) {
            // Attach the tags to the newly added page
            foreach ($cmsTagsToAdd as $tagId) {
                $CmsContentTagLu                 = new CmsContentTagLu;
                $CmsContentTagLu->cms_content_id = $this->id;
                $CmsContentTagLu->cms_tag_id     = $tagId;
                $CmsContentTagLu->save();
            }
        } else {
            CmsContentTagLu::model()->deleteAllByAttributes(
                array('cms_content_id' => $this->id,)
            );
        }

        if(Yii::app()->controller->module->id !== 'hq' && $Transaction = $this->getTransaction()) {
            $Contact = $Transaction->contact;
            if(Yii::app()->user->contact->id == $Contact->id) {
                $this->logAndNotifyUserUpdatedCms($Contact, $Transaction);
            }
        }

        parent::afterSave();
    }

    protected function logAndNotifyUserUpdatedCms(Contacts $Contact, Transactions $Transaction) {

        //@todo: add task for listing manager ... check first to see if there is a task for this today.

        // add activity log for client updating storyboard
        $ActivityLog = new ActivityLog;
        $Assignments = $Transaction->assignments;

        $ActivityLog->setAttributes(
            array(
                'account_id'         => Yii::app()->user->account->id,
                'component_type_id'  => $Transaction->component_type_id,
                'component_id'       => $Transaction->id,
                'task_type_id'       => TaskTypes::NOTE,
                'activity_date'      => new CDbExpression('NOW()'),
                'note'               => 'Storyboard content updated by ' . Yii::app()->user->fullName . '.',
                'lead_gen_type_ma'   => ActivityLog::LEAD_GEN_NONE_ID,
                'is_spoke_to'        => 0,
                'asked_for_referral' => 0,
                'is_public'          => 0,
                'is_action_plan'     => 0,
            )
        );

        if(!$ActivityLog->save()) {
            //@todo: error message about saving model
        }

        //email all people assigned to this transactions.
        $this->emailAlertStoryboardUpdatedByClient($Transaction);

        return;
    }

    public function getIsStoryboard() {
        if(in_array($this->type_ma, array(CmsContents::TYPE_LISTING_SB, CmsContents::TYPE_BUYER_SB))) {
            return true;
        } else {
            return false;
        }
    }

    public function getPageTypeList()
    {
        if(Yii::app()->controller->module->id == 'admin') {
            return array(
                self::TYPE_PAGE          => 'Web Page',
                self::TYPE_BLOG          => 'Blog Page',
                self::TYPE_LISTING_SB    => 'Listing Storyboard',
                self::TYPE_BUYER_SB      => 'Buyer Storyboard',
                self::TYPE_HOUSE_VALUES  => 'House Value Pages',
                self::TYPE_LANDING_PAGES => 'Landing Pages',
            );
        }
        else {
            return array(
                self::TYPE_BLOG          => 'Web Page',
//                self::TYPE_LANDING_PAGES => 'Landing Pages',
            );
        }
    }

    public function getContentDataByName($name) {
        return $data = Yii::app()->db->createCommand()
            ->select('value')
            ->from('cms_content_values')
            ->where('cms_content_id='.$this->id)
            ->andWhere('element_id=:name', array(':name'=>$name))
            ->queryScalar();
    }

    protected function emailAlertStoryboardUpdatedByClient(Transactions $Transaction) {
        $message = new YiiMailMessage;
        $logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Our Logo">';

        $Assignments = $Transaction->assignments;
        $toEmail = array();
        if(!empty($Assignments)) {
            foreach($Assignments as $Assignment) {
                $toEmail[$Assignment->contact->primaryEmail] = $Assignment->contact->fullName;
            }
        }

        $storyboardType = ($Transaction->component_type_id == ComponentTypes::SELLERS) ? 'Listing':'Buyer';
        $content = '<span style="font-size:16px;">'.$storyboardType.' Storyboard has been updated by the '.(($Transaction->component_type_id == ComponentTypes::SELLERS) ? 'Seller':'Buyer').'.';
        $content .= '<br /><b>Please review and touch base with client as needed.</b>';
        $content .= '<br /><br />Client Name: '.$Transaction->contact->fullName;
        if($Transaction->component_type_id == ComponentTypes::SELLERS) {
            $content .= '<br />Address: '.Yii::app()->format->formatAddress($Transaction->address, array('lineBreak'=>false));
        }
        $clientUrl = 'http://www.'.Yii::app()->user->domain->name.'/admin/'.strtolower($Transaction->componentType->name).'/'.$Transaction->id;
        $content .= '<br /><br /><a href="'.$clientUrl.'">Click here to view Client details</a>';
        $content .= '</span><br />'.$logoImage;
        $message->view = 'plain';
        $message->setBody(array(
                          'toEmail' => $toEmail,
                          'content' => $content,
                          ), 'text/html'
        );
        $message->setSubject($storyboardType.'Storyboard Updated by Client');

        $toEmail = (YII_DEBUG)? array(StmMailMessage::DEBUG_EMAIL_ADDRESS) : $toEmail ;
        foreach($toEmail as $email => $name) {
            $message->addTo($email, $name);
        }

        $message->addFrom('Do-Not-Reply@SeizeTheMarket.net', 'Storyboard Notification');
//        Yii::app()->mail->send($message);
        StmZendMail::sendYiiMailMessage($message);
        return;
    }
}