<?php

	/**
	 * This is the model class for table "term_component_lu".
	 *
	 * The followings are the available columns in table 'term_component_lu':
	 *
	 * @property integer $component_type_id
	 * @property integer $component_id
	 * @property integer $term_id
	 * @property string  $value
	 * @property string  $term_conjunctor
	 * @property integer $group_id
	 *
	 * The followings are the available model relations:
	 * @property Terms   $term
	 */
	class TermComponentLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TermComponentLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'term_component_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'component_type_id, component_id, term_id, value',
					'required',
                    'message' => 'Missing value for {attribute}. Term ID: '.$this->term_id,
				),
				array(
					'id, component_type_id, component_id, term_id, group_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'term_conjunctor',
					'length',
					'max' => 5
				),
                array(
                    'id, component_type_id, component_id, term_id, value, term_conjunctor, group_id',
                    'safe',
                ),
                // The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, component_type_id, component_id, term_id, value, term_conjunctor, group_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'term' => array(
					self::BELONGS_TO,
					'Terms',
					'term_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'Id',
				'component_type_id' => 'Component Type',
				'component_id' => 'Component',
				'term_id' => 'Term',
				'value' => 'Value',
				'term_conjunctor' => 'Term Conjunctor',
				'group_id' => 'Group',
			);
		}


        protected function beforeSave()
        {
            $this->value = trim($this->value);

            if (!$this->isNewRecord && $this->group_id && $this->group_id == $this->id) {
                $this->group_id = null;
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') ERROR: Term Component Lu group_id same as id. Group ID has been set to NULL. Investigate ASAP.'.PHP_EOL.'Data: '.print_r($this->attributes, true), CLogger::LEVEL_ERROR);
            }

            return parent::beforeSave();
        }

		protected function beforeDelete() {
			// check to see if group_id is affected, if so make the proper adjustments
			$countGroupChild = $this->countGroupChild();
			if ($countGroupChild == 1) {
				$this->setAssociatedGroupId(null);
			} elseif ($countGroupChild > 1) {
				$groupId = $this->getNewGroupId();
				$this->setAssociatedGroupId($groupId);
			}

			return parent::beforeDelete();
		}

		public function getSavedSearchValues($id) {
			return $this->findAll(array(
					'condition' => 'component_type_id=:component_type_id AND component_id=:component_id',
					'params' => array(
						':component_type_id' => ComponentTypes::SAVED_SEARCHES,
						':component_id' => $id
					),
					'order' => 'term_id ASC, group_id ASC',
				)
			);
		}

        public function getFeaturedAreasValues($id) {
            return $this->findAll(array(
                    'condition' => 'component_type_id=:component_type_id AND component_id=:component_id',
                    'params' => array(
                        ':component_type_id' => ComponentTypes::FEATURED_AREAS,
                        ':component_id' => $id
                    ),
                    'order' => 'term_id ASC, group_id ASC',
                )
            );
        }

		public function getSavedSearchValueByTermId($termId, $componentId) {
			return $this->findAll(array(
					'condition' => 'component_type_id=:component_type_id AND component_id=:component_id AND term_id=:term_id',
					'params' => array(
						':component_type_id' => ComponentTypes::SAVED_SEARCHES,
						':component_id' => $componentId,
						':term_id' => $termId
					),
					'order' => 'group_id DESC',
				)
			);
		}

        public function getFeaturedAreaValueByTermId($termId, $componentId) {
            return $this->findAll(array(
                    'condition' => 'component_type_id=:component_type_id AND component_id=:component_id AND term_id=:term_id',
                    'params' => array(
                        ':component_type_id' => ComponentTypes::FEATURED_AREAS,
                        ':component_id' => $componentId,
                        ':term_id' => $termId
                    ),
                    'order' => 'group_id DESC',
                )
            );
        }

		public function getParentIdFromSavedSearchValueByTermId($termId, $componentId) {
			$model = $this->find(array(
					'condition' => 'component_type_id=:component_type_id AND component_id=:component_id AND term_id=:term_id AND group_id IS NULL',
					'params' => array(
						':component_type_id' => ComponentTypes::SAVED_SEARCHES,
						':component_id' => $componentId,
						':term_id' => $termId
					),
				)
			);
			if ($model) {
				return $model->id;
			} else {
				return null;
			}
		}

        public function getParentIdFromFeaturedAreaValueByTermId($termId, $componentId) {
            $model = $this->find(array(
                    'condition' => 'component_type_id=:component_type_id AND component_id=:component_id AND term_id=:term_id AND group_id IS NULL',
                    'params' => array(
                        ':component_type_id' => ComponentTypes::FEATURED_AREAS,
                        ':component_id' => $componentId,
                        ':term_id' => $termId
                    ),
                )
            );
            if ($model) {
                return $model->id;
            } else {
                return null;
            }
        }

		public function countGroupChild() {
			return $this->count('component_type_id=:component_type_id AND component_id=:component_id AND term_id=:term_id AND group_id=:group_id', array(
					':component_type_id' => ComponentTypes::SAVED_SEARCHES,
					':component_id' => $this->component_id,
					':term_id' => $this->term_id,
					':group_id' => $this->id
				)
			);
		}

		public function setAssociatedGroupId($value = null) {
			$successFlag = true;
			if ($value) {
				$parentModel = $this->findByPk($value);
				$parentModel->group_id = null;
				$parentModel->term_conjunctor = null;
				$parentModel->save();
			}

			$models = $this->findAll('component_type_id=:component_type_id AND component_id=:component_id AND term_id=:term_id', array(
					':component_type_id' => ComponentTypes::SAVED_SEARCHES,
					':component_id' => $this->component_id,
					':term_id' => $this->term_id
				)
			);

			foreach ($models as $model) {
				$model->group_id = $value;
				if ($value) {
					$model->term_conjunctor = 'OR';
				} else {
					$model->term_conjunctor = null;
				}

				if (!$model->save()) {
					$successFlag = false;
				}
			}

			return $successFlag;
		}

		public function getNewGroupId($value = null) {
			$successFlag = true;

			$model = $this->find('component_type_id=:component_type_id AND component_id=:component_id AND term_id=:term_id AND id !=:id', array(
					':component_type_id' => ComponentTypes::SAVED_SEARCHES,
					':component_id' => $this->component_id,
					':term_id' => $this->term_id,
					':id' => $this->id
				)
			);

			if ($model) {
				return $model->id;
			} else {
				return null;
			}
		}

        public function getGuiTermsDropDownList() {
            return array(
                Terms::CITY => 'City',
                Terms::COUNTY => 'County',
                Terms::NEIGHBORHOOD => 'Neighborhood',
                Terms::ZIP => 'Zip Code',
            );
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('component_id', $this->component_id);
			$criteria->compare('term_id', $this->term_id);
			$criteria->compare('value', $this->value, true);
			$criteria->compare('term_conjunctor', $this->term_conjunctor, true);
			$criteria->compare('group_id', $this->group_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}