<?php

/**
 * This is the model class for table "call_list_import_log".
 *
 * The followings are the available columns in table 'call_list_import_log':
 * @property integer $id
 * @property integer $call_list_id
 * @property string $description
 * @property string $start
 * @property string $end
 * @property integer $processed_count
 * @property integer $duplicate_count
 * @property integer $missing_data_count
 * @property string $file_location
 * @property integer $added_by
 *
 * The followings are the available model relations:
 * @property CallLists $callList
 * @property Contacts $addedBy
 */
class CallListImportLog extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallListImportLog the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_list_import_log';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_list_id, added_by', 'required'),
            array('call_list_id, processed_count, duplicate_count, missing_data_count, added_by', 'numerical', 'integerOnly'=>true),
            array('file_location', 'length', 'max'=>250),
            array('description, start, end', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_list_id, description, start, end, processed_count, duplicate_count, missing_data_count, file_location, added_by', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callList' => array(self::BELONGS_TO, 'CallLists', 'call_list_id'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_list_id' => 'Call List',
            'start' => 'Start',
            'end' => 'End',
            'processed_count' => 'Processed Count',
            'duplicate_count' => 'Duplicate Count',
            'missing_data_count' => 'Missing Data Count',
            'file_location' => 'File Location',
            'added_by' => 'Added By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('call_list_id',$this->call_list_id);
        $criteria->compare('start',$this->start,true);
        $criteria->compare('end',$this->end,true);
        $criteria->compare('processed_count',$this->processed_count);
        $criteria->compare('duplicate_count',$this->duplicate_count);
        $criteria->compare('missing_data_count',$this->missing_data_count);
        $criteria->compare('file_location',$this->file_location,true);
        $criteria->compare('added_by',$this->added_by);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}