<?php

/**
 * This is the model class for table "flash_cards".
 *
 * The followings are the available columns in table 'flash_cards':
 * @property string $id
 * @property string $name
 * @property string $front_content
 * @property string $back_content
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property FlashCardTopics[] $flashCardTopics
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */
class FlashCards extends StmBaseActiveRecord
{
    public $topicCollection = array();
    public $topicId;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FlashCards the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'flash_cards';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, front_content, back_content', 'required'),
            array('updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>100),
            array('front_content, back_content', 'length', 'max'=>65535),
            array('topicCollection, topicId, updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, front_content, back_content, updated_by, updated, added_by, added, is_deleted, topicCollection, topicId', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'flashCardTopics' => array(self::MANY_MANY, 'FlashCardTopics', 'flash_card_topic_lu(flash_card_id, flash_card_topic_id)'),
            'flashCardTopicLu' => array(self::HAS_MANY, 'FlashCardTopicLu', 'flash_card_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'front_content' => 'Front Content',
            'back_content' => 'Back content',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }
        $this->updated_by = Yii::app()->user->id;
        $this->updated = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->together = true;

        if($this->topicId) {
            $criteria->with = array('flashCardTopicLu');
            $criteria->order = 'flashCardTopicLu.sort_order';
            $criteria->compare('flashCardTopicLu.flash_card_topic_id',$this->topicId);
        }

//        if($this->topicCollection) {
//            $criteria->with = array('flashCardTopicLu');
//            $criteria->addInCondition('flashCardTopicLu.flash_card_id',$this->topicCollection);
//        }

        $criteria->compare('id',$this->id,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('front_content',$this->front_content,true);
        $criteria->compare('back_content',$this->back_content,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}