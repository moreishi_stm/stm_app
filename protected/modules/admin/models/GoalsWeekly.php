<?php

/**
 * This is the model class for table "goals_weekly".
 *
 * The followings are the available columns in table 'goals_weekly':
 * @property integer $id
 * @property integer $goal_id
 * @property integer $week_number
 * @property integer $working_days
 * @property integer $lead_gen_hours
 * @property integer $contacts
 * @property integer $appointments_set
 * @property integer $appointments_met
 * @property integer $signed
 * @property integer $contracts
 * @property integer $closings
 * @property string $one_thing_type
 * @property integer $one_thing_quantity
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 * @property Goals $goal
 * @property Contacts $updatedBy
 */
class GoalsWeekly extends StmBaseActiveRecord
{
    const ONE_THING_TYPE_SIGNED = 'Signed Agreements';
    const ONE_THING_TYPE_SELLER_SIGNED = 'Seller Signed Agreements';
    const ONE_THING_TYPE_BUYER_SIGNED = 'Buyer Signed Agreements';
    const ONE_THING_TYPE_APPOINTMENT_SET = 'Appointments Set';
    const ONE_THING_TYPE_SELLER_APPOINTMENT_SET = 'Seller Appointments Set';
    const ONE_THING_TYPE_BUYER_APPOINTMENT_SET = 'Buyer Appointments Set';

    const ONE_THING_TYPE_APPOINTMENT_MET = 'Appointments Met';
    const ONE_THING_TYPE_SELLER_APPOINTMENT_MET = 'Seller Appointments Met';
    const ONE_THING_TYPE_BUYER_APPOINTMENT_MET = 'Buyer Appointments Met';

    public $collection = array();
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GoalsWeekly the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'goals_weekly';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('goal_id, week_number, working_days, lead_gen_hours, contacts, appointments_set, appointments_met, signed, contracts, closings, one_thing_type, one_thing_quantity, updated_by, updated, added_by, added', 'required'),
            array('goal_id, week_number, working_days, lead_gen_hours, contacts, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('one_thing_quantity, appointments_met, appointments_set, signed, signed, contracts, closings','numerical'),
            array('collection', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, goal_id, week_number, working_days, lead_gen_hours, contacts, appointments_set, appointments_met, signed, contracts, closings, one_thing_type, one_thing_quantity, updated_by, updated, added_by, added, collection', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'goal' => array(self::BELONGS_TO, 'Goals', 'goal_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'goal_id' => 'Goal',
            'week_number' => 'Week Number',
            'working_days' => 'Working Days',
            'lead_gen_hours' => 'Lead Gen Hours',
            'contacts' => 'Contacts',
            'appointments_set' => 'Appointments Set',
            'appointments_met' => 'Appointments Met',
            'signed' => 'Signed',
            'contracts' => 'Contracts',
            'closings' => 'Closings',
            'one_thing_type' => 'One Thing',
            'one_thing_quantity' => 'One Thing Quantity',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord) {

            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    public function getOneThingTypes()
    {
        return array(
            'Signed Agreements' => array(
                self::ONE_THING_TYPE_SIGNED => 'Either Signed Agreements',
                self::ONE_THING_TYPE_SELLER_SIGNED => 'Seller Signed Agreements',
                self::ONE_THING_TYPE_BUYER_SIGNED => 'Buyer Signed Agreements',
            ),
            'Met Appointments' => array(
                self::ONE_THING_TYPE_APPOINTMENT_MET=> 'Either Appointments Met',
                self::ONE_THING_TYPE_SELLER_APPOINTMENT_MET => 'Seller Appointments Met',
                self::ONE_THING_TYPE_BUYER_APPOINTMENT_MET => 'Buyer Appointments Met',
            ),
            'Set Appointments' => array(
                self::ONE_THING_TYPE_APPOINTMENT_SET=> 'Either Appointments Set',
                self::ONE_THING_TYPE_SELLER_APPOINTMENT_SET => 'Seller Appointments Set',
                self::ONE_THING_TYPE_BUYER_APPOINTMENT_SET => 'Buyer Appointments Set',
            ),
//            'Lead Gen Hours' => 'Lead Gen Hours',
//            'Contacts' => 'Contacts',
        );
    }

    public function weekNumberDates($weekNumber, $year, $dateFormat='M j')
    {
        $dateObj = new DateTime();
        $dateObj->setISODate($year, $weekNumber);
//        $dateObj->modify('-1 days');
        $return[0] = $dateObj->format($dateFormat);
        $dateObj->modify('+6 days');
        $return[1] = $dateObj->format($dateFormat);

        return $return;
    }

    public function getFieldCollectionName($fieldName, $i)
    {
        return "GoalsWeekly[collection][$i][$fieldName]";
    }

    public static function getOneThingType($goalId)
    {
        if($model = GoalsWeekly::model()->find(array('condition'=>'goal_id='.$goalId))) {
            return $model->one_thing_type;
        }
        return false;
    }

    public static function getGoalNumber($goalId, $contactId, $startDate, $endDate, $goalUnit = 'one_thing_quantity')
    {
        // calculate monthly one thing from weekly goal, parse date/weekdays properly for the current month
        $firstDay = $startDate;
        $date = new DateTime($firstDay);
        $startWeekNumber = $date->format("W");
        $startPercentWeek = 1 - self::getPercentWeek($firstDay);

        $lastDay = $endDate;
        $date = new DateTime($lastDay);
        $endWeekNumber = $date->format("W");
        $endPercentWeek = self::getPercentWeek($lastDay);

        $monthlyGoalCount = 0;
        //@todo: next robust feature is to detect if one thing UNIT is different within the month. Perhaps not allow this?
        // add up the unit count for one thing... // @todo: for now only option is appointments set

        for($i=$startWeekNumber; $i<=$endWeekNumber; $i++) {

            $thisWeekGoal = GoalsWeekly::model()->find(array('condition'=>'goal_id='.$goalId.' AND week_number ='.number_format($i)));

            if($i==$startWeekNumber) {
                $monthlyGoalCount += $thisWeekGoal->{$goalUnit} * $startPercentWeek;
            }
            elseif($i==$endWeekNumber) {
                $monthlyGoalCount += $thisWeekGoal->{$goalUnit} * $endPercentWeek;
            }
            else {
                $monthlyGoalCount += $thisWeekGoal->{$goalUnit};
            }
        }

        // round up goal count
        $monthlyGoalCount = ceil($monthlyGoalCount);

        if($monthlyGoalCount == 0) {

            //check to see if the month is before the goal start date

            //throw exception
//            throw new Exception(__CLASS__.' (:'.__LINE__.') Monthly goal count is zero and is invalid.');
        }

        return $monthlyGoalCount;
    }

    public static function getPercentWeek($date)
    {
        $weekday = date("l", strtotime($date));
        $weekPercent = 0;
        switch($weekday) {

            case 'Monday':
                $weekPercent = 0;
                break;

            case 'Tuesday':
                $weekPercent = 0.2;
                break;

            case 'Wednesday':
                $weekPercent = 0.4;
                break;

            case 'Thursday':
                $weekPercent = 0.6;
                break;

            case 'Friday':
                $weekPercent = 0.8;
                break;

            case 'Saturday':
            case 'Sunday':
                $weekPercent = 1;
                break;

            default:
                // throw exception / error
                break;
        }
        return $weekPercent;
    }

    public function getGoalResults($goalId, $contactId, $startDate, $endDate, $goalUnit)
    {
        $oneThingCount = 0;
        $oneThingCountCommand = Yii::app()->db->createCommand()
            ->select('count(id) as count')
            ->from('appointments')
            ->where('is_deleted=0');

        switch($goalUnit) {

            case GoalsWeekly::ONE_THING_TYPE_SIGNED:
                $dateRangeField = 'signed_date';
                $contactField = 'met_by_id';

                $oneThingCountCommand
                    ->andWhere('is_signed='.Appointments::SIGN_STATUS_YES);
                break;

            case GoalsWeekly::ONE_THING_TYPE_SELLER_SIGNED:
                $dateRangeField = 'signed_date';
                $contactField = 'met_by_id';

                $oneThingCountCommand
                    ->andWhere('is_signed='.Appointments::SIGN_STATUS_YES)
                    ->andWhere('component_type_id='.ComponentTypes::SELLERS);
                break;

            case GoalsWeekly::ONE_THING_TYPE_BUYER_SIGNED:
                $dateRangeField = 'signed_date';
                $contactField = 'met_by_id';

                $oneThingCountCommand
                    ->andWhere('is_signed='.Appointments::SIGN_STATUS_YES)
                    ->andWhere('component_type_id='.ComponentTypes::BUYERS);
                break;

            case GoalsWeekly::ONE_THING_TYPE_APPOINTMENT_SET:
                $dateRangeField = 'set_on_datetime';
                $contactField = 'set_by_id';

                break;

            case GoalsWeekly::ONE_THING_TYPE_SELLER_APPOINTMENT_SET:
                $dateRangeField = 'set_on_datetime';
                $contactField = 'set_by_id';

                $oneThingCountCommand
                    ->andWhere('component_type_id='.ComponentTypes::SELLERS);
                break;

            case GoalsWeekly::ONE_THING_TYPE_BUYER_APPOINTMENT_SET:
                $dateRangeField = 'set_on_datetime';
                $contactField = 'set_by_id';

                $oneThingCountCommand
                    ->andWhere('component_type_id='.ComponentTypes::BUYERS);
                break;

            case GoalsWeekly::ONE_THING_TYPE_APPOINTMENT_MET:
                $dateRangeField = 'set_for_datetime';
                $contactField = 'met_by_id';

                $oneThingCountCommand
                    ->andWhere('met_status_ma='.Appointments::MET_STATUS_MET);

                break;

            case GoalsWeekly::ONE_THING_TYPE_SELLER_APPOINTMENT_MET:
                $dateRangeField = 'set_for_datetime';
                $contactField = 'met_by_id';

                $oneThingCountCommand
                    ->andWhere('met_status_ma='.Appointments::MET_STATUS_MET)
                    ->andWhere('component_type_id='.ComponentTypes::SELLERS);
                break;

            case GoalsWeekly::ONE_THING_TYPE_BUYER_APPOINTMENT_MET:
                $dateRangeField = 'set_for_datetime';
                $contactField = 'met_by_id';

                $oneThingCountCommand
                    ->andWhere('met_status_ma='.Appointments::MET_STATUS_MET)
                    ->andWhere('component_type_id='.ComponentTypes::BUYERS);
                break;

            case 'contracts':
                $oneThingCountCommand
                    ->select('count(distinct(c.id))')
                    ->from('closings c')
                    ->join('transactions t', 't.id=c.transaction_id')
                    ->rightJoin('assignments a', 't.id=a.component_id AND t.component_type_id=a.component_type_id AND a.is_deleted=0')
                    ->where('c.is_deleted=0');
                $dateRangeField = 'DATE(c.contract_execute_date)';
                $contactField = 'a.assignee_contact_id';
                break;

            case 'closings':
                $oneThingCountCommand
                    ->select('count(distinct(c.id))')
                    ->from('closings c')
                    ->join('transactions t', 't.id=c.transaction_id')
                    ->rightJoin('assignments a', 't.id=a.component_id AND t.component_type_id=a.component_type_id AND a.is_deleted=0')
                    ->where('c.is_deleted=0');
                $dateRangeField = 'DATE(c.actual_closing_datetime)';
                $contactField = 'a.assignee_contact_id';
                break;

            case 'contacts':
                $oneThingCountCommand
                    ->select('count(a.id) as count')
                    ->from('activity_log a')
                    ->where('a.is_deleted=0')
                    ->andWhere('a.lead_gen_type_ma > 0')
                    ->andWhere('a.is_spoke_to = 1');
                $dateRangeField = 'DATE(a.activity_date)';
                $contactField = 'a.added_by';
                break;

            case 'leadgen':
                $oneThingCountCommand
                    ->select('ROUND(SUM(TIME_TO_SEC(TIMEDIFF(end_time,start_time)))/3600) hours') //@todo: **** SUM UP THE HOURS ****
                    ->from('timeblocking t')
                    ->where('t.is_deleted=0')
                    ->andWhere('t.timeblock_type_ma IN('.Timeblockings::LEAD_GEN_TYPE_PHONE_ID.')');
                $dateRangeField = 'DATE(t.date)';
                $contactField = 't.contact_id';
                break;

            default:
                throw new Exception(__CLASS__.' (:'.__LINE__.') One Thing Unit missing in Goals Weekly');
                break;
        }

        $oneThingCount = $oneThingCountCommand
            ->andWhere($contactField.'='.$contactId)
            ->andWhere($dateRangeField.' >="'.$startDate.'"')
            ->andWhere($dateRangeField.' <="'.$endDate.'"')
            ->queryScalar();

        return $oneThingCount;
    }

    public function getGoalPercent($goalId, $startDate, $endDate)
    {
        if(!($goal = Goals::model()->findByPk($goalId))) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Goal not found.');
        }
        try {
            //@todo: FYI, this is designed to handle only ONE type of unit for goal tracking. The GUI should enforce that.
            $goalNumber = GoalsWeekly::getGoalNumber($goalId, $goal->contact_id, $startDate, $endDate, 'one_thing_quantity');
        }
        catch(Exception $e) {
            return 0;
        }

        $weekNumber = date('W', strtotime($endDate));

        // get the unit for one thing
        $oneThingType = $this->getOneThingUnit($goalId);

        $goalResults = $this->getGoalResults($goalId, $goal->contact_id, $startDate, $endDate, $oneThingType);

        return ($goalNumber) ? round(($goalResults / $goalNumber) * 100) : 0;
    }

    public function getOneThingUnit($goalId)
    {
        $weekNumber = (int) date('W', strtotime(date('Y-m-d')));

        // get the unit for one thing based on this week
        return Yii::app()->db->createCommand("select one_thing_type from goals_weekly where goal_id={$goalId} AND week_number<={$weekNumber} ORDER BY week_number DESC LIMIT 1")->queryScalar();
    }

    public function getOneThingQuanity($goalId)
    {
        $weekNumber = (int) date('W', strtotime(date('Y-m-d')));

        // get the unit for one thing based on this week
        return Yii::app()->db->createCommand("select one_thing_quantity from goals_weekly where goal_id={$goalId} AND week_number<={$weekNumber} ORDER BY week_number DESC LIMIT 1")->queryScalar();
    }

    public function annualGoalByField($goalId, $fieldName)
    {
        return Yii::app()->db->createCommand("select sum($fieldName) from goals_weekly where goal_id={$goalId}")->queryScalar();
    }

    public function afterFind()
    {
        if($this->appointments_met) {
            $this->appointments_met = number_format($this->appointments_met, 1);
        }

        if($this->signed) {
            $this->signed = number_format($this->signed, 1);
        }

        if($this->contracts) {
            $this->contracts = number_format($this->contracts, 1);
        }
//
//        if($this->closings) {
//            $this->closings = number_format($this->closings);
//        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('goal_id',$this->goal_id);
        $criteria->compare('week_number',$this->week_number);
        $criteria->compare('working_days',$this->working_days);
        $criteria->compare('lead_gen_hours',$this->lead_gen_hours);
        $criteria->compare('contacts',$this->contacts);
        $criteria->compare('appointments_set',$this->appointments_set);
        $criteria->compare('appointments_met',$this->appointments_met);
        $criteria->compare('signed',$this->signed);
        $criteria->compare('contracts',$this->contracts);
        $criteria->compare('closings',$this->closings);
        $criteria->compare('one_thing_type',$this->one_thing_type,true);
        $criteria->compare('one_thing_quantity',$this->one_thing_quantity);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}