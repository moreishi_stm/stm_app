<?php

/**
 * This is the model class for table "lead_route_option_values".
 *
 * The followings are the available columns in table 'lead_route_option_values':
 * @property string $id
 * @property string $lead_route_option_id
 * @property integer $lead_route_id
 * @property string $value
 * @property string $added
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property LeadRouteOptions $leadRouteOption
 * @property LeadRoutes $leadRoute
 */
class LeadRouteOptionValues extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeadRouteOptionValues the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lead_route_option_values';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lead_route_option_id, lead_route_id', 'required'),
            array('lead_route_id', 'numerical', 'integerOnly'=>true),
            array('lead_route_option_id', 'length', 'max'=>10),
            array('value, added, updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, lead_route_option_id, lead_route_id, value, added, updated', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'leadRouteOption' => array(self::BELONGS_TO, 'LeadRouteOptions', 'lead_route_option_id'),
            'leadRoute' => array(self::BELONGS_TO, 'LeadRoutes', 'lead_route_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'lead_route_option_id' => 'Lead Route Option',
            'lead_route_id' => 'Lead Route',
            'value' => 'Value',
            'added' => 'Added',
            'updated' => 'Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('lead_route_option_id',$this->lead_route_option_id,true);
        $criteria->compare('lead_route_id',$this->lead_route_id);
        $criteria->compare('value',$this->value,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
} 