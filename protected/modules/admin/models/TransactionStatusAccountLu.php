<?php

	/**
	 * This is the model class for table "transaction_status_account_lu".
	 *
	 * The followings are the available columns in table 'transaction_status_account_lu':
	 *
	 * @property integer           $id
	 * @property integer           $account_id
	 * @property integer           $component_type_id
	 * @property integer           $transaction_status_id
	 * @property string            $override_name
	 *
	 * The followings are the available model relations:
	 * @property TransactionStatus $transactionStatus
	 * @property Accounts          $account
	 * @property ComponentTypes    $componentType
	 */
	class TransactionStatusAccountLu extends StmBaseActiveRecord {

        public $applyAccountScope = false;
		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TransactionStatusAccountLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'transaction_status_account_lu';
		}

		public function getPrimaryKey() {
			return array(
				'account_id',
				'component_type_id',
				'transaction_status_id'
			);
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, component_type_id, transaction_status_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'override_name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, component_type_id, transaction_status_id, override_name',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'transactionStatus' => array(
					self::BELONGS_TO,
					'TransactionStatus',
					'transaction_status_id'
				),
				// 'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
			);
		}

        public function scopes() {

            return array(
                'byNurtureType' => array(
                    'condition' => 'transaction_status_id <= ' . TransactionStatus::D_NURTURING_SPOKE_TO_ID . ' || transaction_status_id IN(' . TransactionStatus::ACTIVE_LISTING_ID.','.TransactionStatus::E_NOT_YET_INTERESTED.','.TransactionStatus::TRASH_ID.')',
                ),
            );
        }

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'component_type_id' => 'Transaction Type',
				'transaction_status_id' => 'Transaction Status',
				'override_name' => 'Override Name',
			);
		}

        public function byComponentTypeIds($componentTypeIds)
        {
            $componentTypeIds = (array) $componentTypeIds;
            $criteria = new CDbCriteria;
            $criteria->addInCondition('component_type_id', $componentTypeIds);
            $this->getDbCriteria()->mergeWith($criteria);

            return $this;
        }

        public function getDisplayName()
        {
            return (($this->override_name) ? $this->override_name : $this->transactionStatus->name);
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('transaction_status_id', $this->transaction_status_id);
			$criteria->compare('override_name', $this->override_name, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
