<?php

	/**
	 * This is the model class for table "activity_log_email_message_lu".
	 *
	 * The followings are the available columns in table 'activity_log_email_message_lu':
	 *
	 * @property integer $activity_log_id
	 * @property integer $email_message_id
	 *
	 * The followings are the available model relations:
	 */
	class ActivityLogEmailMessageLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ActionPlans the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'activity_log_email_message_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'activity_log_id, email_message_id',
					'required'
				),
				array(
					'activity_log_id, email_message_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'activity_log_id, email_message_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'activityLog' => array(
					self::BELONGS_TO,
					'ActivityLog',
					'activity_log_id'
				),
				'emailMessage' => array(
					self::BELONGS_TO,
					'EmailMessage',
					'email_message_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'activity_log_id' => 'Activity Log ID',
				'email_message_id' => 'Email Message ID'
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('activity_log_id', $this->activity_log_id);
			$criteria->compare('email_message_id', $this->email_message_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}


		public function getPrimaryKey() {
			return array(
				'activity_log_id',
				'email_message_id'
			);
		}
	}
