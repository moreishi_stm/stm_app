<?php
Yii::import('seizethemarket.protected.models.EmailUndeliverable');

/**
 * This is the model class for table "emails".
 *
 * The followings are the available columns in table 'emails':
 *
 * @property integer        $id
 * @property integer        $contact_id
 * @property integer        $email_status_id
 * @property string         $email
 * @property integer        $email_type_ma
 * @property integer        $owner_ma
 * @property integer        $is_deleted
 *
 * The followings are the available model relations:
 * @property EmailOptStatus $emailOptStatus
 */
class Emails extends StmBaseActiveRecord
{

    // Used to create a relationship between the email and the contact
    public $contactId;
    public $notAdmin;

    // Flag to remove the record during processing

    public $remove = false;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {

        return 'emails';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'email',
                'required'
            ),
            array(
                'account_id, contact_id, email_status_id, email, is_primary',
                'required',
                'on' => 'import'
            ),
            array(
                'id, contact_id, email_status_id, email_type_ma, owner_ma, is_primary, is_deleted',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'email',
                'length',
                'max' => 127
            ),
            array(
                'email',
                'unique',
//                'className' => 'Emails',
                'message'   => 'Email is already linked with another contact.',
//                'criteria'  => array(
//                    'condition' => 'account_id = :account_id AND contact_id <> :contact_id',
//                    'params'    => array(':account_id' => Yii::app()->user->getAccountId(),
//                                        ':contact_id'=>$this->contact_id,
//                                    ),
//                ),
                'on'        => array(
                    'insert',
                    'update',
                    'import',
                    'bounceUniqueCheck'
                )
            ),
            array(
                'email',
                'bouncedValidation',
                'on' => 'bounceUniqueCheck',
            ),
            array(
                'email',
                'email',
                'checkMX' => false,
            ),
            array(
                'email',
                'forgotPassword',
                'on' => 'forgotPassword'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'remove, is_primary',
                'safe'
            ),
            array(
                'id, email_status_id, email, email_type_ma, owner_ma, is_deleted',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contact'     => array(
                self::BELONGS_TO,
                'Contacts',
                'contact_id',
            ),
            'account'     => array(
                self::BELONGS_TO,
                'Accounts',
                'account_id'
            ),
            'emailStatus' => array(
                self::BELONGS_TO,
                'EmailStatus',
                'email_status_id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'id'              => 'Email ID',
            'email_status_id' => 'Email Status',
            'email'           => 'Email',
            'email_type_ma'   => 'Email Type Ma',
            'owner_ma'        => 'Owner Ma',
            'is_deleted'      => 'Is Deleted',
        );
    }

    public function byLikeEmail($emailAddress = null)
    {

        if ($emailAddress) {

            $Criteria            = new CDbCriteria;
            $Criteria->condition = 'LOWER(email) like :emailAddress';
            $Criteria->params    = array(':emailAddress' => '%' . $emailAddress . '%');
            $this->getDbCriteria()->mergeWith($Criteria);
        }

        return $this;
    }

    public function getIsBombBomb($boolean = true)
    {

        $BombBomb = Yii::app()->bombbomb;
        $data     = $BombBomb->getContactByEmail($this->email);
        if ($boolean == true) {
            return !empty($data[info][lists]);
        } else {
            return $data;
        }
    }

    /**
     * Determines if the current email is opted out of communication
     *
     * @return bool
     */
    public function getIsOptedOut()
    {

        $optedOutStatuses = Emails::optOutList();
        $emailIsOptedOut  = (in_array($this->email_status_id, $optedOutStatuses)) ? true : false;

        return $emailIsOptedOut;
    }

    public static function optOutList()
    {
        return array(
            EmailStatus::SOFT_BOUNCE,
            EmailStatus::HARD_BOUNCE,
            EmailStatus::OPT_OUT
        );
    }

    public function getIsBounced()
    {
        $bouncedStatuses      = array(
            EmailStatus::SOFT_BOUNCE,
            EmailStatus::HARD_BOUNCE,
        );
        $emailStatusIsBounced = (in_array($this->email_status_id, $bouncedStatuses)) ? true : false;

        // Check to see if the email is universally rejected if the status isn't already bounced
        if (!$emailStatusIsBounced) {
            $emailStatusIsBounced = $this->getIsUniversallyUndeliverable();
        }

        return $emailStatusIsBounced;
    }

    /**
     * Checks to see if the email is universally undeliverable, as defined by the stm_hq database
     *
     * @return bool
     */
    public function getIsUniversallyUndeliverable()
    {
        $undeliverableEmailEntry = EmailUndeliverable::model()->byEmail($this->email)->find();
        $emailIsUndeliverable    = ($undeliverableEmailEntry) ? true : false;

        return $emailIsUndeliverable;
    }

    /**
     * forgotPassword
     * Validates a user entered password reminder
     *
     * @author Chris Willard
     * @since  1/14/2013
     *
     * @param  $attribute
     * @param  $params
     *
     * @return null
     */
    public function forgotPassword($attribute, $params)
    {

        $email = $this->$attribute;

        $ContactFound = $this->byEmail($email)->find();

        if (!$ContactFound) {
            $this->addError($attribute, 'Could not locate a user with this e-mail.');
        }
    }

    /**
     * Checks to see if the email has been marked as 'bounced'
     *
     * @param $attribute
     * @param $params
     */
    public function bouncedValidation($attribute, $params)
    {

        $email                    = $this->$attribute;
        $emailBouncedErrorMessage = 'Sorry, this email has been marked as undeliverable.';

        /**
         * Email exists in the system, and has been marked as bounced (undeliverable)
         */
        $emailEntry = Emails::model()->byEmail($email)->find();
        if ($emailEntry && $emailEntry->getIsBounced()) {
            $this->addError($attribute, $emailBouncedErrorMessage);
        }

        /**
         * Email is marked as undeliverable globally
         */
        $globalEmailEntry = EmailUndeliverable::model()->byEmail($email)->find();
        if ($globalEmailEntry) {
            $this->addError($attribute, $emailBouncedErrorMessage);
        }
    }

    public function byEmail($emailAddress = null)
    {

        if ($emailAddress) {
            $Criteria            = new CDbCriteria;
            $Criteria->condition = 'LOWER(email) = :emailAddress';
            $Criteria->params    = array(':emailAddress' => strtolower($emailAddress));
            $this->getDbCriteria()->mergeWith($Criteria);
        }

        return $this;
    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Emails the static model class
     */
    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $alias = $this->getTableAlias($quote=false, $checkScopes=true);
        $criteria = new CDbCriteria;

        if($this->notAdmin) {
            $criteria->with = array('contact.userGroups');
            $criteria->together = true;
            $criteria->condition = 'itemname IS NULL';
        }

        $criteria->compare($alias.'.id', $this->id);
        $criteria->compare($alias.'.email_status_id', $this->email_status_id);
        $criteria->compare($alias.'.email', $this->email, true);
        $criteria->compare($alias.'.email_type_ma', $this->email_type_ma);
        $criteria->compare($alias.'.owner_ma', $this->owner_ma);
        $criteria->compare($alias.'.is_deleted', $this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize'=>50),
        ));
    }

    protected function beforeValidate()
    {

        $this->email = strtolower(trim($this->email));
        if(substr($this->email, -1) == '.') {
            $this->email = substr($this->email, 0, -1);
        }

        return parent::beforeValidate();
    }

    protected function beforeFind()
    {

        $this->email = trim($this->email);

        parent::beforeFind();
    }

    protected function beforeDelete()
    {
        // log in the activity log that this number was deleted
        $activityLog = new ActivityLog();
        $activityLog->setAttributes(array(
                'component_type_id' => ComponentTypes::CONTACTS,
                'component_id' => $this->contact_id,
                'task_type_id' => TaskTypes::EMAIL_RECORD_LOG,
                'activity_date' => new CDbExpression('NOW()'),
                'note' => 'Deleted Email: '.$this->email,
            ));
        if(!$activityLog->save()) {
            Yii::log(__FILE__ . '(' . __LINE__ . '): Error saving Activity log for delete email. Activity Log attributes:'.print_r($activityLog->attributes, true).' Phone Record: ' . print_r($this->attributes, true), CLogger::LEVEL_ERROR);
        }

        parent::beforeDelete();
    }
}
