<?php
	Yii::import('admin_module.models.interfaces.Component');

	/**
	 * This is the model class for table "recruits".
	 *
	 * The followings are the available columns in table 'recruits':
	 *
	 * @property integer  $id
     * @property integer  $account_id
	 * @property integer  $contact_id
	 * @property integer  $source_id
	 * @property integer  $status_id
     * @property integer  $is_licensed
     * @property string   $licensed_date
     * @property string   $license_exam_approval_date
	 * @property integer  $disc_D
	 * @property integer  $disc_I
	 * @property integer  $disc_S
	 * @property integer  $disc_C
	 * @property integer  $ava_vector_1
	 * @property integer  $ava_vector_1_asterisk
	 * @property integer  $ava_vector_2
	 * @property integer  $ava_vector_2_asterisk
	 * @property integer  $ava_vector_3
	 * @property integer  $ava_vector_3_asterisk
	 * @property integer  $ava_vector_4
	 * @property integer  $ava_vector_4_asterisk
	 * @property integer  $ava_vector_5
	 * @property integer  $ava_vector_5_asterisk
	 * @property string   $current_brokerage
     * @property string   $office_address
     * @property string   $brokerage_mls_id
     * @property string   $brokerage_license_number
     * @property string   $agent_mls_id
     * @property string   $agent_license_number
     * @property string   $website
     * @property integer  $ranking
     * @property integer  $annual_units
     * @property integer  $annual_units_change
     * @property integer  $annual_volume
     * @property integer  $annual_volume_change
     * @property string   $annual_data_updated
	 * @property string   $goal
	 * @property string   $dream_goal
	 * @property string   $motivation
	 * @property integer  $application_status_ma
	 * @property integer  $met_status_ma
	 * @property integer  $rating
	 * @property string   $strengths
	 * @property string   $weaknesses
	 * @property string   $communication_skills
	 * @property string   $experience
	 * @property string   $education
	 * @property string   $current_employer
	 * @property string   $years_current_job
	 * @property string   $years_previous_job
	 * @property string   $average_years_job
	 * @property string   $notes
     * @property integer  $spouse_first_name
	 * @property integer  $updated_by
	 * @property string   $updated
	 * @property integer  $added_by
	 * @property string   $added
     * @property integer  $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts $addedBy
	 * @property Contacts $contact
	 * @property Contacts $updatedBy
	 */
	class Recruits extends StmBaseActiveRecord implements Component {

		const REFERRER = 23;

		public $typesCollection;
        public $excludeContactIds;
        public $excludeStatusIds;
        public $excludeTypesCollection;
        public $fromLicenseDate;
        public $toLicenseDate;
        public $addedFromDate;
        public $addedToDate;
        public $assignmentId;
        public $assignmentCollection = array();
        public $dateSearchFilter;
		private $_componentType;
        public $annualUnitsMin;
        public $annualUnitsMax;
        public $annualVolumeMin;
        public $annualVolumeMax;
        public $annualVolumeChangeMin;
        public $annualVolumeChangeMax;
        public $annualVolumeChangePercentMin;
        public $annualVolumeChangePercentMax;
        public $hasAppointment;
        public $callListId;
        public $myMarketCenter;
        public $excludeBrokerageName;
        public $contactEmails = array();
        public $contactAddresses = array();
        public $contactPhones = array();
		public $referred_by_ids = array();
		public $noActionPlan = NULL;
		public $noTasks = NULL;
        public $actionPlanId = NULL;

        public $appointmentStartDate = NULL;
        public $appointmentEndDate = NULL;

        public $sortByCapStatus = NULL;
        public $setByIds = NULL;
        public $sortByRating = NULL;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Recruits the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'recruits';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, contact_id, source_id, status_id, application_status_ma, met_status_ma, updated, added',
					'required'
				),
                array('contact_id','unique'),
                array('contact_id','validateDupe'),
                array(
					'recruit_production_status_id, account_id, contact_id, source_id, referred_by_id, status_id, is_licensed, disc_D, disc_I, disc_S, disc_C, ava_vector_1, ava_vector_1_asterisk, ava_vector_2, ava_vector_2_asterisk, ava_vector_3, ava_vector_3_asterisk, ava_vector_4, ava_vector_4_asterisk, ava_vector_5, ava_vector_5_asterisk, office_zip, annual_units, annual_volume, annual_volume_change, application_status_ma, met_status_ma, updated_by, added_by, is_deleted, assignmentId, callListId, myMarketCenter, ranking',
					'numerical',
					'integerOnly' => true
				),
                array(
                    'annual_volume_change_percent',
                    'numerical',
                ),
                array(
                    'office_address',
                    'length',
                    'max' => 100
                ),
                array(
                    'office_city, spouse_first_name, spouse_last_name, spouse_email',
                    'length',
                    'max' => 50
                ),
				array(
					'goal, dream_goal, motivation, years_current_job, years_previous_job, average_years_job',
					'length',
					'max' => 127
				),
                array(
                    'motivation, challenges',
                    'length',
                    'max' => 65535
                ),
                array(
					'current_brokerage, strengths, weaknesses, communication_skills, experience, education',
					'length',
					'max' => 255
				),
                array(
                    'current_employer',
                    'length',
                    'max' => 63
                ),
                array(
					'brokerage_mls_id, brokerage_license_number, agent_mls_id, agent_license_number',
					'length',
					'max' => 50
				),
				array(
					'rating, typesCollection, excludeTypesCollection, licensed_date, license_exam_approval_date, agent_mls_id, disc_date, ava_date, annual_data_updated, target_date, hire_date, notes, spouse_first_name, spouse_last_name, spouse_email, spouse_phone, added_by, updated_by, fromLicenseDate, toLicenseDate, addedFromDate, addedToDate, dateSearchFilter, annualUnitsMin, annualUnitsMax, annualVolumeMin, annualVolumeMax, annualVolumeChangeMin, annualVolumeChangeMax, annualVolumeChangePercentMin, annualVolumeChangePercentMax, hasAppointment, annual_volume_change, annual_volume_change_percent, excludeBrokerageName, contactEmails, contactAddresses, contactPhones, website, noActionPlan, noTasks, actionPlanId, excludeStatusIds',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, typesCollection, excludeTypesCollection, account_id, contact_id, source_id, referred_by_id, status_id, is_licensed, licensed_date, license_exam_approval_date, agent_mls_id, disc_D, disc_I, disc_S, disc_C, disc_date, ava_vector_1, ava_vector_1_asterisk, ava_vector_2, ava_vector_2_asterisk, ava_vector_3, ava_vector_3_asterisk, ava_vector_4, ava_vector_4_asterisk, ava_vector_5, ava_vector_5_asterisk, ava_date, current_brokerage, goal, dream_goal, motivation, challenges, application_status_ma, met_status_ma, rating, strengths, weaknesses, $communication_skills, experience, education, current_employer, years_current_job, years_previous_job, average_years_job, notes, updated_by, updated, added_by, added, is_deleted, fromLicenseDate, toLicenseDate, annual_units, annual_volume, annual_volume_change, annual_units_change_percent, annual_data_updated, office_address, office_city, office_zip, assignmentId, dateSearchFilter, annualUnitsMin, annualUnitsMax, annualVolumeMin, annualVolumeMax, hasAppointment, callListId, excludeBrokerageName, myMarketCenter, ranking, website, noTasks, noActionPlan, target_date, hire_date, actionPlanId, spouse_first_name, spouse_last_name, spouse_email, spouse_phone, excludeStatusIds',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
                'assignments' => array(
                    self::HAS_MANY,
                    'Assignments',
                    'component_id',
                    'condition' => 'assignments.component_type_id = :component_type_id AND assignments.assignment_type_id != :excludeReferrerType',
                    'params' => array(':component_type_id' => ComponentTypes::RECRUITS, ':excludeReferrerType'=> AssignmentTypes::REFERRER),
                ),
                'referrers' => array(
                    self::HAS_MANY,
                    'Assignments',
                    'component_id',
                    'condition' => 'referrers.component_type_id = :component_type_id AND referrers.assignment_type_id = :referrerType',
                    'params' => array(':component_type_id' => ComponentTypes::RECRUITS, ':referrerType'=> AssignmentTypes::REFERRER),
                ),
				'types' => array(
					self::MANY_MANY,
					'RecruitTypes',
					'recruit_type_lu(recruit_id, recruit_type_id)'
				),
				'recruitTypeLu' => array(
					self::HAS_MANY,
					'RecruitTypeLu',
					'recruit_id'
				),
				'source' => array(
					self::BELONGS_TO,
					'Sources',
					'source_id'
				),
				'referredBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'referred_by_id'
				),
                'appointment' => array(
                    self::HAS_ONE,
                    'Appointments',
                    'component_id',
                    'condition' => 'appointment.component_type_id = :component_type_id AND appointment.is_deleted=0',
                    'params' => array(':component_type_id' => ComponentTypes::RECRUITS)
                ),
                'appointmentBySetForDateRange' => array(
                    self::HAS_ONE,
                    'Appointments',
                    'component_id',
                    'condition' => 'appointmentBySetForDateRange.component_type_id = :component_type_id AND appointmentBySetForDateRange.is_deleted=0 AND appointmentBySetForDateRange.set_for_datetime >= :set_for_date_time_start AND appointmentBySetForDateRange.set_for_datetime <= :set_for_date_time_end',
                    'params' => array(
                        ':component_type_id' => ComponentTypes::RECRUITS,
                        ':set_for_date_time_start' => $this->appointmentStartDate,
                        ':set_for_date_time_end' => $this->appointmentEndDate
                    )
                ),
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
                'status' => array(
                    self::BELONGS_TO,
                    'TransactionStatus',
                    'status_id'
                ),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
                'capStatus' => array(
                    self::BELONGS_TO,
                    'RecruitProductionStatuses',
                    'recruit_production_status_id',
                )
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'source_id' => 'Source',
				'status_id' => 'Status',
				'type' => 'Recruit Type',
				'is_licensed' => 'Licensed',
                'agent_mls_id' => 'MLS Agent ID',
				'current_brokerage' => 'Current Brokerage',
				'goal' => 'Goal',
				'dream_goal' => 'Dream Goal',
				'motivation' => 'Motivation',
				'application_status_ma' => 'Application',
				'met_status_ma' => 'Met',
				'rating' => 'Rating',
				'strengths' => 'Strengths',
				'weaknesses' => 'Weaknesses',
				'$communication_skills' => 'Communication Skills',
				'experience' => 'Experience',
				'education' => 'Education',
				'current_employer' => 'Current Employer',
				'years_current_job' => 'Years Current Job',
				'years_previous_job' => 'Years Previous Job',
				'average_years_job' => 'Average Years Job',
				'notes' => 'Notes',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'added_by' => 'Added By',
				'added' => 'Added',
				"noTasks" => "No Tasks",
				"noActionPlan" => "No Action Plan"
			);
		}

		/**
		 * @return array behaviors for getTasks
		 */
		public function behaviors() {
			return array(
				'getTasks' => array(
					'class' => 'admin_module.components.behaviors.GetTasksBehavior',
				),
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
			);
		}

		public function getContactId() {
			return $this->contact->id;
		}

		public function getComponentChain() {
			$chain = array($this->asComponentTuple());

			$includeCompleted = true;
			$Tasks = $this->getTasks($this, $includeCompleted);

			if (count($Tasks) > 0) {
				foreach ($Tasks as $Task) {
					array_push($chain, $Task->asComponentTuple());
				}
			}

			return $chain;
		}

		public function asComponentTuple() {
			return array(
				'componentTypeId' => $this->componentType->id,
				'componentId' => $this->id
			);
		}

		public function getComponentType() {
			if (!isset($this->_componentType)) {
				$this->_componentType = ComponentTypes::model()->findByPk(ComponentTypes::RECRUITS);
			}

			return $this->_componentType;
		}

		protected function afterFind() {
			$this->disc_date = ($this->disc_date < 1) ? null : Yii::app()->format->formatDate($this->disc_date, StmFormatter::APP_DATE_PICKER_FORMAT);
			$this->ava_date = ($this->ava_date < 1) ? null : Yii::app()->format->formatDate($this->ava_date, StmFormatter::APP_DATE_PICKER_FORMAT);
            $this->licensed_date = ($this->licensed_date < 1) ? null : Yii::app()->format->formatDate($this->licensed_date, StmFormatter::APP_DATE_PICKER_FORMAT);
            $this->annual_data_updated = ($this->annual_data_updated < 1) ? null : Yii::app()->format->formatDate($this->annual_data_updated, StmFormatter::APP_DATE_PICKER_FORMAT);

			parent::afterFind();
		}

		protected function beforeValidate() {

			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');
                if(empty($this->added_by)) {
                    $this->added_by = Yii::app()->user->id;
                }
                if(empty($this->updated_by)) {
                    $this->updated_by = Yii::app()->user->id;
                }
			}
            else {
                $this->updated_by = Yii::app()->user->id;
            }
            if(!$this->account_id) {
                $this->account_id = Yii::app()->user->accountId;
            }

            $this->annual_units = ($this->annual_units > 0) ? Yii::app()->format->formatInteger($this->annual_units) : $this->annual_units;

            $this->annual_volume = ($this->annual_volume) ? Yii::app()->format->formatInteger($this->annual_volume) : $this->annual_volume;
            $this->annual_volume_change = ($this->annual_volume_change) ? Yii::app()->format->formatInteger($this->annual_volume_change) : $this->annual_volume_change;
            $this->annual_volume_change_percent = ($this->annual_volume_change_percent) ? Yii::app()->format->formatDecimal($this->annual_volume_change_percent) : $this->annual_volume_change_percent;
            $this->updated = new CDbExpression('NOW()');

            if(!is_object($this->annual_data_updated)) {
                $this->annual_data_updated = ($this->annual_data_updated < 1) ? null : Yii::app()->format->formatDate($this->annual_data_updated, StmFormatter::MYSQL_DATETIME_FORMAT);
                if(strpos($this->annual_data_updated, '1969-12') !== false) {
                    $this->annual_data_updated = null;
                }
            }

			return parent::beforeValidate();
		}

        public function validateDupe($attribute, $params)
        {
            if($this->isNewRecord && $this->contact_id) {
                // see if recruit already exists for this contact
                if($recruits = Recruits::model()->findAllByAttributes(array('contact_id'=>$this->contact_id))) {
                    $this->addError($attribute, 'Recruit already exists for this contact.');
                }
            }
        }

		protected function beforeSave()
        {
			if ($this->disc_date > 0) {
				$this->disc_date = Yii::app()->format->formatDate($this->disc_date, StmFormatter::MYSQL_DATETIME_FORMAT);
			} else {
				$this->disc_date = null;
			}

			if ($this->ava_date > 0) {
				$this->ava_date = Yii::app()->format->formatDate($this->ava_date, StmFormatter::MYSQL_DATETIME_FORMAT);
			} else {
				$this->ava_date = null;
			}

            if ($this->licensed_date > 0) {
                $this->licensed_date = Yii::app()->format->formatDate($this->licensed_date, StmFormatter::MYSQL_DATETIME_FORMAT);

                // check for invalid date
                if(strpos($this->licensed_date, '1969-12') == false) {
                    $this->is_licensed = 1;
                }
                else {
                    $this->licensed_date = null;
                }
            } else {
                $this->licensed_date = null;
            }

            if ($this->hire_date > 0) {
                $this->hire_date = Yii::app()->format->formatDate($this->hire_date, StmFormatter::MYSQL_DATETIME_FORMAT);

                // check for invalid date
                if(strpos($this->hire_date, '1969-12') == false) {
                    $this->is_licensed = 1;
                }
                else {
                    $this->hire_date = null;
                }
            } else {
                $this->hire_date = null;
            }

            if ($this->target_date > 0) {
                $this->target_date = Yii::app()->format->formatDate($this->target_date, StmFormatter::MYSQL_DATETIME_FORMAT);

                // check for invalid date
                if(strpos($this->target_date, '1969-12') == false) {
                    $this->is_licensed = 1;
                }
                else {
                    $this->target_date = null;
                }
            } else {
                $this->target_date = null;
            }

            if ($this->license_exam_approval_date > 0) {
                $this->license_exam_approval_date = Yii::app()->format->formatDate($this->license_exam_approval_date, StmFormatter::MYSQL_DATE_FORMAT);

                // check for invalid date
                if(strpos($this->license_exam_approval_date, '1969-12') !== false) {
                    $this->license_exam_approval_date = null;
                }
            } else {
                $this->license_exam_approval_date = null;
            }

			return parent::beforeSave();
		}

		public function printDISCDetails($opts = null) {
			$string = "";
			if ($this->disc_D || $this->disc_I || $this->disc_S || $this->disc_C) {
				$string .= '<div class="disc-data">';
				$string .= ($opts['parentheses'] == true) ? '(' : '';
				$string .= '<span>D:' . $this->conditionalPrint('disc_D') . '</span>';
				$string .= '<span>I:' . $this->conditionalPrint('disc_I') . '</span>';
				$string .= '<span>S:' . $this->conditionalPrint('disc_S') . '</span>';
				$string .= 'C:' . $this->conditionalPrint('disc_C');
				$string .= ($opts['parentheses'] == true) ? ')' : '';
				$string .= '</div>';
			}

			return $string;
		}

		public function getDISCProfile() {
			$discArray = array();

			if ($this->disc_D >= 50) {
				$discArray['D'] = $this->disc_D;
			}
			if ($this->disc_I >= 50) {
				$discArray['I'] = $this->disc_I;
			}
			if ($this->disc_S >= 50) {
				$discArray['S'] = $this->disc_S;
			}
			if ($this->disc_C >= 50) {
				$discArray['C'] = $this->disc_C;
			}

			krsort($discArray);
			foreach ($discArray as $key => $value) {
				$string .= $key;
			}

			return $string;
		}

		public function printAVAProfile($opts = null) {

			if ($this->ava_vector_1 || $this->ava_vector_2 || $this->ava_vector_3 || $this->ava_vector_4 || $this->ava_vector_5) {
				$string .= $this->conditionalPrint('ava_vector_1');
				$string .= ($this->conditionalPrint('ava_vector_1_asterisk')) ? '*' : '';
				$string .= $this->conditionalPrint('ava_vector_2');
				$string .= ($this->conditionalPrint('ava_vector_2_asterisk')) ? '*' : '';
				$string .= $this->conditionalPrint('ava_vector_3');
				$string .= ($this->conditionalPrint('ava_vector_3_asterisk')) ? '*' : '';
				$string .= $this->conditionalPrint('ava_vector_4');
				$string .= ($this->conditionalPrint('ava_vector_4_asterisk')) ? '*' : '';
				$string .= ($this->ava_vector_5) ? '-' : '';
				$string .= $this->conditionalPrint('ava_vector_5');
				$string .= ($this->conditionalPrint('ava_vector_5_asterisk')) ? '*' : '';
			}

			return $string;
		}

		public function conditionalPrint($field, $opts = null) {
			if ($this->$field || $this->$field == '0') {
				if (isset($opts['booleanValue0']) || isset($opts['booleanValue1'])) {
					$string = ($opts['booleanValue0'] == $this->$field) ? $opts['booleanValue0'] : $opts['booleanValue1'];
				} else {
					$string = $this->$field;
				}

				$string .= ($opts['label']) ? $opts['label'] : '';
			}

			return $string;
		}

        public function getStatusName()
        {
            return TransactionStatusAccountLu::model()->findByAttributes(array('transaction_status_id'=>$this->status_id, 'component_type_id'=>ComponentTypes::RECRUITS))->displayName;
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 *
		 * @param $returnAll to return all of the records when updating
		 */
		public function search($returnAll = false) {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;
            $criteria->with = array();

			$criteria->compare('id', $this->id);

			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('t.source_id', $this->source_id);

            if(!empty($this->status_id)) {
                $criteria->addInCondition('status_id', $this->status_id);
            }

            if(!empty($this->excludeStatusIds)) {
                $criteria->addNotInCondition('status_id', (array) $this->excludeStatusIds);
            }

            if(!empty($this->myMarketCenter)) {
                $criteria->addInCondition('status_id', (array) TransactionStatus::ACTIVE_LISTING_ID);
            }

            $criteria->compare('is_licensed', $this->is_licensed);
            $criteria->compare('agent_mls_id', $this->agent_mls_id);
			$criteria->compare('disc_D', '>=' . $this->disc_D, true);
			$criteria->compare('disc_I', '>=' . $this->disc_I, true);
			$criteria->compare('disc_S', '>=' . $this->disc_S, true);
			$criteria->compare('disc_C', '>=' . $this->disc_C, true);
			$criteria->compare('disc_date', $this->disc_date, true);
			$criteria->compare('ava_vector_1', '>=' . $this->ava_vector_1, true);
			$criteria->compare('ava_vector_2', '>=' . $this->ava_vector_2, true);
			$criteria->compare('ava_vector_3', '>=' . $this->ava_vector_3, true);
			$criteria->compare('ava_vector_4', '>=' . $this->ava_vector_4, true);
			$criteria->compare('ava_vector_5', '>=' . $this->ava_vector_5, true);
			$criteria->compare('ava_date', $this->ava_date, true);
			$criteria->compare('current_brokerage', $this->current_brokerage, true);
            if($this->excludeBrokerageName) {

                $excludeOffices = (strpos($this->excludeBrokerageName, ',') !== false) ? explode(',', $this->excludeBrokerageName) : (array) $this->excludeBrokerageName;

                $excludeOfficeParams = array();
                $condition = '';
                foreach($excludeOffices as $i => $excludeOffice) {

                    if($excludeOffice != '') {
                        $paramName = ':excludeOfficeName'.$i;
                        $criteria->addCondition('current_brokerage NOT LIKE '.$paramName);
                        $excludeOfficeParams[$paramName] = "%".trim($excludeOffice)."%";
                    }
                }

                if(count($excludeOfficeParams)) {
                    $criteria->params = CMap::mergeArray($criteria->params, $excludeOfficeParams);
                }
            }

            $criteria->compare('office_city', $this->office_city, true);

            if($this->office_zip) {

                $zips = (strpos($this->office_zip, ',') !== false) ? explode(',', $this->office_zip) : (array) $this->office_zip;
                $criteria->addInCondition('office_zip', $zips);
            }

            $criteria->compare('agent_mls_id', $this->agent_mls_id, true);
            $criteria->compare('brokerage_mls_id', $this->brokerage_mls_id, true);
            $criteria->compare('annual_units', '>='.$this->annual_units);
			$criteria->compare('goal', $this->goal, true);
			$criteria->compare('dream_goal', $this->dream_goal, true);
			$criteria->compare('motivation', $this->motivation, true);
			$criteria->compare('application_status_ma', $this->application_status_ma);
			$criteria->compare('met_status_ma', $this->met_status_ma);
			$criteria->compare('rating', $this->rating);
			$criteria->compare('strengths', $this->strengths, true);
			$criteria->compare('weaknesses', $this->weaknesses, true);
			$criteria->compare('communication_skills', $this->communication_skills, true);
			$criteria->compare('experience', $this->experience, true);
			$criteria->compare('education', $this->education, true);
			$criteria->compare('current_employer', $this->current_employer, true);
			$criteria->compare('years_current_job', $this->years_current_job, true);
			$criteria->compare('years_previous_job', $this->years_previous_job, true);
			$criteria->compare('average_years_job', $this->average_years_job, true);
			$criteria->compare('notes', $this->notes, true);
			$criteria->compare('updated_by', $this->updated_by);
			$criteria->compare('updated', $this->updated, true);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('added', $this->added, true);

            if($this->callListId) {
                $criteria->addCondition("(SELECT COUNT(clp.id) FROM call_list_phones clp JOIN phones p2 ON p2.id=clp.phone_id WHERE clp.call_list_id=".intval($this->callListId)." AND p2.contact_id=t.contact_id) > 0");
            }

            if($this->annualUnitsMin) {
                $criteria->compare('annual_units', '>='.$this->annualUnitsMin);
            }
            if($this->annualUnitsMax) {
                $criteria->compare('annual_units', '<'.$this->annualUnitsMax);
            }

            if($this->annualVolumeMin) {
                $criteria->compare('annual_volume', '>='.$this->annualVolumeMin);
            }
            if($this->annualVolumeMax) {
                $criteria->compare('annual_volume', '<'.$this->annualVolumeMax);
            }
            if($this->annualVolumeChangeMin) {
                $criteria->compare('annual_volume_change', '>='.$this->annualVolumeChangeMin);
            }
            if($this->annualVolumeChangeMax) {
                $criteria->compare('annual_volume_change', '<'.$this->annualVolumeChangeMax);
            }
            if($this->annualVolumeChangePercentMin) {
                $criteria->compare('annual_volume_change_percent', '>='.$this->annualVolumeChangePercentMin);
            }
            if($this->annualVolumeChangePercentMax) {
                $criteria->compare('annual_volume_change_percent', '<'.$this->annualVolumeChangePercentMax);
            }

            if($this->addedFromDate) {
                $criteria->compare('DATE(added)', '>='.date('Y-m-d', strtotime($this->addedFromDate)));
            }
            if($this->addedToDate) {
                $criteria->compare('DATE(added)', '<='.date('Y-m-d', strtotime($this->addedToDate)));
            }

            if($this->hasAppointment || !empty($this->appointmentStartDate) || !empty($this->appointmentEndDate)) {
                $criteria->with = CMap::mergeArray($criteria->with, array('appointment'));
            }

            if($this->hasAppointment) {
                $criteria->together = true;
                $criteria->compare('appointment.id', '>=1');
            }

            if(!empty($this->appointmentStartDate)) {
                $criteria->compare('DATE(appointment.set_for_datetime)', '>='.date('Y-m-d', strtotime($this->addedFromDate)));
            }

            if(!empty($this->appointmentEndDate)) {
                $criteria->compare('DATE(appointment.set_for_datetime)', '<='.date('Y-m-d', strtotime($this->addedFromDate)));
            }

			if ($this->contact->first_name || $this->contact->last_name || trim($this->contact->contactFullName)) {
				// Filter search by a contact's first name, last name, or a combination of the two.
				$criteria->with = CMap::mergeArray($criteria->with, array('contact'));

				// Search explicitly by first name
				if ($this->contact->first_name) {
					$criteria->compare('contact.first_name', $this->contact->first_name, true);
				}

				// Search explicitly by last name
				if ($this->contact->last_name) {
					$criteria->compare('contact.last_name', $this->contact->last_name, true);
				}

				// Search any portion of either name fields
				if ($this->contact->contactFullName) {
					$criteria->compare("CONCAT(contact.first_name,' ',contact.last_name)", $this->contact->contactFullName, true);
				}
			}

			if ($this->contact->emails->email) { // Filter by a contact's e-mail address
				$criteria->with = CMap::mergeArray($criteria->with, array('contact.emails'));
				$criteria->compare('emails.email', $this->contact->emails->email, true);
				$criteria->together = true;
			}

			if ($this->contact->phones->phone) {
				// Filter by a contact's phone number
				$criteria->with = CMap::mergeArray($criteria->with, array('contact.phones'));

				$scrubbedPhoneNumber = Yii::app()->format->formatInteger($this->contact->phones->phone);

				$criteria->compare('phones.phone', $scrubbedPhoneNumber, true);
				$criteria->together = true;
			}

            if($this->assignmentId) {
                $criteria->together = true;
                $criteria->with = CMap::mergeArray($criteria->with, array('assignments'));
                $criteria->compare('assignments.assignee_contact_id', $this->assignmentId);
            }

            if ($this->typesCollection) {
                $criteria->together = true;
                $criteria->with = CMap::mergeArray($criteria->with, array('recruitTypeLu'));
                $criteria->addInCondition('recruitTypeLu.recruit_type_id', $this->typesCollection);
            }

            if (!empty($this->excludeTypesCollection)) {
                // to prevent blanks
                if($excludeTypeIds = implode(',', $this->excludeTypesCollection)) {
                    $criteria->addCondition('(select count(rtlux.recruit_id) FROM recruit_type_lu rtlux WHERE rtlux.recruit_id=t.id AND rtlux.recruit_type_id IN('.$excludeTypeIds.')) < 1');
                }
            }

            if (!empty($this->actionPlanId)) {
                $criteria->addCondition("(SELECT count(*) FROM tasks tsk4 LEFT JOIN action_plan_applied_lu lu3 ON tsk4.id=lu3.task_id LEFT JOIN action_plan_items i3 ON i3.id=lu3.action_plan_item_id LEFT JOIN action_plans ap3 ON ap3.id=i3.action_plan_id WHERE ap3.id=".intval($this->actionPlanId)." AND tsk4.component_type_id=".ComponentTypes::RECRUITS." AND tsk4.component_id=t.id AND tsk4.complete_date IS NULL AND tsk4.is_deleted=0) > 0");
            }

            if ($this->fromLicenseDate) {
                $this->fromLicenseDate = Yii::app()->format->formatDate($this->fromLicenseDate, StmFormatter::MYSQL_DATE_FORMAT);
                $criteria->compare('licensed_date', '>='.$this->fromLicenseDate, true);
            }

            if ($this->toLicenseDate) {
                $this->toLicenseDate = Yii::app()->format->formatDate($this->toLicenseDate, StmFormatter::MYSQL_DATE_FORMAT);
                $criteria->compare('licensed_date', '<='.$this->toLicenseDate, true);
            }

            if($this->dateSearchFilter) {
                switch($this->dateSearchFilter) {
                    case 'status_asc':
                        $statusOrder = implode(',', array(TransactionStatus::HOT_A_LEAD_ID, TransactionStatus::B_LEAD_ID, TransactionStatus::C_LEAD_ID, TransactionStatus::D_NURTURING_SPOKE_TO_ID, TransactionStatus::NURTURING_ID));
                        $criteria->order= "FIELD(t.status_id, $statusOrder)";
                }
            }

			if($this->noTasks){
				$criteria->addCondition("(
					SELECT count(*) FROM tasks tsk2
					WHERE tsk2.component_type_id=".ComponentTypes::RECRUITS."
					AND tsk2.component_id=t.id
					AND tsk2.complete_date IS NULL
					AND tsk2.is_deleted=0) < 1","AND");
			}

			if($this->noActionPlan){
				$criteria
					->addCondition("(
						SELECT count(*) FROM tasks tsk
							LEFT JOIN action_plan_applied_lu lu ON tsk.id=lu.task_id
							LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id
							LEFT JOIN action_plans ap ON ap.id=i.action_plan_id
						WHERE tsk.component_type_id=".ComponentTypes::RECRUITS."
							AND tsk.component_id= t.id
							AND tsk.complete_date IS NULL
							AND tsk.is_deleted=0
						ORDER BY ap.name ASC
						) < 1","AND");
			}

			// Cap status == prodcution status == new custom table
			if(!empty($this->sortByCapStatus)) {
                $criteria->with = CMap::mergeArray($criteria->with, array('capStatus'));
                $criteria->order = 'capStatus.sort_order '.$this->sortByCapStatus;
            }

            // Rating == Recruits model column status_id (FK to transaction_status_id) - the "override" name comes from transaction_status_account_lu
            if(!empty($this->sortByRating)) {
                $criteria->with = CMap::mergeArray($criteria->with, array('status'));
                $criteria->order = 'status.sort_order '.$this->sortByRating;
            }

            if(!$criteria->order) {
                $criteria->order = 't.added DESC';
            }

			$params = array("criteria" => $criteria);
			if(empty($returnAll)){
				$params["pagination"] = array("pageSize"=> 50);
			}

			if(!empty($this->setByIds)) {
                $criteria->with = CMap::mergeArray($criteria->with, array('appointment'));
                $criteria->addInCondition('appointment.set_by_id',$this->setByIds);
            }

			$dataProvider = new CActiveDataProvider($this, $params);

            if($returnAll) {
//                $dataProvider->setPagination(false);
                $dataProvider->pagination = false;
            }
            return $dataProvider;
		}
	}
