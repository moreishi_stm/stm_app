<?php

	/**
	 * This is the model class for table "badge_point_types".
	 *
	 * The followings are the available columns in table 'badge_point_types':
	 *
	 * @property integer             $id
	 * @property string              $name
	 * @property string              $display_name
	 * @property integer             $updated_by
	 * @property string              $updated
	 * @property integer             $added_by
	 * @property string              $added
	 *
	 * The followings are the available model relations:
	 * @property Contacts            $addedBy
	 * @property Contacts            $updatedBy
	 * @property BadgePointsEarned[] $badgePointsEarneds
	 * @property BadgeRequirements[] $badgeRequirements
	 */
	class BadgePointTypes extends StmBaseActiveRecord {

		const THANK_YOU = 1;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return BadgePointTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'badge_point_types';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'name, display_name, updated_by, updated, added_by, added',
					'required'
				),
				array(
					'updated_by, added_by',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name, display_name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, display_name, updated_by, updated, added_by, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
				'badgePointsEarneds' => array(
					self::HAS_MANY,
					'BadgePointsEarned',
					'badge_point_type_id'
				),
				'badgeRequirements' => array(
					self::HAS_MANY,
					'BadgeRequirements',
					'badge_point_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
				'display_name' => 'Display Name',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'added_by' => 'Added By',
				'added' => 'Added',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('display_name', $this->display_name, true);
			$criteria->compare('updated_by', $this->updated_by);
			$criteria->compare('updated', $this->updated, true);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}