<?php

	/**
	 * This is the model class for table "account_contact_lu".
	 *
	 * The followings are the available columns in table 'account_contact_lu':
	 *
	 * @property integer  $id
	 * @property integer  $contact_id
	 * @property integer  $account_id
	 * @property integer  $contact_type_ma
	 * @property integer  $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts $contact
	 * @property Accounts $account
	 */
	class AccountContactLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AccountContactLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'account_contact_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_type_ma',
					'required'
				),
				array(
					'contact_id, account_id, contact_type_ma, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, account_id, contact_type_ma, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'account' => array(
					self::BELONGS_TO,
					'Accounts',
					'account_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'account_id' => 'Account',
				'contact_type_ma' => 'Contact Type Ma',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('contact_type_ma', $this->contact_type_ma);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
		
		public function byContactId($contactId = null) {

			if ($contactId) {
				$Criteria            = new CDbCriteria;
				$Criteria->condition = 'contact_id = :contactId';
				$Criteria->params    = array(':contactId' => $contactId);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}
	}