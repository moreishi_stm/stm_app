<?php

/**
 * This is the model class for table "call_block_calls".
 *
 * The followings are the available columns in table 'call_block_calls':
 * @property string $id
 * @property string $call_block_id
 * @property string $from_phone
 * @property integer $hq_telephone_phone_id
 * @property string $added
 *
 * The followings are the available model relations:
 * @property CallBlocks $callBlock
 * @property StmHq.telephonyPhones $hqTelephonePhone
 */
class CallBlockCalls extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallBlockCalls the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_block_calls';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_block_id, from_phone, hq_telephone_phone_id, added', 'required'),
            array('hq_telephone_phone_id', 'numerical', 'integerOnly'=>true),
            array('call_block_id', 'length', 'max'=>11),
            array('from_phone', 'length', 'max'=>15),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_block_id, from_phone, hq_telephone_phone_id, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callBlock' => array(self::BELONGS_TO, 'CallBlocks', 'call_block_id'),
            'hqTelephonePhone' => array(self::BELONGS_TO, 'StmHq.telephonyPhones', 'hq_telephone_phone_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_block_id' => 'Call Block',
            'from_phone' => 'From Phone',
            'hq_telephone_phone_id' => 'Hq Telephone Phone',
            'added' => 'Added',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('call_block_id',$this->call_block_id,true);
        $criteria->compare('from_phone',$this->from_phone,true);
        $criteria->compare('hq_telephone_phone_id',$this->hq_telephone_phone_id);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}