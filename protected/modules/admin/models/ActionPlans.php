<?php

	/**
	 * This is the model class for table "action_plans".
	 *
	 * The followings are the available columns in table 'action_plans':
	 *
	 * @property integer           $id
	 * @property integer           $account_id
	 * @property integer           $component_type_id
	 * @property string            $name
     * @property string            $operation_manuals
     * @property integer           $on_complete_plan_id
	 * @property integer           $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property ActionPlanItems[] $actionPlanItems
	 * @property Accounts          $account
	 */
	class ActionPlans extends StmBaseActiveRecord
    {
        public $applyAccountScope = false;
        public $componentId;

		const NEW_BUYER_PLAN_ID = 2;
		const NEW_SELLER_PLAN_ID = 4;

		public $applicabilityCheck = ''; //Placeholder for us to put errors in about whether or not this plan is able to be applied to a component.

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ActionPlans the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'action_plans';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, component_type_id, status_ma, name',
					'required'
				),
                array(
                    'id','required','on' => 'apply','message'=>'Please select an Action Plan.'
                ),
				array(
					'account_id, component_type_id, status_ma, is_deleted, componentId',
					'numerical',
					'integerOnly' => true
				),
                array('component_type_id','validateComponentTypeId'),
				array(
					'name',
					'length',
					'max' => 255
				),
				array(
					'operation_manuals, on_complete_plan_id, depends_on_id, componentId',
					'safe'
				),
				//This validation ensures that a given ActionPlan isn't currently applied to a given component
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, component_type_id, name, status_ma, operation_manuals, depends_on_id, is_deleted, componentId',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'actionPlanItems' => array(
					self::HAS_MANY,
					'ActionPlanItems',
					'action_plan_id',
					'order' => 'depends_on_id IS NULL DESC, due_days ASC'//
					//Ensure that parent plan items which are potentially parents come first.
				),
                'actionPlanItemParents' => array(
                    self::HAS_MANY,
                    'ActionPlanItems',
                    'action_plan_id',
                    'condition' => 'depends_on_id IS NULL',
                    'order' => 'due_days ASC'//
                    //Ensure that parent plan items which are potentially parents come first.
                ),
				'account' => array(
					self::BELONGS_TO,
					'Accounts',
					'account_id'
				),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
				'onCompleteActionPlan' => array(
					self::BELONGS_TO,
					'ActionPlans',
					'on_complete_plan_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'component_type_id' => 'Component Type',
				'name' => 'Name',
				'status_ma' => 'Status',
				'on_complete_plan_id' => 'On complete, start action plan',
				'is_deleted' => 'Is Deleted',
			);
		}

        public function scopes() {
            return array(
                'orderByName'=>array('order'=>'name ASC')
            );
        }

		/**
		 * Used to filter results by a account id
		 *
		 * @param $accountId
		 *
		 * @return ActionPlans
		 */
        public function byAccountId($accountId) {
            $this->getDbCriteria()->mergeWith(array(
                    'condition' => 'account_id = :account_id',
                    'params' => array(':account_id' => $accountId),
                )
            );

            return $this;
        }

        public function byStatus($status) {
            $this->getDbCriteria()->mergeWith(array(
                    'condition' => 'status_ma = :status_ma',
                    'params' => array(':status_ma' => $status),
                )
            );

            return $this;
        }

        /**
		 * Scope used to retrieve ActionPlans based on a given Component Type
		 *
		 * @param int   $componentTypeId   the type identifier for the components whose Action Plans are requested.
		 * @param array $omit              an optional collection of Action Plan IDs to Omit from our results.
		 *
		 * @return ActionPlans           a reference to "this" ActionPlans model instance.
		 */
		public function byComponentType($componentTypeId = null, $omit = array()) {
			if ($componentTypeId) {
				$Criteria = new CDbCriteria;
				$Criteria->condition = 'component_type_id = :component_type_id';
				$Criteria->params = array(':component_type_id' => $componentTypeId);

				if (!empty($omit)) {
					$Criteria->addCondition('id NOT IN (:applied_action_plan_ids)');
					$Criteria->params[':applied_action_plan_ids'] = join(',', $omit);
				}

				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}

		/**
		 * getAppliedPlans
		 * Loads activeDataProvider with conditions and returns ready for use (ex. CGridview)
		 *
		 * @param int $componentTypeId
		 * @param int $componentId
		 *
		 * @return mixed activeDataProvider
		 */
		public static function getAppliedPlans($componentTypeId, $componentId, $includeComplete=true) {
			$Criteria = new CDbCriteria;

			if (isset($componentTypeId) && isset($componentId)) {
				$Criteria->distinct = true;
				$Criteria->join = 'INNER JOIN action_plan_items api ON t.id = api.action_plan_id';
				$Criteria->join .= ' INNER JOIN action_plan_applied_lu apal ON api.id = apal.action_plan_item_id';
				$Criteria->join .= ' INNER JOIN tasks ta ON ta.id = apal.task_id';

				$Criteria->condition = 'ta.component_type_id = :component_type_id';

				$Criteria->addCondition('apal.is_deleted = 0');
				$Criteria->addCondition('ta.component_id = :component_id');
				$Criteria->addCondition('ta.is_deleted = 0');

				//$Criteria->group = 't.id';

				$Criteria->params = array(
					':component_type_id' => $componentTypeId,
					':component_id' => $componentId
				);

                if($includeComplete===false) {
                    $Criteria->addCondition('ta.complete_date IS NULL');
                }
			}
            $ActionPlan = new ActionPlans;
            $ActionPlan->skipSoftDeleteCheck();

			$dataProvider = new CActiveDataProvider($ActionPlan, array(
                'criteria'=>$Criteria,
			));

			return $dataProvider;
		}

		public function getIsCompleted($componentId)
        {
			return !(boolean) Tasks::model()->count(array(
					'condition' => 'component_id = :component_id AND complete_date IS NULL AND actionPlan.id=:id',
					'params' => array(':component_id' => $componentId, ':id'=>$this->id),
					'with' => array('actionPlanItem','actionPlanItem.actionPlan'),
				)
			);
		}

		/**
		 * Helper function for applying an Action Plan to the given component.
		 *
		 * @param $component mixed the component to which this Action Plan is to be applied.
		 *
		 * @return void
		 * @internal param \a $mixed valid task enabled component to which this Action Plan will apply itself.
		 */
		public function apply($component, $triggeringParentPlan=null, $triggerDescription=null)
        {
            $taskTypeId = TaskTypes::ACTION_PLAN;
            // prevent double application of the same action plan
            if(($isApplied = $this->isApplied($component))===true) {
                $this->addError('id','Action Plan is already applied.');
                Yii::log(__CLASS__.' ('.__LINE__.'): Attempting to apply an action plan that is already applied. Should not be possible. Unexpected result. Track this down to fix this bug. Action Plan Model attributes: ' . print_r($this->attributes, true), CLogger::LEVEL_ERROR);
                return;
            } elseif ($isApplied === null) {
                // isApplied returning null is a sign of error; Error has already been logged. Get out of this method.
                return;
            }

			if ($this->isApplicable($component)) {
				//TODO: It may be prudent to have some uniform interface for Action Plan Applicable components (that is components which could possibly have Action Plans applied against them).

				$planItemsApplied = array();

				foreach ($this->actionPlanItems as $ActionPlanItem) {
					$planItemApplied = in_array($ActionPlanItem->id, $planItemsApplied);

					if (!$planItemApplied) {
						$planItemsApplied = array_merge($planItemsApplied, $ActionPlanItem->createTask($component));
					}
				}

				$activityLogNote = "Action Plan " . $this->id . " - \"" . $this->name . "\" was applied to " . Yii::app()->format->formatSingular($component->componentType->display_name) . " " . $component->id . ". Please consult the application's logs for additional information.";

                if($triggeringParentPlan) {
                    $activityLogNote .= ' This action plan was triggered by the completion of action plan - "'.$triggeringParentPlan->name.'".';
                }

                if($triggerDescription) {
                    $activityLogNote .= ' '.$triggerDescription;
                    $taskTypeId = TaskTypes::TRIGGER;
                }

				ActivityLog::logActivity($component, $taskTypeId, $activityLogNote);
			}
            // if not pass isApplicable check and it is triggered by a parent plan. Notify user/owner
            elseif($triggeringParentPlan) {
                //@todo: notify owner.
            }
		}

        /**
         * Check to see if this action plan is already applied that is still in progress
         *
         * @param $component
         *
         * @return bool, null means error
         */
        public function isApplied($component) {
            if($this->isNewRecord) {
                Yii::log(__CLASS__.' ('.__LINE__.'): Attempting to apply an action plan from a empty model. Unexpected result. Track this down to fix this bug. Action Plan Model attributes: ' . print_r($this->attributes, true), CLogger::LEVEL_ERROR);
                return null; // null notate error
            }

            $count = Yii::app()->db->createCommand()
                ->select("count(distinct a.id)")
                ->from('tasks t')
                ->leftJoin('action_plan_applied_lu lu', 't.id=lu.task_id')
                ->leftJoin('action_plan_items api', 'api.id=lu.action_plan_item_id')
                ->leftJoin('action_plans a', 'a.id=api.action_plan_id')
                ->andWhere('t.component_type_id=:component_type_id', array(':component_type_id' => $component->componentType->id))
                ->andWhere('t.component_id=:component_id', array(':component_id' => $component->id))
                ->andWhere('a.id=:action_plan_id', array(':action_plan_id' => $this->id))
                ->andWhere('t.is_deleted=0 AND lu.is_deleted=0 AND t.complete_date IS NULL')
                ->queryScalar();

            return ($count==0)? false : true;
        }

        /**
		 * Utility for determining whether or not this {@code ActionPlans} instance is actually applicable.
		 * An ActionPlan is only applicable provided that each of its individual {@code ActionPlanItems} are applicable.
		 *
		 * @param $component mixed the component for which we want to know whether or not this action plan is applicable.
		 *
		 * @return bool true if this action plan is applicable to the given component, false otherwise.
		 */
		private function isApplicable($component) {
			$isApplicable = true;

			foreach ($this->actionPlanItems as $ActionPlanItem) {
				if (!$ActionPlanItem->isApplicable($this, $component)) {
					$isApplicable = false;
					//Developer's Note: Electing not to short circuit here so that all errors
					//                  are surfaced immediately rather than incrementally.
				}
			}

			return $isApplicable;
		}

		public function unapply($component, $triggerDescription=null)
        {
			if (isset($component)) {

                // get all the tasks for this action plan and component that is incomplete
                $results = Yii::app()->db->createCommand()
                    ->select('t.id task_id, lu.action_plan_item_id')
                    ->from('tasks t')
                    ->leftJoin('action_plan_applied_lu lu', 't.id=lu.task_id')
                    ->leftJoin('action_plan_items i', 'i.id=lu.action_plan_item_id')
                    ->leftJoin('action_plans p','p.id=i.action_plan_id')
                    ->where('p.id='.$this->id)
                    ->andWhere('t.component_type_id='.$this->component_type_id)
                    ->andWhere('t.component_id='.$component->id)
                    ->andWhere('t.complete_date IS NULL')
                    ->andWhere('t.is_deleted=0')
                    ->queryAll();

                $incompleteTaskIds = array();
                $actionPlanItemIds = array();

                foreach($results as $result) {
                    $incompleteTaskIds[] = $result['task_id'];
                    $actionPlanItemIds[] = $result['action_plan_item_id'];
                }

                $IncompleteTasks = array();
                // check for empty as it will lookup all
                if(!empty($incompleteTaskIds)) {
                    $IncompleteTasks = Tasks::model()->multiSelect($incompleteTaskIds)->notCompleted()->findAll();
                }

                // just double safety check, as this bug can ge a HUGE boo boo. May need to adjust this number as we see user actions.
                if(count($incompleteTaskIds) > Tasks::MAX_TASK_DELETE) {
                    // check to see if the number of incomplete is greater than actual action plan items
                    $itemsCount = count($this->actionPlanItems);
                    if(count($incompleteTaskIds) > $itemsCount) {
                        Yii::log(__CLASS__.' ('.__LINE__.') **** URGENT Error ****: '.count($incompleteTaskIds).' is exceeds the limit to unapply tasks AND the actual count for Action Plan Items for this Action Plan which is '.$itemsCount.'. This process was terminated. User did not get intended result. Please verify with user.', CLogger::LEVEL_ERROR, __CLASS__);
                        Yii::app()->plivo->sendMessage('9043433200', 'STM Action Plan Unapply Task Delete ERROR: '.count($incompleteTaskIds).' task delete attempted. See error log ASAP.');
                        return array('status'=>'error','message'=>'Action plan was not applied due to more tasks than action plan item. This is a safety measure. Please contact support.');
                    }
                }
                Tasks::$_lastMinuteDeletesActionPlanItemCount = count($this->actionPlanItems);

                // delete all incomplete tasks
				foreach ($IncompleteTasks as $Task) {
					try {
                        $Task->ignoreTriggerActionPlan = true;
						$Task->delete();
					} catch (Exception $e) {
						Yii::log(__CLASS__.' ('.__LINE__.') Encountered exception deleting the following Task: ' . print_r($Task->attributes, true) . ' - ' . $e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
					}
				}
                Tasks::$_lastMinuteDeletesActionPlanItemCount = 0;

                if(YII_IS_CONSOLE) {
                    echo 'Task delete count is: '.Tasks::$_lastMinuteDeletes."\n";
                }

                if(!empty($IncompleteTasks)) {
                    //Removing all action plan applied lookup records for this
                    //Action Plan so that it's possible to re-apply it in the future.
                    $LookupCriteria = new CDbCriteria();
                    $LookupCriteria->addInCondition('action_plan_item_id', $actionPlanItemIds);
                    $LookupCriteria->addInCondition('task_id', $incompleteTaskIds);

                    try {
                        // delete all action plan item lookups
                        ActionPlanAppliedLu::model()->updateAll(array('is_deleted'=>1), $LookupCriteria); //@todo: this ignores the beforeDelete (soft deletes)
                    } catch (Exception $e) {
                        Yii::log('Encountered exception deleting ActionPlanAppliedLu Records: ' . print_r($LookupCriteria->attributes, true) . ' - ' . $e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
                    }

                    $user = (YII_IS_CONSOLE) ? 'a System Process' : Yii::app()->user->contact->fullName;

                    $activityLogNote = "Action Plan " . $this->id . " - \"" . $this->name . "\" was unapplied by " . $user . " from  " . Yii::app()->format->formatSingular($component->componentType->display_name) . " " . $component->id . " with  ". count($IncompleteTasks) . " incomplete tasks deleted. Please consult the application's logs for additional information.";

                    if($triggerDescription) {
                        $activityLogNote .= ' '.$triggerDescription;
                    }

                    ActivityLog::logActivity($component, TaskTypes::ACTION_PLAN, $activityLogNote);
                }
                else {
                    Yii::log(__CLASS__.'(:'.__LINE__.') Attempted to unapply Action Plan #: '.$this->id.', Name: '.$this->name.',  but no incomplete tasks were found. This may be due to a double-click on the action plan unapply. Please verify by reviewing activity log for previously unapplied confirmation.', CLogger::LEVEL_ERROR, __CLASS__);
                }

			} else {
				Yii::log(__CLASS__.'(:'.__LINE__.') Invalid request. Attempted to unapply Action Plan #: '.$this->id.', Name: '.$this->name.', from a null component.', CLogger::LEVEL_ERROR, __CLASS__);
                return array('status'=>'error','message'=>'Component not found.');
			}

            return array('status'=>'success');
        }

        public function validateComponentTypeId($attribute, $params)
        {
            if($this->isNewRecord) {
                return;
            }

            $originalComponentTypeId = Yii::app()->db->createCommand("SELECT component_type_id from action_plans WHERE id=".$this->id)->queryScalar();

            // component type id has changed
            if ($originalComponentTypeId != $this->component_type_id) {
                $isActionPlanApplied = Yii::app()->db->createCommand('SELECT count(*) FROM action_plan_applied_lu lu JOIN action_plan_items i ON i.id=lu.action_plan_item_id WHERE i.action_plan_id='.$this->id)->queryScalar();
                $hasNonCompatibleItems = Yii::app()->db->createCommand()
                    ->select('count(*)')
                    ->from('action_plan_items')
                    ->orWhere(array('NOT IN','task_type_id', array(TaskTypes::TODO, TaskTypes::PHONE, TaskTypes::EMAIL_MANUAL, TaskTypes::TEXT_MESSAGE, TaskTypes::HANDWRITTEN_NOTE, TaskTypes::MAIL)))
                    ->orWhere(array('NOT IN','due_reference_ma', array(ActionPlanItems::DUE_REFERENCE_APPLIED_DATE_ID, ActionPlanItems::DUE_REFERENCE_UPON_COMPLETION_OF_TASK)))
                    ->orWhere(array('NOT IN','assign_to_type_ma', array(AssignmentTypes::CURRENT_USER, AssignmentTypes::SPECIFIC_USER)))
                    ->orWhere('email_template_id IS NOT NULL')
                    ->andWhere('action_plan_id='.$this->id)
                    ->queryScalar();

                if($isActionPlanApplied || $hasNonCompatibleItems) {

                    $this->addError('component_type_id', 'Attempting to change Component Type. Incompatible Action Plan Item exists or this Action Plan has already been applied to a Component.');
                }
            }
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('name', $this->name, true);
            $criteria->compare('operation_manuals', $this->operation_manuals, true);
			$criteria->compare('status_ma', $this->status_ma);
			$criteria->compare('is_deleted', $this->is_deleted);
            $criteria->order = 'name ASC';

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=>50,
                ),
            ));
		}
	}
