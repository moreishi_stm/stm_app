<?php

/**
 * This is the model class for table "featured_area_images".
 *
 * The followings are the available columns in table 'featured_area_images':
 * @property string $id
 * @property integer $featured_area_id
 * @property string $image_location
 * @property string $alt_tag
 * @property string $name_tag
 * @property integer $sort_order
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 * @property FeaturedAreas $featuredArea
 * @property Contacts $updatedBy
 */
class FeaturedAreaImages extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FeaturedAreaImages the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'featured_area_images';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('featured_area_id, image_location, sort_order, added_by, added, is_primary', 'required'),
            array('featured_area_id, sort_order, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('alt_tag, name_tag', 'length', 'max'=>100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, featured_area_id, image_location, alt_tag, name_tag, sort_order, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'featuredArea' => array(self::BELONGS_TO, 'FeaturedAreas', 'featured_area_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'featured_area_id' => 'Featured Area',
            'image_location' => 'File Location',
            'alt_tag' => 'Alt Tag',
            'name_tag' => 'Name Tag',
            'sort_order' => 'Sort Order',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('featured_area_id',$this->featured_area_id);
        $criteria->compare('image_location',$this->image_location,true);
        $criteria->compare('alt_tag',$this->alt_tag,true);
        $criteria->compare('name_tag',$this->name_tag,true);
        $criteria->compare('sort_order',$this->sort_order);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}