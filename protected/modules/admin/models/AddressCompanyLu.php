<?php

	/**
	 * This is the model class for table "address_company_lu".
	 *
	 * The followings are the available columns in table 'address_company_lu':
	 *
	 * @property integer   $id
	 * @property integer   $address_id
	 * @property integer   $company_id
	 * @property integer   $is_deleted
	 * @property integer   $is_primary
	 *
	 * The followings are the available model relations:
	 * @property Addresses $address
	 * @property Companies $company
	 */
	class AddressCompanyLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AddressCompanyLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'address_company_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'address_id, company_id, is_deleted, is_primary',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, address_id, company_id, is_deleted, is_primary',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'address' => array(
					self::BELONGS_TO,
					'Addresses',
					'address_id'
				),
				'company' => array(
					self::BELONGS_TO,
					'Companies',
					'company_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'address_id' => 'Address',
				'company_id' => 'Company',
				'is_deleted' => 'Is Deleted',
				'is_primary' => 'Is Primary',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('address_id', $this->address_id);
			$criteria->compare('company_id', $this->company_id);
			$criteria->compare('is_deleted', $this->is_deleted);
			$criteria->compare('is_primary', $this->is_primary);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}