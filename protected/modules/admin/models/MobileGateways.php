<?php

	/**
	 * This is the model class for table "mobile_gateways".
	 *
	 * The followings are the available columns in table 'mobile_gateways':
	 *
	 * @property integer                $id
	 * @property string                 $carrier_name
	 * @property string                 $sms_gateway
	 * @property string                 $mms_gateway
	 * @property integer                $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property MobileGatewayPhoneLu[] $mobileGatewayPhoneLus
	 */
	class MobileGateways extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return MobileGateways the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'mobile_gateways';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'carrier_name, sms_gateway, mms_gateway',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, carrier_name, sms_gateway, mms_gateway, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( // 'mobileGatewayPhoneLus' => array(self::HAS_MANY, 'MobileGatewayPhoneLu', 'mobile_gateway_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'carrier_name' => 'Carrier Name',
				'sms_gateway' => 'Sms Gateway',
				'mms_gateway' => 'Mms Gateway',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('carrier_name', $this->carrier_name, true);
			$criteria->compare('sms_gateway', $this->sms_gateway, true);
			$criteria->compare('mms_gateway', $this->mms_gateway, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}