<?php

/**
 * This is the model class for table "operation_manuals".
 *
 * The followings are the available columns in table 'operation_manuals':
 * @property integer $id
 * @property integer $status_ma
 * @property string $name
 * @property string $content
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property OperationManualTags[] $operationManualTags
 */
class OperationManuals extends StmBaseActiveRecord
{
	public $tagCollection = array();
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OperationManuals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'operation_manuals';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status_ma, name, content', 'required'),
			array('status_ma, is_deleted', 'numerical','integerOnly' => true),
			array('name', 'length', 'max'=>127),
			array('tagCollection', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, status_ma, name, content, is_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'operationManualTags' => array(self::MANY_MANY, 'OperationManualTags', 'operation_manual_tag_lu(operation_manual_id, operation_manual_tag_id)'),
			'operationManualTagLu' => array(self::HAS_MANY, 'OperationManualTagLu', 'operation_manual_id'),
		);
	}

    public function defaultScope() {
        return array(
            'order' => 'name ASC'
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'content' => 'Content',
			'tagCollection' => 'Tags',
		);
	}

	protected function beforeValidate() {

		return parent::beforeValidate();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('status_ma',$this->status_ma);

        if($this->name) {
            $criteria->addCondition('name like :name OR content like :content');
            $criteria->params = CMap::mergeArray($criteria->params , array( ':name' => "%".$this->name."%", ':content' => "%".$this->name."%"));
        }

        if($this->operationManualTagLu->idCollection) {
            $criteria->together = true;
            $criteria->with = array('operationManualTagLu');
            $criteria->addInCondition('operationManualTagLu.operation_manual_tag_id', $this->operationManualTagLu->idCollection);
        }

        $criteria->compare('is_deleted',$this->is_deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array('defaultOrder'=>'t.name ASC'),
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
	}
}