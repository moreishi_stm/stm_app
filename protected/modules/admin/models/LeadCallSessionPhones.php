<?php

/**
 * This is the model class for table "lead_call_session_phones".
 *
 * The followings are the available columns in table 'lead_call_session_phones':
 * @property string $id
 * @property string $lead_call_session_id
 * @property string $call_hunt_group_phone_id
 * @property integer $phone_id
 * @property string $phone_number
 * @property string $request_uuid
 * @property string $a_leg_uuid
 * @property integer $order
 * @property string $status
 * @property string $added
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property CallHuntGroupPhones $callHuntGroupPhone
 * @property LeadCallSessions $leadCallSession
 * @property Phones $phone
 */
class LeadCallSessionPhones extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeadCallSessionPhones the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lead_call_session_phones';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lead_call_session_id, phone_number, added', 'required'),
            array('phone_id, order', 'numerical', 'integerOnly'=>true),
            array('lead_call_session_id, call_hunt_group_phone_id', 'length', 'max'=>11),
            array('phone_number', 'length', 'max'=>32),
            array('request_uuid, a_leg_uuid', 'length', 'max'=>36),
            array('status', 'length', 'max'=>20),
            array('updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, lead_call_session_id, call_hunt_group_phone_id, phone_id, phone_number, request_uuid, a_leg_uuid, order, status, added, updated', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callHuntGroupPhone' => array(self::BELONGS_TO, 'CallHuntGroupPhones', 'call_hunt_group_phone_id'),
            'leadCallSession' => array(self::BELONGS_TO, 'LeadCallSessions', 'lead_call_session_id'),
            'phone' => array(self::BELONGS_TO, 'Phones', 'phone_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'lead_call_session_id' => 'Lead Call Session',
            'call_hunt_group_phone_id' => 'Call Hunt Group Phone',
            'phone_id' => 'Phone',
            'phone_number' => 'Phone Number',
            'request_uuid' => 'Request Uuid',
            'a_leg_uuid' => 'A Leg Uuid',
            'order' => 'Order',
            'status' => 'Status',
            'added' => 'Added',
            'updated' => 'Updated',
        );
    }

    protected function beforeSave()
    {
        $this->updated = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('lead_call_session_id',$this->lead_call_session_id,true);
        $criteria->compare('call_hunt_group_phone_id',$this->call_hunt_group_phone_id,true);
        $criteria->compare('phone_id',$this->phone_id);
        $criteria->compare('phone_number',$this->phone_number,true);
        $criteria->compare('request_uuid',$this->request_uuid,true);
        $criteria->compare('a_leg_uuid',$this->a_leg_uuid,true);
        $criteria->compare('order',$this->order);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Get Next Lead Call Session Phone
     *
     * Retrieves the next lead call session phone record in line for a lead call session (Useful for cascade)
     * @return mixed LeadCallSessionPhones or null
     */
    public static function getNextLeadCallSessionPhone($leadCallSessionId)
    {
        // Ensure this isn't empty
        if(!$leadCallSessionId) {
            throw new Exception('Error, lead call session ID can not be empty');
        }

        // Create criteria
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        $criteria->condition = 'lead_call_session_id = :lead_call_session_id AND status IS NULL';
        $criteria->params = array(
            ':lead_call_session_id' => $leadCallSessionId,
        );
        $criteria->order = '`order` ASC';
        $criteria->limit = 1;

        // Return next in line
        /** @var LeadCallSessionPhones $leadCallSessionPhones */
        return LeadCallSessionPhones::model()->find($criteria);
    }
}