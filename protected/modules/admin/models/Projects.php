<?php
Yii::import('admin_module.models.interfaces.Component');

/**
 * This is the model class for table "projects".
 *
 * The followings are the available columns in table 'projects':
 * @property integer $id
 * @property integer $account_id
 * @property integer $status_ma
 * @property integer $contact_id
 * @property string $name
 * @property integer $is_priority
 * @property integer $sort_order
 * @property string $due_date
 * @property string $updated
 * @property integer $updated_by
 * @property string $added
 * @property integer $added_by
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property ProjectItems[] $projectItems
 * @property Contacts $addedBy
 * @property Accounts $account
 * @property Contacts $contact
 * @property Contacts $updatedBy
 */
class Projects extends StmBaseActiveRecord implements Component
{
    private $_componentType;
    public $showOthersProjects;
    public $assignedTo;
    private $_nextTaskDate = -1;

    const STATUS_INACTIVE_ID=0;
    const STATUS_ACTIVE_ID=1;
    const STATUS_COMPLETE_ID=2;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Projects the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'projects';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status_ma, name, updated, updated_by, added, added_by', 'required'),
            array('parent_id', 'validateParentLoopCheck', 'on' => 'insert, update'),
            array('account_id, contact_id, status_ma, parent_id, is_priority, sort_order, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>127),
            array('due_date, notes', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, account_id, contact_id, status_ma, parent_id, name, is_priority, sort_order, due_date, notes, updated, updated_by, added, added_by, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'projectItems' => array(self::HAS_MANY, 'ProjectItems', 'project_id'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'parent' => array(self::BELONGS_TO, 'Projects', 'parent_id'),
        );
    }

    public function scopes() {
        return array(
            'byPriorityByName' => array(
                'order' => 'is_priority desc, name asc',
            ),
            'byName' => array(
                'order' => 'name asc',
            ),
            'byDueDateDesc' => array(
                'order' => 'due_date desc',
            ),
            'excludeSelf' => array(
                'condition' => 'id<>:id',
                'params' => array(':id'=>$this->id),
            ),
            'excludeDirectParent' => array(
                'condition' => 'parent_id<>:parent_id OR parent_id is NULL',
                'params' => array(':parent_id'=>$this->id),
            ),
        );
    }

    /**
     * @return array behaviors for getTasks
     */
    public function behaviors() {
        return array(
            'getTasks' => array(
                'class' => 'admin_module.components.behaviors.GetTasksBehavior',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'account_id' => 'Account',
            'status_ma' => 'Status',
            'contact_id' => 'Contact',
            'parent_id' => 'Subcategory of',
            'name' => 'Name',
            'is_priority' => 'Is Priority',
            'sort_order' => 'Sort Order',
            'due_date' => 'Due Date',
            'notes' => 'Notes',
            'updated' => 'Updated',
            'updated_by' => 'Updated By',
            'added' => 'Added',
            'added_by' => 'Added By',
            'is_deleted' => 'Is Deleted',
        );
    }

    public function getContactId() {
        return $this->contact_id;
    }

    public function getComponentType() {
        if (!isset($this->_componentType)) {
            $this->_componentType = ComponentTypes::model()->findByPk(ComponentTypes::PROJECTS);
        }

        return $this->_componentType;
    }

    /**
     * @return array collection of component tuple representations formatted as described in the asComponentTuple function.
     * @see asComponentTuple()
     */
    public function getComponentChain() {
        $chain = array($this->asComponentTuple());

        $includeCompleted = true;
        $Tasks = $this->getTasks($this, $includeCompleted);

        if (count($Tasks) > 0) {
            foreach ($Tasks as $Task) {
                array_push($chain, $Task->asComponentTuple());
            }
        }

        return $chain;
    }

    /**
     * Returns a representation of this component as an associative array containing its
     * componentTypeId and componentId keyed verbatim.
     *
     * @return array associative array representation of this component.
     */
    public function asComponentTuple() {
        return array(
            'componentTypeId' => $this->componentType->id,
            'componentId' => $this->id
        );
    }

    protected function beforeValidate() {

        if ($this->isNewRecord) {
            $this->added = date(StmFormatter::MYSQL_DATETIME_FORMAT);
            $this->added_by = Yii::app()->user->id;
        }

        $this->updated_by = Yii::app()->user->id;
        $this->updated = date('Y-m-d H:i:s');

        return parent::beforeValidate();
    }

    protected function beforeSave() {
        $this->due_date = date(StmFormatter::MYSQL_DATETIME_FORMAT, strtotime($this->due_date));

        return parent::beforeSave();
    }

    protected function afterFind() {
        $this->due_date = ($this->due_date < 1) ? null : Yii::app()->format->formatDate($this->due_date, StmFormatter::APP_DATE_PICKER_FORMAT);

        return parent::afterFind();
    }

    public function printPriorityFlag() {
        return ($this->is_priority) ? '<span class="priority-task-flag">!</span> ' : '';
    }

    public function byParentIds($ids) {
        $ids = StmFormatter::formatToArray($ids);
        $this->getDbCriteria()->addInCondition('parent_id',$ids);
        return $this;
    }

    public function byStatusIds($ids) {
        $ids = StmFormatter::formatToArray($ids);
        $this->getDbCriteria()->addInCondition('status_ma',$ids);
        return $this;
    }

    public function byIsParent() {
        $this->getDbCriteria()->addCondition('parent_id IS NOT NULL');
        return $this;
    }

    public function hasChild() {
        return $this->count(array('condition'=>'parent_id=:parent_id','params'=>array(':parent_id'=>$this->id)));
    }

    public function parentDropDownList() {
        $list = $this->excludeSelf()->excludeDirectParent()->byName()->findAll();
        return CHtml::listData($list,'id','name');
    }

    /**
     * Retrieves an array of model is up to the root parent project.
     * @return array of parent_id's for the model all the way to root project level
     */
    public function getParentIdsChains() {
        $chain = array();
        $model = $this;
        while($parent = $model->parent) {
            array_push($chain,$parent->id);
            $model = $parent;
        }
        return $chain;
    }

    public function validateParentLoopCheck($attribute, $params) {
        if ($this->parent_id && $this->id==Projects::model()->findByPk($this->parent_id)->parent_id) {
            $this->addError($attribute, 'Project cannot be a subcategory of each other simultaneously.');
        }
    }

    public function getStatusName() {
        $status = CMap::mergeArray(StmFormHelper::getStatusBooleanList(true), array(2=>'Complete'));
        return $status[$this->status_ma];
    }

    public function getAssignedToDropDownValues() {
        $contacts = Yii::app()->db->createCommand("select distinct c.id, CONCAT(c.first_name,' ', c.last_name) as fullName from projects p LEFT JOIN contacts c ON p.contact_id=c.id WHERE p.status_ma=1 ORDER BY c.first_name")->queryAll();
        if(!empty($contacts)) {
            foreach($contacts as $contact) {
                $contactList[$contact['id']] = $contact['fullName'];
            }
        }
        else {
            $contactList = array();
        }
        return $contactList;
    }

    public function getNextTaskDate() {
        if($this->_nextTaskDate !== -1) {
            return $this->_nextTaskDate;
        }

        $criteria = new CDbCriteria;
        $criteria->select = 'min(due_date) as due_date';
        $criteria->condition = 'component_type_id='.ComponentTypes::PROJECTS.' AND component_id='.$this->id.' AND complete_date IS NULL';
        $task = Tasks::model()->find($criteria);
        return $this->_nextTaskDate = $task->due_date;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('account_id',$this->account_id);
        $criteria->compare('status_ma',$this->status_ma);

        if($this->assignedTo) {
            $criteria->addInCondition('contact_id', $this->assignedTo);
        } else {
            $criteria->compare('contact_id', $this->contact_id);
        }

        $criteria->compare('parent_id',$this->parent_id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('is_priority',$this->is_priority);
        $criteria->compare('sort_order',$this->sort_order);
        $criteria->compare('due_date',$this->due_date,true);
        $criteria->compare('notes',$this->notes,true);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
    }
}