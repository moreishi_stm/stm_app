
<?php

/**
 * This is the model class for table "call_hunt_groups".
 *
 * The followings are the available columns in table 'call_hunt_groups':
 * @property integer $id
 * @property integer $voicemail_greeting_id
 * @property string $name
 * @property string $description
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property CallHuntGroupPhones[] $callHuntGroupPhones
 * @property CallHuntGroupSessions[] $callHuntGroupSessions
 * @property CallRecordings $voicemailGreeting
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 * @property IvrExtensions[] $ivrExtensions
 * @property Ivrs[] $ivrs
 * @property LeadRoutes[] $leadRoutes
 */
class CallHuntGroups extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallHuntGroups the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'call_hunt_groups';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, description, added_by, added', 'required'),
            array('voicemail_greeting_id, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>100),
            array('description', 'length', 'max'=>500),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, voicemail_greeting_id, name, description, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'callHuntGroupPhones' => array(self::HAS_MANY, 'CallHuntGroupPhones', 'call_hunt_group_id'),
            'callHuntGroupSessions' => array(self::HAS_MANY, 'CallHuntGroupSessions', 'call_hunt_group_id'),
            'voicemailGreeting' => array(self::BELONGS_TO, 'CallRecordings', 'voicemail_greeting_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'ivrExtensions' => array(self::HAS_MANY, 'IvrExtensions', 'call_hunt_group_id'),
            'ivrs' => array(self::HAS_MANY, 'Ivrs', 'call_hunt_group_id'),
            'leadRoutes' => array(self::HAS_MANY, 'LeadRoutes', 'call_hunt_group_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'voicemail_greeting_id' => 'Voicemail Greeting',
            'name' => 'Name',
            'description' => 'Description',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('voicemail_greeting_id',$this->voicemail_greeting_id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
    }

    /**
     * Get Hunt Group List
     *
     * Used to retrieve a select box for hunt groups with name as label and id as value
     * @param int $includeId
     * @return array Select box options
     */
    public static function getHuntGroupList($includeId=null)
    {
        $callHuntGroups = CallHuntGroups::model()->findAll(array('order' => 'name ASC'));

        if($includeId) {
            $includeSource = CallHuntGroups::model()->skipSoftDeleteCheck()->findByPk($includeId);
            if($includeSource->is_deleted) {
                array_push($callHuntGroups, $includeSource);
            }
        }

        $huntGroupList = CHtml::listData($callHuntGroups, 'id', function ($callHuntGroup) { // Anon function for returning the source parent's html content in the dropdown
            return $callHuntGroup->name;
        });

        return $huntGroupList;
    }
}