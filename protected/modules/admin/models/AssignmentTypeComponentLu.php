<?php

	/**
	 * This is the model class for table "assignment_type_account_lu".
	 *
	 * The followings are the available columns in table 'assignment_type_account_lu':
	 *
	 * @property integer         $component_type_id
	 * @property integer         $assignment_type_id
	 *
	 * The followings are the available model relations:
	 * @property AssignmentTypes $assignmentType
	 * @property ComponentTypes  $componentType
	 */
	class AssignmentTypeComponentLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AssignmentTypeComponentLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'assignment_type_component_lu';
		}

		public function primaryKey() {
			// For composite primary key, return an array like the following
			return array(
				'component_type_id',
				'assignment_type_id'
			);
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'component_type_id, assignment_type_id',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'component_type_id, assignment_type_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'assignmentType' => array(
					self::BELONGS_TO,
					'AssignmentTypes',
					'assignment_type_id'
				),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'component_type_id' => 'Component Type',
				'assignment_type_id' => 'Assignment Type',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('assignment_type_id', $this->assignment_type_id);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
