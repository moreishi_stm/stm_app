<?php

	/**
	 * This is the model class for table "email_links".
	 *
	 * The followings are the available columns in table 'email_links':
	 *
	 * @property integer $id
	 * @property string  $url
	 * @property string  $added
	 */
	class EmailLink extends StmBaseActiveRecord {

		const ID_ENCRYPTION_KEY = '505645f6639c4d1a8f16a87866935b60917610fb';

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return EmailLinks the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'email_links';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'url',
					'required'
				),
				array(
					'url',
					'length',
					'max' => 255
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, url, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array();
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'url' => 'Url',
				'added' => 'Added',
			);
		}

		/**
		 * Filter by url
		 *
		 * @param $url
		 *
		 * @return $this
		 */
		public function byUrl($url) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'url = :url',
					'params' => array(
						':url' => $url,
					),
				)
			);

			return $this;
		}

		/**
		 * Add the added column value on new records
		 *
		 * @return bool
		 */
		protected function beforeSave() {
			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');
			}

			return parent::beforeSave();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('url', $this->url, true);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

        /**
         * Encode Link
         *
         * Encodes an array of data in to a link
         * @param $data array Data to encode
         * @return string Encoded data
         * @throws Exception When an invalid argument has been passed
         */
        public static function encodeLink($data)
        {
            // Make sure we have a proper data structure to encode
            if(!is_array($data)) {
                throw new Exception('Data must be an array');
            }

            return urlencode(base64_encode(Yii::app()->getSecurityManager()->encrypt(json_encode($data), self::ID_ENCRYPTION_KEY)));
        }

        /**
         * Decode Link
         *
         * @param $data string Data to decode
         * @return array Decoded data
         * @throws Exception When no data has been passed
         */
        public static function decodeLink($data)
        {
            // Make sure we have data to decode
            if(empty($data)) {
                throw new Exception('Invalid argument for data');
            }
            $dataArray = explode('?utm_source=',$data);
            $jsonString = Yii::app()->getSecurityManager()->decrypt(base64_decode($dataArray[0]), self::ID_ENCRYPTION_KEY);

            //there are some weird characters at the end so going to cut it off
            $cleanJsonString = substr($jsonString,0, strpos($jsonString,'}')+1);
            return (array) json_decode($cleanJsonString);

//            return json_decode(Yii::app()->getSecurityManager()->decrypt(base64_decode($cleanJsonString), self::ID_ENCRYPTION_KEY), true);
        }
	}