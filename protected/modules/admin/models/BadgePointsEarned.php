<?php

	/**
	 * This is the model class for table "badge_points_earned".
	 *
	 * The followings are the available columns in table 'badge_points_earned':
	 *
	 * @property integer         $id
	 * @property integer         $contact_id
	 * @property integer         $badge_point_type_id
	 * @property integer         $points_earned
	 * @property integer         $updated_by
	 * @property string          $updated
	 * @property integer         $added_by
	 * @property string          $added
	 *
	 * The followings are the available model relations:
	 * @property Contacts        $addedBy
	 * @property Contacts        $contact
	 * @property BadgePointTypes $badgePointType
	 * @property Contacts        $updatedBy
	 */
	class BadgePointsEarned extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return BadgePointsEarned the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'badge_points_earned';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'note',
					'required',
					'on' => 'thankyou'
				),
				array(
					'contact_id, badge_point_type_id, points_earned, updated_by, updated, added_by, added',
					'required'
				),
				array(
					'contact_id, badge_point_type_id, points_earned, updated_by, added_by',
					'numerical',
					'integerOnly' => true
				),
				array(
					'note',
					'length',
					'max' => 750
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, badge_point_type_id, points_earned, updated_by, updated, added_by, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'badgePointType' => array(
					self::BELONGS_TO,
					'BadgePointTypes',
					'badge_point_type_id'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'badge_point_type_id' => 'Badge Point Type',
				'points_earned' => 'Points Earned',
				'note' => 'Note',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
				'added_by' => 'Added By',
				'added' => 'Date',
			);
		}

		public function byDateRange($opts, $field) {

			if ($opts['from_date']) {
				$this->getDbCriteria()->mergeWith(array(
												  'condition' => $field.' >= :from_date',
												  'params' => array(':from_date' => $opts['from_date']),
												  )
				);
			}

			if ($opts['to_date']) {
				$this->getDbCriteria()->mergeWith(array(
												'condition' => $field.' <= :to_date',
												'params' => array(':to_date' => $opts['to_date']),
												)
				);
			}

			return $this;
		}

		public function byAwarder($contactId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'added_by=:added_by';
			$Criteria->params = array(':added_by' => $contactId);

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function byAwardee($contactId) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		protected function beforeValidate() {

			if ($this->isNewRecord) {
				$this->added_by = Yii::app()->user->id;
				$this->added = date('Y-m-d H:i:s');
				$this->updated_by = Yii::app()->user->id;
				$this->updated = Yii::app()->user->id;
			}

			return parent::beforeValidate();
		}

		protected function beforeSave() {

			$this->updated_by = Yii::app()->user->id;
			$this->updated = date('Y-m-d H:i:s');

			return parent::beforeSave();
		}

		protected function afterSave() {
			if ($this->isNewRecord) {
                $BadgeEarned = BadgeAwarded::isBadgeAwardedWithNewPoint($this);
                if($toEmail = Yii::app()->user->settings->email_all) {
                    if ($BadgeEarned) {
                        $message = new YiiMailMessage;

                        switch ($BadgeEarned->id) {
                            case Badges::THANK_YOU:
                                $content =
                                    '<span style="font-weight:bold;font-size:16px;">Congrats!<br /><br />' . '<img src="' . Yii::app()->controller->getModule()->getCdnImageAssetsUrl().'badges/' . $BadgeEarned->image_filename
                                        . '" alt="' . $BadgeEarned->name . ' Badge"> ' . $this->contact->fullName . ' has been awarded a "' . $BadgeEarned->name . '" Badge!</span>';
                                if ($this->note) {
                                    $contentBadgeSpecific =
                                        '<br /><br /><span style="text-decoration: underline;">' . $this->addedBy->first_name . ' commented:</span> <span style="font-size: 14px;">' . nl2br($this->note) . '</span>';
                                }
                                break;
                            default:
                                $content = '<span style="font-weight:bold;font-size:16px;">Congrats!<br />' . $this->contact->fullName . ' earned a ' . $BadgeEarned->name . ' Badge!<span>';
                                break;
                        }

                        $contentSentBy = '<br /><br /><span style="font-weight:normal;font-size:13px;">Sent by ' . $this->addedBy->fullName . '</span>';
                        $logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Display Team Logo">';
                        $content = $content . $contentSentBy . $contentBadgeSpecific . $logoImage;
                        $message->view = 'plain';
                        $message->setSubject($this->contact->fullName . ' is awarded a ' . $BadgeEarned->name . ' Badge!');
                        $message->setBody(array(
                                          'toEmail' => $toEmail,
                                          'content' => $content
                                          ), 'text/html'
                        );
                        if (YII_DEBUG) {
                            $message->addTo(StmMailMessage::DEBUG_EMAIL_ADDRESS, 'Team Congrats');
                        } else {
                            $message->addTo($toEmail, 'Team Congrats');
                        }

                        $message->addFrom(Yii::app()->user->contact->getPrimaryEmail(), Yii::app()->user->getFullName());
//                        Yii::app()->mail->send($message);
                        StmZendMail::sendYiiMailMessage($message);
                    }
                }
            }

			parent::afterSave();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('badge_point_type_id', $this->badge_point_type_id);
			$criteria->compare('points_earned', $this->points_earned);
			$criteria->compare('note', $this->note, true);
			$criteria->compare('updated_by', $this->updated_by);
			$criteria->compare('updated', $this->updated, true);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}