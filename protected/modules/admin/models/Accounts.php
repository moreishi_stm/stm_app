<?php

/**
 * This is the model class for table "accounts".
 *
 * The followings are the available columns in table 'accounts':
 *
 * @property integer                       $id
 * @property integer                       $mls_board_id
 * @property integer                       $account_status_ma
 * @property string                        $timezone
 * @property string                        $agent_board_num
 * @property string                        $feed_username
 * @property string                        $feed_password
 * @property string                        $contract_date
 * @property string                        $go_live_date
 * @property string                        $notes
 * @property integer                       $is_deleted
 *
 * The followings are the available model relations:
 * @property AccountContactLu[]            $accountContactLus
 * @property ActionPlans[]                 $actionPlans
 * @property ActivityLog[]                 $activityLogs
 * @property AssignmentTypes[]             $assignmentTypes
 * @property Companies[]                   $companies
 * @property ContactAttributeTypes[]       $contactAttributeTypes
 * @property ContactTypes[]                $contactTypes
 * @property Contacts[]                    $contacts
 * @property CraigslistTemplateGroups[]    $craigslistTemplateGroups
 * @property CraigslistTemplates[]         $craigslistTemplates
 * @property Documents[]                   $documents
 * @property Domains[]                     $domains
 * @property FeaturedAreaTypes[]           $featuredAreaTypes
 * @property FeaturedAreas[]               $featuredAreases
 * @property Forms[]                       $forms
 * @property HistoryLog[]                  $historyLogs
 * @property LeadDistributionGroup[]       $leadDistributionGroups
 * @property LeadDistributionRules[]       $leadDistributionRules
 * @property Permissions[]                 $permissions
 * @property Phones[]                      $phones
 * @property Projects[]                    $projects
 * @property Settings[]                    $settings
 * @property Sources[]                     $sources
 * @property TaskTypes[]                   $taskTypes
 * @property Tasks[]                       $tasks
 * @property TrackingMarketFlow[]          $trackingMarketFlows
 * @property TransactionClosingCostItems[] $transactionClosingCostItems
 * @property TransactionStatus[]           $transactionStatuses
 * @property UserGroups[]                  $userGroups
 */
class Accounts extends StmBaseActiveRecord
{

    const ID_ACCOUNT_CONTACT_TYPE_PRIMARY = 1;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Accounts the static model class
     */
    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {

        return 'accounts';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'mls_board_id, account_status_ma, is_deleted',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'craigslist_area, craigslist_category',
                'length',
                'max' => 15
            ),
            array(
                'agent_board_num, feed_username, feed_password',
                'length',
                'max' => 50
            ),
            array(
                'api_key',
                'length',
                'max' => 63
            ),
            array(
                'notes',
                'length',
                'max' => 500
            ),
            array(
                'timezone',
                'length',
                'max' => 100
            ),
            array(
                'contract_date, go_live_date',
                'safe'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, mls_board_id, account_status_ma, agent_board_num, feed_username, feed_password, contract_date, go_live_date, api_key, notes, craigslist_area, craigslist_category, is_deleted',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'accountContacts'             => array(
                self::MANY_MANY,
                'Contacts',
                'account_contact_lu(account_id, contact_id)',
                'alias' => 't'
            ),
            // This may be deprecated @todo: 2/22/14 - CLee, checked code base... seems safe to remove?
            'mlsBoard'                    => array(
                self::HAS_ONE,
                'MlsBoards',
                'id'
            ),
            'actionPlans'                 => array(
                self::HAS_MANY,
                'ActionPlans',
                'id'
            ),
            'activityLogs'                => array(
                self::HAS_MANY,
                'ActivityLog',
                'id'
            ),
            'assignmentTypes'             => array(
                self::HAS_MANY,
                'AssignmentTypes',
                'id'
            ),
            'companies'                   => array(
                self::HAS_MANY,
                'Companies',
                'id'
            ),
            'contactAttributeTypes'       => array(
                self::HAS_MANY,
                'ContactAttributeTypes',
                'id'
            ),
            'contactTypes'                => array(
                self::HAS_MANY,
                'ContactTypes',
                'id'
            ),
            'contacts'                    => array(
                self::HAS_MANY,
                'Contacts',
                'id'
            ),
            'craigslistTemplateGroups'    => array(
                self::HAS_MANY,
                'CraigslistTemplateGroups',
                'id'
            ),
            'craigslistTemplates'         => array(
                self::HAS_MANY,
                'CraigslistTemplates',
                'id'
            ),
            'documents'                   => array(
                self::HAS_MANY,
                'Documents',
                'account_id'
            ),
            'domains'                     => array(
                self::HAS_MANY,
                'Domains',
                'account_id'
            ),
            'featuredAreaTypes'           => array(
                self::HAS_MANY,
                'FeaturedAreaTypes',
                'id'
            ),
            'featuredAreases'             => array(
                self::HAS_MANY,
                'FeaturedAreas',
                'id'
            ),
            'forms'                       => array(
                self::HAS_MANY,
                'Forms',
                'id'
            ),
            'historyLogs'                 => array(
                self::HAS_MANY,
                'HistoryLog',
                'id'
            ),
            'leadDistributionGroups'      => array(
                self::HAS_MANY,
                'LeadDistributionGroup',
                'id'
            ),
            'leadDistributionRules'       => array(
                self::HAS_MANY,
                'LeadDistributionRules',
                'id'
            ),
            'permissions'                 => array(
                self::HAS_MANY,
                'Permissions',
                'id'
            ),
            'phones'                      => array(
                self::HAS_MANY,
                'Phones',
                'id'
            ),
            'projects'                    => array(
                self::HAS_MANY,
                'Projects',
                'id'
            ),
            'settings'                    => array(
                self::HAS_MANY,
                'Settings',
                'id'
            ),
            'sources'                     => array(
                self::HAS_MANY,
                'Sources',
                'id'
            ),
            'taskTypes'                   => array(
                self::HAS_MANY,
                'TaskTypes',
                'id'
            ),
            'tasks'                       => array(
                self::HAS_MANY,
                'Tasks',
                'id'
            ),
            'trackingMarketFlows'         => array(
                self::HAS_MANY,
                'TrackingMarketFlow',
                'id'
            ),
            'transactionClosingCostItems' => array(
                self::HAS_MANY,
                'TransactionClosingCostItems',
                'id'
            ),
            'transactionStatuses'         => array(
                self::HAS_MANY,
                'TransactionStatus',
                'id'
            ),
            'userGroups'                  => array(
                self::HAS_MANY,
                'UserGroups',
                'id'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'id'                => 'Id',
            'mls_board_id'      => 'Mls Board',
            'account_status_ma' => 'Status',
            'timezone'          => 'Timezone',
            'agent_board_num'   => 'Agent Board #',
            'feed_username'     => 'Feed Username',
            'feed_password'     => 'Feed Password',
            'contract_date'     => 'Contract Date',
            'go_live_date'      => 'Go Live Date',
            'notes'             => 'Notes',
            'api_key'           => 'API Key',
            'craigslist_area'   => 'Craigslist Area',
            'craigslist_category' => 'Craigslist Category',
            'is_deleted'        => 'Is Deleted',
        );
    }

    /**
     * Retrieves the board information based on the hq board
     * This is not utilizing the api as the model was loaded faster in the speed tests
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return CActiveRecord
     */
    public function getBoard()
    {

        Yii::import('seizethemarket.protected.modules.hq.models.MlsBoards');
        $board = MlsBoards::model()->findByPk($this->mls_board_id);

        return $board;
    }

    /**
     * associatedWithDomain
     * Returns a boolean if a specified domain is associated with an account
     *
     * @param string $domain
     *
     * @return boolean
     */
    public function associatedWithDomain($domain = null)
    {

        if (!$domain) {
            $domain = Domains::getActiveDomain();
            if ($domain) {
                return $domain;
            }
        }

        $Domains = $this->domains(
            array(
                'condition' => 'name=:name',
                'params'    => array(
                    ':name' => Yii::app()->format->formatDomain($domain),
                )
            )
        );

        return ($Domains) ? true : false;
    }

    /**
     * getPrimaryContact gets 1 primary contact for an account
     *
     * @return Contacts $contact
     */
    public function getPrimaryContact()
    {

        $Criteria       = new CDbCriteria;
        $Criteria->with = array('accountContactLu');
        $Criteria->condition
                          = 'accountContactLu.account_id=:account_id AND accountContactLu.contact_type_ma=:contact_type';
        $Criteria->params = array(
            ':account_id'   => $this->id,
            ':contact_type' => self::ID_ACCOUNT_CONTACT_TYPE_PRIMARY
        );

        $Contact                    = Contacts::model();
        $Contact->applyAccountScope = false;
        $Contact                    = $Contact->find($Criteria);

        return $Contact;
    }

    /**
     * ApiAuth Takes a key from an API call and returns accountId when found
     *
     * @param  string $key
     *
     * @return int account_id
     */
    public function ApiAuth($key)
    {

        /******** DO WE WANT TO VERIFY IT WITH DOMAIN ALSO???? So someone with an API key and not a matching domain can't use the API ... essentially authorized domains only ******************/
        $Criteria            = new CDbCriteria;
        $Criteria->select    = 'id,mls_board_id';
        $Criteria->condition = 'api_key=:api_key';
        $Criteria->params    = array(':api_key' => $key);

        return array(
            'accountId'  => $this->find($Criteria)->id,
            'mlsBoardId' => $this->find($Criteria)->mls_board_id
        );
    }

    // public function getStatus() {
    // 	switch ($this->account_status_ma) {
    // 		case ID_ACCOUNT_STATUS_ACTIVE:
    // 			# code...
    // 			break;

    // 		case ID_ACCOUNT_STATUS_INACTIVE:
    // 			# code...
    // 			break;
    // 	}
    // 	return $status;
    // }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id', '>1');
        $criteria->compare('mls_board_id', $this->mls_board_id);
        $criteria->compare('account_status_ma', $this->account_status_ma);
        $criteria->compare('agent_board_num', $this->agent_board_num, true);
        $criteria->compare('feed_username', $this->feed_username, true);
        $criteria->compare('feed_password', $this->feed_password, true);
        $criteria->compare('contract_date', $this->contract_date, true);
        $criteria->compare('go_live_date', $this->go_live_date, true);
        $criteria->compare('notes', $this->notes, true);
        $criteria->compare('api_key', $this->api_key, true);
        $criteria->compare('craigslist_area', $this->craigslist_area, true);
        $criteria->compare('craigslist_category', $this->craigslist_category, true);
        $criteria->compare('is_deleted', $this->is_deleted);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}