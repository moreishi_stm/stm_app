<?php

	/**
	 * This is the model class for table "page_views".
	 *
	 * The followings are the available columns in table 'page_views':
	 *
	 * @property integer         $id
	 * @property integer         $contact_id
	 * @property integer         $page_type_ma
     * @property integer         $mls_board_id
     * @property integer         $listing_id
	 * @property string          $url
	 * @property integer         $session_id
	 * @property string          $added
	 *
	 * The followings are the available model relations:
	 * @property PageViewMlsLu[] $pageViewMlsLus
	 * @property Contacts        $contact
	 */
	class ViewedPages extends StmBaseActiveRecord {

		const HOME_PAGES = 1;
		const NON_HOME_PAGES = 2;

		public $averagePriceViewed;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ViewedPages the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'viewed_pages';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'contact_id, page_type_ma',
					'numerical',
					'integerOnly' => true
				),
				array(
					'listing_id',
					'length',
					'max' => 50
				),
                array(
                    'ip',
                    'length',
                    'max' => 100
                ),
                array(
					'url',
					'length',
					'max' => 127
				),
				array(
					'added',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, page_type_ma, mls_board_id, listing_id, url, ip, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'page_type_ma' => 'View Type Ma',
				'listing_id' => 'Listing ID',
				'url' => 'Url',
				'ip' => 'IP',
				'added' => 'Date',
			);
		}

		/**
		 * getAll
		 * Loads activeDataProvider with conditions and returns ready for use (ex. CGridview)
		 *
		 * @param int $contactId
		 * @param int $pageType indicated page or home view
		 *
		 * @return mixed activeDataProvider
		 */
		public static function getAll($contactId, $pageType = self::HOME_PAGES) {

//			$viewedPage = ViewedPages::model()->byContactId($contactId)->byPageType($pageType);
			$dataProvider = new CActiveDataProvider('ViewedPages', array(
				'criteria' => array(
					'condition' => 'listing_id IS NOT NULL AND contact_id=:contact_id AND page_type_ma=:page_type_ma',
                    'params' => array(':contact_id' =>$contactId, ':page_type_ma' => $pageType),
					'order' => 'added desc',
					'limit' => 30,
				),
				'pagination' => array(
					'pageSize' => 100,
				),
			));

			return $dataProvider;
		}

		public static function getListingIds() {
			$model = ViewedPages::model()->findAll(array(
					'select' => 'listing_id',
					'condition' => 'contact_id=:contact_id AND page_type_ma=:page_type_ma',
					'params' => array(
						':contact_id' => Yii::app()->user->id,
						':page_type_ma' => ViewedPages::HOME_PAGES
					),
					'order' => 'added DESC',
					'limit' => 50,
				)
			);
			$listingIds = array();
			foreach ($model as $home) {
				array_push($listingIds, $home->listing_id);
			}

			return $listingIds;
		}

		/**
		 * recentlyViewedHomes
		 *
		 * @param  array $opt options of settings
		 *
		 * @return mixed array of model for the results
		 */
		public function recentlyViewedHomes($opt) {

			$contactId = (!$opt['contactId']) ? Yii::app()->user->id : $opt['contactId'];
			$limit = (!$opt['limit']) ? 20 : $opt['limit'];

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact AND page_type_ma=:page_type_ma';
			$Criteria->params = array(
				':contact_id' => $contactId,
				':page_type_ma' => 1
			);
            $Criteria->order = 'id DESC';
			$Criteria->limit = $limit;

			return $this->findAll($Criteria);
		}

        /**
         * @param     $contactId
         * @param int $limit
         *
         * @return mixed
         */
        public function getViewedHomesData($contactId, $limit=30)
        {
            $mlsTable = Yii::app()->user->board->prefixedName.'_properties';
            $linkState = strtolower(AddressStates::getShortNameById(Yii::app()->user->board->state_id));

            $result = Yii::app()->db->createCommand()
                ->select('v.added, m.listing_id, m.price, m.sq_feet, m.bedrooms, m.baths_full, m.common_subdivision, CONCAT(IFNULL(m.street_number,"")," ", IFNULL(m.street_direction_prefix,""), " ", IFNULL(m.street_name,""), " ", IFNULL(m.street_suffix,""), " ", IFNULL(m.street_direction_suffix,""), " ", IFNULL(m.unit_number,"")) streetAddress, m.city, "'.$linkState.'" as state, SUBSTRING(m.zip,1,5) zip')
                ->from('viewed_pages v')
                ->leftJoin('stm_mls.'.$mlsTable.' m', 'v.listing_id=m.listing_id')
                ->where("v.contact_id=".$contactId)
                ->andWhere("v.listing_id IS NOT NULL")
                ->andWhere("v.page_type_ma=".ViewedPages::HOME_PAGES)
                ->order('added desc')
                ->limit($limit)
                ->queryAll();
			foreach ($result as $i => $r){
				$result[$i]["listing_view_count"] = $this->clientViewdListingCount($contactId,$r["listing_id"]);
			}
			return $result;
        }

		protected function clientViewdListingCount($contactId,$listingId) {
			$model = new ViewedPages;

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id = :contact_id AND :listing_id = :listing_id';
			$Criteria->params = array(
				':contact_id' => $contactId,
				':listing_id' => $listingId
			);

			return count($model->findAllByAttributes(array("contact_id"=>$contactId,"listing_id"=>$listingId)));
		}

        /**
		 * getCount
		 * Returns the # of Homes Viewed for a contact. Used for diplayed number in JUI Tab
		 *
		 * @param int     $contactId
		 * @param int     $pageType  1=homes 2=non-home pages
		 * @param boolean $tabFormat true returns in "(10)" to be used in tabs
		 *
		 * @return int $count
		 */
		public static function getCount($contactId, $pageType = 1, $tabFormat = false) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id AND page_type_ma=:page_type_ma';
			$Criteria->params = array(
				':contact_id' => $contactId,
				':page_type_ma' => $pageType
			);
			$count = ViewedPages::model()->count($Criteria);
			if ($tabFormat) {
				$count = '(' . $count . ')';
			}

			return $count;
		}

		protected function beforeSave() {
			if ($this->isDuplicate) {
				return false;
			}

			return parent::beforeSave();
		}

		protected function getIsDuplicate() {
			$now = new DateTime(date('Y-m-d H:i:s'));
			$time_limit = $now->sub(DateInterval::createFromDateString(PageViewTrackerWidget::DUPLICATE_TIME_LIMIT));

			$model = new ViewedPages;

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id AND added > :added';
			$Criteria->params = array(
				':contact_id' => Yii::app()->user->id,
				':added' => $time_limit->date
			);

			$results = $model->findAll($Criteria);
			if ($results) {
				foreach ($results as $result) {
					if ($this->url == $result->url || $this->listing_id == $result->listing_id) {
						return true;
					}
				}
			}

			return false;
		}

		static function countHomesViewed() {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id AND listing_id is not null';
			$Criteria->params = array(':contact_id' => Yii::app()->user->id);

			return count(ViewedPages::model()->findAll($Criteria));
		}

		public function getProperty() {
			$tableName = Yii::app()->user->board->prefixedName;
			$model = MlsProperties::model($tableName);

			return $model->findByPk($this->listing_id);
		}

		public function byContactId($contactId) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => $contactId),
				)
			);

			return $this;
		}

		public function byPageType($pageType) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'page_type_ma=:page_type_ma',
					'params' => array(':page_type_ma' => $pageType),
				)
			);

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			// $criteria->distinct = '';
			$criteria->limit = 25;
			$criteria->select = 'distinct(listing_id)';
			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('page_type_ma', $this->page_type_ma);
			$criteria->compare('listing_id', $this->listing_id, true);
			$criteria->compare('url', $this->url, true);
			$criteria->compare('ip', $this->ip);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
				'sort' => array(
					'defaultOrder' => 'added DESC',
				),
			));
		}
	}
