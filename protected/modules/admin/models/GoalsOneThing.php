<?php

/**
 * This is the model class for table "goals_one_thing".
 *
 * The followings are the available columns in table 'goal_one_thing':
 * @property integer $id
 * @property integer $contact_id
 * @property string $date
 * @property string $goal_description
 * @property string $results_description
 * @property string $challenges
 * @property string $tomorrow_must_do
 * @property integer $percent_daily_goal_accomplished
 * @property integer $percent_monthly_goal_accomplished
 * @property integer $percent_weekly_goal_accomplished
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Contacts $contact
 * @property Contacts $updatedBy
 */
class GoalsOneThing extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GoalsOneThing the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'goals_one_thing';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date, goal_description, results_description, tomorrow_must_do, percent_daily_goal_accomplished, percent_weekly_goal_accomplished, percent_monthly_goal_accomplished', 'required'),
            array('contact_id, percent_daily_goal_accomplished, percent_weekly_goal_accomplished, percent_monthly_goal_accomplished, updated_by, added_by, is_deleted', 'numerical', 'integerOnly'=>true),
            array('date, results_description, challenges, tomorrow_must_do', 'safe'),

            array('checkDuplicate', 'checkDuplicate'),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, contact_id, date, goal_description, results_description, challenges, tomorrow_must_do, percent_daily_goal_accomplished, percent_weekly_goal_accomplished, percent_monthly_goal_accomplished, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'contact_id' => 'Contact',
            'date' => 'Date',
            'goal_description' => 'Today\'s Goal / One Thing',
            'results_description' => 'Today\'s Results & Accomplishments',
            'percent_daily_goal_accomplished' => 'Today\'s Goal Accomplished %',
            'percent_weekly_goal_accomplished' => 'Week\'s Goal Accomplished %',
            'percent_monthly_goal_accomplished' => 'Month\'s Goal Accomplished %',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
            'is_deleted' => 'Is Deleted',
        );
    }

    public function checkDuplicate($attribute, $params)
    {
        if ($this->isNewRecord) {
            if($this->findByAttributes(array('date'=>$this->date,'contact_id'=>$this->contact_id))) {
                $this->addError('date','One Thing with this date already exists.');
            }
        }
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord) {

            $this->added = new CDbExpression('NOW()');
            $this->added_by = Yii::app()->user->id;
        }

        if($this->date) {
            $this->date = date(StmFormatter::MYSQL_DATE_FORMAT, strtotime($this->date));
        }
        $this->updated = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    protected function afterSave()
    {

        if(YII_DEBUG) {
            //$this->sendEmail();
        }
        else {
            if ($this->isNewRecord) {

                $this->sendEmail();
            }
        }

        parent::afterSave();
    }

    protected function sendEmail()
    {
        if(empty(Yii::app()->user->settings->one_thing_report_email)) {
            return;
        }
        else {
            $emailString = Yii::app()->user->settings->one_thing_report_email;

            //check for spaces and replace with comma
            if(strpos($emailString, ' ') !== false) {
                $emailString = str_replace(' ', ',', $emailString);
            }

            // check for commas
            if(strpos($emailString, ',') !== false) {
                $emails = explode(',', $emailString);
            }

            $emails = ($emails)? $emails: (array) $emailString;

            foreach($emails as $email) {

                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $recipients[$email] = '';
                }
            }

            if(!$recipients) {

                // log error
                return;
            }
        }

        $goal = Goals::model()->find(array('condition'=>'year='.date('Y').' AND contact_id='.$this->contact_id));
        if(!$goal) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Goal was not found for this year for Contact #: '.$this->contact_id);
        }

        $goalId = $goal->id;
        $dailyGoalPercent = $this->getDailyGoalPercent($goalId, date('Y-m-d'));

        $monthlyGoalPercent = GoalsWeekly::model()->getGoalPercent($goalId, date($goal->year.'-m-01'), date($goal->year.'-m-t'));
        $yearlyGoalPercent = GoalsWeekly::model()->getGoalPercent($goalId, date($goal->year.'-01-01'), date($goal->year.'-m-t'));

        $viewPath = YiiBase::getPathOfAlias('admin_module.views.emails.oneThingDaily').'.php';

        $emailBody = Yii::app()->controller->renderFile($viewPath, array(
                'domainName' => Yii::app()->user->domain->name,
                'model' => $this,
                'dailyGoalPercent' => $dailyGoalPercent,
                'monthlyGoalPercent' => $monthlyGoalPercent,
            ), $returnContent=true);

        $subjectPrefix = (Yii::app()->user->settings->one_thing_report_email_subject)? Yii::app()->user->settings->one_thing_report_email_subject : 'One Thing Report';
        $subject = $subjectPrefix.' ('.date('n/j/Y', strtotime($this->date)).') '.Yii::app()->user->contact->fullName.'\'s Goal => '.$dailyGoalPercent.'% Today / '.$monthlyGoalPercent.'% Monthly / '.$yearlyGoalPercent.'% Yearly';

        StmZendMail::easyMail(array(Yii::app()->user->contact->primaryEmail => Yii::app()->user->contact->fullName), $recipients, $subject, $emailBody, null, 'text/html', $awsTransport=false);
    }

    protected function getDailyGoalPercent($goalId, $date)
    {
        return $this->percent_daily_goal_accomplished;
    }

    public static function getMissingDays($contactId, $startDate=null, $endDate=null)
    {
        if(!$contactId) {
            // this means error. Should exception be thrown?
            // throw new Exception(__CLASS__.'(:'.__LINE__.') ContactId is missing. Stop everything.');
            return false;
        }
        $thisYearGoal = Goals::getThisYearGoal($contactId);

        if(!$startDate || !$endDate) {

            // @todo: grab current year goals to get start/end date or the earliest onething entry for the year (this safe guards against false entries for start/end date to avoid redirect)
            // $minEntryDate = Yii::app()->db->createCommand("select MIN(date) from goals_one_thing where contact_id={$contactId} and date >='".date('Y-01-01')."' AND is_deleted=0")->queryScalar();

            if(!$thisYearGoal) {
                return false;
            }

            $startDate = ($startDate) ? $startDate : $thisYearGoal->start_date;
            if($startDate < $thisYearGoal->year.'-01-01') {
                $startDate = $thisYearGoal->year.'-01-01';
            }

            $endDate = ($endDate) ? $endDate : $thisYearGoal->end_date;
        }
        else {

            // if the start date in the goal is later than the start date entered, default to the goal entry NOTE: This may open up a loop hole for avoiding redirects. Keep an eye on this to see if this becomes a problem.
            $startDate = ($startDate < $thisYearGoal->start_date) ? $thisYearGoal->start_date : $startDate;
        }

        $yearMonthWeekdays = StmFunctions::getWeekdayDates($startDate, $endDate);

        $exitingRecordsDates = Yii::app()->db->createCommand("select date from goals_one_thing WHERE contact_id={$contactId} AND date >='".$startDate."' AND date <='".$endDate."' AND is_deleted=0 ORDER BY date ASC")->queryColumn();

        $missingDates = array_diff($yearMonthWeekdays, $exitingRecordsDates);

        // get all weekdays for the month and see which days are missing
        return $missingDates;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('goal_description',$this->goal_description,true);
        $criteria->compare('results_description',$this->results_description,true);
        $criteria->compare('challenges',$this->challenges);
        $criteria->compare('percent_daily_goal_accomplished',$this->percent_daily_goal_accomplished);
        $criteria->compare('percent_weekly_goal_accomplished',$this->percent_weekly_goal_accomplished);
        $criteria->compare('percent_monthly_goal_accomplished',$this->percent_monthly_goal_accomplished);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->is_deleted);
        $criteria->order = 'contact_id, date DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}