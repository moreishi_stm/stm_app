<?php

	/**
	 * This is the model class for table "task_types".
	 *
	 * The followings are the available columns in table 'task_types':
	 *
	 * @property integer          $id
	 * @property string           $name
	 * @property integer          $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property TransactionTypes $transactionType
	 * @property Accounts         $account
	 * @property Tasks[]          $tasks
	 */
	class TaskTypes extends StmBaseActiveRecord
    {
        const TODO = 1;
		const PHONE = 2;
        const EMAIL_MANUAL = 3;
        const TEXT_MESSAGE = 4;
        const HANDWRITTEN_NOTE = 6;
        const MAIL = 7;
		const NOTE = 8;
        const LIVE_DEMO = 9;
        const SHOWING = 11;
        const SHOWING_FEEDBACK = 15;
        const SELLER_MARKET_UPDATE = 17;
		const TRANSFER = 24;
        const WEB_CHAT = 25;
		const PASSWORD_RESET = 28;
		const RESUBMIT = 29;
        const AUTO_EMAIL_DRIP = 30;
		const ACTION_PLAN = 31;
        const WAITING = 32;
        const BAD_PHONE = 33;
        const WELCOME_EMAIL = 34;
        const BLOG_COMMENT = 35;
        const EMAIL_DRIP_ERROR = 36;
        const SYSTEM_NOTE = 37;
        const NEW_LEAD_SUBMIT = 38;
        const MARKETING = 39;
        const DIALER_NO_ANSWER = 40;
        const DIALER_ANSWER = 42;
        const AUTO_CLIENT_UPDATE = 41;
        const DIALER_SKIP = 43;
        const DIALER_BAD_NUMBER_DELETED = 44;
        const SPECIAL_DATES=  45;
        const SAVED_SEARCH_EMAILS = 46;
        const AUTO_STATUS_UPDATE = 47;
        const PHONE_RECORD_LOG = 48;
        const EMAIL_RECORD_LOG = 49;
		const DIALER_ADD_DO_NOT_CALL = 50;

		/* Added Bkozak 3/31 - SMS Activity */
		const SMS_OUTBOUND = 51;
		const SMS_INBOUND = 52;
		/* END Added Bkozak 3/31 - SMS Activity */

        const DIALER_LEFT_VOICEMAIL = 53;
        const DIALER_LIST_ADD = 54;
        const DIALER_LIST_DELETE = 55;

        const DIALER_DIAL = 56;
        const SYSTEM_MERGE = 57;
        const TRIGGER = 58;
        const OPPORTUNITY = 59;

        const INCLUDE_CALL_REPORTING = 60;
        const EXCLUDE_CALL_REPORTING = 61;
        const MASS_ASSIGNMENT_TRANSFER = 62;
        const PROTECTED_ASSIGNMENT = 63;
		const IMPORTED_NOTE = 64;
        const MANUAL_ADD_DO_NOT_CALL = 65;
        const SHOWING_REQUEST = 67;
        const CLICK_TO_CALL = 68;

        const EMAIL_BLAST = 69;
        const SMS_BLAST = 70;

        const HQ_PROFILE_SUBMISSION = 1001;

		const LETTER = 66; //LetterTemplates::$id jamesk 4/16/2016

        const IMPORTED = 71;

//        const TRANSFER_RESPONSE_NEEDED = 9999;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TaskTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'task_types';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 50
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, name, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'taskTypeComponents' => array(
					self::HAS_MANY,
					'TaskTypeComponentLu',
					'task_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Scope: byComponentType
		 *
		 * @param  int $componentTypeId
		 *
		 * @return mixed Criteria
		 */
		public function byComponentType($componentTypeId) {
			$this->getDbCriteria()->mergeWith(array(
					'with' => array('taskTypeComponents'),
					'condition' => 'taskTypeComponents.component_type_id=:component_type_id',
					'params' => array(':component_type_id' => $componentTypeId),
				)
			);

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
