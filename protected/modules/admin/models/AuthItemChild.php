<?php

	/**
	 * This is the model class for table "AuthItemChild".
	 *
	 * The followings are the available columns in table 'AuthItemChild':
	 *
	 * @property string   $parent
	 * @property string   $child
	 *
	 * The followings are the available model relations:
	 * @property AuthItem $parent0
	 * @property AuthItem $child0
	 */
	class AuthItemChild extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AuthItemChild the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'auth_item_child';
		}

		public function primaryKey() {
			return array(
				'parent',
				'child'
			);
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'parent, child',
					'required'
				),
				array(
					'parent, child',
					'length',
					'max' => 64
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'parent, child',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'itemParent' => array(
					self::BELONGS_TO,
					'AuthItem',
					'parent'
				),
				'itemChild' => array(
					self::BELONGS_TO,
					'AuthItem',
					'child'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'parent' => 'Parent',
				'child' => 'Child',
			);
		}

		/**
		 * The arguments are optional to provide more flexibility due to saved states
		 *
		 * @param null $parent
		 * @param null $child
		 *
		 * @return $this
		 */
		public function byParentAndChild($parent, $child) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'parent = :parent AND child = :child',
					'params' => array(
						':parent' => $parent,
						':child' => $child,
					),
				)
			);

			return $this;
		}

		/**
		 * Decorator for child scopes
		 *
		 * @param $child
		 *
		 * @return $this
		 */
		public function byChild($child) {
			if(is_array($child)) {
				$this->getDbCriteria()->addInCondition('child', $child);
			} else {
				$this->getDbCriteria()->mergeWith(array(
						  'condition' => 'child = :child',
						  'params' => array(
							  ':child' => $child,
						)
					)
				);
			}

			return $this;
		}

		/**
		 * Decorator for parent scopes
		 *
		 * @param $parent
		 *
		 * @return $this
		 */
		public function byParent($parent) {
			if (is_array($parent)) {
				$this->getDbCriteria()->addInCondition('parent', $parent);
			} else {
				$this->getDbCriteria()->mergeWith(array(
						'condition' => 'parent = :parent',
						'params' => array(
							':parent' => $parent,
						)
					)
				);
			}

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('parent', $this->parent, true);
			$criteria->compare('child', $this->child, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}