<?php

	/**
	 * This is the model class for table "tracking_market_flow".
	 *
	 * The followings are the available columns in table 'tracking_market_flow':
	 *
	 * @property integer  $id
	 * @property integer  $account_id
	 * @property string   $name
	 * @property string   $description
	 * @property text     $criteria
	 * @property integer  $is_primary
	 * @property integer  $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Accounts $account
	 */
	class MarketTrends extends StmBaseActiveRecord {

		public $trends = array(
			.073,
			.065,
			.079,
			.076,
			.084,
			.107,
			.105,
			.11,
			.086,
			.076,
			.068,
			.071
		);

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TrackingMarketFlow the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'market_trends';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'description',
					'length',
					'max' => 127
				),
				array(
					'criteria',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, name, description, criteria, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( // 'contact' => array(self::BELONGS_TO, 'Contacts', 'account_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Contact',
				'market_trend_type_ma' => 'Type',
				'name' => 'Name',
				'description' => 'Description',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}