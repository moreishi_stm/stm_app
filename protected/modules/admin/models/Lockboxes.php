<?php

/**
 * This is the model class for table "lockboxes".
 *
 * The followings are the available columns in table 'lockboxes':
 * @property integer $id
 * @property string $status
 * @property string $type
 * @property string $serial_number
 * @property integer $transaction_id
 * @property string $notes
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 * @property Transactions $transaction
 * @property Contacts $updatedBy
 */
class Lockboxes extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Lockboxes the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lockboxes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('transaction_id, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('status, type, serial_number', 'required'),
            array('serial_number', 'unique'),
            array('status', 'length', 'max'=>100),
            array('type', 'length', 'max'=>13),
            array('serial_number', 'length', 'max'=>25),
            array('notes, updated, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, status, type, serial_number, transaction_id, notes, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'transaction' => array(self::BELONGS_TO, 'Transactions', 'transaction_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'type' => 'Type',
            'serial_number' => 'Serial Number',
            'transaction_id' => 'Transaction',
            'notes' => 'Notes',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    public function getStatusTypes()
    {
        return array(
            'Unassigned' => 'Unassigned',
            'Assigned / At Property' => 'Assigned / At Property',
            'Assigned / At Office, Needs Processing' => 'Assigned / At Office, Needs Processing',
            'In Transit' => 'In Transit',
            'Lost / Stolen' => 'Lost / Stolen',
        );
    }

    public function getLockboxTypes()
    {
        return array(
            'MLS Lockbox' => 'MLS Lockbox',
            'Combo Lockbox' => 'Combo Lockbox',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('serial_number',$this->serial_number,true);
        $criteria->compare('transaction_id',$this->transaction_id);
        $criteria->compare('notes',$this->notes,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}