<?php
	/**
	 * This is the model class for table "mls_feed_log".
	 *
	 * The followings are the available columns in table 'mls_feed_log':
	 *
	 * @property integer   $id
	 * @property integer   $saved_home_search_id
	 * @property integer   $mls_property_id
	 * @property datetime  $send_date
	 *
	 * The followings are the available model relations:
	 * @property Contacts  $contact
	 */
	class MarketUpdateLog extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return MlsFeedLog the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'market_update_log';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'saved_home_search_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'saved_home_search_id, mls_property_id',
					'required'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, saved_home_search_id, mls_property_id, send_date',
					'safe',
					'on' => 'search'
				),
			);
		}

		public function byDateRange($dateRange) {
			if($dateRange['from_date']) {
				$Criteria = new CDbCriteria;
				$Criteria->condition = 'send_date>=:from_date';
				$Criteria->params = array(':from_date' => $dateRange['from_date']);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			if($dateRange['to_date']) {
				$Criteria = new CDbCriteria;
				$Criteria->condition = 'send_date<=:to_date';
				$Criteria->params = array(':to_date' => $dateRange['to_date']);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}

		/**
		 * Filter the ORM by records that were sent within the last $daysPrior days integer based on the
		 * difference between the formula ((to_days(CURRENT_DATE) - to_days(send_date)) <= $daysPrior)
		 *
		 * @param $daysPrior
		 *
		 * @author Chris Willard <chriswillard.dev@gmail.com>
		 * @since  06/11/2013
		 * @return MarketUpdateLog $this
		 */
		public function byDaysPrior($daysPrior) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => '((to_days(CURRENT_DATE) - to_days(send_date)) <= :daysPrior)',
					'params' => array(
						':daysPrior' => $daysPrior,
					),
				)
			);

			return $this;
		}

		/**
		 * Filter the ORM by records that were specifically sent to the given Contact.
		 *
		 * @param Contacts $contact
		 *
		 * @author Will Madison <will@willmadison.com>
		 * @since  06/27/2013
		 * @return MarketUpdateLog $this
		 */
		public function bySavedHomeSearchId($savedHomeSearchId) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'saved_home_search_id = :savedHomeSearch',
					'params' => array(
						':savedHomeSearch' => $savedHomeSearchId,
					),
				)
			);

			return $this;
		}

		/**
		 * Filter by a list of mls property ids
		 *
		 * @param $listOfPropertyIds
		 * @param $exclude = false If set to true, will exclude the list by the provided property ids
		 *
		 * @author Chris Willard <chriswillard.dev@gmail.com>
		 * @since  06/11/2013
		 * @return MarketUpdateLog $this
		 */
		public function byMlsPropertyIds($listOfPropertyIds, $exclude = false) {
			$propertyIdCriteria = new CDbCriteria;
			$inConditionMethod = ($exclude) ? 'addNotInCondition' : 'addInCondition';
			$propertyIdCriteria->{$inConditionMethod}('mls_property_id', $listOfPropertyIds);
			$this->getDbCriteria()->mergeWith($propertyIdCriteria);

			return $this;
		}

		protected function beforeSave() {
			$this->send_date = new CDbExpression('NOW()');

			return parent::beforeSave();
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contact' => array(
					self::BELONGS_TO,
					'SavedHomeSearches',
					'saved_home_search_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'saved_home_search_id' => 'Saved Home Search Id',
				'mls_property_id' => 'Mls Property Id',
				'send_date' => 'Market Update Send Date',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria();

			$criteria->compare('id', $this->id);
			$criteria->compare('saved_home_search_id', $this->saved_home_search_id);
			$criteria->compare('mls_property_id', $this->mls_property_id);
			$criteria->compare('send_date', $this->send_date);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}