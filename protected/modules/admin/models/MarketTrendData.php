<?php

	/**
	 * This is the model class for table "market_trend_data".
	 *
	 * The followings are the available columns in table 'market_trend_data':
	 *
	 * @property integer      $id
	 * @property integer      $market_trend_id
	 * @property string       $date
	 * @property integer      $value
	 * @property integer      $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property MarketTrends $marketTrend
	 */
	class MarketTrendData extends StmBaseActiveRecord {

		public $remove = false;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return MarketTrendData the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'market_trend_data';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'date, value',
					'required'
				),
				array(
					'market_trend_id, value, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'remove',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, market_trend_id, date, value, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'marketTrend' => array(
					self::BELONGS_TO,
					'MarketTrends',
					'market_trend_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'market_trend_id' => 'Market Trend',
				'date' => 'Date',
				'value' => 'Value',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * format date field from the gui format to the db format
		 *
		 * @return boolean
		 */
		protected function beforeSave() {
			$this->date = Yii::app()->format->formatDate($this->date, $dateFormat = 'Y-m-d');

			return parent::beforeSave();
		}

		/**
		 * format date field from the db format to the gui format
		 *
		 * @return boolean
		 */
		protected function afterFind() {
			// Format anything that needs to be changed for the gui after a record is found
			$this->date = Yii::app()->format->formatDate($this->date);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('market_trend_id', $this->market_trend_id);
			$criteria->compare('date', $this->date, true);
			$criteria->compare('value', $this->value);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}