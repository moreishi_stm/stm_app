<?php

	/**
	 * This is the model class for table "traffic_data_raw".
	 *
	 * The followings are the available columns in table 'traffic_data_raw':
	 *
	 * @property integer $id
	 * @property integer $contact_id
	 * @property integer $user_agent_type
	 * @property integer $home_detail_view_count
	 * @property string  $pageview_cookie
	 * @property string  $domain
	 * @property string  $page
	 * @property string  $referrer
	 * @property string  $campaign_source
	 * @property string  $campaign_name
	 * @property string  $campaign_medium
	 * @property string  $campaign_content
	 * @property string  $campaign_term
	 * @property string  $first_visit
	 * @property string  $previous_visit
	 * @property string  $current_visit_started
	 * @property string  $times_visited
	 * @property string  $pages_viewed
	 * @property string  $utmz
	 * @property string  $utma
	 * @property string  $utmb
	 * @property string  $utmc
	 * @property string  $ip_id
	 * @property string  $session_id
	 * @property string  $added
	 */
	class TrafficTracker extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TrafficDataRaw the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'traffic_tracker';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'session_id, ip_id, added',
					'required'
				),
				array(
					'contact_id, home_detail_view_count, session_id, ip_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'user_agent_type, user_agent_name, user_agent_os_name',
					'length',
					'max' => 63
				),
				array(
					'domain, campaign_source, campaign_name, campaign_medium, campaign_content, campaign_term, first_visit, previous_visit, current_visit_started, times_visited, pages_viewed',
					'length',
					'max' => 255
				),
				array(
					'page, pageview_cookie, referrer, utmz, utma, utmb, utmc',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, user_agent_type, user_agent_name, user_agent_os_name, home_detail_view_count, pageview_cookie, domain, page, referrer, campaign_source, campaign_name, campaign_medium, campaign_content, campaign_term, first_visit, previous_visit, current_visit_started, times_visited, pages_viewed, utmz, utma, utmb, utmc, ip_id, session_id, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'ip' => array(
					self::BELONGS_TO,
					'TrafficTrackerIps',
					'ip_id'
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'session' => array(
					self::BELONGS_TO,
					'TrafficTrackerSessions',
					'session_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'user_agent_type' => 'User Agent Type',
				'user_agent_name' => 'User Agent Name',
				'user_agent_os_name' => 'User Agent OS',
				'home_detail_view_count' => 'Home Views',
				'pageview_cookie' => 'Pageview Cookie',
				'domain' => 'Domain',
				'page' => 'Page',
				'referrer' => 'Referrer',
				'campaign_source' => 'Campaign Source',
				'campaign_name' => 'Campaign Name',
				'campaign_medium' => 'Campaign Medium',
				'campaign_content' => 'Campaign Content',
				'campaign_term' => 'Campaign Term',
				'first_visit' => 'First Visit',
				'previous_visit' => 'Previous Visit',
				'current_visit_started' => 'Current Visit Started',
				'times_visited' => 'Times Visited',
				'pages_viewed' => 'Pages Viewed',
				'utmz' => 'Utmz',
				'utma' => 'Utma',
				'utmb' => 'Utmb',
				'utmc' => 'Utmc',
				'ip_id' => 'IP',
				'session_id' => 'Session',
				'added' => 'Added',
			);
		}

		//	public function findBySessionId($id) {
		//		return $this->findAll(array('condition'=>'session_id=:session_id','params'=>array(':session_id'=>$id)));
		//	}

		public function countBySessionId($id) {
			return $this->count(array(
					'condition' => 'session_id=:session_id',
					'params' => array(':session_id' => $id)
				)
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('user_agent_type', $this->user_agent_type);
			$criteria->compare('user_agent_name', $this->user_agent_name);
			$criteria->compare('user_agent_os_name', $this->user_agent_os_name);
			$criteria->compare('home_detail_view_count', $this->home_detail_view_count);
			$criteria->compare('pageview_cookie', $this->pageview_cookie, true);
			$criteria->compare('domain', $this->domain, true);
			$criteria->compare('page', $this->page, true);
			$criteria->compare('referrer', $this->referrer, true);
			$criteria->compare('campaign_source', $this->campaign_source, true);
			$criteria->compare('campaign_name', $this->campaign_name, true);
			$criteria->compare('campaign_medium', $this->campaign_medium, true);
			$criteria->compare('campaign_content', $this->campaign_content, true);
			$criteria->compare('campaign_term', $this->campaign_term, true);
			$criteria->compare('first_visit', $this->first_visit, true);
			$criteria->compare('previous_visit', $this->previous_visit, true);
			$criteria->compare('current_visit_started', $this->current_visit_started, true);
			$criteria->compare('times_visited', $this->times_visited, true);
			$criteria->compare('pages_viewed', $this->pages_viewed, true);
			$criteria->compare('utmz', $this->utmz, true);
			$criteria->compare('utma', $this->utma, true);
			$criteria->compare('utmb', $this->utmb, true);
			$criteria->compare('utmc', $this->utmc, true);
			$criteria->compare('ip_id', $this->ip_id, true);
			$criteria->compare('session_id', $this->session_id, true);
			$criteria->compare('added', $this->added, true);

			$criteria->order = 'added DESC';

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}