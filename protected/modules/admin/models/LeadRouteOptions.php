<?php

/**
 * This is the model class for table "lead_route_options".
 *
 * The followings are the available columns in table 'lead_route_options':
 * @property integer $id
 * @property string $name
 * @property integer $multiple_values
 * @property string $added
 * @property string $updated
 */
class LeadRouteOptions extends StmBaseActiveRecord
{
    const OPTION_PRICE_EQUAL = 1;
    const OPTION_PRICE_LESS = 2;
    const OPTION_PRICE_GREATER = 3;
    const OPTION_LOCATION_CITY = 4;
    const OPTION_LOCATION_ZIP = 5;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeadRouteOptions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lead_route_options';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('multiple_values', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>45),
            array('added, updated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, multiple_values, added, updated', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'multiple_values' => 'Multiple Values',
            'added' => 'Added',
            'updated' => 'Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('multiple_values',$this->multiple_values);
        $criteria->compare('added',$this->added,true);
        $criteria->compare('updated',$this->updated,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
} 