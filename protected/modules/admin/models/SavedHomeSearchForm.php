<?php

	/**
	 * SavedHomeSearchForm class.
	 * SavedHomeSearchForm is the data structure for saved home searches.
	 */
	class SavedHomeSearchForm extends CFormModel {

		public $name;
		public $description;
		public $frequency;
		public $id;
        public $mls_board_id;
        public $component_type_id;
        public $component_id;
		public $contact_id;
		public $agent_id;
		public $property_type;
		public $region;
        public $area;
		public $zip = array();
        public $city;
		public $price_min;
		public $price_max;
		public $year_built;
        public $year_built_max;
		public $bedrooms;
        public $bedrooms_max;
		public $baths_total;
        public $baths_total_max;
		public $sq_feet;
        public $sq_feet_max;
		public $subdivision;
		public $neighborhood;
        public $school;
		public $waterfront_yn;
		public $pool;
		public $foreclosure;
		public $short_sale;
        public $public_remarks;
        public $acreage_min;
        public $acreage_max;
        public $master_bedroom_level;
        public $county = array();
        public $lot_description;
        public $car_garage;

        public $sunday;
        public $monday;
        public $tuesday;
        public $wednesday;
        public $thursday;
        public $friday;
        public $saturday;
		public $stories;
		public $school_district;


		private $_fields = array(
			'property_type',
			'price_min',
			'price_max',
			'area',
            'region',
			'zip',
            'city',
			'year_built',
            'year_built_max',
			'bedrooms',
            'bedrooms_max',
			'baths_total',
            'baths_total_max',
			'sq_feet',
            'sq_feet_max',
			'subdivision',
			'neighborhood',
            'school',
			'waterfront_yn',
			'pool',
			'foreclosure',
			'short_sale',
            'public_remarks',
            'acreage_min',
            'acreage_max',
            'master_bedroom_level',
            'county',
            'lot_description',
			'stories',
			'school_district',
            'car_garage'
		);

		/**
		 * Declares the validation rules.
		 * The rules state that email and password are required,
		 * and password needs to be authenticated.
		 */
		public function rules() {
			return array(
				array(
					'mls_board_id, component_type_id, component_id, contact_id, property_type, name, frequency, agent_id',
					'required',
					'message' => '{attribute} cannot be blank'
				),
				array(
					'pool, waterfront, foreclosure, short_sale,
					sunday,
                    monday,
                    tuesday,
                    wednesday,
                    thursday,
                    friday,
                    saturday',
					'boolean'
				),
                array(
                    'sunday,
                    monday,
                    tuesday,
                    wednesday,
                    thursday,
                    friday,
                    saturday,
                    stories,
                    car_garage',
                    'numerical',
                    'integerOnly' => true
                ),
				array(
					'id,
					 mls_board_id,
					 component_type_id,
					 component_id,
					 contact_id,
					 property_type,
					 region,
					 area,
					 zip,
					 city,
					 price_min,
					 price_max,
					 year_built,
					 year_built_max,
					 bedrooms,
					 bedrooms_max,
					 baths_total,
					 baths_total_max,
					 sq_feet,
					 sq_feet_max,
					 subdivision,
					 neighborhood,
					 school,
					 waterfront,
					 pool,
					 foreclosure,
					 short_sale,
					 public_remarks,
					 acreage_min,
					 acreage_max,
					 master_bedroom_level,
					 county,
					 lot_description,
					 school_district',
					'safe'
				),
			);
		}

		/**
		 * Declares attribute labels.
		 */
		public function attributeLabels() {
			return array(
				'property_type' => 'Property Type',
				'name' => 'Search Name',
				'agent_id' => 'Agent',
                'region' => 'Region',
				'area' => 'Area',
				'zip' => 'Zip',
                'city' => 'City',
				'price_min' => 'Price',
				'price_max' => 'Price Max',
				'year_built' => 'Year Built',
				'bedrooms' => 'Bedrooms',
				'baths_total' => 'Baths',
				'sq_feet' => 'Sq. Feet',
				'subdivision' => 'Subdivision',
				'neighborhood' => 'Neighborhood',
                'school' => 'School',
				'waterfront' => 'Waterfront',
				'pool' => 'Pool',
				'foreclosure' => 'Foreclosure',
                'short_sale' => 'Short Sale',
                'public_remarks' => 'Keyword/Remarks',
                'acreage_min' => 'Acreage',
                'lot_description' => 'Lot Description',
                'sunday'        =>  'Sunday',
                'monday'        =>  'Monday',
                'tuesday'       =>  'Tuesday',
                'wednesday'     =>  'Wednesday',
                'thursday'      =>  'Thursday',
                'friday'        =>  'Friday',
                'saturday'      =>  'Saturday',
				'stories'		=> 'Stories',
				'school_district' => 'School District',
                'car_garage' => 'Garage'
			);
		}

		public function getFields() {
			return $this->_fields;
		}

		public function getCriteria() {
			$criteria = array();
			foreach ($this->_fields as $field) {
				if ($this->$field != '') {
					$criteria[$field] = $this->$field;
				}
			}

			$criteria = CJSON::encode($criteria);

			return $criteria;
		}

		
	}
