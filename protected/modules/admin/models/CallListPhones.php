<?php
/**
 * This is the model class for table "call_list_phones".
 *
 * The followings are the available columns in table 'call_list_phones':
 * @property integer $id
 * @property integer $phone_id
 * @property integer $task_id
 * @property integer $call_list_id
 * @property integer $last_call_session_id
 * @property string $request_uuid
 * @property string $a_leg_uuid
 * @property string $status
 * @property string $call_count
 * @property string $last_called
 * @property integer $sort_order
 * @property string $added
 * @property string $updated
 * @property integer $updated_by
 * @property integer $is_deleted
 * @property string $hard_deleted
 * @property integer $hard_deleted_by
 *
 * The followings are the available model relations:
 * @property CallLists $callList
 * @property CallSessions $lastCallSession
 * @property Phones $phone
 * @property Tasks $task
 * @property Calls[] $calls
 */
class CallListPhones extends StmBaseActiveRecord
{
    const ANSWERED = 'Answered';
    const NO_ANSWER = 'No Answer';
    const SKIPPED = 'Skipped';
    const QUEUED = 'Queued';
    const BOOMERANG = 'Boomerang';
    const RINGING = 'Ringing';
    const COMPLETE = 'Complete';

    const MIN_LIST_COUNT = 20;
    const BUILD_QUEUE_COUNT = 1000;

    public $hardDeletedFromDate;
    public $hardDeletedToDate;

    /**
     * Model
     *
     * Static method to retrieve an instance of this model
     * @return CallListPhones Object instance
     */
    public static function model()
    {
        return parent::model(__CLASS__);
    }

    /**
     * Table Name
     *
     * Used to retrieve the table name associated with this model
     * @return string Database table name
     */
    public function tableName()
    {
        return 'call_list_phones';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('phone_id, call_list_id', 'required'),
            array('phone_id, task_id, call_list_id', 'required', 'on'=>'task'),
            array('phone_id, task_id, call_list_id, sort_order, last_call_session_id, updated_by, is_deleted, hard_deleted_by', 'numerical', 'integerOnly'=>true),
            array('a_leg_uuid, request_uuid', 'length', 'max'=>36),
            array('call_count', 'length', 'max'=>10),
            array('status, last_called, added, updated, hard_deleted', 'safe'),
            array('phone_id','validateDuplicates'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, phone_id, task_id, call_list_id, last_call_session_id, status, call_count, last_called, sort_order, added, updated, updated_by, is_deleted, hard_deleted, hard_deleted_by', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Relations
     *
     * Model relations with other models
     * @return array
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
        return array(
            'callList' => array(
                self::BELONGS_TO,
                'CallLists',
                'call_list_id'
            ),
            'lastCallSession' => array(
                self::BELONGS_TO,
                'CallSessions',
                'last_call_session_id'
            ),
            'phone' => array(
                self::BELONGS_TO,
                'Phones',
                'phone_id'
            ),
            'task' => array(
                self::BELONGS_TO,
                'Tasks',
                'task_id'
            ),
            'updatedBy' => array(
                self::BELONGS_TO,
                'Contacts',
                'updated_by'
            ),
            'hardDeletedBy' => array(
                self::BELONGS_TO,
                'Contacts',
                'hard_deleted_by'
            ),
        );
    }

    protected function beforeValidate()
    {
        // checks to see if user Id has a value. In console command it does not.
        if($userId = Yii::app()->user->id) {
            $this->updated_by = $userId;
        }

        if($this->isNewRecord) {
            $this->added = new CDbExpression('NOW()');
        }

        return parent::beforeValidate();
    }

    protected function beforeSave()
    {
        $this->updated = new CDbExpression('NOW()');
        if($userId = Yii::app()->user->id) {
            $this->updated_by = $userId;
        }

        return parent::beforeSave();
    }

    public function validateDuplicates($attribute, $params)
    {
        // new record check to make sure same phone_id doesn't exist for current list
        if($this->isNewRecord) {
            $tableType = CallLists::getTableTypeById($this->call_list_id);

            $existCommand = Yii::app()->db->createCommand()
                ->select('count(*)')
                ->from('call_list_phones')
                ->where('phone_id=:phone_id', array(':phone_id'=>$this->phone_id))
                ->andWhere('call_list_id=:call_list_id', array(':call_list_id'=>$this->call_list_id));

                //->andWhere('is_deleted=0')
            if($tableType == 'task') {
                $existCommand->andWhere('task_id=:task_id', array(':task_id'=>$this->task_id));
            }

            if($existCommand->queryScalar()) {
                $this->addError($attribute, 'Phone ID already exists for this Call List.');
            }
        }
    }

    public static function addToCallListByContactId($callListId, $contactId, $taskId=null, &$errorLog=null)
    {
        // check to see if call list is of task type and check to see if task ID is provided
        $callList = CallLists::model()->findByPk($callListId);
        if($callList->type->table_type == CallListTypes::TASK_TABLE_TYPE && !$taskId) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Task ID missing for adding to Task Table Type call list.');
        }

        if($taskId && !($task = Tasks::model()->findByPk($taskId))) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Invalid Task ID: '.$taskId.' Call List ID: '.$callListId.' Contact ID: '.$contactId.' - Remove this log when done monitoring.', CLogger::LEVEL_ERROR);
            return; // no need to add to call list if task does not exist. This may occur if someone deleted a task from between this even and when it was added/changed
//            throw new Exception(__CLASS__.'(:'.__LINE__.') Invalid Task ID.');
        }

        if($errorLog==null || !is_array($errorLog)) {
            $errorLog = array();
        }

        $phones = Phones::model()->findAllByAttributes(array('contact_id'=>$contactId));

        if(empty($phones)) {

            // @todo: should we add error log for no phones found? May not be needed for mass adding contacts to call list as many may not have phones
            return;
        }

        foreach($phones as $phone) {

            $criteria = new CDbCriteria();
            $criteria->compare('call_list_id', $callListId);
            $criteria->compare('phone_id', $phone->id);
            $criteria->order = 'id ASC';

            if($models = CallListPhones::model()->skipSoftDeleteCheck()->findAll($criteria)) {
                $model = $models[0];
                $model->is_deleted = 0;
                $model->hard_deleted = null;
                if(!$model->save()) {
                    $errorLog[$phone->id][] = $model->getErrors();
                }
            }
            else {
                $model = new CallListPhones();
                $model->setAttributes(array(
                        'phone_id'=>$phone->id,
                        'call_list_id'=>$callListId,
                    ));
                if(!$model->save()) {
                    $errorLog[$phone->id][] = $model->getErrors();
                }
            }
        }
    }

    public static function removeFromCallListByContactId($callListId, $contactId, &$errorLog=null)
    {
        if(!$errorLog || !is_array($errorLog)) {
            $errorLog = array();
        }
        $phones = Phones::model()->findAllByAttributes(array('contact_id'=>$contactId));

        if(empty($phones)) {

            // @todo: should we add error log for no phones found? May not be needed for mass adding contacts to call list as many may not have phones
            return;
        }

        foreach($phones as $phone) {

            $criteria = new CDbCriteria();
            $criteria->compare('call_list_id', $callListId);
            $criteria->compare('phone_id', $phone->id);
            $criteria->addCondition('hard_deleted IS NULL');

            $models = CallListPhones::model()->skipSoftDeleteCheck()->findAll($criteria);

            foreach($models as $model) {

                $model->is_deleted = 1;
                $model->hard_deleted = new CDbExpression('NOW()');
                $model->hard_deleted_by = Yii::app()->user->id;
                if(!$model->save()) {
                    $errorLog[$phone->id] = $model->getErrors();
                }
            }
        }
    }

    public static function addTaskToExistingCallList($taskId)
    {
        // check for lead pool contact and if the list already exists //@todo: if not exist, ignore for now - CLee 3/27/16
        if($task = Tasks::model()->findByPk($taskId)) {

            // get phone task call list by component_type_id for contact_id
            if($callListType = CallListTypes::model()->findByAttributes(array('component_type_id'=>$task->component_type_id, 'table_type' => CallListTypes::TASK_TABLE_TYPE))) {

                if($callList = CallLists::model()->findByAttributes(array('preset_ma'=>$callListType->id, 'contact_id'=>$task->assigned_to_id))) {

                    //get the contact for the component tuple (seller, buyer, recruit)
                    if($contact = ComponentTypes::model()->findByPk($task->component_type_id)->getContact($task->component_id)) {
                        // add contact phones to call list
                        self::addToCallListByContactId($callList->id, $contact->id, $taskId);
                    }
                }
            }
        }
    }

    public function searchRemoved()
    {
        $criteria=new CDbCriteria;
        $criteria->addCondition('hard_deleted IS NOT NULL');
        $criteria->compare('hard_deleted_by',$this->hard_deleted_by);

        if($this->hardDeletedFromDate) {
            $criteria->addCondition('DATE(hard_deleted) >= :fromDate');
            $criteria->params[':fromDate'] = $this->hardDeletedFromDate;
        }
        if($this->hardDeletedToDate) {
            $criteria->addCondition('DATE(hard_deleted) <= :toDate');
            $criteria->params[':toDate'] = $this->hardDeletedToDate;
        }
        $criteria->order = 'hard_deleted DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}
