<?php
/**
 * This is the model class for table "form_submissions".
 *
 * The followings are the available columns in table 'form_submissions':
 * @property integer $id
 * @property integer $form_id
 * @property integer $form_account_lu_id
 * @property integer $contact_id
 * @property integer $source_id
 * @property string $url
 * @property integer $component_id
 * @property string $type
 * @property string $submission_step
 * @property string $user_agent
 * @property string $referrer
 * @property string $ip_address
 * @property string $added
 *
 * The followings are the available model relations:
 * @property FormSubmitValues[] $formSubmitValues
 * @property Form $form
 * @property Contacts $contact
 * @property Sources $source
 */
class FormSubmissions extends StmBaseActiveRecord
{

	public $formValueTuples = array();

	// Bool, determines if a contact was created by a form submission process
	protected $contactCreated;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return FormSubmits the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'form_submissions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'form_id',
				'required'
			),
			array(
				'form_id, contact_id, source_id',
				'numerical',
				'integerOnly' => true
			),
			array(
				'url',
				'length',
				'max' => 500
			),
			array(
				'type, submission_step, datetime',
				'safe'
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'id, form_id, contact_id, source_id, url, component_id, type, submission_step, user_agent, referrer, ip_address, type, submission_step, datetime',
				'safe',
				'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'formSubmissionValues' => array(
				self::HAS_MANY,
				'FormSubmissionValues',
				'form_submission_id'
			),
			'form' => array(
				self::BELONGS_TO,
				'Forms',
				'form_id'
			),
			'contact' => array(
				self::BELONGS_TO,
				'Contacts',
				'contact_id',
				'alias' => 't'
			),
			'source' => array(
				self::BELONGS_TO,
				'Sources',
				'source_id'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
//				'form_account_lu_id' => 'Form Account Lu',
			'contact_id' => 'Contact',
			'source_id' => 'Source',
			'url' => 'Url',
			'submission_step' => 'Submission Step',
//				'datetime' => 'Datetime',
		);
	}

	public function behaviors()
	{

		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'added',
				'updateAttribute' => null,
			),
		);
	}

	public function getComponentTypeId()
	{

		if (!$this->form) {
			return false;
		}

		return $this->form->component_type_id;
	}

	public function getComponentId()
	{

		if (!$this->contact_id) {
			return null;
		}

		return $this->contact_id;
	}

	/**
	 * retrieveContactInstance
	 * Used to retrieve a new instance of a contact model, based on form submitted data
	 *
	 * @return Contacts
	 */
	public function retrieveContactInstance()
	{

		if (!$this->contact) {

			// Save the new contact so we can update this form submission properly
			$formId = Yii::app()->getRequest()->getQuery('formId');

			if (!$formId) {
				return;
			}

			// Get Email instance from the owner object
			$Email = $this->retrieveEmailInstance();

			// Does the lead's email already exist?
			// Attempt to get an existing contact by this e-mail address
			$ExistingContact = null;
			$ExistingEmail = Emails::model()->find(array(
					'condition' => 'email=:email',
					'params' => array(':email' => strtolower(trim($Email->email))),
				)
			);

			if ($ExistingEmail) {

				$Contact = $ExistingEmail->contact;
				$Email = $ExistingEmail;

			} else {

				$FormFields = new FormFields;

				$firstNameFieldId = $FormFields->getField('first_name')->id;
				$lastNameFieldId = $FormFields->getField('last_name')->id;
				$passwordNameFieldId = $FormFields->getField('password')->id;

				$formSubmissionValues = $_POST['FormSubmissionValues']['data'];

				// Create the new contact and save it so we can update the form submission record
				$Contact = new Contacts;
				$Contact->first_name = $formSubmissionValues[$firstNameFieldId];
				$Contact->last_name = $formSubmissionValues[$lastNameFieldId];
				$Contact->password = $formSubmissionValues[$passwordNameFieldId];

				if (!YII_DEBUG) {
					$userAgent = Yii::app()->userAgent->getInfo();
					$trafficTracker = new TrafficTracker;
					$trafficTracker = Yii::app()->trafficTracker->loadTrackingData($trafficTracker, $userAgent);
				}
				// see if source exists, if not create one.
				if (!empty($trafficTracker->campaign_source)) {
					if ($source = Sources::model()->findByAttributes(array('name' => $trafficTracker->campaign_source))) {
						$trafficSource = $source->id;
					} else {
						$source = new Sources;
						$source->account_id = Yii::app()->user->accountId;
						$source->name = $trafficTracker->campaign_source;
						if (strpos($trafficTracker->campaign_name, '(organic)') !== false or strpos($trafficTracker->campaign_name, '(direct)') !== false) {
							$source->name .= ' ' . $trafficTracker->campaign_name;
						}
						if (!$source->save()) {
							$trafficSource = Sources::ID_WEBSITE;
						}
						$trafficSource = $source->id;
					}
				} else {
					$trafficSource = Sources::ID_WEBSITE;
				}

				$Contact->source_id = $trafficSource;
				$Contact->account_id = Yii::app()->user->getAccountId();
				$Contact->session_id = session_id();
				$Contact->save(false);

				// Specify the "Prospect" contact type for new contacts
				$ContactTypeLu = new ContactTypeLu;
				$ContactTypeLu->contact_id = $Contact->id;
				$ContactTypeLu->contact_type_id = ContactTypes::PROSPECT;
				$ContactTypeLu->save();

				$Email->account_id = Yii::app()->user->getAccountId();
				$Email->contact_id = $Contact->id;
				$Email->is_primary = 0;
				$Email->save();
			}

			$this->contact_id = $Contact->id;
			$this->contact = $Contact;
			$this->setIsNewRecord(false); // This is necessary otherwise the next save() call will try to insert instead of updating
			$this->save(false);
		}

		// Check for existing phone numbers if the phone number doesn't exist add it to the contact
		$phone = $this->retrievePhoneInstance();
		if ($phone) {
			$existingPhone = Phones::model()->byPhoneNumber($phone->phone)->byContactId($Contact->id)->find();
			if (!$existingPhone) {
				$phone->contact_id = $Contact->id;
				$phone->account_id = Yii::app()->user->getAccountId();
				$phone->is_primary = 0;
				$phone->save();
			}
		}

		$address = $this->retrieveAddressInstance();
		if ($address && $address->isNewRecord) {
			$address->contactId = $Contact->id;
			$address->account_id = Yii::app()->user->getAccountId();
			if ($address->save()) {
				$this->contact->contactAddresses = array($address);
			} else {
				Yii::log("ERROR in saving new Address from Form Submissions (File: " . __FILE__ . " / Line: " . __LINE__ . "). Error Message: " . print_r($address->getErrors(), true) . " Attributes: " . print_r($address->getErrors(), true), CLogger::LEVEL_ERROR, AbstractFormSubmitAction::LOG_CATEGORY);
			}
		}

		return $this->contact;
	}

	/**
	 * retrievePhoneInstance
	 * Used to retrieve a new instance of a phone model, based on the form submitted data
	 *
	 * @return Phones
	 */
	public function retrievePhoneInstance()
	{

		$formId = Yii::app()->getRequest()->getQuery('formId');

		if (!$formId) {
			return;
		}

		$Phone = new Phones;
		$FormFields = new FormFields;

		$phoneFieldId = $FormFields->getField('phone')->id;

		// If we can't find the field id, then there is no data to validate
		if (!$phoneFieldId) {
			return null;
		}

		$formSubmissionValues = $_POST['FormSubmissionValues']['data'];
		$phoneSubmissionValue = $formSubmissionValues[$phoneFieldId];

		if (isset($phoneSubmissionValue)) {
			$Phone->phone = $phoneSubmissionValue;

			if (!$Phone->validate()) {
				Yii::log('Encountered invalid phone number entry: ' . $Phone->phone, 'warning', __CLASS__);
				$Phone = null;
			}
		}

		return $Phone;
	}

	/**
	 * retrieveEmailInstance
	 * Used to retrieve a new instance of a email model, based on the form submitted data.
	 *
	 * @return Emails
	 */
	public function retrieveEmailInstance()
	{

		$formId = Yii::app()->getRequest()->getQuery('formId');

		if (!$formId) {
			return;
		}

		$Email = new Emails;
		$FormFields = new FormFields;

		$emailFieldId = $FormFields->getField('email')->id;

		$formSubmissionValues = $_POST['FormSubmissionValues']['data'];
		$emailSubmissionValue = $formSubmissionValues[$emailFieldId];

		if (isset($emailSubmissionValue)) {
			$Email->email = $emailSubmissionValue;
		}

		return $Email;
	}

	public function retrieveAddressInstance()
	{
		$formSubmissionValues = $_POST['FormSubmissionValues']['data'];
		$Address = new Addresses;
		$FormFields = new FormFields;

		if (empty($formSubmissionValues[$FormFields->getFieldIdByName('address')])) {
			return null;
		}

		$formId = Yii::app()->getRequest()->getQuery('formId');

		if (!$formId) {
			return null;
		}

		$addressFieldId = $FormFields->getFieldIdByName('address');
		if (!$addressFieldId) {
			return null;
		}

		$stateFieldId = $FormFields->getFieldIdByName('state');
		$cityFieldId = $FormFields->getFieldIdByName('city');
		$zipFieldId = $FormFields->getFieldIdByName('zip');


		//@todo: figure out how to handle more complex address matching logic: parsing house # and seeing if strpos() and the rest of the string to see if it's strpos()
		$criteria = new CDbCriteria;
		$criteria->compare('address', substr($formSubmissionValues[$addressFieldId], 0, 7), true);
		$criteria->compare('zip', $formSubmissionValues[$zipFieldId]);
		$criteria->order = 'id ASC';

		//@todo: need to add criteria for the same contact??? or can just check after find for it?
		$existingAddress = $Address->findAll($criteria);

		if (empty($existingAddress)) {
			$existingAddress = null;
		} elseif (count($existingAddress) == 1) {
			$existingAddress = $existingAddress[0];
		} elseif (count($existingAddress) > 1) {
			foreach ($existingAddress as $i => $singleExistingAddress) {
				$sellers = $singleExistingAddress->sellers;
				if (!empty($sellers)) {
					//@todo: check status of seller transaction for delete!!
					$existingAddress = $existingAddress[$i];
					break;
				}
			}
			Yii::log(__CLASS__ . ': There are multiple address matches for this contact form submission. Form Data: ' . print_r($formSubmissionValues, true), CLogger::LEVEL_ERROR, AbstractFormSubmitAction::LOG_CATEGORY);
		}

		if ($existingAddress) {
			$Address = $existingAddress;
			$this->contact->isNewSubmitAddressId = false;
		} else {
			$this->contact->isNewSubmitAddressId = true;
			$Address->address = $formSubmissionValues[$addressFieldId];
			$Address->city = $formSubmissionValues[$cityFieldId];
			$Address->zip = $formSubmissionValues[$zipFieldId];
			if (!is_numeric($formSubmissionValues[$stateFieldId])) {
				$state = AddressStates::model()->findByAttributes(array('short_name' => strtoupper($formSubmissionValues[$stateFieldId])));
				$Address->state_id = $state->id;
			} else {
				$Address->state_id = $formSubmissionValues[$stateFieldId];
			}
			$Address->contactId = $this->contact->id;
			$Address->account_id = Yii::app()->user->getAccountId();
			if (!$Address->save()) {
				Yii::log(__FILE__ . '(' . __LINE__ . '): Error: Address model did not save. Errors: ' . print_r($Address->getErrors(), true), CLogger::LEVEL_ERROR, AbstractFormSubmitAction::LOG_CATEGORY);
			}
		}
		$this->contact->submitAddressId = $Address->id;

		return $Address;
	}

	protected function _searchHelper()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
//			$criteria->compare('form_account_lu_id', $this->form_account_lu_id);
		$criteria->compare('contact_id', $this->contact_id);
		$criteria->compare('source_id', $this->source_id);
		$criteria->compare('url', $this->url, true);
		$criteria->compare('type', $this->type, true);
		$criteria->compare('submission_step', $this->submission_step, true);
//			$criteria->compare('datetime', $this->datetime, true);

		return $criteria;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = $this->_searchHelper();
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function searchForSellerGrid()
	{
		$criteria = $this->_searchHelper();

		$criteria->with = array('form','source');

		$criteria->compare('form.component_type_id', ComponentTypes::SELLERS);
		$criteria->together = true;
        $criteria->order = 't.added DESC';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
		));
	}
}
