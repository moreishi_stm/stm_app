<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class TitleCompanyAssignmentGroup extends AssignmentGroup
{

    protected function assignmentAssigneeComponentColumn() {

        return 'assignee_contact_id';
    }


    public function getAssignmentTypeIds()
    {

        return array(AssignmentTypes::TITLE_AGENT, AssignmentTypes::TITLE_PROCESSOR);
    }
}