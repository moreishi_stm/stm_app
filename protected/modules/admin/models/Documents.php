<?php

	/**
	 * This is the model class for table "documents".
	 *
	 * The followings are the available columns in table 'documents':
	 *
	 * @property integer       $id
	 * @property integer       $account_id
	 * @property integer       $document_type_id
	 * @property integer       $visibility_ma
	 * @property string        $file_name
	 * @property integer       $file_size
	 * @property string        $file_extension
	 * @property string        $description
	 * @property string        $added
	 * @property integer       $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Accounts      $account
	 * @property DocumentTypes $documentType
	 */
	class Documents extends StmBaseActiveRecord {

		const VISIBILITY_COMPONENT = 1;
		const VISIBILITY_PUBLIC = 2;
		const VISIBILITY_TEAM = 3; // only for team docs section

        const IS_S3_LOCATION = false;

        public $componentTypeId;
		public $componentId;
		public $fileReplaceFlag;

        /**
         * @var mixed file content
         */
        public $content;
        public $contentType;

		public $tempFileName; // The instance of a document

        protected $_skipSaveFile = false;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Documents the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'documents';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, document_type_id, visibility_ma',
					'required'
				),
				array(
					'account_id, document_type_id, visibility_ma, file_size, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'file_name',
					'length',
					'max' => 127
				),
				array(
					'file_extension',
					'length',
					'max' => 7
				),
				array(
					'description',
					'length',
					'max' => 255
				),
				array(
					'componentTypeId, componentId, added, fileReplaceFlag, content, contentType',
					'safe'
				),
				array(
					'file_name',
					'required',
					'on' => 'create'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, componentTypeId, componentId, document_type_id, visibility_ma, file_name, file_size, file_extension, description, added, is_deleted, content, contentType',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				//			'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
				'documentPermissions' => array(
					self::HAS_MANY,
					'DocumentPermissions',
					'document_id'
				),
				'documentComponentPermission' => array(
					self::HAS_ONE,
					'DocumentPermissions',
					'document_id',
					'condition' => 'permission_level_ma = :permissionLevel',
					'params' => array(':permissionLevel' => DocumentPermissions::PERMISSION_LEVEL_COMPONENT)
				),
				'documentType' => array(
					self::BELONGS_TO,
					'DocumentTypes',
					'document_type_id'
				),
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'document_type_id' => 'Type',
				'visibility_ma' => 'Sharing',
				'description' => 'Description',
				'file_name' => 'File Name',
				'file_extension' => 'File Type',
				'file_size' => 'File Size',
				'added_by' => 'Added By',
				'added' => 'Added',
				'updated_by' => 'Updated By',
				'updated' => 'Updated',
			);
		}

		/**
		 * Loads activeDataProvider with conditions and returns ready for use (ex. CGridview)
		 *
		 * @param      $recordId        integer record id to limit our search by.
		 * @param null $componentTypeId mixed optional component type id which if omitted will not alter the query in any way.
		 *
		 * @return CActiveDataProvider
		 */
		public static function getAll($componentTypeId = null, $componentId) {
			$Criteria = new CDbCriteria;

			if ($componentTypeId) {
				$Criteria->with = 'documentPermissions';
				$Criteria->together = true;
				$Criteria->condition = 'component_type_id=:component_type_id AND component_id=:component_id';
				$Criteria->params = array(
					':component_type_id' => $componentTypeId,
					':component_id' => $componentId
				);
			}

			$DocumentsProvider = new CActiveDataProvider('Documents', array(
				'criteria' => $Criteria,
                'pagination'=>array(
                    'pageSize'=> 100,
                ),
			));

			return $DocumentsProvider;
		}

		/**
		 * Returns the # of Homes Viewed for a contact. Used for displayed number in JUI Tab
		 *
		 * @param      $component_id
		 * @param null $componentTypeId
		 * @param bool $tabFormat
		 *
		 * @return string
		 */
		public static function getCount($componentId, $componentTypeId = null, $tabFormat = false) {
			$Criteria = new CDbCriteria;
			$Criteria->with = 'documentPermissions';
			$Criteria->condition = 'documentPermissions.component_type_id=:component_type_id AND documentPermissions.component_id=:component_id';
			$Criteria->params = array(
				':component_type_id' => $componentTypeId,
				':component_id' => $componentId
			);
			$count = Documents::model()->count($Criteria);
			if ($tabFormat) {
				$count = '(' . $count . ')';
			}

			return $count;
		}

		protected function beforeSave() {

			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');
				$this->added_by = Yii::app()->user->id;
			}

			$this->updated = new CDbExpression('NOW()');
			$this->updated_by = Yii::app()->user->id;

			return parent::beforeSave();
		}

		/**
		 * Actually save a copy of the file after a instance of this model is saved
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 */
		protected function afterSave() {

			/**
			 * Need to set 'isNewRecord' to false since in CActiveRecord setIsNewRecord(false) comes after this method is called
			 * @see CActiveRecord
			 */
			$this->isNewRecord = false;

			/**
			 * Save the file to the HD
			 */
            //@todo: this should come before? Also replace the old file if different or delete it if irrelevant
            if($this->scenario == 'emailLead' && !$this->_skipSaveFile) {
                $this->saveSaveContent();
            }
			elseif (!$this->_skipSaveFile) {
                $this->saveFile();
				// Throw exception
			}

			parent::afterSave();
		}

		public function saveFile() {
			$uploadedFile = CUploadedFile::getInstance($this, 'file_name');
			$docDirectory = $this->getFilePath();
			if (!$docDirectory || !$uploadedFile instanceof CUploadedFile) {
				return null;
			}

            // New Document method, only do this for select people
            //if(0) {
//            if(strpos($_SERVER['HTTP_HOST'], 'christineleeteam') !== false) {

                $this->tempFileName = $this->id.'.'.$uploadedFile->extensionName;
                $fullFilePath = strtr(':docDirPath/:fileName', array(
                    ':docDirPath' => $docDirectory,
                    ':fileName' => $this->id,
                ));

                // Initialize S3
                //@todo: Store API Key and Secret in a config somewhere, also remove from SetupCommand (It's hardcoded in there as well)
                $s3 = \Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

                // Store the file in S3
                $result = $s3->putObject(array(
                    'Bucket'                =>  (YII_DEBUG) ? 'dev.client-private.seizethemarket.com' : 'client-private.seizethemarket.com',
                    'Key'                   =>  $fullFilePath,
                    'SourceFile'            =>  $uploadedFile->tempName,
                    'ContentType'           =>  $uploadedFile->type,
                    'CacheControl'          =>  'no-cache, no-store, max-age=0, must-revalidate',
                    'ContentDisposition'    =>  'inline; filename="' . $uploadedFile->name . '"'
                ));

                if($result['ObjectURL']) {
                    $this->file_extension = $uploadedFile->extensionName;
                    $this->file_name = $uploadedFile->name;
                    $this->file_size = $uploadedFile->size;
                    $this->_skipSaveFile = true;
                    $this->save();
                }

                return true;
//            }
//            else {
//
//                // Check Directory Structure, if not exist create appropriate folder(s)
//                if (!is_dir($docDirectory)) {
//                    mkdir($docDirectory, $mode=0777, $recursive=true);
//                }
//
//                $this->tempFileName = $this->id.'.'.$uploadedFile->extensionName;
//                $fullFilePath = strtr(':docDirPath/:fileName', array(
//                    ':docDirPath' => $docDirectory,
//                    ':fileName' => $this->tempFileName,
//                ));
//
//                // Save the file
//                if ($uploadedFile->saveAs($fullFilePath)) {
//                    // Save the rest of the model attributes
//                    $this->file_extension = $uploadedFile->extensionName;
//                    $this->file_name = $uploadedFile->name;
//                    $this->file_size = $uploadedFile->size;
//                    $this->_skipSaveFile = true;
//                    return $this->save();
//                }
//
//                return false;
//            }
		}

        public function saveSaveContent()
        {
            if(empty($this->content)) {
                return false;
            }

            $docDirectory = $this->getFilePath();

//             $this->tempFileName = $this->id.'.'.$uploadedFile->extensionName;
            $this->tempFileName = 'docTemp-'. uniqid();
            $tempFileName = '/tmp' . DS . $this->tempFileName;

            if(($handle = fopen($tempFileName, "w")) !== FALSE) {

                file_put_contents($tempFileName, $this->content);
            }
            if(!file_exists($tempFileName)) {
                \Yii::log(__FILE__ . '(' . __LINE__ . '): Temp file does not exist! Investigate immediately! Filename: '.$tempFileName, \CLogger::LEVEL_ERROR);
            }

            $fullFilePath = strtr(':docDirPath/:fileName', array(
                    ':docDirPath' => $docDirectory,
                    ':fileName' => $this->id,
                ));

            // Initialize S3
            $s3 = \Aws\S3\S3Client::factory(StmFunctions::getAwsConfig());

            // Store the file in S3
            $result = $s3->putObject(array(
                    'Bucket'                =>  (YII_DEBUG) ? 'dev.client-private.seizethemarket.com' : 'client-private.seizethemarket.com',
                    'Key'                   =>  $fullFilePath,
                    'SourceFile'            =>  $tempFileName,
                    'ContentType'           =>  $this->contentType,
                    'CacheControl'          =>  'no-cache, no-store, max-age=0, must-revalidate',
                    'ContentDisposition'    =>  'inline; filename="' . $this->file_name . '"'
                ));
            if(!$result['ObjectURL']) {
                //@todo: does this indicate some fail?
                \Yii::log(__FILE__ . '(' . __LINE__ . '): S3 Put Object result missing Object URL. This may indicate some type of failure. Investigate Immediately!! Document ID#: '.$this->id.'. S3 Result Data: ' . print_r($result, true), \CLogger::LEVEL_ERROR);
            }
            else {
                $this->file_size = filesize($tempFileName);
                $this->_skipSaveFile = true;
                $this->save();
            }

            return true;
        }

        public function getFilePath() {

            // New Document method, only do this for select people
            //if(0) {
//            if($this->scenario=='emailLead' || strpos($_SERVER['HTTP_HOST'], 'christineleeteam') !== false) {

                $subdirectory = $this->getSubdirectoryName();
                if (!$subdirectory) {
                    return null;
                }

                $fullFilePath = strtr(':clientId/:componentName/:componentId', array(
                    ':clientId'     =>  Yii::app()->user->clientId,
                    ':componentName' => $subdirectory,
                    ':componentId' => $this->componentId,
                ));
//            }
//            else {
//                $docsBaseFilePath = Yii::app()->user->getDocsBaseFilePath();
//                $subdirectory = $this->getSubdirectoryName();
//                if (!$docsBaseFilePath || !$subdirectory) {
//                    return null;
//                }
//
//                $fullFilePath = strtr(':docBasePath/:componentName/:componentId', array(
//                    ':docBasePath' => $docsBaseFilePath,
//                    ':componentName' => $subdirectory,
//                    ':componentId' => $this->componentId,
//                ));
//            }

            return $fullFilePath;
        }

		public function getSubdirectoryName() {
			// if there is not componentTypeId loaded, see if the model is a component visibility is component level and find the related permission for component info
			// @todo Needs more clarification CW
			if (empty($this->componentTypeId)) {
				$documentPermission = $this->documentComponentPermission;
				if ($documentPermission) {
					$this->componentTypeId = $documentPermission->component_type_id;
					$this->componentId = $documentPermission->component_id;
				}
			}

			/** @var ComponentTypes $componentTypeEntry */
			$componentTypeEntry = ComponentTypes::model()->findByPk($this->componentTypeId);
			return $componentTypeEntry->getComponentName();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('document_type_id', $this->document_type_id);
			$criteria->compare('visibility_ma', $this->visibility_ma);

			$criteria->compare('description', $this->description, true);
			$criteria->compare('file_name', $this->file_name, true);

			$criteria->compare('file_size', $this->file_size);
			$criteria->compare('file_extension', $this->file_extension, true);
			$criteria->compare('added', $this->added, true);
			$criteria->compare('visibility_ma', $this->visibility_ma);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=>50,
                ),
            ));
		}
	}
