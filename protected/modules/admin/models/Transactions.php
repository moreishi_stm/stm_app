<?php
	Yii::import('admin_module.models.interfaces.Component');
	/**
	 * This is the model class for table "transactions".
	 *
	 * The followings are the available columns in table 'transactions':
	 *
	 * @property integer                   $id
     * @property integer                   $account_id
	 * @property integer                   $contact_id
	 * @property integer                   $component_type_id
	 * @property integer                   $transaction_status_id
	 * @property integer                   $source_id
     * @property integer                   $email_frequency_ma
	 * @property integer                   $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Closings[]                $closings
	 * @property TransactionClosingCosts[] $transactionClosingCosts
	 * @property TransactionFieldValues[]  $transactionFieldValues
	 * @property Contacts                  $contact
	 * @property Sources                   $source
	 * @property TransactionStatus         $transactionStatus
	 */

	class Transactions extends StmBaseActiveRecord implements Component {

        const ORIGIN_TYPE_MANUAL = 'Manual';
        const ORIGIN_TYPE_WEB = 'Web';
        const ORIGIN_TYPE_DIALER_IMPORT = 'Dialer Import';
        const ORIGIN_TYPE_GENERAL_IMPORT = 'General Import';
        const ORIGIN_TYPE_3RD_PARTY = '3rd Party';
		const ORIGIN_TYPE_NOTE_IMPORT = 'Notes Import';

        const CONSULTATION_STATUS_NOT_YET = 0;
		const CONSULTATION_STATUS_SET = 1;
		const CONSULTATION_STATUS_MET = 2;
		const CONSULTATION_STATUS_CANCELLED = 3;

		const CONSULTATION_RESULT_SIGNED = 1;
		const CONSULTATION_RESULT_AGENT_REJECT = 2;
		const CONSULTATION_RESULT_WENT_DIFF_AGENT = 3;
		const CONSULTATION_RESULT_NO_RESPONSE = 4;
		const CONSULTATION_RESULT_NOT_SELL_BUY = 5;
		const CONSULTATION_RESULT_LOW_VALUE = 6;
        const CONSULTATION_RESULT_STILL_DECIDING = 7;

        const SALE_TYPE_NORMAL_EQUITY = 1;
		const SALE_TYPE_NEW_CONSTRUCTION = 2;
		const SALE_TYPE_SHORT_SALE = 3;
		const SALE_TYPE_REO = 4;

		const BUYERS = 'buyers';
		const SELLERS = 'sellers';
		const LISTINGS = 'listings';

        const EMAIL_FREQUENCY_NONE = 0;
        const EMAIL_FREQUENCY_DAILY = 1;
        const EMAIL_FREQUENCY_WEEKLY_SUNDAY = 2;

		const FOLLOW_UP_FLAG = '<span class="follow-up-flag">Action Required!</span>';

        public $dateSearchTypes = array(
            'added_desc' => 'Added: newest first',
            'added_asc' => 'Added: oldest first',
            'last_login_desc' => 'Last Login: newest first',
            'last_login_asc' => 'Last Login: oldest first',
            'last_activity_desc' => 'Last Activity: newest first',
            'last_activity_asc' => 'Last Activity: oldest first',
            //'next_task' => 'Next Task',
        );

		private static $transactionTypesById = array(
			2 => self::BUYERS,
			3 => self::SELLERS,
		);

		private static $typeIdsByTransactionType = array(
			self::BUYERS => 2,
			self::SELLERS => 3,
		);

		private $_assignment;

		/** @var $_iResubmit can be used to determine if the transaction has already be submitted */
		private $_isResubmit = null;

		/** @var $_resubmitTransaction the existing transaction based on the $this->getIsResubmit */
		private $_resubmitTransaction = null;

		public $tempContact; // Temporary storage for a contact
		public $transactionFieldData = array(); // Collection of Transaction Field Values
		public $transactionAssignments = array(); // Collection of Assignment models
		public $transactionAddress; // Stores the address for storage

		// Search properties
		public $assignmentId;
		public $addressSearch;
        public $lastActivityDaysSearch;
        public $lastActivityDaysSearchQueryValue;
        public $activityLogQueryDate;

        public $searchNoActivity;
        public $searchNoEmails;
        public $searchNoCalls;
        public $searchNoSavedSearches;
        public $dateSearchFilter;
        public $hasPhone;
        public $includeTrash;

        public $activityPhoneCount;
        public $taskPhoneCount;

        public $mlsSearch;
		public $volume;

		public $transactionType;
        public $leadCount; // for db result field, leads tracking report
		public $leadDate; // for db result field, leads tracking report
		public $pageNumber; // for pagination jump to field
		public $addedFrom;
		public $addedTo;

        public $fromSearchDate;
        public $toSearchDate;

        protected $_lastActivityDays;
        protected $_originalModel;

		public static function newInstance($transactionType, $scenario = 'insert') {
			$transaction = null;

			switch ($transactionType) {
				case self::BUYERS:
					$transaction = new Buyers($scenario);
					$transaction->unsetAttributes();
					break;
				case self::SELLERS:
					$transaction = new Sellers($scenario);
					$transaction->unsetAttributes();
					break;
				case self::LISTINGS:
					$transaction = new Sellers($scenario);
					$transaction->unsetAttributes();
					$transaction->transaction_status_id = TransactionStatus::ACTIVE_LISTING_ID;
					break;
				default:
					throw new Exception('Invalid Transaction Type: ' . $transactionType);
					break;
			}

			return $transaction;
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'transactions';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				// array('consultation_datetime, target_date', 'default', 'setOnEmpty' => true),
				array(
					'account_id, contact_id, source_id, transaction_status_id',
					'required',
					'on' => 'buyerForm'
				),
				array(
					'account_id, contact_id, source_id, transaction_status_id',
					'required',
					'on' => 'sellerForm'
				),
                array(
                    'transaction_status_id',
                    'validateClosingStatusHasClosing',
                    'on' => 'buyerForm, sellerForm'
                ),
                array(
					'id, account_id, contact_id, component_type_id, transaction_status_id, address_id, source_id, email_frequency_ma, internal_referrer_id, internal_referrer_assignment_type_id, mls_property_type_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'notes',
					'length',
					'max' => 1500
				),
				array(
					'origin_type, origin_url, notes,volume,addedFrom,addedTo,added,dateSearchFilter,fromSearchDate,toSearchDate,hasPhone,lastActivityDaysSearch,lastActivityDaysSearchQueryValue,activityLogQueryDate,includeTrash,searchNoActivity,dateSearchFilter,searchNoEmails,searchNoCalls,searchNoSavedSearches,taskPhoneCount,activityPhoneCount',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'
						id,
						account_id,
						component_type_id,
						transaction_status_id,
						contact_id,
						source_id,
						origin_type,
						origin_url,
						email_frequency_ma,
						mls_property_type_id,
						address_id,
						internal_referrer_id,
						internal_referrer_assignment_type_id,
						notes,
						assignmentId
						addressSearch,
						mlsSearch,
						added,
						lastActivityDaysSearch,
						activityLogQueryDate,
						fromSearchDate,
						toSearchDate,
						dateSearchFilter,
						searchNoActivity,
						searchNoEmails,
						searchNoCalls,
						searchNoSavedSearches,
						includeTrash,
						taskPhoneCount,
						activityPhoneCount,
						is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

        protected function instantiate($attributes) {
            $transactionType = self::$transactionTypesById[$attributes['component_type_id']];

            return self::newInstance($transactionType, null);
        }

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			$componentTypeId = self::$typeIdsByTransactionType[$this->transactionType];

			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'assignmentTypes' => array(
					self::MANY_MANY,
					'AssignmentTypes',
					'assignments(component_id, assignment_type_id)'
				),
				'buyerAssignments' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'buyerAssignments.component_type_id = :component_type_id',
					'params' => array(':component_type_id' => ComponentTypes::BUYERS),
				),
				'sellerAssignments' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'buyerAssignments.component_type_id = :component_type_id',
					'params' => array(':component_type_id' => ComponentTypes::SELLERS),
				),
				'buyerSellerAssignments' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'buyerSellerAssignments.component_type_id IN (' . ComponentTypes::SELLERS . ',' . ComponentTypes::BUYERS . ')',
				),
                'transactionTagLu' => array(
                    self::HAS_MANY,
                    'TransactionTagLu',
                    'transaction_id',
                ),
                'transactionTags' => array(
                    self::MANY_MANY,
                    'TransactionTags',
                    'transaction_tag_lu(transaction_id, transaction_tag_id)'
                ),
                'account' => array(
                    self::BELONGS_TO,
                    'Accounts',
                    'account_id'
                ),
                'assignments' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'assignments.component_type_id = :component_type_id',
					'params' => array(':component_type_id' => $componentTypeId)
				),
                'nonLenderAssignments' => array(
                    self::HAS_MANY,
                    'Assignments',
                    'component_id',
                    'condition' => 'nonLenderAssignments.component_type_id = :component_type_id AND nonLenderAssignments.assignment_type_id<>:assignment_type_id',
                    'params' => array(':component_type_id' => $componentTypeId,':assignment_type_id'=>AssignmentTypes::LOAN_OFFICER)
                ),
				//temporary single return version @todo: remove after assignments refactor
				'assignment_single' => array(
					self::HAS_ONE,
					'Assignments',
					'component_id',
					'condition' => 'assignment_single.component_type_id = :component_type_id',
					'params' => array(':component_type_id' => $componentTypeId)
				),
                'assignmentLender' => array(
                    self::HAS_ONE,
                    'Assignments',
                    'component_id',
                    'condition' => 'assignment_type_id=:assignment_type_id AND assignmentLender.component_type_id = :component_type_id',
                    'params' => array(':assignment_type_id'=>AssignmentTypes::LOAN_OFFICER,':component_type_id' => $componentTypeId),
                ),
                'addresses' => array(
					self::MANY_MANY,
					'Addresses',
					'transaction_address_lu(transaction_id, address_id)',
					'alias' => 'addressLu'
				),
				'address' => array(
					self::BELONGS_TO,
					'Addresses',
					'address_id'
				),
				// 'primaryAssignment' => array(self::BELONGS_TO, 'Assignments', 'primary_assignment_id'),
				'appointment' => array(
					self::HAS_ONE,
					'Appointments',
					'component_id',
					'condition' => 'component_type_id = :component_type_id AND is_deleted=0',
					'params' => array(':component_type_id' => $componentTypeId)
				),
                'activityLogs' => array(
                    self::HAS_MANY,
                    'ActivityLog',
                    'component_id',
                    'condition' => 't.component_type_id = :component_type_id',
                    'params' => array(':component_type_id' => $componentTypeId)
                ),
                'opportunity' => array(
                    self::HAS_ONE,
                    'Opportunities',
                    'component_id',
                    'condition' => 'component_type_id = :component_type_id',
                    'params' => array(':component_type_id' => $componentTypeId)
                ),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id'
				),
				'actionPlans' => array(
					self::MANY_MANY,
					'ActionPlans',
					'action_plan_applied_lu(component_id, action_plan_id)',
					'condition' => 'component_type_id = :component_type_id',
					'params' => array(':component_type_id' => $componentTypeId)
				),
				'transactionFieldValues' => array(
					self::HAS_MANY,
					'TransactionFieldValues',
					'transaction_id'
				),
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id',
				),
				'cms' => array(
					self::MANY_MANY,
					'CmsContents',
					'transaction_cms_lu(transaction_id,cms_id)'
				),
				'internalReferrer' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'internalReferrerRole' => array(
					self::BELONGS_TO,
					'AssignmentTypes',
					'internal_referrer_assignment_type_id'
				),
				// t.Fixes in dashboard but... breaks other areas
				'source' => array(
					self::BELONGS_TO,
					'Sources',
					'source_id'
				),
				'status' => array(
					self::BELONGS_TO,
					'TransactionStatus',
					'transaction_status_id'
				),
				'closing' => array(
					self::HAS_ONE,
					'Closings',
					'transaction_id',
				),
                'nonDeadClosing' => array(
                    self::HAS_ONE,
                    'Closings',
                    'transaction_id',
                    'condition' => 'closing_status_ma <> :closing_status_ma',
                    'params' => array(':closing_status_ma' => Closings::STATUS_TYPE_FALLOUT_ID)
                ),
				// 'transactionClosingCosts' => array(self::HAS_MANY, 'TransactionClosingCosts', 'transaction_id'),
			);
		}

		public function getComponentTypeId() {
			if (!self::$typeIdsByTransactionType[$this->transactionType]) {
				return null;
			}

			$componentTypeId = self::$typeIdsByTransactionType[$this->transactionType];

			return $componentTypeId;
		}

		public function getComponentId() {
			return $this->id;
		}

		/**
		 * Return the first address found
		 * There should only be one record for now
		 *
		 * @return Addresses
		 */
		// public function getAddress() {
		// 	$addresses = $this->addresses;
		// 	if (!$addresses || !is_array($addresses)) {
		// 		return null;
		// 	}

		// 	return $addresses[0];
		// }

		/**
		 * Return the first assignment of a transaction.
		 *
		 * @return Assignments
		 */
		public function getAssignment() {
			if ($this->assignments) {
				$this->_assignment = $this->assignments[0];
			} else {
				if (!$this->_assignment) {
					$this->_assignment = new Assignments;
				}
			}

			return $this->_assignment;
		}

		public function setAssignment($value) {
			$this->_assignment = $value;
		}

		/**
		 * @return array behaviors for getTasks
		 */
		public function behaviors() {
			return array(
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
				'getTasks' => array(
					'class' => 'admin_module.components.behaviors.GetTasksBehavior',
				),
			);
		}

		public function defaultScope() {
			$baseDefaultScope = parent::defaultScope();

			$componentTypeId = self::$typeIdsByTransactionType[$this->transactionType];
			if (!$componentTypeId) {
				return $baseDefaultScope;
			}

			//Update default condition to include component_type_id so that we
			//maintain the proper scope for the underlying concrete implementation.
			$tableAlias = $this->getTableAlias(true, false);
			$baseDefaultScope['condition'] .= ' AND ' . $tableAlias . '.component_type_id = :component_type_id';

			$baseDefaultScope['params'] = CMap::mergeArray($baseDefaultScope['params'], array(
					':component_type_id' => $componentTypeId

				)
			);

			return $baseDefaultScope;
		}

        public function scopes() {
            return array(
                'notTrash'=>array('condition'=>'transaction_status_id<>'.TransactionStatus::TRASH_ID),
                'byIdAsc'=>array('order'=>'id ASC')
            );
        }

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'address_id' => 'Address',
				'component_type_id' => 'Type',
				'transaction_status_id' => 'Status',
				'source_id' => 'Source',
				'mls_property_type_id' => 'Property Type',
				'internal_referrer_id' => 'Internal Referrer',
				'notes' => 'Notes',
                'email_frequency_ma' => 'Email Frequency',
				'updated' => 'Updated',
				'updated_by' => 'Updated By',
				'added' => 'Added',
				'added_by' => 'Added By',
				'is_deleted' => 'Is Deleted',
			);
		}

        public function validateClosingStatusHasClosing($attribute, $params)
        {
            if ($this->transaction_status_id == TransactionStatus::CLOSED_ID) {
                if(empty($this->closing)) {
                    $this->addError($attribute, 'There must be a closing associated with this transaction to mark it closed.');
                }
            }
        }

        public function getFieldValue($name, $defaultValue = null) {
			if (!$this->transactionFieldData) {
				// Cache the field value data for a particular transaction
				if ($this->transactionFieldValues != '') {
					foreach ($this->transactionFieldValues as $FieldValue) {
						$this->transactionFieldData[$FieldValue->fieldName] = $FieldValue->value;
					}
				}
			}

			return ($this->transactionFieldData[$name] != '') ? $this->transactionFieldData[$name] : $defaultValue;
		}

		public function getLastActivityLog() {
			$Criteria = new CDbCriteria;
			$Criteria->condition = '(t.component_type_id=:component_type_id AND t.component_id=:component_id) OR (tasks.complete_date > 0 AND t.component_type_id='.ComponentTypes::TASKS.')';
			$Criteria->params = array(
				':component_type_id' => $this->component_type_id,
				':component_id' => $this->id
			);
            $Criteria->join = 'LEFT JOIN tasks ON t.component_id=tasks.id AND tasks.component_id='.$this->id;
			$Criteria->order = 't.activity_date desc';
            $Criteria->addInCondition('t.task_type_id', array(TaskTypes::PHONE, TaskTypes::TODO, TaskTypes::NOTE, TaskTypes::EMAIL_MANUAL));

            $activityLog = new ActivityLog;

            return $activityLog->find($Criteria);
		}

		public function getLastActivityDays() {
            if($this->_lastActivityDays !== null) {
                return $this->_lastActivityDays;
            }

			$LastActivity = $this->lastActivityLog;

			if ($LastActivity == null) {
				return 9999999999;
			}

			return $this->_lastActivityDays = Yii::app()->format->formatDays($this->activity_date, array('printDays' => false));
		}

		public function getNextTask() {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'component_type_id=:component_type_id AND component_id=:component_id AND due_date>=:due_date AND due_date<=:due_date_max';
			$Criteria->params = array(
				':component_type_id' => $this->component_type_id,
				':component_id' => $this->id,
				':due_date' => date("Y-m-d H:i:s"),
				':due_date_max' => date("Y-m-d H:i:s", strtotime('30 days'))
			);
			$Criteria->order = 'due_date asc';

			return Tasks::model()->find($Criteria);
		}

		/**
		 * getSaleTypes
		 * Returns the Sale Types from TransactionFieldValues
		 *
		 * @return string
		 */
		public static function getSaleTypes() {
			return array(
				self::SALE_TYPE_NORMAL_EQUITY => 'Normal Equity',
				self::SALE_TYPE_NEW_CONSTRUCTION => 'New Construction',
				self::SALE_TYPE_SHORT_SALE => 'Short Sale',
				self::SALE_TYPE_REO => 'REO'
			);
		}

        /**
         * getSaleTypes
         * Returns the Sale Types from TransactionFieldValues
         *
         * @return string
         */
        public static function getEmailFrequencyTypes() {
            return array(
                self::EMAIL_FREQUENCY_NONE => 'None',
                self::EMAIL_FREQUENCY_DAILY => 'Daily',
                self::EMAIL_FREQUENCY_WEEKLY_SUNDAY => 'Weekly (Sunday)',
            );
        }

		/**
		 * If a sale type is not passed this will attempt to get the sale type associated with a instance of the model
		 *
		 * @param constant $saleType a indexed constant defined from within this model
		 *
		 * @return string Represents the sale type
		 */
		public function getSaleType($saleType = null) {
			if ($saleType === null && $this->sale_type_ma) {
				$saleType = $this->sale_type_ma;
			}

			return (isset($this->saleTypes[$saleType])) ? $this->saleTypes[$saleType] : null;
		}

		/**
		 * getConsultationMetStatus
		 * Returns the Consultation Status Types
		 *
		 * @return string
		 */
		public static function getConsultationMetStatusTypes() {
			return array(
				self::CONSULTATION_STATUS_NOT_YET => 'Not Set',
				self::CONSULTATION_STATUS_SET => 'Set',
				self::CONSULTATION_STATUS_MET => 'Met',
				self::CONSULTATION_STATUS_CANCELLED => 'Cancelled'
			);
		}

		/**
		 * getConsultationApptResult
		 * Returns the Consultation Status Types
		 *
		 * @return string
		 */
		public static function getConsultationApptResult() {
			return array(
				self::CONSULTATION_RESULT_SIGNED => 'Signed Agreement',
				self::CONSULTATION_RESULT_AGENT_REJECT => 'Agent Reject Client',
				self::CONSULTATION_RESULT_WENT_DIFF_AGENT => 'Went w/Different Agent',
				self::CONSULTATION_RESULT_NO_RESPONSE => 'No Response',
				self::CONSULTATION_RESULT_NOT_SELL_BUY => 'Decided Not to Buy/Sell',
				self::CONSULTATION_RESULT_LOW_VALUE => 'Value too Low',
				self::CONSULTATION_RESULT_STILL_DECIDING => 'Still Deciding',
			);
		}

		public function getBuyerPriceRange() {
			if ($priceMin = $this->getFieldValue('price_min') || $priceMax = $this->getFieldValue('price_max')) {
				$minPriceGridValue = Yii::app()->format->formatGridValue("", Yii::app()->format->formatDollars($this->getFieldValue("price_min"), 'Unspecified'));

				//$maxPriceGridValue will contain the hyphen range delimiter.
				$maxPriceGridValue = Yii::app()->format->formatGridValue(" - ", Yii::app()->format->formatDollars($this->getFieldValue("price_max"), 'Unspecified'), false);

				return $minPriceGridValue . $maxPriceGridValue;
			} else if ($this->contact and Yii::app()->getUser()->getBoard()) {

				return $this->contact->getAveragePriceViewed();
			}
		}

        protected function beforeValidate()
        {

            if ($this->isNewRecord) {
                if(empty($this->account_id)) {
                    $this->account_id = Yii::app()->user->getAccountId();
                }
            }
            else {
                $this->_originalModel = Transactions::model()->findByPk($this->id);
            }

            return parent::beforeValidate();
        }

        protected function beforeSave()
        {
			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');
				$this->added_by = Yii::app()->user->id;

                if (empty($this->account_id)) {
                    $this->account_id = Yii::app()->user->getAccountId();
                }

                //@todo: add logic for checking for dup double click saves?, anything that has been added within same hour or day?
			}

			return parent::beforeSave();
		}

        protected function afterSave()
        {
            // if lead status changed, log it
            if($this->_originalModel->transaction_status_id !== $this->transaction_status_id && !empty($this->_originalModel)) {

                $activityLog = new ActivityLog();
                $activityLog->setAttributes(array(
                        'component_type_id' => $this->component_type_id,
                        'component_id' => $this->id,
                        'task_type_id' => TaskTypes::SYSTEM_NOTE,
                        'activity_date' => new CDbExpression('NOW()'),
                        'note' => 'Status updated to '.$this->status->name.' from '.$this->_originalModel->status->name.'.',
                    ));

                if(!$activityLog->save()) {
                    // error log
                    Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: Transaction status change log did not save. Error Message '.print_r($activityLog->getErrors(), true).PHP_EOL.'Original Data: '.print_r($this->_originalModel, true).PHP_EOL.'Updated Data: '.print_r($this->attributes, true), \CLogger::LEVEL_ERROR);
                }

                // apply any triggers whe status has changed
                $this->_applyTriggers();
            }

            parent::afterSave();
        }

        protected function _applyTriggers()
        {
            // if original data is empty or same as previous one, do not apply triggers, ignore NEW dialer imports
            if(($this->origin_type==Transactions::ORIGIN_TYPE_DIALER_IMPORT && empty($this->_originalModel)) || empty($this->_originalModel) || $this->_originalModel->transaction_status_id == $this->transaction_status_id) {
                return;
            }

            // find any triggers that
            if(!empty($actionPlanIdsToApply = Yii::app()->db->createCommand("SELECT action_plan_id FROM triggers WHERE status_ma=1 AND component_type_id=".$this->component_type_id." AND action='".Triggers::ACTION_APPLY_ACTION_PLAN."' AND event IN('".Triggers::EVENT_SELLER_STATUS_UPDATE."','".Triggers::EVENT_BUYER_STATUS_UPDATE."') AND event_data='".$this->transaction_status_id."'")->queryColumn())) {

                $criteria = new CDbCriteria();
                $criteria->addInCondition('id', $actionPlanIdsToApply);
                $actionPlansToApply = ActionPlans::model()->findAll($criteria);

                foreach($actionPlansToApply as $actionPlanToApply) {

                    $actionPlanToApply->apply($this, null, 'Auto-triggered by Lead Status Update.');
                    //@todo: what to do if could not apply due to some error? - notify user?
//                    $validation = StmActiveForm::validate($actionPlanToApply, null, null, $clearErrors=false);
                }
            }

            if(!empty($actionPlanIdsToUnapply = Yii::app()->db->createCommand("SELECT action_plan_id FROM triggers WHERE status_ma=1 AND component_type_id=".$this->component_type_id." AND action='".Triggers::ACTION_UNAPPLY_ACTION_PLAN."' AND event IN('".Triggers::EVENT_SELLER_STATUS_UPDATE."','".Triggers::EVENT_BUYER_STATUS_UPDATE."') AND event_data='".$this->transaction_status_id."'")->queryColumn())) {

                $criteria = new CDbCriteria();
                $criteria->addInCondition('id', $actionPlanIdsToUnapply);
                $actionPlansToUnapply = ActionPlans::model()->findAll($criteria);
                foreach($actionPlansToUnapply as $actionPlanToUnapply) {

                    $actionPlanToUnapply->unapply($this, 'Auto-triggered by Lead Status Update.');
                }
            }
        }

        public function excludeIds($ids) {
			if(!is_array($ids)) {
				$ids = array($ids);
			}
			$this->getDbCriteria()->addNotInCondition('id', $ids);

			return $this;
		}

		public function byStatusIds($ids) {
			if(!is_array($ids)) {
				$ids = array($ids);
			}
			$this->getDbCriteria()->addInCondition('transaction_status_id', $ids);

			return $this;
		}

        public function byNotStatusIds($ids) {
            if(!is_array($ids)) {
                $ids = array($ids);
            }
            $this->getDbCriteria()->addNotInCondition('transaction_status_id', $ids);

            return $this;
        }

		/**
		 * /Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;
			$criteria->compare('t.id', $this->id);
			$criteria->compare('t.contact_id', $this->contact_id);

            //these 2 settings are very important. Do not change.
            $criteria->together = true;
            $criteria->with = CMap::mergeArray($criteria->with, array('contact'));

//            if(1) { //add more logic for showing current or all accounts
//                $criteria->with = CMap::mergeArray($criteria->with, array('contact'));
//                $criteria->compare('contact.account_id', Yii::app()->user->accountId);
//                $criteria->together = true;
//            }

			$criteria->compare('t.component_type_id', $this->getComponentTypeId());

			if (!$this->transaction_status_id && !$this->includeTrash) {
				$criteria->compare('t.transaction_status_id', '<>' . TransactionStatus::TRASH_ID);
			} else {
				if(is_array($this->transaction_status_id)) {
					$criteria->addInCondition('t.transaction_status_id', $this->transaction_status_id);
				} else {
					$criteria->compare('t.transaction_status_id', $this->transaction_status_id);
				}
			}

			$criteria->compare('t.mls_property_type_id', $this->mls_property_type_id);
			$criteria->compare('t.internal_referrer_id', $this->internal_referrer_id);

			if(is_array($this->source_id)) {
				$criteria->addInCondition('t.source_id', $this->source_id);
			} else {
				$criteria->compare('t.source_id', $this->source_id);
			}

			$criteria->compare('t.is_deleted', $this->is_deleted);

            if ($this->dateSearchFilter and $this->dateSearchTypes[$this->dateSearchFilter]) {
                $fromSearchDate = Yii::app()->format->formatDate($this->fromSearchDate, StmFormatter::MYSQL_DATETIME_FORMAT);
                $toSearchDate = Yii::app()->format->formatDate($this->toSearchDate, StmFormatter::MYSQL_DATETIME_FORMAT);

                // @todo the idea here will be to create a filter that returns a cdbcriteria that can be merged here
                // for the time being just use a switch statement
                switch($this->dateSearchFilter) {
                    case 'last_login_asc':
                    case 'last_login_desc':
                        if ($fromSearchDate && $toSearchDate) {
                            $criteria->addBetweenCondition('DATE(contact.last_login)', $fromSearchDate, $toSearchDate);
                        }
                        $sortOrder = ($this->dateSearchFilter == 'last_login_asc')? 'asc' : 'desc';
                        $criteria->order = "contact.last_login $sortOrder";
                    break;

                    case 'added_asc':
                    case 'added_desc':
                        if ($fromSearchDate && $toSearchDate) {
                            $criteria->addBetweenCondition('DATE(t.added)', $fromSearchDate, $toSearchDate);
                        }
                        $sortOrder = ($this->dateSearchFilter == 'added_asc')? 'asc' : 'desc';
                        $criteria->order = "t.added $sortOrder";
                    break;

                    case 'last_activity_asc':
                    case 'last_activity_desc':
                        $criteria->with = CMap::mergeArray($criteria->with, array('activityLogs'));
                        if ($fromSearchDate && $toSearchDate) {
                            $criteria->addBetweenCondition('activityLogs.activity_date', $fromSearchDate, $toSearchDate);
                        }
                        $sortOrder = ($this->dateSearchFilter == 'last_activity_asc')? 'asc' : 'desc';
                        $criteria->order = "activityLogs.activity_date $sortOrder";
                    break;
                }
            }

			if ($this->contact->first_name || $this->contact->last_name || trim($this->contact->contactFullName)) {
				// Filter search by a contact's first name, last name, or a combination of the two.

				// Search explicitly by first name
				if ($this->contact->first_name) {
					$criteria->compare('contact.first_name', $this->contact->first_name, true);
				}

				// Search explicitly by last name
				if ($this->contact->last_name) {
					$criteria->compare('contact.last_name', $this->contact->last_name, true);
				}

				// Search any portion of either name fields
				if ($this->contact->contactFullName) {
					$criteria->compare("CONCAT(contact.first_name,' ',contact.last_name)", $this->contact->contactFullName, true);
				}
			}

			if ($this->contact->emails->email) { // Filter by a contact's e-mail address
				$criteria->with = CMap::mergeArray($criteria->with, array('contact.emails'));
				$criteria->compare('emails.email', $this->contact->emails->email, true);
			}

            if ($this->hasPhone) {
                $criteria->with = CMap::mergeArray($criteria->with, array('contact.phones'));
                $criteria->compare('phones.phone', '>0', true);
            }

			if ($this->contact->phones->phone) {
				// Filter by a contact's phone number
				$criteria->with = CMap::mergeArray($criteria->with, array('contact.phones'));

				$scrubbedPhoneNumber = Yii::app()->format->formatInteger($this->contact->phones->phone);

				$criteria->compare('phones.phone', $scrubbedPhoneNumber, true);
			}

			if ($this->addressSearch) {
				$criteria->with = CMap::mergeArray($criteria->with, array('address'));
				$criteria->compare("CONCAT(address.address,' ',address.zip)", $this->addressSearch, true);
			}

			if ($this->assignmentId) {
				$criteria->with = CMap::mergeArray($criteria->with, array('assignments'));
				$criteria->compare('assignments.assignee_contact_id', $this->assignmentId);
			}

			if ($this->lastActivityDaysSearch) {
                $applicableTaskTypes = array(TaskTypes::EMAIL_MANUAL, TaskTypes::PHONE, TaskTypes::NOTE, TaskTypes::TODO);
                $applicableTaskTypesStr = implode(',', $applicableTaskTypes);
                $criteria->join = new CDbExpression("INNER JOIN (
                    SELECT * FROM (
                         SELECT
                            t.id AS id,
                            IF(MAX(activityLogs.activity_date) IS NULL AND MAX(taskLogs.activity_date) IS NULL, 1, 0) AS has_no_activity,
                            IF(MAX(DATE(activityLogs.activity_date)) <= DATE_SUB(CURRENT_DATE, INTERVAL {$this->lastActivityDaysSearch} DAY), MAX(activityLogs.activity_date), NULL) AS max_trans_date,
                            IF(MAX(DATE(taskLogs.activity_date)) <= DATE_SUB(CURRENT_DATE, INTERVAL {$this->lastActivityDaysSearch} DAY), MAX(taskLogs.activity_date), NULL) AS max_task_date
                         FROM transactions t
                         LEFT OUTER JOIN activity_log activityLogs ON (activityLogs.component_id=t.id AND activityLogs.task_type_id IN ({$applicableTaskTypesStr}))
                         LEFT OUTER JOIN tasks task ON (task.component_id = t.id AND task.component_type_id = 2)
                         LEFT OUTER JOIN activity_log taskLogs ON (taskLogs.component_id = task.id AND taskLogs.component_type_id = 8 AND taskLogs.task_type_id IN ({$applicableTaskTypesStr}))
                         WHERE
                            t.transaction_status_id <> 13
                            AND
                            t.component_type_id = {$this->componentTypeId}
                         GROUP BY t.id
                         ) data
                        WHERE (1 = IF(data.max_trans_date IS NULL OR data.max_task_date IS NULL, 0, 1)) OR (data.has_no_activity = 1)
                    ) date ON date.id = t.id
                ");
                $criteria->order = 'IF(date.has_no_activity = 1, 1, IF(date.max_trans_date >= date.max_task_date, date.max_trans_date, date.max_task_date)) DESC';
			}

            if ($this->searchNoActivity) {
                $noActivityCriteria = $this->byNoActivity($returnCriteria=true);
                $criteria->mergeWith($noActivityCriteria);
            }

            if ($this->searchNoEmails) {
            }

            if ($this->searchNoCalls) {
//                $this->byNotCalled();

                $noCallsCriteria = new CDbCriteria;
                $noCallsCriteria->condition = '(select count(*) from activity_log activityLog WHERE (t.id=activityLog.component_id AND t.component_type_id=activityLog.component_type_id) AND (activityLog.task_type_id  IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.'))) <1 '.
                    'AND (select count(*) from tasks tasks WHERE  (tasks.task_type_id IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.')) AND tasks.component_id=t.id AND tasks.component_type_id=t.component_type_id AND tasks.complete_date IS NOT NULL) < 1';
                $criteria->mergeWith($noCallsCriteria);

            }

            if ($this->searchNoSavedSearches) {
                $noSearchCriteria = $this->byNoSavedSearches($setById=null, $returnCriteria=true);
                $criteria->mergeWith($noSearchCriteria);
            }


            if ($this->mlsSearch) {
				$criteria->with = CMap::mergeArray($criteria->with, array(
						'transactionFieldValues',
						'transactionFieldValues.transactionField'
					)
				);
				$criteria->compare('value', $this->mlsSearch, true);
				$criteria->compare('name', 'mls_num');
			}

            if ($criteria->together) {
                $criteria->group = 't.id';
            }

            /** If there is no order by this point then use the last login time for each transaction's contact */
            if (empty($criteria->order)) {
                $criteria->order = 'contact.last_login DESC';
            }

			$searchProvider = new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 25),
			));

            /** @var $count Since we are using HAS_MANY relationships here need to manually set the pagination */
            $count = $this->count($criteria);
            $searchProvider->setTotalItemCount($count);
            $searchProvider->pagination->setItemCount($count);

			return $searchProvider;
		}

		public function getContactId() {
			return $this->contact_id;
		}

		/**
		 * Returns the # of Transactions for a contact. Used for displaying in a JUI Tab
         * @todo This cannot be used until Transactions is no longer trying to do the count
		 *
		 * @param      $contactId
		 * @param bool $tabFormat true if tab format should be returned(i.e. count wrapped in parens), false otherwise
		 *                        to return the count plainly.
		 *
		 * @return mixed          the number of transactions (as an integer) or optionally wrapped in
		 *                        parenthesis if $tabFormat = true
		 */
		public static function getCount($contactId, $tabFormat = false) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_id=:contact_id';
			$Criteria->params = array(':contact_id' => $contactId);
			$count = Transactions::model()->count($Criteria);

			if ($tabFormat) {
				$count = '(' . $count . ')';
			}

			return $count;
		}

		public function getComponentChain() {
			$chain = array($this->asComponentTuple());

			$includeCompleted = true;
			$Tasks = $this->getTasks($this, $includeCompleted);

			if (count($Tasks) > 0) {
				foreach ($Tasks as $Task) {
					array_push($chain, $Task->asComponentTuple());
				}
			}

			return $chain;
		}

		public function asComponentTuple() {
			return array(
				'componentTypeId' => $this->component_type_id,
				'componentId' => $this->id
			);
		}

		public function getStatusList(){
			if(!$this->component_type_id)
				$this->component_type_id = self::$typeIdsByTransactionType[$this->transactionType];;
			return 	TransactionStatus::model()->byComponentType($this->component_type_id)->defaultSort()->findAll();
		}

		public function hasNoFutureTask($upToDate = null) {
			if (!$upToDate) {
				$upToDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +30 days'));
			}

			// check to see if there is a future task in 1 weeks of login
			return Tasks::hasNoFutureTask($this->component_type_id, $this->id, $upToDate);
		}

		public function hasNoActivity($sinceDate = null) {
			// check to see if there is no activity since last login
			return ActivityLog::hasNoActivity($this->component_type_id, $this->id, $sinceDate);
		}

		/**
		 * getNewLeadFollowUpFlag - Flags any leads that doesn't have response time required per account setting.
		 *
		 * @return boolean
		 */
		public function getNewLeadFollowUpFlag() {
			$newLeadResponseTime = Yii::app()->user->settings->response_time_new_lead;
			$sinceDate = date('Y-m-d H:i:s', strtotime($this->added . ' + ' . $newLeadResponseTime . ' minutes'));

			return $this->hasNoActivity($sinceDate);
		}

		public function getFollowUpFlag() {
			// @todo: New Lead... not following tasks... past due tasks...

			$sinceDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' -30 days'));
			$sinceDate = ($this->added > $sinceDate) ? $this->added : $sinceDate;

			// The following will trigger this flag: No activity since last login within 5 min AND no task in the next week
			if (/*$this->newLeadFollowUpFlag ||  */$this->hasNoActivity($sinceDate) || $this->hasNoFutureTask()) {
				return self::FOLLOW_UP_FLAG;
			} else {
				return null;
			}
		}

		public function byAddress($addressId) {
			if (isset($addressId)) {
				$Criteria = new CDbCriteria();
				$Criteria->condition = 'address_id = :address_id';
				$Criteria->params = array(':address_id' => $addressId);

				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}

		public function byComponentTypeId($componentTypeId) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'component_type_id=:component_type_id',
					'params' => array(':component_type_id' => $componentTypeId),
				)
			);

			return $this;
		}

		public function byContactId($contactId) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'contact_id=:contact_id',
					'params' => array(':contact_id' => $contactId),
				)
			);

			return $this;
		}

        public function byAssignedTo($contactId) {
            $tableAlias   = $this->getTableAlias(true, false);
            $Criteria = new CDbCriteria;
            $Criteria->condition = 'assignments.assignee_contact_id=:contact_id AND (assignments.is_deleted=0 OR assignments.is_deleted IS NULL)';
            $Criteria->params = array(':contact_id'=>$contactId);
            $Criteria->join = 'LEFT JOIN assignments ON '.$tableAlias.'.id=assignments.component_id AND '.$tableAlias.'.component_type_id=assignments.component_type_id';
            $Criteria->together = true;

            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

		public function getAssignedToIdsInString() {
			foreach ($this->assigments as $Assignment) {
				$string .= ($string) ? ',' . $this->contact_id : $this->contact_id;
			}

			return $string;
		}

		public function printGridviewAssignments() {
			foreach ($this->assignments as $Assignment) {
				$string .= ($string) ? '<br /><span style="width:82px; display:inline-block;"></span>' : '';
				$string .= $Assignment->contact->fullName . ' (' . $Assignment->assignmentType->display_name . ')';
			}

			return $string;
		}

		public function byDateRange($dateRange, $field) {
            $dateRange = Yii::app()->format->formatDateRangeArray($dateRange);

			$criteria = new CDbCriteria;
			$criteria->condition = 'date('.$field.') >=:from_date AND date('.$field.') <=:to_date';
			$criteria->params = array(
				":from_date" => $dateRange['from_date'],
				":to_date" => $dateRange['to_date'],
			);

			$this->getDbCriteria()->mergeWith($criteria);

			return $this;
		}

        /**
		 * is this transaction a resubmit?
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @return boolean
		 */
		public function getIsResubmit($accountId=null)
        {
			if ($this->_isResubmit===null) {

                $transactionModel = new \Transactions();
                if($accountId) {
                    $transactionModel->applyAccountScope = false;
                }
                else {
                    $accountId = Yii::app()->user->accountId;
                }
                // if this is a seller lead
                if($this->address_id || $this->transactionType == Transactions::SELLERS) {

                    $existingTransaction = $transactionModel->byIdAsc()->byNotStatusIds(\TransactionStatus::CLOSED_ID)->findAllByAttributes(array('account_id'=>$accountId,'component_type_id'=>\ComponentTypes::SELLERS, 'address_id'=>$this->address_id, 'contact_id'=>$this->contact->id));
                    if(empty($existingTransaction)) {
                        //check to see if there's any empty transactions without an address, if so, add this address to the transaction and save it.
                        $existingTransaction = $transactionModel->byNotStatusIds(\TransactionStatus::CLOSED_ID)->byIdAsc()->findAllByAttributes(array('account_id'=>$accountId,'component_type_id'=>\ComponentTypes::SELLERS, 'contact_id'=>$this->contact->id));
                        if(!empty($existingTransaction)) {
                            if(count($existingTransaction) == 1) {
                                //if existing seller transaction does not have an address, use the currently submitted one and save it.
                                if(empty($existingTransaction[0]->address_id)) {
                                    $existingTransaction[0]->address_id = $this->address_id;
                                    if(!$existingTransaction[0]->save()) {
                                        \Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: Found a resubmit seller transaction, tried to save the address and got the following error: '.print_r($existingTransaction, true), \CLogger::LEVEL_ERROR, \AbstractFormSubmitAction::LOG_CATEGORY);
                                    }
                                }
                            } elseif(count($existingTransaction) > 1) {
                                \Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: Found a duplicate resubmit seller transactions, none have a matching address_id of "'.$this->address_id.'". Figure out how to handle. All Transaction Models Returned: '.print_r($existingTransaction, true), \CLogger::LEVEL_ERROR, \AbstractFormSubmitAction::LOG_CATEGORY);
                            }
                        }
                    }
                // if this is a buyer lead
                } else {
                    $existingTransaction = $transactionModel->byIdAsc()->byNotStatusIds(\TransactionStatus::CLOSED_ID)->findAllByAttributes(array('account_id'=>$accountId,'component_type_id'=>\ComponentTypes::BUYERS, 'contact_id'=>$this->contact->id));
                }

                if(count($existingTransaction) == 1) {
                    $existingTransaction = $existingTransaction[0];
                } elseif(count($existingTransaction) > 1) {
                    \Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: Duplicate transactions detected on resubmit. Figure out how to handle. All Transaction Models Returned: '.print_r($existingTransaction->attributes, true), \CLogger::LEVEL_ERROR);
                    $existingTransaction = $existingTransaction[0];
                    //@todo: logic needed here if more than 1 transaction found
                    //@todo: Need to email agent/team to let them know about duplicate addresses found?
                }

                if($existingTransaction->transaction_status_id == \TransactionStatus::TRASH_ID || $existingTransaction->transaction_status_id == \TransactionStatus::CANCELLED_ID || $existingTransaction->transaction_status_id == \TransactionStatus::EXPIRED_ID) {
                    $oldStatus = $existingTransaction->status->name;
                    $existingTransaction->transaction_status_id = \TransactionStatus::NURTURING_ID;
                    if($existingTransaction->save()) {

                        $transactionExistingLog = new \ActivityLog('leadGenCreation');
                        $transactionExistingLog->account_id = \Yii::app()->user->accountId;
                        $transactionExistingLog->activity_date = new \CDbExpression('NOW()');
                        $transactionExistingLog->component_id = $existingTransaction->id;
                        $transactionExistingLog->component_type_id = $existingTransaction->component_type_id;
                        $transactionExistingLog->task_type_id = \TaskTypes::SYSTEM_NOTE;
                        $transactionExistingLog->note = 'Re-submit: Status was changed from '.$oldStatus.' to Nurturing.';
                        if(!$transactionExistingLog->save()) {
                            \Yii::log(__CLASS__.' ('.__LINE__.'): ERROR: Could not log the change of status for a Resubmit trash/expired/canceled lead to Nurturing. '.print_r($transactionExistingLog->getErrors(), true), \CLogger::LEVEL_ERROR);
                        }
                    }
                }

				$this->_isResubmit = !empty($existingTransaction);

				if ($this->_isResubmit) {
					$this->setResubmitTransaction($existingTransaction);
				}
			}

            // Return the existing transaction if this is a re-submit, if not, just return false
			return $this->_isResubmit ? $existingTransaction : false;
		}

		/**
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @return transaction
		 */
		public function getResubmitTransaction() {
			return $this->_resubmitTransaction;
		}

		/**
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @param Transactions $resubmitTransaction
		 */
		public function setResubmitTransaction(Transactions $resubmitTransaction) {
			$this->_resubmitTransaction = $resubmitTransaction;
		}

        public function bySpokeTo($together=false) {
            $this->byCalled($spokeTo=true, $together);
            return $this;
        }

        public function byNoSavedSearches($setById=null, $returnCriteria=false)
        {
            //@todo: $setById not used for now
            $Criteria = new CDbCriteria;
//            $Criteria->with = array('contact.savedHomeSearches');
//                $Criteria->with = array('contact.savedHomeSearches'=>array('joinType' => 'LEFT JOIN')); //, 'alias'=>'savedHomeSearches' ... 'contact',
//            $Criteria->with = array('contact'=>array('alias'=>'contact', 'joinType'=>'LEFT JOIN')); //, 'contact.savedHomeSearches'=>array('joinType' => 'LEFT JOIN')
//            $Criteria->join = 'LEFT JOIN saved_home_searches savedHomeSearches ON contact.id=savedHomeSearches.contact_id';
            $Criteria->with = array('contact.savedHomeSearches'=>array('alias'=>'savedHomeSearches', 'joinType'=>'LEFT JOIN')); //, 'contact.savedHomeSearches'=>array('joinType' => 'LEFT JOIN')

//            $Criteria->join = 'LEFT JOIN `contacts` `contact` ON (`t`.`contact_id`=`contact`.`id`) AND ((`contact`.is_deleted = 0 or `contact`.is_deleted IS NULL))  LEFT JOIN `saved_home_searches` `savedHomeSearches` ON (`savedHomeSearches`.`contact_id`=`contact`.`id`) AND ((`savedHomeSearches`.is_deleted = 0 or `savedHomeSearches`.is_deleted IS NULL)) ';

            $Criteria->addCondition('savedHomeSearches.id IS NULL');

            if($returnCriteria) {
                return $Criteria;
            } else {
//                $Criteria->together = true;//set this at top
                $this->getDbCriteria()->mergeWith($Criteria);
                return $this;
            }
        }

        public function byCalled($spokeTo=false, $together=false)
        {
            $tableAlias   = $this->getTableAlias(true, false);

            $Criteria = new CDbCriteria;
            $Criteria->select = 'distinct(activityLog.id)';
            $Criteria->join = 'LEFT JOIN activity_log activityLog ON '.$tableAlias.'.id=activityLog.component_id AND '.$tableAlias.'.component_type_id=activityLog.component_type_id';
            $Criteria->condition = '(activityLog.task_type_id=:task_type_id OR activityLog.task_type_id=:task_type_bad_phone)';
            $Criteria->params = array(':task_type_id'=>TaskTypes::PHONE, ':task_type_bad_phone'=>TaskTypes::BAD_PHONE);
            $Criteria->group = 't.id';

            if($spokeTo == true) {
                $Criteria->join .= ' AND activityLog.is_spoke_to=1';
            }

            if($together==true) {
                $Criteria->together = true;
            }

            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

        public function byNotCalled($returnCriteria=false)
        {
            $Criteria = new CDbCriteria;
            $Criteria->condition = '(select count(*) from activity_log activityLog WHERE (t.id=activityLog.component_id AND t.component_type_id=activityLog.component_type_id) AND (activityLog.task_type_id  IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.'))) <1 '.
                'AND (select count(*) from tasks tasks WHERE (tasks.task_type_id IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.')) AND tasks.component_id=t.id AND tasks.component_type_id=t.component_type_id AND tasks.complete_date IS NOT NULL) < 1';

            if($returnCriteria) {
                return $Criteria;
            } else {
                $Criteria->together = true;
                $this->getDbCriteria()->mergeWith($Criteria);
                return $this;
            }
        }

        public function byNoActivity($returnCriteria=false)
        {
            $Criteria = new CDbCriteria;
            $Criteria->condition = '(select count(*) from activity_log activityLog WHERE (t.id=activityLog.component_id AND t.component_type_id=activityLog.component_type_id) AND (activityLog.task_type_id IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.','.TaskTypes::TODO.','.TaskTypes::NOTE.','.TaskTypes::TEXT_MESSAGE.'))) <1 '.
                'AND (select count(*) from activity_log activityLog LEFT JOIN action_plan_applied_lu actionPlanAppliedLu ON (actionPlanAppliedLu.action_plan_item_id=activityLog.id) WHERE (t.id=activityLog.component_id AND t.component_type_id=activityLog.component_type_id) AND (activityLog.task_type_id IN ('.TaskTypes::EMAIL_MANUAL.')) AND actionPlanAppliedLu.action_plan_item_id IS NULL) <1 '. //
                'AND (select count(*) from tasks tasks WHERE (tasks.task_type_id IN ('.TaskTypes::PHONE.','.TaskTypes::BAD_PHONE.','.TaskTypes::TODO.','.TaskTypes::NOTE.','.TaskTypes::TEXT_MESSAGE.')) AND tasks.component_id=t.id AND tasks.component_type_id=t.component_type_id AND tasks.complete_date IS NOT NULL) < 1 ';

            if($returnCriteria) {
                return $Criteria;
            } else {
                $Criteria->together = true;
                $this->getDbCriteria()->mergeWith($Criteria);
                return $this;
            }
        }

        public function byHasEmailSent()
        {
            $tableAlias   = $this->getTableAlias(true, false);

            // email that is within 1 minute of transaction creation other with email_lu and subject line of Your New Account Info! or your new account is setup.

            $Criteria = new CDbCriteria;
            $Criteria->join = 'LEFT JOIN activity_log activityLog ON '.$tableAlias.'.id=activityLog.component_id AND '.$tableAlias.'.component_type_id=activityLog.component_type_id AND activityLog.task_type_id IN ('.TaskTypes::EMAIL_MANUAL.') LEFT JOIN activity_log_email_message_lu activityLogEmailMessageLu ON activityLog.id=activityLogEmailMessageLu.activity_log_id LEFT JOIN email_messages emailMessages ON activityLogEmailMessageLu.email_message_id=emailMessages.id AND emailMessages.origin_type_ma<>'.EmailMessage::SYSTEM_AUTO_NEW_ACCOUNT_WELCOME;
            $Criteria->condition = 'emailMessages.id IS NOT NULL';
            $Criteria->group = 't.id';
            $Criteria->order = 'emailMessages.id DESC';
            $Criteria->together = true;

            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

        public function bySetAppointment($dateRange=null, $setById)
        {
            $dateRange = Yii::app()->format->formatDateRangeArray($dateRange);
            $tableAlias   = $this->getTableAlias(true, false);

            $Criteria = new CDbCriteria;
            $Criteria->join = 'LEFT JOIN appointments ON '.$tableAlias.'.id=appointments.component_id AND '.$tableAlias.'.component_type_id=appointments.component_type_id';
            $Criteria->condition = 'set_by_id=:set_by_id AND (appointments.is_deleted=0 OR appointments.is_deleted IS NULL)';
            $Criteria->params = array(':set_by_id'=>$setById);
            $Criteria->together = true;

            if(!empty($dateRange)) {
                $Criteria->condition .= ' AND appointments.set_on_datetime>=DATE(:from_date) AND appointments.set_on_datetime<=DATE(:to_date)';
                $Criteria->params = CMap::mergeArray($Criteria->params, array(':from_date'=>$dateRange['from_date'],':to_date'=>$dateRange['to_date']));
            }

            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

        /**
         * Get Latest Buyer Or Seller Transaction
         *
         * Does just what the title says, pass it a contact ID to keep it happy
         * @param integer $contactId
         * @return mixed Array of single record data, null when no data
         */
        public function getLatestBuyerOrSellerTransaction($contactId)
        {
            // Create command
            $command = Yii::app()->db->createCommand()
                ->select('t.*')
                ->from('transactions t')
                ->where('t.contact_id = :contactId', array(':contactId' => $contactId))
                ->andWhere(array('in', 't.component_type_id', array(ComponentTypes::BUYERS, ComponentTypes::SELLERS)))
                ->order('t.component_type_id ASC, t.added DESC')
                ->limit(1);

            // Run query
            $records = $command->queryAll();

            return $records ? $records[0] : null;
        }
    }
