<?php
	Yii::import('admin_module.models.interfaces.Component');

	/**
	 * This is the model class for table "tasks".
	 *
	 * The followings are the available columns in table 'tasks':
	 *
	 * @property integer       $task_id
	 * @property integer       $activity_task_group_lu
	 * @property integer       $task_type_lu
	 * @property integer       $assigned_to_id
	 * @property integer       $assigned_by_id
	 * @property integer       $action_plan_id
     * @property integer       $component_type_id
	 * @property integer       $component_id
	 * @property integer       $is_priority
	 * @property string        $due_date
	 * @property string        $complete_date
	 * @property string        $description
     * @property string        $operation_manual_id
     * @property string        $task_recursion_id
	 * @property string        $google_calendar_id
	 * @property string		   $google_calendar_event_id
	 * @property integer       $is_deleted
	 * @property string        $added
	 * @property integer       $added_by
	 * @property string        $updated
	 * @property integer       $updated_by
	 *
	 * The followings are the available model relations:
	 * @property GlobalLookups $taskTypeLu
	 * @property GlobalLookups $activityTaskGroupLu
	 * @property Contacts      $assignedTo
	 * @property Contacts      $assignedBy
	 * @property Contacts      $addedBy
	 * @property Contacts      $updatedBy
	 */
	class Tasks extends StmBaseActiveRecord implements Component
    {
        const PRIORITY_NORMAL = 0;
        const PRIORITY_EOD = 1;
        const PRIORITY_ASAP = 2;

        public $to_date;
		public $from_date;
        public $ignoreTriggerActionPlan = false;

		public $googleCalendarData = array();

        const MAX_TASK_DELETE = 130;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Task the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'tasks';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'component_type_id, account_id, task_type_id, assigned_to_id, assigned_by_id, status_ma, component_id, is_priority, operation_manual_id, added_by, updated_by, is_deleted, task_recursion_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'account_id, component_type_id, component_id, task_type_id, due_date, assigned_to_id, description ',
					'required', 'on'=>'insert, update',
				),
                array(
                    'component_type_id, component_id, task_type_id, assigned_to_id, description ',
                    'required', 'on'=>'dependentTasks',
                ),
				array(
					'google_calendar_event_id, google_calendar_id',
					'length',
					'max' => 50
				),
				array(
					'googleCalendarData',
					'validateGoogleCalendarData'
				),
				array(
					'description',
					'length',
					'max' => 750
				),
				array(
					'due_date, added, updated, complete_date, from_date, to_date',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, component_type_id, account_id,task_type_id, assigned_to_id, assigned_by_id, status_ma, component_id, is_priority, due_date, complete_date, description, operation_manual_id, updated, updated_by, added, added_by, is_deleted, google_calendar_id, google_calendar_event_id, googleCalendarData',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'taskType' => array(
					self::BELONGS_TO,
					'TaskTypes',
					array('task_type_id' => 'id')
				),
				'componentType' => array(
					self::BELONGS_TO,
					'ComponentTypes',
					'component_type_id',
					'alias' => 't'
				),
				'assignedTo' => array(
					self::BELONGS_TO,
					'Contacts',
					'assigned_to_id',
					'alias' => 't'
				),
				'assignedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'assigned_by_id',
					'alias' => 't'
				),
				'updatedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'updated_by'
				),
				'addedBy' => array(
					self::BELONGS_TO,
					'Contacts',
					'added_by'
				),
				'dependentTasks' => array(
					self::HAS_MANY,
					'Tasks',
					'depends_on_id'
				),
				'dependsOnTask' => array(
					self::BELONGS_TO,
					'Tasks',
					'depends_on_id'
				),
				'actionPlanAppliedLu' => array(
					self::HAS_ONE,
					'ActionPlanAppliedLu',
					'task_id'
				),
                'actionPlanItem' => array(
                    self::MANY_MANY,
                    'ActionPlanItems',
                    'action_plan_applied_lu(task_id, action_plan_item_id)',
                ),
                'taskRecursion' => array(
					self::BELONGS_TO,
					'TaskRecursions',
					'task_recursion_id'
				),
                'callListPhones' => array(
                    self::HAS_MANY,
                    'CallListPhones',
                    'task_id'
                ),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'Task',
				'account_id' => 'Account',
				'status_ma' => 'Status',
				'componentType' => 'Component Type',
				'component_id' => 'Record',
				'is_priority' => 'Priority',
				'task_type_id' => 'Task Type',
				'assigned_to_id' => 'Assigned To',
				'assigned_by_id' => 'Assigned By',
				'due_date' => 'Due Date',
				'complete_date' => 'Complete Date',
				'description' => 'Description',
                'operation_manual_id' => 'Operation Manual',
				'is_deleted' => 'Is Deleted',
				'task_recursion_id'	=>	'Task Recursion ID'
			);
		}


		/**
		 * findTaskByAssignedTo
		 * Find a contact by a e-mail address
		 *
		 * @param int $id
		 *
		 * @return mixed model results
		 */
		//can do through relationship or scope
		public static function findTaskByAssignedTo($id) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'assigned_to_id=:id';
			$Criteria->params = array(':id' => $id);
			$Tasks = Tasks::model()->find($Criteria);

			return $Tasks;
		}

		/**
		 * Retrieve the name of the task, based on the component type
		 * @return mixed|null
		 */
		public function getName() {
			$nameByComponentType = null;

			if ($this->componentType) {
				switch($this->componentType->id) {
					case ComponentTypes::PROJECTS:
						$project = Projects::model()->findByPk($this->component_id);
						if ($project) {
							$nameByComponentType = $project->name;
						}
						break;

					default:
						$contact = $this->contact;
						if ($contact) {
							$nameByComponentType = $contact->getFullName();
						}
						break;
				}
			}

			return $nameByComponentType;
		}

		/**
		 * getContact
		 *
		 * @return model Contact
		 */
		public function getContact() {
			$Contact = $this->componentType->getContact($this->component_id);

			return $Contact;
		}

        /**
         * byHasActionPlanItemEmailTemplate()
         *
         * @return mixed ActionPlanItem
         */
        public function byHasActionPlanItemEmailTemplate() {
            $Criteria = new CDbCriteria;
            $Criteria->with = array('actionPlanAppliedLu'=>array('joinType'=>'INNER JOIN'),'actionPlanAppliedLu.actionPlanItem'=>array('joinType'=>'INNER JOIN'));
            $Criteria->condition = 'actionPlanItem.email_template_id IS NOT NULL';

            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

		/**
		 * Used to return the action plan item associated with the task
		 *
		 * @return mixed ActionPlanItem
		 */
		public function getActionPlanItem(Tasks $task = null) {
			if ($task) {
				$actionPlanAppliedLu = ActionPlanAppliedLu::model()->find(array(
						'condition' => 'task_id = :task_id',
						'params' => array(':task_id' => $task->id),
					)
				);
			} else {
				$actionPlanAppliedLu = $this->actionPlanAppliedLu;
			}

			if (!$actionPlanAppliedLu) {
				return null;
			}

            // do it this way as the action plan item may be deleted and this will cause error.
            return ActionPlanItems::model()->skipSoftDeleteCheck()->findByPk($actionPlanAppliedLu->action_plan_item_id);
		}

		/**
		 * Determines whether this task is currently due, depending on the "depends_on_id"
		 */
		public function getIsDue() {
			$isDue = true;
			if (!$this->dependsOnTask) {
				return $isDue;
			}

			// If the parent task is completed then the task is due
			return $this->dependsOnTask->isComplete();
		}

		public static function getDatePresetList() {
			return array(
				'today' => 'Today',
				'this_week' => 'This Week',
				'next_2_weeks' => 'Next 2 Weeks',
				'this_month' => 'This Month',
				'next_month' => 'Next Month',
				'custom' => 'Custom'
			);
		}

		public static function getDateFromPreset($preset) {
			$opts = array();
			switch ($preset) {
				case 'today':
					$opts['to_date'] = date("Y-m-d");
					break;

				case 'this_week':
					$opts['to_date'] = date("Y-m-d", strtotime("+1 week"));
					break;

				case 'next_2_weeks':
					$opts['to_date'] = date("Y-m-d", strtotime("+2 weeks"));
					break;

				case 'this_month':
					$opts['to_date'] = date("Y-m-t");
					break;

				case 'next_month':
					$opts['to_date'] = date("Y-m-d", strtotime("+1 month"));
					break;

				case 'custom':
					$opts['from_date'] = $opts['custom']['from_date'];
					$opts['to_date'] = $_GET['custom']['to_date'];
					break;
			}

			return $opts;
		}

        /**
         * Last Min Deletes
         *
         * Counts the deletes that occurred in the last minute in order to detect any unintended results such as deleting 1000+ tasks
         * This belongs to an unresolved issue
         * @var int Number of deleted items for this page load
         */
        public static $_lastMinuteDeletes = 0;
        public static $_lastMinuteDeletesData = array();
        public static $_lastMinuteDeletesActionPlanItemCount;

        protected function beforeDelete()
        {
            // Increment deletes and make sure we haven't deleted more than 100 since the beginning of this page load
            self::$_lastMinuteDeletes++;
            self::$_lastMinuteDeletesData[] = $this->attributes;
            if(self::$_lastMinuteDeletes >= self::MAX_TASK_DELETE) {
                // too many tasks to delete if there is a value for action plan item count and it's greater than that
                if(!self::$_lastMinuteDeletesActionPlanItemCount || (self::$_lastMinuteDeletesActionPlanItemCount && self::$_lastMinuteDeletes > self::$_lastMinuteDeletesActionPlanItemCount)) {

                    if(!YII_DEBUG && strpos($_SERVER['SERVER_NAME'], 'myashevillerealestate') === false) {
                        Yii::log(__CLASS__.' (:'.__LINE__.') *** More than '.self::MAX_TASK_DELETE.' tasks have been queued for deletion!!! $self::$_lastMinuteDeletes='.self::$_lastMinuteDeletes.',  $_lastMinuteDeletesActionPlanItemCount='. self::$_lastMinuteDeletesActionPlanItemCount . PHP_EOL . print_r($this->attributes, true), CLogger::LEVEL_ERROR);
                        Yii::app()->plivo->sendMessage('9043433200', 'STM Task Delete ERROR: At least ' . self::$_lastMinuteDeletes . ' task delete were attempted! See error log ASAP!');
                        throw new Exception(__CLASS__.' (:'.__LINE__.') More than '.self::MAX_TASK_DELETE.' tasks have been queued for deletion' . print_r($this->attributes, true));
                    }
                }
            }

            // Chain parent method
            return parent::beforeDelete();
        }

        public static function resetLastMinuteDeleteCount()
        {
            self::$_lastMinuteDeletes = 0;
        }

		protected function beforeSave() {
			if ($this->isNewRecord) {
				$this->added = new CDbExpression('NOW()');
				$this->added_by = Yii::app()->user->id;
                $this->assigned_by_id = Yii::app()->user->id;  //@todo: this is null during cron process
			}

            if(!$this->account_id) {
                $this->account_id = Yii::app()->user->accountId;
            }

			// Date Conversions
            if (!$this->due_date instanceof CDbExpression && !empty($this->due_date)) {
                $this->due_date = date(StmFormatter::MYSQL_DATETIME_FORMAT, strtotime($this->due_date));
            }

			//Attach User who Created Task
			$this->updated_by = Yii::app()->user->id; //@todo: this is null during cron process
			$this->updated = new CDbExpression('NOW()');

            // google calendar handling
            if(!YII_DEBUG &&  $this->googleCalendarData['add_event'] && ApiKeys::model()->findByAttributes(array('contact_id'=>Yii::app()->user->id, 'type'=>'google'))) {
                $this->_saveGoogleCalendarEvent();
            }

            return parent::beforeSave();
		}

//		protected function afterFind() {
//
//			// uses api to find google calendar event
//			if($this->google_calendar_event_id && $this->google_calendar_id && Yii::app()->controller->id=='transactions' && Yii::app()->controller->action->id=='edit' && ApiKeys::model()->findByAttributes(array('contact_id'=>Yii::app()->user->id, 'type'=>'google'))) {
//
//				Yii::import('admin_module.components.StmOAuth2.Google', true);
//				$stmGoogle = new \StmOAuth2\Google();
//				$googleCalendarList = array();
//				foreach($stmGoogle->listCalendars() as $calendar) {
//					$googleCalendarList[$calendar->id] = 'Calendar - '.$calendar->id;
//				}
//
//				// make sure the user has permission to view this calendar
//				if(isset($googleCalendarList[$this->google_calendar_id])) {
//					$this->_getGoogleCalendarEvent();
//				}
//			}
//
//			parent::afterFind();
//		}

		protected function afterSave() {
			parent::afterSave();
			$this->updateDependentTasks();
            $this->applyDependentActionPlans();

            $nowDateTime = Yii::app()->db->createCommand('SELECT NOW()')->queryScalar();

            // only add to callListPhones if due now
            if($this->due_date <= $nowDateTime && $this->complete_date === null && $this->is_deleted==0) {
                $this->_addCallListPhones();
            }
            elseif($this->complete_date !== null || $this->due_date > $nowDateTime || $this->is_deleted==1) {
                $this->deleteDependentCallListPhones();
            }

            // check recurring task to pre-populate with additional tasks if last one
            if($this->is_deleted ==0 && $this->complete_date !== null && ($taskRecursion = $this->taskRecursion) && !$this->hasFutureRecurringTasks()) {

                // set this value so it pre-populates based on today's date
                $taskRecursion->recur_start_date = date('Y-m-d');
                $taskRecursion->createRecursionTasks($this);
            }
		}

        protected function _addCallListPhones()
        {
            // if this task does not exist on the callListPhone
            if(CallListPhones::model()->countByAttributes(array('task_id'=>$this->id)) == 0) {
                CallListPhones::addTaskToExistingCallList($this->id);
            }
        }

        protected function deleteDependentCallListPhones()
        {
            if(!$this->getIsNewRecord()) {
                if($callListPhones = $this->callListPhones) {
                    foreach($callListPhones as $callListPhone) {
                        $callListPhone->delete();
                    }
                }
            }
        }

        protected function applyDependentActionPlans()
        {
            // see if this task is part of an action plan
            $actionPlanItems = $this->actionPlanItem;
            if(!$actionPlanItems || (!$this->complete_date || $this->is_deleted=1)) {
                return;
            }

            // see if action plan has triggered action plan upon completion, if action plan complete and no other tasks remaining in action plan
            $actionPlan = $actionPlanItems[0]->actionPlan;
            if($actionPlan->on_complete_plan_id && !$this->ignoreTriggerActionPlan && $actionPlan->getIsCompleted($this->component_id)) {

                if($component = ComponentTypes::getComponentModel($this->component_type_id, $this->component_id)) {

                    // if not applied, apply the triggered action plan
                    if(!$actionPlan->onCompleteActionPlan->isApplied($component)) {
                        // apply triggered action plan
                        $actionPlan->onCompleteActionPlan->apply($component, $actionPlan);
                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Action plan has been triggered. Remove this log when done monitoring.' . PHP_EOL . 'Data array: ' . print_r($this->attributes, true), \CLogger::LEVEL_ERROR);
                    }
                    // if already applied, ignore
                    else {
                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Triggered action plan already is applied. Remove this log when done monitoring.' . PHP_EOL . 'Data array: ' . print_r($this->attributes, true), \CLogger::LEVEL_ERROR);
                    }
                }
            }
        }

        protected function updateDependentTasks() {
			if (!empty($this->dependentTasks)) {
				foreach ($this->dependentTasks as $dependentTask) {
					if ($this->isComplete() || $this->is_deleted == 1) {
						$this->updateDueDate($dependentTask);
					}
				}
			}
		}

        protected function isComplete() {
			return !empty($this->complete_date);
		}

        protected function updateDueDate(Tasks $dependentTask)
        {
            // if the dependent task is deleted return so it doesn't try to calculate due date on a non-object
            if($dependentTask->is_deleted == 1) {
                return;
            }

			if($ActionPlanItem = $this->getActionPlanItem($dependentTask)) {

                $dueDate = $ActionPlanItem->calculateDueDate(null, new DateTime());
                if(!$dueDate) {
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Due date is missing for updating a dependent task. Task Data: '.print_r($this->attributes, true));
                }
                $dependentTask->due_date = $dueDate->format(StmFormatter::MYSQL_DATETIME_FORMAT);

                try {
                    $dependentTask->save();
                } catch (Exception $e) {
                    Yii::log('Encountered exception saving the following Task: ' . print_r($dependentTask->attributes, true) . ' - ' . $e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
                }
            }
		}

		public function scopes() {
			return array(
				'assignedToCurrentUser' => array(
					'condition' => 'assigned_to_id=:assigned_to_id',
					'params' => array(':assigned_to_id' => Yii::app()->user->id),
				),
				'byCurrentMonth' => array(
					'condition' => 'MONTH(due_date) = MONTH(CURRENT_DATE)',
				),
				'overdue' => array(
					'condition' => 'due_date <= :due_date AND complete_date IS NULL',
					'params' => array(':due_date' => date("Y-m-d", strtotime("-1 day"))),
				),
				'today' => array(
					'condition' => 'due_date <= :due_date',
					'params' => array(':due_date' => date('Y-m-d')),
				),
				'dueToday' => array(
					'condition' => 'DATE(due_date) = CURRENT_DATE',
				),
                'dueUpToToday' => array(
                    'condition' => 'DATE(due_date) <= CURRENT_DATE',
                ),
				'notCompleted' => array(
					'condition' => 'complete_date IS NULL',
				),
				'asc' => array(
					'order' => 'due_date asc',
				),
				'desc' => array(
					'order' => 'due_date desc',
				),
				'byPriority' => array(
					'order' => 'is_priority desc',
				),
			);
		}

		public function dateRange($opts = null) {

			if ($opts == null) {
				$this->getDbCriteria()->mergeWith(array(
						'condition' => 'DATE(due_date) <= DATE(:due_date)',
						'params' => array(':due_date' => date('Y-m-d')),
					)
				);
			} else {
				if ($opts['to_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => 'DATE(due_date) <= DATE(:to_date)',
							'params' => array(':to_date' => $opts['to_date']),
						)
					);
				}

				if ($opts['from_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => 'due_date >= :from_date',
							'params' => array(':from_date' => $opts['from_date']),
						)
					);
				}
			}

			return $this;
		}

		public function byAssignedTo($assignedToId) {
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'assigned_to_id=:assigned_to_id',
					'params' => array(':assigned_to_id' => $assignedToId),
				)
			);

			return $this;
		}

        public function byAddedBy($addedBy)
        {
            if($addedBy) {
                $this->getDbCriteria()->mergeWith(array(
                        'condition' => 't.added_by=:added_by',
                        'params' => array(':added_by' => $addedBy),
                    )
                );
            }

            return $this;
        }

        public function excludeTaskTypeIds($ids) {
            if(!empty($ids)) {
                $this->getDbCriteria()->addNotInCondition('task_type_id',(array)$ids);
            }

            return $this;
        }

		/**
		 * Filter by a specific due date
		 * @param $dueDate
		 * @return $this
		 */
		public function byDueDate($dueDate) {
			$this->getDbCriteria()->mergeWith(array(
				'condition' => 'DATE(due_date) = :dueDate',
				'params' => array(':dueDate' => $dueDate),
			));

			return $this;
		}

		public function byTaskType($taskTypeId = null) {
			if ($taskTypeId != null && !empty($taskTypeId) && is_array($taskTypeId)) {
				$this->getDbCriteria()->addInCondition('t.task_type_id', $taskTypeId);
			}

			return $this;
		}

        public function byComponentTypeIds($componentTypeIds=null) {
            if($componentTypeIds != null && !empty($componentTypeIds)) {
                $componentTypeIds = (array) $componentTypeIds;
                $this->getDbCriteria()->addInCondition('t.component_type_id', $componentTypeIds);
            }

            return $this;
        }

		public function byComponentTuple($tuple) {
			if(!is_array($tuple))
				return $this;

			$this->getDbCriteria()->mergeWith(array(
											  'condition' => 'component_type_id=:component_type_id AND component_id=:component_id',
											  'params' => array(':component_type_id' => $tuple['componentTypeId'],':component_id'=>$tuple['componentId']),
											  )
			);

			return $this;
		}

        /**
         * Has Past Due
         *
         * Returns boolean on if the user id has overdue appointments that need to be inputted correctly.
         * @param $id
         * return boolean
         */
        public static function hasPastDue($id)
        {
            return (boolean) Tasks::model()->overdue()->countByAttributes(array('assigned_to_id'=>($id)? $id : Yii::app()->user->id));
        }

        /**
         * Has Past Due
         *
         * Returns boolean on if the user id has overdue appointments that need to be inputted correctly.
         * @param $id
         * return boolean
         */
        public static function countPastDue($id)
        {
            return Tasks::model()->overdue()->countByAttributes(array('assigned_to_id'=>($id)? $id : Yii::app()->user->id));
        }

		public static function findByComponentType($componentTypeId, $contactId = null) {
			if (!$contactId) {
				$contactId = Yii::app()->user->id;
			}
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'component_type_id=:component_type_id AND assigned_to_id=:assigned_to_id
		                        AND complete_date is NULL AND due_date <= :due_date';
			$Criteria->params = array(
				':component_type_id' => $componentTypeId,
				':assigned_to_id' => $contactId,
				':due_date' => new CDbExpression('NOW()')
			);

			return new CActiveDataProvider('Tasks', array('criteria' => $Criteria));
		}

		public function printPriorityFlag()
        {
            $priorityFlag = '';

            switch($this->is_priority) {

                case self::PRIORITY_ASAP:
                    $priorityFlag = '<span class="priority-task-flag">!</span> ';
                    break;

                case self::PRIORITY_EOD:
                    $priorityFlag = '<span class="priority-task-flag-2">!</span> ';
                    break;
            }
			return $priorityFlag;
		}

		public function printOverdueFlag($padding = true) {
			return ($this->isOverdue) ? '<span class="overdue-task-flag ' . (($padding) ? 'p-pl10' : '') . '">Overdue!</span> ' : '';
		}

		public function getIsOverdue() {
			return ($this->due_date < date('Y-m-d')) ? true : false;
		}

		public function printNameGridColumn() {
			switch($this->component_type_id) {
				case ComponentTypes::PROJECTS:
					return $this->name;
					break;
				case ComponentTypes::RECRUITS:
				case ComponentTypes::CONTACTS:
				case ComponentTypes::BUYERS:
				case ComponentTypes::SELLERS:
					$phones = ($this->taskType->name=="Phone")? Yii::app()->format->formatPrintPhones($this->contact->phones): '';
					return CHtml::link($this->name, array("/".Yii::app()->controller->module->id."/".$this->componentType->name."/".$this->component_id), array("target"=>"_blank"))."<br />".$phones;
					break;
				default:
					$phones = ($this->taskType->name=="Phone")? Yii::app()->format->formatPrintPhones($this->contact->phones): '';
					return CHtml::link($this->name, array("/".Yii::app()->controller->module->id."/".$this->componentType->name."/".$this->component_id), array("target"=>"_blank"))."<br />".$phones;
					break;
			}
		}

		public function getTransaction() {
			switch ($this->component_type_id) {
				case ComponentTypes::BUYERS:
					return Buyers::model()->findByPk($this->component_id);
					break;
                case ComponentTypes::LISTINGS:
				case ComponentTypes::SELLERS:
					return Sellers::model()->findByPk($this->component_id);
					break;
				case ComponentTypes::CLOSINGS:
					return Closings::model()->findByPk($this->component_id)->transaction;
					break;
			}
            return null;
		}

        public function getComponentModel() {
            switch ($this->component_type_id) {
                case ComponentTypes::CONTACTS:
                    return Contacts::model()->findByPk($this->component_id);
                    break;
                case ComponentTypes::BUYERS:
                    return Buyers::model()->findByPk($this->component_id);
                    break;
                case ComponentTypes::LISTINGS:
                case ComponentTypes::SELLERS:
                    return Sellers::model()->findByPk($this->component_id);
                    break;
                case ComponentTypes::CLOSINGS:
                    return Closings::model()->findByPk($this->component_id);
                    break;
                case ComponentTypes::RECRUITS:
                    return Recruits::model()->findByPk($this->component_id);
                    break;
                case ComponentTypes::REFERRALS:
                    return Referrals::model()->findByPk($this->component_id);
                    break;
            }
            return null;
        }

        public function getAddress() {
			switch ($this->component_type_id) {
				case ComponentTypes::SELLERS:
					# code...
					break;
				case ComponentTypes::LISTINGS:
					# code...
					break;
				case ComponentTypes::CLOSINGS:
					return Closings::model()->findByPk($this->component_id)->addressModel;
					break;
			}
		}

		public function getComponentStatus() {
			switch ($this->component_type_id) {
				case ComponentTypes::BUYERS:
				case ComponentTypes::SELLERS:
				case ComponentTypes::LISTINGS:
					return Transactions::model()->findByPk($this->component_id)->status->name;
					break;
				case ComponentTypes::CLOSINGS:
					$statusTypes = Closings::getStatusTypes();

					return $statusTypes[Closings::model()->findByPk($this->component_id)->closing_status_ma];
					break;
                case ComponentTypes::REFERRALS:
                    return 'Referral';
                    break;
			}
		}

		public function getContactId() {
			return $this->getContact()->id;
		}

		public function getComponentChain() {
			return array($this->asComponentTuple());
		}

		public function asComponentTuple() {
			return array(
				'componentTypeId' => ComponentTypes::TASKS,
				'componentId' => $this->id
			);
		}

		public static function hasNoFutureTask($componentTypeId, $componentId, $upToDate) {
			if (empty($upToDate)) {
				$upToDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + 1 month'));
				echo $upToDate;
				exit;
			}

			// @todo: Max it out to 2 weeks from last_visit/login????? or last activity

			$Criteria = new CDbCriteria;
			$Criteria->condition = 'component_type_id=:component_type_id AND component_id=:component_id AND complete_date is null AND due_date>=:today AND due_date<=:due_date';

			$Criteria->params = array(
				':component_type_id' => $componentTypeId,
				':component_id' => $componentId,
				':today' => date('Y-m-d H:i:s'),
				':due_date' => $upToDate
			);

			return (Tasks::model()->count($Criteria)) ? false : true;
		}

		/**
		 * Loads activeDataProvider with conditions and returns ready for use (ex. CGridview)
		 *
		 * @param      $componentId
		 * @param null $componentTypeId optional component type id which if omitted will not alter the query in any way.
		 *
		 * @internal param the $recordId record id to limit our search by.
		 * @return CActiveDataProvider
		 */
		public static function getAll($componentId, $componentTypeId = null) {
			$Criteria = new CDbCriteria;

			if ($componentTypeId) {
				$Criteria->condition = 'component_type_id=:component_type_id AND component_id=:component_id AND complete_date IS NULL';
				$Criteria->params = array(
					':component_type_id' => $componentTypeId,
					':component_id' => $componentId
				);
				$Criteria->order = 'ISNULL(due_date) ASC, due_date ASC, id ASC';
			}

			$TasksProvider = new CActiveDataProvider('Tasks', array(
				'criteria' => $Criteria,
                'pagination'=>array(
                    'pageSize'=> 50,
                ),
			));

			return $TasksProvider;
		}

		public function multiSelect($taskIds = array()) {
			if (count($taskIds) > 0) {
				$this->getDbCriteria()->addInCondition('id', $taskIds);
			}
            else {
                throw new Exception(__CLASS__.'(:'.__LINE__.') TaskIds cannot be empty as this will cause ALL tasks to be found.');
            }

			return $this;
		}

        public function printGridContactInfo() {
            $hidePhones = (Yii::app()->user->settings->hide_phone_numbers_search_results) ? true : false;
            $string = CHtml::link($this->name,'/'.Yii::app()->controller->module->id.'/'.$this->componentType->name.'/'.$this->component_id, $htmlOptions=array('target'=>'_blank'));
            if($this->task_type_id==TaskTypes::PHONE) {

                if($this->contact && is_array($this->contact->phones)) {
                    foreach($this->contact->phones as $phone) {
						if(empty($phoneTypes)){
							$phoneTypes = $phone->getPhoneTypes();
							$phoneOwnerTypes = $phone->getPhoneOwnerTypes();
						}
						$phoneTypeText = "";

						if(!empty($phoneOwnerTypes[$phone->owner_ma])){
							$phoneTypeText = $phoneOwnerTypes[$phone->owner_ma];
						}
						if(!empty($phoneTypes[$phone->phone_type_ma])){
							if(!empty($phoneTypeText)){$phoneTypeText .= " ";}
							$phoneTypeText .= $phoneTypes[$phone->phone_type_ma];
						}
						if(!empty($phoneTypeText)){
							$phoneTypeText.= ": ";
						}

                        $string .= '<br>'.(($hidePhones)? '***-***-**'.substr($phone->phone, -2, 2): $phoneTypeText.Yii::app()->format->formatPhone($phone->phone) );
                        // this is for the phone pad in STM
                        if(Yii::app()->user->clientId == 0) {
                            $string .= ' <button class="text click2callHelper" type="button" data-cid="'.$this->component_id.'" ctid="'.$this->componentType->id.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'" style="margin-left: 0;"><em class="icon i_stm_phone" style="width: 20px;"></em>CLICK-2-CALL</button>';
                        }
                        // this is for the dialog popup
                        elseif(Yii::app()->user->checkAccess('clickToCall')) {
                            $string .= '<a class="action-button green  fa fa-volume-control-phone task-button-tip click-to-call-button" data-tooltip="Click to Call" data-cid="'.$this->component_id.'" ctid="'.$this->componentType->id.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'"></a>';
//                            $string .= ' <button class="text click-to-call-button" type="button" data-cid="'.$this->component_id.'" ctid="'.$this->componentType->id.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'" style="margin-left: 0;"><em class="icon i_stm_phone" style="width: 20px;"></em>CLICK-2-CALL</button>';
                        }
                    }
                }
            }
            if($this->component_type_id==ComponentTypes::SELLERS || $this->component_type_id==ComponentTypes::BUYERS) {
                if($addressModel = Transactions::model()->findByPk($this->component_id)->address) {
                    $string .= '<br />'.Yii::app()->format->formatAddress($addressModel, array('lineBreak'=>true));
                }
            }
            if($this->component_type_id==ComponentTypes::CLOSINGS) {
                if($addressModel = Closings::model()->findByPk($this->component_id)->addressModel) {
                    $string .= '<br />'.Yii::app()->format->formatAddress($addressModel, array('lineBreak'=>true));
                }
            }

            return $string;
        }

        /**
         * printNextTask - Displays the next task date according to it's stats. Future date displays normal, past due is in bold red, none is in bold red.
         */
        public static function formatNextTaskDate($date) {

            $today = date('Y-m-d');
            switch(1) {
                // next task is on time
                case $date >= $today:
                    $nextTaskDate = Yii::app()->format->formatDays($date, array("isFuture"=>true))." ".Yii::app()->format->formatDate($date);
                    break;
                // next task is past due
                case (($date < $today) && ($date !== null)):
                    $nextTaskDate = '<span style="font-weight: bold; color: #D20000;">'.Yii::app()->format->formatDays($date).' PAST DUE!</span> '.Yii::app()->format->formatDate($date);
                    break;
                case empty($date):
                    $nextTaskDate = '<span style="font-weight: bold; color: #D20000;">None</span>';
                    break;
            }
            return $nextTaskDate;
        }

        /**
         * Get existing task dates for recursion
         * @param      $recursionId
         * @param bool $includeDeleted
         *
         * @return array
         */
        public function getExistingTaskDates($recursionId, $includeDeleted=false) {
            $criteria = new CDbCriteria();
            $criteria->select = 'due_date';
            $criteria->condition = 'task_recursion_id=:id';
            $criteria->params = array(':id' => $recursionId);
            $criteria->order = 'id ASC';

            $existingTasks = Tasks::model()->findAll($criteria); //skipSoftDeleteCheck()->

            // Model data for easy lookup @todo: this is called even in a basic task list call and a bunch of saves are happening... not right?
            $existingTaskDates = array();
            foreach($existingTasks as $existingTask) {
                $date = new DateTime($existingTask['due_date']);
                $existingTaskDates[] = $date->format('Y-m-d');
            }
            return $existingTaskDates;
        }

        /**
         * Get max date for task available for recursion
         *
         * @return string date
         */
        public function getRecurringMaxDate()
        {
            if($this->task_recursion_id) {
                $criteria = new CDbCriteria();
                $criteria->select = 'due_date';
                $criteria->condition = 'task_recursion_id=:id';
                $criteria->params = array(':id' => $this->task_recursion_id);
                $criteria->order = 'due_date DESC';

                $model = new Tasks;
                $model = $model->skipSoftDeleteCheck()->find($criteria);
            }
            return ($model) ? $model->due_date : null;
        }

        public static function isTaskOverdueMax($contactId, $accountId)
        {
            if(!is_numeric($accountId)) {
                throw new Exception(__CLASS__.' ('.__LINE__.'): AccountId "'.$accountId.'" is not an integer.');
            }

            if(!is_numeric($contactId)) {
                throw new Exception(__CLASS__.' ('.__LINE__.'): ContactId "'.$contactId.'" is not an integer.');
            }

            // check to see if account setting has task overdue active
            $taskRedirectSettings = Yii::app()->db->createCommand('SELECT (SELECT TRIM(value) FROM setting_account_values where account_id='.$accountId.' AND setting_id='.Settings::SETTING_TASK_REDIRECT.') accountValue, (SELECT TRIM(value) FROM setting_contact_values where setting_id='.Settings::SETTING_TASK_REDIRECT.' AND contact_id='.$contactId.') contactValue')->queryRow();

            // return false if result set is null/empty
            if(!$taskRedirectSettings || ($taskRedirectSettings['accountValue'] == null && $taskRedirectSettings['contactValue'] == null)) {
                return false;
            }

            // check to see if contact setting exists, if so use that value, if not use account settings
            $taskRedirectMax = ($taskRedirectSettings['contactValue'] || $taskRedirectSettings['contactValue'] =='0') ? $taskRedirectSettings['contactValue'] : $taskRedirectSettings['accountValue'];

            // get count of overdue tasks
            $count = Yii::app()->db->createCommand()
                ->select('count(*)')
                ->from('tasks')
                ->where('assigned_to_id=:assignedToId', array(':assignedToId'=>$contactId))
                ->andWhere('DATE(due_date) <= CURRENT_DATE - INTERVAL 1 DAY')
                ->andWhere('complete_date IS NULL')
                ->andWhere('is_deleted=0')
                ->queryScalar();

            return ($count && $count > $taskRedirectMax)? true : false;
        }

        /**
         * Updates Action Plan Tasks where the referenced due date was updated.
         * Ex. Closing date, listing date, appointment set/met date
         *
         * @param $component
         * @param $dueReferenceDateType
         *
         * @throws Exception
         */
        public static function updateElasticActionTaskDueDate($component, $dueReferenceDateType)
        {
            // get all tasks related to closing date
            $taskIds = Yii::app()->db->createCommand()
                ->select('t.id task_id')
                ->from('tasks t')
                ->leftJoin('action_plan_applied_lu lu', 't.id=lu.task_id')
                ->leftJoin('action_plan_items i', 'i.id=lu.action_plan_item_id')
                ->leftJoin('action_plans p','p.id=i.action_plan_id')
                ->where('t.component_type_id='.$component->componentType->id)
                ->andWhere('t.component_id='.$component->id)
                ->andWhere('t.complete_date IS NULL')
                ->andWhere('t.is_deleted=0')
                ->andWhere('i.due_reference_ma='.$dueReferenceDateType)
                ->queryColumn();

//            $originalDate = new DateTime($this->_originalData['contract_closing_date']);
//            $newDate      = new DateTime($this->contract_closing_date);
//            $dateDiff = $originalDate->diff($newDate);
//            $daysDiff = $dateDiff->days;
//            if($dateDiff->invert) {
//                $daysDiff *= -1;
//            }

            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $taskIds);
            if($tasks = Tasks::model()->findAll($criteria)) {

                // go through each task and update the new due date based on closing date.
                foreach($tasks as $task) {

                    $actionPlanItem = $task->getActionPlanItem($task);
                    $newDueDate = $actionPlanItem->calculateDueDate($component);

                    if($newDueDate) {

                        $task->due_date = $newDueDate->format(StmFormatter::MYSQL_DATETIME_FORMAT);

                        if(!$task->save()) {
                            Yii::log(__CLASS__.'(:'.__LINE__.') Error: Task due date could not be updated for new closing date. Attributes: '.print_r($task->attributes, true).PHP_EOL.'Error: '.print_r($task->getErrors(), true), CLogger::LEVEL_ERROR);
                        }
                    }
                    else {
                        throw new Exception(__CLASS__.'(:'.__LINE__.') New Due Date not calculated for update closing date. New Closing Attributes: '.print_r($component->attributes, true).PHP_EOL.'Old Closing Attributes: '.print_r($component->_originalData, true));
                    }
                }
            }
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('t.id', $this->id);
			$criteria->compare('t.account_id', $this->account_id);
			$criteria->compare('t.status_ma', $this->status_ma);
			$criteria->compare('t.component_type_id', $this->component_type_id);
			$criteria->compare('t.task_type_id', $this->task_type_id);
			$criteria->compare('t.component_id', $this->component_id);
			$criteria->compare('t.is_priority', $this->is_priority);
			$criteria->compare('t.assigned_to_id', $this->assigned_to_id);
			$criteria->compare('t.assigned_by_id', $this->assigned_by_id);
			$criteria->compare('t.due_date', $this->due_date, true);
			$criteria->compare('t.complete_date', $this->complete_date, true);
			$criteria->compare('t.description', $this->description, true);
            $criteria->compare('t.operation_manual_id', $this->operation_manual_id);
			$criteria->compare('t.is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		/**
		 * Validate Google Calender fields
		 *
		 * @param  string $attribute The attribute name
		 * @param  array  $params    Parameters for the validation
		 *
		 * @return null
		 */
		public function validateGoogleCalendarData($attribute, $params)
		{
			if(empty($this->googleCalendarData) || !$this->googleCalendarData['add_event']) {
				return;
			}

			// check event_name, event_start //@todo: disable validation right now since the event name is auto generated - CLee 4/2/16
//			if(!$this->googleCalendarData['event_name']) {
//				$this->addError('googleCalendarData[event_name]', 'Enter a Google Calendar Event Name.');
//			}

            if(strpos($this->due_date, ':') == false) {
                $this->addError('due_date', 'Enter a time for the Google Calendar Event.');
            }

//			$this->googleCalendarData['event_start'] = $this->set_for_datetime;
			$this->googleCalendarData['event_start'] = $this->due_date;
		}

		protected function _saveGoogleCalendarEvent()
		{
			// sanity check
			if(!$this->googleCalendarData['add_event']) {
				return;
			}

			Yii::import('admin_module.components.StmOAuth2.Google', true);
			$stmGoogle = new \StmOAuth2\Google();
			// start-time for event_duration //@todo: need to streamline more, make efficient & reusable
			$timezone = new \DateTimeZone(\Yii::app()->user->account->timezone);
			if(!$timezone) {
				throw new \Exception('Timezone is missing.');
			}
			$stmGoogle->setTimeZone($timezone);

			// calculates end time for either scenario of add/update
			$endDateTime = new \DateTime($this->googleCalendarData['event_start']);
			// $endDateTime->setTimezone($timezone); //@todo: there is start.timeZone that we should assign $timezone to
			$endDateTime->modify('+'.$this->googleCalendarData['event_duration'].' minutes');

	//$gmtTimezone = new DateTimeZone('GMT');
	//$myDateTime = new DateTime(date('Y-m-d H:i:s'), $gmtTimezone);
	//$myDateTime->format('c');
	//$offset = $timezone->getOffset($myDateTime);

            // build the description
            $model = $this->componentType->getComponentModel($this->component_type_id, $this->component_id);
            switch($this->component_type_id) {

                case ComponentTypes::CONTACTS:
                    $eventName = '[Task]';
                    break;

                case ComponentTypes::BUYERS:
                    $eventName = '[Buyer Task]';
                    break;
                case ComponentTypes::SELLERS:
                    $eventName = '[Seller Task]';
                    break;
                case ComponentTypes::RECRUITS:
                    $eventName = '[Recruit Task]';
                    break;
            }

            $eventDescription = $this->description.PHP_EOL.PHP_EOL.$eventName.PHP_EOL.'------------------------------------------------------------------------'.PHP_EOL.$model->contact->fullName.PHP_EOL.Yii::app()->format->formatPrintEmails($model->contact->emails, $delimiter=', ', $defaultValue=null).PHP_EOL.Yii::app()->format->formatPrintPhones($model->contact->phones, $delimiter=', ', $defaultValue=null, $primaryLabel=null).PHP_EOL.PHP_EOL.'http://'.$_SERVER['SERVER_NAME'].'/admin/'.$this->componentType->name.'/'.$this->component_id.PHP_EOL.'(Added by: '.Yii::app()->user->contact->fullName.' @ '.date('m/d/Y g:iA').')';

			// edit existing event
			if($this->google_calendar_id && $this->google_calendar_event_id) {
				$stmGoogle->editCalendarEvent($this->google_calendar_id, $this->google_calendar_event_id, $this->googleCalendarData['event_start'], $endDateTime->format('Y-m-d H:i:s'), $this->description.' '.$eventName, $this->googleCalendarData['event_location'], $eventDescription);
			}
			// add new event
			else {
				$this->google_calendar_event_id = $stmGoogle->addCalendarEvent($this->google_calendar_id, $this->googleCalendarData['event_start'], $endDateTime->format('c'), $this->description.' '.$eventName, $this->googleCalendarData['event_location'], $eventDescription, $colorId='11');
			}
		}

		/**
		 * Finds Google Calendar Event and populates it into the Appointments model
		 * @throws Exception
		 */
		protected function _getGoogleCalendarEvent()
		{
			Yii::import('admin_module.components.StmOAuth2.Google', true);
			$stmGoogle = new \StmOAuth2\Google();
			$event = $stmGoogle->getCalendarEvent($this->google_calendar_id, $this->google_calendar_event_id);

			if(!$event) {
				return;
			}

			$this->googleCalendarData['add_event'] = true;
			$this->googleCalendarData['event_name'] = $event->summary;
			$this->googleCalendarData['event_location'] = $event->location;
			$this->googleCalendarData['event_description'] = $event->description;

			// start-time for event_duration //@todo: need to streamline more, make efficient & reusable
			$timezone = new \DateTimeZone(\Yii::app()->user->account->timezone);
			if(!$timezone) {
				throw new \Exception('Timezone is missing.');
			}

			$gcalStart = ($event->start->dateTime)? $event->start->dateTime : $event->start->date;
			$startDateTime = new \DateTime($gcalStart);
			$startDateTime->setTimezone($timezone);
	//            $this->googleCalendarData['event_start'] = $startDateTime->format(\StmFormatter::APP_DATETIME_PICKER_FORMAT);

			// event_duration //@todo: need to streamline more, make efficient & reusable
			$gcalEnd = ($event->end->dateTime)? $event->end->dateTime : $event->end->date;
			$endDateTime = new \DateTime($gcalEnd);
			$endDateTime->setTimezone($timezone);

			$diff = $startDateTime->diff($endDateTime);
			$diffMinutes = ($diff->format('%h') * 60) + $diff->format('%i');

			switch(1) {
				case ($diffMinutes < 75):
					$this->googleCalendarData['event_duration'] = 60;
					break;
				case ($diffMinutes >= 75 & $diffMinutes <= 105):
					$this->googleCalendarData['event_duration'] = 90;
					break;
				case ($diffMinutes > 105):
					$this->googleCalendarData['event_duration'] = 120;
					break;
			}

			$this->googleCalendarData['calendar_id'] = $this->google_calendar_id;
		}

        public function hasFutureRecurringTasks()
        {
            if($this->task_recursion_id) {

                $criteria = new CDbCriteria();
                $criteria->compare('t.task_recursion_id', $this->task_recursion_id);
                $criteria->addCondition('t.due_date > DATE(NOW())');
                $criteria->addCondition('t.complete_date IS NULL');

                return $this->count($criteria);
            }
        }
	}