 <?php
 /**
 * This is the model class for table "cms_menus".
 *
 * The followings are the available columns in table 'cms_menus':
  * @property string $id
  * @property integer $account_id
  * @property string $name
  * @property string $css_classes
  * @property string $added
  * @property integer $added_by
  * @property string $updated
  * @property integer $updated_by
   *
 * The followings are the available model relations:  * @property   * @property   * @property    */
 class CmsMenus extends StmBaseActiveRecord
 {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CmsMenus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_menus';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('account_id, name, added, added_by', 'required'),
 			array('id, account_id, added_by, updated_by', 'numerical', 'integerOnly'=>true),
 			array('name', 'length', 'max'=>100),
 			array('css_classes, updated', 'safe'),
 			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_id, name, css_classes, added, added_by, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'cmsDomainMenus' => array(self::HAS_MANY, 'CmsDomainMenus', 'menu_id'),
 			'cmsMenuLinks' => array(self::HAS_MANY, 'CmsMenuLinks', 'cms_menu_id', 'order'=>'`order` ASC'),
 			'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
 		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
 			'account_id' => 'Account',
 			'name' => 'Name',
 			'css_classes' => 'Css Classes',
 			'added' => 'Added',
 			'added_by' => 'Added By',
 			'updated' => 'Updated',
 			'updated_by' => 'Updated By',
 		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter
conditions.
	 * @return CActiveDataProvider the data provider that can return
the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;
        $criteria->compare('id',$this->id,true);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('css_classes',$this->css_classes,true);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('updated_by',$this->updated_by);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
