<?php

/**
 * This is the model class for table "processed_contact_responses".
 *
 * The followings are the available columns in table 'processed_contact_responses':
 * @property integer $id
 * @property integer $imap_message_id
 * @property integer $email_message_id
 * @property integer $processed
 * @property string $added
 */
class ProcessedContactResponse extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProcessedContactResponse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'processed_contact_responses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('imap_message_id, email_message_id, added', 'required'),
			array('email_message_id, processed', 'numerical', 'integerOnly'=>true),
            array('imap_message_id','length','max' => 100),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, imap_message_id, email_message_id, processed, added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'imap_message_id' => 'Imap Message',
			'email_message_id' => 'Email Message',
			'processed' => 'Processed',
			'added' => 'Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('imap_message_id',$this->imap_message_id);
		$criteria->compare('email_message_id',$this->email_message_id);
		$criteria->compare('processed',$this->processed);
		$criteria->compare('added',$this->added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}