<?php

	/**
	 * This is the model class for table "source_subgroups".
	 *
	 * The followings are the available columns in table 'source_subgroups':
	 *
	 * @property integer        $id
	 * @property integer        $source_id
	 * @property string         $name
	 * @property integer        $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts[]     $contacts
	 * @property Sources        $source
	 * @property Transactions[] $transactions
	 */
	class SourceCampaigns extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return SourceCampaigns the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'source_campaigns';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'source_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				array(
					'description',
					'length',
					'max' => 127
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, source_id, name, description, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				// 'contacts' => array(self::HAS_MANY, 'Contacts', 'source_subgroup_id'),
				'source' => array(
					self::BELONGS_TO,
					'Sources',
					'source_id'
				),
				// 'transactions' => array(self::HAS_MANY, 'Transactions', 'source_subgroup_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'source_id' => 'Source',
				'name' => 'Name',
				'description' => 'Description',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('source_id', $this->source_id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('description', $this->description, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}