<?php
Yii::import('admin_module.models.interfaces.Component');

	/**
	 * This is the model class for table "companies".
	 *
	 * The followings are the available columns in table 'companies':
	 *
	 * @property integer            $id
	 * @property integer            $account_id
     * @property integer            $status_ma
	 * @property string             $name
	 * @property string             $primary_contact_id
     * @property string             $website
     * @property string             $notes
	 * @property integer            $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property AddressCompanyLu[] $addressCompanyLus
	 * @property Accounts           $account
	 * @property CompanyTypes       $companyType
	 * @property CompanyContactLu[] $companyContactLus
	 */
	class Companies extends StmBaseActiveRecord implements Component {

		public $companyTypesToAdd = array(); // Collection of Company Types models
		public $companyAddresses = array(); // Collection of Address models
		public $companyPhones = array(); // Collection of Phones models

		const TITLE_COMPANIES = 'TITLE_COMPANIES';
		const LENDERS = 'LENDERS';
		const INSPECTORS = 'INSPECTORS';
		const REAL_ESTATE_BROKERAGES = 'REAL_ESTATE_BROKERAGES'; //?????

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Companies the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'companies';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, name',
					'required'
				),
				array(
					'account_id, status_ma, primary_contact_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
                array(
                    'website',
                    'length',
                    'max' => 127
                ),
				array(
					'companyTypesToAdd, notes',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, status_ma, name, primary_contact_id, companyTypesToAdd, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'account' => array(
					self::BELONGS_TO,
					'Accounts',
					'account_id'
				),
				'primaryContact' => array(
					self::BELONGS_TO,
					'Contacts',
					'primary_contact_id'
				),
				'phones' => array(
					self::HAS_MANY,
					'Phones',
					'company_id'
				),
				'addresses' => array(
					self::MANY_MANY,
					'Addresses',
					'address_company_lu(company_id, address_id)'
				),
				'contacts' => array(
					self::MANY_MANY,
					'Contacts',
					'company_contact_lu(company_id, contact_id)'
				),
				'types' => array(
					self::MANY_MANY,
					'CompanyTypes',
					'company_type_lu(company_id, company_type_id)'
				),
				'companyTypeLu' => array(
					self::HAS_MANY,
					'CompanyTypeLu',
					'company_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
                'status_ma' => 'Status',
				'account_id' => 'Account',
				'name' => 'Name:',
				'companyTypesToAdd' => 'Company Types:',
				'primary_contact_id' => 'Primary Contact',
                'website' => 'Website:',
                'notes' => 'Notes:',
				'is_deleted' => 'Is Deleted',
			);
		}

        /**
         * @return array behaviors for getTasks
         */
        public function behaviors()
        {
            return array(
                'getTasks' => array(
                    'class' => 'admin_module.components.behaviors.GetTasksBehavior',
                ),
            );
        }

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
            $criteria->compare('status_ma', $this->status_ma, true);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('primary_contact_id', $this->primary_contact_id, true);

			$criteria->together = true;

			//Search by contact types
			if ($this->companyTypeLu->idCollection) {
				$criteria->with = CMap::mergeArray($criteria->with, array('companyTypeLu'));
				$criteria->addInCondition('companyTypeLu.company_type_id', $this->companyTypeLu->idCollection);
			}

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}

		public function byType($type) {

			$Criteria = new CDbCriteria;
			$Criteria->with = array('types');
			$Criteria->together = true;
			$Criteria->addInCondition('types.id', $this->getCompanyTypeIds($type));

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function orderByName($order = 'ASC') {

			$Criteria = new CDbCriteria;
			$Criteria->order = $this->tableAlias . '.name ASC';

			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}


		protected function beforeValidate() {
			if ($this->isNewRecord) {
				$this->account_id = Yii::app()->user->accountId;
			}

			return parent::beforeValidate();
		}

		public function getAllContacts() {
			$allContacts = array();

			$primaryContact = Contacts::model()->findByPk($this->primary_contact_id);

			if (isset($primaryContact)) {
				$allContacts[] = $primaryContact;
			}

			$nonPrimaryContacts = $this->contacts;

			if (isset($nonPrimaryContacts)) {
				foreach ($nonPrimaryContacts as $Contact) {
					if (isset($Contact)) {
						$allContacts[] = $Contact;
					}
				}
			}

			return $allContacts;
		}

		public function getCompanyTypeIds($type) {
			$companyTypeIds = array();

			switch ($type) {
				case self::LENDERS:
					$companyTypeIds = CompanyTypes::lenders();
					break;
				case self::INSPECTORS:
					$companyTypeIds = CompanyTypes::inspectors();
					break;
				case self::TITLE_COMPANIES:
					$companyTypeIds = CompanyTypes::titleCompanies();
					break;
				case self::REAL_ESTATE_BROKERAGES:
					$companyTypeIds = CompanyTypes::realEstateBrokerages();
					break;
			}

			return $companyTypeIds;
		}

		/**
		 * Returns the primary phone number specified for a company, or the first record found if no primary
		 * phone number can be found.
		 *
		 * @param string $defaultValue
		 *
		 * @return string
		 */
		public function getPrimaryPhone($defaultValue = '') {
			$PrimaryPhone = Phones::model()->primary()->find(array(
					'select' => 'phone',
					'condition' => 'company_id = :company_id',
					'params' => array(':company_id' => $this->id),
				)
			);

			if ($PrimaryPhone) {
				return $PrimaryPhone->phone;
			} else {
				if ($this->phones[0]) {
					return $this->phones[0]->phone;
				}
			}

			return $defaultValue;
		}

		public static function getInspectorOptions() {

			$inspectors = Companies::model()->byType(Companies::INSPECTORS)->findAll();
			if (!$inspectors) {
				return;
			}

			$options = array();
			foreach ($inspectors as $Inspector) {

				foreach ($Inspector->types as $InspectorType) {

					array_push($options, array(
							'id' => $Inspector->id,
							'label' => $Inspector->name . ' (' . $InspectorType->getShortName() . ')',
							'group' => $InspectorType->name,
						)
					);
				}
			}

			return $options;
		}

        public function getContactId()
        {
            return null;
        }

        public function getComponentChain()
        {
            //@todo: Not applicable anymore since we stopped using Tasks as a componentTypeId
            $chain = array($this->asComponentTuple());

            $includeCompleted = true;
            $Tasks = $this->getTasks($this, $includeCompleted);

            if (count($Tasks) > 0) {
                foreach ($Tasks as $Task) {
                    array_push($chain, $Task->asComponentTuple());
                }
            }

            return $chain;
        }

        public function asComponentTuple()
        {
            return array(
                'componentTypeId' => $this->componentType->id,
                'componentId'     => $this->id
            );
        }

        public function getComponentType()
        {
            return ComponentTypes::model()->findByPk(ComponentTypes::COMPANIES);
        }
    }
