<?php

/**
 * This is the model class for table "cms_content_values".
 *
 * The followings are the available columns in table 'cms_content_values':
 * @property integer $cms_content_id
 * @property string $element_id
 * @property string $value
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 *
 * The followings are the available model relations:
 * @property Contacts $addedBy
 * @property CmsContents $cmsContent
 * @property Contacts $updatedBy
 */
class CmsContentValues extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CmsContentValues the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cms_content_values';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cms_content_id, element_id, updated_by, updated, added_by, added', 'required', 'on'=>'insert, update'),
            array('cms_content_id, element_id, updated, added', 'required', 'on'=>'stmCommand'),
            array('cms_content_id, updated_by, added_by', 'numerical', 'integerOnly'=>true),
            array('element_id', 'length', 'max'=>100),
            array('value', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('cms_content_id, element_id, value, updated_by, updated, added_by, added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'cmsContent' => array(self::BELONGS_TO, 'CmsContents', 'cms_content_id'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'cms_content_id' => 'Cms Content',
            'element_id' => 'Element',
            'value' => 'Value',
            'updated_by' => 'Updated By',
            'updated' => 'Updated',
            'added_by' => 'Added By',
            'added' => 'Added',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('cms_content_id',$this->cms_content_id);
        $criteria->compare('element_id',$this->element_id,true);
        $criteria->compare('value',$this->value,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated',$this->updated,true);
        $criteria->compare('added_by',$this->added_by);
        $criteria->compare('added',$this->added,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}