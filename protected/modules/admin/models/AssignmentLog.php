<?php

	/**
	 * This is the model class for table "assignment_log".
	 *
	 * The followings are the available columns in table 'assignment_log':
	 *
	 * @property integer        $id
	 * @property integer        $component_type_id
	 * @property integer        $component_id
	 * @property integer        $old_assignment_type_id
	 * @property integer        $new_assignment_type_id
	 * @property integer        $old_assignee_contact_id
	 * @property integer        $new_assignee_contact_id
	 * @property integer        $added_by
	 * @property string         $added
	 *
	 * The followings are the available model relations:
	 * @property Contacts       $addedBy
	 * @property ComponentTypes $componentType
	 * @property Contacts       $oldAssignmentType
	 * @property Contacts       $newAssignmentType
	 * @property Contacts       $oldAssigneeContact
	 * @property Contacts       $newAssigneeContact
	 */
	class AssignmentLog extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return AssignmentLog the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'assignment_logs';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'component_type_id, component_id, old_assignment_type_id, new_assignment_type_id, old_assignee_contact_id, new_assignee_contact_id, added_by, added',
					'required'
				),
				array(
					'component_type_id, component_id, old_assignment_type_id, new_assignment_type_id, old_assignee_contact_id, new_assignee_contact_id, added_by',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, component_type_id, component_id, old_assignment_type_id, new_assignment_type_id, old_assignee_contact_id, new_assignee_contact_id, added_by, added',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( //			'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
				//			'componentType' => array(self::BELONGS_TO, 'ComponentTypes', 'component_type_id'),
				//			'oldAssignmentType' => array(self::BELONGS_TO, 'Contacts', 'old_assignment_type_id'),
				//			'newAssignmentType' => array(self::BELONGS_TO, 'Contacts', 'new_assignment_type_id'),
				//			'oldAssigneeContact' => array(self::BELONGS_TO, 'Contacts', 'old_assignee_contact_id'),
				//			'newAssigneeContact' => array(self::BELONGS_TO, 'Contacts', 'new_assignee_contact_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'component_type_id' => 'Component Type',
				'component_id' => 'Component',
				'old_assignment_type_id' => 'Old Assignment Type',
				'new_assignment_type_id' => 'New Assignment Type',
				'old_assignee_contact_id' => 'Old Assignee Contact',
				'new_assignee_contact_id' => 'New Assignee Contact',
				'added_by' => 'Added By',
				'added' => 'Added',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('component_type_id', $this->component_type_id);
			$criteria->compare('component_id', $this->component_id);
			$criteria->compare('old_assignment_type_id', $this->old_assignment_type_id);
			$criteria->compare('new_assignment_type_id', $this->new_assignment_type_id);
			$criteria->compare('old_assignee_contact_id', $this->old_assignee_contact_id);
			$criteria->compare('new_assignee_contact_id', $this->new_assignee_contact_id);
			$criteria->compare('added_by', $this->added_by);
			$criteria->compare('added', $this->added, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}