<?php

/**
 * This is the model class for table "cms_theme_settings".
 *
 * The followings are the available columns in table 'cms_theme_settings':
 * @property string $id
 * @property string $cms_theme_id
 * @property string $name
 * @property string $default
 * @property string $value
 * @property string $added
 *
 * The followings are the available model relations:
 * @property CmsThemes $cmsTheme
 */
class CmsThemeSettings extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CmsThemeSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_theme_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cms_theme_id, name, added', 'required'),
			array('cms_theme_id', 'length', 'max'=>10),
			array('name', 'length', 'max'=>100),
			array('default', 'length', 'max'=>255),
			array('value', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cms_theme_id, name, default, value, added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cmsTheme' => array(self::BELONGS_TO, 'CmsThemes', 'cms_theme_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cms_theme_id' => 'Cms Theme',
			'name' => 'Name',
			'default' => 'Default',
			'value' => 'Value',
			'added' => 'Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('cms_theme_id',$this->cms_theme_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('default',$this->default,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('added',$this->added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
		));
	}
}