<?php
Yii::import('admin_module.models.interfaces.Component');

/**
 * This is the model class for table "contacts".
 *
 * The followings are the available columns in table 'contacts':
 *
 * @property integer                          $id
 * @property integer                          $account_id
 * @property integer                          $user_group_id
 * @property integer                          $contact_status_ma
 * @property string                           $first_name
 * @property string                           $last_name
 * @property string                           $password
 * @property string                           $session_id
 * @property integer                          $is_deleted
 *
 * The followings are the available model relations:
 * @property AccountContactLu[]               $accountContactLus
 * @property ActiveUsers[]                    $activeUsers
 * @property ActivityLog[]                    $activityLogs
 * @property AddressContactLu[]               $addressContactLus
 * @property Closings[]                       $closings
 * @property CompanyContactLu[]               $companyContactLus
 * @property ContactAssignments[]             $contactAssignments
 * @property ContactTypeLu[]                  $contactTypeLus
 * @property UserGroups                       $userGroup
 * @property Accounts                         $account
 * @property ContactsAttributes[]             $contactsAttributes
 * @property Emails[]                         $emails
 * @property Goals[]                          $goals
 * @property HistoryLog[]                     $historyLogs
 * @property LeadDistributionGroupContactLu[] $leadDistributionGroupContactLus
 * @property LoginLog[]                       $loginLogs
 * @property PageViews[]                      $pageViews
 * @property PermissionOverrides[]            $permissionOverrides
 * @property Phones[]                         $phones
 * @property Projects[]                       $projects
 * @property Referrals[]                      $referrals
 * @property Referrals[]                      $referrals1
 * @property Referrals[]                      $referrals2
 * @property SplashScreens[]                  $splashScreens
 * @property ActionPlans[]                    $actionPlans
 * @property TrackingTimeblock[]              $trackingTimeblocks
 * @property Transaction[]                    $transactions
 * @property SavedHomeSearches[]              $savedHomeSearches
 * @property mixed                            settingValues
 */
class Contacts extends StmBaseActiveRecord implements Component
{
    public $applyAccountScope = false;
    // Profile form properties
    public $passwordNew;
    public $passwordConfirm;
    public $passwordOld;
    public $profilePhoto;
    public $submitAddressId;
    public $isNewSubmitAddressId;

    // Public methods to use for storing data
    public $contactFullName;
    public $contactEmailSearch;
    public $contactEmailAdd;
    public $contactPhoneSearch;
    public $contactPhoneAdd;
    public $contactAddressSearch;
    public $addressToAdd;
    public $lastLoginFrom;
    public $lastLoginTo;
    public $lastSpokeToDate;
    public $assignmentId;
    public $fromSearchDate;
    public $toSearchDate;
    public $dateSearchFilter;
    public $dateSearchTypes = array(
        'added_desc' => 'Added: newest first',
        'added_asc' => 'Added: oldest first',
//        'last_login_desc' => 'Last Login: newest first',
//        'last_login_asc' => 'Last Login: oldest first',
//        'last_activity_desc' => 'Last Activity: newest first',
//        'last_activity_asc' => 'Last Activity: oldest first',
        //'next_task' => 'Next Task',
    );

    public $nextTaskDueDate;
    public $contactAttributeValue; //storage for single contactAttributeValue

    // Collections
    public $contactEmails = array(); // Collection of Emails models
    public $contactAddresses = array(); // Collection of Address models
    public $contactPhones = array(); // Collection of Phones models
    public $contactTypesToAdd = array(); // Collection of ContactType models
    public $contactAttributeValues = array(); // Collection of potential ContactAttributeValues models

	public $linkedAccounts = array();
	public $currentAccountId = NULL;

    protected $cachedEmails = array(); // cached copy of a contact's e-mail addresses
    protected $settings = array();

    private $_attributeValuesById = array();

    /**
     * Model Attributes
     *
     * @var $status : column name 'contact_status_ma'
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_TRASH = -1;

    const TRANSACTION_TYPE = 1; //need to access this from a static method

    const STM_USER_CONTACT_TYPE_ID = 6;

    const LEAD_TYPE_BUYER_ID = 1;
    const LEAD_TYPE_SELLER_ID = 2;
    const LEAD_TYPE_RECRUIT_ID = 3;

    const HASH_SECRET_KEY = "01b46a7b0e827834cffc8325f74d8ba539451c5c6a352eddbf1992176a90a831";

    private $_componentType;

	public $queryLimit = 10;

	public $noActionPlan = NULL;
	public $noTask = NULL;



    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Contacts the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contacts';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'profilePhoto',
                'file',
                'types' => 'jpg, gif, png',
                'allowEmpty'=> true,
                'on'    => 'frontProfileUpdate'
            ),
            array('source_id, account_id, first_name, last_name', 'required', 'on' => 'import'),
            array(
                'source_id, account_id, contactFullName',
                'required',
                'on' => 'insert, update'
            ),
            array(
                'passwordConfirm, passwordNew, passwordOld',
                'required',
                'on' => 'frontProfileUpdate'
            ),
            array(
                'passwordOld',
                'compare',
                'compareAttribute' => 'password',
                'on'               => 'frontProfileUpdate',
                'message'          => 'Incorrect Old password. Please try again.'
            ),
            array(
                'passwordConfirm',
                'compare',
                'compareAttribute' => 'passwordNew',
                'on'               => 'frontProfileUpdate'
            ),
            array(
                'contactEmailAdd, contactPhoneAdd',
                'validateEmailOrPhone',
                'on' => 'insert'
            ),
            array(
                'contactFullName',
                'validateFullName'
            ),
            array(
                'phonetic_name, spouse_first_name, spouse_last_name, spouse_phonetic_name, notes, contactTypesToAdd, addressToAdd, last_login, last_login_ip,lastLoginFrom,lastLoginTo,dateSearchFilter,fromSearchDate,toSearchDate',
                'safe'
            ),
            array(
                'account_id,  lead_type_ma, contact_status_ma, is_deleted',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'first_name, last_name',
                'length',
                'max' => 50
            ),
            array(
                'password, session_id',
                'length',
                'max' => 127
            ),
            array(
                'notes',
                'length',
                'max' => 65535
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id,
                account_id,
                spouse_first_name,
                spouse_last_name,
                contactAddress,
                contactAttributeValue,
                contactEmails,
                contactTypes,
                contactTypesToAdd,
                contactPrimaryEmail,
                contactEmailSearch,
                contactPhoneSearch,
                contactAddressSearch,
                contactFullName,
                dateSearchFilter,
                fromSearchDate,
                toSearchDate,
                account_id,
                contact_status_ma,
                lead_type_ma,
                first_name,
                last_name,
                phonetic_name,
                spouse_phonetic_name,
                last_login,
                last_login_ip,
                lastLoginFrom,
                lastLoginTo,
                nextTaskDueDate,
                password,
                session_id,
                is_deleted',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'account'           => array(
                self::BELONGS_TO,
                'Accounts',
                'account_id'
            ),
            'addresses'         => array(
                self::MANY_MANY,
                'Addresses',
                'address_contact_lu(contact_id, address_id)',
                'condition' => 'addresses_addresses.is_deleted <> 1 OR addresses_addresses.is_deleted IS NULL'
            ),

            // TEMP until addresses condition gets fixed.
            'addressContactLu'  => array(
                self::HAS_MANY,
                'AddressContactLu',
                'contact_id',
                'condition' => 'is_deleted <> 1 OR is_deleted IS NULL'
            ),
            'assignments'       => array(
                self::HAS_MANY,
                'Assignments',
                'assignee_contact_id',
                // 'condition' => 'component_type_id = :component_type_id',
                // 'params' => array(':component_type_id' => ComponentTypes::CONTACTS)
            ),
            'emails'            => array(
                self::HAS_MANY,
                'Emails',
                'contact_id'
            ),
            'phones'            => array(
                self::HAS_MANY,
                'Phones',
                'contact_id'
            ),
            'types'             => array(
                self::MANY_MANY,
                'ContactTypes',
                'contact_type_lu(contact_id, contact_type_id)',
            ),
            'contactTypeLu'     => array(
                self::HAS_MANY,
                'ContactTypeLu',
                'contact_id'
            ),
            'attributeValues'   => array(
                self::HAS_MANY,
                'ContactAttributeValues',
                'contact_id',
//                'with'=>'attribute',
//                'condition'=>'attribute.is_deleted=0',
            ),
            'attributeList'     => array(
                self::MANY_MANY,
                'ContactAttributes',
                'contact_attribute_values(contact_id, contact_attribute_id)'
            ),
            'loginLogs'         => array(
                self::HAS_MANY,
                'LoginLog',
                'contact_id'
            ),
            'source'            => array(
                self::BELONGS_TO,
                'Sources',
                'source_id'
            ),
            'transactions'      => array(
                self::HAS_MANY,
                'Transactions',
                'contact_id',
                'condition' => 'component_type_id IN ('.ComponentTypes::BUYERS.','.ComponentTypes::SELLERS.')',
            ),
            'userGroups'        => array(
                self::MANY_MANY,
                'AuthItem',
                'auth_assignment(userid, itemname)'
            ),
            'userGroup'         => array(
                self::HAS_MANY,
                'AuthAssignment',
                'userid'
            ),
            'accountContactLu'  => array(
                self::HAS_MANY,
                'AccountContactLu',
                'contact_id'
            ),
            //Need to make sure this relationship is correct
            'actionPlanItems'   => array(
                self::MANY_MANY,
                'ActionPlans',
                'action_plan_applied_lu(component_id, action_plan_item_id)',
                'condition' => 'component_type_id = :component_type_id',
                'params'    => array(':component_type_id' => ComponentTypes::CONTACTS)
            ),
            'pagesViewed'       => array(
                self::HAS_MANY,
                'ViewedPages',
                'contact_id'
            ),
            'settingValues'     => array(
                self::HAS_MANY,
                'SettingContactValues',
                'contact_id'
            ),
            'savedHomeSearch'   => array(
                self::HAS_ONE,
                'SavedHomeSearches',
                'contact_id'
            ),
            'savedHomeSearches' => array(
                self::HAS_MANY,
                'SavedHomeSearches',
                'contact_id'
            ),
            'recruit'           => array(
                self::HAS_ONE,
                'Recruits',
                'contact_id'
            ),
            'appointment' => array(
                self::HAS_ONE,
                'Appointments',
                'component_id',
                'condition' => 'component_type_id = :component_type_id AND is_deleted=0',
                'params' => array(':component_type_id' => ComponentTypes::CONTACTS)
            ),
            'relationshipsTo'   => array(
                self::HAS_MANY,
                'ContactRelationshipLu',
                'origin_contact_id'
            ),
            'relationshipsFrom' => array(
                self::HAS_MANY,
                'ContactRelationshipLu',
                'related_to_contact_id'
            ),
            'pipelineComponentLu' => array(
                self::HAS_MANY,
                'PipelineComponentLu',
                'component_id'
            )
        );
    }

    /**
     * @return array behaviors for getTasks
     */
    public function behaviors()
    {
        return array(
            'getTasks'                        => array(
                'class' => 'admin_module.components.behaviors.GetTasksBehavior',
            ),
            'ContactTaskTrackingBehavior'       => array(
                'class' => 'admin_module.components.behaviors.contacts.ContactTaskTrackingBehavior',
            ),
            'ContactConversionTrackingBehavior' => array(
                'class' => 'admin_module.components.behaviors.contacts.ContactConversionTrackingBehavior',
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                => 'ID',
            'source_id'         => 'Source',
            'account_id'        => 'Account',
            'contact_status_ma' => 'Status',
            'lead_type_ma'      => 'Lead Type',
            'first_name'        => 'First Name',
            'last_name'         => 'Last Name',
            'phonetic_name'     => 'Phonetic Name',
            'last_login'        => 'Last Login',
            'last_login_ip'     => 'Last IP',
            'spouse_phonetic_name' => 'Spouse Phonetic Name',
            'password'          => 'Password',
            'passwordOld'       => 'Old Password',
            'passwordNew'       => 'New Password',
            'passwordConfirm'   => 'Confirm Password',
            'session_id'        => 'Session',
            'is_deleted'        => 'Deleted',
            'contactEmailAdd'   => 'Email',
            'primaryEmail'      => 'Email',
            'contactPhoneAdd'   => 'Phone',
            'contactFullName'   => 'Name',
            'fullName'          => 'Name',
            'date_of_birth'     => 'Date of Birth',
            'notes'             => 'Notes',
            'contactTypesToAdd' => 'Contact Types',
        );
    }

    public function getSettings()
    {
        if (!$this->settings && $this->settingValues) {
            foreach ($this->settingValues as $SettingValue) {
                $this->settings[$SettingValue->setting->name] = $SettingValue->value;
            }
        }

        return (object)$this->settings;
    }

    /**
     * hasEmail
     * Checks if a e-mail is assigned to the current contact
     *
     * @param  string $email The e-mail address string
     *
     * @return boolean
     */
    public function hasEmail($email)
    {
        if (!$this->emails) {
            return false;
        }

        if (!$this->cachedEmails) {
            foreach ($this->emails as $Email) {
                array_push($this->cachedEmails, $Email->email);
            }
        }

        return in_array($email, $this->cachedEmails);
    }

    /**
     * Return the primary e-mail address specified for a given contact OR the first
     * record if no primary can be located.
     *
     * @param string $defaultValue
     *
     * @return array|mixed|null|string
     */
    public function getPrimaryEmail($defaultValue = null)
    {
        $primaryEmail = $this->getPrimaryEmailObj();

        return (!$primaryEmail->email) ? $defaultValue : $primaryEmail->email;
    }

    public function getPrimaryEmailObj($activeStatusOnly=false)
    {
        $Criteria = new CDbCriteria;
        $Criteria->select = '*';
        $Criteria->condition = 'is_primary = 1 AND contact_id = :contact_id AND is_deleted=0';
        $Criteria->params = array(
            ':contact_id' => $this->id,
        );

        // If a contact has a primary e-mail use that, if not try to use the first e-mail on their account, otherwise display the default message
        $PrimaryEmail = Emails::model()->byIgnoreAccountScope()->find($Criteria);
        if (!$PrimaryEmail) {
            if ($this->emails[0]) {
                return $this->emails[0];
            }
        }

        return $PrimaryEmail;
    }

    public static function findByEmail($email)
    {
        return Emails::model()->byEmail($email)->find()->contact;
    }

    /**
     * Returns the primary phone number specified for a contact, or the first record found if no primary
     * phone number can be found.
     *
     * @param string $defaultValue
     *
     * @return string
     */
    public function getPrimaryPhone($defaultValue = '')
    {
        $PrimaryPhone = Phones::model()->primary()->find(
            array(
                'select'    => 'phone',
                'condition' => 'contact_id = :contact_id',
                'params'    => array(':contact_id' => $this->id),
            )
        );

        if ($PrimaryPhone) {
            return $PrimaryPhone->phone;
        } else {
            if ($this->phones[0]) {
                return $this->phones[0]->phone;
            }
        }

        return $defaultValue;
    }

    public function getPrimaryPhoneObj()
    {
        $PrimaryPhone = Phones::model()->primary()->find(
            array(
                'condition' => 'contact_id = :contact_id',
                'params'    => array(':contact_id' => $this->id),
            )
        );

        if ($PrimaryPhone) {
            return $PrimaryPhone;
        } else {
            if ($this->phones[0]) {
                return $this->phones[0];
            }
        }

        return $defaultValue;
    }

    public function getPrimaryAddress()
    {
        if (!$this->addresses) {
            return null;
        }

        $Addresses = $this->addresses;
        // If there is only one address use that as the primary  by default
        if (count($Addresses) == 1) {
            return $this->addresses[0];
        }

        // returns primary address, if nothing marked primary, returns the most recent address identified by max ID pk
        $altAddress = null;
        foreach ($Addresses as $Address) {
            if(empty($altAddress) || $altAddress->id < $Address->id) {
                $altAddress = $Address;
            }

            if ($Address->isPrimary) {
                return $Address;
            }
        }

        // if no primary address but alternate address exists
        if(!empty($altAddress)) {
            return $altAddress;
        }

        return null;
    }

    public function getComponentType()
    {
        if (!isset($this->_componentType)) {
            $this->_componentType = ComponentTypes::model()->findByPk(ComponentTypes::CONTACTS);
        }

        return $this->_componentType;
    }

    /**
     * getFullName
     * Returns first and last name of contact with a space in between
     *
     * @return mixed
     */
    public function getFullName()
    {
        $fullName = $this->first_name . ' ' . $this->last_name;

        return $fullName;
    }

    /**
     * getFullName
     * Returns first and last name of contact with a space in between
     *
     * @return mixed
     */
    public function getFullPhoneticName()
    {
        $fullName = $this->first_name . ' ' . $this->last_name.(($this->phonetic_name) ? ' ('.$this->phonetic_name.')' : '');

        return $fullName;
    }

    /**
     * getSpouseFullName
     * Returns first and last name of contact with a space in between
     *
     * @return mixed
     */
    public function getSpouseFullName()
    {
        $fullName = trim($this->spouse_first_name);
        $fullName .= (!empty($fullName)) ? ' ' . trim($this->spouse_last_name) : '';

        return $fullName;
    }

    /**
     * getLastLogin
     * Finds the last login for a contact
     *
     * @return datetime
     */
    public function getLastLogin()
    {
        $LastLogin = LoginLog::model()->mostRecent($this->id, 1);
        $LastLogin = ($LastLogin) ? Yii::app()->format->formatDaysTime($LastLogin->datetime) : 'None';

        return $LastLogin;
    }

    /**
     * getAddedDateTimegetNextTask
     * Finds the last login for a contact
     *
     * @return datetime
     */
    public function getAddedDateTime()
    {
        $added = ($this->added) ? Yii::app()->format->formatDateDays($this->added) : 'None';

        return $added;
    }

    public function getContactTypes()
    {
        $Criteria = new CDbCriteria;
        $Criteria->with = array('contactTypeLus','contactTypeLus.type');
        $Criteria->condition = 'contactTypeLus.is_deleted = 0 and contactTypeLus.contact_id = :contact_id';
        $Criteria->params = array(
            ':contact_id' => $this->id,
        );

        $contactTypes = new ContactTypes();
        return $contactTypes->findAll($Criteria);
    }

    public function getNextTask($maxDate='30 days')
    {
        $Criteria = new CDbCriteria;
        $Criteria->condition
            = 'component_type_id=:component_type_id AND component_id=:component_id AND due_date>=:due_date AND due_date<=:due_date_max';
        $Criteria->params = array(
            ':component_type_id' => ComponentTypes::CONTACTS,
            ':component_id'      => $this->id,
            ':due_date'          => date('Y-m-d'), //H:i:s
            ':due_date_max'      => date("Y-m-d H:i:s", strtotime($maxDate))
        );
        $Criteria->order = 'due_date asc';

        return Tasks::model()->find($Criteria);
    }

    /**
     * getTotalCount for the Account
     *
     * @return int $count
     */
    public function getTotalCount()
    {
        $accountId = Yii::app()->user->accountId;
        return $this->countBySql('select count(*) from contacts where account_id='.$accountId);
    }

    /**
     * getLeadTypesList
     * Returns array of Lead Types
     *
     * @return string
     */
    public static function getLeadTypesList()
    {
        return array(
            self::LEAD_TYPE_BUYER_ID   => 'Buyer',
            self::LEAD_TYPE_SELLER_ID  => 'Seller',
            self::LEAD_TYPE_RECRUIT_ID => 'Recruit',
        );
    }

    public function getProfileAttributeList()
    {
        return ContactAttributeValues::model()->getProfileAttributeList($this->id);
    }

    /**
     * getGroupType
     * Returns string name of Group Type
     *
     * @return string
     */
    public function getLeadType()
    {
        $leadTypes = Contacts::getLeadTypesList();

        return $leadTypes[$this->lead_type_ma];
    }

    public static function updateLastLogin($Contact = null)
    {
        if ($Contact == null) {
            $Contact = Yii::app()->user->contact;
        }

        if ($Contact instanceof Contacts) {
            $Contact->last_login = new CDbExpression('NOW()');
            $Contact->last_login_ip = Yii::app()->stmFunctions->getVisitorIp();

            return $Contact->save(false); // don't care about validating this change
        }

        // update last_login
        if (!Yii::app()->user->isGuest) {
            $Contact = Contacts::model()->findByPk(Yii::app()->user->id);
            $Contact->last_login = date('Y-m-d H:i:s');
            $Contact->last_login_ip = Yii::app()->stmFunctions->getVisitorIp();

            return $Contact->save(false);
        }
    }

    public function getAveragePriceViewed()
    {
        if (!$this->id) {
            return;
        }

        $boardName = Yii::app()->user->getBoardName();
        $AveragePrice = ViewedPages::model()->byContactId($this->id)->byPageType(ViewedPages::HOME_PAGES)->find(
            array(
                'select' => 'AVG(mp.price) as averagePriceViewed',
                'join'   => 'INNER JOIN stm_mls.' . $boardName . '_properties mp ON t.listing_id = mp.listing_id',
            )
        );

        if (!$AveragePrice) {
            return null;
        }

        return Yii::app()->format->formatDollars($AveragePrice->averagePriceViewed);
    }

    /**
     * @todo should remove this need to check for usage CW
     * @return $this
     */
    public function getContact()
    {
        return $this;
    }

    public function scopes()
    {
        return array(
            'primary'     => array(
                'condition' => 'is_primary = 1 and contact_id = :contact_id and account_id = :account_id',
                'params'    => array(
                    ':contact_id' => $this->id,
                    ':account_id' => $this->account_id,
                ),
                'limit'     => 1,
            ),
            'orderByName' => array(
                'order' => 'first_name ASC'
            ),
        );
    }

    /**
     * getIsAgent
     * Returns a boolean based on if the current contact is a agent
     *
     * @author Chris Willard
     * @since  4/30/2013
     * @return bool
     */
    public function getIsAgent()
    {

        if (!$this->userGroups) {
            return false;
        } else {
            return true;
        }
    }

    public function getItemNames()
    {
        return Yii::app()->db->createCommand("select itemname from auth_assignment where userid=".$this->id)->queryColumn();
    }

    /**
     * Return a scope filtering users by agents, owners, or staff members
     *
     * @param      $together  Includes the userGroup result in the query
     * @param      $contactId If a admin has been removed, need easy reusable way to bring them in the selection if need be
     *
     * @param bool $together
     *
     * @return Contacts
     */
    public function byAdmins($together = false, $contactIds = null, $authItemName = null, $accountId=null)
    {

        $contactTableAlias = $this->getTableAlias($quote = false, $checkScopes = true);
        $adminCriteria = new CDbCriteria(array(
            'with'      => array('userGroups'),
            'condition' => 'itemname IS NOT NULL AND type=' . CAuthItem::TYPE_ROLE, // && contact_status_ma = :status
            //            'params' => array(':status' => self::STATUS_ACTIVE),
            'together'  => $together,
            'group'     => $contactTableAlias . '.id',
        ));

        if($accountId) {
            $adminCriteria->addCondition('userGroups_userGroups.account_id='.intval($accountId));
        }

        if ($authItemName == 'allActive') {

            // hack because with the new many-many relationship, it makes using cdbCriteria very difficult. This is a work around.
            $inactiveIds = Yii::app()->db->createCommand("select userid from auth_assignment WHERE itemname='inactive'")->queryColumn();
            $adminCriteria->addNotInCondition('t.id', $inactiveIds);

        } elseif (!empty($authItemName) && $authItemName != '*') {
            $authItemName = StmFormatter::formatToArray($authItemName);
            $adminCriteria->addInCondition('itemname', $authItemName);

        }
        /* elseif($authItemName != '*') {
                        if(empty($authItemName)) {
                            $adminCriteria->addInCondition('itemname', array(
                                                                       'admin',
                                                                       'agent',
                                                                       'owner',
                                                                       'staff'
                                                                       )
                            );
                        }
                    }*/

        // Include a specific contact (used when a view needs to display a agent that no longer has a role and is part of the data set)
        if ($contactIds) {
            if (!is_array($contactIds)) {
                $contactIds = array($contactIds);
            }
            $adminCriteria->addInCondition("$contactTableAlias.id", $contactIds, 'OR');
        }

        $this->getDbCriteria()->mergeWith($adminCriteria);

        return $this;
    }

    public function byActiveAdmins($together = false, $contactId = null)
    {
        $this->byAdmins($together, $contactId, 'allActive');
        return $this;
    }

    public function byNotAdmin()
    {
        $Criteria = new CDbCriteria(array(
            'with'      => 'userGroups',
            'condition' => 'itemname IS NULL',
            'together'  => true,
        ));
        $this->getDbCriteria()->mergeWith($Criteria);
        return $this;
    }

    public function byAssignmentTypeSetting($assignmentTypeIds)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'with'      => 'settingValues',
                'condition' => 'setting_id=:setting_id',
                'params'    => array(':setting_id' => Settings::SETTING_ASSIGNMENT_TYPE_ID),
                'together'  => true,
            )
        );

        if (is_array($assignmentTypeIds) && !empty($assignmentTypeIds)) {
            $this->getDbCriteria()->addInCondition('value', $assignmentTypeIds);
        }

        return $this;
    }

    public function byAssignmentComponentType($componentTypeIds)
    {
        $componentTypeIds = (array) $componentTypeIds;
        $this->getDbCriteria()->addInCondition('assignments.component_type_id', $componentTypeIds);
        $this->getDbCriteria()->mergeWith(
            array(
                'with'      => 'assignments',
            )
        );

        return $this;
    }

    public function byDisplayOnWebsite()
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'with'      => 'settingValues',
                'condition' => 'settingValues.setting_id=:setting_id AND settingValues.value=:value',
                'params'    => array(
                    ':setting_id' => SettingContactValues::SETTING_PROFILE_ON_WEBSITE,
                    ':value'      => 1
                ),
            )
        );

        return $this;
    }

    public function searchBeforeAdd()
    {
        $alias = $this->getTableAlias($quote = false, $checkScopes = true);

        $criteria = new CDbCriteria;
        $criteria->compare($alias . '.id', $this->id);

        if ($this->first_name) {
            $criteria->compare('first_name', trim($this->first_name), true, 'OR');
            $criteria->compare('spouse_first_name', trim($this->spouse_first_name), true, 'OR');
        }

        if ($this->last_name) {
            $criteria->compare('last_name', trim($this->last_name), true, 'OR');
            $criteria->compare('spouse_last_name', trim($this->spouse_last_name), true, 'OR');
        }

        $criteria->with = array('emails');
        //Search by phone number when applicable.
        if ($this->phones->phone) {
            $scrubbedPhoneNumber = trim(Yii::app()->format->formatInteger($this->phones->phone));
            $criteria->compare('phones.phone', $scrubbedPhoneNumber, true, 'OR');
            $criteria->with = CMap::mergeArray($criteria->with, array('phones'));
            $criteria->together = true;
        }

        //Search by email address when applicable.
        if ($this->emails->email) {
            $criteria->together = true;
            $criteria->compare('emails.email', trim($this->emails->email), true, 'OR');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search( $returnAll = false)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('t.account_id', Yii::app()->user->accountId);

        $criteria->compare('t.id', $this->id);
        if ($this->first_name) {
            $criteria->compare('first_name', trim($this->first_name), true);
            $criteria->compare('spouse_first_name', trim($this->first_name), true, 'OR');
        }
        if ($this->last_name) {
            $criteria->compare('last_name', trim($this->last_name), true);
            $criteria->compare('spouse_last_name', trim($this->last_name), true, 'OR');
        }

        if ($this->contactFullName) {
            $criteria->compare("CONCAT(first_name, ' ', last_name)", trim($this->contactFullName), true);
        }

        $criteria->compare('lead_type_ma', $this->lead_type_ma, true);
        $criteria->compare('t.contact_status_ma', $this->contact_status_ma);
        $criteria->compare('t.last_login', $this->last_login);
        $criteria->compare('t.last_login_ip', $this->last_login_ip);

        $criteria->compare('t.is_deleted', 0); // Prevent soft deleted contacts from appearing

        $criteria->together = true;

        if ($this->dateSearchFilter && ($this->dateSearchTypes[$this->dateSearchFilter] || $this->dateSearchFilter == 'demo_date_asc' || $this->dateSearchFilter == 'demo_date_desc')) {
            $fromSearchDate = Yii::app()->format->formatDate($this->fromSearchDate, StmFormatter::MYSQL_DATETIME_FORMAT);
            $toSearchDate = Yii::app()->format->formatDate($this->toSearchDate, StmFormatter::MYSQL_DATETIME_FORMAT);

            // @todo the idea here will be to create a filter that returns a cdbcriteria that can be merged here
            // for the time being just use a switch statement
            switch($this->dateSearchFilter) {
                case 'last_login_asc':
                case 'last_login_desc':
                    if ($fromSearchDate) {
                        $criteria->addCondition('DATE(t.last_login) >= :fromSearchDate');
                        $criteria->params = CMap::mergeArray($criteria->params, array(':fromSearchDate'=>$fromSearchDate)); //(empty($criteria->params)) ? : ;
                    }
                    if ($toSearchDate) {
                        $criteria->addCondition('DATE(t.last_login) <= :toSearchDate');
                        $criteria->params = CMap::mergeArray($criteria->params, array(':toSearchDate'=>$toSearchDate));
                    }
                    $sortOrder = ($this->dateSearchFilter == 'last_login_asc')? 'asc' : 'desc';
                    $criteria->order = "t.last_login $sortOrder";
                    break;

                case 'added_asc':
                case 'added_desc':
                    if ($fromSearchDate) {
                        $criteria->addCondition('DATE(t.added) >= :fromSearchDate');
                        $criteria->params = CMap::mergeArray($criteria->params, array(':fromSearchDate'=>$fromSearchDate)); //(empty($criteria->params)) ? : ;
                    }
                    if ($toSearchDate) {
                        $criteria->addCondition('DATE(t.added) <= :toSearchDate');
                        $criteria->params = CMap::mergeArray($criteria->params, array(':toSearchDate'=>$toSearchDate));
                    }
                    $sortOrder = ($this->dateSearchFilter == 'added_asc')? 'asc' : 'desc';
                    $criteria->order = "t.added $sortOrder";
                    break;

                // this is for HQ only
                case 'demo_date_asc':
                case 'demo_date_desc':
                    $demoSubQuery = "(SELECT activity_date FROM activity_log al WHERE al.task_type_id=".TaskTypes::LIVE_DEMO." AND al.component_type_id=1 AND al.component_id=t.id ORDER BY activity_date DESC LIMIT 1)";
                    if ($fromSearchDate) {
                        $criteria->addCondition('DATE('.$demoSubQuery.') >= :fromSearchDate');
                        $criteria->params = CMap::mergeArray($criteria->params, array(':fromSearchDate'=>$fromSearchDate));
                    }
                    if ($toSearchDate) {
                        $criteria->addCondition('DATE('.$demoSubQuery.') <= :toSearchDate');
                        $criteria->params = CMap::mergeArray($criteria->params, array(':toSearchDate'=>$toSearchDate));
                    }
                    $sortOrder = ($this->dateSearchFilter == 'demo_date_asc')? 'asc' : 'desc';
                    $criteria->order = "$demoSubQuery $sortOrder";
                    break;

//                case 'last_activity_asc':
//                case 'last_activity_desc':
//                    $criteria->with = CMap::mergeArray($criteria->with, array('activityLogs'));
//                    if ($fromSearchDate && $toSearchDate) {
//                        $criteria->addBetweenCondition('activityLogs.activity_date', $fromSearchDate, $toSearchDate);
//                    }
//                    $sortOrder = ($this->dateSearchFilter == 'last_activity_asc')? 'asc' : 'desc';
//                    $criteria->order = "activityLogs.activity_date $sortOrder";
//                    $criteria->together = true;
//                    break;
            }
        }

        //Search by contact types
        if ($this->contactTypeLu->idCollection) {

            if($this->contactTypeLu->idCollection) {
                $criteria->with = CMap::mergeArray($criteria->with, array('contactTypeLu'));
                $criteria->addInCondition('contactTypeLu.contact_type_id', (array) $this->contactTypeLu->idCollection);
            }
        }

        if($this->contactTypeLu->excludeIds) {
            $excludeIds = implode(',',$this->contactTypeLu->excludeIds);
            $criteria->addCondition('(select count(ctlu.id) FROM contact_type_lu ctlu WHERE ctlu.contact_id=t.id AND ctlu.contact_type_id IN('.$excludeIds.')) < 1');
        }

        if ($this->source->idCollection || $this->source->excludeIds) {
            $criteria->with = CMap::mergeArray($criteria->with, array('source'));

            if($this->source->idCollection) {
                $criteria->addInCondition('source.id', $this->source->idCollection);
            }
            if ($this->source->excludeIds) {

                $excludeSourceIds = implode(',',$this->source->excludeIds);
                $criteria->addCondition('t.source_id NOT IN('.$excludeSourceIds.') OR t.source_id IS NULL');
            }
        }

        //Search by email address when applicable.
        if ($this->emails->email) {
            $criteria->with = CMap::mergeArray($criteria->with, array('emails'));
            $criteria->compare('emails.email', trim($this->emails->email), true);
        }

        if ($this->addresses->address) {
            $criteria->with = CMap::mergeArray($criteria->with, array('addresses'));
            $criteria->compare('addresses.address', trim($this->addresses->address), true);
        }

        if ($this->addresses->zip) {
            $criteria->with = CMap::mergeArray($criteria->with, array('addresses'));
            $criteria->compare('addresses.zip', trim($this->addresses->zip), true);
        }

        //Search by phone number when applicable.
        if ($this->phones->phone) {
            $scrubbedPhoneNumber = Yii::app()->format->formatInteger($this->phones->phone);
            $criteria->compare('phones.phone', $scrubbedPhoneNumber, true);
            $criteria->with = CMap::mergeArray($criteria->with, array('phones'));
        }

		if(!empty($this->noActionPlan)){
			$criteria
				->addCondition("(
						SELECT count(*) FROM tasks tsk 
							LEFT JOIN action_plan_applied_lu lu ON tsk.id=lu.task_id 
							LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id 
							LEFT JOIN action_plans ap ON ap.id=i.action_plan_id 
						WHERE tsk.component_type_id=".ComponentTypes::CONTACTS." 
							AND tsk.component_id= t.id 
							AND tsk.complete_date IS NULL 
							AND tsk.is_deleted=0 
						ORDER BY ap.name ASC
						) < 1","AND");
		}

		if(!empty($this->noTask)){
			$criteria->addCondition("(
					SELECT count(*) FROM tasks tsk2 
					WHERE tsk2.component_type_id=".ComponentTypes::CONTACTS." 
					AND tsk2.component_id=t.id 
					AND tsk2.complete_date IS NULL 
					AND tsk2.is_deleted=0) < 1","AND");
		}

		$params = array(
			"criteria" => $criteria,
			"sort"=>array("defaultOrder"=>"t.id DESC")
		);
		if(empty($returnAll)){
			$params["pagination"] = array("pageSize"=> 50);
		}
        $searchProvider = new CActiveDataProvider($this, $params);
        $count = $this->count($criteria);

		if($returnAll && (is_bool($returnAll)) || $returnAll === "1") {
			$searchProvider->pagination = false;
		}elseif(is_numeric($returnAll) && $returnAll > 1){
			$searchProvider->pagination->setItemCount($returnAll);
		}else{
			$searchProvider->setTotalItemCount($count);
		}
        return $searchProvider;
    }

    public function getActiveActionPlan()
    {
        $actionPlanNames = Yii::app()->db->createCommand("select a.name from tasks t LEFT JOIN action_plan_applied_lu lu ON t.id=lu.task_id LEFT JOIN action_plan_items a2 ON a2.id=lu.action_plan_item_id LEFT JOIN action_plans a ON a.id=a2.action_plan_id WHERE t.component_type_id=".ComponentTypes::CONTACTS." AND component_id=".$this->id." AND t.is_deleted=0 AND lu.is_deleted=0 GROUP BY a.name")->queryColumn();
        foreach($actionPlanNames as $actionPlanName) {
            $names .= ($names) ? ', '.$actionPlanName : $actionPlanName;
        }
        return ($names)? $names : '-';
    }

    /**
     * Validate the full name for the contact.
     *
     * @param  string $attribute The attribute name
     * @param  array  $params    Parameters for the validation
     *
     * @return null
     */
    public function validateFullName($attribute, $params)
    {
        if (!$this->first_name || !$this->last_name) {
            $this->addError($attribute, 'Please supply the entire name for the contact.');
        }

        if (!$this->first_name) {
            $this->addError('first_name', 'Please enter your First Name.');
        }

        if (!$this->last_name) {
            $this->addError('last_name', 'Please enter your Last Name.');
        }
    }

    public function validateEmailOrPhone($attribute, $params)
    {
        if (!$this->contactEmailAdd && !$this->contactPhoneAdd) {
            $this->addError($attribute, 'Contact must have an Email or Phone.');
        }
    }

    protected function beforeValidate()
    {

        // Trim the first name and last name fields
        $this->first_name = trim($this->first_name);
        $this->last_name = trim($this->last_name);
        $this->phonetic_name = trim($this->phonetic_name);
        $this->spouse_first_name = trim($this->spouse_first_name);
        $this->spouse_last_name = trim($this->spouse_last_name);
        $this->spouse_phonetic_name = trim($this->spouse_phonetic_name);

        // if new record + primary first/last name empty but spouse info exists, move spouse to primary name field
        if($this->isNewRecord && !$this->first_name && !$this->last_name && $this->spouse_first_name && $this->spouse_last_name) {
            $this->first_name = $this->spouse_first_name;
            $this->last_name = $this->spouse_last_name;
            $this->phonetic_name = $this->spouse_phonetic_name;
        }

        $this->password = trim($this->password);

        $this->contactFullName = $this->first_name . ($this->last_name)? " " . $this->last_name : "";

        return parent::beforeValidate();
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->contact_status_ma = self::STATUS_ACTIVE;
            $this->added = new CDbExpression('NOW()');

            if($this->scenario !== 'addWithoutLastLogin') {
                $this->last_login = new CDbExpression('NOW()');
            }

            if (ctype_lower($this->first_name[0])) {
                $this->first_name = ucwords($this->first_name);
            }

            if (ctype_lower($this->last_name[0])) {
                $this->last_name = ucwords($this->last_name);
            }

            // $this->added_by = Yii::app()->user->id; // Need to account for system generated! and NOT prospect if it's them!

            if (empty($this->password)) {
                $this->password = Yii::app()->passwordGenerator->generate();
            }
        }

        return parent::beforeSave();
    }

    /**
     * generatePassword
     * Generates a unique password for the user by creating a unique salt and salting the user's id (pk)
     *
     * @author Chris Willard
     * @since  3/6/2013
     *
     * @param  boolean $checkForDuplicates   (Not yet implemented) @todo: Implement feature to prevent the system from possibly using the same pw
     * @param  integer $passwordSuffixLength The password suffix length
     *
     * @return string The password generated
     */
    public function generatePassword($checkForDuplicates = false, $passwordSuffixLength = 6)
    {

        $passwordCharsLCase = "abcdefghijkmnopqrstwxyz";
        $passwordCharsUCase = "ABCDEFGHJKLMNPQRSTWXYZ";
        for ($length = 0; $length < $passwordSuffixLength; $length++) {

            $randomInteger = rand(0, 9);
            $passwordSuffix .= (string)$randomInteger;
        }

        return $passwordSuffix;
    }

    /**
     * Returns the appropriate enciphered hash value for this contact provided they actually have a valid contact id.
     *
     * @return null
     */
    public function getHash()
    {
        $hash = null;

        if ($this->id) {
            Yii::app()->getSecurityManager()->setEncryptionKey(self::HASH_SECRET_KEY);
            $hash = Yii::app()->getSecurityManager()->encrypt($this->id);
        }

        return $hash;
    }

    /**
     * Utility method for attempting to decipher a hashed value and return from it a contact Id if applicable.
     *
     * @param $contactIdHash        the enciphered value from which to attempt to extract a contact id.
     *
     * @return null|string the contact id as a string if this hash is representative of a contact in our system,
     *                     null otherwise.
     */
    public static function unHash($contactIdHash)
    {

        Yii::app()->getSecurityManager()->setEncryptionKey(self::HASH_SECRET_KEY);
        $unencryptedValue = Yii::app()->getSecurityManager()->decrypt($contactIdHash);

        return $unencryptedValue;
    }

    public function getPrimaryContactType()
    {
        $primaryType = null;

        if (!empty($this->types)) {
            $primaryType = current($this->types);
            return $primaryType;
        } else {
            $roles = Yii::app()->authManager->getRoles($this->id);
            if ($roles) {
                if (isset($roles['agent']) or isset($roles['owner'])) {
                   $primaryType = ContactTypes::model()->findByPk($agentTypeId=2);
                }
            }
        }

        return $primaryType;
    }

    /**
     * getAll
     * Loads activeDataProvider with conditions and returns ready for use (ex. CGridview)
     *
     * @param int $contactId
     *
     * @return mixed activeDataProvider
     */
    public static function getAll($contactId)
    {
        $Criteria = new CDbCriteria;
        $Criteria->condition = 'contact_id=:contact_id';
        $Criteria->params = array(':contact_id' => $contactId);

        $TransactionsProvider = new CActiveDataProvider('Transactions', array(
            'criteria' => $Criteria
        ));

        return $TransactionsProvider;
    }

    public function byType($contactTypeIds = array())
    {
        if (count($contactTypeIds) > 0) {
            $Criteria = new CDbCriteria;
            $Criteria->with = array('contactTypeLu');
            $Criteria->addInCondition('contact_type_id', $contactTypeIds);

            $this->getDbCriteria()->mergeWith($Criteria);
        }

        return $this;
    }

    public function getContactId()
    {
        return $this->id;
    }

    /**
     * @param $attributeId integer the id of the attribute whose
     *
     * @return mixed the value of the attribute retrieved, null if unset.
     */
    public function getAttributeValue($attributeId)
    {
        if (count($this->_attributeValuesById) === 0) {
            $this->initializeAttributeCache();
        }

        $attributeValue = new ContactAttributeValues();
        $attributeValue->contact_attribute_id = $attributeId;
        $attributeValue->value = null;

        if (array_key_exists($attributeId, $this->_attributeValuesById)) {
            $attributeValue = $this->_attributeValuesById[$attributeId];
        }

        return $attributeValue;
    }

    private function initializeAttributeCache()
    {
        $attributeValues = $this->attributeValues;

        if (isset($attributeValues)) {
            foreach ($attributeValues as $attributeValue) {
                $this->_attributeValuesById[$attributeValue->contact_attribute_id] = $attributeValue;
            }
        }
    }

    public function getComponentChain()
    {
        $chain = array($this->asComponentTuple());

        $includeCompleted = true;
        $Tasks = $this->getTasks($this, $includeCompleted);

        if (count($Tasks) > 0) {
            foreach ($Tasks as $Task) {
                array_push($chain, $Task->asComponentTuple());
            }
        }

        return $chain;
    }

    public function asComponentTuple()
    {
        return array(
            'componentTypeId' => $this->componentType->id,
            'componentId'     => $this->id
        );
    }

    /**
     * @todo: View related code should not be in the model
     *
     * @param $componentTypeId
     *
     * @return string
     */
    public function existingComponentRecordButton($componentTypeId, $break=true)
    {
        $data = '';
        switch ($componentTypeId) {
            case ComponentTypes::CONTACTS:
            case ComponentTypes::BUYERS:
            case ComponentTypes::SELLERS:
                $Transactions = $this->transactions;
                if ($Transactions) {
                    foreach ($Transactions as $Transaction) {
                        $data .= (empty($data) || !$break) ? '' : '<br />';
                        $data
                            .=
                            '<a href="/' . Yii::app()->controller->module->id . '/' . $Transaction->componentType->name
                            . '/' . $Transaction->id
                            . '" class="button gray icon i_stm_'.strtolower($Transaction->componentType->singularName).'_opportunity grey-button" style="width: 120px; text-align: left;" target="_blank">'
                            . $Transaction->componentType->singularName .  ' (' //': #' . $Transaction->id .
                            . $Transaction->status->name . ')</a>';
                    }
                }
                break;
            case ComponentTypes::RECRUITS:
                $Recruit = $this->recruit;
                if ($Recruit) {
                    $data .= (empty($data)) ? '' : '<br />';
                    $data
                        .= '<a href="/' . Yii::app()->controller->module->id . '/' . $Recruit->componentType->name . '/'
                        . $Recruit->id . '" class="button gray icon i_stm_edit grey-button" style="width: 120px; text-align: left;">View '
                        . $Recruit->componentType->singularName . ': #'
                        . $Recruit->id . ' (' . $Recruit->status->name . ')</a>';
                }
                break;
        }

        return $data;
    }

    public function lastSpokeDate($componentTypes = null)
    {
        // @todo: need to handle $componentTypes
        $activityLog = ActivityLog::model()->activityDateDesc()->findbyAttributes(
            array('component_type_id' => ComponentTypes::CONTACTS, 'component_id' => $this->id, 'is_spoke_to' => 1)
        );

        return ($activityLog) ? $activityLog->activity_date : null;
    }

    public function lastCalledDate($componentTypes = null)
    {
        // @todo: need to handle $componentTypes
        $activityLog = ActivityLog::model()->activityDateDesc()->findbyAttributes(
            array('component_type_id' => ComponentTypes::CONTACTS, 'component_id' => $this->id, 'task_type_id' => TaskTypes::PHONE)
        );

        return ($activityLog) ? $activityLog->activity_date : null;
    }

    public function hasStoryboard()
    {
        return (CmsContentContactLu::model()->count(
            array('condition' => 'contact_id=:contact_id', 'params' => array(':contact_id' => $this->id))
        )) ? true : false;
    }

    public function getProfilePhotoUrl()
    {
        $photoUrl = '';
        if (!empty($this->getSettings()->profile_photo)) {
            $photoUrl
                =
                Yii::app()->user->getProfileImageBaseUrl() . DS . $this->id . DS . $this->getSettings()->profile_photo;
        } else {
            $photoUrl = Yii::app()->user->getProfileImageBaseUrl() . DS . 'default_profile_photo.jpg';
        }

        return $photoUrl;
    }

    public function getProfilePhotoPath()
    {
		// If we don't have this info available, then this function can not return a proper value
		if (empty($this->getSettings()->profile_photo)) {
			throw new Exception(__CLASS__."(:".__LINE__.") This doesn't work");
		}

		// Return the profile photo path
		return Yii::app()->user->getProfileImageBaseFilepath() . DS . $this->id . DS . $this->getSettings()->profile_photo;
    }

	public function ensureProfilePhotoPathExists()
	{
		// Check if the sub-directory exists, if it doesn't, create it.
		if(!is_dir(Yii::app()->user->getProfileImageBaseFilepath() . DS . $this->id)) {
			if(!mkdir(Yii::app()->user->getProfileImageBaseFilepath() . DS . $this->id, 0777, true)) {

				// Log error
				Yii::log('Unable to create directory.', CLogger::LEVEL_ERROR);

				// Throw exception
				throw new Exception("Unable to create directory");
			}
		}
	}

    public function getExportData($contacts)
    {
        if(YII_DEBUG) {
            ini_set('memory_limit', '-1');
            set_time_limit (0);
        }

        if(empty($contacts)) {
            return null;
        }

        $exportData = "Contact ID\t"."First name\t". "Last Name\t". "Spouse First Name\t". "Spouse Last Name\t"."Email 1\t". "Email 2\t". "Email 3\t". "Email 4\t". "Phone 1\t". "Phone 2\t". "Phone 3\t". "Phone 4\t". " Primary Address\t". "City\t"."State\t"."Zip\n";
        foreach( $contacts as $key => $contact ) {
            // populate emails
            $emailList = '';
            $maxEmails = 4;
            if($emails = $contact->emails) {
                foreach($emails as $key => $email) {
                    if($key < $maxEmails) {
                        $emailList .= $email->email."\t";
                    }
                }
                if(count($emails) < $maxEmails) {
                    $tabsToAdd = $maxEmails-count($emails);
                    $emailList .= $this->addFillerTabs($tabsToAdd);
                }
            } else {
                $emailList .= $this->addFillerTabs($maxEmails);
            }

            // populate phones
            $phoneList = '';
            $maxPhones = 4;
            if($phones = $contact->phones) {
                foreach($phones as $key => $phone) {
                    if($key < $maxPhones) {
                        $phoneList .= $phone->phone."\t";
                    }
                }
                if(count($phones) < $maxPhones) {
                    $tabsToAdd = $maxPhones-count($phones);
                    $phoneList .= $this->addFillerTabs($tabsToAdd);
                }
            } else {
                $phoneList .= $this->addFillerTabs($maxEmails);
            }

            $address = '';
            if($contact->primaryAddress) {
                $address = $contact->primaryAddress->address . "\t";
                $address .= $contact->primaryAddress->city . "\t";
                $address .= AddressStates::getShortNameById($contact->primaryAddress->state_id) . "\t";
                $address .= $contact->primaryAddress->zip;
            }

            $exportData .= $contact->id. "\t". $contact->first_name. "\t". $contact->last_name. "\t".$contact->spouse_first_name. "\t".$contact->spouse_last_name. "\t". $emailList. $phoneList. $address. "\n";
            unset($contacts[$key]);
        }

        return $exportData;
    }
    protected  function addFillerTabs($count) {
        $tabs = "";
        for($i=0; $i<$count; $i++) {
            $tabs .= "\t";
        }
        return $tabs;
    }

    public static function getAdminNameById($id)
    {
        /*$api_cache_id = 'AdminContacts';
        $cache = Yii::app()->fileCache->get($api_cache_id);
        if(empty($cache) || !isset($cache[$id])) {
            $contact = new Contacts;
            $contact->setSoftDeleteCheck(false);
            if($contact = $contact->findByPk($id)) {
                if(empty($cache)) {
                    $contactArray = array();
                    $contactArray[$id] = $contact;
                    //Yii::app()->fileCache->set($api_cache_id , $contactArray, 60*60*12);
                } else {
                    $cache[$id] = $contact;
                    Yii::app()->fileCache->set($api_cache_id , $cache, 60*60*12);
                }
            }
            $cache = Yii::app()->fileCache->get($api_cache_id);
        }
        return $cache[$id]->fullName;*/

        $contact = new Contacts;
        $contact->setSoftDeleteCheck(false);
        $contact = $contact->findByPk($id);

        if(empty($contact)) {
            return "";
        }

        return $contact->fullName;
    }

    /**
     * Get Contacts For Auto Saved Search
     *
     * Retrieves contacts that need an update for the auto saved search
     * @param mixed $startDate Anything that can be strtotime'd
     * @param mixed $endDate Anything that can be strtotime'd
     * @return mixed Array of minimal contact data
     */
    public function getContactsForAutoSavedSearch($startDate = null, $endDate = null)
    {
        // Hardcoded option for buyer/seller only
        $buyerSellerOnly = true;
        $skipNoPageViews = false;

        // Increase GROUP_CONCAT limit to avoid truncating
        Yii::app()->db->createCommand('SET SESSION group_concat_max_len = 1000000;')->query();

        // Create command
        $command = Yii::app()->db->createCommand()
            ->select('c.id, e.email, GROUP_CONCAT(vp.listing_id) listing_ids, shs.id auto_search_id')
            ->from('contacts c')
                ->leftJoin('saved_home_searches shs', 'c.id = shs.contact_id')
                ->leftJoin('emails e', 'c.id = e.contact_id')
                ->leftJoin('viewed_pages vp', 'vp.contact_id = c.id')
            ->where('shs.id IS NULL OR shs.is_auto_criteria = 1')
                ->andWhere('c.is_deleted = 0')
                ->andWhere('e.is_primary = 1')
            ->group('c.id')
            ->limit(100);

        // Add this filter if buyer/seller only
        if($buyerSellerOnly) {
            $command->leftJoin('transactions t', 't.contact_id = c.id')
                ->andWhere(array('in', 't.component_type_id', array(ComponentTypes::BUYERS, ComponentTypes::SELLERS)));
        }

        // Add in the option to skip contacts without any page view data
        if(!$skipNoPageViews) {
            $command->andWhere('vp.listing_id IS NOT NULL');
        }

        // Add in start date, if we want that
        if($startDate) {
            $command->andWhere('vp.added > :startDate', array(':startDate' => date('Y-m-d H:i:s', strtotime($startDate))));
        }

        // Add in end date, if we want that
        if($endDate) {
            $command->andWhere('vp.added < :endDate', array(':endDate' => date('Y-m-d H:i:s', strtotime($endDate))));
        }

        // Run query and return results
        return $command->queryAll();
    }

    public static function getAccountOwners()
    {
//        $criteria = new CDbCriteria();
//        $criteria
//        return Contacts::model()->findAll($criteria);
    }
}
