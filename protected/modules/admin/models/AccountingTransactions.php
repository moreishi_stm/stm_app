<?php

/**
 * This is the model class for table "accounting_transactions".
 *
 * The followings are the available columns in table 'accounting_transactions':
 * @property integer $id
 * @property integer $accounting_account_id
 * @property string $date
 * @property string $reference_number
 * @property integer $closing_id
 * @property integer $agent_id
 * @property integer $source_id
 * @property string $amount
 * @property string $notes
 * @property integer $updated_by
 * @property string $updated
 * @property integer $added_by
 * @property string $added
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Contacts $source
 * @property Contacts $updatedBy
 * @property Contacts $addedBy
 */
class AccountingTransactions extends StmBaseActiveRecord
{
    // Flag to remove the record during processing
    public $remove = false;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccountingTransactions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'accounting_transactions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('date, amount, accounting_account_id, source_id, updated_by, updated, added_by, added', 'required', 'on'=>'roi'),
			array('date, amount, accounting_account_id, updated_by, updated, added_by, added', 'required'), // source_id not required for closing expenses
            array('date, accounting_account_id, amount, closing_id, updated_by, updated, added_by, added', 'required', 'on'=>'closings'),
//            array('agent_id','validateAgentId', 'on'=>'closings'),
			array('accounting_account_id, closing_id, source_id, agent_id, updated_by, added_by, remove, is_deleted', 'numerical', 'integerOnly'=>true),
			array('reference_number', 'length', 'max'=>15),
			array('amount', 'length', 'max'=>11),
			array('notes', 'length', 'max'=>255),
			array('amount, date, updated, added', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, accounting_account_id, date, reference_number, source_id, closing_id, agent_id, amount, notes, updated_by, updated, added_by, added, is_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'source' => array(self::BELONGS_TO, 'Sources', 'source_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
			'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'accounting_account_id' => 'Expense Account',
			'date' => 'Date',
			'reference_number' => 'Reference Number',
            'agent_id' => 'Agent',
			'source_id' => 'Source',
			'amount' => 'Amount',
			'notes' => 'Notes',
			'updated_by' => 'Updated By',
			'updated' => 'Updated',
			'added_by' => 'Added By',
			'added' => 'Added',
		);
	}

	protected function beforeValidate() {
		// If the value is a number based field strip commas or decimals from it.
		if ($this->isNewRecord) {
			$this->added_by = Yii::app()->user->id;
			$this->added = new CDbExpression('NOW()');
		}
		$this->amount = Yii::app()->format->formatDecimal($this->amount);

		$this->updated_by = Yii::app()->user->id;
		$this->updated = new CDbExpression('NOW()');

		if ($this->date > 0) {
			$this->date = Yii::app()->format->formatDate($this->date, StmFormatter::MYSQL_DATETIME_FORMAT);
		}

		return parent::beforeValidate();
	}

//    public function validateAgentId($attribute, $params)
//    {
//        if (($this->accounting_account_id != AccountingAccounts::REFERRAL_FEE) && !$this->agent_id) {
//            $this->addError($attribute, 'Please select an Agent.');
//        }
//    }

    protected function afterFind() {
		$this->date = Yii::app()->format->formatDate($this->date, StmFormatter::APP_DATE_FORMAT);

		parent::afterFind();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('accounting_account_id',$this->accounting_account_id);
		$criteria->compare('date',$this->date,true);

        $criteria->addCondition('closing_id IS NULL');

		$criteria->compare('reference_number',$this->reference_number,true);
		$criteria->compare('source_id',$this->source_id);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('added',$this->added,true);
        $criteria->compare('is_deleted',$this->added,true);

        $criteria->order = 'date DESC';

		return new CActiveDataProvider($this, array(
											  'criteria'=>$criteria,
                                              'pagination'=>array(
                                                  'pageSize'=> 100,
                                              ),
											  ));
	}
}