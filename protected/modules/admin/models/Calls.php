<?php

/**
 * This is the model class for table "calls".
 *
 * The followings are the available columns in table 'calls':
 *
*@property integer $id
 * @property integer $call_session_id
 * @property integer $call_list_phone_id
 * @property string $status
 * @property string $dial_b_leg_request_uuid
 * @property string $dial_b_leg_uuid
 * @property string $answer_time
 * @property string $dial_b_leg_total_cost
 * @property string $dial_b_leg_status
 * @property string $dial_b_leg_bill_rate
 * @property string $dial_action
 * @property integer $dial_b_leg_bill_duration
 * @property string $event
 * @property string $dial_b_leg_from
 * @property string $dial_a_leg_uuid
 * @property integer $dial_b_leg_position
 * @property string $start_time
 * @property string $dial_b_leg_hangup_cause
 * @property string $call_uuid
 * @property string $end_time
 * @property integer $dial_b_leg_duration
 * @property string $dial_b_leg_to
 * @property string $request_api_id
 * @property string $added
 * @property string $record_api_id
 * @property string $recording_id
 * @property string $recording_call_uuid
 * @property string $record_url
 * @property string $record_message
 * @property integer $recording_duration
 * @property integer $recording_duration_ms
 * @property integer $recording_start_ms
 * @property integer $recording_end_ms
 * @property string  $recording_added
 *
 * The followings are the available model relations:
 * @property CallSessions $callSession
 */
class Calls extends StmBaseActiveRecord
{
    // used for dataprovider for searchRecordings
    public $contactId;
    public $callListId;
    public $phone;
    public $order;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Calls the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'calls';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_session_id, call_list_phone_id', 'required'),
            array('call_session_id, call_list_phone_id, dial_b_leg_bill_duration, dial_b_leg_position, dial_b_leg_duration, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms', 'numerical', 'integerOnly'=>true),
//            array('status, dial_b_leg_total_cost, dial_b_leg_bill_rate', 'length', 'max'=>9),
            array('dial_b_leg_request_uuid, dial_b_leg_uuid, dial_a_leg_uuid, call_uuid, record_api_id, recording_call_uuid, recording_id, request_api_id', 'length', 'max'=>36),
            array('dial_b_leg_status, dial_action, event', 'length', 'max'=>20),
            array('dial_b_leg_hangup_cause', 'length', 'max'=>50),
            array('dial_b_leg_from, dial_b_leg_to', 'length', 'max'=>15),
            array('record_url, record_message', 'length', 'max'=>100),
            array('status, answer_time, start_time, end_time, added, record_api_id, record_message, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, recording_added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, call_session_id, call_list_phone_id, status, dial_b_leg_request_uuid, dial_b_leg_uuid, answer_time, dial_b_leg_total_cost, dial_b_leg_status, dial_b_leg_bill_rate, dial_action, dial_b_leg_bill_duration, event, dial_b_leg_from, dial_a_leg_uuid, dial_b_leg_position, start_time, dial_b_leg_hangup_cause, call_uuid, end_time, dial_b_leg_duration, dial_b_leg_to, request_api_id, added, record_api_id, recording_id, recording_call_uuid, record_url, record_message, recording_duration, recording_duration_ms, recording_start_ms, recording_end_ms, recording_added', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activityLogs' => array(self::HAS_MANY, 'ActivityLog', 'call_id', 'order'=>'id DESC'),
            'activityLog' => array(self::HAS_ONE, 'ActivityLog', 'call_id', 'order'=>'id DESC'), //single version used for grid view to get contact full name
            'callSession' => array(self::BELONGS_TO, 'CallSessions', 'call_session_id'),
            'callListPhone' => array(self::BELONGS_TO, 'CallListPhones', 'call_list_phone_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'call_session_id' => 'Call Session',
            'call_list_phone_id' => 'Call List Phone',
            'status' => 'Status',
            'dial_b_leg_request_uuid' => 'Dial B Leg Request Uuid',
            'dial_b_leg_uuid' => 'Dial B Leg Uuid',
            'answer_time' => 'Answer Time',
            'dial_b_leg_total_cost' => 'Dial B Leg Total Cost',
            'dial_b_leg_status' => 'Dial B Leg Status',
            'dial_b_leg_bill_rate' => 'Dial B Leg Bill Rate',
            'dial_action' => 'Dial Action',
            'dial_b_leg_bill_duration' => 'Dial B Leg Bill Duration',
            'event' => 'Event',
            'dial_b_leg_from' => 'Dial B Leg From',
            'dial_a_leg_uuid' => 'Dial A Leg Uuid',
            'dial_b_leg_position' => 'Dial B Leg Position',
            'start_time' => 'Start Time',
            'dial_b_leg_hangup_cause' => 'Dial B Leg Hangup Cause',
            'call_uuid' => 'Call Uuid',
            'end_time' => 'End Time',
            'dial_b_leg_duration' => 'Dial B Leg Duration',
            'dial_b_leg_to' => 'Dial B Leg To',
            'added' => 'Added',
            'record_api_id' => 'Record Api',
            'recording_id' => 'Recording',
            'recording_call_uuid' => 'Recording Call Uuid',
            'record_url' => 'Record Url',
            'record_message' => 'Message',
            'recording_duration' => 'Recording Duration',
            'recording_duration_ms' => 'Recording Duration Ms',
            'recording_start_ms' => 'Recording Start Ms',
            'recording_end_ms' => 'Recording End Ms',
            'recording_added' => 'Recording Added',
        );
    }

    public static function getIdByAnsweredCallListPhoneId($callListPhoneId, $callSessionId)
    {
        // Force array
        $callSessionId = is_array($callSessionId) ? $callSessionId : array($callSessionId);

        try {
            $command = Yii::app()->db->createCommand()
                ->select('id')
                ->from('calls');
            // forcing the clearing of params. suspect that this may be caching data we don't want.
            $command->params = array();

            //@todo: ignored PDO for debugging purpose, re-enable on 4/10/15 - CLee 4/3/15
//            $command->where('call_list_phone_id=:call_list_phone_id AND dial_b_leg_status="answer"', array(':call_list_phone_id'=>$callListPhoneId));
            $command->where('call_list_phone_id='.$callListPhoneId.' AND dial_b_leg_status="answer"');

            // this is so that in debugging, you can set a call to "Answered" and work on the GUI
            if(count($callSessionId)) {
                //@todo: ignored PDO for debugging purpose, re-enable on 4/10/15 - CLee 4/3/15
//                $command->andWhere('call_session_id=:call_session_id', array(':call_session_id'=>$callSessionId));
//                $command->andWhere('call_session_id='.$callSessionId);
                $command->andWhere(array('IN', 'call_session_id', $callSessionId));
            }

            if(!($callId = $command->queryScalar())) {

                // Create query criteria
                $criteria = new CDbCriteria();
                $criteria->addInCondition('id', $callSessionId);

                $callSessions = CallSessions::model()->findAll($criteria);
                $contactIds = array();
                foreach($callSessions as $callSession) {
                    $contactIds[] = $callSession->contact_id;
                }

                $command2 = Yii::app()->db->createCommand()
                    ->select('c.id')
                    ->from('calls c')
                    ->leftJoin('call_sessions s','s.id=c.call_session_id')
                    ->where('c.call_list_phone_id='.$callListPhoneId.' AND c.dial_b_leg_status="answer"')
//                    ->andWhere('s.contact_id='.$callSession->contact_id)
                    ->andWhere(array('IN', 's.contact_id', $contactIds))
                    ->order('c.id DESC');

                // forcing the clearing of params. suspect that this may be caching data we don't want.
                $command2->params = array();

                // this eliminates the "answer" status portion of the query
                if($callId2 = $command2->queryScalar()) {
                    $callId = $callId2;
                }

                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Error: Answered Call was not found. '.PHP_EOL
                    .'Call List Phone ID: '.$callListPhoneId.PHP_EOL
                    .'Call Session IDs: '.print_r($callSessionId, true).PHP_EOL
                    .'--------------------------------------------------------------------------'.PHP_EOL
                    .'Query 1: '.$command->getText().PHP_EOL
                    .'--------------------------------------------------------------------------'.PHP_EOL
                    .(($callId2)? 'Query 2 FOUND the call. Call ID: '.$callId2 : 'Query 2 did NOT find it either.').PHP_EOL
                    .'--------------------------------------------------------------------------'.PHP_EOL
                    .'Query 2: '.$command2->getText()
                    .'--------------------------------------------------------------------------'.PHP_EOL
                    .'Query 2 Results w/Select *: '.print_r($command2->select('*')->queryAll(), true)

                    , CLogger::LEVEL_ERROR);
            }
        }
        catch(Exception $e) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Error: Answered Call query had Exception!! '.PHP_EOL
                .'Message: '.$e->getMessage().PHP_EOL
                .'Call List Phone ID: '.$callListPhoneId.PHP_EOL
                .'Call Session IDs: '.print_r($callSessionId, true).PHP_EOL
                .'Query 1: '.$command->getText().PHP_EOL
                .'Query 1 Params: '.print_r($command->params, true).
                (($callId2)? 'Query 2: '.$command2->getText().PHP_EOL.'Query 2 Params: '. print_r($command2->params, true): ''), CLogger::LEVEL_ERROR);
        }
        return $callId;
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('call_session_id',$this->call_session_id);
        $criteria->compare('call_list_phone_id',$this->call_list_phone_id);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('dial_b_leg_uuid',$this->dial_b_leg_uuid,true);
        $criteria->compare('answer_time',$this->answer_time,true);
        $criteria->compare('dial_b_leg_total_cost',$this->dial_b_leg_total_cost,true);
        $criteria->compare('dial_b_leg_status',$this->dial_b_leg_status,true);
        $criteria->compare('dial_b_leg_bill_rate',$this->dial_b_leg_bill_rate,true);
        $criteria->compare('dial_action',$this->dial_action,true);
        $criteria->compare('dial_b_leg_bill_duration',$this->dial_b_leg_bill_duration);
        $criteria->compare('event',$this->event,true);
        $criteria->compare('dial_b_leg_from',$this->dial_b_leg_from,true);
        $criteria->compare('dial_a_leg_uuid',$this->dial_a_leg_uuid,true);
        $criteria->compare('dial_b_leg_position',$this->dial_b_leg_position);
        $criteria->compare('start_time',$this->start_time,true);
        $criteria->compare('dial_b_leg_hangup_cause',$this->dial_b_leg_hangup_cause,true);
        $criteria->compare('call_uuid',$this->call_uuid,true);
        $criteria->compare('end_time',$this->end_time,true);
        $criteria->compare('dial_b_leg_duration',$this->dial_b_leg_duration);
        $criteria->addCondition('dial_b_leg_to like "%'.Yii::app()->format->formatStripPhone($this->dial_b_leg_to,true).'%"');
        $criteria->compare('added',$this->added,true);
        $criteria->compare('record_api_id',$this->record_api_id,true);
        $criteria->compare('recording_id',$this->recording_id,true);
        $criteria->compare('recording_call_uuid',$this->recording_call_uuid,true);
        $criteria->compare('record_url',$this->record_url,true);
        $criteria->compare('record_message',$this->record_message,true);
        $criteria->compare('recording_duration',$this->recording_duration);
        $criteria->compare('recording_duration_ms',$this->recording_duration_ms);
        $criteria->compare('recording_start_ms',$this->recording_start_ms);
        $criteria->compare('recording_end_ms',$this->recording_end_ms);
        $criteria->compare('recording_added',$this->recording_added,true);
        $criteria->order = 'added DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }

    public function searchRecordings()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addCondition('recording_duration>45 AND record_url IS NOT NULL');
        $criteria->order = 'recording_added DESC';

        if($this->contactId || $this->callListId) {

            $criteria->together = true;
            $criteria->with = CMap::mergeArray($criteria->with, array('callSession'));
        }

        if($this->contactId) {

            $criteria->compare('callSession.contact_id', $this->contactId);
        }

        if($this->callListId) {

            $criteria->compare('callSession.call_list_id', $this->callListId);
        }

        if($this->phone) {
            $this->phone = trim($this->phone);

            if(strlen($this->phone) == 10) {
                $this->phone = '1'.$this->phone;
            }
            $criteria->compare('dial_b_leg_to', $this->phone, true);
        }

        if($this->order) {
            $criteria->order = $this->order;
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}