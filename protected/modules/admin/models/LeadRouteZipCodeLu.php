<?php

/**
 * This is the model class for table "lead_route_zip_code_lu".
 *
 * The followings are the available columns in table 'lead_route_zip_code_lu':
 * @property integer $id
 * @property string $zip_code_id
 * @property integer $contact_id
 * @property string $type
 *
 * The followings are the available model relations:
 * @property ZipCodes $leadRouteZipCode
 */
class LeadRouteZipCodeLu extends StmBaseActiveRecord
{
    public $data = array();

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeadRouteZipCodeLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lead_route_zip_code_lu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('zip_code_id, contact_id', 'required'),
            array('contact_id', 'numerical', 'integerOnly'=>true),
            array('zip_code_id', 'length', 'max'=>5),
            array('type', 'length', 'max'=>7),
            array('data', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, zip_code_id, contact_id, type', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'contact' => array(self::BELONGS_TO, 'Contacts', 'contact_id'),
            'zipCode' => array(self::BELONGS_TO, 'ZipCodes', 'zip_code_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'zip_code_id' => 'Zip Code',
            'contact_id' => 'Contact',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('zip_code_id',$this->zip_code_id,true);
        $criteria->compare('contact_id',$this->contact_id);
        $criteria->compare('type',$this->type,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 100,
            ),
        ));
    }
}