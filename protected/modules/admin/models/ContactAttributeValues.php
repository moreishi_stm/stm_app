<?php

	/**
	 * This is the model class for table "contact_attributes".
	 *
	 * The followings are the available columns in table 'contact_attributes':
	 *
	 * @property integer          $id
	 * @property integer          $contact_id
	 * @property integer          $contact_attribute_id
	 * @property string           $value
	 * @property integer          $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Contacts         $contact
	 * @property ContactAttribute $contactAttribute
	 */
	class ContactAttributeValues extends StmBaseActiveRecord {

		const PROFILE_HOBBIES_ID = 4;
		const PROFILE_BOOKS_ID = 5;
		const PROFILE_MOVIES_ID = 6;
		const PROFILE_RESTAURANTS_ID = 7;

		public $data = array(); // Collection of potential ContactAttributes models

        /**
         * @var array $contactAttributeModels collection of contact attribute models to make 1 db call when multiple uses in a form
         */
        protected  static $_contactAttributeNameId = array();
        protected  static $_contactAttributeIdLabel = array();

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ContactAttributes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'contact_attribute_values';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
                array('contact_id, contact_attribute_id, value','required', 'on'=>'insertData'),
				array(
					'contact_id, contact_attribute_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'value',
					'length',
					'max' => 750
				),

                array('data', 'safe'),

				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, contact_id, contact_attribute_id, value, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contact' => array(
					self::BELONGS_TO,
					'Contacts',
					'contact_id'
				),
				'attribute' => array(
					self::BELONGS_TO,
					'ContactAttributes',
					'contact_attribute_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'contact_id' => 'Contact',
				'contact_attribute_id' => 'Contact Attribute',
				'value' => 'Value',
				'is_deleted' => 'Is Deleted',
			);
		}

		public function getProfileAttributeList($id) {
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'contact_attribute_id in (:hobbies, :books, :movies, :restaurants) AND contact_id=:contact_id';
			$Criteria->params = array(
				':hobbies' => self::PROFILE_HOBBIES_ID,
				':books' => self::PROFILE_BOOKS_ID,
				':movies' => self::PROFILE_MOVIES_ID,
				':restaurants' => self::PROFILE_RESTAURANTS_ID,
				':contact_id' => $id,
			);

			return $this->findAll($Criteria);
		}

        public function byContactIdAttributesIds($contactId, $attributeIds) {
            $Criteria = new CDbCriteria;
            $Criteria->compare('contact_id',$contactId);
            $Criteria->addInCondition('contact_attribute_id', $attributeIds);

            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

		protected function beforeSave() {
            $this->value = trim($this->value);

            // format HQ State ID attribute
            if($this->contact_attribute_id == ContactAttributes::HQ_STATE_ID && !is_numeric($this->value)) {
                if(!($stateId = AddressStates::getIdByShortName($this->value))) {
                    $this->value = AddressStates::getIdByLongName($this->value);
                }
                else {
                    $this->value = $stateId;
                }
            }

			// Format values for the database
            switch ($this->attribute->data_type_id) {
                case DataTypes::DATE_ID:
                case DataTypes::DATETIME_ID:
                    $this->value = Yii::app()->format->formatDate($this->value, $dateFormat = 'Y-m-d');
                    break;
            }

			return parent::beforeSave();
		}

		protected function afterFind() {
			// Format anything that needs to be changed for the gui after a record is found
			switch ($this->attribute->data_type_id) {
				case DataTypes::DATE_ID:
				case DataTypes::DATETIME_ID:
					$this->value = Yii::app()->format->formatDate($this->value);
					break;
			}
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('contact_id', $this->contact_id);
			$criteria->compare('contact_attribute_id', $this->contact_attribute_id);
			$criteria->compare('value', $this->value, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}