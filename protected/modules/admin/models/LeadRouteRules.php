<?php

	/**
	 * This is the model class for table "lead_route_rules".
	 *
	 * The followings are the available columns in table 'lead_route_rules':
	 *
	 * @property integer    $id
	 * @property integer    $lead_group_id
	 * @property string     $name
	 * @property integer    $condition_field
	 * @property integer    $operator_ma
	 * @property string     $condition_value
	 * @property integer    $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property LeadGroups $leadGroup
	 */
	class LeadRouteRules extends StmBaseActiveRecord {

		const OPERATOR_EQUALS_ID = 1;
		const OPERATOR_LESS_THAN_ID = 2;
		const OPERATOR_GREATER_THAN_ID = 3;
		const OPERATOR_LESS_THAN_EQUAL_ID = 4;
		const OPERATOR_GREATER_THAN_EQUAL_ID = 5;
		const OPERATOR_NOT_EQUALS_ID = 6;

		const FIELD_LEAD_TYPE_ID = 1;
		const FIELD_SOURCE_ID = 2;
		const FIELD_SUB_SOURCE_ID = 3;
        const FIELD_ZIP_ID = 4;
        const FIELD_PRICE_ID = 5;
        const FIELD_CITY_ID = 6;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return LeadRouteRules the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'lead_route_rules';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'lead_route_id, field_ma, operator_ma, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 50
				),
				array(
					'value',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, lead_route_id, name, field_ma, operator_ma, value, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'leadRoute' => array(
					self::BELONGS_TO,
					'LeadRoutes',
					'lead_route_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'lead_route_id' => 'Lead Route',
				'name' => 'Name',
				'field_ma' => 'Field',
				'operator_ma' => 'Operator',
				'value' => 'Value',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * getGroupTypes
		 * Returns array of Group Types
		 *
		 * @return string
		 */
		public static function getOperatorsList() {
			return array(
				self::OPERATOR_EQUALS_ID => '= Equal',
				self::OPERATOR_LESS_THAN_ID => '< Less Than',
				self::OPERATOR_GREATER_THAN_ID => '> Greater Than',
				self::OPERATOR_LESS_THAN_EQUAL_ID => '<= Less Than Equal',
				self::OPERATOR_GREATER_THAN_EQUAL_ID => '>= Greater Than Equal',
				self::OPERATOR_NOT_EQUALS_ID => '!= Not Equal',
			);
		}

		/**
		 * getGroupType
		 * Returns string name of Group Type
		 *
		 * @return string
		 */
		public function getOperator() {
			$operators = array(
				self::OPERATOR_EQUALS_ID => '=',
				self::OPERATOR_LESS_THAN_ID => '<',
				self::OPERATOR_GREATER_THAN_ID => '>',
				self::OPERATOR_LESS_THAN_EQUAL_ID => '<=',
				self::OPERATOR_GREATER_THAN_EQUAL_ID => '>=',
				self::OPERATOR_NOT_EQUALS_ID => '!=',
			);

			return $operators[$this->operator_ma];
		}

		/**
		 * getFieldsList
		 * Returns array of Fields
		 *
		 * @return string
		 */
		public static function getFieldsList() {
			return array(
				self::FIELD_LEAD_TYPE_ID => 'Lead Type',
				self::FIELD_SOURCE_ID => 'Source',
				self::FIELD_SUB_SOURCE_ID => 'Sub-Source',
                self::FIELD_ZIP_ID => 'Zip',
                self::FIELD_PRICE_ID => 'Price',
                self::FIELD_CITY_ID => 'City',
			);
		}

		/**
		 * getField
		 * Returns string name of Field
		 *
		 * @return string
		 */
		public function getField() {
			$fields = LeadRouteRules::getFieldsList();

			return $fields[$this->field_ma];
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('lead_route_id', $this->lead_route_id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('operator_ma', $this->operator_ma);
			$criteria->compare('field_ma', $this->field_ma);
			$criteria->compare('value', $this->value, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}