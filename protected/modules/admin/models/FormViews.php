<?php

	/**
	 * This is the model class for table "form_views".
	 *
	 * The followings are the available columns in table 'form_views':
	 *
	 * @property integer $id
	 * @property integer $home_detail_view_count
	 * @property string  $domain
	 * @property string  $page
	 * @property string  $url
	 * @property string  $referrer
	 * @property string  $utmz
	 * @property string  $utma
	 * @property string  $utmb
	 * @property string  $utmc
	 * @property string  $ip
	 * @property string  $added
	 * @property string  $session_id
	 */
	class FormViews extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FormViews the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'form_views';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'domain, page, ip',
					'required'
				),
				array(
					'home_detail_view_count',
					'numerical',
					'integerOnly' => true
				),
				array(
					'domain',
					'length',
					'max' => 255
				),
				array(
					'ip',
					'length',
					'max' => 40
				),
				array(
					'session_id',
					'length',
					'max' => 63
				),
				array(
					'page, url, referrer, utmz, utma, utmb, utmc, browser, added',
					'safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, home_detail_view_count, domain, page, url, referrer, utmz, utma, utmb, utmc, ip, browser, added, session_id',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array();
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'home_detail_view_count' => 'Home Detail View Count',
				'domain' => 'Domain',
				'page' => 'Page',
				'url' => 'Url',
				'referrer' => 'Referrer',
				'utmz' => 'Utmz',
				'utma' => 'Utma',
				'utmb' => 'Utmb',
				'utmc' => 'Utmc',
				'ip' => 'Ip',
				'browser' => 'Browser',
				'added' => 'Added',
				'session_id' => 'Session',
			);
		}

		public function trafficViewsBySessionId($id) {
			return TrafficTracker::model()->countBySessionId($id);
			//@todo: delete when verify above works return TrafficTracker::model()->count(array('condition'=>'session_id=:session_id','params'=>array(':session_id'=>$id)));
		}

		public function viewsBySessionId($id) {
			return $this->count(array(
					'condition' => 'session_id=:session_id',
					'params' => array(':session_id' => $id)
				)
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('home_detail_view_count', $this->home_detail_view_count);
			$criteria->compare('domain', $this->domain, true);
			$criteria->compare('page', $this->page, true);
			$criteria->compare('url', $this->url, true);
			$criteria->compare('referrer', $this->referrer, true);
			$criteria->compare('utmz', $this->utmz, true);
			$criteria->compare('utma', $this->utma, true);
			$criteria->compare('utmb', $this->utmb, true);
			$criteria->compare('utmc', $this->utmc, true);
			$criteria->compare('ip', $this->ip, true);
			$criteria->compare('browser', $this->browser, true);
			$criteria->compare('added', $this->added, true);
			$criteria->compare('session_id', $this->session_id, true);

			$criteria->order = 'added DESC';
			$criteria->group = 'session_id';

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}