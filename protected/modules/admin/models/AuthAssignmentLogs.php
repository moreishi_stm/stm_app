<?php

	/**
	 * This is the model class for table "auth_assignment_logs".
	 *
	 * The followings are the available columns in table 'auth_assignment_logs':
	 * @property integer $id
	 * @property string $auth_item_name_old
	 * @property string $auth_item_name_new
	 * @property integer $userid
	 * @property integer $added_by
	 * @property string $added
	 *
	 * The followings are the available model relations:
	 * @property AuthAssignment $itemnameIdNew
	 * @property Contacts $user
	 * @property Contacts $addedBy
	 */
	class AuthAssignmentLogs extends StmBaseActiveRecord
	{
		/**
		 * Returns the static model of the specified AR class.
		 * @param string $className active record class name.
		 * @return AuthAssignmentLogs the static model class
		 */
		public static function model($className=__CLASS__)
		{
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName()
		{
			return 'auth_assignment_logs';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules()
		{
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array('userid,auth_item_name_new,added_by,added','required'),
				array('userid, added_by', 'numerical', 'integerOnly'=>true),
				array('auth_item_name_old, auth_item_name_new', 'length', 'max'=>64),
				array('added', 'safe'),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('id, auth_item_name_old, auth_item_name_new, userid, added_by, added', 'safe', 'on'=>'search'),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations()
		{
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
//				'itemnameIdNew' => array(self::BELONGS_TO, 'AuthAssignment', 'auth_item_name_new'),
//				'itemnameIdOld' => array(self::BELONGS_TO, 'AuthAssignment', 'auth_item_name_old'),
//				'user' => array(self::BELONGS_TO, 'Contacts', 'userid'),
//				'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels()
		{
			return array(
				'id' => 'ID',
				'auth_item_name_old' => 'Old',
				'auth_item_name_new' => 'New',
				'userid' => 'User',
				'added_by' => 'Added By',
				'added' => 'Added',
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search()
		{
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria=new CDbCriteria;

			$criteria->compare('id',$this->id);
			$criteria->compare('auth_item_name_old',$this->auth_item_name_old,true);
			$criteria->compare('auth_item_name_new',$this->auth_item_name_new,true);
			$criteria->compare('userid',$this->userid);
			$criteria->compare('added_by',$this->added_by);
			$criteria->compare('added',$this->added,true);

			return new CActiveDataProvider($this, array(
												  'criteria'=>$criteria,
												  ));
		}
	}