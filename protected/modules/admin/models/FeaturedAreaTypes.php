<?php

	/**
	 * This is the model class for table "featured_area_types".
	 *
	 * The followings are the available columns in table 'featured_area_types':
	 *
	 * @property integer              $featured_area_type_id
	 * @property integer              $account_id
     * @property string               $name
	 * @property string               $type
	 * @property integer              $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property FeaturedAreaTypeLu[] $featuredAreaTypeLus
	 * @property Accounts             $account
	 */
	class FeaturedAreaTypes extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FeaturedAreaTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'featured_area_types';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'featured_area_type_id, account_id, name, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array( // 'featuredAreaTypeLu' => array(self::HAS_MANY, 'FeaturedAreaTypeLu', 'featured_area_type_id'),
				// 'account' => array(self::BELONGS_TO, 'Accounts', 'account_id'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'featured_area_type_id' => 'Featured Area Type',
				'account_id' => 'Account',
				'name' => 'Name',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * getByAccount retrieves all featured area types by accountId
		 *
		 * @param  int $id  accountId
		 *
		 * @return mixed   model of results
		 */
		public function getByAccount($id) {
			return $this->findAll(array(
					'condition' => 'account_id=:account_id',
					'params' => array(':account_id' => $id),
				)
			);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=> array('pageSize'=>50),
			));
		}
	}