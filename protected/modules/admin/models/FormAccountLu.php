<?php

	/**
	 * This is the model class for table "form_account_lu".
	 *
	 * The followings are the available columns in table 'form_account_lu':
	 *
	 * @property integer       $id
	 * @property integer       $account_id
	 * @property integer       $form_id
	 * @property integer       $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property Forms         $form
	 * @property Accounts      $account
	 * @property FormFieldLu[] $formFieldLus
	 * @property FormSubmits[] $formSubmits
	 */
	class FormAccountLu extends StmBaseActiveRecord {

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return FormAccountLu the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'form_account_lu';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'account_id, form_id, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, account_id, form_id, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'form' => array(
					self::BELONGS_TO,
					'Forms',
					'form_id'
				),
				'formFieldLus' => array(
					self::HAS_MANY,
					'FormFieldLu',
					'form_account_lu_id'
				),
				'formSubmissions' => array(
					self::HAS_MANY,
					'FormSubmissions',
					'form_account_lu_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'account_id' => 'Account',
				'form_id' => 'Form',
				'is_deleted' => 'Is Deleted',
			);
		}

		public function byAccountId($accountId) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'account_id=:account_id',
					'params' => array(':account_id' => $accountId),
				)
			);

			return $this;
		}

		public function byFormId($formId) {

			$this->getDbCriteria()->mergeWith(array(
					'condition' => 'form_id=:form_id',
					'params' => array(':form_id' => $formId),
				)
			);

			return $this;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('account_id', $this->account_id);
			$criteria->compare('form_id', $this->form_id);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
