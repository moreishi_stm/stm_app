<?php
	Yii::import('admin_module.models.interfaces.Component');

    /**
     * This is the model class for table "closings".
     *
     * The followings are the available columns in table 'closings':
     * @property integer $id
     * @property integer $transaction_id
     * @property integer $is_forever_client
     * @property integer $closing_status_ma
     * @property integer $email_frequency_ma
     * @property integer $post_close_status_ma
     * @property string $fallout_date
     * @property integer $fallout_reason_ma
     * @property string $fallout_reason_other
     * @property integer $price
     * @property integer $sale_type_ma
     * @property integer $financing_type_ma
     * @property integer $loan_amount
     * @property integer $is_new_construction
     * @property string $mls_id
     * @property integer $is_our_listing
     * @property string $ernest_money_deposit
     * @property string $inspection_contingency_date
     * @property string $appraisal_contingency_date
     * @property string $financing_contingency_date
     * @property string $contingencies
     * @property string $notes
     * @property integer $is_referral
     * @property string $referral_commission
     * @property string $commission_amount
     * @property string $contract_execute_date
     * @property string $contract_closing_date
     * @property string $actual_closing_datetime
     * @property integer $is_mail_away
     * @property string $closing_location
     * @property string $closing_gift
     * @property integer $title_company_id
     * @property integer $other_agent_id
     * @property integer $lender_id
     * @property integer $home_warranty_company_id
     * @property integer $is_deleted
     *
     * The followings are the available model relations:
     * @property AccountingTransactions[] $accountingTransactions
     * @property ClosingContacts[] $closingContacts
     * @property Transactions $transaction
     */
	class Closings extends StmBaseActiveRecord implements Component {

		const SALE_TYPE_NORMAL_ID = 1;
		const SALE_TYPE_NEW_CONSTRUCTION_ID = 2;
		const SALE_TYPE_SHORT_SALE_ID = 3;
		const SALE_TYPE_REO_ID = 4;
        const SALE_TYPE_RENTAL_ID = 5;

		const FINANCING_TYPE_CASH_ID = 1;
		const FINANCING_TYPE_CONVENTIONAL_ID = 2;
		const FINANCING_TYPE_FHA_ID = 3;
		const FINANCING_TYPE_VA_ID = 4;
		const FINANCING_TYPE_BOND_ID = 5;
		const FINANCING_TYPE_OTHER_ID = 6;
        const FINANCING_TYPE_USDA_ID = 7;

		const STATUS_TYPE_ACTIVE_ID = 1;
		const STATUS_TYPE_FALLOUT_ID = 2;
		const STATUS_TYPE_CLOSED_ID = 3;

		const POST_CLOSE_STATUS_TYPE_INCOMPLETE_ID = 1;
		const POST_CLOSE_STATUS_TYPE_COMPLETE_ID = 2;

		const FALLOUT_REASON_FINANCING_ID = 1;
		const FALLOUT_REASON_INSPECTIONS_ID = 2;
		const FALLOUT_REASON_OTHER_ID = 3;

        // Forever Client function return type
        const FOREVER_CLIENT_RETURN_TYPE_COUNT = 1;
        const FOREVER_CLIENT_RETURN_TYPE_DATA = 2;
        const FOREVER_CLIENT_RETURN_TYPE_CONTACT_MODELS = 3;

        # Cached company list
        protected $_companyList;
        protected $_originalData;

        protected $_componentType;

        protected $data = array();

        public $sendCongratsEmail = true;

        public $expenseCollection = array();

        public $volumeTotal;
        public $gciTotal;
		public $countTotal;
		public $businessPercent;
		public $parentSource;
		public $income;
		public $closingDateFrom;
		public $closingDateTo;
        public $assignmentIds;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Closings the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'closings';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'closing_status_ma, price, commission_amount, contract_execute_date, contract_closing_date, sale_type_ma, financing_type_ma, is_referral',
					'required'
				),
				array(
					'is_forever_client, assignmentIds, inspection_contingency_date, appraisal_contingency_date, financing_contingency_date, ernest_money_deposit, income,parentSource,businessPercent,countTotal, volumeTotal,title_company_id, other_agent_id, lender_id, contract_execute_date, sale_type_ma, fallout_date, contract_closing_date, actual_closing_datetime, financing_type_ma, closingDateFrom, closingDateTo',
					'safe'
				),
				array(
					'transaction_id, email_frequency_ma, is_forever_client, title_company_id, other_agent_id, lender_id, home_warranty_company_id, closing_status_ma, post_close_status_ma, fallout_reason_ma, sale_type_ma, is_our_listing, is_referral, is_mail_away, is_deleted',
					'numerical',
					'integerOnly' => true
				),
                array(
                    'actual_closing_datetime',
                    'validateClosingStatusActualDatetime'
                ),
				array(
					'price, loan_amount, ernest_money_deposit',
					'numerical'
				),
				array(
					'mls_id',
					'length',
					'max' => 25
				),
				array(
					'fallout_reason_other',
					'length',
					'max' => 63
				),
                array(
                    'closing_gift',
                    'length',
                    'max' => 127
                ),
				array(
					'notes',
					'length',
					'max' => 65535
				),
				array(
					'referral_commission, commission_amount',
					'length',
					'max' => 50
				),
				array(
					'closing_location',
					'length',
					'max' => 255
				),
				array(
					'referral_commission, commission_amount',
					'numerical',
					'max' => 99999999.99
				),
				array(
					'contract_execute_date, contract_closing_date, referral_commission, commission_amount',
					'default'
				),
				// Prevents these values from being saved if null
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'is_forever_client, parentSource, businessPercent, countTotal, volumeTotal, id, transaction_id, closing_status_ma, email_frequency_ma, post_close_status_ma, fallout_date, fallout_reason_ma, price, loan_amount, sale_type_ma, mls_id, is_our_listing, inspection_contingency_date, appraisal_contingency_date, financing_contingency_date, ernest_money_deposit, notes, is_referral, referral_commission, commission_amount, contract_execute_date, contract_closing_date, actual_closing_datetime, is_mail_away, closing_location, closing_gift, closingDateFrom, closingDateTo, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'transaction' => array(
					self::BELONGS_TO,
					'Transactions',
					'transaction_id'
				),
				'assignments' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'assignments.component_type_id = :component_type_id AND (assignments.assignment_type_id IN ('.AssignmentTypes::CLOSING_MANAGER.','.AssignmentTypes::CLOSING_MANAGER_ASSISTANT.'))',
					'params' => array(':component_type_id' => ComponentTypes::CLOSINGS)
				),
				'companyAssignments' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'component_type_id = :component_type_id AND assignee_company_id is not null',
					'params' => array(
						':component_type_id' => ComponentTypes::CLOSINGS,
					),
				),
				'contactAssignments' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'component_type_id = :component_type_id AND assignee_contact_id is not null',
					'params' => array(
						':component_type_id' => ComponentTypes::CLOSINGS,
					),
				),
				'actionPlans' => array(
					self::MANY_MANY,
					'ActionPlans',
					'action_plan_applied_lu(component_id, action_plan_id)',
					'condition' => 'component_type_id = :component_type_id',
					'params' => array(':component_type_id' => ComponentTypes::CLOSINGS)
				),
                'expenses' => array(
                    self::HAS_MANY,
                    'AccountingTransactions',
                    'closing_id',
                ),
				'otherAgent' => array(
					self::HAS_MANY,
					'Assignments',
					'component_id',
					'condition' => 'component_type_id = :component_type_id AND assignment_type_id =:assignment_type_id',
					'params' => array(
						':component_type_id' => ComponentTypes::CLOSINGS,
						':assignment_type_id' => AssignmentTypes::CO_BROKER_AGENT,
					),
				),
				//				'otherAgent' => array(
				//					self::BELONGS_TO,
				//					'Contacts',
				//					'other_agent_id'
				//				),
				'lender' => array(
					self::BELONGS_TO,
					'Companies',
					'lender_id'
				),
				'titleCompany' => array(
					self::BELONGS_TO,
					'Companies',
					'title_company_id'
				),
			);
		}

		public function getCompanies() {
			$companies = array();
			if (!$this->companyAssignments) {
				return $companies;
			}

			foreach ($this->companyAssignments as $assignment) {
				array_push($companies, $assignment->getAssignee());
			}

			return $companies;
		}

		public function getContacts() {
			$contacts = array();
			if (!$this->companyAssignments) {
				return $contacts;
			}

			foreach ($this->contactAssignments as $assignment) {
				array_push($contacts, $assignment->getAssignee());
			}

			return $contacts;
		}

		/**
		 * Wrapper to get the contact from the transaction
		 *
		 * @return Contacts|null
		 */
		public function getContact() {
			if (!$this->transaction) {
				return null;
			}

			return $this->transaction->contact;
		}

		/**
		 * @return array behaviors for getTasks
		 */
		public function behaviors() {
			return array(
				'getTasks' => array(
					'class' => 'admin_module.components.behaviors.GetTasksBehavior',
				),
				'modelAttribute' => array(
					'class' => 'admin_module.components.behaviors.ModelAttributeBehavior',
				),
			);
		}

		public function getComponentType() {
			if (!isset($this->_componentType)) {
				$this->_componentType = ComponentTypes::model()->findByPk(ComponentTypes::CLOSINGS);
			}

			return $this->_componentType;
		}

		/**
		 * getAddress
		 *
		 * @return Addresses model for a transaction if it exists. Else, returns null.
		 */
		public function getAddress() {
			$Address = $this->transaction->address->fullAddress;

			return ($Address) ? $Address : null;
		}

		/**
		 * getAddressModel
		 *
		 * @return Addresses model for a transaction if it exists. Else, returns null.
		 */
		public function getAddressModel() {
			$Address = $this->transaction->address;

			return ($Address) ? $Address : null;
		}

		/**
		 *
		 * @author jamesk 4/19/2016
		 * @author 813.330.0522
		 *
		 */
		public function getAgentCommission(){
			return  Yii::app()->db->createCommand()
				->select(" SUM(at.amount) as sum")
				->from("accounting_transactions at")
				->where("at.closing_id=".$this->id)
				->andWhere("accounting_account_id IN (".AccountingAccounts::SALES_COMMISSION.")")
				->queryScalar();
		}

        /**
         *
         * @author jamesk 4/19/2016
         * @author 813.330.0522
         *
         */
        public function getAgentInternalReferralCommission(){
            return  Yii::app()->db->createCommand()
                ->select(" SUM(at.amount) as sum")
                ->from("accounting_transactions at")
                ->where("at.closing_id=".$this->id)
                ->andWhere("accounting_account_id IN (".AccountingAccounts::REFERRAL_FEE_INTERNAL.")")
                ->andWhere('at.is_deleted=0')
                ->queryScalar();
        }

		/**
		 * getClosingSideName
		 *
		 * @return Address model for a transaction if it exists. Else, returns null.
		 */
		public function getClosingSideName() {
			$transactionType = $this->transaction->transactionType;

			switch ($transactionType) {
				case Transactions::BUYERS:
					$string = 'Buyer';
					break;
				case Transactions::SELLERS:
					$string = 'Seller';
					break;
			}

			return $string;
		}

		public static function countClosings($opts = null, $componentTypeId = null) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);
			if (!isset($opts['contactId']) || empty($opts['contactId'])) {
				$opts['contactId'] = Yii::app()->user->id;
			}

			return Closings::model()->byAssigneeId($opts['contactId'])->dateRangeClosed($opts)->byComponentTypeIds($componentTypeId)->byStatus(array(Closings::STATUS_TYPE_CLOSED_ID))->count();
		}

		public static function countIncome($dates = null) {
			$opts['default_preset'] = 'this_month';
			$opts = Yii::app()->format->formatDateRangeArray($opts);
			if (!isset($opts['contactId']) || empty($opts['contactId'])) {
				$opts['contactId'] = Yii::app()->user->id;
			}

			return Closings::model()->byAssigneeId($opts['contactId'])->dateRangeContractExecuted($opts)->byStatus(array(Closings::STATUS_TYPE_CLOSED_ID))->calculateIncome()->find()->income;
		}

		public function dateRange($opts = null, $field) {

			if ($opts == null) {
				$this->getDbCriteria()->mergeWith(array(
						'condition' => $field . ' <= :date',
						'params' => array(':date' => date('Y-m-d')),
					)
				);
			} else {
				if ($opts['to_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => $field . ' < :to_date',
							'params' => array(':to_date' => date('Y-m-d', strtotime($opts['to_date'] . ' +1 day')))
						)
					);
				}

				if ($opts['from_date']) {
					$this->getDbCriteria()->mergeWith(array(
							'condition' => $field . ' >= :from_date',
							'params' => array(':from_date' => $opts['from_date']),
						)
					);
				}
			}

			return $this;
		}

		public function dateRangeContractExecuted($opts) {
			return $this->dateRange($opts, 'contract_execute_date');
		}

		public function dateRangeClosed($opts) {
			return $this->dateRange($opts, 'contract_closing_date');
		}

		public function calculateIncome() {

			$this->getDbCriteria()->mergeWith(array(
					'select' => '*, SUM(t.commission_amount) as income'
				)
			);

			return $this;
		}

        public function byAccountId($accountId) {
            if($accountId) {
                $Criteria = new CDbCriteria;
                $Criteria->condition = 'transaction.account_id = :accountId';
                $Criteria->params = array(':accountId' => $accountId);
                $Criteria->with = array(
                    'transaction',
                );
                $Criteria->together = true;

                $this->getDbCriteria()->mergeWith($Criteria);
            }

            return $this;
        }

		public function byAssigneeId($contactId) {
            if($contactId) {
                $Criteria = new CDbCriteria;
                $Criteria->condition = 'buyerSellerAssignments.assignee_contact_id = :assignee_contact_id';
                $Criteria->params = array(':assignee_contact_id' => $contactId);
                $Criteria->with = array(
                    'transaction',
                    'transaction.buyerSellerAssignments'
                );
                $Criteria->together = true;

                $this->getDbCriteria()->mergeWith($Criteria);
            }

			return $this;
		}

		public static function getCountExecutedByAssigneeId($opts = null) {
			$opts = Yii::app()->format->formatDateRangeArray($opts);
			if (!isset($opts['contactId']) || empty($opts['contactId'])) {
				$opts['contactId'] = Yii::app()->user->id;
			}

			return Closings::model()->byAssigneeId($opts['contactId'])->dateRangeContractExecuted($opts)->count();
		}

		public function volumeTotal($dateRange, $type, $componentTypeId) {

			switch ($type) {
				case 'executed':
					$dateField = 'contract_execute_date';
					$statusId = Closings::STATUS_TYPE_ACTIVE_ID . ',' . Closings::STATUS_TYPE_CLOSED_ID . ',' . Closings::STATUS_TYPE_FALLOUT_ID;
					break;
				case 'scheduled':
					$dateField = 'contract_closing_date';
					$statusId = Closings::STATUS_TYPE_ACTIVE_ID;
					break;
				case 'closed':
					$dateField = 'contract_closing_date';
					$statusId = Closings::STATUS_TYPE_CLOSED_ID;
					break;
				case 'fallout':
					$dateField = 'fallout_date';
					$statusId = Closings::STATUS_TYPE_FALLOUT_ID;
					break;
			}

			$query = 'SELECT sum(price) FROM `closings` LEFT JOIN transactions ON closings.transaction_id=transactions.id WHERE transactions.component_type_id in(' . $componentTypeId
				. ') AND  `closing_status_ma` in (' . $statusId . ') AND `' . $dateField . '`>="' . $dateRange['from_date'] . '" AND `' . $dateField . '`<="' . $dateRange['to_date'] . '"';
			$volumeTotal = Yii::app()->db->createCommand($query)->queryScalar();

			return ($volumeTotal) ? $volumeTotal : 0;
		}

		public static function getVolumeTotal($Criteria) { //@todo: this should be replaced by volumeTotal - in tracking closings, sources tracking

			$condition = $Criteria->condition;
			$params = $Criteria->params;
			if (!empty($params)) {
				foreach ($params as $key => $value) {
					if (is_string($value)) {
						$value = "'" . $value . "'";
					}

					$condition = str_replace($key, $value, $condition);
				}
			}
//			$condition = str_replace('t.', '', $condition);
			$condition = str_replace('`t`.', '', $condition);
			$condition = str_replace('(is_deleted = 0 or is_deleted IS NULL)', '(closings.is_deleted = 0 or closings.is_deleted IS NULL)', $condition);

			if(in_array('transaction',(array) $Criteria->with)) {
                // @todo: this handling of assignment contact_id is such a hack, but I know we are re-doing this in Admin to and removing alot of ORM usage, which is part of the reason for this hack anyways
                if(in_array('transaction.buyerSellerAssignments', (array) $Criteria->with)) {
                    $assignmentJoin = 'LEFT JOIN assignments buyerSellerAssignments ON buyerSellerAssignments.component_id=transactions.id AND buyerSellerAssignments.component_type_id=transactions.component_type_id';
                }

				$condition = str_replace('transaction', 'transactions', $condition);
				return Yii::app()->db->createCommand("SELECT SUM(price) as volumeTotal FROM `closings` LEFT JOIN `transactions` on `closings`.`transaction_id`=`transactions`.`id` {$assignmentJoin} WHERE " . $condition)->queryScalar();
			} elseif (!$Criteria->with)
				return Yii::app()->db->createCommand("SELECT SUM(price) as volumeTotal FROM `closings` WHERE " . $condition)->queryScalar();
		}

        public static function getGCITotal($Criteria) {

            $condition = $Criteria->condition;
            $params = $Criteria->params;
            if (!empty($params)) {
                foreach ($params as $key => $value) {
                    if (is_string($value)) {
                        $value = "'" . $value . "'";
                    }

                    $condition = str_replace($key, $value, $condition);
                }
            }
            //			$condition = str_replace('t.', '', $condition);
            $condition = str_replace('`t`.', '', $condition);
            $condition = str_replace('(is_deleted = 0 or is_deleted IS NULL)', '(closings.is_deleted = 0 or closings.is_deleted IS NULL)', $condition);

            if(in_array('transaction',(array) $Criteria->with)) {
                // @todo: this handling of assignment contact_id is such a hack, but I know we are re-doing this in Admin to and removing alot of ORM usage, which is part of the reason for this hack anyways
                if(in_array('transaction.buyerSellerAssignments', (array) $Criteria->with)) {
                    $assignmentJoin = 'LEFT JOIN assignments buyerSellerAssignments ON buyerSellerAssignments.component_id=transactions.id AND buyerSellerAssignments.component_type_id=transactions.component_type_id';
                }

                $condition = str_replace('transaction', 'transactions', $condition);
                return Yii::app()->db->createCommand("SELECT SUM(commission_amount) as gciTotal FROM `closings` LEFT JOIN `transactions` on `closings`.`transaction_id`=`transactions`.`id` {$assignmentJoin} WHERE " . $condition)->queryScalar();
            } elseif (!$Criteria->with)
                return Yii::app()->db->createCommand("SELECT SUM(commission_amount) as gciTotal FROM `closings` WHERE " . $condition)->queryScalar();
        }

		public static function getYTDclosingCountByMonth($contactId) {
			$closingCountByMonth = array();
			for ($i = 0; $i < 12; $i++) {
				$month = $i + 1;
				$currentMonth = date('n');
				if ($month <= $currentMonth) {
					$opts['from_date'] = date('Y-' . $month . '-1');
					$opts['to_date'] = date('Y-' . $month . '-t');
					$opts['contactId'] = $contactId;
					$closingCountByMonth[$i] = Closings::countClosings($opts);
				} else {
					$closingCountByMonth[$i] = 0;
				}
			}

			return $closingCountByMonth;
		}

		public function byAssignmentTypes($assignmentTypes = array()) {

			$Criteria = new CDbCriteria;
			$Criteria->addInCondition('assignment_type_id', $assignmentTypes);
			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function byStatus($status) {
			if (!is_array($status)) {
				$status = array($status);
			}

			$Criteria = new CDbCriteria;
			$Criteria->addInCondition('closing_status_ma', $status);
			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

		public function byComponentTypeIds($componentTypeIds) {

			$Criteria = new CDbCriteria;
			if (!empty($componentTypeIds)) {
				if (!is_array($componentTypeIds)) {
					$componentTypeIds = array($componentTypeIds);
				}
				$Criteria->with = 'transaction';
				//					$Criteria->together = true; // only needed if later end up using in gridview
				$Criteria->addInCondition('transaction.component_type_id', $componentTypeIds);
				$this->getDbCriteria()->mergeWith($Criteria);
			}

			return $this;
		}

		public function byClosingDate($dates) {

			$Criteria = new CDbCriteria;
            if($dates['from_date']) {
                $Criteria->compare('DATE(contract_closing_date)', '>=' . $dates['from_date'], true);
            }
            if($dates['to_date']) {
                $Criteria->compare('DATE(contract_closing_date)', '<=' . $dates['to_date'], true);
            }
			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

        public function byFalloutDate($dates) {

            $Criteria = new CDbCriteria;
            $Criteria->compare('DATE(fallout_date)', '>=' . $dates['from_date'], true);
            $Criteria->compare('DATE(fallout_date)', '<=' . $dates['to_date'], true);
            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

        public function byActualClosingDate($dates) {

            $Criteria = new CDbCriteria;
            $Criteria->compare('DATE(actual_closing_datetime)', '>=' . $dates['from_date'], true);
            $Criteria->compare('DATE(actual_closing_datetime)', '<=' . $dates['to_date'], true);
            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

		public function byExecuteDate($dates) {

			$Criteria = new CDbCriteria;
			$Criteria->compare('DATE(contract_execute_date)', '>=' . $dates['from_date'], true);
			$Criteria->compare('DATE(contract_execute_date)', '<=' . $dates['to_date'], true);
			$this->getDbCriteria()->mergeWith($Criteria);

			return $this;
		}

        public function byContactId($contactId) {

            $Criteria = new CDbCriteria;
            $Criteria->condition = 'contact.id = :contact_id';
            $Criteria->params = array(':contact_id' => $contactId);
            $Criteria->with = array(
                'transaction',
                'transaction.contact'
            );
            $Criteria->together = true;

            $this->getDbCriteria()->mergeWith($Criteria);

            return $this;
        }

        /**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'transaction_id' => 'Transaction',
				'closing_status_ma' => 'Status',
                'is_forever_client' => 'Forever Client',
				'post_close_status_ma' => 'Post Close Status',
				'financing_type_ma' => 'Financing Type',
				'price' => 'Price',
				'sale_type_ma' => 'Sale Type',
				'fallout_date' => 'Fallout Date',
				'fallout_reason_ma' => 'Fallout Reason',
				'fallout_reason_other' => 'Other',
				'mls_id' => 'MLS#',
				'is_our_listing' => 'Our Listing',
                'inspection_contingency_date' => 'Inspection Contingency Date',
                'appraisal_contingency_date' => 'Appraisal Contingency Date',
                'financing_contingency_date' => 'Financing Contingency Date',
                'ernest_money_deposit' => 'Ernest Money Deposit',
				'notes' => 'Notes',
				'is_referral' => 'Referral',
				'referral_commission' => 'Referral Commission',
				'commission_amount' => 'Gross Commission',
				'contract_execute_date' => 'Contract Execute Date',
				'contract_closing_date' => 'Contract Closing Date',
				'actual_closing_datetime' => 'Actual Closing Date',
				'is_mail_away' => 'Is Mail Away',
				'closing_location' => 'Closing Location',
                'closing_gift' => 'Closing Gift',
				'title_company_id' => 'Title Company',
				'other_agent_id' => 'Co-Broker Agent',
				'lender_id' => 'Lender',
                'home_warranty_company_id' => 'Home Warranty Company',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * getSaleTypes This is a model attribute for Closing Sale types
		 *
		 * @return array with key and value
		 */
		public static function getSaleTypes() {
			return array(
				self::SALE_TYPE_NORMAL_ID => 'Normal Equity',
				self::SALE_TYPE_NEW_CONSTRUCTION_ID => 'New Construction',
				self::SALE_TYPE_SHORT_SALE_ID => 'Short Sale',
				self::SALE_TYPE_REO_ID => 'REO',
                self::SALE_TYPE_RENTAL_ID => 'Rental',
			);
		}

		/**
		 * getStatus This is a model attribute for Closing Status types
		 *
		 * @return array with key and value
		 */
		public static function getStatusTypes() {
			return array(
				self::STATUS_TYPE_ACTIVE_ID => 'Pending',
				self::STATUS_TYPE_FALLOUT_ID => 'Fall-out',
				self::STATUS_TYPE_CLOSED_ID => 'Closed'
			);
		}

		public static function getPostCloseStatusTypes() {
			return array(
				self::POST_CLOSE_STATUS_TYPE_INCOMPLETE_ID => 'Incomplete',
				self::POST_CLOSE_STATUS_TYPE_COMPLETE_ID => 'Complete'
			);
		}

		/**
		 * getStatus This is a model attribute for Closing Financint types
		 *
		 * @return array with key and value
		 */
		public static function getFinancingTypes() {
			return array(
				self::FINANCING_TYPE_CASH_ID => 'Cash',
				self::FINANCING_TYPE_CONVENTIONAL_ID => 'Conventional',
				self::FINANCING_TYPE_FHA_ID => 'FHA',
				self::FINANCING_TYPE_VA_ID => 'VA',
				self::FINANCING_TYPE_BOND_ID => 'Bond',
                self::FINANCING_TYPE_USDA_ID => 'USDA',
				self::FINANCING_TYPE_OTHER_ID => 'Other',
			);
		}

		/**
		 * getFallout Types This is a model attribute for Closing Financint types
		 *
		 * @return array with key and value
		 */
		public static function getFalloutReasons() {
			return array(
				self::FALLOUT_REASON_FINANCING_ID => 'Financing',
				self::FALLOUT_REASON_INSPECTIONS_ID => 'Inspections',
				self::FALLOUT_REASON_OTHER_ID => 'Other',
			);
		}

		public function getStatusType() {
			$statusTypes = self::getStatusTypes();
			if (!$this->closing_status_ma || !isset($statusTypes[$this->closing_status_ma])) {
				return null;
			}

			return $statusTypes[$this->closing_status_ma];
		}

		public function getSaleType() {
			$saleTypes = self::getSaleTypes();
			if (!$this->sale_type_ma || !isset($saleTypes[$this->sale_type_ma])) {
				return null;
			}

			return $saleTypes[$this->sale_type_ma];
		}

		public function getFinancingType() {
			$financingTypes = self::getFinancingTypes();
			if (!$this->financing_type_ma || !isset($financingTypes[$this->financing_type_ma])) {
				return null;
			}

			return $financingTypes[$this->financing_type_ma];
		}

		public function getFalloutReason() {
			$falloutReasons = self::getFalloutReasons();
			if (!$this->fallout_reason_ma || !isset($financingTypes[$this->fallout_reason_ma])) {
				return null;
			}

			return $falloutReasons[$this->fallout_reason_ma];
		}

		/**
		 * getCompanyList
		 * Returns a list of all of the companies associated with a distinct instance of a closing
		 *
		 * @return array list of companies
		 */
		protected function getCompanyList() {

			if (!$this->companies) {
				return null;
			}

			if (!$this->_companyList) {

				foreach ($this->companies as $Company) {

					foreach ($Company->types as $Type) {

						$this->_companyList[$Type->id] = $Company;
					}
				}
			}

			return $this->_companyList;
		}

        /**
         * getLenders
         * Returns a list of inspector type companies
         *
         * @return array list of inspector type companies
         */
        public function getLenders() {

            $companies = $this->getCompanies();
            if (!$companies) {
                return null;
            }

            $inspectors = array();
            foreach ($companies as $company) {
                foreach ($company->types as $type) {
                    if (in_array($type->id, CompanyTypes::inspectors())) {
                        array_push($inspectors, $company);
                        break;
                    }
                }
            }

            return $inspectors;
        }

		/**
		 * getInspectors
		 * Returns a list of inspector type companies
		 *
		 * @return array list of inspector type companies
		 */
		public function getInspectors() {

			return $this->_inspectors;
		}

		/**
		 *
		 * @author jamesk 4/19/2016
		 * @author 813.330.0522
		 */
		public function getNetCommission(){
			if(!empty($this->commission_amount)){
				$agent = $this->getAgentCommission();
                $internalReferral = $this->getAgentInternalReferralCommission();
				return $this->commission_amount - $agent - $internalReferral;
			}else{return 0;}
		}

		public function getTitleAgent() {
			return Assignments::model()->find(array(
					'select' => 'assignee_contact_id',
					'condition' => 'assignment_type_id=:assignment_type_id AND component_type_id=:component_type_id AND component_id=:component_id',
					'params' => array(
						':assignment_type_id' => AssignmentTypes::TITLE_AGENT,
						':component_type_id' => ComponentTypes::CLOSINGS,
						':component_id' => $this->id,
					)
				)
			)->contact;
		}

		public function getTitleProcessor() {
			return Assignments::model()->find(array(
					'select' => 'assignee_contact_id',
					'condition' => 'assignment_type_id=:assignment_type_id AND component_type_id=:component_type_id AND component_id=:component_id',
					'params' => array(
						':assignment_type_id' => AssignmentTypes::TITLE_PROCESSOR,
						':component_type_id' => ComponentTypes::CLOSINGS,
						':component_id' => $this->id,
					)
				)
			)->contact;
		}

		public function getLoanOfficer() {
			return Assignments::model()->find(array(
					'select' => 'assignee_contact_id',
					'condition' => 'assignment_type_id=:assignment_type_id AND component_type_id=:component_type_id AND component_id=:component_id',
					'params' => array(
						':assignment_type_id' => AssignmentTypes::LOAN_OFFICER,
						':component_type_id' => ComponentTypes::CLOSINGS,
						':component_id' => $this->id,
					)
				)
			)->contact;
		}

        public function getLoanProcessor() {
            return Assignments::model()->find(array(
                    'select' => 'assignee_contact_id',
                    'condition' => 'assignment_type_id=:assignment_type_id AND component_type_id=:component_type_id AND component_id=:component_id',
                    'params' => array(
                        ':assignment_type_id' => AssignmentTypes::LOAN_PROCESSOR,
                        ':component_type_id' => ComponentTypes::CLOSINGS,
                        ':component_id' => $this->id,
                    )
                )
            )->contact;
        }

        public function getCoBrokeringAgent() {
            return Assignments::model()->find(array(
                    'select' => 'assignee_contact_id',
                    'condition' => 'assignment_type_id=:assignment_type_id AND component_type_id=:component_type_id AND component_id=:component_id',
                    'params' => array(
                        ':assignment_type_id' => AssignmentTypes::CO_BROKER_AGENT,
                        ':component_type_id' => ComponentTypes::CLOSINGS,
                        ':component_id' => $this->id,
                    )
                )
            )->contact;
		}

		/**
		 * getListSearchModel provides reusable logic for preparing for the list view
		 *
		 * @return null
		 */
		public static function getListSearchModel() {
			$model = new Closings;
			$model->unsetAttributes(); // clear any default values
			if (!$model->closing_status_ma) {
				$model->closing_status_ma = Closings::STATUS_TYPE_ACTIVE_ID;
			}

			if (isset($_GET['Closings'])) {
				$model->attributes = $_GET['Closings'];
			}

			$model->transaction = new Transactions('search');

			$model->transaction->contact = new Contacts('search');

			if (isset($_GET['Contacts'])) {
				$model->transaction->contact->attributes = $_GET['Contacts'];
			}

			$model->transaction->contact->emails = new Emails('search');

			if (isset($_GET['Emails'])) {
				$model->transaction->contact->emails->attributes = $_GET['Emails'];
			}

			$model->transaction->address = new Addresses('search');
			if (isset($_GET['Addresses'])) {
				$model->transaction->address->attributes = $_GET['Addresses'];
			}

			return $model;
		}

        protected function afterFind() {
            $this->inspection_contingency_date = Yii::app()->format->formatDate($this->inspection_contingency_date, StmFormatter::APP_DATE_FORMAT);
            $this->appraisal_contingency_date = Yii::app()->format->formatDate($this->appraisal_contingency_date, StmFormatter::APP_DATE_FORMAT);
            $this->financing_contingency_date = Yii::app()->format->formatDate($this->financing_contingency_date, StmFormatter::APP_DATE_FORMAT);

            $this->contract_execute_date = Yii::app()->format->formatDate($this->contract_execute_date, StmFormatter::APP_DATE_FORMAT);
            $this->contract_closing_date = Yii::app()->format->formatDate($this->contract_closing_date, StmFormatter::APP_DATE_FORMAT);
            $this->fallout_date = Yii::app()->format->formatDate($this->fallout_date, StmFormatter::APP_DATE_FORMAT);
            $this->actual_closing_datetime = Yii::app()->format->formatDate($this->actual_closing_datetime, StmFormatter::APP_DATETIME_PICKER_FORMAT);

            $this->ernest_money_deposit = Yii::app()->format->formatDecimal($this->ernest_money_deposit);
            $this->ernest_money_deposit = number_format((float) $this->ernest_money_deposit);

            return parent::afterFind();
        }

		protected function beforeValidate() {
			// If the value is a number based field strip commas or decimals from it.
			$this->price = Yii::app()->format->formatInteger($this->price);
			$this->loan_amount = Yii::app()->format->formatInteger($this->loan_amount);
			$this->commission_amount = Yii::app()->format->formatDecimal($this->commission_amount);
			$this->referral_commission = Yii::app()->format->formatDecimal($this->referral_commission);
            $this->ernest_money_deposit = Yii::app()->format->formatDecimal($this->ernest_money_deposit);
			return parent::beforeValidate();
		}

        public function validateClosingStatusActualDatetime($attribute, $params)
        {
            if ($this->closing_status_ma == Closings::STATUS_TYPE_CLOSED_ID && empty($this->actual_closing_datetime)) {
                $this->addError($attribute, 'Actual Closing Date/Time required to mark this as Closed.');
            }
        }

        protected function beforeSave() {
			if ($this->isNewRecord) {
				// Mark the closing status for a new record
				if (empty($this->closing_status_ma)) {
					$this->closing_status_ma = self::STATUS_TYPE_ACTIVE_ID;
				}
			}
            else {
                $this->_originalData = $this->findByPk($this->id)->attributes;
            }

			if ($this->contract_execute_date) {
				$this->contract_execute_date = Yii::app()->format->formatDate($this->contract_execute_date, StmFormatter::MYSQL_DATE_FORMAT);
			}

			if ($this->contract_closing_date) {
				$this->contract_closing_date = Yii::app()->format->formatDate($this->contract_closing_date, StmFormatter::MYSQL_DATE_FORMAT);
			}

			if ($this->actual_closing_datetime) {
				$this->actual_closing_datetime = Yii::app()->format->formatDate($this->actual_closing_datetime, StmFormatter::MYSQL_DATETIME_FORMAT);
			}

			if ($this->fallout_date) {
				$this->fallout_date = Yii::app()->format->formatDate($this->fallout_date, StmFormatter::MYSQL_DATETIME_FORMAT);
			} else {
				$this->fallout_date = null;
			}

            if ($this->inspection_contingency_date) {
                $this->inspection_contingency_date = Yii::app()->format->formatDate($this->inspection_contingency_date, StmFormatter::MYSQL_DATETIME_FORMAT);
            } else {
                $this->inspection_contingency_date = null;
            }

            if ($this->appraisal_contingency_date) {
                $this->appraisal_contingency_date = Yii::app()->format->formatDate($this->appraisal_contingency_date, StmFormatter::MYSQL_DATETIME_FORMAT);
            } else {
                $this->appraisal_contingency_date = null;
            }

            if ($this->financing_contingency_date) {
                $this->financing_contingency_date = Yii::app()->format->formatDate($this->financing_contingency_date, StmFormatter::MYSQL_DATETIME_FORMAT);
            } else {
                $this->financing_contingency_date = null;
            }

			if ($this->fallout_reason_other == "") {
				$this->fallout_reason_other = null;
			}

				return parent::beforeSave();
		}

		protected function afterSave() {

			$Transaction = $this->transaction;
			switch ($this->closing_status_ma) {
				case Closings::STATUS_TYPE_CLOSED_ID:
					$Transaction->transaction_status_id = TransactionStatus::CLOSED_ID;
					break;
				case Closings::STATUS_TYPE_FALLOUT_ID:
					if ($Transaction->component_type_id == ComponentTypes::BUYERS) {
						$Transaction->transaction_status_id = TransactionStatus::HOT_A_LEAD_ID;
					} elseif ($Transaction->component_type_id == ComponentTypes::SELLERS) {
						$Transaction->transaction_status_id = TransactionStatus::ACTIVE_LISTING_ID;
					}
					break;
				case Closings::STATUS_TYPE_ACTIVE_ID:
					$Transaction->transaction_status_id = TransactionStatus::UNDER_CONTRACT_ID;
					break;
			}
			$Transaction->save();

            // @todo: The model should not be sending off an email, this should be part of the action not the responsibility of the model
            if ($this->getIsNewRecord() && $this->sendCongratsEmail && $this->closing_status_ma != Closings::STATUS_TYPE_FALLOUT_ID) {
				$this->sendCongratsEmail();
			}
            else {
                // if the contract date is changed and there is an action plan task dependent on this, update it
                if(!empty($this->_originalData['contract_closing_date']) && Yii::app()->format->formatDate($this->contract_closing_date, StmFormatter::MYSQL_DATE_FORMAT) !== Yii::app()->format->formatDate($this->_originalData['contract_closing_date'], StmFormatter::MYSQL_DATE_FORMAT)) {

                    Tasks::updateElasticActionTaskDueDate($this, ActionPlanItems::DUE_REFERENCE_CLOSING_DATE_ID);
                }
            }

			$this->_applyTriggers();

            parent::afterSave();
		}

		protected function sendCongratsEmail() {
            $toEmail = Yii::app()->user->settings->email_all;
            if(empty($toEmail)) {
                return;
            }

            $phrases = array(
				'Awesome!!',
				'You rock! Wohooooo!',
				'Bling Bling, awesome!!!',
				'Oh yeahhh!!!',
				'Yipee Skippee!!!',
				'Heck yeaaahhhh!!',
				'Boom, blowing it up!!!',
				'Cool dealio, shmeelio!!!',
				'Fantasticoooo!!! :D',
				'Roooaaarrr!!! ',
			);
			$randomKey = array_rand($phrases);

			$photos = array('jump.jpg','kids.jpg','smilingdog.jpg','touchdown.jpg');
			$randomPhotoKey = array_rand($photos);


			$message = new YiiMailMessage;
			$logoImage = '<br /><br /><img src="' . Yii::app()->user->imagesBaseUrl . 'logo_admin.png" alt="Display Team Logo">';

			$assignedAgent = $this->transaction->assignments;
			$agentString = '';
			if(!empty($assignedAgent)) {
				foreach($assignedAgent as $Assignment) {
					$agentString .= ($agentString)? ', ': '';
					$agentString .= $Assignment->contact->fullName;
				}
			}

			$content =
				'<span style="font-weight:bold;font-size:16px;">Congrats ' . $agentString . '! New executed contract with ' . $this->contact->fullName . '!</span><br />Closing is set for '
					. Yii::app()->format->formatDateTime($this->contract_closing_date) . '<br /><br /><span style="color:blue;font-weight:bold;font-style:italic;font-size:14px;">' . $phrases[$randomKey].'</span><br/><br/>'
					.'<img src="' . Yii::app()->controller->getModule()->getCdnImageAssetsUrl().'closings/' . $photos[$randomPhotoKey]
					. '" alt="Congrats on New Executed Contract - '.$phrases[$randomKey].'!">'.$logoImage;
//			$message->view = 'plain';
            $message->setBody($content, 'text/html');
			$message->setSubject('Congrats! ' . $agentString . ' has a New Executed Contract!');

			if (YII_DEBUG) {
				$message->addTo(StmMailMessage::DEBUG_EMAIL_ADDRESS, 'Team Congrats');
			} else {
				$message->addTo($toEmail, 'Team Congrats');
			}

			$message->addFrom(Yii::app()->user->settings->email_all, 'Team Congrats');
//			Yii::app()->mail->send($message);
            StmZendMail::sendYiiMailMessage($message);

		}

		protected function _applyTriggers()
		{
			if(!empty($actionPlanIdsToApply = Yii::app()->db->createCommand("SELECT action_plan_id FROM triggers WHERE status_ma=1 AND component_type_id=".ComponentTypes::CLOSINGS." AND action='".Triggers::ACTION_APPLY_ACTION_PLAN."' AND event IN('".Triggers::EVENT_CLOSING_STATUS."') AND event_data='".$this->closing_status_ma."'")->queryColumn())) {

				$criteria = new CDbCriteria();
				$criteria->addInCondition('id', $actionPlanIdsToApply);
				$actionPlansToApply = ActionPlans::model()->findAll($criteria);

				foreach($actionPlansToApply as $actionPlanToApply) {
					$actionPlanToApply->apply($this, null, 'Auto-triggered Closing Status Updates to: '.$this->getStatusTypes()[$this->closing_status_ma]);
				}
			}

			if(!empty($actionPlanIdsToUnapply = Yii::app()->db->createCommand("SELECT action_plan_id FROM triggers WHERE status_ma=1 AND component_type_id=".ComponentTypes::CLOSINGS." AND action='".Triggers::ACTION_UNAPPLY_ACTION_PLAN."' AND event IN('".Triggers::EVENT_CLOSING_STATUS."') AND event_data='".$this->closing_status_ma."'")->queryColumn())) {

				$criteria = new CDbCriteria();
				$criteria->addInCondition('id', $actionPlanIdsToUnapply);
				$actionPlansToUnapply = ActionPlans::model()->findAll($criteria);
				foreach($actionPlansToUnapply as $actionPlanToUnapply) {
					$actionPlanToUnapply->unapply($this, 'Auto-triggered Closing Status Updates to: '.$this->getStatusTypes()[$this->closing_status_ma]);
				}
			}
		}

		public function scopes() {

			return array();
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('transaction_id', $this->transaction_id);
			$criteria->compare('closing_status_ma', $this->closing_status_ma);
			$criteria->compare('price', $this->price);
			$criteria->compare('sale_type_ma', $this->sale_type_ma);
			$criteria->compare('fallout_date', $this->fallout_date, true);
			$criteria->compare('mls_id', $this->mls_id, true);
			$criteria->compare('is_our_listing', $this->is_our_listing);
            $criteria->compare('inspection_contingency_date', $this->inspection_contingency_date);
            $criteria->compare('appraisal_contingency_date', $this->appraisal_contingency_date);
            $criteria->compare('financing_contingency_date', $this->financing_contingency_date);
            $criteria->compare('ernest_money_deposit', $this->ernest_money_deposit);
			$criteria->compare('notes', $this->notes, true);
			$criteria->compare('is_referral', $this->is_referral);
			$criteria->compare('referral_commission', $this->referral_commission, true);
			$criteria->compare('commission_amount', $this->commission_amount, true);
			$criteria->compare('contract_execute_date', $this->contract_execute_date, true);
			$criteria->compare('contract_closing_date', $this->contract_closing_date, true);
			$criteria->compare('is_mail_away', $this->is_mail_away);
            $criteria->compare('closing_location', $this->closing_location, true);
            $criteria->compare('closing_gift', $this->closing_gift);
            $criteria->compare('other_agent_id', $this->other_agent_id);
            $criteria->compare('lender_id', $this->lender_id);
            $criteria->compare('home_warranty_company_id', $this->home_warranty_company_id);
            $criteria->compare('is_mail_away', $this->is_mail_away);
			$criteria->compare('is_deleted', $this->is_deleted);

			$criteria->with = array();
            $criteria->with = CMap::mergeArray($criteria->with, array('transaction'));
            $criteria->compare('transaction.account_id', Yii::app()->user->accountId);

            if(!Yii::app()->user->checkAccess('owner') && (Yii::app()->user->checkAccess('noBuyerAccess') || Yii::app()->user->checkAccess('noSellererAccess'))) {

                //$criteria->with = CMap::mergeArray($criteria->with, array('transaction'));

                if(Yii::app()->user->checkAccess('noBuyerAccess')) {
                    $criteria->addCondition('transaction.component_type_id !='.ComponentTypes::BUYERS);
                }

                if(Yii::app()->user->checkAccess('noSellererAccess')) {
                    $criteria->addCondition('transaction.component_type_id !='.ComponentTypes::SELLERS);
                }
            }

            if($this->assignmentIds) {
                $criteria->together = true;
                $criteria->join = 'RIGHT JOIN transactions t2 ON t.transaction_id=t2.id RIGHT JOIN assignments assignments ON ((t2.component_type_id=assignments.component_type_id AND t2.id=assignments.component_id) OR (assignments.component_type_id='.ComponentTypes::CLOSINGS.' AND assignments.component_id=t.id)) AND assignments.is_deleted=0 AND assignments.assignee_contact_id='.$this->assignmentIds;
            }

			// Search for closings transaction type
			if ($this->transaction->contact->first_name || $this->transaction->contact->last_name || $this->transaction->contact->contactFullName) {
				// Filter search by a contact's first name, last name, or a combination of the two.
				$criteria->with = CMap::mergeArray($criteria->with, array('transaction.contact'));

				// Search explicitly by first name
				if ($this->transaction->contact->first_name) {
					$criteria->compare('contact.first_name', trim($this->transaction->contact->first_name), true);
				}

				// Search explicitly by last name
				if ($this->transaction->contact->last_name) {
					$criteria->compare('contact.last_name', trim($this->transaction->contact->last_name), true);
				}

				// Search any portion of either name fields
				if ($this->transaction->contact->contactFullName) {
					$criteria->compare("CONCAT(contact.first_name, ' ', contact.last_name)", $this->transaction->contact->contactFullName, true);
				}
			}

			if ($this->transaction->contact->emails->email) {
				// Filter by a contact's e-mail address via the transaction
				$criteria->with = CMap::mergeArray($criteria->with, array('transaction.contact.emails'));
				$criteria->compare('emails.email', $this->transaction->contact->emails->email, true);
				$criteria->together = true;
			}

			if ($this->transaction->address->address) {
				// Filter by a address via the transaction
				$criteria->with = CMap::mergeArray($criteria->with, array('transaction.address'));
				$criteria->compare('address.address', $this->transaction->address->address, true);
				$criteria->together = true;
			}

			if ($this->closingDateFrom) {
				$criteria->compare('contract_closing_date', '>=' . date('Y-m-d', strtotime($this->closingDateFrom)), true);
			}

			if ($this->closingDateTo) {
				$criteria->compare('contract_closing_date', '<' . date('Y-m-d', strtotime($this->closingDateTo . ' +1 day')), true);
			}

			$criteria->order = 'contract_closing_date ASC';

            $command=$this->getCommandBuilder()->createFindCommand($this->getTableSchema(),$criteria);
            $x = $command->getText();
//            SELECT `t`.`id`, `t`.`transaction_id`, `t`.`closing_status_ma`, `t`.`post_close_status_ma`, `t`.`fallout_date`, `t`.`fallout_reason_ma`, `t`.`fallout_reason_other`, `t`.`price`, `t`.`sale_type_ma`, `t`.`financing_type_ma`, `t`.`loan_amount`, `t`.`is_new_construction`, `t`.`mls_id`, `t`.`is_our_listing`, `t`.`ernest_money_deposit`, `t`.`inspection_contingency_date`, `t`.`appraisal_contingency_date`, `t`.`financing_contingency_date`, `t`.`contingencies`, `t`.`notes`, `t`.`is_referral`, `t`.`referral_commission`, `t`.`commission_amount`, `t`.`contract_execute_date`, `t`.`contract_closing_date`, `t`.`actual_closing_datetime`, `t`.`is_mail_away`, `t`.`closing_location`, `t`.`closing_gift`, `t`.`title_company_id`, `t`.`other_agent_id`, `t`.`lender_id`, `t`.`home_warranty_company_id`, `t`.`is_deleted` FROM `closings` `t` LEFT JOIN transactions transaction ON t.transaction_id=transaction.id LEFT JOIN assignments assignments ON transaction.component_type_id=assignments.component_type_id AND transaction.id=assignments.component_id AND assignments.is_deleted=0 AND assignments.assignee_contact_id=7 WHERE closing_status_ma=:ycp2 ORDER BY contract_closing_date ASC
			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination'=>array('pageSize'=>100),
			));
		}

		public function getData() {
			return $this->data;
		}

		public function setData($data) {
			$this->data = $data;
		}

		public function getContactId() {
			return $this->transaction->contact_id;
		}

		public function getComponentChain() {
			$chain = array($this->asComponentTuple());

			$includeCompleted = true;
			$Tasks = $this->getTasks($this, $includeCompleted);

			if (count($Tasks) > 0) {
				foreach ($Tasks as $Task) {
					array_push($chain, $Task->asComponentTuple());
				}
			}

			return $chain;
		}

		public function asComponentTuple() {
			return array(
				'componentTypeId' => ComponentTypes::CLOSINGS,
				'componentId' => $this->id
			);
		}

        public static function getForeverClients($params, $returnType=self::FOREVER_CLIENT_RETURN_TYPE_DATA)
        {
            Yii::import('admin_module.controllers.ForeverClientsController');

            $lastCalledSubQuery = "(SELECT MAX(a1.activity_date) FROM activity_log a1 WHERE a1.component_type_id=".ComponentTypes::CONTACTS." AND a1.component_id=c.id AND a1.task_type_id IN(".TaskTypes::PHONE.",".TaskTypes::DIALER_ANSWER.",".TaskTypes::DIALER_NO_ANSWER.",".TaskTypes::DIALER_LEFT_VOICEMAIL."))";
            $lastSpokeToSubQuery = "(SELECT MAX(a2.activity_date) FROM activity_log a2 WHERE a2.component_type_id=".ComponentTypes::CONTACTS." AND a2.component_id=c.id AND a2.is_spoke_to=1)";
            $lastHandWrittenNoteSubQuery = "(SELECT MAX(a3.activity_date) FROM activity_log a3 WHERE a3.component_type_id=".ComponentTypes::CONTACTS." AND a3.component_id=c.id AND a3.task_type_id=".TaskTypes::HANDWRITTEN_NOTE.")";
            $command = Yii::app()->db->createCommand()
                ->select("
                c.id contact_id,
                c.first_name,
                c.last_name,
                c.spouse_first_name,
                c.spouse_last_name,
                (select GROUP_CONCAT(DISTINCT CONCAT(p.id,'-',p.phone) ORDER BY p.phone SEPARATOR ',') FROM phones p WHERE p.contact_id=c.id AND p.is_deleted=0) phone_id_pairs,
                (select GROUP_CONCAT(DISTINCT e.email ORDER BY e.email SEPARATOR ',') FROM emails e WHERE e.contact_id=c.id AND e.is_deleted=0) emails,
                (select GROUP_CONCAT(DISTINCT IF(v.value, CONCAT(ca.label,': ',DATE_FORMAT(v.value, '%c/%e/%Y')),'') ORDER BY v.value SEPARATOR '<br>') FROM contact_attribute_values v JOIN contact_attributes ca ON ca.id=v.contact_attribute_id AND v.is_deleted=0 WHERE v.contact_id=c.id AND v.contact_attribute_id IN(".ContactAttributes::BIRTHDAY.",".ContactAttributes::CLOSING_ANNIVERSARY.",".ContactAttributes::SPOUSE_BIRTHDAY.",".ContactAttributes::WEDDING_ANNIVERSARY.") AND v.is_deleted=0) special_dates,
                GROUP_CONCAT(DISTINCT CONCAT(DATE_FORMAT(cl.actual_closing_datetime, '%c/%e/%Y'),' - ',IF(t.component_type_id=".ComponentTypes::SELLERS.", 'Seller', 'Buyer')) ORDER BY cl.actual_closing_datetime DESC SEPARATOR '<br>') closing_history,
                DATE_FORMAT($lastSpokeToSubQuery, '%c/%e/%Y') last_spoke_to,
                DATE_FORMAT($lastCalledSubQuery, '%c/%e/%Y') last_called,
                DATE_FORMAT($lastHandWrittenNoteSubQuery, '%c/%e/%Y') last_hand_written_note
            ")
                ->from('closings cl')
                ->join('transactions t','t.id=cl.transaction_id')
                ->join('contacts c', 'c.id=t.contact_id')
                ->where('cl.is_forever_client=1');

            if($params['last_spoke_to_days']) {
                $command->andWhere("COALESCE($lastSpokeToSubQuery, 0) < DATE_SUB(NOW(), INTERVAL ".$params['last_spoke_to_days'].' DAY) ');
            }

            if($params['closingStatus']) {
                $command->andWhere('cl.closing_status_ma = :closingStatus', array(':closingStatus' => $params['closingStatus']));
            }
            else {
                $command->andWhere('cl.closing_status_ma = :closingStatus', array(':closingStatus' => Closings::STATUS_TYPE_CLOSED_ID));
            }

            if($params['firstName']) {
                $command->andWhere('c.first_name like :firstName', array(':firstName' => '%'.$params['firstName'].'%'));
            }

            if($params['lastName']) {
                $command->andWhere('c.last_name like :lastName', array(':lastName' => '%'.$params['lastName'].'%'));
            }

            if($params['email']) {
                $command->join('emails e2','e2.contact_id=c.id');
                $command->andWhere('e2.email like :email', array(':email' => '%'.$params['email'].'%'));
            }

            if($params['phone']) {
                $command->join('phones p2','p2.contact_id=c.id');
                $command->andWhere('p2.phone like :phone', array(':phone' => '%'.$params['phone'].'%'));
            }

            if($params['address']) {
                $command->join('addresses a2','t.address_id=a2.id');
                $command->andWhere('a2.address like :address', array(':address' => '%'.$params['address'].'%'));
            }

            if($params['closingDateFrom']) {
                $command->andWhere('DATE(cl.actual_closing_datetime) >= :closingDateFrom', array(':closingDateFrom' => date('Y-m-d', strtotime($params['closingDateFrom']))));
            }

            if($params['closingDateTo']) {
                $command->andWhere('DATE(cl.actual_closing_datetime) <= :closingDateTo', array(':closingDateTo' => date('Y-m-d', strtotime($params['closingDateTo']))));
            }

            if($params['specialDateFrom'] && $params['specialDateTo']) {
                $command->leftJoin('contact_attribute_values v2','v2.contact_id=c.id AND v2.is_deleted=0 AND v2.contact_attribute_id IN('.ContactAttributes::BIRTHDAY.",".ContactAttributes::CLOSING_ANNIVERSARY.",".ContactAttributes::SPOUSE_BIRTHDAY.",".ContactAttributes::WEDDING_ANNIVERSARY.')');

                //@todo: make it auto include from actual closing dates from closing table ... split into another $command->orWhere()??
                $closingTableSpecialDateSubQuery = 'DATE_FORMAT(cl.actual_closing_datetime, "%m%d") BETWEEN :specialClosingDateFrom AND :specialClosingDateTo'; //DATE_FORMAT(cl.actual_closing_datetime, "%m%d")
//
                $command->andWhere('(DATE_FORMAT(STR_TO_DATE(v2.value, "%Y-%m-%d"), "%m%d") >= :specialDateFrom AND DATE_FORMAT(STR_TO_DATE(v2.value, "%Y-%m-%d"), "%m%d") <= :specialDateTo) OR ('.$closingTableSpecialDateSubQuery.')',

                    //params
                    array(':specialDateFrom' => date('md', strtotime($params['specialDateFrom'])), ':specialDateTo' => date('md', strtotime($params['specialDateTo'])), ':specialClosingDateFrom' => date('md', strtotime($params['specialDateFrom'])), ':specialClosingDateTo' => date('md', strtotime($params['specialDateTo']))));
            }

            // returns contactId only as single column
            if($returnType == self::FOREVER_CLIENT_RETURN_TYPE_CONTACT_MODELS) {
                $command->select = 'c.id';
                $command->distinct = true;
                $contactIds = $command->queryColumn();

                $criteria = new CDbCriteria();
                $criteria->addInCondition('id', $contactIds);
                return Contacts::model()->findAll($criteria);
            }
            elseif($returnType == self::FOREVER_CLIENT_RETURN_TYPE_COUNT) {
                $command->select = 'count(c.id)';
                return $command->queryScalar();
            }

            $countCommand = clone $command;
            $countCommand->select = 'count(DISTINCT c.id)';

            $dataCommand = clone $command;
            $dataCommand->limit = ForeverClientsController::PAGE_SIZE;
            $dataCommand->group('c.id');

            $dataCommand->order = $orderBy = 'cl.id ASC';
            if($params['sort']) {

                switch($params['sort']) {
                    case 'last_spoke_to_asc';
                        $orderBy = 'last_spoke_to ASC';
                        break;

                    case 'last_spoke_to_desc';
                        $orderBy = 'last_spoke_to DESC';
                        break;

                    case 'last_called_asc';
                        $orderBy = 'last_called ASC';
                        break;

                    case 'last_called_desc';
                        $orderBy = 'last_called DESC';
                        break;

                    case 'last_hand_written_note_asc';
                        $orderBy = 'last_hand_written_note ASC';
                        break;

                    case 'last_hand_written_note_desc';
                        $orderBy = 'last_hand_written_note DESC';
                        break;
                }

                $dataCommand->order = $orderBy;
            }

            if($params['page'] >1) {
                $dataCommand->offset(($params['page']-1) * self::PAGE_SIZE);
            }

            $data['count'] = $countCommand->queryScalar();
            $data['leads'] = $dataCommand->queryAll();
            return $data;
        }
	}
