<?php

/**
 * This is the model class for table "email_status_log".
 *
 * The followings are the available columns in table 'email_status_log':
 * @property integer $id
 * @property integer $email_id
 * @property integer $email_status_id
 * @property string $email
 * @property integer $added_by_component_type_id
 * @property integer $added_by_component_id
 * @property string $added
 *
 * The followings are the available model relations:
 * @property ComponentTypes $addedByComponentType
 * @property Emails $email0
 * @property EmailStatuses $emailStatus
 */
class EmailStatusLog extends StmBaseActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailStatusLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_status_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email_id, email_status_id, email, added_by_component_type_id, added_by_component_id', 'required'),
			array('email_id, email_status_id, added_by_component_type_id, added_by_component_id', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>255),
			array('added', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email_id, email_status_id, email, added_by_component_type_id, added_by_component_id, added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addedByComponentType' => array(self::BELONGS_TO, 'ComponentTypes', 'added_by_component_type_id'),
			'email' => array(self::BELONGS_TO, 'Emails', 'email_id'),
			'emailStatus' => array(self::BELONGS_TO, 'EmailStatuses', 'email_status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email_id' => 'Email',
			'email_status_id' => 'Email Status',
			'email' => 'Email',
			'added_by_component_type_id' => 'Added By Component Type',
			'added_by_component_id' => 'Added By Component',
			'added' => 'Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email_id',$this->email_id);
		$criteria->compare('email_status_id',$this->email_status_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('added_by_component_type_id',$this->added_by_component_type_id);
		$criteria->compare('added_by_component_id',$this->added_by_component_id);
		$criteria->compare('added',$this->added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}