<?php

/**
 * This is the model class for table "cms_content_contact_lu".
 *
 * The followings are the available columns in table 'cms_content_contact_lu':
 * @property integer $cms_content_id
 * @property integer $contact_id
 */
class CmsContentContactLu extends StmBaseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CmsContentContactLu the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cms_content_contact_lu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cms_content_id, contact_id', 'required'),
            array('cms_content_id, contact_id', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('cms_content_id, contact_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'cms_content_id' => 'Cms Content',
            'contact_id' => 'Contact',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('cms_content_id',$this->cms_content_id);
        $criteria->compare('contact_id',$this->contact_id);

        return new CActiveDataProvider($this, array(
                                              'criteria'=>$criteria,
                                              ));
    }
}