<?php

	/**
	 * This is the model class for table "contact_attribute_types".
	 *
	 * The followings are the available columns in table 'contact_attribute_types':
	 *
	 * @property integer                     $id
	 * @property integer                     $data_type_id
	 * @property string                      $name
     * @property integer                     $sort_order
	 * @property integer                     $is_deleted
	 *
	 * The followings are the available model relations:
	 * @property ContactAttributeAccountLu[] $contactAttributeAccountLus
	 * @property ContactAttributes[]         $contactAttributes
	 */
	class ContactAttributes extends StmBaseActiveRecord {

        const BIRTHDAY = 1;
        const CLOSING_ANNIVERSARY = 2;
        const WEDDING_ANNIVERSARY = 11;
        const SPOUSE_BIRTHDAY = 12;

        const HQ_BROKERAGE_NAME = 1000;
        const HQ_TEAM_NAME = 1001;
        const HQ_IS_BROKER_OWNER = 1002;
        const HQ_YEARS_IN_BUSINESS=1003;
        const HQ_HAVE_RE_COACH=1004;
        const HQ_RE_COACH=1005;
        const HQ_PREV_COACHES=1006;
        const HQ_CURRENT_CRM=1007;
        const HQ_PREV_CRM=1008;
        const HQ_WEBSITE = 1009;

		const HQ_WISHLIST=1011;
		const HQ_GREATEST_CHALLENGE=1012;
		const HQ_SOLD_LAST_NUMBER=1013;
		const HQ_SOLD_LAST_DOLLAR=1014;
		const HQ_SOLD_THIS_NUMBER=1015;
		const HQ_SOLD_THIS_DOLLAR=1016;
		const HQ_TEAM_STRUCTURE=1017;

        const HQ_TARGET_DATE = 1018;
        const HQ_SOLD_LAST_12_MO = 1019;
        const HQ_TEAM_SIZE = 1020;
        const HQ_PROFILE_COMPLETE = 1021;

        const HQ_PIPELINE = 1022;
        const HQ_LISTING_COUNT = 1023;
        const HQ_AGENT_ID = 1024;
        const HQ_BROKERAGE_ID = 1025;
        const HQ_STATE_ID = 1026;
        const HQ_KW_MARKET_CENTER_NUMBER = 1027;
        const HQ_CITY = 1028;
        const HQ_ROLE = 1029;
        const HQ_REGION = 1030;

        public $accountScope = true;

		//cached collection of contact attribute models, name and labels
        protected $_contactAttributes;

        /**
         * @var array $contactAttributeModels collection of contact attribute models to make 1 db call when multiple uses in a form
         */
        protected  static $_contactAttributeNameId = array();
        protected  static $_contactAttributeIdLabel = array();

        /**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return ContactAttributeTypes the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'contact_attributes';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'id, sort_order, is_deleted',
					'numerical',
					'integerOnly' => true
				),
				array(
					'name, label',
					'length',
					'max' => 63
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, label, sort_order, is_deleted',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'contactAttributeAccountLu' => array(
					self::HAS_MANY,
					'ContactAttributeAccountLu',
					'contact_attribute_id'
				),
				'contactAttributeValue' => array(
					self::HAS_ONE,
					'ContactAttributeValues',
					'contact_attribute_id'
				),
				'contacts' => array(
					self::MANY_MANY,
					'Contacts',
					'contact_attributes(contact_attribute_id, contact_id)'
				),
				'dataType' => array(
					self::BELONGS_TO,
					'DataTypes',
					'data_type_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Name',
                'sort_order' => 'Sort Order',
				'is_deleted' => 'Is Deleted',
			);
		}

		/**
		 * Filter every SELECT request through the account id of the logged in user
		 *
		 * @return array Passed to the CDbCriteria object
		 */
		public function defaultScope() {
			return array(
				'with' => array('contactAttributeAccountLu'),
				'condition' => 'account_id=:account_id',
				'params' => array(':account_id' => Yii::app()->user->accountId),
				'order' => 'sort_order ASC',
			);
		}

		/**
		 * Caches fields by transaction type id
		 *
		 * @param $name Searches the index by the 'name' column in contact_attributes
		 *
		 * @return mixed A standard object containing all of the values pertaining to the field
		 * @throws CHttpException If there is no contact attribute field associated with the given name.
		 */
		public function getField($name) {
			if (!$this->_contactAttributes) {
				foreach (ContactAttributes::model()->byIgnoreAccountScope()->findAll() as $ContactAttribute) {
					$this->_contactAttributes[$ContactAttribute->name] = (object)$ContactAttribute->attributes;
				}
			}

			if ($this->_contactAttributes[$name]) {
				return $this->_contactAttributes[$name];
			}

			throw new CHttpException(500, 'Could not find a field related to this form.');
		}

        /**
         * Get Data Form key name used for forms and rules validation
         * Returns the name to be used in a form using the data array property to contain all the contact attribute values in a id=>value pair.
         *
         * @param string $name contact_attributes name
         * @return string form data[data field name's pk]
         */
        public static function getDataField($name, $fieldName='data')
        {
            if(!isset(self::$_contactAttributeNameId[$name])) {
                self::loadAttributeArrays();
            }

            return $fieldName.'[' . self::$_contactAttributeNameId[$name] . ']';
        }

        public static function getIdByName($name)
        {
            if(!isset(self::$_contactAttributeNameId[$name])) {
                self::loadAttributeArrays();
            }

            return self::$_contactAttributeNameId[$name];
        }

        public static function getLabelById($id)
        {
            if(!isset(self::$_contactAttributeIdLabel[$id])) {
                self::loadAttributeArrays();
            }

            return self::$_contactAttributeIdLabel[$id];
        }

        public static function loadAttributeArrays()
        {
            $contactAttributes = Yii::app()->db->createCommand()
                ->select('id, name, label')
                ->from('contact_attributes')
                ->queryAll();

            foreach($contactAttributes as $contactAttribute) {
                $contactAttribute = (object) $contactAttribute;
                self::$_contactAttributeNameId[$contactAttribute->name] = $contactAttribute->id;
                self::$_contactAttributeIdLabel[$contactAttribute->id] = $contactAttribute->label;
            }
        }

        /**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('label', $this->label, true);
            $criteria->compare('sort_order', $this->sort_order, true);
			$criteria->compare('is_deleted', $this->is_deleted);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}
