<?php
	/**
	 * ApiCriteria
	 *
	 * @author Chris Willard
	 * @since  12/10/2012
	 */

	/**
	 * Example ApiCriteria array structure for Julington Creek Plantation featured area:
	 *   array(
	 *     [0] => array( ['common_subdivision'] =>
	 *                    array(
	 *                        array( array('value'=>'%Julington Creek Plan%',
	 *                                     'operator'=>'like',
	 *                                     'conjunctor'=>'or'
	 *                                     )
	 *                    )
	 *
	 *            )
	 *      [1] => array( ['legal_subdivision'] =>
	 *                    array(
	 *                            array( array('value'=>'%Julington Creek Plan%',
	 *                                         'operator'=>'like',
	 *                                         'conjunctor'=>'or'
	 *                                         )
	 *                    )
	 *
	 *            )
	 *    )
	 */

	class ApiCriteria extends CFormModel {

		public $account_id;
		protected $_dbCriteria;
		protected $_apiConditions = array();
		protected $_secondaryMlsTableApplied = false;

		protected $lastConjunctor;

		public function addApiCondition(ApiCriteriaCondition $ApiCriteriaCondition) {

			if (!is_array($this->_apiConditions)) {
				$this->_apiConditions = array();
			}

			//check to see if it exists (ex. price_min, price_max) and do an array merge?? - is that needed or can 2 price conditions exists

			return array_push($this->_apiConditions, $ApiCriteriaCondition);
		}

		public function getApiConditions() {

			return $this->_apiConditions;
		}

		public function getDbCriteria() {

			$this->_dbCriteria = new CDbCriteria;
			if (!$this->_apiConditions) {
				return $this->_dbCriteria;
			}

			foreach ($this->_apiConditions as $ApiCondition) {
				$this->applyDbCondition($ApiCondition);
			}

			return $this->_dbCriteria;
		}

		protected function applyDbCondition(ApiCriteriaCondition $ApiCriteriaCondition) {
			foreach ($ApiCriteriaCondition->condition as $conditionPart) {
				$operator = $conditionPart['operator'];

				if (!$operator && isset($ApiCriteriaCondition->condition['is_nested'])) {
					throw new CHttpException(500, 'Could not apply db condition.');
				}

				$conjunctor = isset($conditionPart['conjunctor']) ? $conditionPart['conjunctor'] : $this->lastConjunctor;
				$this->lastConjunctor = $conjunctor; // preserve the last conjunctor

				if ($this->isValidOperator($operator)) {

					$value = $conditionPart['value'];

					// checks to see if the field is in the secondary table and joins if needed.
					//				if ($operator != ApiCriteriaCondition::OPERATOR_NESTED_FLAG)
					//					if ($this->checkSecondaryField($ApiCriteriaCondition->column)) {
					//						$tableName = Yii::app()->user->board->prefixedName;
					//						$tableName = MlsPropertiesSecondary::getTableNameFromBoard($tableName);
					//						$ApiCriteriaCondition->column = $tableName.'.'.$ApiCriteriaCondition->column;
					//					}

					switch ($operator) {

						case ApiCriteriaCondition::OPERATOR_LIKE:
							$this->_dbCriteria->compare($ApiCriteriaCondition->column, $value, $partialMatch = true, $conjunctor, $escape = false);
							// var_dump($this->_dbCriteria);exit;
							break;

						case ApiCriteriaCondition::OPERATOR_IN:
							$this->_dbCriteria->addInCondition($ApiCriteriaCondition->column, $value, $conjunctor);
							break;

						case ApiCriteriaCondition::OPERATOR_NOT_IN:
							$this->_dbCriteria->addNotInCondition($ApiCriteriaCondition->column, $value, $conjunctor);
							break;

						case ApiCriteriaCondition::OPERATOR_GT:
						case ApiCriteriaCondition::OPERATOR_LT:
						case ApiCriteriaCondition::OPERATOR_GTE:
						case ApiCriteriaCondition::OPERATOR_LTE:
							$this->_dbCriteria->addCondition($ApiCriteriaCondition->column . " $operator '$value'", $conjunctor);
							break;

						case ApiCriteriaCondition::OPERATOR_NESTED_FLAG:
							$this->_dbCriteria->addCondition($ApiCriteriaCondition->column, $conjunctor);
							break;

						default:
							$this->_dbCriteria->compare($ApiCriteriaCondition->column, $value, $partialMatch = false, $conjunctor, $escape = false);
							break;
					}
				} else {

					throw new CHttpException(500, 'Could not apply db condition.');
				}
			}
		}

		/**
		 * checkSecondaryField checks to see if field is in the secondary table and joins it as needed
		 *
		 * @param  [type] $name [description]
		 *
		 * @return none
		 */
		public function checkSecondaryField($column_name) {
			if ($this->_secondaryMlsTableApplied) {
				return;
			}

			if (in_array($column_name, $this->secondaryFields)) {
				$tableName = Yii::app()->user->board->prefixedName;
				$tableName = MlsPropertiesSecondary::getTableNameFromBoard($tableName);

				$this->getDbCriteria()->mergeWith(array(
						'join' => $tableName,
					)
				);
				$this->_secondaryMlsTableApplied = true;

				return true;
			}
		}

		/**
		 * getSecondaryFields returns array of secondary fields for MlsPropertiesSecondary
		 *
		 * @return array of field names
		 */
		public function getSecondaryFields() {
			// isCliEnv
			if (defined('STDIN')) {
				$tableName = Accounts::model()->findByPk($this->account_id)->board->prefixedName;
			} else {
				$tableName = Yii::app()->user->board->prefixedName;
			}

			$model = MlsPropertiesSecondary::model($tableName);
			$attributes = $model->attributes;
			$data = array();
			foreach ($attributes as $key => $name) {
				array_push($data, $key);
			}

			return $data;
		}

		public function getIsSecondaryTableApplied() { //checkForSecondaryProperty
		}

		public function isValidOperator($operator) {

			$validOperators = array_flip(ApiCriteriaCondition::getOperators());
			if (isset($validOperators[$operator])) {
				return true;
			}

			return false;
		}

		public function isValidConjunctor($conjunctor) {

			$validConjunctors = array_flip(ApiCriteriaCondition::getConjunctors());
			if (isset($validConjunctors[$conjunctor])) {
				return true;
			}

			return false;
		}

		/**
		 * findApiCriteria searches existing apiConditions array to see a condition with a term exists.
		 *
		 * @param  string $column that you want to find
		 *
		 * @return mixed array with key and ApiCriteria as elements if found, false if not found
		 */
		public function findApiCriteriaByTerm($column) {
			// remove any spaces so that after explode it doesn't throw off the search
			$column = str_replace(' ', '', $column);
			$columnArray = explode(',', $column); //var_dump($this->_apiConditions);
			foreach ($this->_apiConditions as $key => $ApiCondition) {
				if (in_array($ApiCondition->column, $columnArray)) {
					return array(
						'ApiCondition' => $ApiCondition,
						'key' => $key
					);
				}
			}

			return false;
		}

		/**
		 * addConditionByTerm take in the field name such as 'price_min' and create a ApiCriteriaCondition
		 *
		 * @param  string $field (ex. price_min, bedrooms)
		 * @param  string $value (ex. 200000, 3)
		 *
		 * @return mixed ApiCriteriaCondition
		 */
		public function addConditionByTerm($field, $value) {
			$Condition = new ApiCriteriaCondition;

			switch ($field) {
				case 'status':
					$Condition->attributes = array(
						'status' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_EQUALS,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'price_min':
					$Condition->attributes = array(
						'price' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_GTE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'price_max':
					$Condition->attributes = array(
						'price' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_LTE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'bedrooms':
					$Condition->attributes = array(
						'bedrooms' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_GTE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'baths_total':
					$Condition->attributes = array(
						'baths_total' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_GTE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'sq_feet':
					$Condition->attributes = array(
						'sq_feet' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_GTE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'gated_community_yn':
				case 'new_construction_yn':
					$Condition->attributes = array(
						$field => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_EQUALS,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;
				case 'year_built':
					$Condition->attributes = array(
						'year_built' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_GTE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;
				case 'area':
					$Condition->attributes = array(
						'area' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;
				case 'street_number':
					$Condition->attributes = array(
						'street_number' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;
				case 'street_name':
					$Condition->attributes = array(
						'street_name' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;
				case 'street_suffix':
					$Condition->attributes = array(
						'street_suffix' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;
				case 'city':
					$Condition->attributes = array(
						'city' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'zip':
					$Condition->attributes = array(
						'zip' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_EQUALS,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'county':
					$Condition->attributes = array(
						'county' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_EQUALS,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);

					return;
					break;

				case 'neighborhood':
				case 'subdivision':
					$Condition->attributes = array(
						'common_subdivision' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						),
						'legal_subdivision' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						),
					);
					break;

				case 'waterfront': //*********** TODO: NEED TO CUSTOM STANDARDIZE IN FEED PROCESS *************************
					$Condition->attributes = array(
						'waterfront' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_EQUALS,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'oceanfront': // Oceanfront //*********** TODO: NEED TO CUSTOM STANDARDIZE IN FEED PROCESS *************************
					$Condition->attributes = array(
						'waterfront_description' => array(
							array(
								'value' => 'Oceanfront',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'pool_yn': //*********** TODO: NEED TO CUSTOM STANDARDIZE IN FEED PROCESS *************************
					$Condition->attributes = array(
						'pool_yn' => array(
							array(
								'value' => $value,
								'operator' => ApiCriteriaCondition::OPERATOR_EQUALS,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						)
					);
					break;

				case 'foreclosure':
				case 'short_sale':
					$otherField = ($field == 'foreclosure') ? 'short_sale' : 'foreclosure';

					// if both foreclosure and ss are checked
					if (($result = $this->findApiCriteriaByTerm($otherField))) {

						$ApiCondition = $this->_apiConditions[$result['key']];
						$ApiCondition->column = '(' . $otherField . '=' . $ApiCondition->condition[0]['value'] . ' OR ' . $field . '=' . $value . ')';

						// clear out condition, since format is diff for this scenario of nested conditions
						$ApiCondition->condition = array();
						$ApiCondition->condition[0]['conjunctor'] = ApiCriteriaCondition::CONJUNCTOR_AND;
						$ApiCondition->condition[0]['operator'] = ApiCriteriaCondition::OPERATOR_NESTED_FLAG;

						$this->_apiConditions[$result['key']] = $ApiCondition;
						// only one of the 2 fields
					} else {
						$Condition->attributes = array(
							$field => array(
								array(
									'value' => $value,
									'operator' => ApiCriteriaCondition::OPERATOR_EQUALS,
									'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
								),
							)
						);
					}
					break;
				case 'school':
					$Condition->attributes = array(
						'elementary_school' => array(
							array(
								'value' => '%' . $value . '%',
								'operator' => ApiCriteriaCondition::OPERATOR_LIKE,
								'conjunctor' => ApiCriteriaCondition::CONJUNCTOR_AND
							),
						),
						// 'middle_school' => array(
						//              				array('value'=>'%'.$value.'%', 'operator'=>ApiCriteriaCondition::OPERATOR_LIKE,'conjunctor'=>ApiCriteriaCondition::CONJUNCTOR_OR),
						//              			),
						// 'high_school' => array(
						//              				array('value'=>'%'.$value.'%', 'operator'=>ApiCriteriaCondition::OPERATOR_LIKE,'conjunctor'=>ApiCriteriaCondition::CONJUNCTOR_OR),
						//              			),
					);
					break;

				default:
					// handle if term does not exist;
					break;
			}
			if ($Condition->column && $Condition->condition) {
				$this->addApiCondition($Condition);
			}
		}
	}


	//-----------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * ApiCriteriaCondition
	 *
	 * @author Chris Willard
	 * @since  12/10/2012
	 */
	class ApiCriteriaCondition extends CFormModel {

		const OPERATOR_EQUALS = '=';
		const OPERATOR_NOT_EQUALS = '<>';
		const OPERATOR_IN = 'in';
		const OPERATOR_NOT_IN = 'notin';
		const OPERATOR_GT = '>';
		const OPERATOR_LT = '<';
		const OPERATOR_GTE = '>=';
		const OPERATOR_LTE = '<=';
		const OPERATOR_NESTED_FLAG = 'nested';
		const OPERATOR_LIKE = 'like';
		const OPERATOR_BETWEEN = 'between';

		const CONJUNCTOR_AND = 'and';
		const CONJUNCTOR_OR = 'or';

		public $column;
		public $condition;

		public function setAttributes($apiPart, $safeOnly = true) {

			if (!is_array($apiPart)) {
				return;
			}

			// $keys = array_keys($apiPart);
			// foreach($keys as $key){
			// 	$this->column = $key;
			// }
			$this->column = key($apiPart);
			$this->condition = $apiPart[$this->column];
		}

		public static function getOperators() {

			return array(
				self::OPERATOR_EQUALS,
				self::OPERATOR_NOT_EQUALS,
				self::OPERATOR_BETWEEN,
				self::OPERATOR_GT,
				self::OPERATOR_LT,
				self::OPERATOR_GTE,
				self::OPERATOR_LTE,
				self::OPERATOR_NESTED_FLAG,
				self::OPERATOR_LIKE,
			);
		}

		public static function getConjunctors() {

			return array(
				self::CONJUNCTOR_AND,
				self::CONJUNCTOR_OR,
			);
		}
	}
