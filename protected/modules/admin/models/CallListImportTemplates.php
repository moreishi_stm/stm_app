<?php
/**
 * This is the model class for table "call_list_import_templates".
 *
 * The followings are the available columns in table 'call_list_import_templates':
 * @property integer $id
 * @property string $name
 * @property string $mappings
 * @property integer $added_by
 * @property string $added
 */
class CallListImportTemplates extends StmBaseActiveRecord
{
    /**
     * Model
     *
     * Static method to retrieve an instance of this model
     * @return CallListPhones Object instance
     */
    public static function model()
    {
        return parent::model(__CLASS__);
    }

    /**
     * Table Name
     *
     * Used to retrieve the table name associated with this model
     * @return string Database table name
     */
    public function tableName()
    {
        return 'call_list_import_templates';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//            array('name, mappings', 'required', 'on'=>'task'),
            array('added_by', 'numerical', 'integerOnly'=>true),
            array('name', 'unique'),
            array('name', 'length', 'max'=>100),
            array('name, mappings, added', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, mappings, added, added_by', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Relations
     *
     * Model relations with other models
     * @return array
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
        return array(
            'addedBy' => array(self::BELONGS_TO, 'Contacts', 'added_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Contacts', 'updated_by'),
        );
    }

    protected function beforeValidate()
    {
        if($this->isNewRecord) {
            $this->added_by = Yii::app()->user->id;
            $this->added = new CDbExpression('NOW()');
        }

        $this->updated_by = Yii::app()->user->id;
        $this->updated = new CDbExpression('NOW()');

        return parent::beforeValidate();
    }
}