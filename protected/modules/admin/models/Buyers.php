<?php
	class Buyers extends Transactions {

        public $transactionType = Transactions::BUYERS;
        public $lenderAssignment;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return Transactions the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function __construct($scenario = 'insert') {
			parent::__construct($scenario);
			$this->transactionType = Transactions::BUYERS;
		}
	}
