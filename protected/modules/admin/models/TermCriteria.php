<?php

	/**
	 * This is the model class for table "term_criteria".
	 *
	 * The followings are the available columns in table 'term_criteria':
	 *
	 * @property integer   $id
	 * @property integer   $term_id
	 * @property integer   $type_ma
	 * @property string    $column
	 * @property integer   $operator_id
	 * @property string    $constant_value
	 * @property string    $default_conjunctor
	 *
	 * The followings are the available model relations:
	 * @property Operators $operator
	 * @property Terms     $term
	 */
	class TermCriteria extends StmBaseActiveRecord {

		const TYPE_DYNAMIC = 1;
		const TYPE_CONSTANT = 2;

		/**
		 * Returns the static model of the specified AR class.
		 *
		 * @param string $className active record class name.
		 *
		 * @return TermCriteria the static model class
		 */
		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName() {
			return 'term_criteria';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules() {
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array(
					'term_id, type_ma, column, operator_id, constant_value, default_conjunctor',
					'required'
				),
				array(
					'term_id, type_ma, operator_id',
					'numerical',
					'integerOnly' => true
				),
				array(
					'column',
					'length',
					'max' => 127
				),
				array(
					'default_conjunctor',
					'length',
					'max' => 5
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, term_id, type_ma, column, operator_id, constant_value, default_conjunctor',
					'safe',
					'on' => 'search'
				),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations() {
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
				'operator' => array(
					self::BELONGS_TO,
					'Operators',
					'operator_id'
				),
				'term' => array(
					self::BELONGS_TO,
					'Terms',
					'term_id'
				),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'term_id' => 'Term',
				'type_ma' => 'Type Ma',
				'column' => 'Column',
				'operator_id' => 'Operator',
				'constant_value' => 'Constant Value',
				'default_conjunctor' => 'Default Conjunctor',
			);
		}

		public function findByTermId($id) {
			return $this->findAll(array(
					'condition' => 'term_id=:termId',
					'params' => array(':termId' => $id)
				)
			);
		}


		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search() {
			// Warning: Please modify the following code to remove attributes that
			// should not be searched.

			$criteria = new CDbCriteria;

			$criteria->compare('id', $this->id);
			$criteria->compare('term_id', $this->term_id);
			$criteria->compare('type_ma', $this->type_ma);
			$criteria->compare('column', $this->column, true);
			$criteria->compare('operator_id', $this->operator_id);
			$criteria->compare('constant_value', $this->constant_value, true);
			$criteria->compare('default_conjunctor', $this->default_conjunctor, true);

			return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
		}
	}