<?php
/**
 * ValidationEngine Class File
 *
 * @author Christine Lee
 * @version 1
 * @license http://www.opensource.org/licenses/mit-license.php MIT license
 *
 * @desc ValidationEngine is a wrapper for http://posabsolute.github.com/jQuery-Validation-Engine/
 */

class ValidationEngine extends CWidget {

    public $selector;

    public function init() {
        Yii::import('admin_module.AdminModule');
        $assets = dirname(__FILE__).DS.'assets';
        $csssAssetsUrl = Yii::app()->getAssetManager()->publish($assets.DS.'css', false, -1, AdminModule::REFRESH_CSS_ASSETS);
        Yii::app()->clientScript->registerCssFile($csssAssetsUrl.DS.'validationEngine.jquery.css');

        $jsAssetsUrl = Yii::app()->getAssetManager()->publish($assets.DS.'js', false, -1, AdminModule::REFRESH_JS_ASSETS);
        Yii::app()->clientScript->registerScriptFile($jsAssetsUrl.DS.'jquery.validationEngine.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($jsAssetsUrl.DS.'languages'.DS.'jquery.validationEngine-en.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($jsAssetsUrl.DS.'custom'.DS.'stm-validations.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScript('validationEngineScript', "$('$this->selector').validationEngine('attach', {promptPosition : 'centerRight', scroll: false});");
    }
}

?>
