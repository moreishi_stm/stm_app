<?php
/**
 * Placeholder Class File
 *
 * @author Christine Lee / Chris Willard
 *
 * @desc Placeholder is a wrapper for a simple javascript that puts in placeholder via js
 */

class Placeholder extends CWidget {

	public function init() {
		$js = <<<JS
        var input = document.createElement("input");
        if(('placeholder' in input)==false) {
            $('[placeholder]').focus(function() {
                var i = $(this);
                if(i.val() == i.attr('placeholder')) {
                    i.val('').removeClass('placeholder');
                    if(i.hasClass('password')) {
                        i.removeClass('password');
                        this.type='password';
                    }
                }
            }).blur(function() {
                var i = $(this);
                if(i.val() == '' || i.val() == i.attr('placeholder')) {
                    if(this.type=='password') {
                        i.addClass('password');
                        this.type='text';
                    }
                    i.addClass('placeholder').val(i.attr('placeholder'));
                }
            }).blur().parents('form').submit(function() {
                $(this).find('[placeholder]').each(function() {
                    var i = $(this);
                    if(i.val() == i.attr('placeholder'))
                        i.val('');
                })
            });
        }
JS;
		Yii::app()->clientScript->registerScript('placeHolderExtension', $js);

        $css = <<<CSS
        .placeholder { color :#aaa; }
CSS;
        Yii::app()->clientScript->registerCss('placeHolderCss', $css);
	}
}

?>
