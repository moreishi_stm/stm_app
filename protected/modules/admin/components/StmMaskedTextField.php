<?php

class StmMaskedTextField extends CMaskedTextField {

	public $stmMaskedTextFieldClass = 'stmMaskedField';

	/**
	 * Registers the needed CSS and JavaScript.
	 */
	public function registerClientScript()
	{
		$this->htmlOptions['class'] .= ' '.$this->stmMaskedTextFieldClass;
		$id=$this->htmlOptions['id'];
		$miOptions=$this->getClientOptions();
		$options=$miOptions!==array() ? ','.CJavaScript::encode($miOptions) : '';
		$js='';
		if(is_array($this->charMap))
			$js.='jQuery.mask.definitions='.CJavaScript::encode($this->charMap).";\n";
		$js.="jQuery(\".{$this->stmMaskedTextFieldClass}\").inputmask(\"{$this->mask}\"{$options});";

		$cs=Yii::app()->getClientScript();
		$cs->registerCoreScript('maskedinput');
		$cs->registerScript('Yii.CMaskedTextField#'.$id,$js);
	}

	protected function getClientOptions() {

		$options = parent::getClientOptions();
		$options['clearIncomplete'] = false;
		return $options;
	}
}