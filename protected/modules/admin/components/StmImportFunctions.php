<?php

class StmImportFunctions extends CApplicationComponent
{

    /**
     * Used for Imports. Storing already queried sources
     * @var mixed Array
     */
    public static $sourcesCollection;
    public static $contactTypesCollection;
    public static $existingTransactionTags = null;
    protected static $_adminUserIds;
    protected static $_validStatusIds;
    public static $mappings = array();

    public static $suggestedHeaderRowFieldName = array();

    /**
     * Data Structure:
     * first_name
     * last_name
     * spouse_first_name
     * spouse_last_name
     * email_1
     * email_2
     * email_3
     * email_4
     * phone_1
     * phone_2
     * phone_3
     * phone_4
     * phone_5
     * address
     * city
     * state
     * zip
     * ...etc.
     */

    public static function getMappings()
    {
        // had to move it here cuz starting out with this declared above was giving error. Possible PHP version issue.
        return self::$mappings = array(
            'base' => array(
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'full_name' => 'Full Name (Combined)',
                'spouse_first_name' => 'Spouse First Name',
                'spouse_last_name' => 'Spouse Last Name',
                'phone_1' => 'Phone 1',
                'phone_2' => 'Phone 2',
                'phone_3' => 'Phone 3',
                'phone_4' => 'Phone 4',
                'phone_5' => 'Phone 5',
                'phone_6' => 'Phone 6',
                'phone_7' => 'Phone 7',
                'phone_8' => 'Phone 8',
                'phone_9' => 'Phone 9',
                'email_1' => 'Email 1',
                'email_2' => 'Email 2',
                'email_3' => 'Email 3',
                'email_4' => 'Email 4',
                'email_5' => 'Email 5',
                'contact_notes' => 'Contact Level Notes',
                'source' => 'Source (Override)',
                'contactTags' => 'Contact Tags',
                'added' => 'Date Added',
                'ignore_email' => 'Ignore Email Duplicates',
                'ignore_phone' => 'Ignore Phone Duplicates',
            ),
            'notes' => array(
                'email' => 'Email',
                'note' => 'Notes',
                'private_note' => 'Private Note',
                'origin_type' => 'Origin Type',
                'activity_date' => 'Activity Date'
            ),
            'sellers' => array(
                'address' => 'Address',
                'city' => 'City',
                'state' => 'State',
                'zip' => 'Zip',
                'transaction_status_id' => 'Tnx/Lead Status',
                'seller_notes' => 'Seller Level Notes',
                'transaction_tags' => 'Transaction Tags',
                'transactionFields:' . TransactionFields::PREV_EXPIRED_DATE_SELLER => 'Previous Expired Date (Status Change Date)',
                'transactionFields:' . TransactionFields::PREV_EXPIRED_MLS_NUMBER_SELLER => 'Previous Expired MLS ID',
                'transactionFields:' . TransactionFields::PREV_EXPIRED_PRICE_SELLER => 'Expired Price',
                'transactionFields:' . TransactionFields::CURRENT_PRICE_SELLER => 'List Price / FSBO Price',
                'transactionFields:' . TransactionFields::SELLER_LISTING_DATE => 'Listing Date',
                'transactionFields:' . TransactionFields::SELLER_LISTING_EXPIRED_DATE => 'Listing Expiration Date',
                'transactionFields:' . TransactionFields::SELLER_MLS_NUMBER => 'MLS #',
                'transactionFields:' . TransactionFields::SELLER_SHOWING_INSTRUCTIONS => 'Showing Instructions',
                'transactionFields:' . TransactionFields::SQ_FEET_SELLER => 'Sq Feet',
                'transactionFields:' . TransactionFields::BEDROOMS_SELLER => 'Bedrooms',
                'transactionFields:' . TransactionFields::BATHS_SELLER => 'Bath',
                'transactionFields:' . TransactionFields::YEAR_BUILT_SELLER => 'Year Built',
                'transactionFields:' . TransactionFields::SELLER_LOCKBOX_NUMBER => 'Lockbox #',
                'transactionFields:' . TransactionFields::SELLER_IVR_EXTENSION => 'IVR Ext #',
                'transactionFields:' . TransactionFields::AREA_NEIGHBORHOOD_SELLER => 'Area/Neighborhood',
                'transactionFields:' . TransactionFields::EXPIRED_LISTING_OFFICE_SELLER => 'Expired Listing Office',
                'transactionFields:' . TransactionFields::EXPIRED_LISTING_AGENT_SELLER => 'Expired Listing Agent',
                'transactionFields:' . TransactionFields::EXPIRED_AGENT_REMARKS_SELLER => 'Expired Agent Remarks',
                'transactionFields:' . TransactionFields::FEATURES_SELLER => 'Public Remarks',
                'transactionFields:' . TransactionFields::IMPORT_EXTERNAL_IDENTIFIER_SELLER => 'External Identifier',
                'assignment_type_id' => 'Assignment Type ID (USE CAUTION)',
                'assigned_to_user_id' => 'Assigned To User ID (USE CAUTION)',
                'transactionFields:' . TransactionFields::REAL_ESTATE_TAX_ID => 'Real Estate Tax ID',
                'transactionFields:' . TransactionFields::PREVIOUS_SALE_DATE => 'Previous Sale Date',
                'transactionFields:' . TransactionFields::PREVIOUS_SALE_PRICE => 'Previous Sale Price',
                'transactionFields:' . TransactionFields::PREVIOUS_SALE_OWNER => 'Previous Sale Owner',
                'bedrooms' => 'Bedrooms',
                'baths' => 'Baths',
                'price_min' => 'Price Min',
                'price_max' => 'Price MaxDuplicates',
                'city' => 'City',
                'zip' => 'Zip',
                'county' => 'County',
                'kunversion_areas' => 'Saved Search Data'
            ),
            'contacts' => array(
                'address' => 'Address',
                'city' => 'City',
                'state' => 'State',
                'zip' => 'Zip',
                'birthday' => 'Birthday (override)',
                'spouse_birthday' => 'Spouse Birthday (override)',
                'closing_anniversary' => 'Closing Anniversary'
            ),
            'buyers' => array(
                // price_min, price_max, zip, areas/cities
                'address' => 'Address',
                'city' => 'City',
                'state' => 'State',
                'zip' => 'Zip',
                'transaction_status_id' => 'Tnx/Lead Status',
                'transaction_tags' => 'Transaction Tags',
                'buyer_notes' => 'Buyer Level Notes',
                'transactionFields:' . TransactionFields::PRICE_MIN_BUYER => 'Price Min',
                'transactionFields:' . TransactionFields::PRICE_MAX_BUYER => 'Price Max',
                'transactionFields:' . TransactionFields::LOCATION_BUYER => 'Location/Area',
                'transactionFields:' . TransactionFields::IMPORT_EXTERNAL_IDENTIFIER_BUYER => 'External Identifier',
                'assignment_type_id' => 'Assignment Type ID (USE CAUTION)',
                'assigned_to_user_id' => 'Assigned To User ID (USE CAUTION)',
                'bedrooms' => 'Bedrooms',
                'baths' => 'Baths',
                'price_min' => 'Price Min',
                'price_max' => 'Price MaxDuplicates',
                'city' => 'City',
                'zip' => 'Zip',
                'county' => 'County',
                'kunversion_areas' => 'Saved Search Data'
            ),
            'closings' => array(
                'tnx_type' => 'Tnx Type (Seller/Buyer)',
                'price' => 'Price',
                'closing_status_ma' => 'Closing Status',
                'commission_amount' => 'Gross Commission',
                'address' => 'Address',
                'city' => 'City',
                'state' => 'State',
                'zip' => 'Zip',
                'contract_execute_date' => 'Contract Execute Date',
                'contract_close_date' => 'Closing Date',
                'closing_notes' => 'Closing Level Notes',
                'source' => 'Source',
                'loan_amount' => 'Loan Amount',
                'mls_id' => 'MLS# (Closings)',
                'assignment_type_id' => 'Assignment Type ID (USE CAUTION)',
                'assigned_to_user_id' => 'Assigned To User ID (USE CAUTION)',
                //                'primary_agent_id'      =>  'Primary Agent ID',
                //                'secondary_agent_id'    =>  'Secondary Agent ID',
                'external_id' => 'External Identifier',
            ),
            'recruits' => array(
                'tags' => 'Recruit Tags',
                'address' => 'Address',
                'city' => 'City',
                'state' => 'State',
                'zip' => 'Zip',
                'annual_units' => '12 mo # Units (replace data)',
                'annual_volume' => '12 mo $ Volume (replace data)',
                'annual_volume_change' => '12 mo $ Volume Change (replace data)',
                'annual_volume_change_percent' => '12 mo % Volume Change (replace data)',
                'ranking'                   =>  '# Ranking',
                'current_brokerage' => 'Current Brokerage (replace data)',
                'office_city' => 'Office City (replace data)',
                'office_zip' => 'Office Zip (replace data)',
                'brokerage_mls_id' => 'Brokerage MLS ID (replace data)',
                'brokerage_license_number' => 'Brokerage License Number (replace data)',
                'agent_mls_id' => 'Agent MLS ID (replace data)',
                'agent_license_number' => 'Agent License # (replace data)',
                'licensed_date' => 'Licensed Date (replace data)',
                'website' => 'Website',
                'motivation'                   =>  'Hot Button / Motivation',
//                'recruit_spouse_first_name'    =>  'Recruit Spouse First name',
//                'recruit_spouse_last_name'     =>  'Recruit Spouse Last name',
//                'recruit_spouse_phone'         =>  'Recruit Spouse Phone',
//                'recruit_spouse_email'         =>  'Recruit Spouse Email',
                'recruit_notes' => 'Recruit Level Notes',
            ),
            'appointments' => array(
                'first name' => 'first_name',
                'last name' => 'last_name',
                'spouse first name' => 'spouse_first_name',
                'spouse last name' => 'spouse_last_name',
                'phone 1' => 'phone_1',
                'phone 2' => 'phone_2',
                'phone 3' => 'phone_3',
                'phone 4' => 'phone_4',
                'phone 5' => 'phone_5',
                'email 1' => 'email_1',
                'email 2' => 'email_2',
                'email 3' => 'email_3',
                'email 4' => 'email_4',
                'email 5' => 'email_5',

                'external id' => 'external_id', //TransactionFields::IMPORT_EXTERNAL_IDENTIFIER_SELLER

                'date appointment set' => 'set_on_datetime',
                //'isa' => 'isa',  // ignored because of set for and set by
                'set by contact id' => 'met_by_id',
                'date of appt' => 'set_for_datetime',
                //'type of appt' => 'component_type_id', // ignored for component_type_id
                'set activity type id' => 'set_activity_type_id',
                'component type id' => 'component_type_id',
                'appt kept' => 'met_status_ma', //Appointments::MET_STATUS_MET || Appointments::MET_STATUS_CANCEL
                //'count of appt' => '' // skipped not used
                //'fallout reason' => 'met_status_reason', // not used for next column because of name
                'fallout reason not met' => 'met_status_reason',
                //'agent' => 'agent', // not used because of set for contact id
                'set for contact id' => 'set_by_id',
                'client name' => 'full_name',
                //'lead source' => '', // not used merged into single source
                //'other source => '', // not used merged into single source
                'single source' => 'source_id',
                //'lead status' => '', not used for tnx status id
                'tnx status id' => 'transaction_status_id', // this is for the transaction status id
                //'buyer agreement signed' => '', //not used for is signed
                //'count of ba signed' => '',  //not used for is signed
                //'listing agreement signed' => '', //not used for is signed
                //'count of la signed' => '', //not used for is signed
                'is signed' => 'is_signed', // appointments::is_signed
                //'fallout reason' => 'not_signed_reason',
                'fallout not signed id' => 'not_signed_reason', // used as the id in our system for the not signed fallout reason
                'fallout not signed other' => 'not_signed_reason_other', // Text field for not signed reason with id of other
                //'buyer contract => '', //not used
                //'count of bc => '', //not used
                //'seller contract => '', //not used
                //'count of s => '', //not used
                //'fallout reason => '', //not used
                //'buyer sales price => '', //not used
                //'seller sales price => '', //not used
                'price' => 'price', // Closings::price
                'address' => 'address',
                'city' => 'city',
                'state' => 'state',
                'zip' => 'zip',
                'commission amount' => 'commission_amount',
                //'buyer closing date' => '', //not used
                //'seller closing date' => '', //not used
                'closing actual contract date' => 'actual_closing_datetime', // used for both Closings::actual_closing_datetime, Closings::contract_closing_datetime
                //'buyer closed price' => '', //not used
                //'seller closed price' => '', //not used
                //'closed' => '', // not used
                'closing status' => 'closing_status_ma', //Closings::closing_status_ma should be a preset interger: const STATUS_TYPE_ACTIVE_ID = 1; const STATUS_TYPE_FALLOUT_ID = 2; const STATUS_TYPE_CLOSED_ID = 3;
            )
        );
    }

    /**
     * Initial Data
     *
     * Creates initial data for an import for process to compare against existing data for phone, email, contact names, addresses
     * @return array
     */
    public static function initialData()
    {
        self::getMappings();

        self::$suggestedHeaderRowFieldName = array(
            'fistname',
            'lastname',
            'city',
            'state',
            'zip',
            'postal',
            'referrer',
            'agentfirst',
            'agentlast',
            'created',
            'mortgage_agent',
            'mortgage_company',
            'alerts_areas',
            'alerts_beds',
            'alerts_baths',
            'alerts_min',
            'alerts_max',
            'hashtags',
            'last_date_visited',
            'last_note_date',
            'seller_address',
            'seller_stats',
            'seller_valuation'
        );

        // Retrieve all states and build dictionaries for lookups of state IDs
        $stateFullNames = array();
        $stateAbbreviations = array();

        //$startAddressFindAll = microtime(true);
        $stateResults = AddressStates::model()->findAll();
        //echo "Time to AddressStates::model()->findAll: ".(microtime(true)-$startAddressFindAll)." seconds\n";

        foreach ($stateResults as $stateResult) {
            $stateFullNames[strtolower($stateResult['name'])] = $stateResult['id'];
            $stateAbbreviations[strtolower($stateResult['short_name'])] = $stateResult['id'];
        }

        //$startEmailLookup = microtime(true);
        // Create a flat array of existing emails indexed by email address for lookups //@todo: pulling all email records doesn't seem scalable for large db's - need to optimize
        $emailsResults = Yii::app()->db->createCommand("select TRIM(LOWER(email)) email, contact_id from emails WHERE email IS NOT NULL AND is_deleted=0 ORDER BY email ASC")->queryAll();
        //echo "Time to get Emails: ".(microtime(true)-$startEmailLookup)." seconds\n";

        $emailsExisting = array();
        foreach ($emailsResults as $emailsResult) {
            $emailsExisting[$emailsResult['email']] = $emailsResult['contact_id'];
        }
        unset($emailsResults);

        // Create a array of existing FULL NAME (first and last name) as key and element is array of contactIds
        //$startFullNames = microtime(true);
        $namesResults = Yii::app()->db->createCommand('select LOWER(CONCAT(TRIM(first_name)," ",TRIM(last_name))) fullName, id contactId from contacts WHERE first_name != "" AND last_name !="" ORDER BY TRIM(first_name)')->queryAll(); //GROUP BY LOWER(CONCAT(TRIM(first_name)," ",TRIM(last_name)))
        //echo "Time to get Full Names: ".(microtime(true)-$startFullNames)." seconds\n";
        $namesExisting = array();
        foreach ($namesResults as $namesResult) {
            $namesExisting[$namesResult['fullName']][] = $namesResult['contactId'];
        }
        unset($namesResults);

        // Create a array of existing LAST NAME as key and element is array of contactIds
        //$startLastNameResults = microtime(true);
        $lastNamesResults = Yii::app()->db->createCommand('select LOWER(TRIM(last_name)) lastName, id contactId from contacts WHERE last_name !="" ORDER BY TRIM(last_name)')->queryAll();
        //echo "Time to get last names: ".(microtime(true)-$startLastNameResults)." seconds\n";

        //$startLastNameResults1 = microtime(true);
        $spouseLastNamesResults = Yii::app()->db->createCommand('select LOWER(TRIM(spouse_last_name)) lastName, id contactId from contacts WHERE spouse_last_name !="" ORDER BY TRIM(spouse_last_name)')->queryAll();
        //echo "Time to get last names1: ".(microtime(true)-$startLastNameResults1)." seconds";

        $lastNamesResults = CMap::mergeArray($lastNamesResults, $spouseLastNamesResults);
        $lastNamesExisting = array();
        foreach ($lastNamesResults as $lastNamesResult) {
            $lastNamesExisting[$lastNamesResult['lastName']][] = $lastNamesResult['contactId'];
        }
        unset($lastNamesResult);

        // create array of existing phone #s as key element and element is array of contactIds
        //$startPoneResults = microtime(true);
        $phonesResults = Yii::app()->db->createCommand("select DISTINCT phone, contact_id contactId from phones WHERE phone IS NOT NULL AND is_deleted=0 ORDER BY phone ASC")->queryAll();
        //echo "Time to get phones: ".(microtime(true)-$startPoneResults)." seconds\n";

        $phonesExisting = array();
        foreach ($phonesResults as $phonesResult) {
            $phonesExisting[$phonesResult['phone']][] = $phonesResult['contactId'];
        }
        unset($phonesResults);

        //$startAddressResults = microtime(true);
        $addressesResults = Yii::app()->db->createCommand("select TRIM(LOWER(SUBSTRING(address,1," . Addresses::COMPARE_LENGTH . "))) address, zip, lu.contact_id contactId FROM addresses a JOIN address_contact_lu lu ON a.id=lu.address_id WHERE a.address != '' AND a.address != '-' AND a.is_deleted=0 ORDER BY a.address ASC")->queryAll();
        //echo "Time to get addresses: ".(microtime(true)-$startAddressResults)." seconds\n";

        $addressesExisting = array();
        foreach ($addressesResults as $addressesResult) {
            $addressesExisting[$addressesResult['address']]['contactIds'][] = $addressesResult['contactId'];
            $addressesExisting[$addressesResult['address']]['contactIds'] = array_unique($addressesExisting[$addressesResult['address']]['contactIds']);
            $addressesExisting[$addressesResult['address']]['zips'][] = $addressesResult['zip'];
            $addressesExisting[$addressesResult['address']]['zips'] = array_unique($addressesExisting[$addressesResult['address']]['zips']);
        }
        unset($addressesResults);

        return array($stateFullNames, $stateAbbreviations, $emailsExisting, $namesExisting, $phonesExisting, $addressesExisting, $lastNamesExisting);
    }

    public static function isHeaderRow($newRecord, $emailMaxIndex = 5, $phoneMaxIndex = 5)
    {
        if (strtolower($newRecord['first_name']) == 'first name' || strtolower($newRecord['last_name']) == 'last name') {
            return [true, 'This row was detected as a header row due to First or Last Name data.'];
        }

        foreach ($newRecord as $k => $v) {
            if (in_array(strtolower($v), self::$suggestedHeaderRowFieldName)) {
                return [true, 'This row was detected as a header row due to one of: ' . implode(", ", self::$suggestedHeaderRowFieldName)];
            }
        }

        // check phone fields
        for ($i = 1; $i <= $phoneMaxIndex; $i++) {
            // skip if this record is empty, zero value is a value
            if (isset($newRecord['phone_' . $i]) && !empty($newRecord['phone_' . $i]) && strpos(trim(strtolower($newRecord['phone_' . $i])), 'phone') !== false) {
                return [true, 'This row was detected as a header row due to phone data for column phone_' . $i . '.'];
            }
        }

        // check email fields
        for ($i = 1; $i <= $emailMaxIndex; $i++) {
            // skip if this record is empty, zero value is a value
            if (isset($newRecord['email_' . $i]) && !empty($newRecord['email_' . $i]) && strpos(trim(strtolower($newRecord['email_' . $i])), 'email') !== false && strpos(trim(strtolower($newRecord['email_' . $i])), '@') === false && strpos(trim(strtolower($newRecord['email_' . $i])), '.') === false) {
                return [true, 'This row was detected as a header row due to email data for column email_' . $i . '.'];
            }
        }

        // return not a header row
        return [false, null];
    }


    /**
     * Field Set Exists
     *
     * Checks to see if value exists by comparing to array of existing data for that field. Example: If there is a phone_1 through phone_5, start/end Index loops through that and compares against existing data
     *
     * @param      $newRecord
     * @param      $fieldName
     * @param      $existingData
     * @param int $startIndex
     * @param int $endIndex
     * @param bool $returnData array('contactIds'=> array(contactId integers), 'existingValues' = array(value strings))
     *
     * @return array|bool
     */
    public static function fieldsetExists($newRecord, $fieldName, $existingData, $startIndex = 1, $endIndex = 5, $returnData = true)
    {
        if (!in_array($fieldName, array('email', 'phone'))) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid field name.');
        }

        $exists = false;
        $data = false; //array('contactIds' => array(), 'existingValues' => array());
        for ($i = $startIndex; $i <= $endIndex; $i++) {

            // skip if this record is empty, zero value is a value
            if (isset($newRecord[$fieldName . '_' . $i]) && !empty($newRecord[$fieldName . '_' . $i]) && isset($existingData[strtolower($newRecord[$fieldName . '_' . $i])])) {

                $exists = true;

                // collect data if return set to true
                if ($returnData && $existingData[strtolower($newRecord[$fieldName . '_' . $i])]) {
                    switch ($fieldName) {
                        case 'email':
                            // phone contactIds element only has 1 possible contact Id
                            $data['contactIds'][] = (isset($data['contactIds'])) ? $data['contactIds'][] = $existingData[strtolower($newRecord[$fieldName . '_' . $i])] : $existingData[strtolower($newRecord[$fieldName . '_' . $i])];
                            break;

                        case 'phone':
                            // phone contactIds element can have multiple contact Ids - use mergeArray
                            $data['contactIds'] = (isset($data['contactIds'])) ? CMap::mergeArray($data['contactIds'], $existingData[strtolower($newRecord[$fieldName . '_' . $i])]) : $existingData[strtolower($newRecord[$fieldName . '_' . $i])];
                            break;

                        default:
                            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid field name.');
                            break;
                    }

                    $data['existingValues'][] = $newRecord[$fieldName . '_' . $i];
                }
            }
        }
        if ($data['contactIds'] && !empty($data['contactIds'])) {

            if (is_object($data['contactIds'])) {
                $x = 1;
            }

            $data['contactIds'] = array_unique($data['contactIds']);
            $data['existingValues'] = array_unique($data['existingValues']);
        }

        // return data only if flag for return data is set, else return boolean
        return ($returnData) ? $data : $exists;
    }

    public static function addressExists($newRecord, $existingData, $returnData = true)
    {
        if (!isset($newRecord['address']) || empty(trim($newRecord['address']))) {
            return false;
        }

        $exists = false;
        $data = false; //array('contactIds' => array(), 'existingValues' => array());
        $key = trim(substr(strtolower($newRecord['address']), 0, Addresses::COMPARE_LENGTH));

        if (isset($newRecord['address']) && !empty($newRecord['address']) && isset($existingData[$key])) {

            if (!empty($newRecord['zip']) && !empty($existingData[$key]['zips']) && in_array($newRecord['zip'], $existingData[$key]['zips'])) {
                $exists = true;

                // collect data if return set to true
                if ($returnData) {
                    $data['contactIds'] = $existingData[$key];
                    $data['existingValues'][] = $newRecord['address'];
                }
            }
        }

        return ($returnData) ? $data : $exists;
    }

    public static function nameExists($newRecord, $existingData, $allowedDupeNames = array(), $returnData = true)
    {
        $exists = false;
        $data = false; //array('contactIds' => array(), 'existingValues' => array());
        $key = strtolower(trim($newRecord['first_name']) . ' ' . trim($newRecord['last_name']));

        if (!in_array(trim(strtolower($newRecord['first_name'])), $allowedDupeNames) && isset($newRecord['first_name']) && !empty($newRecord['first_name']) && isset($existingData[$key]) && !in_array($key, $allowedDupeNames)) {

            $exists = true;

            // collect data if return set to true
            if ($returnData) {
                $data['contactIds'] = $existingData[$key];
                $data['existingValues'][] = $newRecord['first_name'] . ' ' . $newRecord['last_name'];
            }
        }

        return ($returnData) ? $data : $exists;
    }

    public static function lastNameExists($newRecord, $existingData, $allowedDupeNames = array(), $returnData = true)
    {
        $exists = false;
        $data = false;
        $key = strtolower(trim($newRecord['last_name']));

        if (!in_array(trim(strtolower($newRecord['last_name'])), $allowedDupeNames) && isset($newRecord['last_name']) && !empty($newRecord['last_name']) && isset($existingData[$key])) {

            $exists = true;

            // collect data if return set to true
            if ($returnData) {
                $data['contactIds'] = $existingData[$key];
                $data['existingValues'][] = $newRecord['last_name'];
            }
        }

        return ($returnData) ? $data : $exists;
    }

    /**
     * Format Phones
     * Removes any non-numeric values and removes duplicate numbers
     *
     * return void
     */
    public static function formatPhones(&$newRecord, $startIndex = 1, $endIndex = 9)
    {
        $processedPhones = array();
        for ($i = $startIndex; $i <= $endIndex; $i++) {

            // skip if this record is empty, zero value is a value
            if (isset($newRecord['phone_' . $i]) && !empty($newRecord['phone_' . $i])) {

                if (in_array(Yii::app()->format->formatInteger($newRecord['phone_' . $i]), $processedPhones)) {
                    $newRecord['phone_' . $i] = null;
                    continue;
                }

                $newRecord['phone_' . $i] = Yii::app()->format->formatInteger($newRecord['phone_' . $i]);
                $processedPhones[] = $newRecord['phone_' . $i];
            }
        }
    }

    public static function validateMinPhoneExists(&$newRecord, $startIndex = 1, $endIndex = 9)
    {
        for ($i = $startIndex; $i <= $endIndex; $i++) {

            // skip if this record is empty, zero value is a value
            if (isset($newRecord['phone_' . $i]) && !empty($newRecord['phone_' . $i])) {
                $phone = Yii::app()->format->formatInteger($newRecord['phone_' . $i]);
                if (strlen($phone) == 10 || (strlen($phone) == 11 && substr($phone, 0, 1) == 1)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function validateMinEmailExists(&$newRecord, $startIndex = 1, $endIndex = 5)
    {
        for ($i = $startIndex; $i <= $endIndex; $i++) {

            // skip if this record is empty, zero value is a value
            if (isset($newRecord['email_' . $i]) && !empty($newRecord['email_' . $i])) {
                $email = trim($newRecord['email_' . $i]);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Format Emails
     * Trims and makes Lower Case
     *
     * return void
     */
    public static function formatEmails(&$newRecord, $startIndex = 1, $endIndex = 5)
    {
        $processedEmails = array();

        for ($i = $startIndex; $i <= $endIndex; $i++) {

            // skip if this record is empty, zero value is a value
            if (isset($newRecord['email_' . $i]) && !empty($newRecord['email_' . $i])) {

                $email = strtolower(trim($newRecord['email_' . $i]));

                // check if same email has already been processed, if so ignore it as it is dupe. This prevents dupe emails for same contact from causing errors.
                if (!in_array($email, $processedEmails)) {
                    $newRecord['email_' . $i] = $email;
                    $processedEmails[] = $email;
                } else {
                    // if email is dupe, clear out the field
                    $newRecord['email_' . $i] = '';
                }
            }
        }
    }

    /**
     * Get ContactIds by Email
     *
     * Takes an string or array of emails and finds unique contactIds
     * @param string | array $emails
     *
     * @return mixed
     */
    public static function getContactIdsByEmails($emails)
    {
        // turn to email array, in case it's string
        $emails = (array)$emails;

        return Yii::app()->db->createCommand()
            ->selectDistinct('contact_id')
            ->from('emails')
            ->where(array('in', 'email', $emails))
            ->andWhere('is_deleted=0')
            ->queryColumn();
    }

    /**
     * Get ContactIds by Phones
     *
     * Takes an string or array of emails and finds unique contactIds
     * @param string | array $phones
     *
     * @return mixed
     */
    public static function getContactIdsByPhones($phones)
    {
        // turn to email array, in case it's string
        $phones = (array)$phones;

        return Yii::app()->db->createCommand()
            ->selectDistinct('contact_id')
            ->from('phones')
            ->where(array('in', 'phone', $phones))
            ->andWhere('is_deleted=0')
            ->queryColumn();
    }

    /**
     * Get ContactIds by Phones
     *
     * Takes an string or array of emails and finds unique contactIds
     * @param string | array $phones
     *
     * @return mixed
     */
    public static function getContactIdsByAddress($address, $zip)
    {
        return Yii::app()->db->createCommand()
            ->selectDistinct('lu.contact_id')
            ->from('addresses a')
            ->join('address_contact_lu lu', 'lu.address_id=a.id')
            ->where('LOWER(TRIM(SUBSTRING(a.address, 1, ' . Addresses::COMPARE_LENGTH . '))) like :address', array(':address' => strtolower(trim(substr($address, 0, Addresses::COMPARE_LENGTH)))))
            //->andWhere('a.address != "-"')
            ->andWhere('TRIM(zip)=:zip', array(':zip' => trim(substr($zip, 0, 5))))
            ->queryColumn();
    }

    /**
     * Get ContactIds by Phones
     *
     * Takes an string or array of emails and finds unique contactIds
     * @param string | array $phones
     *
     * @return mixed
     */
    public static function getContactIdsByFullNameAddress($firstName, $lastName, $address, $zip)
    {
        return Yii::app()->db->createCommand()
            ->select('c.id')
            ->from('contacts c')
            ->join('address_contact_lu lu', 'lu.contact_id=c.id')
            ->join('addresses a', 'lu.address_id=a.id')
            ->where('LOWER(SUBSTRING(a.address, 1,  ' . Addresses::COMPARE_LENGTH . ')) like :address', array(':address' => strtolower(substr($address, 0, Addresses::COMPARE_LENGTH))))
            //->andWhere('a.address != "-"')
            ->andWhere('c.is_deleted=0')
            ->andWhere('LOWER(CONCAT(TRIM(first_name))) like :firstName', array(':firstName' => trim(strtolower($firstName))))
            ->andWhere('LOWER(CONCAT(TRIM(last_name))) like :lastName', array(':lastName' => trim(strtolower($lastName))))
            ->andWhere('a.zip=:zip', array(':zip' => substr(trim($zip), 0, 5)))
            ->queryColumn();
    }

    /**
     * Gets all Contact ID matches based on import data using emails, phones, address, names
     *
     * @param $newContact
     * @param $emailsExisting
     * @param $phonesExisting
     * @param $addressesExisting
     * @param $namesExisting
     * @param $allowedDupeNames
     *
     * @return null|array of matches found. Each type of match will have it's own key and an array of contactIds.
     * Example array result: array('emails' => contactIdArray, 'phones' => contactIdArray, 'address' => contactIdArray, 'name' => contactIdArray)
     */
    public static function getContactIdMatches($newContact, $emailsExisting, $phonesExisting, $addressesExisting, $namesExisting, $lastNamesExisting, $allowedDupeNames)
    {
        $contactModel = null;
        $contactIdMatches = null;
        $matchErrorLogs = null;

        // check for any email matches in existing records. Get contactIds and values for any matches
        if ($emailMatches = StmImportFunctions::fieldsetExists($newContact, 'email', $emailsExisting)) {

            // if ONE match, we are certain of the contact. Proceed with merging contact info, etc.
            if (count($emailMatches['contactIds']) == 1) {
                $contactModel = Contacts::model()->findByPk($emailMatches['contactIds'][0]);
                $contactIdMatches['emails'] = $emailMatches['contactIds'];
            } // there are multiple contact matches. Either perform additional checks or send to error and skip record.
            elseif (count($emailMatches['contactIds']) > 1) {
                $contactIdMatches['emails'] = $emailMatches['contactIds'];
                $matchErrorLogs['emails'][] = 'Emails matches: ' . implode(',', $emailMatches['existingValues']);
                //continue;
            }
        }

        // check for any phone matches in existing records. Get contactIds and values for any matches
        $phoneMatches = StmImportFunctions::fieldsetExists($newContact, 'phone', $phonesExisting);
        if ($phoneMatches && (!isset($newContact['ignore_dupe_phone']) || $newContact['ignore_dupe_phone'] === FALSE)) {

            // if ONE match, we are certain of the contact. Proceed with merging contact info, etc.
            if (count($phoneMatches['contactIds']) == 1) {

                $contactIdMatches['phones'] = $phoneMatches['contactIds'];
                if (empty($contactModel)) {
                    // comment out for now. See below when reviewing all matches
                    //self::$contactModel = Contacts::model()->findByPk($phoneMatches['contactIds'][0]);
                    $matchErrorLogs['phones'][] = 'Phone match: ' . $phoneMatches['existingValues'][0];
                } // existing _contactModel does not match single contactId found with phones
                elseif (!empty($contactModel->id) && ($contactModel->id != $phoneMatches['contactIds'][0])) {
                    $matchErrorLogs['phones'][] = 'Multiple contacts found, 1 each between email and phone. Email match: ' . $emailMatches['existingValues'][0] . ' Phone match: ' . $phoneMatches['existingValues'][0];
                    //continue;
                }
            } // there are multiple contact matches. Either perform additional checks or send to error and skip record.
            elseif (count($phoneMatches['contactIds']) > 1) {
                $contactIdMatches['phones'] = $phoneMatches['contactIds'];
                $matchErrorLogs['phones'][] = 'Phone multiple Contact matches: ' . implode(',', $phoneMatches['existingValues']);
                //continue;
            }
        }

        //@todo: ************** make address matching compatible for contacts AND transactions, currently only looking at contacts **************
        // check for any address matches in existing records. Get contactIds and values for any matches and cross reference for
        $addressMatches = StmImportFunctions::addressExists($newContact, $addressesExisting);
        if ($addressMatches && (!isset($newContact['ignore_dupe_address']) || $newContact['ignore_dupe_address'] === false)) {

            //NOTE: address must have another type of match aside from address only??? at least a name match to confirm.
            $contactIdMatches['address'] = $addressMatches['contactIds'];
            $matchErrorLogs['address'][] = 'Multiple contacts found for address and zip.';
        }

        // check if name matches in existing records. Get contactIds and values for any matches, ignores allowed dupe names
        if ($nameMatches = StmImportFunctions::nameExists($newContact, $namesExisting, $allowedDupeNames)) {

            // name match found. Need another match to confirm it is the same contact.
            //@todo: keep an eye on logs to confirm any other patterns that need to be adopted into this logic. Is stand alone name match ok in some instances? Dunno... TBD
            //@todo: if it's an allowable dupe name, perhaps take a real one if available? requires more logic
            $contactIdMatches['name'] = $nameMatches['contactIds'];
            $matchErrorLogs['name'][] = 'Exact name match found.';
        }

        if ($lastNameMatches = StmImportFunctions::lastNameExists($newContact, $lastNamesExisting, $allowedDupeNames)) {
            $contactIdMatches['lastName'] = $lastNameMatches['contactIds'];
            //$matchErrorLogs['lastName'][] = 'Last name match found. Contact ID matches: '.implode(',', array_unique($contactIdMatches['lastName']));
        }

        return array($contactIdMatches, $matchErrorLogs);
    }

    public static function getConclusiveContactId($contactIdMatches, $emailMatchConclusive = true)
    {
        $conclusiveContactId0 = array();
        $conclusiveContactId1 = array();
        $conclusiveContactId2 = array();
        $conclusiveContactId3 = array();
        $conclusiveContactId4 = array();
        $conclusiveContactId5 = array();
        $conclusiveContactId6 = array();
        $conclusiveContactId7 = array();
        $conclusiveContactId8 = array();
        $conclusiveContactId9 = array();
        switch (1) {
            // include email match only
            case($emailMatchConclusive && is_array($contactIdMatches['emails']) && !empty($conclusiveContactId0 = $contactIdMatches['emails'])):
                if ($emailMatchConclusive && $conclusiveContactId0) {
                    return $conclusiveContactId0;
                }
                break;

            // name, address & phone match
            case (is_array($contactIdMatches['name']) && is_array($contactIdMatches['address']['contactIds']) && is_array($contactIdMatches['phones'])
                && !empty($conclusiveContactId1 = array_intersect($contactIdMatches['name'], $contactIdMatches['address']['contactIds'], $contactIdMatches['phones']))):
                break;

            // name, address
            case (is_array($contactIdMatches['name']) && is_array($contactIdMatches['address']['contactIds'])
                && !empty($conclusiveContactId2 = array_intersect($contactIdMatches['name'], $contactIdMatches['address']['contactIds']))):

                // address & phone
            case (is_array($contactIdMatches['address']) && is_array($contactIdMatches['phones'])
                && !empty($conclusiveContactId3 = array_intersect($contactIdMatches['address']['contactIds'], $contactIdMatches['phones']))):

                // email & name
            case (is_array($contactIdMatches['emails']) && is_array($contactIdMatches['name'])
                && !empty($conclusiveContactId4 = array_intersect($contactIdMatches['emails'], $contactIdMatches['name']))):

                // email & phone
            case (is_array($contactIdMatches['emails']) && is_array($contactIdMatches['phones'])
                && !empty($conclusiveContactId5 = array_intersect($contactIdMatches['emails'], $contactIdMatches['phones']))):

                // name & phone
            case (is_array($contactIdMatches['name']) && is_array($contactIdMatches['phones'])
                && !empty($conclusiveContactId6 = array_intersect($contactIdMatches['name'], $contactIdMatches['phones']))):

                // email & address
            case (is_array($contactIdMatches['emails']) && is_array($contactIdMatches['address']['contactIds'])
                && !empty($conclusiveContactId7 = array_intersect($contactIdMatches['emails'], $contactIdMatches['address']['contactIds']))):

//            // last name & address
//            case (is_array($contactIdMatches['lastName']) && is_array($contactIdMatches['address']['contactIds'])
//                && !empty($conclusiveContactId8 = array_intersect($contactIdMatches['lastName'], $contactIdMatches['address']['contactIds']))):
//
//            // last name & phone
//            case (is_array($contactIdMatches['lastName']) && is_array($contactIdMatches['phones'])
//                && !empty($conclusiveContactId9 = array_intersect($contactIdMatches['lastName'], $contactIdMatches['phones']))):
        }

        return array_unique(CMap::mergeArray($conclusiveContactId0, $conclusiveContactId1, $conclusiveContactId2, $conclusiveContactId3, $conclusiveContactId4, $conclusiveContactId5, $conclusiveContactId6, $conclusiveContactId7, $conclusiveContactId8, $conclusiveContactId9));
    }

    /**
     * Merge Contact Info
     *
     * Merges any add spouse, email, phones to an existing record that does not exist.
     * @param $contactId
     * @param $newRecord
     *
     * @return mixed
     */
    public static function mergeContactInfo($contactId, $accountId, $newRecord, $numEmailsFields = 5, $numPhoneFields = 5, &$existingEmailData, &$existingPhoneData, &$importErrorRows, $allowedDupeNames = array())
    {
        $newEmailIds = array();
        $newPhoneIds = array();
        $isDupeName = false;
        $contact = Contacts::model()->findByPk($contactId);

        if(empty($contact)) {
            $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Could not find contact for ID, mergeContactInfo FAILED: '.$contactId);
            return NULL;
        }

        // for allowed dupe names, if last name empty, put in dash
        if (in_array(strtolower(trim($newRecord['first_name'])), $allowedDupeNames)) {
            $isDupeName = true;
        }

        // spouse first name if not empty, compare and merge
        if (!$isDupeName && trim($newRecord['spouse_first_name'])) {

            // existing spouse record empty and check that spouse name is not same as primary first name
            if (empty(trim($contact->spouse_first_name)) && strtolower(trim($newRecord['spouse_first_name'])) != strtolower(trim($contact->first_name))) {
                $contact->spouse_first_name = ucwords(strtolower(trim($newRecord['spouse_first_name'])));
            } // if no spouse existing data and spouse first name is already the primary for existing contact
            elseif (empty(trim($contact->spouse_first_name)) && strtolower(trim($newRecord['spouse_first_name'])) == strtolower(trim($contact->first_name))) {

                // if last names match for both spouse AND it matches existing data
                if (strtolower(trim($newRecord['spouse_last_name'])) == strtolower(trim($contact->last_name)) && strtolower(trim($newRecord['last_name'])) == strtolower(trim($newRecord['spouse_last_name']))) {
                    // move new record primary first name to spouse first name
                    $contact->spouse_first_name = $newRecord['first_name'];
                } elseif (strtolower(trim($contact->last_name)) == strtolower(trim($newRecord['spouse_last_name'])) && strtolower(trim($contact->first_name)) == strtolower(trim($newRecord['spouse_first_name'])) && empty($contact->spouse_first_name) && empty($contact->spouse_last_name)) {

                    // check to see if spouse names is primary and merge into empty spouse field if last names are different but spouse first/last new record matches existing primary first/last
                    $contact->spouse_first_name = $newRecord['first_name'];
                    $contact->spouse_last_name = $newRecord['last_name'];
                } else {
                    $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'New Contact record Spouse first name is same as primary first name but last names do not match. Review data.');
                }
            }
        } // if only primary first/last name
        elseif (!$isDupeName && (strtolower(trim($newRecord['first_name'])) != strtolower(trim($contact->first_name)) || strtolower(trim($newRecord['last_name'])) != strtolower(trim($contact->last_name)))) {

            $errorMessage = 'Proceeding with contact merge & save. Please manually verify this record. FYI: ';
            $lastNameDupeHandled = false;

            // first name does not match
            if (strtolower(trim($newRecord['first_name'])) != strtolower(trim($contact->first_name))) {

                // spouse name open and first name not match, put name in spouse name
                if (trim($contact->spouse_first_name)) {
                    $errorMessage .= 'Import first name "' . $newRecord['first_name'] . '" is not an exact match to the existing contact first name "' . $contact->first_name . '". Spouse name field is has existing value of "' . $contact->spouse_first_name . '"';
                } else {
                    $contact->spouse_first_name = ucwords(trim($newRecord['first_name']));
                    $errorMessage .= 'Import first name "' . $newRecord['first_name'] . '" is not an exact match to the existing contact first name "' . $contact->first_name . '" and was merged in as the spouse first name as it was empty.';

                    // if last name not a match import that as well as long as a spouse_first_name if from current record
                    if (strtolower(trim($newRecord['last_name'])) != strtolower(trim($contact->last_name)) && !trim($contact->spouse_last_name) && $contact->spouse_first_name == ucwords(trim($newRecord['first_name']))) {

                        $errorMessage .= ($errorMessage) ? ' ' : '';
                        $errorMessage .= 'Import last name "' . $newRecord['last_name'] . '" is not an exact match to the existing contact last name "' . $contact->last_name . '" and was merged in as the spouse last name as it was empty and spouse first name was merged.';

                        $contact->spouse_last_name = ucwords(trim($newRecord['last_name']));
                        // flag that last name non match already handled
                        $lastNameDupeHandled = true;
                    }
                }
            }

            // last name does not match
            if (!$lastNameDupeHandled && strtolower(trim($newRecord['last_name'])) != strtolower(trim($contact->last_name))) {
                $errorMessage .= ($errorMessage) ? ' ' : '';
                $errorMessage .= 'Import last name "' . $newRecord['last_name'] . '" is not an exact match to the existing contact last name "' . $contact->last_name . '".';
            }

            // if names don't match, log warning
            $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, $errorMessage);
        }

        // spouse last name if not empty, compare and merge
        if (trim($newRecord['spouse_last_name'])) {

            // existing spouse record empty and check that spouse name is not same as primary first name
            if (empty(trim($contact->spouse_last_name)) && strtolower(trim($newRecord['spouse_last_name'])) != strtolower(trim($contact->last_name))) {
                $contact->spouse_last_name = ucwords(strtolower(trim($newRecord['spouse_last_name'])));
            }
        }

        if (!$contact->save()) {
            $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Existing Contact did not save during name merge. Error message: ' . print_r($contact->getErrors(), true));
        }

        //@todo: can we make birthday part in merge and create contact re-usable???
        $birthdayFields = array(ContactAttributes::BIRTHDAY => 'birthday', ContactAttributes::SPOUSE_BIRTHDAY => 'spouse_birthday', ContactAttributes::CLOSING_ANNIVERSARY => 'closing_anniversary');
        foreach ($birthdayFields as $contactAttributeId => $birthdayField) {
            $birthdayContactAttribute = null;
            // on process if data exists
            if (trim($newRecord[$birthdayField])) {
                // check for existing data
                if ($birthdayContactAttribute = ContactAttributeValues::model()->findByAttributes(array('contact_id' => $contact->id, 'contact_attribute_id' => $contactAttributeId))) {
                    $birthdayContactAttribute->value = Yii::app()->format->formatDate(trim($newRecord[$birthdayField]), StmFormatter::MYSQL_DATE_FORMAT);
                } else {
                    $birthdayContactAttribute = new ContactAttributeValues();
                    $birthdayContactAttribute->contact_id = $contact->id;
                    $birthdayContactAttribute->contact_attribute_id = $contactAttributeId;
                    $birthdayContactAttribute->value = Yii::app()->format->formatDate(trim($newRecord[$birthdayField]), StmFormatter::MYSQL_DATE_FORMAT);
                    if (!$birthdayContactAttribute->save()) {
                        $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Error: Birthday did not save. ' . PHP_EOL . 'Error Message: ' . print_r($birthdayContactAttribute->getErrors(), true) . PHP_EOL . 'Birthday Data: ' . print_r($birthdayContactAttribute->attributes, true));
                    }
                }
            }
        }

        // see which contact attribute fields already exists
        $contactAttributeResults = Yii::app()->db->createCommand("SELECT contact_attribute_id, value FROM contact_attribute_values WHERE is_deleted=0 AND contact_id=" . $contact->id)->queryAll();

        $existingContactAttributeValues = array();
        foreach ($contactAttributeResults as $contactAttributeResult) {
            $existingContactAttributeValues[$contactAttributeResult['contact_attribute_id']] = $contactAttributeResult['value'];
        }

        // go through each field and pick out the contactAttributes
        foreach ($newRecord as $key => $value) {
            // If we have a transaction field value, add it
            if (trim($value) != '' && preg_match('/^contactAttributes\:([0-9]+)$/', $key, $matches) === 1) {
                // determine tnx field id

                if ($existingContactAttributeValues[$matches[1]]) {

                    // override existing record with imported record
//                    if($existingContactAttributeValues[$matches[1]] == '' && $value != '') {
                    $contactAttributeValue = ContactAttributeValues::model()->findByAttributes(array('contact_attribute_id' => $matches[1], 'contact_id' => $contact->id));
                    $contactAttributeValue->value = $value;
                    if (!$contactAttributeValue->save()) {
                        // add to error log so user can be aware
                        $errorData = 'Did not save values into empty contact_attribute_value. Please review data. Error Message: ' . print_r($contactAttributeValue->getErrors(), true);
                        $errors[] = $errorData;
                    }
//                    }
                    continue;
                }

                if (!in_array(trim($value), array('.', ',', ''))) {
                    // Create new contact attribute value
                    $contactAttributeValue = new ContactAttributeValues();
                    $contactAttributeValue->contact_attribute_id = $matches[1];
                    $contactAttributeValue->contact_id = $contact->id;
                    if (strlen($value) > 1000) {
                        $contactAttributeValue->value = substr($value, 0, 1000);

                        // add to error log so user can be aware
                        $errorData = 'Notification: Contact Attribute value was greater than 1000 characters and it was saved but cut off at 1000 characters. Please review value for full data.';
                        $errors[] = $errorData;
                    } else {
                        $contactAttributeValue->value = trim($value);
                    }

                    // save transaction field values
                    if (!$contactAttributeValue->save()) {
                        $errorData = 'Error: Transaction field value did not save. Error Message: ' . print_r($contactAttributeValue->getErrors(), true);
                        $errors[] = $errorData;
                    }
                }
            }
        }

        // if contactTag exist, link contact tag to contact, create tag if not exist
        if ($newRecord['contactTags']) {

            $contactTags = explode(',', $newRecord['contactTags']);
            foreach ($contactTags as $contactTagName) {

                // if the tag is not in collection, create and add.
                if (!isset(self::$contactTypesCollection[trim(strtolower($contactTagName))])) {

                    // see if it already exists, if not create a new one
                    if (($contactTag = ContactTypes::model()->findByAttributes(array('name' => $contactTagName)))) {
                        self::$contactTypesCollection[trim(strtolower($contactTagName))] = $contactTag;
                    } else {
                        $contactTag = new ContactTypes();
                        $contactTag->name = $contactTagName;
                        $contactTag->account_id = $accountId;
                        if ($contactTag->save()) {
                            self::$contactTypesCollection[trim(strtolower($contactTagName))] = $contactTag;
                        } else {
                            $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Error: Contact Tag did not save. Error Message: ' . print_r($contactTag->getErrors(), true) . PHP_EOL . 'Contact Tag Data: ' . print_r($contactTag->attributes, true));
                        }
                    }
                }

                // double check to see that tag exists in collection before saving the lookup
                if (isset(self::$contactTypesCollection[trim(strtolower($contactTagName))]) && !ContactTypeLu::model()->findByAttributes(array('contact_id' => $contact->id, 'contact_type_id' => self::$contactTypesCollection[trim(strtolower($contactTagName))]->id))) {

                    // create a new contact tag lookup
                    $contactTagLu = new ContactTypeLu();
                    $contactTagLu->contact_id = $contact->id;
                    $contactTagLu->contact_type_id = self::$contactTypesCollection[trim(strtolower($contactTagName))]->id;
                    if (!$contactTagLu->save()) {
                        $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Error: Contact Tag Lookup did not save Data: ' . print_r($contactTagLu->attributes, true) . PHP_EOL . 'Error Message: ' . print_r($contactTagLu->getErrors(), true));
                    }
                }
            }
        }

        $existingContactEmails = Yii::app()->db->createCommand("select TRIM(LOWER(email)) FROM emails where contact_id=" . $contact->id)->queryColumn();
        // compare and merge emails
        for ($i = 1; $i <= $numEmailsFields; $i++) {

            // add email if not exist
            if (!empty($newRecord['email_' . $i]) && !in_array(strtolower(trim($newRecord['email_' . $i])), $existingContactEmails)) {

                $newRecord['email_' . $i] = trim($newRecord['email_' . $i]);

                $email = new Emails('import');
                $email->setAttributes(array(
                    'account_id' => $accountId,
                    'contact_id' => $contactId,
                    'email_status_id' => EmailStatus::IMPORT,
                    'email' => trim($newRecord['email_' . $i]),
                    'email_type_ma' => 1,
                    'owner_ma' => 0,
                    'is_primary' => 0
                ));

                if (!$email->save()) {
                    $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Email #' . $i . ' did not save during contact merge with existing record. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($email->getErrors(), true));
                } else {
                    $existingEmailData[trim(strtolower($email->email))] = $email->contact_id;
                }
            }
        }


        // compare and merge phones
        $existingContactPhones = Yii::app()->db->createCommand("select TRIM(phone) FROM phones where contact_id=" . $contact->id)->queryColumn();
        for ($i = 1; $i <= $numPhoneFields; $i++) {

            $newRecord['phone_' . $i] = Yii::app()->format->formatInteger($newRecord['phone_' . $i]);

            if (!empty($newRecord['phone_' . $i]) && !in_array(trim($newRecord['phone_' . $i]), $existingContactPhones)) {
                // add to data array if value exists
                $phone = new Phones('import');
                $phone->setAttributes(array(
                    'account_id' => $accountId,
                    'contact_id' => $contactId,
                    'phone_type_ma' => null,
                    'phone' => $newRecord['phone_' . $i],
                    'owner_ma' => 0,
                    'is_primary' => 0
                ));

                if (!$phone->save()) {
                    $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Phone #' . $i . ' did not save during contact merge with existing record. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($phone->getErrors(), true));
                } else {
                    $existingPhoneData[$phone->phone][] = $phone->contact_id;
                }
            }
        }

        // return array with elements status (string, "success" or "error"), message (string), newPhoneIds (array), newEmailIds (array), contact (Contacts model) //????newAddressIds (array)
        return $contact;
    }


    /**
     * Compares existing contact data
     *
     * Compares existing contact data with the new record data for name, phones and emails to see if is exact match. True if exact match in all 3 cases, false if not.
     *
     * return boolean
     */
    //    protected function _compareContactData($newRecord, $contactId)
    //    {
    //        if(!($contact = Contacts::model()->findByPk($contactId))) {
    //            throw new Exception('Contact does not exist.');
    //        }
    //
    //        // compare all names
    //        if(strcasecmp($newRecord['first_name'], $contact->first_name) <> 0 || strcasecmp($newRecord['last_name'], $contact->last_name) <> 0 || strcasecmp($newRecord['spouse_first_name'], $contact->spouse_first_name) <> 0 || strcasecmp($newRecord['spouse_last_name'], $contact->spouse_last_name) <> 0) {
    //
    //            // names are not an exact match, return false
    //            return false;
    //        }
    //
    //        // compare all emails
    //        $emails = Yii::app()->db->createCommand("SELECT LOWER(email) email from emails where contact_id={$contactId}")->queryColumn();
    //
    //        $newEmails = array();
    //        for($i=1; $i<=4; $i++) {
    //            if(!empty($newRecord['email_'.$i])) {
    //                $newEmails[] = strtolower($newRecord['email_'.$i]);
    //            }
    //        }
    //
    //        $emailDiff1 = array_diff($emails, $newEmails);
    //        $emailDiff2 = array_diff($newEmails, $emails);
    //
    //        // emails are not an exact match, return false
    //        if(!empty($emailDiff1) || !empty($emailDiff2)) {
    //
    //            return false;
    //        }
    //
    //        // compare all phones
    //        $phones = Yii::app()->db->createCommand("SELECT phone from phones where contact_id={$contactId}")->queryColumn();
    //        $newPhones = array();
    //        for($i=1; $i<=4; $i++) {
    //            if(!empty($newRecord['phone_'.$i])) {
    //                $newPhones[] = $newRecord['phone_'.$i];
    //            }
    //        }
    //
    //        $phoneDiff1 = array_diff($phones, $newPhones);
    //        $phoneDiff2 = array_diff($newPhones, $phones);
    //
    //        // emails are not an exact match, return false
    //        if(!empty($phoneDiff1) || !empty($phoneDiff2)) {
    //
    //            return false;
    //        }
    //
    //        return true;
    //    }

    /**
     * Add New Contact
     *
     * Add new contact based on the new record data format of first_name, last_name, etc.
     * @param $newRecord
     * @param $importErrorRows
     *
     * @return bool|Contacts
     */
    public static function addNewContact($newRecord, &$namesExisting, &$lastNamesExisting, &$existingEmailData, &$existingPhoneData, &$importErrorRows, $scenario = null, $allowedDupeNames = array())
    {
        StmImportFunctions::formatPhones($newRecord);
        // put existing phone in contactPhoneAdd property so it clears validation. A phone # may be in later position while first is blank.
        for ($i = 1; $i <= 5; $i++) {
            if (!isset($contactPhoneAdd) && !empty($newRecord['phone_' . $i])) {
                $contactPhoneAdd = $newRecord['phone_' . $i];
                break;
            }
        }

        StmImportFunctions::formatEmails($newRecord);
        // put existing email in contactEmailAdd property so it clears validation. An email # may be in later position while first is blank.
        for ($i = 1; $i <= 5; $i++) {
            if (!isset($contactEmailAdd) && !empty($newRecord['email_' . $i])) {
                $contactEmailAdd = $newRecord['email_' . $i];
                break;
            }
        }

        // for allowed dupe names, if last name empty, put in dash
        if (in_array(strtolower(trim($newRecord['first_name'])), $allowedDupeNames) && trim($newRecord['last_name']) == '') {
            $newRecord['last_name'] = '-';
        }

        // check to see if contact exists by same name.???????????

        // Create and save new contact record

        $account_id = (!isset($newRecord['account_id']) || empty($newRecord['account_id']) ? Yii::app()->user->accountId : $newRecord['account_id']);

        $contact = new Contacts();
        if ($scenario) {
            $contact->setScenario($scenario);
        }
        $sourceId = self::getSourceId($newRecord);

        $contact->setAttributes(array(
            'account_id' => $account_id,
            'source_id' => ($sourceId) ? $sourceId : Sources::UNKNOWN,
            'contact_status_ma' => Contacts::STATUS_ACTIVE,
            'first_name' => ucwords(strtolower(trim($newRecord['first_name']))),
            'last_name' => ucwords(strtolower(trim($newRecord['last_name']))),
            'spouse_first_name' => ucwords(strtolower(trim($newRecord['spouse_first_name']))),
            'spouse_last_name' => ucwords(strtolower(trim($newRecord['spouse_last_name']))),
            'contactEmailAdd' => $contactEmailAdd,
            'contactPhoneAdd' => $contactPhoneAdd,
            'notes' => (trim($newRecord['contact_notes'])) ? trim($newRecord['contact_notes']) : null,
        ));


        if (trim($newRecord['added'])) {
            $addedDate = date('Y-m-d H:i:s', strtotime(trim($newRecord['added'])));
        }
        $contact->added = (isset($addedDate) && strpos($addedDate, '1969') !== false) ? $addedDate : $contact->added = new CDbExpression('NOW()');

        if (!$contact->save()) {
            $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'New Contact did not save. Error message: ' . print_r($contact->getErrors(), true));
            return false;
        } else {

            $namesExisting[strtolower(trim($contact->first_name . ' ' . $contact->last_name))][] = $contact->id;
            $lastNamesExisting[strtolower(trim($contact->last_name))][] = $contact->id;
            $contact->source_id = self::getSourceId($newRecord);

            if (!$contact->save()) {
                $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Error: Contact did not save Source: ' . print_r(self::$sourcesCollection[$newRecord['source']]->attributes, true) . PHP_EOL . 'Error Message: ' . print_r($contact->getErrors(), true) . PHP_EOL . 'Contact Data: ' . print_r($contact->attributes, true));
            }

            $birthdayFields = array(ContactAttributes::BIRTHDAY => 'birthday', ContactAttributes::SPOUSE_BIRTHDAY => 'spouse_birthday');
            foreach ($birthdayFields as $contactAttributeId => $birthdayField) {
                $birthdayContactAttribute = null;
                // on process if data exists
                if (trim($newRecord[$birthdayField])) {
                    // check for existing data
                    if ($birthdayContactAttribute = ContactAttributeValues::model()->findByAttributes(array('contact_id' => $contact->id, 'contact_attribute_id' => $contactAttributeId))) {
                        $birthdayContactAttribute->value = Yii::app()->format->formatDate(trim($newRecord[$birthdayField]), StmFormatter::MYSQL_DATE_FORMAT);
                    } else {
                        $birthdayContactAttribute = new ContactAttributeValues();
                        $birthdayContactAttribute->contact_id = $contact->id;
                        $birthdayContactAttribute->contact_attribute_id = $contactAttributeId;
                        $birthdayContactAttribute->value = Yii::app()->format->formatDate(trim($newRecord[$birthdayField]), StmFormatter::MYSQL_DATE_FORMAT);
                        if (!$birthdayContactAttribute->save()) {
                            $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Error: Birthday did not save. ' . PHP_EOL . 'Error Message: ' . print_r($birthdayContactAttribute->getErrors(), true) . PHP_EOL . 'Birthday Data: ' . print_r($birthdayContactAttribute->attributes, true));
                        }
                    }
                }
            }

            // go through each field and pick out the contactAttributes
            foreach ($newRecord as $key => $value) {
                // If we have a transaction field value, add it
                if (trim($value) != '' && preg_match('/^contactAttributes\:([0-9]+)$/', $key, $matches) === 1) {

                    if (!in_array(trim($value), array('.', ',', ''))) {
                        // Create new contact attribute value
                        $contactAttributeValue = new ContactAttributeValues();
                        $contactAttributeValue->contact_attribute_id = $matches[1];
                        $contactAttributeValue->contact_id = $contact->id;
                        if (strlen($value) > 1000) {
                            $contactAttributeValue->value = substr($value, 0, 1000);

                            // add to error log so user can be aware
                            $errorData = 'Notification: Contact Attribute value was greater than 1000 characters and it was saved but cut off at 1000 characters. Please review value for full data.';
                            $errors[] = $errorData;
                        } else {
                            $contactAttributeValue->value = trim($value);
                        }

                        // save transaction field values
                        if (!$contactAttributeValue->save()) {
                            $errorData = 'Error: Contact Attribute field value did not save. Error Message: ' . print_r($contactAttributeValue->getErrors(), true);
                            $errors[] = $errorData;
                        }
                    }
                }
            }

            // link contact tag to contact, create tag if not exist
            if ($newRecord['contactTags']) {

                $contactTags = explode(',', $newRecord['contactTags']);
                foreach ($contactTags as $contactTagName) {

                    // if the tag is not in collection, create and add.
                    if (!isset(self::$contactTypesCollection[trim(strtolower($contactTagName))])) {

                        // see if it already exists, if not create a new one
                        if (($contactTag = ContactTypes::model()->findByAttributes(array('name' => $contactTagName)))) {
                            self::$contactTypesCollection[trim(strtolower($contactTagName))] = $contactTag;
                        } else {
                            $contactTag = new ContactTypes();
                            $contactTag->name = $contactTagName;
                            $contactTag->account_id = $account_id;
                            if ($contactTag->save()) {
                                self::$contactTypesCollection[trim(strtolower($contactTagName))] = $contactTag;
                            } else {
                                $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Error: Contact Tag did not save. Error Message: ' . print_r($contactTag->getErrors(), true) . PHP_EOL . 'Contact Tag Data: ' . print_r($contactTag->attributes, true));
                            }
                        }
                    }

                    // double check to see that tag exists in collection before saving the lookup
                    if (isset(self::$contactTypesCollection[trim(strtolower($contactTagName))])) {
                        // create a new contact tag lookup
                        $contactTagLu = new ContactTypeLu();
                        $contactTagLu->contact_id = $contact->id;
                        $contactTagLu->contact_type_id = self::$contactTypesCollection[trim(strtolower($contactTagName))]->id;
                        if (!$contactTagLu->save()) {
                            $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Error: Contact Tag Lookup did not save Data: ' . print_r($contactTagLu->attributes, true) . PHP_EOL . 'Error Message: ' . print_r($contactTagLu->getErrors(), true));
                        }
                    }
                }
            }

            self::saveEmailFields($newRecord, $contact->id, $account_id, $existingEmailData, $startIndex = 1, $endIndex = 5, $importErrorRows);

            self::savePhoneFields($newRecord, $contact->id, $account_id, $existingPhoneData, $startIndex = 1, $endIndex = 5, $importErrorRows);
        }

        return $contact;
    }

    public static function addContactToDialerList(Contacts $contact, $callListId, $newRowData = null, &$importErrorRows = null)
    {
        if (!is_numeric($callListId)) {
            throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Call List # is not numeric. CallListId: ' . $callListId);
        }
        $unDeleteTopStackPerformed = false;
        // get phones data to see if it already exist in the call_list_phone or not.
        $phoneDialerListData = Yii::app()->db->createCommand("select p.id phone_id, clp.id call_list_phone_id, clp.call_list_id, p.is_deleted FROM phones p LEFT JOIN call_list_phones clp ON clp.phone_id=p.id AND clp.call_list_id={$callListId} WHERE p.contact_id=" . $contact->id)->queryAll();

        // go through each phone record to see if call_list_phone record exists. Create if not exist. If exist, check for soft delete and undelete.
        foreach ($phoneDialerListData as $phoneRow) {

            // Add call list phone record
            if (empty($phoneRow['call_list_phone_id'])) {

                $callListPhone = new CallListPhones();
                $callListPhone->setAttributes(array(
                    'phone_id' => $phoneRow['phone_id'],
                    'call_list_id' => $callListId,
                    'status' => CallListPhones::QUEUED,
                    'call_count' => 0,
                    'sort_order' => Yii::app()->db->createCommand("SELECT MIN(sort_order) from call_list_phones where call_list_id={$callListId}")->queryScalar() - 1,
                    'updated_by' => Yii::app()->user->id,
                ));
                if (!$callListPhone->save()) {
                    if ($newRowData && $importErrorRows) {
                        $newRowData['errorDesc'] = 'Call List Phone did not save. Error Message: ' . print_r($callListPhone->getErrors(), true);
                        $importErrorRows[] = $newRowData;
                    }
                }
            } elseif ($unDeleteTopStackPerformed == false) {
                // un-delete and re-queue all phones for this contact on call_list_phones, reset the added date to NOW() so it populates at the top of the list
                // Get sort order # to make it sort to the TOP of the list
                $sortOrder = Yii::app()->db->createCommand("SELECT MIN(sort_order) from call_list_phones where call_list_id={$callListId}")->queryScalar() - 1;
                Yii::app()->db->createCommand("UPDATE phones p JOIN call_list_phones clp ON clp.phone_id=p.id SET clp.status='" . CallListPhones::QUEUED . "', clp.hard_deleted=NULL, clp.hard_deleted_by=NULL, clp.is_deleted=0, added=NOW() WHERE clp.call_list_id={$callListId} AND sort_order={$sortOrder} AND p.contact_id=" . $contact->id)->execute();

                $unDeleteTopStackPerformed = true;
            }
        }
    }

    public static function getSourceId($newRecord)
    {
        $sourceId = null;

        if (isset(self::$sourcesCollection[strtolower(trim($newRecord['source']))])) {
            return self::$sourcesCollection[strtolower(trim($newRecord['source']))]->id;
        }

        // link source name to contact, create source if not exist
        if ($newRecord['source'] && !empty(trim($newRecord['source']))) {

            if (($source = Sources::model()->findByAttributes(array('name' => $newRecord['source'])))) {
                self::$sourcesCollection[strtolower(trim($newRecord['source']))] = $source;
                $sourceId = $source->id;
            } else {

                $source = new Sources();
                $source->account_id = Yii::app()->user->accountId;
                $source->name = $newRecord['source'];
                if ($source->save()) {
                    $sourceId = $source->id;
                    self::$sourcesCollection[strtolower(trim($newRecord['source']))] = $source;
                } else {
                    $newRecord['errorDesc'] = 'Error: New Source did not save. Error Message: ' . print_r($source->getErrors(), true) . PHP_EOL . 'Source Data: ' . print_r($source->attributes, true);
                    $importErrorRows[] = $newRecord;
                }
            }
        }

        if (!$sourceId && $newRecord['sourceId'] && is_numeric($newRecord['sourceId'])) {
            if (($source = Sources::model()->findByAttributes(array('id' => $newRecord['sourceId'])))) {
                $sourceId = $source->id;
                self::$sourcesCollection[strtolower(trim($newRecord['source']))] = $source;
            }
        }
        return $sourceId;
    }


    /**
     * Get Fields Sets
     *
     * Takes email and phone fields and puts them into 1 array. Loops through email_1 thru email_4 and loads data array with emails that exist.
     *
     * return boolean
     */
    public static function getFieldSet($newRecord, $fieldName, $startIndex, $endIndex)
    {
        $data = array();
        for ($i = $startIndex; $i <= $endIndex; $i++) {

            // add to data array if value exists
            if ($newRecord[$fieldName . '_' . $i]) {
                $data[] = $newRecord[$fieldName . '_' . $i];
            }
        }
        // no match is found, return false
        return $data;
    }

    /**
     * Save Email Fields
     *
     * Loops through email_1 thru email_4 and saves them as new record. If error, adds to import error rows for review.
     *
     * return integer $errorCount
     */
    public static function saveEmailFields($newRecord, $contactId, $accountId, &$existingEmailData, $startIndex = 1, $endIndex = 5, &$importErrorRows)
    {
        StmImportFunctions::formatEmails($newContact);
        $existingContactEmails = Yii::app()->db->createCommand("select TRIM(LOWER(email)) FROM emails where contact_id=" . $contactId)->queryColumn();
        $errors = array();
        // compare and merge emails
        for ($i = $startIndex; $i <= $endIndex; $i++) {

            // add email if not exist
            if (isset($newRecord['email_' . $i]) && !empty($newRecord['email_' . $i]) && !isset($existingData[$newRecord['email_' . $i]]) && !in_array(strtolower(trim($newRecord['email_' . $i])), $existingContactEmails)) {

                $newRecord['email_' . $i] = trim($newRecord['email_' . $i]);

                $email = new Emails('import');
                $email->setAttributes(array(
                    'account_id' => $accountId,
                    'contact_id' => $contactId,
                    'email_status_id' => 1,
                    'email' => trim($newRecord['email_' . $i]),
                    'email_type_ma' => 1,
                    'owner_ma' => 0,
                    'is_primary' => 0
                ));

                if (!$email->save()) {
                    $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Email #' . $i . ' did not save.  Contact already created. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($email->getErrors(), true));
                } else {
                    $existingEmailData[$email->email] = $contactId;
                }
            }
        }

        return $errors;
    }

    /**
     * Save Phone Fields
     *
     * Loops through email_1 thru email_4 and saves them as new record. If error, adds to import error rows for review.
     *
     * return integer $errorCount
     */
    public static function savePhoneFields($newRecord, $contactId, $accountId, &$existingPhoneData, $startIndex = 1, $endIndex = 5, &$importErrorRows)
    {
        StmImportFunctions::formatPhones($newRecord);
        $errors = array();
        $existingContactPhones = array();
        $existingContactPhoneResults = Yii::app()->db->createCommand("select id phone_id, TRIM(phone) phone, is_deleted FROM phones where contact_id={$contactId}")->queryColumn();
        // compare and merge phones
        foreach ($existingContactPhoneResults as $existingContactPhoneResult) {
            $existingContactPhones[$existingContactPhoneResult['phone']]['is_deleted'] = $existingContactPhoneResult['is_deleted'];
            $existingContactPhones[$existingContactPhoneResult['phone']]['phone_id'] = $existingContactPhoneResult['phone_id'];
        }

        for ($i = $startIndex; $i <= $endIndex; $i++) {

            if (isset($newRecord['phone_' . $i]) && !empty($newRecord['phone_' . $i]) && !in_array($newRecord['phone_' . $i], $existingContactPhones)) {
                // add to data array if value exists
                $phone = new Phones('import');
                $phone->setAttributes(array(
                    'account_id' => $accountId,
                    'contact_id' => $contactId,
                    'phone_type_ma' => 1,
                    'phone' => $newRecord['phone_' . $i],
                    'owner_ma' => 0,
                    'is_primary' => 0
                ));

                if (!$phone->save()) {
                    $importErrorRows[] = StmImportFunctions::addErrorLogData($newRecord, 'Phone #' . $i . ' did not save during contact merge with existing record. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($phone->getErrors(), true));
                } else {
                    $existingPhoneData[$phone->phone][] = $contactId;
                }
            } // if the existing phone for contact is soft deleted, un-deleted it.
            elseif (isset($newRecord['phone_' . $i]) && !empty($newRecord['phone_' . $i]) && in_array($newRecord['phone_' . $i], $existingContactPhones) && $existingContactPhones[$newRecord['phone_' . $i]]['is_deleted'] == 1) {
                if ($phone = Phones::model()->findByPk($existingContactPhones[$newRecord['phone_' . $i]]['phone_id'])) {
                    $phone->is_deleted = 0;
                    $phone->save();
                }
            }
        }

        return $errors;
    }

    /**
     * Save Address Fields
     *
     * return array
     */
    public static function saveAddressFields($newRecord, $contactId, $accountId, &$existingAddressData, $stateFullNames, $stateShortNames, &$importErrorRows, $checkForDupes = true)
    {
        $errors = array();
        $address = null;

        // sanity check for contact ID
        if (!$contactId) {
            $errorData = StmImportFunctions::addErrorLogData($newRecord, 'Contact ID required. Cannot save address without it. Skipping address.');
            $errors[] = $errorData;
            $importErrorRows[] = $errorData;
            return array(false, $errors);
        }

        // check numeric contact ID
        if (!is_numeric($contactId)) {
            $errorData = StmImportFunctions::addErrorLogData($newRecord, 'Contact ID must be numeric. Cannot save address without it. Skipping address.');
            $errors[] = $errorData;
            $importErrorRows[] = $errorData;
            return array(false, $errors);
        }

        // trim and proper case address fields
        $newRecord['address'] = ucwords(strtolower(trim($newRecord['address'])));
        $newRecord['address'] = preg_replace('/\s+/', ' ', $newRecord['address']);

        $newRecord['city'] = ucwords(strtolower(trim($newRecord['city'])));
        $newRecord['zip'] = substr(trim($newRecord['zip']), 0, 5);

        // check for missing data fields
//        if(!$newRecord['address'] || !$newRecord['city'] || !$newRecord['state'] || !$newRecord['zip']) {
//            $errorData = StmImportFunctions::addErrorLogData($newRecord, 'Address has missing fields/data. Contact already created. Manually add to contact.');
//            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
//            return array(false, $errors);
//        }

        // Determine state ID, if we have one
        $stateId = array_key_exists(strtolower(trim($newRecord['state'])), $stateFullNames) ? $stateFullNames[strtolower(trim($newRecord['state']))] : (array_key_exists(strtolower(trim($newRecord['state'])), $stateShortNames) ? $stateShortNames[strtolower(trim($newRecord['state']))] : null);
        if (!$stateId) {
            $errorData = StmImportFunctions::addErrorLogData($newRecord, 'Address has invalid state. Contact already created. Manually add to contact.');
            $errors[] = $errorData;
            $importErrorRows[] = $errorData;
            return array(false, $errors);
        }

        if (!isset($newRecord['ignore_dupe_address']) || $newRecord['ignore_dupe_address'] === FALSE) {
            // check for dupes addresses
            if ($newRecord['address']) {
                $existingAddresses = Addresses::model()->findAllByContactIdAddress($contactId, $newRecord['address'], $newRecord['zip']);
                if ($existingAddresses) {
                    return array($existingAddresses[0], $errors);
                }
            }

            if ($existingAddressId = Addresses::existsForContact($newRecord['address'], $newRecord['city'], $stateId, $newRecord['zip'], $contactId)) {
                $existingAddress = Addresses::model()->findByPk($existingAddressId);

                // address already exists for this contactId
                return array($existingAddress, $errors);
            }
        }

        // Create and save new address record
        $address = new Addresses();
        $address->setAttributes(array(
            'account_id' => $accountId,
            'address' => $newRecord['address'],
            'city' => $newRecord['city'],
            'state_id' => $stateId,
            'zip' => $newRecord['zip']
        ));

        if (!$address->save()) {
            $errorData = StmImportFunctions::addErrorLogData($newRecord, 'Address did not save. Contact already created. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($address->getErrors(), true));
            $errors[] = 'Address did not save. Contact already created. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($address->getErrors(), true);
            $importErrorRows[] = $errorData;
            return array(false, $errors);
        } else {
            $existingAddressData[substr($address->address, 0, Addresses::COMPARE_LENGTH)][] = $contactId;

            // Link address to contact
            $addressContact = new AddressContactLu();
            $addressContact->setAttributes(array(
                'address_id' => $address->id,
                'contact_id' => $contactId,
                'is_primary' => 1
            ));

            if (!$addressContact->save()) {
                $errorData = StmImportFunctions::addErrorLogData($newRecord, 'Address Contact Lookup did not save. Contact already created. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($addressContact->getErrors(), true));
                $errors[] = 'Address Contact Lookup did not save. Contact already created. Manually add to contact.' . PHP_EOL . 'Error message: ' . print_r($addressContact->getErrors(), true);
                $importErrorRows[] = $errorData;
            }
        }

        return array($address, $errors);
    }

    /**
     * Adds error log data
     */
    public static function addErrorLogData($newRecord, $errorMessage)
    {
        $newRecord['errorDesc'] = $errorMessage;
        return $newRecord;
    }

    public static function saveComponentModel($componentTypeId, $newRecord, $contactId, &$importErrorRows, $originType, $addressId = null)
    {
        $componentModel = null;
        $errors = null;

        switch ($componentTypeId) {
            case ComponentTypes::CONTACTS:
                // not sure if retreiving the model is needed. Leave blank for now until explicitly needed.
                break;

            case ComponentTypes::SELLERS:
            case ComponentTypes::BUYERS:
                list($componentModel, $errors) = self::saveTransactionsModel($componentTypeId, $newRecord, $contactId, $importErrorRows, $originType, $addressId);
                break;

            case ComponentTypes::CLOSINGS:
                switch (strtolower($newRecord['tnx_type'])) {
                    case ComponentTypes::SELLERS:
                    case '3':
                    case 's':
                    case 'seller':
                    case 'sellers':
                        $tnxComponentTypeId = ComponentTypes::SELLERS;
                        break;

                    case ComponentTypes::BUYERS:
                    case '2':
                    case 'b':
                    case 'buyer':
                    case 'buyers':
                        $tnxComponentTypeId = ComponentTypes::BUYERS;
                        break;

                    default:
                        throw new Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid Component Type for Closing: "' . $newRecord['tnx_type'] . '"' . print_r($newRecord, true));
                        break;
                }

                list($tnxModel, $errors) = self::saveTransactionsModel($tnxComponentTypeId, $newRecord, $contactId, $importErrorRows, $originType, $addressId);
                if ($tnxModel && is_object($tnxModel) && $tnxModel->id) {
                    list($componentModel, $errors) = self::saveClosingsModel($tnxComponentTypeId, $newRecord, $contactId, $importErrorRows, $tnxModel->id);
                }
                break;

            case ComponentTypes::RECRUITS:
                list($componentModel, $errors) = self::saveRecruitsModel($newRecord, $contactId, $importErrorRows);
                break;
        }

        return array($componentModel, $errors);
    }

    public static function saveTransactionsModel($componentTypeId, $newRecord, $contactId, &$importErrorRows, $originType, $addressId)
    {
        //$originalComponentTypeId = $componentTypeId;
        $componentModel = null;
        $errors = null;

        //@todo: check to see if contact already has lead that is non-closed/pending???? - make this an argument??? sometimes want to ignore closed status, sometimes not??


        //@todo: see if a transaction with status not pending/closed exists. If it's trashed, expired, update to Nurturing
        $transactionCommand = Yii::app()->db->createCommand()
            ->select('t.id')
            ->from('transactions t')
            ->where('component_type_id=:componentTypeId', array(':componentTypeId' => $componentTypeId))
            ->andWhere('contact_id=:contactId', array(':contactId' => $contactId))
            ->andWhere('t.is_deleted=0')
            ->order('id ASC');
        //->andWhere(array('in', 'transaction_status_id', array()))

        // see if there is an active transaction
        $activeTransactionExistsCommand = clone $transactionCommand;
        $activeTransactionExistsCommand->andWhere(array('NOT IN', 't.transaction_status_id', array(TransactionStatus::CLOSED_ID)));
        $activeTransactionId = $activeTransactionExistsCommand->queryColumn();
        if (!empty($activeTransactionId)) {
            $componentModel = Transactions::model()->findByPk($activeTransactionId[0]);
            //$errorData = 'FYI: Found existing ' . ComponentTypes::getNameById($componentTypeId) . '# ' . ((count($activeTransactionId) == 1) ? $activeTransactionId[0] : print_r($activeTransactionId, true)) . ' for Contact ID# ' . $contactId . ', did not create new record and used existing one.';

            //@todo: ADD logic for if existing seller does not have an address, adding it to the tnx model
            if ($componentModel->component_type_id == ComponentTypes::SELLERS && $addressId && !$componentModel->address_id) {
                $componentModel->address_id = $addressId;
                $componentModel->save();
            }

            //$errors[] = $errorData;
            //$importErrorRows[] = $errorData;
            //return array($componentModel, $errors);
        }

        if(empty($activeTransactionId)) {
            //@todo: $addressId used to be optional. Keep an eye on this for now, see if it needs to be made optional
            if (!empty($addressId)) {
                $transactionCommand->andWhere('address_id=:addressId', array(':addressId' => $addressId));
            }

            if ($transactionExists = $transactionCommand->queryColumn()) {
                if (count($transactionExists)) {
                    $componentModel = Transactions::model()->findByPk($transactionExists[0]);

                    // address for component is not the same (or empty) as $addressId.
                    if ($componentModel && $componentModel->address_id != $addressId && !empty($addressId)) {
                        // if there is no address, save this address
                        if (!$componentModel->address_id) {
                            $componentModel->address_id = $addressId;
                            $componentModel->save();
                        } // existing tnx address ID and one provided not equal, log error
                        elseif ($componentModel->address_id) {
                            $errorData = '*** URGENT: THIS SHOULD NOT OCCUR *** Existing ' . ComponentTypes::getNameById($componentTypeId) . ' transaction address does not match provided addressId: ' . $addressId . '. Review data.';
                            $errors[] = $errorData;
                            //$importErrorRows[] = $errorData;
                            return array($componentModel, $errors);
                        }
                    } elseif (empty($componentModel)) {
                        $errorData = '*** URGENT: THIS SHOULD NOT OCCUR *** No Transaction found when Transaction ID exists. ' . ComponentTypes::getNameById($componentTypeId) . ' found for this Contact ID. Review data.';
                        $errors[] = $errorData;
//                    $importErrorRows[] = $errorData;
                        return array($componentModel, $errors);
                    }
                } // multiple transactions found
                else {
                    $errorData = '*** URGENT: THIS SHOULD NOT OCCUR *** Multiple ' . ComponentTypes::getNameById($componentTypeId) . ' found for this Contact ID. Review data.';
                    $errors[] = $errorData;
//                $importErrorRows[] = $errorData;
                    return array($componentModel, $errors);
                }
            }

            if (!$componentModel) {
                $sourceId = self::getSourceId($newRecord);

                $transactionStatusId = (in_array($newRecord['transaction_status_id'], array(TransactionStatus::NEW_LEAD_ID, TransactionStatus::HOT_A_LEAD_ID, TransactionStatus::B_LEAD_ID, TransactionStatus::C_LEAD_ID, TransactionStatus::D_NURTURING_SPOKE_TO_ID, TransactionStatus::NURTURING_ID, TransactionStatus::ACTIVE_LISTING_ID, TransactionStatus::UNDER_CONTRACT_ID, TransactionStatus::CLOSED_ID))) ? $newRecord['transaction_status_id'] : TransactionStatus::NEW_LEAD_ID;

                // Create new transaction
                $componentModel = new Transactions();
                $componentModel->setAttributes(array(
                    'account_id' => Yii::app()->user->account_id,
                    'transactionType' => ($componentTypeId == ComponentTypes::SELLERS) ? 'sellers' : 'buyers',
                    'component_type_id' => $componentTypeId,
                    'origin_type' => $originType,
                    'transaction_status_id' => $transactionStatusId,
                    'source_id' => ($sourceId) ? $sourceId : Sources::UNKNOWN,
                    'contact_id' => $contactId,
                    'notes' => ($componentTypeId == ComponentTypes::SELLERS) ? trim($newRecord['seller_notes']) : trim($newRecord['buyer_notes']),
                    'added' => (isset($newRecord['added'])) ? date('Y-m-d H:i:s', strtotime(trim($newRecord['added']))) : new CDbExpression('NOW()'),
                ));

                // Checks to see if an address was submitted
                if ($addressId) {
                    $componentModel->address_id = $addressId;
                }

                // Attempt to save
                if (!$componentModel->save()) {
                    $errorData = 'Error: Transaction did not save. Error Message: ' . print_r($componentModel->getErrors(), true);
                    $errors[] = $errorData;
//                $importErrorRows[] = $errorData;
                    return array(false, $errors);
                }
            }
        }

        // see which transaction fields already exists
        $transactionFieldValueResults = Yii::app()->db->createCommand("SELECT transaction_field_id, value FROM transaction_field_values WHERE transaction_id=" . $componentModel->id)->queryAll();

        $existingTransactionFieldValues = array();
        foreach ($transactionFieldValueResults as $transactionFieldValueResult) {
            $existingTransactionFieldValues[$transactionFieldValueResult['transaction_field_id']] = $transactionFieldValueResult['value'];
        }

        // save transactionTags
        if (!empty(trim($newRecord['transaction_tags']))) {

            // if static var is null that means its never been set. At minimum should be empty array.
            if (self::$existingTransactionTags == null) {
                $results = Yii::app()->db->createCommand("SELECT id, TRIM(LOWER(name)) name FROM transaction_tags WHERE account_id=" . Yii::app()->user->accountId . " AND is_deleted=0")->queryAll();
                if (empty($results)) {
                    self::$existingTransactionTags = array();
                } else {
                    foreach ($results as $r => $row) {
                        self::$existingTransactionTags[$row['name']] = $row['id'];
                    }

                }
            }

            $transactionTags = explode(',', trim($newRecord['transaction_tags']));
            foreach ($transactionTags as $transactionTagName) {
                // reset so previous row data is not carrying over
                $transactionTagId = null;
                if (isset(self::$existingTransactionTags[trim(strtolower($transactionTagName))])) {
                    $transactionTagId = self::$existingTransactionTags[trim(strtolower($transactionTagName))];
                } else {
                    $transactionTag = new TransactionTags();
                    $transactionTag->setAttributes(array(
                        'account_id' => Yii::app()->user->accountId,
                        'name' => trim($transactionTagName),
                    ));
                    if (!$transactionTag->save()) {
                        //log error message
                        $errors[] = 'Transaction Tag did now save. Error Message: ' . print_r($transactionTag->getErrors(), true);
                    } else {
                        $transactionTagId = $transactionTag->id;
                        self::$existingTransactionTags[trim(strtolower($transactionTagName))] = $transactionTag->id;
                    }
                }

                if ($transactionTagId) {
                    // if exist create lookup
                    $transactionTagLu = new TransactionTagLu();
                    $transactionTagLu->setAttributes(array(
                        'transaction_tag_id' => $transactionTagId,
                        'transaction_id' => $componentModel->id
                    ));
                    if (!$transactionTagLu->save()) {
                        // log error message
                        $errors[] = 'Transaction Tag Lu did now save. Error Message: ' . print_r($transactionTagLu->getErrors(), true);
                    }
                }
            }
        }

        if (!empty(trim($newRecord['assignment_type_id'])) && is_numeric(trim($newRecord['assignment_type_id'])) && !empty(trim($newRecord['assigned_to_user_id'])) && is_numeric(trim($newRecord['assigned_to_user_id']))) {

            $assignmentErrorFlag = false;

            // check to see if assignment types are valid
            $validAssignmentTypeIds = array(
                ComponentTypes::SELLERS => array(AssignmentTypes::LISTING_AGENT, AssignmentTypes::SELLER_ISA),
                ComponentTypes::BUYERS => array(AssignmentTypes::BUYER_AGENT, AssignmentTypes::BUYER_ISA),
            );
            // check for valid assignment type id
            if (!in_array(intval(trim($newRecord['assignment_type_id'])), $validAssignmentTypeIds[$componentTypeId])) {
                $assignmentErrorFlag = true;
                //@todo: error invalid assignment type -
                $errors[] = 'Invalid Assignment Type ID. Assignment not saved.';
            }

            // load list of existing users if empty to compare against for validity of assigned to user ID
            if (self::$_adminUserIds == null) {

                self::$_adminUserIds = array();
                if ($adminUsers = Contacts::model()->byAdmins()->findAll()) {
                    foreach ($adminUsers as $adminUser) {
                        self::$_adminUserIds[] = $adminUser->id;
                    }
                }
            }

            // check to see if assigned to is valid admin user id
            if (!in_array(intval(trim($newRecord['assigned_to_user_id'])), self::$_adminUserIds)) {
                $assignmentErrorFlag = true;
                $errors[] = 'Invalid Assigned To User ID. Assignment not saved.';
            }

            // check to see if assignment already exists
            if (!$assignmentErrorFlag) {

                $assignment = new Assignments();
                $assignment->setAttributes(array(
                    'component_type_id' => $componentTypeId,
                    'component_id' => $componentModel->id,
                    'assignment_type_id' => $newRecord['assignment_type_id'],
                    'assignee_contact_id' => $newRecord['assigned_to_user_id'],
                ));

                if (!$assignment->save()) {
                    //@todo: error message
                    $errors[] = 'Assignment Type did not save. Error Message: ' . print_r($assignment->getErrors(), true);
                }
            }
        }

        //@todo: 2nd assignment option


        // save external identifier
        // Look for transaction field values

        $transactionFieldValuesUpdated = "";
        foreach ($newRecord as $key => $value) {

            // fudge/hack the $key for external_id since it is a special case
            if ($key == 'external_id') {
                switch ($componentTypeId) {
                    case ComponentTypes::SELLERS:
                        $tnxFieldId = TransactionFields::IMPORT_EXTERNAL_IDENTIFIER_SELLER;
                        break;

                    case ComponentTypes::BUYERS:
                        $tnxFieldId = TransactionFields::IMPORT_EXTERNAL_IDENTIFIER_BUYER;
                        break;

                    default:
                        $errorData = 'Error: Invalid Component Type Id: ' . $componentTypeId . '.';
                        $errors[] = $errorData;
//                        $importErrorRows[] = $errorData;
                        return array(false, $errors);
                        break;
                }
                $key = 'transactionFields:' . $tnxFieldId;
            }


            // If we have a transaction field value, add it
            if (trim($value) != '' && preg_match('/^transactionFields\:([0-9]+)$/', $key, $matches) === 1) {
                // determine tnx field id

                if ($existingTransactionFieldValues[$matches[1]]) {

                    // if existing record is blank,
                    if ($existingTransactionFieldValues[$matches[1]] == '' && $value != '') {
                        $transactionFieldValue = TransactionFieldValues::model()->findByAttributes(array('transaction_field_id' => $matches[1], 'transaction_id' => $componentModel->id));

                        $origValue = $transactionFieldValue->value;

                        $transactionFieldValue->value = $existingTransactionFieldValues[$matches[1]];
                        if (!$transactionFieldValue->save()) {
                            // add to error log so user can be aware
                            $errorData = 'Did not save values into empty transaction_field_value. Please review data. Error Message: ' . print_r($transactionFieldValue->getErrors(), true);
                            $errors[] = $errorData;
//                            $importErrorRows[] = $errorData;
                        } else {
                            $transactionFieldValuesUpdated .= $transactionFieldValue->transactionField->name.": from ".$origValue." to ".$existingTransactionFieldValues[$matches[1]]."\n";
                        }
                    }
                    continue;
                }

                if (!in_array(trim($value), array('.', ',', ''))) {
                    // Create new transaction field value
                    $transactionFieldValue = new TransactionFieldValues();
                    $transactionFieldValue->transaction_field_id = $matches[1];
                    $transactionFieldValue->transaction_id = $componentModel->id;
                    if (strlen($value) > 1000) {
                        $transactionFieldValue->value = substr($value, 0, 1000);

                        // add to error log so user can be aware
                        $errorData = 'Notification: Transaction field value was greater than 1000 characters and it was saved but cut off at 1000 characters. Please review value for full data.';
                        $errors[] = $errorData;
//                        $importErrorRows[] = $errorData;

                    } else {
                        $transactionFieldValue->value = trim($value);
                    }

                    // save transaction field values
                    if (!$transactionFieldValue->save()) {
                        $errorData = 'Error: Transaction field value did not save. Error Message: ' . print_r($transactionFieldValue->getErrors(), true);
                        $errors[] = $errorData;
//                        $importErrorRows[] = $errorData;
                    }
                }
            }
        }

        if(!empty($transactionFieldValuesUpdated)) {
            $activityLog = new ActivityLog();
            $activityLog->account_id = Yii::app()->user->accountId;
            $activityLog->added = date("Y-m-d H:i:s", time());
            $activityLog->activity_date = date("Y-m-d H:i:s", time());
            $activityLog->component_type_id = $componentTypeId;
            $activityLog->note = "The following fields were updated:\n".$transactionFieldValuesUpdated;
            $activityLog->task_type_id = TaskTypes::IMPORTED;
            //$activityLog->origin_type = Transactions::ORIGIN_TYPE_GENERAL_IMPORT;

            if (!$activityLog->save()) {}
        }

        return array($componentModel, $errors);
    }

    public static function saveClosingsModel($componentTypeId, $newRecord, $contactId, &$importErrorRows, $tnxId)
    {
        $componentModel = null;
        $tnxModel = Transactions::model()->findByPk($tnxId);
        if (!$tnxModel) {
            $errorData = 'Transaction ID: ' . $tnxId . ' not found. This record has not been imported.';
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
            return array($componentModel, $errors);
        }

        if ($tnxModel->component_type_id != $componentTypeId) {
            $errorData = 'Closing Transaction Type does not match provided ComponentTypeId: ' . $componentTypeId . '. This record has not been imported.';
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
            return array($componentModel, $errors);
        }

        // format closing fields
        $newRecord['price'] = Yii::app()->format->formatInteger($newRecord['price']);
        if (!$newRecord['price']) {
            $errorData = 'Closing Price missing. This record has not been imported.';
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
            return array($componentModel, $errors);
        }

        $newRecord['commission_amount'] = Yii::app()->format->formatDecimal($newRecord['commission_amount']);
        if (!$newRecord['commission_amount']) {
            $errorData = 'Closing Commission Amount missing. This record has not been imported.';
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
            return array($componentModel, $errors);
        }

        if (!$newRecord['contract_execute_date'] || $newRecord['contract_execute_date'] == 0) {
            $errorData = 'Closing Contract Execute Date missing. This record has not been imported.';
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
            return array($componentModel, $errors);
        }
        $newRecord['contract_execute_date'] = Yii::app()->format->formatDate($newRecord['contract_execute_date'], StmFormatter::MYSQL_DATE_FORMAT);

        if (!$newRecord['contract_close_date'] || $newRecord['contract_close_date'] == 0) {
            $errorData = 'Closing Contract Closing Date missing. This record has not been imported.';
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
            return array($componentModel, $errors);
        }
        $newRecord['contract_close_date'] = Yii::app()->format->formatDate($newRecord['contract_close_date'], StmFormatter::MYSQL_DATE_FORMAT);

        // check for other closings for this contact and compare closing date, side and price for dupe.
        $duplicateClosing = Yii::app()->db->createCommand()
            ->select('cl.*')
            ->from('closings cl')
            ->join('transactions t', 't.id=cl.transaction_id')
            ->join('contacts c', 'c.id=t.contact_id')
            ->where('c.id=' . $contactId)
            ->andWhere('cl.price=' . $newRecord['price'])
            ->andWhere('t.is_deleted=0 AND c.is_deleted=0')
            ->andWhere('t.component_type_id=' . $componentTypeId)
            ->andWhere('cl.contract_closing_date="' . $newRecord['contract_close_date'] . '"')
            ->queryAll();

        // if dupe, log error and skip
        if ($duplicateClosing) {
            $errorData = 'Duplicate closing detected that has the same price, side and closing date. This record has not been imported.';
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
            return array($componentModel, $errors);
        }

        $closingStatusId = (in_array($newRecord['closing_status_ma'], array(Closings::STATUS_TYPE_CLOSED_ID, Closings::STATUS_TYPE_ACTIVE_ID))) ? $newRecord['closing_status_ma'] : Closings::STATUS_TYPE_CLOSED_ID;

        // create/save closing
        $closing = new Closings();
        $closing->sendCongratsEmail = false;
        $closing->setAttributes(array(
            'transaction_id' => $tnxModel->id,
            'closing_status_ma' => $closingStatusId,
            'price' => Yii::app()->format->formatInteger($newRecord['price']),
            'commission_amount' => Yii::app()->format->formatDecimal($newRecord['commission_amount']),
            'contract_execute_date' => date('Y-m-d', strtotime($newRecord['contract_execute_date'])),
            'contract_closing_date' => date('Y-m-d', strtotime($newRecord['contract_close_date'])),
            'actual_closing_datetime' => date('Y-m-d', strtotime($newRecord['contract_close_date'])) . ' 00:00:00',
            'sale_type_ma' => Closings:: SALE_TYPE_NORMAL_ID, //@todo: can import field map later
            'financing_type_ma' => Closings::FINANCING_TYPE_CONVENTIONAL_ID, //@todo: can import field map later
            'is_referral' => 0, //@todo: can import field map later
        ));
        if (!$closing->save()) {
            $errorData = 'Error: Closing did not save. Error Message: ' . print_r($closing->getErrors(), true);
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
        } else {
            // update tnx record to closed. //@todo: handle PENDING status also
            $tnxModel->transaction_status_id = TransactionStatus::CLOSED_ID;
            if (!$tnxModel->save()) {
                $errorData = 'Error: Transaction status not updated to match Closing record. Please perform manually. Error Message: ' . print_r($tnxModel->getErrors(), true);
                $errors[] = $errorData;
//                $importErrorRows[] = $errorData;
            }

            $componentModel = $closing;
        }
        return array($componentModel, $errors);
    }

    public static function saveRecruitsModel($newRecord, $contactId, &$importErrorRows)
    {
        //throw new Exception(__CLASS__.' (:'.__LINE__.') Recruits Import model save not enabled.');
        $fields = array(
            'annual_units',
            'annual_volume',
            'annual_volume_change',
            'annual_volume_change_percent',
            'ranking',
            'status_id',
            'current_brokerage',
            'office_address',
            'office_city',
            'office_zip',
            'brokerage_mls_id',
            'brokerage_license_number',
            'agent_mls_id',
            'agent_license_number',
            'licensed_date',
            'license_exam_approval_date',
            'website',
            'met_status_ma',
            'recruit_notes',
        );

        $agentProductionFields = array(
            'annual_units',
            'annual_volume',
            'annual_volume_change',
            'annual_volume_change_percent',
            'ranking',
            'current_brokerage',
            'office_city',
            'office_zip',
        );

        $dateFields = array(
//            'annual_data_updated',
            'licensed_date',
        );

        // search for existing recruits model
        if ($recruit = Recruits::model()->findByAttributes(array('contact_id' => $contactId))) {
            foreach ($fields as $field) {
                // if $field has value assign to existing model column/attribute
                if ($newRecord[$field] !== '' && $newRecord[$field] !== null && $field !== 'recruit_notes') {

                    // if it's empty just assign value
                    if ($recruit->{$field} == '' || $recruit->{$field} == null) {
                        $recruit->{$field} = $newRecord[$field];
                    } // if value already exists, compare if same. If different, append unless date field.
                    else {

                        // assign to variables for model and import value in order to preserve $newRecord data for error logging output
                        // if it's date type field, convert to same format to compare
                        $isDateField = false;
                        if (in_array($field, $dateFields)) {
                            $isDateField = true;
                            $modelValue = Yii::app()->format->formatDate($recruit->{$field}, StmFormatter::MYSQL_DATE_FORMAT);
                            $importValue = Yii::app()->format->formatDate($newRecord[$field], StmFormatter::MYSQL_DATE_FORMAT);
                        } else {
                            $modelValue = $recruit->{$field};
                            $importValue = $newRecord[$field];
                        }

                        if ($modelValue !== $importValue) {

                            // if date field, replace date.
                            if ($isDateField) {
                                $recruit->{$field} = $newRecord[$field];
                            } // if not date field and not equal, append.
                            else {
                                // check to see that the entirety of the string to append hasn't already been appended from a previous import
                                if (strpos($recruit->{$field}, $newRecord[$field]) === false) {
                                    if ($field == 'recruit_notes') {
                                        $recruit->notes .= trim($newRecord[$field]);
                                    } elseif ($field == 'status_id' && is_numeric($newRecord[$field])) {

                                        // load valid status fields if not already loaded
                                        if (!isset(self::$_validStatusIds[ComponentTypes::RECRUITS])) {
                                            self::$_validStatusIds[ComponentTypes::RECRUITS] = Yii::app()->db->createCommand()
                                                ->select('transaction_status_id')
                                                ->from('transaction_status_account_lu')
                                                ->where('component_type_id=' . ComponentTypes::RECRUITS . ' AND account_id=' . Yii::app()->user->accountId)
                                                ->queryColumn();
                                        }

                                        // check to see if legit status
                                        if (in_array($newRecord[$field], self::$_validStatusIds[ComponentTypes::RECRUITS])) {
                                            $recruit->{$field} = $newRecord[$field];
                                        } else {
                                            // outputting error but letting the rest of the process continue as it will default to New Lead status
                                            $errorData = 'Error: Recruit status #' . $newRecord[$field] . ' is invalid. Lead status will not be updated. Manually update status as needed.';
                                            $errors[] = $errorData;
                                        }
                                    } else {
                                        $recruit->{$field} = $newRecord[$field];
                                    }
                                }
                            }
                        }

                        // if it's an agent production field, update the last_updated field to current datetime
                        if (in_array($field, $agentProductionFields)) {
                            $recruit->annual_data_updated = new CDbExpression('NOW()');
                        }
                    }
                } elseif ($field === 'recruit_notes' && $newRecord[$field] !== '' && $newRecord[$field] !== null) {
                    $recruit->notes = (($recruit->notes) ? PHP_EOL : '') . trim($newRecord['recruit_notes']);
                }
            }
        } else {

            $recruit = new Recruits();
            $recruit->contact_id = $contactId;
            $recruit->source_id = StmImportFunctions::getSourceId($newRecord);
            if (isset($newRecord['account_id'])) {
                $recruit->account_id = $newRecord['account_id'];
            }
            $recruit->status_id = 1;

            // if $field has value assign to model column/attribute
            foreach ($fields as $field) {

                if ($field !== 'recruit_notes' && $newRecord[$field] !== '' && $newRecord[$field] !== null) {

                    // if it's a date field format it, else use raw values
                    $recruit->{$field} = (in_array($field, $dateFields)) ? Yii::app()->format->formatDate($recruit->{$field}, StmFormatter::MYSQL_DATE_FORMAT) : $newRecord[$field];
                } elseif ($field == 'status_id' && is_numeric($newRecord[$field])) {

                    // load valid status fields if not already loaded
                    if (!isset(self::$_validStatusIds[ComponentTypes::RECRUITS])) {
                        self::$_validStatusIds[ComponentTypes::RECRUITS] = Yii::app()->db->createCommand()
                            ->select('transaction_status_id')
                            ->from('transaction_status_account_lu')
                            ->where('component_type_id=' . ComponentTypes::RECRUITS . ' AND account_id=' . Yii::app()->user->accountId)
                            ->queryColumn();
                    }

                    // check to see if legit status
                    if (in_array($newRecord[$field], self::$_validStatusIds[ComponentTypes::RECRUITS])) {
                        $recruit->{$field} = $newRecord[$field];
                    } else {
                        // outputting error but letting the rest of the process continue as it will default to New Lead status
                        $errorData = 'Error: Recruit status #' . $newRecord[$field] . ' is invalid. Lead status will not be updated. Manually update status as needed.';
                        $errors[] = $errorData;
                    }
                } elseif ($field == 'recruit_notes') {
                    $recruit->notes = trim($newRecord[$field]);
                }

                // if it's an agent production field, update the last_updated field to current datetime
                if (in_array($field, $agentProductionFields) && !is_object($recruit->annual_data_updated)) {
                    $recruit->annual_data_updated = new CDbExpression('NOW()');
                }
            }
        }

        // save recruits model
        if (!$recruit->save()) {
            $errorData = 'Error: Recruit model not saved. Please perform manually. Error Message: ' . print_r($recruit->getErrors(), true);
            $errors[] = $errorData;
//            $importErrorRows[] = $errorData;
        } // process tags if recruit model saved successfully
        elseif (!empty(trim($newRecord['tags'])) || strlen(trim($newRecord['tags'])) > 0) {

            $tags = array();
            if (strpos($newRecord['tags'], ',') !== false) {
                $tags = explode(',', $newRecord['tags']);
            } elseif (!is_array($newRecord['tags'])) {
                $tags = array($newRecord['tags']);
            }

            $results = Yii::app()->db->createCommand("SELECT id, LOWER(TRIM(name)) name FROM recruit_types where is_deleted=0")->queryAll();
            $allTags = array();
            foreach ($results as $row) {
                $allTags[$row['name']] = $row['id'];
            }

            // see if tags already exist in system and then exists for this recruit.
            foreach ($tags as $tag) {

                // if tag already exists in system
                if (isset($allTags[strtolower(trim($tag))])) {

                    // see if this recruit already has lookup for this, if not create one
                    $recruitType = new RecruitTypes();
                    if (!($recruitTypeLu = RecruitTypeLu::model()->findByAttributes(array('recruit_id' => $recruit->id, 'recruit_type_id' => $allTags[strtolower(trim($tag))])))) {

                        $recruitTypeLu = new RecruitTypeLu();
                        $recruitTypeLu->setAttributes(array(
                            'recruit_id' => $recruit->id,
                            'recruit_type_id' => $allTags[strtolower(trim($tag))],
                        ));
                        if (!$recruitTypeLu->save()) {
                            // log error
                            $errorData = 'Error: Recruit Type Lookup model not saved. Please perform manually. Error Message: ' . print_r($recruitTypeLu->getErrors(), true);
                            $errors[] = $errorData;
//                            $importErrorRows[] = $errorData;
                            continue;
                        }
                    }
                } else {
                    //create new recruit tag
                    $recruitType = new RecruitTypes();
                    $recruitType->name = $tag;
                    if (!$recruitType->save()) {
                        // log error
                        $errorData = 'Error: Recruit Type model not saved. Please perform manually. Error Message: ' . print_r($recruitType->getErrors(), true);
                        $errors[] = $errorData;
//                        $importErrorRows[] = $errorData;
                        continue;
                    } else {
                        $allTags[trim(strtolower($tag))] = $recruitType->id;
                    }

                    // add tag to lookup for this recruit
                    $recruitTypeLu = new RecruitTypeLu();
                    $recruitTypeLu->setAttributes(array(
                        'recruit_id' => $recruit->id,
                        'recruit_type_id' => $recruitType->id,
                    ));
                    if (!$recruitTypeLu->save()) {
                        // log error
                        $errorData = 'Error: Recruit Type lookup model not saved. Please perform manually. Error Message: ' . print_r($recruitTypeLu->getErrors(), true);
                        $errors[] = $errorData;
//                        $importErrorRows[] = $errorData;
                        continue;
                    }
                }
            }
        }

        return array($recruit, $errors);
    }

    public static function contactIdMatchesToString($contactIdMatches)
    {
        $string = '';
        if (!is_array($contactIdMatches)) {
            return $string;
        }

        // ex format: [phone=>1234,5678], [email=>1234,5678]
        foreach ($contactIdMatches as $field => $values) {
            $string .= ($string) ? ', ' : '';

            //check to see if element is array also, this is the max depth of array
            if (is_array(current($values))) {
                $valuesString = '';
                // this array level is specific for address
                foreach ($values as $matchType => $value) {
                    $valuesString .= ($valuesString) ? ', ' : '';
                    $valuesString .= $matchType . ':' . implode(',', $value);
                }
                $string .= "[$field=>" . $valuesString . "]";
            } else {
                $string .= "[$field=>" . implode(',', $values) . "]";
            }
        }
        return $string;
    }

    // @TODO: move these to the top once testing is done. Here for convenience
    protected static $importErrorRows = [];
    protected static $rowNumber = 0;
    public static $allowedDupeNames = array('fsbo ad', 'property owner', 'no name', 'corporate owned', 'possible owner');
    protected static $duplicateCount;
    public static $importType = "UNKNOWN";

    protected static function _writeLog($data)
    {
        $fp = fopen('/tmp/importLog-' . self::$importType . '_' . time(), 'w');
        @fwrite($fp, print_r($data, 1));
        fclose($fp);

        return true;
    }

    public static function getImportErrorRows()
    {
        return self::$importErrorRows;
    }

    public static function logError($errorMessage, $newContact)
    {
        $newContact['errorDesc'] = $errorMessage;

        // if there is already content in here, add a space or divider between error logs.
        if (self::$importErrorRows[self::$rowNumber]) {
            // append to the existing error description
            self::$importErrorRows[self::$rowNumber]['errorDesc'] .= PHP_EOL . $errorMessage;
        } else {
            self::$importErrorRows[self::$rowNumber] = $newContact;
        }
    }

    public static function doImport($rows, $componentTypeId, $callListId = NULL, $sourceId = NULL)
    {
        if (!YII_DEBUG) {
            set_time_limit(300);
        }

        // loads initial data to compare against later
        list($stateFullNames, $stateShortNames, $emailsExisting, $namesExisting, $phonesExisting, $addressesExisting, $lastNamesExisting) = StmImportFunctions::initialData();

        // Iterate over all the results
        foreach ($rows as $rowNumber => $newContact) {
            self::$rowNumber = $rowNumber;

            //@todo: how to handle if Command and doesn't have value for Yii::app()->user->accountId ??????
            $account_id = ((!isset($newContact['account_id']) || empty($newContact['account_id'])) ? Yii::app()->user->accountId : $newContact['account_id']);

            if ($newContact['first_name'] == 'First Name' || $newContact['last_name'] == 'Last Name') {
                self::logError('This row was detected as a header row.', $newContact);
                continue;
            }

            $contactIdMatches = null;
            $matchErrorLogs = null;
            $conclusiveContactId = null;
            $contactModel = null;

            // Parse out full name if we specified that option, then merge it in for the current iteration. Do this first as it skips if invalid.
            if (isset($newContact['full_name'])) {
                if (!class_exists('\StmDialerImport\Parser')) {
                    require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmDialerImport') . "/Parser.php");
                }

                $parsedName = \StmDialerImport\Parser::fullName($newContact['full_name']);
                if ($parsedName) {
                    $newContact = array_merge($newContact, $parsedName);
                } else {
                    self::logError('Name Format Alert! Unrecognized Full Name format. Review data and re-import as needed.', $newContact);
                    continue;
                }
            }

            // trim and lowercase emails for accurate comparison
            StmImportFunctions::formatEmails($newContact);

            // remove any non-numeric values from phone fields
            StmImportFunctions::formatPhones($newContact);

            // verify there is at least one phone number to import to dialer
            if (!StmImportFunctions::validateMinPhoneExists($newContact)) {
                self::logError('No valid phones numbers. Review data re-import if needed. Skipping this contact record.', $newContact);
            }

            // gets all contactId matches based on email, phone, address, name
            list($contactIdMatches, $matchErrorLogs) = StmImportFunctions::getContactIdMatches($newContact, $emailsExisting, $phonesExisting, $addressesExisting, $namesExisting, $lastNamesExisting, self::$allowedDupeNames);

            // review all matches from all fields in contactId matches - name, email, phone. If narrowed down to ONE contact, proceed with that contact. If lastName is the only match, treat as non-match as it is not enough to be conclusive on anything. It is only there to be used in combination with other matching fields
            if (!empty($contactIdMatches) && !(count($contactIdMatches) == 1 && isset($contactIdMatches['lastName']))) {

                // remove address match condition for: fullName="Fsbo Ad -" and address="-". Prevents FSBO being erroneously matched as same contact.
                if (trim(strtolower($newContact['first_name'])) == 'fsbo ad' && $newContact['address'] == '-') {
                    unset($contactIdMatches['address']);
                }

                // see if there is a contact ID common to all emails, phones, address
                if (count($contactIdMatches) > 1) {
                    // get conclusive contactId if available
                    $conclusiveContactId = StmImportFunctions::getConclusiveContactId($contactIdMatches);

                    if ($conclusiveContactId && count($conclusiveContactId) == 1) {
                        $contactModel = Contacts::model()->findByPk($conclusiveContactId);
                    } else {
                        unset($contactIdMatches['lastName']);
                        self::logError('Existing record Match Alert (A): ' . StmImportFunctions::contactIdMatchesToString($matchErrorLogs) . ' Contact ID Matches: ' . StmImportFunctions::contactIdMatchesToString($contactIdMatches), $newContact);
                        self::$duplicateCount++;
                        continue;
                    }
                } // only one field has contactIdMatch
                else {
                    // single email match is acceptable for contact match
                    if (isset($contactIdMatches['emails']) && count($contactIdMatches['emails']) == 1) {
                        // this was already done above
                        $contactModel = Contacts::model()->findByPk($contactIdMatches['emails'][0]);
                    } else {

                        unset($contactIdMatches['lastName']);
                        // there is no other contactId matches to cross reference, skip and let user review and re-import.
                        self::logError('Existing record Match Alert (B): ' . StmImportFunctions::contactIdMatchesToString($matchErrorLogs) . ' Contact ID Matches: ' . StmImportFunctions::contactIdMatchesToString($contactIdMatches), $newContact);
                        self::$duplicateCount++;
                        continue;
                    }
                }
            }

            if (!empty($sourceId)) {
                $newContact['sourceId'] = $sourceId;
            }

            // if a contact was determined using the $newContact row data and cross referencing email, phone, address, name
            if ($contactModel) {
                //merge contact for this import
                $contactModel = StmImportFunctions::mergeContactInfo($contactModel->id, $account_id, $newContact, 5, 5, $emailsExisting, $phonesExisting, self::$importErrorRows, self::$allowedDupeNames);
            } // this means there were no potential matches and contact needs to be created
            else {
                //create new contact for this import - add all emails, phones, address
                if (!($contactModel = StmImportFunctions::addNewContact($newContact, $namesExisting, $lastNamesExisting, $emailsExisting, $phonesExisting, self::$importErrorRows, 'addWithoutLastLogin', self::$allowedDupeNames))) {
                    // errors have been logged, skip record.
                    continue;
                }
            }

            // save address
            if ($newContact['address']) {

                list($addressModel, $addressErrors) = StmImportFunctions::saveAddressFields($newContact, $contactModel->id, $account_id, $addressesExisting, $stateFullNames, $stateShortNames, self::$importErrorRows, false);

                if (!empty($addressErrors)) {
                    self::logError('Address save had errors. Review data and re-import as needed.' . print_r($addressErrors, true), $newContact);
                }
            }

            // add necessary component - transaction or recruit
            if (in_array($componentTypeId, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::CLOSINGS, ComponentTypes::RECRUITS))) {

                $addressId = ($addressModel) ? $addressModel->id : null;
                // see if there is a component model exists
                list($componentModelResult, $componentModelErrors) = StmImportFunctions::saveComponentModel($componentTypeId, $newContact, $contactModel->id, self::$importErrorRows, Transactions::ORIGIN_TYPE_GENERAL_IMPORT, $addressId);

                if (!empty($componentModelErrors)) {
                    // if there's only 1 error, print that in non array format
                    self::logError(((count($componentModelErrors) == 1) ? $componentModelErrors[0] : print_r($componentModelErrors, true)), $newContact);
                }
            }

            if (!empty($sourceId) && !empty($callListId)) {

                // Find existing call list
                $callList = CallLists::model()->findByAttributes(array(
                    'id' => $callListId
                ));

                if (!$callList) {
                    self::logError('Error: Unable to load call list. Skipping creation of Dialer List.', $newContact);
                    continue;
                }

                StmImportFunctions::addContactToDialerList($contactModel, $callListId, $newContact, self::$importErrorRows);
            }
        }

        if (YII_DEBUG) {
            self::_writeLog(self::$importErrorRows);
        }

        return true;
    }
}