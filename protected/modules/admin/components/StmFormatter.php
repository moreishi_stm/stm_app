<?php

	/**
	 * StmFormatter
	 * Provides common formatters
	 *
	 * @package default
	 */
	class StmFormatter extends CFormatter {

		/**
		 * A default value to use throughout the application
		 */
		const DEFAULT_VALUE = '';
		const MYSQL_DATE_FORMAT = 'Y-m-d';
		const MYSQL_DATETIME_FORMAT = 'Y-m-d H:i:s';
		const MYSQL_TIME_FORMAT = 'G:i:s';
		const APP_DATE_FORMAT = 'm/d/Y';
		const APP_DATE_PICKER_FORMAT = 'm/d/Y';
		const APP_TIME_FORMAT = 'h:i a';
		const APP_DATETIME_PICKER_FORMAT = 'm/d/Y g:i a';

		public static function formatInteger($number) {
			//Essentially remove any characters which are non-digits and return the remaining characters
			if ($number) {

                // this removes any decimal places
                if(is_numeric($number) && strpos($number, '.') != false) {
                    $number = round($number);
                }

                $isNegative = (strpos($number, '-') === 0) ? true: false;
                //@todo: need to detect negative symbol vs. phone dash via regex
				$number = preg_replace('/\D/', '', $number);

                $number = ($isNegative) ? -$number : $number;
			} else {
				$number = self::DEFAULT_VALUE;
			}

			return $number;
		}

		public function formatDecimal($number) {
			//Essentially remove any characters which are non-digits and return the remaining characters
			if ($number) {
				$number = preg_replace('/[^0-9\.\-]+/', '', $number);
			} else {
				$number = self::DEFAULT_VALUE;
			}

			return $number;
		}

		/**
		 * Remove the plural from a word
		 *
		 * @param $string
		 *
		 * @return mixed
		 */
		public function formatSingular($string) {
			$string = preg_replace('/s$/', '', $string);

			return $string;
		}

		/**
		 * Pluralize a word
		 *
		 * @author Chris Willard <chriswillard.dev@gmail.com>
		 *
		 * @param      $string
		 * @param null $countOfStringItems (optional) determines if we need to pluralize based on a integer
		 * @param null $pluralString       (optional) manually entered plural string (foot|feet)
		 *
		 * @return null|string
		 */
		public function formatPluralize($string, $countOfStringItems = null, $pluralString = null) {
			// If there is a specific count, and the count is less than one return singular
			if (is_integer($countOfStringItems) && $countOfStringItems <= 1) {
				return $this->formatSingular($string);
			}

			// Return the string with a 's' or the manually entered plural string
			return ($pluralString === null) ? $string . 's' : $pluralString;
		}

		/**
		 * formatDomain
		 * Strip the www. for use in search the domains model
		 *
		 * @param string $domain
		 *
		 * @return string
		 */
		public function formatDomain($domain) {
			return str_replace('www.', '', $domain);
		}

		/**
		 * formatGridValue
		 * Prevents a row of data contained with a cell of a GridView widget from displaying if there is no value
		 *
		 * @param  string $label  Label associated with a value contained with a cell of a GridView widget
		 * @param  string $value  Value to display
		 * @param bool    $strong true if the label should be displayed in strong format, false otherwise.
		 *
		 * @return string
		 */
		public function formatGridValue($label, $value = null, $strong = true) {
			if (!$value) {
				return null;
			}

			if ($strong == true) {
				return '<strong>' . $label . '</strong> ' . $value;
			} else {
				return $label . ' ' . $value;
			}
		}

		/**
		 * Formats phone # to (999) 555-1212 Ext.1234
		 *
		 * @param      $rawPhone     Phone # data usually 10 digit no spaces from DB
		 * @param null $options      Options variables
		 *
		 * @return string       Can return or print string before return depending on option
		 */
		public function formatPhone($rawPhone, $options = null) {

            $rawPhone = preg_replace( '/[^0-9]/', '', $rawPhone );

			if (!empty($rawPhone)) {
				if (strlen($rawPhone) == 10) {
					$phone = '(' . substr($rawPhone, 0, 3) . ') ' . substr($rawPhone, 3, 3) . '-' . substr($rawPhone, 6, 4);
				} elseif(strlen($rawPhone) == 11) {
					$phone = substr($rawPhone, 0, 1).' (' . substr($rawPhone, 1, 3) . ') ' . substr($rawPhone, 4, 3) . '-' . substr($rawPhone, 7, 4);
				} elseif (strlen($rawPhone) == 7) {
					$phone = substr($rawPhone, 0, 3) . '-' . substr($rawPhone, 3, 4);
				}

				if ($options['extension']) {
					$phone .= ' Ext. ' . $options['extension'];
				}
			}

            return $phone;
		}

		/**
		 * Strips common items from a phone leaving (999) 555-1212 to 9995551212
		 *
		 * @param  string $phoneString String containing a formatted phone number
		 *
		 * @return string Stripped down phone number
		 */
		public function formatStripPhone($phoneString) {
			return str_replace(array(
					'(',
					')',
					'-',
					' ',
					'.',
					'_',
				), '', $phoneString
			);
		}

		public function formatDateTimeDays($timeString, $opt = null) {
			if (!$timeString) {
				if ($opt['default']) {
					return $opt['default'];
				} else {
					return self::DEFAULT_VALUE;
				}
			}

			$optDateFormat = ($opt['dateFormat']) ? $opt['dateFormat'] : self::APP_DATE_FORMAT;
			$optTimeFormat = ($opt['timeFormat']) ? $opt['timeFormat'] : self::APP_TIME_FORMAT;

			$dateFormat = $this->formatDate($timeString, $optDateFormat);
			$timeFormat = $this->formatTime($timeString, $optTimeFormat);
			$daysFormat = $this->formatDays($timeString, $opt = array('minIfZeroDays' => true));

			$dateTimeDaysFormat = $dateFormat;
			$dateTimeDaysFormat .= ' ' . $timeFormat;
			if ($opt['break'] == true) {
				$dateTimeDaysFormat .= '<br />';
			} else {
				$dateTimeDaysFormat .= ' ';
			}
			$dateTimeDaysFormat .= '(' . $daysFormat . ')';

			return $dateTimeDaysFormat;
		}

		/**
		 * formatDateTime Calculates days since and time am/pm, primarily used for Last Login & Submit datetime
		 *
		 * @param mixed $timeString
		 * @param null  $opt
		 *
		 * @return string $loginFormat
		 */
		public function formatDateTime($timeString, $opt = null) {
			if (!$timeString || $timeString < 1) {
				return self::DEFAULT_VALUE;
			}

			$optDateFormat = ($opt['dateFormat']) ? $opt['dateFormat'] : self::APP_DATE_FORMAT;
			$optTimeFormat = ($opt['timeFormat']) ? $opt['timeFormat'] : self::APP_TIME_FORMAT;
			$dateFormat = $this->formatDate($timeString, $optDateFormat);

			$timeFormat = $this->formatTime($timeString, $optTimeFormat);
			$dateTimeFormat = $dateFormat;

			if ($opt['break']) {
				$dateTimeFormat .= '<br />';
			}

			if ($opt['mySqlFormat'] == true) {
				$dateTimeFormat .= ' ' . $timeFormat;
			} else {
				$dateTimeFormat .= ' (' . $timeFormat . ')';
			}

			return $dateTimeFormat;
		}

		/**
		 * formatDateDays Calculates days since and time am/pm, primarily used for Last Login & Submit datetime
		 *
		 * @param      $timeString
		 * @param null $opt
		 *
		 * @internal param bool $break
		 * @return string
		 */
		public function formatDateDays($timeString, $opt = null) {

            if(!$timeString) {
                return;
            }
			$currentDateTs = time();
			$comparedTs = strtotime($timeString);
			$dayLen = 60 * 60 * 24;
			$dayDifference = floor(($currentDateTs - $comparedTs) / $dayLen);
			$dayDifference = ($dayDifference <= 0) ? 0 : $dayDifference;

			if ($dayDifference === 0) {
				$dayDifferenceString = 'Today';
			} elseif ($dayDifference > 365) {
				$years = floor($dayDifference / 365);
				$yearsLabel = ($years > 1) ? 'Years' : 'Year';
				$days = ($dayDifference - $years * 365) % 365;
				$dayDifferenceString = $years . ' ' . $yearsLabel . ' ' . $days . ' Days';
			} else {
                $dayDifferenceString = $dayDifference.' '.$this->formatPluralize('Day', (int) $dayDifference);
			}

			$dayDifferenceString = CHtml::tag('strong', $htmlOptions = array(), $dayDifferenceString);

            if($opt['includeTime'] !== false) {
                $dayString = $this->formatDateTime($timeString);
            }
            else {
                $dayString = $this->formatDate($timeString);
            }

			$dateDayFormat = $dayDifferenceString . ' ' . $dayString;

			return $dateDayFormat;
		}

		/**
		 * formatDaysTime Calculates days since and time am/pm, primarily used for Last Login & Submit datetime
		 *
		 * @param  string $timeString [description]
		 * @param null    $opt
		 *
		 * @return string $loginFormat
		 */
		public function formatDaysTime($timeString, $opt = null) {
			$daysFormat = $this->formatDays($timeString, $opt);
			$timeFormat = $this->formatTime($timeString);

			if ($opt['break'] == true) {
				$daysFormat .= '<br />';
			}

			$loginFormat = $daysFormat . ' (' . $timeFormat . ')';

			return $loginFormat;
		}

		public function formatTime($timeString, $timeFormat = self::APP_TIME_FORMAT) {
			if (strpos($timeString, ':')) {
				$time = strtotime($timeString);
			}

			return date($timeFormat, $time);
		}

		/**
		 * formatDate formats date as specified in $dateFormat
		 *
		 * @param  string $dateFormat specifieds desired format
		 * @param  string $timeString string in English datetime format
		 *
		 * @return string fully formated date string
		 */
		public function formatDate($timeString, $dateFormat = self::APP_DATE_FORMAT) {
			if ($timeString && $timeString > 0) {
				return date($dateFormat, strtotime($timeString));
			} else {
				return null;
			}
		}


        /**
		 * formatDays Calculates # of days from provided date and returns
		 * it in a string "5 days"
		 *
		 * @param      $timeString
		 * @param null $opts
		 *
		 * @internal param bool $printdays
		 * @internal param string $daysLabel
		 * @return float|string $daysFormat ex: "5 days"
		 */
		public function formatDays($timeString, $opts = null) {
			$defaultValue = (isset($opts['defaultValue'])) ? $opts['defaultValue'] : null;
			// minIfZeroDays
			if (!$timeString) {
				return $defaultValue;
			}

			$daysLabel = (isset($opts['daysLabel'])) ? $opts['daysLabel'] : 'day';
			$printDays = (isset($opts['printDays'])) ? $opts['printDays'] : true;
			$now = time();

			if (strpos($timeString, '-')) {
				$time = strtotime($timeString);
			}

			$dateDelta = $now - $time;
//			$dateDelta = ($dateDelta <= 0) ? 0 : $dateDelta;

			if(isset($opts['isFuture']) && $opts['isFuture']==true) {   //&& $dateDelta!=0
				$dateDelta *= -1;
			}

			if (isset($opts['nearestMinHourDays']) && $opts['nearestMinHourDays'] == true) {
				if ($dateDelta >= 86400) { // 1 days
					$daysFormat = floor($dateDelta / (60 * 60 * 24));
					if ($daysFormat == 1) {
						$daysLabel = ($opts['daysLabel'] != null) ? $opts['daysLabel'] : 'day';
					}elseif($daysFormat >1) {
                        $daysLabel = $this->formatPluralize($daysLabel, $dateDelta);
                    }
				} elseif ($dateDelta >= 3600) { // Hours
					$daysFormat = floor($dateDelta / (60 * 60)); // Hours
                    $daysLabel = ($opts['daysLabel'] != null) ? $opts['daysLabel'] : 'hour';
                    if($daysFormat >1 && $opts['daysLabel'] == null){
                        $daysLabel = $this->formatPluralize($daysLabel, $dateDelta);
                    }
				} elseif ($dateDelta < 3600) { // Minutes
					$daysFormat = floor($dateDelta / (60)); // Minutes
                    $daysLabel = ($opts['daysLabel'] != null) ? $opts['daysLabel'] : 'minute';
                    if($daysFormat >1 && $opts['daysLabel'] == null){
                        $daysLabel = $this->formatPluralize($daysLabel, $dateDelta);
                    }
				}
			} else {
				$daysFormat = ceil($dateDelta / (60 * 60 * 24));
				if($daysFormat == -0) {
					$daysFormat = '';
					$daysLabel = 'Today';
                    return $daysLabel;

				} elseif ($daysFormat == 1) { //$daysLabel = 'Today';
					$daysLabel = ($daysLabel != null) ? $daysLabel : 'day';
                    if($daysLabel == 'day') {
                        return '1 day';
                    }
				} elseif ($daysFormat > 1 && ((isset($opts['daysLabel'])) && $opts['daysLabel'] == null) || $daysLabel=='day') {
					$daysLabel = $this->formatPluralize($daysLabel, $dateDelta);
				}
			}

			$daysConcat = (isset($opts['daysLabelConcat'])) ? ' '.$opts['daysLabelConcat'] : '';

			if ($printDays === true) {
				$daysFormat .= ' ' . $daysLabel . $daysConcat;
			}

			return $daysFormat;
		}

        /**
         * formatDaysDiff calculates the difference in 2 dates
         *
         * @param      $dollars
         * @param null $defaultValue
         *
         * @return null|string
         */
        public function formatDaysDiff($fromDate, $toDate, $opts=null) {
            $fromDate = date('Y-m-d', strtotime($fromDate));
            $toDate = date('Y-m-d', strtotime($toDate));

            $fromTime = strtotime($fromDate);
            $toTime = strtotime($toDate);

            $dateDiff = round(($toTime - $fromTime)/3600/24);
            return $dateDiff;
        }

		/**
		 * formatDollars Puts '$' sign and number_formats
		 *
		 * @param      $dollars
		 * @param null $defaultValue
		 *
		 * @return null|string
		 */
		public function formatDollars($dollars, $defaultValue = null) {
			if (!$dollars) {
				return (isset($defaultValue)) ? $defaultValue : self::DEFAULT_VALUE;
			}

			$dollarsFormat = '$' . Yii::app()->format->formatNumber(floatval($dollars));

			return $dollarsFormat;
		}

		public function formatPercentages($number, $opts=null) {
			if(!$opts['symbol'])
				$opts['symbol'] = '%';

			if(!$opts['decimalPlaces'] && $opts['decimalPlaces'] !=0)
				$opts['decimalPlaces'] = 1;

			return number_format(floatval($number*100), $opts['decimalPlaces']).$opts['symbol'];
		}


		/**
		 * formatToArray if not an array, puts it into an array and returns value in array. Used for passing variable to scope.
		 *
		 * @param  mixed  $value
		 * @return array
		 */
		public static function formatToArray($value) {
			if(!is_array($value)) {
				return array($value);
			} else {
				return $value;
			}
		}

		/**
		 * Return a list of emails formatted such as [email]<br />
		 *
		 * @param        $Emails               Collection of Emails models.
		 * @param string $delimiter
		 * @param string $defaultValue
		 *
		 * @return string               String representation of the given emails collection.
		 */
		public function formatPrintEmails($Emails, $delimiter = '<br />', $defaultValue = self::DEFAULT_VALUE) {
			if (!$Emails) {
				return $defaultValue;
			}

			$printString = '';
			foreach ($Emails as $Email) {
				$printString .= ($printString) ? $delimiter : '';
				$printString .= $Email->email;
                if($Email->email_status_id == EmailStatus::OPT_OUT) {
                    $statusAlert = 'Opted-out';
                } elseif($Email->email_status_id == EmailStatus::HARD_BOUNCE) {
                    $statusAlert = 'Bounced';
                }
                if($statusAlert) {
                    $printString .= '<div class="errorMessage" style="display:inline-block;">&nbsp;'.$statusAlert.'</div>';
                }
			}

			return $printString;
		}

		/**
		 * @param        $Phones               Collection of Phones models.
		 * @param string $delimiter
		 * @param string $defaultValue
		 *
		 * @return string               String representation of the given phones collection.
		 */
		public function formatPrintPhones($Phones, $delimiter = '<br />', $defaultValue = self::DEFAULT_VALUE, $primaryLabel=' <em class="icon icon-only i_stm_star"></em>') {
			if (!$Phones) {
				return $defaultValue;
			}

			$printString = '';
			foreach ($Phones as $Phone) {
				$printString .= ($printString) ? $delimiter : '';
				$printString .= $this->formatPhone($Phone->phone);

                if($Phone->is_primary) {
                    $printString .= $primaryLabel;
                }
			}

			return $printString;
		}

		/**
		 * formatHomeViewSaved prepares string for Homes Viewed & Saved, used for grid views (Buyers,Sellers,Listings)
		 *
		 * @param  string $contactId
		 *
		 * @return string $string
		 */
		public function formatHomeViewSaved($contactId) {
			$viewedPages = ViewedPages::getCount($contactId) . ' Viewed';
			$string = ($viewedPages > 0) ? CHtml::tag('span', array('id' => 'listview-viewed-homes'), $viewedPages) : $viewedPages;

			$string .= ' / ';
			$savedHomes = SavedHomes::getCount($contactId) . ' Saved';
			$string .= ($savedHomes > 0) ? CHtml::tag('span', array('id' => 'listview-saved-homes'), $savedHomes) : $savedHomes;

			$string .= ' / ';
			$savedSearches = SavedHomeSearches::getCount($contactId);
			$string .= ($savedSearches > 0) ? $savedSearches . ' Search' : $savedSearches . ' Searches';

			return 'Homes: '.$string;
		}

		/**
		 * formatAddress prepares string for Grid View
		 *
		 * @param model $address
		 * @param null  $opt
		 *
		 * @internal param bool $lineBreak
		 * @return null|string the formatted address if given, null otherwise.
		 */
		public function formatAddress($address, $opt = null) {
			if (!$address->address) {
				return null;
			}

			$streetAddress = $address->address;
			if ($opt['CityStZipOnly'] === true) {
				return $address->city . ', ' . AddressStates::getShortNameById($address->state_id) . ' ' . $address->zip;
			}

			$cityStZip = $address->city . ', ' . AddressStates::getShortNameById($address->state_id) . ' ' . $address->zip;
			if ($opt['streetOnly'] === true) {
				return $streetAddress;
			}

			$streetAddress .= ($opt['lineBreak'] === false) ? ', ' : '<br />';
			$streetAddress .= $cityStZip;

			return $streetAddress;
		}

        public function formatPrintAddresses($addresses)
        {
            if(!is_array($addresses)) {
                return;
            }

            $string = '';
            foreach($addresses as $address) {
                $string .= ($string) ? '<br>' : '';
                $string .= Yii::app()->format->formatAddress($address);
            }
            return $string;
        }

		/**
		 * formatAddress prepares string for Grid View
		 *
		 * @param  string $models
		 * @param  string $field
		 *
		 * @return string $string
		 */
		public function formatCommaDelimited($models, $field) {
			$string = '';

			foreach ($models as $model) {
				$string .= ($string) ? ', ' . $model->$field : $model->$field;
			}

			return $string;
		}

        /**
         * Format a string into a link friendly version
         * Removes non-alpha characters and replaces space with dash. Removes duplicate spaces
         *
         * @return string $string
         */
        public static function formatLink($string)
        {
            $string = str_replace(array(',','/',' ','*','_'), '-', trim($string));
            $string = preg_replace('/-{2,}/', '-', $string);
            $string = preg_replace("/[^A-Za-z0-9\-]/", '', $string);

            return $string;
        }

        /**
         * Format a string into a link friendly version
         * Removes non-alpha characters and replaces space with dash. Removes duplicate spaces
         *
         * @return string $string
         */
        public static function formatRemoveExtraSpaces($string)
        {
            return preg_replace('/\s{2,}/', ' ', $string);
        }

		/**
		 * Format a text string to proper case.  Ensures casing by converting the string to lower case
		 *
		 * @param  string $string String to be proper cased.
		 *
		 * @return string
		 */
		public static function formatProperCase($string) {
			return ucwords(strtolower($string));
		}

		/**
		 * formatFileSize Puts Kb or Mb unit to end of file
		 *
		 * @param $fileSize
		 *
		 * @return string
		 */
		public function formatFileSize($fileSize) {
			if (!$fileSize) {
				return self::DEFAULT_VALUE;
			}

			switch ($fileSize) {
				case ($fileSize < 1024):
					$unit = 'bytes';
					break;

				case ($fileSize >= 1024 && $fileSize < 1048576):
					$unit = 'Kb';
					$fileSizeString = number_format((float)($fileSize / 1024), 1);
					break;

				case ($fileSize >= 1048576):
					$unit = 'Mb';
					$fileSizeString = number_format((float)($fileSize / 1048576), 1);
					break;
			}

			return $fileSizeString . ' ' . $unit;
		}

		public function formatTextArea($text) {
			return nl2br($text);
		}

		public function formatGoogleAnalyticsCookie($cookie) {
			$data = $this->parseGoogleAnalyticsCookie($cookie); //$campaign_data
			$string = '';
			$string = $this->addGoogleAnalyticsFieldString('campaign_source', 'Source', $data, $string);
			$string = $this->addGoogleAnalyticsFieldString('campaign_name', 'Name', $data, $string);
			$string = $this->addGoogleAnalyticsFieldString('campaign_medium', 'Medium', $data, $string);
			$string = $this->addGoogleAnalyticsFieldString('campaign_content', 'Content', $data, $string);
			$string = $this->addGoogleAnalyticsFieldString('campaign_term', 'Term', $data, $string);

			//
			//		if(!empty($data['campaign_source']))                                               ßß
			//			if(!empty($string))
			//
			//			$string = 'Source: '.$data['campaign_source'];
			//		if(!empty($data['campaign_name']))
			//			$string .= '<br />Name: '.$data['campaign_name'];
			//		if(!empty($data['campaign_medium']))
			//			$string .= '<br />Medium: '.$data['campaign_medium'];
			//		if(!empty($data['campaign_content']))
			//			$string .= '<br />Content: '.$data['campaign_content'];
			//		if(!empty($data['campaign_term']))
			//			$string .= '<br />Term: '.$data['campaign_term'];
			return $string;
		}

		public function addGoogleAnalyticsFieldString($field, $label, $data, $string) {
			if (!empty($data[$field])) {
				if (!empty($string)) {
					$string .= '<br />';
				}

				$string .= $label . ': ' . $data[$field];
			}

			return $string;
		}

		public function parseGoogleAnalyticsCookie($rawData) {
			if (is_array($rawData)) // Parse __utmz cookie
			{
				list($domain_hash, $timestamp, $session_number, $campaign_numer, $campaign_data) = preg_split('[\.]', $cookie["__utmz"], 5);
			} else {
				$campaign_data = $rawData;
			}
			// Parse the campaign data
			parse_str(strtr($campaign_data, "|", "&"));
			$data['campaign_source'] = $utmcsr;
			$data['campaign_name'] = $utmccn;
			$data['campaign_medium'] = $utmcmd;
			if (isset($utmcct)) {
				$data['campaign_content'] = $utmcct;
			}
			if (isset($utmctr)) {
				$data['campaign_term'] = $utmctr;
			}

			// Parse the __utma Cookie
			list($domain_hash, $random_id, $time_initial_visit, $time_beginning_previous_visit, $time_beginning_current_visit, $session_counter) = preg_split('[\.]', $cookie["__utma"]);
			if (date("Y-m-d H:i:s", $time_initial_visit) > '1969-12-31 19:00:00') {
				$data['first_visit'] = date("Y-m-d H:i:s", $time_initial_visit);
			}
			if (date("Y-m-d H:i:s", $time_beginning_previous_visit) > '1969-12-31 19:00:00') {
				$data['previous_visit'] = date("Y-m-d H:i:s", $time_beginning_previous_visit);
			}
			if (date("Y-m-d H:i:s", $time_beginning_current_visit) > '1969-12-31 19:00:00') {
				$data['current_visit_started'] = date("Y-m-d H:i:s", $time_beginning_current_visit);
			}
			$data['times_visited'] = $session_counter;

			return $data;
		}

		public function formatDateRangeArray($opts) {

			if ($opts['default_preset'] && (!isset($opts['from_date']) || empty($opts['from_date']) || !isset($opts['to_date']) || empty($opts['to_date']))) {
				switch ($opts['default_preset']) {
					case 'this_month':
						$opts['from_date'] = date('Y-m-01');
						$opts['to_date'] = date('Y-m-d 23:59:59');
						break;

					default:
						$opts['from_date'] = date("Y-m-d", strtotime("-30 days"));
						$opts['to_date'] = date('Y-m-d');
						break;
				}
			}

			$opts['from_date'] = ($opts['from_date']) ? date('Y-m-d', strtotime($opts['from_date'])) : date("Y-m-d", strtotime("-30 days"));
			$opts['to_date'] = ($opts['to_date']) ? date('Y-m-d', strtotime($opts['to_date'])) : date("Y-m-d");

			return $opts;
		}

		public function formatSecToHours($seconds, $opts) {
			$opts['decimalPlaces'] = (isset($opts['decimalPlaces'])) ? $opts['decimalPlaces'] : 2;

			if ($opts['decimalConditional'] == true) {
				return str_replace('.00', '', number_format(($seconds / 3600), $opts['decimalPlaces']));
			} else {
				return number_format(($seconds / 3600), $opts['decimalPlaces']);
			}
		}

        public function formatSeconds($seconds, $opts=null)
        {
            // default options
            $opts['decimalPlaces'] = (isset($opts['decimalPlaces'])) ? $opts['decimalPlaces'] : 1;

            switch(1) {
                case ($seconds < 60):
                    $string = $seconds.' sec';
                    // seconds
                    break;

                case ($seconds > 360 && $seconds > 3600):
                    // hours
                    $string = round($seconds/360, $opts['decimalPlaces']).' hours';
                    break;

                case ($seconds >= 60 && $seconds < 3600):
                    //minutes
                    $string = round($seconds/60).' mins';
                    break;
            }
            return $string;
        }

        public function urlSafeBase64Encode($data) {
            return strtr(base64_encode($data), '+/=', '-,_');
        }

        public function urlSafeBase64Decode($data) {
            $decodeString = base64_decode(strtr($data, '-,_', '+/='));
            if(!$decodeString) {
                throw new Exception('Url Decode had invalid base 64 characters');
            }
            return $decodeString;
        }

        public static function generateUuid()
        {
            return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

                // 32 bits for "time_low"
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),

                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),

                // 16 bits for "time_hi_and_version", Four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,

                // 16 bits, 8 bits for "clk_seq_hi_res", 8 bits for "clk_seq_low", Two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,

                // 48 bits for "node"
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff)
            );
        }
    }
