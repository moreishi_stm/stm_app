<?php

	/**
	 * StmFormHelper
	 *
	 * @author  Chris Willard <chris@seizethemarket.com>
	 * @since   09/04/2012
	 * @version 1.2
	 */
	class StmFormHelper extends CComponent {

		const BLANK_DROPDOWN_VALUE = '';
		const EMPTY_DROPDOWN_VALUE = 'Select One';
		const MONEY_PLACEHOLDER = '$0';
		const ACTIVE = 1;
		const INACTIVE = 0;
        const YES = 1;
        const NO = 0;

		/**
		 * Return the label of a boolean status
		 *
		 * @return string - 'Active' or 'Inactive'
		 */
		public static function getStatusBooleanName($value, $inactiveLabel=null) {
            $inactiveLabel = ($inactiveLabel)? $inactiveLabel:'Inactive';
			return (intval($value) == 1 || $value === true) ? 'Active' : $inactiveLabel;
		}

        public static function getBooleanImage($value, $classes) {
            $imgName = (intval($value) == 1 || $value === true) ? 'success' : 'error';
            return "<em class='icon i_stm_{$imgName} {$classes}'></em>";
            //return "<img src='http://cdn.seizethemarket.com/assets/images/{$imgName}.png'/>";
        }

		/**
		 * Return a list of boolean values for forms based on the native yii format
		 *
		 * @return array List in the format $booleanList[0] = 'No'
		 */
		public static function getStatusBooleanList($reverse=false, $inactiveLabel=null) {
            $inactiveLabel = ($inactiveLabel)? $inactiveLabel:'Inactive';
			if($reverse) {
				return array(
					1 => 'Active',
					0 => $inactiveLabel
				);
			} else {
				return array(
					0 => $inactiveLabel,
					1 => 'Active'
				);
			}
		}

		/**
		 * Return the label of a boolean status
		 *
		 * @return string - 'Yes' or 'No'
		 */
		public static function getYesNoName($value, $displayEmpty = false) {
			if ($value != '') {
				return (intval($value) == 1 || $value === true) ? 'Yes' : 'No';
			} elseif (!$value && $displayEmpty) {
				return 'No';
			} else {
				return null;
			}
		}

		/**
		 * Return a list of boolean values for forms based on the native yii format
		 *
		 * @return array List in the format $booleanList[0] = 'No'
		 */
		public static function getYesNoList($orderYesFirst = true) {
			if ($orderYesFirst) {
				return array(
					1 => 'Yes',
					0 => 'No'
				);
			} else {
				return array(
					0 => 'No',
					1 => 'Yes'
				);
			}
		}

		/**
		 * Return a list of boolean values for forms based on the native yii format
		 *
		 * @return array List in the format $booleanList[0] = 'No'
		 */
		public static function getRequiredExcludeBooleanList() {
			return array(
				Terms::REQUIRED => 'Required',
				Terms::EXCLUDE => 'Exclude'
			);
		}

		/**
		 * Return a list of boolean values for forms based on the native yii format
		 *
		 * @return array List in the format $booleanList[0] = 'No'
		 */
		public static function getBooleanList() {
			$booleanList = array();
			for ($i = 0; $i <= 1; $i++) {
				$booleanList[$i] = Yii::app()->format->formatBoolean($i);
			}

			return $booleanList;
		}

		public static function getHoursList() {
			$hours = array();
			for ($i = 0; $i < 24; $i++) {

				$hours[$i] = date("ga", strtotime($i . ":00:00"));
			}

			return $hours;
		}

		public static function getMinutesList() {
			$minutes = array();
			for ($i = 0; $i < 60; $i++) {
				$minutes[sprintf("%02d", $i)] = sprintf("%02d", $i);
			}

			return $minutes;
		}

		/**
		 * Return a list of price dropdown list values for forms based on the native yii format
		 *
		 * @return array List in the format $list[100000] = '$100,000'
		 */
		public static function getPriceList($value = null) {
			$list = array();
			$priceBreaks = array(
				1 => array(
					'start' => 25000,
					'end' => 400000,
					'increment' => 5000
				),
				2 => array(
					'start' => 400000,
					'end' => 1000000,
					'increment' => 25000
				),
				3 => array(
					'start' => 1000000,
					'end' => 3000000,
					'increment' => 250000
				),
				4 => array(
					'start' => 3000000,
					'end' => 5000000,
					'increment' => 500000
				),
				5 => array(
					'start' => 5000000,
					'end' => 10000000,
					'increment' => 1000000
				),
			);
			foreach ($priceBreaks as $priceBreak) {
				for ($i = $priceBreak['start']; $i < $priceBreak['end']; $i = $i + $priceBreak['increment']) {
					$list[$i] = Yii::app()->format->formatDollars($i);
				}
			}

			return $list;
		}

		/**
		 * Return a list of bedroom dropdown list values for forms based on the native yii format
		 *
		 * @return array List in the format $list[2] = '2+ Bedrooms'
		 */
		public static function getBedroomList($label = null) {
			$label = ($label) ? $label : '+ Bedrooms';
			$list = array();
			for ($i = 1; $i < 10; $i++) {
				$list[$i] = $i . $label;
			}

			return $list;
		}

		/**
		 * Return a list of bathroom dropdown list values for forms based on the native yii format
		 *
		 * @return array List in the format $list[2] = '2+ Baths'
		 */
		public static function getBathList($label = null) {
			$label = ($label) ? $label : '+ Baths';
			$list = array();
			for ($i = 2; $i < 6; $i++) {
				$list[$i] = $i . $label;
			}

			return $list;
		}

        public static function getHouseValuesBathList($label = null) {
            $label = ($label) ? $label : ' Baths';
            $list = array();
            for ($i = 1; $i <= 6; $i=$i+.5) {
                $list["$i"] = $i . $label;
            }

            return $list;
        }

		/**
		 * Return a list of bathroom dropdown list values for forms based on the native yii format
		 *
		 * @return array List in the format $list[2] = '2+ Baths'
		 */
		public static function getSqFeetList($label=true, $labelText=null) {
			$list = array();
            $labelName = '';
            if($label) {
                // if label is true use default, else use passed in label
                $labelName = ($label===true)? ' SF' : ' '.$label;

                if($labelText) {
                    $labelName = $labelText;
                }
            }

			$breaks = array(
				0 => array(
					'start' => 800,
					'end' => 1000,
					'increment' => 100
				),
				1 => array(
					'start' => 1000,
					'end' => 3000,
					'increment' => 100
				),
				2 => array(
					'start' => 3000,
					'end' => 6000,
					'increment' => 200
				),
				3 => array(
					'start' => 6000,
					'end' => 8000,
					'increment' => 500
				),
			);
			foreach ($breaks as $break) {
				for ($i = $break['start']; $i < $break['end']; $i = $i + $break['increment']) {
					$list[$i] = number_format($i) . $labelName;
				}
			}

			return $list;
		}

        public static function getAcreageList($label = null, $labelText=null) {

            $list = array();
            $labelName = '';
            if($label) {
                // if label is true use default, else use passed in label
                $labelName = ($label===true)? '+ Acres' : ' '.$label;

                if($labelText) {
                    $labelName = $labelText;
                }
            }

            $breaks = array(
                0 => array(
                    'start' => .05,
                    'end' => .5,
                    'increment' => .05
                ),
                1 => array(
                    'start' => .5,
                    'end' => 5,
                    'increment' => .5
                ),
                2 => array(
                    'start' => 5,
                    'end' => 20,
                    'increment' => 1
                ),
                3 => array(
                    'start' => 20,
                    'end' => 100,
                    'increment' => 5
                ),
                4 => array(
                    'start' => 100,
                    'end' => 500,
                    'increment' => 25
                ),
            );
            foreach ($breaks as $break) {
                for ($i = $break['start']; $i < $break['end']; $i = $i + $break['increment']) {
                    $list["$i"] = $i . $labelName;
                }
            }

            return $list;
        }

		/**
		 * Return a list of bedroom dropdown list values for forms based on the native yii format
		 *
		 * @return array List in the format $list[2] = '2+ Bedrooms'
		 */
		public static function getYrBuiltList($value = null) {
			$list = array();
			for ($i = date("Y"); $i > 1950; $i--) {
				$list[$i] = $i . '+ Yr Built';
			}

			return $list;
		}

        public static function getGarageList($value = null) {
            $label = ($label) ? $label : '+ Car';
            $list = array();
            for ($i = 1; $i <= 4; $i++) {
                $list[$i] = $i . $label;
            }

            return $list;
        }

        /**
		 * Return a list of 1 to 100 precentage dropdown list values for forms based on the native yii format
		 *
		 * @return array
		 */
		public static function getPercentageList($opt = null) {
            $min = ($opt['min'] || ($opt['min'] == 0) && ($opt['min'] !== null)) ? $opt['min'] : 1;
			$max = ($opt['max']) ? $opt['max'] : 100;

			$list = array();
			for ($i = $max; $i >= $min; $i--) {
				$list[number_format(($i / 100), 2)] = $i . '%';
			}

			if ($opt['sort'] == 'asc') {
				asort($list, SORT_NUMERIC);
			}

			return $list;
		}

		/**
		 * Return a list of 1 to 100 precentage dropdown list values for forms based on the native yii format
		 *
		 * @return array
		 */
		public static function getTimeHoursList($opt = null) {
			$list = array();
			for ($i = 0; $i < 24; $i++) {
				$list[$i] = date("g a", strtotime($i . ":00"));
			} //date("H", strtotime("5:55 pm"));
			return $list;
		}

		public static function getDropDownList($opt = null) {

            $decimalPlaces = 0;
            if(!isset($opt['increment'])) {
                $opt['increment'] = 1;
            }
            // figure out decimal places needed
            if($opt['increment'] < 1) {
                $decimalPlaces = strlen(substr($opt['increment'], strpos($opt['increment'], '.') + 1));
            }

			$i = $opt['start'];
			$list = array();

			while ($i <= $opt['end']) {
//                $list["$i"] = $i . $opt['label'];
				$list[number_format($i, $decimalPlaces, '.', '')] = number_format($i, $decimalPlaces, '.', '') . $opt['label'];
				$i += $opt['increment'];
			}

			return $list;
		}

		/**
		 * Saves model items for a MultiSelect widget.
		 * Loops through all existing records through the relationship and compares to the multiSelect property inside the model. If the
		 * existing record is a match for the most recent post variables, nothing needs to be done, so unset it. If it doesn't exist, it
		 * means it wasn't select anymore so it needs to be deleted. The remaining ones need to have a new record saved so new models are
		 * created for each, data inputed and then saved.
		 *
		 * @param  mixed $model                (Ex. Contacts ... not ContactTypes)
		 * @param  array $options
		 *
		 * @return boolean
		 */
		public static function multiSelectSaver($model, $options) {
			//$options['collectionField'] = 'typesCollection';
			//$options['lookupRelation'] = 'typeLu';
			//$options['lookupModelName'] = 'FeaturedAreaTypeLu';
			//$options['foreignKeyColumn'] = 'featured_area_id';
			//$options['relationPKColumn'] = 'featured_area_type_id';

			// turns each $option into it's own variable for ease of code read
			foreach ($options as $key => $value) {
				$$key = $value;
			}

			// Flips array to that array key is the PK not the value, easier to process/reference for processing
			$modelCollection = array_flip($model->$collectionField);
			$successFlag = true;

			// Ex. $Contacts->types as $ContactTypes
			foreach ($model->$lookupRelation as $LookupModel) {
				// delete any lookups not in the collection
				if (!isset($modelCollection[$LookupModel->$relationPKColumn])) {

					$LookupModel->delete();
					unset($modelCollection[$LookupModel->$relationPKColumn]);
				} // if the lookup already exists, no change is needed, mark it as done by unset-ing it
				else {
					if (isset($modelCollection[$LookupModel->$relationPKColumn])) {
						unset($modelCollection[$LookupModel->$relationPKColumn]);
					}
				}
			}

			// Attach the contact types to the newly contact
			foreach ($modelCollection as $key => $value) {
				$LookupModel = new $lookupModelName;
				$LookupModel->$foreignKeyColumn = $model->id;
				$LookupModel->$relationPKColumn = $key;

				if (!$LookupModel->save() || $successFlag) {
					$successFlag = false;
				}
			}

			return $successFlag;
		}

		/**
		 * Loads model items for a MultiSelect widget
		 *
		 * @return array $fieldCollection
		 */

		public static function multiSelectLoader($model, $options) {
			// turns each $option into it's own variable for ease of code read
			foreach ($options as $key => $value) {
				$$key = $value;
			}

			$dataCollection = array();
			if ($model->$relationName) {
				foreach ($model->$relationName as $modelSingle) {
					array_push($dataCollection, $modelSingle->$relationPKColumn);
				}
			}

			return $dataCollection;
		}

        public static function getGoogleTrackingData() {
            $trackingData = array();
            // Parse __utmz cookie
            list($domain_hash, $timestamp, $session_number, $campaign_number, $campaign_data) = preg_split('[\.]', $_COOKIE["__utmz"], 5);

            // Parse the campaign data
            parse_str(strtr($campaign_data, "|", "&"));
            $trackingData['campaign_source'] = $utmcsr;
            $trackingData['campaign_name'] = $utmccn;
            $trackingData['campaign_medium'] = $utmcmd;
            if (isset($utmcct)) {
                $trackingData['campaign_content'] = $utmcct;
            }
            if (isset($utmctr)) {
                $trackingData['campaign_term'] = $utmctr;
            }

            return $trackingData;
        }
	}