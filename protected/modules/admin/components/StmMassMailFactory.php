<?php
/**
 * Stm Mass Mail Factory
 * 
 * @author Nicole Xu
 */
class StmMassMailFactory {
	
	/**
	 * Mass Mail Adapter Mail Chimp
	 * 
	 * This represents an adapter that is selectable using the factory method
	 */
	const MASS_MAIL_ADAPTER_MAIL_CHIMP = 'MailChimp';
	
	/**
	 * Mass Mail Adapter Constant Contact
	 * 
	 * This represents an adapter that is selectable using the factory method
	 */
	const MASS_MAIL_ADAPTER_CONSTANT_CONTACT = 'ConstantContact';
	
	/**
	 * Adapter Mapping
	 * 
	 * Mapping of adapter names to class names
	 * @var array Adapter nice name => Adapter class name
	 */
	protected static $_adapterMapping = array(
		'MailChimp'			=>	'MailChimp',
		'ConstantContact'	=>	'ConstantContact'
	);
	
	/**
	 * Factory
	 * 
	 * @param string $adapter See class constants for available adapters
	 * @return \MassMail\Adapters\Base An adapter class that implenents this interface
	 * @throws Exception When an invalid adapter has been passed
	 */
	public static function factory($adapter)
	{
		// Make sure we selected a valid adapter
		if(!array_key_exists($adapter, self::$_adapterMapping)) {
			throw new Exception('Invalid adapter chosen');
		}
		
		// Determine class to use
		$class = "\MassMail\Adapters\\" . self::$_adapterMapping[$adapter];
		
		// Require the class
		include_once 'MassMail/' . self::$_adapterMapping[$adapter] . '.php';
		
		// Return new adapter
		return new $class();
	}
}