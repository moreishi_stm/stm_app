<?php

/**
 * WebUser
 * Provides access to common attributes from the contact user via Yii::app()->user->firstName
 *
 * @since   8/12/2012
 * @version 0.1 Beta
 * @author  Chris Willard <chris@seizethemarket.com>
 * @extends CWebUser
 */
class StmWebUser extends CWebUser {

    // Pointer to the registry value containing the current requested domain
    const REGISTRY_DOMAIN = 'activeDomain';
    const CDN_ROOT = '/mnt/stm_images';
    const ROLE_ADMINISTRATOR = 'admin';
    const ROLE_ACCOUNT_OWNER = 'owner';
    const ROLE_ACCOUNT_AGENT = 'agent';
    const ROLE_INACTIVE      = 'inactive';

    public $loginUrl = array('/admin');
    protected $_client;
    protected $_clientId;
    protected $_board;
    protected $_hasMultipleBoards = null;
    protected $_account;
    protected $_accountId;
    protected $_domain;
    protected $_primaryDomain;
    protected $_replyToDomain;
    protected $_settings;
    protected $_accountSettings;
    protected $_contactModel;
    protected $_topLinks;

    public function init()
    {
        parent::init();
        if(!YII_IS_CONSOLE) {
            // @todo: this was done to accomonodate HQ module
            $this->loginUrl = array('/'.Yii::app()->controller->module->name);
        }
    }

    /**
     * Attempts to utilize the contact model to retrieve properties or methods, else this will utilize this component
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        /**
         * This object takes priority over the contact model for properties and methods
         */
        $getterName = "get{$name}";
        if (method_exists($this, $getterName)) {
            return $this->$getterName();
        } elseif (method_exists($this, $name)) {
            return $this->$name();
        } elseif (property_exists($this, $name)) {
            return $this->$name;
        }

        $contact = $this->getContact();
        if ($contact) {
            if (isset($contact->$name)) {
                return $contact->$name;
            }
        }

        return parent::__get($name);
    }

    /**
     * Fetch and cache the contact model after login
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return ContactsModel
     */
    public function getContact() {
        if (!$this->_contactModel && Yii::app()->user->id) {
            $this->_contactModel = $this->loadContact(Yii::app()->user->id);
        }

        return $this->_contactModel;
    }

    /**
     * Returns the first name of the user
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getFirstName() {
        return $this->contact->first_name;
    }

    /**
     * Returns the last name of the user
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getLastName() {
        return $this->contact->last_name;
    }

    /**
     * Returns the first and last name for the user
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getFullName() {
        return $this->getFirstName().' '.$this->getLastName();
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @throws CHttpException
     * @return mixed
     */
    public function getAccount()
    {
//        $api_cache_id = 'Account';

        // DO NOT CACHE FOR Seize the Market.com
        /*if(strstr($_SERVER['HTTP_HOST'], 'seizethemarket.com') && !strstr($_SERVER['HTTP_HOST'], 'demo.seizethemarket.com')) {
            //clear out cache
            Yii::app()->fileCache->delete($api_cache_id);
        }*/

        if(empty($this->_account)) {
            // IMPORTANT: ***** There can only be one account per repo! *****
            //$accountCache = Yii::app()->fileCache->get($api_cache_id);
            // only use file cache if not seizethemarket.com
            //if(!empty($accountCache) && !strstr($_SERVER['HTTP_HOST'], 'seizethemarket.com')) {
            //    return $this->_account = $accountCache;
            //}

            if(Yii::app()->user->isCliEnv || YII_IS_CONSOLE) {
                if(isset($_SERVER['argv'])) {
                    foreach($_SERVER['argv'] as $arg) {
                        if(strpos($arg, '--accountId=') !== false) {
                            $accountId = str_replace('--accountId=', '', $arg);
                            return $this->_account = Accounts::model()->findByPk($accountId);
                        }
                    }
                }
                if(!$this->_account) {

                    throw new CException(__CLASS__.' ('.__LINE__.') In CLI Environment. Account ID not provided as an argument. Cannot retrieve Account model.', 500);
                }
            }

            // If the user is a guest, then assume the account tied to the domain
//            if ($this->isGuest) {
                try {
//                        $this->_account = $this->getDomain()->account; // this was not retrieving account model correctly on local for some reason, therfore not ever caching.
                    $domain = $this->getDomain();
                    if(!$domain->account) {
                        $this->_account = Accounts::model()->byIgnoreAccountScope()->findByPk($domain->account_id);
                    }
                    else {
                        $this->_account = $domain->account; //This is the alternative for $this->getDomain()->account. this one tweak seems to have a 3-5% improvement on speed locally. Theoretically on production should be more cuz of network latency, as this is reducing db calls
                    }
                } catch (Exception $e) {
                    throw new CHttpException(500, 'Could not get account from domain.');
                }
//            } else {
////                    $this->_account = $this->getContact()->account;
//                $contact = $this->getContact();
//                $this->_account = $contact->account; //This is the alternative for $this->getDomain()->account. this one tweak seems to have a 3-5% improvement on speed locally. Theoretically on production should
//            }
            //Yii::app()->fileCache->set($api_cache_id , $this->_account, 60*60*12);
        }

        /*if(strstr($_SERVER['HTTP_HOST'], 'seizethemarket.com') && !strstr($_SERVER['HTTP_HOST'], 'demo.seizethemarket.com')) {
            $accountCache = Yii::app()->fileCache->get($api_cache_id);
            if($accountCache->id==2) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Account caching error: seizethemarket.com is attempting to have an account ID #2. Review data to correct this bug. Account Cache Attributes: '.print_r($accountCache->attributes, true), CLogger::LEVEL_ERROR);
            }
        }*/
        return $this->_account;
    }

    /**
     * Checks to see if this account has multiple mls feeds/boards
     */
    public function hasMultipleBoards()
    {
        if($this->_hasMultipleBoards === false || $this->_hasMultipleBoards === true) {
            $this->_hasMultipleBoards;
        }
        else {
            $clientId=$this->clientId;
            $accountId=$this->accountId;
            if(!$clientId) {
                // get client Id by db name
                //@todo: ...
            }
            if(!$accountId) {
                // get account Id by domain
                //@todo: ...

            }
            if($clientId && $accountId) {
                $this->_hasMultipleBoards = (Yii::app()->db->createCommand("select count(*) from stm_hq.mls_feeds where status_ma=1 AND client_id=".$clientId." AND account_id=".$accountId)->queryScalar() >1);
            }
        }

        return $this->_hasMultipleBoards;
    }

    /**
     * Retrieve the Client Id of this client from the HQ level
     * @return mixed
     */
    public function getClientId() {
        if(!empty($this->_clientId)) {
            return $this->_clientId;
        }

        if(Yii::app()->user->isCliEnv || YII_IS_CONSOLE) {
            if(isset($_SERVER['argv'])) {
                foreach($_SERVER['argv'] as $arg) {
                    if(strpos($arg, '--clientId=') !== false) {
                        return $this->_clientId = str_replace('--clientId=', '', $arg);
                    }
                }
            }
            if(!$this->_clientId) {
                throw new CException(__CLASS__.' ('.__LINE__.') In CLI Environment. Client ID not provided as an argument.', 500);
            }
        }
        else {
            $dbName = Yii::app()->db->createCommand("select DATABASE()")->queryScalar();
            if($client = Clients::model()->findByAttributes(array('db_name'=>$dbName))) {
                $this->_clientId = $client->id;
            }
            else {
                throw new CException(__CLASS__.' ('.__LINE__.') Could not find Client ID.', 500);
            }
        }

        return $this->_clientId;
    }

    public function getClient()
    {
        if(!empty($this->_client)) {
            return $this->_client;
        }

        $dbName = Yii::app()->db->createCommand("select DATABASE()")->queryScalar();
        if($client = Clients::model()->findByAttributes(array('db_name'=>$dbName))) {
            $this->_client = $client;
        }
        else {
            throw new CException(__CLASS__.' ('.__LINE__.') Could not find Client.', 500);
        }
        return $this->_client;
    }

    /**
     * Set Client
     * NOTE: Only use this for YII_CONSOLE. This was created for emailLeadProcessing as clientID was inaccessible and breaking the process. Use this SPARINGLY!!!!
     */
    public function setClientId($clientId)
    {
        $client = Clients::model()->findByPk($clientId);
        if(empty($client)) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Client ID '.$clientId.' Not found. This may be a critical error to a cron process. Investigate immediately.');
        }

        if(YII_IS_CONSOLE || YII_IS_CONSOLE_PRODUCTION) {
            if(isset($_SERVER['argv'])) {
                foreach($_SERVER['argv'] as $i => $arg) {
                    if(strpos($arg, '--clientId=') !== false) {
                        // set the new clientId
                        $_SERVER['argv'][$i] = '--clientId='.$clientId;
                        $this->_clientId = $clientId;
                        return;
                    }
                }
            }

            // if this is reached it means --clientId argv does not exist so set it
            $_SERVER['argv'][] = '--clientId='.$clientId;
            $this->_clientId = $clientId;
        }
        else {
            // NOTE: this function is only meant to be used for Console Command cuz it could not be accessed via Yii::app()->user->clientId. This is NOT a short cut for non-console scenarios.
            throw new Exception(__CLASS__.' (:'.__LINE__.') Client ID Not found. This may be a critical error to a cron process. Investigate immediately.');
        }
    }

    /**
     * Retrieve the account id of the user
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getAccountId() {
        if(!empty($this->_accountId)) {
            return $this->_accountId;
        }

        if(Yii::app()->user->isCliEnv || YII_IS_CONSOLE || YII_IS_CONSOLE_PRODUCTION) {
            if(isset($_SERVER['argv'])) {
                foreach($_SERVER['argv'] as $arg) {
                    if(strpos($arg, '--accountId=') !== false) {
                        return $this->_accountId = str_replace('--accountId=', '', $arg);
                    }
                }
            }
            if(!$this->_accountId) {
                throw new CException(__CLASS__.' ('.__LINE__.') In CLI Environment. Account ID not provided as an argument.', 500);
            }
        }

        if(!empty($this->_account)) {
            return $this->_accountId = $this->_account->id;
        } else {
            $this->_accountId = $this->getAccount()->id;
        }

        return $this->_accountId;
    }

    /**
     * Only to be used for YII_CONSOLE as it is hard to pass this around and set it properly in cron jobs when this changes all the time.
     * @param $accountId
     */
    public function setAccountId($accountId)
    {
        $account = Accounts::model()->findByPk($accountId);
        if(empty($account)) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Account ID '.$accountId.' Not found. This may be a critical error to a cron process. Investigate immediately.');
        }

        if(YII_IS_CONSOLE || YII_IS_CONSOLE_PRODUCTION) {
            if(isset($_SERVER['argv'])) {
                foreach($_SERVER['argv'] as $i => $arg) {
                    if(strpos($arg, '--accountId=') !== false) {
                        // set the new accountId
                        $_SERVER['argv'][$i] = '--accountId='.$accountId;
                        $this->_accountId = $accountId;
                        return;
                    }
                }
            }

            // if this is reached it means --clientId argv does not exist so set it
            $_SERVER['argv'][] = '--accountId='.$accountId;
            $this->_accountId = $accountId;
        }
        else {
            // NOTE: this function is only meant to be used for Console Command cuz it could not be accessed via Yii::app()->user->clientId. This is NOT a short cut for non-console scenarios.
            throw new Exception(__CLASS__.' (:'.__LINE__.') Client ID Not found. This may be a critical error to a cron process. Investigate immediately.');
        }
    }

    /**
     * Retrieve the primary domain for the user
     * @todo Is this necessary?
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return CActiveRecord
     */
    public function getPrimaryDomain() {
        if (!$this->_primaryDomain) {
            //$api_cache_id = 'PrimaryDomain';
            /*if(($primaryDomainCache = Yii::app()->fileCache->get($api_cache_id)) !== false) {

                //@todo in here for debuggin stm db connection weirdness
                if(strstr($_SERVER['HTTP_HOST'], 'seizethemarket.com') && Yii::app()->controller->id !== 'sns') {
                    if($primaryDomainCache && $primaryDomainCache->name !== 'seizethemarket.com') {
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Domain conflict for seizethemarket.com (non-CLI). Domain Attributes: '.print_r($primaryDomainCache->attributes, true). 'DB Connection String: '.print_r(Yii::app()->db->connectionString, true), CLogger::LEVEL_ERROR);
                    }
                }


                $this->_primaryDomain = $primaryDomainCache;
            }*/

            $domain = Domains::model()->find(array(
                    'condition' => 'is_primary = 1',
                )
            );
            $this->_primaryDomain = $domain;
            //Yii::app()->fileCache->set($api_cache_id , $this->_primaryDomain, 60*60*12);
        }

        return $this->_primaryDomain;
    }

    /**
     * Retrieves the domain to use for sending emails
     * @author Chris Willard <chris@seizethemarket.com>
     * @since 1.0
     * @return array|CActiveRecord|mixed|null
     */
    public function getReplyToDomain()
    {
        // file caching this may not be necessary as it is only crucial in mass emails like email drips and market updates. if stmwebuser caches this once per script run, that is same effectiveness as file caching.
        if (!$this->_replyToDomain) {
            // @todo: NOTE disabling cache as I think it's creating issues
//                $api_cache_id = 'replyToDomain';
//                if(($replyToDomainCache = Yii::app()->fileCache->get($api_cache_id))!==false) {
//                    $this->_replyToDomain = $replyToDomainCache;
//                }
//                else {
            $this->_replyToDomain = Domains::model()->findByAttributes(array(
                'account_id' => $this->getAccount()->id,
                'for_emails' => 1,
            ));
            /**
             * Use the primary domain if the one marked for emailing does not exist
             * @todo: this is not a good sanity check but is temporarily needed to appease clients... should this even exist - Ask Nicole
             */
            // as secondary backup if the domain query doesn't work
            if (empty($this->_replyToDomain)) {
                Yii::log(__CLASS__ . ' (:'. __LINE__ .') Reply to Domain is empty even after a Domain model find(). Please investigate situation.', CLogger::LEVEL_ERROR);
                $this->_replyToDomain = $this->getPrimaryDomain();
            }
//                    else {
            //@todo: **** FILE CACHING! NOT WORKING!! **** this was coming back as false... need to see why???? ******
//                        Yii::app()->fileCache->set($api_cache_id , $this->_replyToDomain, 86400);
//                    }
//                }
        }

        return $this->_replyToDomain;
    }

    /**
     * Retrieves the domain for the user
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getDomain() {
        if (!$this->_domain) {
            $this->_domain = Domains::getActiveDomain();
            Yii::app()->registry->set(self::REGISTRY_DOMAIN, $this->_domain);
//				$this->_domain = Yii::app()->registry->get(self::REGISTRY_DOMAIN); //@todo: when it's retrieved from registry, doesn't preserve account relationship
        }

        return $this->_domain;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getVerboseDomainName() {
        $domain = $this->getDomain();
        if (!$domain) {
            return '';
        }

        return $domain->getVerboseName();
    }

    /**
     * Returns the mls image directory
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getMlsImagesBaseUrl() {
        $domain = $this->getDomain()->name;
        if (YII_DEBUG) {
            $domain = str_replace('.local', '.com', $domain);
        }

        return 'http://mlsimages.seizethemarket.com/'.$this->getBoardName();
    }

    /**
     * Retrieves the image base url for the user
     * @todo Not really contact specific
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return null|string
     */
    public function getImagesBaseUrl() {
        $domain = $this->getVerboseDomainName();
        if (!($domain = $this->getVerboseDomainName())) {
            return null;
        }
        if (YII_DEBUG) {
            $domain = str_replace('.com', '.local', $domain);
        }

        return 'http://'.$domain.DS.'images'.DS;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getProfileImageBaseUrl() {

        return 'http://' . (YII_DEBUG ? 'dev.' : '') . 'sites.seizethemarket.com/site-files/' . Yii::app()->user->clientId . '/profile';

        $domain = $this->getDomain()->name;

        if(YII_DEBUG) {
            $domain = str_replace(array('.com','.net','.org'),'.local',$domain);
        }

        return 'http://images.'.$domain.DS.'profile';
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getProfileImageUrl() {
        $basePhotoUrl = $this->getProfileImageBaseUrl();
        $photoUrl = $this->getSettings()->profile_photo;

        return $basePhotoUrl. '/' .$this->getId() . $photoUrl;
    }

    /**
     * @todo Should this just be Yii::app()->user->getProfileImagePath()?
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getProfileImageBaseFilepath() {

        return 'site-files/' . Yii::app()->user->clientId . '/profile';
//        return $this->getCdnDomainRoot().DS.'profile';
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return null|string
     */
    public function getDocsBaseFilePath() {
        if (!($domain = $this->getPrimaryDomain()->name)) {
            return null;
        }

        return $this->getCdnDomainRoot().DS.'docs';
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return null|string
     */
    public function getCdnDomainRoot() {
        if (!($domain = $this->getPrimaryDomain()->name)) {
            return null;
        }

        return $this->getCdnRoot().DS.$domain;
    }

    /**
     * @todo Should we remove this?
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getCdnRoot() {
        return self::CDN_ROOT;
    }

    /**
     * Wrapper for user and account level settings
     * Contact level takes precedence over Account level
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return stdClass
     */
    public function getSettings() {
        if (!$this->_settings) {
            $this->_settings = $this->getAccountSettings();

            //@todo: memcache candidate
            $userSettings = SettingContactValues::model()->findAll(array(
                    'condition' => 'contact_id=:contactId',
                    'params' => array(
                        ':contactId' => $this->contact->id
                    )
                )
            );

            if ($userSettings) {
                foreach ($userSettings as $userSetting) {
                    $this->_settings[$userSetting->setting->name] = $userSetting->value;
                }
            }
        }

        return (object) $this->_settings;
    }

    /**
     * Retrieve the account level settings for the user
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getAccountSettings() {
        //@todo: memcache candidate
        $accountSettings = SettingAccountValues::model()->findAllByAttributes(array('account_id'=>Yii::app()->user->accountId));

        if ($accountSettings) {
            foreach ($accountSettings as $accountSetting) {
                $this->_accountSettings[$accountSetting->setting->name] = $accountSetting->value;
            }
        }

        return $this->_accountSettings;
    }

    /**
     * Retrieves the current board for an account
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getBoard() {
        if (!$this->_board) {
            /*$api_cache_id = 'Board';
            if(($boardCache = Yii::app()->fileCache->get($api_cache_id))!==false) {
                if(get_class($boardCache) == 'MlsBoards') {
                    return $boardCache;
                }
            }*/

            // IMPORTANT: ***** There can only be one account per repo! *****
            $this->_board = $this->getAccount()->board;
            //Yii::app()->fileCache->set($api_cache_id , $this->_board, 60*60*12);
        }

        return $this->_board;
    }

    /**
     * Returns the user's board id
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return board id
     */
    public function getBoardId() {
        $board = $this->getBoard();
        if (!$board) {
            return null;
        }

        return $board->id;
    }

    /**
     * Returns the board name
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return string
     */
    public function getBoardName() {
        $board = $this->getBoard();
        if (!$board) {
            return '';
        }
        return $board->getStateShortName($lowercase=true) . '_' . strtolower($board->short_name);
    }

    /**
     * Retrieve the roles for the user
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getRoles() {
        return Yii::app()->authManager->getRoles($this->contact->id);
    }

    /**
     * Does the user belong to a specific role?
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @param $roleName
     * @return bool
     */
    public function hasRole($roleName) {
        if (!$this->roles) {
            return false;
        }

        return ($this->roles[$roleName]) ? true : false;
    }

    public function isMaxUsers() {
        $currentUserCount = Contacts::model()->byActiveAdmins()->count();
        if($currentUserCount <= $this->settings->max_admin_users) {
            return false;
        } else {
            return true;
        }
    }

    public function loadContact($contactId) {

        //@todo: memcache candidate? for when account Id is looked up.
        return Contacts::model()->byIgnoreAccountScope()->findByPk($contactId);
    }

    public function getTopLinks() {
        if ($this->_topLinks) {
            return $this->_topLinks;
        }

        $topLinksArray = array();
        if ($topLinks = Yii::app()->user->settings->top_links) {
            $topLinks = CJSON::decode($topLinks);
            krsort($topLinks);
            foreach ($topLinks as $key => $value) {
                array_push($topLinksArray, array(
                        'label' => $value['label'],
                        'url' => $value['url'],
                        'itemOptions' => array('class'=>'hidden-xs'),
                        'linkOptions' => array('target' => $value['window']),
                    )
                );
            }
        }

        // multi account
        $multiAccountMenuLabel = '';
        $currentAccountId = Yii::app()->user->getContact()->currentAccountId;
        $linkedAccounts = Yii::app()->user->getContact()->linkedAccounts;
        if(!empty($linkedAccounts)):
            ob_start();
            ?>
            <div style="float: right; position: relative; left: auto; height: auto; padding: 0; margin: 0; min-height: 10px;">
                <select id="changeAccount" style="height: auto;">
                    <option value="0">Change Account</option>
                    <?php foreach($linkedAccounts as $linkedAccount):?>
                        <?php if($linkedAccount->account->id != $currentAccountId):?>
                            <option value="<?=$linkedAccount->account->id;?>"><?=$linkedAccount->account->notes;?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            </div>
            <?php
            $multiAccountMenuLabel = ob_get_contents();
            ob_end_clean();

            Yii::app()->clientScript->registerScript('changeAccountScript', <<<JS
                    $("#changeAccount").on('change',function() {
                        if($(this).val() == 0) {
                            Message.create("error","You must select an account to use");
                            return false;
                        }
                        window.location='/admin/authccd/auth/'+$(this).val();
                    });
JS
            );
        endif;

        $basicLinks = array(
            array(
                'label' => $multiAccountMenuLabel
            ),
            array(
                'label' =>
                    '<section id="autocomplete-search">
                            <div id="search-form">
                                <form method="POST" action="">
                                    <input type="text" name="query" id="query" autocomplete="off" placeholder="Search Name, Email Phone...">
                                </form>
                            </div>
                            <div id="results-container" style="position: relative;">
                                <div id="search-results" style="display: none; position: absolute; width: 100%; top: 0px; left: 0px; z-index: 1000;"></div>
                            </div>
                        </section>'
            ,
            ),
            array(
                'label' => 'Hi ' . Yii::app()->user->firstName,
                'itemOptions' => array('class' => 'topLinkName hidden-xs')
            ),
//				array(
//					'label' => '&nbsp;&nbsp;&nbsp;'
//				),
            array(
                'label' => 'Support',
                'itemOptions' => array('class'=>'hidden-xs'),
                'url' => array('/'.Yii::app()->controller->module->id.'/support')
            ),
//                array(
//                    'label' => 'Send Thank You!',
//                    'url' => array('/'.Yii::app()->controller->module->id.'/badges/addPoint/1')
//                ),
        );
        $topLinksArray = array_merge($basicLinks, $topLinksArray);

        return $topLinksArray;
    }

    public function getSavedHomesCount() {
        return SavedHomes::getCount($this->id);
    }

    /**
     * Extending this functionality so if no default value is passed it will return null instead of '/'
     * @author Chris Willard <chris@seizethemarket.com>
     * @since 1.0
     * @param null $defaultUrl
     * @return null|string
     */
    public function getReturnUrl($defaultUrl=null) {
        $returnUrl = null;

        $usersReturnUrlState = $this->getState('__returnUrl', $defaultUrl);
        if ($defaultUrl === null and $usersReturnUrlState) {
            $returnUrl = $usersReturnUrlState;
        } elseif ($defaultUrl) {
            $returnUrl = CHtml::normalizeUrl($defaultUrl);
        }

        return $this->getState('__returnUrl', $defaultUrl);
    }

    /**
     * Determine if the environment is running via the cli.
     * @return boolean
     */
    public function getIsCliEnv() {
        return (defined('STDIN')) ? true : false;
    }

}
