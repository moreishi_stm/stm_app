<?php

class ApiCriteriaBuilderBehavior extends CActiveRecordBehavior {


	public function getApiCriteria($apiData) {

		$ApiCriteria = new ApiCriteria;

		foreach ($apiData as $apiDataSet) {
			$Condition = new ApiCriteriaCondition;
			$Condition->attributes = $apiDataSet;
			$ApiCriteria->addApiCondition($Condition);
		}
		return $ApiCriteria;
	}

	public function getDataProviderFromApi($apiData) {

		if (get_class($apiData) == 'ApiCriteria')
			$ApiCriteria = $apiData;
		else
			$ApiCriteria = $this->getApiCriteria($apiData);

		if (!$ApiCriteria)
			throw new CHttpException(500, 'Could not get data provider.');

		$DataProvider = new CActiveDataProvider($this->owner, array('criteria'=>$ApiCriteria->dbCriteria,
		                                        					'sort' => array(
														                'defaultOrder'=>'t.price DESC',
														            ),
															  ));
		return $DataProvider;
	}

}

