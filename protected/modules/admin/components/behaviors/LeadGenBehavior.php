<?php
	Yii::import('front_widgets.PageViewTrackerWidget.PageViewTrackerWidget');
	Yii::import('front_widgets.RecentSavedHomesWidget.RecentSavedHomesWidget');

	class LeadGenBehavior extends CActiveRecordBehavior {

		// Defines the category name used when processing Yii logs
		const LOG_CATEGORY = 'leadgen';

		// Flag for if this object added a new contact to the system
		protected $newContact = false;
		protected $isResubmit;

		public function afterConstruct($event) {
			// Prevent this from being applied if the owner model does not implement the LeadGenInterface defined in this file
			if (!$this->owner instanceof LeadGenInterface) {
				Yii::log(get_class($this->owner) . ' must implement the LeadGenInterface, in order to apply the lead gen behavior.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
				$this->detach($this->owner);
			}
		}

		/**
		 * getLeadRoute
		 * Determines which lead route this lead falls into
		 *
		 * @since  12/20/2012
		 * @author Chris Willard
		 */
		public function getActiveLeadRoute($componentTypeId) {
			// First get the lead route by the component type of the attached model
			$LeadRoute = LeadRoutes::model()->byComponentTypeId($componentTypeId)->bySortAsc()->find();
			if (!$LeadRoute) {
				Yii::log(__CLASS__ . ': could not find lead route for component type id: ' . $componentTypeId, CLogger::LEVEL_ERROR, self::LOG_CATEGORY);

				return false;
			}

			return $LeadRoute;
		}

		/**
		 * getActingAgents
		 * Retrieves the acting agents for a specific lead route shift or round robin
		 *
		 * @param  LeadRoutes $LeadRoute
		 *
		 * @return Contacts The acting agent
		 */
		public function getActingAgents(LeadRoutes $LeadRoute) {
			// Storage for any acting agents
			$actingAgents = array();

			if ($LeadRoute->isShiftBased) {
				// Need to retrieve any shifts within the time block
				$Shifts = LeadShift::model()->byLeadRouteId($LeadRoute->id)->byCurrentDate()->withinCurrentTime()->findAll();
				if (!$Shifts) {
					// If we do not have any shifts then the overflow rules for the lead route apply
					$Agent = $LeadRoute->getOverflowAgent();
					array_push($actingAgents, $Agent);
				}

				foreach ($Shifts as $Shift) {
					if ($Shift->contact) {
						array_push($actingAgents, $Shift->contact);
					}
				}
			} else {
				// Round Robin Based... to be implemented
			}

			return ($actingAgents) ? $actingAgents : null;
		}

		/**
		 * @param $Agents
		 */
		protected function processLeadAlerts($Agents, $Contact) {

			// @todo: check for multiple agents and process a unique agent
			$ActingAgent = current($Agents);

			$leadStatus = ($this->isResubmit)? ' (Resubmit)':'';
			// No need to create a constant for PHP_EOL
			$message =
				' WebLead Alert!'. $leadStatus . PHP_EOL . 'Name: ' . $Contact->fullName . PHP_EOL . 'Phone: ' . Yii::app()->format->formatPhone($Contact->primaryPhone) . PHP_EOL . 'Email: ' . $Contact->primaryEmail;
			$gateway = MobileGateways::model()->findByPk($ActingAgent->settings->cell_carrier_id);
			$gateway = $ActingAgent->settings->cell_number . $gateway->sms_gateway;
			// Yii::app()->sms->sendMessage($gateway, $ActingAgent, $message);
			if (!YII_DEBUG) {
				mail($gateway, "", $message, "From: Leads@SeizetheMarket.com");
			} // temp until sms component is working

			// @todo: remove this later, I just hardcoded a CC to myself - - CLee
			/**/
			$mySelf = Contacts::model()->findByPk(1); // Contact model for myself
			/**/
			$myGateway = MobileGateways::model()->findByPk(4); // my cell carrier
			/**/
			$myGateway = $mySelf->settings->cell_number . $myGateway->sms_gateway;
			// /**/ Yii::app()->sms->sendMessage($myGateway, $mySelf, $message);

			if (!YII_DEBUG) {
				mail($myGateway, "", $message, "From: Leads@SeizetheMarket.com");
			} // temp until sms component is working
		}

		/**
		 * @param          $componentTypeId
		 * @param Contacts $ActingAgent
		 *
		 * @return Buyers|null|Sellers
		 */
		protected function createTransaction($componentTypeId, Contacts $Contact, Contacts $ActingAgent) {

			$transactionType = null;
			switch ($componentTypeId) {

				case ComponentTypes::BUYERS:
					$transactionType = Transactions::BUYERS;
					break;

				case ComponentTypes::SELLERS:
					$transactionType = Transactions::SELLERS;
					break;

				default:
					return null;
					break;
			}

			// Create the assignment (ActingAgent to transaction)
			$Assignment = new Assignments;
			$Assignment->assignee_contact_id = $ActingAgent->id;
			$Assignment->component_type_id = $componentTypeId;
			$assignmentTypeId = $this->getAssignmentType($componentTypeId);
			$Assignment->assignment_type_id = $assignmentTypeId;
			$Assignment->save(false);

			// Create the instance of the transaction
			$Transaction = Transactions::newInstance($transactionType);
			$Address = $Contact->getPrimaryAddress();
			if ($Address) {
				$Transaction->address_id = $Address->id;
			}
			$Transaction->contact_id = $Contact->id;
			$Transaction->component_type_id = $componentTypeId;
			$Transaction->transaction_status_id = TransactionStatus::NEW_LEAD_ID;
			$Transaction->source_id = Sources::ID_WEBSITE;
			$Transaction->save(false);

			// Update the assignment with the transaction id
			$Assignment->component_id = $Transaction->id;
			$Assignment->save(false);

			return $Transaction;
		}

		/**
		 * Returns the correct assignment type id based on the component id
		 *
		 * @param $componentTypeId
		 *
		 * @return mixed
		 */
		protected function getAssignmentType($componentTypeId) {

			$assignmentTypeId = null;
			switch ($componentTypeId) {
				case ComponentTypes::BUYERS:
					$assignmentTypeId = AssignmentTypes::BUYER_AGENT;
					break;

				case ComponentTypes::SELLERS:
					$assignmentTypeId = AssignmentTypes::LISTING_AGENT;
					break;

				default:
					$assignmentTypeId = AssignmentTypes::ASSIGNED_TO;
					break;
			}

			return $assignmentTypeId;
		}

		/**
		 * @param LeadRoutes          $LeadRoute
		 * @param Contacts            $ActingAgent
		 * @param StmBaseActiveRecord $Component
		 *
		 * @return bool
		 */
		protected function createLead(LeadRoutes $LeadRoute, Contacts $ActingAgent, $Component = null) {

			// If no component no need to create the lead.
			if (!$Component) {
				return;
			}

			// Use the first found acting agent
			// @todo: Add additional logic to determine who should receive the lead if multiple acting agents
			$newLead = new Lead;
			$newLead->lead_route_id = $LeadRoute->id;
			$newLead->origin_component_id = $this->owner->getComponentId();
			$newLead->origin_component_type_id = $this->owner->getComponentTypeId();
			$newLead->component_id = $Component->getComponentId();
			$newLead->component_type_id = $Component->getComponentTypeId();
			$newLead->assigned_to_id = $ActingAgent->id;

			return $newLead->save();
		}

		protected function createContact() {

			Yii::log(__CLASS__ . ': createContact() invoked....', CLogger::LEVEL_INFO, self::LOG_CATEGORY);

			$Contact = $this->owner->retrieveContactInstance();
			$Phone = $this->owner->retrievePhoneInstance();
			$Email = $this->owner->retrieveEmailInstance();

			$accountId = Yii::app()->user->accountId;

			// If the contact does not have a email object, it was just created
			if (!$Contact->emails) {
				$Email->account_id = $accountId;
				$Email->email_status_id = 1;
				$Email->email_type_ma = 1;
				$Email->owner_ma = 0;
				$Email->is_primary = 1;

				if ($Email->validate(false)) {
					$Contact->account_id = $accountId;
					$Contact->contact_status_ma = 1;

					if ($Contact->save(false)) {
						$Email->contact_id = $Contact->id;

						$validate = false; //Since we've already validated.
						$Email->save($validate);

						if ($Phone) {
							$Phone->contact_id = $Contact->id;
							$Phone->account_id = $accountId;
							$Phone->save(false);
						}
					} else {
						Yii::log('Unable to save contact to the system.', CLogger::LEVEL_WARNING, self::LOG_CATEGORY);
					}
				} else {
					Yii::log('Unable to create new contact due to invalid email address: ' . $Email->email, CLogger::LEVEL_WARNING, self::LOG_CATEGORY);
				}

				$this->newContact = true;
			} else {
				// Contact exists
				// Check for existing phone numbers if the phone number doesn't exist add it to the contact
				$existingPhone = Phones::model()->byPhoneNumber($Phone->phone)->byContactId($Contact->id)->find();
				if (!$existingPhone) {
					$Phone->contact_id = $Contact->id;
					$Phone->account_id = $accountId;
					$Phone->is_primary = 0; // This is an additional phone number
					$Phone->save(false);
				}
				// Check for existing e-mails
			}

			return $Contact;
		}

		protected function sendWelcomeEmail(Contacts $Contact, $ActingAgent, Transactions $Transaction) {

			// Send a welcome e-mail to the newly created contact
			$EmailMessage = new EmailMessage;
			$EmailMessage->subject = 'Your New Account Info!';
			$EmailMessage->component_type_id = $Transaction->component_type_id;
			$EmailMessage->component_id = $Transaction->id;
			$EmailMessage->to_email_id = $Contact->getPrimaryEmailObj()->id;
			$EmailMessage->from_email_id = $ActingAgent->getPrimaryEmailObj()->id;
			$EmailMessage->save();

			$message = new StmMailMessage;
			$message->view = 'welcomeEmail';
			$message->init($EmailMessage);

			Yii::app()->mail->send($message);
		}

		/**
		 * Applies the action plan to the transaction
		 *
		 * @param Transactions $transaction
		 *
		 * @return bool
		 */
		protected function applyActionPlan(Transactions $transaction) {

			$actionPlan = null;
			switch ($transaction->component_type_id) {
				case ComponentTypes::BUYERS:
					$actionPlan = ActionPlans::model()->findByPk(ActionPlans::NEW_BUYER_PLAN_ID);
					break;

				case ComponentTypes::SELLERS:
					$actionPlan = ActionPlans::model()->findByPk(ActionPlans::NEW_SELLER_PLAN_ID);
					break;

				default:
					return false;
					break;
			}

			$actionPlan->apply($transaction);
		}

        protected function afterSave($event) {

			if (!$this->owner->isNewRecord) {
				return parent::afterSave($event);
			}

			// getComponentTypeId() defined in parent model, must be implemented via the Lead Gen Interface
			$componentTypeId = $this->owner->getComponentTypeId();

			// Attempt to get the lead route, return false as to allow for alternative error handling
			$LeadRoute = $this->getActiveLeadRoute($componentTypeId);
			if (!$LeadRoute) {

				Yii::log('Lead route not found.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);

				return false;
			}

			// Get the working agents
			$Agents = $this->getActingAgents($LeadRoute);
			if (!$Agents) {

				Yii::log('Acting agents could not be established.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);

				return false;
			}

			// Get the acting agent
			// @todo: check for multiple agents and process a unique agent
			$ActingAgent = current($Agents);

			// Create the new contact and send a welcome e-mail containing their password (if they are new only)
			$Contact = $this->createContact();

			// Create the transaction
			$Transaction = null;
			$ExistingTransaction = Transactions::model()->byComponentTypeId($componentTypeId)->byContactId($Contact->id)->find();
			if (!$ExistingTransaction) {

				$Transaction = $this->createTransaction($componentTypeId, $Contact, $ActingAgent);
			} else {
				$this->isResubmit = true;
				// Generate a log that the lead gen behavior found an existing transaction and took no action.
				$TransactionExistingLog = new ActivityLog('leadGenCreation');
				$TransactionExistingLog->activity_date = 'now'; // The beforeSave() call on the ActivityLog model needs to be updated so we don't have to pass a string here
				$TransactionExistingLog->component_id = $ExistingTransaction->id;
				$TransactionExistingLog->component_type_id = $ExistingTransaction->component_type_id;
				$TransactionExistingLog->task_type_id = TaskTypes::RESUBMIT;
				$TransactionExistingLog->note = 'Re-submit: ' . $Contact->getFullName() . ', ' . $Contact->getPrimaryEmail() . ', ' . Yii::app()->format->formatPhone($Contact->getPrimaryPhone());
				$TransactionExistingLog->save(false);
			}

			// If a new contact was created by this lead then let's send them a welcome e-mail and log them in
			if ($this->newContact) {
				$Transaction = ($ExistingTransaction) ? $ExistingTransaction : $Transaction;
				$this->sendWelcomeEmail($Contact, $ActingAgent, $Transaction);
				// Will probably want to clean this portion up a bit
				// This handles logining the user in automatically
				$LoginForm = new LoginForm;
				$LoginForm->email = $Contact->getPrimaryEmail();
				$LoginForm->password = $Contact->password;

				if ($LoginForm->validate() && $LoginForm->login()) {
					PageViewTrackerWidget::processLogin();
					RecentSavedHomesWidget::processLogin();
					Contacts::updateLastLogin($Contact);
					LoginLog::processLogin();
				}
			}

			// Apply action plan if necessary
			//$this->applyActionPlan($Transaction);

			// Create the lead
			$this->createLead($LeadRoute, $ActingAgent, $Transaction);

			// Notify agents of the new lead
			$this->processLeadAlerts($Agents, $Contact);
		}
	}