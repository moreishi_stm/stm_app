<?php
/**
 * Model Attribute Behavior
 * Handles the getting of Model Attributes (_ma) fields
 */
class GetTasksBehavior extends CActiveRecordBehavior {

	public function getTasks($parentModel, $includeCompletedTasks = false) {
		$Criteria = new CDbCriteria;
		$Criteria->condition = 'component_id = :component_id AND component_type_id = :component_type_id';

		if (!$includeCompletedTasks) {
			$Criteria->condition = $Criteria->condition . ' AND (complete_date is NULL || complete_date = 0)';
		}

		$Criteria->params = array(
			':component_id' => $parentModel->id, ':component_type_id' => $parentModel->componentType->id
		);

		$Criteria->order = 'due_date asc';
		$Tasks = new Tasks;
		$Tasks = $Tasks->findAll($Criteria);

		return $Tasks;
	}
}
