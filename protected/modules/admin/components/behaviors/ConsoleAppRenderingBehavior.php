<?php
	Yii::import('yii_framework.web.*');
	Yii::import('yii_framework.web.renderers.*');

	class ConsoleAppRenderingBehavior extends CBehavior {

		public function getThemeManager() {
			$themeManager = new CThemeManager();

			$scriptFile = Yii::app()->getRequest()->getScriptFile();

			//Note: We want to go one directory above as most console apps are run from domain/protected/yiic
			//and we want the themes directory which is under the domain subtree
			$basePath = dirname($scriptFile) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . CThemeManager::DEFAULT_BASEPATH;

			$themeManager->setBasePath($basePath);

			return $themeManager;
		}

		public function getWidgetFactory() {
			return new CWidgetFactory();
		}

		public function getViewRenderer() {
			return new CPradoViewRenderer();
		}

		public function getTheme() {
			return $this->getThemeManager()->getTheme('default');
		}

		public function getViewPath() {
			return Yii::getPathOfAlias('stm_app.modules.admin.views');
		}

		public function getSession() {
			return new CHttpSession();
		}
	}