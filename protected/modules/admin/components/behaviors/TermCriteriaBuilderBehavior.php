<?php
	/**
	 * Class TermCriteriaBuilderBehavior
	 * Description: This can be attached to Featured Areas, Saved Search or MlsProperties
	 */
	class TermCriteriaBuilderBehavior extends CActiveRecordBehavior {

		protected $_criteriaCollection = array();
		protected $_masterCriteria;
		protected $_secondaryMlsTableApplied = false;
        protected $_secondaryFields;

		public $board_name;
		public $listing_id;
        public $agent_id;
		public $price_min;
		public $price_max;
		public $sq_feet;
        public $sq_feet_max;
		public $bedrooms;
		public $baths_total;
		public $neighborhood;
		public $year_built;
		public $pool;
		public $new_construction_yn;
		public $navig_waterfront;
		public $foreclosure;
		public $short_sale;
        public $school;
		public $elementary_school;
		public $middle_school;
		public $high_school;
        public $school_district;
        public $property_type;
        public $property_types;
        public $acreage_min;
        public $acreage_max;
        public $master_bedroom_level;
        public $street_name;
        public $region;
        public $city;
        public $county;
        public $zip;
        public $fifty_five_plus_community_yn;
        public $membership_required_yn;
        public $stories;

		public function getCriteria($componentTypeId, $componentId) {
			$TermComponentCollection = TermComponentLu::model()->findAll(array(
					'condition' => 'component_type_id = :component_type_id AND component_id=:component_id AND group_id is NULL',
					'params' => array(
						':component_type_id' => $componentTypeId,
						':component_id' => $componentId
					),
				)
			);

			// do more stuff to build criteria
			if ($TermComponentCollection) {
				foreach ($TermComponentCollection as $TermComponent) {
					// builds criteria for each term and adds it to the collection to be processed by buildCriteria later
					$this->addCriteriaToCollection($this->buildCriteriaFromTermComponentLu($TermComponent));
				}
			}

			return $this->buildMasterCriteria();
		}

		protected function buildCriteriaFromTermComponentLu(TermComponentLu $TermComponent, CDbCriteria $GroupCriteria = null) {
			// Find out of TermComponent has any records grouped with itself (group_id)
			$GroupTermComponents = TermComponentLu::model()->findAll(array(
					'condition' => 'component_type_id = :component_type_id AND component_id=:component_id AND group_id=:group_id',
					'params' => array(
						':component_type_id' => $TermComponent->component_type_id,
						':component_id' => $TermComponent->component_id,
						':group_id' => $TermComponent->id
					)
				)
			);
			if (count($GroupTermComponents) > 0) {
				$GroupCriteria = new CdbCriteria;
				$GroupCriteria = $this->getCriteriaByTermId($TermComponent->term_id, $TermComponent->value);

				//@todo: groups need a sort order?? cuz if x=1 OR y=2 AND z=3 OR a=4.... order matters? ... do we even need this, is it overkill?
				foreach ($GroupTermComponents as $GroupTermComponent) { //recursion happens here using conjunctor in current record vs. previous (refactored) - CLee
                    $CriteriaB = $this->buildCriteriaFromTermComponentLu($GroupTermComponent, $GroupCriteria);
					$GroupCriteria->mergeWith($CriteriaB, $GroupTermComponent->term_conjunctor);
				}
				$Criteria = $GroupCriteria;
			} elseif ($GroupCriteria) {
				$SingleTermCriteria = $this->getCriteriaByTermId($TermComponent->term_id, $TermComponent->value, $TermComponent->term_conjunctor, $TermComponent->id);
				$GroupCriteria->mergeWith($SingleTermCriteria, $TermComponent->term_conjunctor);
				$Criteria = $GroupCriteria;
			} else {
				$Criteria = $this->getCriteriaByTermId($TermComponent->term_id, $TermComponent->value);
			}

			return $Criteria;
		}

		public function addCriteriaByTermId($id, $value = null, $paramSuffix=null) { //need paramSuffix due to the way mergeWith ignores identical conditions

			if (!is_array($this->_criteriaCollection)) {
				$this->_criteriaCollection = array();
			}

			$Criteria = $this->getCriteriaByTermId($id, $value, $paramSuffix);

			return $this->addCriteriaToCollection($Criteria);
		}

		public function addCriteriaToCollection($Criteria) {
			return array_push($this->_criteriaCollection, $Criteria);
		}

		/**
		 * Called by Featured Area, Saved Serach, searchbox via lookup table or user input that may provide dynamic values.
		 *
		 * @param      $name
		 * @param null $value
		 *
		 * @return int
		 */
		public function addCriteriaByTermName($name, $value = null) {

			if (!is_array($this->_criteriaCollection)) {
				$this->_criteriaCollection = array();
			}

			$Criteria = $this->getCriteriaByTermName($name, $value);

			return array_push($this->_criteriaCollection, $Criteria);
		}

		public function getCriteriaByTermName($name, $value = null) {

			$model = Terms::model()->findByName($name);

			return $this->getCriteriaByTermId($model->id, $value);
		}

		public function getCriteriaByTermId($id, $value = null, $conjunctor = 'AND', $paramSuffix=null) {
			// NOTE: The current structure assumes that terms will not have a nested structure. If this changes, this function should have a recursive feature that builds it out.
			$Criteria = new CDbCriteria;
			$model = Terms::model()->findByPk($id);
			$TermCriteriaCollection = $model->termCriteria;

            if(empty($TermCriteriaCollection)) {
                Yii::log(__FILE__.'('.__LINE__."): ERROR: Missing criteria records for term# {$model->id}: {$model->name}.", CLogger::LEVEL_ERROR);
            }

			foreach ($TermCriteriaCollection as $TermCriteriaModel) {
				if($CriteriaFromModel = $this->buildCriteriaFromTermCriteriaModel($TermCriteriaModel, $value, $paramSuffix)) {
					$Criteria->mergeWith($CriteriaFromModel, $TermCriteriaModel->default_conjunctor);
				}
			}

			return $Criteria;
		}

		public function buildCriteriaFromTermCriteriaModel(TermCriteria $model, $value, $paramSuffix=null) {
			if ($model->type_ma == TermCriteria::TYPE_DYNAMIC) {
				if ($value == null) {
                    return null;
					//throw new CHttpException(500, 'Dynamic Value for Term "' . $model->term->name . '" is missing.');
				}
			} else {
				$value = $model->constant_value;
			}

			$Criteria = new CDbCriteria;

            // change ( to CAST(
            $model->column = (strpos($model->column, 'CAST (') !== false) ? str_replace('CAST (', 'CAST(', $model->column) : $model->column;

            // extract column name if cast
            $columnName = (strpos($model->column, 'CAST(') !== false) ? trim(substr($model->column, strpos(strtoupper($model->column), 'CAST(') + 5, strpos(strtoupper($model->column), ' AS ') - 5)) : $model->column;

			// checks to see if the field is in the secondary table and joins if needed.
			if ($SecondaryCriteria = $this->checkSecondaryField($columnName)) {
                if($SecondaryCriteria !== true) {
                    $model_2 = MlsPropertiesSecondary::model($this->getBoardName());
                    $tableName = $this->getBoardName() . MlsPropertiesSecondary::getTableSuffix();
        //			$model->column = $tableName.'.'.$model->column; //@todo: Is this necessary if all column names are unique? (except the foreign key)
                }
			} else {
				//check if column exists in main table
				$MlsProperties = MlsProperties::model($this->getBoardName());
				if(!$MlsProperties->hasAttribute($columnName)) {
                    Yii::log(__FILE__.'('.__LINE__."): Unexpected ERROR: Column {$columnName} not exist in MlsProperties. Please investigate and fix.",CLogger::LEVEL_ERROR);
					return null;
				}
			}

            // special case for listing_id ONLY
            if($model->column == 'listing_id') {
                $model->column = 't.listing_id';
            }

			switch ($model->operator->symbol) {

				case Operators::LIKE:
					$Criteria->compare($model->column, trim($value), $partialMatch = true, $model->default_conjunctor, $escape = false);
					break;

				case Operators::LIKE_WILDCARD:
					$Criteria->mergeWith(array(
							'condition' => $model->column . ' like :' . $model->column.$paramSuffix,
							'params' => array(':' . $model->column.$paramSuffix => '%' . trim($value) . '%')
						)
					);
					break;

				case Operators::IN:
                    $value = explode(',', $value);
					$Criteria->addInCondition($model->column, trim($value), $model->default_conjunctor);
					break;

				case Operators::NOT_IN:
					$Criteria->addNotInCondition($model->column, trim($value), $model->default_conjunctor);
					break;

				case Operators::GT:
				case Operators::LT:
				case Operators::GTE:
				case Operators::LTE:
					$Criteria->addCondition($model->column . " " . $model->operator->symbol . ':tc'.$model->id, $model->default_conjunctor); //" '$value'"
                    $Criteria->params[':tc'.$model->id] = trim($value);
					break;

				default:
					$Criteria->compare($model->column, trim($value), $partialMatch = false, $model->default_conjunctor, $escape = false);
					break;
			}

			if ($SecondaryCriteria && ($SecondaryCriteria !== true)) {
				$Criteria->mergeWith($SecondaryCriteria);
			}

			return $Criteria;
		}

		protected function buildMasterCriteria() {
			$this->_masterCriteria = new CDbCriteria;
			if (!$this->_criteriaCollection) {
				return $this->_masterCriteria;
			}

			foreach ($this->_criteriaCollection as $Criteria) {
				$this->_masterCriteria->mergeWith($Criteria);
			}

			return $this->_masterCriteria;
		}

		/**
		 * checkSecondaryField checks to see if field is in the secondary table and joins it as needed
		 *
		 * @param  [type] $name [description]
		 *
		 * @return mixed: null or CdbCriteria
		 */
		public function checkSecondaryField($column_name) {
			if ($this->_secondaryMlsTableApplied) {
				return true;
			}

			if (in_array($column_name, $this->getSecondaryFields())) {
                $tableName = $this->getBoardName() . MlsPropertiesSecondary::getTableSuffix();

				$Criteria = new CDbCriteria(array(
					'join' => 'LEFT JOIN ' . $tableName . ' ON t.listing_id=' . $tableName . '.listing_id',
				));
				$this->_secondaryMlsTableApplied = true;

				return $Criteria;
			}

			return null;
		}

		/**
		 * getSecondaryFields returns array of secondary fields for MlsPropertiesSecondary
		 *
		 * @return array of field names
		 */
		public function getSecondaryFields() {
            if(!empty($this->_secondaryFields)) {
                return $this->_secondaryFields;
            }

			$model = MlsPropertiesSecondary::model($this->getBoardName());
			$attributes = $model->attributes;
			$data = array();
			foreach ($attributes as $key => $name) {
				array_push($data, $key);
			}

			return $this->_secondaryFields = $data;
		}

        /**
         * setSecondaryFields in the case where bulk processing of term criteria, this is used to prevent the repeated DB call of MlsPropertiesSecondary. Set this in cache in the beginning for efficiency.
         *
         * @return none
         */
        public function setSecondaryFields($data) {
            $this->_secondaryFields = $data;
            return;
        }

		public function getDataProvider(CDbCriteria $Criteria) {

            //@todo: determine whether user has multiple boards or not
//            if(Yii::app()->user->hasMultipleBoards()) {
//                $mlsBoard = MlsBoards::model()->findByPk($this->mls_board_id);
//            }
//            else {
//                $mlsBoard = Yii::app()->user->board;
//            }
//
//            $boardPrefixName = $mlsBoard->getPrefixedName();
//            $this->board_name = $boardPrefixName;
//            $properties = MlsProperties::model($boardPrefixName);

			$model = MlsProperties::model($this->getBoardName());

			$DataProvider = new CActiveDataProvider($model, array(
				'criteria' => $Criteria,
				'sort' => array(
					'defaultOrder' => 't.price DESC',
				),
			));

			return $DataProvider;
		}


        public function getListRowSelect($boardPrefix=null, $stateShortName=null, $includeOffice=true, $mlsBoard=null,  $domainName=null) {
            if(defined('STDIN')) {
                // use the passed in params
                if(!$boardPrefix) {
                    Yii::log(__CLASS__.' ('.__FILE__.') Board prefix not provided in CLI environment.', CLogger::LEVEL_ERROR);
                    Yii::app()->end();
                }
                if(!$stateShortName) {
                    Yii::log(__CLASS__.' ('.__FILE__.') State Short Name not provided in CLI environment.', CLogger::LEVEL_ERROR);
                    Yii::app()->end();
                }
                if(!$mlsBoard) {
                    Yii::log(__CLASS__.' ('.__FILE__.') MLS Board not provided in CLI environment.', CLogger::LEVEL_ERROR);
                    Yii::app()->end();
                }
                if(!$domainName) {
                    Yii::log(__CLASS__.' ('.__FILE__.') Domain Name not provided in CLI environment.', CLogger::LEVEL_ERROR);
                    Yii::app()->end();
                }
                $hasPhotoUrl = $mlsBoard->has_photo_url;

            } else {
                $boardPrefix = Yii::app()->user->board->getPrefixedName();
                $stateShortName = Yii::app()->user->board->getStateShortName($lowercase=true);
                $hasPhotoUrl = Yii::app()->user->board->has_photo_url;
                if(!$domainName) {
                    $domainName = Yii::app()->user->domain->name;
                }
            }

            if($hasPhotoUrl) {
                $photoSelect = '(select url from '.$boardPrefix.'_property_photos where listing_id=t.listing_id AND photo_number=1 LIMIT 1) as firstPhotoUrl';
            } else {
                $photoSelect = 'CONCAT("http://mlsimages.seizethemarket.com/'.$boardPrefix.'/",t.mls_property_type_id,"/",t.listing_id,"/",t.listing_id,"-1.jpg") as firstPhotoUrl';
            }

            $selectString = '*, CONCAT("'.$stateShortName.'") as stateShortName, '.$photoSelect;

            if($includeOffice) {
                $selectString .= ', (select name from '.$boardPrefix.'_offices where office_id=t.listing_office_id LIMIT 1) as listingOfficeName';
            }
            return $selectString;
        }

        /**
		 * @param CDbCriteria $Criteria
		 *
		 * @return mixed
		 */
		public function findAllProperties(CDbCriteria $Criteria, $limitOfProperties=50) {
			return MlsProperties::model($this->getBoardName())->byLimit($limitOfProperties)->findAll($Criteria);
		}

		/**
		 * @param CDbCriteria $Criteria
		 *
		 * @return string
		 */
		public function findAllPropertiesCount(CDbCriteria $Criteria) {
			return MlsProperties::model($this->getBoardName())->count($Criteria);
		}

		/**
		 * @param int $limit
		 * @return $this
		 */
		public function byLimit($limit=50) {
			$this->owner->getDbCriteria()->mergeWith(array(
					'limit' => $limit,
				));

			return $this->owner;
		}

		private function getBoardName() {
			if (defined('STDIN') || !empty($this->board_name)) {
				$boardName = $this->board_name;
			} else {
				$boardName = Yii::app()->user->board->prefixedName;
			}

			return $boardName;
		}
	}