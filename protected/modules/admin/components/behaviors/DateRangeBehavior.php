<?php
/**
 * Model Attribute Behavior
 * Handles the getting of Model Attributes (_ma) fields
 */
class DateRangeBehavior extends CActiveRecordBehavior {

    public function byDateRange($dateRange,$field) {
        $dateRange = Yii::app()->format->formatDateRangeArray($dateRange);

        $criteria = new CDbCriteria;
        $criteria->condition = 'date('.$field.') >=:from_date AND date('.$field.') <=:to_date';
        $criteria->params = array(
            ":from_date" => $dateRange['from_date'],
            ":to_date" => $dateRange['to_date'],
        );

        $this->getDbCriteria()->mergeWith($criteria);

        return $this;
    }
}

/**
 * DIRECTIONS on how to implement a model attribute:
 * 1) Example: field name is 'setting_type_ma'
 * 2)
 *
 *    getSettingTypes - model attribute setting_type_ma
 *
 *    public function getSettingTypes()
 *    {
 *        return array(
 *            self::SETTING_TYPE_ACCOUNT_ID =>'Account',
 *            self::SETTING_TYPE_CONTACT_ID =>'Contact',
 *            self::SETTING_TYPE_BOTH_ID    =>'Both',
 *        );
 *    }
 *
 * 3) Usage would be:             name of getter method, field value from model
 *         $model->getModelAttribute("getDueReferences", $model->setting_type_ma)
 */