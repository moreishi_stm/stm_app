<?php
/**
 * Forces constraints on the owner object for children who extend this class
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class ContactTrackingBehavior extends CActiveRecordBehavior
{

    /**
     * Forces the owner to be a instance of the "Contacts" model
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return Contacts
     * @throws CHttpException
     */
    public function getOwner()
    {

        if (!parent::getOwner() instanceof Contacts) {
            throw new CHttpException(500, get_called_class() . ' must be attached to a instance of the "Contacts" model.');
        }

        return parent::getOwner();
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function __toString()
    {

        return __CLASS__;
    }
}