<?php
Yii::import('admin_module.components.behaviors.contacts.ContactTrackingBehavior');

/**
 * Helper, applied on TrackingController's TaskAction
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class ContactTaskTrackingBehavior extends ContactTrackingBehavior
{

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param Contacts $contact
     * @param null     $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getCountCompletedTasks($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Tasks::model()->count(
            "assigned_to_id=:assigned_to_id AND complete_date >=:complete_from AND DATE(complete_date) <=:complete_to AND is_deleted <> 1",
            array(":assigned_to_id" => $this->owner->id, ":complete_from" => $dateRangeFormatOpts['from_date'], ":complete_to" => $dateRangeFormatOpts['to_date'],)
        );
    }

    public function getCountIncompleteTasks($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Tasks::model()->count(
            "assigned_to_id=:assigned_to_id AND (complete_date is NULL OR complete_date = 0) AND is_deleted <> 1",
            array(":assigned_to_id" => $this->owner->id)
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getCountFutureTasks($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        $Criteria            = new CDbCriteria;
        $Criteria->condition = 'assigned_to_id=:assigned_to_id AND complete_date IS NULL AND DATE(added)<=:added AND t.is_deleted <> 1';
        $Criteria->join      = 'LEFT JOIN action_plan_applied_lu ON t.id = action_plan_applied_lu.task_id';
        $Criteria->params    = array(":assigned_to_id" => $this->owner->id, ":added" => $dateRangeFormatOpts['to_date'],);

        // @todo: exlude emails from action plans (relationship with aciton plan lu)
        return Tasks::model()->count($Criteria);
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getCountOverdueTasks($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Tasks::model()->count(
            "complete_date IS NULL AND assigned_to_id=:assigned_to_id AND DATE(due_date)<:due_date AND added<=:added AND is_deleted <> 1",
            array(":assigned_to_id" => $this->owner->id, ":due_date" => $dateRangeFormatOpts['to_date'], ":added" => $dateRangeFormatOpts['to_date'],)
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getScheduledFollowupTasks($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return $this->getCountNewTasks($dateRangeFormatOpts) + $this->getCountUpdatedTasks($dateRangeFormatOpts);
        //		return Tasks::model()->count("assigned_to_id=:assigned_to_id AND ((added >=:added_from) OR (complete_date IS NULL AND added < updated AND updated >=:update_from AND updated <=:updated_to))",
        //									array(":assigned_to_id"=>$this->owner->id,
        //									      ":added_from"=>date("Y-m-d H:i:s",strtotime("-30 days")),
        //									      ":update_from"=>date("Y-m-d H:i:s",strtotime("-30 days")),
        //											":updated_to"=>date("Y-m-d H:i:s"),
        //									));
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getCountNewTasks($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Tasks::model()->count(
            "assigned_to_id=:assigned_to_id AND added >=:added_from AND DATE(added) <=:added_to AND is_deleted <> 1", array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => $dateRangeFormatOpts['to_date'],)
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getCountUpdatedTasks($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Tasks::model()->count(
            "complete_date IS NULL AND assigned_to_id=:assigned_to_id AND added < updated AND updated >=:updated_from AND DATE(updated) <=:updated_to AND is_deleted <> 1",
            array(":assigned_to_id" => $this->owner->id, ":updated_from" => $dateRangeFormatOpts['from_date'], ":updated_to" => $dateRangeFormatOpts['to_date'],)
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountActivitiesLogged($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

//        $taskTypesNotIn = TaskTypes::BAD_PHONE.','.TaskTypes::AUTO_EMAIL_DRIP.','.TaskTypes::RESUBMIT;
        $taskTypesIn = TaskTypes::PHONE.','.TaskTypes::EMAIL_MANUAL.','.TaskTypes::TODO.','.TaskTypes::TEXT_MESSAGE.','.TaskTypes::NOTE.','.TaskTypes::HANDWRITTEN_NOTE.','.TaskTypes::MAIL.','.TaskTypes::WEB_CHAT;

        return ActivityLog::model()->count(
            "added_by=:assigned_to_id AND DATE(added) >=:added_from AND DATE(added) <=:added_to AND task_type_id IN($taskTypesIn) AND is_deleted <> 1", array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => $dateRangeFormatOpts['to_date'],)
        );
    }

    public function getCountActivitiesLoggedPerWeekday($dateRangeFormatOpts = null)
    {
        $count = $this->getCountActivitiesLogged($dateRangeFormatOpts);

        $start = $dateRangeFormatOpts['from_date'];
        $end = $dateRangeFormatOpts['to_date'];
        $weekdaysCount = 0;
        while($start<=$end && $start <= date('Y-m-d')){
            if(date('w',strtotime($start))<6 && date('w',strtotime($start))>0){
                $weekdaysCount++;
            }
            $currDate = $start = date('Y-m-d', strtotime($start.' +1 day'));
        }
        //@todo: put login in here for counting TODAY if after noon & timezone sensitive.

        return ($weekdaysCount) ? number_format($count/$weekdaysCount) : 0;
    }
}