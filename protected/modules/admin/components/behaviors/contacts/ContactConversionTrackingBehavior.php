<?php
Yii::import('admin_module.components.behaviors.contacts.ContactTrackingBehavior');

/**
 * Helper, applied on TrackingController's ConversionAction
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class ContactConversionTrackingBehavior extends ContactTrackingBehavior
{

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getActualLeadGen($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        $tableName           = Timeblockings::model()->tableName();

        $result                                    = Timeblockings::model()->findBySql(
            'select SUM(TIME_TO_SEC(TIMEDIFF(end_time,start_time))) AS queryCount from `' . $tableName
                . '` `t` WHERE t.contact_id=:contact_id AND t.date>=:from_date AND DATE(t.date)<=:to_date AND timeblock_type_ma IN(:timeblockLeadgenPhone, :timeblockLeadgenEmail, :timeblockLeadgenRecruits) and t.is_deleted <> 1',
            array(':contact_id'            => $this->owner->id,
                  ':from_date' => $dateRangeFormatOpts['from_date'],
                  ':to_date' => $dateRangeFormatOpts['to_date'],
                  ':timeblockLeadgenPhone' => Timeblockings::LEAD_GEN_TYPE_PHONE_ID,
                  ':timeblockLeadgenEmail' => Timeblockings::LEAD_GEN_TYPE_EMAIL_ID,
                  ':timeblockLeadgenRecruits' => Timeblockings::LEAD_GEN_PHONE_RECRUITING_ID,            )
        );
        $dateRangeFormatOpts['decimalConditional'] = ($dateRangeFormatOpts['decimalConditional']) ? $dateRangeFormatOpts['decimalConditional'] : true;
        $dateRangeFormatOpts['decimalPlaces']      = ($dateRangeFormatOpts['decimalPlaces']) ? $dateRangeFormatOpts['decimalPlaces'] : 2;

        return Yii::app()->format->formatSecToHours($result->queryCount, $dateRangeFormatOpts);
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountAskForReferrals($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return ActivityLog::model()->count(
            "added_by=:assigned_to_id AND added >=:added_from AND added <:added_to AND asked_for_referral=:asked_for_referral AND t.is_deleted <> 1",
            array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => date('Y-m-d', strtotime($dateRangeFormatOpts['to_date']. '+1 days')), ":asked_for_referral" => 1,)
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return float|null
     */
    public function getCountSignedAppointmentsPercentage($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        if ($signed = $this->getCountSignedAppointments($dateRangeFormatOpts)) {
            return ($signed / $this->getCountMetAppointments($dateRangeFormatOpts));
        } else {
            return null;
        }

    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountSignedAppointments($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Appointments::model()->count(
            "met_by_id=:met_by_id AND met_status_ma=:met_status_ma AND set_for_datetime>=:set_for_datetime_from AND set_for_datetime<:set_for_datetime_to AND is_signed=1 AND t.is_deleted <> 1",
            array(":met_by_id"           => $this->owner->id, ":met_status_ma" => Appointments::MET_STATUS_MET, ":set_for_datetime_from" => $dateRangeFormatOpts['from_date'],
                  ":set_for_datetime_to" => date('Y-m-d', strtotime($dateRangeFormatOpts['to_date']. '+1 days')),)
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return int|mixed|string
     */
    public function getContactsPerSetAppointment($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts                       = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        $dateRangeFormatOpts['decimalConditional'] = ($dateRangeFormatOpts['decimalConditional']) ? $dateRangeFormatOpts['decimalConditional'] : true;
        $dateRangeFormatOpts['decimalPlaces']      = ($dateRangeFormatOpts['decimalPlaces']) ? $dateRangeFormatOpts['decimalPlaces'] : 1;

        if ($countSetAppointments = $this->getCountSetAppointments($dateRangeFormatOpts)) {
            $countTotalContacts = $this->getCountTotalContacts($dateRangeFormatOpts);
            if (empty($countSetAppointments)) {
                return null;
            }
            $result = number_format($countTotalContacts / $countSetAppointments, $dateRangeFormatOpts['decimalPlaces']);

            if ($dateRangeFormatOpts['decimalConditional'] == true) {
                $result = str_replace('.0', '', number_format($result, $dateRangeFormatOpts['decimalPlaces']));
            }

            return $result;
        } else {
            return null;
        }
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountMetAppointments($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Appointments::model()->count(
            "met_by_id=:met_by_id AND met_status_ma=:met_status_ma AND set_for_datetime>=:set_for_datetime_from AND set_for_datetime<:set_for_datetime_to AND t.is_deleted <> 1",
            array(":met_by_id"           => $this->owner->id, ":met_status_ma" => Appointments::MET_STATUS_MET, ":set_for_datetime_from" => $dateRangeFormatOpts['from_date'],
                  ":set_for_datetime_to" => date('Y-m-d', strtotime($dateRangeFormatOpts['to_date']. '+1 days')),)
        );
    }

    public function getPercentageSetToMetAppointments($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts['to_date'] = ($dateRangeFormatOpts['to_date']>date('Y-m-d')) ? date('Y-m-d') : $dateRangeFormatOpts['to_date'];
        if($setAppt = $this->getCountSetForAppointments($dateRangeFormatOpts)) {
            if($metAppt = $this->getCountMetAppointments($dateRangeFormatOpts)) {
                $percentMet = Yii::app()->format->formatPercentages($metAppt/$setAppt);
            }
        }
        return $percentMet;
    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountDials($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Yii::app()->db->createCommand()
            ->select('count(id)')
            ->from('activity_log a')
            ->where('a.task_type_id IN('.TaskTypes::PHONE.','.TaskTypes::DIALER_ANSWER.','.TaskTypes::DIALER_NO_ANSWER.','.TaskTypes::DIALER_BAD_NUMBER_DELETED.')')
            ->andWhere('a.added_by='.$this->owner->id)
            ->andWhere('DATE(a.added)>=:added_from', array(':added_from'=>$dateRangeFormatOpts['from_date']))
            ->andWhere('DATE(a.added)<=:added_to', array(':added_to'=>$dateRangeFormatOpts['to_date']))
            ->andWhere('a.lead_gen_type_ma IN ('.ActivityLog::LEAD_GEN_NEW_ID.','.ActivityLog::LEAD_GEN_FOLLOW_UP_ID.')')
            ->andWhere('a.is_deleted=0')
            ->queryScalar();

//        return ActivityLog::model()->count(
//            "task_type_id IN(:task_type_id) AND added_by=:assigned_to_id AND DATE(added) >=:added_from AND DATE(added) <=:added_to AND (lead_gen_type_ma=:lead_gen_new OR lead_gen_type_ma=:lead_gen_follow_up) AND t.is_deleted <> 1",
//            array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => $dateRangeFormatOpts['to_date'], ":lead_gen_new" => ActivityLog::LEAD_GEN_NEW_ID,":lead_gen_follow_up"=>ActivityLog::LEAD_GEN_FOLLOW_UP_ID, ":task_type_id"=> TaskTypes::PHONE.','.TaskTypes::DIALER_ANSWER.','.TaskTypes::DIALER_NO_ANSWER.','.TaskTypes::DIALER_BAD_NUMBER_DELETED)
//        );
    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getDialsPerHour($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        if($actualLeadGen = $this->getActualLeadGen($dateRangeFormatOpts)) {
            return round($this->getCountDials($dateRangeFormatOpts) / $actualLeadGen ,1);
        }

    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountTotalContacts($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return $this->getCountNewContacts($dateRangeFormatOpts) + $this->getCountFollowupContacts($dateRangeFormatOpts);
    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getContactsPerHour($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        if($actualLeadGen = $this->getActualLeadGen($dateRangeFormatOpts)) {
            return round($this->getCountTotalContacts($dateRangeFormatOpts) / $actualLeadGen ,1);
        }

    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getDialsPerContact($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        if($totalContacts = $this->getCountTotalContacts($dateRangeFormatOpts)) {
            return round(($this->getCountDials($dateRangeFormatOpts) / $totalContacts),1);
        }

    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountNewContacts($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        // select count(*) from (select DATE(activity_date), component_id from `activity_log` WHERE component_id=32082 AND activity_date > '2014-10-27' AND is_spoke_to=1  GROUP BY DATE(activity_date), component_id) a
//        $manualDialCount = Yii::app()->db->createCommand()
//            ->select("count(*)")
//            ->from("activity_log")
//            ->where("added_by=:assigned_to_id AND activity_date >=:added_from AND DATE(activity_date) <=:added_to AND lead_gen_type_ma=".ActivityLog::LEAD_GEN_NEW_ID." AND is_spoke_to=1 AND call_id IS NULL AND is_deleted <> 1", array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => $dateRangeFormatOpts['to_date']))
//            ->queryScalar();
//
//        $dialerCount = Yii::app()->db->createCommand()
//            ->select("count(distinct call_id)")
//            ->from("activity_log")
//            ->where("added_by=:assigned_to_id AND activity_date >=:added_from AND DATE(activity_date) <=:added_to AND lead_gen_type_ma=".ActivityLog::LEAD_GEN_NEW_ID." AND is_spoke_to=1 AND call_id IS NOT NULL AND is_deleted <> 1", array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => $dateRangeFormatOpts['to_date']))
//            ->queryScalar();
//
//        return ($manualDialCount + $dialerCount);

        return Yii::app()->db->createCommand()
            ->select("count(*)")
            ->from("(select DATE(activity_date) from `activity_log` WHERE added_by=".$this->owner->id." AND (lead_gen_type_ma=".ActivityLog::LEAD_GEN_NEW_ID." OR task_type_id IN (".TaskTypes::PHONE.",".TaskTypes::DIALER_ANSWER."))AND DATE(activity_date) >= '".$dateRangeFormatOpts['from_date']."' AND DATE(activity_date) <= '".$dateRangeFormatOpts['to_date']."'AND is_spoke_to=1  GROUP BY DATE(activity_date), component_type_id, component_id) a")
            ->queryScalar();

    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountFollowupContacts($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

//        $manualDialCount = Yii::app()->db->createCommand()
//            ->select("count(*)")
//            ->from("activity_log")
//            ->where("added_by=:assigned_to_id AND activity_date >=:added_from AND DATE(activity_date) <=:added_to AND lead_gen_type_ma=".ActivityLog::LEAD_GEN_FOLLOW_UP_ID." AND is_spoke_to=1 AND call_id IS NULL AND is_deleted <> 1", array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => $dateRangeFormatOpts['to_date']))
//            ->queryScalar();
//
//        $dialerCount = Yii::app()->db->createCommand()
//            ->select("count(distinct call_id)")
//            ->from("activity_log")
//            ->where("added_by=:assigned_to_id AND activity_date >=:added_from AND DATE(activity_date) <=:added_to AND lead_gen_type_ma=".ActivityLog::LEAD_GEN_FOLLOW_UP_ID." AND is_spoke_to=1 AND call_id IS NOT NULL AND is_deleted <> 1", array(":assigned_to_id" => $this->owner->id, ":added_from" => $dateRangeFormatOpts['from_date'], ":added_to" => $dateRangeFormatOpts['to_date']))
//            ->queryScalar();
//
//        return ($manualDialCount + $dialerCount);
        return Yii::app()->db->createCommand()
            ->select("count(*)")
            ->from("(select DATE(activity_date) from `activity_log` WHERE added_by=".$this->owner->id." AND (lead_gen_type_ma=".ActivityLog::LEAD_GEN_FOLLOW_UP_ID." OR task_type_id IN(".TaskTypes::PHONE.",".TaskTypes::DIALER_ANSWER.")) AND DATE(activity_date) >= '".$dateRangeFormatOpts['from_date']."' AND DATE(activity_date) <= '".$dateRangeFormatOpts['to_date']."'AND is_spoke_to=1  GROUP BY DATE(activity_date), component_type_id, component_id) a")
            ->queryScalar();
    }

    public function getCountOpportunities($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Opportunities::model()->count(
            "added_by=:added_by AND DATE(added)>=:addedFrom AND DATE(added)<=:addedTo AND t.is_deleted = 0",
            array(":added_by"            => $this->owner->id,
                  ":addedFrom" => $dateRangeFormatOpts['from_date'], ":addedTo" => $dateRangeFormatOpts['to_date'])
        );
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountSetAppointments($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Appointments::model()->count(
            "set_by_id=:set_by_id AND DATE(set_on_datetime)>=:set_on_datetime_from AND DATE(set_on_datetime)<=:set_on_datetime_to AND t.is_deleted <> 1",
            array(":set_by_id"            => $this->owner->id,
                  ":set_on_datetime_from" => $dateRangeFormatOpts['from_date'], ":set_on_datetime_to" => $dateRangeFormatOpts['to_date'])
        );
    }

    public function getCountSetForAppointments($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Appointments::model()->count(
            "set_by_id=:set_by_id AND DATE(set_for_datetime)>=:set_for_datetime_from AND DATE(set_for_datetime)<=:set_for_datetime_to AND t.is_deleted <> 1",
            array(":set_by_id"            => $this->owner->id,
                  ":set_for_datetime_from" => $dateRangeFormatOpts['from_date'], ":set_for_datetime_to" => $dateRangeFormatOpts['to_date'])
        );
    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getHoursPerSetAppointment($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        if($setAppts = $this->getCountSetAppointments($dateRangeFormatOpts)) {
            return round(($this->getActualLeadGen($dateRangeFormatOpts) / $setAppts),1);
        }

    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return int|mixed|string
     */
    public function getContactsPerMetAppointment($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts                       = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        $dateRangeFormatOpts['decimalConditional'] = ($dateRangeFormatOpts['decimalConditional']) ? $dateRangeFormatOpts['decimalConditional'] : true;
        $dateRangeFormatOpts['decimalPlaces']      = ($dateRangeFormatOpts['decimalPlaces']) ? $dateRangeFormatOpts['decimalPlaces'] : 1;

        if ($apptCount = $this->getCountMetAppointments($dateRangeFormatOpts)) {
            $result = number_format(($this->getCountTotalContacts($dateRangeFormatOpts) / $this->getCountMetAppointments($dateRangeFormatOpts)), $dateRangeFormatOpts['decimalPlaces']);

            if ($dateRangeFormatOpts['decimalConditional'] == true) {
                $result = str_replace('.0', '', number_format($result, $dateRangeFormatOpts['decimalPlaces']));
            }

            return $result;
        } else {
            return 0;
        }
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed
     */
    public function getCountExecutedContracts($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts              = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        $dateRangeFormatOpts['contactId'] = $this->owner->id;

        return Closings::model()->getCountExecutedByAssigneeId($dateRangeFormatOpts);
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return mixed|string
     */
    public function getAppointmentsPerSale($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts                       = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        $dateRangeFormatOpts['decimalConditional'] = ($dateRangeFormatOpts['decimalConditional']) ? $dateRangeFormatOpts['decimalConditional'] : true;
        $dateRangeFormatOpts['decimalPlaces']      = ($dateRangeFormatOpts['decimalPlaces']) ? $dateRangeFormatOpts['decimalPlaces'] : 1;

        if ($apptCount = $this->getCountClosings($dateRangeFormatOpts)) {
            $result = number_format(($this->getCountMetAppointments($dateRangeFormatOpts) / $this->getCountClosings($dateRangeFormatOpts)), $dateRangeFormatOpts['decimalPlaces']);

            if ($dateRangeFormatOpts['decimalConditional'] == true) {
                $result = str_replace('.0', '', number_format($result, $dateRangeFormatOpts['decimalPlaces']));
            }

            return $result;
        } else {
            return '-';
        }
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountClosings($dateRangeFormatOpts = null)
    {

        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Closings::model()->countBySql(
            "SELECT count(*) as countTotal FROM `closings` c left join transactions t on c.transaction_id=t.id left join assignments a on a.component_id=t.id WHERE c.closing_status_ma=:closing_status_ma AND `contract_closing_date` >=:actual_closing_datetime_from AND `contract_closing_date` <:actual_closing_datetime_to AND a.assignee_contact_id=:assignee_contact_id AND a.component_type_id in(:buyer_type,:seller_type) AND c.is_deleted <> 1",
            array(":assignee_contact_id"          => $this->owner->id, ":closing_status_ma" => Closings::STATUS_TYPE_CLOSED_ID, ":buyer_type" => ComponentTypes::BUYERS,
                  ":seller_type"                  => ComponentTypes::SELLERS, ":actual_closing_datetime_from" => $dateRangeFormatOpts['from_date'],
                  ":actual_closing_datetime_to"   => date('Y-m-d', strtotime($dateRangeFormatOpts['to_date']. '+1 days')),)
        );
    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountLeadsByDateRange($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Transactions::model()->byAssignedTo($this->owner->id)->byDateRange($dateRangeFormatOpts, 'added')->count();
    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountLeads()
    {
        return Transactions::model()->byAssignedTo($this->owner->id)->count();
    }

    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param null $dateRangeFormatOpts
     *
     * @return string
     */
    public function getCountNoSavedSearches($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Transactions::model()->byAssignedTo($this->owner->id)->byNoSavedSearches()->byDateRange($dateRangeFormatOpts, 't.added')->notTrash()->count();
    }

    public function getCountCalledLeads($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Transactions::model()->byAssignedTo($this->owner->id)->byCalled()->byDateRange($dateRangeFormatOpts, 't.added')->count();
    }

    public function getCountNotCalledLeads($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Transactions::model()->byAssignedTo($this->owner->id)->byNotCalled()->notTrash()->byDateRange($dateRangeFormatOpts, 't.added')->count();
    }

    public function getCountSpokeToLeads($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Transactions::model()->byAssignedTo($this->owner->id)->bySpokeTo()->byDateRange($dateRangeFormatOpts, 't.added')->count();
    }

    public function getCountNoActivityLeads($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Transactions::model()->byAssignedTo($this->owner->id)->byNoActivity()->byDateRange($dateRangeFormatOpts, 't.added')->count();
    }

    public function getCountNoEmailsLeads($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);
        $leadCount = $this->getCountLeadsByDateRange($dateRangeFormatOpts);
        $hasEmailCount = Transactions::model()->byAssignedTo($this->owner->id)->byHasEmailSent()->byDateRange($dateRangeFormatOpts, 't.added')->count(array('select'=>'t.id'));
        return $leadCount - $hasEmailCount;
    }

    public function getCountLeadSetAppointment($dateRangeFormatOpts = null)
    {
        $dateRangeFormatOpts = Yii::app()->format->formatDateRangeArray($dateRangeFormatOpts);

        return Transactions::model()->byAssignedTo($this->owner->id)->bySetAppointment($dateRangeFormatOpts, $setById=$this->owner->id)->byDateRange($dateRangeFormatOpts, 't.added')->count(array('select'=>'t.id'));
    }
}