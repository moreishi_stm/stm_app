<?php
/**
 * Model Attribute Behavior
 * Handles the getting of Model Attributes (_ma) fields
 */
class ModelAttributeBehavior extends CActiveRecordBehavior {

	public function getModelAttribute($getterFunctionName, $value) {
		if (!method_exists($this->owner, $getterFunctionName)) {
			return null;
		}

		// Get the results from the getter
		$getterResults = $this->owner->{$getterFunctionName}();

		return (!empty($getterResults[$value])) ? $getterResults[$value] : null;
	}
}

/**
 * DIRECTIONS on how to implement a model attribute:
 * 1) Example: field name is 'setting_type_ma'
 * 2)
 *
 *    getSettingTypes - model attribute setting_type_ma
 *
 *    public function getSettingTypes()
 *    {
 *        return array(
 *            self::SETTING_TYPE_ACCOUNT_ID =>'Account',
 *            self::SETTING_TYPE_CONTACT_ID =>'Contact',
 *            self::SETTING_TYPE_BOTH_ID    =>'Both',
 *        );
 *    }
 *
 * 3) Usage would be:             name of getter method, field value from model
 *         $model->getModelAttribute("getDueReferences", $model->setting_type_ma)
 */