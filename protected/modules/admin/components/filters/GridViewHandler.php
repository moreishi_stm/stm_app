<?php
/**
 *
 */
class GridViewHandler extends CFilter {

    protected function preFilter($filterChain) {
        if (Yii::app()->request->getIsAjaxRequest() && isset($_GET['ajax'])) {
            $selectedTable = $_GET['ajax'];
            $gridViewMethod = '_getGridView'.$selectedTable;
            if (method_exists($filterChain->controller, $gridViewMethod)) {
                $filterChain->controller->$gridViewMethod();
                Yii::app()->end();
            }
        }

        return true;
    }
}
