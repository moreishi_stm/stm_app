<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 7/3/13
 */
class StmAccessControl extends CAccessControlFilter {

	/** @var $guestAllowedActions array */
	public $guestAllowedActions = array();
	public $moduleName = 'admin';

	/**
	 * Used to automatically control access to controllers and actions via our RBAC manager
	 * @param CFilterChain $filterChain
	 * @return bool
	 */
	protected function preFilter($filterChain) {

		/** @var CController $controller */
		$controller = $filterChain->controller;

		/** @var CAction $action */
		$action = $filterChain->action;

		$controllerId = $controller->getId();
		$actionId = $action->getId();

		/** @var StmWebUser $currentUser */
		$currentUser = Yii::app()->user;
		$userHasControllerAccess = $currentUser->checkAccess($controllerId);
		if (!$userHasControllerAccess) {
			$actionAuthItemName = $controllerId.strtoupper($actionId{0}).substr($actionId, 1).'Action';
			$userHasActionAccess = $currentUser->checkAccess($actionAuthItemName);
		}

		// Check for controller specific permissions
		$guestManuallyAllowed = ($this->guestAllowedActions && in_array($actionId, $this->guestAllowedActions));

		/**
		 * If the logged in user has controller level access (via task auth items) then allow them passage
		 * If the logged in user has action level access (via operation auth items) then allow them passage
		 * Otherwise deny access
		 */
        if($contact = Yii::app()->user->contact) {
            $itemNames = $contact->getItemNames();
        }

        if($itemNames && in_array('inactive', $itemNames) && Yii::app()->controller->action->id !== 'logout') { //
            if(Yii::app()->controller->module->id == 'front') {
                $redirectUrl = '/front/site/login';
            }
            else {
				if(Yii::app()->request->isAjaxRequest) {
					$this->_sendJsonLogoutMessage();
				}
				else {
					$redirectUrl = '/admin/logout';
				}
            }
            $controller->redirect(array($redirectUrl));
        }

		if (!($userHasControllerAccess || $userHasActionAccess || $guestManuallyAllowed) && Yii::app()->controller->action->id !== 'logout') {

			// If the user is a guest, then just redirect them to the login
			if (Yii::app()->user->isGuest) {
				if (!$guestManuallyAllowed) {
					Yii::app()->user->setReturnUrl($_SERVER['REQUEST_URI']);
				}
				if(Yii::app()->request->isAjaxRequest) {
					$this->_sendJsonLogoutMessage();
				}
				else {
					$controller->redirect(array('main/index'));
				}
			}
            if(in_array('inactive', $itemNames)) {

                if(Yii::app()->controller->module->id == 'front') {
                    $redirectUrl = '/front/site/login';
                }
                else {
					if(Yii::app()->request->isAjaxRequest) {
						$this->_sendJsonLogoutMessage();
					}
					else {
						$redirectUrl = '/admin/logout';
					}
                }
                $controller->redirect(array($redirectUrl));
            }

            if(!in_array('admin',$itemNames) && !in_array('owner', $itemNames) && !in_array('agent', $itemNames) && !in_array('sales', $itemNames) && Yii::app()->controller->module->id == 'admin') {
                $controller->redirect(array('/admin/logout'));
            }

            $controller->layout = 'plain';
			$controller->render('/main/accessDenied', array());
			return false;
		}

		return true;
	}

	/**
	 * Send JSON Logout Message
	 *
	 * Outputs JSON logout message and terminates application
	 * @param array $data Array of data
	 */
	protected function _sendJsonLogoutMessage()
	{
		// Set header and output JSON data
		header('HTTP/1.0 403 Forbidden');

		// Display message of forbiddenness
		echo ('403 Forbidden');

		// Terminate application
		Yii::app()->end();
	}
}