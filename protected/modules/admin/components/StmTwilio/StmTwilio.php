<?php
	class StmTwilio extends CApplicationComponent {

		public $sid;
		public $token;
		protected $client;

		public function init()
		{
			parent::init();

//            $this->sid = (YII_DEBUG) ? "" : "";
//            $this->token = (YII_DEBUG) ? "" : "";
//            $capability = new Services_Twilio_Capability('AC35d8934c5baa6c3efbd2a385f66129e4', 'c2b8598778253c1073833c95561007bc');
//            $capability->allowClientOutgoing(YII_DEBUG ? 'APca36c3cd123e1875d60d06395cfb73cf' : 'APadf38643641936d6c833d5fc66d8b70c');  // Nicole's Test Application for development


            $baseDir = dirname(__FILE__).'/twilio-php';

			require($baseDir . '/Services/Twilio/Resource.php');
			require($baseDir . '/Services/Twilio/ListResource.php');
			require($baseDir . '/Services/Twilio/UsageResource.php');
            require($baseDir . '/Services/Twilio/InstanceResource.php');
            require($baseDir . '/Services/Twilio/NextGenListResource.php');
            require($baseDir . '/Services/Twilio/NextGenInstanceResource.php');

			foreach (glob($baseDir."/Services/Twilio/*.php") as $filename)
			{
				include_once $filename;
			}
			foreach (glob($baseDir . "/Services/*.php") as $filename)
			{
				include $filename;
			}
			foreach (glob($baseDir . "/Services/Twilio/Rest/*.php") as $filename)
			{
				include $filename;
			}
            foreach (glob($baseDir . "/Services/Twilio/Rest/Lookups/*.php") as $filename)
            {
                include $filename;
            }
			//instantiate Twillio
//			if(!$this->client) {
//				$this->client = new Services_Twilio($this->sid, $this->token);
//			}
		}
        protected function getCurlResponse($url)
        {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $this->sid.":".$this->token);

            return curl_exec($ch);
        }

		public function sendSms($toNumber, $fromNumber) {

			$this->client = new Services_Twilio($this->sid, $this->token);

			$message = $this->client->account->sms_messages->create(
				$fromNumber,//+15005550000 or 9047704151
				$toNumber,
				"Hello how are you?"
			);

			return $message->sid;
		}

        public function validatePhone($phone) {

            if(strlen($phone) !== 10) {
                throw new Exception(__CLASS__.' (:'.__LINE__.') Phone must be 10 digits. Phone: '.$phone);
            }

            // check our central db for existing validation
            if($phoneValidated = PhonesValidated::model()->findByAttributes(array('phone'=> $phone))) {
                $data = array(
                    'caller_name' => $phoneValidated->caller_name,
                    'carrier' => (object) array('name' => $phoneValidated->carrier_name, 'type' => $phoneValidated->type)
                );
                return (object) $data;
            }

            if(YII_DEBUG) {
                return null;
            }

            // can't use test account tokens, only works with production tokens
            $this->sid = "AC35d8934c5baa6c3efbd2a385f66129e4";
            $this->token = "c2b8598778253c1073833c95561007bc";

            // data->carrier = [name="Verizon Wireless",type="mobile", error_code=null] | type: mobile, voip, landline | data->caller_name = string | null | ERROR: status, more_info, code, message
            // NOTE: using curl cuz the Twilio PHP SDK does not work for phone validation
            $data = json_decode($this->getCurlResponse("https://lookups.twilio.com/v1/PhoneNumbers/+1".intval($phone).'?CountryCode=US&Type=carrier'));

            if($data->error_code) {
                Yii::log(__CLASS__." (:".__LINE__.") Phone Validation Check had an Error. Please investigate. Returned Data: ".print_r($data, true), CLogger::LEVEL_ERROR);
            }
            // store in our central db for reuse validation
            $dataToStore = new PhonesValidated();
            $dataToStore->setAttributes(array(
                    'phone' => $phone,
                    'type' => $data->carrier->type,
                    'caller_name' => $data->caller_name,
                    'carrier_name' => $data->carrier->name,
                    'mobile_network_code' => $data->carrier->mobile_network_code,
                    'mobile_country_code' => $data->carrier->mobile_country_code,
                    'error_code' => $data->error_code,
                    'error_message' => $data->error_message,
                    'status' => $data->status,
                    'vendor' => 'Twilio',
                ));
            if(!$dataToStore->save()) {
                Yii::log(__CLASS__." (:".__LINE__.") Phone Validated model did not save. Error Message: ".print_r($dataToStore->getErrors(), true).PHP_EOL."Data Attributes: " . print_r($dataToStore->attributes, true), CLogger::LEVEL_ERROR);
            }

            return $data;
        }
	}