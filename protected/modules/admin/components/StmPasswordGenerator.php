<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/12/13
 */
class StmPasswordGenerator extends CApplicationComponent {

	public $passwordCharsLCase = "abcdefghijkmnopqrstwxyz";
	public $passwordCharsUCase = "ABCDEFGHJKLMNPQRSTWXYZ";
	public $passwordCharsNumbers = "23456789";

	/**
	 * Returns a password of given $length
	 * @param int $length
	 *
	 * @return string
	 */
	public function generate($length = 6) {
		$allPossibleCharacters = $this->passwordCharsLCase.$this->passwordCharsUCase.$this->passwordCharsNumbers;
		$allCharactersList = str_split($allPossibleCharacters);

		$password = '';
		for ($charIdx = 0; $charIdx < $length; $charIdx++) {
			$passwordPosIdx = rand(0, (count($allCharactersList) - 1));
			$password .= $allCharactersList[$passwordPosIdx];
		}

		return $password;
	}
}