$(function () {

    function webrtcNotSupportedAlert() {
        alert("Your browser doesn't support WebRTC. You need Chrome 25 to use this demo");
    }

    function isNotEmpty(n) {
        return n.length > 0;
    }

    function onReady() {
        console.log("onReady...");
        console.log('Login');
        login();
    }

    function login() {
        var username = 'app131114051947';
        var password = 'stmapp';
        Plivo.conn.login(username, password);
    }

    function logout() {
        Plivo.conn.logout();
    }

    function onLogin() {
        console.log('Logged in');
    }

    function onLoginFailed() {
        console.log("Login Failed");
    }

    function onLogout() {

    }

    function onCalling() {
        console.log("onCalling");
        console.log('Connecting....');
    }

    function onCallRemoteRinging() {
        console.log('Ringing..');
    }

    function onCallAnswered() {
        console.log('onCallAnswered');
        console.log('Call Answered');
    }

    function onCallTerminated() {
        console.log("onCallTerminated");
        Message.create("notice", "Call Ended.");
    }

    function onCallFailed(cause) {
        console.log("onCallFailed:" + cause);
        console.log("Call Failed:" + cause);
        hangup();
    }

    function hangup() {
        Plivo.conn.hangup();
    }

    function dtmf(digit) {
        console.log("send dtmf=" + digit);
        Plivo.conn.send_dtmf(digit);
    }

    function mute() {
        Plivo.conn.mute();
    }

    function unmute() {
        Plivo.conn.unmute();
    }

    function onIncomingCall(account_name, extraHeaders) {
        console.log("onIncomingCall:" + account_name);
        console.log("extraHeaders=");
        for (var key in extraHeaders) {
            console.log("key=" + key + ".val=" + extraHeaders[key]);
        }
    }

    function onIncomingCallCanceled() {

    }

    function onMediaPermission(result) {
        if (result) {
            console.log("get media permission");
        } else {
            alert("you don't allow media permission, you will can't make a call until you allow it");
        }
    }

    function answer() {
        console.log("answering")
        Plivo.conn.answer();
    }

    function reject() {
        Plivo.conn.reject();
    }

    /**
     * Run Plivo
     */
    Plivo.onWebrtcNotSupported = webrtcNotSupportedAlert;
    Plivo.onReady = onReady;
    Plivo.onLogin = onLogin;
    Plivo.onLoginFailed = onLoginFailed;
    Plivo.onLogout = onLogout;
    Plivo.onCalling = onCalling;
    Plivo.onCallRemoteRinging = onCallRemoteRinging;
    Plivo.onCallAnswered = onCallAnswered;
    Plivo.onCallTerminated = onCallTerminated;
    Plivo.onCallFailed = onCallFailed;
    Plivo.onMediaPermission = onMediaPermission;
    Plivo.onIncomingCall = onIncomingCall;
    Plivo.onIncomingCallCanceled = onIncomingCallCanceled;
    Plivo.init();
});

