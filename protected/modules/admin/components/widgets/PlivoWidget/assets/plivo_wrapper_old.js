/**
 * Trial number: 17162261110
 */
$(function() {
    /**
     * Build the available call actions html
     */
    var callOptions = {
        '': '',
        'voip-call': 'Call This Number',
        'voip-hangup': 'Hang Up',
        'voip-calllist-add': 'Add Number to Call List'
    }
    var callActionsSelect = $("<select data-phone='{phone}' class='voip-actions' />");
    for (var callOptionIdx in callOptions) {
        $("<option />", {value: callOptionIdx, text: callOptions[callOptionIdx]}).appendTo(callActionsSelect);
    }
    var callActionsSelectHtml = callActionsSelect.clone().wrap('<p>').parent().html();

    /**
     * Replace all of the phone numbers with the call actions select
     */
    var phoneNumberRegex = /((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}/g;
    var text = $("body:first").html();
    text = text.replace(phoneNumberRegex, '<span class="phone-number">$&</span> ' + callActionsSelectHtml);
    $("body:first").html(text);

    /**
     * Process the call actions
     */
    $('select.voip-actions').each(function() {
        if ($(this).attr('data-phone') == '{phone}') {
            var cleanPhoneNumber = $(this).parent().find('.phone-number').text();
            cleanPhoneNumber = '1'+cleanPhoneNumber.replace(/[() -]/g, '');
            $(this).attr('data-phone', cleanPhoneNumber);
        }
        $(this).find("option[value='voip-hangup']").hide();
    }).change(function() {
        switch ($(this).val()) {
            case 'voip-call':
                $.proxy(call, $(this))();
            break;

            case 'voip-hangup':
                $.proxy(hangup, $(this))();
            break;

            case 'voip-calllist-add':

            break;
        }
    });

    /**
     * resets the call action select options
     */
    function resetCallActionsGui() {
        $("select.voip-actions").each(function() {
            $(this).find("option[value='voip-hangup']").hide();
            $(this).find("option[value='voip-call']").show();
            $(this).parent().parent().find('select').each(function() {
                $(this).find("option:eq(0)").text('') ;
                $(this).prop('disabled', false);
            });
            $(this).val(0);
        });
    }

    function webrtcNotSupportedAlert() {
        alert("Your browser doesn't support WebRTC. You need Chrome 25 to use this demo");
    }

    function isNotEmpty(n) {
        return n.length > 0;
    }

    function onReady() {
        console.log("onReady...");
        console.log('Login');
        login();
    }

    function login() {
        var username = 'app131114051947';
        var password = 'stmapp';
        Plivo.conn.login(username, password);
    }

    function logout() {
        Plivo.conn.logout();
    }

    function onLogin() {
        console.log('Logged in');
    }

    function onLoginFailed() {
        console.log("Login Failed");
    }

    function onLogout() {

    }

    function onCalling() {
        console.log("onCalling");
        console.log('Connecting....');
    }

    function onCallRemoteRinging() {
        console.log('Ringing..');
    }

    function onCallAnswered() {
        console.log('onCallAnswered');
        console.log('Call Answered');
    }

    function onCallTerminated() {
        console.log("onCallTerminated");
        Message.create("notice", "Call Ended.");
        resetCallActionsGui();
    }

    function onCallFailed(cause) {
        console.log("onCallFailed:"+cause);
        console.log("Call Failed:"+cause);
        hangup();
    }

    function call() {
        Message.create("notice", "Call Starting.");
        var dest = $(this).attr('data-phone');
        if (isNotEmpty(dest)) {
            console.log('Calling..');
            console.log(dest);
            Plivo.conn.call(dest);
        }
        else{
            console.log('Invalid Destination');
        }

        $(this).find("option:selected").hide();
        $(this).find("option[value='voip-hangup']").show();
        $(this).find("option:eq(0)").text('***Call in Progress***') ;
        $(this).parent().parent().find('select').not($(this)).each(function() {
            $(this).find("option:eq(0)").text('***Line in Use***') ;
            $(this).prop('disabled', true);
        });
        $(this).val(0);
    }

    function hangup() {
        Plivo.conn.hangup();
        resetCallActionsGui();
    }

    function dtmf(digit) {
        console.log("send dtmf="+digit);
        Plivo.conn.send_dtmf(digit);
    }

    function mute() {
        Plivo.conn.mute();
    }

    function unmute() {
        Plivo.conn.unmute();
    }

    function onIncomingCall(account_name, extraHeaders) {
        console.log("onIncomingCall:"+account_name);
        console.log("extraHeaders=");
        for (var key in extraHeaders) {
            console.log("key="+key+".val="+extraHeaders[key]);
        }
    }

    function onIncomingCallCanceled() {
        
    }

    function  onMediaPermission (result) {
        if (result) {
            console.log("get media permission");
        } else {
            alert("you don't allow media permission, you will can't make a call until you allow it");
        }
    }

    function answer() {
        console.log("answering")
        Plivo.conn.answer();
    }

    function reject() {
        Plivo.conn.reject();
    }

    /**
     * Run Plivo
     */
    Plivo.onWebrtcNotSupported = webrtcNotSupportedAlert;
    Plivo.onReady = onReady;
    Plivo.onLogin = onLogin;
    Plivo.onLoginFailed = onLoginFailed;
    Plivo.onLogout = onLogout;
    Plivo.onCalling = onCalling;
    Plivo.onCallRemoteRinging = onCallRemoteRinging;
    Plivo.onCallAnswered = onCallAnswered;
    Plivo.onCallTerminated = onCallTerminated;
    Plivo.onCallFailed = onCallFailed;
    Plivo.onMediaPermission = onMediaPermission;
    Plivo.onIncomingCall = onIncomingCall;
    Plivo.onIncomingCallCanceled = onIncomingCallCanceled;
    Plivo.init();
})