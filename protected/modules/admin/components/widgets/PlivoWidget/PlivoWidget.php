<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class PlivoWidget extends CWidget {

    private $_assetPath;

    public function run() {

        $this->_assetPath = Yii::app()->getAssetManager()->publish(dirname(__FILE__).DS.'assets');
        Yii::app()->getClientScript()->registerScriptFile($this->_assetPath.DS.'plivo_wrapper.js');
        Yii::app()->getClientScript()->registerScriptfile($this->_assetPath.DS.'plivo.min.js');
    }
}