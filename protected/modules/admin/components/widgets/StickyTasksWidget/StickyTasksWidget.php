<?php

class StickyTasksWidget extends CWidget {

	public function init() {
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_widgets.StickyTasksWidget.assets.css'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
		Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'stickyTasks.css');
	}

	public function run() {

		$Tasks = Tasks::model()->assignedToCurrentUser()->today()->asc()->notCompleted();
		$DataProvider = new CActiveDataProvider($Tasks, array(
			'pagination' => array(
				'pageSize' => 5,
			),
		));

		$this->render('stickyTasksWidget', array('DataProvider'=>$DataProvider));
	}
}