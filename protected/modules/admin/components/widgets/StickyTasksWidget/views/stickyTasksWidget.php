<?php
// Checks to see if a activity log dialog already exists and creates one only if it doesn't exist.
Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
        'id'             => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
        'title'          => 'Complete Task & Log Activity',
        'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
    ));
}

$css = <<<CSS
	div#sticky-task-list div.items ul li:first-child {
		border: 1px solid #000;
	}
	div#sticky-task-list.list-view .summary {
		text-align: center !important;
		clear: both;
	}
	div#sticky-task-list.list-view-loading {
		background: none !important;
		margin: 0 !important;
	}
	div#sticky-task-list div.pager {
		float: none !important;
	}
CSS;
Yii::app()->clientScript->registerCss('stickyTaskCss', $css);
?>
<div id="sticky-tasks-widget">
	<div id="check-icon">&#x2713;<br /><span>Done</span></div>
	<h2>My Tasks:</h2>
	<div class="sticky-tasks-body">
		<?php
			$this->widget('front_module.components.StmListView', array(
			    'dataProvider'  => $DataProvider,
			    'id'            => 'sticky-task-list',
			    'itemView'      => '_stickyTaskRow',
				'summaryText'	=> 'Showing {start}-{end} of {pages} results.',
				'itemsTagName' => 'ul',
				'pager' => array(
					'maxButtonCount' => 0,
					'header'         => '',
					'lastPageLabel'  => '',
					'firstPageLabel' => '',
				),
			));
		?>
	</div>
</div>
