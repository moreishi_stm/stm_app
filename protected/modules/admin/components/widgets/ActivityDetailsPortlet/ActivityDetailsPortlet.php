<?php
	Yii::import('admin_module.models.interfaces.Component');

	class ActivityDetailsPortlet extends StmBasePortlet {

		public $containerId = 'activity-details';
		public $containerClass = 'g100 p-p0 p-mv10 p-mr0';
		public $handleTitle = 'Activity Details';
		public $handleIconCssClass = 'i_mouse';
		public $handleButtons = array( // array(
			// 	'label'        => 'Send Email',
			// 	'iconCssClass' => 'i_stm_mail',
			// 	'htmlOptions'  => array('id' => 'send-email-button'),
			// 	'type'         => 'button',
			// 	'name'         => 'send-email-button',
			// 	// 'display'      => 'inline',
			// ),
			// array(
			// 	'label'        => 'Add Document',
			// 	'iconCssClass' => 'i_stm_add',
			// 	'htmlOptions'  => array('id'=>'add-document-button'),
			// 	'type'         => 'button',
			// 	'name'         => 'add-document',
			// 	// 'display'      => 'inline',
			// )
		);

		public $handleButtonBreak = true;

		public $componentType;
		public $model;
		public $leftActiveTab = 0;
		const LEFT_ACTIVE_TAB_DOCS = 3;

		public function init() {
			$this->registerAssets();
			parent::init();
		}

		public function renderContent() {
			if (!($this->model instanceof Component)) {
				throw new Exception('ActivityDetailsPortlet CANNOT be used on a non Stm Component Object.');
			}

			$referralsProvider = '';

			$SavedHomeSearches = new SavedHomeSearches;
			$SavedHomeSearches->contact_id = $this->model->getContactId();
            $SavedHomeSearches->component_type_id = $this->componentType->id;
            $SavedHomeSearches->component_id = $this->model->id;

			$tasksProvider = Tasks::getAll($this->model->id, $this->componentType->id);
			$tasksContent = $this->controller->renderPartial('../tasks/_listMini', array(
					'parentModel' => $this->model,
					'dataProvider' => $tasksProvider,
					'componentTypeId' => $this->componentType->id,
					'componentId' => $this->model->id,
				), true
			);

			$actionPlanProvider = ActionPlans::getAppliedPlans($this->model->componentType->id, $this->model->id);
			$actionPlansContent = $this->controller->renderPartial('../actionPlans/_listMiniAppliedPlans', array(
					'dataProvider' => $actionPlanProvider,
					'parentComponentId' => $this->model->id,
					'parentComponentTypeId' => $this->model->componentType->id
				), true
			);
            $actionPlansActiveCount = ActionPlans::getAppliedPlans($this->model->componentType->id, $this->model->id, false)->itemCount;

//			$documentsProvider = Documents::getAll($this->model->componentType->id, $this->model->id);
//			$documentsContent = $this->controller->renderPartial('../documents/_listMini', array(
//				   'dataProvider' => $documentsProvider,
//				   'componentId' => $this->model->id,
//				   'componentTypeId' => $this->model->componentType->id
//				   ), true
//			);
			if(Yii::app()->user->getState('activityDetailPortletLeftTabView') == self::LEFT_ACTIVE_TAB_DOCS) {
				$this->leftActiveTab = self::LEFT_ACTIVE_TAB_DOCS;
				Yii::app()->user->setState('activityDetailPortletLeftTabView',null);
			}
            $tnxCount = Transactions::getCount($this->model->getContactId());

            //@todo: hacky for Companies - need more input
            if($this->model->componentType->id != ComponentTypes::COMPANIES) {
                $referralCount = Yii::app()->db->createCommand('select count(*) from referrals where contact_id='.$this->model->getContactId())->queryScalar();
            }
            $transactionReferralCount = $referralCount + $tnxCount;
			// homes viewed, saved homes, documents, action plans & edrips, transactions, referrals
			$this->render('activityDetailsPortlet', array(
					'componentType' => $this->model->componentType,
					'model' => $this->model,
					'contactId' => $this->model->getContactId(),
					// 'tasksCount' => Tasks::getCount($this->model->component_type_id, $this->model->id, true),
					'tasksCount' => $tasksProvider->totalItemCount,
					'tasksContent' => $tasksContent,
					'actionPlansContent' => $actionPlansContent,
                    'actionPlansActiveCount' => $actionPlansActiveCount,
					'homesViewedCount' => ViewedPages::getCount($this->model->getContactId(), 1, true),
					'homesSavedCount' => SavedHomes::getCount($this->model->getContactId(), true),
					'savedHomeSearchCount' => ($this->model->componentType->id == ComponentTypes::COMPANIES) ? null : SavedHomeSearches::getCount($this->model->contact->id, true),
					'savedHomeSearchContent' => ($this->model->componentType->id == ComponentTypes::COMPANIES) ? null : $this->controller->renderPartial('../savedHomeSearch/_listMini', array('model' => $SavedHomeSearches, 'componentTypeId'=>$this->model->componentType->id, 'componentId'=>$this->model->id), true),
					'transactionsCount' => '('.$transactionReferralCount.')',
					'documentsCount' => Documents::getCount($this->model->id, $this->model->componentType->id, true),
//					'documentsContent'=>$documentsContent,
					'referralsProvider' => $referralsProvider,
					'leftActiveTab'=>$this->leftActiveTab,
				)
			);
		}

		protected function registerAssets() {
			$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_widgets.ActivityDetailsPortlet.assets.css'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
			Yii::app()->clientScript->registerCssFile($cssAssetsUrl . DS . 'activityDetails.css');
		}
	}
