<a name="activityDetails-docs"></a>
<div class="res-section res-group">
    <!-- ====== LOG LEFT STARTS =============================================================================================== -->

    <?php $activityDetailsWidth = (Yii::app()->getUser()->getBoard() && $model->componentType->id != ComponentTypes::COMPANIES) ? 'span_6_of_12' : 'span_12_of_12'; ?>
    <div id="activity-details-left" class="res-col <?php echo $activityDetailsWidth; ?> wider-collapse">
        <?php
            $leftTabs =  array(
                'Tasks (' . $tasksCount . ')' => array( //TODO: We need a nifty way to update this tab's title via ajax when the dom changes...
                    'content' => $tasksContent,
                    'id' => 'tasks-tab',
                ),
                'Action Plans'.(($actionPlansActiveCount)? ' ('.$actionPlansActiveCount.')':'') => array(
//                    'content' => $actionPlansContent,
                    'ajax' => '/admin/actionPlans/listMiniAppliedPlans/componentTypeId/' . $model->componentType->id.'/componentId/'.$model->id,
                    'id' => 'action-plans-tab',
                ),
            );

            //TODO: How can we get a transaction id from non transactionary models???
            if($model->componentType->id != ComponentTypes::COMPANIES) {
                $leftTabs['Transactions /Referrals ' . $transactionsCount] =  array(
                    'ajax' => '/admin/contacts/transactions/' . $contactId,
                    'id' => 'transactions-tab',
                );
            }

            $leftTabs['Docs '.$documentsCount] =  array(
                //						 'content' => $documentsContent,
                'ajax' => '/admin/documents/listMini/componentTypeId/' . $model->componentType->id.'/componentId/'.$model->id,
                'id' => 'documents-tab',
            );

        $this->widget('zii.widgets.jui.CJuiTabs', array(
                    'tabs' => $leftTabs,
                    'options'=>array('active'=>$leftActiveTab),
                    'id' => 'activity-details-widget-left',
                )
            );
        ?>
    </div>
    <!-- ====== LOG RIGHT STARTS =============================================================================================== -->
    <?php if (Yii::app()->getUser()->getBoard() && $model->componentType->id != ComponentTypes::COMPANIES) : ?>
    <div id="activity-details-right" class="res-col span_6_of_12 wider-collapse">
        <?php
            $rightTabs =  ($model->componentType->id == ComponentTypes::COMPANIES) ? array() : array(
                'Homes Viewed ' . $homesViewedCount => array(
                    'ajax' => '/admin/contacts/homesViewed/' . $contactId,
                    'id' => 'homes-viewed-tab',
                ),
                'Saved Homes ' . $homesSavedCount => array(
                    'ajax' => '/admin/contacts/homesSaved/' . $contactId,
                    'id' => 'homes-saved-tab',
                ),
                'Saved Search ' . $savedHomeSearchCount => array(
                    'content' => $savedHomeSearchContent,
                    'id' => 'saved-home-search-tab',
                ),
            );

            $this->widget('zii.widgets.jui.CJuiTabs', array(
                    'tabs' => $rightTabs,
                    'id' => 'activity-details-widget-right',
                )
            );
        ?>
    </div>
     <?php endif; ?>
</div>
<!-- ====== DIALOGS =============================================================================================== -->
<?php
	// Document Dialog
	$this->widget('admin_widgets.DialogWidget.DocumentsDialogWidget.DocumentsDialogWidget', array(
			'id' => 'add-document-dialog',
			'parentModel' => $model,
			'title' => 'Add New Document',
			'componentType' => $componentType,
			'triggerElement' => '#add-document-button, .edit-document-button',
			// Trigger for Add Document Button (See DocumentsPortlet.DocumentsPortlet.php)
		)
	);

	// Action Plans
	$this->widget('admin_widgets.DialogWidget.ActionPlanDialogWidget.ActionPlanDialogWidget', array(
			'id' => 'action-plan-dialog',
			'parentModel' => $model,
			'title' => 'Add Action Plan',
			// 'componentType'  => $componentType,
			'triggerElement' => '#add-action-plan-button',
			// Trigger for Add Action Plan
		)
	);

	// Home Alert Search
    if (Yii::app()->getUser()->getBoard() && $model->componentType->id != ComponentTypes::COMPANIES) {
        $this->widget('admin_widgets.DialogWidget.HomeSearchDialogWidget.HomeSearchDialogWidget', array(
                'id' => 'home-search-dialog',
                'contactId' => $model->contact->id,
                'parentModel'    => $model,
                'title' => 'Add New Saved Search',
                // 'componentType'  => $componentType,
                'triggerElement' => '#add-home-search-button, .edit-saved-home-search-button',
                // Trigger for Add Home Search Button
            )
        );
    }

	// Email Composer Dialog
	//$this->widget('admin_widgets.DialogWidget.EmailDialogWidget.EmailDialogWidget', array(
	//	'id' => 'email-dialog',
	//	'title' => 'Compose Email',
	//	'triggerElement' => '#send-email-button',
	//	'contactId' => (isset($model->contact)) ? $model->contact->id : $model->id,
	//	'componentTypeId'=>$model->componentType->id,
	//));

	Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
	$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
			'id' => TaskDialogWidget::TASK_DIALOG_ID,
			'title' => 'Create New Task',
			'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button, .delete-task-button',
			'parentModel' => $model,
		)
	);