<!-- ====== LOG LEFT STARTS =============================================================================================== -->
<a name="activityDetails-docs"></a>
<div id="activity-details-content" class="g12 p-ml0">
	<?php
		$this->widget('zii.widgets.jui.CJuiTabs', array(
				'tabs' => array(
					'Tasks (' . $tasksCount . ')' => array( //TODO: We need a nifty way to update this tab's title via ajax when the dom changes...
						'content' => $tasksContent,
						'id' => 'tasks-tab',
					),
					'Action Plans' => array(
						'content' => $actionPlansContent,
						'id' => 'action-plans-tab',
					),
//					 'Docs '.$documentsCount => array(
////						 'content' => $documentsContent,
//						 'ajax' => '/'.Yii::app()->controller->module->id.'/documents/listMini/componentTypeId/' . $model->componentType->id.'/componentId/'.$model->id,
//						 'id' => 'documents-tab',
//					 ),
				),
				'options'=>array('active'=>$leftActiveTab),
				'id' => 'activity-details-widget-left',
			)
		);
	?>
</div>
<!-- ====== DIALOGS =============================================================================================== -->
<?php
	// Document Dialog
	$this->widget('admin_widgets.DialogWidget.DocumentsDialogWidget.DocumentsDialogWidget', array(
			'id' => 'add-document-dialog',
			'parentModel' => $model,
			'title' => 'Add New Document',
			'componentType' => $componentType,
			'triggerElement' => '#add-document-button, .edit-document-button',
			// Trigger for Add Document Button (See DocumentsPortlet.DocumentsPortlet.php)
		)
	);

	// Action Plans
	$this->widget('admin_widgets.DialogWidget.ActionPlanDialogWidget.ActionPlanDialogWidget', array(
			'id' => 'action-plan-dialog',
			'parentModel' => $model,
			'title' => 'Add Action Plan',
			// 'componentType'  => $componentType,
			'triggerElement' => '#add-action-plan-button',
			// Trigger for Add Action Plan
		)
	);

	// Email Composer Dialog
	//$this->widget('admin_widgets.DialogWidget.EmailDialogWidget.EmailDialogWidget', array(
	//	'id' => 'email-dialog',
	//	'title' => 'Compose Email',
	//	'triggerElement' => '#send-email-button',
	//	'contactId' => (isset($model->contact)) ? $model->contact->id : $model->id,
	//	'componentTypeId'=>$model->componentType->id,
	//));

	Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
	$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
			'id' => TaskDialogWidget::TASK_DIALOG_ID,
			'title' => 'Create New Task',
			'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button, .delete-task-button',
			'parentModel' => $model,
		)
	);