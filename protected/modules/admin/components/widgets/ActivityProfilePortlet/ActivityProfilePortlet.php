<?php

class ActivityProfilePortlet extends StmBasePortlet
{
	public $containerId = 'activity-profile	';
	public $containerClass = 'number-widget g12 p-p0';
	public $contentCssClass = 'g99';
	public $handleTitle = 'Activity Profile';
	public $handleIconCssClass = 'i_wizard';
	public $handleButtons = null;

	/**
	 * Transaction types values are to be linked to the table transaction_types.
	 * Examples types: contacts, buyers, sellers, closings, projects, etc.
	 * @var int
	 */
	// public $transactionType;
	public $model;

	/**
	 * FromDate & ToDate is the date range to query tasks for the widget. Default will show all tasks up to today.
	 * @var string
	 */
	// public $startDate;
	// public $endDate;

	public function renderContent()
	{
		$this->render('activityProfilePortlet', array(
			'homesViewedCount' => ViewedPages::getCount($this->model->id, 1),
			'homesSavedCount' => SavedHomes::getCount($this->model->id),
			'savedHomeSearchCount' => SavedHomeSearches::getCount($this->model->id),
			'transactionsCount' => Transactions::getCount($this->model->id),
			'savedHomeSearchCount' => SavedHomeSearches::getCount($this->model->id),
			'numDocuments' => Documents::getCount($this->model->id, ComponentTypes::CONTACTS),
		));
	}
}