<?php

class TaskPortlet extends StmBasePortlet
{
	public $containerId = 'task-portlet';
	public $containerClass = 'p-0';
	public $handleTitle = 'Tasks';
	public $handleIconCssClass = 'i_tick';
	public $handleButtons = array(
		// array(
		// 	'label'=>'Add Tasks',
		// 	'iconCssClass'=>'i_stm_add',
		// 	'htmlOptions'=>array('id'=>'add-task-button'),
		// 	'type'=>'',
		// 	'name'=>'add-task',
		// )
	);

	public $contentCssClass = 'portlet-content';
	public $componentType;
	public $parentModel;

	/**
	 * FromDate & ToDate is the date range to query tasks for the widget. Default will show all tasks up to today.
	 * @var string
	 */
	public $startDate;
	public $endDate;

	public function init()
	{
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_widgets.TaskPortlet.assets.css'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
		Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'taskPortlet.css');

		parent::init();
	}

	public function renderContent() {
        if (!is_array($this->parentModel->getTasks($this->parentModel))) {
            throw new CHttpException(500, __CLASS__.' requires a task relationship.');
        }

		$model = new Tasks;

        if (!$this->endDate) {
            $this->endDate =  date('Y-m-d');
        }

		if (!$model->due_date)
			$model->due_date = date('Y-m-d h:i a');

		$dataProvider = new CArrayDataProvider($this->parentModel->getTasks($this->parentModel), array());

		$this->render('taskPortlet', array(
		    'parentModel'   => $this->parentModel,
			'model'         => $model,
			'startDate'     => $this->startDate,
			'endDate'       => $this->endDate,
			'dataProvider'  => $dataProvider,
		));
	}
}
