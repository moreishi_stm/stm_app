<div class="button-container">
	<button id="add-task-button" class="text" type="button" data="<?php echo $parentModel->id; ?>"
			ctid="<?php echo $parentModel->componentType->id;?>"><em class="icon i_stm_add"></em>Add Task
	</button>
</div>
<div class="scroll-content-container">
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'task-grid',
		'htmlOptions' => array(
			'class' => 'task-grid'
		),
		'template' => '{items}',
		'enablePagination' => false,
		'dataProvider' => $dataProvider,
		'itemsCssClass' => 'datatables',
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Due/Assign',
				'value' => 'Yii::app()->format->formatDate($data->due_date)."<br>".$data->assignedTo->fullName .
					  "<input type=\"hidden\" class=\"task_id\" value=\"".$data->id."\">" .
					  "<input type=\"hidden\" class=\"task_type_id\" value=\"".$data->task_type_id."\">"',
				'htmlOptions' => array(
					'style' => 'width:85px'
				),
			),
			array(
				'name' => 'Type', //"<i>(".$data->taskType->name.")</i><br />"
				'value' => '$data->taskType->name',
				'type' => 'raw',
			),
			array(
				'name' => 'Description',
				'value' => '$data->description',
				'type' => 'raw',
			),
			array(
				'name' => '',
				'type' => 'raw',
				'value' => '"<button class=\"edit-task-button\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\">Edit</button>"',
				'htmlOptions' => array(
					'style' => 'width:45px'
				),
			),
			array(
				'name' => '',
				'type' => 'raw',
				'value' => '"<button class=\"complete-task-button\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\">Complete</button>"',
				'htmlOptions' => array(
					'style' => 'width:75px'
				),
			),
			array(
				'name' => '',
				'type' => 'raw',
				'value' => '"<button class=\"delete-task-button\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\">Delete</button>"',
				'htmlOptions' => array(
					'style' => 'width:75px'
				),
			),
		),
	));
	?>
</div>
<?php
Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
	'id' => TaskDialogWidget::TASK_DIALOG_ID,
	'title' => 'Create New Task',
	'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button, .delete-task-button',
	'parentModel' => $parentModel,
));