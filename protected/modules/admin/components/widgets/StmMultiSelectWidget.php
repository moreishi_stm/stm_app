<?php

/**
 * StmMultiSelectWidget
 * Wrapper for the current admin theme's multi select widget
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since 08/15/2012
 * @version 0.1 Beta
 * @extends CWidget
 */
class StmMultiSelectWidget extends CWidget
{
	public $jsOptions   = array();
	public $htmlOptions = array();
	public $listData    = array();
	public $jQuerySelector = 'select[multiple]';

	// Using an AR
	public $model;
	public $attribute;

	// If not using an AR
	public $name;
	public $selectedValue;

	protected $_defaultHtmlOptions = array(
		'multiple'=>'multiple'
	);

	/**
	 * init
	 * Setup the multi select box and do not call it if we do not have data
	 *
	 * @return null
	 */
	public function init()
	{
		if ( !$this->listData )
			return;

		$this->htmlOptions = CMap::mergeArray($this->_defaultHtmlOptions, $this->htmlOptions);

        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl.DS.'wl_Multiselect.js', CClientScript::POS_END);
	}

	/**
	 * run
	 * Generate the multi select box as seen by the current admin theme
	 *
	 * @return null
	 */
	public function run()
	{
		if ( $this->model instanceof CActiveRecord ) {
            // sort orders the selected values. Note that it uses the order of $this->listData in the selected values
            $orderedList = array();
            $listData = $this->listData;
            foreach($this->model->{$this->attribute} as $key => $value) {
                $orderedList[$value] = $listData[$value];
                unset($listData[$value]);
            }
            $orderedList = CMap::mergeArray($orderedList,$listData);
            $this->listData = $orderedList;

			echo CHtml::activeDropDownList($this->model, $this->attribute, $this->listData, $this->htmlOptions);
        } else {
			echo CHtml::dropDownList($this->name, $this->selectedValue, $this->listData, $this->htmlOptions);
        }

		// Format the multi selects
		$jsOptions = CJSON::encode($this->jsOptions);
		$js = '$("'.$this->jQuerySelector.'").wl_Multiselect('.$jsOptions.');';
		Yii::app()->clientScript->registerScript(__CLASS__, $js);
	}
}