<div class="rounded-text-box odd-static">
    <div class="g4">
        <table id="companiesPortlet" class="container">
            <tr>
                <th>Status:</th>
                <td><?php echo StmFormHelper::getStatusBooleanName($this->model->status_ma);?></td>
            </tr>
            <tr>
                <th>Name:</th>
                <td><?php echo $this->model->name;?></td>
            </tr>
            <tr>
                <th>Website:</th>
                <td><?php echo $this->model->website;?></td>
            </tr>
            <tr>
                <th>Type:</th>
                <td><?php echo Yii::app()->format->formatCommaDelimited($this->model->types, 'name');?></td>
            </tr>
        </table>
    </div>
    <div class="g8">
        <table class="container">
            <tr>
                <th>Phones:</th>
                <td><?
                        if ($this->model->phones)
                            foreach($this->model->phones as $PhoneModel) {
                                $primaryText = ( $PhoneModel->is_primary ) ? ' (Primary)' : '';

                                if ($PhoneModel->extension)
                                    $opt=array('ext'=>$PhoneModel->extension);

                                $extension = ($PhoneModel->phone_type_ma) ? ' ('.$PhoneModel->phoneTypes[$PhoneModel->phone_type_ma].')' : '';
                                $phoneText = Yii::app()->format->formatPhone($PhoneModel->phone, $opt).$extension.$primaryText;

                                echo CHtml::tag('div', $htmlOptions=array('style'=>'font-weight:bold;'), $phoneText);
                            }
                        else
                            echo CompanyDetailsPortlet::NO_RESULT_TEXT;
                    ?>
                </td>
            </tr>
            <tr>
                <th>Address:</th>
                <td>
                    <?
                        if ($this->model->addresses)
                            foreach($this->model->addresses as $AddressesModel) {
                                $primaryText = ( $AddressesModel->isPrimary ) ? ' (Primary)' : '';
                                echo CHtml::tag('div', $htmlOptions=array('class'=>'strong'),
                                    $AddressesModel->address.' '.$AddressesModel->city.', '.AddressStates::getShortNameById($AddressesModel->state_id).' '.$AddressesModel->zip.$primaryText
                                );
                            }
                        else
                            echo CompanyDetailsPortlet::NO_RESULT_TEXT;
                    ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="g12">
        <table class="container">
            <tr>
                <th style="width:11.5%;">Notes:</th>
                <td><?php echo $this->model->notes;?></td>
            </tr>
        </table>
    </div>
</div>