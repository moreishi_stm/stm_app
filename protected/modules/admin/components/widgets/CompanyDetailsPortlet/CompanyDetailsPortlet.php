<?php

class CompanyDetailsPortlet extends StmBasePortlet
{
	const EDIT_COMPANY_BUTTON_ID = 0;
	const NO_RESULT_TEXT         = 'None Specified';

	public $containerId        = 'companies';
	public $containerClass     = 'g100 p-0 p-fr';
	public $handleTitle        = 'Company Information';
	public $handleIconCssClass = 'i_buildings';
	public $handleButtons      = array(
		array(
			'label'        =>'Edit',
			'iconCssClass' =>'i_stm_edit',
			'htmlOptions'  =>array('id'=>'edit-company-button'),
			'name'         =>'edit-company',
			'type'         =>'link',
		)
	);

	/**
	 * Transaction types values are to be linked to the table transaction_types.
	 * Examples types: contacts, buyers, sellers, closings, projects, etc.
	 * @var int
	 */
	public $transactionType;
	public $model;
	public $profileContent;
	public $view;

	public function init()
	{
		$this->initHandleButtons();

		parent::init();
	}

	public function initHandleButtons()
	{
		$this->handleButtons[self::EDIT_COMPANY_BUTTON_ID]['link'] = $this->controller->createUrl('companies/edit', array('id'=>$this->model->id));
		$this->handleTitle = $this->model->name;
	}

	public function renderContent()
	{
		$this->render('companyViewPortlet', array(
			'model'=>$model,
		));
	}
}