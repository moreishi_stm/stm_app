<?php

class StmWidget extends CWidget {

	private static $_viewPaths;

	public function getViewPath($checkTheme = false) {
        if(in_array(Yii::app()->theme->name, array('default','standard1'))) {
            return parent::getViewPath($checkTheme);
        }
		$className = get_class($this);
		if (isset(self::$_viewPaths[$className]))
			return self::$_viewPaths[$className];
		else {
			if ($checkTheme && ($theme = Yii::app()->getTheme()) !== null) {
				$paths = $theme->getViewPath();
				$paths = array_reverse($paths);
				$class = new ReflectionClass($className);

				foreach ($paths as $path) {
					$path .= DIRECTORY_SEPARATOR;
					if (strpos($className, '\\') !== false) { // namespaced class
						$path.=str_replace('\\', '_', ltrim($className, '\\'));
					} else {
						$path.=$className;
					}

					#echo("<pre>Checking: ".$path."</pre>");

					if (is_dir($path)) {
						self::$_viewPaths[$className][] = $path;
					}
				}
                // add the default directory as last one always
                $class=new ReflectionClass($className);
                self::$_viewPaths[$className][]=dirname($class->getFileName()).DIRECTORY_SEPARATOR.'views';
			}

			return self::$_viewPaths[$className];
		}
	}

	public function getViewFile($viewName) {
        if(in_array(Yii::app()->theme->name, array('default','standard1'))) {
            return parent::getViewFile($viewName);
        }
		if (($renderer = Yii::app()->getViewRenderer()) !== null) {
			$extension = $renderer->fileExtension;
		} else {
			$extension = '.php';
		}

		if (strpos($viewName, '.')) { // a path alias
			$viewFile = Yii::getPathOfAlias($viewName);
		} else {
			$viewPaths = $this->getViewPath(true);

			#die("</ul><pre>Paths: ".print_r($viewPaths, 1));

			foreach($viewPaths as $path) {
				$path .= DIRECTORY_SEPARATOR . $viewName;
				if (is_file($path . $extension)) {
					return Yii::app()->findLocalizedFile($path . $extension);
				} elseif ($extension !== '.php' && is_file($path . '.php')) {
					return Yii::app()->findLocalizedFile($path . '.php');
				}
				$viewFile = $this->getViewPath(false) . DIRECTORY_SEPARATOR . $viewName;
			}
		}

		if (is_file($viewFile . $extension)) {
			return Yii::app()->findLocalizedFile($viewFile . $extension);
		} elseif ($extension !== '.php' && is_file($viewFile . '.php')) {
			return Yii::app()->findLocalizedFile($viewFile . '.php');
		}

		return false;
	}

}
