<?php

$domainSuffix = (YII_DEBUG) ? 'local': 'com';
$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";
Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);

$componentId = $model->id;
$componentTypeId = ComponentTypes::CLOSINGS;
Yii::app()->clientScript->registerCss('closingPortletCss', <<<CSS
    #closing-info-tab, #closing-documents-tab {
        height: 600px !important;
    }
CSS
);

$js = <<<JS
$("#preview-email-client-update-button").live('click', function(){

	$("body").prepend("<div class='loading-container loading'><em></em></div>");

    $.post('/admin/contacts/emailUpdate',{ componentTypeId: $componentTypeId, componentId: $componentId, mode: "preview" }, function(data) {
        var clientUpdateHtml = '<div style="margin-bottom: 15px; text-align: center;"><button id="email-client-update-button" type="submit" class="submit wide">Email Client Update Now</button></div>' + data.html;
        $('#email-client-update-content').html(clientUpdateHtml);

		$("div.loading-container.loading").remove();

        $.fancybox({
                'type': 'inline',
                'content' : $('#email-client-update-content').html(),
                'width': 600,
                'height': 100,
                'autoSize':false,
                'scrolling':'no',
                'autoDimensions':false,
                'padding': 40
            });

    }, 'json');

    return false;
});

$("#email-client-update-button").live('click',function(){
    $("body").prepend("<div class='loading-container loading'><em></em></div>");

    $.post('/admin/contacts/emailUpdate',{ componentTypeId: $componentTypeId, componentId: $componentId, mode: "sendEmail" }, function(data) {
        // zero means success
        if(data.result == 'success') {
        Message.create('success','Closing Update has been sent.');
            $.fn.yiiGridView.update('activity-log-grid', {});
        }
        else {
            Message.create('error','Closing Update had an error. Please contact support.');
        }

        $.fancybox.close();
        $("div.loading-container.loading").remove();
    }, 'json');

    return false;
});

JS;

Yii::app()->clientScript->registerScript('closingsPortletScript', $js);

/*$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
        'target' => 'a#preview-email-client-update-button', 'config' => array(
            'width' => '800', 'height' => '600', 'scrolling' => 'auto', 'autoDimensions' => false, 'padding' => '40',
        ))
);*/
?>

<div style="display: none">
    <div id="email-client-update-content">
        <div id="email-client-update-content" style="font-size: 14px; font-family: Arial, Helvetica, sans-serif !important;"></div>
    </div>
</div>

<div class="g99 p-ph4">
	<div class="g12">
		<h3 style="color: #555;" class="p-pb10"><?php echo Yii::app()->format->address($model->transaction->address, array('lineBreak' => false)); ?></h3>
		<?php
			$this->widget('zii.widgets.jui.CJuiTabs', array(
					'tabs' => array(
						'Closing Info' => array(
							'ajax' => '/admin/closings/info/' . $model->id,
							'id' => 'closing-info-tab',
                        ),
						'Documents '.$documentsCount => array(
							'ajax' => '/admin/documents/listMini/componentTypeId/' . $model->componentType->id . '/componentId/' . $model->id,
							'id' => 'closing-documents-tab',
						),
					),
					'id' => 'transaction-closing-details',
					'htmlOptions' => array('class' => 'g12'),
				)
			);
		?>
	</div>
</div>
