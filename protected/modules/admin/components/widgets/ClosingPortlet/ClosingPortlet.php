<?php

class ClosingPortlet extends StmBasePortlet {

	public $containerId = 'transactions';
	public $containerClass = 'g12 p-0 p-fr';
	public $handleTitle = 'Closing Details';
	public $handleIconCssClass = 'i_price_tag';
	public $handleButtons = array(
		array(
			'label' => 'View Transaction',
			'iconCssClass' => 'i_stm_search',
			'htmlOptions' => array('id' => 'view-transaction-button'),
			'name' => 'view-transaction',
			'type' => 'link',
			'link' => '/admin',
		),
	);

	/**
	 * Closings Model
	 * @var ClosingsModel
	 */
	public $model;
	public $TransactionType;

	public function init() {
		$this->handleButtons[0]['label'] = 'View ' . $this->model->transaction->componentType->display_name;
		$this->handleButtons[0]['link'] .= DS . $this->model->transaction->componentType->name . DS . $this->model->transaction->id;

        if(Yii::app()->user->checkAccess('owner') || !Yii::app()->user->checkAccess('noClosingEdit')) {
            $editButton = 		array(
                'label' => 'Edit',
                'iconCssClass' => 'i_stm_edit',
                'htmlOptions' => array('id' => 'edit-transaction-button'),
                'name' => 'edit-transaction',
                'type' => 'link',
                'link' => '/admin/closings/edit',
            );
            $editButton['link'] .= DS . $this->model->id;
            array_push($this->handleButtons, $editButton);
        }
		parent::init();
	}

	public function renderContent() {
		echo '<div class="rounded-text-box odd-static">';

		$this->render('closingPortlet', array(
			'model' => $this->model,
            'documentsCount' => Documents::getCount($this->model->id, $this->model->componentType->id, true),
		));

		echo '</div>';
	}
}
