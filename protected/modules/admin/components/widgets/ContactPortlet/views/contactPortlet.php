<style type="text/css">
    em.bombBomb {
        height: 15px;
        width: 83px;
        background-image: url('http://cdn.seizethemarket.com/assets/images/bombBomb.png');
        display: inline-block;
        background-position: -13px 0px;
    }
    .bombBombList-button {
        padding: 9px;
        display: inline-block;
    }
    #contactPortlet .click2callHelper {
        color: #37F;
    }
</style>
<div class="rounded-text-box odd-static">
	<div class="g6">
		<table id="contactPortlet" class="">
			<tbody>
			<tr>
				<th>Contact ID:</th>
				<td><?php echo $this->model->id; ?></td>
			</tr>
			<tr>
				<th>Name:</th>
				<td><?php echo $this->model->fullName; ?></td>
			</tr>
            <? if($this->model->phonetic_name): ?>
            <tr>
                <th>Phonetic Name:</th>
                <td><?php echo $this->model->phonetic_name; ?></td>
            </tr>
            <? endif; ?>
            <tr>
                <th>Spouse:</th>
                <td><?php echo $this->model->spouseFullName; ?></td>
            </tr>
            <? if($this->model->spouse_phonetic_name): ?>
                <tr>
                    <th>Spouse Phonetic Name:</th>
                    <td><?php echo $this->model->spouse_phonetic_name; ?></td>
                </tr>
            <? endif; ?>
            <tr>
                <th><em class="icon icon-only i_stm_star"></em>=</th>
                <td>Primary</td>
            </tr>
			<tr>
				<th>Email:</th>
				<td>
                    <?php
                    if ($this->model->emails) {
                        foreach ($this->model->emails as $key => $EmailModel) {
                            if ($EmailModel->email) {
                                $primaryText = ($this->model->getPrimaryEmail() == $EmailModel->email) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
                                echo '<div>';
                                    echo $EmailModel->email . $primaryText;
                                    $statusAlert = '';
                                    if($EmailModel->email_status_id == EmailStatus::OPT_OUT) {
                                        $statusAlert = 'Opted-out';
                                    } elseif($EmailModel->email_status_id == EmailStatus::HARD_BOUNCE) {
                                        $statusAlert = 'Bounced';
                                    }
                                    if($EmailModel->getIsUniversallyUndeliverable()) {
                                        $statusAlert .= ($statusAlert)? ', ': '';
                                        $statusAlert .= 'Undeliverable';
                                    }
                                    if($statusAlert) {
                                        echo '<div class="errorMessage" style="display:inline-block;">&nbsp;'.$statusAlert.'</div>';
                                    }
                                echo '</div>';

                    if(Yii::app()->user->checkAccess('bombBomb') && Yii::app()->user->settings->bombbomb_api_key) {
                                    echo ($EmailModel->isBombBomb)? '<em class="bombBomb"></em>':
                                        '<a class="text-button add-bombBomb-email" href="javascript:void(0)" style="margin-left:0; display:inline-block;" data-id="'.$key.'"><em class="icon i_stm_add"></em>Add to BombBomb</a>';
                                    echo $this->bombBombLists();
                                    echo '<a href="javascript:void(0);" id="bombBombList-button-'.$key.'" class="text-button bombBombList-button hidden" data-id="'.$key.'"  data-email="'.$EmailModel->email.'">Add</a>';
                                }
                            }
                        }
                    }
                    ?>
					<?php
//						if ($this->model->emails) {
//							foreach ($this->model->emails as $EmailsModel) {
//								$primaryText = ($this->model->getPrimaryEmail() == $EmailsModel->email) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
//								echo CHtml::tag('div', $htmlOptions = array(), $EmailsModel->email . $primaryText);
//							}
//						}
					?>
				</td>
			</tr>
<!--			<tr>-->
<!--				<th>Password:</th>-->
<!--				<td>--><?php //echo ($this->model->isAgent)? '******' :$this->model->password; ?><!--</td>-->
<!--			</tr>-->
			<tr>
				<th>Phone:</th>
				<td><?php
                        $hidePhones = (Yii::app()->user->settings->hide_phone_numbers_search_results) ? true : false;
                        if ($this->model->phones) {
                            $primaryPhone = $this->model->getPrimaryPhone();



                            foreach ($this->model->phones as $PhoneModel) {
								if(empty($phoneTypes)){
									$phoneTypes = $PhoneModel->getPhoneTypes();
									$phoneOwnerTypes = $PhoneModel->getPhoneOwnerTypes();
								}
								$phoneTypeText = "";

								if(!empty($phoneOwnerTypes[$PhoneModel->owner_ma])){
									$phoneTypeText = $phoneOwnerTypes[$PhoneModel->owner_ma];
								}
								if(!empty($phoneTypes[$PhoneModel->phone_type_ma])){
									if(!empty($phoneTypeText)){$phoneTypeText .= " ";}
									$phoneTypeText .= $phoneTypes[$PhoneModel->phone_type_ma];
								}

								$primaryText = ($primaryPhone == $PhoneModel->phone) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
                                if ($PhoneModel->phone) {
                                    if ($PhoneModel->extension) {
                                        $opt = array('extension' => $PhoneModel->extension);
                                    }

                                    echo (($hidePhones) ? '***-***-**'.substr($phone->phone, -2, 2) : Yii::app()->format->formatPhone($PhoneModel->phone, $opt) . (($phoneTypeText) ? " ($phoneTypeText)" : "") .$primaryText); // class="click2callHelper"

                                    echo '<a style="padding:4px 8px; display: inline;" class="text-button phone-status-button" href="javascript:void(0)" data-id="'.$PhoneModel->id.'" data-ctid="'.ComponentTypes::CONTACTS.'" data-cid="'.$this->model->id.'" data-phone="'.Yii::app()->format->formatPhone($PhoneModel->phone).'"><em class="icon i_stm_question"></em>Action</a>';

                                    if($AreaCodeInfo = $PhoneModel->getAreaCodeInfo()) {
                                        $phoneText = '<br /><span class="label">';
                                        $phoneText .= ($AreaCodeInfo->city)?$AreaCodeInfo->city.', ':'';
                                        $phoneText .= $AreaCodeInfo->state.'</span><br />';
                                        echo $phoneText;
                                    }
                                    echo '<br />';
                                }
                            }
                        }
					?>
				</td>
			</tr>
			<tr>
				<th>Address:</th>
				<td>
					<?php
						if ($this->model->addresses) {
                            $primaryAddress = $this->model->getPrimaryAddress();
							foreach ($this->model->addressContactLu as $AddressContactLu) {
                                $primaryText = ($primaryAddress->id == $AddressContactLu->address->id) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
								echo CHtml::tag('div', $htmlOptions = array(), $AddressContactLu->address->address);
								echo CHtml::tag('div', $htmlOptions = array(), $AddressContactLu->address->city . ', ' . AddressStates::getShortNameById($AddressContactLu->address->state_id). ' ' . $AddressContactLu->address->zip.$primaryText);
							}
						} else {
							echo ContactPortlet::NO_RESULT_TEXT;
						}
					?>
				</td>
			</tr>
            <?if((Yii::app()->user->checkAccess('dialer') || Yii::app()->user->checkAccess('owner')) && in_array($this->model->componentType->id, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS, ComponentTypes::CONTACTS))):?>
                <tr>
                    <th>Dialer Lists:</th>
                    <td>
                        <div id="call-list-tag-container">
                            <?php

                            switch($this->model->componentType->id) {
                                case ComponentTypes::SELLERS:
                                    $preset = CallLists::CUSTOM_SELLERS;
                                    break;

                                case ComponentTypes::BUYERS:
                                    $preset = CallLists::CUSTOM_BUYERS;
                                    break;

                                case ComponentTypes::RECRUITS:
                                    $preset = CallLists::CUSTOM_RECRUITS;
                                    break;

                                case ComponentTypes::CONTACTS:
                                    $preset = CallLists::CUSTOM_CONTACTS;
                                    break;

                                default:
                                    throw new Exception(__CLASS__.' (:'.__LINE__.') Invalid Component Type Id');
                                    break;
                            }

                            $dialerLists = Yii::app()->db->createCommand()
                                ->select('clp.call_list_id call_list_id, l.name name, c.id contact_id')
                                ->from('call_list_phones clp')
                                ->join('call_lists l', 'l.id=clp.call_list_id')
                                ->join('phones p', 'p.id=clp.phone_id')
                                ->join('contacts c', 'c.id=p.contact_id')
                                ->where('c.id='.$this->model->id)
                                ->andWhere('l.preset_ma='.$preset)
                                ->andWhere('clp.is_deleted=0')
                                ->andWhere('l.is_deleted=0')
                                ->andWhere('clp.hard_deleted IS NULL')
                                ->group('clp.call_list_id')
                                ->queryAll();

                            foreach($dialerLists as $dialerList) {
                                ?>
                                <div id="call-list-tag-<?=$dialerList['call_list_id']?>" class="call-list-tag"><span class="label"><?=$dialerList['name']?></span><span class="remove-from-call-list" data-id="<?=$dialerList['call_list_id']?>" data-contactid="<?=$dialerList['contact_id']?>" data-ctid="<?=$this->model->componentType->id?>" data-cid="<?=$this->model->id?>">X</span></div>
                            <?
                            }
                            ?>
                        </div>
                        <button class="text add-to-dialer-button" style="display: inline-block; margin-left: -4px;" data-ctid="<?=$this->model->componentType->id?>" data-cid="<?=$this->model->id?>" data-contactid="<?=$this->model->id?>"><em class="icon i_stm_add"></em>Add to Dialer</button>
                    </td>
                </tr>
            <? endif;?>
            </tbody>
		</table>
	</div>
	<div class="g6">
		<table class="">
			<tbody>
			<tr>
				<th>Tags:</th>
				<td>
					<?php
                    if($contactLus = $this->model->contactTypeLu) {

                        $types = '';
                        foreach($contactLus as $contactLu) {
                            $types .= ($types) ? ', '.$contactLu->type->name : $contactLu->type->name;
                        }
                        echo $types;
                    }
                    ?>
				</td>
			</tr>
			<tr>
				<th>Source:</th>
				<td><?php echo $this->model->source->name; ?></td>
			</tr>
			<!-- <tr><th></th><td>Profile:</td></tr> -->
			<tr>
				<td colspan="2">
					<?php
						if ($this->model->attributeValues) {
							foreach ($this->model->attributeValues as $ContactAttributeValue) {
								$attributes .= '<tr><th class="p-p2">' . $ContactAttributeValue->attribute->label . ':</th>' . '<td class="p-0">';

                                switch(1) {
                                    case $ContactAttributeValue->attribute->data_type_id == DataTypes::BOOLEAN_ID:
                                        $attributes .= StmFormHelper::getYesNoName($ContactAttributeValue->value);
                                        break;


                                    case $ContactAttributeValue->contact_attribute_id == ContactAttributes::HQ_PIPELINE:

                                        $attributes .= ($ContactAttributeValue->value !== '') ? Yii::app()->controller->getPipelineList()[$ContactAttributeValue->value] : '';
                                        break;

                                    case $ContactAttributeValue->contact_attribute_id == ContactAttributes::HQ_STATE_ID:

                                        $attributes .= ($ContactAttributeValue->value !== '') ? AddressStates::getShortNameById($ContactAttributeValue->value) : '';
                                        break;

                                    default:
                                        $attributes .= $ContactAttributeValue->value;
                                        break;
                                }

                                $attributes .= '</td></tr>';
							}
						}
						echo $attributes;
					?>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="g12">
		<table class="">
			<tbody>
			<tr class="p-p10">
				<th class="narrow">Notes:</th>
				<td><?php echo nl2br($this->model->notes); ?></td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php
//Yii::import('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget');
//if (!class_exists('DialerListDialogWidget', false)) {
//    $this->widget('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget', array(
//            'id' => DialerListDialogWidget::DIALOG_ID,
//            'title' => 'Add to Dialer',
//            'triggerElement' => '.add-to-dialer-button',
//        )
//    );
//}
Yii::import('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget');
if (!class_exists('DialerListDialogWidget', false)) {

    Yii::app()->clientScript->registerScript('dialerAddToListContactsJs', <<<JS
        function postAddToDialerList(data) {
            // return if there's no added data for some reason
            if(data.added == undefined) {
                Message.create("error", 'No phones were added to the Call List. Please verify that phone numbers exist for this contact.');
                return;
            }
            var callListTag = '<div id="call-list-tag-'+ data.added.call_list_id +'" class="call-list-tag"><span class="label">'+ data.added.name +'</span><span class="remove-from-call-list" data-id="'+ data.added.call_list_id +'" data-pid="' + data.added.phoneId + '" data-ctid="' + data.added.componentTypeId + '" data-cid="' + data.added.componentId + '" data-contactid="' + data.added.contactId + '">X</span></div>';
            $('#call-list-tag-container').append(callListTag);
        }
JS
    );
    $this->widget('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget', array(
            'id' => DialerListDialogWidget::DIALOG_ID,
            'title' => 'Add to Dialer',
            'triggerElement' => '.add-to-dialer-button',
            'postAddFunction' => 'postAddToDialerList',
        )
    );
}

Yii::import('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget');
if (!class_exists('ClickToCallDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget', array(
            'id' => ClickToCallDialogWidget::CLICK_TO_CALL_DIALOG_ID,
            'title' => 'Click to Call',
            'triggerElement' => '.click-to-call-button',
        )
    );
}

Yii::import('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget');
$this->widget('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget', array(
        'id' => PhoneDialogWidget::PHONE_DIALOG_ID,
        'title' => 'Phone Actions',
        'triggerElement' => '.phone-status-button',
        'parentModel' => $this->model,
    )
);
?>