<?
$moduleName = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('contactPortletMiniScript',<<<JS
	 $('.click-to-call-button').tipsy({live: true, gravity: 's', title: 'data-tooltip'});
JS
, CClientScript::POS_END);

Yii::app()->clientScript->registerCss('contactPortletMiniCss', <<<CSS
    a.phone-info:hover {
        display: inline;
    }
    em.bombBomb {
		height: 15px;
		width: 83px;
		background-image: url('http://cdn.seizethemarket.com/assets/images/bombBomb.png');
		display: inline-block;
		background-position: -13px 0px;
	}
	.bombBombList-button {
		padding: 9px;
		display: inline-block;
	}
CSS
);
?>
<div class="rounded-text-box odd-static">
	<div class="g12">
		<table class="">
			<tbody>
			<tr>
				<th>Contact ID:</th>
				<td><?php echo $this->model->id; ?><a href="/<?php echo Yii::app()->controller->module->id; ?>/contacts/<?php echo $this->model->id; ?>" class="text-button" style="display: inline;"><em class="icon i_stm_search"></em>View Contact</a></td>
			</tr>
			<tr>
				<th>Name:</th>
				<td><?php echo $this->model->fullName; ?></td>
			</tr>
            <tr>
                <th>Spouse:</th>
                <td><?php echo $this->model->spouseFullName; ?></td>
            </tr>
			<tr>
				<th>Email:</th>
				<td>
					<?php
						if ($this->model->emails) {
							foreach ($this->model->emails as $key => $EmailModel) {
                                $primaryText = '';
								if ($EmailModel->email) {
                                    $primaryText = ($this->model->getPrimaryEmail() == $EmailModel->email) ? ' <em class="icon icon-only i_stm_star"></em>' : '';

                                    echo '<div style="font-weight: bold;">';
                                        echo $EmailModel->email . $primaryText;
                                        $statusAlert = '';
                                        if($EmailModel->email_status_id == EmailStatus::OPT_OUT) {
                                            $statusAlert = 'Opted-out';
                                        } elseif($EmailModel->email_status_id == EmailStatus::HARD_BOUNCE) {
                                            $statusAlert = 'Bounced';
                                        }
                                        if($EmailModel->getIsUniversallyUndeliverable()) {
                                            $statusAlert .= ($statusAlert)? ', ': '';
                                            $statusAlert .= 'Undeliverable';
                                        }
                                        if($statusAlert) {
                                            echo '<div class="errorMessage" style="display:inline-block;">&nbsp;'.$statusAlert.'</div>';
                                        }
                                    echo '</div>';

									if(Yii::app()->user->checkAccess('bombBomb') && Yii::app()->user->settings->bombbomb_api_key) {
										echo ($EmailModel->isBombBomb)? '<em class="bombBomb"></em>':
											'<a class="text-button add-bombBomb-email" href="javascript:void(0)" style="margin-left:0; display:inline-block;" data-id="'.$key.'"><em class="icon i_stm_add"></em>Add to BombBomb</a>';
										echo $this->bombBombLists();
										echo '<a href="javascript:void(0);" id="bombBombList-button-'.$key.'" class="text-button bombBombList-button hidden" data-id="'.$key.'"  data-email="'.$EmailModel->email.'">Add</a>';
									}
								}
							}
						}
					?>
				</td>
			</tr>
			<tr>
				<th>Phone:</th>
				<td><?php
						if ($this->model->phones) {
                            $hidePhones = (Yii::app()->user->settings->hide_phone_numbers_search_results) ? true : false;
                            $primaryPhone = $this->model->getPrimaryPhone();
							foreach ($this->model->phones as $PhoneModel) {
//								if(empty($phoneTypes)){
//									$phoneTypes = $PhoneModel->getPhoneTypes();
//									$phoneOwnerTypes = $PhoneModel->getPhoneOwnerTypes();
//								}
//								$phoneTypeText = "";
//
//								if(!empty($phoneOwnerTypes[$PhoneModel->owner_ma])){
//									$phoneTypeText = $phoneOwnerTypes[$PhoneModel->owner_ma];
//								}
//								if(!empty($phoneTypes[$PhoneModel->phone_type_ma])){
//									if(!empty($phoneTypeText)){$phoneTypeText .= " ";}
//									$phoneTypeText .= $phoneTypes[$PhoneModel->phone_type_ma];
//								}
//								if(!empty($phoneTypeText)){
//									echo $phoneTypeText."<br>";
//								}

                                $primaryText = '';
                                if ($PhoneModel->phone_type_ma) {
                                    $primaryText = ' (' . (($PhoneModel->owner_ma) ? $PhoneModel->phoneOwnerTypes[$PhoneModel->owner_ma]." " : "") . $PhoneModel->phoneTypes[$PhoneModel->phone_type_ma] . ')';
                                }

                                $primaryText .= ($primaryPhone == $PhoneModel->phone) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
								if ($PhoneModel->phone) {
									if ($PhoneModel->extension) {
										$opt = array('extension' => $PhoneModel->extension);
									}
                                    echo (($hidePhones)? '***-***-**'.substr($phone->phone, -2, 2): Yii::app()->format->formatPhone($PhoneModel->phone, $opt).$primaryText);

                                    if(Yii::app()->user->checkAccess('clickToCall')) {
                                        echo '<a class="action-button green  fa fa-volume-control-phone contact-button-tip click-to-call-button" data-tooltip="Click to Call" data-cid="'.$this->originComponentId.'" ctid="'.$this->originComponentTypeId.'" data-phone="'.Yii::app()->format->formatPhone($PhoneModel->phone).'"></a>';
                                    }

                                    if($AreaCodeInfo = $PhoneModel->getAreaCodeInfo()) {
                                        $phoneText = '<br /><span style="font-weight: normal; color: #555;">';
                                        $phoneText .= ($AreaCodeInfo->city)?$AreaCodeInfo->city.', ':'';
                                        $phoneText .= $AreaCodeInfo->state.'</span><br />';
                                        echo $phoneText;
                                    }
                                    echo '<a style="padding:4px 8px; display: inline;" class="text-button phone-status-button" href="javascript:void(0)" data-id="'.$PhoneModel->id.'" data-ctid="'.$this->originComponentTypeId.'" data-cid="'.$this->originComponentId.'" data-phone="'.Yii::app()->format->formatPhone($PhoneModel->phone).'"><em class="icon i_stm_add"></em>Action</a>';
									echo '<br />';
								}
							}
						}
					?>
				</td>
			</tr>
			<?php if ($this->model->addresses) { ?>
				<tr>
					<th>Address:</th>
					<td>
						<div class="contact-portlet-address">
							<?php
								if ($this->model->addresses) {
                                    $primaryAddress = $this->model->getPrimaryAddress();
									foreach ($this->model->addresses as $AddressModel) {
										if ($AddressModel->address) {
                                            $primaryText = ($primaryAddress->id == $AddressModel->id) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
											echo '<p>' . Yii::app()->format->formatAddress($AddressModel) . $primaryText.'</p>';
										}
									}
								}
							?>
						</div>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<th>Password:</th>
				<td><?php echo ($this->model->isAgent)? '******' :$this->model->password;
                    echo '<a style="padding:4px 8px; display: inline;" class="text-button send-password-button" href="javascript:void(0)" data-id="'.$this->model->id.'" data-ctid="'.$this->originComponentTypeId.'" data-cid="'.$this->model->contact->id.'><em class="icon i_stm_mail"></em>Send Password</a>';
                    ?>
                </td>
			</tr>
			<tr>
				<th>Notes:</th>
				<td><?php echo Yii::app()->format->formatTextArea($this->model->notes); ?></td>
			</tr>
            <?if((Yii::app()->user->checkAccess('dialer') || Yii::app()->user->checkAccess('owner')) && in_array($this->originComponentTypeId, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS))):?>
            <tr>
                <th>Dialer Lists:</th>
                <td>
                    <div id="call-list-tag-container">
                        <?php

                        switch($this->originComponentTypeId) {
                            case ComponentTypes::SELLERS:
                                $preset = CallLists::CUSTOM_SELLERS;
                                break;

                            case ComponentTypes::BUYERS:
                                $preset = CallLists::CUSTOM_BUYERS;
                                break;

                            case ComponentTypes::RECRUITS:
                                $preset = CallLists::CUSTOM_RECRUITS;
                                break;

                            default:
                                throw new Exception(__CLASS__.' (:'.__LINE__.') Invalid Component Type Id');
                                break;
                        }

                        $dialerLists = Yii::app()->db->createCommand()
                            ->select('clp.call_list_id call_list_id, l.name name, c.id contact_id')
                            ->from('call_list_phones clp')
                            ->join('call_lists l', 'l.id=clp.call_list_id')
                            ->join('phones p', 'p.id=clp.phone_id')
                            ->join('contacts c', 'c.id=p.contact_id')
                            ->where('c.id='.$this->model->id)
                            ->andWhere('l.preset_ma='.$preset)
                            ->andWhere('clp.is_deleted=0')
                            ->andWhere('l.is_deleted=0')
                            ->andWhere('clp.hard_deleted IS NULL')
                            ->group('clp.call_list_id')
                            ->queryAll();

                        foreach($dialerLists as $dialerList) {
                            ?>
                            <div id="call-list-tag-<?=$dialerList['call_list_id']?>" class="call-list-tag"><span class="label"><?=$dialerList['name']?></span><span class="remove-from-call-list" data-id="<?=$dialerList['call_list_id']?>" data-contactid="<?=$dialerList['contact_id']?>" data-ctid="<?=$this->originComponentTypeId?>" data-cid="<?=$this->originComponentId?>">X</span></div>
                            <?
                        }
                        ?>
                    </div>
                    <button class="text add-to-dialer-button" style="display: inline-block; margin-left: -4px;" data-ctid="<?=$this->originComponentTypeId?>" data-cid="<?=$this->originComponentId?>" data-contactid="<?=$this->model->id?>"><em class="icon i_stm_add"></em>Add to Dialer</button>
                </td>
            </tr>
            <? endif;?>
			</tbody>
		</table>
	</div>
</div>
<?php
if(in_array($this->originComponentTypeId, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS)) && (Yii::app()->user->checkAccess('dialer') || Yii::app()->user->checkAccess('owner'))) {
    Yii::import('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget');
    if (!class_exists('DialerListDialogWidget', false)) {

        Yii::app()->clientScript->registerScript('dialerAddToListJs', <<<JS
        function postAddToDialerList(data) {
            // return if there's no added data for some reason
            if(data.added == undefined) {
                Message.create("error", 'No phones were added to the Call List. Please verify that phone numbers exist for this contact.');
                return;
            }
            var callListTag = '<div id="call-list-tag-'+ data.added.call_list_id +'" class="call-list-tag"><span class="label">'+ data.added.name +'</span><span class="remove-from-call-list" data-id="'+ data.added.call_list_id +'" data-pid="' + data.added.phoneId + '" data-ctid="' + data.added.componentTypeId + '" data-cid="' + data.added.componentId + '" data-contactid="' + data.added.contactId + '">X</span></div>';
            $('#call-list-tag-container').append(callListTag);
        }
JS
        );
        $this->widget('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget', array(
                'id' => DialerListDialogWidget::DIALOG_ID,
                'title' => 'Add to Dialer',
                'triggerElement' => '.add-to-dialer-button',
                'postAddFunction' => 'postAddToDialerList',
            )
        );
    }
}

Yii::import('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget');
if (!class_exists('ClickToCallDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget', array(
            'id' => ClickToCallDialogWidget::CLICK_TO_CALL_DIALOG_ID,
            'title' => 'Click to Call',
            'triggerElement' => '.click-to-call-button',
        )
    );
}

Yii::import('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget');
$this->widget('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget', array(
        'id' => PhoneDialogWidget::PHONE_DIALOG_ID,
        'title' => 'Phone Actions',
        'triggerElement' => '.phone-status-button',
//        'parentModel' => $model,
    )
);
?>