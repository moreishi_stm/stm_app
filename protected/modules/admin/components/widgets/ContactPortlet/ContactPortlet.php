<?php

	class ContactPortlet extends StmBasePortlet {

		const EDIT_CONTACT_BUTTON_ID = 0;
		const NO_RESULT_TEXT = '';

		private  $_bombBombLists;

		public $containerId = 'contacts';
		public $containerClass = 'g12 p-0 p-fr';
		public $handleTitle = 'Contact Information';
		public $handleIconCssClass = 'i_user';
		public $handleButtons = array(
			array(
				'label' => 'Edit',
				'iconCssClass' => 'i_stm_edit',
				'htmlOptions' => array('id' => 'edit-contact-button'),
				'name' => 'edit-contact',
				'type' => 'link',
			)
		);

		/**
		 * Transaction types values are to be linked to the table transaction_types.
		 * Examples types: contacts, buyers, sellers, closings, projects, etc.
		 *
		 * @var int
		 */
		public $transactionType;
		public $model;
		public $profileContent;
		public $view;
        public $originComponentTypeId;
        public $originComponentId;

		public function init() {
			$this->initHandleButtons();
			parent::init();
		}

		public function initHandleButtons() {
			$this->handleButtons[self::EDIT_CONTACT_BUTTON_ID]['link'] = $this->controller->createUrl('contacts/edit', array('id' => $this->model->id));
		}

		public function bombBombLists() {
			if(!$this->_bombBombLists) {
				$BombBomb = Yii::app()->bombbomb;
				$data = $BombBomb->getLists();

				$listArray = array();
				if($data && is_array($data['info'])) {

					foreach($data['info'] as $list) {
						$listArray[$list['id']] = $list['name'];
					}
				}
				$this->_bombBombLists = CHtml::dropDownList('bombBombList',null, $listArray, $htmlOptions=array('style'=>'display:none;','class'=>'bombBombLists g8','id'=>'bombBombList-'.$list['id']));
			}

			return $this->_bombBombLists;
		}

		public function renderContent() {
            $moduleName = Yii::app()->controller->module->id;
            $js = <<<JS
                $('.remove-from-call-list').live('click', function() {

                    if(!confirm('Confirm delete from the Dialer Call List.')) {
                        return;
                    }

                    $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI

                    $.post('/$moduleName/dialer/removeFromCallList', {
                        callListId : $(this).data('id'),
                        contactId : $(this).data('contactid'),
                        componentTypeId : $(this).data('ctid'),
                        componentId : $(this).data('cid')
                    }, function(data) {
                        if(data.status=='success') {

                            Message.create('success','Successfully removed from the Dialer.');
                            $('#call-list-tag-' + data.callListId ).remove();
                            $.fn.yiiGridView.update("activity-log-grid");
                        } else if (data.status=='error') {
                            Message.create('error','Error removing from Call List.');
                        }
                    });
                });


                $('.send-password-button').click(function(){
                    var contactId = $(this).data('cid');
                    $('body').prepend('<div class="loading-container loading"><em></em></div>');
                    $.post( '/admin/contacts/sendPassword', { 'contactId':contactId}, function( data ) {
                        $("div.loading-container.loading").remove(); console.log(data);
                        if(data.result == 'success') {
                            Message.create('success','Successfully sent Password.');
                        } else {
                            Message.create('error','Error in sending Password. Please try again or contact support.');
                        }
                    }, "json");
                });

                $('.add-bombBomb-email').click(function(){
                    $(this).hide();
                    $(this).next('.bombBombLists').show('normal');
                    $('#bombBombList-button-'+$(this).data('id')).removeClass('hidden');
                });

                $('.bombBombList-button').click(function(){
                    var email = $(this).data('email');
                    var list = $(this).parent().find('select')
                    $('body').prepend('<div class="loading-container loading"><em></em></div>');
                    $.post( '/admin/bombBomb', { 'type':'addEmailToList','email': email, 'listId': list.val() }, function( data ) {
                        $("div.loading-container.loading").remove();
                        if(data.result != 'error') {
                            Message.create('success','Successfully added to BombBomb.');
                            list.hide('normal').remove();
                            $('.bombBombList-button').parent().append('<em class="bombBomb"></em>').hide().show('normal');
                            $('.bombBombList-button').hide('normal').remove();
                        } else {
                            Message.create('error','Error adding to BombBomb.');
                        }
                    });
            //			.done(function() {
            //				alert( "second success" );
            //			})
            //			.fail(function() {
            //				alert( "error" );
            //			})
            //			.always(function() {
            //				alert( "finished" );
            //			});

                });
JS;

            Yii::app()->clientScript->registerScript('contactPortletScript', $js);

//            Yii::app()->clientScript->registerCss('contactPortletCss', <<<CSS
//
//
//CSS
//            );

			// $taskModel = new Tasks;
			// $dataProvider = Contacts::getTasks($this->model->id);

			$dataProvider = new CArrayDataProvider(array()); //Temporarily so that the interface below is satisfied.

			if ($this->view == 'mini') {
				$this->render('contactPortletMini', array(
						'transactionType' => $this->transactionType,
						'dataProvider' => $dataProvider,
					)
				);
			} else {
				$this->render('contactPortlet', array(
						'transactionType' => $this->transactionType,
						'dataProvider' => $dataProvider,
						// 'taskModel' => $taskModel,
					)
				);
			}
		}
	}