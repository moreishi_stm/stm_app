<?php

class FlashMessageWidget extends CWidget
{
	const TOAST_MESSAGE_STYLE   = 'toast';
	const DEFAULT_MESSAGE_STYLE = 'default';

	public $useToastTheme = true;
	public $messageStyle;

	/**
	 * Keys are the setFlash() attribute names, the values correspond to the current themes css class name
	 */
	public $statusCssClasses = array(
		'success' => 'success',
		'error'   => 'error',
		'warning' => 'warning',
		'notice'  => 'notice',
	);

	public function init()
	{
		if ($this->useToastTheme)
			$this->initToastTheme();
		else
			$this->initDefaultTheme();
	}

	public function run()
	{
		foreach( $this->statusCssClasses as $status => $statusCssClass )
		{
			if ( ($usersFlashMessage = Yii::app()->user->getFlash($status)) )
			{
				$this->render('flashMessageWidget', array(
					'message'=>$usersFlashMessage,
					'cssClass'=>$statusCssClass,
				));
			}
		}
	}

	protected function initToastTheme() {
		$this->messageStyle = self::TOAST_MESSAGE_STYLE;
        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl.DS.'jquery.toastmessage.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($this->controller->module->cssAssetsUrl.DS.'jquery.toastmessage.css');
	}

	protected function initDefaultTheme() {
		$this->messageStyle = self::DEFAULT_MESSAGE_STYLE;
        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl.DS.'wl_Dialog.js');
	}
}