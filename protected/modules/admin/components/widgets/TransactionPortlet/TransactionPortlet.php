<?php

class TransactionPortlet extends StmBasePortlet {
	const EDIT_TRANSACTION_BUTTON_ID = 1;
	const ADD_CLOSING_BUTTON_ID      = 0;

	public $containerId = 'transactions';
	public $containerClass = 'g12 p-0 p-fr';
	public $handleTitle = 'Transactions';
	public $handleIconCssClass = 'i_archive';
	public $handleButtons      = array(
		array(
			'label'        =>'Add Closing',
			'iconCssClass' =>'i_stm_add',
			'htmlOptions'  =>array('id'=>'add-closing-button', 'target'=>'_blank'),
			'name'         =>'add-closing',
			'type'         =>'link',
			'link'         =>'/admin',
		),
		array(
			'label'        =>'Edit',
			'iconCssClass' =>'i_stm_edit',
			'htmlOptions'  =>array('id'=>'edit-transaction-button'),
			'name'         =>'edit-transaction',
			'type'         =>'link',
			'link'			=>'/admin',
		),
	);

	public $model;

	/**
	 * Stores which view the portlet will handle depending on transaction type in init()
	 * @var string
	 */
	public $view;

	public function init() {
		$this->registerAssets();
		$this->initHandleButtons();

		$this->view        = $this->model->transactionType;
		$this->handleTitle = $this->model->contact->fullName.' :: ' . ucwords(Yii::app()->format->formatSingular($this->model->transactionType)) . ' Details';

		switch($this->model->transactionType) {
			case Transactions::BUYERS:
				$this->handleIconCssClass = 'i_shopping_cart';
			break;
			case Transactions::SELLERS:
				$this->handleIconCssClass = 'i_key_2';
			break;
			case Transactions::LISTINGS:
				$this->handleIconCssClass = 'i_house';
			break;
		}

		parent::init();
	}

	public function renderContent() {
		echo CHtml::openTag('div', $htmlOptions=array(
			'class'=>'rounded-text-box odd-static'
		));

		if(!$Appointment = $this->model->appointment)
			$Appointment = new Appointments;

        $Opportunity = $this->model->opportunity;

		$this->render($this->view, array(
			'model'=>$this->model,
			'Appointment'=>$Appointment,
            'Opportunity'=>$Opportunity,
		));

		echo CHtml::closeTag('div');
	}

	public function initHandleButtons() {
		$transactionTypeName = strtolower($this->model->transactionType);
		$this->handleButtons[self::EDIT_TRANSACTION_BUTTON_ID]['link'] = $this->controller->createUrl($transactionTypeName.'/edit', array('id'=>$this->model->id));
		$this->handleButtons[self::ADD_CLOSING_BUTTON_ID]['link']      = $this->controller->createUrl('closings/add', array('id'=>$this->model->id));

		// If the transaction has a closing then hide the "Add Closing" handle button
        if ($this->model->nonDeadClosing) {
			$viewClosingButton =	array(
										'label'        =>'View Closing',
										'iconCssClass' =>'i_stm_search',
										'htmlOptions'  =>array('id'=>'view-closing-button'),
										'name'         =>'add-closing',
										'type'         =>'link',
										'link'         =>$this->controller->createUrl('/admin/closings/'.$this->model->nonDeadClosing->id),
									);

			$this->handleButtons[self::ADD_CLOSING_BUTTON_ID] = $viewClosingButton;
			// $this->handleButtons[self::ADD_CLOSING_BUTTON_ID]['visible'] = false;
        }
	}

	protected function registerAssets() {
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_widgets.TransactionPortlet.assets.css'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
		Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'transactionPortlet.css');
	}
}
