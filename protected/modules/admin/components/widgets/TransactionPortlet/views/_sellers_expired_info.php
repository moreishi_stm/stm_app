            <tr>
                <th>Expired Info:</th>
                <td>
                    <span class="g2 p-0 p-f0 label">Date:</span>
                    <span class="g8 p-0 p-f0 field">
                        <?php $expiredDate = $model->getFieldValue('expired_date');
                        echo ($expiredDate)? Yii::app()->format->formatDate($expiredDate, $dateFormat = 'm/d/Y') : '';?>
                    </span><br />
                    <span class="p-0 p-f0 label">MLS #:</span>
                    <span class="g3 p-0 p-f0 p-tl field"><?php echo $model->getFieldValue(expired_mls_number);?></span>
                    <span class="p-0 p-f0 label">Price:</span>
                    <span class="g3 p-0 p-f0 field"><?php echo Yii::app()->format->formatDollars($model->getFieldValue(expired_price));?></span>
                </td>
            </tr>
