<div class="g6 p-fl">
    <table class="container">
        <tbody>
            <tr>
                <th>Status:</th>
                <td>
                    <?php echo StmHtml::value($model,'status.name');?>
                    <?
                        if ($model->nonDeadClosing)
                        {
                            $icon = CHtml::tag('em', $htmlOptions=array(
                                'class'=>'icon i_stm_search',
                            ), null);

                            echo CHtml::link($icon.'View Closing', array('/admin/closings/view', 'id'=>$model->nonDeadClosing->id), $htmlOptions=array(
                                'class'=>'text-button',
                            ));
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <th>Sale Type:</th>
                <td>
                    <?php echo StmHtml::value($model, 'saleType');?>
                </td>
            </tr>
            <tr>
                <th>Assigned to:</th>
                <td>
                    <?php echo StmHtml::renderListItems($model->assignments, 'contactName', $htmlOptions=array('class'=>'strong')); ?>
                </td>
            </tr>
            <tr>
                <th>Source:</th>
                <td>
                    <?php echo StmHtml::value($model, 'source.name');?>
                </td>
            </tr>
            <tr>
                <th>Address:</th>
                <td>
                    <?php echo StmHtml::value($model, 'address.fullAddress');?>
                </td>
            </tr>
            <tr>
                <th>Bedrooms:</th>
                <td>
                    <span class="g2 p-0 field"><?php echo $model->getFieldValue('bedrooms');?></span>
                    <span class="g2 p-0 label">Baths:</span>
                    <span class="g2 p-0 field"><?php echo $model->getFieldValue('baths');?></span>
                    <span class="g2 p-0 label">Sq Ft:</span>
                    <span class="g1 p-0 field">
                        <?php
                            if ($model->getFieldValue('sq_feet', null) !== null)
                                echo Yii::app()->format->formatNumber($model->getFieldValue('sq_feet'));
                            else
                                echo StmFormatter::DEFAULT_VALUE;
                        ?>
                    </span>
                </td>
            </tr>
        	<tr>
                <th>Subdivision:</th>
                <td><?php echo $model->getFieldValue('subdivision');?></td>
            </tr>
            <tr>
                <th>Features:</th>
                <td><?php echo $model->getFieldValue('features');?></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="g6 p-fr">
    <table class="container">
        <tbody>
        	<tr>
                <th>MLS#:</th>
                <td><?php echo $model->getFieldValue('mls_num');?></td>
            </tr>
        	<tr>
                <th>IVR#:</th>
                <td><?php echo $model->getFieldValue('ivr_num');?></td>
                <th class="narrow">Lockbox#:</th>
                <td><?php echo $model->getFieldValue('lockbox_num');?></td>
            </tr>
        	<tr>
                <th>Listing Date:</th>
                <td><?php echo $model->getFieldValue('listing_date');?></td>
                <th class="wide">Days on Market:</th>
                <td><?php echo $model->getFieldValue('days_on_market');?></td>
            </tr>
        	<tr>
                <th>Upfront:</th>
                <td><?php echo $model->getFieldValue('upfront');?></td>
            </tr>
        	<tr>
                <th>Showing Instr:</th>
                <td colspan="2"><?php echo $model->getFieldValue('showing_instructions');?></td>
            </tr>
        	<tr>
                <th>Motivation:</th>
                <td colspan="2"><?php echo StmHtml::value($model, 'motivation');?></td>
            </tr>
        	<tr>
                <th>Mtg 1st:</th>
                <td><?php echo $model->getFieldValue('1st_mortgage');?></td>
                <th>2nd:</th>
                <td><?php echo $model->getFieldValue('2nd_mortgage');?></td>
            </tr>
        </tbody>
    </table>
</div>