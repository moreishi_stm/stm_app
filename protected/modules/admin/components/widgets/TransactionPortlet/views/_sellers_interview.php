			<tr>
				<th></th>
				<td colspan="2">
					<a id="view-seller-interview-button" class=" text-button" style="margin-left:0;"><em class="icon i_stm_search"></em><span id="seller-interview-button-text">View Seller Interview</span></a>
				</td>
			</tr>
        	<tr class="seller-interview hidden">
                <th>Motivation:</th>
                <td colspan="2">
                    <?php echo $model->getFieldValue(motivation);?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Where:</th>
                <td colspan="2">
                    <?php echo $model->getFieldValue('where_moving_to');?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Timeframe:</th>
                <td>
                    <?php echo $model->getFieldValue(target_date);?>
                </td>
                <th class="narrow">If Not Sell:</th>
                <td>
                    <?php echo $model->getFieldValue(if_not_happen);?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Est Value:</th>
                <td>
                    <?php echo $model->getFieldValue('idea_of_value');?>
                </td>
                <th class="narrow">Must Get:</th>
                <td>
                    <?php echo $model->getFieldValue('must_get_price');?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Mtg 1st:</th>
                <td><span style="min-width:100px; display:inline-block;">
                    <?php
                        if ($model->getFieldValue('mortgage_amount_1', null) !== null)
                            echo Yii::app()->format->formatDollars($model->getFieldValue('mortgage_amount_1'));
                        else
                            echo StmFormatter::DEFAULT_VALUE;
                    ?>
                    </span>
                    <span class="label">2nd:</span>
                    <?php
                        if ($model->getFieldValue('mortgage_amount_2', null) !== null)
                            echo Yii::app()->format->formatDollars($model->getFieldValue('mortgage_amount_2'));
                        else
                            echo StmFormatter::DEFAULT_VALUE;
                    ?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Sole Owner:</th>
                <td colspan="2">
                    <?php echo StmFormHelper::getYesNoName($model->getFieldValue('is_sole_owner'));?>
                    <span class="label">Are there any other decision makers? </span>
                    <?php echo $model->getFieldValue('owners_comment');?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Have Agent:</th>
                <td>
                    <?php echo StmFormHelper::getYesNoName($model->getFieldValue(other_agent));?>
                </td>
                <th class="narrow">Signed:</th>
                <td>
                    <?php echo StmFormHelper::getYesNoName($model->getFieldValue(other_agent_signed));?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Qualities Wanted:</th>
                <td colspan="3">
                    <?php echo $model->getFieldValue('agent_quality_1');?>
                    <?php echo $model->getFieldValue('agent_quality_2');?>
                    <?php echo $model->getFieldValue('agent_quality_3');?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Concerns:</th>
                <td colspan="3">
                    <?php echo $model->getFieldValue('concerns');?>
                </td>
            </tr>
			<tr class="seller-interview hidden">
                <th>Interview Others:</th>
                <td colspan="3">
                    <?php echo StmFormHelper::getYesNoName($model->getFieldValue('interview_others'));?>
                    <span class="label">Can we be last? </span>
                    <?php echo StmFormHelper::getYesNoName($model->getFieldValue('interview_last'));?>
                    <br /><span class="label">Who? </span>
                    <?php echo $model->getFieldValue('interview_who');?>
                </td>
            </tr>
            <tr class="seller-interview hidden">
                <th class="narrow">Trial Close:</th>
                <td colspan="3">
                    <?php echo StmFormHelper::getYesNoName($model->getFieldValue('trial_close'));?>
                </td>
            </tr>