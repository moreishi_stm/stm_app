<?php
$css = <<<CSS
#email-client-update-content {
    font-size: 13px !important;
    color: black;
}
#email-client-update-content table{
	float: none;
	background-image: none;
	width: inherit;
}
CSS;
Yii::app()->clientScript->registerCss('sellerTransactionPortletStyle', $css);

$domainSuffix = (YII_DEBUG) ? 'local': 'com';
$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";
Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);

$componentId = $model->id;
$componentTypeId = $model->component_type_id;
$sellerPortletViewScript = <<<JS
	$("#view-seller-interview-button").click(function(){
		if($("#view-seller-interview-button em").hasClass("i_stm_search")) {
			$('.seller-interview').each(function() {
				$(this).hide().removeClass("hidden").show("normal");
			});
			$(this).find('em').removeClass("i_stm_search").addClass("i_stm_delete");
			$("#seller-interview-button-text").html("Hide Seller Interview");
		} else {
			$('.seller-interview').each(function() {
				$(this).hide("normal").removeClass("hidden");
			});
			$(this).find('em').removeClass("i_stm_delete").addClass("i_stm_search");
			$("#seller-interview-button-text").html("View Seller Interview");
		}
	});

    $("#preview-email-client-update-button").click(function(){

        $("body").prepend("<div class='loading-container loading'><em></em></div>");

        $.post('/admin/contacts/emailUpdate',{ componentTypeId: $componentTypeId, componentId: $componentId, mode: "preview" }, function(data) {
            var clientUpdateHtml = '<div style="margin-bottom: 15px; text-align: center;"><button id="email-client-update-button" type="submit" class="submit wide">Email Client Update Now</button><button id="print-client-update-button" type="submit" class="submit wide">Print</button></div>' + data.html;
            $('#email-client-update-content').html(clientUpdateHtml);

            $("div.loading-container.loading").remove();

            $.fancybox({
                'type': 'inline',
                'content' : $('#email-client-update-content').html(),
                'width': 800,
                'height': 600,
                'autoSize':false,
                'scrolling':'yes',
                'autoDimensions':false,
                'padding': 40
            });

        }, 'json');

        return false;
    });

    $("#print-client-update-button").live('click',function(){

        $.post('/admin/contacts/emailUpdate',{ componentTypeId: $componentTypeId, componentId: $componentId, mode: "preview" }, function(data) {
    		var w = window.open('', 'Seller Update', 'width=785,height=700,scrollbars=yes');
    		w.document.write(data.html);
    		w.print();
        }, 'json');

        return false;
    });

    $("#email-client-update-button").live('click',function(){
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.post('/admin/contacts/emailUpdate',{ componentTypeId: $componentTypeId, componentId: $componentId, mode: "sendEmail" }, function(data) {
            // zero means success
            if(data.result == 'success') {
                Message.create('success','Seller Update has been sent.');
                $.fn.yiiGridView.update('activity-log-grid', {});
            }
            else {
                Message.create('error','Seller Update had an error. Please contact support.');
            }
            $.fancybox.close();
            $("div.loading-container.loading").remove();
        }, 'json');

        return false;
    });

JS;

Yii::app()->clientScript->registerScript('sellerPortletViewScript', $sellerPortletViewScript);

/*$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
        'target' => 'a#preview-email-client-update-button', 'config' => array(
            'width' => '800', 'height' => '600', 'scrolling' => 'auto', 'autoDimensions' => false, 'padding' => '40',
        ),
    )
);*/
?>
<div style="display: none">
    <div id="email-client-update-container">
        <div id="email-client-update-content" style="font-size: 14px; font-family: Arial, Helvetica, sans-serif !important;"></div>
    </div>
</div>

<div class="g6 p-fl">
    <table class="">
        <tbody>
        <tr>
            <th>Transaction ID:</th>
            <td><?php echo $model->id; ?></td>
        </tr>
        <tr>
            <th>Status:</th>
            <td>
                <?php echo StmHtml::value($model, 'status.name');?>
            </td>
        </tr>
        <tr>
            <th>Certified Pre-Owned:</th>
            <td>
                <?php echo $model->getFieldValue('certified_pre_owned');?>
            </td>
        </tr>        <tr <?php echo ($model->status->id == TransactionStatus::CANCELLED_ID) ? '' : 'style="display:none;"'; ?>>
            <th>Cancel Date:</th>
            <td>
                <?php echo $model->getFieldValue('cancel_date'); ?>
            </td>
        </tr>
        <tr>
            <th>Tags:</th>
            <td><?php echo Yii::app()->format->formatCommaDelimited($model->transactionTags,"name");?></td>
        </tr>
        <tr>
            <th>Sale Type:</th>
            <td><?php echo $model->getModelAttribute("getSaleTypes", $model->getFieldValue('sale_type_ma'));?></td>
        </tr>
        <tr>
            <th>Source:</th>
            <td><?php echo Sources::model()->skipSoftDeleteCheck()->findByPk($model->source_id)->name; ?></td>
        </tr>
        <? if($model->origin_type != 'Unknown'): ?>
            <tr>
                <th>Origin:</th>
                <td><?php echo StmHtml::value($model, 'origin_type'); ?></td>
            </tr>
        <? endif; ?>
        <tr>
            <th>Auto Email Updates:</th>
            <td>
                <?php echo $model->getModelAttribute("getEmailFrequencyTypes", $model->email_frequency_ma);?>
                <a href="#email-client-update-container" id="preview-email-client-update-button" class="text-button" style="margin-left:0;"><em class="icon i_stm_search"></em><span>Preview Seller Update Email</span></a>

            </td>
        </tr>
        <? if(Yii::app()->user->settings->market_snapshot == 1): ?>
            <tr>
                <th>Market Snapshot:</th>
                <td><?php echo ($model->getFieldValue('market_snapshot')) ? 'Every '.$model->getFieldValue('market_snapshot') . ' Week'.(($model->getFieldValue('market_snapshot') >1) ? 's': '') : '' ?></td>
            </tr>
        <? endif; ?>
        <tr>
            <th>Address:</th>
            <td>
                <?php echo Yii::app()->format->formatAddress($model->address);?>
            </td>
        </tr>
        <tr>
            <th>Bedrooms:</th>
            <td>
                <span class="g1 p-0 p-f0 field"><?php echo $model->getFieldValue(bedrooms);?></span>
                <span class="g4 p-0 p-f0 p-tr  label">Baths:</span>
                <span class="g1 p-0 p-f0 field"><?php echo $model->getFieldValue(baths);?></span>
                <span class="g4 p-0 p-f0 p-tr  label">Half Baths:</span>
                <span class="g1 p-0 p-f0 field"><?php echo $model->getFieldValue(half_baths);?></span>
            </td>
        </tr>
        <tr>
            <th>SF:</th>
            <td>
                <span class="g1 p-0 p-f0 field"><?php echo $model->getFieldValue(sq_feet);?></span>
                <span class="g4 p-0 p-f0 p-tr  label">Year Built:</span>
                <span class="g1 p-0 p-f0 field"><?php echo $model->getFieldValue(year_built);?></span>
            </td>
        </tr>
        <tr>
            <th>Subdivision:</th>
            <td>
                <?php echo $model->getFieldValue(area);?>
            </td>
        </tr>
        <?  if ($model->transaction_status_id == TransactionStatus::ACTIVE_LISTING_ID
            || $model->transaction_status_id == TransactionStatus::UNDER_CONTRACT_ID
            || $model->transaction_status_id == TransactionStatus::CLOSED_ID
            || $model->transaction_status_id == TransactionStatus::EXPIRED_ID
            || $model->transaction_status_id == TransactionStatus::CANCELLED_ID
        ) {

            // $this->render('_sellers_listing_info', array('model'=>$model));
        } else {
            $this->render('_sellers_expired_info', array('model' => $model));
        }?>
        <? if($Opportunity): ?>
            <tr>
                <th colspan="2"><hr style="margin: 0;"/></th>
            </tr>
            <tr>
                <th>Opportunity Found by:</th>
                <td><?=$Opportunity->addedBy->fullName?> @ <?=Yii::app()->format->formatDate($Opportunity->added)?></td>
            </tr>
            <tr>
                <th>Opportunity Target Move Date:</th>
                <td><?=Yii::app()->format->formatDate($Opportunity->target_date)?></td>
            </tr>
            <tr>
                <th>Opportunity Next Follow-up Date:</th>
                <td><?=Yii::app()->format->formatDate($Opportunity->next_followup_date)?></td>
            </tr>
            <tr>
                <th>How well did you Dig for Motivation:</th>
                <td><?=($Opportunity->dig_motivation_rating) ? $Opportunity->dig_motivation_rating.' out of 10' : ''?></td>
            </tr>
        <? else: ?>
            <tr>
                <th>Opportunities:</th>
                <td>None</td>
            </tr>
        <? endif; ?>

        <? if(!$Appointment->isNewRecord): ?>
            <tr>
                <th colspan="2"><hr style="margin: 0;"/></th>
            </tr>
            <tr>
                <th>Appt by:</th>
                <td colspan="3">
                    <?php echo $model->appointment->setBy->fullName;?>
                    <?php if ($Appointment->set_on_datetime > 0) {
                        echo '<br/><label class="label p-p5">@</label>';
                    } ?>
                    <?php echo Yii::app()->format->formatDateTime($Appointment->set_on_datetime);?>
                </td>
            </tr>
            <tr>
                <th>Appt Set Method:</th>
                <td colspan="3">
                    <?php echo $Appointment->taskType->name;?>
                </td>
            </tr>
            <tr>
                <th>Appt Set for:</th>
                <td colspan="3">
                    <?php echo $model->appointment->metBy->fullName;?>
                    <?php if ($Appointment->set_for_datetime > 0) {
                        echo '<br/><label class="label p-p5">@</label>';
                    } ?>
                    <?php echo Yii::app()->format->formatDateTime($Appointment->set_for_datetime);?>
                </td>
            </tr>
            <tr>
                <th>Appt Location:</th>
                <td><?php echo $Appointment->getModelAttribute(
                        "getLocationTypes", $Appointment->location_ma
                    );?><?php if ($Appointment->location_other) {
                        echo ': ' . $Appointment->location_other;
                    }?></td>
            </tr>
            <tr>
                <th>Appt Status:</th>
                <td>
                    <?php echo $Appointment->getModelAttribute("getMetStatusTypes", $Appointment->met_status_ma);?>
                    <?php echo ($Appointment->met_status_ma == Appointments::MET_STATUS_CANCEL || $Appointment->met_status_ma == Appointments::MET_STATUS_RESCHEDULE)? ' Reason: '.$Appointment->met_status_reason : ''; ?>
                </td>
            </tr>
            <tr>
                <th>How well did you Dig for Motivation?</th>
                <td>
                    <?php echo ($Appointment->dig_motivation_rating) ? $Appointment->dig_motivation_rating .' out of 10' : '';?>
                </td>
            </tr>
            <tr>
                <th>Signed:</th>
                <td>
                    <?php echo StmFormHelper::getYesNoName(
                        $model->appointment->is_signed
                    );?><?php if ($Appointment->not_signed_reason) {
                        echo
                            ': ' . $Appointment->getModelAttribute(
                                "getNotSignedSellerReasonTypes", $Appointment->not_signed_reason
                            );
                    }?>
                    <?php if ($Appointment->not_signed_reason_other) {
                        echo
                            ': ' . $Appointment->not_signed_reason_other;
                    }?>
                    <?php if ($Appointment->signed_date>0) {
                        echo
                            ': ' . $Appointment->signed_date;
                    }?>
                </td>
            </tr>
        <? else: ?>
            <tr>
                <th>Appointments:</th>
                <td>None</td>
            </tr>
        <? endif; ?>
        <tr>
            <th>Features:</th>
            <td>
                <?php echo Yii::app()->format->formatTextArea($model->getFieldValue(features));?>
            </td>
        </tr>

        <? if ($model->getFieldValue('showing_instructions')) { ?>
            <tr>
                <th>Showing Instr:</th>
                <td colspan="2"><?php echo Yii::app()->format->formatTextArea(
                        $model->getFieldValue('showing_instructions')
                    );?></td>
            </tr>
        <? } ?>

        <tr>
            <th class="narrow">Notes:</th>
            <td>
                <?php echo Yii::app()->format->formatTextArea($model->notes);?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="g6 p-fr">
    <table class="">
        <tbody>
        <?php if (Yii::app()->user->checkAccess('cmsEditListingStoryboardAction')) { ?>
            <tr>
                <th>Storyboard:</th>
                <td><?php
                    if (empty($model->cms)) {
                        $addressIsMissing = (int) empty($model->address);
                        echo CHtml::link(
                            '<em class="icon i_stm_add"></em>Add Storyboard',
                            array('/admin/cms/addListingStoryboard', 'transactionId' => $model->id),
                            array('style' => 'margin-left: 0;', 'class' => 'text-button', 'onClick'=>"
                                if ($addressIsMissing) {
                                    alert('Address is required to add a Storyboard.');
                                    return false;
                                }
                            ")
                        );
                    } else {
                        echo CHtml::link(
                            '<em class="icon i_stm_search"></em>View Storyboard</em>',
                            Yii::app()->controller->createUrl('/front/site/storyboard', array('title'=>$model->cms[0]->url, 'id'=>$model->cms[0]->id)),
                            array('id'    => 'view-storyboard-button', 'class' => 'text-button', 'target' => '_blank',
                                'style' => 'margin-left: 0px; display: block',)
                        );
                        echo CHtml::link(
                            '<em class="icon i_stm_edit"></em>Edit Storyboard</em>',
                            array('/admin/cms/editListingStoryboard', 'id' => $model->cms[0]->id),
                            array('id'    => 'add-storyboard-button', 'class' => 'text-button', 'target' => '_blank',
                                'style' => 'margin-left: 0px; display: block',)
                        );
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <th>Assigned to:</th>
            <td>
                <?php
                if ($Assignments = $model->nonLenderAssignments) {
                    foreach ($Assignments as $Assignment) {
                        echo'<span style="font-weight:bold; display:inline-block;">' . $Assignment->contact->fullName
                            . ' (' . $Assignment->assignmentType->display_name . ')</span>';
                    }
                }
                ?>
            </td>
        </tr>
        <? $this->render('_sellers_listing_info', array('model' => $model));
        $this->render('_sellers_interview', array('model' => $model));
        ?>
        </tbody>
    </table>
</div>