<div class="g6 p-fl">
	<table class="">
		<tbody>
		<tr>
			<th>Transaction ID:</th>
			<td><?php echo $model->id; ?></td>
		</tr>
		<tr>
			<th>Status:</th>
			<td><?php echo StmHtml::value($model, 'status.name'); ?></td>
		</tr>
        <tr>
            <th>Tags:</th>
            <td><?php echo Yii::app()->format->formatCommaDelimited($model->transactionTags,"name");?></td>
        </tr>
		<tr>
			<th>Location:</th>
			<td><?php echo $model->getFieldValue('location'); ?></td>
		</tr>
		<tr>
			<th>Price:</th>
			<td>
				<?php
					$hasMinPrice = $model->getFieldValue('price_min', null) !== null;
					$hasMaxPrice = $model->getFieldValue('price_max', null) !== null;

					if ($hasMinPrice && $hasMaxPrice) {
						echo Yii::app()->format->formatDollars($model->getFieldValue('price_min'));
						echo ' - ';
						echo Yii::app()->format->formatDollars($model->getFieldValue('price_max'));
					} elseif($hasMinPrice) {
                        echo Yii::app()->format->formatDollars($model->getFieldValue('price_min'));
                    } elseif($hasMaxPrice) {
                        echo Yii::app()->format->formatDollars($model->getFieldValue('price_max'));
                    } else
                    {
						echo StmFormatter::DEFAULT_VALUE;
					}
				?>
			</td>
		</tr>
		<tr>
			<th>Motivation:</th>
			<td><?php echo $model->getFieldValue('motivation'); ?></td>
		</tr>
		<tr>
			<th>Agent:</th>
			<td><?php echo StmFormHelper::getYesNoName($model->getFieldValue('other_agent')); ?></td>
		</tr>
		<tr>
			<th>Mortgage:</th>
			<td>
				<?php
					$prequalAmount = $model->getFieldValue('prequal_amount', null);

					if (($prequalAmount != null) && ($prequalAmount > 0)) {
						echo Yii::app()->format->formatDollars($model->getFieldValue('prequal_amount'));
					} else {
						echo StmFormatter::DEFAULT_VALUE;
					}
				?> <span class="label">Pre-qual Date:</span>
				<?php echo Yii::app()->format->formatDate($model->getFieldValue('prequal_date')); ?>
			</td>
		</tr>
		<tr>
			<th></th>
			<td><span class="label">Lender:</span> <?php echo $model->getFieldValue('lender'); ?></td>
		</tr>
        <? if($Opportunity): ?>
            <tr>
                <th colspan="2"><hr style="margin: 0;"/></th>
            </tr>
            <tr>
                <th colspan="2" style="text-align: center;">OPPORTUNITY</th>
            </tr>
            <tr>
                <th>Found by:</th>
                <td><?=$Opportunity->addedBy->fullName?> @ <?=Yii::app()->format->formatDate($Opportunity->added)?></td>
            </tr>
            <tr>
                <th>Target Date:</th>
                <td><?=Yii::app()->format->formatDate($Opportunity->target_date)?></td>
            </tr>
            <tr>
                <th>Next Follow-up Date:</th>
                <td><?=Yii::app()->format->formatDate($Opportunity->next_followup_date)?></td>
            </tr>
            <tr>
                <th>How well did you Dig for Motivation:</th>
                <td><?=($Opportunity->dig_motivation_rating) ? $Opportunity->dig_motivation_rating.' out of 10' : ''?></td>
            </tr>
        <? else: ?>
            <tr>
                <th>Opportunities:</th>
                <td>None</td>
            </tr>
        <? endif; ?>
        <tr>
            <th colspan="2"><hr style="margin: 0;"/></th>
        </tr>
        <? if(!$Appointment->isNewRecord): ?>
		<tr>
			<th>Appt Set by:</th>
			<td colspan="3">
				<?php echo $model->appointment->setBy->fullName; ?>
				<?php if ($Appointment->set_on_datetime > 0) {
					echo '<br/><label class="label p-p5">@</label>';
				} ?>
				<?php echo Yii::app()->format->formatDateTime($Appointment->set_on_datetime); ?>
			</td>
		</tr>
		<tr>
			<th>Appt Set Method:</th>
			<td colspan="3">
				<?php echo $Appointment->taskType->name; ?>
			</td>
		</tr>
		<tr>
			<th>Appt Set for:</th>
			<td colspan="3">
				<?php echo $model->appointment->metBy->fullName; ?>
				<?php if ($Appointment->set_for_datetime > 0) {
					echo '<br/><label class="label p-p5">@</label>';
				} ?>
				<?php echo Yii::app()->format->formatDateTime($Appointment->set_for_datetime); ?>
			</td>
		</tr>
		<tr>
			<th>Appt Location:</th>
			<td><?php echo $Appointment->getModelAttribute("getLocationTypes", $Appointment->location_ma); ?><?php if ($Appointment->location_other) {
					echo ': ' . $Appointment->location_other;
				} ?></td>
		</tr>
		<tr>
			<th>Appt Status:</th>
			<td><?php echo $Appointment->getModelAttribute("getMetStatusTypes", $Appointment->met_status_ma); ?>
                <?php echo ($Appointment->met_status_ma == Appointments::MET_STATUS_CANCEL || $Appointment->met_status_ma == Appointments::MET_STATUS_RESCHEDULE)? ' Reason: '.$Appointment->met_status_reason : ''; ?>
            </td>
		</tr>
        <tr>
            <th>How well did you Dig for Motivation?</th>
            <td>
                <?php echo ($Appointment->dig_motivation_rating) ? $Appointment->dig_motivation_rating .' out of 10' : '';?>
            </td>
        </tr>
		<tr>
			<th>Signed:</th>
			<td>
				<?php echo StmFormHelper::getYesNoName($model->appointment->is_signed); ?><?php if ($Appointment->not_signed_reason) {
					echo ': ' . $Appointment->getModelAttribute("getNotSignedBuyerReasonTypes", $Appointment->not_signed_reason);
				} ?><?php if ($Appointment->not_signed_reason_other) {
					echo ': ' . $Appointment->not_signed_reason_other;
				} ?>
			</td>
		</tr>
        <? else: ?>
        <tr>
            <th>Appointment:</th>
            <td>None</td>
        </tr>
        <? endif; ?>
		</tbody>
	</table>
</div>
<div class="g6 p-fr">
	<table class="">
		<tbody>
        <?php if (Yii::app()->user->checkAccess('cmsEditBuyerStoryboardAction')) { ?>
            <tr>
                <th>Storyboard:</th>
                <td><?php
                    if (empty($model->cms)) {
                        echo CHtml::link(
                            '<em class="icon i_stm_add"></em>Add Storyboard',
                            array('/admin/cms/addBuyerStoryboard', 'transactionId' => $model->id),
                            array('style' => 'margin-left: 0;', 'target' => '_blank', 'class' => 'text-button')
                        );
                    } else {
                        echo CHtml::link(
                            '<em class="icon i_stm_search"></em>View Storyboard</em>',
                            Yii::app()->controller->createUrl('/front/site/storyboard', array('title'=>$model->cms[0]->url, 'id'=>$model->cms[0]->id)),
//                            array('/storyboard/'.$model->cms[0]->url.'/'.$model->cms[0]->id),
                            array('id'    => 'view-storyboard-button', 'class' => 'text-button', 'target' => '_blank',
                                  'style' => 'margin-left: 0px; display: block',)
                        );
                        echo CHtml::link(
                            '<em class="icon i_stm_edit"></em>Edit Storyboard</em>',
                            array('/admin/cms/editBuyerStoryboard', 'id' => $model->cms[0]->id),
                            array('id'    => 'add-storyboard-button', 'class' => 'text-button', 'target' => '_blank',
                                  'style' => 'margin-left: 0px; display: block',)
                        );
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>
		<tr>
			<th>Assigned to:</th>
			<td>
				<?php
					if ($Assignments = $model->nonLenderAssignments) {
						foreach ($Assignments as $Assignment) {
							echo '<span style="font-weight:bold; display:inline-block;">' . $Assignment->contact->fullName . ' (' . $Assignment->assignmentType->display_name . ')</span>';
						}
					}
				?>
			</td>
		</tr>
        <tr>
            <th>Preferred Lender:</th>
            <td>
                <?php
                if(($Lender = $model->assignmentLender) && $Lender->contact->fullName) {
                    echo '<span style="font-weight:bold; display:inline-block;">' . $Lender->contact->fullName . ' (' . $Lender->assignmentType->display_name . ')</span>';
                }
                ?>
            </td>
        </tr>
        <tr>
			<th>Source:</th>
			<td><?php echo StmHtml::value($model, 'source.name'); ?></td>
		</tr>
        <? if($model->origin_type != 'Unknown'): ?>
        <tr>
            <th>Origin:</th>
            <td><?php echo StmHtml::value($model, 'origin_type'); ?></td>
        </tr>
        <? endif; ?>
		<tr>
			<th>Target Date:</th>
			<td><?php echo $model->getFieldValue('target_date'); ?></td>
		</tr>
		<tr>
			<th>Bedrooms:</th>
			<td>
				<span class="g2 p-0 p-f0 field"><?php echo $model->getFieldValue('bedrooms'); ?></span>
				<span class="g3 p-0 p-f0 label">Baths:</span>
				<span class="g2 p-0 p-f0 field"><?php echo $model->getFieldValue('baths'); ?></span>
				<span class="g3 p-0 p-f0 label">Sq Ft:</span>
				<span class="g1 p-0 p-f0 field"><?php echo $model->getFieldValue('sq_feet'); ?></span>
			</td>
		</tr>
		<tr>
			<th>Features:</th>
			<td><?php echo $model->getFieldValue('features'); ?></td>
		</tr>
		<tr>
			<th>Best Time to Look:</th>
			<td><?php echo $model->getFieldValue('best_time_to_look'); ?></td>
		</tr>
		<tr>
			<th>Own/Rent:</th>
			<td><?php echo $model->getFieldValue('own_rent'); ?></td>
		</tr>
		<tr>
			<th>Home to Sell?:</th>
			<td><?php echo StmFormHelper::getYesNoName($model->getFieldValue('home_to_sell')); ?></td>
		</tr>
		</tbody>
	</table>
</div>
<div class="g12 p-0">
	<table class="">
		<tbody>
		<tr>
			<th class="narrow">Notes:</th>
			<td>
                <?php echo Yii::app()->format->formatTextArea($model->notes);?>
			</td>
		</tr>
		</tbody>
	</table>
</div>