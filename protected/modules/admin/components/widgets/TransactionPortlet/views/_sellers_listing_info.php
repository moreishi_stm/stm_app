        	<tr>
                <th>Price:</th>
                <td><?php echo Yii::app()->format->formatDollars($model->getFieldValue('price_current'));?></td>
            </tr>
        	<tr>
                <th>Original Price:</th>
                <td><?php echo Yii::app()->format->formatDollars($model->getFieldValue('price_original'));?></td>
            </tr>
			<tr>
				<th>Recommended Price:</th>
				<td><?php echo Yii::app()->format->formatDollars($model->getFieldValue('price_recommend'));?></td>
			</tr>
        	<tr>
                <th>MLS#:</th>
                <td><?php echo $model->getFieldValue('mls_number');?></td>
            </tr>
        	<tr>
                <th>IVR#:</th>
                <td><?php echo $model->getFieldValue('ivr_extension');?>
	                <span class="label">Lockbox#:</span>
	                <?php echo $model->getFieldValue('lockbox_number');?>
    			</td>
            </tr>
        	<tr>
                <th>Listing Date:</th>
                <td><?php echo $model->getFieldValue('listing_date');?></td>
            </tr>
            <tr>
                <th>Expire Date:</th>
                <td><?php echo $model->getFieldValue('listing_expire_date');?></td>
            </tr>
        	<tr>
                <th>Days on  Market:</th>
                <td><?php $listingDate = $model->getFieldValue('listing_date');
                    echo ($listingDate) ? Yii::app()->format->formatDays(Yii::app()->format->formatDate($listingDate,'Y-m-d')) : '';?></td>
            </tr>
        	<tr>
                <th>Upfront:</th>
                <td><?php echo $model->getFieldValue('upfront_fee');?></td>
            </tr>
            <tr>
                <th>Home Inspection:</th>
                <td>
                    <?php echo ($model->getFieldValue('home_inspection_ordered_date'))? 'Ordered: &nbsp; &nbsp; &nbsp; &nbsp; '.$model->getFieldValue('home_inspection_ordered_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('home_inspection_date'))? 'Appointment: '. $model->getFieldValue('home_inspection_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('home_inspection_received_date'))? 'Received: &nbsp; &nbsp; &nbsp; &nbsp;'.$model->getFieldValue('home_inspection_received_date').'<br>' : '';?>
                </td>
            </tr>
            <tr>
                <th>Appraisal:</th>
                <td>
                    <?php echo ($model->getFieldValue('appraisal_ordered_date'))? 'Ordered: &nbsp; &nbsp; &nbsp; &nbsp; '.$model->getFieldValue('appraisal_ordered_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('appraisal_inspection_date'))? 'Appointment: '. $model->getFieldValue('appraisal_inspection_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('appraisal_received_date'))? 'Received: &nbsp; &nbsp; &nbsp; &nbsp;'.$model->getFieldValue('appraisal_received_date').'<br>' : '';?>
                </td>
            </tr>
            <tr>
                <th>Staging:</th>
                <td>
                    <?php echo ($model->getFieldValue('staging_ordered_date'))? 'Ordered: &nbsp; &nbsp; &nbsp; &nbsp; '.$model->getFieldValue('staging_ordered_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('staging_inspection_date'))? 'Appointment: '. $model->getFieldValue('staging_inspection_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('staging_report_received_date'))? 'Received: &nbsp; &nbsp; &nbsp; &nbsp;'.$model->getFieldValue('staging_report_received_date').'<br>' : '';?>
                </td>
            </tr>
            <tr>
                <th>Photography:</th>
                <td>
                    <?php echo ($model->getFieldValue('photography_ordered_date'))? 'Ordered: &nbsp; &nbsp; &nbsp; &nbsp; '.$model->getFieldValue('photography_ordered_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('photography_date'))? 'Appointment: '. $model->getFieldValue('photography_date').'<br>' : '';?>
                    <?php echo ($model->getFieldValue('photography_received_date'))? 'Received: &nbsp; &nbsp; &nbsp; &nbsp;'.$model->getFieldValue('photography_received_date').'<br>' : '';?>
                </td>
            </tr>
            <tr>
                <th>Mtg 1st:</th>
                <td><span style="min-width:100px; display:inline-block;">
                    <?php
                        if ($model->getFieldValue('mortgage_amount_1', null) !== null)
                            echo $model->getFieldValue('mortgage_amount_1');
                        else
                            echo StmFormatter::DEFAULT_VALUE;
                    ?>
                    </span>
                    <span class="label">2nd:</span>
                    <?php
                        if ($model->getFieldValue('mortgage_amount_2', null) !== null)
                            echo $model->getFieldValue('mortgage_amount_2');
                        else
                            echo StmFormatter::DEFAULT_VALUE;
                    ?>
                </td>
            </tr>