<?php
/**
 * Author: christinelee
 * Date: 4/21/13
 */
$fromName = $this->fromName;
$toName = $this->toName;
$isForm = $this->isForm;
$formSelector = $this->formSelector;
$returnSubmit = ($this->returnSubmit=='true') ? 'true' : 'false';
$updateNonGridElement = $this->updateNonGridElement;
$gridName = $this->gridName;
$customFromDateFieldId = $this->customFromDateFieldId;
$customToDateFieldId = $this->customToDateFieldId;
$fromDateLabelSelector = $this->fromDateLabelSelector;
$toDateLabelSelector = $this->toDateLabelSelector;

Yii::app()->clientScript->registerScript('dateRanger', <<<JS

	$("select#DateRanger_date_preset").change(function(){
		if($("select#DateRanger_date_preset").val() == "custom")
			$("#custom-date-container").show("slow");
		else {
			$("#custom-date-container").hide("slow");
			$("#$customFromDateFieldId").val("");
			$("#$customToDateFieldId").val("");
		}
	});

	$("$formSelector").submit(function() {

        var datePresetValue = $("#DateRanger_date_preset").val();
        var datePresetArray  = jQuery.parseJSON('$date_preset');
        var dateRange = datePresetArray[datePresetValue];

        var nonGridScript = "";
        var nonGridElement = "$updateNonGridElement";


        if(nonGridElement) {
            if("$gridName" != "") {
                $.fn.yiiGridView.update("$gridName", {
                    data: $(this).serialize(),
                    complete: function(jqXHR, status) {
                            if (status=='success'){
                                var html = $.parseHTML(jqXHR.responseText);
                                var elementsToUpdate = "$updateNonGridElement";
                                elementsToUpdate = elementsToUpdate.split(",");

                                $.each(elementsToUpdate, function(key, elementName) {
                                    var elementContent = $(elementName, $(html)).html();
                                    $(elementName).html(elementContent);
                                });
                            }
                        }
                });
            }
        } else {
            if("$gridName" != "") {
                $.fn.yiiGridView.update("$gridName", { data: $(this).serialize()});
            }
        }

        if("$customFromDateFieldId" != "" && "$customToDateFieldId" != "") {

            var fromDate = $("#$customFromDateFieldId").val();
            var toDate = $("#$customToDateFieldId").val();

            if(datePresetValue == "custom") {
                if(fromDate) {
                    $("$fromDateLabelSelector").html(fromDate);
                }

                if(toDate) {
                    $("$toDateLabelSelector").html(toDate);
                }
            } else if("$fromDateLabelSelector" !=""){

                $("$fromDateLabelSelector").html(dateRange.from_date);
                $("$toDateLabelSelector").html(dateRange.to_date);
            }
        }

        if("$isForm" != "" || "$returnSubmit" == "false") {
	        return false;
        }
	});
JS
);

if($this->container) {
	echo $openTag = CHtml::openTag($this->container['tag'], $this->container['htmlOptions']);
}
?>
	<?php
	if($this->isForm) {
		$form=$this->beginWidget('CActiveForm', array(
				'id'=>'date-ranger-form',
				'action'=>'',
				'method'=>'get',
		));
	} else
		echo '<div id="date-ranger-form">';
		?>
		<?php echo ($this->submitButtonVisible) ? CHtml::submitButton('Go', $htmlOptions=array('class'=>'p-fr button gray date-ranger-go-button','style'=>'font-size:10px;top:-4px;')) : ''; ?>
			<div id="custom-date-container" class="p-fr" style="<?php echo ($this->defaultSelect == 'custom' && ($this->custom_from_date || $this->custom_to_date)) ? '' : 'display: none;'; ?> width:300px;">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
																'model'=>$this->model,
																'attribute'=>$this->toDateField,  // name of post parameter
																'name'=>$this->toName,
				                                                'options'=>array(
																	'numberOfMonths'=>2,
					                                                'showAnim'=>'fold',
					                                                'dateFormat'=>'mm/dd/yy',
				                                                ),
                                                                'value' => $this->custom_to_date,
				                                                'htmlOptions'=>array(
					                                                'style'=>'width:90px;font-size: 12px;',
					                                                'class'=>'g2 p-fr',
				                                                ),
				                                                ));
				?>
				<div class="p-fr p-pt10 p-ph4"> to </div>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
																'model'=>$this->model,
				                                                'attribute'=>$this->fromDateField,
																'name'=>$this->fromName,
				                                                'options'=>array(
																	'numberOfMonths'=>2,
																	'showAnim'=>'fold',
					                                                'dateFormat'=>'mm/dd/yy',
				                                                ),
                                                                'value' => $this->custom_from_date,
				                                                'htmlOptions'=>array(
					                                                'style'=>'width:90px;font-size: 12px;',
					                                                'class'=>'g2 p-fr',
				                                                ),
				                                                ));

				?>
				<div class="p-fr p-pt10">Date Range:</div>
			</div>
		<?php echo CHtml::dropDownList('DateRanger[date_preset]', $this->defaultSelect, $this->getDateRangePresetList(), $htmlOptions=array('class'=>'p-fr p-mh8','style'=>'font-size:13px;top:-4px;')); ?>
<?php
	if($this->isForm) {
//		CHtml::hiddenField($this->fromName,null);
//		CHtml::hiddenField($this->toName,null);
		$this->endWidget();
	} else {
		echo '</div>';
	}

	if($this->container) {
		echo $closeTag = CHtml::closeTag($this->container['tag']);
	}
?>