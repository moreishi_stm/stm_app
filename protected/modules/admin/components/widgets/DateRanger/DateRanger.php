<?php
	/*
	 * DateRanger widget class file.
	 * @author Christine Lee
	 */
	class DateRanger extends CWidget {

		public $gridName;
		public $isForm = true;
        public $returnSubmit = true;
		public $container = array('tag'=>'div','htmlOptions'=>array('class'=>'g12 p-fr'));
		public $formSelector = '#date-ranger-form';
		public $model = null;
		public $fromName = 'DateRanger[custom_from_date]';
		public $toName = 'DateRanger[custom_to_date]';
        public $customFromDateFieldId = 'DateRanger_custom_from_date';
        public $customToDateFieldId = 'DateRanger_custom_to_date';

		public $defaultSelect = 'this_month';
		public $datePresetList;
        public $submitButtonVisible = true;

        public $custom_from_date;
        public $custom_to_date;

        protected $fromDateField = 'custom_from_date';
		protected $toDateField = 'custom_to_date';

		public $fromDateSelector;
		public $toDateSelector;
		public $fromDateLabelSelector = '#from-date-label';
		public $toDateLabelSelector = '#to-date-label';

        public $updateNonGridElement = '';

        public function init() {
			//check to see if fromName/toName pair OR model/toDateField/fromDateField exists. If not, throw error
			if ($this->model) {
				$this->fromDateField = $this->fromName;
				$this->toDateField = $this->toName;
			}

//            $DateRanger = $_REQUEST['DateRanger'];
//            if($DateRanger[$this->fromDateField]) {
//                $this->custom_from_date = ($DateRanger[$this->fromDateField])? $$this->fromName: null;
//                $this->custom_to_date = ($DateRanger[$this->toDateField])? ($$this->fromName): null;
//            }
		}

		public function run() {
			$date_preset = array(
				'All Time' => $this->getPresetDates('all', 'm/d/Y'),
				'today' => $this->getPresetDates('today', 'm/d/Y'),
                'yesterday' => $this->getPresetDates('yesterday', 'm/d/Y'),
				'up_to_today' => $this->getPresetDates('today', 'm/d/Y'),
				'last_1_week' => $this->getPresetDates('last_1_week', 'm/d/Y'),
				'last_2_weeks' => $this->getPresetDates('last_2_weeks', 'm/d/Y'),
				'month_to_date' => $this->getPresetDates('month_to_date', 'm/d/Y'),
				'this_month' => $this->getPresetDates('this_month', 'm/d/Y'),
				'last_month' => $this->getPresetDates('last_month', 'm/d/Y'),
				'last_30_days' => $this->getPresetDates('last_30_days', 'm/d/Y'),
                'last_3_months' => $this->getPresetDates('last_3_months', 'm/d/Y'),
                'last_6_months' => $this->getPresetDates('last_6_months', 'm/d/Y'),
                'last_9_months' => $this->getPresetDates('last_9_months', 'm/d/Y'),
				'last_12_months' => $this->getPresetDates('last_12_months', 'm/d/Y'),
				'this_year' => $this->getPresetDates('this_year', 'm/d/Y'),
			);
			if($this->datePresetList) {
				$customPresetList = array();
				foreach($this->datePresetList as $key => $value) {
					$customPresetList[$key] = $value;
				}
				$date_preset = $customPresetList;
			}

			$this->render('dateRanger', array('date_preset' => CJSON::encode($date_preset)));
		}

		public function getDateRange($dates) {

			if($dates['date_preset'] == 'custom') {
				if ($dates['custom_from_date'] > 0) {
                    $dateRange['from_date'] = Yii::app()->format->formatDate($dates['custom_from_date'], $dateFormat = 'Y-m-d');
				}

				if ($dates['custom_to_date'] > 0) {
                    $dateRange['to_date'] = Yii::app()->format->formatDate($dates['custom_to_date'], $dateFormat = 'Y-m-d');
				}
				return $dateRange;
			} else {
				if (!$dates) {
					$dates['date_preset'] = $this->defaultSelect;
				}

				return $this->getPresetDates($dates['date_preset']);
			}
		}

		/**
		 * Return a list of preset date ranges
		 *
		 * @return array
		 */
		public function getDateRangePresetList() {
//				'this_month' => 'This Month',
//				'next_month' => 'Next Month',
			$presetList = array(
				'all' => 'All Time',
				'up_to_today' => 'Today',
				'today' => 'Today',
                'yesterday' => 'Yesterday',
				'up_to_next_2_weeks' => 'Next 2 Weeks',
				'last_1_week' => 'Last 1 Week',
				'last_2_weeks' => 'Last 2 Weeks',
				'month_to_date' => 'Month to Date',
				'up_to_next_1_week' => 'Next 1 Week',
				'up_to_this_month' => 'This Month',
				'this_week' => 'This Week',
				'this_month' => 'This Month',
				'last_month' => 'Last Month',
				'last_30_days' => 'Last 30 Days',
                'last_3_months' => 'Last 3 Months',
                'last_6_months' => 'Last 6 Months',
                'last_9_months' => 'Last 9 Months',
				'last_12_months' => 'Last 12 Months',
				'this_year' => 'This Year',
				'custom' => 'Custom'
			);

			if($this->datePresetList) {
				$customPresetList = array();
				foreach($this->datePresetList as $preset) {
					$customPresetList[$preset] = $presetList[$preset];
				}
				$presetList = $customPresetList;
			} else {
				$presetList = array(
					'today' => 'Today',
                    'yesterday' => 'Yesterday',
					'last_1_week' => 'Last 1 Week',
					'last_2_weeks' => 'Last 2 Weeks',
					'month_to_date' => 'Month to Date',
					'this_month' => 'This Month',
					'last_month' => 'Last Month',
					'last_30_days' => 'Last 30 Days',
                    'last_3_months' => 'Last 3 Months',
                    'last_6_months' => 'Last 6 Months',
                    'last_9_months' => 'Last 9 Months',
					'last_12_months' => 'Last 12 Months',
					'this_year' => 'This Year',
					'custom' => 'Custom'
				);
			}

			return $presetList;
		}

		public function getPresetDates($preset, $format = 'Y-m-d') {
			switch ($preset) {
				case 'all';
					$dateRange['from_date'] = '0000-00-00';
					break;

				case 'up_to_today';
					$dateRange['to_date'] = date($format);
					break;

				case 'today':
					$dateRange['from_date'] = date($format);
					$dateRange['to_date'] = date($format);
					break;

                case 'yesterday':
                    $dateRange['from_date'] = date($format, strtotime("-1 days"));
                    $dateRange['to_date'] = date($format, strtotime("-1 days"));
                    break;

				case 'up_to_next_1_week':
					$dateRange['to_date'] = date("Y-m-d", strtotime("+1 week"));
                    $dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
					break;

				case 'up_to_next_2_weeks':
					$dateRange['to_date'] = date("Y-m-d", strtotime("+2 weeks"));
                    $dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
					break;

                case 'up_to_next_1_month':
                    $dateRange['to_date'] = date("Y-m-d", strtotime("+1 month"));
                    $dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
                    break;

                case 'up_to_next_2_months':
                    $dateRange['to_date'] = date("Y-m-d", strtotime("+2 month"));
                    $dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
                    break;

                case 'up_to_next_3_months':
                    $dateRange['to_date'] = date("Y-m-d", strtotime("+3 month"));
                    $dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
                    break;

                case 'last_1_week':
					$dateRange['from_date'] = date($format, strtotime("-7 days"));
					$dateRange['to_date'] = date($format);
					break;

				case 'last_2_weeks':
					$dateRange['from_date'] = date($format, strtotime("-14 days"));
					$dateRange['to_date'] = date($format);
					break;

                case 'this_week':
                    $dateRange['from_date'] = date($format, strtotime("last week"));
                    $dateRange['to_date'] = date($format, strtotime("this week"));
                    break;

				case 'month_to_date':
					$dateRange['from_date'] = date("Y-m-") . '1';
					$dateRange['from_date'] = date($format, strtotime($dateRange['from_date']));
					$dateRange['to_date'] = date("Y-m-d");
                    $dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
					break;

				case 'up_to_this_month':
					$dateRange['to_date'] = date("Y-m-t");
                    $dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
					break;

				case 'this_month':
					$dateRange['from_date'] = date("Y-m-") . '1';
					$dateRange['from_date'] = date($format, strtotime($dateRange['from_date']));
                    $dateRange['to_date'] = date("Y-m-t");
					$dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
					break;

				case 'last_month':
					$dateRange['from_date'] = date("Y-m-", strtotime("-1 month")) . '1';
					$dateRange['from_date'] = date($format, strtotime($dateRange['from_date']));
					$dateRange['to_date'] = date("Y-m-t", strtotime("last month"));
					$dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
					break;

                case 'last_3_months':
                    $dateRange['from_date'] = date($format, strtotime("-90 days"));
                    $dateRange['to_date'] = date($format);
                    break;

                case 'last_6_months':
                    $dateRange['from_date'] = date($format, strtotime("-180 days"));
                    $dateRange['to_date'] = date($format);
                    break;

                case 'last_9_months':
                    $dateRange['from_date'] = date($format, strtotime("-270 days"));
                    $dateRange['to_date'] = date($format);
                    break;

                case 'last_12_months':
					$dateRange['from_date'] = date($format, strtotime("-1 year"));
					$dateRange['to_date'] = date($format);
					break;

				case 'this_year':
					$dateRange['from_date'] = date("Y") . '-01-01';
					$dateRange['from_date'] = date($format, strtotime($dateRange['from_date']));
					$dateRange['to_date'] = date("Y") . '-12-31';
					$dateRange['to_date'] = date($format, strtotime($dateRange['to_date']));
					break;
				case 'last_30_days':
				default:
					$dateRange['from_date'] = date($format, strtotime("-30 days"));
					$dateRange['to_date'] = date($format);
					break;
			}
			return $dateRange;
		}
	}