<?php
Yii::import('admin_module.models.interfaces.Component');

class ActivityLogPortlet extends StmBasePortlet {

	public $containerId = 'activity-log-portlet';
	public $containerClass = 'p-0 g100 p-fr';
	public $handleTitle = 'Activity Log';
	public $handleIconCssClass = 'i_pencil';
	public $handleButtons = array();
	public $contentCssClass = 'portlet-content';

	public $parentModel;

	public function init() {
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_widgets.ActivityLogPortlet.assets.css'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
		Yii::app()->clientScript->registerCssFile($cssAssetsUrl . DS . 'activityLogPortlet.css');

		parent::init();
	}

	public function renderContent() {
		$model = new ActivityLog;

		if (!($this->parentModel instanceof Component)) {
			throw new Exception('ActivityLogPortlet CANNOT be used on a non Stm Component Model.');
		}
        $excludeTaskTypeIds = array();
        if($_GET['excludeDialerActivities'] == 1 || $_COOKIE['ActivityLogPortlet']['excludeDialerActivities'] == 1) {
            $excludeTaskTypeIds = array(TaskTypes::DIALER_NO_ANSWER, TaskTypes::DIALER_SKIP, TaskTypes::DIALER_LEFT_VOICEMAIL, TaskTypes::DIALER_DIAL, TaskTypes::DIALER_ANSWER);
        }
        setcookie("ActivityLogPortlet[excludeDialerActivities]", (($_GET['excludeDialerActivities'] == 1) ? $_GET['excludeDialerActivities'] : ""), time()+3600*24*30);

        $dataProvider = new CActiveDataProvider(ActivityLog::model()->byExcludeTaskTypes($excludeTaskTypeIds)->byComponentTypes($this->parentModel->getComponentChain()), array(
			'criteria' => array(
				'order' => 'activity_date desc',
			),
		    'pagination'=>array(
		        'pageSize'=>15,
		    ),
		));

		$id = ($id) ? $id : Yii::app()->user->id;
		$Contact = Contacts::model()->findByPk($id);

		$checkForSmsNumberHQ = false;
		if(Yii::app()->user->checkAccess('sms')) { //in_array(strtolower($_SERVER['SERVER_NAME']), array('www.christineleeteam.com','www.christineleeteam.local'))
            $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=> Yii::app()->user->accountId));

            $Criteria = new CDbCriteria;
            $Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id=:clientAccountId';
            $Criteria->params = array(
                ':clientAccountId'     => $clientAccount->id,
                ':contactId'    => $Contact->id,
                ':active'		=> 1
            );
            $checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);
		}
		$hasSmsProvidedNumber = false;

		if(!empty($checkForSmsNumberHQ)) {
			$hasSmsProvidedNumber =  true;
		}

		$this->render('activityLogPortlet', array(
			'hasSmsProvidedNumber' => $hasSmsProvidedNumber,
			'model' => $model,
			'parentModel' => $this->parentModel,
			'dataProvider' => $dataProvider,
		));
	}

    public static function renderContentOnly($parentModel) {
        $model = new ActivityLog;

        if (!($parentModel instanceof Component)) {
            throw new Exception('ActivityLogPortlet CANNOT be used on a non Stm Component Model.');
        }
        $excludeTaskTypeIds = array();

        if ((Yii::app()->request->isAjaxRequest && $_GET['excludeDialerActivities'] == 1) || (!Yii::app()->request->isAjaxRequest && $_COOKIE['ActivityLogPortlet']['excludeDialerActivities'] == 1)) {
            $excludeTaskTypeIds = array(TaskTypes::DIALER_NO_ANSWER, TaskTypes::DIALER_SKIP, TaskTypes::DIALER_LEFT_VOICEMAIL, TaskTypes::DIALER_DIAL, TaskTypes::DIALER_ANSWER);
            setcookie("ActivityLogPortlet[excludeDialerActivities]", 1, time()+3600*24*30);
        }

        $dataProvider = new CActiveDataProvider(ActivityLog::model()->byExcludeTaskTypes($excludeTaskTypeIds)->byComponentTypes($parentModel->getComponentChain()), array(
            'criteria' => array(
                'order' => 'activity_date desc',
            ),
            'pagination'=>array(
                'pageSize'=>15,
            ),
        ));

        $id = ($id) ? $id : Yii::app()->user->id;
        $Contact = Contacts::model()->findByPk($id);

        $checkForSmsNumberHQ = false;
        if(Yii::app()->user->checkAccess('sms')) {
            $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=> Yii::app()->user->accountId));

            $Criteria = new CDbCriteria;
            $Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id=:clientAccountId';
            $Criteria->params = array(
                ':clientAccountId'     => $clientAccount->id,
                ':contactId'    => $Contact->id,
                ':active'		=> 1
            );
            $checkForSmsNumberHQ = TelephonyPhones::model()->find($Criteria);
        }
        $hasSmsProvidedNumber = false;

        if(!empty($checkForSmsNumberHQ)) {
            $hasSmsProvidedNumber =  true;
        }

        $viewPathOriginal = Yii::app()->controller->module->getViewPath();
        Yii::app()->controller->module->setViewPath(Yii::getPathOfAlias('admin_widgets.ActivityLogPortlet.views.'));

        $content = Yii::app()->controller->renderFile(YiiBase::getPathOfAlias('admin_widgets.ActivityLogPortlet.views.activityLogPortlet').'.php', array(
                'hasSmsProvidedNumber' => $hasSmsProvidedNumber,
                'model' => $model,
                'parentModel' => $parentModel,
                'dataProvider' => $dataProvider,
            ), true);
        Yii::app()->controller->module->setViewPath($viewPathOriginal);

        return $content;
    }

	public static function formatActivityNote($id, $text, $opts) {
        $text = ($opts['nl2br']==true)? nl2br($text): $text;
        /** @var Remove any non-utf8 characters $text */
        $text = preg_replace('/[^(\x20-\x7F)]*/','', $text);
        $text = strip_tags($text, '<p><br>');
        /** @var Removing multiple <br>'s $text */
        $text = preg_replace("/(<br\s*\/?>\s*)+/mi", "<br/>", $text);
        /** @var Remove inline styles $text */
        $text = preg_replace('/(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)/mi', '\\1\\6', $text);
		$breakCount = substr_count($text,'<br');
		if($opts['maxHeight']) {
			// checks to see if the expandable div is necessary. Threshold is more than 200 characters or exceeding linebreak/maxheight as calculated below
			if(($breakCount > ($opts['maxHeight']/25)) || (strlen($text)>700)) {
				$text = '<div style="position:relative;">'.
					'<div class="expandableDiv min" id="expandableDiv'.$id.'" data="'.$id.'">'.$text.
					'</div>'.
					'<div class="p-mt10"><a href="javascript:void(0);" id="moreLink'.$id.'" class="moreLink" data="'.$id.'">[View More...+]</a></div>'.
					'</div>';
			}
		}
		return $text;
	}

    public static function printDescription($activityLog)
    {
        $description = '';
        $description .= ($activityLog->is_spoke_to)? "<div style=\"display:inline-block; float:left;\"><em class=\"icon_large i_stm_spoke_to_2\" style=\"margin-right: 0; height:36px;\"></em></div>":"";
        $description .= ActivityLogPortlet::formatActivityNote($activityLog->id, $activityLog->note, array("nl2br"=>true,"maxRows"=>4,"maxHeight"=>"100px;"));
        $description .= (($activityLog->url) ? " -  <a href=\"".$activityLog->url."\" target=\"_blank\">Click to View</a>" : "");
        $description .= ($activityLog->private_note)? "<br><br><strong><u>PRIVATE NOTE</u></strong>:<br>".nl2br($activityLog->private_note) : "";
        if($activityLog->call_id) {
            //$dialerListName = $activityLog->call->skipSoftDeleteCheck()->callListPhone->callList->name;
            $dialerListName = Yii::app()->db->createCommand()
                ->select('l.name')
                ->from('activity_log a')
                ->join('calls c','a.call_id=c.id')
                ->join('call_list_phones clp', 'c.call_list_phone_id=clp.id')
                ->join('call_lists l','clp.call_list_id=l.id')
                ->where('a.id='.$activityLog->id)
                ->queryScalar();

            $description .= ($dialerListName) ? ' - (Dialer: '. $dialerListName.')' : '';
        }
        return $description;
    }
}
