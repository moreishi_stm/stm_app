<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScript('activityLogScriptJs', <<<JS

    $('#activity-log-grid .activity-log-portlet-button-tip').tipsy({live: true, gravity: 's', title: 'data-tooltip'});
    $('#activity-log-grid .activity-log-portlet-button-tip-right').tipsy({live: true, gravity: 'e', title: 'data-tooltip'});

    $(".expandableDiv, .moreLink").live("click", function() {
        var id = $(this).attr("data");
        if($("#expandableDiv"+id).hasClass("min")) {
            $("#expandableDiv"+id).removeClass("min");
            $("#moreLink"+id).html($("#moreLink"+id).html().replace("More","Less"));
            $("#moreLink"+id).html($("#moreLink"+id).html().replace("+","-"));
        } else {
            $("#expandableDiv"+id).addClass("min");
            $("#moreLink"+id).html($("#moreLink"+id).html().replace("Less","More"));
            $("#moreLink"+id).html($("#moreLink"+id).html().replace("-","+"));
        }
    });

    $("#excludeDialerActivities").live("click", function() {

        $.fn.yiiGridView.update("activity-log-grid", { data: { "excludeDialerActivities" : $("#excludeDialerActivities").is(':checked') ? 1 : 0 }});
    });

JS
, CClientScript::POS_END);
?>
<style type="text/css">
	.activityDescription{max-height: 200px;}
	.expandableDiv.min{max-height: 100px; overflow-y: hidden;}
	.expandableDiv:hover{cursor: pointer;}
    .ui-tabs.ui-widget #activity-log-tab .button-container {
        width: 100% !important;
    }
    .button-container .typeFilter {
        padding: 7px;
        font-weight: bold;
        float: left;
    }
    .button-container .typeFilter input {
        margin-right: 6px;
    }
</style>

<div class="button-container">
    <div class="typeFilter" style="display: inline-block; float: left;"><input id="excludeDialerActivities" name="excludeDialerActivities" type="checkbox" value="1" <?=($_COOKIE['ActivityLogPortlet']['excludeDialerActivities']) ? " checked ": ""?>/>Hide Dialer Auto Logs</div>
	<button class="text add-activity-log-button" type="button" data="<?php echo $parentModel->id ?>" ctid="<?php echo $parentModel->componentType->id ?>"><em class="icon i_stm_add"></em>Add Note / Activity</button>

    <button class="text add-activity-log-button phone-call-button" type="button" data="<?php echo $parentModel->id ?>" ctid="<?php echo $parentModel->componentType->id ?>"><em class="icon i_stm_phone"></em>Add Phone Call</button>
    <?if($parentModel->componentType->id != ComponentTypes::COMPANIES):?>
    <button id="send-email-button" class="text" type="button"><em class="icon i_stm_mail"></em>Send Email</button>
    <?endif;?>
	<?php
	if($hasSmsProvidedNumber && Yii::app()->user->checkAccess('sms') && in_array($parentModel->componentType->id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS, ComponentTypes::CONTACTS, ComponentTypes::RECRUITS, ComponentTypes::CLOSINGS))):?>
	<button id="send-sms-button" class="text" type="button"><em class="icon i_stm_mail"></em>Send SMS</button>
	<?php endif;?>
</div>
<div class="scroll-content-container">
	<?php
		$this->widget('admin_module.components.StmGridView', array(
				'id' => 'activity-log-grid',
				'template' => '{pager}{items}{pager}',
				'enablePagination' => true,
				'dataProvider' => $dataProvider,
				'itemsCssClass' => 'datatables',
				'columns' => array(
					array(
						'type' => 'raw',
						'name' => 'Date / By / Activity',
						'value' => '"<div style=\"display:inline-block\">".Yii::app()->format->formatDate($data->activity_date, "n/j/y")." ".Yii::app()->format->formatTime($data->activity_date).
                      "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data->addedBy->fullName.
                      "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(".$data->taskType->name.")".
                      "<br>"."<input type=\"hidden\" class=\"log_id\" value=\"".$data->id."\"></div>"
            ',
						'htmlOptions' => array(
							'style' => 'width:150px;'
						),
					),
					array(
						'type' => 'raw',
						'name' => 'description',
						'value' => '(($data->is_spoke_to)? "<i class=\"action-button blue fa fa-comment fa-flip-horizontal activity-log-portlet-button-tip\" data-tooltip=\"Spoke to\"></i>":"").
						ActivityLogPortlet::formatActivityNote($data->id, $data->note, array("nl2br"=>true,"maxRows"=>4,"maxHeight"=>"100px;")).(($data->url) ? " -  <a href=\"".$data->url."\" target=\"_blank\">Click to View</a>" : "").(($data->is_public) ? "<br><em class=\"icon i_stm_success\"></em> Visible to Client" : "").(($data->asked_for_referral) ? "<div class=\"p-clr\"><em class=\"icon i_stm_success\"></em> Asked for Referral</div>" : "").(($data->private_note)? "<br><br><strong><u>PRIVATE NOTE</u></strong>:<br>".nl2br($data->private_note) : "")',
						'htmlOptions' => array(
							'class' => 'activityDescription'
						),
					),
					array(
					     'name'=>'',
					     'type'=>'raw',
					     'value' => '(($data->isShowActivityLogEditButton)? "<a class=\"action-button orange fa fa-pencil activity-log-portlet-button-tip-right edit-activity-log-button\" data-tooltip=\"Edit Activity Log\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\" ctido=\"".$data->originalTaskComponentTypeId."\"></a>" :"").((!empty($data->emailMessageLu))? "<a class=\"action-button blue fa fa-eye activity-log-portlet-button-tip-right view-activity-log-email-button\" data-tooltip=\"View E-mail\" data-id=\"".$data->id."\"></a>" :"")',
                         'htmlOptions' => array('style' => 'width:45px;'),
					),
				),
			)
		);
	?>
</div>
<?
$this->widget('admin_widgets.DialogWidget.EmailDialogWidget.EmailDialogWidget', array(
        'id' => 'email-dialog',
        'title' => 'Compose Email',
        'triggerElement' => '#send-email-button',
        'contactId' => (isset($parentModel->contact)) ? $parentModel->contact->id : $parentModel->id,
        'componentTypeId' => $parentModel->componentType->id,
    )
);

if($hasSmsProvidedNumber && Yii::app()->user->checkAccess('sms')
    && in_array($parentModel->componentType->id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS, ComponentTypes::CONTACTS, ComponentTypes::RECRUITS, ComponentTypes::CLOSINGS))) {
    $this->widget('admin_widgets.DialogWidget.SmsDialogWidget.SmsDialogWidget', array(
            'id' => 'sms-dialog',
            'title' => 'Compose SMS',
            'triggerElement' => '#send-sms-button',
            'contactId' => (isset($parentModel->contact)) ? $parentModel->contact->id : $parentModel->id,
            'componentTypeId' => $parentModel->componentType->id,
            'componentId' => $parentModel->id
        )
    );
}

$this->widget('admin_widgets.DialogWidget.ViewEmailDialogWidget.ViewEmailDialogWidget', array(
        'id' => 'view-email-dialog',
        'title' => 'Viewing Email',
        'triggerElement' => '.view-activity-log-email-button',
    )
);

Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
            'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
            'title' => 'Add New Activity Log',
            'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
            'parentModel' => $parentModel,
        )
    );
}