<?php

	/**
	 * DialogWidget
	 */
	abstract class DialogWidget extends CWidget {

		public $id; // Id of the dialog widget
		public $formId = null; // If null, automatically assign the formId as $this->id.'-form'
		public $title; // Title of the dialog box
		public $triggerElement; // jQuery selector for the element that will trigger the dialog
		public $theme=null;

		protected $validate;

		/**
		 * init
		 *
		 * @return null
		 */
		public function init() {
			if ($this->id && $this->triggerElement) {
				if ($this->formId === null) {
					$this->formId = $this->id . '-form';
				}

				$this->registerTriggerScript();
			}
		}

		/**
		 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
		 *
		 * @return null
		 */
		public function registerTriggerScript() {

			if($this->theme == "standard2") {
			$js = <<<JS
	    $("$this->triggerElement").live("click",function() {
	    	var Dialog = $("#$this->id");
			Dialog.dialog({
				"open": function(event, ui) {
					var vpHeight = parseInt($(window).height());
					if(vpHeight < 708) {
						$(Dialog).parent().css('top','115px');
					}

					var vpWidth = parseInt($(window).width());
					if(vpWidth < 768) {
						$(Dialog).parent().css('top','145px');
					}
				}
			});
			$(window).resize(function() {
				var vpHeight = parseInt($(window).height());
				if(vpHeight < 708) {
					$(Dialog).parent().css('top','115px');
				}

				var vpWidth = parseInt($(window).width());
				if(vpWidth < 768) {
					$(Dialog).parent().css('top','145px');
				}
			});
	        Dialog.dialog('open');

			var vpHeight = parseInt($(window).height());
			if(vpHeight < 708) {
				$(Dialog).parent().css('top','115px');
			}

			var vpWidth = parseInt($(window).width());
			if(vpWidth < 768) {
				$(Dialog).parent().css('top','145px');
			}

	        Dialog.find('div.errorMessage').html('').hide();

	        Dialog.find('input, select, textarea, span').removeClass('error');
	        Dialog.find('input').blur();

	        return false;
	    });
JS;
			} else {
				$js = <<<JS
	    $("$this->triggerElement").live(clickTouchEvent, function() {
	    	var Dialog = $("#$this->id");
	        Dialog.dialog('open');

	        Dialog.find('div.errorMessage').html('').hide();

	        Dialog.find('input, select, textarea, span').removeClass('error');
	        Dialog.find('input').blur();

	        return false;
	    });
JS;
			}
			Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
		}

		protected function processAjaxRequest(CActiveRecord $model) {
			if (isset($_POST['ajax']) && $_POST['ajax'] == $this->formId) {
				$this->validate = CActiveForm::validate($model);
			}
		}

		protected function getIsValidRequest() {
			return (count(CJSON::decode($this->validate)) == 0) ? true : false;
		}
	}
