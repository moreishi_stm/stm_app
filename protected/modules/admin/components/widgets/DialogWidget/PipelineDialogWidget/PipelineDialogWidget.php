<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class PipelineDialogWidget extends DialogWidget
{
    const PIPELINE_DIALOG_ID = 'pipeline-dialog';

    public $componentTypeId = NULL;
    public $updateContainer = NULL;

    /**
     * Response
     *
     * Response object for Plivo
     * @var \StmPlivoFunctions\Response
     */
    protected $_response;

    public function run()
    {
        // Can't run without a way to call the dialog
        if (!$this->triggerElement) {
            return false;
        }

        $this->render('pipelineDialog', array(
                'title' => $this->title,
                'triggerElement' => $this->triggerElement,
                'componentTypeId' => $this->componentTypeId,
                'updateContainer' => $this->updateContainer
            )
        );
    }

    public function registerTriggerScript()
    {

    }
}
