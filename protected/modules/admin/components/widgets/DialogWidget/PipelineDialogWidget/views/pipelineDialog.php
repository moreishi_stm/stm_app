<?php
$userId = Yii::app()->user->id;
$phoneTaskTypeId = TaskTypes::PHONE;

$stages = PipelineStages::model()->findAll();

$finalStages = array();
foreach($stages as $stage) {
    $attributes['id'] = $stage->id;
    $attributes['name'] = $stage->name;
    $finalStages[$stage->pipeline_id][$stage->id] = $attributes;
}

$js = "var pipeLineStages = jQuery.parseJSON('".json_encode($finalStages)."');";
$jsStart = "var moduleName = '".Yii::app()->controller->module->id."';\n"."var updateContainer = false;";
if(!empty($updateContainer)) {
    $jsStart .= "\n"."updateContainer = '".$updateContainer."';"."\n";
}
Yii::app()->clientScript->registerScript('pipelineAssignmentScript', $jsStart.
<<<JS
    $('.assign-pipeline-to-component').live('click', function(e) {

            // Stop default click event
            e.preventDefault();

            // Get component and component type IDs
            var componentTypeId = $(this).data('ctid');
            if(componentTypeId == undefined && $(this).attr('ctid') != undefined) {
                componentTypeId = $(this).attr('ctid');
            }
            if(componentTypeId == undefined && $(this).attr('data-id') != undefined) {
                componentTypeId = $(this).attr('data-id');
            }

            var componentId = $(this).data('cid');

            // Create modal
            stmModal.create({
                title: 'Pipeline Assignment',
                contentVariables: {
                	title: 'Pipeline Assignment'
                },
                content: $('#template-pipeline-assignment').html(),
                height: 370,
                successCallback: function(r) {
                    console.log(r);
                    $("div.loading-container.loading").remove();

                    if(updateContainer !== false && $(updateContainer).length > 0 && r.pipelines !== undefined) {

                        var html = '';

                        $.each(r.pipelines, function(pipelineId, pipelineData) {
                            html += '<tr>' +
                                '<td><a href="/'+moduleName+'/pipeline/view/'+pipelineId+'" target="_blank">'+pipelineData['pipeline']+'</a></td>' +
                                '<td><a href="/'+moduleName+'/pipeline/view/'+pipelineId+'" target="_blank">'+pipelineData['stage']+'</a></td>'+
                            '</tr>';
                        });

                        $(updateContainer).html(html);
                        $(updateContainer).find('td').effect({
                            'effect': 'highlight',
                            'duration': 2500,
                        });
                    }

                    Message.create('success', r.message);
                },
                beforeSubmit: function(data, form) {
                },
                contentVariables: {
                    componentId: componentId,
                    componentTypeId: componentTypeId
                }
            });

            var Dialog = $(".stm-modal form");

	        // clear out all dialog form fields
	        Dialog.find('div.errorMessage').html('').hide();
	        Dialog.find('input, select, textarea, span').removeClass('error');
	        Dialog.find('input').blur();
    });

    $("#Pipelines_id").live('change', function() {
        $("#pipeline_stages").html('');
        $("#pipeline-stage-select").removeClass('hidden');

        $.each(pipeLineStages[$(this).val()], function(item_id, item) {
            $("#pipeline_stages").append('<option value="'+item_id+'">'+item.name+'</option>');
        });

        $("#pipeline_stages").show();
    });
JS
.$js
);

?>
<script id="template-pipeline-assignment" type="text/x-handlebars-template">
<?php
    $form = $this->beginWidget('StmActiveForm', array(
        'id' => 'pipeline-assignment-form',
        'enableAjaxValidation' => false,
        'action' => array('/'.Yii::app()->controller->module->id.'/pipeline/ajaxAssign'),
        'htmlOptions'   =>  array(
            'class' =>  'form-horizontal'
        ),
        'clientOptions' => array(
            'validateOnChange' => false,
            'validateOnSubmit' => false,
        )
    ));
?>

<div class="stm-modal-body">
    <div class="form-group">
        <label class="control-label col-sm-2">Pipeline:</label>
        <div class="col-sm-10">
            <?php
            echo $form->dropDownList(new Pipelines(), 'id', CHtml::listData(Pipelines::model()->findAllByAttributes(array('component_type_id' => $componentTypeId)), 'id', 'name'), array(
                'empty'=>'',
                'placeholder' => 'Search Pipeline Name',
                'class' => 'chzn-select form-control'
            ));
            ?>
        </div>
    </div>
    <div class="form-group hidden" id="pipeline-stage-select">
        <label class="control-label col-sm-2">Stage:</label>
        <div class="col-sm-10">
           <select name="pipeline_stages" id="pipeline_stages" class="form-control"></select>
        </div>
    </div>
</div>
<div class="stm-modal-footer">
    <input type="hidden" name="componentTypeId" value="{{componentTypeId}}">
    <input type="hidden" name="componentId" value="{{componentId}}">
    <button type='submit' data-ctid="{{componentTypeId}}" data-cid="{{componentId}}" class="">Assign</button>
</div>

<div class="loading-container"><em></em></div>
<?php $this->endWidget(); ?>
</script>