<?
Yii::app()->clientScript->registerScript('documentsDialogScript2', <<<JS

    $('.click-to-call-button.phone-action-button').click(function(){
        $('#$dialogId').dialog('close');
    });

	// Add button
    $('#add-document-button').live('click', function(e) {

            e.preventDefault();

            // Create modal
            stmModal.create({
                title: 'Add Document',
                content: $('#template-dialog-form-documents').html(),
                width: 650,
                successCallback: function() {
                    window.location.reload();
                },
                contentVariables: {
					buttonText: 'Add Document',
					url: '/admin/documents/add'
				},
				beforeSubmit: function(data, form) {

					// Make sure file choice is not empty
					if($('.stm-modal input[type="file"]').val() == '') {
						alert('Please choose a file to continue');
						return false;
					}
				}
            });

            //initDocumentsDialog();
    });

	// Edit button
    $('.edit-document-button').live('click', function(e) {

            e.preventDefault();

            // Create modal
            stmModal.create({
                title: 'Edit Document',
                content: $('#template-dialog-form-documents').html(),
                width: 650,
                successCallback: function() {
                    window.location.reload();
                },
                contentVariables: {
					buttonText: 'Edit Document',
					url: '/admin/documents/edit/' + $(this).data('id')
				}
            });

            //initDocumentsDialog();
    });

JS
);

//	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
//			'id' => $this->id,
//			'options' => array(
//				'title' => $title,
//				'autoOpen' => false,
//				'modal' => true,
//				'width' => 650,
//			),
//		)
//	);

echo '<script id="template-dialog-form-documents" type="text/x-handlebars-template">';

	$form = $this->beginWidget('CActiveForm', array(
			'id' => $this->formId,
//			'action' => Yii::app()->createUrl("/".Yii::app()->controller->module->id."/documents/add", array()),
			'action' => '{{url}}',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'beforeValidate' => 'js:function(form) {
	            $(".loading-container").addClass("loading");
				return true;
            }',
				'afterValidate' => 'js:function(form, data, hasError) {
				if(hasError){
			    	$(".loading-container").removeClass("loading");
			    }
			    return true;
			}',
			),
			'htmlOptions' => array(
				'enctype'	=>	'multipart/form-data',
				'class'		=>	'form-horizontal'
			),
		)
	);
?>
<div class="stm-modal-body">
    <div class="form-group">
        <?php echo $form->labelEx($model, 'document_type_id', array(
            'class'	=>	'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'document_type_id', CHtml::listData(DocumentTypes::model()->byComponentType($component_type_id)->orderByName()->findAll(), 'id', 'name'), $htmlOptions = array(
                'class' => 'documentTypes chzn-select',
                'data-placeholder' => 'Type Document Type Here...',
            ));
            ?>
            <?php echo $form->error($model, 'document_type_id'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'file_name', array(
            'class'	=>	'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <div id="fileReplace-container"><?php echo $form->checkBox($model, 'fileReplaceFlag', false, array('style' => 'margin-bottom:10px;font-size:16px;')) ?> Replace File</div>
            <div id="fileField-container">
                <? echo $form->fileField($model, 'file_name', array(
    //				'class' => 'form-control',
                )); ?>
            </div>
            <?php echo $form->error($model, 'file_name'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'visibility_ma', array(
            'class'	=>	'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'visibility_ma', array(
                Documents::VISIBILITY_COMPONENT => 'Don\'t Share with Client',
                Documents::VISIBILITY_PUBLIC => 'Share with Client'
            ), array(
                    'class'	=>	'form-control'
                )
            );?>
            <?php echo $form->error($model, 'visibility_ma'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'description', array(
            'class'	=>	'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textField($model, 'description', array(
                'placeholder' => 'Description',
                'class' => 'form-control',
            )); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
    </div>

    <?php echo $form->hiddenField($model, 'componentTypeId'); ?>
    <?php echo $form->hiddenField($model, 'componentId'); ?>
    <?php echo $form->hiddenField($model, 'id'); ?>
</div>
<div class="stm-modal-footer">
    <button type='submit'>{{buttonText}}</button>
</div>

<!--<div class="submit-button-row p-tc p-p10">-->
<!--	<input id="document-dialog-submit-button" class="button wide" type="submit" value="Add Document">-->
<!--</div>-->

<div class="loading-container"><em></em></div>
<? $this->endWidget();?>
<?='</script>'?>