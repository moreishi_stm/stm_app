<?
// NOTE: for documents list page, the listboxSearch had fields with documents model already,
// so the formID was added to the selector to ID properly

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
 'id'      => $this->id,
 'options' => array(
     'title'    => $title,
     'autoOpen' => false,
     'modal'    => true,
     'width'    => 500,
 ),
));

$form = $this->beginWidget(
    'CActiveForm',
    array(
		'id' => $this->formId,
		'action' => Yii::app()->createUrl("/".Yii::app()->controller->module->id."/documents/add", array()),
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
			'beforeValidate' => 'js:function(form) {
					$(".loading-container").addClass("loading");
					return true;
				}',
			'afterValidate' => 'js:function(form, data, hasError) {
					if(hasError) {
//						Message.create("error","Document add had error.");
						$(".loading-container").removeClass("loading");
						return false;
					} else {
//						Message.create("success","Document added successfully.");
//						$.fn.yiiGridView.update("documents-grid", { data: $(this).serialize() });
						return true;
					}
				}',
		),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )
);
?>
	<fieldset>
		<section>
			<?php echo $form->labelEx($model, 'document_type_id'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'document_type_id', CHtml::listData(DocumentTypes::model()->byComponentType($component_type_id)->orderByName()->findAll(), 'id', 'name'), $htmlOptions = array(
						'class' => 'documentTypes chzn-select',
						'data-placeholder' => 'Type Document Type Here...',
					)
				);

				$this->widget('admin_module.extensions.EChosen.EChosen', array(
																		 'target' => 'select.documentTypes',
																		 'options' => array(
																			 'allow_single_deselect' => true,
																			 'width' => '83%'
																		 ),
																		 )
				);
				?>

			</div>
			<?php echo $form->error($model, 'document_type_id'); ?>
		</section>
		<section>
		   <?php echo $form->labelEx($model,'file_name');?>
		    <div>
                <div id="fileReplace-container"><?php echo $form->checkBox($model, 'fileReplaceFlag', false, $htmlOptions = array('style' => 'margin-bottom:10px;font-size:16px;')) ?> Replace File</div>
                <div id="fileField-container">
                    <? echo $form->fileField($model, 'file_name', $htmlOptions = array(
                            'class' => 'g10',
                        )
                    ); ?>
                </div>
            </div>
            <?php echo $form->error($model, 'file_name'); ?>
        </section>
<!--		<section>-->
<!--			--><?php //echo $form->labelEx($model, 'visibility_ma'); ?>
<!--			<div>-->
<!--				--><?php //echo $form->dropDownList($model, 'visibility_ma', array(
//						Documents::VISIBILITY_COMPONENT => 'Don\'t Share with Client',
//						Documents::VISIBILITY_PUBLIC => 'Share with Client'
//						), $htmlOptions = array()
//				);?>
<!--			</div>-->
<!--			--><?php //echo $form->error($model, 'visibility_ma'); ?>
<!--		</section>-->
        <section>
           <?php echo $form->labelEx($model,'description'); ?>
        	<div>
				<?php echo $form->textArea($model,'description',$htmlOptions=array(
					'placeholder'=>'Description',
					'class'=>'g11',
				)); ?>
        	</div>
        	<?php echo $form->error($model, 'notes'); ?>
        </section>
		<?php echo $form->hiddenField($model, 'visibility_ma', $htmlOptions=array('value'=>Documents::VISIBILITY_TEAM)); ?>
		<?php echo $form->hiddenField($model, 'componentTypeId'); ?>
		<?php echo $form->hiddenField($model, 'componentId', $htmlOptions=array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->hiddenField($model, 'id'); ?>
		<input type="hidden" name="returnURL" value="<? echo "/",Yii::app()->request->getPathInfo(); ?>" />
	</fieldset>
	<div class="submit-button-row p-tc p-p10">
		<input id="document-dialog-submit-button" class="button wide" type="submit" value="Add Document">
	</div>
	<div class="loading-container"><em></em></div>
<?
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');

?>
