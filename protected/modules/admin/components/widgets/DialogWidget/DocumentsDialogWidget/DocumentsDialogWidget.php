<?
Yii::import('admin_widgets.DialogWidget.DialogWidget');
class DocumentsDialogWidget extends DialogWidget {
	public $view = 'documentsDialog';

	public $parentModel;
	public $componentType;

	public function run() {
		// Can't run without a way to call the dialog
        if (!$this->triggerElement) {
            return false;
        }

		$model                    = new Documents('create');
		$model->componentTypeId  = $this->componentType->id;
		$model->componentId      = $this->parentModel->id;

		if (isset($_POST['Documents'])) {
			$model->attributes = $_POST['Documents'];
			$model->setScenario('create-processing');

			// Process the request if it's submitted via ajax
			$this->processAjaxRequest($model);
		}

		// Set the return url, used by the upload action of the documents controller
		Yii::app()->user->setState('documentReferrer',Yii::app()->request->requestUri.'#activityDetails-docs');
		Yii::app()->user->returnUrl = Yii::app()->request->requestUri;

		$this->render($this->view, array(
						 'model'             => $model,
						 'component_type_id' => $this->componentType->id,
						 'component_id'      => $this->parentModel->id,
						 'title'             => $this->title,
						 'triggerElement'    => $this->triggerElement,
					   ));
	}

	/**
	 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
	 * @return null
	 */
	public function registerTriggerScript() {

        // stm-modal adjustment
        $this->id = '.stm-modal';

		$visibilityComponent = Documents::VISIBILITY_COMPONENT;
        $moduleName = Yii::app()->controller->module->id;
		$js = <<<JS
		//var initDocumentsDialog = function () {

			$('#Documents_fileReplaceFlag').live('click', function() {
				if ($(this).is(':checked')) {
					$('#Documents_fileReplaceFlag').val(1);
					$('#$this->id #fileField-container').show('normal');
					$('#$this->id input#Documents_file_name').removeAttr('disabled');
					return;
				} else {
					$('#Documents_fileReplaceFlag').val(0);
					$('#$this->id #fileField-container').hide('normal');
					$('#$this->id input#Documents_file_name').attr('disabled','disabled');
					return;
				}
			});

			$('$this->triggerElement').live('click', function() {
				var Dialog = $('.stm-modal');
				var element = $(this);
				var documentId = element.data('id');
//				var DialogForm = Dialog.find('form#$this->formId');
				var DialogForm = Dialog.find('form');

				var DialogFormAction = DialogForm.attr('action');

				Dialog.find(':input:not(input[type=hidden],input[type=submit]),#Documents_id').val('');  //clear all input execpt the hidden ones
				Dialog.find('select').val([]);
				Dialog.find('select#Documents_visibility_ma').val($visibilityComponent);
				Dialog.find('div').removeClass('error');
				Dialog.find('.errorMessage').html('');

				Dialog.find('.chzn-select').trigger("liszt:updated");
				Dialog.find('div.errorMessage').html('').hide();

				// Reset the action on each triggering attempt for Add New Search
				DialogForm.attr('action', '/$moduleName/documents/add');
				//DialogForm.data('settings').validationUrl = DialogForm.attr('action');

				// If edit was clicked then we need to dynamically fill in the values for the popup
				if (element.hasClass('edit-document-button')) {

					// Create the scenario specific text for Edit Saved Search
					$('span.ui-button-text:contains("Add Document")').html('Update Document');
					$('span.ui-dialog-title:contains("Add New Document")').html('Edit Document');
					$('#fileField-container').hide();
					$('#fileReplace-container').show();
					$('input#Documents_file_name').attr('disabled','disabled');

console.log(Dialog.find('form'));

					DialogForm.attr('action', '/$moduleName/documents/edit/' + documentId);
					//DialogForm.data('settings').validationUrl = DialogForm.attr('action');

					$.getJSON('/$moduleName/documents/' + documentId, function(data) {
						$('#Documents_id').val(data.id);
						$('#Documents_description').val(data.description);
						$('.stm-modal select[name*="document_type_id"]>option[value="'+data.document_type_id+'"]').attr('selected', true);
						$('.stm-modal select[name*="visibility_ma"]>option[value="'+data.visibility_ma+'"]').attr('selected', true);
						Dialog.find('.chzn-select').trigger("liszt:updated");
					});
				} else {

					// Revert the scenario specific text for Add New Search
					$('span.ui-button-text:contains("Update Document")').html('Add Document');
					$('span.ui-dialog-title:contains("Edit Document")').html('Add New Document');
					$('#fileField-container').show();
					$('#fileReplace-container').hide();
					$('input#Documents_file_name').removeAttr('disabled');
				}

//				Dialog.dialog('open');

				return false;
			});
//		};
//			});
//		};

JS;

		Yii::app()->clientScript->registerScript('popupTrigger-'.$this->id, $js);
	}
}
