<?php
	Yii::import('admin_widgets.DialogWidget.DialogWidget');

	class TaskDialogWidget extends DialogWidget {

		const TASK_DIALOG_ID = 'task-dialog';
		public $parentModel;

		public function init() {
			parent::init();

			if (!$this->parentModel instanceof CActiveRecord) {
				throw new CHttpException(500, 'Could not render the task dialog.');
			}
		}

		public function run() {

			$this->render('taskDialog', array(
					'model'				=> new Tasks(),
					'taskRecursions'	=> new TaskRecursions(),
					'parentModel' => $this->parentModel,
				)
			);
		}

		/**
		 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
		 *
		 * @return null
		 */
		public function registerTriggerScript() {
			$varName = 'taskList';
            $phoneTaskTypeId = TaskTypes::PHONE;
			$taskTypeDropDownLists = array('taskTypeDropDownLists' => TaskTypeComponentLu::getDropDownLists());
			$taskTypeDropDownLists = CJSON::encode($taskTypeDropDownLists);
			$module = Yii::app()->controller->module->name;
			$currentUserId = Yii::app()->user->id;
            $dialogId = $this->id;

			$js = <<<JS
		var initTaskDialog = function (ele) {
			var STM = {
				dialog : {
					State : function (formSubmitAction, buttonText, dialogTitle) {
						this.formSubmitAction = formSubmitAction;
						this.buttonText = buttonText;
						this.dialogTitle = dialogTitle;
					}
				}
			};

			var Edit = new STM.dialog.State('edit', 'Save Task', 'Edit Task');
			var Delete = new STM.dialog.State('delete', 'Delete Task', 'Delete Task');
			var Add = new STM.dialog.State('add', 'Create Task', 'Create New Task');

			//By default we assume the current state is "Add" as that's the default behavior of the dialog.
			var currentState = Add, previousState;

			//$('$this->triggerElement').live('click', function() {
			(function(ele) {

				var Dialog = $('.stm-modal');
				var componentTypeId = $(ele).attr('ctid');
				var taskTypeDropDownLists = (jQuery.parseJSON('$taskTypeDropDownLists')).taskTypeDropDownLists;

				$('select#Tasks_task_type_id').html(taskTypeDropDownLists[componentTypeId]);

				Dialog.find(':input:not([type=hidden], [name*=google]), #Tasks_google_calendar_event_id').val('');   //clear all input except the hidden and google calendar ones
                $('#sync-google-container, #no-calendar-permission-container').addClass('hidden');
                $('#Tasks_googleCalendarData_add_event').attr('checked', false).removeClass('hidden');

				Dialog.find('div.errorMessage').html('').hide();
				Dialog.find('div.error').removeClass('error');
                $('#recur-type-wrapper').addClass('hidden');
                $('#recur-date-wrapper').addClass('hidden');
                $('#isRecurring').attr('checked', false);
                //$('#recurStartDateType_0').click();
                $('#recurEndDateType_0').click();

				// Hide the delete type by default, we only show this if our task is recurring
				$('#delete-type-wrapper').hide();

				var DialogForm = Dialog.find('#$this->formId');

				// Reset the action on each triggering attempt
				DialogForm.attr('action', '/$module/tasks/' + currentState.formSubmitAction);
				//DialogForm.data('settings').validationUrl = DialogForm.attr('action');

				var DialogFormAction = DialogForm.attr('action');

				// If edit was clicked then we need to dynamically fill in the values for the popup
				var element = $(ele);

				var isEditRequest = element.hasClass('edit-task-button'),
					isDeleteRequest = element.hasClass('delete-task-button');

				previousState = currentState;

				// Bind additional functionality for recursion, but only bind once
				if(typeof recurBound == 'undefined') {

					// Change event for recur type
					//$('body').delegate('#recur-type-wrapper select', 'change', function() {
					$('#recur-type-wrapper select').live('change', function() {
					console.log('CHANGE!');

						// Hide recursion fields if we are one time only, otherwise, show them.
						if($(this).val() == 'One time only' || $(this).val() == '') {
							$('#recur-date-wrapper').hide('normal');
						}
						else {
							$('#recur-date-wrapper').show('normal');
						}

					});

					// Flag this as complete
					recurBound = true;
				}

				if (isEditRequest || isDeleteRequest) {
				    currentState = (isEditRequest) ? Edit : Delete;

					var taskId = element.attr('data');

					$('span.ui-button-text:contains("' + previousState.buttonText + '")').html(currentState.buttonText);
					$('span.ui-dialog-title:contains("' + previousState.dialogTitle + '")').html(currentState.dialogTitle);

					var dialogFormAction = DialogFormAction.replace(previousState.formSubmitAction,
						currentState.formSubmitAction) + '/' + taskId;
					DialogForm.attr('action', dialogFormAction);
					//DialogForm.data('settings').validationUrl = dialogFormAction;

					$.getJSON('/$module/tasks/' + taskId, function(data) {

                        if(data.complete_date == true) {

                            //refresh grid + send message
                            Message.create("error","This Task has already been completed. Please refresh your Task list");
                            $("#$dialogId").dialog("close");
                            return;
                        }

						// Hide recursion delete if we don't have recursion
						if (isDeleteRequest && data.recur_type != 'One time only' && data.recur_type) {
							$('#delete-type-wrapper').show();
						}

						// Hide recursion fields if we are one time only, otherwise, show them.
						if(data.recur_type == 'One time only' || data.recur_type == '' || !data.recur_type) {
							$('#recur-date-wrapper').hide('normal');
                            $('#recur-type-wrapper').addClass('hidden');
                            $('#isRecurring').attr('checked', false);
						}
						else {
							$('#recur-date-wrapper').show('normal');
                            $('#recur-type-wrapper').removeClass('hidden');
                            $('#isRecurring').attr('checked', true);
						}

						// Recursion related input
						$('select[name*="recur_type"]>option[value="' + data.recur_type + '"]').attr('selected', true);
						$('input[name*="recur_repeat_every"]').val(data.recur_repeat_every);
						$('input[name*="recur_end_after_occurrences"]').val(data.recur_end_after_occurrences);
						$('input[name*="recur_start_date"]').val(data.recur_start_date);
						$('input[name*="recur_end_date"]').val(data.recur_end_date);

                        if(data.recur_start_date != '' && typeof data.recur_start_date != 'undefined') {
                            $('#recurStartDateType_1').click();
                        }
                        if(data.recur_end_date != '' && typeof data.recur_end_date != 'undefined') {
                            $('#recurEndDateType_1').click();
                        }

						// All other input
						$('input[name*="due_date"]').val(data.due_date);
						$('textarea[name*="description"]').val(data.description);
						$('select[name*="task_type_id"]>option[value="' + data.task_type_id + '"]').attr('selected', true);
						$('select[name*="assigned_to_id"]>option[value="' + data.assigned_to_id + '"]').attr('selected', true);
						$('select[name*="operation_manual_id"]>option[value="' + data.operation_manual_id + '"]').attr('selected', true);
						$('select[name*="is_priority"]>option[value="' + data.is_priority + '"]').attr('selected', true);
						$('h2.contact-name').html(data.fullName);
						$('input[name*="component_id"]').val(data.component_id);
						$('input[name*="component_type_id"]').val(data.component_type_id);

						if(data.google_calendar_event_id && data.has_calendar_permission == true) {
                            $('#sync-google-container').removeClass('hidden');
                            $('#no-calendar-permission-container').addClass('hidden');
                            $('#Tasks_googleCalendarData_add_event').attr('checked', true);
						}
						else if(data.google_calendar_event_id && data.has_calendar_permission == false) {
                            $('#sync-google-container, #Tasks_googleCalendarData_add_event').addClass('hidden');
                            $('#no-calendar-permission-container').removeClass('hidden');
						}

						$('input[name*="google_calendar_id"]').val(data.google_calendar_id);
						$('input[name*="google_calendar_event_id"]').val(data.google_calendar_event_id);
					});
				} else {
					currentState = Add;

					//Let's just ensure we have the appropriate action for this state.
					DialogForm.attr('action', '/$module/tasks/' + currentState.formSubmitAction);
					//DialogForm.data('settings').validationUrl = DialogForm.attr('action');

                    $('#Tasks_googleCalendarData_add_event').attr('checked', false);
                    $('#sync-google-container').addClass('hidden');

					$('span.ui-button-text:contains("' + previousState.buttonText + '")').html(currentState.buttonText);
					$('span.ui-dialog-title:contains("' + previousState.dialogTitle + '")').html(currentState.dialogTitle);
					$('select[name*="assigned_to_id"]>option[value="' + $currentUserId + '"]').attr('selected', true);

					$('input[name*="component_id"]').val($(ele).attr('data'));
					$('input[name*="component_type_id"]').val($(ele).attr('ctid'));

                    // Pre-populate the date if we have a day interval
                    if(ele.hasAttribute('data-days')) {

                        // Calculate date
                        var date = new Date();
                        date.setDate(date.getDate() + parseInt(ele.getAttribute('data-days')));

                        // Pre-populate the date
                        $('#Tasks_due_date').val((date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear());
                    }

				}

                if ($(ele).hasClass('phone-call-button')) {
                    $('#Tasks_task_type_id').val($phoneTaskTypeId);
//                    $("select#Tasks_task_type_id").trigger("liszt:updated");
                }

				//Dialog.dialog('open');

				return false;
			})(ele);

//			$('#recurStartDateType_0').live('click', function() {
//			    $('#recur-start-date-wrapper').hide('normal');
//			    $('#TaskRecursions_recur_start_date').val('');
//			});

//			$('#recurStartDateType_1').live('click', function() {
//			    $('#recur-start-date-wrapper').show('normal');
//			});

			$('#recurEndDateType_0').live('click', function() {
			    $('#recur-end-date-wrapper').hide('normal');
			    $('#TaskRecursions_recur_end_date').val('');
			});

			$('#recurEndDateType_1').live('click', function() {
			    $('#recur-end-date-wrapper').show('normal');
			});

            // need to figure out a way where it's not changing on edit load data
//			$('#TaskRecursions_recur_start_date').change(function() {
//			    $('#Tasks_due_date').val($('#TaskRecursions_recur_start_date').val());
//			});

//			var recurStartDateType = $('input[name=recurStartDateType]');
//			recurStartDateType.click(function() {
//				if ($(this).is(':checked'))
//					return $(this).val(1);
//
//				return $(this).val(0);
//			});

		};

		$('#task-dialog-quick-dates-container .quick-date').live('click', function(){
            var daysToAdd = $(this).attr('data-days');
            var dueDate = new Date();
            dueDate.setDate(dueDate.getDate() + parseInt(daysToAdd));
            var dd = dueDate.getDate();
            var mm = dueDate.getMonth() + 1;
            var y = dueDate.getFullYear();

            $('#Tasks_due_date').val(mm + '/' + dd + '/' + '' + y);
		});

		$('#task-dialog-quick-task-types-container .quick-task-type').live('click', function(){

            $('#Tasks_task_type_id').val($(this).attr('data-id'));
		});

		$('#isRecurring').live('click', function(){
            if ($(this).is(':checked')) {
                $('#recur-type-wrapper').removeClass('hidden');
                $('#recur-date-wrapper').removeClass('hidden');
            }
            else {
                if($('#TaskRecursions_recur_type').val() != '') {
                    if(!confirm('Are you sure you want to remove the Recurring event?')) {
                        return false;
                    }
                }
                $('#recur-type-wrapper').addClass('hidden');
                $('#recur-date-wrapper').addClass('hidden');
                $('#TaskRecursions_recur_type').val(null);
                $('#TaskRecursions_recur_end_date').val(null);
                $('#TaskRecursions_recur_start_date').val(null);
            }

		});

		$('#Tasks_googleCalendarData_add_event').live('click', function(){
            if ($(this).is(':checked')) {
                $('#sync-google-container').removeClass('hidden');
            }
            else {
                $('#sync-google-container').addClass('hidden');
            }
		});
JS;
			Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
		}
	}