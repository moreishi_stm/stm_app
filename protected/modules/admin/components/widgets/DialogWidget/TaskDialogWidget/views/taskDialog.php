<?
$css = <<<CSS
#recur-date-wrapper label{
    padding-left: 0;
    font-size: 12px;
}
#recur-date-wrapper input{
    margin-left: 8px;
}
#recur-date-wrapper label[for=recurEndDateType_0] {
    margin-right: 34px;
}
#task-dialog-quick-dates-container {
    padding-bottom: 4px;
}
#task-dialog-quick-dates-container a {
    padding-right: 14px;
}
#task-dialog-quick-task-types-container a {
    padding-right: 14px;
}
CSS;
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->cdnAssetUrl . '/v1/js/source/jui/css/base/jquery-ui.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->cdnAssetUrl . '/js/jquery/jquery-ui-timepicker-addon-1.6.3/jquery-ui-timepicker-addon.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->cdnAssetUrl . '/js/jquery/jquery-ui-timepicker-addon-1.6.3/jquery-ui-timepicker-addon.css');

Yii::app()->clientScript->registerCss('taskDialogCss', $css);
Yii::app()->clientScript->registerScript('taskDialogScriptJs', <<<JS

    // Add button
    $('.add-task-button, #add-task-button').live('click', function(e) {

        // Stop default behavior
        e.preventDefault();

		// Create modal
		stmModal.create({
			title: 'Create New Task',
			content: $('#template-dialog-form-task').html(),
			height: 370,
			width: 750,
			modalId: 'task-dialog',
			successCallback: function() {

                if (document.getElementById("task-grid")) {
                    $.fn.yiiGridView.update("task-grid");
                }
			},
			contentVariables: {
				buttonText: 'Create a Task'
			}
		});

        // Instantiate date picker
        $('#Tasks_due_date').datetimepicker({timeFormat: "hh:mm tt", controlType: 'select', oneLine: true});
        $('#TaskRecursions_recur_start_date').datetimepicker({timeFormat: "hh:mm tt", controlType: 'select', oneLine: true});
        $('#TaskRecursions_recur_end_date').datetimepicker({timeFormat: "hh:mm tt", controlType: 'select', oneLine: true});
		initTaskDialog(this);
    });

	// Edit button
    $('.edit-task-button, .delete-task-button').live('click', function(e) {

        // Stop default behavior
        e.preventDefault();

		// Set window title
        var title = $(this).hasClass('delete-task-button') ? 'Delete Task' : 'Edit Task';

		// Create modal
		stmModal.create({
			title: title,
			content: $('#template-dialog-form-task').html(),
			height: 370,
			width: 750,
			modalId: 'task-dialog',
			successCallback: function() {
                if (document.getElementById("task-grid")) {
                    $.fn.yiiGridView.update("task-grid");
                }
			},
			contentVariables: {
				buttonText: title
			}
		});


        // Instantiate date picker
        $('#Tasks_due_date').datetimepicker({timeFormat: "hh:mm tt", controlType: 'select', oneLine: true});
        $('#TaskRecursions_recur_start_date').datetimepicker({timeFormat: "hh:mm tt", controlType: 'select', oneLine: true});
        $('#TaskRecursions_recur_end_date').datetimepicker({timeFormat: "hh:mm tt", controlType: 'select', oneLine: true});

		initTaskDialog(this);
    });
JS
);

//	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
//			'id' => $this->id,
//			'options' => array(
//				'title' => $this->title,
//				'autoOpen' => false,
//				'modal' => true,
//				'width' => 700,
//				'buttons' => array(
//					'Create Task' => 'js:function() { $("#' . $this->formId . '").submit(); }',
//				),
//			),
//		)
//	);

	echo '<script id="template-dialog-form-task" type="text/x-handlebars-template">';
	$form = $this->beginWidget('CActiveForm', array(
			'id' => $this->formId,
			'action' => array('/'.Yii::app()->controller->module->name.'/tasks/add'),
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'htmlOptions'   =>  array(
				'class' =>  'form-horizontal'
			),
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'beforeValidate' => 'js:function(form) {
					$(".loading-container").addClass("loading");
						return true;
					}',
				'afterValidate' => 'js:function(form, data, hasError) {
					$(".loading-container").removeClass("loading");

					if (!hasError) { // Action was successful
						$("#' . $this->id . '").dialog("close");

                        if(data.status == "success") {
						    Message.create("success", "Successful Task Update!");
                        } else {
                            Message.create("error", "Error: Task did not update.");
                        }

						if(document.getElementById("task-grid")) {
    						$.fn.yiiGridView.update("task-grid");
						}

						return false;
					}
		        }',
			),
		)
	);

?>
<div class="stm-modal-body">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'task_type_id', array(
			'class' =>  'col-sm-2 control-label'
		)); ?>
		<div class="col-sm-4">
            <div id="task-dialog-quick-task-types-container">
                <a href="javascript:void(0)" class="quick-task-type" data-id="1">To Do</a>
                <a href="javascript:void(0)" class="quick-task-type" data-id="2">Phone</a>
                <a href="javascript:void(0)" class="quick-task-type" data-id="3">Email</a>
            </div>
			<?php echo $form->dropDownList($model, 'task_type_id', CHtml::listData(TaskTypeComponentLu::getTaskTypes($parentModel->componentType->id), 'taskType.id', 'taskType.name'), $htmlOptions = array(
				'empty' => 'Select One',
				'class'	=>	'form-control'
			)
			); ?>
			<?php echo $form->error($model, 'task_type_id'); ?>
		</div>
		<?php echo $form->labelEx($model, 'is_priority', $htmlOptions = array(
			'class' => 'col-sm-2 control-label',
		)
		); ?>
		<div class="col-sm-4">
			<?php echo $form->dropDownList($model, 'is_priority', array(Tasks::PRIORITY_NORMAL=>'NORMAL',Tasks::PRIORITY_EOD=>'MEDIUM (Must Do TODAY)', Tasks::PRIORITY_ASAP=>'HIGH (ASAP! Drop Everything!)'), $htmlOptions = array(
				'class'	=>	'form-control'
			)
			); ?>
		</div>
	</div>

	<div id="duedate-wrapper" class="form-group">
		<?php echo $form->labelEx($model, 'due_date', array(
			'class' => 'col-sm-2 control-label',
		)); ?>
		<div class="col-sm-7">

				<div id="task-dialog-quick-dates-container">
					<a href="javascript:void(0)" class="quick-date" data-days="0">Today</a>
					<a href="javascript:void(0)" class="quick-date" data-days="1">Tomorrow</a>
					<a href="javascript:void(0)" class="quick-date" data-days="3">3 Days</a>
					<a href="javascript:void(0)"  class="quick-date" data-days="7">1 Week</a>
					<a href="javascript:void(0)" class="quick-date" data-days="14">2 Weeks</a>
					<a href="javascript:void(0)" class="quick-date" data-days="30">1 Month</a>
				</div>

			<?php echo $form->textField($model, 'due_date', array(
				'class'	=>	'form-control'
			)); ?>
		</div>

		<label class="col-sm-2 control-label">Is Recurring:</label>
		<div class="col-sm-1">
			<?php echo CHtml::checkBox('isRecurring', array(
				'class'	=>	'form-control'
			)) ?>
			<?php echo $form->error($model, 'due_date'); ?>
		</div>
	</div>

	<div id="recur-type-wrapper" class="hidden form-group">
		<label style="" class="control-label col-sm-2">Recurring Type:</label>
		<div class="col-sm-10">
			<div>
				<?php echo $form->dropDownList($taskRecursions, 'recur_type', array(
					'One time only'	=> 'One time only',
					'Daily' => 'Daily',
					'Every weekday (Monday to Friday)' => 'Every weekday (Monday to Friday)',
					'Weekly' => 'Weekly',
					'Bi-Weekly' => 'Bi-Weekly',
					'Monthly' => 'Monthly',
					'Bi-Monthly' => 'Bi-Monthly',
					'Quarterly' => 'Quarterly',
					'Semi-Annually' => 'Semi-Annually',
					'Yearly' => 'Yearly'
				), array(
		//						'empty' => ''
					)
				); ?>
				<?php echo $form->error($taskRecursions, 'recur_type'); ?>
			</div>
		</div>
	</div>

	<div id="recur-date-wrapper" style=" padding-top: 0;">
		<div id="recur-start-wrapper" class="form-group">
			<label class="control-label col-sm-2">Start:</label>
			<?php //echo CHtml::radioButtonList('recurStartDateType', 1, array(1=>''), $htmlOptions=array('separator'=>' ')); ?>
			<div id="recur-start-date-container" class="col-sm-10">
				<div id="recur-start-date-wrapper">
					<?php echo $form->textField($taskRecursions, 'recur_start_date', array(
						'class'	=>	'form-control'
					)); ?>
					<?php /*$this->widget('admin_module.extensions.CJuiDatePicker.CJuiDatePicker', array(
											'attribute' => 'recur_start_date',
											'model' => $taskRecursions,
											'options' => array( // 'showAnim'=>'fold',
											),
											'htmlOptions' => array(
												'placeholder' => 'Select Start Date',
												'style' => 'width:200px;',
												'readonly' => false,
											),
										));*/
					?><span class="label"> (Creates future tasks only)</span>
				</div>
				<?php echo $form->error($taskRecursions, 'recur_start_date'); ?>
			</div>

		</div>
		<div id="recur-end-wrapper" class="form-group">
			<label class="control-label col-sm-2">End: </label>
			<div class="col-sm-10">
				<?php echo CHtml::radioButtonList('recurEndDateType', 0, array(0=>'Never',1=>'Select Date'), array('separator'=>' ')); ?>
				<div id="recur-start-date-container" style="display: inline-block;">
					<div id="recur-end-date-wrapper" style="display: none;">
						<?php echo $form->textField($taskRecursions, 'recur_end_date', array(
							'class'	=>	'form-control'
						)); ?>
						<?php /*$this->widget('admin_module.extensions.CJuiDatePicker.CJuiDatePicker', array(
												'attribute' => 'recur_end_date',
												'model' => $taskRecursions,
												'options' => array( // 'showAnim'=>'fold',
												),
												'htmlOptions' => array(
													'style' => 'width:100px;',
													'readonly' => false,
												),
											));*/
						?>
					</div>
				</div>
				<?php echo $form->error($taskRecursions, 'recur_end_date'); ?>
			</div>
		</div>
	</div>


	<div id="description-wrapper" class="form-group">
		<?php echo $form->labelEx($model, 'description', array(
			'class'	=>	'col-sm-2 control-label'
		)); ?>
		<div class="col-sm-10">
			<?php echo $form->textArea($model, 'description', $htmlOptions = array(
				'placeholder' => 'Say What Next? (Ex. "Ask them about how their vacation to Florida was and if they still plan on seeing homes next week.")',
				'class' => 'form-control',
				'rows' => 3,
			)); ?>
			<?php echo $form->error($model, 'description'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'operation_manual_id', array(
			'class'	=>	'control-label col-sm-2'
		)); ?>
		<div class="col-sm-10">
			<?php echo $form->dropDownList($model, 'operation_manual_id', CHtml::listData(OperationManuals::model()->findAll(), 'id', 'name'), array(
				'empty'	=>	StmFormHelper::EMPTY_DROPDOWN_VALUE,
				'class'	=>	'form-control',
			)); ?>
			<?php echo $form->error($model, 'operation_manual_id'); ?>
		</div>
	</div>

	<div id="task-type-wrapper" class="form-group">
		<?php echo $form->labelEx($model, 'assigned_to_id', array(
			'class'	=>	'control-label col-sm-2'
		)); ?>
		<div class="col-sm-10">
			<?php echo $form->dropDownList($model, 'assigned_to_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->assigned_to_id)->orderByName()->findAll(), 'id', 'fullName'), array(
				'empty' =>	StmFormHelper::EMPTY_DROPDOWN_VALUE,
				'class'	=>	'form-control'
			)); ?>
			<?php echo $form->error($model, 'assigned_to_id'); ?>
		</div>
	</div>

	<div id="delete-type-wrapper" style="background: none; background-color: darkred; color: white; padding: 12px 0; display: none;" class="form-group">
		<label for="delete_type" class="control-label col-sm-2">Delete Type</label>
		<div class="col-sm-10">
			<select name="delete_type" id="delete_type" class="form-control">
				<option value="Single">This occurrence only</option>
				<option value="All">All occurrences</option>
			</select>
		</div>
	</div>
</div>
<? if($apiKey = ApiKeys::model()->findByAttributes(array('type'=>'google','contact_id'=>Yii::app()->user->id))): //!YII_DEBUG &&  ?>
    <div class="form-group">
        <label class="control-label col-sm-2">
            Sync w/Google
        </label>
        <div>
            <?php echo $form->checkBox($model, 'googleCalendarData[add_event]', $htmlOptions=array()) ?>
            <div id="sync-google-container" class="hidden col-sm-10">
				<?php
				// if the user has permission for the calendar display data, if not display error message
				if(/*!YII_DEBUG && */(!$model->google_calendar_id || ($model->google_calendar_id && !empty($model->googleCalendarData)))) {

					Yii::import('admin_module.components.StmOAuth2.Google', true);
					$stmGoogle = new \StmOAuth2\Google();
					$googleCalendarList = array();
					$googleCalendarListSecondary = array();

					$success = false;
					try {
						$listCalendars = $stmGoogle->listCalendars();
						$success = true;
					}
					catch (\Google_Auth_Exception $e) {
						echo "Your Google Calendar token is invalid. Please try again or re-do your Integration under Tools => Integration.";
					}
					catch (\Exception $e) {
						\Yii::log(__CLASS__.' (:'.__LINE__.") Google Calendar List error: " . $e->getMessage(), \CLogger::LEVEL_ERROR);
					}

					if($success) {

						foreach($listCalendars as $calendar) {
							// set the primary
							if($calendar->primary) {
								$googleCalendarList[$calendar->id] = $calendar->summary;
								$calendarDefault = $calendar->id;
							}
							else {
								$googleCalendarListSecondary[$calendar->id] = $calendar->summary;
							}
						}
						$googleCalendarList = CMap::mergeArray($googleCalendarList, $googleCalendarListSecondary);

						$model->google_calendar_id = ($model->google_calendar_id)? $model->google_calendar_id : $calendarDefault;
						if($calendarDefault):
							?>
							<?php //echo $form->dropDownList($model, 'googleCalendarData[add_event]', array(1=>'Yes, Sync to Google Calendar Now',0=>'No, Ignore Google Calendar'), $htmlOptions = array('class'=>'g12')); ?>
							<?php //echo $form->textField($model, 'googleCalendarData[event_name]', $htmlOptions = array('placeholder' => 'Google Calendar Event Name','class' => 'p-fl g12', 'value'=>$eventName)); ?>
							<?php //echo $form->error($model, 'googleCalendarData[event_name]'); ?>
							<?php //echo $form->textField($model, 'googleCalendarData[event_location]', $htmlOptions = array('placeholder' => 'Google Calendar Location','class' => 'p-fl g12','value'=>$evenLocation)); ?>
                        <div class="col-sm-6">
                            <?php echo $form->dropDownList($model, 'googleCalendarData[event_duration]', array('30'=>'30 min duration', '60'=>'1 hour duration','90'=>'1.5 hours duration','120'=>'2 hours duration'), $htmlOptions = array('class'=>'form-control')); ?>
                        </div>
                        <? $model->google_calendar_id = $calendarDefault; ?>
                        <div class="col-sm-6">
                            <?php echo $form->dropDownList($model, 'google_calendar_id', $googleCalendarList, $htmlOptions = array('class' => 'form-control col-sm-6')); ?>
                        </div>
                                <?php echo $form->error($model, 'google_calendar_id'); ?>
                                <?php echo $form->hiddenField($model, 'google_calendar_event_id'); ?>
                                <?php echo $form->error($model, 'google_calendar_event_id'); ?>
                            <?php
                            endif;
                        }
				}
				?>
            </div>
            <div id="no-calendar-permission-container" class="hidden col-sm-10">
                <span class="contact-name">Google Calendar event exists, but you do not have permission to edit this calendar.</span>
            </div>
        </div>
    </div>
<?php endif; ?>





<div class="stm-modal-footer">
    <?php //=$form->hiddenField($model, 'id');?>
    <?php echo $form->hiddenField($model, 'component_id'); ?>
    <?php echo $form->hiddenField($model, 'component_type_id'); ?>
    <button type='submit'>{{buttonText}}</button>
</div>

<?$this->endWidget();?>
<?='</script>'?>
