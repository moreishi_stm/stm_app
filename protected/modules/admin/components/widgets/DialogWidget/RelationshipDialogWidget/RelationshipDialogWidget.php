<?php
	Yii::import('admin_widgets.DialogWidget.DialogWidget');

	class RelationshipDialogWidget extends DialogWidget {

		const RELATIONSHIP_DIALOG_ID = 'relationship-dialog';
		const RELATIONSHIP_TRIGGERS = '.add-relationship-button, .edit-relationship-button';
		public $contactModel;

		public function run() {
			$model = new ContactRelationshipLu;
            if($this->contactModel) {
                $model->origin_contact_id = $this->contactModel->id;
            }

			$this->render('relationshipDialog', array(
					'model' => $model,
                    'contactName' => $this->contactModel->fullName,
				)
			);
		}

		/**
		 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
		 *
		 * @return null
		 */
		public function registerTriggerScript() {
            Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);

//			$varName = 'taskList';
//
//			$taskTypeDropDownLists = array('taskTypeDropDownLists' => TaskTypeComponentLu::getDropDownLists());
//			$taskTypeDropDownLists = CJSON::encode($taskTypeDropDownLists);
//			$showLeadGenTypes = '["'.ComponentTypes::CONTACTS.'", "'.ComponentTypes::BUYERS.'", "'.ComponentTypes::SELLERS.'", "'.ComponentTypes::RECRUITS.'"]';
			$module = Yii::app()->controller->module->name;
			$js = <<<JS
                $(document).ready(function () {
                    $("select#ContactRelationshipLu_related_to_contact_id").ajaxChosen({
                        type: 'POST',
                        url: '/$module/contacts/autocomplete',
                        dataType: 'json'
                    }, function (data) {
                        var results = [];

                        $.each(data, function (i, val) {
                            results.push({ value: val.value, text: val.text+" ("+val.email+")" });
                        });

                        return results;
                    });
                });

            $('$this->triggerElement').live('click', function() {

                var Dialog = $('#$this->id');

    			Dialog.find(':input:not(input[type=hidden])').val('');   //clear all input except the hidden ones
    			Dialog.find('div.errorMessage').html('').hide();
    			Dialog.find('.error').removeClass('error');
			    $("#is-past-referral-container").hide();

                var isReferral = $('#ContactRelationshipLu_is_referral');
                var isPastReferral = $('#ContactRelationshipLu_is_past_referral');
    			isReferral.attr("checked",false);
    			isReferral.val("0");
    			isPastReferral.attr("checked",false);
    			isPastReferral.val("0");

    			var DialogForm = Dialog.find('form#$this->formId');

    			// Reset the action on each triggering attempt
    			DialogForm.attr('action', '/$module/contacts/addRelationship');
    			DialogForm.data('settings').validationUrl = DialogForm.attr('action');
    			var DialogFormAction = DialogForm.attr('action');

    			// If edit was clicked then we need to dynamically fill in the values for the popup
    			var element = $(this);
    			if (element.hasClass('edit-relationship-button')) {
        			var relationshipId = $(this).data('id');
    				DialogForm.attr('action', '/$module/contacts/editRelationship/'+relationshipId);

    				// Create the scenario specific text for Edit Activity
    				$('span.ui-button-text:contains("Add Relationship")').html('Edit Relationship');
    				$('span.ui-dialog-title:contains("Add Relationship")').html('Edit Relationship');

    				var editDialogFormAction = DialogFormAction.replace('add', 'edit')+'/'+relationshipId;
    				DialogForm.attr('action', editDialogFormAction);
    				DialogForm.data('settings').validationUrl = editDialogFormAction;

    				$.post('/$module/contacts/viewRelationship/' + relationshipId, function(data) {
    					data = jQuery.parseJSON(data);
    		        	$('input[name*="ContactRelationshipLu[id]"]').val(data.id);
    		        	$('select[name*="ContactRelationshipLu[origin_relationship_type_id]"]>option[value="'+data.origin_relationship_type_id+'"]').attr('selected', true);
                        $('select#ContactRelationshipLu_origin_relationship_type_id').trigger('liszt:updated');
                        $('select#ContactRelationshipLu_related_to_contact_id').html('<option value="'+data.related_to_contact_id+'">'+data.related_to_contact_fullName+'</option>');
                        $('select#ContactRelationshipLu_related_to_contact_id').trigger('liszt:updated');
    		        	$('select[name*="ContactRelationshipLu[task_type_id]"]>option[value="'+data.task_type_id+'"]').attr('selected', true);
    		        	$('select[name*="ContactRelationshipLu[related_to_relationship_type_id]"]>option[value="'+data.related_to_relationship_type_id+'"]').attr('selected', true);
                        $('select#ContactRelationshipLu_related_to_relationship_type_id').trigger('liszt:updated');
    		        	if(data.is_referral=='1'){
                            $('#ContactRelationshipLu_is_referral').attr('checked',true);
                            $('#ContactRelationshipLu_is_referral').val('1');
    		        	}
    		        	if(data.is_past_referral=='1'){
                            $('#ContactRelationshipLu_is_past_referral').attr('checked',true);
                            $('#ContactRelationshipLu_is_past_referral').val('1');
    		        	}
    		        	$('textarea[name*="ContactRelationshipLu[notes]"]').val(data.notes);

            		    $('.summary-origin-relationship').html($('#ContactRelationshipLu_origin_relationship_type_id option:selected').text());
		                $('.summary-related-relationship').html($('#ContactRelationshipLu_related_to_relationship_type_id option:selected').text());
		                $('.summary-related-contact').html($('#ContactRelationshipLu_related_to_contact_id option:selected').text());
    				});
    			} else {
    				// Revert the scenario specific text for Add Relationship
    				$('span.ui-button-text:contains("Edit Relationship")').html('Add Relationship');
    				$('span.ui-dialog-title:contains("Edit Relationship")').html('Add Relationship');
                    $('select[name*="ContactRelationshipLu[origin_relationship_type_id]"]>option[value=""]').attr('selected', true);
                    $('select#ContactRelationshipLu_origin_relationship_type_id').trigger('liszt:updated');
                    $("select#ContactRelationshipLu_related_to_contact_id").html("<option></option>");
                    $("select#ContactRelationshipLu_related_to_contact_id").trigger("liszt:updated");
                    $('select[name*="ContactRelationshipLu[related_to_relationship_type_id]"]>option[value=""]').attr('selected', true);
                    $('select#ContactRelationshipLu_related_to_relationship_type_id').trigger('liszt:updated');

                    $('.summary-origin-relationship').html('[Relationship]');
                    $('.summary-related-relationship').html('[Relationship]');
                    $('.summary-related-contact').html('[Contact]');
    			}

			Dialog.dialog('open');
			return false;
		});

		var isReferral = $('#ContactRelationshipLu_is_referral');
		isReferral.click(function() {
			if ($(this).is(':checked')) {
			    $("#is-past-referral-container").show('normal');
				return $(this).val(1);
			} else {
			    $("#is-past-referral-container").hide('normal');
			    return $(this).val(0);
			}
		});

		var isPastReferral = $('#ContactRelationshipLu_is_past_referral');
		isPastReferral.click(function() {
			if ($(this).is(':checked'))
				return $(this).val(1);

			return $(this).val(0);
		});

		var originRelationship = $('#ContactRelationshipLu_origin_relationship_type_id');
		originRelationship.change(function(){
		    $('.summary-origin-relationship').html($('#ContactRelationshipLu_origin_relationship_type_id option:selected').text());
		});

		var relatedRelationship = $('#ContactRelationshipLu_related_to_relationship_type_id');
		relatedRelationship.change(function(){
		    $('.summary-related-relationship').html($('#ContactRelationshipLu_related_to_relationship_type_id option:selected').text());
		});

		var relatedContact = $('#ContactRelationshipLu_related_to_contact_id');
		relatedContact.change(function(){
		    var nameEmail = $('#ContactRelationshipLu_related_to_contact_id option:selected').text();
		    var name = nameEmail.replace(/ *\([^)]*\) */g, '');
		    $('.summary-related-contact').html(name);
		});
JS;

			Yii::app()->clientScript->registerScript('relationshipDialogTrigger', $js);
		}
	}
