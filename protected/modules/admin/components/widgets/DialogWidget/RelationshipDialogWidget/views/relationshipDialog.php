<style type="text/css">
    form fieldset > section > div {
        width: 71%;
    }
    form fieldset > section label {
        width: 24%;
    }
    .chzn-container-single .chzn-single span {
        font-size: 13px;
    }
    .chzn-container-single .chzn-single {
        height: 28px;
    }
    .chzn-container-single .chzn-single div b {
        top: 3px;
        position: relative;
    }
    .chzn-container-single .chzn-single abbr {
        top: 8px;
    }
    #summary-relationship span{
        /*color: black;*/
        font-weight: bold;
    }
    .summary-origin-relationship, .summary-related-relationship {
        text-decoration: underline;
        color: #58F;
    }
</style>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
     'id'       => $this->id,
     'options'  => array(
     	'title'    => $this->title,
		'autoOpen' => false,
		'modal'    => true,
		'width'    => 750,
		'buttons'  => array(
			'Add Relationship' => "js:function() {
				$('#".$this->formId."').submit();
			}",
       ),
     ),
));

$form=$this->beginWidget('CActiveForm', array(
    'id'=>$this->formId,
    'action'=>array('/'.Yii::app()->controller->module->name.'/contacts/addRelationship'),
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>false,
    'clientOptions'=>array(
    	'validateOnChange'=>false,
    	'validateOnSubmit'=>true,
		'beforeValidate'    => 'js:function(form) {
	            $(".loading-container").addClass("loading");
				return true;
            }',
        'afterValidate'=>'js:function(form, data, hasError) {
            if(!hasError) {
                location.reload();
            } else {
    			$(".loading-container").removeClass("loading");
            }
        }',
    ),
));
?>
<fieldset>
    <section>
        <h2 class="contact-name" style="margin:10px 0 10px 0; color: #58F;"><?php echo $contactName; ?></h2>
    </section>
    <section>
        <label for="" class="required">Summary</label>
        <div id="summary-relationship" style="font-weight: bold; color: #444;">
            <span class="summary-related-contact">[Contact]</span> is <span class="summary-origin-relationship">[Relationship]</span> to <span class="summary-origin-contact"><?php echo $contactName;?></span>.<br />
            <span class="summary-origin-contact"><?php echo $contactName;?></span> is <span class="summary-related-relationship">[Relationship]</span> to <span class="summary-related-contact">[Contact]</span>.
        </div>
    </section>
    <section>
        <?php echo $form->labelEx($model,'related_to_contact_id'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'related_to_contact_id', array(''=>''), $htmlOptions = array(
                    'data-placeholder' =>'Type Contact Name Here...',
                ));
            echo $form->error($model, 'related_to_contact_id');

            $this->widget('admin_module.extensions.EChosen.EChosen', array(
                                                                     'target' => 'select#ContactRelationshipLu_related_to_contact_id',
                                                                     'options'=>array(
                                                                         'allow_single_deselect'=>true,
                                                                         'width' => '400px',
                                                                     ),
                                                                     ));
            ?>
        </div>
    </section>
    <section>
        <?php echo $form->labelEx($model,'origin_relationship_type_id'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'origin_relationship_type_id', CHtml::listData(ContactRelationshipTypes::model()->findAll(), 'id', 'name'), $htmlOptions=array(
                    'data-placeholder'=>'Select Relationship',
                    'empty'=>'',
                )); ?>
            <?php echo $form->error($model, 'origin_relationship_type_id'); ?>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ContactRelationshipLu_origin_relationship_type_id','options'=>array('allow_single_deselect'=>true, 'width'=>'400px;'))); ?>
        </div>
    </section>
    <section>
        <?php echo $form->labelEx($model,'related_to_relationship_type_id'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'related_to_relationship_type_id', CHtml::listData(ContactRelationshipTypes::model()->findAll(), 'id', 'name'), $htmlOptions=array(
                    'data-placeholder'=>'Select Relationship',
                    'empty'=>'',
                )); ?>
            <?php echo $form->error($model, 'related_to_relationship_type_id'); ?>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ContactRelationshipLu_related_to_relationship_type_id','options'=>array('allow_single_deselect'=>true, 'width'=>'400px;'))); ?>
        </div>
    </section>
	<section>

		<div>
			<?php echo $form->checkBox($model, 'is_referral' ,$htmlOptions=array(
					'uncheckValue'=>'0',
					'style'=>'font-size:16px',
				)); ?><?php echo $form->labelEx($model,'is_referral',$htmlOptions=array('style'=>'padding-left:3px')); ?>
                <span id="is-past-referral-container" style="display: none;">
    				<?php echo $form->checkBox($model, 'is_past_referral' ,$htmlOptions=array(
                            'uncheckValue'=>'0',
                            'style'=>'font-size:16px',
                        )); ?>
				    <?php echo $form->labelEx($model,'is_past_referral',$htmlOptions=array('style'=>'padding-left:0')); ?>
                </span>
            <?php echo $form->error($model, 'is_past_referral'); ?>
			<?php echo $form->error($model, 'is_referral'); ?>
		</div>
	</section>
    <section>
        <?php echo $form->labelEx($model,'notes'); ?>
        <div>
            <?php echo $form->textArea($model,'notes',$htmlOptions=array(
                'placeholder'=>'Relationship Notes',
                'class'=>'g11',
                'rows'=>8,
            )); ?>
            <?php echo $form->error($model, 'notes'); ?>
        </div>
    </section>
    <?php echo $form->hiddenField($model, 'origin_contact_id');?>
</fieldset>
<div class="loading-container"><em></em></div>
<?php
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');
