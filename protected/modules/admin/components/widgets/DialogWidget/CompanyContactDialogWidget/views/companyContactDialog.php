<?
$css = <<<CSS
	.chzn-container .chzn-results {
		max-height: 155px;
	}

	div#contactNameSelectorContainer {
        padding: 10px;
        margin-left: 50px;
	}

	h4 {
	    padding-top: 10px;
	}
CSS;
Yii::app()->clientScript->registerCss('chosenStyle', $css);

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'       => $this->id,
        'options'  => array(
        'title'    => $title,
        'autoOpen' => false,
        'modal'    => true,
        'width'    => 500,
        'height'   => 350,
        'beforeClose'   => 'js:function(event, ui) {
                                $("select.companyContacts").html("<option></option>");
                                $(".companyContacts").trigger("liszt:updated");
                            }',
	 ),
));

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'company-contacts-form',
		'action' => Yii::app()->createUrl("/".Yii::app()->controller->module->id."/companies/addContact"),
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
			'afterValidate' => 'js:function(form, data, hasError) {
				if (!hasError) { // Action was successful
					$("#' . $this->id . '").dialog("close");
					Message.create("success", "Successfully Added Company Contact!");
					$.fn.yiiGridView.update("company-contacts-grid");
				}
				return false;
			}',
		)
	));

	?>
	<h4>Type in the name of the contact below:</h4>
	<div id="contactNameSelectorContainer">
		<?php echo $form->dropDownList($model, 'contact_id', array(''=>''), $htmlOptions = array(
				'class' => 'companyContacts',
                'style' => 'width: 350px;',
				'data-placeholder' =>'Type Contact Name Here...',
			));
			echo $form->error($model, 'contact_id');
			echo $form->hiddenField($model, 'company_id', $htmlOptions=array('value'=>$companyId));

		$this->widget('admin_module.extensions.EChosen.EChosen', array(
			'target' => 'select.companyContacts',
			'options'=>array(
				'allow_single_deselect'=>true,
				'width' => '400px',
			),
		));
		?>
	</div>
	<div class="p-tc" style="margin-top:115px;">
		<input type="submit" class="button" style="font-size:10px;margin-left:auto;margin-right:0;" value="Add Company Contact">
	</div>
	<?php
	$this->endWidget();
    $this->endWidget('zii.widgets.jui.CJuiDialog');
