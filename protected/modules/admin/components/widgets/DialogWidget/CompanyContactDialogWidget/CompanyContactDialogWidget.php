<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class CompanyContactDialogWidget extends DialogWidget
{

	public $companyId;

	public function run() {
		// Can't run without a way to call the dialog
        if (!$this->triggerElement)
            return false;

        $this->registerScript();

        $model = new CompanyContactLu;
        $model->company_id = $this->companyId;

		$this->render('companyContactDialog', array(
			'model'          => $model,
			'title'          => $this->title,
		));
	}

	public function registerScript() {
		Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);

        $moduleName = Yii::app()->controller->module->id;
		$js = <<<JS
			$("select.companyContacts").ajaxChosen({
				type: 'POST',
				url: '/$moduleName/contacts/autocomplete',
				dataType: 'json'
			}, function (data) {
				var results = [];

				$.each(data, function (i, val) {
                    results.push({ value: val.value, text: val.text+" ("+val.email+")" });
				});

				return results;
			});
JS;
		Yii::app()->clientScript->registerScript('company-contact-script', $js);
	}
}
