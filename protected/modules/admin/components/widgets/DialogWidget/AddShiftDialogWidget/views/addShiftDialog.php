<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id' => $this->id,
			'options' => array(
				'title' => $this->title,
				'autoOpen' => false,
				'modal' => true,
				'width' => 440,
				'buttons' => array(
					'Add Shift' => "js:function() {
                $('#" . $this->formId . "').submit();
            }",
					'Remove Shift' => "js:function() {
                $('#deleteShiftForm').submit();
            }",
				),
			),
		)
	);

	$form = $this->beginWidget('CActiveForm', array(
			'id' => $this->formId,
			'action' => array('/admin/shift/add'),
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) { // Action was successful

                    $("#' . $this->id . '").dialog("close");
                    var actionVerb = ($(\'span[class="ui-button-text"]:contains("Add Shift")\').length) ? "added" : "saved";
                    Message.create("success", "Successfully " + actionVerb + " shift!");

                    $("div.calendar").fullCalendar("refetchEvents");
                    return false;
                }
            }',
			),
		)
	);
?>

<table>
	<tr>
		<th><?php echo $form->labelEx($model, 'contact_id'); ?>:</th>
		<td>
			<?php echo $form->dropDownList($model, 'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->contact_id)->findAll(), 'id', 'fullName', 'types.name'), $htmlOptions = array(
					'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
				)
			); ?>
			<?php echo $form->error($model, 'contact_id'); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo $form->labelEx($model, 'date'); ?>:</th>
		<td>
			<input type="hidden" name="hide_autofocus"/>
			<? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'date',
					'mode' => 'date',
					// additional javascript options for the date picker plugin
					'options' => array(
						'showAnim' => 'fold',
					),
					'htmlOptions' => array(
						'class' => 'g6',
					),
				)
			);?>
			<?php echo $form->error($model, 'date'); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo $form->labelEx($model, 'start_time'); ?>:</th>
		<td>
			<? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'start_time',
					'mode' => 'time',
					// additional javascript options for the date picker plugin
					'options' => array(
						'showAnim' => 'fold',
					),
					'htmlOptions' => array(
						'class' => 'g6',
					),
				)
			);?>
			<?php echo $form->error($model, 'start_time'); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo $form->labelEx($model, 'end_time'); ?>:</th>
		<td>
			<? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model' => $model,
					'attribute' => 'end_time',
					'mode' => 'time',
					// additional javascript options for the date picker plugin
					'options' => array(
						'showAnim' => 'fold',
					),
					'htmlOptions' => array(
						'class' => 'g6',
					),
				)
			);?>
			<?php echo $form->error($model, 'end_time'); ?>
		</td>
	</tr>
</table>

<?php
	echo $form->hiddenField($model, 'lead_route_id', $htmlOptions = array(
			'value' => Yii::app()->request->getQuery('id'),
		)
	);
	$this->endWidget();
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'deleteShiftForm',
			'action' => array('/admin/shift/delete'),
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) { // Action was successful

                    $("#' . $this->id . '").dialog("close");
                    Message.create("success", "Successfully removed shift!");

                    $("div.calendar").fullCalendar("refetchEvents");
                    return false;
                }
            }',
			),
		)
	);
?>
<?php echo $form->hiddenField($model, 'id'); ?>
<?php echo $form->error($model, 'id'); ?>
<?php
	$this->endWidget();
?>
