<?
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class AddShiftDialogWidget extends DialogWidget {

	public $updateShiftDialogId = 'updateShiftId';

	public function run() {

		$model = new LeadShift;

		$this->render('addShiftDialog', array(
			'model' => $model,
		));
	}

	public function registerTriggerScript() {
		$js = <<<JS
        var originalFormAction = $('#$this->formId').attr('action');

	    $('$this->triggerElement').live('click', function() {

	    	var Dialog = $('#$this->id');
	        Dialog.dialog('open');
	        Dialog.find(':input:not(input[type=hidden])').val('');   //clear all input execpt the hidden ones
	        Dialog.find('div.errorMessage').html('').hide();
	        Dialog.find('input, select, textarea').removeClass('error');

        	$('span[class="ui-button-text"]:contains("Remove Shift")').parent().hide();

			$('#$this->formId').attr('action', originalFormAction);
			var formSettings = $('#$this->formId').data('settings');
			formSettings.validationUrl = originalFormAction;
			$('#$this->formId').data('settings', formSettings);

            $('span[class="ui-dialog-title"]:contains("Edit Shift")').html("Add New Shift");
            $('span[class="ui-button-text"]:contains("Save Shift")').html("Add Shift");

	        var daySelected = $(this).parent().next().text();
	        if (daySelected) {

	        	var monthYearParts = $('span.fc-header-title h2').text().split(' ');
	        	var monthSelected = monthYearParts[0]; // As string "April"
	        	var yearSelected = monthYearParts[1];

	        	// Parses the month string, a literal map is quicker than programatically solving the problem
	        	var monthMap = {
					'January' : '01',
					'February' : '02',
					'March' : '03',
					'April' : '04',
					'May' : '05',
					'June' : '06',
					'July' : '07',
					'August' : '08',
					'September' : '09',
					'October' : '10',
					'November' : '11',
					'December' : '12'
	        	}
	        	monthSelected = monthMap[monthSelected];

	        	daySelected = Number(daySelected);
	        	if (daySelected < 10)
	        		daySelected = "0"+daySelected;

	        	var dateSelected = monthSelected+'/'+daySelected+'/'+yearSelected;

	        	Dialog.find('input[id*="date"]').val(dateSelected);
	        }

	        return false;
	    })

		function callUpdateShiftDialog(event) {
			$('span[class="ui-button-text"]:contains("Remove Shift")').parent().show();

	    	var Dialog = $('#$this->id');
	        Dialog.dialog('open');

            $('span[class="ui-dialog-title"]:contains("Add New Shift")').html("Edit Shift");
            $('span[class="ui-button-text"]:contains("Add Shift")').html("Save Shift");

	        $.getJSON('/admin/shift/getShift/'+event.shiftId, function(data) {
                Dialog.find('select[name*="contact_id"]>option[value="'+data.contactId+'"]').attr('selected', 'selected');
                Dialog.find('input[name*="date"]').val(data.date);
                Dialog.find('input[name*="start_time"]').val(data.startTime);
                Dialog.find('input[name*="end_time"]').val(data.endTime);

				// Set the id for potential removal
                $('input[name="LeadShift[id]"]').val(data.shiftId);

				var editFormAction = originalFormAction.replace('add', 'edit') + '/' + data.shiftId;
				$('#$this->formId').prop('action', editFormAction);

				var formSettings = $('#$this->formId').data('settings');
				formSettings.validationUrl = editFormAction;
				$('#$this->formId').data('settings', formSettings);
	        });

	        return false;
		}
JS;

    	Yii::app()->clientScript->registerScript('popupTrigger-'.$this->id, $js);
	}
}
