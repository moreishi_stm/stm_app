<style type="text/css">
	.jcrop-holder {
		margin-left: auto;
		margin-right: auto;
	}
	.jcrop-hline.bottom{
		padding: 0;
		border: none;
	}
	/*.ui-dialog-titlebar-close {display: none;}*/
</style>
<?
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => $this->id,
	'options' => array(
		'title' => $this->title,
		'autoOpen' => false,
		'closeOnEscape'=>true,
		'modal' => true,
		'width' => 600,
		'height'=> 600,
		'open'=>'js:function(event, ui) {
                    JcropAPI = $("#dialog-profile-photo").data("Jcrop");

                    if(JcropAPI) {
                        JcropAPI.destroy();
                    }

                    // Get on screen image
                    var profilePhoto = $("#dialog-profile-photo");
                    var photoHeight = profilePhoto.data("naturalheight");
                    var photoWidth = profilePhoto.data("naturalwidth");

                    $("#dialog-profile-photo").Jcrop({
                        aspectRatio:'.SettingContactValues::PROFILE_ASPECT_RATIO.',
                        setSelect:['.ProfilePhotoDialogWidget::PROFILE_PHOTO_INITIAL_SELECT.'],
                        trueSize: [photoWidth,photoHeight],
                        onChange: updateCoords,
                        onSelect: updateCoords
                    });

                    function updateCoords(c) {
                        var ratio = $("#dialog-profile-photo").data("ratio");
                        $("#x").val(c.x / ratio);
                        $("#y").val(c.y / ratio);
                        $("#w").val(c.w / ratio);
                        $("#h").val(c.h / ratio);
                    };
		}',
	),
));
$form = $this->beginWidget('CActiveForm', array(
	'id' => $this->formId,
	'action' => array(''),
));

?>
	<div style="text-align: center; margin-top:15px;">
		<?php  $image = ($this->photoPath) ? Yii::app()->user->getProfileImageBaseUrl().DS.$parentModel->contact_id.DS.$this->photoPath : Yii::app()->controller->getModule()->getCdnImageAssetsUrl().'profile_placeholder_1.jpg';
			echo CHtml::image($image, 'Profile Photo',$htmlOptions=array('id'=>'dialog-profile-photo','style'=>'max-width:400px;','data-naturalheight'=>'0','data-naturalwidth'=>'0','data-ratio'=>'1'));
			echo CHtml::hiddenField('x',null);
			echo CHtml::hiddenField('y',null);
			echo CHtml::hiddenField('w',null);
			echo CHtml::hiddenField('h',null);
			echo CHtml::hiddenField('fileType',null);
		?>
		<br />
		<br />
		<a class="button p-clr" id="apply-cropping-button" style="font-size:12px;">Apply Cropping</a>
	</div>

	<div class="loading-container"><em></em></div>
<?
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');