<?
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class ProfilePhotoDialogWidget extends DialogWidget {

	const PROFILE_PHOTO_DIALOG_ID = 'profile-photo-dialog';
	const PROFILE_PHOTO_INITIAL_SELECT = '25,25,119,157';
	public $parentModel;
	public $photoPath;
	public $contactId;

	public function init() {
		parent::init();
		$this->registerScript();
		if (!$this->parentModel instanceof CActiveRecord) {
			throw new CHttpException(500, 'Could not render the Profile Photo Dialog.');
		}
	}

	public function run() {
		$model = new Tasks;

		$this->render('profilePhotoDialog', array(
			'model' => $model,
			'parentModel' => $this->parentModel,
		));
	}

	/**
	 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
	 * @return null
	 */
	public function registerScript() {
        $moduleName = Yii::app()->controller->module->id;
		$js = <<<JS
			$("#apply-cropping-button").click(function(){
				if($('#w').val()==0 && $('#h').val()==0) {
					Message.create("notice","Please select a crop area.");
					return;
				}
				// ajax request to send
				var photoCropData = {};
				var data = {};
				photoCropData['x'] = $('#x').val();
				photoCropData['y'] = $('#y').val();
				photoCropData['h'] = $('#h').val();
				photoCropData['w'] = $('#w').val();
				photoCropData['fileType'] = $('#w').val();

				data['photoCropData'] = photoCropData;

				$.post('/$moduleName/settings/'+ $this->contactId, data, function(data) {
					if(data !="error") {console.log(data);
						var Dialog = $("#profile-photo-dialog");
						var Photo = $(".profile-photo");
						data = jQuery.parseJSON(data);
						Dialog.dialog("close");
						Photo.attr("src", data.urlPath + data.filename + "?time="+new Date());
					}
				});
			});
JS;

		Yii::app()->clientScript->registerScript('profileScript', $js);
	}
}