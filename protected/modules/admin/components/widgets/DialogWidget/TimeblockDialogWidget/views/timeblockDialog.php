<?
	Yii::app()->clientScript->registerScript('timeblockStartEndTime', '
	$("#timeblocking-form #Timeblockings_startHour, #timeblocking-form #Timeblockings_startMinute, #timeblocking-form #Timeblockings_endHour, #timeblocking-form #Timeblockings_endMinute").change(function(){

		var startHourInput = $("#timeblocking-form #Timeblockings_startHour");
		var startHour = Number($("#timeblocking-form #Timeblockings_startHour").val());
		var endHourInput = $("#timeblocking-form #Timeblockings_endHour");
		var endHour = Number($("#timeblocking-form #Timeblockings_endHour").val());

		var startMinuteInput = $("#timeblocking-form #Timeblockings_startMinute");
		var startMinute = startMinuteInput.val();
		var endMinuteInput = $("#timeblocking-form #Timeblockings_endMinute");
		var endMinute = endMinuteInput.val();

		var timeblockTotal = $("#timeblocking-form #timeblock-total");

		if((endHour < startHour) || ((endHour == startHour) && (endMinute <= startMinute))) {
			timeblockTotal.html("Invalid");
		} else {
			var startMinuteDecimal = startMinute/60;
			var endMinuteDecimal = endMinute/60;

			var startTime;
			var endTime;

			if(startMinuteDecimal == "0")
				startTime = startHour;
			else {
				startTime = startHour + startMinuteDecimal;
			}

			if(endMinuteDecimal == "0")
				endTime = endHour;
			else {
				endTime = endHour + endMinuteDecimal;
			}

			var totalHours = endTime - startTime;
			totalHours = Math.round(10*totalHours)/10;

			timeblockTotal.html(totalHours+" Hours");
		}
	});
'
	);

	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id' => $this->id,
			'options' => array(
				'title' => $title,
				'autoOpen' => false,
				'modal' => true,
				'width' => 500,
				'height' => 'auto',
				'beforeClose' => 'js:function() {
                $("#' . $this->formId . '")[0].reset();
            }',
				'buttons' => array(
					'Add Timeblock' => "js:function() {
                    $('#" . $this->formId . "').submit();

                    return false;
                }",
				),
			),
		)
	);

	$form = $this->beginWidget('CActiveForm', array(
			'id' => $this->formId,
			'action' => array('/admin/tracking/addTimeblocking'),
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'beforeValidate' => 'js:function(form) {
	            $(".loading-container").addClass("loading");
				var startHourInput = $("#timeblocking-form #Timeblockings_startHour");
				var startHour = Number($("#timeblocking-form #Timeblockings_startHour").val());
				var endHourInput = $("#timeblocking-form #Timeblockings_endHour");
				var endHour = Number($("#timeblocking-form #Timeblockings_endHour").val());

				var startMinuteInput = $("#timeblocking-form #Timeblockings_startMinute");
				var startMinute = $("#timeblocking-form #Timeblockings_startMinute").val();
				var endMinuteInput = $("#timeblocking-form #Timeblockings_endMinute");
				var endMinute = $("#timeblocking-form #Timeblockings_endMinute").val();

				var timeblockTotal = $("#timeblocking-form #timeblock-total");

				if((endHour < startHour) || ((endHour == startHour) && (endMinute <= startMinute))) {
					alert("Please enter valid time block.\nEnd time must be greater than your start time.");
					timeblockTotal.html("Invalid");
					$(".loading-container").removeClass("loading");
					return false;
				}

				return true;
            }',
				'afterValidate' => 'js:function(form, data, hasError) {
				$(".loading-container").removeClass("loading");
				if(!hasError) {
					if ($.isEmptyObject(data)) {
						$("#' . $this->id . '").dialog("close");
						Message.create("success", "Successfully saved timeblock!");
						$.fn.yiiGridView.update("timeblocking-grid",{\'complete\': function(jqXHR, status) {
							if (status=\'success\') { $("#timeblocking-tab").updateRecordCount(); }
						}});
						$.fn.yiiGridView.update("monthly-goals-grid");
						return false;
					} else if(!($.isEmptyObject(data))) {
						Message.create("error", "Please fill out all the required fields!");
					}
				}
            }',
			),
		)
	);
?>

<table>
	<tr>
		<th><?php echo $form->labelEx($model, 'timeblock_type_ma'); ?>:</th>
		<td>
			<?php
				$model->timeblock_type_ma = Timeblockings::LEAD_GEN_TYPE_PHONE_ID;
				$types = Timeblockings::getTypes();
				asort($types);
				echo $form->dropDownList($model, 'timeblock_type_ma', $types, $htmlOptions = array(
						'class' => 'g8',
						'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
					)
				);?>
			<?php echo $form->error($model, 'timeblock_type_ma'); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo $form->labelEx($model, 'assignment_type_id'); ?>:</th>
		<td>
			<?php
				$model->assignment_type_id = Yii::app()->user->settings->default_assignment_type;
				echo $form->dropDownList($model, 'assignment_type_id', CHtml::listData(AssignmentTypes::model()->byTimeblockTypes()->findAll(), 'id', 'display_name'), $htmlOptions = array(
						'class' => 'g8',
						'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
					)
				);?>
			<?php echo $form->error($model, 'assignment_type_id'); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo $form->labelEx($model, 'date'); ?>:</th>
		<td>
			<?php if (empty($model->date)) {
				$model->date = date("m/d/Y");
			}
				$this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
						'model' => $model,
						'attribute' => 'date',
						'mode' => 'date',
						// additional javascript options for the date picker plugin
						'options' => array(
							'showAnim' => 'fold',
						),
						'htmlOptions' => array(
							'class' => 'g6',
						),
					)
				);?>
			<?php echo $form->error($model, 'date'); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo $form->labelEx($model, 'start_time'); ?>:</th>
		<td>
			<?php if (empty($model->startHour)) {
				$model->startHour = date("H");
			}
				echo $form->dropDownList($model, 'startHour', StmFormHelper::getHoursList(), $htmlOptions = array(
						'class' => 'startHour',
					)
				);
				echo $form->dropDownList($model, 'startMinute', StmFormHelper::getMinutesList(), $htmlOptions = array(
						'class' => 'startMinute',
					)
				);
			?>
			<?php echo $form->error($model, 'start_time'); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo $form->labelEx($model, 'end_time'); ?>:</th>
		<td>
			<?php if (empty($model->endHour)) {
				$model->endHour = date("H", strtotime("+1 hours"));
			}
				echo $form->dropDownList($model, 'endHour', StmFormHelper::getHoursList(), $htmlOptions = array(
						'class' => 'endHour',
					)
				);
				echo $form->dropDownList($model, 'endMinute', StmFormHelper::getMinutesList(), $htmlOptions = array(
						'class' => 'endMinute',
					)
				);
			?>
			<?php echo $form->error($model, 'end_time'); ?>
		</td>
	</tr>
	<tr>
		<th>Total:</th>
		<td><span id="timeblock-total" style="font-size: 20px;font-weight: bold;">1 Hour</span>
		</td>
	</tr>
	<?php if(Yii::app()->user->checkAccess('timeblockUpdateOthers')) { ?>
		<tr>
			<th><?php echo $form->labelEx($model, 'contact_id'); ?>:</th>
			<td>
				<?php
				$model->contact_id = Yii::app()->user->id;
				echo $form->dropDownList($model, 'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->contact_id)->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions = array(
						'class' => 'g8',
						'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
					)
				);?>
				<?php echo $form->error($model, 'contact_id'); ?>
			</td>
		</tr>
	<?php } ?>
</table>
<div class="loading-container"><em></em></div>
<?
	$this->endWidget();
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
