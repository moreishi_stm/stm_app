<?
	Yii::import('admin_widgets.DialogWidget.DialogWidget');

	class TimeblockDialogWidget extends DialogWidget {

		public function run() {
			// Can't run without a way to call the dialog
			if (!$this->triggerElement) {
				return false;
			}

			$model = new Timeblockings;

			$this->render('timeblockDialog', array(
					'model' => $model,
					'title' => $this->title,
					'triggerElement' => $this->triggerElement,
				)
			);
		}

		public function registerTriggerScript() {
			// Register the normal triggers
			parent::registerTriggerScript();
			$defaultAssignmentTypeId = Yii::app()->user->settings->default_assignment_type;
			$defaultAssignmentTypeId = ($defaultAssignmentTypeId) ? $defaultAssignmentTypeId : 0;

			$isEditOthers = (Yii::app()->user->checkAccess('timeblockUpdateOthers'))? 1: 0;
			$timeblockingJs = <<<TIMEBLOCKING_JS
            var originalFormAction = $('#$this->formId').attr('action');

            // Reset the verbage for adding a timeblock
            $('#add-timeblock').click(function() {

                $('span:contains("Edit Timeblock")').html("Add Timeblock");

                $('select[name*="timeblock_type_ma"]').prop('selectedIndex', 0);
                if($defaultAssignmentTypeId) {
	                $('select[name*="assignment_type_id"]').val($defaultAssignmentTypeId);
                } else {
              		$('select[name*="assignment_type_id"]').prop('selectedIndex', 0);
				}

                $('#$this->formId').attr('action', originalFormAction);
                var formSettings = $('#$this->formId').data('settings');
                formSettings.validationUrl = originalFormAction;
                $('#$this->formId').data('settings', formSettings);
            });


            // If the edit button was clicked, then dynamically pull in the relevant data
            $('a.edit-timeblock').live('click', function() {

                $('span:contains("Add Timeblock")').html("Edit Timeblock");

                var fetchTimeblockUrl = $(this).attr('href');
                $.getJSON(fetchTimeblockUrl, function(data) {

                    $('select[name*="timeblock_type_ma"]>option[value="'+data.timeblock_type_ma+'"]').attr('selected', 'selected');
                    $('select[name*="assignment_type_id"]>option[value="'+data.assignment_type_id+'"]').attr('selected', 'selected');
                    $('select[name*="startHour"]>option[value="'+data.startHour+'"]').attr('selected', 'selected').click();
                    $('select[name*="startMinute"]>option[value="'+data.startMinute+'"]').attr('selected', 'selected').click();
                    $('select[name*="endHour"]>option[value="'+data.endHour+'"]').attr('selected', 'selected').click();
                    $('select[name*="endMinute"]>option[value="'+data.endMinute+'"]').attr('selected', 'selected').click();
                    if($isEditOthers) {
	                    $('select[name*="contact_id"]>option[value="'+data.contact_id+'"]').attr('selected', 'selected').click();
	                }

                    $('input[name*="date"]').val(data.date);

                    var editFormAction = originalFormAction.replace('add', 'edit') + '/' + data.id;
                    $('#$this->formId').prop('action', editFormAction);

                    var formSettings = $('#$this->formId').data('settings');
                    formSettings.validationUrl = editFormAction;
                    $('#$this->formId').data('settings', formSettings);
                });
            });
TIMEBLOCKING_JS;

			Yii::app()->getClientScript()->registerScript('timeblockingJs', $timeblockingJs);
		}
	}