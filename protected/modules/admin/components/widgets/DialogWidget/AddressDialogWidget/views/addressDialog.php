<style>
    #address-form input {
        font-size: 16px;
    }
    #address-form select {
        font-size: 13px;
        height: 32px;
    }
    #existing-addresses .address-row label{
        font-size: 13px;
        font-weight: bold;
        color: #D20000;
    }
</style>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'       => $this->id,
    'options'  => array(
        'title'    => $title,
        'autoOpen' => false,
        'modal'    => true,
        'width'    => 600,
        'height'   => 'auto',
        'beforeClose'   => 'js:function(event, ui) {
                                $("select.companyContacts").html("<option></option>");
                                $(".companyContacts").trigger("liszt:updated");
                            }',
    ),
));
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'address-form',
        'action' => Yii::app()->createUrl('/admin/'.$transactionType.'/addAddress',array('transaction_id'=>$transactionId,'contact_id'=>$contactId)),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnChange' => false,
            'validateOnSubmit' => true,
            'afterValidate' => 'js:function(form, data, hasError) {
                    if(!hasError) {
						var currentValue = $("select.address-dropdown").val();
						if(data.existing != true) {
                        	$("select.address-dropdown").append("<option value="+data.id+" selected>"+data.fullAddress+"</option>");
                            if(currentValue != "") {
                                $("select.address-dropdown > option[value="+currentValue+"]").removeAttr("selected");
                            }
                            Message.create("success", "Successfully added Address!");
						}
						else {
						    $("select.address-dropdown").val(data.id);
                            Message.create("success", "Selected existing matching address. Please review selection to verify.");
						}

                        $("#' . $this->id . '").dialog("close");
                    }
                    return false;
                }',
        )
    ));
    ?>
    <div id="red">
    <?php
    $addresses = (isset($this->transaction->contact->addresses))? $this->transaction->contact->addresses : false;
    if($addresses) { ?>
    <div id="existing-addresses">
        <h4>Select an Existing Address</h4>
        <?php foreach($addresses as $address) { ?>
        <div class="p-tc address-row" style="margin: 10px 0 20px 0;">
			<table>
				<tr>
					<td style="width:150px;text-align: right;">
						<div><a href="javascript:void(0)" class="text-button select-address-button" data-aid="<?php echo $address->id;?>"><em class="icon i_stm_add"></em>Select</a></div>
					</td>
					<td style="text-align: left;">
						<label><?php echo $address->fullAddress;?></label>
					</td>
				</tr>
			</table>
        </div>
        <?php } ?>
        <h4>OR</h4>
    </div>
    <?php } ?>
    <br />
    <h4>Type in the address below:</h4>
    <br />
    <div class="g1"></div>
    <div class="g10 p-pl10">
        <div class="g12">
            <?php echo $form->textField($model, 'address', $htmlOptions = array(
                'placeholder' => 'Address',
                'class'=>'g12',
                'style' => 'width: 93%'
            )); ?>
            <?php echo $form->error($model, 'address'); ?>
        </div>
        <div class="g6">
            <?php echo $form->textField($model, 'city', $htmlOptions = array('placeholder' => 'City')); ?>
            <?php echo $form->error($model, 'city'); ?>
        </div>
        <div class="g2 p-ml10 p-tc">
            <?php echo $form->dropDownList($model, 'state_id', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array(
                'empty' => 'State',
                'style' => 'max-width:90%',
            )); ?>
            <?php echo $form->error($model, 'state_id'); ?>
        </div>
        <div class="g3 p-ml0">
            <?php $this->widget(
                'StmMaskedTextField', array(
                    'model'       => $model,
                    'attribute'   => 'zip',
                    'mask'        => '99999',
                    'htmlOptions' => $htmlOptions = array('placeholder' => 'Zip'),
                )
            ); ?>
            <?php echo $form->error($model, 'zip'); ?>
        </div>

    </div>
    <div class="p-tc" style="margin-top:115px;margin-bottom:20px;">
        <input type="submit" class="button wide" style="font-size:10px;margin-left:auto;margin-right:0;" value="Add Address">
    </div>
    </div>
    <?php
    $this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');
