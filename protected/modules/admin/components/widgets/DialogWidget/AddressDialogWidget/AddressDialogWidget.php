<?php

Yii::import('admin_widgets.DialogWidget.DialogWidget');

class AddressDialogWidget extends DialogWidget {

    public $parentModel;
	public $contactId;
	public $transaction;

    public function run() {
        if (!$this->triggerElement)
            return false;

        $this->registerScript();

        $model = new Addresses;
        $this->render('addressDialog', array(
			'model'          => $model,
			'title'          => $this->title,
			'transactionType'=> strtolower(get_class($this->transaction)),
			'contactId'      => $this->contactId,
			'transactionId'  => $this->transaction->id,
        ));
    }

    public function registerScript() {
//        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js');
        $formId = $this->id;
        $js = <<<JS
		$(document).ready(function () {
		    $(".select-address-button").click(function(){
                var Dialog = $("#$formId");
				$('select.address-dropdown > option[value="$(this).data("aid")"]').attr("selected", "selected");
                Dialog.dialog("close");
                Message.create("success","Address successfully Added!");
		    });
		});
JS;
        Yii::app()->clientScript->registerScript('address-dialog-script', $js);
    }
}