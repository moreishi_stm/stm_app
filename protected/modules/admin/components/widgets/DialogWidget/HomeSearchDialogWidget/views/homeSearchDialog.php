<?php
	$css = <<<CSS
	#home-search-dialog-form .chzn-container{
		float:left;
		clear:none;
	}
	#home-search-dialog-form .chzn-container .chzn-results {
		max-height: 180px;
	}
	form#home-search-dialog-form fieldset > section label:first-child {
	    width: 20%;
	}
	form#home-search-dialog-form fieldset > section > div {
        width: 75%;
    }
CSS;
	Yii::app()->clientScript->registerCss('homeSearchChosenStyle', $css);

	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id' => $this->id,
			'options' => array(
				'title' => $title,
				'autoOpen' => false,
				'modal' => true,
				'width' => 700,
			),
		)
	);

	$form = $this->beginWidget('CActiveForm', array(
			'id' => $this->formId,
			'action' => array('/admin/savedHomeSearch/add'),
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'beforeValidate' => 'js:function(form) {
	            $(".loading-container").addClass("loading");
				return true;
            }',
				'afterValidate' => 'js:function(form, data, hasError) {
                $(".loading-container").removeClass("loading");
                if (!$.isEmptyObject(data) || hasError) { // Action was successful
                    Message.create("error", data[Object.keys(data)[0]][0]);
                } else {
                    $("#' . $this->id . '").dialog("close");
                    Message.create("success", "Saved Search successfully updated.");
                    $.fn.yiiGridView.update("saved-home-search-grid");
                    return false;
                }
		}',
			),
		)
	);

// chosen for all select dropdowns applicable for this form
$this->widget('admin_module.extensions.EChosen.EChosen', array(
        'target' => '#home-search-dialog-form select.area, #home-search-dialog-form select.neighborhood, #home-search-dialog-form select.county, #home-search-dialog-form select.zip, #home-search-dialog-form select.city, #home-search-dialog-form select.region',
        'options' => array(
            'width' => '450px',
        ),
    )
);
?>
	<fieldset>
		<section>
			<?php echo $form->labelEx($model, 'name'); ?>
			<div>
				<?php echo $form->textField($model, 'name', $htmlOptions = array(
						'placeholder' => 'Search Name',
						'class' => 'searchName g9'
					)
				); ?>
                <?php echo $form->error($model, 'name'); ?>
			</div>
		</section>
        <section>
            <?php echo $form->labelEx($model, 'frequency'); ?>
            <div>
                <?php echo $form->dropDownList($model, 'frequency', SavedHomeSearches::getFrequencyTypes(), $htmlOptions = array('class' => 'frequency')); ?>
                <span id="days-of-week-container">
                Sun&nbsp;<?php echo $form->checkBox($model, 'sunday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                Mon&nbsp;<?php echo $form->checkBox($model, 'monday', array('value'=> 1, 'uncheckValue'=>0, 'class'=>'day-of-week')) ; ?>
                Tue&nbsp;<?php echo $form->checkBox($model, 'tuesday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                Wed&nbsp;<?php echo $form->checkBox($model, 'wednesday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                Thu&nbsp;<?php echo $form->checkBox($model, 'thursday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                Fri&nbsp;<?php echo $form->checkBox($model, 'friday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                Sat&nbsp;<?php echo $form->checkBox($model, 'saturday', array('value'=>1, 'uncheckValue'=>0, 'class'=>'day-of-week')); ?>
                </span>
            </div>
        </section>
        <section <?=($hasMultipleBoards) ? "" : "style='display:none;'"?>>
            <?php echo $form->labelEx($model, 'mls_board_id'); ?>
            <div>
                <?php echo $form->dropDownList($model, 'mls_board_id', CHtml::listData(MlsFeeds::model()->findAllByAttributes(array('client_id'=>Yii::app()->user->clientId,'account_id'=>Yii::app()->user->accountId)), 'mls_board_id', 'mlsBoard.name'), $htmlOptions = array()); ?>
            </div>
            <?php echo $form->error($model, 'mls_board_id'); ?>
        </section>
		<section>
			<?php echo $form->labelEx($model, 'agent_id'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'agent_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->agent_id)->orderByName()->findAll(), 'id', 'fullPhoneticName'), $htmlOptions = array()); ?>
			</div>
			<?php echo $form->error($model, 'agent_id'); ?>
		</section>
		<section>
			<?php echo $form->labelEx($model, 'property_type'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'property_type', CHtml::listData(MlsPropertyTypeBoardLu::model()->findAllByAttributes(array('mls_board_id'=>Yii::app()->user->boardId,'status_ma'=>1)), 'mls_property_type_id', 'propertyType.name'), $htmlOptions = array('class' => 'propertyType')
				); ?>
			</div>
			<?php echo $form->error($model, 'property_type'); ?><?php echo $form->error($model, 'frequency'); ?>
		</section>
        <? if(in_array('region', $searchableFields)): ?>
            <section>
                <?php echo $form->labelEx($model, 'region'); ?>
                <div>
                    <?php echo $form->dropDownList($model, 'region', CHtml::listData(MlsProperties::getRegionsList(Yii::app()->user->boardId), 'region', 'region'), $htmlOptions = array(
                            'empty' => '',
                            'class' => 'region chzn-select',
                            'style' => 'width:300px; float:left;',
                            'data-placeholder' => ' ',
                            'multiple' => 'multiple'
                        )
                    );
                    ?>
                </div>
                <?php echo $form->error($model, 'region'); ?>
            </section>
        <?php endif;?>
		<section>
			<?php echo $form->labelEx($model, 'area'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'area', CHtml::listData(MlsProperties::getAreasList(Yii::app()->user->boardId), 'area', 'area'), $htmlOptions = array(
						'empty' => '',
						'class' => 'area chzn-select',
						'style' => 'width:300px; float:left;',
						'data-placeholder' => ' ',
						'multiple' => 'multiple'
					)
				);
				?>
			</div>
            <?php echo $form->error($model, 'area'); ?>
		</section>
        <section>
            <?php echo $form->labelEx($model, 'public_remarks'); ?>
            <div>
                <?php echo $form->textField($model, 'public_remarks',  $htmlOptions = array('class' => 'g11')
                ); ?>
            </div>
            <?php echo $form->error($model, 'public_remarks'); ?>
        </section>
        <section>
            <?php echo $form->labelEx($model, 'city'); ?>
            <div>
                <?php echo $form->listBox($model, 'city', MlsProperties::getCityList(Yii::app()->user->boardId), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'city chzn-select',
                        'data-placeholder' => ' ',
                        'multiple' => 'multiple'
                    )
                );

//                echo $form->textField($model, 'city', $htmlOptions = array(
//                        'class' => 'g8',
//                        'style' => 'width:450px;',
//                    )
//                );
                ?>
            </div>
            <?php echo $form->error($model, 'city'); ?>
        </section>
        <section>
            <?php echo $form->labelEx($model, 'zip'); ?>
            <div>
                <?php echo $form->listBox($model, 'zip', MlsProperties::getZipList(Yii::app()->user->boardId), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'zip chzn-select',
                        'data-placeholder' => ' ',
                        'multiple' => 'multiple'
                    )
                );
                ?>
            </div>
            <?php echo $form->error($model, 'zip'); ?>
        </section>
        <section>
            <?php echo $form->labelEx($model, 'county'); ?>
            <div>
                <?php echo $form->listBox($model, 'county', MlsProperties::getCountyList(Yii::app()->user->boardId), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'county chzn-select',
                        'data-placeholder' => ' ',
                        'multiple' => 'multiple'
                    )
                );
                ?>
            </div>
            <?php echo $form->error($model, 'area'); ?><?php echo $form->error($model, 'zip'); ?>
        </section>
		<section>
			<?php echo $form->labelEx($model, 'price_min'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'price_min', StmFormHelper::getPriceList(), $htmlOptions = array(
						'empty' => '',
						'class' => 'price-min'
					)
				); ?> to
				<?php echo $form->dropDownList($model, 'price_max', StmFormHelper::getPriceList(), $htmlOptions = array(
						'empty' => '',
						'class' => 'price-max'
					)
				); ?>
				<?php echo $form->labelEx($model, 'sq_feet'); ?><?php echo $form->dropDownList($model, 'sq_feet', StmFormHelper::getSqFeetList(false), $htmlOptions = array(
						'empty' => '',
						'class' => 'sq-feet'
					)
				); ?>
                to <?php echo $form->dropDownList($model, 'sq_feet_max', StmFormHelper::getSqFeetList(false), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'sq-feet'
                    )
                ); ?>
			</div>
			<?php echo $form->error($model, 'price-min'); ?><?php echo $form->error($model, 'price_max'); ?><?php echo $form->error($model, 'sq_feet'); ?><?php echo $form->error($model, 'sq_feet_max'); ?>
		</section>
		<section>
			<?php echo $form->labelEx($model, 'bedrooms'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'bedrooms', StmFormHelper::getBedroomList(' '), $htmlOptions = array(
						'empty' => '',
						'class' => 'bedrooms'
					)
				); ?> to
                <?php echo $form->dropDownList($model, 'bedrooms_max', StmFormHelper::getBedroomList(' '), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'bedrooms'
                    )
                ); ?>
                <?php echo $form->labelEx($model, 'baths_total'); ?>
                <?php echo $form->dropDownList($model, 'baths_total', StmFormHelper::getBathList(' '), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'baths'
                    )
                ); ?>
                      to
                <?php echo $form->dropDownList($model, 'baths_total_max', StmFormHelper::getBathList(' '), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'baths'
                    )
                ); ?>
			</div>
			<?php echo $form->error($model, 'bedrooms'); ?>
		</section>


		<?php
		if(in_array("stories",$searchableFields)) { ?>
			<section>
				<?php echo $form->labelEx($model, 'stories'); ?>
				<div>
					<?php echo $form->textField($model, 'stories', $htmlOptions = array(
						'class' => 'g1',
						'style' => 'width:40px;',
					)
					);
					?>
				</div>
				<?php echo $form->error($model, 'stories'); ?>
			</section>
		<?php } ?>
		<?php
		if(in_array("school_district",$searchableFields)){ ?>
			<section>
				<?php echo $form->labelEx($model, 'school_district'); ?>
				<div>
					<?php echo $form->listBox($model, 'school_district', MlsProperties::getSchoolDistricts(), $htmlOptions = array(
						'empty' => '',
						'class' => 'county chzn-select',
						'data-placeholder' => ' ',
						'multiple' => 'multiple'
						)
					);
					?>
				</div>
				<?php echo $form->error($model, 'school_district'); ?>
			</section>
			<?php
		}
		?>

        <section>
            <?php echo $form->labelEx($model, 'year_built'); ?>
            <div>
                <?php echo $form->dropDownList($model, 'year_built', StmFormHelper::getYrBuiltList(' '), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'year-built'
                    )
                ); ?>
                to
                <?php echo $form->dropDownList($model, 'year_built_max', StmFormHelper::getYrBuiltList(' '), $htmlOptions = array(
                        'empty' => '',
                        'class' => 'year-built'
                    )
                ); ?>
                <?php echo $form->error($model, 'year_built_max'); ?>
            </div>
        </section>
		<section>
			<?php echo $form->labelEx($model, 'neighborhood'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'neighborhood', array(), $htmlOptions = array(
						'empty' => '',
						'class' => 'neighborhood g8 chzn-select',
						'style' => 'width:300px;',
						'data-placeholder' => 'Type Neighborhood Here...',
						'multiple' => 'multiple'
					)
				);
				?>
			</div>
			<?php echo $form->error($model, 'neighborhood'); ?>
		</section>

        <? if(in_array('car_garage', $searchableFields)): ?>
        <section>
            <?php echo $form->labelEx($model, 'car_garage'); ?>
            <div>
                <?php echo $form->dropDownList($model, 'car_garage', array(
                    1 => '1 Car Garage',
                    2 => '2 Car Garage',
                    3 => '3 Car Garage',
                    4 => '4 Car Garage',
                    5 => '5+ Car Garage'
                ), $htmlOptions = array(
                    'empty' => '',
                    'class' => 'car_garage g8',
                )
                );
                ?>
            </div>
            <?php echo $form->error($model, 'neighborhood'); ?>
        </section>
        <?php endif;?>

        <section>
            <?php echo $form->labelEx($model, 'school'); ?>
            <div>
                <?php echo $form->textField($model, 'school', $htmlOptions = array(
                        'class' => 'g8',
                        'style' => 'width:450px;',
                    )
                );
                ?>
            </div>
            <?php echo $form->error($model, 'school'); ?>
        </section>
    <? if(in_array('waterfront_yn', $searchableFields) || in_array('acreage', $searchableFields)): ?>
        <section>

            <label><? if(in_array('waterfront_yn', $searchableFields)): ?> Waterfront <? endif;?></label>
			<div>
                <? if(in_array('waterfront_yn', $searchableFields)): ?>
				<?php echo $form->dropDownList($model, 'waterfront_yn', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
						'empty' => '',
						'class' => 'waterfront'
					)
				); ?>
                <?php echo $form->error($model, 'waterfront_yn'); ?><?php echo $form->error($model, 'pool'); ?>
                <?endif;?>
                <? if(in_array('pool', $searchableFields)): ?>
                <?php echo $form->labelEx($model, 'pool'); ?>
				<?php echo $form->dropDownList($model, 'pool', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
						'empty' => '',
						'class' => 'pool'
					)
				); ?>
                <?endif;?>
                <? if(in_array('acreage', $searchableFields)): ?>
                <?php echo $form->labelEx($model, 'acreage_min'); ?>
                <?php
                    $acreageDropDownList = StmFormHelper::getAcreageList();
                    echo $form->dropDownList($model, 'acreage_min', $acreageDropDownList, $htmlOptions = array(
                        'empty' => '',
                        'class' => 'pool'
                    )
                ); ?>
                to
                <?php echo $form->dropDownList($model, 'acreage_max', $acreageDropDownList, $htmlOptions = array(
                        'empty' => '',
                        'class' => 'pool'
                    )
                ); ?>
                <?php endif;?>
			</div>
		</section>
        <?php endif;?>
		<section>
			<?php echo $form->labelEx($model, 'foreclosure'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'foreclosure', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
						'empty' => '',
						'class' => 'foreclosure'
					)
				); ?><label>Short Sales:</label>
				<?php echo $form->dropDownList($model, 'short_sale', StmFormHelper::getRequiredExcludeBooleanList(), $htmlOptions = array(
						'empty' => '',
						'class' => 'short_sale'
					)
				); ?>

                <? if(in_array('masterbr_room_level', $searchableFields)): ?>

                    <label>Master BR Level:</label>
                    <? echo $form->dropDownList($model, 'master_bedroom_level', array(1=>'1', 2=>'2', 3=>'3'), $htmlOptions = array(
                            'empty' => '',
                            'class' => 'short_sale'
                        )
                    );

                endif; ?>

			</div>
			<?php echo $form->error($model, 'foreclosure'); ?><?php echo $form->error($model, 'short_sale'); ?>
		</section>
        <? if(YII_DEBUG || in_array('lot_description', $searchableFields)): ?>
        <section>
            <?php echo $form->labelEx($model, 'lot_description'); ?>
            <div>
                <?php echo $form->textField($model, 'lot_description', $htmlOptions = array(
                        'class' => 'g8',
                        'style' => 'width:450px;',
                    )
                );
                ?>
            </div>
            <?php echo $form->error($model, 'lot_description'); ?>
        </section>
        <?php endif;?>
	</fieldset>
<?php echo $form->hiddenField($model, 'contact_id'); ?>
<?php echo $form->hiddenField($model, 'component_type_id'); ?>
<?php echo $form->hiddenField($model, 'component_id'); ?>
<?php echo $form->hiddenField($model, 'id'); ?>
	<div style="text-align: center; padding: 15px; font-size:12px;">
		<input id="home-search-dialog-submit-button" class="wide button" style="font-size:12px;" type="submit" value="Update Home Search">
	</div>
	<div class="loading-container"><em></em></div>
<?php
	$this->endWidget(); //CActiveForm
	$this->endWidget('zii.widgets.jui.CJuiDialog');