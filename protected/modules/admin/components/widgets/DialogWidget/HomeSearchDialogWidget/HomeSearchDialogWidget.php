<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class HomeSearchDialogWidget extends DialogWidget
{
    public $parentModel;
	public $contactId;

	public function run()
	{
		// Can't run without a way to call the dialog
		if (!$this->triggerElement)
			return false;

		$model = new SavedHomeSearchForm;
		$model->frequency = SavedHomeSearches::FREQUENCY_DAILY_ID;
		$model->contact_id = $this->contactId;
        $model->component_type_id = $this->parentModel->componentType->id;
        $model->component_id = $this->parentModel->id;

        $model->agent_id = ($leadPoolContactId = Yii::app()->user->settings->lead_pool_contact_id)? $leadPoolContactId : Yii::app()->user->id;
        $searchableFields = MlsFieldMaps::getSearchableColumns(Yii::app()->user->board->id);

        $hasMultipleBoards = Yii::app()->user->hasMultipleBoards();
        if(!$hasMultipleBoards) {
            $model->mls_board_id = Yii::app()->user->board->id;
        }

        $this->render('homeSearchDialog', array(
			'model'          => $model,
			'title'          => $this->title,
			'triggerElement' => $this->triggerElement,
            'hasMultipleBoards' => $hasMultipleBoards,
            'searchableFields' => $searchableFields,
		));
	}

	/**
	 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
	 * @return null
	 */
	public function registerTriggerScript() {
		Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);
        $componentTypeId = $this->parentModel->componentType->id;
        $componentId = $this->parentModel->id;
        $defaultAgentId = ($leadPoolContactId = Yii::app()->user->settings->lead_pool_contact_id)? $leadPoolContactId : Yii::app()->user->id;

		$js = <<<JS
		$(document).ready(function () {
			$("#home-search-dialog-form select.neighborhood").ajaxChosen({
				type: 'POST',
				url: '/admin/savedHomeSearch/autocomplete',
				dataType: 'json'
			}, function (data) {
				var results = [];

				$.each(data, function (i, val) {
					results.push({ value: val.value, text: val.text });
				});

				return results;
			});
		});

		$('#SavedHomeSearchForm_frequency').change(function(){
            if($(this).val() == 1) {
                $('#days-of-week-container').hide('normal');
            } else if($(this).val() == 2) {
                $('#days-of-week-container').show('normal');
            }
		});

		$('$this->triggerElement').live('click', function() {
			var Dialog = $('#$this->id');
			var element = $(this);
			var savedHomeSearchId = element.attr('data');
			var contactId = element.attr('data-id');
			var DialogForm = Dialog.find('form#$this->formId');
			var DialogFormAction = DialogForm.attr('action');

			Dialog.find(':input:not(input[type=hidden],input[type=submit])').val('');  //clear all input execpt the hidden ones
            //Dialog.find('#home-search-dialog-form option[selected*="selected"]').removeAttr('selected'); //@todo: need to clear zips for new adds
			Dialog.find('select').val([]);

			Dialog.find('#SavedHomeSearchForm_agent_id').val($defaultAgentId);
			Dialog.find('#SavedHomeSearchForm_mls_board_id').val('');
			Dialog.find('#SavedHomeSearchForm_property_type').val(1);
			Dialog.find('#SavedHomeSearchForm_frequency').val(1);
			Dialog.find('#home-search-dialog-form select.neighborhood option').remove();
			Dialog.find('div.errorMessage').html('').hide();

			Dialog.find('#SavedHomeSearchForm_stories').val('');
            Dialog.find('#SavedHomeSearchForm_zip').val(null);
            Dialog.find('#SavedHomeSearchForm_school_district').val(null);
			Dialog.find('.chzn-select').trigger("liszt:updated");

            $('#days-of-week-container').hide();
            Dialog.find('#home-search-dialog-form input.day-of-week').attr('checked', false);

			// Reset the action on each triggering attempt for Add New Search
			DialogForm.attr('action', '/admin/savedHomeSearch/add/' + contactId);
			DialogForm.data('settings').validationUrl = DialogForm.attr('action');

			// If edit was clicked then we need to dynamically fill in the values for the popup
			if (element.hasClass('edit-saved-home-search-button')) {

				// Create the scenario specific text for Edit Saved Search
				$('span.ui-button-text:contains("Add Saved Search")').html('Update Home Search');
				$('span.ui-dialog-title:contains("Add New Saved Search")').html('Edit Saved Search');

				savedHomeSearchId = element.attr('data');
				DialogForm.attr('action', '/admin/savedHomeSearch/edit/' + savedHomeSearchId);
				DialogForm.data('settings').validationUrl = DialogForm.attr('action');

				$.getJSON('/admin/savedHomeSearch/' + savedHomeSearchId, function(data) {
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_id').val(data.id);
		        	$('#home-search-dialog-form select#SavedHomeSearchForm_mls_board_id').val(data.mls_board_id);
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_name').val(data.name);
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_contact_id').val(data.contact_id);
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_component_type_id').val(data.component_type_id);
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_component_id').val(data.component_id);
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_public_remarks').val(data.public_remarks);
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_school').val(data.school);
		        	$('#home-search-dialog-form input#SavedHomeSearchForm_lot_description').val(data.lot_description);
					$('#home-search-dialog-form input#SavedHomeSearchForm_stories').val(data.stories);

					$('#home-search-dialog-form select[name*="property_type"]>option[value="'+data.property_type+'"]').attr('selected', true);
					if(data.area)
						$.each( data.area, function( key, value ) {
							$('#home-search-dialog-form select[name*="area"]>option[value="'+value+'"]').attr('selected', true);
						});
					if(data.school_district)
						$.each( data.school_district, function( key, value ) {
							$('#home-search-dialog-form select[name*="school_district"]>option[value="'+value+'"]').attr('selected', true);
						});
					if(data.region)
						$.each( data.region, function( key, value ) {
							$('#home-search-dialog-form select[name*="region"]>option[value="'+value+'"]').attr('selected', true);
						});
					if(data.city)
						$.each( data.city, function( key, value ) {
                            if($('#home-search-dialog-form select[name*="city"]>option[value="'+value+'"]').val() != undefined) {
							    $('#home-search-dialog-form select[name*="city"]>option[value="'+value+'"]').attr('selected', true);
                            }
                            else {
                                // this accomodates any manually entered cities that may not be in dropdown box
                                $('#home-search-dialog-form select[name*="city"]').append($("<option></option>").attr("value",value).text(value).attr('selected', true));
                            }
						});
					if(data.zip)
						$.each( data.zip, function( key, value ) {
							$('#home-search-dialog-form select[name*="zip"]>option[value="'+value+'"]').attr('selected', true);
						});
					if(data.county)
						$.each( data.county, function( key, value ) {
							$('#home-search-dialog-form select[name*="county"]>option[value="'+value+'"]').attr('selected', true);
						});
					if(data.neighborhood)
						$.each( data.neighborhood, function( key, value ) {
							var list = $("#home-search-dialog-form select.neighborhood");
							list.append(new Option(value, value));

							$('#home-search-dialog-form select[name*="neighborhood"]>option[value="'+value+'"]').attr('selected', true);
						});
					Dialog.find('.chzn-select').trigger("liszt:updated");
					$('#home-search-dialog-form select[name*="mls_board_id"]>option[value="'+data.mls_board_id+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="agent_id"]>option[value="'+data.agent_id+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="frequency"]>option[value="'+data.frequency+'"]').attr('selected', true);

					$('#home-search-dialog-form select[name*="price_min"]>option[value="'+data.price_min+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="price_max"]>option[value="'+data.price_max+'"]').attr('selected', true);

					$('#home-search-dialog-form #SavedHomeSearchForm_bedrooms').val(data.bedrooms);
					$('#home-search-dialog-form #SavedHomeSearchForm_bedrooms_max').val(data.bedrooms_max);

					$('#home-search-dialog-form #SavedHomeSearchForm_baths_total').val(data.baths_total);
					$('#home-search-dialog-form #SavedHomeSearchForm_baths_total_max').val(data.baths_total_max);

					$('#home-search-dialog-form select#SavedHomeSearchForm_sq_feet').val(data.sq_feet);
					$('#home-search-dialog-form select#SavedHomeSearchForm_sq_feet_max').val(data.sq_feet_max);

					$('#home-search-dialog-form select#SavedHomeSearchForm_year_built').val(data.year_built);
					$('#home-search-dialog-form select[name*="year_built_max"]>option[value="'+data.year_built_max+'"]').attr('selected', true);

					$('#home-search-dialog-form select[name*="waterfront"]>option[value="'+data.waterfront+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="pool"]>option[value="'+data.pool+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="foreclosure"]>option[value="'+data.foreclosure+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="short_sale"]>option[value="'+data.short_sale+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="acreage_min"]>option[value="'+data.acreage_min+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="acreage_max"]>option[value="'+data.acreage_max+'"]').attr('selected', true);
					$('#home-search-dialog-form select[name*="master_bedroom_level"]>option[value="'+data.master_bedroom_level+'"]').attr('selected', true);


                    if($('#SavedHomeSearchForm_frequency').val() == 2) {
                        $('#days-of-week-container').show();

                        $('#home-search-dialog-form input[name*="sunday"]').attr('checked', data.sunday == '0' ? false : true);
                        $('#home-search-dialog-form input[name*="monday"]').attr('checked', data.monday == '0' ? false : true);
                        $('#home-search-dialog-form input[name*="tuesday"]').attr('checked', data.tuesday == '0' ? false : true);
                        $('#home-search-dialog-form input[name*="wednesday"]').attr('checked', data.wednesday == '0' ? false : true);
                        $('#home-search-dialog-form input[name*="thursday"]').attr('checked', data.thursday == '0' ? false : true);
                        $('#home-search-dialog-form input[name*="friday"]').attr('checked', data.friday == '0' ? false : true);
                        $('#home-search-dialog-form input[name*="saturday"]').attr('checked', data.saturday == '0' ? false : true);

                        $('#home-search-dialog-form input[name*="sunday"]').val(data.sunday);
                        $('#home-search-dialog-form input[name*="monday"]').val(data.monday);
                        $('#home-search-dialog-form input[name*="tuesday"]').val(data.tuesday);
                        $('#home-search-dialog-form input[name*="wednesday"]').val(data.wednesday);
                        $('#home-search-dialog-form input[name*="thursday"]').val(data.thursday);
                        $('#home-search-dialog-form input[name*="friday"]').val(data.friday);
                        $('#home-search-dialog-form input[name*="saturday"]').val(data.saturday);
                    }
				});
			} else {

                $('#home-search-dialog-form input[name*="component_type_id"]').val($componentTypeId);
                $('#home-search-dialog-form input[name*="component_id"]').val($componentId);
				// Revert the scenario specific text for Add New Search
				$('span.ui-button-text:contains("Update Home Search")').html('Add Saved Search');
				$('span.ui-dialog-title:contains("Edit Saved Search")').html('Add New Saved Search');
			}

            $('#home-search-dialog-form .day-of-week').click(function() {
                $(this).attr('value', $(this).is(':checked') ? '1' : '0');
            });

			Dialog.dialog('open');

			return false;
		});
JS;

		Yii::app()->clientScript->registerScript('popupTrigger-'.$this->id, $js);
	}
}