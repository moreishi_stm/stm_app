<style>
    #calllist-requeue-form {
        padding-top: 20px;
        padding-bottom: 20px;
    }
    #calllist-requeue-form input, #calllist-requeue-form select {
        font-size: 13px;
        font-family: arial,sans-serif;
    }
    #calllist-requeue-form select {
        height: 30px;
    }
    #calllist-requeue-form .requeue-action-button {
        padding: 15px;
        width: 70%;
        font-size: 20px;
        font-weight: bold;
        display: inline-block;
        -webkit-border-radius:6px;
        -moz-border-radius:6px;
        border-radius:6px;
    }

    #calllist-requeue-form .requeue-all-button {
        background-color: #deedff;
        border: 2px solid #cce5f9;
    }
    #calllist-requeue-form .requeue-all-button:hover {
        border-color: #98baf2;
        text-decoration: none;
    }

    #calllist-requeue-form .spot-requeue-button {
        background-color: #e3ffd4;
        border: 2px solid #d6efc7;
    }
    #calllist-requeue-form .spot-requeue-button:hover {
        border-color: #73d273;
        text-decoration: none;
    }
</style>

<?php
$module = Yii::app()->controller->module->name;
//Yii::app()->clientScript->registerScript('phoneDialogScript', <<<JS
//
//    $('.click-to-call-button.phone-action-button').click(function(){
//        $('#$dialogId').dialog('close');
//    });
//JS
//);

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'       => $this->id,
        'options'  => array(
        'title'    => $this->title,
        'autoOpen' => false,
        'modal'    => true,
        'width'    => 500,
        'height'   => 'auto',
        'beforeClose'   => 'js:function(event, ui) {
                                $("#phoneId").attr("value", "");
                            }',
	 ),
));

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'calllist-requeue-form',
		'action' => Yii::app()->createUrl("/".Yii::app()->controller->module->id."/dialer/requeue"),
		'enableAjaxValidation' => true,
        'enableClientValidation' => false,
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
            'beforeValidate' => 'js:function(form) {


                $("body").prepend("<div class=\"loading-container loading\" style=\"z-index: 1005 !important;\"><em></em></div>");
                return true;
            }',
            'afterValidate' => 'js:function(form, data, hasError) {
				if (!hasError) { // Action was successful

				    var message;

				    if(data.status=="success") {
				        if(data.message != undefined && data.message != "") {
				            message = data.message;
				        }
				        else {
                            message = "Successfully Re-Queued the Call List";
                        }
                        Message.create("success", message);

						if(document.getElementById("call-list-grid")) {
    						$.fn.yiiGridView.update("call-list-grid");
						}
				    }
				    else if(data.status=="error") {
				        if(data.message != undefined && data.message != "") {
				            message = data.message;
				        }
				        else {
                            message = "There was an error re-queueing.";
                        }

    					Message.create("error", message);
				    }
					$("#' . $this->id . '").dialog("close");
				}

                $("div.loading-container.loading").remove();

				return false;
			}',
		)
	));

	?>
	<h1 id="calllist-requeue-form-title" style="margin-bottom: 20px; color: #444">Call List Name</h1>
<!--    <div>Bad Number</div>-->
<!--    <div>Verified Number</div>-->

    <div class="p-tc p-mb10">
        <div id="phone-dialog-click-to-call-button" class="spot-requeue-button requeue-action-button" data-id=""  data-phone="">
            Spot Re-Queue<br>
<!--            <span style="font-size: 13px;">Based on Added Date</span><br>-->
            <span style="font-size: 13px; font-weight: normal">Add contacts to the top of the dialer <br>call list based on your input below.</span><br>
            <div>
                <span class="g3">Date Range: </span>
                <div class="g9 p-tl">
                <?
                echo CHtml::dropDownList('dateRangeType', null, array('added'=>'Added Date', 'last_login'=>'Last Login Date'/*, 'expired_date'=>'Expired Date'*/), $htmlOptions=array('class'=>'g11 p-mb5', 'style'=>'width: 100%;'));
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'name' => 'requeueDateFrom',
                        'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                            'placeholder' => 'From Date'
                        ),
                    )
                );
                echo '<span class="g1 p-tc"> - </span>';
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'name' => 'requeueDateTo',
                        'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                            'placeholder' => 'To Date'
                        ),
                    )
                );
                ?>
                </div>
            </div>
            <?
            //@todo: filter by # calls in the past X days
            ?>
            <div>
                <span class="g3">Order by: </span>
                <div class="g9">
                    <?
                    echo CHtml::dropDownList('orderBy', null, array('least_dialed'=>'# Dials 30 days: least first', 'login_desc'=>'Last Login: newest first', 'component_added_desc'=>'Added: newest first', 'component_added_asc'=>'Added: oldest first'/*, 'expired_date_desc'=>'Expired Date: newest first', 'expired_date_asc'=>'Expired Date: oldest first' */), $htmlOptions=array('class'=>'g9'));
                    echo CHtml::button('Go', $htmlOptions=array('style'=>'width: 50px; margin-left: 10px;','id'=>'spot-requeue-submit-button', 'class'=>'button'))
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="p-tc">
        <a href="javascript:void(0);" class="requeue-all-button requeue-action-button" data-id="">Re-Queue All</a>
    </div>
	<?php
        echo $form->hiddenField($model, 'id', $htmlOptions=array());
        echo $form->error($model, 'id');
        echo CHtml::hiddenField('requeueType', null, $htmlOptions=array());
	$this->endWidget();
    $this->endWidget('zii.widgets.jui.CJuiDialog');
