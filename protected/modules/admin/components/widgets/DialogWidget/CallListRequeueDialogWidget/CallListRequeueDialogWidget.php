<?
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class CallListRequeueDialogWidget extends DialogWidget
{
    const DIALOG_ID = 'call-list-requeue-dialog';

	public function run() {
		// Can't run without a way to call the dialog
        if (!$this->triggerElement)
            return false;

		$this->render('callListRequeueDialog', array(
                'model'=> new CallLists,
		));
	}

    public function registerTriggerScript() {

        $js = <<<JS
			$('$this->triggerElement').live('click', function() {
                var Dialog = $("#$this->id");
                Dialog.dialog('open');

                Dialog.find('div.errorMessage').html('').hide();

                Dialog.find('input, select, textarea, span').removeClass('error');
                Dialog.find('input').blur();

                $('#requeueDateFrom, #requeueDateTo').attr('value', '');

                $('#CallLists_id').attr('value', $(this).data('id'));
                $('#calllist-requeue-form-title').html($(this).data('name'));
//                $('#phone-dialog-click-to-call-button').attr('data-phone', $(this).data('phone'));
//                $('#clickToCallToPhoneNumber').attr('value', $(this).data('phone'));
//                $('#phoneOriginComponentTypeId').attr('value', $(this).data('ctid'));
//                $('#phoneOriginComponentId').attr('value', $(this).data('cid'));

    	        return false;
            });

			$('#spot-requeue-submit-button').live('click', function() {

			    if($('#requeueDateFrom').val()=='') {
			        alert('Please enter a From Date.');
			        return false;
			    }

			    if($('#requeueDateTo').val()=='') {
			        alert('Please enter a To Date.');
			        return false;
			    }

                if(confirm('Confirm Spot Re-queue.')) {
                    $('#requeueType').val('spot');
                    $('form#calllist-requeue-form').submit();
                }
            });

			$('.requeue-all-button').live('click', function() {

                if(confirm('Confirm Re-queueing the whole list.')) {
                    $('#requeueType').val('all');
                    $('form#calllist-requeue-form').submit();
                }
            });
JS;

        Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
    }
}
