<?php

Yii::app()->clientScript->registerScript('actionPlanDialogScriptJs', <<<JS

    // Bind event to open dialog`
    $('#add-action-plan-button').live('click', function(e) {

        // Stop default behavior
        e.preventDefault();

        // Create modal
        stmModal.create({
            title: 'Action Plans',
            content: $('#template-dialog-form-action-plan').html(),
            height: 370,
            width: 650,
            successCallback: function() {
                if({$reloadAfterValidate}){
                    location.reload();
                }
                else{
                    $("div.loading-container.loading").remove();
                }
            }
        });

        initActionPlanDialog($(this));
    });
JS
);

//	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
//			'id' => $this->id,
//			'options' => array(
//				'title' => $title,
//				'autoOpen' => false,
//				'modal' => true,
//				'width' => 600,
//				'height' => auto,
//				'buttons' => array(
//					'Apply Action Plan' => "js:function() { $('#action-plan-dialog-form').submit(); }",
//				),
//			),
//		)
//	);
echo '<script id="template-dialog-form-action-plan" type="text/x-handlebars-template">';
	$form = $this->beginWidget('StmActiveForm', array(
			'id' => 'action-plan-dialog-form',
			'enableAjaxValidation' => true,
			'action' => array('/'.Yii::app()->controller->module->id.'/actionPlans/apply'),
            'htmlOptions'   =>  array(
                'class' =>  'form-horizontal'
            ),
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'beforeValidate' => 'js:function(form) {
	            	$("body").prepend("<div class=\'loading-container loading\'><em></em></div>");
                    return true;
                }',
				'afterValidate' => 'js:function(form, data, hasError) {
					if(data.length !== undefined || hasError) {
                        $("div.loading-container.loading").remove();
					    return false;
					}

					if (!hasError) { // Action was successful

					    if(data.status == "success") {
                            $("#' . $this->id . '").dialog("close");
                            Message.create("success", "Action Plan Successfully Applied!");
                            if('.$reloadAfterValidate.'){
                                location.reload();
                            }
                            else{
                                $("div.loading-container.loading").remove();
                            }
					    }
					    else {
                            Message.create("error", "Action Plan did not apply. " +  data.message);
                            $("div.loading-container.loading").remove();
					    }
						return false;
					}
				}',
			),
		)
	);
?>
<div class="stm-modal-body">
    <div class="form-group">
        <div style="width:98%; text-align: center;">Please select an Action Plan below:</div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Action Plan:</label>

        <div class="col-sm-10">
            <?php
            $ActionPlans = array();
            if($parentModel) {

                $omissions = array();
                $AppliedActionPlans = ActionPlans::getAppliedPlans($componentTypeId, $parentModel->id)->getData();
                $hasAppliedActionPlans = !empty($AppliedActionPlans);

                if ($hasAppliedActionPlans) {
                    foreach ($AppliedActionPlans as $ActionPlan) {
                        if(!$ActionPlan->getIsCompleted($parentModel->id)) {
                            $omissions[] = $ActionPlan->id;
                        }
                    }
                }
            }

            $AvailableActionPlans = ActionPlans::model()->byComponentType($componentTypeId, $omissions)->byStatus(StmFormHelper::ACTIVE)->orderByName()->findAll();
            $ActionPlans = CHtml::listData($AvailableActionPlans, 'id', 'name');

            echo $form->dropDownList($model, 'id', $ActionPlans, $htmlOptions = array(
                'empty' => 'Select One',
                    'class' => 'form-control',
                    )
                );
            echo $form->error($model, 'id');

            ?>
        </div>
        <?php echo $form->hiddenField($model, 'applicabilityCheck'); ?>
        <?php echo $form->error($model, 'applicabilityCheck'); ?>
    </div>
    <?php
        if ($hasAppliedActionPlans):
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Applied Plans:</label>
                <div class="col-sm-10">

                <?php
                    if($parentModel) {

                        foreach ($AppliedActionPlans as $ActionPlan) {
                            echo '<div>' .(($ActionPlan->getIsCompleted($parentModel->id))? '<strong>[COMPLETED]</strong> - ':''). $ActionPlan->name .'</div>';
                        }
                    }
                ?>
                </div>
            </div>
        <?php
        endif;
    ?>
</div>

<div class="stm-modal-footer">
    <button type='submit'>Apply Action Plan</button>
</div>
<?php
    echo $form->hiddenField($model, 'component_type_id');
    echo $form->hiddenField($model, 'componentId');
?>
<?$this->endWidget();?>
<?='</script>'?>
