<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class ActionPlanDialogWidget extends DialogWidget
{
    public $parentModel;
    public $componentTypeId;

    /**
     * Init
     * Overloaded the parent version to remove registerTrigger Script and have it called in the run method. Properties were not set in the init and the script was breaking.
     * @return null|void
     */
    public function init() {
        if ($this->id && $this->triggerElement) {
            if ($this->formId === null) {
                $this->formId = $this->id . '-form';
            }
        }
    }

    public function run()
    {
        // Can't run without a way to call the dialog
        if (!$this->triggerElement) {
            return false;
        }

        $ActionPlan = new ActionPlans('search');
        $ActionPlan->component_type_id = $this->parentModel->componentType->id;
        $ActionPlan->componentId = $this->parentModel->id;

        if (isset($_POST['ActionPlans'])) {
            $ActionPlan->attributes = $_POST['ActionPlans'];

            $ActionPlan = $ActionPlan->findByPk($ActionPlan->id);
        }

        $this->registerTriggerScript();

        $this->render('actionPlanDialog', array(
                'parentModel' => $this->parentModel,
                'componentTypeId' => ($this->parentModel) ? $this->parentModel->componentType->id : $this->componentTypeId,
                'model' => $ActionPlan,
                'title' => $this->title,
                'triggerElement' => $this->triggerElement,
                'reloadAfterValidate' => ($this->parentModel) ? 1 : 0,
            )
        );
    }

    public function registerTriggerScript()
    {
        $module = Yii::app()->controller->module->name;
        $js = <<<JS

//	    $("$this->triggerElement").live(clickTouchEvent, function() {
        var initActionPlanDialog = function(ele) {

            //@todo: can make even more dynamic if pull action plans by ctid attribute and populate it - may not need right now.
            // if there is an element attribute, clear current data and populate new info
//	        if($(this).attr('cntid') != undefined && $(this).attr('cntid') != "") {
//
//                contactId = $(this).attr('cntid');
//
//                // clear out email address field
//                $('#EmailMessage_to_email_id').html('').trigger('liszt:updated');
//
//                // dynamically populate
//                $.post('/$module/contacts/getEmails/' + contactId , function(data) {
//
//                    if(data.status == 'success') {
//                        var emailListOptions = '';
//                        $.each( data.emails, function( key, value ) {
//                            emailListOptions += '<option value="' + key + '">' + value + '</option>';
//                        });
//                        $('#EmailMessage_to_email_id').html(emailListOptions).trigger('liszt:updated');
//                    } else {
//                        Message.create("error","Could not retrieve this contacts emails addresses.");
//                    }
//
//                },"json");
//	        }

	        if(ele.attr('ctid') != undefined && ele.attr('ctid') != "") { console.log(1);
                componentTypeId = ele.attr('ctid');
                $('#ActionPlans_component_type_id').val(componentTypeId);
	        }

	        if(ele.attr('cid') != undefined && ele.attr('cid') != "") { console.log(2);
                componentId = ele.attr('cid');
                $('#ActionPlans_componentId').val(componentId);
	        }

//	    	var Dialog = $("#$this->id");
//	        Dialog.dialog('open');
//
//	        Dialog.find('div.errorMessage').html('').hide();
//
//	        Dialog.find('input, select, textarea, span').removeClass('error');
//	        Dialog.find('input').blur();

	        return false;
	    };

JS;
        Yii::app()->clientScript->registerScript('action-plan-dialog-TriggerScript-' . $this->id, $js);    }
}
