<?php
$css = <<<CSS
    #dialer-list-form fieldset > section > div {
        width: 74%;
    }
    #dialer-list-form fieldset > section label {
        width: 20%;
    }
CSS;
Yii::app()->clientScript->registerCss('dialerListDialogCss', $css);

Yii::app()->clientScript->registerScript('addToDialerScript2', <<<JS

	// Open modal on click
    $("$this->triggerElement").live('click', function(e) {

		e.preventDefault();

		// Create modal
		stmModal.create({
			title: 'Add to Dialer',
			contentVariables: {
				title: 'Add to Dialer'
			},
			content: $('#template-dialog-add-to-dialer').html(),
			height: 240
		});

		initDialerListDialog($(this));
    });
JS
);

echo '<script id="template-dialog-add-to-dialer" type="text/x-handlebars-template">';

$form = $this->beginWidget('StmActiveForm', array(
		'id' => 'dialer-list-form',
		'enableAjaxValidation' => false,
		'action' => array('/'.Yii::app()->controller->module->id.'/contacts/addToDialer'),
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => false,
		),
	)
);
?>
	<fieldset>
        <section>
            <label>Dialer List:</label>
            <div>
                <select name="callListId" id="DialerListDialog_callListId" class="g11">
                </select>
                <div id="call-list-error" class="errorMessage" style="display: none;">Please select a Dialer Call List.</div>
                <?=CHtml::hiddenField('contactId', $this->contactId, $htmlOptions=array('id'=>'DialerListDialog_contactId')); ?>
                <?=CHtml::hiddenField('phoneId', $this->contactId, $htmlOptions=array('id'=>'DialerListDialog_phoneId')); ?>
                <?=CHtml::hiddenField('componentTypeId', $this->componentTypeId, $htmlOptions=array('id'=>'DialerListDialog_componentTypeId')); ?>
                <?=CHtml::hiddenField('componentId', $this->componentId, $htmlOptions=array('id'=>'DialerListDialog_componentId')); ?>
            </div>
        </section>
        <section>
            <label></label>
            <div>
                Note: Lead must exit for the respective Dialer List type.<br>
                Example: To add to a seller list, a seller lead must exist.
            </div>
        </section>
        <div style="text-align: center; padding-top: 10px; clear:both;">
            <button type="submit">Add to Dialer</button>
        </div>
	</fieldset>

	<div class="loading-container"><em></em></div>
<?$this->endWidget();?>
<?='</script>'?>
