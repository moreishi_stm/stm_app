<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class DialerListDialogWidget extends DialogWidget
{
    const DIALOG_ID = 'dialer-list-dialog';
    public $contactId;
    public $componentTypeId;
    public $componentId;
    public $postAddFunction;

    public function run()
    {
        // Can't run without a way to call the dialog
        if (!$this->triggerElement) {
            return false;
        }

        $this->render('dialerListDialog', array(
                'title' => $this->title,
                'triggerElement' => $this->triggerElement,
            )
        );
    }

    public function registerTriggerScript()
    {
        $dialogId = $this->id;
        $module = Yii::app()->controller->module->name;

        $callListsData = Yii::app()->db->createCommand()
            ->select('id, name, preset_ma')
            ->from('call_lists')
            ->where(array('in', 'preset_ma', array(CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_BUYERS, CallLists::CUSTOM_RECRUITS, CallLists::CUSTOM_CONTACTS)))
            ->andWhere('is_deleted=0')
            ->order('name ASC')
            ->queryAll();

        $callListsGroupData = array();
        foreach($callListsData as $callListData) {
            switch($callListData['preset_ma']) {
                case CallLists::CUSTOM_SELLERS:
                    $componentTypeId=ComponentTypes::SELLERS;
                    break;

                case CallLists::CUSTOM_BUYERS:
                    $componentTypeId=ComponentTypes::BUYERS;
                    break;

                case CallLists::CUSTOM_RECRUITS:
                    $componentTypeId=ComponentTypes::RECRUITS;
                    break;

                case CallLists::CUSTOM_CONTACTS:
                    $componentTypeId=ComponentTypes::CONTACTS;
                    break;

                default:
                    throw new Exception(__CLASS__.' (:'.__LINE__.') Invalid Component Type.');
                    break;
            }
            $callListName = str_replace(array("'",'"'), '', $callListData['name']);
            // build drop down string for each component type
            $callListsGroupData[$componentTypeId] .= '<option value="'.$callListData['id'].'">'.$callListName.'</option>'; //array('id'=>$callListData['id'], 'name' => $callListData['name']);
        }

        // need to double escape for quotes for js
        $callListsJson = str_replace('\"','\\\"', CJSON::encode($callListsGroupData));
        $postAddFunction = $this->postAddFunction;
        $js = <<<JS
        var callListDropDownLists = jQuery.parseJSON('$callListsJson');

        var initDialerListDialog = function (ele) {
	    	var Dialog = $("#$dialogId");

            $('#DialerListDialog_componentTypeId').val(ele.data('ctid'));
            $('#DialerListDialog_componentId').val(ele.data('cid'));
            $('#DialerListDialog_phoneId').val(ele.data('pid'));
            $('#DialerListDialog_contactId').val(ele.data('contactid'));

	        // clear out all dialog form fields
	        Dialog.find('div.errorMessage').html('').hide();
	        Dialog.find('input, select, textarea, span').removeClass('error');
	        Dialog.find('input').blur();

            $('select#DialerListDialog_callListId').html(callListDropDownLists[ele.data('ctid')]);
        }

        $("#dialer-list-form").live('submit',function() {

			$("body").prepend("<div class='loading-container loading'><em></em></div>");
            var callListId = $('#callListId').val();
            var callListError = $('#call-list-error');
            $('#callListId').removeClass('error');
            callListError.html();
            callListError.hide();

            if(callListId == '') {
                $('#callListId').addClass('error');
                $("div.loading-container.loading").remove();
                callListError.html('Please select a Dialer Call List');
                callListError.show();
            }
            else {

                $.post('/$module/contacts/addToDialerList/', $(this).serialize(), function(data) {

                    if(data.status=='success') {
                        var message;
                        if(data.message != undefined || data.message == '') {
                            message = data.message;
                        }
                        else {
                            message = "Added to Dialer List successfully.";
                        }
                        Message.create("success", message);
                        $("#$dialogId").dialog("close");

                        if('$this->postAddFunction' != '') {
                            $this->postAddFunction(data);
                        }

                        // update activity log grid if exists
                        if (document.getElementById("activity-log-grid")) {
                            $.fn.yiiGridView.update("activity-log-grid");
                        }

                        setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);
                    } else {
                        Message.create("error", data.message);
                        $("div.loading-container.loading").remove();
                    }

                },"json");
            }

	        return false;
	    });

	    $("$this->triggerElement").live('click', function() {
	    	var Dialog = $("#$dialogId");

            $('#DialerListDialog_componentTypeId').val($(this).data('ctid'));
            $('#DialerListDialog_componentId').val($(this).data('cid'));
            $('#DialerListDialog_phoneId').val($(this).data('pid'));
            $('#DialerListDialog_contactId').val($(this).data('contactid'));

	        // clear out all dialog form fields
	        Dialog.find('div.errorMessage').html('').hide();
	        Dialog.find('input, select, textarea, span').removeClass('error');
	        Dialog.find('input').blur();

            $('select#DialerListDialog_callListId').html(callListDropDownLists[$(this).data('ctid')]);

	        Dialog.dialog('open');

	        return false;
	    });
JS;

        Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
    }
}
