<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class ClickToCallDialogWidget extends DialogWidget
{
    const CLICK_TO_CALL_DIALOG_ID = 'click-to-call-dialog';

    /**
     * Response
     *
     * Response object for Plivo
     * @var \StmPlivoFunctions\Response
     */
    protected $_response;

    public function run()
    {
        // Can't run without a way to call the dialog
        if (!$this->triggerElement) {
            return false;
        }

        $clickToCallSettingCallInitialNumber = (Yii::app()->user->settings->click_to_call_initial_number) ? Yii::app()->user->settings->click_to_call_initial_number : Yii::app()->user->settings->cell_number;
        $clickToCallSettingCallerIdName = (Yii::app()->user->settings->click_to_call_caller_id_name) ? Yii::app()->user->settings->click_to_call_caller_id_name : Yii::app()->user->settings->cell_number;
        $clickToCallSettingCallerIdNumber = (Yii::app()->user->settings->click_to_call_caller_id_number) ? Yii::app()->user->settings->click_to_call_caller_id_number : Yii::app()->user->settings->cell_number;
        $this->render('clickToCallDialog', array(
                'title' => $this->title,
                'triggerElement' => $this->triggerElement,
                'clickToCallSettingCallInitialNumber' => $clickToCallSettingCallInitialNumber,
                'clickToCallSettingCallerIdName' => $clickToCallSettingCallerIdName,
                'clickToCallSettingCallerIdNumber' => $clickToCallSettingCallerIdNumber,
            )
        );
    }

    public function registerTriggerScript()
    {
        $dialogId = $this->id;
        $module = Yii::app()->controller->module->name;
        $clickToCallSettingPopup = (Yii::app()->user->settings->click_to_call_popup) ? 1 : 0;
        $clickToCallSettingCallerIdName = Yii::app()->user->settings->click_to_call_caller_id_name;
        $clickToCallSettingCallerIdNumber = Yii::app()->user->settings->click_to_call_caller_id_number;
        $clickToCallSettingCallInitialNumber = Yii::app()->user->settings->click_to_call_initial_number;
        $smsVerified = Yii::app()->user->settings->sms_verified;
        $cellNumber = Yii::app()->user->settings->cell_number;

        if($clickToCallSettingPopup && $smsVerified && $cellNumber  && $cellNumber == $clickToCallSettingCallInitialNumber && trim($clickToCallSettingCallerIdName) && trim($clickToCallSettingCallerIdNumber) && trim($clickToCallSettingCallInitialNumber)) {
            $showPopup = 0;
        }
        else {
            $showPopup = 1;
        }

        $js = <<<JS
        //$("#click-to-call-form").submit(function() {
        //
			//$("body").prepend("<div class='loading-container loading'><em></em></div>");
         //   var clickToCallInitialPhone = $('#clickToCallInitialPhone').val();
         //   var clickToCallCallerIdName = $('#clickToCallCallerIdName').val();
         //   var clickToCallCallerIdNumber = $('#clickToCallCallerIdNumber').val();
         //   var clickToCallToPhoneNumber = $('#clickToCallToPhoneNumber').val();
        //
         //   $('#clickToCallInitialPhone').removeClass('error');
         //   $('#clickToCallCallerIdName').removeClass('error');
         //   $('#clickToCallCallerIdNumber').removeClass('error');
        //
         //   if(clickToCallInitialPhone == '') {
         //       Message.create("error","Please enter your phone #.");
         //       $('#clickToCallInitialPhone').addClass('error');
         //       $("div.loading-container.loading").remove();
         //   }
         //   else if(clickToCallCallerIdName == '') {
         //       Message.create("error","Please enter your phone #.");
         //       $('#clickToCallCallerIdName').addClass('error');
         //       $("div.loading-container.loading").remove();
         //   }
         //   else if(clickToCallCallerIdNumber == '') {
         //       Message.create("error","Please enter your phone #.");
         //       $('#clickToCallCallerIdNumber').addClass('error');
         //       $("div.loading-container.loading").remove();
         //   }
         //   else {
        //
         //       $.post('/admin/clickToCall/call/', {
         //           clickToCallInitialPhone : clickToCallInitialPhone,
         //           clickToCallCallerIdName : clickToCallCallerIdName,
         //           clickToCallCallerIdNumber : clickToCallCallerIdNumber,
         //           clickToCallToPhoneNumber : clickToCallToPhoneNumber
         //       }, function(data) {
        //
         //           if(data.status=='success') {
         //               Message.create("success","Call Connect started successfully.");
         //               Message.create("success","Please log your call details.");
         //               stmModal.destroy();
         //               $("div.loading-container.loading").remove();
         //               //setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1000);
         //           } else {
         //               Message.create("error", data.message);
         //               $("div.loading-container.loading").remove();
         //           }
        //
         //       },"json");
         //   }
        //
	    //    return false;
	    //});

//	    $("$this->triggerElement").live('click', function() {
//
//            console.log(this);
//            console.log($(this).attr('data-phone'));
//
//	    	var Dialog = $(".stm-modal form");
//
//	        // clear out all dialog form fields
//	        Dialog.find('div.errorMessage').html('').hide();
//	        Dialog.find('input, select, textarea, span').removeClass('error');
//	        Dialog.find('input').blur();
//	        $('#click-to-call-number-label').html($(this).attr('data-phone'));
//	        $('#clickToCallToPhoneNumber').val($(this).attr('data-phone'));
//
////            if('$showPopup' == '1') {
////	            Dialog.dialog('open');
////            }
////            else {
////                Message.create('success', 'Calling ' + $(this).attr('data-phone') + '...');
////                $("#click-to-call-form").submit();
////            }
//
//	        return false;
//	    });
JS;

        Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
    }
}
