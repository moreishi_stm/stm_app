<?php
$css = <<<CSS
    form#click-to-call-form fieldset > section > div {
        width: 58%;
    }
    form#click-to-call-form fieldset > section label {
        width: 36%;
    }
    #click-to-call-number-label {
        font-size: 22px;
        font-weight: bold;
        color: black;
    }
    form#click-to-call-form input {
        font-size: 20px;
    }
CSS;
Yii::app()->clientScript->registerCss('clickToCallCss', $css);
$userId = Yii::app()->user->id;
$phoneTaskTypeId = TaskTypes::PHONE;
Yii::app()->clientScript->registerScript('clickToCallDialogScript', <<<JS

//    $('.click-to-call-button.phone-action-button').click(function(){
//        $('#$dialogId').dialog('close');
//    });

    $('.click-to-call-button').live('click', function(e) {

            // Stop default click event
            e.preventDefault();

            // Get component and component type IDs
            var componentTypeId = $(this).data('ctid');
            if(componentTypeId == undefined && $(this).attr('ctid') != undefined) {
                componentTypeId = $(this).attr('ctid');
            }
            if(componentTypeId == undefined && $(this).attr('data-id') != undefined) {
                componentTypeId = $(this).attr('data-id');
            }
            var componentId = $(this).data('cid');

            // Create modal
            stmModal.create({
                title: 'Click to Call',
                contentVariables: {
                	title: 'Click to Call'
                },
                content: $('#template-dialog-click-to-call').html(),
                height: 370,
                successCallback: function(r) {
                    //location.reload();
                    $("body").prepend("<div class='loading-container loading'><em></em></div>");
                    Message.create('success', r.message);
                },
                beforeSubmit: function() {

                    $("body").prepend("<div class='loading-container loading'><em></em></div>");
                    var clickToCallInitialPhone = $('#clickToCallInitialPhone').val();
                    var clickToCallCallerIdName = $('#clickToCallCallerIdName').val();
                    var clickToCallCallerIdNumber = $('#clickToCallCallerIdNumber').val();
                    var clickToCallToPhoneNumber = $('#clickToCallToPhoneNumber').val();

                    $('#clickToCallInitialPhone').removeClass('error');
                    $('#clickToCallCallerIdName').removeClass('error');
                    $('#clickToCallCallerIdNumber').removeClass('error');

                    if(clickToCallInitialPhone == '') {
                        Message.create("error","Please enter your phone #.");
                        $('#clickToCallInitialPhone').addClass('error');
                        $("div.loading-container.loading").remove();
                    }
                    else if(clickToCallCallerIdName == '') {
                        Message.create("error","Please enter your phone #.");
                        $('#clickToCallCallerIdName').addClass('error');
                        $("div.loading-container.loading").remove();
                    }
                    else if(clickToCallCallerIdNumber == '') {
                        Message.create("error","Please enter your phone #.");
                        $('#clickToCallCallerIdNumber').addClass('error');
                        $("div.loading-container.loading").remove();
                    }
                    else {

                        $.post('/admin/clickToCall/call/', {
                            clickToCallInitialPhone : clickToCallInitialPhone,
                            clickToCallCallerIdName : clickToCallCallerIdName,
                            clickToCallCallerIdNumber : clickToCallCallerIdNumber,
                            clickToCallToPhoneNumber : clickToCallToPhoneNumber,
                            componentTypeId : componentTypeId,
                            componentId : componentId,
                            userId : {$userId}
                        }, function(data) {

                            if(data.status=='success') {
                                Message.create("success","Call Connect started successfully.");
                                Message.create("success","Please log your call details.");
                                stmModal.destroy();
                                $("div.loading-container.loading").remove();

                                var ele = $('<span>', { 'ctid': componentTypeId, 'data' : componentId, 'data-ttid':  {$phoneTaskTypeId}});

                                // have to manually do data as it does not get set, prolly cuz a protected name
                                ele.attr('data', componentId);

                                // Popup the activity log dialog
                                doAddActivityDialog(ele);

                                //setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1000);
                            } else {
                                Message.create("error", data.message);
                                $("div.loading-container.loading").remove();
                            }

                        },"json");
                    }

                    return false;
                },
                contentVariables: {
                    componentId: componentId,
                    componentTypeId: componentTypeId
                }
            });

            var Dialog = $(".stm-modal form");

	        // clear out all dialog form fields
	        Dialog.find('div.errorMessage').html('').hide();
	        Dialog.find('input, select, textarea, span').removeClass('error');
	        Dialog.find('input').blur();
	        $('#click-to-call-number-label').html($(this).attr('data-phone'));
	        $('#clickToCallToPhoneNumber').val($(this).attr('data-phone'));

		//initPhoneDialog($(this));
    });
JS
);

    echo '<script id="template-dialog-click-to-call" type="text/x-handlebars-template">';
	$form = $this->beginWidget('StmActiveForm', array(
			'id' => 'click-to-call-form',
			'enableAjaxValidation' => false,
			'action' => array('/'.Yii::app()->controller->module->id.'/clickToCall/call'),
            'htmlOptions'   =>  array(
                'class' =>  'form-horizontal'
            ),
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => false,
			),
		)
	);
?>

<div class="stm-modal-body">
    <div class="form-group">
        <label class="control-label col-sm-2">Click to Call Number:</label>
        <div class="col-sm-10" id="click-to-call-number-label">
            () -
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Your Phone Number to Call:</label>
        <div class="col-sm-10">
            <?php $this->widget('StmMaskedTextField', array(
                'name' => 'clickToCallInitialPhone',
                'mask' => '(999) 999-9999',
                'id' => 'clickToCallInitialPhone',
                'value' => $clickToCallSettingCallInitialNumber,
                'htmlOptions' => array('placeholder' => 'Your Phone Number', 'class' => 'form-control')
            )); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Caller ID Name:</label>
        <div class="col-sm-10">
            <?=CHtml::textField('clickToCallCallerIdName', $clickToCallSettingCallerIdName, $htmlOptions = array('placeholder' => 'Caller ID Name', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Caller ID Number:</label>
        <div class="col-sm-10">
            <?php $this->widget('StmMaskedTextField', array(
                'name' => 'clickToCallCallerIdNumber',
                'mask' => '(999) 999-9999',
                'id' => 'clickToCallCallerIdNumber',
                'value' => $clickToCallSettingCallerIdNumber,
                'htmlOptions' => array('placeholder' => 'Caller ID Number', 'class' => 'form-control')
            )); ?>
            <?=CHtml::hiddenField('clickToCallToPhoneNumber', null); ?>
        </div>
    </div>
</div>
<div class="stm-modal-footer">
    <input type="hidden" name="componentTypeId" value="{{componentTypeId}}">
    <input type="hidden" name="componentId" value="{{componentId}}">
    <button type='submit' ctid="{{componentTypeId}}" data="{{componentId}}" class="">Call Now</button>
</div>

<div class="loading-container"><em></em></div>
<?$this->endWidget();?>
<?='</script>'?>
