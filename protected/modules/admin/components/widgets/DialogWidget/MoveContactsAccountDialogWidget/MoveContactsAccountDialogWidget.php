<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class MoveContactsAccountDialogWidget extends DialogWidget
{
    public $parentModel;

    public function run() {
        // Can't run without a way to call the dialog
        if (!$this->triggerElement) {
            return false;
        }


        $this->render('moveContactsAccount',array(
            'model' => new Accounts(),
            'parentModel' => $this->parentModel,
            'title' => $this->title,
            'triggerElement' => $this->triggerElement,
        ));
    }

    public function registerTriggerScript()
    {
        $js = <<<JS
			$('$this->triggerElement').live('click', function() {
                var Dialog = $("#$this->id");
                Dialog.dialog('open');

                Dialog.find('div.errorMessage').html('').hide();

                Dialog.find('input, select, textarea, span').removeClass('error');
                Dialog.find('input').blur();

                Dialog.find('#contactId').val($(this).data('contact-id'));
    	        return false;
            });
JS;

        Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);

    }
}
