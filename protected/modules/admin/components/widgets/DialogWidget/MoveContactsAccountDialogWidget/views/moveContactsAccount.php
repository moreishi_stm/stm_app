<?php

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => $this->id,
        'options' => array(
            'title' => $title,
            'autoOpen' => false,
            'modal' => true,
            'width' => 500,
            'height' => auto,
            'buttons' => array(
                'Move To Account' => "js:function() { $('#move-contact-to-account-form').submit(); }",
            ),
        ),
    )
);

$form = $this->beginWidget('StmActiveForm', array(
        'id' => 'move-contact-to-account-form',
        'enableAjaxValidation' => true,
        'action' => array('/'.Yii::app()->controller->module->id.'/contacts/moveContact'),
        'clientOptions' => array(
            'validateOnChange' => false,
            'validateOnSubmit' => true,
            'beforeValidate' => 'js:function(form) {

                if($("#move-contact-to-account-form").find("#moveToAccountId").val() == "") {
                    Message.create("error","Please select an account");
                    return false;
                }

                $(".loading-container").addClass("loading");
                return true;
            }',
            'afterValidate' => 'js:function(form, data, hasError) {
                $(".loading-container").removeClass("loading");
                if(data.length !== undefined || hasError) {
                    return false;
                }

                if (!hasError) { // Action was successful
                    $("#' . $this->id . '").dialog("close");
                    Message.create("success", "Action Plan Successfully Applied!");
                    location.reload();
                    return false;
                }
            }',
        ),
    )
);
?>
    <fieldset style="width:98%;">
        <section>
            <div style="width:98%; text-align: center;">Please select an Account:</div>
        </section>
        <section>
            <label style="width:20%;" for="Account_id" class="required">Accounts:<span class="required">*</span></label>

            <div style="width:73%;">
                <?php
                $linkedAccounts = Yii::app()->user->getContact()->linkedAccounts;
                $AvailableAccounts[] = Yii::app()->user->getAccount();

                foreach($linkedAccounts as $linkedAccount) {
                    $AvailableAccounts[] = Accounts::model()->findByPk($linkedAccount->account_id);
                }


                $accounts = CHtml::listData($AvailableAccounts, 'id', 'notes');

                echo $form->dropDownList($model, 'id', $accounts, $htmlOptions = array(
                    'empty' => 'Select One',
                    'class' => 'g9',
                    'name' => 'moveToAccountId',
                    'id' => 'moveToAccountId'
                ));

                echo $form->error($model, 'id'); // THIS IS REQURIED FOR AJAX TO WORK????
                ?>
            </div>
            <?php echo $form->hiddenField($parentModel, 'id'); ?>
            <input type="hidden" name="contactId" id="contactId" value="" />
        </section>
    </fieldset>
    <div class="loading-container"><em></em></div>
<?
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');
