<?
$css = <<<CSS

CSS;
Yii::app()->clientScript->registerCss('chosenStyle', $css);

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'       => $this->id,
        'options'  => array(
        'title'    => $this->title,
        'autoOpen' => false,
        'modal'    => true,
        'width'    => 500,
        'height'   => 250,
        'beforeClose'   => 'js:function(event, ui) {
                                $("#CmsTags_name").val("");
                            }',
        'buttons' => array(
            'Add CMS Tag' => 'js:function() { $("#' . $this->formId . '").submit(); }',
        ),
	 ),
));

	$form = $this->beginWidget('CActiveForm', array(
        'id'=>$this->formId,
		'action' => Yii::app()->createUrl("/".Yii::app()->controller->module->id."/cms/tagsAdd"),
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
            'beforeValidate'    => 'js:function(form) {
	            $(".loading-container").addClass("loading");
				return true;
            }',
			'afterValidate' => 'js:function(form, data, hasError) {
    			$(".loading-container").removeClass("loading");
				if (!hasError) { // Action was successful
					$("#' . $this->id . '").dialog("close");
					Message.create("success", "Successfully Added Cms Tag!");
					$("#CmsContents_cmsTagsToAdd").append("<option selected value="+data.id+">"+data.name+"</option>");
                    $("#CmsContents_cmsTagsToAdd").trigger("liszt:updated");
				}
				return false;
			}',
		)
	));

	?>
    <fieldset class="g12 p-p0">
        <section>
            <div>Type in the name of the tag below:</div>
        </section>
        <section>
            <?php echo $form->labelEx($model, 'name');?>
            <div>
                <?php echo $form->textField($model, 'name', $htmlOptions = array('placeholder' => 'Tag Name', 'class' => 'g9')); ?>
                <?php echo $form->error($model, 'name');?>
            </div>
        </section>
        <section>
            <?php echo $form->labelEx($model, 'url');?>
            <div>
                <?php echo $form->textField($model, 'url', $htmlOptions = array('placeholder' => 'Tag Url', 'class' => 'g9')); ?>
                <?php echo $form->error($model, 'url');?>
            </div>
        </section>
    </fieldset>
    <div class="loading-container"><em></em></div>
	<?php
	$this->endWidget();
    $this->endWidget('zii.widgets.jui.CJuiDialog');
