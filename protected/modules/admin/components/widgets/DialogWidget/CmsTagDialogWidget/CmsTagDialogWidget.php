<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class CmsTagDialogWidget extends DialogWidget
{

	public $companyId;

	public function run() {
		// Can't run without a way to call the dialog
        if (!$this->triggerElement)
            return false;

        $this->registerScript();

        $model = new CmsTags();

		$this->render('cmsTagDialog', array(
			'model'          => $model,
		));
	}

	public function registerScript() {
		Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js',CClientScript::POS_END);

        $moduleName = Yii::app()->controller->module->id;
		$js = <<<JS
//		$(document).ready(function () {
//			$("select.companyContacts").ajaxChosen({
//				type: 'POST',
//				url: '/$moduleName/contacts/autocomplete',
//				dataType: 'json'
//			}, function (data) {
//				var results = [];
//
//				$.each(data, function (i, val) {
//                            results.push({ value: val.value, text: val.text+" ("+val.email+")" });
//				});
//
//				return results;
//			});
//		});
JS;
		Yii::app()->clientScript->registerScript('cms-tag-script', $js);
	}
}
