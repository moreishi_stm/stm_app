<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class ChooseSmsNumberDialogWidget extends DialogWidget {

	public $contactId;
	public function run() {
		$this->registerScript();

		$this->render('smsNumberDialog');
	}

	public function registerScript() {
		$module = Yii::app()->controller->module->name;

		$js = <<<JS
			$("#choose-sms-number-form").submit(function(e) {
				var area_code = $("#area_code", $(this)).val();

				if(area_code == "" || area_code == null || area_code.length < 3) {
					Message.create("error", "Error: Please enter a 3 digit area code to search in.");
					return false;
				}

				$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");

				var postData = $(this).serializeArray();

                $.ajax({
					url: '/$module/settings/searchAvailableNumbersAction',
					type: "POST",
                    data: postData,
					cache: false,
					//dataType: "json",
					success: function(data) {

						if(!data) {
							$('.loading-container.loading').remove();
							Message.create("error", "Error: There has been an error. Please try again.");
							return false;
						}

						$("#data-grid-view").html(data);

						// Remove loading modal
						$('.loading-container.loading').remove();

						$('#save-selected-number').show();

						$("#save-selected-number").unbind('click');
						$("#save-selected-number").click(function() {
							$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");

							var selectedValue = $("input[name='phone_to_use']:checked",$("#data-grid-view")).val();

							if(selectedValue == "" || selectedValue == null || typeof selectedValue == "undefined" || selectedValue.length < 1) {
								Message.create("error", "Error: Please select a number.");
								$('.loading-container.loading').remove();
								return false;
							}

							var postData = {
								'phone_to_use': selectedValue,
								'contact_id': $this->contactId
							};

							$.ajax({
								url: '/$module/settings/saveAvailableNumbersAction',
								type: "POST",
								data: postData,
								cache: false,
								dataType: "json",
								success: function(data) {

									if(!data) {
										$('.loading-container.loading').remove();
										Message.create("error", "Error: There has been an error. Please try again.");
										return false;
									}

									if(!data.status) {
										$('.loading-container.loading').remove();
										Message.create("error", "Error: There has been an error. Please try again.");
										return false;
									}

									if(data.status == "error" && !data.message) {
										$('.loading-container.loading').remove();
										Message.create("error", "Error: There has been an error. Please try again.");
										return false;
									}

									if(data.status == "error" && data.message) {
										$('.loading-container.loading').remove();
										Message.create("error", "Error: "+data.message);
										return false;
									}

									Message.create("success", "Successfully created SMS Number!");

									$("#$this->id").dialog("close");
									$("#$this->id").siblings(".ui-dialog-buttonpane").find("button").button("enabled");

									location.reload();
								}
							});
						});

					}
                });
				e.preventDefault(); //STOP default action
				return false;
			});
JS;
		Yii::app()->clientScript->registerScript(__CLASS__.'search-available-numbers-dialog-script', $js);
	}
}
