<?php
$dialogId = $this->id;
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id'      => $this->id,
	'options' => array(
		'title'       => $this->title,
		'autoOpen'    => false,
		'modal'       => true,
		'width'       => 545,
		//'beforeClose' => 'js:function() {return false;}',
		//'close' => 'js:function() {return false;}',
		'closeOnEscape' => true,
		
	),
));
$moduleName = Yii::app()->controller->module->name;
$form = $this->beginWidget('CActiveForm',
    array('id'  => 'choose-sms-number-form',
    'action' => '',
    'enableAjaxValidation' => true,
    'clientOptions'        => array(
         'validateOnChange' => false,
         'validateOnSubmit' => true,
         'beforeValidate'   => 'js:function(form) {
            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
            return true;
         }',
		)
	)
);
?>
    <fieldset>
        <section>
            Type an Area Code To Search:
            <div>
				<?php echo CHtml::textField('area_code', '', $htmlOptions = array( 'class'=>'g6', 'minlength' => 3, 'maxlength' => 3 )); ?>
            </div>
        </section>
		<section>
			<div id="data-grid-view">
			</div>
		</section>
    </fieldset>
    <div class="submit-button-row p-tc p-p10">
        <input id="search-numbers-dialog-submit-button" class="button wide" type="submit" value="Search Numbers" />
		<input id="save-selected-number" class="button wide" type="button" value="Save Selected Number" style="display: none;" />
    </div>
    <div class="loading-container"><em></em></div>
<?php
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');
