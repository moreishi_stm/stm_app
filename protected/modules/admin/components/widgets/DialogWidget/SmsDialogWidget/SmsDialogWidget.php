<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class SmsDialogWidget extends DialogWidget {

	public $componentTypeId;
	public $fromContact;
    public $toContact;
	public $contactId;
	public $componentId;

    /**
     * Init
     * Overloaded the parent version to remove registerTrigger Script and have it called in the run method. Properties were not set in the init and the script was breaking.
     * @return null|void
     */
    public function init()
    {
        if ($this->id && $this->triggerElement) {
            if ($this->formId === null) {
                $this->formId = $this->id . '-form';
            }
        }
    }

	public function run()
    {
		$model = new SmsMessages;

		if ($this->contactId && !($this->fromContact instanceof Contacts)) {
            $this->toContact = Contacts::model()->findByPk($this->contactId);
        }

		$model->component_type_id = $this->componentTypeId;
		$model->component_id = Yii::app()->getRequest()->getQuery('id');

        // We need this to enable iframe "ajax" calls for file uploads
        Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/jquery/plugins/jquery.iframe-transport.js');

		$id = ($id) ? $id : Yii::app()->user->id;
        $this->fromContact = Contacts::model()->findByPk($id);

        $clientAccount = ClientAccounts::model()->findByAttributes(array('client_id'=> Yii::app()->user->clientId, 'account_id'=> Yii::app()->user->accountId));

        $Criteria = new CDbCriteria;
        $Criteria->condition = 'contact_id = :contactId AND status = :active AND client_account_id=:clientAccountId';
        $Criteria->params = array(
            ':clientAccountId'     => $clientAccount->id,
            ':contactId'    => $this->fromContact->id,
            ':active'		=> 1
        );

		$smsProvidedNumber = TelephonyPhones::model()->find($Criteria);

        $this->registerTriggerScript();

        $this->render('smsDialog', array(
			'smsProvidedNumber' => Yii::app()->format->formatPhone($smsProvidedNumber->phone),
			'model' => $model,
		));
	}

	public function registerTriggerScript()
    {
		$phones = $this->toContact->phones;

		$foundOneCell = ((bool)count($phones)) ? 'true' : 'false';
		/*foreach($phones as $phone) {
			if(!isset($phone->phone_type_ma) || $phone->phone_type_ma != 1) {
				continue;
			}

			$foundOneCell = "true";
			break;
		}*/

        $contactId = $this->contactId;
		$module = Yii::app()->controller->module->name;

        //@todo: FROM agent_id - so it can be done on behalf of someone??? - by permission - later feature INSERT HERE - CLee 4/25/16
        //
        //  Goes here
        //

		$js = <<<JS
        // set initial value of variables, they can get overridden later from element attribute data for dynamic usage
        var contactId = ("$contactId" != "") ? "$contactId" : null;
        var componentTypeId = ("$this->componentTypeId" != "") ? "$this->componentTypeId" : null;
        var componentId = ("$this->componentId" != "") ? "$this->componentId" : null;

        var initSmsDialog = function(ele) {

            // if there is an element attribute, clear current data and populate new info
	        if(ele.attr('cntid') != undefined && ele.attr('cntid') != "") {

                contactId = ele.attr('cntid');

                // clear out phone field
                $('#SmsMessages_to_phone_id').html('').trigger('liszt:updated');

                // dynamically populate
                $.post('/$module/contacts/getPhones/' + contactId , function(data) {

                    if(data.status == 'success') {

                        var listOptions = '';
                        $.each( data.phones, function( key, value ) {
                            listOptions += '<option value="' + key + '">' + value + '</option>';
                        });
                        $('#SmsMessages_to_phone_id').html(listOptions).trigger('liszt:updated');

                    } else {
                        Message.create("error","Could not retrieve this contact's phone numbers.");
                    }

                },"json");
	        }

	        if(ele.attr('ctid') != undefined && ele.attr('ctid') != "") {
                componentTypeId = ele.attr('ctid');
                $('#SmsMessages_component_type_id').val(componentTypeId);
	        }

	        if(ele.attr('cid') != undefined && ele.attr('cid') != "") {
                componentId = ele.attr('cid');
                $('#SmsMessages_component_id').val(componentId);
	        }

//	    	var Dialog = $("#$this->id");
//	        Dialog.dialog('open');
//
//	        Dialog.find('div.errorMessage').html('').hide();
//
//	        Dialog.find('input, select, textarea, span').removeClass('error');
//	        Dialog.find('input').blur();

	        return false;
        };

			$("#send-sms-button").live('click',function() {
				var foundOneCell = $foundOneCell;
				if(foundOneCell == 'false') {
					Message.create("error","Contact does not have any phone numbers. Please add one to the contact and try again.");
					return false;
				}
			});

            /*$("select#SmsMessages_to_phone_id").ajaxChosen({
                type: 'POST',
                url: '/$module/contacts/smsAutocomplete',
                dataType: 'json'
            }, function (data) {
                var results = [];

                $.each(data, function (i, val) {
					results.push({ value: val.value, text: val.number });
                });

                return results;
            });*/

			$("select#smsTemplate").live('change',function() {
				var id = $("select#smsTemplate").val();
				$.post('/$module/smsTemplates/'+id+'/contact/' + contactId + '?componentTypeId=' + componentTypeId + '&componentId=' + componentId , function(data) {
					if(data) {
						$('#SmsMessages_content').val(data.content);
					} else
						Message.create("error","Error: Template did not load.");
				},"json");
			});

//			$('#sms-form').submit(function(e) {
//				var postData = $(this).serializeArray();
//				postData['componentTypeId'] = componentTypeId;
//				postData['componentId'] = componentId;
//                $.ajax({
//					url: '/$module/activityLog/sms',
//					type: "POST",
//                    data: postData,
//					cache: false,
//					dataType: "json",
//					success: function(data) {
//
//						if(!data.status) {
//							$('.loading-container.loading').remove();
//							Message.create("error", "Error: SMS could not be sent. Please try again.");
//							return false;
//						}
//
//						if(data.status == "error") {
//							$('.loading-container.loading').remove();
//							Message.create("error", "Error: "+data.message);
//							return false;
//						}
//
//						$('.errorMessage').remove();
//
//						if(data.elements) {
//							// Report errors
//							var eleError = false;
//							for(var i in data.elements) {
//								for(var j = 0; j < elements[i].length; j++) {
//
//									// Create and append new element container
//									eleError = jQuery('<div class="errorMessage" id="' + i + '_em_' + '">' + elements[i][j] + '</div>');
//									$(eleError).insertAfter('#' + i);
//								}
//							}
//
//							if(eleError != false) {
//								$('.loading-container.loading').remove();
//								return false;
//							}
//						}
//
//						// Remove loading modal
//						Message.create("success", "Successfully sent SMS!");
//
//                        if(document.getElementById("activity-log-grid")) {
//                            $.fn.yiiGridView.update("activity-log-grid");
//                        }
//                        else {
//                            $('.loading-container.loading').remove();
//                        }
//
//						$("#$this->id").dialog("close");
//						$("#$this->id").siblings(".ui-dialog-buttonpane").find("button").button("enabled");
//					}
//                });
//				e.preventDefault(); //STOP default action
//				return false;
//			});
JS;
		Yii::app()->clientScript->registerScript(__CLASS__.'sms-dialog-script', $js);
	}
}
