<?php
$css
    = <<<CSS
.ui-dialog.ui-widget, .ui-dialog .ui-dialog-content {
    overflow: visible;
}
/*#emailTemplate_chzn{*/
	/*width: 80% !important;*/
/*}*/
#sms-dialog fieldset {
    width: 748px;
}
CSS;
Yii::app()->clientScript->registerCss('smsDialogCss', $css);

Yii::app()->clientScript->registerScriptFile($this->controller->module->cdnAssetUrl . '/v1/EChosen/assets/chosen.jquery.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile($this->controller->module->cdnAssetUrl . '/v1/EChosen/assets/chosen.css');
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/ajax_chosen.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScript('smsDialogScriptJs', <<<JS

    // Bind event to open dialog`
    $('#send-sms-button').live('click', function(e) {

        // Stop default behavior
        e.preventDefault();

        // Create modal
        stmModal.create({
            title: 'Send SMS',
            content: $('#template-dialog-form-sms').html(),
            height: 370,
            width: 750,
            successCallback: function() {
                if(document.getElementById("activity-log-grid")) {
                    $.fn.yiiGridView.update("activity-log-grid");
                }
            }
            //saveFormDataUrl: "/admin/documents/add"
        });
        initSmsDialog($(this));
    });
JS
);

$module = Yii::app()->controller->module->name;
$dialogId = $this->id;


//$this->beginWidget('zii.widgets.jui.CJuiDialog',
//    array('id'      => $this->id,
//        'options' => array('title'       => $this->title,
//                           'autoOpen'    => false,
//                           'modal'       => true,
//                           'width'       => 750,
//                           'height'      => 'auto',
//                           'beforeClose' => 'js:function() {'. <<<JS
//                                $('#$dialogId').find('.error').removeClass('error');
//                                $("#SmsMessage_to_phone_id").val("");
//                                $("#SmsMessage_to_phone_id").trigger("liszt:updated");
//                                $("#template_id").val(0);
//                                $("#SmsMessage_content").val('');
//                                $("select.smsTemplate").trigger("liszt:updated");
//JS
//                           .'}',
//                           'buttons'  => array(
//                               'Send SMS' => "js:function() {
//                                $('#sms-form').submit();
//                            }"
//                           ),
//                        )
//                    )
//                );
$moduleName = Yii::app()->controller->module->name;

echo '<script id="template-dialog-form-sms" type="text/x-handlebars-template">';
$form = $this->beginWidget('CActiveForm',
    array('id'  => 'sms-form',
    'action' => '/'.$module.'/activityLog/sms',
    'enableAjaxValidation' => true,
    'htmlOptions'   =>  array(
        'class' =>  'form-horizontal'
    ),
    'clientOptions'        => array(
         'validateOnChange' => false,
         'validateOnSubmit' => true,
         'beforeValidate'   => 'js:function(form) {
            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
            return true;
         }',
         'afterValidate'=>'js:function(form, data, hasError)
        {'.
$js=<<<JS
            if(!data.status) {
                $('.loading-container.loading').remove();
                Message.create("error", "Error: SMS could not be sent. Please try again.");
                return false;
            }

            if(data.status == "error") {
                $('.loading-container.loading').remove();
                Message.create("error", "Error: "+data.message);
                return false;
            }

            // Remove loading modal
            Message.create("success", "Successfully sent SMS!");

            if(document.getElementById("activity-log-grid")) {
                $.fn.yiiGridView.update("activity-log-grid");
            }
            else {
                $('.loading-container.loading').remove();
            }

            $("#$this->id").dialog("close");

            return false;
JS
            .' }',
        )
	)
);
?>
<div class="stm-modal-body">
    <div class="form-group">
        <label for="SmsMessages_from" class="control-label col-sm-2">From: </label>
        <div class="col-sm-10">
            <?php echo $smsProvidedNumber; ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'to_phone_id', array(
            'class' =>  'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php
            $phonesListData = array();
            if($this->toContact) {
                $phones = $this->toContact->phones;
                $phonesListData = CHtml::listData($phones, 'id', 'friendlyPhone');
            }
            echo $form->dropDownList($model, 'to_phone_id', $phonesListData, array(
                'class' => 'form-control',
            ));

            echo $form->error($model, 'to_phone_id');
            $this->widget('admin_module.extensions.EChosen.EChosen', array(
                'target' => 'select#SmsMessage_to_phone_id',
                'options'=>array(
                    'allow_single_deselect'=>true,
                    'width' => '92%',
                ),
            ));
            ?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2">Template:</label>
        <div class="col-sm-10">
            <?php
            $this->widget(
                'admin_module.extensions.EChosen.EChosen', array('target'  => '#smsTemplate',
                    'options' => array('allow_single_deselect' => true,
                        'width' => '91%',
                        'enable_split_word_search'=>true,
                        'search_contains'=>true,
                    ),
                )
            );

            $smsTemplateListData = CHtml::listData(SmsTemplates::getByComponentTypeId($this->componentTypeId), 'id', 'dropDownDisplay');
            $smsTemplateListData = CHtml::listData(SmsTemplates::getByComponentTypeId($this->componentTypeId), 'id', 'dropDownDisplay');
            echo CHtml::dropDownList(
                'template_id', null, $smsTemplateListData, $htmlOptions = array('id'               => 'smsTemplate',
                'class'            => 'smsTemplate form-control',
                'empty'            => '',
                //'style'            => 'width: 91%;',
                'data-placeholder' => 'Select Sms Template...',
            ));
            ?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2">Message:</label>
        <div class="col-sm-10">
            <?php echo $form->textArea($model, 'content', $htmlOptions=array(
                'placeholder'=>'Message',
                'class'=>'form-control',
                'rows'=>4,
            )); ?>
            <?php echo $form->error($model, 'content'); ?>
        </div>
    </div>
</div>
<div class="stm-modal-footer">
    <button type='submit'>Send SMS</button>
</div>

<?php
echo $form->hiddenField($model, 'component_id');
echo $form->hiddenField($model, 'component_type_id');
$this->endWidget();
?>
<?='</script>'?>