<?
$css = <<<CSS
    #email-body {
        padding: 5px;
        margin: 5px;
        font-family: Verdana !important;
        min-height: 500px;
    }
    blockquote div table {
        width: 100% !important;
        background-image: none !important;
        float: inherit !important;
        margin: auto !important;
        border-collapse: inherit !important;
    }
CSS;
Yii::app()->clientScript->registerCss('chosenStyle', $css);

$this->beginWidget('zii.widgets.jui.CJuiDialog', array('id' => $this->id,
        'options' => array('title' => $this->title,
            'autoOpen' => false,
            'modal' => true,
            'width' => 750,
        ),
    )
);
?>
<div id="email-body"></div>
<div class="loading-container"><em></em></div>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
