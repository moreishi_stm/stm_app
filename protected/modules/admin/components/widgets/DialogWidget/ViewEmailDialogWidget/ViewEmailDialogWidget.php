<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class ViewEmailDialogWidget extends DialogWidget {

	public function run() {


		$this->render('emailDialog');
	}

    public function registerTriggerScript() {
        $js = <<<JS
	    $('$this->triggerElement').live('click', function() {
	        $(".loading-container").addClass("loading");
	    	var Dialog = $('#$this->id');
	        Dialog.dialog('open');

	        Dialog.find('#email-body').html('Loading e-mail message.');
	        $.getJSON('/admin/activityLog/viewEmail', {'id' : $(this).data('id')}, function(data) {
	            var emailInfo = '';

	            emailInfo += 'From: ' + data.fromName + ' &lt;'+ data.fromEmail +'&gt;<br>';
	            emailInfo += 'To: ' + data.toName + ' &lt;'+ data.toEmail +'&gt;';

                if(data.cc) {
                    emailInfo += 'Cc: ' + ' '+ data.cc;
                }
	            emailInfo += '<hr />';
	            emailInfo += data.result;
	            Dialog.find('#email-body').html(emailInfo);
	            $(".loading-container").removeClass("loading");
	        });

	        Dialog.find('div.errorMessage').html('').hide();

	        Dialog.find('input, select, textarea, span').removeClass('error');
	        Dialog.find('input').blur();

	        return false;
	    });
JS;
        Yii::app()->clientScript->registerScript('email-dialog-script', $js);
    }
}
