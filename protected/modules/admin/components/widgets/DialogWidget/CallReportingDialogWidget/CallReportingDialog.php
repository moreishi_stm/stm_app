<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class CallReportingDialog extends DialogWidget {

    const DIALOG_ID = 'call-reporting-dialog';
    const TRIGGERS = '.call-report-status-dialog-button';

    public $parentModel;

    public function run()
    {
        $model = new CallReportingStatusForm();

        $this->render('callReportingDialog', array(
                'model' => $model,
            )
        );
    }

    /**
     * Used to register the trigger script for calling the dialog. Override this to change this functionality.
     *
     * @return null
     */
    public function registerTriggerScript()
    {
        $excludeReportingId = TaskTypes::EXCLUDE_CALL_REPORTING;
        $includeReportingId = TaskTypes::INCLUDE_CALL_REPORTING;

        $js = <<<JS
            $('$this->triggerElement').live('click', function() {

                var Dialog = $('#$this->id');
                Dialog.find('#CallReportingStatusForm_call_hunt_group_session_id').val('');
                Dialog.find('#CallReportingStatusForm_exclude_reason').val('');
                Dialog.find('#CallReportingStatusForm_exclude_reason_other').val('');
                Dialog.find('#CallReportingStatusForm_notes').val('');


                $('#CallReportingStatusForm_call_hunt_group_session_id').val($(this).data('id'));

                if($(this).hasClass('exclude-call-reporting')) {
                    $('#CallReportingStatusForm_task_type_id').val($excludeReportingId);
                    $('.call-reporting-action-label').html('Exclude');
                    $('#CallReportingStatusForm_exclude_from_stats').attr('value', 1);
                }
                else if($(this).hasClass('include-call-reporting')) {
                    $('#CallReportingStatusForm_task_type_id').val($includeReportingId);
                    $('.call-reporting-action-label').html('Include');
                    $('#CallReportingStatusForm_exclude_from_stats').attr('value', 0);
                }

                $('#CallReportingStatusForm_exclude_reason').change(function(){
                    if($(this).val() == 'Other') {
                        $('.call-reporting-reason-other').show('normal');
                    }
                    else {
                        $('.call-reporting-reason-other').hide('normal');
                    }
                });

    			Dialog.dialog('open');
            });
JS;

        Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
    }
}
