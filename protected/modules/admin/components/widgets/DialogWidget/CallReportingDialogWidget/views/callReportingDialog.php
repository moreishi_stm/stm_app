<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
     'id'       => $this->id,
     'options'  => array(
     	'title'    => $this->title,
		'autoOpen' => false,
		'modal'    => true,
		'width'    => 600,
        'height'    => 430,
		'open'=>'js:function(){
		}',
		'buttons'  => array(
			'Update Reporting Status' => "js:function() {
				$('#".$this->formId."').submit();
			}",
       ),
     ),
));

$form=$this->beginWidget('CActiveForm', array(
    'id'=>$this->formId,
    'action'=>array('/'.Yii::app()->controller->module->name.'/phone/editCallReportingStatus'),
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>false,
    'clientOptions'=>array(
    	'validateOnChange'=>false,
    	'validateOnSubmit'=>true,
		'beforeValidate'    => 'js:function(form) {
	            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");

				return true;
            }',
        'afterValidate'=>'js:function(form, data, hasError) {
            if (!hasError) { // Action was successful
                $("#'.$this->id.'").dialog("close");


                if (data.status=="success") {

                    Message.create("success", "Successful Updated Call Reporting Status.");

                    if(data.actionType=="include") {
                        $(".report-status-text-" + data.id + " .report-status-action-text").removeClass("include-call-reporting").addClass("exclude-call-reporting").html("Exclude from Reports");
                        $(".report-status-image-" + data.id).removeClass("i_stm_error").addClass("i_stm_success");
                    }
                    else if(data.actionType=="exclude") {
                        $(".report-status-text-" + data.id + " .report-status-action-text").removeClass("exclude-call-reporting").addClass("include-call-reporting").html("Include in Reports");
                        $(".report-status-image-" + data.id).removeClass("i_stm_success").addClass("i_stm_error");
                    }


                    if (document.getElementById("call-history-grid")) {
        				$.fn.yiiGridView.update("call-history-grid", {
                            data: $("form#call-history-search").serialize()
                        });
                    }
                }

                $("div.loading-container.loading").remove();

                return false;
            }
            $("div.loading-container.loading").remove();
        }',
    ),
));
?>
<fieldset>
    <section><label>Action:</label><div style="color: #0085FF; font-weight: bold; font-size: 15px;" class="call-reporting-action-label">Include</div></section>
    <section>
        <?php echo $form->labelEx($model,'exclude_reason'); ?>
        <div>
            <?php echo $form->dropDownList($model, 'exclude_reason', CallHuntGroupSessions::getExcludeReasonList(), $htmlOptions=array(
                    'empty'=>'Select Activity',
                ));
            ?>
            <?php echo $form->error($model, 'exclude_reason'); ?>
        </div>
    </section>
    <section class="call-reporting-reason-other" style="display: none;">
        <?php echo $form->labelEx($model,'exclude_reason_other'); ?>
        <div>
            <?php echo $form->textField($model, 'exclude_reason_other', $htmlOptions=array('placeholder'=>'Other Reason','class'=>'g11')); ?>
            <?php echo $form->error($model, 'exclude_reason_other'); ?>
        </div>
    </section>
    <section>
        <?php echo $form->labelEx($model,'notes'); ?>
        <div>
            <?php echo $form->textArea($model,'notes',$htmlOptions=array(
                    'placeholder'=>'Notes',
                    'class'=>'g11',
                    'rows'=>5,
                )); ?>
            <?php echo $form->error($model, 'notes'); ?>
        </div>
    </section>

</fieldset>
<?php echo $form->hiddenField($model, 'call_hunt_group_session_id');?>
<?php echo $form->hiddenField($model, 'exclude_from_stats');?>
<?php echo $form->hiddenField($model, 'task_type_id');?>
<?php
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');
