<?php
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class EmailDialogWidget extends DialogWidget {

	public $componentTypeId;
	public $contact;
	public $contactId;

    /**
     * Init
     * Overloaded the parent version to remove registerTrigger Script and have it called in the run method. Properties were not set in the init and the script was breaking.
     * @return null|void
     */
    public function init() {
        if ($this->id && $this->triggerElement) {
            if ($this->formId === null) {
                $this->formId = $this->id . '-form';
            }
        }
    }

	public function run() {

		$model = new EmailMessage;

		if ($this->contactId && !$this->contact instanceof Contacts) {
			$this->contact = Contacts::model()->findByPk($this->contactId);
		}

		$model->component_type_id = $this->componentTypeId;
		$model->component_id = Yii::app()->getRequest()->getQuery('id');

        // We need this to enable iframe "ajax" calls for file uploads
        Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/jquery/plugins/jquery.iframe-transport.js', CClientScript::POS_END);
        $this->registerTriggerScript();

		$this->render('emailDialog', array(
			'model' => $model,
			'contact' => $this->contact,
			'componentTypeId'=>$this->componentTypeId,
		));
	}

    public function registerTriggerScript()
    {
        Yii::app()->clientScript->registerScriptFile($this->controller->module->cdnAssetUrl . '/v1/EChosen/assets/chosen.jquery.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($this->controller->module->cdnAssetUrl . '/v1/EChosen/assets/chosen.css');
        Yii::app()->clientScript->registerScriptFile($this->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);

        $email = $this->contact->primaryEmail;
        $contactId = $this->contact->id;
        $componentId = Yii::app()->getRequest()->getQuery('id');
        $module = Yii::app()->controller->module->name;

        $js = <<<JS

        var initEmailDialog = function(ele) {

            // set initial value of variables, they can get overridden later from element attribute data for dynamic usage
            var contactId = ("$contactId" != "") ? "$contactId" : ((ele.attr('cntid') != undefined && ele.attr('cntid') != "") ? ele.attr('cntid') : null);
            $('#EmailDialog_contactId').val(contactId);
            var componentTypeId = ("$this->componentTypeId" != "") ? "$this->componentTypeId" : null;
            var componentId = ("$componentId" != "") ? "$componentId" : null;

//            $("$this->triggerElement").live(clickTouchEvent, function() {

                // if there is an element attribute, clear current data and populate new info
                if(ele.attr('cntid') != undefined && ele.attr('cntid') != "") {

                    contactId = ele.attr('cntid');
                    $('#EmailDialog_contactId').val(contactId);

                    // clear out email address field
                    $('#EmailMessage_to_email_id').html('').trigger('liszt:updated');

                    // dynamically populate
                    $.post('/$module/contacts/getEmails/' + contactId , function(data) {

                        if(data.status == 'success') {
                            var emailListOptions = '';
                            $.each( data.emails, function( key, value ) {
                                emailListOptions += '<option value="' + key + '">' + value + '</option>';
                            });
                            $('#EmailMessage_to_email_id').html(emailListOptions).trigger('liszt:updated');
                        } else {
                            Message.create("error","Could not retrieve this contact's emails addresses.");
                        }

                    },"json");
                }

                if(ele.attr('ctid') != undefined && ele.attr('ctid') != "") {
                    componentTypeId = ele.attr('ctid');
                    $('#EmailMessage_component_type_id').val(componentTypeId);
                }

                if(ele.attr('cid') != undefined && ele.attr('cid') != "") {
                    componentId = ele.attr('cid');
                    $('#EmailMessage_component_id').val(componentId);
                }

                $("select#EmailMessage_to_email_id").ajaxChosen({
                    type: 'POST',
                    url: '/$module/contacts/emailAutocomplete',
                    dataType: 'json'
                }, function (data) {
                    var results = [];

                    $.each(data, function (i, val) {
                                results.push({ value: val.value, text: val.text+" ("+val.email+")" });
                    });

                    return results;
                });

//                var Dialog = $("#$this->id");
//                Dialog.dialog('open');
//
//                Dialog.find('div.errorMessage').html('').hide();
//
//                Dialog.find('input, select, textarea, span').removeClass('error');
//                Dialog.find('input').blur();

//                return false;
//            });
        };

        $('.cc-button').live('click', function(){
            $('.cc-container').show('normal');
            $('.cc-button').hide();
        });

        $('.bcc-button').live('click', function(){
            $('.bcc-container').show('normal');
            $('.bcc-button').hide();
        });

//            $("#send-email-button").live('click', function() {
//                if("$email" == "") {
//                    Message.create("error","Contact does not have an email address. Please add one to the contact and try again.");
//                    return false;
//                }
//            });

        // Bind event for adding extra attachment
        $('#add-attachment').live('click', function() {

            var ele = jQuery('#EmailMessage_attachments').clone().val('');
            ele.attr('id', null);

            jQuery(ele).insertBefore('#add-attachment');
        });


        $("select#emailTemplate").live('change', function() {
            var id = $("select#emailTemplate").val();

            $.post('/$module/emailTemplates/' + id + '/contact/' + $('#EmailDialog_contactId').val() + '?componentTypeId=' + $('#EmailMessage_component_type_id').val() + '&componentId=' + $('#EmailMessage_component_id').val() + '', function(data) {
                if(data) {
                    data.content += '<br /><br />' + $('#agentSignature').html();
                    $('#EmailMessage_subject').val(data.subject);
                    //$('#EmailMessage_content').val(data.content);

                    tinyMCE.get('EmailMessage_content').setContent(data.content, {format : 'raw'});
                    tinyMCE.triggerSave();

                } else
                    Message.create("error","Error: Template did not load.");
            },"json");
        });
JS;
        Yii::app()->clientScript->registerScript('email-dialog-TriggerScript-' . $this->id, $js);
    }
}
