<?php
$css
    = <<<CSS
#emailTemplate_chzn, #EmailMessage_to_email_id_chzn{
	width: 92% !important;
}
/*form#email-form fieldset > section label {*/
    /*width: 15%;*/
/*}*/
/*form#email-form fieldset > section > div {*/
    /*width: 80%;*/
/*}*/
CSS;
$scope = (YII_DEBUG) ? "local" : "com";
Yii::app()->clientScript->registerCss('emailDialogScript', $css);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/v1/EChosen/assets/chosen.jquery.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/ajax_chosen.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScript('emailDialogScriptJs', <<<JS

    // Open attachments container
    $('.add-attachments').live('click', function(){
        $('.attachments-container').show('');
    });

    // Bind event to open dialog
    $('#send-email-button').live('click', function(e) {

        // Stop default behavior
        e.preventDefault();

        // Create modal
        stmModal.create({
            title: 'Compose Email',
            content: $('#template-dialog-form-email').html(),
            height: 799,
            width: 750,
			successCallback: function() {

                if (document.getElementById("activity-log-grid")) {
                    $.fn.yiiGridView.update("activity-log-grid");
                }
			}
        });

        // Init other stuff
        initEmailDialog($(this));

        stmLoadingGif.show();

        // chosen
        $('#EmailMessage_to_email_id').chosen({enable_split_word_search:true, search_contains:true, allow_single_deselect: true}); //, width:"62%"

        // Initialize TinyMCE
        tinymce.init({
            init_instance_callback : function(editor) {
                stmLoadingGif.hide();
                var top = ($('.stm-modal').css('top').replace('px','') - 170);
                if($('.stm-modal').height() > $(window).height()) {
                    top = document.body.scrollTop;
                }
                $('.stm-modal').css({ top: top });
            },
            selector: "#EmailMessage_content",
            convert_urls: false,
            relative_urls: false,
            height : 300,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor textcolor paste",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu, spellchecker"
            ],
            cleanup_on_startup: false,
            trim_span_elements: false,
            verify_html : false,

            extended_valid_elements : "span[*|top|position],a[*],a*,div[*],span*,div[*]",
            cleanup: false,
            toolbar: "fontselect fontsizeselect spellchecker | forecolor forecolorpicker backcolor bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link undo redo | preview | code",
            autosave_ask_before_unload: false,
            setup : function(ed) {
                ed.on('init', function() { this.getDoc().body.style.fontSize = '13px'; });
                ed.on('keyUp', function() { tinyMCE.triggerSave(); });
            },
            spellchecker_rpc_url: "http://www.seizethemarket.{$scope}/js/tinymce/plugins/spellchecker/spellchecker.php"
        });
    });
JS
);

$attachFileField = "<input placeholder='Attachment' class='g11 emailAttachmentInput' type='file' name='EmailMessage[attachments][]' id='EmailMessage_attachments' />";
$dialogId = $this->id;


//$this->beginWidget('zii.widgets.jui.CJuiDialog',
//    array('id'      => $this->id,
//        'options' => array('title'       => $this->title,
//                           'autoOpen'    => false,
//                           'modal'       => true,
//                           'width'       => 850,
//                           'draggable' => true,
//                           'beforeClose' => 'js:function() {'. <<<JS
//                                $('#$dialogId').find('.error').removeClass('error');
//                                var signatureContent = $("#agentSignature").html();
//                                $("#EmailMessage_to_email_id").val("");
//                                $("#EmailMessage_to_email_id").trigger("liszt:updated");
//
//                                $("#EmailMessage_to_email_cc").val("");
//                                $("#EmailMessage_to_email_bcc").val("");
//                                $("#template_id").val(0);
//                                $("#EmailMessage_subject").val("");
//                                $("#EmailMessage_to_email_id").val('').trigger("liszt:updated");
//
//                                tinyMCE.get('EmailMessage_content').setContent(signatureContent, {format : 'raw'});
//
//
//                                $(".emailAttachmentInput").remove();
//                                $("#attachment-container").prepend("$attachFileField");
//                                $(".attachments-container").hide();
//JS
//                           .'}',
//                           'buttons'  => array(
//                               'Send Email' => "js:function() {
//                                $('#email-form').submit();
//                            }"
//                           ),
//                      ),
//                )
//            );
$moduleName = Yii::app()->controller->module->name;

echo '<script id="template-dialog-form-email" type="text/x-handlebars-template">';
$form = $this->beginWidget('CActiveForm',
    array('id'  => 'email-form',
    'action' => array('/' . $moduleName.'/activityLog/email'),
    'enableAjaxValidation' => true,
    'htmlOptions'   =>  array(
        'class' =>  'form-horizontal'
    ),
    'clientOptions'        => array(
         'validateOnChange' => false,
         'validateOnSubmit' => true,
         'beforeValidate'   => 'js:function(form) {
            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
            return true;
         }',
         'afterValidate'    => 'js:function(form, data, hasError) {'.
    $js=<<<JS
            // Action was successful
            if (!hasError) {

                // Try new AJAX w/ iframe things
                $.ajax('/$moduleName/activityLog/email', {
                    data: $(":text, select, textarea, input", form).serializeArray(),
                    files: $(":file", form),
                    dataType: "json",
                    iframe: true,
                    processData: false
                })
                .complete(function(data) {

                    // Attempt to retrieve errors
                    try {
                        var elements = jQuery.parseJSON(data.responseText);

                        // Remove any pre-existing errors we might have
                        jQuery('.errorMessage').remove();

                        // Report errors
                        var eleError;
                        for(var i in elements) {
                            for(var j = 0; j < elements[i].length; j++) {

                                // Create and append new element container
                                eleError = jQuery('<div class="errorMessage" id="' + i + '_em_' + '">' + elements[i][j] + '</div>');
                                jQuery(eleError).insertAfter('#' + i);
                            }
                        }

                        // Remove loading modal
                        $('.loading-container.loading').remove();
                        if(data.responseText != undefined) {
                            response = $.parseJSON(data.responseText);
                        }

                        if(response.emailStatus == 'success') {
                            Message.create("success", "Successfully sent e-mail!");
                        }
                        else {
                            var errorMessage = '';
                            if(response.emailErrorMessage !== undefined) {
                                errorMessage = response.emailErrorMessage
                            }
                            Message.create("error", "Email did not send.  " + errorMessage + " Please try again or contact customer support.");
                        }
                    }
                    catch(e) {}

                    if(document.getElementById("activity-log-grid")) {
                        $.fn.yiiGridView.update("activity-log-grid");
                    }
                    else {
                        $('.loading-container.loading').remove();
                    }
                    $("#$dialogId").dialog("close");
                });
            }
            else {
                $("div.loading-container.loading").remove();
            }

            return false;
JS
        .' }',
     ),
    )
);
?>
<div class="stm-modal-body">
    <div class="form-group">
        <?php echo $form->labelEx($model, 'to_email_id', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <?php
            $emails = ($this->contact->emails) ? $this->contact->emails : array();
            echo $form->dropDownList($model, 'to_email_id', CHtml::listData($emails, 'id', 'email'), $htmlOptions = array(
                'data-placeholder' =>'Select One or Type Email',
                'class' => 'form-control',
                'empty' => '',
            ));
            echo '<br><a href="javascript:void(0)" class="cc-button" style="margin-left: 8px;">Cc</a> <a href="javascript:void(0)" class="bcc-button" style="margin-left: 8px;">Bcc</a>';
            echo $form->error($model, 'to_email_id');
            ?>
        </div>
    </div>

    <div class="form-group cc-container" style="display: none;">
        <?php echo $form->labelEx($model, 'to_email_cc', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textField(
                $model, 'to_email_cc', $htmlOptions = array('placeholder' => 'Cc Emails',
                'class'       => 'form-control'
            )
            ); ?>
            <div>Separate multiple emails by commas</div>
            <?php echo $form->error($model, 'to_email_cc'); ?>
        </div>
    </div>

    <div class="form-group bcc-container" style="display: none;">
        <?php echo $form->labelEx($model, 'to_email_bcc', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textField(
                $model, 'to_email_bcc', $htmlOptions = array('placeholder' => 'Bcc Emails',
                'class'       => 'form-control'
            )
            ); ?>
            <div>Separate multiple emails by commas</div>
            <?php echo $form->error($model, 'to_email_bcc'); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Template:</label>
        <div class="col-sm-10">
            <?php // NOTE: this is the echosen for both drop-downs
//            $this->widget('admin_module.extensions.EChosen.EChosen', array(
//                'target'  => '#emailTemplate, select#EmailMessage_to_email_id',
//                'options' => array(
//                    'allow_single_deselect' => true,
//                    'enable_split_word_search'=>true,
//                    'search_contains'=>true
//                ))
//            );
            echo CHtml::dropDownList(
                'template_id', null, CHtml::listData(EmailTemplates::getByComponentTypeId($componentTypeId), 'id', 'dropDownDisplay'), $htmlOptions = array(
                    'id'               => 'emailTemplate',
                    'class'            => 'emailTemplate form-control chzn-select',
                    'empty'            => '',
                    'data-placeholder' => 'Select Email Template...',
                )
            );
            ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'subject', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textField($model, 'subject', $htmlOptions = array(
                'placeholder' => 'Subject',
                'class'       => 'form-control'
            )); ?>
            <?php echo $form->error($model, 'subject'); ?>
        </div>
    </div>

    <div class="form-group">
        <div id="agentSignature" style="display: none">
            <?php
            $signature = Yii::app()->user->accountSettings->email_signature;
            if (!$signature) {
                $signature = Yii::app()->user->settings->email_signature;
            }
            echo $signature;
            $model->content .= CHtml::tag('p', $htmlOptions = array(), $signature);
            ?>
        </div>
        <?php echo $form->labelEx($model, 'content', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textArea($model, 'content'); ?>
            <?php echo $form->error($model, 'content'); ?>
            <a href="javascript:void(0)" class="add-attachments">Add Attachments</a>
        </div>
    </div>

    <div class="attachments-container form-group" style="display: none;">
        <?php echo $form->labelEx($model, 'attachment', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>(Max Size 10Mb Total)
        <div id="attachment-container" class="col-sm-10">
            <?php echo $attachFileField; ?>
            <?php echo $form->error($model, 'attachment'); ?>
            <button type="button" id="add-attachment">Add Attachment</button>
        </div>
    </div>
</div>
<div class="stm-modal-footer">
    <button type='submit'>Send Email</button>
</div>

    <div class="loading-container"><em></em></div>
<?php
echo $form->hiddenField($model, 'component_id');
echo $form->hiddenField($model, 'component_type_id');
echo CHtml::hiddenField('EmailDialog_contactId');

?>
<?$this->endWidget();?>
<?='</script>'?>

