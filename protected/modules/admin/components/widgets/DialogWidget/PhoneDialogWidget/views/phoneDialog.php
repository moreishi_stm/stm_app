<style>
    #phone-status-form {
        padding-top: 20px;
    }
    #phone-status-form .phone-action-button {
        padding: 15px;
        width: 70%;
        font-size: 20px;
        font-weight: bold;
        display: inline-block;
        -webkit-border-radius:6px;
        -moz-border-radius:6px;
        border-radius:6px;
        margin-bottom: 10px;
    }

    #phone-status-form .deleteBadNumber-button {
        background-color: #ffeaea;
        border: 2px solid #F9DCDB;
    }
    #phone-status-form .deleteBadNumber-button:hover {
        border-color: #d23f4f;
        text-decoration: none;
    }

    #phone-status-form .click-to-call-button {
        background-color: #e3ffd4;
        border: 2px solid #d6efc7;
    }
    #phone-status-form .click-to-call-button:hover {
        border-color: #73d273;
        text-decoration: none;
    }
    #phone-status-form #phone-action-do-not-call-container {
        background-color: #DFEBFF;
        border: 2px solid #C7DAEF;
    }
    #phone-status-form #phone-action-do-not-call-container select {
        font-size: 15px;
    }
</style>

<?php
$dialogId = $this->id;
Yii::app()->clientScript->registerScript('phoneDialogScript2', <<<JS

    $('.click-to-call-button.phone-action-button').click(function(){
        $('#$dialogId').dialog('close');
    });

    $('.phone-status-button').on('click', function(e) {

            // Stop default behavior
            e.preventDefault();

            // Get component and component type IDs
            var componentTypeId = $(this).data('ctid');
            var componentId = $(this).data('cid');

            // Create modal
            stmModal.create({
                title: 'Phone Actions',
                contentVariables: {
                	title: 'Phone Actions'
                },
                content: $('#template-dialog-form').html(),
                height: 370,
                successCallback: function(r) {
                    location.reload();
                    $("body").prepend("<div class='loading-container loading'><em></em></div>");
                    Message.create('success', r.message)
                },
                contentVariables: {
                    componentId: componentId,
                    componentTypeId: componentTypeId
                }
            });

		initPhoneDialog($(this));
    });
JS
);

echo '<script id="template-dialog-form" type="text/x-handlebars-template">';

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'phone-status-form',
		'action' => Yii::app()->createUrl("/".Yii::app()->controller->module->id."/contacts/phoneStatus"),
		'enableAjaxValidation' => true,
        'enableClientValidation' => false,
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
            'beforeValidate' => 'js:function(form) {
                $("body").prepend("<div class=\"loading-container loading\" style=\"z-index: 1005 !important;\"><em></em></div>");
                return true;
            }',
            'afterValidate' => 'js:function(form, data, hasError) {
				if (!hasError) { // Action was successful
                    Message.create(data.status, data.message);
                    $("#' . $this->id . '").dialog("close");

                    if(status == "warning") {
                        $("div.loading-container.loading").remove();
                    }
                    else {
    				    location.reload();
                    }
				} else {
                    $("div.loading-container.loading").remove();
				}
				return false;
			}',
		)
	));

	?>
<!--	<h4 style="margin-top: 20px;">Select a Phone Status:</h4>-->
<!--    <div>Bad Number</div>-->
<!--    <div>Verified Number</div>-->

    <? if(Yii::app()->user->checkAccess('clickToCall')):?>
    <div class="p-tc">
        <a href="javascript:void(0);" id="phone-dialog-click-to-call-button" class="click-to-call-button phone-action-button" data-id=""  data-phone="" data-cid="{{componentId}}" data-ctid="{{componentTypeId}}">Click to Call</a>
    </div>
    <? endif; ?>
    <div class="p-tc">
        <a href="javascript:void(0);" class="deleteBadNumber-button phone-action-button" data-id="">Delete Bad Number</a>
    </div>
    <div class="p-tc">
        <div id="phone-action-do-not-call-container" class="phone-action-button">
            Add to Do Not Call:<br>
            <select id="do-not-call-days" name="doNotCallDays">
                <option value="">-- Select One --</option>
                <option value="7">Do Not Call for 1 week</option>
                <option value="14">Do Not Call for 2 week</option>
                <option value="21">Do Not Call for 3 week</option>
                <option value="28">Do Not Call for 4 week</option>
                <option value="42">Do Not Call for 6 week</option>
                <option value="56">Do Not Call for 8 week</option>
                <option value="70">Do Not Call for 10 week</option>
                <option value="90">Do Not Call for 3 months</option>
                <option value="180">Do Not Call for 6 months</option>
                <option value="99999">Do Not Call Permanently</option>
            </select>
            <button class="button gray add-do-not-call-button" style="font-size:10px;top:-4px;">Go</button>
        </div>
    </div>
	<?php
        echo $form->hiddenField($model, 'id', $htmlOptions=array());
        echo $form->error($model, 'id');
        echo CHtml::hiddenField('actionType', null, $htmlOptions=array());
        echo CHtml::hiddenField('phoneOriginComponentTypeId', null, $htmlOptions=array());
        echo CHtml::hiddenField('phoneOriginComponentId', null, $htmlOptions=array());?>
<?$this->endWidget(); ?>
<?='</script>'?>
