<?
Yii::import('admin_widgets.DialogWidget.DialogWidget');

class PhoneDialogWidget extends DialogWidget
{
    const PHONE_DIALOG_ID = 'phone-dialog';
    public $parentModel;

	public function run() {
		// Can't run without a way to call the dialog
        if (!$this->triggerElement)
            return false;

		$this->render('phoneDialog', array(
                'model'=> new Phones,
		));
	}

    public function registerTriggerScript() {

        $js = <<<JS
//			$('$this->triggerElement').live('click', function() {
            var initPhoneDialog = function (ele) {

//                var Dialog = $("#$this->id");
//                Dialog.dialog('open');
//                Dialog.find('div.errorMessage').html('').hide();
//                Dialog.find('input, select, textarea, span').removeClass('error');
//                Dialog.find('input').blur();

                $('#Phones_id').attr('value', ele.data('id'));
                $('#phone-dialog-click-to-call-button').attr('data-phone', ele.data('phone'));
                $('#clickToCallToPhoneNumber').attr('value', ele.data('phone'));
                $('#phoneOriginComponentTypeId').attr('value', ele.data('ctid'));
                $('#phoneOriginComponentId').attr('value', ele.data('cid'));
                $('#actionType').attr('value', '');
                $('#do-not-call-days').val('');

    	        return false;
            };

			$('.deleteBadNumber-button').live('click', function() {
                $('#actionType').attr('value', 'deleteBadNumber');
			    $('form#phone-status-form').submit();
                return false;
            });

			$('.add-do-not-call-button').live('click', function() {
			    if($('#do-not-call-days').val() == '') {
			        alert('Missing Do Not Call Timeframe. Please select one.');
			        return false;
			    }

                $('#actionType').attr('value', 'addDoNotCall');
			    $('form#phone-status-form').submit();
                return false;
            });
JS;

        Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
    }
}
