<?php
	Yii::import('admin_widgets.DialogWidget.DialogWidget');

	class ActivityLogDialog extends DialogWidget {

		const ACTIVITY_LOG_DIALOG_ID = 'activity-log-dialog';
		const ACTIVITY_LOG_TRIGGERS = '.complete-task-button, .add-activity-log-button, .edit-activity-log-button';
		public $parentModel;

		public function run() {
			$model = new ActivityLog;
            $model->followupTask = new Tasks();
            $model->referrerUrl = $_SERVER['REQUEST_URI'];

			$this->render('activityLogDialog', array(
					'model' => $model,
				)
			);
		}

		/**
		 * Used to register the trigger script for calling the dialog. Override this to change this functionality.
		 *
		 * @return null
		 */
		public function registerTriggerScript() {
			$varName = 'taskList';

            $userId = Yii::app()->user->id;
			$taskTypeDropDownLists = array('taskTypeDropDownLists' => TaskTypeComponentLu::getDropDownLists());
			$taskTypeDropDownLists = CJSON::encode($taskTypeDropDownLists);
			$showLeadGenTypes = '["'.ComponentTypes::CONTACTS.'", "'.ComponentTypes::BUYERS.'", "'.ComponentTypes::SELLERS.'", "'.ComponentTypes::RECRUITS.'", "'.ComponentTypes::DIRECT_INCOMING_CALLS.'"]';
			$module = Yii::app()->controller->module->name;
            $phoneTaskTypeId = TaskTypes::PHONE;
            $leadGenNo = ActivityLog::LEAD_GEN_NONE_ID;
            $leadGenYes = ActivityLog::LEAD_GEN_YES_ID;
            $leadGenFollowUp = ActivityLog::LEAD_GEN_FOLLOW_UP_ID;
            $taskTypeMarketing = TaskTypes::MARKETING;
            $dialogId = $this->id;

			$js = <<<JS
		var initActivityLogDialog = function(ele) {

			var Dialog = $('#$this->id');
			var componentTypeId = $(ele).attr('ctid');
			var componentId = $(ele).attr('data');
			var taskTypeDropDownLists = (jQuery.parseJSON('$taskTypeDropDownLists')).taskTypeDropDownLists;

            if(componentTypeId==8) {
			    componentTypeId = $(ele).attr('ctido');
            }

			$('select#ActivityLog_task_type_id').html(taskTypeDropDownLists[componentTypeId]);
			$('select#ActivityLog_followupTask_task_type_id').html(taskTypeDropDownLists[componentTypeId]);

			Dialog.find(':input:not([type=hidden], #lead_gen_type_yes, #lead_gen_type_no, #ActivityLog_lead_gen_type_ma, #ActivityLog_url, #ActivityLog_notify)').val('');   //clear all input execpt the hidden ones
			Dialog.find('#lead_gen_type_yes, #lead_gen_type_no, #ActivityLog_notifyLender, #is_public').removeAttr('checked');   //clear all input execpt the hidden ones
			Dialog.find('h2.contact-name').html('');
			Dialog.find('div.errorMessage').html('').hide();
			Dialog.find('#url-wrapper').hide();
			Dialog.find('.error').removeClass('error');
            Dialog.find('.follow-up-task-row').hide();
            Dialog.find('#ActivityLog_addFollowupTask').attr("checked",false);
            Dialog.find('#ActivityLog_addFollowupTask').val(0);

			if($.inArray(componentTypeId,$showLeadGenTypes) == -1) {
				$('#ActivityLog_lead_gen_type_ma').val(0);
				$('#lead-gen-row').addClass('hidden');
			} else {
				$('#lead-gen-row').removeClass('hidden');
			}


			var DialogForm = Dialog.find('form#$this->formId');

			// Reset the action on each triggering attempt
			DialogForm.attr('action', '/$module/activityLog/add');
			//DialogForm.data('settings').validationUrl = DialogForm.attr('action');
			var DialogFormAction = DialogForm.attr('action');

            DialogForm.find('#ActivityLog_component_type_id').val(componentTypeId);
            $('#ActivityLog_followupTask_component_type_id').val(componentTypeId);

            DialogForm.find('#ActivityLog_component_id').val(componentId);
            $('#ActivityLog_followupTask_component_id').val(componentId);

			// If edit was clicked then we need to dynamically fill in the values for the popup
			var element = $(ele);
			if (element.hasClass('edit-activity-log-button')) {

				var activityLogId = element.attr('data');
				DialogForm.attr('action', '/$module/activityLog/edit/'+activityLogId);
				$('.follow-up-task-checkbox').hide();

				// Create the scenario specific text for Edit Activity
				$('span.ui-button-text:contains("Add Activity Log"), span.ui-button-text:contains("Complete Task & Log Activity")').html('Update Activity Log');
				$('span.ui-dialog-title:contains("Add New Activity Log"), span.ui-dialog-title:contains("Complete Task & Log Activity")').html('Edit Activity Log');

				var editDialogFormAction = DialogFormAction.replace('add', 'edit')+'/'+activityLogId;
				DialogForm.attr('action', editDialogFormAction);
				//DialogForm.data('settings').validationUrl = editDialogFormAction;

				$.post('/$module/activityLog/' + activityLogId, function(data) {
					data = jQuery.parseJSON(data);
		        	$('input[name*="ActivityLog[id]"]').val(data.id);
                    $('input[name*="ActivityLog[component_id]"]').val(data.component_id);
                    $('input[name*="ActivityLog[component_type_id]"]').val(data.component_type_id);
		        	$('input[name*="ActivityLog[activity_date]"]').val(data.activity_date);
		        	$('textarea[name*="ActivityLog[note]"]').val(data.note);
		        	$('textarea[name*="ActivityLog[private_note]"]').val(data.private_note);
		        	$('input[name*="ActivityLog[url]"]').val(data.url);
		        	$('select[name*="ActivityLog[task_type_id]"]>option[value="'+data.task_type_id+'"]').attr('selected', true);
		        	$("select#ActivityLog_task_type_id").trigger("liszt:updated");
				    $('input[name*="task_id"]').val(data.task_id);
                    $('#ActivityLog_followupTask_component_type_id').val(data.component_type_id);
		        	$('#ActivityLog_followupTask_component_id').val(data.component_id);

//		        	if(data.lead_gen_type_ma=="$leadGenFollowUp"){
//                        $('#lead_gen_type_follow_up').attr("checked",true);
//                        $('#lead_gen_type_yes').removeAttr("checked");
//                        $('#lead_gen_type_no').removeAttr("checked");
//                        $('#lead_gen_type_follow_up').val("$leadGenFollowUp");
//		        	} else

                    $('#ActivityLog_lead_gen_type_ma').val(data.lead_gen_type_ma);
		        	if(data.lead_gen_type_ma=="$leadGenYes" || data.lead_gen_type_ma=="$leadGenFollowUp") {
                        $('#lead_gen_type_yes').attr("checked",true);
//                        $('#lead_gen_type_follow_up').removeAttr("checked");
                        $('#lead_gen_type_no').removeAttr("checked");
                        $('#lead_gen_type_yes').val("$leadGenYes");
		        	} else if(data.lead_gen_type_ma=="$leadGenNo") {
                        $('#lead_gen_type_no').attr("checked",true);
                        $('#lead_gen_type_yes').removeAttr("checked");
//                        $('#lead_gen_type_follow_up').removeAttr("checked");
                        $('#lead_gen_type_no').val("$leadGenNo");
		        	}

		        	if(data.task_type_id==$taskTypeMarketing) {
                        $('#url-wrapper').show();
                        if(data.address !== '') {
                            $('#url-ad-search-link').attr('href','http://www.google.com/search?q=site:'+ data.description + '+' + data.address);
                        }
		        	}

		        	if(data.is_public=="1"){
                        $('#is_public').attr("checked",true);
                        $('#ActivityLog_is_public').val("1");
				        $('.private-note').show();
		        	}
		        	else {
                        $('#is_public').attr("checked",false);
				        $('.private-note').hide();
		        	}
		        	if(data.is_spoke_to=="1"){
                        $('#ActivityLog_is_spoke_to').attr("checked",true);
                        $('#ActivityLog_is_spoke_to').val("1");
		        	}
		        	if(data.asked_for_referral=="1"){
                        $('#ActivityLog_asked_for_referral').attr("checked",true);
                        $('#ActivityLog_asked_for_referral').val("1");
		        	}
				});
			} else if (element.hasClass('complete-task-button')) {

				var taskId = element.attr('data');
				var componentTypeId = element.attr('ctid');

				// Create the scenario specific text for Complete Task
				$('span.ui-button-text:contains("Update Activity Log"), span.ui-button-text:contains("Add Activity Log")').html('Complete Task & Add Activity Log');
				$('span.ui-dialog-title:contains("Edit Activity Log"), span.ui-dialog-title:contains("Add New Activity Log")').html('Complete Task & Log Activity');
				$('.follow-up-task-checkbox').show();

				$('.private-note, .send-updates-container').hide();
				$('select#ActivityLog_notify').val(null).trigger('liszt:updated');

				$.post('/$module/tasks/' + taskId, function(data) {
					data = jQuery.parseJSON(data);
					if(data.complete_date == true) {

					    //refresh grid + send message
					    Message.create("error","This Task has already been completed. Please refresh your Task list");
					    $("#$dialogId").dialog("close");
					    return;
					}
    				$('#ActivityLog_component_id').val(data.component_id);
                    $('#ActivityLog_followupTask_component_id').val(data.component_id);

		        	$('textarea[name*="ActivityLog[note]"]').val(data.description);

					var taskTypeDropDownLists = (jQuery.parseJSON('$taskTypeDropDownLists')).taskTypeDropDownLists;
					$('select#ActivityLog_task_type_id').html(taskTypeDropDownLists[componentTypeId]);
					$('select#ActivityLog_task_type_id').val(data.task_type_id);
		        	$("select#ActivityLog_task_type_id").trigger("liszt:updated");
					$('h2.contact-name').html(data.fullName);

		        	$('select[name*="ActivityLog[task_type_id]"]>option[value="'+data.task_type_id+'"]').attr('selected', true);

		        	if(data.task_type_id==$taskTypeMarketing) {
                        $('#url-wrapper').show();
                        if(data.address !== '') {
                            $('#url-ad-search-link').attr('href','http://www.google.com/search?q=site:'+ data.description + '+' + data.address);
                        }
		        	}
				});

	        	// Set the task id
				$('input[name*="task_id"]').val(taskId);

            // Add Activity Log handling
			} else {
				$('span.ui-button-text:contains("Complete Task & Add Activity Log"), span.ui-button-text:contains("Update Activity Log")').html('Add Activity Log');
				$('span.ui-dialog-title:contains("Edit Activity Log"), span.ui-dialog-title:contains("Complete Task & Log Activity")').html('Add New Activity Log');
				$('#ActivityLog_component_id').val($(ele).attr('data'));
				$('#ActivityLog_component_type_id').val($(ele).attr('ctid'));
                $('#lead_gen_type_no').removeAttr("checked");
                $('#lead_gen_type_yes').removeAttr("checked");
//                $('#lead_gen_type_follow_up').removeAttr("checked");
                $('input#task_id').val('');

			    if($(ele).data('ttid') !== "undefined") {
		        	$('select[name*="ActivityLog[task_type_id]"]>option[value="'+ $(ele).data('ttid') +'"]').attr('selected', true);
		        	$("select#ActivityLog_task_type_id").trigger("liszt:updated");
			    }
				$('.private-note, .send-updates-container').hide();
                $('select#ActivityLog_notify').val(null).trigger('liszt:updated');

			}

			if (element.hasClass('phone-call-button')) {
			    $('#ActivityLog_task_type_id').val($phoneTaskTypeId);
                $("select#ActivityLog_task_type_id").trigger("liszt:updated");
            }

			if (element.hasClass('lead-gen-follow-up-phone-call-button')) {
			    $('#ActivityLog_task_type_id').val($phoneTaskTypeId);
//			    $('#lead_gen_type_follow_up').click();
                $("select#ActivityLog_task_type_id").trigger("liszt:updated");
            }

			if (element.hasClass('lead-gen-new-phone-call-button')) {
			    $('#ActivityLog_task_type_id').val($phoneTaskTypeId);
			    $('#lead_gen_type_yes').click();
                $("select#ActivityLog_task_type_id").trigger("liszt:updated");
            }

			$('#ActivityLog_activity_date').val(getNowDateTime());
            // Reset the lead gen type "is contact" span
            var isSpokeTo = $('#ActivityLog_is_spoke_to');
            var askedForReferral = $('#ActivityLog_asked_for_referral');
            var isPublic = $('#ActivityLog_is_public');
			isSpokeTo.attr("checked",false);
			isSpokeTo.val("0");
			$("#isSpokeToLabel").hide();
			askedForReferral.attr("checked",false);
			askedForReferral.val("0");
			isPublic.attr("checked",false);
			isPublic.val("0");

			//Dialog.dialog('open');

			return false;
		};

		//----------------------------------------------------------------------

//        $('#ActivityLog_is_spoke_to, #ActivityLog_is_public, #ActivityLog_asked_for_referral').click(function(){
//			if ($(this).is(':checked'))
//				return $(this).val(1);
//
//			return $(this).val(0);
//        });

		var isSpokeTo = $('#ActivityLog_is_spoke_to');
		isSpokeTo.live('click', function() {
			if ($(this).is(':checked'))
				return $(this).val(1);

			return $(this).val(0);
		});

		var isPublic = $('#is_public');
		isPublic.live('click', function() {
			if ($(this).is(':checked')) {
				$('.private-note, .send-updates-container').show('normal');
				return $('#ActivityLog_is_public').val(1);
			}
			else {
				$('.private-note, .send-updates-container').hide('normal');
    			return $('#ActivityLog_is_public').val(0);
			}
		});

		$('#ActivityLog_addFollowupTask').live('click', function(){
			if ($(this).is(':checked')) {
		    	$('#ActivityLog_addFollowupTask').val(1);
                $('.follow-up-task-row').show('normal');
			}
			else {
                $('.follow-up-task-row').hide();
                $('#ActivityLog_addFollowupTask').val(0);
                $('#ActivityLog_followupTask_due_date').val('');
                $('#ActivityLog_followupTask_task_type_id').val('');
                $('#ActivityLog_followupTask_description').val('');
                $('#ActivityLog_followupTask_assigned_to_id').val($userId);
			}
		});

		var askedForReferral = $('#ActivityLog_asked_for_referral');
		askedForReferral.live('click', function() {
			if ($(this).is(':checked'))
				return $(this).val(1);

			return $(this).val(0);
		});

		var notifyLender = $('#ActivityLog_notifyLender');
		notifyLender.live('click', function() {
			if ($(this).is(':checked'))
				return $(this).val(1);

			return $(this).val(0);
		});

		var isLeadGen = $('select#ActivityLog_lead_gen_type_ma');
		isLeadGen.change(function(){
			if(isLeadGen.val() == 0) {
				$("#isSpokeToLabel").hide("normal");
			} else {
				$("#isSpokeToLabel").show("normal");
			}
		});
		$('#lead_gen_type_no, #lead_gen_type_yes').live('click', function(){
            if($(this).attr('id')=="lead_gen_type_no"){
                $('#ActivityLog_lead_gen_type_ma').val('$leadGenNo');
                $('#lead_gen_type_yes').removeAttr('checked');
//                $('#lead_gen_type_follow_up').removeAttr('checked');
            } else if($(this).attr('id')=="lead_gen_type_yes") {
                $('#ActivityLog_lead_gen_type_ma').val('$leadGenYes');
                $('#lead_gen_type_no').removeAttr('checked');
//                $('#lead_gen_type_follow_up').removeAttr('checked');
            }
//            else if($(this).attr('id')=="lead_gen_type_follow_up") {
//                $('#ActivityLog_lead_gen_type_ma').val('$leadGenFollowUp');
//                $('#lead_gen_type_yes').removeAttr('checked');
//                $('#lead_gen_type_no').removeAttr('checked');
//            }
        });

        $('#ActivityLog_task_type_id').change(function() {
            if($(this).val() == $taskTypeMarketing) {
                $('#url-wrapper').show('normal');
            }
            else {
                $('#url-wrapper').hide('normal');
                $('#ActivityLog_url').val('');
            }
        });

		//----------------------------------------------------------------------

		var getNowDateTime = function () {
			var d = new Date();
			var seconds = d.getSeconds();
			var minutes = d.getMinutes();
			var hours = d.getHours();
			var ampm;

			if (minutes < 10) { minutes = '0'+minutes; }
			if (hours > 12) {hours = hours-12; ampm = 'pm'; } else ampm = 'am';
			if (hours < 10) { hours = '0'+hours; }
			if (seconds < 10) { seconds = '0'+seconds; }

			return $.datepicker.formatDate('mm/dd/yy', new Date()) + ' ' + hours + ':' + minutes + ' ' + ampm;
		};
JS;

			Yii::app()->clientScript->registerScript('popupTrigger-' . $this->id, $js);
		}
	}
