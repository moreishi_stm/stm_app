<?php
Yii::app()->clientScript->registerCss('activityLogDialogCss', <<<CSS

    .ui-dialog .ui-dialog-content {
        padding-bottom: 0;
    }
CSS
);
Yii::app()->clientScript->registerScript('activityLogDialogScriptJs', <<<JS

    // Helper function
    doAddActivityDialog = function(ele) {

        // Create modal
        stmModal.create({
            title: 'Add Activity Log',
            content: $('#template-dialog-form-activity-log').html(),
            height: 370,
            width: 750,
            modalId: 'activity-log-dialog',
            successCallback: function() {
                if (document.getElementById("activity-log-grid")) {
                    $.fn.yiiGridView.update("activity-log-grid");
                }
                if (document.getElementById("task-grid")) {
                    $.fn.yiiGridView.update("task-grid");
                }
            },
            contentVariables: {
                buttonText: 'Add Activity Log'
            }
        });

        // Initialize other stuff
        initActivityLogDialog(ele);

        // Initialize date picker
        $('#ActivityLog_activity_date').datepicker();
        $('#ActivityLog_followupTask_due_date').datepicker();
    }

    // Add activity button
    $('.add-activity-log-button').live('click', function(e) {

        // Stop default behavior
        e.preventDefault();

        doAddActivityDialog(this);
    });

    // Edit activity button
    $('.edit-activity-log-button, .complete-task-button').live('click', function(e) {

        // Stop default behavior
        e.preventDefault();

        // Set window title
        var title = $(this).hasClass('complete-task-button') ? 'Complete Task & Log Activity' : 'Edit Activity Log';

        // Create modal
        stmModal.create({
            title: title,
            content: $('#template-dialog-form-activity-log').html(),
            height: 370,
            width: 750,
            modalId: 'activity-log-dialog',
            successCallback: function() {
                if (document.getElementById("activity-log-grid")) {
                    $.fn.yiiGridView.update("activity-log-grid");
                }
                if (document.getElementById("task-grid")) {
                    $.fn.yiiGridView.update("task-grid");
                }
            },
            contentVariables: {
                buttonText: 'Save Activity Log'
            }
        });

        // Initialize other stuff
        initActivityLogDialog(this);

        // Initialize date picker
        $('#ActivityLog_activity_date').datepicker();
        $('#ActivityLog_followupTask_due_date').datepicker();
    });
JS
);

//$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
//     'id'       => $this->id,
//     'options'  => array(
//     	'title'    => $this->title,
//		'autoOpen' => false,
//		'modal'    => true,
//		'width'    => 700,
//        'height'    => 'auto',
//        'draggable' => true,
//		'open'=>'js:function(){
//			$("#ActivityLog_activity_date").datepicker( "hide" );
//			$("#ActivityLog_task_type_id").focus();
//		}',
//		'buttons'  => array(
//			'Add Activity Log' => "js:function() {
//				$('#".$this->formId."').submit();
//			}",
//       ),
//     ),
//));
echo '<script id="template-dialog-form-activity-log" type="text/x-handlebars-template">';
$form=$this->beginWidget('CActiveForm', array(
    'id'=>$this->formId,
    'htmlOptions'   =>  array(
        'class' =>  'form-horizontal'
    ),
    'action'=>array('/'.Yii::app()->controller->module->name.'/activityLog/add'),
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>false,
    'clientOptions'=>array(
    	'validateOnChange'=>false,
    	'validateOnSubmit'=>true,
		'beforeValidate'    => 'js:function(form)
		{
	            $(".loading-container").addClass("loading");

	            // if all 3 radio buttons are unchecked and component Type Id is contact, buyer, seller, recruit
	            if(!$("#lead_gen_type_yes").is(":checked") && !$("#lead_gen_type_no").is(":checked") && ($("#ActivityLog_component_type_id").val() < 3 && $("#ActivityLog_component_type_id").val() == 14)) {
	                $("#ActivityLog_lead_gen_type_ma").val("");
	            }
				return true;
            }',
        'afterValidate'=>'js:function(form, data, hasError)
        {
            if (!hasError) { // Action was successful
                if(data.status == "success") {
                    Message.create("success", "Successful Activity Log update.");
                    $("#'.$this->id.'").dialog("close");
                    if (document.getElementById("activity-log-grid")) {
                        $.fn.yiiGridView.update("activity-log-grid");
        //              $.fn.yiiListView.update("sticky-task-list");
                    }
                    if (document.getElementById("task-grid")) {
                        $.fn.yiiGridView.update("task-grid");
                    }

                    // Remove the task from the list
                    var taskId = $("#'.$this->formId.'").find("input[name=task_id]").val();
    //                $("div.sticky-tasks-body").find("input[value="+taskId+"]").parent().remove();

                } else {
                    Message.create("error", "Error: Activity Log did not save.");
                }

                if(typeof data.error !== "undefined") {
                    Message.create("error", "Error: " + data.error);
                }
    			$(".loading-container").removeClass("loading");

                return false;
            }
			$(".loading-container").removeClass("loading");
        }',
    ),
));
?>
<div class="stm-modal-body">
    <h2 class="contact-name" style="margin:10px 0 10px 0; color: #58F;"></h2>

    <div id="activity-type-row" class="form-group">
        <?php echo $form->labelEx($model,'task_type_id', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'task_type_id', CHtml::listData(TaskTypeComponentLu::model()->byComponentType($parentModel->componentType->id)->findAll(), 'id', 'name'), $htmlOptions=array(
                'empty'=>'Select Activity',
                'class' =>  'form-control chzn-select'
            ));
            ?>
            <?php echo $form->error($model, 'task_type_id'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'activity_date', array(
            'class' =>  'col-sm-2 control-label'
        ));?>
        <div class="col-sm-10">
            <?php echo $form->textField($model, 'activity_date', array(
                'class'	=>	'form-control'
            )); ?>
            <?php echo $form->error($model, 'activity_date'); ?>
        </div>
    </div>

    <div id="lead-gen-row" class="form-group">
        <?php echo $form->labelEx($model,'lead_gen_type_ma', array(
            'class' =>  'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-10">
            <?php echo CHtml::radioButton('lead_gen_type_yes', false ,$htmlOptions=array(
                'value'=>ActivityLog::LEAD_GEN_YES_ID,
                'uncheckValue'=>"",
                'style'=>'margin-left: 20px;',
            )); ?> Yes<br>
            <?php echo CHtml::radioButton('lead_gen_type_no', false ,$htmlOptions=array(
                'value'=>ActivityLog::LEAD_GEN_NONE_ID,
                'uncheckValue'=>"",
                'style'=>'margin-left: 20px;',
            )); ?> No
            <?php echo $form->hiddenField($model, 'lead_gen_type_ma'); ?>

            <?php echo $form->error($model, 'lead_gen_type_ma'); ?>
        </div>
    </div>

    <div id="activity-type-row" class="form-group">
        <?php echo $form->labelEx($model,'is_spoke_to', array(
            'class' =>  'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->checkBox($model, 'is_spoke_to' ,$htmlOptions=array(
                'uncheckValue'=>'0',
                'style'=>'font-size:16px',
            )); ?> Yes <label id="isSpokeToLabel" display="none">=> Lead gen contact leads you closer to an appointment</label>
            <br>
            <?php echo $form->checkBox($model, 'asked_for_referral' ,$htmlOptions=array(
                'uncheckValue'=>'0',
                'style'=>'font-size:16px',
            )); ?>
            <?php echo $form->labelEx($model,'asked_for_referral',$htmlOptions=array('style'=>'padding-left:0; font-weight: normal;')); ?>
            <?php echo $form->error($model, 'asked_for_referral'); ?>
            <?php echo $form->error($model, 'is_spoke_to'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'note', array(
            'class' =>  'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textArea($model,'note', array(
                'placeholder'=>'Activity Notes',
                'class'=>'form-control',
                'rows'=>5,
            )); ?>
            <?php echo $form->error($model, 'note'); ?>
        </div>
    </div>

    <div class="private-note form-group" <?php echo ($model->private_note)? 'style="display: none;"' :''; ?>>
        <?php echo $form->labelEx($model,'private_note', array(
            'class' =>  'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textArea($model,'private_note', array(
                'placeholder'=>'Private Notes',
                'class'=>'form-control',
                'rows'=>2,
            )); ?>
            <?php echo $form->error($model, 'private_note'); ?>
        </div>
    </div>

    <div id="url-wrapper" style="display: none;" class="form-group">
        <?php echo $form->labelEx($model, 'url', array(
            'class' =>  'control-label col-sm-2'
        )); ?>
        <div class="col-sm-10">
            <?php echo $form->textField($model, 'url', array(
                'placeholder' => 'URL',
                'class' => 'form-control',
            )
            ); ?>
            <a id="url-ad-search-link" href="" target="_blank">Lookup ad on Google</a>
            <?php echo $form->error($model, 'url'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'is_public', array(
            'col-sm-2 control-label'
        )); ?>
        <div class="col-sm-offset-2 col-sm-10">
            <?php echo CHtml::checkBox('is_public', false ,$htmlOptions=array(
                'uncheckValue'=>'0',
                'style'=>'font-size:16px',
            )); ?> Notify / Share Notes<br>
            <?if($this->parentModel->componentType->id == ComponentTypes::RECRUITS):?>
                <div class="send-updates-container">
                    Send Update to:
                    <?php
                    $activeAssignments = CHtml::listData(Assignments::model()->findAllByAttributes(array('component_type_id'=>$this->parentModel->componentType->id,'component_id'=>$this->parentModel->id)), 'contact.id', 'contact.fullName');
                    $activeAdmin = CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName');
                    $notifyUserList = array_unique(CMap::mergeArray($activeAssignments, $activeAdmin));

                    echo $form->dropDownList($model, 'notify', $notifyUserList, array(
                        'empty'=>'',
                        'multiple' =>'multiple',
                        'data-placeholder'=>'Select Names',
                        'class' => 'chzn-select form-control'
                    ));

    //                $this->widget('admin_module.extensions.EChosen.EChosen', array(
    //                        'target' => 'select#ActivityLog_notify',
    //                        'options' => array('width'=>'300px')
    //                    )
    //                );
                    ?><br><span class="label">Choose from anyone assigned to this record.</span>
                </div>
            <?endif;?>

            <?php echo $form->error($model, 'is_public'); ?>
            <?php echo $form->hiddenField($model, 'is_public', $htmlOptions=array('value'=>0)); ?>
            <? if(in_array($model->component_type_id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))):?>
                <?php echo $form->checkBox($model, 'notifyLender' ,$htmlOptions=array(
                    'uncheckValue'=>'0',
                    'style'=>'font-size:16px;',
                )); ?> Notify Lender
                <?php echo $form->error($model, 'notifyLender'); ?>
            <?endif;?>
        </div>
    </div>

    <div class="follow-up-task-checkbox form-group">
        <label class="control-label col-sm-2">Add Follow-up</label>
        <div class="col-sm-10">
            <?php echo $form->checkBox($model, 'addFollowupTask', false ,$htmlOptions=array(
                'uncheckValue'=>'0',
                'style'=>'font-size:16px',
                'class'=>'form-control'
            )); ?>
            <?php echo $form->error($model, 'addFollowupTask'); ?>
        </div>
    </div>

    <div class="follow-up-task-row form-group" style="display: none;">
        <label class="control-label col-sm-2">Follow-up Date</label>
        <div class="col-sm-10">

            <?php echo $form->textField($model, 'followupTask[due_date]', array(
                'class'	=>	'form-control'
            )); ?>

            <?php /*$this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'attribute' => 'followupTask[due_date]',
                    'model' => $model,
                    'options' => array( // 'showAnim'=>'fold',
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:250px;',
                        // 'value'=>date('m/d/Y h:i a'), //moved to jquery
                        'readonly' => true,
                        ''
                    )
                )
            );*/
            ?>
            <?php echo $form->error($model, 'followupTask[due_date]'); ?>
        </div>
    </div>

    <div class="follow-up-task-row form-group" style="display: none;">
        <label class="control-label col-sm-2">Priority</label>
        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'followupTask[is_priority]', array(Tasks::PRIORITY_NORMAL=>'NORMAL',Tasks::PRIORITY_EOD=>'MEDIUM (Must Do TODAY)', Tasks::PRIORITY_ASAP=>'HIGH (ASAP! Drop Everything!)'),  $htmlOptions = array(
                    'class' =>  'form-control'
                )); ?>
        </div>
        <?php echo $form->error($model, 'followupTask[is_priority]'); ?>
    </div>

    <div class="follow-up-task-row form-group" style="display: none;">
        <label class="control-label col-sm-2">Task Type</label>
        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'followupTask[task_type_id]', CHtml::listData(TaskTypeComponentLu::getTaskTypes($parentModel->componentType->id), 'taskType.id', 'taskType.name'), $htmlOptions = array(
                'empty' => 'Select One',
                'class' =>  'form-control'
            )); ?>
        </div>
        <?php echo $form->error($model, 'followupTask[task_type_id]'); ?>
    </div>

    <div class="follow-up-task-row form-group" style="display: none;">
        <label class="control-label col-sm-2">Description</label>
        <div class="col-sm-10">
            <?php echo $form->textArea($model, 'followupTask[description]', array(
                'placeholder' => 'Say What Next? (Ex. "Ask them about how their vacation to Florida was and if they still plan on seeing homes next week.")',
                'class' => 'form-control',
                'rows' => 3,
            )
            ); ?>
            <?php echo $form->error($model, 'followupTask[description]'); ?>
        </div>
    </div>

    <div class="follow-up-task-row form-group" style="display: none;">
        <label class="control-label col-sm-2">Assigned to</label>
        <div class="col-sm-10">
            <?php echo $form->dropDownList($model, 'followupTask[assigned_to_id]', CHtml::listData(Contacts::model()->byActiveAdmins(false)->orderByName()->findAll(), 'id', 'fullName'), array(
                'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE
            )); ?>
            <?php echo $form->error($model, 'followupTask[assigned_to_id]'); ?>
        </div>
    </div>
</div>

<div class="stm-modal-footer">
    <button type='submit'>{{buttonText}}</button>
</div>

<?php echo $form->hiddenField($model, 'followupTask[component_id]');?>
<?php echo $form->hiddenField($model, 'followupTask[component_type_id]');?>

<?php echo $form->hiddenField($model, 'component_id');?>
<?php echo $form->hiddenField($model, 'component_type_id');?>
<?php echo $form->hiddenField($model, 'referrerUrl');?>
<?php echo CHtml::hiddenField('task_id');?>

<?php
$this->endWidget();
?>
<?='</script>'?>
