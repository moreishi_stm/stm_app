<?php

Yii::import('zii.widgets.CBreadcrumbs');

class StmBreadcrumbs extends CBreadcrumbs
{
	public $separator = null;
	public $tagName = 'ul';
	public $htmlOptions = array(
		'class'=>'breadcrumb'
	);

	public function __construct($owner=null)
	{
		if (Yii::app()->controller->id == 'transactions') {
			$homeLinkText = ucfirst($_GET['transactionType']);
			$homeLink = Yii::app()->homeUrl.'admin/'.$_GET['transactionType'];
		}else{
			$homeLinkText = (isset(Yii::app()->controller->displayName)) ? ucfirst(Yii::app()->controller->displayName) : ucfirst(Yii::app()->controller->id);
			// $homeLinkText = ucfirst(Yii::app()->controller->id);
			$homeLink = Yii::app()->homeUrl.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id;
		}

		$breadcrumbEntry = CHtml::tag('li', $htmlOptions=array(), '<a href="{url}">{label}</a>');
		$this->activeLinkTemplate = $this->inactiveLinkTemplate = $breadcrumbEntry;

		$this->homeLink = CHtml::tag('li', $htmlOptions=array(), '<a href="'.$homeLink.'">'.$homeLinkText.'</a>');
		return parent::__construct($owner);
	}
}