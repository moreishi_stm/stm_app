<?php

class StickyNotesWidget extends CWidget
{
	public function init()
	{
		$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_widgets.StickyNotesWidget.assets.css'), false, -1, AdminModule::REFRESH_CSS_ASSETS);
		Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'stickyNotes.css');

		$jsAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admin_widgets.StickyNotesWidget.assets.js'), false, -1, AdminModule::REFRESH_JS_ASSETS);
		Yii::app()->clientScript->registerScriptFile($jsAssetsUrl.DS.'stickyNotes.js');
	}

	public function run()
	{
		$Criteria = new CDbCriteria;
		$Criteria->condition = 'contact_id = :contact_id AND setting_id = :setting_id';
		$Criteria->params    = array(
			':contact_id' => Yii::app()->user->id,
			':setting_id' => Settings::SETTING_STICKY_NOTES_ID,
		);
		$SettingContactValue = SettingContactValues::model()->find($Criteria);

		$this->render('stickyNotesWidget', array('stickyNotes'=>$SettingContactValue->value));
	}
}