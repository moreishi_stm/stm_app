<?php

class ThemeManager extends CThemeManager
{
	/**
	 * default themes base path
	 */
	const DEFAULT_BASEPATH='themes';

	/**
	 * @var string the name of the theme class for representing a theme.
	 * Defaults to {@link CTheme}. This can also be a class name in dot syntax.
	 */
	public $themeClass='StmTheme';

	public $_basePath=array();
	protected $_baseUrl=null;


	/**
	 * @param string|int $name name of the theme to be retrieved or $id of the theme_id from cms_themes
	 * @return CTheme the theme retrieved. Null if the theme does not exist.
	 */
	public function getTheme($name)
	{
		$basePaths = $this->getBasePath();
		$paths = array();
		foreach($basePaths as $basePath) {
			$themePath=$basePath.DIRECTORY_SEPARATOR.$name;
			if(is_dir($themePath)) {
				$paths[] = $themePath;
			}
		}

		if(!empty($paths)) {
			$class=Yii::import($this->themeClass, true);
			return new $class($name,$paths,$this->getBaseUrl().'/'.$name);
		}

		return NULL;
	}

	/**
	 * @return array list of available theme names
	 */
	public function getThemeNames()
	{
		//@TODO: Why is this static?
		static $themes;

		if($themes===null) {
			$themes=array();
			$basePaths = $this->getBasePath();
			foreach($basePaths as $basePath) {
				$folder=@opendir($basePath);
				while(($file=@readdir($folder))!==false)
				{
					if($file!=='.' && $file!=='..' && $file!=='.svn' && $file!=='.gitignore' && is_dir($basePath.DIRECTORY_SEPARATOR.$file))
						$themes[]=$file;
				}
				closedir($folder);
			}
			$themes = sort(array_unique($themes));
		}
		return $themes;
	}

	/**
	 * @return string the base path for all themes. Defaults to "WebRootPath/themes".
	 */
	public function getBasePath()
	{
		$this->setBasePath(dirname(Yii::app()->getRequest()->getScriptFile()).DIRECTORY_SEPARATOR.self::DEFAULT_BASEPATH);
		return $this->_basePath;
	}

	/**
	 * @param string $value the base path for all themes.
	 * @throws CException if the base path does not exist
	 */
	public function setBasePath($value)
	{
		$count = count($this->_basePath);
		$path = realpath($value);
		if($this->_basePath!==false || is_dir($path)) {
			$this->_basePath[$count] = $path;
		}
		
		if(empty($this->_basePath)) {
			throw new CException(Yii::t('yii','No theme directories found: {dirs}.',array('{dirs}'=>$this->_basePath)));
		}
	}

	/**
	 * @return string the base URL for all themes. Defaults to "/WebRoot/themes".
	 */
	public function getBaseUrl()
	{
		if($this->_baseUrl===null)
			$this->_baseUrl=Yii::app()->getBaseUrl().'/'.self::DEFAULT_BASEPATH;
		return $this->_baseUrl;
	}

	/**
	 * @param string $value the base URL for all themes.
	 */
	public function setBaseUrl($value)
	{
		$this->_baseUrl=rtrim($value,'/');
	}
}
