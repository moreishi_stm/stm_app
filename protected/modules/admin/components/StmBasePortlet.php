<?php
Yii::import('zii.widgets.CPortlet');

class StmBasePortlet extends CPortlet {
	public $containerId;
	public $containerClass;
	public $handleIconCssClass;
	public $handleButtons = array();
	public $handleButtonBreak;
	public $handleTitle   = 'Widget';
	public $contentCssClass = '';

	public $decorationHtmlOptions;

	public function init() {
        if ($this->containerId) {
			$this->id = $this->containerId;
        }

		$this->htmlOptions = CMap::mergeArray($this->htmlOptions, array(
			'class'=>'widget '.$this->containerClass,
		));

		parent::init();
	}

	public function renderDecoration() {
        if ($this->handleIconCssClass) {
            $handleIcon = $this->getEmIcon($this->handleIconCssClass);
        }

		// Add the handle's icon if one is specified
		$handleContent = ( $handleIcon ) ? $handleIcon.$this->handleTitle : $this->handleTitle;

		// Break for new line for icons if specified by $handleButtonBreak = true
        if ($this->handleButtonBreak == true) {
			$handleContent .= '<br>';
        }

		echo CHtml::tag('h3', CMap::mergeArray($this->decorationHtmlOptions, array(
			'class'=>'handle',
		)), $handleContent.$this->renderHandleButtons());
	}

	public function renderHandleButtons() {
        if (!$this->handleButtons) {
			return;
        }

		// Create a string of button links
		$buttons = '';
		foreach ($this->handleButtons as $button) {
            if ($button['visible'] === false) {
                continue;
            }

            if ($button['iconCssClass']) {
				$buttonIcon = $this->getEmIcon('icon '.$button['iconCssClass']);
            }

			$buttonText = ( $buttonIcon ) ? $buttonIcon.$button['label'] : $button['label'];

            if ($button['display'] == 'inline') {
				$button['htmlOptions']['class'] .= ' inline-button';
            }

			if ($button['type'] == 'link') {
				$button['htmlOptions']['class'] .= ' text-button';
				$buttons .= CHtml::link($buttonText, $button['link'], $button['htmlOptions']);
			} else {
				$button['htmlOptions']['class'] .= ' text';
				$buttons .= CHtml::htmlButton($buttonText, CMap::mergeArray(array(
					'name' => $button['name'],
				), $button['htmlOptions']));
			}
		}

		return CHtml::tag('div', $htmlOptions=array(
			'class'=>'handleButtons'
		), $buttons);
	}

	public function getEmIcon($iconCssClass) {
		return CHtml::tag('em', $htmlOptions=array(
			'class'=>$iconCssClass
		), null);
	}
}
