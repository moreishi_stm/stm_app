<?php
// Import stuff we need
Yii::import('admin_module.components.StmDialer.Lists.Factory', true);
Yii::import('admin_module.components.StmDialerImport.Parser', true);

abstract class BaseImportController extends AdminController
{
    protected $_importLog = array();
    protected $_importErrorRows = array();
    protected $_startTime;
    protected $_startDateTime;
    protected $_duplicateCount = 0;
    protected $_contactMissingDataCount = 0;
    protected $_processedCount = 0;
    protected $_callListImportLog;
    protected $_activeMappings;
    protected $_rowNumber;

    protected $_componentTypeId;

    protected $_sourceCollection;
    protected $_contactTypeCollection;

    public static $allowedDupeNames = array('fsbo ad','property owner','no name','corporate owned','possible owner');

    protected $_logFileBaseName = NULL;

    protected $_requireSourceID = FALSE;
    protected $_requireCallList = FALSE;

    /**
     * Flag for whether phones were already import for the record. Gets reset every row to false.
     * @var boolean
     */
    protected $_phonesImported;

    protected $_transactionMap = array(
        ComponentTypes::BUYERS     =>  \Transactions::BUYERS,
        ComponentTypes::SELLERS    =>  \Transactions::SELLERS
    );

    /**
     * Call Lists
     *
     * @var CallLists
     */
    protected $_callList;

    /**
     * Mappings
     *
     * Available mapping options
     * @var array List of mappings
     */
    protected $_mappings;

    /**
     * Call List Mapper
     *
     * Maps component types to call list types
     * @var array Dictionary of mappings
     */
    protected $_callListMapper = array(
        ComponentTypes::BUYERS      =>  CallLists::CUSTOM_BUYERS,
        ComponentTypes::SELLERS     =>  CallLists::CUSTOM_SELLERS,
        ComponentTypes::CONTACTS    =>  CallLists::CUSTOM_CONTACTS,
        ComponentTypes::RECRUITS    =>  CallLists::CUSTOM_RECRUITS,
    );

    /**
     * Sources
     *
     * List of available sources
     * @var Array database sources
     */
    protected $_sources;
    protected $_sourceId;

    /**
     * Contacts Model
     *
     * @var Contacts model
     */
    protected $_contactModel;

    protected $_isAppointment = false;

    protected $_s3Client = null;
    protected $_s3Bucket;

    public $s3UploadLocation = '/import_files/uploaded/';

    protected function _importPrep() {
        return $this;
    }

    protected function _syncToS3($currentLocation, $newFileName, $target = NULL)
    {
        if(empty($target)) {
            $target = $this->s3UploadLocation;
        }

        if(!$this->_s3Client->doesObjectExist($target)) {
            if (!$this->_s3Client->createDirectory($target)) {
                return false;
            }
        }

        $fileAdded = $this->_s3Client->addFile($currentLocation, $target."/".$newFileName);

        if(!isset($fileAdded['ObjectURL']) || empty($fileAdded['ObjectURL'])) {

            return false;
        }

        return true;
    }

    protected function _readRows($file, $maxRows = NULL, $startLine = NULL, $asIndex = false, $returnCount = false, &$count = 0)
    {
        if(!file_exists($file)) {
            return false;
        }

        $fp = @fopen($file, 'r');

        if($fp === false) {
            return false;
        }

        $rowNumber = 0;
        $headerRow = array();

        $returnData = array();
        while (($data = fgetcsv($fp, null, ",",'"')) !== FALSE) {
            $rowNumber++;

            if(!empty($startLine)) {
                if($rowNumber < $startLine) {
                    continue;
                }
            }

            if(!empty($maxRows) && $rowNumber > $maxRows) {
                if(!$returnCount) {
                    break;
                } else {
                    continue;
                }
            }

            array_walk($data, function(&$value, $key) {
                // Removes any weird encoding
                $charset = mb_detect_encoding($value, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                if($charset !== 'UTF-8') {
                    $value = iconv($charset,'UTF-8//IGNORE', $value);
                }

                $value = preg_replace('/\s+/', ' ', $value);
                $value = str_replace(array("\r", "\n", "\t")," ",$value);
                $value = str_replace('"',"",$value);
            });

            list($isHeaderRow, $isHeaderRowMessage) = StmImportFunctions::isHeaderRow($data);

            if(!$asIndex) {
                // if its the first row grab the headers
                if($rowNumber == 1) {
                    $num = count($data);
                    for ($c=0; $c < $num; $c++) {
                        $headerRow[] = $data[$c];
                    }

                    continue;
                } // If it's not the first row


                $rowData = array();
                $i = 0;
                foreach ($headerRow as $c => $headerRowColumnName) {
                    $rowData[$headerRowColumnName] = $data[$i];
                    $i++;
                }

                $returnData[] = $rowData;
            } else {
                if($rowNumber == 1) {
                    continue;
                }
                $returnData[] = $data;
            }

        }
        @fclose($fp);

        $count = $rowNumber;

        return $returnData;
    }

    /**
     * Before Action
     *
     * Called before controller actions
     * @param CAction $action The action
     * @return bool (See parent for documentation)
     */
    public function beforeAction($action)
    {
        // Get sources
        $this->_sources = Sources::model()->byApplyAccountScope()->findAll();

        // Setup mappings
        $this->_mappings = \StmImportFunctions::getMappings();

        if(Yii::app()->controller->module->id == 'hq') {
            $hqMappings = array(
                'contactAttributes:' . ContactAttributes::HQ_BROKERAGE_NAME          => 'Brokerage Name',
                'contactAttributes:' . ContactAttributes::HQ_LISTING_COUNT           => 'Listing Count',
                'contactAttributes:' . ContactAttributes::HQ_WEBSITE                 => 'Agent Website URL',
                'contactAttributes:' . ContactAttributes::HQ_BROKERAGE_ID            => 'Brokerage ID',
                'contactAttributes:' . ContactAttributes::HQ_AGENT_ID                => 'Agent ID',
                'contactAttributes:' . ContactAttributes::HQ_CITY                    => 'Market City',
                'contactAttributes:' . ContactAttributes::HQ_STATE_ID                => 'Market State',
                'contactAttributes:' . ContactAttributes::HQ_ROLE                    => 'Role',
                'contactAttributes:' . ContactAttributes::HQ_REGION                  => 'KW Region',
                'contactAttributes:' . ContactAttributes::HQ_KW_MARKET_CENTER_NUMBER => 'KW Market Center #',
            );
            // merge here as it will get re-used in printing out error rows
            $this->_mappings['base'] = array_merge($this->_mappings['base'], $hqMappings);
            $mappings = array(
                ComponentTypes::CONTACTS    =>  $this->_mappings['base'],
            );
        }

        $this->_s3Bucket = (YII_DEBUG) ? "dev.client-private.seizethemarket.com" : "client-private.seizethemarket.com";

        // Require AWS S3 support - has to be done this way there are pathing issues inside YII
        require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmAws.S3')."/FileManagement.php");

        //@todo: dig through StmAws and see if it will fail on connecting to a bucket not available
        $this->_s3Client = new \StmAws\S3\FileManagement($this->_s3Bucket);

        // moved here to use in all actions
        $this->_componentTypeId = Yii::app()->request->getPost('componentTypeId');

        // Chain parent init
        return parent::beforeAction($action);
    }

    protected function _logError($errorMessage, $newContact)
    {
        $newContact['errorDesc'] = $errorMessage;

        // if there is already content in here, add a space or divider between error logs.
        if($this->_importErrorRows[$this->_rowNumber]) {
            // append to the existing error description
            $this->_importErrorRows[$this->_rowNumber]['errorDesc'] .= PHP_EOL.$errorMessage;
        }
        else {
            $this->_importErrorRows[$this->_rowNumber] = $newContact;
        }
    }

    /**
     * Send JSON
     *
     * Test function that should be moved up the chain of hierarchy later if found useful
     * @return void
     */
    protected function _sendJson($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    /**
     * Checks to see if value exists
     *
     * return boolean
     */
    protected function _checkExist($newContact, $fieldName, $existingData)
    {
        if(!$newContact[$fieldName]) {
            return false;
        }
        return array_key_exists(strtolower($newContact[$fieldName]), $existingData);
    }

    protected function _checkExistingAddress($newContact, $fieldName, $existingData)
    {
        if(!$newContact[$fieldName]) {
            return false;
        }
        return array_key_exists(strtolower(substr($newContact[$fieldName],0,8)), $existingData);
    }

    /**
     * Email Log
     *
     * Emails the log out
     * @return void
     */
    protected function _emailLog()
    {
        $now = microtime(true);
        $runtime = round($now - $this->_startTime, 2);

        $body = '';
        $body .= 'Please see below for import error logs. '.$this->_contactMissingDataCount.' Contacts had missing data and were not saved. Please review log and re-import after updating contact data or manually enter. '.$this->_duplicateCount.' duplicates have been found and not imported. The duplicate contacts have been logged below with full contact data.';

        $body .= '<br><br>This import was performed by '.Yii::app()->user->contact->fullName.' and completed on '.date('Y-m-d H:i:s').'<br><br>';

        if(is_array($this->_importLog) || is_array($this->_importErrorRows)) {
            $body .= '<br><br>---------------------------------------------------------------------------------------------------------------<br><br>';
            $body .= 'The text below are the rows of data that were not saved based on the errors stated above. This list is provided for your convenience only. It can be used to copy/paste into a text file and save as a CSV. Update the CSV data as needed and re-import if needed.';
            $body .= '<br><br>---------------------------------------------------------------------------------------------------------------<br><br>';
            $body .= '<br>Import Complete.<br>Duration: ' . $runtime . ' seconds';
            $body .= '<br><br>This is the end of your Import Log.';
            $body.= '<br><br>Thank you.<br><br>Seize the Market';

            // Create email message
            $mail = new StmZendMail();
            $mail->checkBounceUndeliverable = false;

            // Set parts
            $mail->setFrom('donotreply@seizethemarket.com', 'STM Notification');
            $toEmail = Yii::app()->user->contact->primaryEmail;

            /**
             * Multi-account hotfix - figure out better way to handle this
             */
            if(empty($toEmail)) {
                $contactId = Yii::app()->user;
                $toEmail = Yii::app()->db->createCommand("SELECT email FROM emails WHERE contact_id={$contactId} ORDER BY is_primary DESC, id ASC")->queryScalar();
            }
            $mail->addTo((YII_DEBUG || empty($toEmail)) ? StmMailMessage::DEBUG_EMAIL_ADDRESS : $toEmail);

            $mail->addBcc('support@seizethemarket.com');
            $mail->setSubject(ComponentTypes::getNameById($this->_componentTypeId).' Import Log - ' . Yii::app()->user->domain->name . ' @ '.date('Y-m-d H:i:s')); //.' - '.$this->_callListImportLog->description
            $mail->setBodyHtml(((strpos($body, '<html>') === false || strpos($body, '<html>') > 5))? nl2br($body) : $body);

            // Create attachment for CSV file of failed imports
            if($this->_importErrorRows) {

                // Create and open temp file
                $fileName = '/tmp/'.$this->_logFileBaseName . uniqid() . '.csv';
                if(($handle = fopen($fileName, "w")) !== FALSE) {

                    // print first header row of data
                    $headerRow = array();
                    $mapTypeToMerge = ComponentTypes::getNameById($this->_componentTypeId);

                    if(!$this->_isAppointment) {
                        $headerMap = CMap::mergeArray($this->_mappings['base'], $this->_mappings[$mapTypeToMerge]);
                    } else {
                        $headerMap = CMap::mergeArray($this->_mappings['base'], $this->_mappings['appointments']);
                    }

                    foreach($this->_activeMappings as $headerItem) {
                        if(!empty($headerMap[$headerItem]) && $headerMap[$headerItem] != '-') {
                            $headerRow[] = $headerMap[$headerItem];
                        }
                    }
                    $headerRow[] = 'Import Log Notes';
                    fputcsv($handle, $headerRow);

                    // Add each CSV entry to the file
                    foreach($this->_importErrorRows as $errorRow) {
                        if(isset($errorRow['sourceId'])) {
                            unset($errorRow['sourceId']);
                        }
                        fputcsv($handle, $errorRow);
                    }

                    // Close file handle
                    fclose($handle);

                    // Add CSV attachment to email
                    $mail->createAttachment(file_get_contents($fileName), 'text/csv', Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, 'import-log-' . date('Y-m-d_H.i.s') . '-' .$this->_callListImportLog->description . '.csv');

                    // Delete file
                    unlink($fileName);
                }
            }

            // Attempt to send email message
            try {
                $mail->send(StmZendMail::getAwsSesTransport());
            }
            catch(Exception $e) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') An error has occurred while attempting to send a dialer import log (Zend Mail). Code: "' . $e->getCode() . '" Exception Message:' . $e->getMessage().PHP_EOL, CLogger::LEVEL_ERROR);
            }
        }
    }

    public function printGridActions($data)
    {
        if(in_array($data->status, array('Error', 'Reconcile'))) {
            echo (($data->collisions > 0) ? '<a href="/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/reconcile/' . $data->id . '" title="Reconcile" class="btn"><i class="fa fa-2x fa-compress" aria-hidden="true"></i>&nbsp;Reconcile</a>' : '')
                .
                (($data->errors > 0) ? '<a href="/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/downloadErrors/' . $data->id . '" title="Download" class="btn"><i class="fa fa-2x fa-download" aria-hidden="true"></i>&nbsp;Download Import Log</a>' : '');
        }

        if(in_array($data->status, array('Completed', 'Reconcile Completed'))) {
            echo '<a href="/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/view/' . $data->id . '" title="View" class="btn"><i class="fa fa-2x fa-eye" aria-hidden="true"></i>&nbsp;View</a>';
        }
    }

    /**
     * Action Process File
     *
     * Step 1 of Dialer Import. This now syncs the file to S3 and adds it to the import_reconcile_files table, queue
     * @throws Exception When issues have come up with a file upload
     * @return void
     */
    public function actionPreviewfile()
    {
        // this setting is required for Mac environment
        ini_set("auto_detect_line_endings", true);

        $type = "contacts-file";

        $postType = Yii::app()->request->getPost('type');

        if(!empty($postType)) {
            $type = $postType."-file";
        }
        // Make sure we had a file upload
        if(!$_FILES[$type]) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'No file upload detected'
            ));
        }

        // Make sure it is actually an uploaded file and not some hack
        if(!is_uploaded_file($_FILES[$type]['tmp_name'])) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'Please select a valid file upload to continue.'
            ));
        }

        // Use file info extension to make sure the file is a proper csv file and not some weird file, we can't process excel files
        $info = new finfo(FILEINFO_MIME_TYPE);
        $approvedFileTypes = array('text/plain','text/csv','text/tsv','text/x-fortran', 'text/x-c', 'application/octet-stream');
        if(!in_array($info->file($_FILES[$type]['tmp_name']), $approvedFileTypes) && !in_array($_FILES[$type]['type'], $approvedFileTypes)) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'The selected file is a file type: '.$info->file($_FILES[$type]['tmp_name']).' and is not a proper csv file. Please open the file and "Save as" a csv to ensure it is a plain text/csv file. For further support, screenshot this and email it to support@seizethemarket.com.'
            ));
        }

        $newFileName = date("Y-m-d", time())."-".time()."-".ComponentTypes::getComponentNameFromID($this->_componentTypeId).".csv"; //@TODO: Check ext.

        $synced = $this->_syncToS3($_FILES[$type]['tmp_name'], $newFileName, Yii::app()->user->clientId."/".Yii::app()->user->accountId."/".$this->s3UploadLocation);

        if(!$synced) {
            clearstatcache();
            @unlink($_FILES[$type]['tmp_name']);
            unset($_FILES);
            clearstatcache();
            gc_enable();
            gc_collect_cycles();

            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'There was an error uploading the file. Please try again. #14055'
            ));
        }

        $importAsNotes = Yii::app()->request->getPost('importAsNotes');
        $callListId = Yii::app()->request->getPost('callListId');
        if($this->_requireCallList) {
            if ($callListId) {

                // Find existing call list
                $callList = CallLists::model()->findByAttributes(array(
                    'id' => $callListId
                ));

                if (!$callList) {
                    $this->_sendJson(array(
                        'status' => 'error',
                        'message' => 'There was an error uploading the file. Please try again. #14056'
                    ));
                }
            } else {
                // check to see if call list name is duplicate, if so return error message
                $callListExists = CallLists::model()->findByAttributes(array(
                    'name' => Yii::app()->request->getPost('listName')
                ));
                if ($callListExists) {
                    $this->_sendJson(array(
                        'status' => 'error',
                        'message' => 'A list with the name ' . Yii::app()->request->getPost('listName') . ' already exists'
                    ));
                }

                $this->_componentTypeId = Yii::app()->request->getPost('componentTypeId');
                if (!$this->_componentTypeId) {
                    $this->_sendJson(array(
                        'status' => 'error',
                        'message' => 'Component Type missing.'
                    ));
                }

                $this->_sourceId = Yii::app()->request->getPost('sourceId');
                if (!$this->_sourceId && $this->_requireSourceID) {
                    $this->_sendJson(array(
                        'status' => 'error',
                        'message' => 'Source ID missing.'
                    ));
                }

                // Create new call list
                $callList = new CallLists();
                $callList->preset_ma = $this->_callListMapper[$this->_componentTypeId];
                $callList->name = Yii::app()->request->getPost('listName');
                $callList->requeue_timeframe = CallLists::REQUEUE_3HOURS;
                $callList->added = date('Y-m-d H:i:s');

                if (!$callList->save()) {
                    $this->_sendJson(array(
                        'status' => 'error',
                        'message' => 'There was an error saving the call list. Please contact support. #14677'
                    ));
                }
            }
            $this->_callList = $callList;


            // start logging
            $this->_callListImportLog = new CallListImportLog();
            $this->_callListImportLog->setAttributes(array(
                'description' => Yii::app()->request->getPost('importDescription'),
                'call_list_id' => $callList->id,
                'start' => $this->_startDateTime,
                'added_by' => Yii::app()->user->id,
            ));

            if(!$this->_callListImportLog->save()) {
                $this->_sendJson(array(
                    'status'    =>  'error',
                    'message'   =>  'There was an error saving the call list. Please contact support. #14678' // should use a different message but we don't want the user to know!
                ));
            }
        }

        $importFileObj = new ImportReconcileFiles();
        $importFileObj->account_id = Yii::app()->user->accountId;
        $importFileObj->title = Yii::app()->request->getPost('importDescription');
        $importFileObj->type = (!empty($importAsNotes) && $importAsNotes > 0) ? $importAsNotes : $this->_componentTypeId;
        $importFileObj->file_name = $newFileName;
        $importFileObj->added_by = Yii::app()->user->id;
        $importFileObj->added = date("Y-m-d H:i:s", time());
        $importFileObj->status = "New";
        $importFileObj->ignore_dupe_phones = Yii::app()->request->getPost('ignoreDupePhones',0);
        $importFileObj->ignore_dupe_address = Yii::app()->request->getPost('ignoreDupeAddress',0);

        if(!empty($this->_callList)) {
            $importFileObj->call_list_id = $this->_callList->id;
        }

        if(!empty($this->_sourceId)) {
            $importFileObj->sourceId = $this->_sourceId;
        }

        $saved = $importFileObj->save();

        if(!$saved) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'There was an error uploading the file. Please try again. #14812' // should use a different message but we don't want the user to know!
            ));
        }

        // Generate CSV data
        $count = 0;
        $csvData = $this->_readRows($_FILES[$type]['tmp_name'], 4, NULL, TRUE, TRUE, $count);

        $importFileObj->total_rows = $count;
        $saved = $importFileObj->save();

        if(!$saved) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'There was an error uploading the file. Please try again. #14813' // should use a different message but we don't want the user to know!
            ));
        }

        clearstatcache();
        @unlink($_FILES[$type]['tmp_name']);
        unset($_FILES);
        clearstatcache();
        gc_enable();
        gc_collect_cycles();

        // Simply return the contents for the view to process
        $this->_sendJson(array(
            'status'    =>  'success',
            'data'      =>  $csvData,
            'import_id' => $importFileObj->id
        ));
    }

    public function actionImport()
    {
        if (YII_DEBUG) {
            ini_set('memory_limit', '500M');
        }

        $this->_startTime = microtime(true);
        $this->_startDateTime = date('Y-m-d H:i:s');

        $import_id = Yii::app()->request->getPost('import_id');

        if(empty($import_id)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => "Missing import ID. Please try again."
            ));
        }

        $import = ImportReconcileFiles::model()->findByPk($import_id);

        if(empty($import)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => "Could not find import file. Please try again."
            ));
        }

        $type = Yii::app()->request->getPost('type');
        // Get the selected import description
        $importDescription = Yii::app()->request->getPost('importDescription');
        if(!$importDescription && $type != 'appointments') {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'Import Description missing.'
            ));
        }

        // Get the selected source
        if(!$this->_componentTypeId && $type != 'appointments') {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'Component Type missing.'
            ));
        }

        // Retrieve mappings
        $mappings = Yii::app()->request->getPost('mappings');
        if(!$mappings) {
            $this->_sendJson(array(
                'status'    =>  'error',
                'message'   =>  'No mappings have been found'
            ));
        }

        $import->field_map = $mappings;
        $import->status = 'Scheduled';
        $import->sourceId = Yii::app()->request->getPost('sourceId');
        $import->scheduled = date("Y-m-d H:i:s", time());
        $import->save();

        // End
        $this->_sendJson(array(
            'status' => 'success'
        ));
    }

    public function actionReconcile($id)
    {
        $this->title = 'Import Reconciliation';

        if(empty($id)) {
            Yii::app()->user->setFlash('error', 'File ID could not be found.');
            return $this->redirect("/admin/import/files");
        }

        $reconcileFiles = ImportReconcileFiles::model()->findByPk($id);

        if(!$reconcileFiles) {
            Yii::app()->user->setFlash('error', 'File not be found.');
            return $this->redirect('/admin/import/files');
        }

        if($reconcileFiles->collisions < 1 || !in_array($reconcileFiles->status, array('Error', 'Reconcile'))) {
            Yii::app()->user->setFlash('error', 'File does not need to be reconciled.');
            return $this->redirect('/admin/import/files');
        }

        $Criteria = new CDbCriteria();
        $Criteria->condition = "file_id = :file_id AND error_type = :error_type";
        $Criteria->params = array(
            ':file_id' => $id,
            ':error_type' => 'Collision'
        );
        $Criteria->order = "id asc";

        $ImportReconcileData = ImportReconcileData::model()->findAll($Criteria);

        Yii::app()->clientScript->registerScriptFile('//cdn.datatables.net/u/dt/dt-1.10.12,fc-3.2.2,fh-3.1.2,r-2.1.0,rr-1.1.2,sc-1.4.2/datatables.min.js', CClientScript::POS_END);

        return $this->render('reconcileDisplayRows', array(
            'ImportReconcileData' => $ImportReconcileData,
            'reconcile_file_id' => $id,
            'view_only' => 'false'
        ));
    }

    public function actionFiles()
    {
        $this->title = 'Import Controller';
        $Criteria = new CDbCriteria();
        $Criteria->order = "added desc";

        $dataProvider = new CActiveDataProvider('ImportReconcileFiles', array(
            'criteria' => $Criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

        return $this->render('reconcileSelectFile', array(
            'dataProvider' => $dataProvider
        ));
    }

    public function actionView($id) {
        if(empty($id)) {
            Yii::app()->user->setFlash('error', 'File ID could not be found.');
            return $this->redirect("/admin/import/files");
        }

        $reconcileFiles = ImportReconcileFiles::model()->findByPk($id);

        if(!$reconcileFiles) {
            Yii::app()->user->setFlash('error', 'File not be found.');
            return $this->redirect('/admin/import/files');
        }

        if($reconcileFiles->status != "Complete" && $reconcileFiles->status != "Reconcile Completed") {
            Yii::app()->user->setFlash('error', 'File not completed.');
            return $this->redirect('/admin/import/files');
        }

        $Criteria = new CDbCriteria();
        $Criteria->condition = "file_id = :file_id AND error_type = :error_type";
        $Criteria->params = array(
            ':file_id' => $id,
            ':error_type' => 'Collision'
        );
        $Criteria->order = "id asc";

        $ImportReconcileData = ImportReconcileData::model()->findAll($Criteria);

        Yii::app()->clientScript->registerScriptFile('//cdn.datatables.net/u/dt/dt-1.10.12,fc-3.2.2,fh-3.1.2,r-2.1.0,rr-1.1.2,sc-1.4.2/datatables.min.js', CClientScript::POS_END);

        return $this->render('reconcileDisplayRows', array(
            'ImportReconcileData' => $ImportReconcileData,
            'reconcile_file_id' => $id,
            'view_only' => TRUE
        ));
    }

    public function actionDownloadErrors($id) {

        if(empty($id)) {
            Yii::app()->user->setFlash('error', 'File ID could not be found.');
            return $this->redirect("/admin/import/files");
        }

        $reconcileFiles = ImportReconcileFiles::model()->findByPk($id);

        if(!$reconcileFiles) {
            Yii::app()->user->setFlash('error', 'File not be found.');
            return $this->redirect('/admin/import/files');
        }

        if($reconcileFiles->errors < 1) {
            Yii::app()->user->setFlash('error', 'There were no errors to export for the selected file.');
            return $this->redirect('/admin/import/files');
        }

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: text/csv; charset=utf-8");
        header( "Content-Disposition: inline; filename=\"Errors-For-".$reconcileFiles->file_name."\"");

        $rows = ImportReconcileData::model()->findAllByAttributes(array('file_id' => $id, 'error_type' => 'error'));

        if(!$rows) {
            Yii::app()->user->setFlash('error', 'There were no errors to export for the selected file.');
            return $this->redirect('/admin/import/files');
        }

        $output = fopen('php://output', 'w');
        $i=0;
        foreach($rows as $row) {
            $fields = json_decode($row->data, true);
            if($i == 0) {
                foreach($fields as $key => $value) {
                    $headers[] = $key;
                }
                $headers[] = 'Reason';
                fputcsv($output, $headers,',','"');
            }

            $fields[] = $row->reason;
            fputcsv($output, array_values($fields),',','"');
            $i++;
        }
        Yii::app()->end();
    }

    public function actionGetColissionData($id) {
        if(empty($id)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Missing Row ID'
            ));
        }

        $reconcileDataRow = ImportReconcileData::model()->findByPk($id);

        if(empty($reconcileDataRow)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Could not find row for ImportReconcileData ID: '.$id
            ));
        }

        $collisionsByType = json_decode($reconcileDataRow->collisions, true);

        $allMatches = array();
        $matchesShown = array();
        foreach($collisionsByType as $type => $collisions) {
            $matches = Contacts::model()->findAllByPk($collisions);

            if (!empty($matches)) {
                foreach ($matches as $match) {

                    if(in_array($match->id, $matchesShown)) {
                        // Already shown match do't display again.
                        continue;
                    }

                    //$email = "";
                    if ($match->emails) {
                        foreach ($match->emails as $key => $EmailModel) {
                            if ($EmailModel->email) {
                                /*$primaryText = ($match->getPrimaryEmail() == $EmailModel->email) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
                                $email .= '<div>';
                                $email .= $EmailModel->email . $primaryText;
                                $statusAlert = '';
                                if ($EmailModel->email_status_id == EmailStatus::OPT_OUT) {
                                    $statusAlert = 'Opted-out';
                                } elseif ($EmailModel->email_status_id == EmailStatus::HARD_BOUNCE) {
                                    $statusAlert = 'Bounced';
                                }
                                if ($EmailModel->getIsUniversallyUndeliverable()) {
                                    $statusAlert .= ($statusAlert) ? ', ' : '';
                                    $statusAlert .= 'Undeliverable';
                                }
                                if ($statusAlert) {
                                    $email .= '<div class="errorMessage" style="display:inline-block;">&nbsp;' . $statusAlert . '</div>';
                                }
                                $email .= '</div>';*/
                                $allMatches[$match->id]['data']['emails'][] = $EmailModel->email;
                            }
                        }
                    }

                    // If we had no emails add the empty value
                    if(empty($allMatches[$match->id]['data']['emails'])) {
                        $allMatches[$match->id]['data']['emails'][] = 'None Found';
                    }

                    //$addressesHtml = "";
                    $allMatches[$match->id]['data']['addresses'][] = 'None Found';
                    if ($match->addresses) {
                        $primaryAddress = $match->getPrimaryAddress();
                        foreach ($match->addressContactLu as $AddressContactLu) {
                            /*$primaryText = ($primaryAddress->id == $AddressContactLu->address->id) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
                            $addressesHtml .= CHtml::tag('div', $htmlOptions = array(), $AddressContactLu->address->address);
                            $addressesHtml .= CHtml::tag('div', $htmlOptions = array(), $AddressContactLu->address->city . ', ' . AddressStates::getShortNameById($AddressContactLu->address->state_id). ' ' . $AddressContactLu->address->zip.$primaryText);*/
                            $allMatches[$match->id]['addresses'][] = $AddressContactLu->address->address." ".$AddressContactLu->address->city.", ".AddressStates::getShortNameById($AddressContactLu->address->state_id). ' ' . $AddressContactLu->address->zip;
                        }
                    } /*else {
                        $addressesHtml .= 'None Found';
                    }*/

                    // If we had no addresses add the empty value
                    if(empty($allMatches[$match->id]['data']['addresses'])) {
                        $allMatches[$match->id]['data']['addresses'][] = 'None Found';
                    }

                    //$phonesHtml = "";
                    $hidePhones = (Yii::app()->user->settings->hide_phone_numbers_search_results) ? true : false;
                    if ($match->phones) {
                        $primaryPhone = $match->getPrimaryPhone();
                        foreach ($match->phones as $PhoneModel) {
                            //$primaryText = ($primaryPhone == $PhoneModel->phone) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
                            if ($PhoneModel->phone) {
                                $opt = array('extension' =>"");
                                if ($PhoneModel->extension) {
                                    $opt = array('extension' => $PhoneModel->extension);
                                }
                                /*$phonesHtml .= (($hidePhones) ? '***-***-**'.substr($PhoneModel->phone, -2, 2) : Yii::app()->format->formatPhone($PhoneModel->phone, $opt)  .$primaryText);

                                if ($PhoneModel->phone_type_ma > 1) {
                                    $phonesHtml .= ' (' . $PhoneModel->phoneTypes[$PhoneModel->phone_type_ma] . ')';
                                }
                                if($AreaCodeInfo = $PhoneModel->getAreaCodeInfo()) {
                                    $phoneText = '<br />';
                                    $phoneText .= ($AreaCodeInfo->city)?$AreaCodeInfo->city.', ':'';
                                    $phoneText .= $AreaCodeInfo->state.'<br />';
                                    $phonesHtml .= $phoneText;
                                }
                                $phonesHtml .= '<br />';*/
                                $allMatches[$match->id]['data']['phones'][] = ($hidePhones) ? '***-***-**'.substr($PhoneModel->phone, -2, 2) : Yii::app()->format->formatPhone($PhoneModel->phone, $opt);
                            }
                        }
                    }

                    // Set this empty if we had none
                    if(empty($allMatches[$match->id]['data']['phones'])) {
                        $allMatches[$match->id]['data']['phones'][] = 'None Found';
                    }

                    $selected = false;
                    if($reconcileDataRow->merge_with == $match->id) {
                        $selected = true;
                    }

                    $allMatches[$match->id]['selected'] = $selected;
                    $allMatches[$match->id]['name'] = $match->contact->fullName;

                    /*$allMatches .= '
<div class="res-col p-0 p-fl" style="margin: 0 4px; width: 250px;">
    <div class="widget g12 p-0 p-fr match '.(($selected) ? ' selected-match' : '').'" id="contact-'.$match->id.'" data-pk="'.$match->id.'" data-file-id="'.$reconcileDataRow->file_id.'">
        <h3 class="handle">
            <i class="fa fa-2x fa-check-circle'.(($selected) ? '' : ' hidden').'" style="color: #5cb85c; position: absolute; left: 3px; top: 2px;"></i>
            <em class="i_user"></em>Contact Info
        </h3>
        <div class="rounded-text-box odd-static">
            <div class="g12">
                <table class="container">
                    <tbody>
                    <tr>
                        <th>Contact ID:</th>
                        <td>'.$match->contact->id.'</td>
                    </tr>
                    <tr>
                        <th>Name:</th>
                        <td>'.$match->contact->fullName.'</td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>
                            <div style="font-weight: bold;">'.$email.'</div>
                        </td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>
                        '.$phonesHtml.'
                        </td>
                    </tr>
                    <tr>
                        <th>Address:</th>
                        <td>
                            '.$addressesHtml.'
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
';*/

                    $matchesShown[] = $match->id;
                }
            }
        }

        $this->_sendJson(array('status' => 'success', 'matches' => $allMatches, 'id' => $reconcileDataRow->id));
    }

    public function actionUpdateCollision($id) {
        if(empty($id)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Missing Row ID'
            ));
        }


        $ids = array();
        if($id !== "ALL") {
            $reconcileDataRows = ImportReconcileData::model()->findAllByPk($id);
        } else {
            $ids =  Yii::app()->request->getParam('ids', NULL);

            if(empty($ids)) {
                $this->_sendJson(array(
                    'status' => 'error',
                    'message' => 'You must select at least 1 row.'
                ));
            }

            $criteria = new CDbCriteria();
            $criteria->addInCondition("id", $ids);
            $reconcileDataRows = ImportReconcileData::model()->findAll($criteria);
        }

        if(empty($reconcileDataRows)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Could not find row for ID: '.$id
            ));
        }

        $action = Yii::app()->request->getParam('action', NUILL);

        if(empty($action)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'No action was passed.'
            ));
        }

        $pkToUse= Yii::app()->request->getParam('pk', NUILL);

        if($action == 'Merge' && empty($pkToUse)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Could not merge no ID was passed.'
            ));
        }

        $updatedIds = array();
        foreach($reconcileDataRows as $reconcileDataRow) {
            $reconcileDataRow->action = $action;
            $reconcileDataRow->merge_with = $pkToUse;

            if (!$reconcileDataRow->save()) {
                $this->_sendJson(array(
                    'status' => 'error',
                    'message' => 'There has been an error. Please try again.',
                    'reasons' => $reconcileDataRow->getErrors()
                ));
            }
            $updatedIds[] = $reconcileDataRow->id;
        }

        $this->_sendJson(array(
            'status' => 'success',
            'id' => $id,
            'ids' => $updatedIds,
            'passedIds' => $ids
        ));
    }

    public function actionIgnoreCollision($id) {
        if(empty($id)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Missing Row ID'
            ));
        }

        $updatedIds = $ids = array();

        if($id !== "ALL") {
            $reconcileDataRows = ImportReconcileData::model()->findAllByPk($id);
        } else {
            $ids =  Yii::app()->request->getParam('ids', NULL);

            if(empty($ids)) {
                $this->_sendJson(array(
                    'status' => 'error',
                    'message' => 'You must select at least 1 row.'
                ));
            }

            $criteria = new CDbCriteria();
            $criteria->addInCondition("id", $ids);
            $reconcileDataRows = ImportReconcileData::model()->findAll($criteria);
        }

        if(empty($reconcileDataRows)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Could not find reconcile data for ID: '.$id
            ));
        }

        foreach($reconcileDataRows as $reconcileDataRow) {
            $reconcileDataRow->action = 'Ignore';
            $reconcileDataRow->merge_with = NULL;

            if (!$reconcileDataRow->save()) {
                $this->_sendJson(array(
                    'status' => 'error',
                    'message' => 'There has been an error. Please try again.',
                    'reasons' => $reconcileDataRow->getErrors()
                ));
            }

            $updatedIds[] = $reconcileDataRow->id;
        }

        $this->_sendJson(array(
            'status' => 'success',
            'id' => $id,
            'ids' => $updatedIds,
            'passedIds' => $ids
        ));
    }

    public function actionScheduleReconcileImport($id) {
        if(empty($id)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Missing Row ID'
            ));
        }

        $reconcileFile = ImportReconcileFiles::model()->findByPk($id);

        if(empty($reconcileFile)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'Could not find row for ID: '.$id
            ));
        }

        $criteria = new CDbCriteria();
        $criteria->condition = "file_id = :file_id AND action = 'Not Selected' AND error_type='Collision'";
        $criteria->params = array(
            ':file_id' => $reconcileFile->id
        );

        $reconcileData = ImportReconcileData::model()->count($criteria);
        if(!empty($reconcileData)) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'There are one or more rows that need to be reconciled.'
            ));
        }

        $reconcileFile->reconciled_scheduled = date("Y-m-d H:i:s", time());
        $reconcileFile->status = 'Reconcile Import Ready';

        if(!$reconcileFile->save()) {
            $this->_sendJson(array(
                'status' => 'error',
                'message' => 'There has been an error. Please try again.',
                'errors' => (YII_DEBUG) ? $reconcileFile->getErrors() : array()
            ));
        }

        $this->_sendJson(array(
            'status' => 'success',
        ));
    }
}