<?php
//	if(YII_DEBUG)
//		set_include_path(get_include_path().':/Applications/MAMP/bin/php/php5.4.4/lib/php/');
	require_once(dirname(__FILE__).'/plivo.php');

	class StmPlivo extends CApplicationComponent {

        /**
         * Auth ID
         *
         * @var string API Authentication ID
         */
        public $auth_id;

        /**
         * Auth Token
         *
         * @var string API Authentication token
         */
        public $auth_token;

        /**
         * Primary Phone
         *
         * @var string Primary Plivo registered phone number to use
         */
        public $primary_phone;

        /**
         * Client
         *
         * @var \StmPlivoFunctions\RestAPI Plivo API library
         */
        protected $_client;

        /**
         * Init
         */
        public function init()
		{
			// Singleton design pattern
			if(!($this->_client instanceof \StmPlivoFunctions\RestAPI)) {
				$this->_client = new \StmPlivoFunctions\RestAPI($this->auth_id, $this->auth_token);
			}

            // Chain parent init
            parent::init();
		}

        /**
         * Get Plivo
         *
         * Returns an instance of the Plivo object!
         * @return \StmPlivoFunctions\RestAPI
         */
        public function getPlivo()
        {
            return $this->_client;
        }

        /**
         * Make Call
         *
         * Makes a phone call
         * @return string
         */
        public function call() {
			$params = array(
				'to' => 'sip:clee130902023454@phone.plivo.com',
				'from' => '9042803315',
				'ring_url' => 'http://plivodirectdial.herokuapp.com/response/sip/route/?DialMusic=real',//&CLID=PLEASE_PUT_CALLER_ID_HERE
				'answer_url' => 'https://s3.amazonaws.com/plivosamplexml/speak_url.xml',
				'hangup_url' => 'https://s3.amazonaws.com/plivosamplexml/speak_url.xml',
			);
			return CJSON::encode($this->_client->make_call($params));
		}

        /**
         * Call Test
         *
         * For testing purpopses!1!!11!
         * @return string
         */
        public function callTest()
        {
            $params = array(
                'to' => '+12167025892',
                'from' => '19042803315',
//                'ring_url' => 'http://plivodirectdial.herokuapp.com/response/sip/route/?DialMusic=real',//&CLID=PLEASE_PUT_CALLER_ID_HERE
                'answer_url' => 'https://s3.amazonaws.com/plivosamplexml/speak_url.xml',
//                'hangup_url' => 'https://s3.amazonaws.com/plivosamplexml/speak_url.xml',
            );

            return CJSON::encode($this->_client->make_call($params));
        }

        /**
         * Send Message
         *
         * Sends an SMS message
         * @param $target The target phone number
         * @param $message The message to send
         * @param mixed $source Optional, can be source phone number, defaults to internally configured number
         * @return array API call results
         */
		/** Updated: Bkozak - 3/30/2015 - SMS Message Sent Callback */
        public function sendMessage($target, $message, $source = null, $callbackUrl = null)
        {
            // Set the source
            $source = is_null($source) ? $this->primary_phone : $source;
            $source = (strlen($source) == 10) ? '1'.$source : $source;

            // in case target phone already has 1 prefix on it.
            if(strlen($target) == 11) {
                $target = substr($target, 1);
            }

			$data = array(
                'src'   =>  $source,
                'dst'   =>  '1' . $target,
                'text'  =>  $message
            );

			if(!empty($callbackUrl)) {
				$data['url'] = $callbackUrl;
			}

            // Send text message API call
            return $this->_client->send_message($data);
        }
		/** End Updated: Bkozak - 3/30/2015 - SMS Message Sent Callback */

		/** Updated Bkozak - 4/02/2015 - Start of search numbers */
		public function searchNumbers($params)
        {
            return $this->_client->search_number($params);
        }

		public function buyNumber($params)
        {
            return $this->_client->buy_number($params);
        }

        public function editNumber($params)
        {
            return $this->_client->modify_number($params);
        }
		/** End Updated Bkozak - 4/02/2015 - Start of search numbers */
	}