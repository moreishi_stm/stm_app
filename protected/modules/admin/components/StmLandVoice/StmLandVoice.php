<?php

// Stash this in this namespace
namespace StmLandVoice;

/**
 * Stm Land Voice
 *
 */
class StmLandVoice
{
    /**
     * Username
     *
     * @var string
     */
    protected $_username;

    /**
     * Password
     *
     * @var string
     */
    protected $_password;

    /**
     * Construct
     */
    public function __construct($username, $password)
    {
        // Set username and password
        $this->_username = $username;
        $this->_password = $password;
    }

    /**
     * Make call
     *
     * Executes an API call
     * @param string $uri The request URI for the specific API call
     * @param string $action The HTTP action (GET, POST, DELETE, etc.)
     * @param array $postParams POST variables to post
     * @return array JSON decoded response
     * @throws Exception When unable to decode JSON response
     */
    protected function _makeCall($uri, $action = 'GET', array $postParams = array())
    {
        // Initialize and configure cURL
        $ch = curl_init('https://api.landvoice.com/v1' . $uri);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER  =>  true,
            CURLOPT_CUSTOMREQUEST   =>  $action,
            CURLOPT_HTTPHEADER      =>  array(
                'Content-Type: application/json',
                'Authorization: ' . $this->_username . ':' . $this->_password
            )
        ));

        // If we have POST params, add them and make this a POST request
        if(count($postParams)) {
            curl_setopt_array($ch, array(
                CURLOPT_POST        =>  true,
                CURLOPT_POSTFIELDS  =>  json_encode($postParams)
            ));
        }

        // Execute cURL call and get HTTP status code
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Close cURL
        curl_close($ch);

        // Make sure that we had a 2xx status
        if($status < 200 || $status > 299) {
            throw new Exception('HTTP error code ' . $status . ' has occurred while attempting an API call to Land Voice!');
        }

        // Attempt to decode JSON result
        $response = json_decode($response, true);
        if(json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Error, unable to decode JSON response from Land Voice API call!');
        }

        // Return for further processing
        return $response;
    }

    /**
     * Get Customer
     *
     * Gets array of Customer objects for authenticated user
     * @return array
     * @throws Exception
     */
    public function getCustomer()
    {
        return $this->_makeCall('/customer');
    }

    public function postCustomer(\StmLandVoice\Schemas\FullCustomer $fullCustomer)
    {
        //@todo: Get account to actually test this stuff
    }

    /**
     * Get MLS List
     *
     * Returns all MlsList objects
     * @return array
     * @throws Exception
     */
    public function getMlsList()
    {
        return $this->_makeCall('/mlslist');
    }
}