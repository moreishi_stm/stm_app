<?php
namespace StmLandVoice\Schemas;

class CustomerMls extends Base
{
    public $credentials;
    public $credentials_datecheck;
    public $is_assisted;
    public $mls_id;
    public $mls_name;
    public $upload_allowed;
    public $valid_credentials;
}