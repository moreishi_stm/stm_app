<?php
namespace StmLandVoice\Schemas;

class Products extends Base
{
    public $product_id;
    public $product_name;
    public $product_status;
    public $signup_date;
    public $future_start;
    public $future_stop;
    public $available_credits;
    public $mls;
    public $fsbo_areas;
}