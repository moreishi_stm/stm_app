<?php
namespace StmLandVoice\Schemas;

class FullCustomer extends Base
{
    public $customer_id;
    public $first_name;
    public $last_name;
    public $email;
    public $active;
    public $create_date;
    public $product;
}
