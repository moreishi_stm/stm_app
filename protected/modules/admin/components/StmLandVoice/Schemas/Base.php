<?php
namespace StmLandVoice\Schemas;

/**
 * Base
 *
 * Allows for array access of schema objects
 * @package StmLandVoice\Schemas
 */
class Base implements \ArrayAccess
{
    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}