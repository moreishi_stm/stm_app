<?php

namespace StmDialer\Lists\Time;

include_once('Base.php');

class AllDBuyers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::BUYERS;
        $this->_transactionStatusId = \TransactionStatus::D_NURTURING_SPOKE_TO_ID;
        $this->_presetId = \CallLists::ALL_D_BUYER_PROSPECTS;

        // Call parent construct
        parent::__construct();
    }
}
