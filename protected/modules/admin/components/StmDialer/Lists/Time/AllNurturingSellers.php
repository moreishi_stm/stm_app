<?php

namespace StmDialer\Lists\Time;

include_once('Base.php');

class AllNurturingSellers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::SELLERS;
        $this->_transactionStatusId = \TransactionStatus::NURTURING_ID;
        $this->_presetId = \CallLists::ALL_NURTURING_SELLERS;

        // Call parent construct
        parent::__construct();
    }
}
