<?php

namespace StmDialer\Lists\Time;

include_once('Base.php');

class LeadPoolASellers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::SELLERS;
        $this->_transactionStatusId = \TransactionStatus::HOT_A_LEAD_ID;
        $this->_assignedToId = \Yii::app()->user->settings->lead_pool_contact_id;
        $this->_presetId = \CallLists::LEAD_POOL_A_SELLERS;

        // Call parent construct
        parent::__construct();
    }
}
