<?php

namespace StmDialer\Lists\Time;

include_once('Base.php');

class LeadPoolBBuyers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::BUYERS;
        $this->_transactionStatusId = \TransactionStatus::B_LEAD_ID;
        $this->_assignedToId = \Yii::app()->user->settings->lead_pool_contact_id;
        $this->_presetId = \CallLists::LEAD_POOL_B_BUYERS;

        // Call parent construct
        parent::__construct();
    }
}
