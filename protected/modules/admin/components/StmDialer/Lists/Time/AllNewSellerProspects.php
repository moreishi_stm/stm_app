<?php

namespace StmDialer\Lists\Time;

include_once('Base.php');

class AllNewSellerProspects extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::SELLERS;
        $this->_transactionStatusId = \TransactionStatus::NEW_LEAD_ID;
        $this->_presetId = \CallLists::ALL_NEW_SELLER_PROSPECTS;

        // Call parent construct
        parent::__construct();
    }
}
