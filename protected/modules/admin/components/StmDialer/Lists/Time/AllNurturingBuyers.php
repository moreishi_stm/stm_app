<?php

namespace StmDialer\Lists\Time;

include_once('Base.php');

class AllNurturingBuyers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::BUYERS;
        $this->_transactionStatusId = \TransactionStatus::NURTURING_ID;
        $this->_presetId = \CallLists::ALL_NURTURING_BUYERS;

        // Call parent construct
        parent::__construct();
    }
}
