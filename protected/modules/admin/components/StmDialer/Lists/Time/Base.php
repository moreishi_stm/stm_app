<?php

namespace StmDialer\Lists\Time;

include_once(__DIR__ . '/../Base.php');

/**
 * Base
 *
 * @package StmDialer\Lists\Time
 */
abstract class Base extends \StmDialer\Lists\Base
{
    /**
     * Component Type ID
     *
     * @var int
     */
    protected $_componentTypeId;

    /**
     * Transaction Status Id
     *
     * @var int
     */
    protected $_transactionStatusId;

    /**
     * Construct
     *
     * Called during instantiation
     */
    public function __construct()
    {
        // Set things
        $this->_tableType = \CallLists::TABLE_TYPE_TIME;

        // Call parent constructor
        parent::__construct();
    }

    /**
     * Build Queue by Sort Order //@todo: This is New Queue Logic Refactor - 4/2/16
     *
     * Generates list of phone numbers for a call list phones (queue)
     * @return void
     */
    public function buildQueueBySortOrder($callListId, $filters)
    {
        $callList = \CallLists::model()->findByPk($callListId);
        if(!$callList) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') URGENT DIALER Error! Call List NOT FOUND using Call List ID: '.$callListId.' Investigate Immediately!');
        }

        //@todo: when buliding, set the limit to much larger + CHANGE ORDER - as it defaults to sort, but for building a queue it' time based
        // ORDER = 'FIELD(status,"Answered","Ringing","Boomerang","Queued"), GREATEST(IF(cc.last_login IS NULL, 1, cc.last_login), t.added, cc.added) DESC'
        //->group('p.phone') //@todo: needed to remove dupes somehow? the build query had it

        if(!$this->_assignedToId) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Assigned to ID is missing.');
        }

        $leadsCommand = \Yii::app()->db->createCommand()
            ->select('c.id contact_id')
            ->from('transactions t')
            ->join('contacts c', 't.contact_id = c.id')
            ->join('phones p', 'p.contact_id = c.id AND p.is_deleted=0')
            ->leftJoin('call_list_phones clp', 'p.id = clp.phone_id AND clp.call_list_id=' . $this->_callList->id.' AND clp.is_deleted=0')
            ->andWhere('(select count(p2.id) from phones p2 where p2.contact_id=c.id AND p2.is_deleted=0 AND p2.phone REGEXP "[0-9]{10}") >0')
            ->andWhere('t.component_type_id=' . $this->_componentTypeId)
            ->andWhere('t.account_id=:accountId', array(':accountId' => \Yii::app()->user->accountId))
            ->andWhere('t.is_deleted=0 AND c.is_deleted=0')
            ->order('GREATEST(c.last_login, t.added) DESC, t.id, t.contact_id') //c.last_login DESC, t.added DESC
            ->group('p.contact_id')
            ->limit(\CallListPhones::BUILD_QUEUE_COUNT);

        // Add this in if it's available
        if($this->_transactionStatusId) {
            $leadsCommand->andWhere('t.transaction_status_id=' . $this->_transactionStatusId);
        }






        //@todo: Need to handle filter the NEW BUYER list for X hours - It does get removed in cleanQueue, but should really happen here too - CLee 2016/06/14
        //
        //
        if((!$filters['maxDialerCallCount'] && $filters['maxDialerCallCount'] != 0) || !$filters['maxDialerCallDays']) {
            $filters['maxDialerCallCount'] = \DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
            $filters['maxDialerCallDays'] = \DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;
        }

        //@todo: how to handle hours from new buyer list - everything else is # of days
        $lastCalledMaxDateTime = date('Y-m-d H:i:s', strtotime("-".$filters['maxDialerCallDays']." days"));
        $subQuery = '(select count(id) from activity_log a WHERE activity_date > "' . $lastCalledMaxDateTime . '" AND a.component_id=t.id AND a.component_type_id=' . $this->_componentTypeId . ' AND a.task_type_id IN(' . \DialerController::getDialTaskTypes($includeSkip=true) .'))';








        $leadsCommand->andWhere($subQuery.' <= '.($filters['maxDialerCallCount']? $filters['maxDialerCallCount'] : \DialerController::FILTER_DEFAULT_MAX_CALL_COUNT));

        // removes custom lists items so it can remain separated, add this to query only for lists that are NOT custom
        if(in_array($this->_presetId, array(\CallLists::ALL_NEW_SELLER_PROSPECTS, \CallLists::ALL_NURTURING_SELLERS, \CallLists::ALL_NEW_BUYER_PROSPECTS, \CallLists::ALL_NURTURING_BUYERS))) {

            switch($this->_presetId) {

                case \CallLists::ALL_NEW_SELLER_PROSPECTS:
                case \CallLists::ALL_NURTURING_SELLERS:
                    $componentTypeId = \ComponentTypes::SELLERS;
                    $customPresetId = \CallLists::CUSTOM_SELLERS;
                    break;

                case \CallLists::ALL_NEW_BUYER_PROSPECTS:
                case \CallLists::ALL_NURTURING_BUYERS:
                    $componentTypeId = \ComponentTypes::BUYERS;
                    $customPresetId = \CallLists::CUSTOM_BUYERS;
                    break;

            }
            // removes "New Leads" from Dialer Import - this removes Expired / FSBO's or any other mass import that are truly not new leads
            $leadsCommand->andWhere('t.origin_type NOT IN ("'.\Transactions::ORIGIN_TYPE_DIALER_IMPORT.'", "'.\Transactions::ORIGIN_TYPE_GENERAL_IMPORT.'")');
        }

        $leadContactIds = $leadsCommand->queryColumn();

        // return if there is nothing to add or build onto
        if(empty($leadContactIds)) {
            return;
        }

        // rebuild the list by setting everything to sort_order=0 AND is_deleted=1
        \Yii::app()->db->createCommand("UPDATE call_list_phones clp SET sort_order=0, is_deleted=1 WHERE call_list_id=".$this->_callList->id)->execute();

        // find any existing callListPhones for the lead contact Ids
        $data = \Yii::app()->db->createCommand()
            ->select('clp.id call_list_phone_id, clp.phone_id')
            ->from('call_list_phones clp')
            ->leftJoin('phones p','clp.phone_id=p.id')
            ->leftJoin('contacts c','p.contact_id=c.id')
            ->where(array('in', 'c.id', $leadContactIds))
            ->andWhere('clp.call_list_id='.$this->_callList->id)
            ->andWhere('p.is_deleted=0 AND c.is_deleted=0')
            ->group('p.phone')
            ->queryAll();

        // build a list to check against for existing call_list_phone_ids
        $existingCallListPhones = array_column($data, 'call_list_phone_id', 'phone_id');

        // pull all phone numbers that have the right length and numeric value based on $leadContactIds
        $data = \Yii::app()->db->createCommand()
            ->select('p.id phone_id, p.contact_id')
            ->from('contacts c')
            ->leftJoin('phones p','p.contact_id=c.id')
            ->where(array('in', 'c.id', $leadContactIds))
            ->andWhere("p.phone REGEXP '[0-9]{10}'")
            ->andWhere('p.is_deleted=0 and c.is_deleted=0')
            ->queryAll();

        // build library for phoneIds and corresponding contactId
        $phoneContactIds = array_column($data, 'contact_id', 'phone_id');

        $updateQueryString = '';
        $sortOrder = 1;
        // build the list by going through each lead
        foreach($leadContactIds as $contactId) {

            // if lead has phone #s to add to call list
            if(!empty($phoneIdsToAdd = array_keys($phoneContactIds, $contactId))) {

                // go through each phone and see if it needs to be updated or added
                foreach($phoneIdsToAdd as $phoneIdToAdd) {
                    // if phone exists, build sql update query string.
                    if(isset($existingCallListPhones[$phoneIdToAdd])) {

                        $updateQueryString .= "UPDATE call_list_phones SET sort_order={$sortOrder}, status='".\CallListPhones::QUEUED."', is_deleted=0 WHERE id=".$existingCallListPhones[$phoneIdToAdd]."; ";
                        if(($sortOrder % 500) == 0 && $updateQueryString) {
                            \Yii::app()->db->createCommand($updateQueryString)->execute();
                            $updateQueryString = '';
                        }
                    }
                    // not exist, create a new record
                    else {
                        // Create a new call record
                        $callListPhones = new \CallListPhones();
                        $callListPhones->setAttributes(array(
                                'phone_id' => $phoneIdToAdd,
                                'task_id' => null,
                                'sort_order' => $sortOrder,
                                'call_list_id' => $this->_callList->id,
                                'added' => new \CDbExpression('NOW()'),
                            ));
                        if(!$callListPhones->save()) {
                            \Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not save new CallListPhones. Investigate query as it should arrange for this. Error Message: ' . print_r($callListPhones->getErrors(), true) . PHP_EOL . print_r($callListPhones->attributes, true), \CLogger::LEVEL_ERROR);

                            // this will prevent sortOrder from incrementing
                            continue;
                        }
                    }
                    $sortOrder++;
                }
            }
        }
        // final execution of query string, whatever is left after batching
        if($updateQueryString) {
            $x = \Yii::app()->db->createCommand($updateQueryString)->execute();
        }
    }

    /**
     * ReQueue By Sort Order  //@todo: This is New Queue Logic Refactor - 4/2/16
     *
     * Re-Queue call list based on filters
     * @param int $callListId The call list to update
     * @param array $filters Filters
     * @return void
     */
    public function reQueueBySortOrder($callListId, $userId, $filters)
    {
        $this->_assignedToId = $userId;

        $this->buildQueueBySortOrder($callListId, $filters);
    }

    /**
     * Clean Queue by Sort Order
     * This is New Queue Logic Refactor - 4/2/16
     *
     * @param $callListId
     * @return mixed|void
     */
    public function cleanQueueBySortOrder($callListId)
    {
        $callList = \CallLists::model()->findByPk($callListId);
        if(!$callList) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') URGENT DIALER Error! Call List NOT FOUND using Call List ID: '.$callListId.' Investigate Immediately!');
        }

        if(!($callListType = \CallListTypes::model()->findByPk($callList->preset_ma))) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') URGENT DIALER Error! Call List Type NOT FOUND using Call List ID: '.$callListId.' Investigate Immediately!');
        }

        $componentTypeId = $callListType->component_type_id;
        $presetId = $callListType->id;

        // count spoke to activity for a lead from a preset date for this list componentTypeId (contact/buyer/seller/recruit)
        $lastActivityInRangeCountSubQuery = '(select count(id) from activity_log a WHERE activity_date > "'.\CallLists::getCallbackDateByListId($callListId).'" AND a.component_id=t.id AND a.component_type_id='.$componentTypeId.' AND a.task_type_id IN('.\DialerController::getDialTaskTypes($includeSkip=true).') ORDER BY activity_date DESC LIMIT 1)';
        //a.task_type_id IN('.\TaskTypes::BAD_PHONE.','.\TaskTypes::DIALER_ANSWER.','.\TaskTypes::DIALER_SKIP.') OR (a.task_type_id='.\TaskTypes::PHONE.' AND a.is_spoke_to=1)
        //\DialerController::getDialTaskTypes($includeSkip=true)
//        ','.\TaskTypes::DIALER_NO_ANSWER.','.\TaskTypes::PHONE.','.\TaskTypes::DIALER_LEFT_VOICEMAIL;

//        if((!$callList->filter_max_call_count && $callList->filter_max_call_count!= 0) || !$callList->filter_max_call_days) {
//
//            $maxCallCount = \DialerController::FILTER_DEFAULT_MAX_CALL_COUNT;
//
//            $filters['maxDialerCallDays'] = \DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;
//            $filters['maxDialerCallCount'] = $maxCallCount;
//        }
//
//        if(($callList->filter_max_call_count || $callList->filter_max_call_count == 0) && $callList->filter_max_call_days) {
//
//            $lastCalledMaxDateTime = date('Y-m-d', strtotime("-".$filters['maxDialerCallDays']." days"));
//
//            $subQuery = $this->_getFilterMaxCallCountSubquery($lastCalledMaxDateTime);
//
//            $command->andWhere($subQuery.' < '.($callList->filter_max_call_count ? $callList->filter_max_call_count : 1));
//        }


        $transactionStatusId = $this->_transactionStatusId;//($presetId == \CallLists::ALL_NURTURING_SELLERS || $presetId == \CallLists::ALL_NURTURING_BUYERS) ? \TransactionStatus::NURTURING_ID : \TransactionStatus::NEW_LEAD_ID;

        //@todo: this query can probably be combined with the one below as subquery
        // get list of all contactIds related to callListPhones for the current callListId
        $contactIds = \Yii::app()->db->createCommand("select distinct p.contact_id from call_list_phones c LEFT JOIN phones p ON p.id=c.phone_id AND c.is_deleted=0 AND c.call_list_id={$callListId} AND c.status NOT IN('".\CallListPhones::ANSWERED."','".\CallListPhones::RINGING."') WHERE p.contact_id IS NOT NULL")->queryColumn();

        // pre-filter out contactsIds that need to be removed - reasons are because transaction status changed or spoke to changed
        if($contactIds) {


            // select contactIds that no longer have a transaction with the required status
            $contactIdsToRemove = \Yii::app()->db->createCommand("select distinct c.id from contacts c LEFT JOIN phones p ON c.id=p.contact_id LEFT JOIN call_list_phones c2 ON c2.phone_id=p.id AND c2.is_deleted=0 LEFT JOIN transactions t ON t.contact_id=c.id AND t.transaction_status_id IN ({$transactionStatusId}) AND t.component_type_id={$componentTypeId} AND t.is_deleted=0 WHERE c2.call_list_id={$callListId} AND  t.id IS NULL AND c.id IN (".implode(',',$contactIds).")")->queryColumn();

            if($contactIdsToRemove) {
                // remove the contact Ids from the call_list_phones without transaction with proper status related to them
                $contactIdsDeleted = \Yii::app()->db->createCommand("UPDATE call_list_phones c LEFT JOIN phones p ON c.phone_id=p.id SET c.is_deleted=1 WHERE c.call_list_id={$callListId} AND p.contact_id IN(".implode(',',$contactIdsToRemove).") AND c.status NOT IN ('".\CallListPhones::ANSWERED."','".\CallListPhones::RINGING."')")->execute();

                if($contactIdsDeleted) {
                    //$this->_log2(__CLASS__, __LINE__, $contactIdsDeleted.' callListPhones have been deleted where the transaction status id is no longer a match for the list type.'.PHP_EOL.'Contact Ids to remove: '.print_r($contactIdsToRemove, true));
                }
            }
        }

        //select any transaction/leads that have been spoken to within the time frame according to CallLists::getCallbackDateByListId() and remove them from the phone list to prevent calling again before desired timeframe
        $leadsToRemoveCommand = \Yii::app()->db->createCommand()
            ->select('c2.id call_list_phone_id')
            ->from('transactions t')
            ->leftJoin('contacts c', 't.contact_id = c.id')
            ->leftJoin('phones p', 'p.contact_id = c.id')
            ->leftJoin('call_list_phones c2', 'c2.phone_id = p.id')
            ->leftJoin('call_lists c3', 'c2.call_list_id = c3.id')
            ->where($lastActivityInRangeCountSubQuery.' >= 1') // narrows it down to last activity before the time frame threshold based on the call list setting
            ->andWhere('t.component_type_id='.$componentTypeId)
            ->andWhere('t.transaction_status_id='.$transactionStatusId)
            ->andWhere('t.is_deleted=0 AND c.is_deleted=0')
            ->andWhere('c3.preset_ma='.$presetId)
            ->andWhere('c2.status NOT IN ("'.\CallListPhones::ANSWERED.'","'.\CallListPhones::RINGING.'")') //@todo: add any other status needed ... check with $beforeLastDate criteria for all scenarios, had to rush to put it in for hotfix
            ->andWhere('c2.is_deleted=0');

        if($callListPhonesToRemove = $leadsToRemoveCommand->queryColumn()) {

            $callListPhonesToRemoveIds = implode(',',$callListPhonesToRemove);

            $leadsToRemoveResult = \Yii::app()->db->createCommand('UPDATE call_list_phones c SET c.is_deleted=1 WHERE c.id IN('.$callListPhonesToRemoveIds.') AND c.status NOT IN ("'.\CallListPhones::RINGING.'","'.\CallListPhones::ANSWERED.'")')->execute();

            if($leadsToRemoveResult) {
                //$logContent = date('Y-m-d H:i:s').': RESULT: Call List Preset: '.$presetId.' | Transaction Status ID: '.$transactionStatusId.' | '.$leadsToRemoveResult.' call_list_phones records Successfully Removed!'.PHP_EOL;
                //$this->_log2(__CLASS__, __LINE__, $leadsToRemoveResult.' callListPhones have been deleted due to number of activities in timeframe.'.PHP_EOL.'Call List Phone Ids to remove: '.print_r($callListPhonesToRemove, true));
            }
        }
    }

    /**
     * Get Filter Max Call Count Subquery
     *
     * This is subquery for the max call count filter used in ReQueue and the Command Builder
     * @param none
     * @return string $query
     */
    protected function _getFilterMaxCallCountSubquery($reQueueDate, $callListPhoneAlias='c')
    {
        return '(select count(c2.id) from calls c2 WHERE '.$callListPhoneAlias.'.id=c2.call_list_phone_id AND DATE(c2.added) > "' . $reQueueDate . '")';
//        return '(select count(a.id) from activity_log a WHERE activity_date > "' . $reQueueDate . '" AND a.component_id=t.id AND a.component_type_id=' . $this->_componentTypeId . ' AND a.task_type_id IN(' .  \DialerController::getDialTaskTypes().'))';
    }

    /**
     * Builds Command to query for this list //@todo: This is New Queue Logic Refactor - 4/2/16
     *
     * @param int  $callListId
     * @param      $filters
     * @param bool $nextCallMode
     *
     * @return \CdbCommand
     */
    protected function _buildGetCommandBySortOrder($callListId, $filters, $nextCallMode=false)
    {
        $command = \Yii::app()->db->createCommand()
            ->select("c.id call_list_phone_id,
                    (SELECT count(c3.id) FROM calls c3 WHERE c.id=c3.call_list_phone_id AND DATE(c3.added) > '".date('Y-m-d', strtotime('-30 days'))."') dialer_count_30_days,
                    c.updated updated,
                    c.updated_by updated_by,
                    DATE_FORMAT(cc.last_login,'%m/%d/%Y') last_login,
                    DATE_FORMAT(t.added,'%m/%d/%Y') t_added,
                    DATE_FORMAT(cc.added,'%m/%d/%Y') cc_added,
                    (SELECT DATE_FORMAT(DATE(activity_date),'%m/%d/%Y') FROM activity_log a WHERE a.component_id=t.id AND a.component_type_id=t.component_type_id AND a.task_type_id IN(".  \DialerController::getDialTaskTypes() .") ORDER BY activity_date DESC LIMIT 1) last_called,
                    p.id phone_id,
                    p.phone phone,
                    p.is_primary is_primary_phone,
                    c.status status,
                    s.name source,
                    CONCAT(TRIM(cc.first_name), IF(cc.spouse_first_name IS NULL OR cc.spouse_first_name ='', ' ', CONCAT(' & ',TRIM(cc.spouse_first_name),' ')), IF(cc.last_name IS NULL OR cc.last_name='','',TRIM(cc.last_name))) contactInfo,
                    t.id component_id,
                    t.component_type_id component_type_id,
                    DATE_FORMAT(t.added,'%m/%d/%Y') added,
                    ct.name component_type_name,
                    (select count(*) from activity_log a WHERE a.component_type_id=t.component_type_id AND a.component_id=t.id) activity_count,
                    (select MAX(date(activity_date)) from activity_log a WHERE a.component_type_id=t.component_type_id AND a.component_id=t.id ORDER BY activity_date DESC LIMIT 1) last_activity_date,
                    (select DATE_FORMAT(MAX(date(activity_date)),'%c/%e/%Y') from activity_log a WHERE a.component_type_id=t.component_type_id AND a.component_id=t.id AND a.is_spoke_to=1 ORDER BY activity_date DESC LIMIT 1) last_spoke_to_date")
            ->from('call_list_phones c')
            ->join('phones p', 'c.phone_id = p.id')
            ->join('contacts cc', 'p.contact_id = cc.id')
            ->join('transactions t', 't.contact_id = cc.id')
            ->join('component_types ct', 't.component_type_id = ct.id')
            ->join('sources s', 't.source_id = s.id')
            ->where('c.call_list_id=:call_list_id', array(':call_list_id' => $callListId))
            ->andWhere('t.component_type_id='.$this->_componentTypeId)
            ->andWhere('t.is_deleted=0')
            ->andWhere('p.is_deleted=0')
            ->andWhere('c.hard_deleted IS NULL')
            ->andWhere('(status IN("'.\CallListPhones::QUEUED.'","'.\CallListPhones::BOOMERANG.'")) OR (status="'.\CallListPhones::ANSWERED.'" AND c.updated_by='.$this->_assignedToId.') OR (status="'.\CallListPhones::RINGING.'" AND c.updated_by='.$this->_assignedToId.')')
            ->andWhere('(c.is_deleted = 0 OR (c.is_deleted = 1 AND (c.status IN ("'.\CallListPhones::ANSWERED.'","'.\CallListPhones::RINGING.'"))))') // call list phone not deleted, or if deleted can be answered or ringing
            ->limit(\DialerController::TASK_QUEUE_LIMIT_SMALL)
            ->order('FIELD(status,"'.\CallListPhones::COMPLETE.'","'.\CallListPhones::SKIPPED.'","'.\CallListPhones::NO_ANSWER.'","'.\CallListPhones::QUEUED.'","'.\CallListPhones::BOOMERANG.'","'.\CallListPhones::RINGING.'","'.\CallListPhones::ANSWERED.'") DESC, sort_order ASC');

        return $command;
    }
}