<?php

namespace StmDialer\Lists\Custom;

include_once(__DIR__ . '/../Base.php');

/**
 * Base
 *
 * @package StmDialer\Lists\Time
 */
abstract class Base extends \StmDialer\Lists\Base
{
    /**
     * Component Type ID
     *
     * @var int
     */
    protected $_componentTypeId;

    /**
     * Transaction Status Id
     *
     * @var int
     */
    protected $_transactionStatusId;

    /**
     * Component Table Name
     *
     * @var string
     */
    protected $_componentTableName;

    /**
     * Construct
     *
     * Called during instantiation
     */
    public function __construct()
    {
        // Set things
        $this->_tableType = \CallLists::TABLE_TYPE_TIME;

        // Call parent constructor
        parent::__construct();
    }

    /**
     * Build Queue by Sort Order
     *
     * Generates list of phone numbers for a call list phones (queue)
     * @return void
     */
    public function buildQueueBySortOrder($callListId, $filters)
    {

    }

    /**
     * Cleans Queue
     * @param $callListId
     *
     * @return mixed|void
     */
    public function cleanQueueBySortOrder($callListId) { }

    /**
     * Get Command for this custom list
     *
     * This getter was created to use to re-queue the custom list type. See getFilterMaxCallCountSubquery() for the same purpose.
     * @param $userId
     *
     * @return \CdbCommand
     */
    public function getReQueueData($userId)
    {
        $this->_assignedToId = $userId;

        $filter['maxDialerCallDays'] = ($this->_callList->filter_max_call_days) ? $this->_callList->filter_max_call_days : \DialerController::FILTER_DEFAULT_MAX_CALL_DAYS;
        $command =  $this->_buildGetCommandBySortOrder($this->_callList->id, $filter, $nextCallMode=false);

        $subQuery = $this->_getFilterMaxCallCountSubquery(date('Y-m-d', strtotime("-".$filter['maxDialerCallDays']." days")));

        // this handles modification of query based on componentTypeId
        if(in_array($this->_componentTypeId, array(\ComponentTypes::SELLERS, \ComponentTypes::BUYERS))) {
            $command->andWhere('t.component_type_id='.$this->_componentTypeId);
        }
        elseif(in_array($this->_componentTypeId, array(\ComponentTypes::RECRUITS, \ComponentTypes::CONTACTS))) {
            $subQuery = str_replace('t.component_type_id', $this->_componentTypeId, $subQuery);
        }

        $allCallListId = \Yii::app()->db->createCommand("SELECT id from call_list_phones where call_list_id=".$this->_callList->id." AND hard_deleted IS NULL")->queryColumn();

        // hard delete any deleted phones
        \Yii::app()->db->createCommand("UPDATE call_list_phones clp JOIN phones p ON p.id=clp.phone_id SET clp.hard_deleted=NOW(), clp.hard_deleted_by=".$userId." WHERE p.is_deleted=1 AND clp.call_list_id=".$this->_callList->id." AND clp.hard_deleted IS NULL")->execute();

        $command
            ->select('c.id call_list_phone_id, '.$subQuery.' dialer_count_sort')
            ->where('c.call_list_id=:call_list_id', array(':call_list_id' => $this->_callList->id))
            //->andWhere(array('not in', 'c.status', array(\CallListPhones::ANSWERED, \CallListPhones::RINGING)))
            ->andWhere('c.hard_deleted IS NULL')
            //SORT ----> FIELD(status,"Answered","Ringing")
            ->order('dialer_count_sort ASC') //, GREATEST(IF(cc.last_login IS NULL, 1, cc.last_login), t.added, cc.added) DESC
            ->limit(-1);

        // hard resets everything to start of at clean slate. May not be needed.
        // $resetCount = \Yii::app()->db->createCommand("UPDATE call_list_phones SET sort_order=0, is_deleted=0, status='".\CallListPhones::QUEUED."', updated=NOW(), updated_by=".\Yii::app()->user->id." WHERE call_list_id=".$this->_callList->id." AND (is_deleted=1 OR sort_order <>0 OR status NOT IN('".\CallListPhones::RINGING."','".\CallListPhones::ANSWERED."') )")->execute();

        $requeueData = $command->queryColumn();

        if(count($requeueData)==0) {
            return array();
        }

        // these will be queued last
        $leftOutData = array_diff($allCallListId, $requeueData);
        foreach($leftOutData as $leftOutCallListPhoneId) {
            $requeueData[] = $leftOutCallListPhoneId;
        }
        return $requeueData;
    }

    /**
     * ReQueue By Sort Order
     *
     * Re-Queue call list based on filters
     * @param int $callListId The call list to update
     * @param array $filters Filters
     * @return void
     */
    public function reQueueBySortOrder($callListId, $userId, $filters) { }

    /**
     * Get Filter Max Call Count Subquery
     *
     * This is subquery to sort by number of calls in X # of days. Used in ReQueue and the Command Builder
     * @param none
     * @return string $query
     */
    protected function _getFilterMaxCallCountSubquery($reQueueDate)
    {
        //'(select count(*) from activity_log a WHERE a.component_id=t.id AND a.component_type_id=t.component_type_id AND a.task_type_id IN(' .  \DialerController::getDialTaskTypes().') AND DATE(activity_date) > "' . $reQueueDate . '")'
        return '(select count(c2.id) from calls c2 WHERE c.id=c2.call_list_phone_id AND DATE(c2.added) > "' . $reQueueDate . '")';
    }

    /**
     * Builds Command to query for this list
     *
     * @param int  $callListId
     * @param      $filters
     * @param bool $nextCallMode
     *
     * @return \CdbCommand
     */
    protected function _buildGetCommandBySortOrder($callListId, $filters, $nextCallMode=false)
    {
        // Retrieve records
        $command = \Yii::app()->db->createCommand()
            ->select("c.id call_list_phone_id,
                    c.updated updated,
                    c.updated_by updated_by,

                    (SELECT count(c3.id) FROM calls c3 WHERE c.id=c3.call_list_phone_id AND DATE(c3.added) > '".date('Y-m-d', strtotime('-30 days'))."') dialer_count_30_days,

                    DATE_FORMAT(cc.last_login,'%m/%d/%Y') last_login,
                    DATE_FORMAT(t.added,'%m/%d/%Y') t_added,
                    DATE_FORMAT(cc.added,'%m/%d/%Y') cc_added,
                    p.id phone_id,
                    p.phone phone,
                    p.is_primary is_primary_phone,
                    c.status status,
                    s.name source,
                    CONCAT(TRIM(cc.first_name), IF(cc.spouse_first_name IS NULL OR cc.spouse_first_name ='', ' ', CONCAT(' & ',TRIM(cc.spouse_first_name),' ')), IF(cc.last_name IS NULL OR cc.last_name='','',TRIM(cc.last_name))) contactInfo,
                    t.id component_id,
                    t.component_type_id component_type_id,
                    DATE_FORMAT(t.added,'%m/%d/%Y') added,
                    ct.name component_type_name
                    ")
            ->from('call_list_phones c')
            ->join('phones p', 'c.phone_id = p.id AND p.is_deleted=0')
            ->join('contacts cc', 'p.contact_id = cc.id')
            ->join($this->_componentTableName.' t', ($this->_componentTypeId==\ComponentTypes::CONTACTS) ? 't.id = cc.id' : 't.contact_id = cc.id')
            ->join('sources s', 't.source_id = s.id')
            ->where('c.call_list_id=:call_list_id', array(':call_list_id' => $callListId))
            ->andWhere('(c.is_deleted = 0 OR (c.is_deleted = 1 AND c.status="'.\CallListPhones::ANSWERED.'")  OR (c.is_deleted = 1 AND c.status="'.\CallListPhones::RINGING.'"))')
            ->andWhere('c.hard_deleted IS NULL')
            ->andWhere('t.is_deleted=0')
            ->andWhere('(status IN("'.\CallListPhones::QUEUED.'","'.\CallListPhones::BOOMERANG.'")) OR (status="'.\CallListPhones::ANSWERED.'" AND c.updated_by='.$this->_assignedToId.') OR (status="'.\CallListPhones::RINGING.'" AND c.updated_by='.$this->_assignedToId.')')
            ->group('p.phone')
            ->limit(\CallListPhones::MIN_LIST_COUNT)
            ->order('FIELD(status,"'.\CallListPhones::QUEUED.'","'.\CallListPhones::BOOMERANG.'","'.\CallListPhones::RINGING.'","'.\CallListPhones::ANSWERED.'") DESC, sort_order ASC, call_list_phone_id DESC');

        // handles the different parts of the query depending on the component type
        switch($this->_componentTypeId) {
            case \ComponentTypes::SELLERS:
            case \ComponentTypes::BUYERS:
                $command->andWhere('t.component_type_id='.$this->_componentTypeId);
                //->andWhere('t.account_id=:accountId', array(':accountId'=> \Yii::app()->user->accountId));
                $command->join('component_types ct', 't.component_type_id = ct.id');
                break;

            case \ComponentTypes::RECRUITS:
                $command->join('component_types ct', \ComponentTypes::RECRUITS.' = ct.id');
                $command->select = str_replace('t.component_type_id', \ComponentTypes::RECRUITS, $command->select);
                break;

            //@todo: hasn't been tested yet
            case \ComponentTypes::CONTACTS:
                $command->join('component_types ct', \ComponentTypes::CONTACTS.' = ct.id');
                $command->select = str_replace('t.component_type_id', \ComponentTypes::CONTACTS, $command->select);
                break;

            default:
                throw new \Exception(__CLASS__.' (:'.__LINE__.') Invalid Component Type.');
                break;
        }

        return $command;
    }
}