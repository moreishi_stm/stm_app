<?php

namespace StmDialer\Lists\Custom;

include_once('Base.php');

class Buyers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::BUYERS;
        $this->_componentTableName = 'transactions';
        $this->_presetId = \CallLists::CUSTOM_BUYERS;

        // Call parent construct
        parent::__construct();
    }
}