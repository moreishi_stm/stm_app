<?php

namespace StmDialer\Lists\Custom;

include_once('Base.php');

class Contacts extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::CONTACTS;
        $this->_componentTableName = 'contacts';
        $this->_presetId = \CallLists::CUSTOM_CONTACTS;

        // Call parent construct
        parent::__construct();
    }
}