<?php

namespace StmDialer\Lists\Custom;

include_once('Base.php');

class Recruits extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::RECRUITS;
        $this->_componentTableName = 'recruits';
        $this->_presetId = \CallLists::CUSTOM_RECRUITS;

        // Call parent construct
        parent::__construct();
    }
}