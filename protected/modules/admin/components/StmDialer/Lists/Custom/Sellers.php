<?php

namespace StmDialer\Lists\Custom;

include_once('Base.php');

class Sellers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::SELLERS;
        $this->_componentTableName = 'transactions';
        $this->_presetId = \CallLists::CUSTOM_SELLERS;

        // Call parent construct
        parent::__construct();
    }
}