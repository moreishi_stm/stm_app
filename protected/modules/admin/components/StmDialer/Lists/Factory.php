<?php

namespace StmDialer\Lists;

/**
 * Factory
 *
 * Used primarily to retrieve a list class by preset ID
 * @package StmDialer\Lists
 */
class Factory
{
    /**
     * Class Map
     *
     * Maps list types to classes
     * @var array Class mapping (List Type ID => List Class)
     */
    protected static $_classMap = array(
        \CallLists::MY_SELLERS_TASKS                  =>  'Task\MySellers',
        \CallLists::MY_BUYERS_TASKS                   =>  'Task\MyBuyers',
        \CallLists::MY_CONTACTS_TASKS                 =>  'Task\MyContacts',
        \CallLists::MY_RECRUITS_TASKS                 =>  'Task\MyRecruits',

        \CallLists::LEAD_POOL_CONTACTS_TASKS          =>  'Task\LeadPoolContacts',
        \CallLists::LEAD_POOL_BUYERS_TASKS            =>  'Task\LeadPoolBuyers',
        \CallLists::LEAD_POOL_SELLERS_TASKS           =>  'Task\LeadPoolSellers',

        \CallLists::ALL_NEW_SELLER_PROSPECTS    =>  'Time\AllNewSellerProspects',
        \CallLists::ALL_NEW_BUYER_PROSPECTS     =>  'Time\AllNewBuyerProspects',
        \CallLists::ALL_NURTURING_BUYERS        =>  'Time\AllNurturingBuyers',
        \CallLists::ALL_NURTURING_SELLERS       =>  'Time\AllNurturingSellers',
        \CallLists::ALL_D_BUYER_PROSPECTS       =>  'Time\AllDBuyers',
        \CallLists::ALL_D_SELLER_PROSPECTS      =>  'Time\AllDSellers',

        \CallLists::LEAD_POOL_A_SELLERS          =>  'Time\LeadPoolASellers',
        \CallLists::LEAD_POOL_B_SELLERS          =>  'Time\LeadPoolBSellers',
        \CallLists::LEAD_POOL_C_SELLERS          =>  'Time\LeadPoolCSellers',
        \CallLists::LEAD_POOL_D_SELLERS          =>  'Time\LeadPoolDSellers',

        \CallLists::LEAD_POOL_A_BUYERS          =>  'Time\LeadPoolABuyers',
        \CallLists::LEAD_POOL_B_BUYERS          =>  'Time\LeadPoolBBuyers',
        \CallLists::LEAD_POOL_C_BUYERS          =>  'Time\LeadPoolCBuyers',
        \CallLists::LEAD_POOL_D_BUYERS          =>  'Time\LeadPoolDBuyers',

        //        \CallLists::CUSTOM_SELLERS              =>  'Time\Custom\Sellers',
//        \CallLists::CUSTOM_BUYERS               =>  'Time\Custom\Buyers',
//        \CallLists::CUSTOM_CONTACTS             =>  'Time\Custom\Contacts',
//        \CallLists::CUSTOM_RECRUITS             =>  'Time\Custom\Recruits'

        \CallLists::CUSTOM_SELLERS              =>  'Custom\Sellers',
        \CallLists::CUSTOM_BUYERS               =>  'Custom\Buyers',
        \CallLists::CUSTOM_CONTACTS             =>  'Custom\Contacts',
        \CallLists::CUSTOM_RECRUITS             =>  'Custom\Recruits'
    );

    /**
     * Get Instance
     *
     * Returns an instance of the specified class by list type ID
     * @param int $presetId The preset ID
     * @param int $listId The list ID (Not always requred)
     * @param array $filters Filters to apply
     * @return \StmDialer\Lists\Base List object from a given preset ID
     * @throws \Exception When an invalid list ID has been specified
     */
    public static function getInstance($presetId, $callListId, array $filters = array())
    {
        // Ensure we can handle this preset ID
        if(!array_key_exists($presetId, self::$_classMap)) {
            throw new \Exception('Error, unknown list type specified');
        }

        // Include the required list class
        include_once(str_replace('\\', '/', self::$_classMap[$presetId]) . '.php');

        // Generate new instance of list class and return it
        $className = __NAMESPACE__ . "\\" . self::$_classMap[$presetId];

        // Instantiate our new class
        /** @var \StmDialer\Lists\Base $o */
        $o = new $className();

        // Run list initialization functions
        $o->initializeList($presetId, $callListId, $filters);

        // Return our newly created instance
        return $o;
    }
}