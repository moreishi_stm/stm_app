<?php
namespace StmDialer\Lists;
\Yii::import('admin_module.controllers.DialerController');

/**
 * Base
 *
 * All call lists ultimately extend from this abstract class
 * @package StmDialer\Lists
 */
abstract class Base
{
    /**
     * Preset ID
     *
     * @var string The current preset ID
     */
    protected $_presetId;

    /**
     * Table Type
     *
     * @var string Signals the table paradigm
     */
    protected $_tableType;

    /**
     * Assigned To ID
     *
     * @var string User ID for assignment
     */
    protected $_assignedToId;

    /**
     * Call List
     *
     * @var \CallLists
     */
    protected $_callList;

    /**
     * Current User ID
     *
     * Needed for Shared Task List such as Lead Pool Buyer Tasks to pull up Answered call by the current user ID not the assigned to ID
     * @var integer current user ID
     */
    protected $_currentUserId;

    /**
     * Construct
     *
     * Called during instantiation
     */
    public function __construct()
    {

    }

    /**
     * Build Queue by Sort Order
     *
     * Builds the list for call list phones (queue)
     * @param int $callListId The call list ID to build from
     * @param array $filters Filters
     * @return void
     */
    public abstract function buildQueueBySortOrder($callListId, $filters);

    /**
     * ReQueue by Sort Order
     *
     * Re-Queue call list based on filter dates
     * @param int $callListId The call list to update
     * @param array $filters Filters
     * @return void
     */
    public abstract function reQueueBySortOrder($callListId, $userId, $filters); //@todo: Ask Nicole - should this be abstract or inherited function

    /**
     * Clean Queue By Sort
     * Removes any items from call list that should not be in the list.
     * Example: For lists are based on lead status, then it checks for phones that should be removed due to lead status change.
     *
     * @param $callListId
     *
     * @return mixed
     */
    public abstract function cleanQueueBySortOrder($callListId);

    /**
     * Build Call List Phones Commend by Sort Order (queue)
     *
     * @param int $callListId The call list ID to build from
     * @return \CdbCommand
     */
    protected abstract function _buildGetCommandBySortOrder($callListId, $filters, $nextCallMode=false);

    /**
     * Get Call List
     *
     * Retrieves the currently loaded call list record
     * @return \CallLists Currently loaded call list record
     */
    public function getCallList()
    {
        return $this->_callList;
    }

    /**
     * Initialize List
     *
     * Initializes list stuff
     * @param int $id The list preset ID
     * @param int $callListId The call list ID
     * @param array $filters Filters to apply
     */
    public function initializeList($presetId, $callListId, array $filters = array())
    {
        if(!$presetId) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') Preset ID not found in initializeList(). $id=' . $presetId . ', $callListId= ' . $callListId);
        }

        // Get list by preset ID (default behavior
        $this->_callList = \CallLists::model()->findByPk($callListId);

        // If we don't have a list for the preset, create one
        if(!$this->_callList) {

            // Create new list record
            $this->_callList = new \CallLists();
            $this->_callList->setAttributes(array(
                'preset_ma'             =>  $presetId,
                'contact_id'            =>  null,
                'added'                 =>  new \CDbExpression('NOW()'),
            ));

            // Determine whether or not this list is shared and handle accordingly
            $isShared = \CallLists::getIsPresetShared($presetId);
            $this->_callList->name = ($isShared) ? $this->_callList->getPresetTypes()[$presetId] . ' List' : \Yii::app()->user->contact->fullName . ' - ' . $this->_callList->getPresetTypes()[$presetId] . ' List';
            $this->_callList->contact_id = ($isShared) ? null : \Yii::app()->user->id;

            // Attempt to save the list
            if(!$this->_callList->save()) {
                throw new \Exception('Error in creating Preset Call List! Error Message: ' . print_r($this->_callList->getErrors(), true));
            }
        }
    }

    /**
     * Get Queue by Sort Order
     * @param      $callListId
     * @param      $userId
     * @param      $filters
     * @param null $limit
     * @param bool $nextCallMode
     *
     * @return array
     */
    public function getQueueBySortOrder($callListId, $userId, $filters, $limit=null, $nextCallMode=false)
    {
        // primarily created for Lead Pool Tasks Lists (or any task list that is shared), needed to pull up Answered calls for the user but is assigned to someone else
        $this->_currentUserId = $userId;
        if(!in_array($this->_callList->preset_ma, array(\CallLists::LEAD_POOL_SELLERS_TASKS, \CallLists::LEAD_POOL_BUYERS_TASKS, \CallLists::LEAD_POOL_CONTACTS_TASKS))) {
            $this->_assignedToId = $this->_currentUserId;
        }

        // helper function to get our base command
        $command = $this->_buildGetCommandBySortOrder($callListId, $filters, $nextCallMode);

        // don't apply the do not call if on a task list
        $callListType = \CallLists::getTableTypeById($callListId);

        // all lists except phone tasks lists are subject to the internal do not call list
        if($callListType !== 'task') {
            // removes people on do not call list up to the expired date
            $command->leftJoin('call_list_do_not_call d', '(d.call_list_phone_id=c.id OR p.phone=d.phone) AND expire_date > NOW()');
            $command->andWhere('d.id IS NULL');
        }

        // If we only wanted queued/boomerang calls (no ringing, completed, skipped)
        if($nextCallMode === true) {
            $command->select('c.id call_list_phone_id, p.phone phone, c.sort_order');
            $command->andWhere(array('IN','c.status', array(\CallListPhones::QUEUED, \CallListPhones::BOOMERANG)));
            $command->order('FIELD(status, "'.\CallListPhones::QUEUED.'","'.\CallListPhones::BOOMERANG.'") DESC, sort_order ASC, call_list_phone_id DESC');

            //@todo: need to handle how not to call the same person/task multiple times at the same time - check any calls Ringing or Answered? We don't have this now so not a deal breaker - CLee 4/2/16
            //
            //
            //
        }

        if($limit) {
            $command->limit($limit);
        }

        // return records
        return $command->queryAll();
    }
}