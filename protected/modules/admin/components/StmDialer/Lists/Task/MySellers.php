<?php

namespace StmDialer\Lists\Task;

include_once('Base.php');

class MySellers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::SELLERS;
        $this->_componentTable = 'transactions';
        $this->_assignedToId = \Yii::app()->request->getParam('userId') ? \Yii::app()->request->getParam('userId') : \Yii::app()->user->id;
        $this->_presetId = \CallLists::MY_SELLERS_TASKS;

        // Call parent constructor
        parent::__construct();
    }
}
