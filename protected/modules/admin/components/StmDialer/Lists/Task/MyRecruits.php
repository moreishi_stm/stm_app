<?php

namespace StmDialer\Lists\Task;

include_once('Base.php');

class MyRecruits extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::RECRUITS;
        $this->_componentTable = 'recruits';
        $this->_assignedToId = \Yii::app()->request->getParam('userId') ? \Yii::app()->request->getParam('userId') : \Yii::app()->user->id;
        $this->_presetId = \CallLists::MY_RECRUITS_TASKS;

        // Call parent constructor
        parent::__construct();
    }
}
