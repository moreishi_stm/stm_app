<?php

namespace StmDialer\Lists\Task;

include_once('Base.php');

class MyBuyers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::BUYERS;
        $this->_componentTable = 'transactions';
        $this->_assignedToId = \Yii::app()->request->getParam('userId') ? \Yii::app()->request->getParam('userId') : \Yii::app()->user->id;
        $this->_presetId = \CallLists::MY_BUYERS_TASKS;

        // Call parent constructor
        parent::__construct();
    }
}
