<?php

namespace StmDialer\Lists\Task;

include_once('Base.php');

class LeadPoolRecruits extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::RECRUITS;
        $this->_componentTable = 'recruits';
        $this->_assignedToId = \Yii::app()->user->settings->recruit_lead_pool_contact_id;
        $this->_currentUserId = \Yii::app()->user->id;
        $this->_presetId = \CallLists::LEAD_POOL_SELLERS_TASKS;

        // Call parent constructor
        parent::__construct();
    }
}
