<?php

namespace StmDialer\Lists\Task;

include_once('Base.php');

class MyContacts extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::CONTACTS;
        $this->_componentTable = 'contacts';
        $this->_assignedToId = \Yii::app()->request->getParam('userId') ? \Yii::app()->request->getParam('userId') : \Yii::app()->user->id;
        $this->_presetId = \CallLists::MY_CONTACTS_TASKS;

        // Call parent constructor
        parent::__construct();
    }
}
