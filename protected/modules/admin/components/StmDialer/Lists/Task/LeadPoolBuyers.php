<?php

namespace StmDialer\Lists\Task;

include_once('Base.php');

class LeadPoolBuyers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::BUYERS;
        $this->_componentTable = 'transactions';
        $this->_assignedToId = \Yii::app()->user->settings->lead_pool_contact_id;
        $this->_currentUserId = \Yii::app()->user->id;
        $this->_presetId = \CallLists::LEAD_POOL_BUYERS_TASKS;

        // Call parent constructor
        parent::__construct();
    }
}
