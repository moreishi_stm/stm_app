<?php

namespace StmDialer\Lists\Task;

include_once('Base.php');

class LeadPoolSellers extends Base
{
    /**
     * Construct
     *
     */
    public function __construct()
    {
        // Set things
        $this->_componentTypeId = \ComponentTypes::SELLERS;
        $this->_componentTable = 'transactions';
        $this->_assignedToId = \Yii::app()->user->settings->lead_pool_contact_id;
        $this->_currentUserId = \Yii::app()->user->id;
        $this->_presetId = \CallLists::LEAD_POOL_SELLERS_TASKS;

        // Call parent constructor
        parent::__construct();
    }
}
