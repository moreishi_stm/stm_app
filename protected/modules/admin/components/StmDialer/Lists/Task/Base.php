<?php

namespace StmDialer\Lists\Task;

include_once(__DIR__ . '/../Base.php');

abstract class Base extends \StmDialer\Lists\Base
{
    /**
     * Component Type ID
     *
     * @var int
     */
    protected $_componentTypeId;

    /**
     * Component Table
     *
     * @var string Database table for the component
     */
    protected $_componentTable;

    /**
     * Construct
     *
     * Called during instantiation
     */
    public function __construct()
    {
        // Set things
        $this->_tableType = \CallLists::TABLE_TYPE_TASK;

        // Call parent constructor
        parent::__construct();
    }

    /**
     * Sets Assigned To Id
     * @todo: Is there a way to make this more streamlined? Had to make this method last minute as hotfix - Ask Nicole - CLee 2015-12-22
     */
    protected function _setAssignedToId()
    {
        // If we have a contact ID, use that
        if($this->_callList->contact_id) {
            $this->_assignedToId = $this->_callList->contact_id;
        }
        else {
            switch($this->_callList->preset_ma) {
                case \CallLists::LEAD_POOL_SELLERS_TASKS:
                case \CallLists::LEAD_POOL_BUYERS_TASKS:
                case \CallLists::LEAD_POOL_CONTACTS_TASKS:
                    $this->_assignedToId = \Yii::app()->user->settings->lead_pool_contact_id;
                    break;

                case \CallLists::LEAD_POOL_RECRUITS_TASKS:
                    $this->_assignedToId = \Yii::app()->user->settings->recruit_lead_pool_contact_id;
                    break;

                default:
                    \Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Dialer Error URGENT! $this->_assignedToId is not found! Call List Data: '.print_r($this->_callList->attributes, true), \CLogger::LEVEL_ERROR);
                    break;
            }
        }

        if(!$this->_assignedToId) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Assigned to ID is missing. Call List data: '.print_r($this->_callList->attributes, true));
        }
    }

    /**
     * ReQueue By Sort Order
     *
     * Re-Queue call list based on filters
     * @param int $callListId The call list to update
     * @param array $filters Filters
     * @return void
     */
    public function reQueueBySortOrder($callListId, $userId, $filters)
    {
        $reQueueDate = date('Y-m-d', strtotime("-1 days"));
        // Re-queue any phone call tasks since yesterday
        $result = \Yii::app()->db->createCommand('UPDATE call_list_phones clp JOIN tasks t ON t.id=clp.task_id SET clp.status="'.\CallListPhones::QUEUED.'" WHERE clp.call_list_id='.$callListId.' AND clp.status IN("'.\CallListPhones::NO_ANSWER.'","'.\CallListPhones::SKIPPED.'","'.\CallListPhones::COMPLETE.'") AND t.complete_date IS NULL AND t.is_deleted=0 AND DATE(clp.last_called) <="'.$reQueueDate.'" AND DATE(clp.updated) <="'.$reQueueDate.'" AND clp.is_deleted=0')->execute();

        $this->buildQueueBySortOrder($callListId, $filters);
    }

    /**
     * Cleans Queue by Sort Order
     * @param $callListId
     *
     * @return mixed|void
     */
    public function cleanQueueBySortOrder($callListId)
    {
        // sets the right assigned to ID
        $this->_setAssignedToId();

        // search and removes call list phones with tasks that are completed or deleted or doesn't match the user
        \Yii::app()->db->createCommand("UPDATE call_list_phones clp LEFT JOIN tasks t ON clp.task_id=t.id SET clp.is_deleted=1, clp.updated=NOW() WHERE clp.call_list_id={$callListId} AND clp.is_deleted=0 AND (t.is_deleted=1 OR t.complete_date IS NOT NULL OR t.assigned_to_id !=".$this->_assignedToId.")")->execute();
    }

    /**
     * Builds Command to query for this list //@todo: This is New Queue Logic Refactor - 4/2/16
     *
     * @param int  $callListId
     * @param      $filters
     * @param bool $nextCallMode
     *
     * @return \CdbCommand
     */
    protected function _buildGetCommandBySortOrder($callListId, $filters, $nextCallMode=false)
    {
        // sets the assigned to for tasks, can be for person or lead pool tasks
        $this->_setAssignedToId();

        $command = \Yii::app()->db->createCommand()
            ->select(  "c.id call_list_phone_id,
                        c.updated_by updated_by,
                        c.call_count call_count,
                        IF(c.last_called>0,DATE_FORMAT(c.last_called,'%m/%d/%Y'),'') last_called,
                        p.id phone_id,
                        p.phone phone,
                        p.is_primary is_primary_phone,
                        c.status status,
                        t.id task_id,
                        CONCAT(cc.first_name, IF(cc.spouse_first_name IS NULL OR cc.spouse_first_name ='', ' ', CONCAT(' & ',cc.spouse_first_name,' ')), cc.last_name) contactInfo,
                        t.description description,
                        DATE_FORMAT(t.due_date,'%m/%d/%Y') due_date,
                        t.component_id component_id,
                        t.is_priority is_priority,
                        t.component_type_id component_type_id,
                        ct.name component_type_name,
                        (select date(activity_date) from activity_log a WHERE a.component_type_id=t.component_type_id AND a.component_id=t.component_id AND a.is_spoke_to=1 ORDER BY activity_date DESC LIMIT 1) date_last_spoke_to")
            ->from('call_list_phones c')
            ->join('phones p', 'c.phone_id = p.id')
            ->join('tasks t', 'c.task_id = t.id')
            ->join('contacts cc', 'p.contact_id = cc.id')
            ->join('component_types ct', 't.component_type_id = ct.id')
            ->where('c.call_list_id=:call_list_id', array(':call_list_id' => $callListId))
            ->andWhere('(c.status IN ("'.\CallListPhones::ANSWERED.'","'.\CallListPhones::RINGING.'") AND c.updated_by='.$this->_currentUserId.') OR (c.status NOT IN ("'.\CallListPhones::ANSWERED.'","'.\CallListPhones::RINGING.'"))')
            ->andWhere('t.due_date <= NOW()')
            ->andWhere('(c.is_deleted = 0 OR (c.is_deleted = 1 AND c.status="'.\CallListPhones::ANSWERED.'"))')
            ->andWhere('p.is_deleted=0')
            ->andWhere('t.account_id=:accountId', array(':accountId' => \Yii::app()->user->accountId))
            ->limit(\DialerController::TASK_QUEUE_LIMIT_SMALL)
            ->order('FIELD(status,"'.\CallListPhones::COMPLETE.'","'.\CallListPhones::SKIPPED.'","'.\CallListPhones::NO_ANSWER.'","'.\CallListPhones::QUEUED.'","'.\CallListPhones::BOOMERANG.'","'.\CallListPhones::RINGING.'","'.\CallListPhones::ANSWERED.'") DESC, sort_order ASC, call_list_phone_id DESC');

        return $command;
    }

    /**
     * Build Queue by Sort Order //@todo: This is New Queue Logic Refactor - 4/2/16
     *
     * Generates list of phone numbers for a call list phones (queue)
     * @return void
     */
    public function buildQueueBySortOrder($callListId, $filters)
    {
        if(!$callList = \CallLists::model()->findByPk($callListId)) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') Call List invalid.');
        }

        if(!$this->_componentTable) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') Component Table is missing.');
        }

        if(!$this->_componentTypeId) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') Component Type Id is missing.');
        }

        if(!$this->_assignedToId) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') Assigned to Id is missing.');
        }

        if(!$callListId) {
            throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') Call List ID is missing.');
        }

        $this->_setAssignedToId();

        //@todo: these can eventually be combined into 1 query using nested queries, stored procedures or views
        // some tasks that were out of scope/due_date timeframe may be due now, user change due_dates
        $callListPhonesToUndelete = \Yii::app()->db->createCommand()
            ->select("l.id")
            ->from('tasks t')
            ->join($this->_componentTable . ' ct', 't.component_id = ct.id')
            ->join('contacts c', (($this->_componentTypeId == \ComponentTypes::CONTACTS) ? 'ct.id' :'ct.contact_id').' = c.id')
            ->join('phones p', 'c.id = p.contact_id')
            ->leftJoin('call_list_phones l', 'l.phone_id = p.id AND l.call_list_id=:call_list_id AND t.id=l.task_id', array(':call_list_id'=>$callListId))
            ->where('t.due_date <= NOW()')
            ->andWhere('t.complete_date IS NULL')
            ->andWhere('t.is_deleted=0')
            ->andWhere('p.is_deleted=0')
            ->andWhere('l.is_deleted=1')
            ->andWhere("p.phone REGEXP '[0-9]{10}'")
            ->andWhere('t.assigned_to_id=:assigned_to_id', array(':assigned_to_id' => $this->_assignedToId))
            ->andWhere('t.component_type_id=' . $this->_componentTypeId)
            ->andWhere('t.task_type_id=:task_type_name', array(':task_type_name' => \TaskTypes::PHONE))
            ->queryColumn();


        //@todo: 2016-04-06 - CLee combining into 1 query
        //example:
//        $command = Yii::app()->db->createCommand()
//            ->update(
//                'queue q',
//                array('i.status_id' => ':status_id'),
//                array('in', 'queue_id', $ids),
//                array(':status_id' => $status_id),
//    )
//            ->join('item i', 'q.item_id = i.item_id');

        //UPDATE call_list_phones clp RIGHT JOIN tasks t ON t.id=clp.task_id JOIN :componentTable ct ON t.component_id = ct.id JOIN contacts c ON ct.contact_id = c.id JOIN phones p ON c.id=p.contact_id SET clp.is_deleted=0, clp.updated=NOW(), clp.updated_by=1 WHERE t.due_date <= NOW() AND t.complete_date IS NULL AND p.phone REGEXP '[0-9]{10}' t.assigned_to_id=1 t.component_type_id=0 t.task_type_id=2 (t.is_deleted = 0 OR t.is_deleted IS NULL) AND p.is_deleted=0 clp.is_deleted=1

        // combining into 1 query
//        $callListPhonesUndeletedCount = \Yii::app()->db->createCommand(
//                "UPDATE call_list_phones clp"
//                . " RIGHT JOIN tasks t ON t.id=clp.task_id"
//                . " JOIN :componentTable ct ON ct.id = t.component_id"
//                . " JOIN contacts c ON ".(($this->_componentTypeId == \ComponentTypes::CONTACTS) ? 'ct.id' :'ct.contact_id') . " = c.id"
//                . " JOIN phones p ON c.id=p.contact_id"
//                . " SET clp.is_deleted=0, clp.updated=NOW(), clp.updated_by=" . intval($this->_assignedToId)
//                . " WHERE t.due_date <= NOW()"
//                . " AND t.complete_date IS NULL"
//                . " AND p.phone REGEXP '[0-9]{10}'"
//                . " AND t.assigned_to_id=".intval($this->_assignedToId)
//                . " AND t.component_type_id=" . intval($this->_componentTable)
//                . " AND t.task_type_id=" . intval(\TaskTypes::PHONE)
//                . " AND (t.is_deleted = 0 OR t.is_deleted IS NULL) AND p.is_deleted=0"
//                . " AND clp.is_deleted=1"
//            )->bindParam(':componentTable', $this->_componentTable)
//            ->execute();


        if($callListPhonesToUndelete) {

            $callListPhonesToUndeleteString = implode(',', $callListPhonesToUndelete);

            //@todo: $this->_currentUserId does not have value - temporarily using _assignedToId - CLee 10/20/2015 ... or for My Task lists, it can lookup the contact_id for the list and use that for updated_by... how about lead pool tasks? who is it updated by?
            $updatedRows = \Yii::app()->db->createCommand("UPDATE call_list_phones SET is_deleted=0, updated=NOW(), updated_by={$this->_assignedToId} WHERE id IN({$callListPhonesToUndeleteString})")->execute();
        }

        $phonesToAdd = \Yii::app()->db->createCommand()
            ->select("t.id task_id, p.id phone_id")
            ->from('tasks t')
            ->join($this->_componentTable . ' ct', 't.component_id = ct.id')
            ->join('contacts c', (($this->_componentTypeId == \ComponentTypes::CONTACTS) ? 'ct.id' :'ct.contact_id').' = c.id')
            ->join('phones p', 'c.id = p.contact_id')
            ->leftJoin('call_list_phones l', 'l.phone_id = p.id AND l.call_list_id=:call_list_id AND t.id=l.task_id AND l.is_deleted=0', array(':call_list_id'=>$callListId))
            ->where('t.due_date <= NOW()')
            ->andWhere('t.complete_date IS NULL')
            ->andWhere("p.phone REGEXP '[0-9]{10}'")
            ->andWhere('t.assigned_to_id=:assigned_to_id', array(':assigned_to_id' => $this->_assignedToId))
            ->andWhere('t.component_type_id=' . $this->_componentTypeId)
            ->andWhere('t.task_type_id=:task_type_id', array(':task_type_id' => \TaskTypes::PHONE))
            ->andWhere('l.id IS NULL')
            ->andWhere('(t.is_deleted = 0 OR t.is_deleted IS NULL) AND p.is_deleted=0')
            ->order('t.is_priority DESC, t.due_date DESC, c.id, p.is_primary DESC')
            ->queryAll();

        // Add each number for the task, check for duplicates for task_id and phone_id, allow to see if new phone # is added since previous list create
        foreach($phonesToAdd as $phoneToAdd) {

            // Create a new call record
            $callListPhones = new \CallListPhones('task');
            $callListPhones->setAttributes(array(
                    'phone_id' => $phoneToAdd['phone_id'],
                    'task_id' => $phoneToAdd['task_id'],
                    'call_list_id' => $callListId,
                    'added' => new \CDbExpression('NOW()'),
                ));
            if(!$callListPhones->save()) {
                \Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Could not save new CallListPhones. Error Message: ' . print_r($callListPhones->getErrors(), true) . PHP_EOL . 'Attributes: '.print_r($callListPhones->attributes, true), \CLogger::LEVEL_ERROR);
            }
        }

        // default sort order when no filter is selected
        $order = 't.due_date DESC';

        // orders it by oldest task first only if specified. otherwise sorts by newest first
        switch($filters['task_order']) {
            case 'due_date_asc':
                $order = 't.due_date ASC';
                break;

            case 'due_date_desc':
                $order = 't.due_date DESC';
                break;

            case 'status_asc':
                $order = 'ts.sort_order ASC';
                break;
        }

        // set everything to zero first, except if it's a lead pool that is ringing
        if($callList->type->is_shared) {
            $sharedCondition = " AND clp.updated_by=".$this->_currentUserId;
        }
        $sortedCount = \Yii::app()->db->createCommand("UPDATE call_list_phones clp SET clp.sort_order=0 WHERE clp.call_list_id={$callListId} AND clp.is_deleted=0 {$sharedCondition}")->execute();

        //set @i=0;
        //UPDATE favorites SET `order` = (@i:=@i+1); //@todo: Error : Incorrect usage of UPDATE and ORDER BY
//        $sortedCount = \Yii::app()->db->createCommand(
//            "SET @i=0; ".
//            "UPDATE call_list_phones clp " .
//            "JOIN tasks t ON t.id=clp.task_id " .
//            "SET clp.sort_order=(@i:=@i+1) " .
//            "WHERE clp.call_list_id={$callListId} " .
//            "AND clp.is_deleted=0 " .
//            "ORDER BY is_priority DESC, due_date {$order}"
//        )->execute();

        // order it based on priority and due date order per filter
        $clpsToSortCommand = \Yii::app()->db->createCommand()
            ->select("clp.id")
            ->from('call_list_phones clp')
            ->join('tasks t', 't.id=clp.task_id')
            ->where("clp.call_list_id={$callListId}")
            ->andWhere("t.is_deleted=0")
            ->andWhere("t.complete_date IS NULL")
            ->andWhere("clp.is_deleted=0")
            ->order("t.is_priority DESC, {$order}, t.id")
            ->limit(1000); // some have 1000's of tasks overdue, takes forever to queue all and it will never get used. This way it queues the next 1k and that is sufficient.

        // add filter for sort by lead heat - buyers and sellers
        if($filters['task_order'] == 'status_asc') {

            switch($callList->type->component_type_id) {

                case \ComponentTypes::SELLERS:
                case \ComponentTypes::BUYERS:
                    $componentTableName = 'transactions';
                    $statusField = 'transaction_status_id';
                    break;

                case \ComponentTypes::RECRUITS:
                    $componentTableName = 'recruits';
                    $statusField = 'status_id';
                    break;

                default:
                    throw new \Exception(__CLASS__ . ' (:' . __LINE__ . ') Invalid Component Type ID.');
                    break;
            }

            $clpsToSortCommand
                ->join("{$componentTableName} cm", 't.component_id=cm.id') // join with the component model
                ->join("transaction_status ts","ts.id=cm.{$statusField}");
        }

        $clpsToSort = $clpsToSortCommand->queryColumn();

//        @todo: not working query - CLee 4/7/16
//        $maxSubquery = "SELECT MAX(sort_order) FROM call_list_phones clp2 WHERE clp.call_list_id={$callListId} AND clp.is_deleted";
//        \Yii::app()->db->createCommand("UPDATE call_list_phones clp JOIN tasks t ON t.id=clp.task_id SET sort_order=({$$maxSubquery}) WHERE clp.call_list_id={$callListId} AND clp.is_deleted ORDER BY t.is_priority DESC, t.due_date {$order}")->execute();

        $updateQuery = '';
        if(!empty($clpsToSort)) {
            foreach($clpsToSort as $i => $clpToSort) {
                $updateQuery .= "UPDATE call_list_phones SET status='".\CallListPhones::QUEUED."', sort_order={$i} WHERE id={$clpToSort}; ";

                // break up the query if it gets too big
                if($i == 500) {
                    \Yii::app()->db->createCommand($updateQuery)->execute();
                    $updateQuery = '';
                }
            }
        }

        if($updateQuery) {
            \Yii::app()->db->createCommand($updateQuery)->execute();
        }
    }
}