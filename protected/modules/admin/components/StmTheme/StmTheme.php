<?php
class StmTheme extends CTheme {

	protected $_name;
	protected $_baseUrl;
	protected $_basePath = array();

	public function __construct($name,$basePath,$baseUrl)
	{
		$this->_name=$name;
		$this->_baseUrl=$baseUrl;
        $this->_basePath = array_unique(array_merge($this->_basePath, $basePath));
	}

	/**
	 * @return string theme name
	 */
	public function getName()
	{
		return $this->_name;
	}

	public function getViewPath()
	{
        return array_map(function($value) { return $value.DIRECTORY_SEPARATOR.'views'; }, $this->_basePath);
	}

	/**
	 * @return string the path for widget skins. Defaults to 'ThemeRoot/views/skins'.
	 * @since 1.1
	 */
	public function getSkinPath()
	{
		$viewPaths = $this->getViewPath();
		$viewPaths = array_reverse($viewPaths);
		foreach($viewPaths as $viewPath) {
			if(file_exists($viewPath.DIRECTORY_SEPARATOR.'skins')) {

				return $viewPath.DIRECTORY_SEPARATOR.'skins';
			}
		}
		return null;
	}

	/**
	 * Finds the view file for the specified controller's view.
	 * @param CController $controller the controller
	 * @param string $viewName the view name
	 * @return string the view file path. False if the file does not exist.
	 */
	public function getViewFile($controller,$viewName)
	{
        $moduleViewPaths = $this->getViewPath();

		$moduleViewPaths = array_reverse($moduleViewPaths);

		#die('<pre>$moduleViewPaths: '.print_r($moduleViewPaths, 1));

		$addMoudlePath = "";
		if(($module=$controller->getModule())!==null)
				$addMoudlePath='/'.$module->getId();

		foreach($moduleViewPaths as $viewPath) {

			#echo("<!-- Checking: ".$viewPath."//-->\n");

			$moduleViewPath = $viewPath.$addMoudlePath;

			$found = $controller->resolveViewFile($viewName,$viewPath.'/'.$controller->getUniqueId(),$viewPath,$moduleViewPath);
			if($found === false) {
				continue;
			} else {
				break;
			}
		}

		#echo("<!-- Found: <pre>".print_r($found,1)." //--> \n");

		return $found;
	}

	/**
	 * Finds the layout file for the specified controller's layout.
	 * @param CController $controller the controller
	 * @param string $layoutName the layout name
	 * @return string the layout file path. False if the file does not exist.
	 */
	public function getLayoutFile($controller,$layoutName)
	{
		$moduleViewPath=$basePath=$this->getViewPath();
		$module=$controller->getModule();
		if(empty($layoutName))
		{
			while($module!==null)
			{
				if($module->layout===false)
					return false;
				if(!empty($module->layout))
					break;
				$module=$module->getParentModule();
			}
			if($module===null)
				$layoutName=Yii::app()->layout;
			else
			{
				$layoutName=$module->layout;
				$moduleViewPath.='/'.$module->getId();
			}
		}
		elseif($module!==null)
			$moduleViewPath.='/'.$module->getId();

		return $controller->resolveViewFile($layoutName,$moduleViewPath.'/layouts',$basePath,$moduleViewPath);
	}
}

