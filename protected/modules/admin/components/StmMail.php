<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 1.0
 */
class StmMail extends YiiMail {

	const OPTED_OUT_LOG_CATEGORY = 'optout';

    /**
     * @var array
     */
    public $ccRecipients = array();

    /**
     * @var array
     */
    public $bccRecipients = array();

    public static $accountId;

	/**
	 * Requires a StmMailMessage component to send, validates that the email address used has not been opted out
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @param YiiMailMessage $message
	 * @param null           $failedRecipients
	 * @return int
	 */
	public function send(YiiMailMessage $message, &$failedRecipients = null)
    {
		if (isset($message->emailMessage)) {
			/** @var EmailMessage $emailMessage */
			$emailMessage = $message->emailMessage;

			/** @var Emails $toEmail */
            if (!$emailMessage->toEmail instanceof Emails) {
                Yii::log("Could not load email from message: {$emailMessage}", CLogger::LEVEL_WARNING, self::OPTED_OUT_LOG_CATEGORY);
                return false;
            }

			if ($emailMessage->toEmail->getIsOptedOut()) {
				Yii::log($emailMessage->toEmail->email.' has been opted out, cannot send mail to this recipient.', CLogger::LEVEL_WARNING, self::OPTED_OUT_LOG_CATEGORY);
				return false;
			}

            if($emailMessage->origin_type_ma == EmailMessage::SYSTEM_AUTO_NEW_ACCOUNT_WELCOME) {
                $taskTypeId = TaskTypes::WELCOME_EMAIL;
            } elseif($emailMessage->origin_type_ma == EmailMessage::ORIGIN_TYPE_ACTION_PLAN_ID) {
                $taskTypeId = TaskTypes::AUTO_EMAIL_DRIP;
            } else {
                $taskTypeId = TaskTypes::EMAIL_MANUAL;
            }

			// Determine if the activity log should be created based on the component the $emailMessage has attached to it
			if ($this->activityLogIsApplicable($emailMessage)) {
				$emailMessageLog = new ActivityLog('mailSent');
				$emailMessageLog->activity_date = new CDbExpression('NOW()');
				$emailMessageLog->account_id = (self::$accountId)? self::$accountId : Yii::app()->user->getAccountId();
				$emailMessageLog->added_by = $emailMessage->fromEmail->contact->id;
				$emailMessageLog->component_id = $emailMessage->component_id;
				$emailMessageLog->component_type_id = $emailMessage->component_type_id;
				$emailMessageLog->task_type_id = $taskTypeId;
				$emailMessageLog->note = "
					<p>Subject: {$message->getSubject()},</p>
					{$emailMessage}
				";
				$emailMessageLog->save();

				// Create a relationship between the email messages and the corresponding activity log entry
				$activityLogEmailLu = new ActivityLogEmailMessageLu;
				$activityLogEmailLu->activity_log_id = $emailMessageLog->id;
				$activityLogEmailLu->email_message_id = $emailMessage->id;
				$activityLogEmailLu->save();
			}
		}

        if (!empty($this->ccRecipients)) {
            foreach ($this->ccRecipients as $ccRecipient) {
                $message->addCc($ccRecipient);
            }
        }

        if (!empty($this->bccRecipients)) {
            foreach ($this->bccRecipients as $bccRecipient) {
                $message->addBcc($bccRecipient);
            }
        }

		return parent::send($message, $failedRecipients);
	}

	/**
	 * If the component type of the email message
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @param $componentTypeId
	 * @return bool
	 */
	private function activityLogIsApplicable(EmailMessage $emailMessage) {
		$activityLogIsApplicable = false;
		$componentTypeId = $emailMessage->component_type_id;
		if ($componentTypeId and Yii::app()->params['isCliEnv'] === false) {
			$applicableComponentTypes = array(ComponentTypes::BUYERS, ComponentTypes::SELLERS, ComponentTypes::CLOSINGS, ComponentTypes::CONTACTS);
			$activityLogIsApplicable = in_array($componentTypeId, $applicableComponentTypes);
		}

		return $activityLogIsApplicable;
	}
}