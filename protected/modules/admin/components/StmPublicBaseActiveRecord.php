<?php
abstract class StmPublicBaseActiveRecord extends CActiveRecord
{
    public $deleteColumn    = 'is_deleted';
    private $_skipSoftDeleteCheck = false;

    /**
     * Ensure that the 'db' component is set to active
     *
     * @return CDbConnection
     */
    public function getDbConnection()
    {

        self::$db = Yii::app()->stm_public_records;

        if (self::$db instanceof CDbConnection) {
            self::$db->setActive(true);

            return self::$db;
        }
    }

    public function defaultScope() {

        $defaultScope = array();
        $tableAlias   = $this->getTableAlias(true, false);

        // Ignore the soft delete for the default scope if the query is already checking the column for something
        if ($this->hasAttribute($this->deleteColumn) and $this->getSoftDeleteCheck() === false) {
            $defaultScope['condition'] = "({$tableAlias}.{$this->deleteColumn} = 0 or {$tableAlias}.{$this->deleteColumn} IS NULL)";
        }

        return $defaultScope;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return bool
     */
    public function getSoftDeleteCheck() {
        return $this->_skipSoftDeleteCheck;
    }

    /**
     * When a delete call is made on a model inherited from this one, only update the deleted column value
     *
     * @return boolean
     */
    protected function beforeDelete() {
        // Only apply this to models that have the soft delete column
        if ($this->hasAttribute($this->deleteColumn)) {
            $this->{$this->deleteColumn} = true;
            $this->save(false); // no need to validate the model here

            //prevent real deletion
            return false;
        } else {
            return true;
        }
    }
}
