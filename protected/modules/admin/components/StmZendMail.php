<?php
	include_once(Yii::getPathOfAlias('admin_module.extensions.') . DS . 'SimpleHtmlDom.php');

	class StmZendMail extends Zend_Mail
    {
        const NO_EMAIL_ID = 0;

        const DEBUG_EMAIL_ADDRESS = 'debug@seizethemarket.com';
        const BOUNCED_EMAIL_ADDRESS = 'bounced@seizethemarket.com'; //@todo: ********** change to seizethemarket.NET ******** - DISCUSS WITH NICOLE
        const SES_ATTACHMENT_MAX_BYTES = 10485760;

        protected static $_unsupportedAttachmentTypes = array('.ade','.adp','.app','.asp','.bas','.bat','.cer','.chm','.cmd','.com','.cpl','.crt','.csh','.der','.exe','.fxp','.gadget','.hlp','.hta','.inf','.ins','.isp','.its','.js','.jse','.ksh','.lib','.lnk','.mad','.maf','.mag','.mam','.maq','.mar','.mas','.mat','.mau','.mav','.maw','.mda','.mdb','.mde','.mdt','.mdw','.mdz','.msc','.msh','.msh1','.msh2','.mshxml','.msh1xml','.msh2xml','.msi','.msp','.mst','.ops','.pcd','.pif','.plg','.prf','.prg','.reg','.scf','.scr','.sct','.shb','.shs','.sys','.ps1','.ps1xml','.ps2','.ps2xml','.psc1','.psc2','.tmp','.url','.vb','.vbe','.vbs','.vps','.vsmacros','.vss','.vst','.vsw','.vxd','.ws','.wsc','.wsf','.wsh','.xnk');

        protected static $_clientAccountDomains;
        protected static $_replyToDomain;
        protected $_emailMessage;
        protected static $_bouncedEmails = array();
        protected static $_complaintEmails = array('cwood6@cox.net','dayvea@yahoo.com','glojaclark@aol.com','howdyr@yahoo.com','jbraun1068@aol.com','jskye97@yahoo.com','limplam@comcast.net','mfederico85@aol.com','njsnoop@aol.com','planetb_99@yahoo.com','redlion_76@yahoo.com','vallas37@yahoo.com','yankwb@yahoo.com','drtchr1@comcast.net','mmcleod741@aol.com','edward_mclarty@yahoo.com'); //@todo: make dynamic, hard coded for now. 2014-09-14
        protected static $_optOutEmails = array();

        /**
         * Flag for checking if email is bounced or not. Ad hoc emails turns this off. Any other mass delivery should check this such as email drips and market updates.
         * @var bool
         */
        public $checkBounceUndeliverable = true;

        /**
         * Mail character set
         * @var string
         */
        protected $_charset = 'utf-8';

        /**
         * Sends this email using the given transport or a previously
         * set DefaultTransport or the internal mail function if no
         * default transport had been set.
         *
         * If in YII_DEBUG mode, replaces to email with debug constant as defined.
         *
         * @param  Zend_Mail_Transport_Abstract $transport
         * @param  EmailMessage                 $emailMessageToLog - if this is present, activity log gets logged if applicable component type
         * @return Zend_Mail                    Provides fluent interface
         */
        public function send($transport = null, $emailMessageToLog = null)
        {
            if(YII_DEBUG) {
                $this->clearRecipients();
                $this->addTo(self::DEBUG_EMAIL_ADDRESS);
            }

            // loads the clientAccountDomains map to take domains and find client and accounts for email bounce prevention
            if(!self::$_clientAccountDomains) {
                $emailDomains = EmailDomains::model()->findAll();
                if($emailDomains && is_array($emailDomains)) {
                    foreach($emailDomains as $emailDomain) {
                        self::$_clientAccountDomains[$emailDomain->domain]['client_id'] = $emailDomain->client_id;
                        self::$_clientAccountDomains[$emailDomain->domain]['account_id'] = $emailDomain->account_id;
                    }
                }
            }

            //@todo: may want to log this somewhere if it is undeliverable and we sent anyways - 10/19/2015 - CLee
            if($this->checkBounceUndeliverable) {

                // If bounced email array is empty, load bounced email from table. If we are debugging, replace to address with debug address
                if(empty(self::$_bouncedEmails)) {

                    // populate array of bounced emails from hq
                    $bouncedQuery = "select TRIM(LOWER(email)) from emails_undeliverable WHERE (client_id IS NULL AND email not like '%seizethemarket.net' AND email not like '%seizethemarket.com')";

                    list($name,$domain) = explode('@',$this->getFrom());

                    if(isset(self::$_clientAccountDomains[$domain])) {
                        $clientId = self::$_clientAccountDomains[$domain]['client_id'];
                        $accountId = self::$_clientAccountDomains[$domain]['account_id'];
                        // if domain is in the clientAccountDomains map, add client_id, account_id complaint to the query condition
                        $bouncedQuery .= " OR (client_id={$clientId} AND account_id={$accountId} AND email not like '%seizethemarket.net' AND email not like '%seizethemarket.com')";

    //                    if(self::$_replyToDomain) {
    //                        $bouncedQuery .= " AND email NOT LIKE '@".self::$_replyToDomain."'";
    //                    }
    //                    elseif($replyToDomain = Domains::model()->findByAttributes(array('to_email'=>1))) {
    //                        $bouncedQuery .= " AND email NOT LIKE '@".self::$_replyToDomain."'";
    //                    }
                    }

                    self::$_bouncedEmails = Yii::app()->stm_hq->createCommand($bouncedQuery)->queryColumn();
                }

                // load opt out emails for client
                self::$_optOutEmails = Yii::app()->db->createCommand("select TRIM(LOWER(email)) from emails where email_status_id =".EmailStatus::OPT_OUT)->queryColumn();

                // handles if any recipients are in the bounced list
                $recipients = $this->getRecipients();

                $recipients = array_map("strtolower", $recipients);
                $recipients = array_map("trim", $recipients);
                $bouncedRecipients = array_intersect($recipients, self::$_bouncedEmails);
                $complaintRecipients = array_intersect($recipients, self::$_complaintEmails);
                $optOutRecipients = array_intersect($recipients, self::$_optOutEmails);

                // at least 1 recipient is on the bounced list
                if($bouncedRecipients || $complaintRecipients || $optOutRecipients) {

                    // remove the recipients that are bounced and send to the rest if there are any recipients left
                    $allInvalidRecipients = array_unique(array_merge($bouncedRecipients, $complaintRecipients, $optOutRecipients));

                    //@todo: provide error message - ASK NICOLE HOW TO HANDLE DIFF SCENARIOS - Ad hoc, Drips, Market Update
                    // stop email from sending to bounced
                    //\Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Bounced/Complaint/Optout. Recipients: '.print_r($recipients, true).'Bounced'.print_r($bouncedRecipients, true).'Opt Out: '.print_r($optOutRecipients, true).'Complaint: '.print_r($complaintRecipients, true).' All Invalid Recipients: '.print_r($allInvalidRecipients, true), \CLogger::LEVEL_ERROR);


                    //@todo: Send error message to user so they know why things didn't get logged. .... or in replace url link tracking - ASAP!!!! CLee 4/13/15

                    // excludes STM from being invalid recipients //@todo: later have a db table for this? Dunno...
                    $isSTMEmail = false;
                    foreach ($allInvalidRecipients as $allInvalidRecipient) {
                        if (strpos($allInvalidRecipient,'seizethemarket.com') !== false && strpos($allInvalidRecipient,'seizethemarket.net') !== false) {
                            $isSTMEmail = true;
                        }
                    }

                    if(!$isSTMEmail) {
                        return false;
                    }
                }
            } //end if check email undeliverable

            // handle logging email message in activity log if applicable
            if($emailMessageToLog) {
                $this->_logEmailMessage($emailMessageToLog);
            }

            // @TODO: TEMP - catch some weirdness going on and report it

            return parent::send($transport);
        }

        /**
         * Logs Email Message into Activity Log for client
         * @param EmailMessage $emailMessage
         *
         * @return bool
         */
        protected function _logEmailMessage(EmailMessage $emailMessage)
        {
            // if component type id missing, in console command mode or not applicable component type, don't log anything, return now
            if(!$emailMessage->component_type_id || (Yii::app()->params['isCliEnv'] !== false) || !in_array($emailMessage->component_type_id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS, ComponentTypes::CLOSINGS, ComponentTypes::PROJECTS, ComponentTypes::RECRUITS, ComponentTypes::CONTACTS, ComponentTypes::REFERRALS))) {
                return false;
            }

            if (!$emailMessage->toEmail instanceof Emails) {
                Yii::log(__CLASS__." (:".__LINE__.") Could not load email model from Email Message. Attributes: ".print_r($emailMessage, true), CLogger::LEVEL_ERROR);
                return false;
            }

            if ($emailMessage->toEmail->getIsOptedOut()) {
                Yii::log($emailMessage->toEmail->email.' has been opted out, cannot send mail to this recipient.', CLogger::LEVEL_ERROR);
                return false;
            }

            // based on the origin type, it sets the task type id accordingly
            switch($emailMessage->origin_type_ma) {

                case EmailMessage::SYSTEM_AUTO_NEW_ACCOUNT_WELCOME:
                    $taskTypeId = TaskTypes::WELCOME_EMAIL;
                    break;

                case EmailMessage::ORIGIN_TYPE_ACTION_PLAN_ID:
                    $taskTypeId = TaskTypes::AUTO_EMAIL_DRIP;
                    break;

                default:
                    $taskTypeId = TaskTypes::EMAIL_MANUAL;
                    break;
            }

            // Determine if the activity log should be created based on the component the $emailMessage has attached to it
            $emailMessageLog = new ActivityLog('mailSent');
            $emailMessageLog->activity_date = new CDbExpression('NOW()');
            $emailMessageLog->account_id = Yii::app()->user->getAccountId(); //@todo: ************** make this persistent if using in crons and such **************
            $emailMessageLog->added_by = $emailMessage->fromEmail->contact->id;
            $emailMessageLog->component_id = $emailMessage->component_id;
            $emailMessageLog->component_type_id = $emailMessage->component_type_id;
            $emailMessageLog->task_type_id = $taskTypeId;
            $bodyHtml = quoted_printable_decode($this->getBodyHtml(true));
            preg_match('/\<body\>(.*)\<\/body\>/ismU', $bodyHtml, $matches);
            $body = ($matches) ? $matches[1] : $bodyHtml;
            $emailMessageLog->note = "<p>Subject: {$this->getSubject()},</p>{$body}";
            if(!$emailMessageLog->save()) {
                Yii::log(__CLASS__.' ('.__LINE__.') Activity Log for email did not save! Error: '.print_r($emailMessageLog->getErrors(), true).'Attributes: '.print_r($emailMessageLog->attributes, true), CLogger::LEVEL_ERROR);
            }

            // Create a relationship between the email messages and the corresponding activity log entry
            $activityLogEmailLu = new ActivityLogEmailMessageLu;
            $activityLogEmailLu->activity_log_id = $emailMessageLog->id;
            $activityLogEmailLu->email_message_id = $emailMessage->id;
            if(!$activityLogEmailLu->save()) {
                Yii::log(__CLASS__.' ('.__LINE__.') Activity Log Email Lookup for email did not save!'.print_r($activityLogEmailLu->attributes, true), CLogger::LEVEL_ERROR);
            }
        }


        public static function getAwsSesTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('email-smtp.us-east-1.amazonaws.com', array(
                'auth'      =>  'login',
                'ssl'       =>  'tls',
                'port'      =>  587,
                'username'  =>  'AKIAJKCAXELHHWFQDVTA',
                'password'  =>  'Ap9pkvq+wphphQ2cazcjlenYBa+R9EMvRc/LqvIwZL84'
            ));
        }

        public static function getZimbraTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('mail.seizethemarket.net', array(
                'auth'      =>  'login',
                'ssl'       =>  'tls',
                'port'      =>  587,
                'username'  =>  'contactresponses@seizethemarket.net',
                'password'  =>  'bruTr7Ph'
            ));
        }

        public static function getTopGunTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('smtp.mailgun.org', array(
                'auth'      =>  'login',
                'ssl'       =>  'tls',
                'port'      =>  587,
                'username'  =>  'postmaster@jax-homefinder.com',
                'password'  =>  'c54871dd24fde1078fd87646f83d7a9b'
            ));
        }

        public static function getSendGridTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('smtp.sendgrid.net', array(
                'auth'      =>  'login',
                'ssl'       =>  'tls',
                'port'      =>  587,
                'username'  =>  'stmmail',
                'password'  =>  'ChunaSPAqawaru68'
            ));
        }

        public static function getMandrillTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('smtp.mandrillapp.com', array(
                'auth'      =>  'login',
                'ssl'       =>  'tls',
                'port'      =>  587,
                'username'  =>  'admin@seizethemarket.com',
                'password'  =>  '1DK_1tPGRJ9qC5Q0-OkgFg'
            ));
        }

        public static function getCLeeGmailTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('74.125.196.28', array( //74.125.138.28 //smtp-relay.gmail.com
                'port'      =>  587,
            ));
        }

        public static function getVladGmailTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('smtp-relay.gmail.com', array( //74.125.138.28 //smtp-relay.gmail.com
                'port'      =>  587,
            ));
        }

        public static function getVladSparkpostTransport()
        {
            // Create a new mail transport
            return $transport = new Zend_Mail_Transport_Smtp('smtp.sparkpostmail.com', array(
                'auth'      =>  'login',
                'ssl'       =>  'tls',
                'port'      =>  587,
                'username'  =>  'SMTP_Injection',
                'password'  =>  'f776cb80b3afbf6439fd23bba997b07c8871ab58'
            ));
        }

        /**
         * Get Reply To Domain
         *
		 * @author Chris Willard <chris@seizethemarket.com>
		 * @since  1.0
		 * @param Emails    $email
		 * @return string E-mail address
		 */
		public static function getReplyToEmail(EmailMessage $emailMessage, $replyToDomainName=null)
        {
            if(!$replyToDomainName) {
                $replyToDomainName = Yii::app()->user->getReplyToDomain()->name;
            }

            // this is a weird situation where christineleeteam started off on an old design. Transition still weird. Leave here for now until 100% not need anymore
            if($replyToDomainName == 'christineleeteam.com') {
                $replyToDomainName = 'jax-homefinder.com';
            }

            if(!$emailMessage->component_type_id) {
                throw new Exception(__CLASS__.'(:'.__LINE__.') Component Type Id missing in email message.');
            }

            if(!$emailMessage->component_id) {
                throw new Exception(__CLASS__.'(:'.__LINE__.') Component Id missing in email message.');
            }

            //@todo: question for Nicole: Should we require the setEmailMessageModel to be used prior to this method being called or allow it to be passed in? If so, should we set it in this method?
            $contactName = str_replace(' ', '', strtolower($emailMessage->fromEmail->contact->getFullName()));
			$contactName = preg_replace('/\(.*\)/', '', $contactName); // this left comma on the name ex. andyprokes,md
            $contactName = preg_replace('/[^a-zA-Z0-9]/', '', $contactName);
			$componentId = $emailMessage->component_id;
			$componentTypeId = $emailMessage->component_type_id;
			$fromEmailId = $emailMessage->from_email_id;
            $replyToEmailPtrn = ':contactName-:messageId-:componentTypeId-:componentId-:fromEmailId@:replyToDomain';
            $replyToEmail = strtr($replyToEmailPtrn, array(
                ':contactName' => $contactName,
                ':messageId' => $emailMessage->id,
                ':componentTypeId' => $componentTypeId,
                ':componentId' => $componentId,
                ':fromEmailId' => $fromEmailId,
                ':replyToDomain' => $replyToDomainName,
            ));

            return $replyToEmail;
		}


        /**
         * Set Body HTML Tracking
         *
         * @param $html
         */
        public function setBodyHtmlTracking(EmailMessage $emailMessage, $domainName=null, $emailTemplateId=null)
        {
            $this->setBodyHtml(self::replaceLinksForTracking($emailMessage->content, $emailMessage->toEmail->contact->id, $emailMessage->id, $domainName, true, $emailTemplateId));
        }

        /**
         * Replace Links For Tracking
         *
         * Replaces links in emails so that they can be funnelled through the tracking action
         * @param $emailMessage EmailMessage message body to perform find/replace on
         * @param $domainName string specifies domain name to use for tracking link
         * @param $trackEmailMessages boolean tracking email message on true will relate the link to an email message ID. On ignore, it only tracks it to the contact ID and updates last_login date only.
         * @return string Email message body after find/replace
         */
//        public static function replaceLinksForTracking(EmailMessage $emailMessage, $domainName=null, $trackEmailMessages=true) //$content, $emailMessageId, $contactId
        public static function replaceLinksForTracking($content, $contactId, $emailMessageId, $domainName=null, $trackEmailMessages=true, $emailTemplateId=null)
        {
            // Make sure this is of type Email Message
            if(!($content)) {
                Yii::log(__CLASS__.' ('.__LINE__.') Email content is required to send trackable links', CLogger::LEVEL_ERROR);
                throw new Exception(__CLASS__.' ('.__LINE__.') Email content is required to send trackable links.');
            }

            // Make sure contact ID exists
            if(!$contactId) {
                throw new Exception(__CLASS__.' ('.__LINE__.') Contact ID required to create trackable links.');
            }

            //@todo: cross reference contactId + emailMessageId

            // Handle errors internally
            libxml_use_internal_errors(true);

            if(!$domainName) {
                $domainName = Yii::app()->user->getReplyToDomain()->name;
            }
            elseif(!$domainName && YII_IS_CONSOLE) {
                throw new Exception(__CLASS__.' ('.__LINE__.') Domain is required in Console to create trackable links.');
            }

            // Load content
            $dom = new DOMDocument('1.0', 'UTF-8');
            $dom->formatOutput = false;
            @$dom->loadHTML($content);

            // Replace all links found in content
            $tags = $dom->getElementsByTagName('a');
            foreach($tags as $tag) {

                // Skip this if for some reason we don't have a href attribute
                if(!$tag->getAttribute('href')) {
                    continue;
                }

                $encodedContactId = StmFunctions::click3Encode($contactId);
                $href = 'http://www.' . $domainName . '/click3/?ec='.$encodedContactId.'&rc='.$contactId;

                if($emailTemplateId) {
                    $href .= '&etid=' . StmFunctions::click3Encode($emailTemplateId) . '&tid=' . $emailTemplateId;
                }

                // if tracking clicks relating to email messages, add extra encoding for email message id, raw and encoded pair
                if($emailMessageId && $trackEmailMessages) {

                    $encodedEmailMessageId = StmFunctions::click3Encode($emailMessageId);
                    $href .= '&em='.$encodedEmailMessageId.'&rei='.$emailMessageId;

                    //@todo: DETECT weirdness going on - DELETE after fixed, as of 3/4/2015
                    $testDecodeEmailMessageId = StmFunctions::click3Decode($encodedEmailMessageId);
                    if($testDecodeEmailMessageId != $emailMessageId) {
                        Yii::log(__CLASS__.' ('.__LINE__.') Email encoded ID does not match decode. Please investigate ASAP.', CLogger::LEVEL_ERROR);
                    }
                }

                $tag->setAttribute('href', $href.'&url='.urlencode($tag->getAttribute('href')));

                //@todo: DETECT weirdness going on - DELETE after fixed, as of 3/4/2015
                // url has uggc
                if(strpos(urlencode($tag->getAttribute('href')), 'uggc') !== false) {
                    Yii::log(__CLASS__.' ('.__LINE__.') Email url uggc text. Please investigate ASAP.', CLogger::LEVEL_ERROR);
                }

                // check encoded and decoded email and contact matching
                $testDecodedContactId = StmFunctions::click3Decode($encodedContactId);
                if($testDecodedContactId != $contactId) {
                    Yii::log(__CLASS__.' ('.__LINE__.') Contact encoded ID does not match decode. Please investigate ASAP.', CLogger::LEVEL_ERROR);
                }
            }

            // Perform find and replace
            $fullHtml = $dom->saveHTML();
            $startHtml = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">'."\r\n<html><body>";
            $endHtml = "</body></html>";
            $bodyHtml = substr($fullHtml, strlen($startHtml)-1);
            $bodyHtml = substr($bodyHtml, 0, -1*strlen($endHtml)-1);
            return $bodyHtml;
        }

        /**
         * Easy Mail
         * Sends emails in a more regular fashion without the need for an EmailMessages model.
         *
         * @param        $from
         * @param        $to
         * @param        $subject
         * @param        $body
         * @param null   $bcc
         * @param string $type
         * @param bool   $awsTransport
         * @param bool   $ignoreOnDebug
         *
         * @return bool
         */
        public static function easyMail($from, $to, $subject, $body, $bcc=null, $type='text/html', $awsTransport=true, $ignoreOnDebug = true, $replyTo=null, $checkBounceUndeliverable=true) //@todo: add option for tracking link flag
        {
            $to = (YII_DEBUG) ? \StmMailMessage::DEBUG_EMAIL_ADDRESS : $to;

            $zendMessage = new StmZendMail();
            if($checkBounceUndeliverable === false) {
                $zendMessage->checkBounceUndeliverable = false;
            }

            // sanity check for recipients - temp while we phase out YiiMailMessage. As of 12/16/2014 - CLee
            if(is_array($to)) {
                foreach($to as $email => $name) {
                    // sanity check for blank emails, $name ==1 check is for something from Yii Mail Message. Delete after that email class is complete removed from system. Noted: 12/16/2014 CLee
                    if($email && $name !== '1') {
                        $zendMessage->addTo($email, $name);
                    }
                }
            }
            elseif(strpos($to,',') !== false) {



                // handle any non-name label strings with commas for multiple emails
                if((strpos($to,'<') === false) && (strpos($to,'>') === false)) {
                    $commaSeparatedTos = explode(',',  $to);
                    foreach($commaSeparatedTos as $commaSeparatedTo) {
                        $zendMessage->addTo($commaSeparatedTo);
                    }
                }
                else {
                    // @todo: remove any command inside < > so it's not mistaken as separation of emails
                    // this should not occur since it's easyMail
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Unexpected characters "< >" in TO string. Not equipped to handle, may produce errors. Look for error logs.'.PHP_EOL.'$from = '.print_r($from, true).PHP_EOL.'$to = '.print_r($to, true).PHP_EOL.'Zend From: '.print_r($zendMessage->getFrom(), true).PHP_EOL.'Zend To: '.print_r($zendMessage->getRecipients(), true).PHP_EOL.'Mail Class: '.print_r($zendMessage, true), CLogger::LEVEL_ERROR);
                }
            }
            else {
                if($to) {
                    // sanity check for blank emails - temp while we phase out YiiMailMessage. As of 12/16/2014 - CLee
                    $zendMessage->addTo($to);
                }
            }

            // sanity check for if bcc is array - temp while we phase out YiiMailMessage. As of 12/16/2014 - CLee
            if(is_array($from)) {
                $zendMessage->setFrom(key($from), current($from));
            }
            else {
                $zendMessage->setFrom($from);
            }
            $zendMessage->setSubject($subject);
            $body = ($type == 'text/html' && (strpos($body, '<html>') === false || strpos($body, '<html>') > 5))? nl2br($body) : $body;
            $zendMessage->setBodyHtml($body);

            // sanity check for if bcc is array - temp while we phase out YiiMailMessage. As of 12/16/2014 - CLee
            if(is_array($bcc)) {
                foreach($bcc as $email => $name) {
                    $zendMessage->addBcc($email);
                }
            }
            elseif($bcc) {
                $zendMessage->addBcc($bcc);
            }

            if(is_array($replyTo) && isset($replyTo['email']) && !empty($replyTo['email'])) {
                $name = isset($replyTo['name']) && !empty($replyTo['name']) ? $replyTo['name'] : null;
                $zendMessage->setReplyTo($replyTo['email'], $name);
            }

            try {
                $zendMessage->send(($awsTransport)?StmZendMail::getAwsSesTransport() : StmZendMail::getZimbraTransport());
                return true;
            }
            catch(Exception $e) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" Exception Message:' . $e->getMessage().PHP_EOL.'Yii Mail Message: '.print_r($zendMessage, true).PHP_EOL.'$from = '.print_r($from, true).PHP_EOL.'$to = '.print_r($to, true).PHP_EOL.'Zend From: '.print_r($zendMessage->getFrom(), true).PHP_EOL.'Zend To: '.print_r($zendMessage->getRecipients(), true), CLogger::LEVEL_ERROR);
                return false;
            }
        }

        /**
         * Sends email using YiiMailMessage class
         * This is a transition temporary method. Everything should go to AWS or Zimbra mail functions. This will be deprecated soon.
         * @param YiiMailMessage $message
         *
         * @return bool
         */
        public static function sendYiiMailMessage(YiiMailMessage $message)
        {
            try {
                self::easyMail($message->getFrom(),$message->getTo(), $message->getSubject(), $message->getBody(), $message->getBcc(), $type='text/html', $awsTransport=false, $ignoreOnDebug = false);
                return true;
            }
            catch(Exception $e) {
                Yii::log(__CLASS__ . ' (:' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" Exception Message:' . $e->getMessage().PHP_EOL.'Yii Mail Message: '.print_r($message, true), CLogger::LEVEL_ERROR);
                return false;
            }
        }
    }
