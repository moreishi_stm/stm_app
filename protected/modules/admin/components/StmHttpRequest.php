<?php

/**
 * StmHttpRequest
 * Extension for CHttpRequest, provides easy access to $_COOKIES
 * @author Chris Willard
 * @since 11/7/2012
 */
class StmHttpRequest extends CHttpRequest {

	/**
	 * Checks if cookie exists
	 * @param  string $name cookie key name
	 * @return boolean       
	 */
	public function hasCookie($name) {

		return Yii::app()->request->cookies->contains($name);
	}

	/**
	 * Returns cookie value
	 * @param  string $name cookie key name
	 * @return CHttpCookie the cookie associated with the given name.       
	 */
    public function getCookie($name) {

        return Yii::app()->request->cookies[$name];
    }

	/**
	 * Creates/Updates a cookie by key name
	 * @param string $name cookie key name
	 * @param mixed $value data to be stored in cookie
	 * @param int cookie expiration
	 * @param boolean make the cookie http request only?
	 * @return string cookie value       
	 */
    public function setCookie($name, $value, $time=0, $disableClientCookies=false) {

		$cookie           = new CHttpCookie($name, $value);
		$cookie->expire   = time() + $time;
		$cookie->httpOnly = $disableClientCookies;   
        Yii::app()->request->cookies[$name] = $cookie;
    }

    /**
     * Unsets a cookie
     * @param string $name cookie key name
     * @return null
     */
    public function removeCookie($name) {

        unset(Yii::app()->request->cookies[$name]);
    }
}
