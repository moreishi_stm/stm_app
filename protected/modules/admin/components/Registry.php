<?php
/**
 * This class implements the Registry pattern, where we can save objects in a
 * global library and have them accessible throughout the whole application
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 7/10/13
 */
final class Registry extends CApplicationComponent {
	/**
	 * The internal class library
	 *
	 * @var array
	 */
	private static $library;

	/**
	 * Returns an object reference from the library
	 *
	 * @param string $name
	 * @return object|null
	 */
	public static function get($name=null) {
		self::_init();

		if(empty($name)) {
			return null;
		}

		if(array_key_exists($name, self::$library)) {
			return self::$library[$name];
		}
	}

	/**
	 * Sets a reference to an object in the internal library
	 *
	 * @param string $name
	 * @param object $obj
	 */
	public static function set($name=null, $obj) {
		self::_init();

		if(empty($name)) {
			return;
		}

		self::$library[$name] = $obj;
	}

	/**
	 * Initialize the library
	 */
	protected static function _init() {
		if(empty(self::$library)) {
			self::$library = array();
		}
	}
}
?>