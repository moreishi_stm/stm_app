<?php

abstract class LeadRouteRule {

    private $_leadRoute;

    abstract public function getNotificationList();

    abstract protected function getRouteType();

    public function setLeadRoute(LeadRoutes $leadRoute) {
        $this->_leadRoute = $leadRoute;
    }

    protected function getLeadRoute() {
        return $this->_leadRoute;
    }

    public function isApplicable() {
        $isApplicable = ($this->getRouteType() == $this->_leadRoute->type_ma);
        return $isApplicable;
    }
}