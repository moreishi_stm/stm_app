<?php
Yii::import('admin_module.components.observers.leads.RouteObserver');

/**
 * Base implementation for the various lead route observers
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class AgentRouteObserver extends RouteObserver
{
    /**
     * @var Transactions the transaction returned from the CEvent component.
     * @since 1.0
     */
    protected $transaction;

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $event
     *
     * @param $usersToNotify
     *
     * @return bool
     */
    protected function afterInitLead($event, $usersToNotify)
    {
        $this->transaction = $event->sender;

        /**
         * Check for an existing transaction, if it exists then no need to create the transaction.
         * Create a activity log entry for the re-submit and prevent the transaction from being created
         */
        if ($this->transaction->getIsResubmit()) {
            $existingTransaction = $this->transaction->getResubmitTransaction();

            if ($existingTransaction->transaction_status_id == TransactionStatus::TRASH_ID) {
                $existingTransaction->transaction_status_id = TransactionStatus::NURTURING_ID;
                $existingTransaction->save();
                //@todo: should add log note that status was changed from trash to nurturing.

            }

            $this->addLeadSubmitActivityLog();
            $this->sendResubmitAlerts();

            //send alerts ot any triggered routes
            $triggeredRoutes = $this->_leadRoute->triggeredRoutes;
            if (!empty($triggeredRoutes)) {
                foreach ($triggeredRoutes as $triggeredRoute) {
                    switch ($triggeredRoute->context_ma) {
                        /**
                         * This will process a company route and process an additional assignment
                         */
                        case LeadRoutes::CONTEXT_LENDER_ID:
                            // get lender that is assigned & alert them
                            $lenders = $existingTransaction->assignmentLender;
                            if(empty($lenders)) {
                                // Get the first applicable route rule - shift or round robbin
                                $rule = $this->getAvailableRouteRules($applicableRouteRule=true, $triggeredRoute);
                                if (!empty($rule)) {
                                    $lenders = $rule->getNotificationList();
                                }
                            } else {
                                $lenders = array($lenders);
                            }

                            if (!empty($lenders)) {
                                $contacts = array();
                                foreach($lenders as $lender) {
                                    $contacts[] = $lender->contact;
                                }
                                $this->sendSmsAlerts(
                                    $contacts, $existingTransaction, $isResubmit = true, $this->transaction->formSubmission
                                ); //@todo: need to pass down the form alert_label... how to get access to it??
                                $this->sendEmailAlerts(
                                    $contacts, $existingTransaction, $isResubmit = true, $this->transaction->formSubmission
                                ); //@todo: need to pass down the form alert_label... how to get access to it??
                            }
                            // @todo: what if no lender??? notify current lender on route & alert them?????

                            break;
                    }
                }
            }

            return null;
        }

        return $event;
    }


    /**
     *
     */
    protected function addLeadSubmitActivityLog()
    {
        if($this->transaction) {
            $isResubmit = $this->transaction->getIsResubmit();
            $existingTransaction = $this->transaction->getResubmitTransaction();

        } elseif($this->sender->getIsResubmit() == false) {
            $existingTransaction = $this->sender;
            $isResubmit = false;
        }

        $contact = $this->transaction->contact;

        $transactionExistingLog = new ActivityLog('leadGenCreation');
        $transactionExistingLog->activity_date = new CDbExpression('NOW()');
        $transactionExistingLog->component_id = $existingTransaction->id;
        $transactionExistingLog->component_type_id = $existingTransaction->component_type_id;
        $transactionExistingLog->task_type_id = ($isResubmit) ? TaskTypes::RESUBMIT : TaskTypes::NEW_LEAD_SUBMIT;

        $formFields = new FormFields;
        $formSubmission = $_POST['FormSubmissionValues']['data'];

        if(isset($formSubmission[FormFields::STATE_ID]) && !empty($formSubmission[FormFields::STATE_ID]) && is_numeric($formSubmission[FormFields::STATE_ID])) {
            $formSubmission[FormFields::STATE_ID] = AddressStates::getNameById($formSubmission[FormFields::STATE_ID], $opt=array('short_name'=>true));
        }
        // process basic fields for the activity log note, leaving the "others" to follow afterwards
        $basicFieldIds = array(FormFields::FIRST_NAME_ID, FormFields::EMAIL_ID, FormFields::PHONE_ID);
        $transactionExistingLog->note = ($isResubmit) ? 'Resubmit: ' : '';

        foreach($basicFieldIds as $basicFieldId) {
            if(isset($formSubmission[$basicFieldId]) && !empty($formSubmission[$basicFieldId])) {
                $transactionExistingLog->note .= (empty($transactionExistingLog->note))? '' : ', ';

                $transactionExistingLog->note .= $formSubmission[$basicFieldId];
                if($basicFieldId == FormFields::FIRST_NAME_ID) {
                    if(isset($formSubmission[FormFields::LAST_NAME_ID]) && !empty($formSubmission[FormFields::LAST_NAME_ID])) {
                        $transactionExistingLog->note .= ' '.$formSubmission[FormFields::LAST_NAME_ID];
                        unset($formSubmission[FormFields::LAST_NAME_ID]);
                    }
                }
                unset($formSubmission[$basicFieldId]);
            }
        }

        // concatenate address fields for log note
        if(isset($formSubmission[FormFields::ADDRESS_ID]) && !empty($formSubmission[FormFields::ADDRESS_ID])) {
            $transactionExistingLog->note .= PHP_EOL.'Address: '.$formSubmission[FormFields::ADDRESS_ID].', '.$formSubmission[FormFields::CITY_ID].', '.$formSubmission[FormFields::STATE_ID].' '.$formSubmission[FormFields::ZIP_ID].PHP_EOL;
            unset($formSubmission[FormFields::ADDRESS_ID]);
            unset($formSubmission[FormFields::CITY_ID]);
            unset($formSubmission[FormFields::STATE_ID]);
            unset($formSubmission[FormFields::ZIP_ID]);
        }

        // process the leftover form submissions after the basic fields have been formatted into the log note
        foreach ($formSubmission as $formFieldId => $fieldValue) {
            $transactionExistingLog->note .= (empty($transactionExistingLog->note))? '' : PHP_EOL;
            $transactionExistingLog->note .= $formFields->getFieldLabelById($formFieldId).': '.$fieldValue;
        }

        if (!$transactionExistingLog->save(false)) {
            Yii::log(
                'Could not log resubmit info.' . print_r($transactionExistingLog->attributes, true) . PHP_EOL
                . 'Model Errors: ' . print_r($transactionExistingLog->errors, true), CLogger::LEVEL_ERROR,
                self::LOG_CATEGORY
            );
        }
    }

    protected function sendResubmitAlerts()
    {
        if (!($leadRoute = $this->getLeadRoute())) {
            Yii::log('Could not find a applicable lead route for new lead.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            return null;
        }

        $usersToNotify = $this->getUsersToNotify($leadRoute);
        if (!$usersToNotify) {
            return false;
        }

        $agentContactIds = array();
        foreach ($usersToNotify as $userToNotify) {
            $agentContactIds[] = $userToNotify->id;
        }

        // also notify anyone else that is assigned to this transaction
        if ($assignments = $this->transaction->getResubmitTransaction()->assignments) {
            foreach ($assignments as $assignment) {
                if (!in_array($assignment->assignee_contact_id, $agentContactIds)) {
                    array_push($usersToNotify, $assignment->contact);
                }
            }
        }

        $this->sendSmsAlerts(
            $usersToNotify, $this->transaction->getResubmitTransaction(), $isResubmit = true, $this->transaction->formSubmission
        ); //@todo: need to pass down the form alert_label... how to get access to it??
        $this->sendEmailAlerts(
            $usersToNotify, $this->transaction->getResubmitTransaction(), $isResubmit = true, $this->transaction->formSubmission
        ); //@todo: need to pass down the form alert_label... how to get access to it??
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $event
     */
    protected function processNewLead($event, $usersToNotify)
    {
        if (empty($this->transaction)) {
            $this->transaction = Transactions::model()->findByPk($event->sender->id);
        }

        foreach ($usersToNotify as $userToNotify) {
            $LeadRouteAssigned = new LeadRouteAssigned;
            $LeadRouteAssigned->setAttributes(
                array(
                    'lead_route_id'     => $this->getLeadRoute()->id,
                    'agent_contact_id'  => $userToNotify->id,
                    'component_type_id' => $this->getLeadRoute()->component_type_id,
                    'component_id'      => $this->transaction->id,
                )
            );
            if (!$LeadRouteAssigned->save()) {
                $errors = print_r($LeadRouteAssigned->getErrors(), true);
                Yii::log(
                    'Lead route assigned did not save properly. Errors: ' . $errors, CLogger::LEVEL_ERROR,
                    self::LOG_CATEGORY
                );
            }
        }

        $this->createAssignments($usersToNotify);
        $this->sendWelcomeEmail($usersToNotify);
        $this->applyAutoResponders($usersToNotify);
        $this->sendSmsAlerts(
            $usersToNotify, $this->transaction, $isResubmit = $event->sender->isResubmit,
            $event->sender->formSubmission
        );
        $this->sendEmailAlerts(
            $usersToNotify, $this->transaction, $isResubmit = $event->sender->isResubmit,
            $event->sender->formSubmission
        );

        return $event;
    }

    /**
     * Retrieves the lead route based on if the lead route is applicable, as defined by each observer
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     */
    protected function getLeadRoute()
    {
        if (empty($this->_leadRoute)) {
            $this->_leadRoute = LeadRoutes::model()->byComponentTypeId($this->sender->component_type_id)->find(
                array(
                    'order' => 'sort_order asc',
                )
            );
        }

        return $this->_leadRoute;
    }

    /**
     * Attaches the agents to the transaction via the Assignments model
     *
     * @author   Chris Willard <chris@seizethemarket.com>
     * @since    1.0
     *
     * @param $usersToNotify
     *
     * @internal param \LeadRoutes $leadRoute
     */
    protected function createAssignments($usersToNotify)
    {
        foreach ($usersToNotify as $userToNotify) {
            $assignment = new Assignments;
            $assignment->assignee_contact_id = $userToNotify->id;
            $assignment->component_type_id = $this->transaction->component_type_id;
            $assignmentTypeId = $this->getAssignmentType($assignment->component_type_id);
            $assignment->assignment_type_id = $assignmentTypeId;
            $assignment->component_id = $this->transaction->id;
            $assignment->updated_by = $this->transaction->contact_id;
            $assignment->save();
        }
    }

    /**
     * Sends a welcome email from a collection of agents to the contact tagged in the transaction
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $usersToNotify
     *
     * @return bool
     */
    protected function sendWelcomeEmail($usersToNotify)
    {
        if (!$this->transaction->contact or !$this->transaction->contact->getPrimaryEmailObj()) {
            Yii::log(
                'Could not retrieve the contact\'s email object to send welcome email.', CLogger::LEVEL_ERROR,
                self::LOG_CATEGORY
            );
            return false;
        }

        /**
         * @todo if multiple agents exist or team email wants to be sent, how to process
         */
        $userToNotify = current($usersToNotify);

        $emailMessage = new EmailMessage;
        $emailMessage->origin_type_ma = EmailMessage::SYSTEM_AUTO_NEW_ACCOUNT_WELCOME;
        $emailMessage->subject = 'Your New Account Info!';
        $emailMessage->component_type_id = $this->transaction->component_type_id;
        $emailMessage->component_id = $this->transaction->id;

        // this needs to be positioned after EmailMessage is instantiated.
        if (empty($userToNotify)) {
            Yii::log(__CLASS__.' ('.__LINE__.') Missing agent to notify in lead route. Used first owner instead and BCC\'d all owners.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            $ownerAssignments = AuthAssignment::model()->byAccountId(Yii::app()->user->accountId)->findAllByAttributes(array('itemname' => 'owner'));
            $bccString = '';
            foreach ($ownerAssignments as $key => $ownerContact) {
                if ($key == 0) {
                    $userToNotify = $ownerContact->contact;
                } else {
                    $bccString .= (empty($bccString)) ? '' : ',';
                    $bccString .= $ownerContact->contact->getPrimaryEmail();
                }
            }
            $emailMessage->to_email_bcc = $bccString;

            $to = $userToNotify->contact->getPrimaryEmail();
            $to .= (empty($bccString)) ? '' : ',' . $bccString;

            $leadRouteMissingContactMessage = 'Please login and check your Lead Routes and verify that the setting for default contact is set.';

            if (!YII_DEBUG) {
                $subject = 'Alert! ACTION REQUIRED:  Lead Route Missing Default Contact.';
                StmZendMail::easyMail(array('Do-Not-Reply@SeizetheMarket.com'=>'Seize the Market Notification'), $to, $subject, $leadRouteMissingContactMessage, StmMailMessage::DEBUG_EMAIL_ADDRESS);
            }
        }

        $emailMessage->to_email_id = $this->transaction->contact->getPrimaryEmailObj()->id;
        $emailMessage->from_email_id = $userToNotify->getPrimaryEmailObj()->id;
        if ($emailMessage->save()) {
            $message = new StmMailMessage;
            $message->view = 'welcomeEmail';
            $message->init($emailMessage);
            Yii::app()->mail->send($message);
        } else {
            Yii::log('Could not send welcome email.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
        }
    }

    /**
     * Sends SMS alerts for a new lead, could be changed out later with more internal alerts as well
     *
     * @todo   This needs to be replaced with new SMS code
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param      $usersToNotify
     * @param      $transaction
     * @param bool $isResubmit
     * @param null $formSubmission
     *
     * @return bool
     */
    public function sendSmsAlerts($usersToNotify, $transaction, $isResubmit = false, $formSubmission = null)
    {
        foreach ($usersToNotify as $userToNotify) {
            $contactFullName = $transaction->contact->getFullName();
            $frmPrimaryPhone = Yii::app()->format->formatPhone($transaction->contact->getPrimaryPhone());
            $contactPrimaryEmail = $transaction->contact->getPrimaryEmail();

            // If it's a seller type of transaction, add the address to the text message
            $sellerAddress = "";
            if ($transaction->address instanceof Addresses and
                $transaction->getComponentTypeId() == ComponentTypes::SELLERS
            ) {
                $address = $transaction->address;
                $sellerAddress = $address->address . PHP_EOL;
                $sellerAddress .= $address->city . ', ' . AddressStates::getShortNameById($address->state_id) . ' ' . $address->zip;
            }

            $messagePrefix = ($isResubmit === true) ? 'Resubmit' : 'New';
            $message = '';

            $message .= ($formSubmission) ? (($formSubmission->form->alert_label) ?
                $formSubmission->form->alert_label . PHP_EOL : '') : '';

             if(!empty($formSubmission->formValueTuples) && isset($formSubmission->formValueTuples[FormFields::SOURCE_DESCRIPTION_ID]) && !empty($formSubmission->formValueTuples[FormFields::SOURCE_DESCRIPTION_ID])) {
                  $message .= $formSubmission->formValueTuples[FormFields::SOURCE_DESCRIPTION_ID] . PHP_EOL;
             }


            $message .= $messagePrefix . ' WebLead Alert!' . PHP_EOL
                . $contactFullName . PHP_EOL
                . $frmPrimaryPhone . PHP_EOL
                . $contactPrimaryEmail . PHP_EOL
                . $sellerAddress;

            $formField = new FormFields;
            foreach($_POST['FormSubmissionValues']['data'] as $i => $value) {
                if(!in_array($i, array(FormFields::FIRST_NAME_ID, FormFields::LAST_NAME_ID, FormFields::EMAIL_ID, FormFields::PHONE_ID, FormFields::ADDRESS_ID, FormFields::CITY_ID, FormFields::STATE_ID, FormFields::ZIP_ID ))) {
                    $message .= PHP_EOL.$formField->getFieldLabelById($i).': '.$value;
                }
            }

            if(!YII_DEBUG) {
                Yii::app()->plivo->sendMessage($userToNotify->settings->cell_number, $message);
            }

            // CLee copied on everything
            if (!YII_DEBUG) {
                $referrerUrl = PHP_EOL . str_replace(array('www.','http://'), '', $_SERVER['HTTP_REFERER']);
                Yii::app()->plivo->sendMessage('9043433200', '(CLee Copy)' . PHP_EOL . $message . $referrerUrl);
            }
        }
    }

    /**
     * @param      $usersToNotify
     * @param      $transaction
     * @param bool $isResubmit
     * @param null $formSubmission
     */
    public function sendEmailAlerts($usersToNotify, $transaction, $isResubmit = false, $formSubmission = null)
    {

        $ccContactIds = $this->getLeadRoute()->cc_contact_ids;
        if(!empty($ccContactIds)) {

            $criteria = new CDbCriteria;
            $criteria->addInCondition('id', $ccContactIds);
            $ccContacts = Contacts::model()->findAll($criteria);

            // get list of existing contact ids to prevent duplication
            $existingIds = array();
            foreach($usersToNotify as $existingContacts) {
                $existingIds[] = $existingContacts->id;
            }

            if(!empty($ccContacts)) {
                foreach($ccContacts as $ccContact) {
                    if(!in_array($ccContact->id, $existingIds)) {
                        array_push($usersToNotify, $ccContact);
                    }
                }
            }
        }

        $contactFullName = $transaction->contact->getFullName();
        $frmPrimaryPhone = Yii::app()->format->formatPhone($transaction->contact->getPrimaryPhone());
        $contactPrimaryEmail = $transaction->contact->getPrimaryEmail();

        // If it's a seller type of transaction, add the address to the text message
        $sellerAddress = "";
        if ($transaction->address instanceof Addresses and
            $transaction->getComponentTypeId() == ComponentTypes::SELLERS
        ) {
            $address = $transaction->address;
            $sellerAddress = $address->address . PHP_EOL;
            $sellerAddress .= $address->city . ', ' . AddressStates::getShortNameById($address->state_id) . ' ' . $address->zip;
        }

        $messagePrefix = ($isResubmit === true) ? 'Resubmit' : 'New';
        $message = '';

        $message .= ($formSubmission) ? (($formSubmission->form->alert_label) ?
            $formSubmission->form->alert_label . PHP_EOL : '') : '';
        $message .= $messagePrefix . ' WebLead Alert!' . PHP_EOL
            . 'Name: ' . $contactFullName . PHP_EOL
            . 'Phone: ' . $frmPrimaryPhone . PHP_EOL
            . 'Email: ' . $contactPrimaryEmail . PHP_EOL
            . $sellerAddress.PHP_EOL;

        $formValues = FormSubmissionValues::model()->findAllByAttributes(
            array('form_submission_id' => $formSubmission->id)
        );
        $formData = '<br />';
        if (!empty($formValues)) {
            foreach ($formValues as $formValue) {
                if (!in_array($formValue->form_field_id, array(FormFields::FIRST_NAME_ID, FormFields::LAST_NAME_ID, FormFields::EMAIL_ID, FormFields::PHONE_ID, FormFields::ADDRESS_ID, FormFields::CITY_ID, FormFields::STATE_ID, FormFields::ZIP_ID)))
                {
                    if (!isset($formValue->formField)) {
                        Yii::log(__CLASS__ . ' ('.__LINE__.') Form Field not available in email alert. Form Submission Values: ' . print_r($formValue->attributes, true) . ' Form Field: ' . print_r($formValue->formField, true), CLogger::LEVEL_ERROR,self::LOG_CATEGORY);
                    }
                    $formData .= $formValue->formField->label . ': ' . $formValue->value . '<br />';
                }
            }
        } else {
            $emailAlertFormValuesErrorMessage = 'Form Submission Values not available in email alert. Form Submission Values: ' . print_r($formSubmission->attributes, true);
            Yii::log(__CLASS__.' ('.__LINE__.') '.$emailAlertFormValuesErrorMessage, CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
        }

        if($assignments = $transaction->assignments) {
            $assignmentInfo = '';
            foreach($assignments as $assignment) {
                $assignmentInfo .= $assignment->assignmentType->display_name.': '.$assignment->contact->fullName.'<br />';
            }
            $assignmentInfo .= '<br/>';
        }
        $from = array('Do-Not-Reply-Lead-Alert@SeizetheMarket.com'=>'Lead Alert (Seize the Market)');
        $bcc = 'christine@seizethemarket.com';

        if($formSubmission->form->component_type_id==ComponentTypes::SELLERS) {
            $subject='Seize the Market - Seller Lead Alert - '.$address->address;
        } else {
            $subject='Seize the Market - Lead Alert';
        }

        $adminUrl
            = 'http://www.' . Yii::app()->user->getPrimaryDomain()->name . '/admin/' . $transaction->componentType->name
            . '/' . $transaction->id;
        $leadInfo = $message;
        $message = '<html><body style="font-size:14px;">';
        $message .= '<h2>' . $messagePrefix . ' ' . $transaction->componentType->singularName . ' Lead</h2>';
        $leadInfo = str_replace(PHP_EOL, '<br />', $leadInfo);
        $message .= $leadInfo . $formData . '<br /><br />';
        $message .= $assignmentInfo;
        $message .= 'You have received a New Lead. Please contact them immediately.<br /><br />';
        $message .= 'Login to: <a href="' . $adminUrl . '">' . $adminUrl . '</a><br /><br />';
        $message .= 'Go and Seize the Market! =)<br /><br />';
        $message .= '<img src="http://www.' . Yii::app()->user->getPrimaryDomain(
            )->name . '/images/logo.png" style="width:200px;">';
        $message .= '</body></html>';


        if(YII_DEBUG) {
            $emailAlertToEmail = StmMailMessage::DEBUG_EMAIL_ADDRESS;
        } else {
            $emailAlertToEmail = array();
            foreach ($usersToNotify as $userToNotify) {
                $emailAlertToEmail[$userToNotify->getPrimaryEmail()] = $userToNotify->fullName;
            }
        }

        if (!(StmZendMail::easyMail($from, $emailAlertToEmail, $subject, $message, $bcc))) {
            Yii::log(__CLASS__.'('.__LINE__.') Email alert for lead was not sent. Message: ' . $message, CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
        }
    }

    /**
     * Applies auto responders as necessary
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $usersToNotify
     *
     * @return array collection of applied auto responders
     */
    protected function applyAutoResponders($usersToNotify)
    {
        $autoResponders = LeadRouteAutoresponder::model()->findAllByAttributes(
            array(
                'lead_route_id' => $this->getLeadRoute()->id,
            )
        );
        if (!$autoResponders) {
            return null;
        }

        // Apply the auto responders for each unique agent/contact instance
        $appliedAutoResponders = array();
        foreach ($usersToNotify as $userToNotify) {
            foreach ($autoResponders as $autoResponder) {
                if ($this->applyAutoResponder($userToNotify, $autoResponder)) {
                    array_push($appliedAutoResponders, $autoResponder);
                }
            }
        }

        return $appliedAutoResponders;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param Contacts               $userToNotify
     * @param LeadRouteAutoresponder $autoResponder
     *
     * @return bool|void
     */
    protected function applyAutoResponder(Contacts $userToNotify, LeadRouteAutoresponder $autoResponder)
    {
        $isApplied = false;

        switch ($autoResponder->leadRoute->component_type_id) {
            case ComponentTypes::BUYERS:
            case ComponentTypes::SELLERS:
                $isApplied = $autoResponder->apply($userToNotify, $this->transaction);
                break;

            default:
                $warningMessage = strtr(
                    'Could not apply responder with unknown component type: :componentTypeId', array(
                        ':componentTypeId' => $autoResponder->leadRoute->component_type_id,
                    )
                );
                Yii::app()->log($warningMessage, 'warning');
                break;
        }

        return $isApplied;
    }

    /**
     * Returns the correct assignment type id based on the component id
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  9/22/2013
     *
     * @param $componentTypeId
     *
     * @return mixed
     */
    private function getAssignmentType($componentTypeId)
    {
        $assignmentTypeId = null;
        switch ($componentTypeId) {
            case ComponentTypes::BUYERS:
                $assignmentTypeId = AssignmentTypes::BUYER_AGENT;
                break;

            case ComponentTypes::SELLERS:
                $assignmentTypeId = AssignmentTypes::LISTING_AGENT;
                break;

            default:
                $assignmentTypeId = AssignmentTypes::ASSIGNED_TO;
                break;
        }

        return $assignmentTypeId;
    }
}