<?php
Yii::import(RouteObserver::RULES_PATH_ALIAS . '.*');

/**
 * Base implementation for the various lead route observers
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
abstract class RouteObserver extends CComponent
{
    const RULES_PATH_ALIAS = 'admin_module.components.observers.leads.rules';

    /**
     * @var string the category used to store the log entries under.  Should be used for all Yii::log() calls.
     * @since 1.0
     */
    const LOG_CATEGORY = 'leadgen';

    /**
     * @var array the agents to notify of the lead based on each child route observer
     * @since 1.0
     */
    protected $usersToNotify = array();

    protected $sender;

    /**
     * @var LeadRoute the lead route that is currently being utilized
     * @since 1.0
     */
    protected $_leadRoute;

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $event
     *
     * @return bool
     */
    protected function initLead($event)
    {
        // Sanity Check for correct sender class
        if (!($event->isValid = $this->eventSenderIsValid($event))) {
            Yii::log('Event sender is not valid.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            return null;
        }

        if (!($leadRoute = $this->getLeadRoute())) {
            Yii::log('Could not find a applicable lead route for new lead.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            return null;
        }

        $usersToNotify = $this->getUsersToNotify($leadRoute);
        if (!$usersToNotify) {
            Yii::log('There are no user(s) to notify on this lead route.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            return null;
        }

        $event = $this->afterInitLead($event, $usersToNotify);

        return $event;
    }

    abstract protected function afterInitLead($event, $usersToNotify);


    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $event
     */
    protected function afterLeadCreated($event)
    {
        if (!$this->eventSenderIsValid($event)) {
            return null;
        }

        //if it is a new lead log it in activity log since it could not be done before - this is a hack but the rewrite is almost done so whatever...
        $this->addLeadSubmitActivityLog();


        if (!($leadRoute = $this->getLeadRoute())) {
            return null;
        }

        $usersToNotify = $this->getUsersToNotify($leadRoute);
        if (!$usersToNotify) {
            Yii::log('There are no user(s) to notify on this lead route.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
            return null;
        }

        $event = $this->processNewLead($event, $usersToNotify);

        /**
         * Process any triggered routes
         */
        $triggeredRoutes = $leadRoute->triggeredRoutes;
        if (!empty($triggeredRoutes)) {
            foreach ($triggeredRoutes as $triggeredRoute) {
                switch ($triggeredRoute->context_ma) {
                    /**
                     * This will process a company route and process an additional assignment
                     */
                    case LeadRoutes::CONTEXT_LENDER_ID:
                        // Get the first applicable route rule
                        $rule = $this->getAvailableRouteRules($applicableRouteRule=true, $triggeredRoute);

                        /** @var LeadRouteRule $rule */
                        if (!empty($rule)) {
                            $lenders = $rule->getNotificationList();
                            if (!empty($lenders)) {
                                foreach ($lenders as $lender) {
                                    $lenderAssignment = new Assignments;
                                    $lenderAssignment->assignee_contact_id = $lender->id;
                                    $lenderAssignment->component_type_id = $this->getLeadRoute()->component_type_id;
                                    $lenderAssignment->assignment_type_id = AssignmentTypes::LOAN_OFFICER;
                                    $lenderAssignment->component_id = $this->sender->id;
                                    $lenderAssignment->updated_by = $this->sender->contact_id;
                                    $lenderAssignment->attachEventHandler('onAfterSave', array(new LenderRouteObserver, 'afterLeadCreated'));
                                    $lenderAssignment->save();
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    protected function addLeadSubmitActivityLog() {
        // this is a hack that will go away with the refactor. Had to do this so it would record initial submits for transactions via AgentRouteObserver method.
    }

    /**
     * @param $event
     * @param $usersToNotify
     *
     * @return mixed
     */
    abstract protected function processNewLead($event, $usersToNotify);

    /**
     * @return mixed
     */
    abstract protected function getLeadRoute();

    /**
     * @return mixed
     */
    public function getUsersToNotify(LeadRoutes $leadRoute)
    {
        /** @var LeadRouteRule $applicableRule */
        $applicableRule = $this->getAvailableRouteRules($applicableOnly=true, $leadRoute);

        return $applicableRule->getNotificationList();
    }

    public function getAvailableRouteRules($applicableOnly=false, $leadRoute=null)
    {
        $availableRouteRules = array();

        $filePattern = Yii::getPathOfAlias(self::RULES_PATH_ALIAS) . DS . '*RouteRule.php';
        foreach (glob($filePattern) as $ruleClassPath) {
            // Get the class name from the file path
            $ruleClassParts = explode(DS, $ruleClassPath);
            $ruleClassName = end($ruleClassParts);

            // Remove the file extension
            $ruleClassNameParts = explode('.', $ruleClassName);
            $ruleClassName = current($ruleClassNameParts);

            // Test the class name
            if (!empty($ruleClassName)) {
                try {
                    /** @var LeadRouteRule $ruleClass */
                    $ruleClass = new $ruleClassName;
                    $leadRoute = ($leadRoute === null) ? $this->getLeadRoute() : $leadRoute;
                    $ruleClass->setLeadRoute($leadRoute);
                    if ($ruleClass instanceof LeadRouteRule) {
                        if ($applicableOnly and !$ruleClass->isApplicable()) {
                            continue;
                        }
                        array_push($availableRouteRules, $ruleClass);
                    }
                } catch (Exception $error) {
                    continue;
                }
            }
        }

        // Should only be one route rule if applicability is applied
        return ($applicableOnly) ? current($availableRouteRules) : $availableRouteRules;
    }


    /**
     * Validates a event sender under the presumption that the sender is a instance of a Transaction model
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param CEvent $event
     *
     * @return bool
     */
    protected function eventSenderIsValid(CEvent $event)
    {
        $sender = $event->sender;
        $this->sender = $sender;

        return true;
    }
}