<?php

/**
 * Determines how to aggregate a collection of agents to notify for a new lead
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 * @see    RouteObserver
 */
class LeadShiftRouteRule extends LeadRouteRule
{
    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     *
     * @param none
     *
     * @return int lead route type ma
     */
    protected function getRouteType()
    {
        return LeadRoutes::ROUTE_TYPE_SHIFT_ID;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getNotificationList()
    {
        $contactsToNotify = array();

        $shifts = LeadShift::model()->byLeadRouteId($this->getLeadRoute()->id)->byCurrentDate()->withinCurrentTime()
            ->findAll();
        if ($shifts) {
            foreach ($shifts as $shift) {
                if ($shift->contact) {
                    array_push($contactsToNotify, $shift->contact);
                }
            }
        } else {
            $overflowContact = $this->getLeadRoute()->getOverflowAgent();
            if ($overflowContact) {
                array_push($contactsToNotify, $overflowContact);
            }
        }

        return (!empty($contactsToNotify)) ? $contactsToNotify : null;
    }
}