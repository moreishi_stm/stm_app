<?php

/**
 * Determines how to aggregate a collection of agents to notify for a new lead
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 * @see    RouteObserver
 */
class LeadRoundRobinRouteRule extends LeadRouteRule
{
    /**
     * @return int
     */
    protected function getRouteType()
    {
        return LeadRoutes::ROUTE_TYPE_ROUND_ROBIN_ID;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     * @return mixed
     */
    public function getNotificationList()
    {
        $actingUser = $this->getLeadRoute()->getNextRoundRobinAgent();
        if ($actingUser) {
            $actingUser = array($actingUser);
        }
        return ($actingUser) ? $actingUser : null;
    }
}
