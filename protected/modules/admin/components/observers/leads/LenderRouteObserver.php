<?php
Yii::import('admin_module.components.observers.leads.RouteObserver');

/**
 * Base implementation for the various lead route observers
 *
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
class LenderRouteObserver extends RouteObserver
{
    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $event
     *
     * @param $usersToNotify
     *
     * @return bool
     */
    protected function afterInitLead($event, $usersToNotify)
    {
        return $event;
    }

    /**
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     *
     * @param $event
     *
     * @param $usersToNotify
     *
     * @return mixed|void
     */
    protected function processNewLead($event, $usersToNotify)
    {
        $this->transaction = $event->sender;

        return $event;
    }

    /**
     * Retrieves the lead route based on if the lead route is applicable, as defined by each observer
     *
     * @author Chris Willard <chris@seizethemarket.com>
     * @since  1.0
     */
    protected function getLeadRoute()
    {
        if (!$this->_leadRoute) {
            $this->_leadRoute = LeadRoutes::model()->findByAttributes(
                array(
                    'context_ma' => LeadRoutes::CONTEXT_LENDER_ID,
                ),
                array(
                    'order' => 'sort_order asc',
                )
            );
        }

        return $this->_leadRoute;
    }
}