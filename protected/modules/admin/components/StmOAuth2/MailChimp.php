<?php
/**
 * Namespace Declaration
 */
namespace StmOAuth2;

// Include our base adapter
include_once('Base.php');

/**
 * Base
 *
 * @author Christine Lee
 * @package StmOAuth2
 */
class MailChimp extends Base
{
    /**
     * Name of the service
     *
     * @var string
     */
    protected $_name = 'Mail Chimp';

    /**
     * Type name. Used for get "type" variable
     *
     * @var string
     */
    protected $_type = 'mailchimp';

    /**
     * Level of service. Either Account or Contact
     *
     * @var string
     */
    protected $_level = 'Account';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_authorizeUrl = 'https://login.mailchimp.com/oauth2/authorize';

    /**
     * Token Url
     *
     * @var string
     */
    protected $_tokenUrl = 'https://login.mailchimp.com/oauth2/token';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_redirectUrl = 'http://www.seizethemarket.com/oauth2callback/type/mailchimp'; //@todo: figure out

    /**
     * OAuth2 Key
     *
     * @var string
     */
    protected $_key = '187996071018';

    /**
     * OAuth2 Secret
     *
     * @var string
     */
    protected $_secret = 'cf8fd9efb741e54269c41d38f2beb249';

    /**
     * Get Authorize Url
     *
     * @return string authorize url
     * @throws \Exception
     */
    public function getAuthorizeUrl()
    {
        if(!$this->_authorizeUrl) {
            throw new \Exception('Missing Authorize Url.');
        }
//        return $this->_authorizeUrl.'?scope=email%20profile&redirect_uri='.urlencode('http://www.seizethemarket.com/oauth2callback/google/'.$cuuid).'&response_type=code&client_id='.$this->_key;
    }

}