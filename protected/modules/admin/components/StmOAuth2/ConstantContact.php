<?php
/**
 * Namespace Declaration
 */
namespace StmOAuth2;

// Include our base adapter
include_once('Base.php');

/**
 * Base
 *
 * @author Christine Lee
 * @package StmOAuth2
 */
class ConstantContact extends Base
{
    /**
     * Name of the service
     *
     * @var string
     */
    protected $_name = 'Constant Contact';

    /**
     * Type name. Used for get "type" variable
     *
     * @var string
     */
    protected $_type = 'constantcontact';

    /**
     * Level of service. Either Account or Contact
     *
     * @var string
     */
    protected $_level = 'Account';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_authorizeUrl = 'https://oauth2.constantcontact.com/oauth2/oauth/siteowner/authorize';

    /**
     * Token Url
     *
     * @var string
     */
    protected $_tokenUrl = 'https://oauth2.constantcontact.com/oauth2/oauth/token';

    /**
     * Authorize Url
     *
     * @var string
     */
//    protected $_redirectUrl = 'http://www.seizethemarket.com/oauth2callback/type/mailchimp'; //@todo: figure out

    /**
     * OAuth2 Key
     *
     * @var string
     */
    protected $_key = 't9ph2y84kggxftakg4g6hu6f';

    /**
     * OAuth2 Secret
     *
     * @var string
     */
    protected $_secret = 'PSaUW5aZSJwcq34YKBAHsmNN';

    /**
     * Process Callback
     *
     * Saves token data passed by client
     */
    public function processCallback()
    {
//        $username = \Yii::app()->request->getParam('username'); //@todo: story in other info in api key table
//        $level = 'Account'; //@todo: temp for constant contact - need to consolidate these settings to make it reusable. right now in Oauth2Controller
//        $x=$apiKey->byIgnoreAccountScope()->findAll();
//
//        // NOTE: IGNORE account scope must be applied. Example: seizethemarket is account #1, the client is #2. System will look for account_id=1 when it should be account_id=2
//        if($existingKey = $apiKey->byIgnoreAccountScope()->findByAttributes(
//            array('type'=>$type, 'account_id'=>$clientAccount->account_id, 'level'=>$level))) {
//            $apiKey = $existingKey;
//        }
//        //url: http://www.christineleeteam.local/admin/massmail/authorize?type=ConstantContact&code=thgR87Bnyif8pKQmOOYPj7JQnWE&username=christine%40christineleeteam.com
//        // constant contact
//        //array (size=3)
//        //  'token' => string '91c678f9-40f3-40e7-ba52-65fe43a5ac62' (length=36)
//        //  'dc' => null
        //  'endpoint' => null

    }
}