<?php
/**
 * Namespace Declaration
 */
namespace StmOAuth2;

// Include our base adapter
include_once('Base.php');

/**
 * Base
 *
 * @author Christine Lee
 * @package StmOAuth2
 */
class Dropbox extends Base
{
    /**
     * Name of the service
     *
     * @var string
     */
    protected $_name = 'Dropbox';

    /**
     * Type name. Used for get "type" variable
     *
     * @var string
     */
    protected $_type = 'dropbox';

    /**
     * Level of service. Either Account or Contact
     *
     * @var string
     */
    protected $_level = 'Account';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_authorizeUrl = 'https://www.dropbox.com/1/oauth2/authorize';

    /**
     * Token Url
     *
     * @var string
     */
    protected $_tokenUrl = 'https://api.dropbox.com/1/oauth2/token';

    /**
     * Authorize Url
     *
     * @var string
     */
//    protected $_redirectUrl = 'http://www.seizethemarket.com/oauth2callback/type/mailchimp'; //@todo: figure out

    /**
     * OAuth2 Key
     *
     * @var string
     */
    protected $_key = 'utg7zwft3nbpj74';

    /**
     * OAuth2 Secret
     *
     * @var string
     */
    protected $_secret = 'brfu0luddb1gox6';


}