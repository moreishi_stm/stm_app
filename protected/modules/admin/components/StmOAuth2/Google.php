<?php
/**
 * Namespace Declaration
 */
namespace StmOAuth2;

// Include our base adapter
include_once('Base.php');

/**
 * Google
 *
 * @author Christine Lee
 * @package StmOAuth2
 *
 * Notes: Api key1=code, key2=access_token, key3=refresh_token
 */
class Google extends Base
{
    /**
     * Name of the service
     *
     * @var string
     */
    protected $_name = 'Google';

    /**
     * Type name. Used for get "type" variable
     *
     * @var string
     */
    protected $_type = 'google';

    /**
     * Level of service. Either Account or Contact
     *
     * @var string
     */
    protected $_level = 'Contact';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_authorizeUrl = 'https://accounts.google.com/o/oauth2/auth';

    /**
     * Token Url
     *
     * @var string
     */
    protected $_tokenUrl = 'https://accounts.google.com/o/oauth2/token';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_redirectUrl = 'http://www.seizethemarket.com/oauth2callback/google';

    /**
     * OAuth2 Key
     *
     * @var string
     */
    protected $_key = '992396858350-5rfcsq5r5n3fd8uoqecs2te5b0frlg6o.apps.googleusercontent.com';

    /**
     * OAuth2 Secret
     *
     * @var string
     */
    protected $_secret = 'GTkhx3sqj9-pI77MVQfIL4Vr';

    /**
     * Scope for API code, a Google specific property ... @todo: incorporate into the redirect url???
     * @var string
     */
    protected $_scope = 'email%20profile%20https://www.googleapis.com/auth/calendar%20https://www.googleapis.com/auth/drive';

    /**
     * Timezone
     *
     * @var string
     */
    protected $_timezone;

    /**
     * Get Authorize Url
     *
     * @return string authorize url
     * @throws \Exception
     */
    public function getAuthorizeUrl()
    {
        if(!$this->_authorizeUrl) {
            throw new \Exception('Missing Authorize Url.');
        }
        if(!$this->_contactId) {
            throw new \Exception('Missing Contact Id.');
        }
        if(!$this->_clientAccount) {
            throw new \Exception('Missing Client Accounts model.');
        }
        $stmComboId = $this->_clientAccount->id.'.'.$this->_contactId;
        $forceRefreshTokenParams = (1)?'&approval_prompt=force&access_type=offline': '';
        return $this->_authorizeUrl.'?scope='.$this->_scope.'&redirect_uri='.urlencode($this->_redirectUrl).'&state='.$stmComboId.'&response_type=code&client_id='.$this->_key.$forceRefreshTokenParams;
    }

    /**
     * Process both Callbacks
     *
     * Saves token data passed by client
     */
    public function processCallback()
    {
        // Example url: http://www.seizethemarket.local/oauth2callback/google?code=123&access_token=456&state=c797088f-6098-11e4-8add-0e25643075f0.1
        $code = \Yii::app()->request->getParam('code');
        if(!$code) {
            throw new \Exception('Access code missing.');
        }

        /***********************************************************************
         * Stm Combo Ids for ClientAccount_uuid.contact_id passed via oAuth2 params //@todo: put in separate method to make reusable????
         */
        if(!\Yii::app()->request->getParam('state')) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Invalid: StmComboIds missing in callback.');
        }
        $stateParts = explode('.', \Yii::app()->request->getParam('state'));

        if(!($this->_clientAccount = \ClientAccounts::model()->findByPk($stateParts[0]))) {
            throw new \Exception('Client Account not found.');
        }
        $this->_contactId = $stateParts[1];

        //********************** END OF Stm Combo Code *************************

        if(!$code) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Invalid: Both Missing Code & Access Token in callback. One of them must exist.');
        }

        if(!$this->_contactId) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Invalid: Contact Id missing in callback.');
        }

        // change db connections per clientAccount
        \StmFunctions::connectDbByClientIdAccountId($this->_clientAccount->client_id, $this->_clientAccount->account_id);

        $this->processCodeToken($code);
    }

    protected function processCodeToken($code)
    {
        //http://www.seizethemarket.com/oauth2callback/google?state=c797088f-6098-11e4-8add-0e25643075f0.1&code=4/tvB_fTrVY2Kfd0tw8098NIlsEqvPROx1L_f7T2RQCtA.0mfNp1vbob0b3oEBd8DOtNDvVjr-kwI
        // if api key not exist create new one
        $apiKey = $this->getExistingApiKey();

        if(!$apiKey) {
            $apiKey = new \ApiKeys();
            $apiKey->setAttributes(array(
                    'account_id' => $this->_clientAccount->account_id,
                    'contact_id' => $this->_contactId,
                    'type' => $this->_type,
                    'level' => $this->_level,
                    'key1' => $code,
                    'added' => new \CDbExpression('NOW()'),
                ));
        }
        else {
            $apiKey->key1 = $code;
        }

        if(!$apiKey->save()) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') API Key did not save properly. Error Message: '.print_r($apiKey->getErrors(), true).' Api Key Attributes: '.print_r($apiKey->attributes, true));
        }

        $oAuthClient = new \OAuth2\Client($this->_key, $this->_secret);
        $response = $oAuthClient->getAccessToken($this->_tokenUrl, 'authorization_code', array(
                'code'          => $code,
                'redirect_uri'	=> $this->_redirectUrl, // NOTE: Do NOT urlencode()
            ));

        // Make sure we had an access token
        if(empty($response['result']['access_token'])) {
            throw new \Exception('Unable to retrieve an access token at this time, please check your information try again later.');
        }

        $apiKey->setAttributes(array(
                'key2' => $response['result']['access_token'],
                'key3' => $response['result']['refresh_token'],
                'other_data'=> json_encode($response['result'])
            ));
        if(!$apiKey->save()) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') API Key did not save properly. Error Message: '.print_r($apiKey->getErrors(), true).' Api Key Attributes: '.print_r($apiKey->attributes, true));
        }

        // forward to the user's original site to get token with the stored code
        $domain = \Domains::model()->byIgnoreAccountScope()->findByAttributes(array('is_primary'=>1,'is_active'=> 1,'account_id'=>$this->_clientAccount->account_id));
        if(!$domain) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Domain not found');
        }

        if(YII_DEBUG) {
            $strToReplace = substr($domain->name, strrpos($domain->name,'.'));
            $domainName = str_replace($strToReplace,'.local',$domain->name);
        }
        else {
            $domainName = $domain->name;
        }

        // forward to success page
        header('Location: http://www.'.$domainName.'/admin/oauth2/success/google');
    }

    /**
     * List Calendars
     *
     * @return null
     */
    public function listCalendars()
    {
        $calendarList = array();
        if($client = $this->_verifyAccess()) {
            $calendarService = new \Google_Service_Calendar($client);
            $calendarList = $calendarService->calendarList->listCalendarList(array('minAccessRole'=>'writer'));
            $calendarList->getItems();
        }
        return $calendarList;
    }

    /**
     * View Calendar Event

     * @param $id
     *
     * @return null
     */
    public function viewCalendarEvents($calendarId, $start='Y-m-01\TH:i:sP', $end='Y-m-t\TH:i:sP', $singleEvent=false)
    {
        $client = $this->_verifyAccess();
        $calendarService = new \Google_Service_Calendar($client);

        $events = $calendarService->events->listEvents($calendarId, $optParams=array('timeMin'=>$start, 'timeMax'=>$end, 'singleEvents'=>$singleEvent,'maxResults'=>350)); // 'orderBy'=>'starttime',

        return $events->getItems();
    }

    /**
     * Add Calendar Event
     *
     * @param      $calendarId
     * @param      $startDatetime
     * @param      $endDatetime
     * @param      $summary
     * @param null $location
     * @param null $description
     *
     * @return mixed
     */
    public function addCalendarEvent($calendarId, $startDatetime, $endDatetime, $summary, $location=null, $description=null, $colorId=null)
    {
        $client = $this->_verifyAccess();
        $calendarService = new \Google_Service_Calendar($client);

        // add a new event
        $event = new \Google_Service_Calendar_Event();
        $event->setSummary($summary);

        if($location) {
            $event->setLocation($location);
        }

        if($description) {
            $event->setDescription($description);
        }

        if($colorId) {
            $event->setColorId($colorId);
        }

        $event->setStart($this->_formatGoogleDateTime($startDatetime));
        $event->setEnd($this->_formatGoogleDateTime($endDatetime));

        $createdEvent = $calendarService->events->insert($calendarId, $event);
        $id= $createdEvent->getId();

        return $id;
    }

    /**
     * Edit Calendar Event
     *
     * @param      $calendarId
     * @param      $eventId
     * @param      $startDatetime
     * @param      $endDatetime
     * @param      $summary
     * @param null $location
     * @param null $description
     *
     * @return string
     * @throws \Exception if existing calendar even not found
     */
    public function editCalendarEvent($calendarId, $eventId, $startDatetime, $endDatetime, $summary, $location=null, $description=null, $status=null)
    {
        $client = $this->_verifyAccess();

        // general google API access differs from a specific calendar access as this may be a different user that doesn't have access to the calendar linked to this task
        if(!$this->verifyCalendarAccess($client, $calendarId)) {
            return false;
        }

        $calendarService = new \Google_Service_Calendar($client);

        // update event information
        if(!($event = $calendarService->events->get($calendarId, $eventId))) {
            \Yii::log(__CLASS__.' (: '.__LINE__.') Calendar Event not found. Calendar ID: '.$calendarId.' Event ID: '.$eventId, \CLogger::LEVEL_ERROR);
            throw new \Exception('Calendar Event not found.');
        }

        // || $event->status=='cancelled'

        $event->setSummary($summary);

        if($location) {
            $event->setLocation($location);
        }
        if($description) {
            $event->setDescription($description);
        }

        //@todo: what to do if appointment is cancelled??
        // status can be "confirmed" or "cancelled"
        if($status) {
            $event->setStatus($status);
        }

//        $event->getStart()->setTimeZone($this->_timezone->timezone);
        $event->setStart($this->_formatGoogleDateTime($startDatetime));
//        $event->getEnd()->setTimeZone($this->_timezone->timezone);
        $event->setEnd($this->_formatGoogleDateTime($endDatetime));
        $updatedEvent = $calendarService->events->update($calendarId, $event->getId(), $event);
        return $updatedEvent->getId().' Updated: '.$updatedEvent->getUpdated();
    }

    /**
     * Get Calendar Event
     *
     * @param      $calendarId
     * @param      $eventId
     *
     * @return string
     * @throws \Exception if existing calendar even not found
     */
    public function getCalendarEvent($calendarId, $eventId)
    {
        $client = $this->_verifyAccess();
        $calendarService = new \Google_Service_Calendar($client);

        // get event information
        try {
            $event = $calendarService->events->get($calendarId, $eventId);
        }
        catch(\Exception $e) {
            \Yii::log(__CLASS__.' (: '.__LINE__.') Calendar Event not found. Exception Message: '.$e->getMessage().' Calendar ID: '.$calendarId.' Event ID: '.$eventId, \CLogger::LEVEL_ERROR);
        }

        return $event;
    }

    /**
     * Set Timezone
     *
     * @param $timezone
     *
     * @return void
     */
    public function setTimeZone($timezone)
    {
        $this->_timezone = $timezone;
    }

    /**
     * Verify Access using data from ApiKeys table
     * If ApiKeys record does not exist, returns false
     *
     * @return \Google_Client
     * @throws \Exception if ApiKey record not found
     */
    protected function _verifyAccess()
    {
        if(!($apiKey = \ApiKeys::model()->findByAttributes(array('contact_id'=>\Yii::app()->user->id, 'type'=>$this->_type))) || !$apiKey->key2) {
            throw new \Exception('Api Key does not exist.');
        }

        $client = new \Google_Client();
        $client->setApplicationName("Seize the Market");
        $client->setClientId($this->_key);
        $client->setClientSecret($this->_secret);
        $client->setAccessToken(json_encode(array('access_token'=>$apiKey->key2)));

        if($client->isAccessTokenExpired()) {
            // this will throw exception if invalid to be caught on higher level
            $client->refreshToken($apiKey->key3);
        }
        return $client;
    }

    // not sure if this should be somehow wrapped into verifyAccess in general? @todo: ask nicole - CLee 4/2/16
    public function verifyCalendarAccess($calendarId)
    {
        $hasAccess = false;
        foreach($this->listCalendars() as $calendar) {
            if($calendar = $calendarId) {
                $hasAccess = true;
                return true;
            }
        }

        return false;
    }

    /**
     * Takes a datetime string and formats it into a Google Calendar Event Datetime class.
     * Gets timezone from user account if available. If not, throws exceiption.
     *
     * @param               $dateTime
     * @param \DateTimeZone $timezone
     *
     * @return \Google_Service_Calendar_EventDateTime
     * @throws \Exception If timezone data not available, throws exception
     */
    protected function _formatGoogleDateTime($dateTime, \DateTimeZone $timezone=null)
    {
        $timezone = ($timezone)? $timezone : new \DateTimeZone(\Yii::app()->user->account->timezone);
        if(!$timezone) {
            throw new \Exception('Timezone is missing.');
        }

        $startDateTime = new \DateTime($dateTime, $timezone);

//\Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI, Timezone: '.print_r($timezone, true) . PHP_EOL
//    .'Start Datetime Obj: '.print_r($startDateTime, true). PHP_EOL
//    .'UTC: ' . $startDateTime->format('P') . PHP_EOL
////    .'Server Default Timezone: '.$serverDefaultTimezone . PHP_EOL
////    .'Current Default Timezone: '.$x
//    , \CLogger::LEVEL_ERROR);

        // google event datetime handles time zones and daylight savings
        $googleDatetime = new \Google_Service_Calendar_EventDateTime();
        $googleDatetime->setDateTime($startDateTime->format('c'));
        $googleDatetime->setTimeZone($timezone->timezone);
//        date_default_timezone_set($serverDefaultTimezone);

        return $googleDatetime;
    }
}