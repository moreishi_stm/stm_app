<?php
/**
 * Namespace Declaration
 */
namespace StmOAuth2;

// Include our base adapter
include_once('Base.php');

/**
 * Google
 *
 * @author Christine Lee
 * @package StmOAuth2
 *
 * Notes: Api key1=code, key2=access_token, key3=refresh_token
 */
class Facebook extends Base
{
    /**
     * Name of the service
     *
     * @var string
     */
    protected $_name = 'Facebook';

    /**
     * Type name. Used for get "type" variable
     *
     * @var string
     */
    protected $_type = 'facebook';

    /**
     * Level of service. Either Account or Contact
     *
     * @var string
     */
    protected $_level = '';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_authorizeUrl = 'https://www.facebook.com/dialog/oauth';
    //https://www.facebook.com/dialog/oauth?client_id=180522222360769&redirect_uri=http://www.seizethemarket.com/oauth2callback/facebook

    /**
     * Token Url
     *
     * @var string
     */
    protected $_tokenUrl = 'https://graph.facebook.com/oauth/access_token';

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_redirectUrl = 'http://www.seizethemarket.com/oauth2callback/facebook';

    /**
     * OAuth2 Key
     *
     * @var string
     */
    protected $_key = '180522222360769';
//FB App ID: 180522222360769
    /**
     * OAuth2 Secret
     *
     * @var string
     */
    protected $_secret = '7c345848fbb246cec67e7b0c56f803f9';

    /**
     * Scope for API code, a Google specific property ... @todo: incorporate into the redirect url???
     * @var string
     */
//    protected $_scope = 'email%20profile%20https://www.googleapis.com/auth/calendar%20https://www.googleapis.com/auth/drive';

    /**
     * Get Authorize Url
     *
     * @return string authorize url
     * @throws \Exception
     */
    public function getAuthorizeUrl()
    {
        if(!$this->_authorizeUrl) {
            throw new \Exception('Missing Authorize Url.');
        }
        if(!$this->_contactId) {
            throw new \Exception('Missing Contact Id.');
        }
        if(!$this->_clientAccount) {
            throw new \Exception('Missing Client Accounts model.');
        }
        $stmComboId = $this->_clientAccount->id.'.'.$this->_contactId;
        $forceRefreshTokenParams = (1)?'&approval_prompt=force&access_type=offline': '';
        return $this->_authorizeUrl.'?scope='.$this->_scope.'&redirect_uri='.urlencode($this->_redirectUrl).'&state='.$stmComboId.'&response_type=code&client_id='.$this->_key.$forceRefreshTokenParams;
    }

    /**
     * Process both Callbacks
     *
     * Saves token data passed by client
     */
    public function processCallback()
    {
        // Example url: http://www.seizethemarket.local/oauth2callback/google?code=123&access_token=456&state=c797088f-6098-11e4-8add-0e25643075f0.1
        $code = \Yii::app()->request->getParam('code');
        if(!$code) {
            throw new \Exception('Access code missing.');
        }

        /***********************************************************************
         * Stm Combo Ids for ClientAccount_uuid.contact_id passed via oAuth2 params //@todo: put in separate method to make reusable????
         */
        if(!\Yii::app()->request->getParam('state')) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Invalid: StmComboIds missing in callback.');
        }
        $stateParts = explode('.', \Yii::app()->request->getParam('state'));

        if(!($this->_clientAccount = \ClientAccounts::model()->findByPk($stateParts[0]))) {
            throw new \Exception('Client Account not found.');
        }
        $this->_contactId = $stateParts[1];

        //********************** END OF Stm Combo Code *************************

        if(!$code) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Invalid: Both Missing Code & Access Token in callback. One of them must exist.');
        }

        if(!$this->_contactId) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Invalid: Contact Id missing in callback.');
        }

        // change db connections per clientAccount
        \StmFunctions::connectDbByClientIdAccountId($this->_clientAccount->client_id, $this->_clientAccount->account_id);

        $this->processCodeToken($code);
    }

    protected function processCodeToken($code)
    {
        //http://www.seizethemarket.com/oauth2callback/google?state=c797088f-6098-11e4-8add-0e25643075f0.1&code=4/tvB_fTrVY2Kfd0tw8098NIlsEqvPROx1L_f7T2RQCtA.0mfNp1vbob0b3oEBd8DOtNDvVjr-kwI
        // if api key not exist create new one
        $apiKey = $this->getExistingApiKey();

        if(!$apiKey) {
            $apiKey = new \ApiKeys();
            $apiKey->setAttributes(array(
                    'account_id' => $this->_clientAccount->account_id,
                    'contact_id' => $this->_contactId,
                    'type' => $this->_type,
                    'level' => $this->_level,
                    'key1' => $code,
                    'added' => new \CDbExpression('NOW()'),
                ));
        }
        else {
            $apiKey->key1 = $code;
        }

        if(!$apiKey->save()) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') API Key did not save properly. Error Message: '.print_r($apiKey->getErrors(), true).' Api Key Attributes: '.print_r($apiKey->attributes, true));
        }

        $oAuthClient = new \OAuth2\Client($this->_key, $this->_secret);
        $response = $oAuthClient->getAccessToken($this->_tokenUrl, 'authorization_code', array(
                'code'          => $code,
                'redirect_uri'	=> $this->_redirectUrl, // NOTE: Do NOT urlencode()
            ));

        // Make sure we had an access token
        if(empty($response['result']['access_token'])) {
            throw new \Exception('Unable to retrieve an access token at this time, please check your information try again later.');
        }

        $apiKey->setAttributes(array(
                'key2' => $response['result']['access_token'],
                'key3' => $response['result']['refresh_token'],
                'other_data'=> json_encode($response['result'])
            ));
        if(!$apiKey->save()) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') API Key did not save properly. Error Message: '.print_r($apiKey->getErrors(), true).' Api Key Attributes: '.print_r($apiKey->attributes, true));
        }

        // forward to the user's original site to get token with the stored code
        $domain = \Domains::model()->byIgnoreAccountScope()->findByAttributes(array('is_primary'=>1,'is_active'=> 1,'account_id'=>$this->_clientAccount->account_id));
        if(!$domain) {
            throw new \Exception(__CLASS__.'(:'.__LINE__.') Domain not found');
        }

        if(YII_DEBUG) {
            $strToReplace = substr($domain->name, strrpos($domain->name,'.'));
            $domainName = str_replace($strToReplace,'.local',$domain->name);
        }
        else {
            $domainName = $domain->name;
        }

        // forward to success page
        header('Location: http://www.'.$domainName.'/admin/oauth2/success/google');
    }
}