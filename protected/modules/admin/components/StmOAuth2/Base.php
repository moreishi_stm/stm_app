<?php
/**
 * Namespace Declaration
 */
namespace StmOAuth2;

// Use this namespaces
use \StmOAuth2;

/**
 * Base
 *
 * @author Christine Lee
 * @package StmOAuth2
 */
abstract class Base
{
    /**
     * Name of the service
     *
     * @var string
     */
    protected $_name;

    /**
     * Type name. Used for get "type" variable
     *
     * @var string
     */
    protected $_type;

    /**
     * Level of service. Either Account or Contact
     *
     * @var string
     */
    protected $_level;

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_authorizeUrl;

    /**
     * Token Url
     *
     * @var string
     */
    protected $_tokenUrl;

    /**
     * Authorize Url
     *
     * @var string
     */
    protected $_redirectUrl;

    /**
     * OAuth2 Key
     *
     * @var string
     */
    protected $_key;

    /**
     * OAuth2 Secret
     *
     * @var string
     */
    protected $_secret;

    /**
     * Account Id
     *
     * @var int
     */
    protected $_accountId; //@todo: Ask Nicole, should this be public or protected?

    /**
     * Contact Id
     *
     * @var int
     */
    protected $_contactId; //@todo: Ask Nicole, should this be public or protected?

    /**
     * Client Accounts model
     *
     * @var \ClientAccounts
     */
    protected $_clientAccount; //@todo: Ask Nicole, should this be public or protected?

    /**
     * Get Authorize Url abstract fucntion
     */
    abstract function getAuthorizeUrl();

//    /**
//     * Gets Key value
//     * @return string key
//     */
//    public function getKey()
//    {
//        return $this->_key;
//    }
//
//    /**
//     * Gets Secret value
//     * @return string key
//     */
//    public function getSecret()
//    {
//        return $this->_secret;
//    }

    /**
     * Set Client Account model
     * @param \ClientAccounts
     * @return void
     */
    public function setClientAccount(\ClientAccounts $model)
    {
        $this->_clientAccount = $model;
    }

    /**
     * Set Contact Id
     * @param $id
     *
     * @throws \Exception
     */
    public function setContactId($id)
    {
        if(!is_numeric($id)) {
            throw new \Exception('Contact ID must be an integer.');
        }
        $this->_contactId = $id;
    }

    /**
     * Gets Existing ApiKey model if it exists
     *
     * @return mixed ApiKeys model if found, null if not found
     * @throws \Exception
     */
    public function getExistingApiKey()
    {
        if(!$this->_clientAccount) {
            throw new \Exception('Client Accounts model is required.');
        }
        $attributes = array('type'=>$this->_type,'account_id'=>$this->_clientAccount->account_id);

        if($this->_level == 'Contact') {
            if(!$this->_contactId) {
                throw new \Exception('Contact Id does not exist.');
            }
            $attributes['contact_id'] = $this->_contactId;
        }

        return \ApiKeys::model()->byIgnoreAccountScope()->findByAttributes($attributes);
    }

    public function getName()
    {
        return $this->_name;
    }
}