<?php
	/**
	 * Custom CActiveForm implementation.
	 *
	 * @author Will Madison (will@willmadison.com)
	 */

	class StmActiveForm extends CActiveForm {

		public static function validate($models, $attributes = null, $loadInput = true, $clearErrors = true) {
			$result = array();

			if (!is_array($models)) {
				$models = array($models);
			}

			foreach ($models as $model) {
				if ($loadInput && isset($_POST[get_class($model)])) {
					$model->attributes = $_POST[get_class($model)];
				}

				$model->validate($attributes, $clearErrors);

				foreach ($model->getErrors() as $attribute => $errors) {
					$result[CHtml::activeId($model, $attribute)] = $errors;
				}
			}

			return function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);
		}
	}