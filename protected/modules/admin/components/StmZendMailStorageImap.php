<?php
/**
 * Zend Mail Storage Imap
 *
 * Abstraction to swap out the mail message class we made with helper functions
 */
class StmZendMailStorageImap extends Zend_Mail_Storage_Imap
{
    /**
     * Message Class
     *
     * The class to use for each message
     * @var string Message Class Name
     */
    protected $_messageClass = 'StmZendMailMessageImap';

}