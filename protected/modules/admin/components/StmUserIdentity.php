<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 *
 * Predefined Error Codes from CUserIdentity:
 * ERROR_USERNAME_INVALID = 1
 * ERROR_PASSWORD_INVALID = 2
 *
 */
class StmUserIdentity extends CUserIdentity
{
	const ERROR_USER_NOT_WITH_DOMAIN = 3;

	const ERROR_MSG_EMAIL_PASS      = 'Email or password incorrect, please try again.';
	const ERROR_MSG_NOT_WITH_DOMAIN = 'Sorry, we could not locate your account.';
	const ERROR_MSG_DEFAULT         = 'Sorry, we encountered an error.';

	private $_contactModel;

	/**
	 * Attempt to login a user by locating their e-mail address and then matching the password
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$this->errorCode=self::ERROR_NONE;

        $ContactEmail = Emails::model()->byIgnoreAccountScope()->byEmail($this->username)->find(); //Emails::model()->byEmail($this->username)->find();

		/** @var Contacts $Contact */
        $Contact = ( $ContactEmail ) ? $ContactEmail->contact : null;

		if ( !$Contact ) {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		} else if ( $Contact->password!==$this->password || in_array('inactive', $Contact->getItemNames())) {
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		} else {

			$Domain = $Contact->account->associatedWithDomain();

			if(!$Domain) {
				$this->errorCode=self::ERROR_USER_NOT_WITH_DOMAIN;
			} else {

                $AuthAssignments = AuthAssignment::model()->findByAttributes(array(
                        'userid' => $Contact->id,
                        'account_id' => $Domain->account_id
                    ));

				if(Yii::app()->controller->module->id != 'front' && !(strpos($_SERVER['SERVER_NAME'], 'seizethemarket') && Yii::app()->controller->id=='site') && empty($AuthAssignments)) {
					$this->errorCode=self::ERROR_USER_NOT_WITH_DOMAIN;
				} else {
                    $criteria = new CDbCriteria();
                    $criteria->compare('userid',$Contact->id);
                    $criteria->addInCondition('itemname', array('owner','agent','lender'));
                    $criteria->addNotInCondition('account_id', array(Yii::app()->user->accountId));
                    $Contact->linkedAccounts = AuthAssignment::model()->byIgnoreAccountScope()->findAll($criteria); //@todo: need to exclude if they are "inactive"
					$Contact->currentAccountId = $Domain->account_id;
				}
			}
		}

		$this->_contactModel = $Contact;

		return !$this->errorCode;
	}

	public function getContact()
	{
		return $this->_contactModel;
	}

	/**
	 * Instead of having the identity return the username, return the id instead
	 * @return type
	 */
	public function getId()
	{
		return $this->contact->id;
	}

	/**
	 * errorMessage
	 * Return the error message associated with this object
	 *
	 * @param int $errorCode
	 * @return string
	 */
	public static function errorMessage($errorCode=null)
	{
		$errorMessages = array(
			self::ERROR_USERNAME_INVALID     => self::ERROR_MSG_EMAIL_PASS,
			self::ERROR_PASSWORD_INVALID     => self::ERROR_MSG_EMAIL_PASS,
			self::ERROR_USER_NOT_WITH_DOMAIN => self::ERROR_MSG_NOT_WITH_DOMAIN,
		);

		if ( !$errorCode )
			$errorCode = self::$errorCode;

		if ( $errorMessages[$errorCode] )
			return $errorMessages[$errorCode];

		return self::ERROR_MSG_DEFAULT;
	}
}