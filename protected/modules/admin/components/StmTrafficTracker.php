<?php

	class StmTrafficTracker extends CApplicationComponent {

		public function saveTrackingData() {
			$userAgent = \Yii::app()->userAgent->getInfo();
			if (strtolower($userAgent['typ']) != 'robot') {

				$model = new \TrafficTracker;
				$model = $this->loadTrackingData($model, $userAgent);
				if (!$model->save()) {
					$this->emailNotify($model);
				}
			}
		}

		public function loadTrackingData(TrafficTracker $model, $userAgent) {

			if (!\Yii::app()->user->isGuest) {
				$model->contact_id = \Yii::app()->user->id;
			}

    //		if(Yii::app()->controller->action->id == 'home')
    //			$model->home_detail_view_count = $model->home_detail_view_count+1;

            $model->attributes = \StmTrafficTracker::getUtmTrackingData();

			return $model;
		}

        /**
         * getUtmTrackingData()
         * Dependent on Yii::app()->userAgent->getInfo()
         * return array loaded with utm code / google tracking data
         */
        public static function getUtmTrackingData() {
            $userAgent = \Yii::app()->userAgent->getInfo();

            // Parse __utmz cookie
            list($domain_hash, $timestamp, $session_number, $campaign_number, $campaign_data) = preg_split('[\.]', $_COOKIE["__utmz"], 5);
            parse_str(strtr($campaign_data, "|", "&"));
            $utmData = array(
                'user_agent_type' => $userAgent['typ'],
                'user_agent_name' => $userAgent['ua_name'],
                'user_agent_os_name' => $userAgent['os_name'],
                'home_detail_view_count' => \PageViewTrackerWidget::countHomesViewed($unique = true),
                'pageview_cookie' => $_COOKIE['page_views'],

                'campaign_source' => $utmcsr,
                'campaign_name' => $utmccn,
                'campaign_medium' => $utmcmd,

                'utmz' => $_COOKIE['__utmz'],
                'utma' => $_COOKIE['__utma'],
                'utmb' => $_COOKIE['__utmb'],
                'utmc' => $_COOKIE['__utmc'],

                'domain' => str_replace(array('www.','http://'), '', $_SERVER['SERVER_NAME']),
                'page' => $_SERVER['REQUEST_URI'],
                'referrer' => str_replace(array('www.','http://'), '', $_SERVER['HTTP_REFERER']),
                'added' => date('Y-m-d H:i:s'),
            );

            if (!\Yii::app()->user->isGuest) {
                $utmData['contact_id'] = \Yii::app()->user->id;
            }

            if (isset($utmcct)) {
                $utmData['campaign_content'] = $utmcct;
            }
            if (isset($utmctr)) {
                $utmData['campaign_term'] = $utmctr;
            }

            // Parse the __utma Cookie
            list($domain_hash, $random_id, $time_initial_visit, $time_beginning_previous_visit, $time_beginning_current_visit, $session_counter) = preg_split('[\.]', $_COOKIE["__utma"]);
            if (date("Y-m-d H:i:s", $time_initial_visit) > '1969-12-31 19:00:00') {
                $utmData['first_visit'] = date("Y-m-d H:i:s", $time_initial_visit);
            }
            if (date("Y-m-d H:i:s", $time_beginning_previous_visit) > '1969-12-31 19:00:00') {
                $utmData['previous_visit'] = date("Y-m-d H:i:s", $time_beginning_previous_visit);
            }
            if (date("Y-m-d H:i:s", $time_beginning_current_visit) > '1969-12-31 19:00:00') {
                $utmData['current_visit_started'] = date("Y-m-d H:i:s", $time_beginning_current_visit);
            }

            $utmData['times_visited'] = $session_counter;

            // Parse the __utmb Cookie
            list($domain_hash, $pages_viewed, $garbage, $time_beginning_current_session) = preg_split('[\.]', $_COOKIE["__utmb"]);
            $utmData['pages_viewed'] = $pages_viewed;

            $utmData['ip_id'] = \TrafficTrackerIps::getIdByIp(\Yii::app()->stmFunctions->getVisitorIp());
            $utmData['session_id'] = \TrafficTrackerSessions::getIdBySessionId(session_id());

            return $utmData;
        }

		public function emailNotify($model) {
			$message = date('Y-m-d | H:i:s');
			$message .= PHP_EOL . 'Traffic Data Raw Error';
			$errorMsg = print_r($model->getErrors(), true);;

			$message .= PHP_EOL . 'Model Error Message: => ' . $errorMsg;
			$referrer = str_replace(array(
					'www.',
					'http://'
				), '', $_SERVER['HTTP_REFERER']
			);
			$message .= PHP_EOL . 'URL: => ' . str_replace(array(
						'www.',
						'http://'
					), '', $_SERVER['SERVER_NAME']
				) . $_SERVER['REQUEST_URI'];
			$message .= PHP_EOL . 'Referrer: => ' . $referrer;
			$message .= PHP_EOL . 'UTMZ: =>' . $model->utmz;
			$message .= PHP_EOL . 'IP: => ' . \Yii::app()->stmFunctions->getVisitorIp();

			@mail("Admin@SeizetheMarket.com", "Traffic Data Raw Error", $message, "From: TrafficRawData@SeizetheMarket.com");
		}
	}