<?php

/**
 * Stm Session
 *
 * Used to add support for JSON column data of session
 */
class StmSession extends CDbHttpSession
{
    /**
     * Session write handler.
     * Do not call this method directly.
     * @param string $id session ID
     * @param string $data session data
     * @return boolean whether session write is successful
     */
    public function writeSession($id,$data)
    {
        // Call parent write session
        $result = parent::writeSession($id, $data);

        // Store session data in JSON format as well so we can read it in other programming languages that don't have support to de-serialize PHP sessions
        $db = $this->getDbConnection();
        $db->createCommand()->update($this->sessionTableName,array(
            'json'  =>  json_encode(array(
                'contact_id'    =>  Yii::app()->user->id,
                'account_id'    =>  Yii::app()->user->account->id,
                'client_id'     =>  Yii::app()->user->clientId,
                'domain'        =>  Yii::app()->user->domain->name
            ))
        ),'id=:id',array(':id'=>$id));

        // Return result from parent call
        return $result;
    }
}