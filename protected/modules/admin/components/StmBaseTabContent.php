<?php
Yii::import('zii.widgets.CPortlet');

class StmBaseTabContent extends CPortlet
{
	public $containerId;
	public $containerClass;
	public $tabButtons = array();
	public $tabButtonBreak;
	public $contentCssClass = '';

	public $decorationHtmlOptions;

	public function init()
	{
		if ( $this->containerId )
			$this->id = $this->containerId;

		$this->htmlOptions = CMap::mergeArray($this->htmlOptions, array(
			'class'=>'tabContent '.$this->containerClass,
		));

		parent::init();
	}

	public function renderTabButtons()
	{
		if ( !$this->tabButtons )
			return;

		// Create a string of button links
		$buttons = '';
		foreach( $this->tabButtons as $button )
		{
			if ($button['visible'] === false)
				continue;

			$buttonText = $button['label'];

			if ($button['display'] == 'inline')
				$button['htmlOptions']['class'] .= ' button';

			if ($button['type'] == 'link') {
				$button['htmlOptions']['class'] .= ' text-button';
				$buttons .= CHtml::link($buttonText, $button['link'], $button['htmlOptions']);
			}else{
				$button['htmlOptions']['class'] .= ' button';
				$buttons .= CHtml::tag('span', CMap::mergeArray(array(
					'name' => $button['name'],
				), $button['htmlOptions']),$buttonText);
			}
		}
		return CHtml::tag('div', $htmlOptions=array(
			'class'=>'ui-panel-button'
		), $buttons);

	}
}
