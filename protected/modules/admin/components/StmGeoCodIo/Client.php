<?php

// Stash this in this namespace
namespace StmGeoCodIo;

/**
 * Client
 *
 */
class Client
{
    /**
     * API Key
     *
     * @var string
     */
    protected $_apiKey;

    /**
     * Construct
     */
    public function __construct($apiKey = null)
    {
        // Set API Key, use default STM API key if we didn't specify one
        $this->_apiKey = $apiKey ? $apiKey : 'da97077d072252f7df65f720df1f9d9f1f15ff7';
    }

    /**
     * Make call
     *
     * Executes an API call
     * @param string $uri The request URI for the specific API call
     * @param string $action The HTTP action (GET, POST, DELETE, etc.)
     * @param array $getParams GET variables to send
     * @param array $postParams POST variables to post
     * @return array JSON decoded response
     * @throws Exception When unable to decode JSON response
     */
    protected function _makeCall($uri, $action = 'GET', array $getParams = array(), array $postParams = array())
    {
        // Construct URL
        $url = '?api_key=' . $this->_apiKey . (count($getParams) ?  '&' . http_build_query($getParams) : '');

        // Initialize and configure cURL
        $ch = curl_init('https://api.geocod.io/v1' . $uri . '?api_key=' . $this->_apiKey);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER  =>  true,
            CURLOPT_CUSTOMREQUEST   =>  $action,
            CURLOPT_HTTPHEADER      =>  array(
                'Content-Type: application/json'
            )
        ));

        // If we have POST params, add them and make this a POST request
        if(count($postParams)) {
            curl_setopt_array($ch, array(
                CURLOPT_POST        =>  true,
                CURLOPT_POSTFIELDS  =>  json_encode($postParams)
            ));
        }

        // Execute cURL call and get HTTP status code
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Close cURL
        curl_close($ch);

        // Make sure that we had a 2xx status
        if($status < 200 || $status > 299) {
            throw new \Exception('HTTP error code ' . $status . ' has occurred while attempting an API call to GeoCo.IO!'.print_r($response,1));
        }

        // Attempt to decode JSON result
        $response = json_decode($response, true);
        if(json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Error, unable to decode JSON response from Land Voice API call!');
        }

        // Return for further processing
        return $response;
    }

    /**
     * Batch Geocode
     *
     * @return array
     * @throws \Exception When something went wrong
     */
    public function batchGeocode(array $addresses)
    {
        return $this->_makeCall('/geocode', 'POST', array(), $addresses);
    }
}