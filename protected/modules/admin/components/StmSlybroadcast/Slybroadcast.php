<?php
// Declare namespace
namespace StmSlybroadcast;

/**
 * SlyBroadcast
 *
 * @package StmSlybroadcast
 */
class Slybroadcast
{
    /**
     * Email
     *
     * Email address for API authentication
     * @var string
     */
    protected $_email;

    /**
     * Password
     *
     * Password for API authentication
     * @var string
     */
    protected $_password;

    /**
     * API Endpoint
     *
     * The location we are POSTing to
     * @var string
     */
    protected $_apiEndpoint = 'https://www.mobile-sphere.com/gateway/vmb.php';

    /**
     * Construct
     *
     * Instantiates the object
     */
    public function __construct()
    {
        $this->_email = \SettingAccountValues::model()->getSettingsByAccountSettingId(\Yii::app()->user->accountId, \Settings::SETTING_SLY_BROADCAST_EMAIL_ID);
        $this->_password  = \SettingAccountValues::model()->getSettingsByAccountSettingId(\Yii::app()->user->accountId, \Settings::SETTING_SLY_BROADCAST_PASSWORD_ID);

//        $this->_email = 'admin@seizethemarket.com';
//        $this->_password  = 'Stm2016!';
    }

    /**
     * Do Post
     *
     * Makes a POST to the Slybroadcast API
     * @param array $fields Fields (key/value pairs) to include in POST
     * @return string HTTP Response
     */
    protected function _doPost(array $fields = array())
    {
        // Add in default fields required for all API calls
        $fields = array_merge(array(
            'c_uid'         =>  $this->_email,
            'c_password'    =>  $this->_password
        ), $fields);

        // Initialize and configure cURL
        $ch = curl_init($this->_apiEndpoint);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER  =>  true,
            CURLOPT_POST            =>  true,
            CURLOPT_POSTFIELDS      =>  http_build_query($fields, '', '&')
        ));

        // Execute cURL call and close
        $response = curl_exec($ch);
        curl_close($ch);

        // Make sure that the response was successful
        //OK\nsession_id=7062677994\nnumber of phone=25
        if(strpos(trim($response), 'OK') === false) {
            //ERROR/nNo enough fund remaining\n
            \Yii::log(__CLASS__.' (:'.__LINE__.') Error, unable to complete API call, response: ' . print_r($response, true));
            $responseParts = explode("\n", $response);
            $responseMessage = (isset($responseParts[1])) ? $responseParts[1] : "Error, unable to complete API call.";
            throw new SlyBroadcastException($responseParts[1]);
        }

        // Return the response
        return $response;
    }

    /**
     * Create Campaign
     *
     * API call to create a campaign
     * @param Campaign $campaign The campaign object to use
     * @return string API response
     */
    public function createCampaign(Campaign $campaign)
    {
        // Construct fields
        $fields = array(
            'c_record_audio'    =>  $campaign->recordAudio,
            'c_url'             =>  $campaign->audioUrl,
            'c_phone'           =>  $campaign->getPhonesForPost(),
            'c_callerID'        =>  $campaign->callerId,
            'c_date'            =>  $campaign->date,
            'mobile_only'       =>  $campaign->mobileOnly,
            'c_dispo_url'       =>  $campaign->dispoUrl
        );

        // Execute API POST and return results
        return $this->_doPost($fields);
    }
}
