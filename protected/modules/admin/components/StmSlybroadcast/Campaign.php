<?php
namespace StmSlybroadcast;

/**
 * Campaign
 *
 * @package StmSlybroadcast
 */
class Campaign
{
    /**
     * Record Audio
     *
     * Use if file was created with slybroadcast Recording Center. Must contain the 5-digit file name entered during the recording.
     * @var string
     */
    public $recordAudio;

    /**
     * Audio URL
     *
     * Use if providing your own audio file (.WAV format preferred).
     * @var string
     */
    public $audioUrl;

    /**
     * Phones
     *
     * Destination phone number(s) or list, separated by comma.
     * @var array
     */
    protected $_phones = array();

    /**
     * Caller ID
     *
     * Caller ID of the campaign (Required)
     * @var string
     */
    public $callerId;

    /**
     * Date
     *
     * Date and time of delivery. Use format “YYYY-MM-DD HH:MM:SS”, all times must be eastern time. To send out a campaign immediately, enter “now”.
     * @var string
     */
    public $date;

    /**
     * Mobile Only
     *
     * When 0, sends to all phone types, when 1, sends to only mobile phones.
     * @var string
     */
    public $mobileOnly;

    /**
     * Dispo URL
     *
     * Webhook URL to post back results.
     * @var string
     */
    public $dispoUrl = '';

    /**
     * Add Phone
     *
     * Adds a phone entry to the phones list
     * @param $phone Phone number to add
     */
    public function addPhone($phone)
    {
        $this->_phones[] = $phone;
    }

    /**
     * Get Phones For POST
     *
     * Retrieves phones in proper format to POST
     * @return string Phones
     */
    public function getPhonesForPost()
    {
        return implode(',', $this->_phones);
    }
}