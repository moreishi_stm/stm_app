<?php
	class StmBombBomb extends CApplicationComponent {

        public $apiKey;
        const LOG_CATEGORY = 'bombbomb';

//		public $apiKey = '393d4f0b-ebf2-0211-2bd8-abf1adfa28dc';
		const BOMBBOMB_URL = 'https://app.bombbomb.com/app/api/api.php?';

        public function init() {

            if(!($this->apiKey = Yii::app()->user->settings->bombbomb_api_key)) {
                Yii::log('Could not find BombBomb API Key.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                return null;
            }
            parent::init();
        }

		public function callApi($method, $params=null) {
			$paramString = '';
			if($params) {
				foreach($params as $key => $value) {
					$paramString .= '&'.$key.'='.$value;
				}
			}
            if(empty($this->apiKey)) {
                Yii::log('Could not find BombBomb API Key.', CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                return null;
            }
			$url = self::BOMBBOMB_URL.'api_key='.trim($this->apiKey).'&method='.$method.$paramString;
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$response = CJSON::decode(curl_exec($curl));
			curl_close($curl);
			return $response;
		}

		public function getLists() {
			$method='GetLists';
			return $this->callApi($method);
		}

		public function getListContacts($listId) {
			$method='GetListContacts';
			return $this->callApi($method, array('list_id'=>$listId));
		}

		public function addContact($firstName, $lastName, $email, $phone, $lists) { //listlist
			$method='AddContact';
			return $this->callApi($method, array(
										   'firstname'=>$firstName,
										   'lastname'=>$lastName,
										   'eml'=>$email,
										   'phone_number'=>$phone,
										   'listlist'=>$lists,
										   ));
		}

		public function getContact($firstName, $lastName, $email, $phone, $lists) { //listlist
			$method='GetContactFields';
			return $this->callApi($method, array(
										   'firstname'=>$firstName,
										   'lastname'=>$lastName,
										   'eml'=>$email,
										   'phone_number'=>$phone,
										   'listlist'=>$lists,
										   ));
		}

		public function getContactByEmail($email) {
			$method='GetContact';
			return $this->callApi($method, array(
										   'eml'=>$email,
										   ));
		}

		public function createList($name) {
			$method='CreateList';
			return $this->callApi($method, array(
										   'name'=>urlencode($name),
										   ));
		}

		public function addEmailToList($email, $listId) {
			$method='AddEmailToList';
			return $this->callApi($method, array(
										   'new_email_address'=>$email,
										   'list_id'=>$listId,
										   ));
		}

		public function getEmails() {
			$method='GetEmails';
			return $this->callApi($method);
		}

		public function getDrips() {
			$method='GetDrips';
			return $this->callApi($method);
		}

		public function getVideos() {
			$method='GetVideos';
			return $this->callApi($method);
		}

		public function sendEmailByIdToEmailAddress($emailId, $email) {
			$method='SendEmailToEmailAddress';
			return $this->callApi($method, array('email_id'=>$emailId, 'email_address'=>$email));
		}

		public function addToDrip($dripId, $email) {
			$method='addToDrip';
			return $this->callApi($method, array('list_id'=>$listId));
		}
	}