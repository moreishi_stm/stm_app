<?php
class StmAssetManager extends CAssetManager {
	
	private $_published=array();
	public $cdnUrl = NULL;
	public $enableCdn = false;
	protected $moduleName = NULL;
	
	public function init() {
		$this->moduleName = strtolower(Yii::app()->controller->module->id);
		
		return parent::init();
	}
	
	public function getBaseUrl() {
		if(!$this->enableCdn) { //|| $this->moduleName == "admin"
			return parent::getBaseUrl();
		}
		return $this->cdnUrl;
	}
	
	public function setBaseUrl($value) {
		if(!$this->enableCdn) { //|| $this->moduleName == "admin"
			return parent::setBaseUrl($value);
		}
		return $this;
	}
	
	public function publish($path, $hashByName = false, $level = -1, $forceCopy = null) {
		
		if(!$this->enableCdn) { //|| $this->moduleName == "admin"
			return parent::publish($path, $hashByName, $level, $forceCopy);
		}

		if(isset($this->_published[$path])) {
			return $this->_published[$path];
		}
		
		if(($src=realpath($path))!==false) {
			
			$fullDirExploded = explode("/",$path);
			
			//echo(print_r($path, 1)."<br />");
			
			array_pop($fullDirExploded);
			
			$dir=basename(implode("/",$fullDirExploded)).DIRECTORY_SEPARATOR.basename($path);//$this->generatePath($src,$hashByName);
			
			if(is_file($src)) {
				$fileName=basename($src);
				return $this->_published[$path]=$this->getBaseUrl()."/$dir/$fileName";
			} elseif(is_dir($src)) {
				return $this->_published[$path] = $this->getBaseUrl()."/$dir";
			}
		}
		
		throw new CException(Yii::t('yii','The asset "{asset}" to be published does not exist.', array('{asset}'=>$path)));
	}
}

