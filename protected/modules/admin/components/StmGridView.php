<?php
Yii::import('zii.widgets.grid.CGridView');

class StmGridView extends CGridView {
	public $enableSorting = false;
	public $extraParams;
	public $itemsCssClass = 'datatables';
	public $beforeAjaxUpdate =  'js:function(id) {$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");}';
	public $afterAjaxUpdate =  'js:function(id) {$("div.loading-container.loading").remove();}';
	public $pager = array(
//		'class' => 'CListPager',
        'class' => 'admin_module.components.StmListPager',
	);

}
