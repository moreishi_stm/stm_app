<?php
/**
 * CListPager class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */


/**
 * CListPager displays a dropdown list that contains options leading to different pages of target.
 *
 * @author Christine Lee
 */
class StmListPager extends CListPager
{
	/**
	 * @var string the text shown before page buttons. Defaults to 'Go to page: '.
	 */
	public $header;
//	/**
//	 * @var string the text shown after page buttons.
//	 */
//	public $footer;
//	/**
//	 * @var string the text displayed as a prompt option in the dropdown list. Defaults to null, meaning no prompt.
//	 */
//	public $promptText;
//	/**
//	 * @var string the format string used to generate page selection text.
//	 * The sprintf function will be used to perform the formatting.
//	 */
	public $pageTextFormat;
	/**
	 * @var array HTML attributes for the enclosing 'div' tag.
	 */
	public $htmlOptions=array();

	/**
	 * Initializes the pager by setting some default property values.
	 */
	public function init()
	{
		if($this->header===null)
			$this->header=Yii::t('yii','Go to page: ');
		if(!isset($this->htmlOptions['id']))
			$this->htmlOptions['id']=$this->getId();
		if($this->promptText!==null)
			$this->htmlOptions['prompt']=$this->promptText;
		if(!isset($this->htmlOptions['onchange'])) {

            $getVars = $_GET;
            $allVars = CMap::mergeArray($getVars,$getVars);
            $serializedVars = CJSON::encode($allVars);

            $pageVar = $this->getPages()->pageVar;
            $stmListOnChange = <<<JS
                var url = $(this).val();
                var params = {};
                var urlVars;

                if(url.indexOf("?")>0) {
                    urlVars = url.substr(0).split("?");
                    urlVars = urlVars[1];
                } else {
                    urlVars = url;
                }

                urlVars = urlVars.substr(0).split("&");
                for (var i = 0; i < urlVars.length; ++i) {
                    var varPieces=urlVars[i].split("=");
                    if (varPieces.length != 2)
                            continue;

                    if(varPieces[0] == "$pageVar") {
                        params[varPieces[0]] = decodeURIComponent(varPieces[1].replace(/\+/g, " "));
                    }
                }

                var pageParam = new Array();
                if(params.$pageVar != 'undefined') {
                    pageParam["$pageVar"] = params.$pageVar;
                }
                if(this.value!="") {
                    $.fn.yiiGridView.update($(this).parent().parent().attr("id"), {
                        data: {"$pageVar" : params.$pageVar}
                    });
                }
JS;
            $this->htmlOptions['onchange']= $stmListOnChange;
        }
	}

	/**
	 * Executes the widget.
	 * This overrides the parent implementation by displaying the generated page buttons.
	 */
	public function run()
	{
		if(($pageCount=$this->getPageCount())<=1)
			return;
		$pages=array();
		for($i=0;$i<$pageCount;++$i) {
            $num = $i+1;
            $pageVar = array();
            $pageVar[$this->getPages()->pageVar] = $num;

            $getVars = $_GET;

            $params = CMap::mergeArray($getVars, $pageVar);
            if(isset($params['url']))
                unset($params['url']);

            $pages[Yii::app()->controller->createUrl('',$params)]=$this->generatePageText($i);
        }

        $selection = null;
        if(isset($_GET['ajax']) && $_GET['ajax']!='') {
            $selection = ($key = array_search($_GET[$this->getPages()->pageVar], $pages))? $key : null;
        }
		echo $this->header;
		echo CHtml::dropDownList($this->getId(),$selection,$pages,$this->htmlOptions);
		echo $this->footer;
	}

//	/**
//	 * Generates the list option for the specified page number.
//	 * You may override this method to customize the option display.
//	 * @param integer $page zero-based page number
//	 * @return string the list option for the page number
//	 */
//	protected function generatePageText($page)
//	{
//		if($this->pageTextFormat!==null)
//			return sprintf($this->pageTextFormat,$page+1);
//		else
//			return $page+1;
//	}
}