<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Base.php');

/**
 * Web
 *
 * @author Nicole Xu
 * @package StmLeads\Adapters
 */
class Web extends Base
{
    /**
     * Form ID
     *
     * Required to correctly process a form
     * @var int Form ID is the ID of the submitted form
     */
    protected $_formId;

    /**
     * Component Type Id
     *
     * Example: Buyer, Seller, etc.
     * @var string Component Type ID
     */
    protected $_componentTypeId;

    /**
     * Form Submission
     *
     * @var \FormSubmissions
     */
    protected $_formSubmission;

    /**
     * Scenario for Form
     * Example: Form #6 or 6_Part1
     * @var string scenario
     */
    public $formScenario;

    /**
     * Submission Step
     *
     * @var string Acceptable values: "Partial", "Full", "Full Plus"
     */
    public $submissionStep = 'Full';

    /**
     * Construct
     *
     * Instantiates the object
     */
    public function __construct($formID)
    {
        // Set form ID
        $this->_formId = $formID;

        // Setup data
        $this->_data = new \StmLeads\Data\Web();
    }

    /**
     * Set Form ID
     *
     * Setter for Form ID
     * @param string $id The ID to set
     */
    public function setFormId($id) {
        $this->_formId = $id;
    }

    /**
     * Get Form Submission ID
     *
     * Returns ID for the currently loaded form submission
     * @return int
     */
    public function getFormSubmissionId()
    {
        return $this->_formSubmission->id;
    }

    /**
     * Import Data
     *
     * Imports raw form submission data to the appropriate data object
     * @param array $data Form post data
     * @throws \Exception When form post data is not an array
     */
    public function importData(array $data = array())
    {
        // Check input
        if(!is_array($data)) {
            throw new \Exception('Data must be an array');
        }

        $form = \Forms::model()->findByPk($this->_formId);
        $this->_componentTypeId = $form->component_type_id;

        // Wipe existing data
        $this->_data = new \StmLeads\Data\Web();

        // @todo: account_id needs to be required - how to structure this?
        $this->data()->account_id = \Yii::app()->user->getAccountId();

        // @todo: primary_domain_name needs to be required - how to structure this?
        $this->data()->primary_domain_name = \Yii::app()->user->getPrimaryDomain()->name;

        //$this->data()->current_domain_name = str_replace('www.','',$_SERVER['SERVER_NAME']);

        // if buyer and have address, move that into the comments field.
        if($this->_componentTypeId == \ComponentTypes::BUYERS) {

            $addressComment = '';

            if($data[\FormFields::ADDRESS_ID]) {

                // format state
                $state = \AddressStates::findByString($data[\FormFields::STATE_ID]);

                $addressComment = 'I am interested in '.$data[\FormFields::ADDRESS_ID].', '.$data[\FormFields::CITY_ID].', '.$state->short_name.' '.$data[\FormFields::ZIP_ID].'.';

                unset($data[\FormFields::CITY_ID]);
                unset($data[\FormFields::STATE_ID]);
                unset($data[\FormFields::ZIP_ID]);
            }

            $data[\FormFields::COMMENTS_ID] = $addressComment . (($data[\FormFields::COMMENTS_ID]) ? ' '.$data[\FormFields::COMMENTS_ID] : '');
        }

        // set source from UTM data
        $utmData = \StmTrafficTracker::getUtmTrackingData();

        // if form has specific source ID set that source for processing
        if($this->data()->source_id) {

            if($source = \Sources::model()->findByPk($this->data()->source_id)) {
                $this->_source = $source;
            }
        // if form has specific source name set that source for processing
//        } elseif ($this->data()->source_description) { //@todo: not sure if I want description to automatically create a source - should use drop down
//                $this->_setSourceDataByName($this->data()->account_id, $this->data()->source_description);
        }
        // see if source exists, if not create one.
        elseif(!empty($utmData['campaign_source'])) {

            // Attempt to load up existing source
            $this->_source = $source = \Sources::model()->findByAttributes(array('name' => $utmData['campaign_source']));
            if($this->_source) {
                $trafficSource = $this->_source->id;
            }
            else {
                // Create new source if we didn't already have one
                $this->_source = new \Sources;
                $this->_source->account_id = $this->data()->account_id;
                $this->_source->name = $utmData['campaign_source'];

                $this->_source->save() ? $this->_source->id : \Sources::ID_WEBSITE;
            }
        }
        else {

            $this->_setSourceDataByName($this->data()->account_id, str_replace('www.','', $_SERVER['SERVER_NAME']));
//            $this->_setSourceDataById($this->data()->account_id, \Sources::ID_WEBSITE);
        }

        // if this is a showing request, flag that task type as it will affect processes down the line
        if($this->_formId == \Forms::FORM_SHOWING_REQUEST) {
            $this->_activityTaskTypeId = \TaskTypes::SHOWING_REQUEST;
        }

        foreach($data as $id => $value) {
            $this->data()->{\FormFields::getFieldNameById($id)} = $value;
        }

        // process full name, check that first_name & last_name does not exist
        if($this->data()['full_name'] && !$this->data()['first_name'] && !$this->data()['last_name']) {
            $fullNameParts = explode(' ', $this->data()['full_name'], 2);
            $this->data()->first_name = ucwords(strtolower(trim($fullNameParts[0])));
            $this->data()->last_name = ($fullNameParts[1]) ? ucwords(strtolower(trim($fullNameParts[1]))) : "-";
        }
    }

    /**
     * Validates using CModel
     * @return bool
     */
    public function validate()
    {
        // Stash data in form submission values model for validation
        if($this->formScenario) {
            $FormSubmissionValue = new \FormSubmissionValues($this->formScenario);
        }
        else {
            $FormSubmissionValue = new \FormSubmissionValues($this->_formId);
        }
        $FormSubmissionValue->data = $this->data()->getFormIdValueData();

        return $FormSubmissionValue->validate();
    }

    /**
     * Validate Web Form
     *
     * @return string
     */
    public function validateWebForm()
    {
        $scenario = ($this->formScenario)? $this->formScenario : $this->_formId;

        // Stash data in form submission values model for validation
        $FormSubmissionValue = new \FormSubmissionValues($scenario);
        $FormSubmissionValue->data = $this->data()->getFormIdValueData();

        return \CActiveForm::validate($FormSubmissionValue, null, false);
    }

    /**
     * Save Component Model
     *
     * Overrides parent methods to acquire ID automatically from given form
     * @return void
     */
    protected function _saveComponentModel($componentTypeId=null)
    {
        // Make sure we have previously set a form ID
        if(!$this->_formId) {
            throw new \Exception('No Form ID available!');
        }

        // Get reference to form
        $form = \Forms::model()->findByPk($this->_formId);
        if(!$form) {
            throw new \Exception('Unable to load form');
        }

        // Chain parent method
        $componentTypeId = ($componentTypeId) ? $componentTypeId : $form->component_type_id;
        parent::_saveComponentModel($componentTypeId);
    }

    /**
     * Save Form Submission Data
     *
     * Ugly Hack to call internal function because we need it to be external now
     * @throws \Exception When things go wrong
     */
    public function saveFormSubmissionData()
    {
        $this->_saveFormSubmissionData();
    }

    /**
     * Save Form Submission Data
     *
     * Saves data to the form submissions table, this is specific to web
     * @throws \Exception When unable to save
     */
    protected function _saveFormSubmissionData()
    {
        // Get google tracking info
        $googleTrackingData = \StmFormHelper::getGoogleTrackingData();

        // Get IP address and take in to consideration whether or not we are behind a load balancer
        $ipAddress = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? explode(', ', $_SERVER['HTTP_X_FORWARDED_FOR'])[0] : $_SERVER['REMOTE_ADDR'];

        // Create the instance of the form submission
        $formSubmissionValues = array();
        //if($this->data()['form_submission_id']) {
        if($this->data()->form_submission_id) {

            // Locate existing record and use IP address as a security measure
            $this->_formSubmission = \FormSubmissions::model()->findByAttributes(array(
                'id'            =>  $this->data()->form_submission_id,
                'ip_address'    =>  $ipAddress
            ));

            // Make sure this works
            if(!$this->_formSubmission) {
                throw new Exception('Error, no form submission exists for id and IP address combo, possible security issue!');
            }

            // Get existing records
            $formSubmissionValueRecords = \FormSubmissionValues::model()->findAllByAttributes(array(
                'form_submission_id'    =>  $this->data()->form_submission_id
            ));

            // Model data for easy lookup
            foreach($formSubmissionValueRecords as $formSubmissionValueRecord) {
                $formSubmissionValues[$formSubmissionValueRecord->form_field_id] = $formSubmissionValueRecord;
            }
        }
        else {
            // Create new
            $this->_formSubmission = new \FormSubmissions();
        }

        $this->_formSubmission->form_id         = $this->_formId;
        $this->_formSubmission->source_id       = $this->_source->id;
        $this->_formSubmission->url             = \Yii::app()->request->urlReferrer;
        $this->_formSubmission->domain          = str_replace('www.','',$_SERVER['SERVER_NAME']);
        $this->_formSubmission->campaign        = $googleTrackingData['campaign_name'];
        $this->_formSubmission->source          = $this->_source->name;
        $this->_formSubmission->source          = $googleTrackingData['campaign_source'];
        $this->_formSubmission->keywords        = $googleTrackingData['campaign_term'];
        $this->_formSubmission->medium          = $googleTrackingData['campaign_medium'];
//        $this->_formSubmission->component_id    = $this->getComponentId();
        $this->_formSubmission->type            = ($this->submissionStep == 'Partial' ? null : ($this->getIsResubmit() ? 'Resubmit' : 'New'));
        $this->_formSubmission->submission_step = $this->submissionStep;
        $this->_formSubmission->user_agent      = $_SERVER['HTTP_USER_AGENT'];
        $this->_formSubmission->referrer        = $_SERVER['HTTP_REFERER'];
        $this->_formSubmission->ip_address      = $ipAddress;

        // Attempt to save form submission
        if(!$this->_formSubmission->save()) {
            throw new \Exception('Unable to save form submission data');
        }

        // Store form submission values
        foreach ($this->data()->getFormIdValueData() as $formFieldId => $fieldValue) {

            // Skip this iteration if we don't have a value
            if (!$fieldValue) {
                continue;
            }

            // If we have an existing item, update it instead of creating new
            $FormSubmissionValue = array_key_exists($formFieldId, $formSubmissionValues) ? $formSubmissionValues[$formFieldId] : new \FormSubmissionValues();

            // Add the form submission value record
//            $FormSubmissionValue = new \FormSubmissionValues;
            $FormSubmissionValue->form_submission_id = $this->_formSubmission->id;
            $FormSubmissionValue->form_field_id      = $formFieldId;
            $FormSubmissionValue->value              = $fieldValue;
            if(!$FormSubmissionValue->save()) {
                \Yii::log(__FILE__ . '(' . __LINE__ . '): Form Submission Value did not save properly.' . print_r($FormSubmissionValue->getErrors(), true) . PHP_EOL . ' Submission Value: ' . print_r($FormSubmissionValue->attributes, true), \CLogger::LEVEL_ERROR);
            }

            // Add this
            $this->_formSubmission->formValueTuples[$formFieldId] = $fieldValue;
        }

        // Re-save
        $this->_formSubmission->save();
    }

    /**
     * Route Lead
     *
     * Handles lead route stuff
     * @throws \Exception When no form id is available
     */
    protected function _routeLead()
    {
        // Make sure we have previously set a form ID
        if(!$this->_formId) {
            throw new \Exception('No Form ID available!');
        }

        // Get reference to form
        $form = \Forms::model()->findByPk($this->_formId);
        if(!$form) {
            throw new \Exception('Unable to load form');
        }

        // Chain parent method
        parent::_routeLead($form->component_type_id);
    }

    /**
     * Is New Transaction
     *
     * Used to determine whether or not the transaction record has successfully saved
     * @return bool True when is new transaction
     */
    public function isNewTransaction()
    {
        return $this->_componentModel->isNewRecord;
    }


    /**
     * Save
     *
     * Encapsulates all the save functionality
     * @return int
     */
    public function save()
    {
        // Start by storing basic info
        $this->_saveBasicInfo();

        // Save form submission data
        $this->_saveFormSubmissionData();

        // If the form has a multi-componentId field which allows the prospect to identify themselves as buyer and seller, this handles that scenario
        if($this->data()->multi_component_type) {

            if($this->data()->multi_component_type && in_array($this->data()->multi_component_type, array(\ComponentTypes::CONTACTS, \ComponentTypes::SELLERS, \ComponentTypes::BUYERS, \ComponentTypes::RECRUITS))) {

                $this->_saveComponentModel($this->data()->multi_component_type);
            }
            elseif($this->data()->multi_component_type  == 'seller_and_buyer') {

                $this->_saveComponentModel(\ComponentTypes::SELLERS);
                $this->_saveComponentModel(\ComponentTypes::BUYERS);
            }
        }
        else {
            $this->_saveComponentModel();
        }

        // Update form submission record with component ID
        $this->_formSubmission->component_id = $this->getComponentId();
        $this->_formSubmission->save();

        // Save component model
        //$this->_saveComponentModel();

        // Handle lead routing
        $this->_routeLead();

        return 1; //@todo: should there be a flag for whether everything saved properly? This is used to login the user if everything went right.
    }
}