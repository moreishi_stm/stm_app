<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Add Plivo
\Yii::import('admin_module.components.StmPlivo.plivo', true);

// Include our lead data object
include_once(__DIR__ . '/../Data/Base.php');
include_once(__DIR__ . '/../Data/Web.php');

// Use this namespaces
use Admin\TableGateway\Client\ComponentTypes;
use \StmLeads\Data;

/**
 * Base
 *
 * @author Nicole Xu
 * @package StmLeads\Adapters
 */
abstract class Base
{
    /**
     * Data
     * Elements: See mapping in _saveBuyerSeller()
     * Basic Fields: first_name, last_name, email, phone, address, city, state, zip, comments
     * Seller Fields: bedrooms, baths, sq_feet, condition
     * Buyer Fields: price_min, bedrooms, baths, sq_feet, zip_codes, location
     */
    protected $_data;

    /**
     * Contact
     *
     * Reference to contact record
     * @var \Contacts
     */
    protected $_contact;

    /**
     * Is Resubmit
     *
     * Used to determine whether or not this data represents a resubmit of a component.
     * @var bool (nullable) True when resubmit, false otherwise
     */
    protected $_isResubmit;

    /**
     * Contact Type LU
     *
     * Reference to contact type lookup record
     * @var \ContactTypeLu
     */
    protected $_ContactTypeLu;

    /**
     * Address
     *
     * Reference to address record
     * @var \Addresses
     */
    protected $_address;

    /**
     * Email
     *
     * Reference to email record
     * @var \Emails
     */
    protected $_email;

    /**
     * Phones
     *
     * Reference to phone record
     * @var \Phones
     */
    protected $_phone;

    /**
     * Transaction
     *
     * Reference to transaction record
     * @var model of the component you are using
     */
    protected $_componentModel;

    /**
     * Source
     *
     * Reference to source record
     * @var \Sources
     */
    protected $_source;

    /**
     * Component Type ID
     *
     * @var string Assigned from ComponentTypes constants
     */
    protected $_componentTypeId;

    /**
     * Task Type ID
     *
     * @var string Assigned from TaskTypes constants
     */
    protected $_activityTaskTypeId;

    /**
     * Data
     *
     * Reference to underlying data object
     * @return \StmLeads\Data\Base
     */
    public function data()
    {
        return $this->_data;
    }

    /**
     * Save
     *
     * All adapters must come up with their own save functionality
     * @return mixed
     */
    abstract public function save();

    /**
     * getContact
     *
     * Retrieves Contact associated with this lead
     * @return mixed Contacts model
     */
    public function getContact()
    {
        return $this->_contact;
    }

    /**
     * Get Component Type ID
     *
     * @return int The component type ID
     */
    public function getComponentTypeId()
    {
        return $this->_componentTypeId;
    }

    /**
     * Get Component ID
     *
     * @return int The component ID
     */
    public function getComponentId()
    {
        return $this->_componentModel->id;
    }

    /**
     * Spam Check
     *
     * @return bool True when spam, false when not spam
     */
    public function isSpam()
    {
        // Keep track of the form submission values to check for duplicates
        $storedValues = array();

        // Signifies that the submission was infact spam
        $spamFlag = false;

        // Filter #1: check for a tags, href links
        $spamTerms = array('href=', 'http://', '<a>', '</a>');
        // scenario for photo_url ONLY - allow "http://"
        $photoUrlSpamTerms = array('href=', '<a>', '</a>');

        foreach ($this->data() as $key => $value) {

            $spamTermsToUse = (in_array($key, array('photo_url','home_url','lead_url'))) ? $photoUrlSpamTerms : $spamTerms;

            // Check for any terms that match our spamTerms patterns (case insensitive)
            foreach ($spamTermsToUse as $spamTerm) {
                if (stripos($value, $spamTerm) !== false) {
                    $spamFlag = true;
                    break 2;
                }
            }

            array_push($storedValues, $value);
        }

        // Filter #2: phone # not empty + has other characters aside from 0-9, -, ()
        if(!$spamFlag && !empty($this->data()->phone)) {
            $phone = str_replace(array('(',')','-','_',' '), '', $this->data()->phone);
            if(!is_numeric($phone)) {
                $spamFlag = true;
            }
        }

        // Filter #3: to and from email fields have no @ in them + comment not empty
        if(!$spamFlag && !empty($this->data()->email) && !empty($this->data()->to_email)
            && !empty($this->data()->comments)) {
            if(strpos($this->data()->email, '@')==false && strpos($this->data()->to_email, '@')==false) {
                $spamFlag = true;
            }
        }

        /**
         * Ensure there is more than 2 unique values for a form
         * Example of Spam
         * First Name: test
         * Last Name: test
         * E-mail: test
         * Phone: 1231231234
         * Number of Distinct Values: 2
         * Result: Spam
         */
        if (!$spamFlag) {

            $uniqueArrayResults = array_unique($storedValues);
            $numUniqueArrayValues = count($uniqueArrayResults);

            // Exclude all blanks from being marked as spam
            $spamFlag = ($numUniqueArrayValues == 1 && $numUniqueArrayValues[0]!="" /*&& $numUniqueArrayValues <= 2*/)? true: false;
        }

        // If we have a spam entry, process it
        if($spamFlag==true) {

            if(!empty($rawValuesRecordId)) {
                $model = \FormSubmissionRawData::model()->findByPk($rawValuesRecordId);
                $model->is_spam = 1;
                if(!$model->save()) {
                    \Yii::log(__FILE__.'('.__LINE__.'): Spam submission value did not save. Data: ' . print_r($this->data(), true).PHP_EOL.'Error Message: '.print_r($model->getErrors(), true), \CLogger::LEVEL_ERROR, 'spam');
                }
            }

            // Construct message body
            $message = 'Raw Value Record ID: '.$rawValuesRecordId.' was detected as spam.'.PHP_EOL.PHP_EOL;
            $message .= date('Y-m-d H:i:s') .'(Form:'.$_GET['formId'].')';

            // Add each piece of data
            foreach($this->data() as $key => $value) {
                $message .= PHP_EOL.$key.'} '.$value;
            }

            // Add referrer and IP address
            $message .= PHP_EOL.'Referrer: => ' . str_replace(array('www.','http://'),'',$_SERVER['HTTP_REFERER']);
            $message .= PHP_EOL.'IP: => ' . \Yii::app()->stmFunctions->getVisitorIp();

            // Send email
            @\StmZendMail::easyMail('SpamAlert@SeizetheMarket.com', 'Team@seizethemarket.com', 'Spam Alert', $message);

            // Log data
            \Yii::log(__FILE__.'('.__LINE__.'): Spam submission detected.' . print_r($this->data(), true), \CLogger::LEVEL_WARNING, 'spam');
        }

        // Return spam flag
        return $spamFlag;
    }

    /**
     * Send To CLee
     *
     * Email and text raw contents to CLee, call it after we check that this isn't spam
     * @return void
     */
    public function sendToCLee()
    {
        // If we are under debug mode, skip sending
        if (YII_DEBUG) {
            return;
        }

        if(empty($this->data()['first_name']) && empty($this->data()['last_name']) && empty($this->data()['email']) && empty($this->data()['phone'])) {
            return;
        }

        // Construct message body
        $message = date('Y-m-d H:i:s') .'(Form)';
        foreach($this->data() as $key => $value) {
            $message .= PHP_EOL . \FormFields::getFieldIdByName($key) . '} '.$value;
        }
        $referrer = str_replace(array('www.', 'http://'), '', $_SERVER['HTTP_REFERER']);
        $message .= PHP_EOL.'Referrer: => '.$referrer;
        $message .= PHP_EOL.'IP: => ' . \Yii::app()->stmFunctions->getVisitorIp();

        // Send email to CLee
        @\StmZendMail::easyMail('Leads@SeizetheMarket.com', 'Team@seizethemarket.com', 'Register Dialog Submission Values', $message); //clee

        // Send text notification to CLee
        try {
            \Yii::app()->plivo->sendMessage('9043433200', $message);
        }
        catch(\Exception $e) {
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Error sending Plivo text to CLee. Error Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL.
                'Message: '.$message, \CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Set Source Data
     *
     * @return mixed sourceId(int) if found, false if not
     */
    public function _setSourceData($accountId, $settingId, $sourceName)
    {
        if(!is_numeric($accountId)) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Account ID is not numeric.');
        }

        if(!is_numeric($settingId)) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Setting ID is not numeric.');
        }

        $this->data()->source_description = $sourceName;

        // sets the source based on client account settings if exists
        $sourceId = \SettingAccountValues::model()->getSettingsByAccountSettingId($this->data()->account_id, $settingId);

        if($sourceId) {
            $this->_source = \Sources::model()->findByPk($sourceId);
        }
        elseif(!($this->_source = \Sources::model()->findByAttributes(array('name' => $sourceName, 'account_id' => $accountId)))) {
            // if not found, see if a source with the same name exists, if not create one.
            $source = new \Sources();
            $source->setAttributes(array('account_id' => $accountId, 'name' => $sourceName));
            if($source->save()) {
                $this->_source = $source;
            }
            else {
                $this->_source = \Sources::model()->findByPk(\Sources::UNKNOWN);
                throw new \Exception(__CLASS__.' (:'.__LINE__.') Source did not exist and a new record could not be saved. Attributes: '.print_r($source->attributes, true).'Error Message:'.print_r($source->getErrors(), true));
            }
        }
    }

    public function _setSourceDataByName($accountId, $sourceName)
    {
        if(!is_numeric($accountId)) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Account ID is not numeric.');
        }

        $this->data()->source_description = $sourceName;

        // see if source exists by name
        if(!($this->_source = \Sources::model()->findByAttributes(array('name' => $sourceName, 'account_id' => $accountId)))) {

            // if not found, see if a source with the same name exists, if not create one.
            $source = new \Sources();
            $source->setAttributes(array('account_id' => $accountId, 'name' => $sourceName));
            if($source->save()) {
                $this->_source = $source;
            }
            else {
                throw new \Exception(__CLASS__.' (:'.__LINE__.') Source did not exist and a new record could not be saved. Attributes: '.print_r($source->attributes, true).PHP_EOL.'Error Message:'.print_r($source->getErrors(), true).PHP_EOL.'Submission Data: '.print_r($this->data(), true));
            }
        }
    }

    public function _setSourceDataById($accountId, $sourceId)
    {
        if(!is_numeric($accountId)) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Account ID is not numeric.');
        }

        if(!is_numeric($sourceId)) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Source ID is not numeric.');
        }

        if(!($this->_source = \Sources::model()->findByPk($sourceId))) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Source did not exist and a new record could not be set for lead processing.'.PHP_EOL.'Submission Data: '.print_r($this->data(), true));
        }
    }

    /**
     * Save
     *
     * Store contact record, phone record(s), and all other records related to a lead.
     * @return void
     */
    protected function _saveBasicInfo()
    {
        // sanity check to see if account ID has been set
        if(!$this->data()->account_id) {
            throw new \Exception('Account ID required. Should be set by now.'); //@todo: this should use error log so it emails us.
        }
        // Lookup email
        $email = \Emails::model()->byIgnoreAccountScope()->skipSoftDeleteCheck()->findByAttributes(array(
            'email' =>  strtolower(trim(str_replace(' ', '',$this->data()['email']))),
            'account_id' => $this->data()->account_id,
        ));

        // detects that this email was deleted and un-deletes it from the contact.
        if($email->is_deleted) {
            $email->is_deleted=0;
            $email->save();

            //@todo: add log note about un-deleting email based on re-submission.
            //...
        }

        if($this->data()->source_id) {
            if($source = \Sources::model()->findByPk($this->data()->source_id)) {
                $this->_source = $source;
            }
        }

        // Retrieve contact
        $this->_contact = $email ? $email->contact : new \Contacts();

        // Add in data if this is new
        if($this->_contact->isNewRecord) {

            // Set appropriate contact data
            $this->_contact->source_id = ($this->_source)? $this->_source->id : \Sources::UNKNOWN;  //@todo: need to have an default Unknown source per account_id via setting
            $this->_contact->account_id = $this->data()->account_id;
            $this->_contact->session_id = session_id();
            $this->_contact->first_name = $this->data()->first_name;
            $this->_contact->last_name = $this->data()->last_name;
            $this->_contact->contact_status_ma = \Contacts::STATUS_ACTIVE;
            if(!$this->_contact->save(false)) {
                \Yii::log(__CLASS__.' (:'.__LINE__.') Unable to save contact record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Contact Error: '.print_r($this->_contact->getErrors(), true), \CLogger::LEVEL_ERROR);
                throw new \Exception('Unable to save new contact record'); //@todo: this should use error log so it emails us.
            }

            // Specify the "Prospect" contact type
            $this->_ContactTypeLu = new \ContactTypeLu();
            $this->_ContactTypeLu->contact_id = $this->_contact->id;
            $this->_ContactTypeLu->contact_type_id = \ContactTypes::PROSPECT;
            if(!$this->_ContactTypeLu->save()) {
                \Yii::log(__CLASS__.' (:'.__LINE__.') Unable to save contact type lu record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Contact Type Lu Error: '.print_r($this->_ContactTypeLu->getErrors(), true), \CLogger::LEVEL_ERROR);
                throw new \Exception('Unable to save new contact type LU record'); //@todo: this should use error log so it emails us.
            }

            // Email
            if(!empty($this->data()['email'])) {
                $this->_email = new \Emails();
                $this->_email->contact_id = $this->_contact->id;
                $this->_email->email = $this->data()['email'];
                $this->_email->account_id = $this->data()->account_id;
                $this->_email->is_primary = 1;
                if(!$this->_email->save()) {
                    \Yii::log(__CLASS__.' (:'.__LINE__.') Unable to save email record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Email Error: '.print_r($this->_email->getErrors(), true), \CLogger::LEVEL_ERROR);
                    throw new \Exception(__CLASS__.' (:'.__LINE__.') Unable to save save email record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Email Error: '.print_r($this->_email->getErrors(), true));
                }
            }
        }
        else {
            // if they are not a new record, note that they logged in again to show as active prospect
            $this->_contact->last_login = new \CDbExpression('NOW()');
            if(!$this->_contact->save(false)) {
                \Yii::log(__CLASS__.' (:'.__LINE__.') Unable to save contact record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Contact Error: '.print_r($this->_contact->getErrors(), true), \CLogger::LEVEL_ERROR);
                throw new \Exception(__CLASS__.' (:'.__LINE__.') Unable to save resubmit contact record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Contact Error: '.print_r($this->_contact->getErrors(), true));
            }
        }

        // address data for seller
        if(($this->_componentTypeId==\ComponentTypes::SELLERS || in_array($this->data()['multi_component_type'], array('seller_and_buyer', \ComponentTypes::SELLERS))) && !empty($this->data()['address']) && !empty($this->data()['city']) && !empty($this->data()['state']) && !empty($this->data()['state'])) {

            // Criteria to pull existing address
            $criteria = new \CDbCriteria;
            $criteria->compare('address', substr($this->data()->address, 0, 7), true);
            $criteria->compare('zip', $this->data()->zip);
            $criteria->compare('account_id', $this->data()->account_id);
            $criteria->with = array('addressContactLus');
            $criteria->compare('addressContactLus.contact_id', $this->_contact->id);
            $criteria->order = 't.id ASC';

            //@todo: need to add criteria for the same contact??? or can just check after find for it?
            $existingAddress = \Addresses::model()->byIgnoreAccountScope()->findAll($criteria);

            // If we don't have an existing address, null value will trigger the creation of a new address
            if(empty($existingAddress)) {
                $existingAddress = null;                        //@todo: MAYBE put the code for making a new address here?!
            }

            // If we have one match, then, that's the one we want
            elseif(count($existingAddress) == 1) {
                $existingAddress = $existingAddress[0];         //@todo: Assign address model here?
            }

            // Multiple addresses are found, pick the first that is a seller transaction        //@todo: This logic can be specified a little bit more intricate
            elseif (count($existingAddress) > 1) {
                foreach($existingAddress as $i => $singleExistingAddress) {

                    $sellers = $singleExistingAddress->sellers;
                    if(!empty($sellers)) {
                        //@todo: check status of seller transaction for delete!!
                        $existingAddress = $existingAddress[$i];
                        break;
                    }
                }

                // Notifying team about this situation which should not technically occur
                \Yii::log(__CLASS__.': There are multiple address matches for this contact form submission. Form Data: ' . print_r($this->data(), true), \CLogger::LEVEL_ERROR);
            }

            if($existingAddress) {
                $address = $existingAddress;
                $this->_address = $existingAddress;
//                    $this->contact->isNewSubmitAddressId = false;                     //@todo: Possibly delete from contacts model if not being used
            }
            else {
//                    $this->contact->isNewSubmitAddressId = true;                      //@todo: Possibly delete from contacts model if not being used
                $address = new \Addresses();
                $address->address = $this->data()->address;
                $address->city = $this->data()->city;
                $address->zip = $this->data()->zip;
                if(!is_numeric($this->data()['state'])) {

                    $state = \AddressStates::findByString($this->data()['state']);
                    $address->state_id = $state->id;
                }
                else {
                    $address->state_id = $this->data()->state;
                }

                $address->contactId = $this->_contact->id;                          //@todo: Should use contact_id as property, which needs to be added and accounted for in the address model
                $address->account_id = $this->data()->account_id;

                // Attempt to save address
                if(!$address->save()) {
                    \Yii::log(__FILE__ . '(' . __LINE__ . '): Error: Address model did not save. Errors: ' . print_r($address->getErrors(), true), \CLogger::LEVEL_ERROR);
                } else {
                    $this->_address = $address;
                }
            }
        }

        // Phone
        if(!empty($this->data()['phone'])) {

            // Check for existing phone
            $this->_phone = \Phones::model()->byIgnoreAccountScope()->findByAttributes(array(
                'phone'         =>  $this->data()->phone,
                'contact_id'    =>  $this->_contact->id,
                'account_id'    =>  $this->data()->account_id,
            ));

            // If we don't have an existing phone, add a new one
            if(!$this->_phone) {
                $this->_phone = new \Phones();
                $this->_phone->account_id = $this->data()->account_id;
                $this->_phone->contact_id = $this->_contact->id;
                $this->_phone->phone = $this->data()->phone;
                if(!$this->_phone->save()) {
                    \Yii::log(__FILE__ . '(' . __LINE__ . '): Error: Phone model did not save. Errors: ' . print_r($this->_phone->getErrors(), true).' Attributes: '.print_r($this->_phone->attributes, true), \CLogger::LEVEL_ERROR);
                }
            }
        }
    }

    /**
     * Save Buyer / Seller
     *
     * Saves buyer / seller transaction and handle re-submit
     * @param int $componentTypeId The component type ID mapping
     * @param integer $sourceId The traffic source
     * @throws \InvalidArgumentException When bad things happen
     * @throws \Exception When invalid arguments have been passed in to the function call
     */
    protected function _saveBuyerSeller($componentTypeId, $sourceId)
    {
        \Yii::import('stm_app.modules.front.controllers.actions.forms.AbstractFormSubmitAction');

        // Mapping of component types to transaction types
        $map = array(
            \ComponentTypes::BUYERS     =>  \Transactions::BUYERS,
            \ComponentTypes::SELLERS    =>  \Transactions::SELLERS
        );

        // Check argument for component type id
        if(!array_key_exists($componentTypeId, $map)) {
            throw new \InvalidArgumentException('Invalid argument for component type id');
        }

        // Determine whether or not this is a re-submit
        $transaction = ($componentTypeId==\ComponentTypes::BUYERS) ? new \Buyers() : new \Sellers();
        $transaction->contact_id = $this->_contact->id;
        if($this->_address) {
            $transaction->address_id = $this->_address->id;
        }

        // Check if this is a resubmit
        if($transaction->getIsResubmit($this->data()->account_id)) {

            $existingTransaction = $transaction->getResubmitTransaction();
            // This is a resubmit
            $this->_isResubmit = true;

            if($existingTransaction->transaction_status_id == \TransactionStatus::CANCELLED_ID || $existingTransaction->transaction_status_id == \TransactionStatus::TRASH_ID) {
                $existingTransaction->transaction_status_id = \TransactionStatus::NURTURING_ID;
            }

            // The existing transaction becomes our current working transaction
            $this->_componentModel = $existingTransaction;
        }
        else {
            // This is not a resubmit
            $this->_isResubmit = false;

            // Create new transaction
            $this->_componentModel = new \Transactions();

            // Set transaction properties
            $this->_componentModel->account_id = $this->data()->account_id;
            $this->_componentModel->transactionType = $map[$componentTypeId];
            $this->_componentModel->component_type_id = $componentTypeId;
            $this->_componentModel->transaction_status_id = \TransactionStatus::NEW_LEAD_ID;
            $this->_componentModel->source_id = $sourceId;
            $this->_componentModel->contact_id = $this->_contact->id;

            // checks to see if an address was submitted
            if($this->_address) {
                $this->_componentModel->address_id = $this->_address->id;
            }
        }

        // Attempt to save
        try {
            if(!$this->_componentModel->save()) {
                \Yii::log(__FILE__ . '(' . __LINE__ . '): Transaction did not save properly.' . print_r($this->_componentModel->getErrors(), true), \CLogger::LEVEL_ERROR);
                throw new \Exception('Unable to save transaction!');
            }
        }
        catch(\Exception $e) {
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Saving transaction had error. Error Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL.
                'Source Data: '.print_r($this->_source->attributes, true).' Transaction Data: '.print_r($this->_componentModel->attributes, true).' Data: '.print_r($this->data(), true), \CLogger::LEVEL_ERROR);
            throw $e;
        }

        // If this is not a resubmit, add additional information
        if(!$this->_isResubmit) {

            $leadFields = array();
            if($this->_componentModel->component_type_id == \ComponentTypes::SELLERS) {
                // Lead fields map
                $leadFields = array(
                    'bedrooms'      =>  \TransactionFields::BEDROOMS_SELLER,
                    'baths'         =>  \TransactionFields::BATHS_SELLER,
                    'sq_feet'       =>  \TransactionFields::SQ_FEET_SELLER,
                    'condition'     =>  \TransactionFields::FEATURES_SELLER,
                );
            }
            elseif($this->_componentModel->component_type_id == \ComponentTypes::BUYERS) {
                // Lead fields map
                $leadFields = array(
                    'price_min'     =>  \TransactionFields::PRICE_MIN_BUYER,
                    'bedrooms'      =>  \TransactionFields::BEDROOMS_BUYER,
                    'baths'         =>  \TransactionFields::BATHS_BUYER,
                    'sq_feet'       =>  \TransactionFields::SQ_FEET_BUYER,
                    'zip_codes'     =>  \TransactionFields::ZIP_CODES_BUYER,
                    'location'      =>  \TransactionFields::LOCATION_BUYER,
                );
            }

            // Iterate through lead fields
            foreach($leadFields as $leadFieldId => $transactionFieldId) {

                // If the value isn't set, skip it
                if(!isset($this->data()->$leadFieldId) && empty($this->data()->$leadFieldId)) {
                    continue;
                }

                // check to see if it already exists
                $transactionFieldValue = \TransactionFieldValues::model()->findByAttributes(array('transaction_id'=>$this->_componentModel->id, 'transaction_field_id'=>$transactionFieldId));
                if(!$transactionFieldValue) {

                    // Create new transaction field value
                    $transactionFieldValue = new \TransactionFieldValues();
                    $transactionFieldValue->transaction_id = $this->_componentModel->id;
                    $transactionFieldValue->transaction_field_id = $transactionFieldId;
                    $transactionFieldValue->value = ($leadFieldId == 'condition') ? 'Condition: ' . $this->data()->$leadFieldId : $this->data()->$leadFieldId;

                    // Attempt to save transaction field value
                    if(!$transactionFieldValue->save()) {
                        \Yii::log(__FILE__ . '(' . __LINE__ . '): New Transaction Field Values did not save properly.' . print_r($transactionFieldValue->getErrors(), true), \CLogger::LEVEL_ERROR);
                        throw new \Exception('Unable to save transaction field value');
                    }
                }
                elseif($transactionFieldValue->value === '' || $transactionFieldValue->value === null) {

                    $transactionFieldValue->value = ($leadFieldId == 'condition') ? 'Condition: ' . $this->data()->$leadFieldId : $this->data()->$leadFieldId;
                    // Attempt to save transaction field value
                    if(!$transactionFieldValue->save()) {
                        \Yii::log(__FILE__ . '(' . __LINE__ . '): Transaction Field Values did not save properly.' . print_r($transactionFieldValue->getErrors(), true), \CLogger::LEVEL_ERROR);
                        throw new \Exception('Unable to save transaction field value');
                    }
                }
            }
        }
    }

    /**
     * Save Recruit
     *
     * For future use, to save a recruit.
     * @param integer $sourceId
     * @throws \Exception Always, because this function has not yet been implemented
     */
    protected function _saveRecruit($sourceId)
    {
        // contact exists, treat as resubmit. Log activity and send appropriate alerts.
        if($recruit = \Recruits::model()->byIgnoreAccountScope()->findByAttributes(array('contact_id'=>$this->_contact->id, 'account_id'=>$this->data()['account_id']))) {
            $this->_isResubmit = true;
        }
        // contact does not exist. Completely new submission, create all records. Log activity and send appropriate alerts.
        else {
            $recruit = new \Recruits();
            $recruit->contact_id = $this->_contact->id;
            $recruit->source_id = ($sourceId)? $sourceId : \Sources::UNKNOWN;   //@todo: need to have an default Unknown source per account_id via setting
            $recruit->status_id = \TransactionStatus::NEW_LEAD_ID;
            $recruit->application_status_ma = 0;
            $recruit->met_status_ma = 0;
            $recruit->typesCollection = null;
            $recruit->added_by = $this->_contact->id; //@todo: if recruit referral - make it Yii::app()->user->id??? How to do this?? Or make it a data field
            $recruit->updated_by = $this->_contact->id;

            // the following fields are from refer an agent
            if($this->data()->is_licensed) {
                $recruit->is_licensed = $this->data()->is_licensed;
            }
            if($this->data()->current_brokerage) {
                $recruit->current_brokerage = $this->data()->current_brokerage;
            }

            if(!$recruit->save()) {

                \Yii::log(__CLASS__ . '(' . __LINE__ . ') Lead Gen Error: Recruit did not save properly. Error: '.print_r($recruit->errors, true).'Recruit Data: '.print_r($recruit->attributes, true), \CLogger::LEVEL_ERROR);
            }
        }

        // if referred by an agent, add that to assignment
        if($this->data()->referrer_first_name && $this->data()->referrer_last_name && $this->data()->referrer_email) {

            // see if we can find this person by email, if not create that contact
            if($referrerEmail = \Emails::model()->findByAttributes(array('email' => $this->data()->referrer_email))) {
                $referrerContact= $referrerEmail->contact;
            }
            else {
                $referrerContact = new \Contacts;
                $referrerContact->first_name = $this->data()->referrer_first_name;
                $referrerContact->last_name = $this->data()->referrer_last_name;

//                $this->_contact->source_id = ($this->_source)? $this->_source->id : \Sources::UNKNOWN;  //@todo: need to have an default Unknown source per account_id via setting


                $referrerContact->account_id = $this->data()->account_id;
                $referrerContact->session_id = session_id();
                $referrerContact->contact_status_ma = \Contacts::STATUS_ACTIVE;
                if(!$referrerContact->save(false)) {
                    throw new \Exception(__CLASS__.' (:'.__LINE__.') Unable to save contact record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Contact Error: '.print_r($referrerContact->getErrors(), true));
                }

                // Email
                if(!empty($this->data()['referrer_email'])) {
                    $referrerEmail = new \Emails();
                    $referrerEmail->contact_id = $referrerContact->id;
                    $referrerEmail->email = $this->data()['referrer_email'];
                    $referrerEmail->account_id = $this->data()->account_id;
                    $referrerEmail->is_primary = 1;
                    if(!$referrerEmail->save()) {
                        throw new \Exception(__CLASS__.' (:'.__LINE__.') Unable to save save email record. Form Data: ' . print_r($this->data(), true).PHP_EOL.'Email Error: '.print_r($referrerEmail->getErrors(), true));
                    }
                }

                if(!empty($this->data()['referrer_phone'])) {

                    // If we don't have an existing phone, add a new one
                    $referrerPhone = new \Phones();
                    $referrerPhone->account_id = $this->data()->account_id;
                    $referrerPhone->contact_id = $referrerContact->id;
                    $referrerPhone->phone = $this->data()->referrer_phone;
                    if(!$referrerPhone->save()) {
                        \Yii::log(__FILE__ . '(' . __LINE__ . '): Error: Phone model did not save. Errors: ' . print_r($referrerPhone->getErrors(), true).' Attributes: '.print_r($referrerPhone->attributes, true), \CLogger::LEVEL_ERROR);
                    }
                }
            }

            // add the referrer as an assignment
            $referrerAssignment = new \Assignments();
            $referrerAssignment->assignment_type_id = \AssignmentTypes::REFERRER;
            $referrerAssignment->component_type_id = \ComponentTypes::RECRUITS;
            $referrerAssignment->component_id = $recruit->id;
            $referrerAssignment->assignee_contact_id = $referrerContact->id;
            if(!$referrerAssignment->save()) {
                \Yii::log(__FILE__ . '(' . __LINE__ . '): Error: Assignment model did not save for Recruit Referrer. Errors: ' . print_r($referrerAssignment->getErrors(), true).' Attributes: '.print_r($referrerAssignment->attributes, true).PHP_EOL . print_r($this->data(), true), \CLogger::LEVEL_ERROR);
            }
        }

        if($recruit->id && $this->data()['document_disc']) {

            // save on hard disc first on @todo: S3
            //@todo: Make Documents->saveFile() friendly to passing in file content

            // save disc document
            $document = new \Documents('emailLead');
            $document->setAttributes(array(
                    'visibility_ma'=> \Documents::VISIBILITY_COMPONENT,
                    'componentTypeId' => \ComponentTypes::RECRUITS,
                    'componentId' => $recruit->id,
                    'document_type_id' => \DocumentTypes::TYPE_DISC,
                    'file_name' => $this->data()['document_disc']['filename'],
                    'file_extension' => 'pdf',
                    'content' => $this->data()['document_disc']['content'],
                    'contentType' => 'application/pdf',
                ));
            if($document->save()) {
                $DocumentPermission = new \DocumentPermissions;
                $DocumentPermission->document_id = $document->id;
                $DocumentPermission->component_type_id = $document->componentTypeId;
                $DocumentPermission->component_id = $document->componentId;
                $DocumentPermission->permission_level_ma = \DocumentPermissions::PERMISSION_LEVEL_COMPONENT;

                if(!$DocumentPermission->save()) {
                    //@todo: Log Error
                    \Yii::log(__FILE__ . '(' . __LINE__ . '): Document Permission did not save properly.' . print_r($DocumentPermission->getErrors(), true), \CLogger::LEVEL_ERROR);
                }
            }
            else {
                \Yii::log(__FILE__ . '(' . __LINE__ . '): Document for Recruit did not save properly.' . print_r($document->getErrors(), true), \CLogger::LEVEL_ERROR);
            }
        }
        $this->_componentModel = $recruit;
    }

    /**
     * Save Contact
     *
     * For future use, to save a recruit.
     * @todo Finish this functionality
     * @param $trafficSource The traffic source
     * @throws \Exception Always, because this function has not yet been implemented
     */
    protected function _saveContact($trafficSource)
    {
        throw new \Exception('This method is not yet implemented');
    }

    /**
     * Save Activity Log
     *
     * Saves activity log data
     * @param int $componentTypeId The component type ID
     * @param int $componentId The component ID
     * @throws \Exception When invalid arguments are present, also when unable to save info
     */
    protected function _saveActivityLog($componentTypeId, $componentId)
    {
        // Make sure this value isn't empty
        if(empty($componentTypeId)) {
            \Yii::log(__CLASS__ . '(' . __LINE__ . ') Invalid value for component type id', \CLogger::LEVEL_ERROR);
            throw new \Exception('Invalid value for component type id');
        }

        // Make sure this value isn't empty
        if(empty($componentId)) {
            \Yii::log(__CLASS__ . '(' . __LINE__ . ') Invalid value for component id', \CLogger::LEVEL_ERROR);
            throw new \Exception('Invalid value for component id');
        }

        // Create a new activity log entry
        $activityLog = new \ActivityLog();
        $activityLog->activity_date = new \CDbExpression('NOW()');
        $activityLog->account_id = $this->data()['account_id'];
        $activityLog->component_id = $componentId;
        $activityLog->component_type_id = $componentTypeId;

        $activityLog->task_type_id = ($this->_activityTaskTypeId) ? $this->_activityTaskTypeId : (($this->_isResubmit) ? \TaskTypes::RESUBMIT : \TaskTypes::NEW_LEAD_SUBMIT);

        $activityLog->added = new \CDbExpression('NOW()');
        $activityLog->updated = new \CDbExpression('NOW()');
        $activityLog->added_by = ($this->data()['added_by']) ? $this->data()['added_by'] : $this->_contact->id;

        // Determine the state name, if we have an ID then we need to perform a lookup
        $state = (!empty($this->data()['state']) && is_numeric($this->data()->state)) ? \AddressStates::getNameById($this->data()->state, array('short_name' =>  true)) : $this->data()->state;

        $activityLog->note = ($this->_isResubmit) ? 'RE-SUBMIT:'.PHP_EOL : '';

        // Add contact details before all other details
        $activityLog->note .= !empty($this->data()['first_name']) || !empty($this->data()->last_name) ? 'Name: ' . $this->data()->first_name . ' ' . $this->data()->last_name . PHP_EOL : '';
        $activityLog->note .= !empty($this->data()['email']) ? 'Email: ' . $this->data()->email . PHP_EOL : '';
        $activityLog->note .= !empty($this->data()['phone']) ? 'Phone: ' . $this->data()->phone . PHP_EOL : '';
        $activityLog->note .= !empty($this->data()['address']) ? 'Address: ' . $this->data()->address . PHP_EOL : '';
        $activityLog->note .= !empty($this->data()['city']) ? $this->data()->city . ', ' . $state . ' ' . $this->data()->zip . PHP_EOL : '';

        if($this->data()['multi_component_type']) {
            $leadType = '';
            switch($this->data()['multi_component_type']) {
                case \ComponentTypes::SELLERS:
                    $leadType = 'Seller';
                    break;

                case \ComponentTypes::BUYERS:
                    $leadType = 'Buyer';
                    break;

                case 'seller_buyer':
                    $leadType = 'Both: Seller & Buyer';
                    break;
            }
            $activityLog->note .= 'Lead Type: ' . $leadType . PHP_EOL;

        }

        if($this->data()['source_id'] && $this->_source) {
            $activityLog->note .= 'Source ID: ' . $this->_source->name . PHP_EOL;
        }

        // Items we will skip that were processed earlier
        $skips = array(
            'first_name',
            'last_name',
            'email',
            'phone',
            'address',
            'city',
            'state',
            'zip',
            'account_id',
            'multi_component_type',
            'source_id',
            'photo_url',
        );

        // Process the leftover form submissions after the basic fields have been formatted into the log note
        foreach ($this->data() as $key => $fieldValue) {

            // Skip items we already accounted for in our special processing
            if(in_array($key, $skips) || $fieldValue == '' || $fieldValue== null) {
                continue;
            }

            // Add the data to the note
            $activityLog->note .= (empty($activityLog->note) || substr($activityLog->note, -1) == "\n")? '' : PHP_EOL;
            if(\FormFields::getFieldLabelByName($key)) {
                $activityLog->note .= \FormFields::getFieldLabelByName($key) . ': ' . $fieldValue;
            }
        }

        // replace any double line breaks

        // Attempt to save the entry
        if (!$activityLog->save(false)) {
            \Yii::log('Could not log resubmit info.' . print_r($activityLog->attributes, true) . PHP_EOL . 'Model Errors: ' . print_r($activityLog->errors, true), \CLogger::LEVEL_ERROR);
            throw new \Exception('Unable to save activity log information');
        }
    }

    /**
     * Save Component Model
     *
     * @param $componentTypeId
     * @throws \Exception When unable to save transaction record
     * @return void
     */
    protected function _saveComponentModel($componentTypeId)
    {
        if(!$this->_source) {
            //\Yii::log('Source is not set, defaulting to Unknown. Check to see why source is missing.' . print_r($this->data(), true), \CLogger::LEVEL_ERROR);
            $this->_source = \Sources::model()->byIgnoreAccountScope()->findByAttributes(array('id' => \Sources::UNKNOWN, 'account_id'=>$this->data()->account_id));  //@todo: need to have an default Unknown source per account_id via setting
        }

        // Store the component type id for later use
        $this->_componentTypeId = $componentTypeId;

        // Assign component type id to transaction (Buyer or Seller)
        switch($componentTypeId) {

            // Buyers and sellers have transactions
            case \ComponentTypes::BUYERS:
            case \ComponentTypes::SELLERS:

                // Determine if this a re-submit


                // Save things
                $this->_saveBuyerSeller($componentTypeId, $this->_source->id);
                $this->_saveActivityLog($componentTypeId, $this->_componentModel->id);
            break;

            // Recruits
            case \ComponentTypes::RECRUITS:
                $this->_saveRecruit($this->_source->id);
                $this->_saveActivityLog($componentTypeId, $this->_componentModel->id);
            break;

            // Contacts
            case \ComponentTypes::CONTACTS:
                $this->_saveContact($this->_source->id);
            break;

            // When we can't even handle this component type id
            default:
                throw new \Exception('Unknown component type ID passed in!');
            break;
        }
    }

    /**
     * Route Lead
     *
     * Routes the lead
     * @param $componentTypeId integer component type ID
     */
    protected function _routeLead($componentTypeId)
    {
        // Get all existing lead routes
        if(!$leadRoutes = \LeadRoutes::model()->byIgnoreAccountScope()->findAllByAttributes(array(
            'component_type_id' =>  $componentTypeId,
            'account_id' => $this->data()->account_id,
        ))) {
            \Yii::log(__CLASS__ . '(' . __LINE__ . ') URGENT: No Lead Routes! Adapter Data'.print_r($this->data(), true), \CLogger::LEVEL_ERROR);
            throw new \Exception('Error, no lead routes found!');
        }

        // Loop through lead routes for the current component type id (buyer, seller, etc.)
        foreach($leadRoutes as $leadRoute) {

            /** @var \LeadRoutes $leadRoute */

            // Build data for lead route option values
            $options = array();
            $leadRouteOptionValues = $leadRoute->leadRouteOptionValues;
            foreach($leadRouteOptionValues as $leadRouteOptionValue) {

                /** @var \LeadRouteOptions $leadRouteOption */

                $leadRouteOption = $leadRouteOptionValue->leadRouteOption;
                switch($leadRouteOption->id) {

                    // Items with one value
                    case \LeadRouteOptions::OPTION_PRICE_EQUAL:
                    case \LeadRouteOptions::OPTION_PRICE_LESS:
                    case \LeadRouteOptions::OPTION_PRICE_GREATER:

                        // Store the value
                        $options[$leadRouteOption->id] = $leadRouteOptionValue->value;
                    break;

                    // Items with multiple values
                    case \LeadRouteOptions::OPTION_LOCATION_CITY:
                    case \LeadRouteOptions::OPTION_LOCATION_ZIP:

                        // Ensure array exists, if not then declare it
                        if(!array_key_exists($leadRouteOption->id, $options)) {
                            $options[$leadRouteOption->id] = array();
                        }

                        // Store the value
                        $options[$leadRouteOption->id][] = $leadRouteOptionValue->value;
                    break;

                    // Handle the unknown
                    default:
                        throw new \Exception('Error, an unhandled type has been passed! We have no idea what to do!');
                    break;
                }

            }

            // Check if we're using equal price
            if(array_key_exists(\LeadRouteOptions::OPTION_PRICE_EQUAL, $options)) {
                if($this->data()->price != $options[\LeadRouteOptions::OPTION_PRICE_EQUAL]) {
                    continue;
                }
            }
            else {

                // If we used both greater than and less than
                if(array_key_exists(\LeadRouteOptions::OPTION_PRICE_GREATER, $options) && array_key_exists(\LeadRouteOptions::OPTION_PRICE_LESS, $options)) {
                    if(!($this->data()->price > $options[\LeadRouteOptions::OPTION_PRICE_GREATER] && $this->data()->price <  \LeadRouteOptions::OPTION_PRICE_LESS)) {
                        continue;
                    }
                }
                elseif(array_key_exists(\LeadRouteOptions::OPTION_PRICE_GREATER, $options)) {
                    if(!($this->data()->price > $options[\LeadRouteOptions::OPTION_PRICE_GREATER])) {
                        continue;
                    }
                }
                elseif(array_key_exists(\LeadRouteOptions::OPTION_PRICE_LESS, $options)) {
                    if(!($this->data()->price > $options[\LeadRouteOptions::OPTION_PRICE_LESS])) {
                        continue;
                    }
                }
            }

            // Handle city
            if(array_key_exists(\LeadRouteOptions::OPTION_LOCATION_CITY, $options)) {
                if(!in_array($this->data()->city, $options[\LeadRouteOptions::OPTION_LOCATION_CITY])) {
                    continue;
                }
            }

            // Handle zip code
            if(array_key_exists(\LeadRouteOptions::OPTION_LOCATION_ZIP, $options)) {
                if(!in_array($this->data()->zip, $options[\LeadRouteOptions::OPTION_LOCATION_ZIP])) {
                    continue;
                }
            }

            // Hold an array of contacts to notify
            $contactsToNotify = array();
            $contactsToNotifyEmails = array();
            $contactsToNotifyTxts = array();
            $contactIdsToNotify = array();

            // Retrieve contacts to notify
            $contactsToNotify = \LeadRoutes::getContactsToNotify($leadRoute);

            // sanitize contacts to notify
            if ($contactsToNotify === null || empty($contactsToNotify)) {
                $contactsToNotify = array();
            }

            // Add emails to notify
            foreach ($contactsToNotify as $contact) {

                // Add email address to the list of addresses
                $contactsToNotifyEmails[$contact->getPrimaryEmail()] = $contact->fullName;

                // assign agents to lead if new record
                if (!$this->getIsResubmit()) {
                    if ($componentTypeId == \ComponentTypes::SELLERS || $componentTypeId == \ComponentTypes::BUYERS) {
                        $assignment = new \Assignments();
                        $assignment->assignee_contact_id = $contact->id;
                        $assignment->component_type_id = $componentTypeId;
                        $assignment->component_id = $this->_componentModel->id;
                        $assignment->assignment_type_id = ($componentTypeId == \ComponentTypes::SELLERS) ? \AssignmentTypes::LISTING_AGENT : \AssignmentTypes::BUYER_AGENT;
                        $assignment->updated_by = $this->_componentModel->contact->id;
                        if (!$assignment->save()) {
                            \Yii::log(__CLASS__ . '(' . __LINE__ . ') Assignment did not save. Error message: ' . print_r($assignment->getErrors(), true) . ' Model Attributes: ' . print_r($assignment->attributes, true), \CLogger::LEVEL_ERROR);
                        } else {
                            $leadRouteAssigned = new \LeadRouteAssigned();
                            $leadRouteAssigned->setAttributes(array(
                                'lead_route_id' => $leadRoute->id,
                                'agent_contact_id' => $contact->id,
                                'component_type_id' => $componentTypeId,
                                'component_id' => $this->_componentModel->id,
                                'added' => new \CDbExpression('NOW()')
                            ));
                            if (!$leadRouteAssigned->save()) {
                                \Yii::log(__CLASS__ . '(' . __LINE__ . ') Lead Route Assigned did not save. Error message: ' . print_r($leadRouteAssigned->getErrors(), true) . ' Model Attributes: ' . print_r($leadRouteAssigned->attributes, true), \CLogger::LEVEL_ERROR);
                            }
                        }
                    }
                }
            }

            // also notify people who have been added to get CC'd on new leads.
            if ($leadRoute->cc_contact_ids) {

                // Attempt to decode JSON values
                $ccContactIds = $leadRoute->cc_contact_ids;
                if (!is_null($ccContactIds) && count($ccContactIds)) {

                    $existingAgents = array();

                    $activeAgentIds = array();
                    if ($activeAgents = \Contacts::model()->byActiveAdmins()->findAll()) {
                        foreach ($activeAgents as $activeAgent) {
                            $activeAgentIds[] = $activeAgent->id;
                        }
                    }

                    // Iterate over each contact
                    foreach ($ccContactIds as $ccContactId) {

                        // Load up the contact record and add it if it isn't already in the list
                        $contact = \Contacts::model()->byIgnoreAccountScope()->findByAttributes(array('id' => $ccContactId, 'account_id' => $this->data()->account_id));

                        // check to see if contact exists and if the primary email is in contacts to notify list
                        if ($contact && !array_key_exists($contact->getPrimaryEmail(), $contactsToNotifyEmails) && in_array($ccContactId, $activeAgentIds)) {
                            $contactsToNotifyEmails[$contact->getPrimaryEmail()] = $contact->fullName;
                        }
                    }
                }
            }

            // get any triggered lead routes and the contacts to notify and add it to the list (applicable for lender lead routes)
            $triggeredRoutes = $leadRoute->triggeredRoutes;
            if ($triggeredRoutes) {
                // go through each triggered route and retrieve the contacts to notify
                foreach ($triggeredRoutes as $triggeredRoute) {
                    // if there are any contacts to notify, check to see if they are already on the list and notify them
                    if ($triggeredContacts = \LeadRoutes::getContactsToNotify($triggeredRoute)) {

                        // add to contact/email and/or text list

                        // if the lead route is for a lender, process the lender assignment for a new lead.
                        if ($triggeredRoute->context_ma == \LeadRoutes::CONTEXT_LENDER_ID) {
                            foreach ($triggeredContacts as $triggeredContact) {
                                // assign agents to lead if new record for buyer and sellers
                                if (!$this->getIsResubmit()) {
                                    if ($componentTypeId == \ComponentTypes::SELLERS || $componentTypeId == \ComponentTypes::BUYERS) {
                                        $assignment = new \Assignments();
                                        $assignment->assignee_contact_id = $triggeredContact->id;
                                        $assignment->component_type_id = $componentTypeId;
                                        $assignment->component_id = $this->_componentModel->id;
                                        $assignment->assignment_type_id = \AssignmentTypes::LOAN_OFFICER;
                                        $assignment->updated_by = $this->_componentModel->contact->id;
                                        if (!$assignment->save()) {
                                            \Yii::log(__CLASS__ . '(' . __LINE__ . ') Assignment did not save loan officer. Error message: ' . print_r($assignment->getErrors(), true) . ' Model Attributes: ' . print_r($assignment->attributes, true), \CLogger::LEVEL_ERROR);
                                        }
                                    }
                                } // if resubmit, also notify the original agent or have some other login
                                else {
                                    // compare current assignments with original assignments, IF different do stuff
                                }
                            }
                        }

                        // get array of contactIds from contacts to Notify to cross check against lenders
                        foreach ($contactsToNotify as $contact) {
                            $contactIdsToNotify[$contact->id] = '';
                        }

                        // check to see if lender is not in contactsToNotify and add them if not in the group
                        foreach ($triggeredContacts as $triggeredContact) {
                            if (!isset($contactIdsToNotify[$triggeredContact->id])) {
                                array_push($contactsToNotify, $triggeredContact);
                            }

                            if ($triggeredContact && !array_key_exists($triggeredContact->getPrimaryEmail(), $contactsToNotifyEmails)) {
                                $contactsToNotifyEmails[$triggeredContact->getPrimaryEmail()] = $triggeredContact->fullName;
                            }
                        }
                    }
                }
            }

            // Get full name, primary phone, and primary email for contact
            $contactFullName = $this->_componentModel->contact->getFullName();
            $frmPrimaryPhone = \Yii::app()->format->formatPhone($this->_componentModel->contact->getPrimaryPhone());
            $contactPrimaryEmail = $this->_componentModel->contact->getPrimaryEmail();

            if ($componentTypeId == \ComponentTypes::SELLERS || $componentTypeId == \ComponentTypes::BUYERS) {
                // If it's a seller type of transaction, add the address to the text message
                $addressString = "";
                if ($this->_componentModel->address instanceof \Addresses && $this->_componentModel->getComponentTypeId() == \ComponentTypes::SELLERS) {
                    $address = $this->_componentModel->address;
                    $addressString = PHP_EOL . $address->address . PHP_EOL;
                    $addressString .= $address->city . ', ' . \AddressStates::getShortNameById($address->state_id) . ' ' . $address->zip;
                } elseif ($this->_componentModel->getComponentTypeId() == \ComponentTypes::SELLERS) {

                }
            }

//            $multiComponentString = '';
//            if($this->data()['multi_component_type']) {

//                switch($this->data()['multi_component_type']) {
//                    case \ComponentTypes::SELLERS:
//                        $multiComponentString = 'Type: Seller';
//                        break;
//
//                    case \ComponentTypes::BUYERS:
//                        $multiComponentString = 'Type: Buyer';
//                        break;

//                    case 'seller_and_buyer':
//                        $multiComponentString = 'Note: Selling & Buying, see both leads types.';
//                        break;
//                }
//            }


            //
            // Text Message
            //

            // Construct message body
            $newOrResubmit = ($this->_isResubmit) ? 'Re-submit' : 'New';

            $message = $newOrResubmit . ' ' . ucwords(substr(\ComponentTypes::getNameById($componentTypeId), 0, -1)) . ' Alert!' . PHP_EOL
                . $contactFullName . PHP_EOL
                . $frmPrimaryPhone . PHP_EOL
                . $contactPrimaryEmail
//                . (($multiComponentString) ? $multiComponentString : "")
                . $addressString;

            // Add data to message body
            foreach ($this->data() as $key => $value) {
                if (!in_array($key, array('first_name', 'last_name', 'email', 'phone', 'account_id', 'primary_domain_name', 'photo_url', 'home_url'))) {
                    $message .= (\FormFields::getFieldLabelByName($key)) ? PHP_EOL . \FormFields::getFieldLabelByName($key) . ': ' . $value : '';
                }
            }

            // Send the text message, if we aren't in debug mode and per lead route settings
            if (!YII_DEBUG && $leadRoute->send_sms) {

                // Send text notifications
                foreach ($contactsToNotify as $userToNotify) {
                    if ($userToNotify->settings->cell_number) {
                        \Yii::app()->plivo->sendMessage($userToNotify->settings->cell_number, $message);
                    }
                }
            }

            // CLee copied on everything
            if (!YII_DEBUG) {
                $referrerUrl = PHP_EOL . str_replace(array('www.', 'http://'), '', $_SERVER['HTTP_REFERER']);
                \Yii::app()->plivo->sendMessage('9043433200', '(CLee Copy)' . PHP_EOL . $message . $referrerUrl);
            }


            //
            // Email
            //

            // Construct message body
            $message = $newOrResubmit . ' WebLead Alert!' . PHP_EOL
                . 'Name: ' . $contactFullName . PHP_EOL
                . 'Phone: ' . $frmPrimaryPhone . PHP_EOL
                . 'Email: ' . $contactPrimaryEmail
                . $addressString;

            $formData = '<br />';

            // Add data
            foreach ($this->data() as $key => $value) {
                if (!in_array($key, array('first_name', 'last_name', 'email', 'phone', 'account_id', 'primary_domain_name', 'photo_url', 'home_url','multi_component_type'))) {

                    $value = ($key == 'state' && is_numeric($key)) ? \AddressStates::getShortNameById($value) : $value;

                    $formData .= (\FormFields::getFieldLabelByName($key)) ? \FormFields::getFieldLabelByName($key) . ': ' . $value . '<br />' : '';
                }
            }

            if ($this->data()['photo_url']) {

                $urlInfo = '<img src="' . $this->data()['photo_url'] . '" width="300px">';

                if ($this->data()['home_url']) {
                    $urlInfo = '<a href="' . $this->data()['home_url'] . '">' . $urlInfo . '</a><br />';
                    $urlInfo .= '<a href="' . $this->data()['home_url'] . '">Click here to View the Home</a>';
                }
                $formData = '<br /><br />' . $urlInfo . '<br />' . $formData;
            }

            // recruits does not have assignments
            if ($componentTypeId != \ComponentTypes::RECRUITS) {
                // Handle assignments if NOT recruiting module
                if ($assignments = $this->_componentModel->assignments) {
                    $assignmentInfo = '';
                    foreach ($assignments as $assignment) {
                        $assignmentInfo .= $assignment->assignmentType->display_name . ': ' . $assignment->contact->fullName . '<br />';
                    }
                    $assignmentInfo .= '<br/>';
                }
            }

            // Setup email
            $from = array('do-not-reply@seizethemarket.net' => 'Lead Alert (Seize the Market)');
            $bcc = 'christine@seizethemarket.com';

            $subject = 'Seize the Market - ' . $this->_componentModel->componentType->singularName . ' Lead Alert';

            if($this->_activityTaskTypeId == \TaskTypes::SHOWING_REQUEST) {
                $subject .= ' - *** SHOWING REQUEST *** '.$this->data()['address'];
            }

            if ($this->_componentModel->componentType->id == \ComponentTypes::SELLERS && $address->address) {
                $subject .= ' - ' . $this->data()['address'] . ' (Full Submit)';
            }

            $adminUrl = 'http://www.' . $this->data()['primary_domain_name'] . '/admin/' . $this->_componentModel->componentType->name . '/' . $this->_componentModel->id;
            $leadInfo = $message;
            $message = '<html><body style="font-size:14px;">';
            $message .= '<h2>' . $this->_componentModel->componentType->singularName . ' Lead</h2>';
            $leadInfo = str_replace(PHP_EOL, '<br />', $leadInfo);
            $message .= $leadInfo . $formData . '<br /><br />';
            $message .= $assignmentInfo;
            $message .= 'You have received a New Lead. Please contact them immediately.<br /><br />';
            $message .= 'Login to: <a href="' . $adminUrl . '">' . $adminUrl . '</a><br /><br />';
            $message .= 'Go and Seize the Market! =)<br /><br />';
            $domain = (substr_count($this->data()->primary_domain_name, '.') > 1) ? $this->data()->primary_domain_name : "www.".$this->data()->primary_domain_name ;
            $message .= '<img src="http://' . $domain . '/images/logo.png" style="width:200px;">';
            $message .= '</body></html>';

            // Send all emails
            $contactsToNotifyEmails = (YII_DEBUG) ? \StmMailMessage::DEBUG_EMAIL_ADDRESS : $contactsToNotifyEmails;

            // Attempt to send message
            try {
                \StmZendMail::easyMail($from, $contactsToNotifyEmails, $subject, $message, $bcc, $type = 'text/html', $awsTransport = true, $ignoreOnDebug = false);

            } catch (\Exception $e) {
                \Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT ERROR: Email alert for lead was not sent. An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" ' . PHP_EOL . 'Exception Message:' . $e->getMessage(), \CLogger::LEVEL_ERROR);
            }

            // apply the auto responder as long as it's not a resubmit
            if(!$this->getIsResubmit()) {

                // Get Autoresponder record
                $autoResponders = \LeadRouteAutoresponder::model()->findAllByAttributes(array(
                    'lead_route_id' => $leadRoute->id,
                ));

                // Do this for each contact
                //@todo: THIS SHOULD NOT GO OUT FOR EVERY CONTACT TO NOTIFY - there's FIRST RESPONDERS that will be DUPE
                foreach ($contactsToNotify as $userToNotify) {

                    // Handle each auto-responder for the current contact
                    foreach ($autoResponders as $autoResponder) {

                        switch ($autoResponder->leadRoute->component_type_id) {
                            //                    case \ComponentTypes::CONTACTS: //@todo: not coded for this yet
                            //                        break;

                            case \ComponentTypes::BUYERS:
                            case \ComponentTypes::SELLERS:
                                $isApplied = $autoResponder->apply($userToNotify, $this->_componentModel);
                                break;

                            case \ComponentTypes::RECRUITS:
                                $isApplied = $autoResponder->apply($userToNotify, $this->_componentModel);
                                break;

                            default:
                                \Yii::log(__CLASS__ . '(:' . __LINE__ . ') Could not apply responder with unknown component type: ' . $autoResponder->leadRoute->component_type_id, \CLogger::LEVEL_ERROR);
                                break;
                        }
                    }
                }
            }

            // Handle Plivo calls
            if ($leadRoute->make_calls && $this->data()->phone) {

                // Fire up Plivo
                /** @var \StmPlivoFunctions\RestAPI $plivo */
                $plivo = \Yii::app()->plivo->getPlivo();

                // List of numbers to call
                $phones = array();

                // see if lead route type is round robin and setting is for cascading round robin
                if ($leadRoute->type_ma = \LeadRoutes::ROUTE_TYPE_ROUND_ROBIN_ID && $leadRoute->is_cascading) {

                    $leadRouteRoundRobinContactIds = \Yii::app()->db->createCommand()
                        ->select('contact_id')
                        ->from('lead_round_robin_contact_lu')
                        ->where('lead_route_id=:leadRouteId', array(':leadRouteId' => $leadRoute->id))
                        ->order('sort_order ASC')
                        ->queryColumn();

                    // re order them so that the "next" person is first and the rest is after in the proper order
                    $nextRoundRobinAgent = $leadRoute->getNextRoundRobinAgent();

                    if (!$nextRoundRobinAgent) {
                        \Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: No Agent', \CLogger::LEVEL_ERROR);
                        // no agents available, do nothing and return
                        return;
                    }

                    if ($nextAgentPhoneObj = $nextRoundRobinAgent->getPrimaryPhoneObj()) {
                        $phones[] = array(
                            'id' => $nextAgentPhoneObj->id,
                            'phone' => '1' . $nextRoundRobinAgent->getPrimaryPhone()
                        );
                    }

                    // see where the next person is positioned in the round robin records
                    $key = array_search($nextRoundRobinAgent->id, $leadRouteRoundRobinContactIds);

                    // re-orders the round robin contactIds based on who is next in line. This is one complete round of the round robin.
                    if ($key > 0) {
                        $cascadeOrderedRoundRobinContactIds = array_slice($leadRouteRoundRobinContactIds, $key);
                        $part2 = array_slice($leadRouteRoundRobinContactIds, $key);

                        foreach ($part2 as $p2) {
                            $cascadeOrderedRoundRobinContactIds[] = $p2;
                        }
                    } else {
                        $cascadeOrderedRoundRobinContactIds = $leadRouteRoundRobinContactIds;
                    }
                } // If we have a hunt group, then we want to use the phone numbers in that, if we don't, we want to use the lead route numbers
                elseif ($leadRoute->call_hunt_group_id) {

                    // Retrieve call hunt group
                    $callHuntGroup = $leadRoute->callHuntGroup;

                    if($callHuntGroupPhones = $callHuntGroup->callHuntGroupPhones) {

                        // Iterate over each, adding them to the list of people to call
                        foreach ($callHuntGroupPhones as $callHuntGroupPhone) {
                            if ($callHuntGroupPhone->status_ma) {
                                $phones[] = array(
                                    'id' => $callHuntGroupPhone->id,
                                    'phone' => '1' . $callHuntGroupPhone->phone
                                );
                            }
                        }
                    }
                } // not hunt group means use lead route, aka contacts to notify
                else {

                    // Iterate contacts to notify
                    foreach ($contactsToNotify as $contactToNotify) {

                        // Retrieve primary phone record
                        /** @var \Phones $phone */
                        $phone = $contactToNotify->getPrimaryPhoneObj();

                        // If for some reason we don't have a phone number on file for this agent, skip them and continue
                        if (!$phone) {
                            //@todo: Log this someplace perhaps?
                            continue;
                        }

                        // Add the number to the list of people to call
                        $phones[] = array(
                            'id' => $phone->id,
                            'phone' => '1' . $phone->phone
                        );
                    }
                }

                // Get a list of numbers to call
                $numbersToCall = array();
                foreach ($phones as $phone) {
                    $numbersToCall[] = $phone['phone'];
                }

                if (empty($numbersToCall)) {
                    \Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: No Nums to Call. Phones Data: ' . print_r($phones, true) . PHP_EOL . 'Lead Data array: ' . print_r($this->data(), true) . PHP_EOL . 'Lead Route Data: ' . print_r($leadRoute->attributes, true) . PHP_EOL . 'Contacts to Notify Data: ' . print_r($contactsToNotify, true), \CLogger::LEVEL_ERROR);
                    return;
                }

                // go for primary domain first, if it's empty default to the pulling from client/account from db connection
                if (strlen($this->data()['primary_domain_name']) >= 8) {
                    $sitePlivoDomain = 'http://';
                    $sitePlivoDomain .= (\Domains::isSubDomain($this->data()['primary_domain_name'])) ? '' : 'www.';
                    $sitePlivoDomain .= $this->data()['primary_domain_name'];
                } // pull domain from client & account info
                else {
                    //                $sitePlivoDomain = (strlen(\StmFunctions::getSiteUrl()) > 15) ? \StmFunctions::getSiteUrl() : 'http://www.'.$this->data()['primary_domain_name'];
                }

                if (empty($sitePlivoDomain)) {
                    \Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI Primary domain missing for making plivo call. Ending process' . PHP_EOL . 'Data array: ' . print_r($this->data(), true), \CLogger::LEVEL_ERROR);
                    return;
                }

                if (empty($sitePlivoDomain) || $sitePlivoDomain == 'http://www.') {
                    \Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Site Plivo Domain is empty! ' . $sitePlivoDomain . PHP_EOL . 'Data array: ' . print_r($this->data(), true), \CLogger::LEVEL_ERROR);
                }

                // Create a new lead call session record
                $leadCallSession = new \LeadCallSessions();
                $leadCallSession->lead_route_id = $leadRoute->id;
                $leadCallSession->component_id = $this->getComponentId();
                $leadCallSession->component_type_id = $this->getComponentTypeId();
                $leadCallSession->source_phone = $this->data()->phone;
                $leadCallSession->added = new \CDbExpression('NOW()');
                if ($leadRoute->is_cascading && $cascadeOrderedRoundRobinContactIds) {
                    $leadCallSession->cascading_data = \CJSON::encode($cascadeOrderedRoundRobinContactIds);
                }

                // Attempt to save lead call session
                if (!$leadCallSession->save()) {
                    \Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Unable to save lead call session! Calls were not initiated.' . PHP_EOL . 'Numbers to Call: ' . print_r($numbersToCall, true) . PHP_EOL . 'Error Data: ' . print_r($leadCallSession->getErrors(), true), \CLogger::LEVEL_ERROR);
                    return;
                }

                // Start call
                $response = $plivo->make_call(array(
                    'from' => '19048007633', // this # is specifically for Lead Connect Calls
                    'to' => $leadRoute->is_cascading ? $numbersToCall[0] : implode('<', $numbersToCall),
                    'answer_url' => $sitePlivoDomain . '/plivoleadroute/answer', //$this->data()['primary_domain_name']
                    'hangup_url' => $sitePlivoDomain . '/plivoleadroute/hangup',
                    'fallback_url' => $sitePlivoDomain . '/plivoleadroute/fallback',
                ));

                // Get request UUIDs
                $requestUuids = is_array($response['response']['request_uuid']) ? $response['response']['request_uuid'] : array($response['response']['request_uuid']);

                // error in plivo call response
                if ($response['status'] < 200 || $response['request_uuid'] > 299) {

                    \Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Error: Lead Route Call response. Please investigate ASAP!' . PHP_EOL . 'Numbers to Call: ' . print_r($numbersToCall, true) . PHP_EOL . 'Request UUIDs: ' . print_r($requestUuids, true) . PHP_EOL . 'Response Data: ' . print_r($response, true), \CLogger::LEVEL_ERROR);
                }

                // Perform a sanity check
                if (count($requestUuids) == 0 || (count($requestUuids) != count($phones) && !$leadRoute->is_cascading)) {

                    \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Error, some how the number of phones to call does not equal the number of request UUIDs!' . PHP_EOL . 'Numbers to Call: ' . print_r($numbersToCall, true) . PHP_EOL . 'Request UUIDs: ' . print_r($requestUuids, true) . PHP_EOL . 'Response Data: ' . print_r($response, true), \CLogger::LEVEL_ERROR);

                    // do not go any further
                    return;
                }

                // Add lead call session phone records
                for ($i = 0; $i < count($phones); $i++) {

                    //@todo: FYI: Request/response. See what we can log. Need To phone #. See post data.
                    //                \Yii::log(__CLASS__ . ' (' . __LINE__ . ') FYI: Request/response. See what we can log. Need To phone #. See post data.', \CLogger::LEVEL_ERROR);

                    // Create a new lead call session phone record
                    $leadCallSessionPhone = new \LeadCallSessionPhones();
                    $leadCallSessionPhone->lead_call_session_id = $leadCallSession->id;
                    $leadCallSessionPhone->phone_number = $phones[$i]['phone'];

                    // If we are cascading, then only store this data for the very first item
                    if ($leadRoute->is_cascading && $i == 0 || !$leadRoute->is_cascading) {
                        $leadCallSessionPhone->request_uuid = $requestUuids[$i];
                        $leadCallSessionPhone->status = 'Ringing';
                    }

                    $leadCallSessionPhone->order = $i;
                    $leadCallSessionPhone->added = new \CDbExpression('NOW()');

                    // Determine what piece of data we're storing
                    if ($leadRoute->call_hunt_group_id && !$leadRoute->is_cascading) {
                        $leadCallSessionPhone->call_hunt_group_phone_id = ($phones[$i]['id']) ? $phones[$i]['id'] : null;
                    } else {
                        $leadCallSessionPhone->phone_id = $phones[$i]['id'];
                    }

                    // Attempt to save
                    if (!$leadCallSessionPhone->save()) {
                        throw new \Exception('Unable to save lead call session phone record!');
                    }
                }
            }
        }
    }

    /**
     * Get isResubmit
     * @return boolean
     */
    public function getIsResubmit() {
        return $this->_isResubmit;
    }
}