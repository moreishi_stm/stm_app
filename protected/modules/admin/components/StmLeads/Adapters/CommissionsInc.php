<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/CommissionsInc.php');

/**
 * Commissions Inc
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class CommissionsInc extends Email
{
    protected $_leadTypeName = 'Commissions Inc';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $message \StmZendMailMessageImap
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        if($message->getHeaders()['content-transfer-encoding'] !== 'base64') {
            return false;
        }

        // strips out all tags and replaces html breaks with \n
        $content = strip_tags(str_replace(array('<br />','<br>','<br/>'),PHP_EOL, base64_decode($message->getContent())));

        // Grab name and parse into first and last name
        preg_match('/Name:\ (.*)/i', $content, $matches);
        $name = explode(' ',$matches[1], 2);
        if(count($name) == 2) {
            $this->data()->first_name = ucwords(strtolower(trim($name[0])));
            $this->data()->last_name = ucwords(strtolower(trim($name[1])));
        }

        // Grab email address
        preg_match('/Email:\ (.*)/i', $content, $matches);
        $this->data()->email = strtolower(trim($matches[1]));

        // Grab phone number
        preg_match('/Phone:\ (.*)/i', $content, $matches);
        if($phone = preg_replace('/[^0-9]/i', '', $matches[1])) {
            $this->data()->phone = $phone;
        }

        // Grab price
        preg_match('/Price\ of\ property\ viewed:(.*)/i', $content, $matches);
        if($matches[1]) {
            $this->data()->price_min = preg_replace('/[^0-9]/i', '', $matches[1]); //@todo: need to tweak field entry and such... trace it down to Base
        }

        // Grab city
        preg_match('/City\ of\ property\ viewed:(.*)/i', $content, $matches);
        if($matches[1]) {
            $this->data()->area = trim($matches[1]);
        }

        // Grab source
        preg_match('/Source:\ (.*)/i', $content, $matches);
        $this->data()->source_description = ucwords(strtolower(trim($matches[1]))).' / Commissions Inc'; //@todo: do some more to make it match to a source

        switch($message->subject) {
            case 'Seller Lead Alert':
                $this->_componentTypeId = \ComponentTypes::SELLERS;
                break;

            case 'Buyer Lead Alert':
            default:
            $this->_componentTypeId = \ComponentTypes::BUYERS;
                break;
        }

        if(!($this->_source = \Sources::model()->findByAttributes(array('name'=>'Commissions Inc')))) {
            $this->_source = \Sources::model()->findByPk(\Sources::UNKNOWN); //@todo: need to have an default Unknown source per account_id via setting
        }

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        return base64_decode($message->getContent());
    }
}