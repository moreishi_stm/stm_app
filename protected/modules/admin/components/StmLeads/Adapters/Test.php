<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Base.php');
include_once(__DIR__ . '/../Data/Test.php');

/**
 * Test
 *
 * @author Nicole
 * @package StmLeads\Adapters
 */
class Test extends Base
{
    /**
     * Construct
     *
     * @param $accountId
     */
    function __construct($accountId)
    {
        // Set the account ID
        if(!is_numeric($accountId)) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Account ID is not numeric.');
        }

        // New data object
        $this->_data = new \StmLeads\Data\Test();

        // Store account ID
        $this->data()->account_id = $accountId;
    }

    /**
     * Import Data
     *
     * @param array $data
     */
    public function importData(array $data = array())
    {
        // Set data
        $this->data()->first_name = 'Test First';
        $this->data()->last_name = 'Last Name';

        $this->data()->email = 'user@example.com';

        $this->data()->address = '1 some street';
        $this->data()->city = 'Some City';
        $this->data()->state = 'Some State';
        $this->data()->zip = '12345';

        $this->data()->phone = '1234567890';

        $this->data()->comments = 'Test submission';

        $this->data()->bedrooms = 3;
        $this->data()->bathroome = 5;
        $this->data()->sq_feet = 1000;
        $this->data()->half_baths = 1;
    }

    /**
     * Save
     *
     * Performs save operation
     * @return mixed
     */
    public function save()
    {
        // validates basic contact fields in data()
        if(!$this->_validateContactInfo()) {
            return false;
        }

        // Start by storing basic info
        $this->_saveBasicInfo();

        // Save component model, default to buyer if not available
        $this->_componentTypeId = $this->_componentTypeId ? $this->_componentTypeId : \ComponentTypes::BUYERS;
        $this->_saveComponentModel($this->_componentTypeId);

        // Handle lead routing
        $this->_routeLead($this->_componentTypeId);

        return true;
    }

    /**
     * Validate Contact Info
     *
     * Validates Basic contact info in $this->data()
     * @return bool
     */
    protected function _validateContactInfo()
    {
        $validated = true;

        if(!$this->data()->first_name) {
            $validated = false;
        }
        if(!$this->data()->last_name) {
            $validated = false;
        }
        if(!$this->data()->email) {
            $validated = false;
        }

        if(strlen($this->data()->phone) > 10) {
            $this->data()->phone = substr($this->data()->phone, 0, 10);
        }

        return $validated;
    }
}