<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/LCM.php');

/**
 * LCM
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class LCM extends Email
{
    protected $_leadTypeName = 'LCM';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $content Email message contents
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        // if the subject line does not contain register, it is not a valid email to parse.
        if(strpos($message->subject, 'New LCM-Web Lead') === false) {
            return false;
        }

        $content = $message->getMessageBodyText();

        // Grab name and parse into first and last name
        preg_match('/(.*) (.*)\\r\\n'
            .'(.*)\\r\\n'
            .'(.*)\\r\\n'
            .'Source: (.*)\\r\\n/ismU', $content, $matches);

        if(empty($matches)) {
            return false;
        }


        $this->data()->first_name = ucwords(strtolower(trim($matches[1])));
        $this->data()->last_name = ucwords(strtolower(trim($matches[2])));

        // Grab email address
        $this->data()->email = strtolower(trim($matches[3]));

        // Grab phone number
        $this->data()->phone = preg_replace('/[^0-9]/i', '', $matches[4]);

        // Grab type of Buyer of Seller //@todo: NOTE assuming all LCM leads are buyer leads
        $this->_componentTypeId = \ComponentTypes::BUYERS;

        if(!($this->_source = \Sources::model()->findByAttributes(array('name'=>'LCM')))) {
            $this->_source = \Sources::model()->findByPk(\Sources::UNKNOWN); //@todo: need to have an default Unknown source per account_id via setting
        }

        $this->data()->source_description = 'LCM';

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        return nl2br($message->getMessageBodyText());
    }
}