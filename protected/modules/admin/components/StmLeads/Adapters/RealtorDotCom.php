<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/RealtorDotCom.php');

/**
 * RealtorDotCom
 *
 * @author Nicole Xu
 * @package StmLeads\Adapters
 */
class RealtorDotCom extends Email
{
    protected $_leadTypeName = 'Realtor.com';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $message \StmZendMailMessageImap
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        if($message->getHeaders()['content-transfer-encoding'] == 'base64') {
            $content = strip_tags(str_replace(array('<br />','<br>','<br/>'),PHP_EOL, base64_decode($message->getContent())));
        }
        else {
            $content = $message->getMessageBodyText();
        }

        // Grab first name
        preg_match('/First Name:\ (.*)/i', $content, $matches);
        $this->data()->first_name = ucwords(strtolower(trim($matches[1])));

        // Grab last name
        preg_match('/Last Name:\ (.*)/i', $content, $matches);
        $lastName = trim($matches[1]);
        $this->data()->last_name = (!empty($lastName)) ? ucwords(strtolower($lastName)) : '-';

        // Grab email address
        preg_match('/Email\ Address:\ (.*)/i', $content, $matches);
        $this->data()->email = trim($matches[1]);

        // Grab phone number
        preg_match('/Phone\ Number:\ (.*)/i', $content, $matches);
        $this->data()->phone = preg_replace('/[^0-9]/i', '', $matches[1]);

        // Grab comments
        preg_match('/Comment:\\r\\n(.*)\\n/ismU', $content, $matches);
        $this->data()->comments = trim($matches[1]);

        // Grab address & MLS ID
        preg_match('/Property\ Address\:(.*)\\n(.*)\\n(.*)\\nMLSID\ \#(.*)\\n/ismU', $content, $matches);
        // MLS ID
        if(!empty($matches[4])) {
            $this->data()->comments .= ' - MLS ID: '.trim($matches[4]);
        }

        // this will get entered on the contact level... needs to be a note in the log ONLY for buyers
        // Get address parts //@todo: make sure it's a seller ... or how to handle as buyer
        //        $this->data()->address = trim($matches[2]);
        // handle city, st zip
        preg_match('/(.*),\ ([a-z]{2})\ ([0-9]{5})/i', trim($matches[3]), $matches);
        //        if($matches) {
        //            $this->data()->city = trim($matches[1]);
        //            $this->data()->state = trim($matches[2]);
        //            $this->data()->zip = trim($matches[3]);
        //        }

        // This needs to be set
        $this->data()->primary_domain_name = \Domains::model()->findByAttributes(array('is_primary'=>1))->name;

        // if not component type specified, default to buyer
        if(!$this->_componentTypeId) {
            $this->_componentTypeId = \ComponentTypes::BUYERS;
        }

        // Set source data
        $this->_setSourceData($this->data()->account_id, \Settings::REALTORDOTCOM_SOURCE_ID, $sourceDescription = 'Realtor.com');

        return true;
    }
    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        if($message->getHeaders()['content-transfer-encoding'] == 'base64') {
            $content = base64_decode($message->getContent());
            preg_match('/\<body\>(.*)\<\/body\>/ismU', $content, $matches);
            $content = trim($matches[1]);
        }
        else {
            $content = trim(nl2br($message->getMessageBodyText()));
        }
        return $content;
    }
}