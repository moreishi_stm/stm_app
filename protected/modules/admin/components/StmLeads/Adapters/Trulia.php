<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/Trulia.php');

/**
 * Trulia
 *
 * @author Nicole Xu / Christine Lee
 * @package StmLeads\Adapters
 */
class Trulia extends Email
{
    protected $_leadTypeName = 'Trulia';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $message \StmZendMailMessageImap
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        $content = $message->getMessageBodyText();
        //check to see if body has some verbiage that make it a non-lead verbiage
        if(preg_match('/We have automatically linked (.*) to your Trulia account/ismU', $content, $matches) || (strpos($message->subject,'Please confirm your email address') !== false)) {
            return false;
        }

        // Name
        preg_match('/From:\ (.*)/i', $content, $matches);

        $fullName = explode(' ', trim($matches[1]));
        if(count($fullName) > 1) {
            $this->data()->first_name = ucwords(strtolower(trim($fullName[0])));
            $this->data()->last_name = ucwords(strtolower(trim($fullName[1])));
        }
        else {
            $this->data()->first_name = ucwords(strtolower(trim($matches[1])));
            $this->data()->last_name = '-';
        }

        // Email
        preg_match('/Email:\ (.*)/i', $content, $matches);
        if(strpos($matches[1], ':') !== false) {
            $emailParts = explode(':', $matches[1], 2);
            $this->data()->email = trim($emailParts[0]);
        }
        else {
            $this->data()->email = trim($matches[1]);
        }

        // Phone
        preg_match('/Phone:\ (.*)/i', $content, $matches);
        $this->data()->phone = preg_replace('/[^0-9]/i', '', $matches[1]);

        switch(1) {
            case(strpos($message->subject, 'You have a new seller lead!') !== false):
                // seller lead
                $this->_importSellerLead($content);
                break;

            case(strpos($message->subject, 'Contact request:') !== false):
                $this->_importContactRequestLead($content);
                break;

            case(strpos($message->subject, 'New Lead from Trulia:') !== false):
                $this->_importNewLeadfromTrulia($content);
                break;

            case(strpos($message->subject, 'New message for') !== false):
                $this->_importNewMessageLead($content);
                break;

            default:
                //@todo: error message goes here notifying of new type of lead format
                break;
        }

        if(!$this->_componentTypeId) {
            $this->_componentTypeId = \ComponentTypes::BUYERS;
        }

        // sets source model and description
         $this->_setSourceData($this->data()->account_id, \Settings::TRULIA_SOURCE_ID, $sourceDescription = 'Trulia');

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        return nl2br($message->getMessageBodyText());
    }

    protected function _importSellerLead($content)
    {
        $this->_componentTypeId = \ComponentTypes::SELLERS;

        // Get planning to sell
        preg_match('/Planning to Sell: (.*)\n/ismU', $content, $matches);
        if($matches[1]) {
            $this->data()->comments = 'Planning to Sell: '.trim($matches[1]);
        }

        // Get address parts
        // block of info that has address br, bath, price
        preg_match('/www.trulia.com\/inbox\n\n(.*)\n(.*), (.*)\n(.*)\n(.*)\(Trulia Estimated Value\)/ismU', $content, $matches);
        $this->data()->comments .= PHP_EOL.'Trulia Estimate: '.trim($matches[5]);

        // extract address
        $this->data()->address = $matches[1];
        $this->data()->city = $matches[2];
        $this->data()->state = $matches[3];

        // using google to get zip cuz the email doesn't provide one. This is important if you are lead routing by zip code
        if($this->data()->address && $this->data()->city && $this->data()->state) {
            $zip = $this->_getZipFromGoogle($this->data()->address, $this->data()->city, $this->data()->state);
            $this->data()->zip = ($zip) ? $zip : '00000';
        }

        // populate the br, bath, sf data
        if(preg_match('/(.*)bd, (.*) full ba (.*) sqft /i', $content, $matches)) {
            $this->data()->bedrooms = $matches[1];
            $this->data()->baths = $matches[2];
            $this->data()->sq_feet = str_replace(',','',$matches[3]);
        }
        elseif(preg_match('/(.*)bd, (.*) full\, (.*) partial ba (.*) sqft /i', $content, $matches)) {
            $this->data()->bedrooms = $matches[1];
            $this->data()->baths = $matches[2];
            $this->data()->half_baths = $matches[3];
            $this->data()->sq_feet = str_replace(',','',$matches[4]);
        }
        elseif(preg_match('/(.*) sqft /i', $content, $matches)) {
            $this->data()->sq_feet = trim(str_replace(',','',$matches[1]));
        }
    }

    protected function _importNewLeadfromTrulia($content)
    {
        $this->_componentTypeId = \ComponentTypes::BUYERS;
        preg_match('/Message: (.*)From: /ismU', $content, $matches);
        $this->data()->comments = trim($matches[1]);
    }

    protected function _importContactRequestLead($content)
    {
        $this->_componentTypeId = \ComponentTypes::BUYERS;

        // Message / Comments
        preg_match('/Message\:(.*)Reply URL\:/ismU', $content, $matches);
        $this->data()->comments = trim($matches[1]);

        preg_match('/(.*)br, (.*) ba\\n(.*)\\nMLS ID: (.*)\\n/i', $content, $matches);
        $this->data()->bedrooms = $matches[1];
        $this->data()->baths = $matches[2];
        $this->data()->price_min = preg_replace('/[^0-9]/i', '', $matches[3]);;
        $this->data()->listing_id = $matches[4];
    }

    protected function _importNewMessageLead($content)
    {
        $this->_componentTypeId = \ComponentTypes::BUYERS;

        // Message / Comments
        preg_match('/Message:(.*)\\nCheck for pre\-qualification info on this buyer/ismU', $content, $matches);
        $this->data()->comments .= trim($matches[1]);

        // listing id
        preg_match('/MLS ID:(.*)\\n/ismU', $content, $matches);
        $this->data()->listing_id = $matches[1];

        // Example text from email: Ilona Parker is interested in this listing priced at $244,900. Please contact them as soon as possible:\n18 Pine Tops Dr, Asheville, NC
        preg_match('/is interested in this listing priced at (\\$([0-9]+)+(\,[0-9]{3})). Please contact them as soon as possible:\\n(.*)\\n/ismU', $content, $matches);

        // price min
        $price = $matches[1];
        $this->data()->price_min = \Yii::app()->format->formatInteger($price);

        // address without zip cuz email doesn't have it.  Example text: 18 Pine Tops Dr, Asheville, NC
        $address = trim($matches[4]);
        preg_match('/(.*), ((.*), ([a-z]{2}))/i', $address, $matches);
        $addressStreet = $matches[1]; // full address (without zip cuz email doesn't have it)
        $city = $matches[3];
        $state = $matches[4];

        // check to see if message has zip included
        if(preg_match('/(.*), ('.addslashes($city).', '.addslashes($state).' ([0-9]{5}))/i', $this->data()->comments, $matches)) {
            $this->data()->zip = $matches[3];
        }
        else {
            // get zip code from Google if needed... may be necessary for lead routing ... if you find this,
            $zip = $this->_getZipFromGoogle($addressStreet, $city, $state);
            $this->data()->zip = $this->data()->zip_codes = ($zip) ? $zip : '00000'; //@todo: find out why load both zip and zip_codes
        }

        $this->data()->location = $city.', '.$state.' '.$this->data()->zip;
    }

    protected function _getZipFromGoogle($address, $city, $state)
    {
        // Get zip code from Google API as Trulia leaves that data out
        $ch = curl_init();

        // must use proxy, current server is block, just like craigslist
        if(!YII_DEBUG) {
            curl_setopt($ch, CURLOPT_PROXY, 'http://cgp.seizethemarket.net');
            curl_setopt($ch, CURLOPT_PROXYPORT, '8080');
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'stm:Kaqufr3y');
        }

        // Gets google maps api data based on address
        $curlUrl = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address.', '.$city.', '.$state).'&sensor=true';
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $googleAddressInfo = curl_exec($ch);
        if($googleAddressInfo) {
            $googleAddressInfo = json_decode($googleAddressInfo, true);
            if($googleAddressInfo['results'][0]['address_components']) {
                foreach($googleAddressInfo['results'][0]['address_components'] as $addressComponent) {
                    if($addressComponent['types'][0] == 'postal_code') {
                        $zipCode = $addressComponent['short_name'];
                        break;
                    }
                }
            }
            else {
                // send error message reporting what happened and data for debugging
                \Yii::log(__CLASS__.' (:'.__LINE__.') Could not find google address result. Address: '.$address.' City: '.$city.' State: '.$state.' Google Response: '.print_r($googleAddressInfo, true), \CLogger::LEVEL_ERROR);
            }

            //check to see if zipData has the right type, incase google changed their order around
            $zipCode = ($zipCode) ? $zipCode : null;
        }

        return $zipCode;
    }
}