<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Base.php');
include_once(__DIR__ . '/../Data/Zillow.php');

/**
 * Sns
 *
 * @author Nicole Xu
 * @package StmLeads\Adapters
 */
class Zillow extends Base
{
    /**
     * Import Data
     *
     * Abstract method that must be defined in all sub-classes
     * @param array $data Data to import
     * @return void
     */
    public function importData($data = array())
    {
        // Check input
        if(!is_array($data)) {
            throw new \Exception('Data must be an array');
        }

        // Wipe existing data
        $this->_data = new \StmLeads\Data\Zillow();

        $contactInfo = $data['ContactInfo'];
        $searchCriteria = $data['SearchCriteria'];
        $propertyInfo = $data['PropertyInfo'];

        // figure out which client it is by PartnerAgentIdentifier
        $this->data()->stm_zillow_id = trim($contactInfo['PartnerAgentIdentifier']);

        $clientAccount = \ClientAccounts::model()->findByAttributes(array('id'=>$contactInfo['PartnerAgentIdentifier'])); //, 'outside_lead_source_id'=>\OutsideLeadKeys::SOURCE_ZILLOW

        // if not found client match, log error with all data and stop process
        if(!$clientAccount) {
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Zillow client match not found with Agent Identifier: '.$contactInfo['PartnerAgentIdentifier'].PHP_EOL.' Data: '.print_r($data, true).PHP_EOL, \CLogger::LEVEL_ERROR);
            \Yii::app()->end();
        }

        $this->data()->client_id = $clientAccount->client_id;
        $this->data()->account_id = $clientAccount->account_id;

        //configure what db it goes into based on the partner identifier
        if(!\StmFunctions::connectDbByClientIdAccountId($this->data()['client_id'], $this->data()['account_id'])) {
            //error message
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Zillow adapter could not make client database connection. Partner Identifier: '.$contactInfo['PartnerAgentIdentifier'].PHP_EOL.' Client ID: '.$this->data()['client_id'].' Account ID: '.$this->data()['account_id'].' Data: '.print_r($data, true).PHP_EOL, \CLogger::LEVEL_ERROR);
            \Yii::app()->end();
        }

        //@todo: log until bug is figure out with accountID missing for LuxeHomesTeam for Yii::app()->user->accountId
        \Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Zillow Lead Submission: Yii::app()->user->accountId is missing. Investigate immediately. Remove log when done debugging. Yii::app()->user data: ' . print_r(\Yii::app()->user, 1).PHP_EOL.'POST Data: '.PHP_EOL.print_r($_POST, 1), \CLogger::LEVEL_ERROR);

        // @todo: primary_domain_name needs to be required - how to structure this?
        // set the primary domain (required) after database connection is set
        $this->data()->primary_domain_name = \Domains::model()->byIgnoreAccountScope()->findByAttributes(array('account_id'=>$this->data()->account_id, 'for_emails'=>1, 'is_active'=>1))->name;

        // format name to first and last name
        $name = explode(' ', $contactInfo['ContactName'], 2);
        $this->data()->first_name = $name[0];
        $this->data()->last_name = (isset($name[1]))? trim($name[1]) : '';

        $this->data()->email = $contactInfo['ContactEmail'];
        $this->data()->phone = preg_replace('/[^0-9]/', '', $contactInfo['ContactPhone']);
        $this->data()->comments = $contactInfo['ContactMessage'];
        $this->data()->source_description = 'Zillow';

        // import property data if information is provided
        if(!empty($propertyInfo)) {
            if(isset($propertyInfo['ListingPrice'])) {
                $this->data()->price = $propertyInfo['ListingPrice'];
            }

            if(isset($propertyInfo['ListingStatus'])) {
                $this->data()->listing_status = $propertyInfo['ListingStatus'];
            }

            if(in_array($this->data()->listing_status, array('Not For Sale', 'PreForeclosure', 'Foreclosure'))) {

                // There are instances where StreetAddress has data such as 123 Main St, Jacksonville, FL 32202 and the duplicate city state is in it's own field.
                $addressCityStateDelimiter = ', '.$propertyInfo['City'].', '.$propertyInfo['State'];
                $addressParts = explode($addressCityStateDelimiter, $propertyInfo['StreetAddress'], 2);

                $this->data()->address = trim($addressParts[0]);
                $this->data()->city = $propertyInfo['City'];
                $this->data()->state = $propertyInfo['State'];
                $this->data()->zip = $propertyInfo['Zip'];

                if($this->data()->listing_status == 'PreForeclosure') {
                    $this->data()->comments = 'Lead Type: Pre-Foreclosure Lead. '.PHP_EOL.$this->data()->comments;
                }
                if($this->data()->listing_status == 'Foreclosure') {
                    $this->data()->comments = 'Lead Type: Foreclosure Lead. '.PHP_EOL.$this->data()->comments;
                }
            }

            $this->data()->bedrooms = $propertyInfo['Beds'];
            $this->data()->baths = $propertyInfo['Baths'];
        }
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Determine the component type ID
        switch($this->data()->listing_status) {
            case 'PreForeclosure':
            case 'Foreclosure':
            case 'For Sale':
                $this->_componentTypeId = \ComponentTypes::BUYERS;
                break;

            case 'Not For Sale':
                $this->_componentTypeId = \ComponentTypes::SELLERS;
                break;
            default:
                $this->_componentTypeId = \ComponentTypes::BUYERS;
                \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Zillow component type was not determined. Default to buyer. See Data: '.print_r($this->data(), true), \CLogger::LEVEL_ERROR);
                //throw new \Exception('An unknown listing status has been detected');
                break;
        }

        // Set source data
        $this->_setSourceData($this->data()->account_id, \Settings::ZILLOW_SOURCE_ID, $sourceDescription = 'Zillow');
    }

    /**
     * Save
     *
     * All adapters must come up with their own save functionality
     * @return mixed
     */
    public function save()
    {
        // Start by storing basic info
        $this->_saveBasicInfo();

        // Save form submission data !? are we even doing this? //@todo: can add zillow form to db if needed
//        $this->_saveFormSubmissionData();

        // Save component model
        //@todo: have new type in activity log submission?
        $this->_saveComponentModel($this->_componentTypeId);

        // Handle lead routing
        $this->_routeLead($this->_componentTypeId);

        return 1; //@todo: should there be a flag for whether everything saved properly? This is used to login the user if everything went right.
    }
}