<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/TigerLead.php');

/**
 * Commissions Inc
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class TigerLead extends Email
{
    protected $_leadTypeName = 'Tiger Lead';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $message \StmZendMailMessageImap
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        if(!$message) {
            return false;
        }

        // subject line with Returned to Website is not a valid email to parse
        if(strpos($message->subject, 'Returned to Website') !== false) {
            return false;
        }

        $content = quoted_printable_decode($message->getMessageBodyText());

        // Grab the top Contact Information section
        preg_match('/--Contact Information--\\n(.*)'
                    .'Name: (.*)\\n'
                    .'Email: (.*)\\n'
                    .'Phone: (.*)\\n'
                    .'Timeframe: (.*)\\n'
                    .'Homeowner: (.*)\\n'
                    .'Comments: (.*)\\n'
                    .'\\n\\n--/ismU', $content, $matches);
        if($contactInfo = $matches[1]) {
            // Grab name and parse into first and last name
            $name = explode(' ',$matches[2], 2);
            $this->data()->first_name = ucwords(strtolower(trim($name[0])));
            $this->data()->last_name = (isset($name[1]))? ucwords(strtolower(trim($name[1]))) : '-';

            // Grab email address
            $this->data()->email = strtolower(trim($matches[3]));

            // Grab phone number
            if($phone = preg_replace('/[^0-9]/i', '', $matches[4])) {
                $this->data()->phone = $phone;
            }

            // Grab timeframe
            $this->data()->timeframe = trim($matches[5]);

            // Grab is homeowner
            $this->data()->is_homeowner = trim($matches[6]);

            // this is being commented out as it is parsed below???
//            if($matches[7]) {
//                $this->data()->comments = trim($matches[7]);
//            }
        }

        // Grab the top New Lead Information section. This doesn't always exist so check for it first
        preg_match('/--New Lead Info--\\n(.*)'
            .'Buyer or Seller: (.*)\\n'
            .'Full Name: (.*)\\n'
            .'Email: (.*)\\n'
            .'Phone: (.*)\\n'
            .'Area: (.*)\\n'
            .'Min Bedroom: (.*)\\n'
            .'Price Range: (.*)\\n'
            .'Time Frame: (.*)\\n'
            .'Preferred Contact Method: (.*)\\n'
            .'Preferred Contact Time: (.*)\\n'
            .'\\n\\n/ismU', $content, $matches);

        if($matches) {
            if(trim($matches[2]) == 'Buyer') {
                $this->_componentTypeId = \ComponentTypes::BUYERS;
            }
            if(trim($matches[2]) == 'Seller') {
                $this->_componentTypeId = \ComponentTypes::SELLERS;
            }

            // Area
            if(trim($matches[6])) {
                $this->data()->location = trim($matches[6]);
            }

            // Minimum bedrooms
            if(trim($matches[7])) {
                $this->data()->bedrooms = trim($matches[7]);
            }

            // Preferred Contact method
            if(trim($matches[10])) {
                $this->data()->preferred_contact_method = trim($matches[10]);
            }

            // Preferred Contact time
            if(trim($matches[11])) {
                $this->data()->preferred_contact_time = trim($matches[11]);
            }
        }

//        From: tyrone cooks <a style="white-space: nowrap" href="http://paws.tigerlead.com/login/lead/466372843-vtf5"><img src="http://paws.tigerlead.com/images/url_icon.gif" style="vertical-align:middle" border="0" /></a> <a href="http://paws.tigerlead.com/login/lead/466372843-vtf5">View Lead File</a>
//    Phone: 2059602278 (Daytime)
//Email: rico.three@yahoo.com
//Source: SearchAllBirminghamHomes.com - Showing Request Form
//
//Address: 2015 NW 9th St Cir, Center Point, AL 35215 <a style="white-space: nowrap" href="http://searchallbirminghamhomes.com/l/1000071490216"><img src="http://paws.tigerlead.com/images/url_icon.gif" style="vertical-align:middle" border="0" /></a> <a href="http://searchallbirminghamhomes.com/l/1000071490216">View Home</a>
//    MLS #: 741269
//
//I'd like to see it this Friday I'll be in the area.

        preg_match('/From: (.*) \<a(.*)\<\/a>\\n'
            .'Phone: (.*)\((.*)\\n'
            .'Email: (.*)\\n'
            .'Source: (.*) \- Showing Request Form\\n\\n'
            .'Address: (.*) \<a(.*)\<\/a>\\n'
            .'MLS \#: (.*)\\n\\n'
            .'(.*)\\n'
            .'/ismU', $content, $matches);

        if($matches) {
            $this->_componentTypeId = \ComponentTypes::BUYERS;

            if($contactInfo = $matches[1]) {
                // Grab name and parse into first and last name
                $name = explode(' ',$matches[1], 2);
                $this->data()->first_name = ucwords(strtolower(trim($name[0])));
                $this->data()->last_name = (isset($name[1]))? ucwords(strtolower(trim($name[1]))) : '-';

                // Grab email address
                $this->data()->email = strtolower(trim($matches[5]));

                // Grab phone number
                if($phone = preg_replace('/[^0-9]/i', '', $matches[3])) {
                    $this->data()->phone = $phone;
                }

                $this->data()->comments = 'Showing Request - MLS #: ' . trim($matches[9]) . ' - '.trim($matches[7]).' - '.trim($matches[10]);
            }
        }

        // Grab the top Contact Information section
        preg_match('/--Search Criteria--\\n(.*)'
            .'Region: (.*)\\n'
            .'Area\(s\):\\n(.*)\\n'
            .'Price Range: (.*)\\n'
            .'Property Type\(s\): (.*)\\n'
            .'Bedrooms: (.*)\\n'
            .'Bathrooms: (.*)\\n'
            .'Min. Sqft: (.*)\\n'
            .'Garage: (.*)\\n'
            .'Year Built: (.*)\\n'
            .'/ism', $content, $matches);
        if($matches) {
            // $['region'] = trim($matches[2]); // region is so general, it's not useful

            if(trim($matches[3])) { //@todo: need to accomodate for single and multiple area

                $areaString = '';
                $areaParts = explode(PHP_EOL,trim($matches[3]));
                if(count($areaParts) > 1) {
                    $areaData = array();
                    // organize county/city data into an array pair of count => array of cities to loop through later and build string format of "county: city1, city2 | county2: city3, city4"
                    foreach($areaParts as $key => $value) {
                        $valueParts = explode(' \ ', $value);
                        $areaData[trim($valueParts[0])][] = trim($valueParts[1]);
                    }

                    foreach($areaData as $county => $cities) {
                        $uniqueCities = array_unique($cities);
                        $areaString .= ($areaString)? ' | ' : '';
                        $areaString .= $county.': '.implode(', ', $uniqueCities);
                    }
                }
                else {
                    $valueParts = explode(' \ ', $areaParts[0]);
                    $areaString = $valueParts[0].': '.$valueParts[1];
                }
                $this->data()->location .= ($this->data()->location)? ', '.$areaString : $areaString;
            }


            /**
             * Check for default values that equate to no user input such as -, +, All. If not default, then assign to $data, else ignore
             */
            if(trim($matches[4]) !== '-') {
                $priceRange = trim($matches[4]);  //value of - equates to none
                if(strpos($priceRange, '-') !== false) {
                    //@todo: price min/max
                }
            }

            if(trim($matches[5]) !== 'All') {
                $this->data()->property_types = trim($matches[5]); //value of All equates to none
            }

            if(trim($matches[6]) !== '+') {
                $this->data()->bedrooms = trim($matches[6]); //value of + equates to none
            }

            if(trim($matches[7]) !== '+') {
                $this->data()->baths = trim($matches[7]); //value of + equates to none
            }

            if(trim($matches[8]) !== '+') {
                $this->data()->sq_feet = trim($matches[8]); //value of + equates to none
            }

            if(trim($matches[9]) !== '+') {
                $this->data()->garage = trim($matches[9]); //value of + equates to none
            }

            if(trim($matches[10]) !== '-') {
                $this->data()->yr_built = trim($matches[10]);  //value of - equates to none
            }
        }

        if(!$this->_componentTypeId) {
            $this->_componentTypeId = \ComponentTypes::BUYERS;
        }

        if(!($this->_source = \Sources::model()->findByAttributes(array('name'=>'Tiger Lead')))) {
            $this->_source = \Sources::model()->findByPk(\Sources::UNKNOWN); //@todo: need to have an default Unknown source per account_id via setting
        }

        $this->data()->source_description = 'Tiger Lead';

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        return nl2br(quoted_printable_decode($message->getMessageBodyText()));
    }
}