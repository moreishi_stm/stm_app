<?php
namespace StmLeads\Adapters;

include_once('Base.php');
/**
 * Namespace Declaration
 */

/**
 * Email
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
abstract class Email extends Base
{
    protected $_leadTypeName;

    function __construct($accountId)
    {
        if(!is_numeric($accountId)) {
            throw new \Exception(__CLASS__.' (:'.__LINE__.') Account ID is not numeric.');
        }

        // should we automated this??? ... should I use reflection class or the explode method?
        $className = end(explode('\\', get_class()));
        $className = (new \ReflectionClass($this))->getShortName();
        $dataClassName = '\StmLeads\Data\\'.$className;
        $this->_data = new $dataClassName();

        $this->data()->account_id = $accountId;
        $this->data()->primary_domain_name = \Domains::model()->findByAttributes(array('is_primary'=>1))->name;
    }

    abstract public function importData(\StmZendMailMessageImap $message);

    public function sendEmailCopy(\StmZendMailMessageImap $message)
    {
        $emailCC = \Yii::app()->db->createCommand("SELECT LOWER(TRIM(value)) FROM setting_account_values WHERE account_id=".$this->data()['account_id']." AND setting_id=".\Settings::THIRD_PARTY_LEADS_EMAIL_CC." AND is_deleted=0")->queryScalar();

        // if not email in setting return
        if(!$emailCC) {
            return;
        }

        $body = '<html><body>'.$this->_getHtmlEmailBody($message).'</body></html>';

        //@todo: include a message before the body? - "This is your copy of the lead that has been processed."

        // send email
        try{

            \StmZendMail::easyMail($from=array('do-not-reply@seizethemarket.net' => 'New Lead (Copy)'), $emailCC, $subject='New Lead (Copy) Seize the Market - '.$this->_leadTypeName, $body, $bcc='christine@seizethemarket.com', $type='text/html', $awsTransport=true, $ignoreOnDebug = false);

        }catch (\Exception $e) {

            \Yii::log(__CLASS__ . ' (:'. __LINE__ .') Email Lead Processing had error when sending Email Copy to Client. Data: '.print_r($this->data(), true).PHP_EOL.'Exception Message: '.$e->getMessage(), \CLogger::LEVEL_ERROR);
        }
    }

    /**
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed
     */
    abstract protected function _getHtmlEmailBody(\StmZendMailMessageImap $message);

    /**
     * Save
     *
     * Performs save operation
     * @return mixed
     */
    public function save()
    {
        // validates basic contact fields in data()
        if(!$this->_validateContactInfo()) {
            return false;
        }

        // Start by storing basic info
        $this->_saveBasicInfo();

        // Save component model, default to buyer if not available
        if(!$this->_componentTypeId) {

            $this->_componentTypeId = \ComponentTypes::BUYERS;

            //send error log as this is a last resort default. Should be specified in child class.
            \Yii::log(__CLASS__ . ' (:'. __LINE__ .') Email Lead Processing is missing Component Type Id. Defaulted to Buyer. Investigate child class to see if handling for this is missing.', \CLogger::LEVEL_ERROR);
        }
        $this->_saveComponentModel($this->_componentTypeId);

        // Handle lead routing
        $this->_routeLead($this->_componentTypeId);

        return true;
    }

    /**
     * Logs the original email into the component activity log
     * @return bool
     */
    public function logEmail(\StmZendMailMessageImap $message)
    {
        if(!$this->_componentTypeId) {
            \Yii::log(__CLASS__.' (:'.__LINE__.') Component Type Id missing for logEmail() in processing a 3rd party lead. Investigate immediately!' . print_r($message->subject, true), \CLogger::LEVEL_ERROR);
            return false;
        }
        if(!$this->_componentModel->id) {
            \Yii::log(__CLASS__.' (:'.__LINE__.') Component Id missing for logEmail() in processing a 3rd party lead. Investigate immediately!' . print_r($message->subject, true), \CLogger::LEVEL_ERROR);
            return false;
        }

        // Create a new activity log entry
        $activityLog = new \ActivityLog();
        $activityLog->activity_date = new \CDbExpression('NOW()');
        $activityLog->account_id = $this->data()['account_id'];
        $activityLog->component_id = $this->_componentModel->id;
        $activityLog->component_type_id = $this->_componentTypeId;;
        $activityLog->task_type_id = \TaskTypes::SYSTEM_NOTE;
        $activityLog->added = new \CDbExpression('NOW()');
        $activityLog->updated = new \CDbExpression('NOW()');
        $activityLog->added_by = null; // system notes do not have a contact associated with it

        $activityLog->note = ($message->subject) ? 'Subject: '.$message->subject.PHP_EOL.PHP_EOL : '';
        $activityLog->note .= $this->_getHtmlEmailBody($message);

        if (!$activityLog->save(false)) {
            \Yii::log(__CLASS__.' (:'.__LINE__.') Could not log resubmit info.' . print_r($activityLog->attributes, true) . PHP_EOL . 'Model Errors: ' . print_r($activityLog->errors, true), \CLogger::LEVEL_ERROR);
            //throw new \Exception(__CLASS__.' (:'.__LINE__.') Unable to save activity log information');
        }
        return true;
    }

    /**
     * Validates Basic contact info in $this->data()
     * @return bool
     */
    protected function _validateContactInfo()
    {
        $validated = true;

        if(!$this->data()->first_name) {
            $validated = false;
        }
        if(!$this->data()->last_name) {
            $validated = false;
        }
        if(!$this->data()->email) {

            $validated = false;
        }
        else {

            // check for invalid email address @todo: this error-ing any resubmits
//            $emailModel = new \Emails();
//            $emailModel->email = $this->data()['email'];
//            if(!$emailModel->validate(array('email'))) {
//                $validated = false;
//            }
        }

        if(strlen($this->data()->phone) > 10) {
            $this->data()->phone = substr($this->data()->phone, 0, 10);
        }

        //@todo: what should happen if the phone is too short? - as NICOLE on how to unset and if that's the recommended way to handle this
//        if(strlen($this->data()->phone) < 10) {
//            $this->data()->phone = substr($this->data()->phone, 0, 10);
//        }

        return $validated;
    }
}