<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/REW.php');

/**
 * REW
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class REW extends Email
{
    protected $_leadTypeName = 'REW';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $content Email message contents
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        $content = $message->getMessageBodyHtml();

        switch($message->subject) {
            case 'IDX Registration';

                // Grab name and parse into first and last name
                preg_match('/Name:<\/strong>(.*)<br>/i', $content, $matches);
                $name = explode(' ',trim($matches[1]), 2);
                if(count($name) == 2) {
                    $this->data()->first_name = ucwords(strtolower(trim($name[0])));
                    $this->data()->last_name = ucwords(strtolower(trim($name[1])));
                }

                // Grab email address
                preg_match('/Email:(.*)<a(.*)>(.*)<\/a>/i', $content, $matches);
                $this->data()->email = strtolower(trim($matches[3]));

                // Grab phone number
                preg_match('/Phone:<\/strong>(.*)<br>/i', $content, $matches);
                $this->data()->phone = preg_replace('/[^0-9]/i', '', trim($matches[1]));

                // Grab Viewed Listing
                preg_match('/(.*)Viewed Listings:<\/strong>(.*)<br>/i', $content, $matches);
                if($matches) {
                    $this->data()->comments = 'Viewed Listings: '.trim($matches[2]);
                }

                // All leads are Buyers until we get sample otherwise
                $this->_componentTypeId = \ComponentTypes::BUYERS;

                break;

            case 'Connected via Google':

                // Grab name and parse into first and last name
                preg_match('/Name:<\/strong>(.*)<br>/i', $content, $matches);
                $name = explode(' ',trim($matches[1]), 2);
                if(count($name) == 2) {
                    $this->data()->first_name = ucwords(strtolower(trim($name[0])));
                    $this->data()->last_name = ucwords(strtolower(trim($name[1])));
                }

                // Grab email address
                preg_match('/Email:(.*)<a(.*)>(.*)<\/a>/i', $content, $matches);
                $this->data()->email = strtolower(trim($matches[3]));

                // Grab phone number
                preg_match('/Phone:<\/strong>(.*)<br>/i', $content, $matches);
                $this->data()->phone = preg_replace('/[^0-9]/i', '', trim($matches[1]));

                // Grab google profile - put in comments
                //Profile:</strong> <a href="https://plus.google.com/117044446757594481425">https://plus.google.com/117044446757594481425</a>
                preg_match('/Profile:(.*)<a(.*)>https:\/\/plus.google.com\/([0-9]*)<\/a>(.*)/i', $content, $matches);
                $this->data()->comments = 'Google Plus Profile: https://plus.google.com/'.$matches[3];

                // Grab Viewed Listing
                preg_match('/(.*)Viewed Listings:<\/strong>(.*)<br>/i', $content, $matches);
                if($matches) {
                    $this->data()->comments .= PHP_EOL.'Viewed Listings: '.trim($matches[2]);
                }

                $this->data()->source_description = 'REW - Connected via Google';

                // All leads are Buyers until we get sample otherwise
                $this->_componentTypeId = \ComponentTypes::BUYERS;
                break;

            case 'Seller Form';

                // Grab first name
                preg_match('/First name(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->first_name = ucwords(strtolower(trim($matches[2])));

                // Grab last name
                preg_match('/Last name(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->last_name = ucwords(strtolower(trim($matches[2])));

                // Grab email address
                preg_match('/Telephone(.*)<br>(.*)<br><br>/i', $content, $matches);
                $this->data()->phone = preg_replace('/[^0-9]/i', '', trim($matches[2]));

                // Grab phone
                preg_match('/Email(.*)<br>(.*)<br><br>/i', $content, $matches);
                $this->data()->email = strtolower(trim($matches[2]));

                // Grab address
                preg_match('/Fm-addr(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->address = ucwords(strtolower(trim($matches[2])));

                // Grab city
                preg_match('/Fm-town(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->city = ucwords(strtolower(trim($matches[2])));

                // Grab state
                preg_match('/Fm-state(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->state = strtoupper(trim($matches[2]));

                // Grab zip
                preg_match('/Fm-postcode(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->zip = preg_replace('/[^0-9]/i', '', trim($matches[2]));

                // Grab beds
                preg_match('/Bedrooms(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->bedrooms = preg_replace('/[^0-9]/i', '', trim($matches[2]));

                // Grab baths
                preg_match('/Bathrooms(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                $this->data()->baths = preg_replace('/[^0-9]/i', '', trim($matches[2]));

                // Grab sq_feet - REW SF is a range, put it in notes, sq_feet is a numeric only field
                preg_match('/Square feet(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                if(!empty(trim($matches[2]))) {
                    $this->data()->comments .= (!empty($this->data()['comments'])) ? PHP_EOL : '';
                    $this->data()->comments .= 'Sq Feet: ' . trim($matches[2]);
                }

                preg_match('/Type of property(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                if(!empty(trim($matches[2]))) {
                    $this->data()->comments .= (!empty($this->data()['comments'])) ? PHP_EOL : '';
                    $this->data()->comments .= 'Property Type: ' . trim($matches[2]);
                }

                preg_match('/When sell(.*)<\/b><br>(.*)<br><br>/i', $content, $matches);
                if(!empty(trim($matches[2]))) {
                    $this->data()->comments .= (!empty($this->data()['comments'])) ? PHP_EOL : '';
                    $timeframe = trim($matches[2]);
                    $timeframe = (strpos($matches[2], 'month') !== false) ? str_replace('_', '-', trim($matches[2])) : str_replace('_', ' ', trim($matches[2]));
                    $this->data()->comments .= 'Selling Timeframe: ' . $timeframe;
                }

                $this->_componentTypeId = \ComponentTypes::SELLERS;
                break;

            case 'Quick Inquire';
                return false;
                break;

            case (strpos($message->subject, 'Contact Form - ') !== false): //Contact Form - Selling our home in Canton
                return false;
                break;

            case (strpos($message->subject, 'IDX Inquiry - Property Showing -') !== false): //IDX Inquiry - Property Showing - 24640 Ridgeview Drv, Farmington Hills, MI
                return false;
                break;

            case (strpos($message->subject, 'Saved Favorite Notification - ') !== false): //Saved Favorite Notification - MLS® #215017632
                return false;
                break;

            default;
                return false;
                break;
        }

        if(!($this->_source = \Sources::model()->findByAttributes(array('name'=>'REW')))) {
            $this->_source = \Sources::model()->findByPk(\Sources::UNKNOWN); //@todo: need to have an default Unknown source per account_id via setting
        }

        if(!$this->data()->source_description) {
            $this->data()->source_description = 'REW';
        }

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        return $message->getMessageBodyHtml();
    }
}