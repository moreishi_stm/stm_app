<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Base.php');
include_once(__DIR__ . '/../Data/InsideRealEstate.php');

/**
 * REW
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class InsideRealEstate extends Base
{
    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $content Email message contents
     * @return boolean
     */
    public function importData($data = array())
    {
        // Check input
        if(!is_array($data)) {
            throw new \Exception('Data must be an array');
        }

        // Wipe existing data
        $this->_data = new \StmLeads\Data\InsideRealEstate();

        list(, $clientAccountId) = explode('.',$data['identifier']);

        $clientAccount = \ClientAccounts::model()->findByAttributes(array('id'=>$clientAccountId));

        // if not found client match, log error with all data and stop process
        if(!$clientAccount) {
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Inside Real Estate client Account ID invalid. '.PHP_EOL.' Data: '.print_r($data, true).PHP_EOL, \CLogger::LEVEL_ERROR);
            \Yii::app()->end();
        }

        if(\Yii::app()->request->getPost('cid') != $clientAccount->client_id) {
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Inside Real Estate client Client ID post does not match the UUID. '.PHP_EOL.' Data: '.print_r($data, true).PHP_EOL, \CLogger::LEVEL_ERROR);
            \Yii::app()->end();
        }

        if(\Yii::app()->request->getPost('aid') != $clientAccount->account_id) {
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Inside Real Estate client Account ID post does not match the UUID. '.PHP_EOL.' Data: '.print_r($data, true).PHP_EOL, \CLogger::LEVEL_ERROR);
            \Yii::app()->end();
        }

        $this->data()->client_id = $clientAccount->client_id;
        $this->data()->account_id = $clientAccount->account_id;

        //configure what db it goes into based on the partner identifier
        if(!\StmFunctions::connectDbByClientIdAccountId($this->data()['client_id'], $this->data()['account_id'])) {
            //error message
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Inside Real Estate adapter could not make client database connection. Partner Identifier: '.$data['identifier'].PHP_EOL.' Client ID: '.$this->data()['client_id'].' Account ID: '.$this->data()['account_id'].' Data: '.print_r($data, true).PHP_EOL, \CLogger::LEVEL_ERROR);
            \Yii::app()->end();
        }

        // @todo: primary_domain_name needs to be required - how to structure this?
        // set the primary domain (required) after database connection is set
        $this->data()->primary_domain_name = \Domains::model()->byIgnoreAccountScope()->findByAttributes(array('account_id'=>$this->data()->account_id, 'for_emails'=>1, 'is_active'=>1))->name;

        // format name to first and last name
        $this->data()->first_name = ucwords(strtolower(trim($data['first_name'])));
        $this->data()->last_name = ucwords(strtolower(trim($data['last_name'])));

        $this->data()->email = strtolower(trim($data['email']));

        switch($data['lead_type']) {
            case 'buyer':
                $this->_componentTypeId = \ComponentTypes::BUYERS;
                break;

            case 'seller':
                $this->_componentTypeId = \ComponentTypes::SELLERS;
                break;

            default:
                throw new \Exception(__CLASS__ . ' (' . __LINE__ . ') Lead type invalid. '.PHP_EOL.' Data: '.print_r($data, true));
                break;

        }

        $this->data()->source_description = $data['source_description'];

        // go through all 4 phone fields. If there is more than 1 send log. Configuring for 1 as it is highly unlikely that there will be more than 1 phone #. Email alert if there is more than one.

        $phonesToAdd = array();
        for($i=0; $i<=4; $i++) {

            if(!empty($data['phone_'.$i])) {
                $phonesToAdd[] = \Yii::app()->format->formatPhone($data['phone_'.$i]);
            }
        }

        if(count($phonesToAdd) == 1) {
            $this->data()->phone = $phonesToAdd[0];
        } elseif(count($phonesToAdd) > 1) {
            \Yii::log(__CLASS__ . ' (' . __LINE__ . ') Inside Real Estate has more than 1 phone #. '.PHP_EOL.' Data: '.print_r($data, true).PHP_EOL, \CLogger::LEVEL_ERROR);

            $additionalPhones = '';
            for($i=1; $i < count($phonesToAdd); $i++) {
                $additionalPhones .= ($additionalPhones) ? ', '.$phonesToAdd[$i] : $phonesToAdd[$i];
            }
            $this->data()->comments = 'Additional Phone #: '.$additionalPhones;
        }

        if($data['city']) {
            $this->data()->comments .= PHP_EOL.'City: '.$data['city'];
        }

        if($data['price_min']) {
            $this->data()->comments .= PHP_EOL.'Min Price: '.$data['price_min'];
        }

        if($data['listing_id']) {
            $this->data()->comments .= PHP_EOL.'Listing ID: '.$data['listing_id'];
        }

        if($data['zip']) {
            $this->data()->comments .= PHP_EOL.'Zip: '.$data['zip'];
        }

        if($data['price']) {
            $this->data()->comments .= PHP_EOL.'Price: '.$data['price'];
        }

        if($data['listing_status']) {
            $this->data()->comments .= PHP_EOL.'Listing Status: '.$data['listing_status'];
        }

        if($data['capture_method']) {
            $this->data()->comments .= PHP_EOL.'Capture Method: '.$data['capture_method'];
        }
    }

    /**
     * Save
     *
     * All adapters must come up with their own save functionality
     * @return mixed
     */
    public function save()
    {
        // Start by storing basic info
        $this->_saveBasicInfo();

        // Save form submission data !? are we even doing this? //@todo: can add separate form to db if needed
        //        $this->_saveFormSubmissionData();

        // Save component model
        //@todo: have new type in activity log submission?
        $this->_saveComponentModel($this->_componentTypeId);

        // Handle lead routing
        $this->_routeLead($this->_componentTypeId);

        return 1; //@todo: should there be a flag for whether everything saved properly? This is used to login the user if everything went right.
    }
}