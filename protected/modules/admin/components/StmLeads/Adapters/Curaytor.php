<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/Curaytor.php');

/**
 * RealtorDotCom
 *
 * @author Nicole Xu
 * @package StmLeads\Adapters
 */
class Curaytor extends Email
{
    protected $_leadTypeName = 'Curaytor';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $message \StmZendMailMessageImap
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
    }
    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
    }
}