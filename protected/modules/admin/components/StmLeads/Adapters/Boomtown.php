<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/Boomtown.php');

/**
 * Boomtown
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class Boomtown extends Email
{
    protected $_leadTypeName = 'Boomtown';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $content Email message contents
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        // if the subject line does not contain register, it is not a valid email to parse.
        if(strpos($message->subject, '[REGISTER]') === false) {
            return false;
        }

        $content = quoted_printable_decode($message->getMessageBodyText());

        // Grab name and parse into first and last name
        preg_match('/Name................(.*)/i', $content, $matches);
        $name = explode(' ',$matches[1], 2);
        if(count($name) == 2) {
            $this->data()->first_name = ucwords(strtolower(trim($name[0])));
            $this->data()->last_name = ucwords(strtolower(trim($name[1])));
        }

        // Grab email address
        preg_match('/Email...............(.*)/i', $content, $matches);
        $this->data()->email = strtolower(trim($matches[1]));

        // Grab phone number
        preg_match('/Phone...........(.*)/i', $content, $matches);
        $this->data()->phone = preg_replace('/[^0-9]/i', '', $matches[1]);

        // Grab type of Buyer of Seller
        preg_match('/Type................(.*)/i', $content, $matches);
        $this->_componentTypeId = ($matches[1] == 'Buyer')? \ComponentTypes::BUYERS : \ComponentTypes::SELLERS;

        if(!($this->_source = \Sources::model()->findByAttributes(array('name'=>'Boomtown')))) {
            $this->_source = \Sources::model()->findByPk(\Sources::UNKNOWN); //@todo: need to have an default Unknown source per account_id via setting
        }

        $this->data()->source_description = 'Boomtown';

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        return nl2br(quoted_printable_decode($message->getMessageBodyText()));
    }
}