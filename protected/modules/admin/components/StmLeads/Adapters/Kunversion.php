<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/Kunversion.php');

/**
 * Commissions Inc
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class Kunversion extends Email
{
    protected $_leadTypeName = 'Kunversion';
    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $message \StmZendMailMessageImap
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        if($message->getPart(1)->getHeaders()['content-transfer-encoding'] !== 'base64') {
            return false;
        }

        $this->data()->primary_domain_name = \Domains::model()->findByAttributes(array('is_primary'=>1))->name;

        // strips out all tags and replaces html breaks with \n
        $content = base64_decode(trim($message->getPart(1)->getContent()));
        preg_match('/\<body\>(.*)\<\/body\>/ismU', $content, $matches);
        preg_match('/\<h2\> Lead Details(.*)\<div\>(.*)\<\/div\>(.*)\<\/td\>(.*)\<td\>/ismU', trim($matches[1]), $matches);
        $content = strip_tags(trim($matches[2]));

        // Grab the top Contact Information section
        preg_match('/(.*)Name: (.*)\\n'
                    .'(.*)Email: (.*)\\n'
                    .'(.*)Phone: (.*)\\n'
                    .'(.*)Type: (.*)\\n'
                    .'(.*)Referrer: (.*)\\n'
                    .'/ismU', $content, $matches);

        if($matches) {
            // Grab name and parse into first and last name
            $name = explode(' ',$matches[2], 2);
            if(count($name) == 2) {
                $this->data()->first_name         = ucwords(strtolower(trim($name[0])));
                $lastName = ($lastName = trim($name[1])) ? $lastName : '-';
                $this->data()->last_name = ucwords(strtolower($lastName));
            }

            // Grab email address
            if($matches[4]) {
                $this->data()->email = strtolower(trim($matches[4]));
            }

            // Grab phone number
            $phone = preg_replace('/[^0-9]/i', '', $matches[6]);
            if($phone) {
                $this->data()->phone = $phone;
            }

            if(trim($matches[8]) == 'Buyer') {
                $this->_componentTypeId = \ComponentTypes::BUYERS;
            }
            if(trim($matches[7]) == 'Seller') {
                $this->_componentTypeId = \ComponentTypes::SELLERS;
            }
        }

        $this->data()->source_description = 'Kunversion';
        if(strpos(trim($matches[10]), 'facebook.com') !== false) {  //@todo: what to do with this? ... there quite a few with facebook.com in the string, may be able to do something with this.
            $this->data()->source_description .= ' / Facebook';
        };

        // update source description if email is for chat transcript
        if(strpos($message->subject, 'Chat Transcript') !== false) {
            $this->data()->source_description .= ' / Chat Transcript';
        }

        if(!$this->_componentTypeId) {
            $this->_componentTypeId = \ComponentTypes::BUYERS;
        }

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
        $content = base64_decode(trim($message->getPart(1)->getContent()));
        preg_match('/\<body\>(.*)\<\/body\>/ismU', $content, $matches);
        return trim($matches[1]);
    }
}