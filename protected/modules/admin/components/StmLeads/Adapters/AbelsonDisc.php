<?php
/**
 * Namespace Declaration
 */
namespace StmLeads\Adapters;

// Include our base adapter
include_once('Email.php');
include_once(__DIR__ . '/../Data/AbelsonDisc.php');

/**
 * Boomtown
 *
 * @author Christine Lee
 * @package StmLeads\Adapters
 */
class AbelsonDisc extends Email
{
    protected $_leadTypeName = 'Abelson DISC';

    /**
     * Import From Email Contents
     *
     * Parses email and loads into data for further processing
     * @param $content Email message contents
     * @return boolean
     */
    public function importData(\StmZendMailMessageImap $message)
    {
        // if the subject line does not contain register, it is not a valid email to parse.
        if(strpos($message->subject, 'TTI Talent Insights') === false) {
            return false;
        }

        $content = quoted_printable_decode($message->getMessageBodyHtml());
        $content = strip_tags($content);

        // Grab name and parse into first and last name
        preg_match('/Respondent Name: \\n(.*)\\n\\n/ismU', $content, $matches);
        $name = explode(' ',$matches[1], 2);
        if(count($name) == 2) {
            $this->data()->first_name = ucwords(strtolower(trim($name[0])));
            $this->data()->last_name = ucwords(strtolower(trim($name[1])));
        }

        // Grab email address
        preg_match('/Respondent E-mail: \\n(.*)\\n\\n/ismU', $content, $matches);
        $this->data()->email = strtolower(trim($matches[1]));

        // Grab position
        preg_match('/Respondent Position: \\n(.*)\\n\\n/ismU', $content, $matches);
        if(trim($matches[1])) {
            $this->data()->comments = 'Position: '.trim($matches[1]);
        }

        // get DISC attachment
        $attachments = $message->getAttachments();
        if(!empty($attachments) && is_array($attachments)) {
            foreach($attachments as $attachment) {

                if(strpos($attachment['filename'],'_TImanR4_') !== false) {

                    $this->data()->document_disc = array(
                        'content' => $attachment['content'],
                        'filename' => $attachment['filename'],
                    );
                    break;
                }

                if(strpos($attachment['filename'],'_TIsalesR4_') !== false) {
                    $this->data()->document_disc = array(
                        'content' => $attachment['content'],
                        'filename' => $attachment['filename'],
                    );
                    break;
                }

                //@todo: delete after debugging 2015/10/02
                if(!YII_DEBUG) {
                    \Yii::log(__FILE__ . '(' . __LINE__ . '): FYI: DISC attachment found. Data: '.print_r($this->data(), true), \CLogger::LEVEL_ERROR);
                }
            }
        }

        if(!($this->_source = \Sources::model()->findByAttributes(array('name'=>'Abelson DISC')))) {
            $this->_source = \Sources::model()->findByPk(\Sources::UNKNOWN); //@todo: need to have an default Unknown source per account_id via setting
        }

        $this->_componentTypeId = \ComponentTypes::RECRUITS;
        $this->data()->source_description = 'Abelson DISC';

        return true;
    }

    /**
     * Gets HTML Email body content of lead. If body is text then converts to HTML ready with nl2br
     * @param \StmZendMailMessageImap $message
     *
     * @return mixed|void
     */
    protected function _getHtmlEmailBody(\StmZendMailMessageImap $message)
    {
//        return nl2br(quoted_printable_decode($message->getMessageBodyText()));
    }
}