<?php
/**
 * Namespace
 */
namespace StmLeads\Data;

/**
 * Base
 *
 * Base class for data
 * @package StmLeads\Data
 */
abstract class Base implements \ArrayAccess, \Iterator
{
    /**
     * Name Value Data
     *
     * Holds dynamic properties
     * @var array used for magic getters and setters to handle form fields dynamically
     */
    protected $_nameValueData = array();


    protected $_idValueData = array();

    /**
     * Format Value
     *
     * Formats a value according to it's key
     * @param $name The key
     * @param $value The value
     * @return string  The formatted value
     */
    protected function formatValue($name, $value)
    {
        // Handle different types of formatting
        switch($name) {
            case 'address':
            case 'city':
                return ucwords(strtolower($value));
            break;
            case 'email':
                return str_replace(' ', '', $value);
                break;
            case 'phone':
                return preg_replace('/[^0-9]/', '', $value);
            default:
                return $value;
            break;
        }
    }

    /**
     * Set
     *
     * Set field property dynamically
     * @param string $name Key
     * @param string $value Value
     */
    public function __set($name, $value)
    {
        $this->_nameValueData[$name] = $this->formatValue($name, $value);
    }

    /**
     * Get
     *
     * Get field property dynamically
     * @param  string $name
     * @return string The array value
     */
    public function __get($name)
    {
        return isset($this->_nameValueData[$name]) ? $this->_nameValueData[$name] : null;
    }

    /**
     * Offset Exists
     *
     * From ArrayAccess, used to determine whether or not an offset exists
     * @param string $offset The array key
     * @return bool True when offset exists, false otherwise
     */
    public function offsetExists($offset) {
        return isset($this->_nameValueData[$offset]);
    }

    /**
     * Offset Get
     *
     * Returns the value for the given offset
     * @param string $offset Array key to retrieve data by
     * @return mixed|null String value when exists, null otherwise
     */
    public function offsetGet($offset) {
        return $this->offsetExists($offset) ? $this->_nameValueData[$offset] : null;
    }

    /**
     * Offset Set
     *
     * Sets an offsets value
     * @param string $offset The key
     * @param string $value The value
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_nameValueData[] = $value;
        } else {
            $this->_nameValueData[$offset] = $this->formatValue($offset, $value);
        }
    }

    /**
     * Offset Unset
     *
     * Removes a value
     * @param string $offset Key to remove
     */
    public function offsetUnset($offset) {
        if ($this->offsetExists($offset)) {
            unset($this->_nameValueData[$offset]);
        }
    }

    /**
     * Rewind
     *
     * Takes us back to the start of the array
     * @return void
     */
    function rewind() {
        reset($this->_nameValueData);
    }

    /**
     * Current
     *
     * Returns the current item in the array
     * @return string Array value
     */
    function current() {
        return current($this->_nameValueData);
    }

    /**
     * Key
     *
     * Retrieves the key
     * @return mixed
     */
    function key() {
        return key($this->_nameValueData);
    }

    /**
     * Next
     *
     * Moves the position to the next item in the array
     * @return void
     */
    function next() {
        next($this->_nameValueData);
    }

    /**
     * Valid
     *
     * Determines whether or not an array key is valid
     * @return bool True when valid, false otherwise
     */
    function valid() {
        return key($this->_nameValueData) !== null;
    }

    /**
     * Get Form ID Value Data
     *
     * Retrieves data with form field IDs for keys
     * @return array Formatted array
     */
    public function getFormIdValueData()
    {
        if(empty($this->_idValueData)) {
            foreach($this->_nameValueData as $name => $value) {
                $this->_idValueData{\FormFields::getFieldIdByName($name)} = $value;
            }
        }

        return $this->_idValueData;
    }
}