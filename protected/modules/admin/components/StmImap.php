<?php
//Yii::import('admin_exts.EIMap.EIMap', true);
Yii::import('admin_module.components.StmEIMap', true);

/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 4/11/13
 */
class StmImap extends CApplicationComponent {

	// Connection details, defined in the main configuration
	public $connectionString;
	public $userName;
	public $password;

	// Result of a successful imap_open() call
	protected $mailBox;

    /**
     * Imap Object reference
     *
     * @var StmEImap
     */
    private $_imap;

	/**
	 * init
	 * Preprocessing before any method is called within this component
	 * @author Chris Willard <chriswillard.dev@gmail.com>
	 * @since 4/14/13
	 */
	public function init() {

		if ($this->connectionString && $this->userName && $this->password)
			$this->open($this->connectionString, $this->userName, $this->password);

		return parent::init();
	}

    /**
     * open
     * Establishes a connection with a imap server
     * @author Chris Willard <chriswillard.dev@gmail.com>
     * @since 4/14/13
     * @param $connectionString
     * @param $userName
     * @param $password
     * @return resource
     */
	public function open($connectionString, $userName, $password) {

        $this->_imap = new StmEImap($connectionString, $userName, $password);
        return $this->_imap->connect();
	}

    /**
     * changeUser
     * Uses the existing connection string and changes the user info
     * @param $userName
     * @param $password
     * @since 5/28/13
     * @return resource
     */
    public function changeUser($userName, $password) {
        return $this->open($this->connectionString, $userName, $password);
    }

	/**
	 * searchMessages
	 * Searches the provided mailbox connection by multiple types of criteria
	 * @author Chris Willard <chriswillard.dev@gmail.com>
	 * @since 4/14/13
	 * @return collection of message ids {@see getMessageBody for usage with message id}
	 */
	public function searchMessages($criteria) {
        return $this->_imap->searchmails(StmEImap::SEARCH_ALL);
	}

    /**
     * getMessageBody
     * Returns the text only version of the message body
     * http://us.php.net/imap_fetchstructure
     * http://www.php.net/manual/en/function.imap-fetchbody.php
     *
     * @author Chris Willard <chriswillard.dev@gmail.com>
     * @since  4/14/13
     *
     * @param        $messageId
     * @param string $section
     *
     * @return string
     */
	public function getMessageBody($messageId)
    {
        $mail = $this->_imap->getMail($messageId);
        return $mail->textHtmlOriginal ? $mail->textHtmlOriginal : $mail->textPlain;
	}

    public function getMessage($messageId) {

        return $this->_imap->getMail($messageId);
    }
	/**
	 * getMessageHeaders
	 * Returns a object of type stdClass, containing all of the headers for a e-mail
	 * @author Chris Willard <chriswillard.dev@gmail.com>
	 * @since 4/14/13
	 * @see http://www.php.net/manual/en/function.imap-headerinfo.php
	 * @param $messageId
	 * @return object
	 */
//	public function getMessageHeaders($messageId) {
//
//		return imap_headerinfo($this->mailBox, $messageId);
//	}

	/**
	 * markMessageAsRead
	 * Marks a message as "Seen"
	 * @author Chris Willard <chriswillard.dev@gmail.com>
	 * @since 4/14/13
	 * @param $messageId
	 * @return bool
	 */
//	public function markMessageAsRead($messageId) {
//		return imap_setflag_full($this->mailBox, $messageId, "\\Seen");
//	}

	/**
	 * unmarkMessageAsRead
	 * Unmarks a message as "Seen"
	 * @author Chris Willard <chriswillard.dev@gmail.com>
	 * @since 4/14/13
	 * @param $messageId
	 * @return bool
	 */
//	public function unmarkMessageAsRead($messageId) {
//		return imap_clearflag_full($this->mailBox, $messageId, "\\Seen");
//	}


    /**
     * Imap Mail Move
     *
     * Moves a message from one mailbox (imap folder) to another
     * @param $msgId string The message ID (or message id specification)
     * @param $destination The destination mailbox (imap folder)
     * @return bool
     */
    public function imapMailMove($msgId, $destination)
    {
        return $this->_imap->imapMailMove($msgId, $destination);
    }

    /**
     * Imap List
     *
     * Lists mailboxes for an account (imap folders)
     * @param $pattern string The pattern to check against
     * @return array List of mailboxes (imap folders)
     */
    public function imapList($pattern)
    {
        return $this->_imap->imapList($pattern);
    }

    /**
     * Imap header
     *
     * Retrieves headers for a given message
     * @param $msgId The message ID to retrieve headers for
     * @return bool|void
     */
    public function imapHeader($msgId)
    {
        return $this->_imap->getMailHeader($msgId);
    }

    /**
     * Change Mailbox
     *
     * Used to switch to another mailbox (imap folder)
     * @param $mailbox String the mailbox to switch to
     * @return bool True on success, false otherwise
     */
    public function changeMailbox($mailbox)
    {
        return $this->_imap->changeMailbox($mailbox);
    }

    /**
     * Imap Close
     *
     * Used to close an IMAP connection
     */
    public function imapClose()
    {
        return $this->_imap->close();
    }

    /**
     * Imap Search
     *
     * Used to search messages in the current mailbox
     * @param $criteria search criteria (see http://www.php.net//manual/en/function.imap-search.php for details)
     * @return array List of message numbers or UIDs
     */
    public function imapSearch($criteria)
    {
        return $this->_imap->searchMails($criteria);
    }

    public function ensureMailboxExists($mailbox)
    {
        return $this->_imap->ensureMailboxExists($mailbox);
    }
}