<?php
	Yii::import('admin_module.extensions.UASParser');

	class StmUserAgentParser extends CApplicationComponent {

		public function getInfo() {
			$parser = new UASParser();
			$parser->SetCacheDir(sys_get_temp_dir() . "/uascache/");

			$useragent = 'unknown';
			if (isset($_SERVER['HTTP_USER_AGENT'])) {
				$useragent = $_SERVER['HTTP_USER_AGENT'];
			}

			return $parser->Parse($useragent);
		}
	}