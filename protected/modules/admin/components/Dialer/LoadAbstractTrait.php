<?php

/**
 * Load Abstract Trait
 *
 * Commonly used stuff
 */
trait LoadAbstractTrait
{
    /**
     * Load Action
     *
     * Returns JSON formatted data for DataTable
     * @return void
     */
    public function actionLoad()
    {
        // Call load helper
        $results = $this->_loadHelper(
            Yii::app()->request->getParam('callListId'),
            Yii::app()->request->getParam('presetId'),
            Yii::app()->request->getParam('userId') ? Yii::app()->request->getParam('userId') : Yii::app()->user->id,
            Yii::app()->request->getParam('callsession'),
            $_POST['eventType'],
            Yii::app()->request->getParam('maxDialerCallCount'),
            Yii::app()->request->getParam('maxDialerCallDays'),
            Yii::app()->request->getParam('initialize')
        );

        // Encode and send JSON data
        $this->_sendJsonForLoad($results);
    }

    /**
     * Load Helper
     *
     * Helper to perform load operations
     * @param $callListId
     * @param $presetId
     * @param $currentUser
     * @param $callSessionId
     * @param $eventType
     * @param $maxDialerCallCount
     * @param $maxDialerCallDays
     * @throws Exception When things go horribly wrong
     * @return array Load data
     */
    protected function _loadHelper($callListId, $presetId, $currentUser, $callSessionId, $eventType, $maxDialerCallCount, $maxDialerCallDays, $initialize=null)
    {
        // Filters
        $filters = array();
        $filters['maxDialerCallCount'] = $maxDialerCallCount;
        $filters['maxDialerCallDays'] = $maxDialerCallDays;

        if(!$eventType) {
            // if have user session but no eventType specified set to admin
            $eventType = (Yii::app()->user->id) ? 'admin' : $eventType;
        }

        if(!$presetId) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Preset ID is required.');
        }

        if($callListId) {
            $callList = \CallLists::model()->findByPk($callListId);
            $presetId = $callList->preset_ma;
        }

        if(!$callList) {
            // Retrieve call list
            /** @var CallLists $callList */
            $callList = \CallLists::getCallListByPresetId($presetId, $filters);

        }

        // Ensure we have a call list id set
        if(!$callListId) {
            $callListId = $callList->id;
        }

//        if(!$id && !$presetId) {
//            throw new Exception(__CLASS__.'(:'.__LINE__.') Dialer load missing Call List ID and Preset ID. At least one is required.');
//        }

//        if($id && $id !='undefined' && !is_numeric($id)) {
//            throw new Exception(__CLASS__.'(:'.__LINE__.') Invalid Call List ID.');
//        }

        if($presetId && !is_numeric($presetId)) {
            throw new Exception(__CLASS__.'(:'.__LINE__.') Invalid Preset ID.');
        }

        // Use new adapter pattern to get a list instance
        $o = \StmDialer\Lists\Factory::getInstance($presetId, $callListId);
        $id = $o->getCallList()->id;

        // In this initialize, checks to see if current user has any sessions active today. If not any "Ringing" call_list_phones records that belong to this user are set to "No Answer"
        if($initialize == 1) {

            // check to see if current user has any open sessions, if not update all Ringing, Answered status to No Answer.
            if(!CallSessions::model()->hasActiveSession(Yii::app()->user->id)) {

                // this can be reused in the actual update and error reporting if set to true. Prevents condition from going out of sync.
                $ringingCallListCondition = "l.preset_ma={$presetId} AND c.status IN ('Ringing','Answered') AND c.updated_by={$currentUser} AND (l.contact_id IS NULL OR l.contact_id={$currentUser})";

                // query so "Ringing" can be reported
                if($ringingIds = Yii::app()->db->createCommand("select c.*, l.id call_list_id, l.preset_ma, l.contact_id, l.name FROM call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id WHERE {$ringingCallListCondition}")->queryAll()) {

                    // set the ringing Id's to No Answer
                    Yii::app()->db->createCommand("UPDATE call_list_phones c LEFT JOIN call_lists l ON c.call_list_id=l.id SET status='No Answer' WHERE {$ringingCallListCondition}")->execute();

                    // send email of list of erroneously "Ringing" call_list_phone Id's
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') URGENT Error: Initial Load found Ringing phone # without an active Call Session. Changing to "No Answer".'.PHP_EOL.'You can update the flag above this line to disable this reporting.'.PHP_EOL.'Call List Phone Id #: '.print_r($ringingIds, true), CLogger::LEVEL_ERROR);
                }
            }

            // re-queues any call_list_phones that are ready to return to the queue based on time passed and list type
            $o->reQueueBySortOrder($id, Yii::app()->user->id, $filters);
        }

        // Retrieve current call session
//        $inProgress = false;

        // Retrieve all active sessions for call list
//        $inProgress = array();
        $activeSessionUserIds = array();
        $activeSessions = CallSessions::model()->getActiveSessionsByCallListId($callList->id);
        foreach($activeSessions as $activeSession) {
            if(empty($activeSession->end)) {
                $activeSessionUserIds[$activeSession->contact_id] = $activeSession->id;
            }
        }

//        if($callSessionId) {
//            // get the latest active session
//            if(!($callSession = CallSessions::model()->hasActiveSession(Yii::app()->user->id, true))) {
//                // fall back to provided sessionId
//                $callSession = CallSessions::model()->findByPk($callSessionId);
//            }
//            else {
//                $callSessionId = $callSession->id;
//            }
//
//            // if session is available, should go off of what is saved. This prevents user from changing mid-session. Form elements should be disabled, but just in case.
//            if($callSession->filter_max_call_count) {
//                $filters['maxDialerCallCount'] = $callSession->filter_max_call_count;
//            }
//            if($callSession->filter_max_call_days) {
//                $filters['maxDialerCallDays'] = $callSession->filter_max_call_days;
//            }
//
//            if($callSession && empty($callSession->end)) {
//                $inProgress = true;
//            }
//        }

        // Get phone numbers to call
        $callListPhones = $o->getQueue2($id, $filters);

        // Get in progress call, if we have one
        $active = array();
        foreach($callListPhones as $callListPhone) {
            if($callListPhone['status'] == 'Answered') {
                // if "Answered call does not have a active session, set the call as "No Answer" & email error log it. Ignore if action is hangupagent as user is hanging up making final load request
                if(!isset($activeSessionUserIds[$callListPhone['updated_by']])/* && Yii::app()->action->id != 'hangupagent'*/) {
                    // get data for call_list_phone before updating it to "No Answer"
                    $callListPhoneMissingSession = Yii::app()->db->createCommand("select * from  call_list_phones WHERE id={$callListPhone['call_list_phone_id']}")->queryRow();
                    Yii::app()->db->createCommand("UPDATE call_list_phones SET `status`='".CallListPhones::NO_ANSWER."' WHERE id={$callListPhone['call_list_phone_id']}")->execute();

                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') **** URGENT Error****'.PHP_EOL.'A call_list_phone record is marked as Answered without a call session ID. Marking this call as No Answer. Please investigate. As of 7/23/15 - Remove this log when ready. Call List Phone ID: '.$callListPhone['call_list_phone_id'].' | Call List ID#: '.$id.' | Phone#: '.$callListPhone['phone'].' | Call List Phone Record: '.print_r($callListPhoneMissingSession, true).' | Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                $callId = Calls::getIdByAnsweredCallListPhoneId($callListPhone['call_list_phone_id'], $callSessionId);

                if(!$callId) { // && !YII_DEBUG
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find CallID.  Please investigate. '.PHP_EOL
                        .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                        .'Call Session ID: '.$callSessionId.PHP_EOL
                        .'Call List ID#: '.$id.PHP_EOL
                        .'Phone#: '.$callListPhone['phone'].PHP_EOL
                        .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                // Get phone record
                $phone = Phones::model()->skipSoftDeleteCheck()->findByPk($callListPhone['phone_id']);

                if(!$phone) { // && !YII_DEBUG
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: "Answered" status did not find Phone model.  Please investigate. '.PHP_EOL
                        .'Call List Phone ID: '.$callListPhone['call_list_phone_id'].PHP_EOL
                        .'Call Session ID: '.$callSessionId.PHP_EOL
                        .'Call List ID#: '.$id.PHP_EOL
                        .'Phone#: '.$callListPhone['phone'].PHP_EOL
                        .'Call List Data from getCallsByList(): '.print_r($callListPhone, true), CLogger::LEVEL_ERROR);
                    continue;
                }

                // Get the associated contact
                $contact = $phone->contact;

                $command = Yii::app()->db->createCommand()
                    ->select('a.id')
                    ->from('activity_log a')
                    ->where('a.call_id='.$callId)
                    ->order('a.id DESC');

                $answeredActivityLogIds = $command->queryAll();

                // get the activity log that is marked as Answered for this call to pass to GUI as it may be a voicemail
                // @todo: one can not be found between the lag of being answered and marking as voicemail??? - keep an eye on this, watch error log emails
                if(!$answeredActivityLogIds) {

                    // log error, no activity log id was found
                    Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: No activity log record was found for an answered call. Call ID#: '.$callId.' Call List ID#: '.$id.' Call List Phone ID#: '.$callListPhone['call_list_phone_id'].' Query: '.$command->getText(), CLogger::LEVEL_ERROR);

                    //@todo: this would indicated call_list_phone with status of Answered BUT may be "stuck" and really hung up. Possibly need to hang up and send error log note FYI untill fully fixed?
                    //@todo: this may also be caused by lag between answer and updating activity log. Can check this by last_updated and now being more than 5 seconds apart?
                    // check to see if it is linked to current call session, if not it's a leftover "Answered" bug caused by a different session
                }
                else {
                    $answeredActivityLogId = $answeredActivityLogIds[0];

                    // there should only be one result, but
                    if(count($answeredActivityLogIds)>1) {

                        $activityLogsAttributesToReport = array();
                        foreach($answeredActivityLogIds as $answeredActivityLogIdToReport) {
                            $activityLogsAttributesToReport[] = $answeredActivityLogIdToReport->attributes;
                        }

                        // error in finding more than 1 activity_log entry. Check query logic to narrow down more effectively
                        Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error: More than activity log was found. Query needs to be narrowed down more or a call status of Answered is left in that status unnecessarily. Please investigate immediately. Call ID#: '.$callId.' Call List ID#: '.$id.PHP_EOL.'All Activity Log Attributes: '.print_r($activityLogsAttributesToReport, true).' Query: '.$command->getText(), CLogger::LEVEL_ERROR);
                    }
                }

                $componentType = ComponentTypes::model()->findByPk($callListPhone['component_type_id']);

                $callListIds = Yii::app()->db->createCommand()
                    ->select('clp.call_list_id id, l.name name, CONCAT("'.$phone->id.'") as phone_id, '.$callListPhone['component_type_id'].' component_type_id, '.$callListPhone['component_id'].' component_id')
                    ->from('call_list_phones clp')
                    ->join('call_lists l', 'l.id=clp.call_list_id')
                    ->where('clp.phone_id='.$phone->id)
                    ->andWhere('clp.is_deleted=0')
                    ->group('clp.call_list_id')
                    ->queryAll();

                // Mode data for active content
                $active[$callListPhone['updated_by']] = array(
                    'call_id'           =>  $callId,
                    'call_list_ids'       => $callListIds,
                    'call_list_phone_id'=>  $callListPhone['call_list_phone_id'],
//                    'source'            =>  $callListPhone['source'], //@todo: test on task based first to make sure the blank doesn't error out
                    'activity_log_id'   =>  $answeredActivityLogId['id'],
                    'phone_id'          =>  $phone->id,
                    'first_name'        =>  $contact->first_name,
                    'last_name'         =>  $contact->last_name,
                    'spouse_first_name' =>  ($contact->spouse_first_name)? ' & '.$contact->spouse_first_name : '',
                    'spouse_last_name'  =>  $contact->spouse_last_name,
                    'address'           =>  $contact->primaryAddress->address,
                    'city'              =>  $contact->primaryAddress->city,
                    'state'             =>  AddressStates::getShortNameById($contact->primaryAddress->state_id),
                    'zipcode'           =>  $contact->primaryAddress->zip,
                    'phone'             =>  $phone->phone,
                    'task_id'           =>  $callListPhone['task_id'],
                    'description'       =>  $callListPhone['description'],
                    'component_id'      =>  $callListPhone['component_id'],
                    'component_type_id' =>  $callListPhone['component_type_id'],
                    'component_name'    =>  $componentType->name,
                    'component_label'   =>  $componentType->getSingularName(),
                );
            }
        }

        // Return load data
        return array(
            'status'    =>  'success',
            'results'   =>  $callListPhones,
            'meta'	=>	array(
                'event_type'                 =>  $eventType,
//                'in_progress'               =>  (empty($activeSessionUserIds)) ? null : $inProgress, //@todo: this need to match plivo3.js structure, wasn't passing via node so need to figure out - CLee 12/6/2015, should this return null if empty or let handle on GUI side?
                'session_id'                =>  (empty($activeSessionUserIds)) ? null : $activeSessionUserIds, //@todo: FYI: This passes all session for this list, that's what NodeAPI does - CLee 12/6/2015
                'active'                    =>  (empty($active)) ? null : $active,
            )
        );
    }

    /**
     * Send JSON For Load
     *
     * Outputs JSON data and terminates application
     * @param array $data Array of data
     */
    protected function _sendJsonForLoad($data)
    {
        // Set header and output JSON data
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // Terminate application
        Yii::app()->end();
    }
}