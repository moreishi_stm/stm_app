<?php
abstract class MlsBaseActiveRecord extends CActiveRecord
{
    private static $_models = array();

    protected $tableName;

    private $_boardName;

    protected static $_md = array(); // meta data

    public function __construct($tableName, $scenario = 'insert')
    {
        $this->tableName  = $tableName;
        $tableParts       = explode('_', $this->tableName);
        $this->_boardName = $tableParts[0] . '_' . $tableParts[1];

        parent::__construct($scenario);
    }

    protected function getBoardName()
    {
        return $this->_boardName;
    }

    public function tableName()
    {
        $childClass = get_class($this);
        return $this->_boardName . $childClass::getTableSuffix();
    }

    /**
     * Ensure that the 'db' component is set to active
     *
     * @return CDbConnection
     */
    public function getDbConnection()
    {

        self::$db = Yii::app()->stm_mls;

        if (self::$db instanceof CDbConnection) {
            self::$db->setActive(true);

            return self::$db;
        }
    }

    public function getMetaData()
    {
        $className=get_class($this);
        if(!array_key_exists($className,self::$_md))
        {
            self::$_md[$className]=null; // preventing recursive invokes of {@link getMetaData()} via {@link __get()}
            self::$_md[$className]=new CActiveRecordMetaData($this);
        }
        return self::$_md[$className];
    }

    /**
     * Overriding the base model behavior from CActiveRecord so we can pass a tableName, either through the constructor or through a static model call
     * You can create a model instance like: MlsProperties::model('fl_nefar')->findByPk(2)
     *
     * @see http://www.yiiframework.com/doc/api/1.1/CActiveRecord#model-detail
     */
    public static function model($boardName = null, $className = __CLASS__)
    {

        $tableName = $boardName . $className::getTableSuffix();
        if (($tableName === null) && empty(self::$_models[$className])) {
            throw new CHttpException(500, 'Could not load model.');
        }

        $model = null;

        if (isset(self::$_models[$className])) {
            $model = self::$_models[$className];
        } else {
            $model      = self::$_models[$className] = new $className($tableName);
            $model->_md = new CActiveRecordMetaData($model);
            $model->attachBehaviors($model->behaviors());
        }

        return $model;
    }

    /**
     * Refreshes the meta data for this AR class.
     * By calling this method, this AR class will regenerate the meta data needed.
     * This is useful if the table schema has been changed and you want to use the latest
     * available table schema. Make sure you have called {@link CDbSchema::refresh}
     * before you call this method. Otherwise, old table schema data will still be used.
     */
    public function refreshMetaData()
    {
        $finder      = self::model($this->_boardName, get_class($this));
        $finder->_md = new CActiveRecordMetaData($finder);
        if (self::$_md !== $finder) {
            self::$_md = array();
        }
    }

    /**
     * This method is used by the populateRecord and populateRecords.  We override this functionality to provide the dynamic table name.
     *
     * @see http://www.yiiframework.com/doc/api/1.1/CActiveRecord#instantiate-detail
     */
    protected function instantiate($attributes)
    {

        $class = get_class($this);
        $model = new $class($this->tableName, null);

        return $model;
    }
}
