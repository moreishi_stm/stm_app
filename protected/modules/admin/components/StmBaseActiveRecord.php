<?php

/**
 * StmBaseActiveRecord
 * All models extend this one
 *
 * @version 0.1 Beta
 * @since 08/14/2012
 * @author Chris Willard <chris@seizethemarket.com>
 * @extends CActiveRecord
 */
class StmBaseActiveRecord extends CActiveRecord {

	public $applyAccountScope = true;
	public $deleteColumn    = 'is_deleted';
	public $accountIdColumn = 'account_id';

	private $_skipSoftDeleteCheck = false;
	private static $_events = array();

	/**
	 * Ensure that the 'db' component is set to active
	 * @return CDbConnection
	 */
	public function getDbConnection() {
		self::$db = Yii::app()->db;

		if (self::$db instanceof CDbConnection) {
			self::$db->setActive(true);
			return self::$db;
		}
	}
    /**
     * @author Christine Lee <christine@seizethemarket.com>
     * @since  1.0
     * @param $ignoreAccountScope
     */
    public function byIgnoreAccountScope() {
        $this->applyAccountScope = false;
        return $this;
    }

    public function byApplyAccountScope() {
        $this->applyAccountScope = true;
        return $this;
    }

	/**
	 * Allows us to skip the soft delete check
	 * @internal Has to be the first part of the criteria, before it's built
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @return $this
	 */
	public function skipSoftDeleteCheck() {
		$this->setSoftDeleteCheck(true);
		return $this;
	}

	/**
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @param $shouldCheckSoftDelete
	 */
	public function setSoftDeleteCheck($shouldCheckSoftDelete) {
		$this->_skipSoftDeleteCheck = $shouldCheckSoftDelete;
	}

	/**
	 * @author Chris Willard <chris@seizethemarket.com>
	 * @since  1.0
	 * @return bool
	 */
	public function getSoftDeleteCheck() {
		return $this->_skipSoftDeleteCheck;
	}

	public function defaultScope() {

		$defaultScope = array();
		$tableAlias   = $this->getTableAlias(true, false);

		// Ignore the soft delete for the default scope if the query is already checking the column for something
		if ($this->hasAttribute($this->deleteColumn) && $this->getSoftDeleteCheck() === false) {
			$defaultScope['condition'] = "({$tableAlias}.{$this->deleteColumn} = 0 or {$tableAlias}.{$this->deleteColumn} IS NULL)";
		}

		// These scopes should not be applied when running under a cli environment
		if (!$this->isCliEnv) {
			if ($this->applyAccountScope && Yii::app()->user->getAccountId() && $this->hasAttribute($this->accountIdColumn)) { //Yii::app()->user->contact->account_id
				$concat = ( $defaultScope['condition'] ) ? ' AND' : '';
				$defaultScope['condition'] .= "{$concat} ({$tableAlias}.{$this->accountIdColumn} = :account_id) OR ({$tableAlias}.{$this->accountIdColumn} = :account_id_global)";
				$defaultScope['params'] = array(':account_id'=>Yii::app()->user->getAccountId(), ':account_id_global'=>0);
			}
		}

		return $defaultScope;
	}

	/**
	 * Returns an instance of the child model based on the child's class name.
	 *
	 * @param string $className
	 * @return CActiveRecord
	 */
	public static function model($className = '') {
		$parentClassName = get_called_class();
	    return parent::model($parentClassName);
	}

	/**
	 * When a model is saved, the account_id will be automatically filled in if empty
	 *
	 * @return boolean
	 */
	protected function beforeValidate() {
		// Only apply this to models that have the soft delete column
		if ($this->hasAttribute($this->accountIdColumn) && empty($this->{$this->accountIdColumn})) {
			$this->{$this->accountIdColumn} = Yii::app()->user->accountId;
		}
		return parent::beforeValidate();
	}

	/**
	 * When a delete call is made on a model inherited from this one, only update the deleted column value
     *
	 * @return boolean
	 */
    protected function beforeDelete() {
    	// Only apply this to models that have the soft delete column
    	if ($this->hasAttribute($this->deleteColumn)) {
	        $this->{$this->deleteColumn} = 1;
	        $this->save(false); // no need to validate the model here

	        //prevent real deletion
	        return false;
    	} else {
            return true;
        }
    }

	/**
	 * Provide a way to limit all active record models
	 * @param int $limit
	 * @return $this
	 */
	public function byLimit($limit=50) {
		$this->getDbCriteria()->mergeWith(array(
			'limit' => $limit,
		));

		return $this;
	}

    /**
     * Determine if the environment is running via the cli.
     * @return boolean
     */
    public function getIsCliEnv() {
    	return (defined('STDIN')) ? true : false;
    }

	/**
	 * Attach exists events while model creation
	 */
	public function init()
	{
		$this->attachEvents($this->events());
	}

	/**
	 * Attach events
	 *
	 * @param array $events
	 */
	public function attachEvents($events)
	{
		foreach ($events as $event) {
			if ($event['component'] == get_class($this))
				parent::attachEventHandler($event['name'], $event['handler']);
		}
	}

	/**
	 * Get exists events
	 *
	 * @return array
	 */
	public function events()
	{
		return self::$_events;
	}

	/**
	 * Attach event handler
	 *
	 * @param string $name Event name
	 * @param mixed $handler Event handler
	 */
	public function attachEventHandler($name,$handler)
	{
		self::$_events[] = array(
			'component' => get_class($this),
			'name' => $name,
			'handler' => $handler
		);
		parent::attachEventHandler($name, $handler);
	}

	/**
	 * Dettach event hander
	 *
	 * @param string $name Event name
	 * @param mixed $handler Event handler
	 * @return bool
	 */
	public function detachEventHandler($name,$handler)
	{
		foreach (self::$_events as $index => $event) {
			if ($event['name'] == $name && $event['handler'] == $handler)
				unset(self::$_events[$index]);
		}
		return parent::detachEventHandler($name, $handler);
	}
}