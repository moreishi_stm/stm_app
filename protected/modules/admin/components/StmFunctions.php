<?php

class StmFunctions extends CApplicationComponent
{
    public static function getVisitorIp()
    {
        return empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];
    }

    /**
     * Get Site URL
     *
     * Function to return the site URL, as executed. Does not include query string or path infromation.
     * @return string Current working URL of the site
     */
    public static function getSiteUrl()
    {
        return (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . (in_array($_SERVER['SERVER_PORT'], array('80', '443')) ? '' : ':' . $_SERVER['SERVER_PORT']);
    }

    /**
     * Db Switcher Helper
     *
     * Helper function to switch the database
     * @param $clientId
     * @param $accountId
     * @param bool $clearComponent
     * @param string $dbClass
     * @return bool
     * @throws Exception
     */
    protected static function _dbSwitcherHelper($clientId, $accountId, $clearComponent=true, $dbClass = 'application.components.RepairableDbConnection')
    {
        // clears out component
        if($clearComponent) {
            Yii::app()->setComponent('db', null);
        }

        // Attempt to lookup the client by the ID, if we don't have one, stop right here
        $client = Clients::model()->findByPk($clientId);
        if (!$client instanceof Clients) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Client not found by ID#: '.$clientId);
        }

        // get time zone - *************** NOTE: ASSUMES all client dbs are on same server. If the client db's are ever separated, this query must be modified. ***************
        $timezone = Yii::app()->db->createCommand("select `timezone` from `".$client->db_name."`.accounts WHERE id={$accountId}")->queryScalar();

        if (!$timezone) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Timezone not found. Client DB Name: '.$client->db_name.', Account ID: '.$accountId);
        }

        // Attempt to configure db connection
        try {

            // getClientDbConnection FUNCTION
            $host = (YII_DEBUG) ? 'localhost': STM_DB_HOST;

            // Configuration
            $config = array(
                'connectionString' => strtr('mysql:host='.$host.';dbname={database}', array('{database}' => $client->db_name)),
//                    'class' => 'application.components.RepairableDbConnection',
                'class' =>  $dbClass,
                'emulatePrepare' => true,
                'username' => Yii::app()->db->username,
                'password' => Yii::app()->db->password,
                'charset' => 'utf8',
                'enableProfiling' => false,
                'enableParamLogging' => false,
                'initSQLs'=> array(
                    "SET time_zone = '" . $timezone . "';",
                )
            );

            // Create component
            $clientDbConn = Yii::createComponent($config);

            Yii::app()->setComponent('db', $clientDbConn);
        }
        catch (CDbException $e) {
            Yii::log(__CLASS__ . ' (:'. __LINE__ .') Could not set DB by Client ID & Account ID. Code: "' . $e->getCode() . '" '.PHP_EOL . 'Exception Message:' . $e->getMessage(), CLogger::LEVEL_ERROR);
            Yii::app()->end();
        }

        // check to see that the connection is active and that the correct db_name is connected.
        if(!Yii::app()->db->getActive() || strpos(Yii::app()->db->connectionString, $client->db_name) == false) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Database connection is not active for Client ID#: '.$clientId.' Account ID# '.$accountId.'.');
        }

        return true;
    }

    /**
     * Connect Db By Client ID and Account ID
     *
     * Finds the right db and makes it the new Yii::app()->db connection. Throws exception with errors. This is a MAJOR reused core function with HUGE impact. Modify with extreme caution!!
     * @param $clientId
     * @param $accountId
     * @return mixed
     * @throws Exception
     */
    public static function connectDbByClientIdAccountId($clientId, $accountId, $clearComponent=true)
    {
        return self::_dbSwitcherHelper($clientId, $accountId, $clearComponent);
    }

    /**
     * Front Connect Db By Client ID Account ID
     *
     * Use this when you need to change
     * @param int $clientId The client ID
     * @param int $accountId The account ID
     * @param bool $clearComponent
     */
    public static function frontConnectDbByClientIdAccountId($clientId, $accountId, $clearComponent=true)
    {
        return self::_dbSwitcherHelper($clientId, $accountId, $clearComponent, 'CDbConnection');
    }

    /**
     * Translates a number to a short alhanumeric version
     *
     * Translated any number up to 9007199254740992
     * to a shorter version in letters e.g.:
     * 9007199254740989 --> PpQXn7COf
     *
     * specifiying the second argument true, it will
     * translate back e.g.:
     * PpQXn7COf --> 9007199254740989
     *
     * this function is based on any2dec && dec2any by
     * fragmer[at]mail[dot]ru
     * see: http://nl3.php.net/manual/en/function.base-convert.php#52450
     *
     * If you want the alphaID to be at least 3 letter long, use the
     * $pad_up = 3 argument
     *
     * In most cases this is better than totally random ID generators
     * because this can easily avoid duplicate ID's.
     * For example if you correlate the alpha ID to an auto incrementing ID
     * in your database, you're done.
     *
     * The reverse is done because it makes it slightly more cryptic,
     * but it also makes it easier to spread lots of IDs in different
     * directories on your filesystem. Example:
     * $part1 = substr($alpha_id,0,1);
     * $part2 = substr($alpha_id,1,1);
     * $part3 = substr($alpha_id,2,strlen($alpha_id));
     * $destindir = "/".$part1."/".$part2."/".$part3;
     * // by reversing, directories are more evenly spread out. The
     * // first 26 directories already occupy 26 main levels
     *
     * more info on limitation:
     * - http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/165372
     *
     * if you really need this for bigger numbers you probably have to look
     * at things like: http://theserverpages.com/php/manual/en/ref.bc.php
     * or: http://theserverpages.com/php/manual/en/ref.gmp.php
     * but I haven't really dugg into this. If you have more info on those
     * matters feel free to leave a comment.
     *
     * The following code block can be utilized by PEAR's Testing_DocTest
     * <code>
     * // Input //
     * $number_in = 2188847690240;
     * $alpha_in  = "SpQXn7Cb";
     *
     * // Execute //
     * $alpha_out  = alphaID($number_in, false, 8);
     * $number_out = alphaID($alpha_in, true, 8);
     *
     * if ($number_in != $number_out) {
     *	 echo "Conversion failure, ".$alpha_in." returns ".$number_out." instead of the ";
     *	 echo "desired: ".$number_in."\n";
     * }
     * if ($alpha_in != $alpha_out) {
     *	 echo "Conversion failure, ".$number_in." returns ".$alpha_out." instead of the ";
     *	 echo "desired: ".$alpha_in."\n";
     * }
     *
     * // Show //
     * echo $number_out." => ".$alpha_out."\n";
     * echo $alpha_in." => ".$number_out."\n";
     * echo alphaID(238328, false)." => ".alphaID(alphaID(238328, false), true)."\n";
     *
     * // expects:
     * // 2188847690240 => SpQXn7Cb
     * // SpQXn7Cb => 2188847690240
     * // aaab => 238328
     *
     * </code>
     *
     * @author	Kevin van Zonneveld <kevin@vanzonneveld.net>
     * @author	Simon Franz
     * @author	Deadfish
     * @author  SK83RJOSH
     * @copyright 2008 Kevin van Zonneveld (http://kevin.vanzonneveld.net)
     * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
     * @version   SVN: Release: $Id: alphaID.inc.php 344 2009-06-10 17:43:59Z kevin $
     * @link	  http://kevin.vanzonneveld.net/
     *
     * @param mixed   $in	  String or long input to translate
     * @param boolean $to_num  Reverses translation when true
     * @param mixed   $pad_up  Number or boolean padds the result up to a specified length
     * @param string  $pass_key Supplying a password makes it harder to calculate the original ID
     *
     * @return mixed string or long
     */
    const EMAIL_LINK_TRACKING_SALT = 'tRafrEYas5';
    const EMAIL_LINK_TRACKING_PADDING = 8;
    const EMAIL_LINK_TRACKING_ADD = 872610349;
    public static function alphaID($in, $to_num = false, $pad_up = false, $pass_key = null)
    {
        $out   =   '';
        $index = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $base  = strlen($index);

        if ($pass_key !== null) {
            // Although this function's purpose is to just make the
            // ID short - and not so much secure,
            // with this patch by Simon Franz (http://blog.snaky.org/)
            // you can optionally supply a password to make it harder
            // to calculate the corresponding numeric ID

            for ($n = 0; $n < strlen($index); $n++) {
                $i[] = substr($index, $n, 1);
            }

            $pass_hash = hash('sha256',$pass_key);
            $pass_hash = (strlen($pass_hash) < strlen($index) ? hash('sha512', $pass_key) : $pass_hash);

            for ($n = 0; $n < strlen($index); $n++) {
                $p[] =  substr($pass_hash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            $index = implode($i);
        }

        if ($to_num) {
            // Digital number  <<--  alphabet letter code
            $len = strlen($in) - 1;

            for ($t = $len; $t >= 0; $t--) {
                $bcp = bcpow($base, $len - $t);
                $out = $out + strpos($index, substr($in, $t, 1)) * $bcp;
            }

            if (is_numeric($pad_up)) {
                $pad_up--;

                if ($pad_up > 0) {
                    $out -= pow($base, $pad_up);
                }
            }
        } else {
            // Digital number  -->>  alphabet letter code
            if (is_numeric($pad_up)) {
                $pad_up--;

                if ($pad_up > 0) {
                    $in += pow($base, $pad_up);
                }
            }

            for ($t = ($in != 0 ? floor(log($in, $base)) : 0); $t >= 0; $t--) {
                $bcp = bcpow($base, $t);
                $a   = floor($in / $bcp) % $base;
                $out = $out . substr($index, $a, 1);
                $in  = $in - ($a * $bcp);
            }
        }

        return $out;
    }

    public static function click3Encode($data)
    {
        $data = $data + StmFunctions::EMAIL_LINK_TRACKING_ADD;
        return StmFunctions::alphaID($data, false, false, StmFunctions::EMAIL_LINK_TRACKING_SALT);
    }

    public static function click3Decode($data)
    {
        if(empty($data)) {
            return null;
        }
        $decodedData = StmFunctions::alphaID($data, true, false, StmFunctions::EMAIL_LINK_TRACKING_SALT);
        return ($decodedData - StmFunctions::EMAIL_LINK_TRACKING_ADD);
    }

    public static function getCanSpamFooter($domainName, $officeName, $officeAddress, $emailId)
    {
        $encodedEmailId = StmFunctions::click3Encode($emailId);
        return '<br><br><div style="text-align:center; font-size: 10px; padding-top: 25px;">'.$officeName.'<br>'.$officeAddress.'<br /><a href="http://www.'.$domainName.'/optout/'.$emailId.'/eeid/'.$encodedEmailId.'" style="color: #222; text-decoration: none;">Click here to opt-out from email communication.</a></div>';
        //We hope you found this message to be useful. However, if you'd rather not receive future e-mails of this sort from Amazon.com, please unsubscribe here
    }

    function validatePlivoSignature($uri, $post_params=array(), $signature, $auth_token) {
        ksort($post_params);
        foreach($post_params as $key => $value) {
            $uri .= "$key$value";
        }
        $generated_signature = base64_encode(hash_hmac("sha1",$uri, $auth_token, true));
        return $generated_signature == $signature;
    }

    public static function getAwsConfig()
    {
        return array(
            'key'       =>  'AKIAITXC4AAKGIAYACBA',
            'secret'    =>  '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'
        );
    }

    public static function getGoogleAddressData($address, $city, $state, $zip=null, $zipOnly=false)
    {
        // Get zip code from Google API as Trulia leaves that data out
        $ch = curl_init();

        // must use proxy, current server is block, just like craigslist
        if(!YII_DEBUG) {
            curl_setopt($ch, CURLOPT_PROXY, 'http://cgp.seizethemarket.net');
            curl_setopt($ch, CURLOPT_PROXYPORT, '8080');
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'stm:Kaqufr3y');
        }

        // Gets google maps api data based on address
        $address = ($zip && $zipOnly) ?  urlencode($zip) : urlencode($address.', '.$city.', '.$state);

        $curlUrl = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAIBeX3PikL5KOxZiREc7s1F1p6_egHOYo&sensor=true'; //key=AIzaSyAIBeX3PikL5KOxZiREc7s1F1p6_egHOYo& - via Lee@Christineleeteam.com
        //AIzaSyC6lQwVZbKvM4Aoy2auZxMZ2nmblxVptbw - admin@seizethemarket.com Web API Key || Server Api Key - AIzaSyC6lQwVZbKvM4Aoy2auZxMZ2nmblxVptbw

        curl_setopt($ch, CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $googleAddressInfo = curl_exec($ch);
        if($googleAddressInfo) {
            $googleAddressInfo = json_decode($googleAddressInfo, true);
            return $googleAddressInfo;
        }
        return false;
    }

    public static function formatChartDataToJson($data, $isString=true)
    {
        if(empty($data)) {
            return '[]';
        }
        $string = '';
        $i=0;
        foreach($data as $row) {
            $string .= ($string) ? ',' : '';
            $string .= ($isString) ? "[$i, '{$row}']" : "[$i, $row]";
            $i++;
        }
        return '['.$string.']';
    }

    /**
     * Takes 2 arrays and creates array of their percentages based on input. Element names must mirror each other. Loops through numerator data to create values. Any missing data will result in zero for value.
     * @param $numeratorData
     * @param $denominator
     *
     * @return array
     */
    public static function array2Percent($numeratorData, $denominator, $multiplyByHundred=true, $ignorePercentThreshold=null, $minCount=null)
    {
        $percentData = array();
        foreach($numeratorData as $i => $value) {
            $isValid = true;
            if($minCount !== null && $value < $minCount) {
                $percentData[$i] = 0;
                $isValid = false;
            }

            $percentValue = round(($multiplyByHundred ? 100 : 1) * (($value && $denominator[$i]) ? $value / $denominator[$i]: 0));
            if($isValid && $ignorePercentThreshold !== null && $percentValue >= $ignorePercentThreshold) {
                $percentData[$i] = 0;
                $isValid = false;
            }

            if($isValid && $multiplyByHundred) {
                $percentData[$i] = round((($value && $denominator[$i]) ? $value / $denominator[$i]: 0) * 100);
            }
        }
        return $percentData;
    }

    public static function arrayFiller($data, $startIndex, $endIndex, $blankFiller)
    {
        if(!is_numeric($startIndex)) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Start Index must be numeric.');
        }

        if(!is_numeric($endIndex)) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') End Index must be numeric.');
        }

        if(!is_numeric($endIndex)) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') End Index must be numeric.');
        }

        if($startIndex > $endIndex) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') End Index must be greater than Start Index.');
        }

        for($i=$startIndex; $i <= $endIndex; $i++) {
            $data[$i] = isset($data[$i]) ? $data[$i] : $blankFiller;
        }
        ksort($data);
        return $data;
    }

    public static function getWeekdayDates($startDate, $endDate)
    {
            $startYear = date('Y', strtotime($startDate));
            $startMonth = date('n', strtotime($startDate));
            $startMonthDay = date('j', strtotime($startDate));

            $endYear = date('Y', strtotime($endDate));
            $endMonth = date('n', strtotime($endDate));
            $endMonthDay = date('j', strtotime($endDate));

            $dates = array();

            // build array of month weekday numbers
            for($y=$startYear; $y<=$endYear; $y++) {

                // loop through all 12 months as long as its not the endYear OR  <= to the end month/year
                for($m=$startMonth; ($m<=$endMonth || $y < $endYear) && $m <= 12; $m++) {

                    $iStart = ($m == $startMonth && $y == $startYear) ? $startMonthDay : 1;
                    $iEnd = ($m == $endMonth && $y == $endYear) ? $endMonthDay : cal_days_in_month(CAL_GREGORIAN, $m, $y); //cal_days_in_month(CAL_GREGORIAN, $m, $y);
                    for($i=$iStart; $i<=$iEnd; $i++) {

                        $iDate = date("$y-").sprintf("%02d",$m).'-'.sprintf("%02d",$i);
                        // check to see if less than today AND weekday
                        $weekDayName = date('l', strtotime($iDate));
                        if($iDate < date('Y-m-d') && $weekDayName !== 'Sunday' && $weekDayName !== 'Saturday') {
                            $dates[] = $iDate;
                        }
                    }
                }
            }

        return $dates;
    }

    /**
     * Turns any array into a 1 dimensional array specifying which elements to use as key and element. If key is null, just push to array using default values.
     * @param $arrayData
     * @param $keyName
     * @param $elementName
     *
     * @return array
     */
    public static function to1DArray($arrayData, $keyName, $elementName)
    {
        $newArray = array();
        foreach($arrayData as $i => $row)
        {
            if($keyName) {
                $newArray[$row[$keyName]] = $row[$elementName];
            }
            else {
                $newArray[] = $row[$elementName];
            }
        }
        return $newArray;
    }

    /**
     * Grabs youtube key from url string
     * @param $videoSrcUrl
     *
     * @return null
     */
    public static function getYoutubeKey($videoSrcUrl)
    {
        // check to see if youtube.com or youtu.be is exists in string
        if(strpos($videoSrcUrl, 'youtube.com') == false && strpos($videoSrcUrl, 'youtu.be') == false) {
            return null;
        }

        $keyPatternQueue = array(
            '/.*v=([-[:word:]]*)/is',
            '/.*youtube.com\/embed\/([-[:word:]]*)/is',
            '/.*youtu.be\/embed\/([-[:word:]]*)/is',
            '/.*vi\/([-[:word:]]*)/is',
            '/.*youtu.be\/([-[:word:]]*)/is'
        );

        foreach($keyPatternQueue as $keyPattern) {
            preg_match($keyPattern, $videoSrcUrl, $keyMatches);

            // Return the first offset (the youtube key)
            if ($keyMatches[1])
                return $keyMatches[1];
        }

        return null;
    }

    /**
     * Removes weird known characters
     * @param $string
     */
    public static function removeWeirdCharacters($string)
    {
        return str_replace(array('â','€','‹'), '', $string);
    }

    /**
     * Get YouTube Video Play Thumbnail
     *
     * Creates a youtube image with the play button overlay if it doesn't already exist
     * @param string $youtubeKey The YouTube video key to generate the image for
     * @param bool $overwriteExisting if True, the image is overwrote if it's pre-existing, if false, we check to see if it exists first
     * @return string The resulting URL of the newly created image
     * @throws Exception When invalid input for YouTube key function argument is present
     */
    public static function getYouTubePlayThumbnail($youtubeKey, $width=null, $height=null, $overwriteExisting = false)
    {
        // If we didn't specify a youtube key, stop right here
        if(empty($youtubeKey)) {
            throw new Exception('Error, YouTube key is not set!');
        }

        /** @var \Aws\S3\S3Client $s3 */
        $s3 = \Aws\S3\S3Client::factory(\StmFunctions::getAwsConfig());

        require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmAws.S3')."/FileManagement.php");
        $bucket = \StmAws\S3\FileManagement::getBucketNameByType(\StmAws\S3\FileManagement::SITE_BUCKET_TYPE);

        // If this throws a certain exception, then the file doesn't already exist, if it doesn't exist, create it
        $imageExists = true;
        if(!$overwriteExisting) {
            try {

                // Check to see if we already have an image for this youtube key
                $s3->headObject(array(
                    'Bucket'    =>  $bucket,
                    'Key'       =>  'video-email/' . $youtubeKey . '.png'
                ));

            }
            catch(\Aws\S3\Exception\NoSuchKeyException $e) {
                $imageExists = false;
            }
        }

        // Do this if the image doesn't already exist, or if we want to overwrite regardless
        if(!$imageExists || $overwriteExisting) {

            // Initialize cURL
            $ch = curl_init('http://i1.ytimg.com/vi/' . $youtubeKey . '/mqdefault.jpg');//mqdefault.jpg or hqdefault

            // Set cURL options
            curl_setopt_array($ch, array(
                CURLOPT_HEADER          =>  0,
                CURLOPT_RETURNTRANSFER  =>  1,
                CURLOPT_BINARYTRANSFER  =>  1
            ));

            // Retrieve image blob
            $imageBlob = curl_exec($ch);

            // Close cURL handle
            curl_close($ch);

            // Fire up ImageMagick
            $i = new Imagick();
            $i->readimageblob($imageBlob);
            $i->setimageformat('png');
            if($width && $height) {
                $i->resizeImage($width, $height, imagick::FILTER_BOX, 0);
            }

            //grab the normal or small play button depending on image size
            $playButtonFileName = ($i->getimagewidth() > 300) ? 'video_email_play_button.png' : 'video_email_play_button_small.png';

            // Get the play button
            $response = $s3->getObject(array(
                    'Bucket'    =>  'cdn.seizethemarket.com',
                    'Key'       =>  'assets/images/'.$playButtonFileName
                ));

            // Get play button image blob
            $iPlayButton = new Imagick();
            $iPlayButton->readimageblob($response['Body']);

            // Overlay the play button on the image
            $i->compositeimage($iPlayButton, Imagick::COMPOSITE_DEFAULT, ($i->getimagewidth()) / 2 - ($iPlayButton->getimagewidth() / 2), ($i->getimageheight() / 2) - ($iPlayButton->getimageheight() / 2));
            $i->flattenimages();

            // Store newly created image in S3
            $s3->putObject(array(
                'Bucket'        =>  $bucket,
                'Key'           =>  'video-email/' . $youtubeKey . '.png',
                'Body'          =>  $i->getimageblob(),
                'ContentType'   =>  'image/png'
            ));
        }

        // Return the URL for the image
        return 'http://'.$bucket.'/video-email/' . $youtubeKey . '.png';
    }

    public static function getSizeFromBytes($bytes, $unit)
    {
        switch(strtolower($unit)) {
            case 'gb':
                $size = round($bytes / 1073741824, 2);
                break;

            case 'mb':
                $size = round($bytes / 1048576, 2);
                break;

            case 'kb':
                $size = round($bytes / 1024, 2);
                break;

            // defaults to returning in bytes
            default:
                $size = $bytes;
                break;
        }

        return $bytes;
    }

    public static function emailMarketSnapshot($tnxId, $toEmail, $fromAgentId, $address, $city, $state, $zip, $toFirstName, $toLastName, $primaryDomainName, $boardId, $accountId)
    {
        if(!($toEmailModel = Emails::model()->findByAttributes(array('email'=> $toEmail)))) {
            throw new Exception(__CLASS__.' (:'.__LINE__.') Could not find Email for sending Market Snapshot');
        }

        $contact = $toEmailModel->contact;
        $agentContact = Contacts::model()->findByPk($fromAgentId);

        $httpDomain = 'http://www.' . $primaryDomainName;
        $boardName = MlsBoards::model()->findByPk($boardId)->getPrefixedName();

        // check to see if this zip code is in the MLS tables if not it will return zero, so skip it
        $zipExists = Yii::app()->stm_mls->createCommand()
            ->select('count(*)')
            ->from($boardName.'_properties')
            ->andWhere('zip='.$zip)
            ->queryScalar();

        $activeCountSubquery = '(SELECT count(listing_id) FROM '.$boardName.'_properties WHERE status="Active" AND zip="'.$zip.'" AND status_change_date >= "'.date('Y-m-d', strtotime('-3 months')).'")';
        $soldCountSubquery = '(SELECT count(listing_id) FROM '.$boardName.'_properties WHERE status IN ("Sold","Closed") AND zip="'.$zip.'")';

        //@todo: LATER - property type? Is is it worth the potential data entry error - default to single family an let user change to other??? TBD
        $stats = Yii::app()->stm_mls->createCommand()
            ->select($activeCountSubquery.' activeCount, '.$soldCountSubquery.' soldCount, MIN(price) minPrice, MAX(price) maxPrice, ROUND(AVG(price)) avgPrice, ROUND(AVG(sq_feet)) avgSf, ROUND(AVG(DATEDIFF(sold_date, list_date))) dom, ROUND(AVG(price)/AVG(sq_feet)) avgDollarSf')
            ->from($boardName.'_properties')
            ->andWhere('status="Active" OR (status IN ("Sold","Closed") AND status_change_date >= "'.date('Y-m-d', strtotime('-12 months')).'")')
            ->andWhere('zip='.$zip)
            ->queryRow();

        if(empty($stats['avgPrice'])) { //empty($stats['dom']) ||

            Yii::log(__CLASS__." (:".__LINE__.") Seller lead did not produce market stats. Please review. Remove this log when ready. Stats: ".print_r($stats, true), CLogger::LEVEL_ERROR);

            // @todo: handle this - notify user ... AND/OR remove market snapshot???
            $missingStatsBody  = 'Seller lead did not produce market stats. Property may need unique or out of area. Please review and contact the seller as needed.<br>'
                                . 'Property Address: '.$address.', '.$city.', '.$state.' '.$zip.'<br>'
                                . 'Name: '.$toFirstName.' '.$toLastName;

            StmZendMail::easyMail(array('do-not-reply@seizethemarket.net'=>'Lead Alert (Seize the Market)'), array($agentContact->getPrimaryEmailObj()->email => $agentContact->fullName),'Results not found for Market Snapshot - '. $address, $missingStatsBody, 'christine@seizethemarket.com');
            return;
        }

        // phone number display
        if((isset($agentContact->getSettings()->team_direct_phone) && !empty($agentContact->getSettings()->team_direct_phone))) {
            $phoneNumberDisplay = $agentContact->getSettings()->team_direct_phone;
        } elseif((isset($agentContact->getSettings()->cell_number) && !empty($agentContact->getSettings()->cell_number))) {
            $phoneNumberDisplay = $agentContact->getSettings()->cell_number;
        } else {
            $phoneNumberDisplay = SettingAccountValues::model()->findByAttributes(array('setting_id' => Settings::OFFICE_PHONE_ID, 'account_id'=> $accountId))->value;
        }

        $officeInfo = SettingAccountValues::getOfficeInfo($accountId);
        $transactionModel = Transactions::model()->findByPk($tnxId);

        // Construct mail message
        $EmailMessage = new EmailMessage;
        $EmailMessage->subject = date("F Y").' House Values Report - ' . $address;
        $EmailMessage->component_type_id = $transactionModel->component_type_id;
        $EmailMessage->component_id = $transactionModel->id;
        $EmailMessage->to_email_id = $toEmailModel->id;
        $EmailMessage->from_email_id = $agentContact->getPrimaryEmailObj()->id;

        if(!$EmailMessage->save()) {
            Yii::log(__CLASS__." (:".__LINE__.") Transaction ID# {$transactionModel->id} / Contact ID# {$contact->id}: Email Message did not save. Email will not be sent. (Line: ".__LINE__.") | " . print_r($EmailMessage->getErrors(), true), CLogger::LEVEL_ERROR);

            // only send email if email message saved properly. Watch error logs for reason why didn't save. For now, we know primary reason is due to no email. Also even if did sent through StmMail, it can't be logged.
        }
        else {

            $controller = new CController('marketSnapshot');

            $emailViewPath = YiiBase::getPathOfAlias('admin_module.views.emails.marketSnapshot').'.php';
            $EmailMessage->content = $controller->renderFile($emailViewPath, array(
                    'componentId' => $transactionModel->id,
                    'address' => $address,
                    'city' => $city,
                    'state' => $state,
                    'zip' => $zip,
                    'dom' => $stats['dom'],
                    'avgSalesPrice' => Yii::app()->format->formatDollars($stats['avgPrice']),
                    'message' => "Here is a report with your property values estimate called the Market Snapshot. If you're thinking about selling, buying or just want to know how the market is, this report will help.",
                    'httpDomain' => $httpDomain,
                    'marketSnahpshotLink' => $httpDomain.'/marketsnapshot/'.$transactionModel->id,
                    'contact' => $contact,
                    'emailId' => $contact->getPrimaryEmailObj()->id,
                    'agent' => $agentContact,
                    'phoneNumberDisplay' => Yii::app()->format->formatPhone($phoneNumberDisplay),
                    'officeAddress'=>$officeInfo['fullAddress'],
                    'officeName' => $officeInfo['name'],
                ), $returnContent=true);

            if(!$EmailMessage->save()) {
                Yii::log(__CLASS__." (:".__LINE__.") Transaction ID# {$transactionModel->id} / Contact ID# {$contact->id}: Email Message did not save. Email will not be sent. (Line: ".__LINE__.") | " . print_r($EmailMessage->getErrors(), true), CLogger::LEVEL_ERROR);
                return;
            }

            $mailSent = false;

            $zendMessage = new StmZendMail();
            $zendMessage->addTo($EmailMessage->toEmail->email, $toFirstName.' '.$toLastName);
            $replyTo = $zendMessage->getReplyToEmail($EmailMessage, $primaryDomainName);
            $zendMessage->setFrom($replyTo, Yii::app()->format->formatProperCase($EmailMessage->fromEmail->contact->getFullName()));
            $zendMessage->setSubject($EmailMessage->subject);
            $zendMessage->setBodyHtmlTracking($EmailMessage, $primaryDomainName);

            $zendMessage->addBcc('christine@seizethemarket.com'); //@todo: temp logging - remove when ready

            // Attempt to send message
            try {
                $zendMessage->send(StmZendMail::getAwsSesTransport());
                $mailSent =true;
            }
            catch(Exception $e) {
            //            if($i ==  1) {
            //                $this->out('Error trying to send mail on attempt # ' . $i . '.');
            //            }
            //            $this->out('An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" Message:' . $e->getMessage());
            //            //@todo: need better logging in case we need to see it all in one list
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') An error has occurred during the email sending (Zend Mail). Code: "' . $e->getCode() . '" '.PHP_EOL.'Exception Message:' . $e->getMessage().PHP_EOL
                    .'Transaction #: '.$transactionModel->id.' | Email ID: '.$EmailMessage->toEmail->id.' | Sender: '.$zendMessage->getFrom().' | Recipients'.print_r($zendMessage->getRecipients(), true), CLogger::LEVEL_ERROR);
            //            sleep(2);
            //            continue;
            }

            //        //@todo: re-do this logic, consider the continues and breaks and see if these every get hit especially in the false cases for mail sent, move error message to below?
            if($mailSent) {
                //echo 'Successfully sent market update for Saved Search #: '.$EmailMessage->component_id.' / Contact#: '.$EmailMessage->toEmail->contact->id.' | Remove this log when ready.';
            }
            else {
                // Cleanup after mail fail
                if(!$mailSent && !$EmailMessage->isNewRecord) {
                    //@todo: identify more clearly the reason for $mailSent false, it could be from mail message not loading, email was on opt-out (not really a failure) or other reasons not yet tracked/specified
                    $emailAttributes = $EmailMessage->getAttributes();
                    unset($emailAttributes['content']);
                    Yii::log(__CLASS__." (".__LINE__.") Transaction ID#: {$transactionModel->id} / Contact ID#: {$contact->id} - Email did not send. Email Message Model: " . print_r($emailAttributes, true), CLogger::LEVEL_ERROR);
                    $EmailMessage->delete();

                    $emailNotDeleted = $EmailMessage->findByPk($EmailMessage->id);
                    if($emailNotDeleted) {
                        Yii::log(__CLASS__. " (".__LINE__.") ERROR: Transaction ID#: {$transactionModel->id} / Contact ID#: {$contact->id} - Could not delete Email Message record when email was not sent.  Error Messages: " . print_r($EmailMessage->getErrors(), true), CLogger::LEVEL_ERROR);
                    }
                }
            }
        }
    }

    public static function getVimeoThumbnail($id)
    {
        //@todo: need to use API to get private stuff
//        $data = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
//        return $data[0]['thumbnail_small'];

        //---------------------------------------------------------------------------------------------
        // vimeo class send config.json paramns
//        $lib = new Vimeo($config['client_id'], $config['client_secret'], $config['access_token']);
//
//        //get data vimeo video
//        $me = $lib->request("/me/videos/$id");
//
//        //iframe vídeo
//        $embed = $me["body"]["embed"]["html"];
//
//        //edit video size
//        $default_size = 'width="'.$me["body"]["width"].'" height="'.$me["body"]["height"].'"';
//        $new_size = 'width="420" height="220"';
//
//        $embed = str_replace($default_size, $new_size, $embed);
//
//        //autoplay
//        $embed = str_replace('player_id=0', 'player_id=0&autoplay=1', $embed);
    }

	/**
	 *
	 * Name sendSms
	 *
	 * @param TelephonyPhones $fromNumber
	 * @param Phones $toNumber
	 * @param string $message
	 * @param int|null $clientId
	 * @return int|null
	 * @static static
	 *
	 * @author RapidMod.com
	 * @author 813.330.0522
	 *
	 */
	public static function sendSms(TelephonyPhones $fromNumber, Phones $toNumber, $message, $componentTypeId, $componentId)
	{
        if(empty($message)){
            throw new Exception(__CLASS__." (:".__LINE__.") SMS message cannot be empty.");
        }

		$smsMessage = new SmsMessages('outbound');
        $smsMessage->component_type_id = $componentTypeId;
        $smsMessage->component_id = $componentId;
        $errors = array();

        $smsMessage->sent_by_contact_id = $fromNumber->id;
		$smsMessage->from_phone_number = '1'.$fromNumber->phone;

		$smsMessage->sent_to_contact_id = $toNumber->contact_id;
		$smsMessage->to_phone_number = '1'.$toNumber->phone;
		$smsMessage->to_phone_id = $toNumber->id;

        $smsMessage->content = $message;
		$smsMessage->direction = SmsMessages::DIRECTION_OUTBOUND;
		$smsMessage->processed_datetime = new \CDbExpression('NOW()');

        if(YII_DEBUG) {
            // use an STM # to send for testing
//            $response = Yii::app()->plivo->sendMessage("18133300522", $message, "1".$toNumber->phone);
            $response = Yii::app()->plivo->sendMessage("19043433200", $message, "19046384718");
        }else{
            $response = Yii::app()->plivo->sendMessage("1".$toNumber->phone, $message, "1".$fromNumber->phone);
        }

        if($response['status'] != 202) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Urgent! Error sending SMS.'.PHP_EOL.'Response: : '.print_r($response, true), CLogger::LEVEL_ERROR);
            return $response;
        }

        $smsMessage->vendor_response = json_encode($response);
        $smsMessage->message_uuid = $response['response']['message_uuid'][0];
//            $response['response']['response']['message'];
//            $response['response']['response'];
//            $response['response']['response']['api_id'];
		if(!empty($errors)){
			$smsMessage->vendor_response = json_encode($errors);
		}
        if (!$smsMessage->save()) {
            Yii::log(__CLASS__ . ' (:' . __LINE__ . ') Error saving SMS Message.'.PHP_EOL.'SmsMessage Attributes: '.print_r($smsMessage->attributes, true).PHP_EOL.'Error: '.print_r($smsMessage->getErrors(), 1), CLogger::LEVEL_ERROR);
            return null;
        }
        return $smsMessage->id;
	}
}