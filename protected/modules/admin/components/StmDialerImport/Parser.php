<?php

namespace StmDialerImport;

/**
 * Parser
 *
 * Parses out special combined data in to bits and pieces
 * @package StmDialerImport
 */
class Parser
{
    /**
     * Full Name
     *
     * Special parser for full name
     * @param string $value The value to parse out
     * @return array|bool Array containing first and last name, sometimes also spouse first and last name (when available)
     */
    public static function fullName($value)
    {
        // John Smith
        if(preg_match('/^([a-z]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2],
                'spouse_first_name' =>  null,
                'spouse_last_name'  =>  null
            );
        }
        // John Smith-Jackson
        if(preg_match('/^([a-z]+)\ ([a-z]+-[a-z]+)\ ([a-z]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2],
                'spouse_first_name' =>  $matches[3],
                'spouse_last_name'  =>  $matches[4]
            );
        }

        // John Smith Jane Smith
        if(preg_match('/^([a-z]+)\ ([a-z]+)\ ([a-z]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2],
                'spouse_first_name' =>  $matches[3],
                'spouse_last_name'  =>  $matches[4]
            );
        }

        // John & Jane Smith
        if(preg_match('/^([a-z]+)\ \&\ ([a-z]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[3],
                'spouse_first_name' =>  $matches[2],
                'spouse_last_name'  =>  $matches[3]
            );
        }

        // John Smith & Jane Jones
        if(preg_match('/^([a-z]+)\ ([a-z]+)\ \&\ ([a-z]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2],
                'spouse_first_name' =>  $matches[3],
                'spouse_last_name'  =>  $matches[4]
            );
        }

        // John T Smith Jane M Smith
        if(preg_match('/^([a-z]+)\ ([a-z])\ ([a-z]+)\ ([a-z]+)\ ([a-z])\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2] . ' ' . $matches[3],
                'spouse_first_name' =>  $matches[4],
                'spouse_last_name'  =>  $matches[5] . ' ' . $matches[6]
            );
        }

        // John Smith Jane M Smith
        if(preg_match('/^([a-z]+)\ ([a-z]+)\ ([a-z]+)\ ([a-z])\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2],
                'spouse_first_name' =>  $matches[3],
                'spouse_last_name'  =>  $matches[4] . ' ' . $matches[5]
            );
        }

        // John T Smith Jane Smith
        if(preg_match('/^([a-z]+)\ ([a-z])\ ([a-z]+)\ ([a-z]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2] . ' ' . $matches[3],
                'spouse_first_name' =>  $matches[4],
                'spouse_last_name'  =>  $matches[5]
            );
        }

        // John T Smith
        if(preg_match('/^([a-z]+)\ ([a-z])\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2] . ' ' . $matches[3],
                'spouse_first_name' =>  null,
                'spouse_last_name'  =>  null
            );
        }

        // John T. Smith
        if(preg_match('/^([a-z]+)\ ([a-z.]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2] . ' ' . $matches[3],
                'spouse_first_name' =>  null,
                'spouse_last_name'  =>  null
            );
        }

        // John Thomas Smith
        if(preg_match('/^([a-z]+)\ ([a-z]+)\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'        =>  $matches[1],
                'last_name'         =>  $matches[2] . ' ' . $matches[3],
                'spouse_first_name' =>  null,
                'spouse_last_name'  =>  null
            );
        }

        // Smith, John
        if(preg_match('/^([a-z]+)\,\ ([a-z]+)$/is', $value, $matches) === 1) {
            return array(
                'first_name'    =>  $matches[2],
                'last_name'     =>  $matches[1],
                'spouse_first_name' =>  null,
                'spouse_last_name'  =>  null
            );
        }

        // default
        // Unable to determine full name, return false
        return false;
    }
}