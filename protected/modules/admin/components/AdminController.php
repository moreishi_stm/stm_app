<?php

class AdminController extends CController {

	//this is the default color theme for pages, can be specified in controller
	public $pageColor = 'red';

	// enables every controller to define which model is their base
	public $baseModel;

	// Remove this once we clear out the side menu
	public $sideMenu = array();

	// Used to store the breadcrumbs
	public $breadcrumbs = array();

	// used for breadcrumbs when controller name differs from display name
	public $displayName;

	// Used to display a title
	public $title = 'Admin';

	// Used to display a subtitle
	public $subTitle;

	/**
	 * @return string page theme color, default is red
	 */
	public function getColor() {
		return $this->pageColor;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		if (!$this->baseModel instanceof CActiveRecord) {
			throw new CHttpException(404, 'Base model not defined in: ' . __CLASS__);
		}

		$model = $this->baseModel->findByPk($id);

		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		return $model;
	}

	/**
	 * Apply access control features for our rbac implementation
	 * @return array
	 */
	public function filters() {
		return array(
			'stmAccessControl' => array(
				'stm_app.modules.admin.components.filters.StmAccessControl',
			), // perform access control
		);
	}

	/**
	 * beginStmPortlet
	 * Handle the logic behind wrapping a content section within a stm portlet layout
	 * @param  array $params possible parameters: 'containerHtmlOptions, $handleIconCss, $handleTitle, $handleHtmlOptions'
	 * @return null
	 */
	public function beginStmPortletContent($params) {
		extract($params);

		// Assign the widget container html options
		$defaultContainerHtmlOptions = array(
			'class' => 'widget g12 p-0',
		);
		if($id)
			$defaultContainerHtmlOptions['id'] = $id;

		if ($containerHtmlOptions) {
			$containerHtmlOptions = CMap::mergeArray($containerHtmlOptions, $defaultContainerHtmlOptions);
		} else {
			$containerHtmlOptions = $defaultContainerHtmlOptions;
		}

		$handleContent = $handleTitle;
		// Render an icon if one is available for the header
		if ($handleIconCss) {
			$handleIcon = CHtml::tag('em', $htmlOptions = array(
				'class' => $handleIconCss
			), null);

			$handleContent = $handleIcon . $handleContent; //**********************
		}

		if ($handleButtons) {
			$handleContent .= $this->renderHandleButtons($handleButtons);
		}

		// Assign the handle html options
		$defaultHandleHtmlOptions = array('class' => 'handle');

		if ($handleHtmlOptions) {
			$handleHtmlOptions = CMap::mergeArray($handleHtmlOptions, $defaultHandleHtmlOptions);
		} else {
			$handleHtmlOptions = $defaultHandleHtmlOptions;
		}

		$this->beginContent('/layouts/_stmPortlet', array(
			'containerHtmlOptions' => $containerHtmlOptions,
			'handleHtmlOptions' => $handleHtmlOptions,
			'handleContent' => $handleContent
		));
	}

	/**
	 * renderHandleButtons
	 * @param  array $handleButtons contains the button style properties necessary to render button
	 * @return string html code with appropriate classes
	 */
	public function renderHandleButtons($handleButtons) {
		if (!$handleButtons) {
			return;
		}

		$buttons = '';
		foreach ($handleButtons as $button) {
			if ($button['iconCssClass']) {
				$buttonIcon = $this->getEmIcon('icon ' . $button['iconCssClass']);
			}

			$buttonText = ($buttonIcon) ? $buttonIcon . $button['label'] : $button['label'];

			if ($button['display'] == 'inline') {
				$button['htmlOptions']['class'] .= ' inline-button';
			}

			if ($button['type'] == 'link') {
				$button['htmlOptions']['class'] .= ' text-button';
				$buttons .= CHtml::link($buttonText, $button['link'], $button['htmlOptions']);
			} else {
				$button['htmlOptions']['class'] .= ' text';
				$buttons .= CHtml::htmlButton($buttonText, CMap::mergeArray(array(
					'name' => $button['name'],
				), $button['htmlOptions']));
			}
		}

		return CHtml::tag('div', $htmlOptions = array(
			'class' => 'handleButtons'
		), $buttons);
	}

	/**
	 * getEmIcon
	 * @param  string $iconCssClass specifies the css icon class
	 * @return string html tag with proper class for handle button
	 */
	public function getEmIcon($iconCssClass) {
		return CHtml::tag('em', $htmlOptions = array(
			'class' => $iconCssClass
		), null);
	}

	/**
	 * Wrapper to end the portlet layout wrapper
	 * @return null
	 */
	public function endStmPortletContent() {
		$this->endContent();
	}

	protected function log($message, $errorLevel=CLogger::LEVEL_ERROR, $category='application') {
		$editedMessage = __CLASS__.': '.$message;
		Yii::log($editedMessage, $errorLevel, $category);
	}

    protected function beforeAction($action)
    {
        // moved this here from admin module as Yii::app()->user had not value at that location. Was working before, not sure what changed.
        if(Yii::app()->user->id) {
            $criteria = new CDbCriteria();
            $criteria->compare('userid',Yii::app()->user->id);
            $criteria->addInCondition('itemname', array('owner','agent','lender'));
            $criteria->addNotInCondition('account_id', array(Yii::app()->user->accountId));
            $criteria->group = 'account_id';
            Yii::app()->user->getContact()->linkedAccounts = AuthAssignment::model()->byIgnoreAccountScope()->findAll($criteria);
            Yii::app()->user->getContact()->currentAccountId = Yii::app()->user->getContact()->account->associatedWithDomain()->account_id;
        }

        $moduleName = Yii::app()->controller->module->id;
        $controllerName = Yii::app()->controller->id;
        $actionName = $action->id;

        // this makes it so that any of this is only applicable for admin users not hq. Allows account settings page view
        if(
            !Yii::app()->user->id ||
            (date('N') >= 6) ||
            $moduleName !== 'admin' || Yii::app()->request->isAjaxRequest ||
            ($controllerName == 'dialer') ||
            ($controllerName == 'settings' && $actionName == 'account') ||
            ($controllerName == 'activityLog') ||
            ($controllerName == 'tasks') ||
            ($controllerName == 'emailTemplates') ||
            ($controllerName == 'activityLog') ||
            ($controllerName == 'contacts' && $actionName == 'homesViewed') ||
            ($controllerName == 'contacts' && $actionName == 'homesSaved') ||
            ($controllerName == 'settings') ||
            ($controllerName == 'goals')  ||
            ($controllerName == 'actionPlans')  ||
            ($controllerName == 'main' && $actionName == 'logout') ||
            (in_array($controllerName, array('transactions','recruits')) && in_array($actionName, array('view','edit'))) ||
            ($controllerName == 'main' && $actionName == 'error') ||
            ($controllerName == 'tracking' && in_array($actionName, array('timeblocking','addTimeblocking','editTimeblocking'))) ||
            ($controllerName == 'tracking' && $actionName == 'appointmentsPastDue')
        ) {
            return parent::beforeAction($action);
        }


        // appointments past due redirect
        if(Yii::app()->user->settings->appointments_past_due_redirect == 1 && Yii::app()->user->settings->appointments_past_due_redirect !== '0' && Appointments::hasPastDue(Yii::app()->user->id)) {
            // if admin module && setting is yes for appointment past due redirect
            if($actionName !== 'appointmentsPastDue' && $actionName !== 'edit') {
                Yii::app()->user->setFlash('error', 'Go to "Tools" => "Appointments Past Due" to complete your info.');
                Yii::app()->controller->redirect('/admin/tracking/appointmentsPastDue');
                return parent::beforeAction($action);
            }
        }

        // tasks past due redirect, allows the view for tasks. NOTE: the ! for the tasks and index is tricky, having the ! inside () doesn't work right for the condition
        if (!($controllerName == 'tasks' && $actionName == 'index') && $tasksPastDueRedirect = Yii::app()->user->accountSettings['tasks_past_due_redirect'] && Yii::app()->user->settings->tasks_past_due_redirect !== '0') {

            // check to see that a numeric value is entered, otherwise, ignores it and uses account level setting
            $tasksPastDueRedirectUser = Yii::app()->user->settings->tasks_past_due_redirect;
            if(is_numeric($tasksPastDueRedirectUser) && $tasksPastDueRedirectUser > 0) {
                $tasksPastDueRedirect = $tasksPastDueRedirectUser;
            }
            if((Tasks::countPastDue(Yii::app()->user->id) > $tasksPastDueRedirect)) {
                Yii::app()->user->setFlash('error', 'You exceeded overdue Task limit. Please complete your tasks to continue.');
                Yii::app()->controller->redirect('/admin/tasks');
                return parent::beforeAction($action);
            }
        }

        // one thing past due redirect
        if(Yii::app()->user->settings->one_thing_daily_past_due_redirect != '0' && !($controllerName == 'goals' && in_array($actionName, array('index','edit','oneThing', 'addOneThing', 'editOneThing')))) {

            //@todo: enable default dates according to goal sheet start/end input
            $oneThingDailyMissingDates = GoalsOneThing::getMissingDays(Yii::app()->user->id, null, null, 'n/j/y');

            if($oneThingDailyMissingDates) {
                $oneThingDailyMissingDatesFormatted = array_map(function($mDate){return date('n/j/y', strtotime($mDate));}, $oneThingDailyMissingDates);
                $oneThingDailyMissingDatesString = implode(', ', $oneThingDailyMissingDatesFormatted);
                Yii::app()->user->setFlash('error', 'One Thing entries are missing. Please complete to continue. Missing Dates: '.$oneThingDailyMissingDatesString);
                Yii::app()->controller->redirect('/admin/goals/oneThing');
                return parent::beforeAction($action);
            }
        }

        if(Yii::app()->user->settings->timeblocks_past_due_redirect == 1 && Yii::app()->user->settings->timeblocks_past_due_redirect !== '0') {
            $timeblockingFromDate = '';
            $timeBlockingMissingDates = Timeblockings::getMissingDays(Yii::app()->user->id, $timeblockingFromDate, null, 'n/j/y');

            if($timeBlockingMissingDates) {

                $timeblockMissingDatesFormatted = array_map(function($mDate){return date('n/j/y', strtotime($mDate));}, $timeBlockingMissingDates);
                $timeblockMissingDatesString = implode(', ', $timeblockMissingDatesFormatted);
                Yii::app()->user->setFlash('error', 'Timeblock entries are missing. Please complete to continue. Missing Dates: '.$timeblockMissingDatesString);
                Yii::app()->controller->redirect('/admin/tracking/timeblocking');
                return parent::beforeAction($action);
            }
        }

        return parent::beforeAction($action);
    }
}