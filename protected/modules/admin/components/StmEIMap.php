<?php
// Import this
Yii::import('admin_exts.EIMap.EIMap', true);

/**
 * STM EImap
 *
 */
class StmEImap extends EIMap
{
    /**
     * Ensure Mailbox Exists
     *
     * @param string $mailbox The mailbox name to create
     */
    public function ensureMailboxExists($mailbox)
    {
        // We need a value for mailbox to continue
        if(empty($mailbox)) {
            throw new InvalidArgumentException('A mailbox must be defined');
        }

        // Proper scope
//        $mailbox = 'INBOX.' . $mailbox;

        // Get existing mailboxes
        $list = imap_getmailboxes($this->stream, $this->mailbox, "*");

        // If we had maiboxes, figure out if the one we want already exists
        if (!is_array($list)) {
            throw new Exception('No existing mailboxes found, there should at least be INBOX');
        }

        // Check over the list of existing mailboxes
        $mailboxFound = false;
        foreach ($list as $val) {

            // Get the mailbox name only
            $nameArr = explode('}', imap_utf7_decode($val->name));
            $nameRaw = $nameArr[count($nameArr)-1];
            if ('INBOX.' . $mailbox == $nameRaw) {
                $mailboxFound = true;
                break;
            }
        }

        // Create the mailbox if it doesn't already exist and if we wanted to create it
        if ($mailboxFound === false) {
            imap_createmailbox($this->stream, imap_utf7_encode($this->mailbox . '.' . $mailbox));
        }
    }

    public function changeMailbox($mailbox)
    {
        // Retrieve parts for current mailbox
        $parts = explode('}', $this->mailbox);

        // Close the current IMAP session and reopen it
        $this->close();
        return $this->connect($parts[0] . '}' . $mailbox, $this->username, $this->password);
    }


    /**
     * Imap List
     *
     * Reads the list of mailboxes
     * @link http://jp1.php.net/manual/en/function.imap-list.php
     * @param $pattern string Specifies where in the mailbox hierarchy to start searching.
     * @return array
     */
    public function imapList($pattern = '*')
    {
        return imap_list($this->stream, $this->mailbox, $pattern);
    }

    /**
     * Imap Mail Move
     *
     * Used to move a message to a different mailbox (imap folder)
     * @param $msgId string ID or IDs (See specification at http://www.php.net//manual/en/function.imap-mail-copy.php)
     * @param $destination Mailbox (imap folder) to relocate email message to
     * @return bool true on success, false on failure
     */
    public function imapMailMove($msgId, $destination)
    {
        return imap_mail_move($this->stream, $msgId, 'INBOX.' . $destination, CP_UID);
    }

    /**
     * Imap Errors
     *
     * Returns all of the IMAP errors that have occurred
     * @return array List of errors that have occurred
     */
    public function imapErrors()
    {
        return imap_errors();
    }
}