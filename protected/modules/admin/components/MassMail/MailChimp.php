<?php
// Stash this in the adapters namespace
namespace MassMail\Adapters;

// Include the base class
include_once 'MassMail/Base.php';

/**
 * Mail Chimp
 *
 * @package \MassMail\Adapters
 * @author Nicole Xu
 */
final class MailChimp extends \MassMail\Adapters\Base
{
	/**
	 * Access Token
	 * 
	 * This is here for development purposes only for a test account
	 * @var string
	 */
	protected $_accessToken = '665713b72792548925c0eaf42fea703d';
	
	/**
	 * Construct
	 * 
	 * Instantiates the object
	 */
	public function __construct()
	{
		// Store settings specific for this adapter
		$this->_apiKey = '468133138678';
		$this->_apiSecret = '0879a14a868e6e6c1391ba6bf0a5ba54';
		
		
		$this->_apiEndpoint = 'https://us8.api.mailchimp.com/2.0';			// THIS NEEDS TO BE BLANK! We can have different endpoints depending on the account (what a pain)
		
		
		$this->_oAuthRedirectUrl = 'http://www.christineleeteam.local/admin/massmail/authorize?type=MailChimp';
		$this->_oAuthAuthorizeUrl = 'https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=' . $this->_apiKey . '&redirect_uri=' . urlencode($this->_oAuthRedirectUrl);
		$this->_oAuthAccessTokenUrl = 'https://login.mailchimp.com/oauth2/token';
	}

	/**
	 * Execute
	 * 
	 * Internal functionality to handle request/response
	 * @param string $uri The URL to make the API call to
	 * @param array $postParams POST params key => value
	 * @return array Multi-dimensional array of data retrieved from API call
	 * @throws \Exception When invalid arguments have been passed, or when an API call has failed
	 */
	protected function _execute($uri, $postParams = array())
	{
		// Make sure we have a URI
		if(empty($uri)) {
			throw new \Exception('URI is required for execute');
		}
		
		// Make sure we have an array
		if(!is_array($postParams)) {
			throw new \Exception('Argument for POST params must be an array');
		}
		
		// Add the API key (access token)
		$postParams['apikey'] = $this->_accessToken;
		
		// Generate URL
		$url = $this->_apiEndpoint . $uri . '.json';
		
		// Fire up cURL
		$ch = curl_init($url);

		// Set cURL options
		curl_setopt_array($ch, array(
			   CURLOPT_RETURNTRANSFER	=>	true,
			   CURLOPT_POST				=>	true,
			   CURLOPT_SSL_VERIFYPEER	=>	false,
			   CURLOPT_POSTFIELDS		=>	json_encode($postParams)
		));

		// Make the request
		$response = trim(curl_exec($ch));
		if($response === FALSE) {
			throw new \Exception('cURL error: ' . curl_error($ch));
		}
		
		// Decode the response
		$response = json_decode($response, true);
		if($response === NULL) {
			throw new \Exception('JSON Decode Error');
		}
		
		// Return results
		return $response;
	}
	
	/**
	 * Get oAuth Access Token
	 * 
	 * Retrieves the access token from the code received
	 * @param string $code Code received from authorize request
	 * @return string Access token
	 * @throws \Exception When no code has been inputted
	 */
	public function getOAuthAccessToken($code)
	{
		// Use the parent method to retrieve the authorization token and store it in the appropriate class property
		parent::getOAuthAccessToken($code);
		
		// Retrieve metadata
		$client = new \OAuth2\Client($this->_apiKey, $this->_apiSecret);
		$client->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_OAUTH);
		$client->setAccessToken($this->_accessToken);
		
		$response = $client->fetch('https://login.mailchimp.com/oauth2/metadata');
		
		// Check our return data
		if(empty($response['result']['dc']) || empty($response['result']['api_endpoint'])) {
			throw new \Exception('Unable to retrieve data center or API endpoint information');
		}
		
		// Return data
		return array(
			'token'		=>	$this->_accessToken,
			'dc'		=>	$response['result']['dc'],
			'endpoint'	=>	$response['result']['api_endpoint']
		);
	}
	
	/**
	 * Get Lists
	 * 
	 * Retrieves all lists
	 * @return array List data
	 */
	public function getLists()
	{
		// Retrieve response
		$response = $this->_execute('/lists/list');
		
		// Model data to be common
		$results = array();
		foreach($response['data'] as $list) {
			$results[] = array(
				'id'	=>	$list['id'],
				'name'	=>	$list['name']
			);
		}
		
		return $results;
	}

	/**
	 * Get List Contacts
	 * 
	 * @param string $listId The list ID to lookup contacts by
	 * @return array Contacts data for list
	 * @throws \Exception When invalid list id has been entered
	 */
	public function getListContacts($listId)
	{
		// Make sure we entered a list id
		if(empty($listId)) {
			throw new \Exception('You must specify a list ID');
		}
		
		// Retrieve response
		$response = $this->_execute('/lists/members', array(
			'id'	=>	$listId
		));
		
		// Model results to be common
		$results = array();
		foreach($response['data'] as $contact) {
			$results[] = array(
				'id'	=>	$contact['id'],
				'email'	=>	$contact['email']
			);
		}
		
		return $results;
	}
	
	
	public function addContactsToList($listId, $contacts = array())
	{
		
	}
}