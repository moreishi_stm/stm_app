<?php
// Stash this in the adapters namespace
namespace MassMail\Adapters;

// Include the interface
include_once 'AdapterInterface.php';

/**
 * Base
 * 
 * Base class that all adapters will extend
 * @package \MassMail\Adapters
 * @author Nicole Xu
 */
abstract class Base implements \MassMail\AdapterInterface
{
	/**
	 * API Key
	 * 
	 * The apps API Key (also oAuth client ID)
	 * @var string 
	 */
	protected $_apiKey = '';
	
	/**
	 * API Secret
	 * 
	 * The apps API Secret (also oAuth secret)
	 * @var string
	 */
	protected $_apiSecret = '';
	
	/**
	 * API Endpoint
	 * 
	 * Location for API calls to be sent to
	 * @var string
	 */
	protected $_apiEndpoint = '';	
	
	/**
	 * oAuth Authorize URL
	 * 
	 * This is the URL used for requesting a token via oAuth
	 * @var string
	 */
	protected $_oAuthAuthorizeUrl = '';
	
	/**
	 * oAuth Redirect URL
	 * 
	 * This is used for completion of an oAuth authorization for an access token
	 * @var string
	 */
	protected $_oAuthRedirectUrl = '';
	
	/**
	 * oAuth Access Token URL
	 * 
	 * This is used for retrieving a new access token from a given code
	 * @var string
	 */
	protected $_oAuthAccessTokenUrl = '';
	
	/**
	 * Access Token
	 * 
	 * The oAuth access token to use for making API calls for a given account
	 * @var string
	 */
	protected $_accessToken = '';
	
	/**
	 * Get oAuth Authorize URL
	 * 
	 * Returns the oAuth authorize URL
	 * @return string The oAuth authorize URL
	 */
	public function getOAuthAuthorizeUrl()
	{
		return $this->_oAuthAuthorizeUrl;
	}
	
	/**
	 * Get oAuth Access Token
	 * 
	 * Retrieves the access token from the code received
	 * @param string $code Code received from authorize request
	 * @return string Access token
	 * @throws \Exception When no code has been inputted
	 */
	public function getOAuthAccessToken($code)
	{
		// Make sure we have a code before we continue
		if(empty($code)) {
			throw new \Exception('A code must be specified');
		}
		
		// Create a new oAuth2 client
		$client = new \OAuth2\Client($this->_apiKey, $this->_apiSecret);
		
		// Retrieve the access token
		$response = $client->getAccessToken($this->_oAuthAccessTokenUrl, 'authorization_code', array(
			'code'			=>	$code,
			'redirect_uri'	=>	$this->_oAuthRedirectUrl
		));
		
		// Make sure we had an access token
		if(empty($response['result']['access_token'])) {
			throw new \Exception('Unable to retrieve an access token at this time, please check your information try again later.');
		}
		
		// Store the access token in this class in case we want to use it
		$this->_accessToken = $response['result']['access_token'];
		
		// Return data
		return array(
			'token'		=>	$response['result']['access_token'],
			'dc'		=>	null,
			'endpoint'	=>	null
		);
	}	
}
