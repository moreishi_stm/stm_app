<?php
// Stash this in the Mass Mail namespace
namespace MassMail;

/**
 * Mass Mail Adapter Interface
 * 
 * @package \MassMail\Adapters
 * @author Nicole Xu
 */
interface AdapterInterface
{
	/**
	 * Get Lists
	 * 
	 * Retrieves all lists
	 * @return array List data
	 */
	public function getLists();
	
	/**
	 * Get List Contacts
	 * 
	 * @param string $listId The list ID to lookup contacts by
	 * @return array Contacts data for list
	 * @throws \Exception When invalid list id has been entered
	 */
	public function getListContacts($listId);
	
	public function addContactsToList($listId, $contacts = array());
}