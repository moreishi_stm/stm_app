<?php
// Stash this in the adapters namespace
namespace MassMail\Adapters;

// Include the base class
include_once 'MassMail/Base.php';

/**
 * Constant Contact
 *
 * @package \MassMail\Adapters
 * @author Nicole Xu
 */
final class ConstantContact extends \MassMail\Adapters\Base
{
	/**
	 * Access Token
	 * 
	 * This is here for development purposes only for a test account
	 * @var string
	 */
	protected $_accessToken = '33489e54-f84c-41e2-999e-008058db701c';
	
	/**
	 * Construct
	 * 
	 * Instantiates the object
	 */
	public function __construct()
	{
		// Store settings specific for this adapter
		$this->_apiKey = '7bnqdskczh7jmwpe7rpv97w7';
		$this->_apiSecret = 'eep7K8XXpR52uEuWTKZV5gsh';
		$this->_apiEndpoint = 'https://api.constantcontact.com/v2';
		$this->_oAuthRedirectUrl = 'http://www.christineleeteam.local/admin/massmail/authorize?type=ConstantContact';
		$this->_oAuthAuthorizeUrl = 'https://oauth2.constantcontact.com/oauth2/oauth/siteowner/authorize?response_type=code&client_id=' . $this->_apiKey . '&redirect_uri=' . urlencode($this->_oAuthRedirectUrl);
		$this->_oAuthAccessTokenUrl = 'https://oauth2.constantcontact.com/oauth2/oauth/token';
	}
	
	/**
	 * Execute
	 * 
	 * Internal functionality to handle request/response
	 * @param string $uri The URL to make the API call to
	 * @param array $getParams GET params key => value
	 * @param array $postParams POST params key => value
	 * @return array Multi-dimensional array of data retrieved from API call
	 * @throws \Exception When invalid arguments have been passed, or when an API call has failed
	 */
	protected function _execute($uri, $getParams = array(), $postParams = array()) {
		
		// Make sure we have a URI
		if(empty($uri)) {
			throw new \Exception('URI is required for execute');
		}
		
		// Make sure we have an array
		if(!is_array($getParams)) {
			throw new \Exception('Argument for GET params must be an array');
		}
		
		if(!is_array($postParams)) {
			throw new \Exception('Argument for POST params must be an array');
		}
		
		// Construct URL from GET params
		$url = $this->_apiEndpoint . $uri . '?api_key=' . $this->_apiKey;
		foreach($getParams as $key => $value) {
			if(!is_null($value)) {
				$url .= '&' . $key . '=' . urlencode($value);
			}
		}
		
		// Initialize cURL
		$ch = curl_init($url);
		
		// Add in cURL options
		curl_setopt_array($ch, array(
			CURLOPT_RETURNTRANSFER	=>	true,
			CURLOPT_SSL_VERIFYPEER	=>	false,
			CURLOPT_HTTPHEADER		=>	array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Authorization: Bearer ' . $this->_accessToken
			)
		));
		
		// Make the request
		$response = trim(curl_exec($ch));
		if($response === FALSE) {
			throw new \Exception('cURL error: ' . curl_error($ch));
		}
		
		// Decode the response
		$response = json_decode($response, true);
		if($response === NULL) {
			throw new \Exception('JSON Decode Error');
		}
		
		// Return results
		return $response;
	}
	
//	public function getAccountSummaryInformation()
//	{
//		return $this->_execute('/account/info');
//	}
	
	/**
	 * Get Lists
	 * 
	 * Retrieves all lists
	 * @return array List data
	 */
	public function getLists()
	{
		// Retrieve response
		$response = $this->_execute('/lists');
		
		// Model data to be common
		$results = array();
		foreach($response as $list) {
			$results[] = array(
				'id'	=>	$list['id'],
				'name'	=>	$list['name']
			);
		}
		
		return $results;
	}
	
	/**
	 * Get List Contacts
	 * 
	 * @param string $listId The list ID to lookup contacts by
	 * @return array Contacts data for list
	 * @throws \Exception When invalid list id has been entered
	 */
	public function getListContacts($listId)
	{
		// Make sure we entered a list id
		if(empty($listId)) {
			throw new \Exception('You must specify a list ID');
		}
		
		// Retrieve response
		$response = $this->_execute('/lists/' . $listId . '/contacts');
		
		// Model results to be common
		$results = array();
		foreach($response['results'] as $contact) {
			$results[] = array(
				'id'	=>	$contact['id'],
				'email'	=>	$contact['email_addresses'][0]['email_address']
			);
		}
		
		return $results;
	}
	
	public function addContactsToList($listId, $contacts = array())
	{
		
	}
}
