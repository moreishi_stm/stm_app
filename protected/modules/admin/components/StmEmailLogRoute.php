<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 1.0
 */
class StmEmailLogRoute extends CEmailLogRoute {

    private $_traceLevel = 6;

    public function init() {

        $this->setSubject('An Error Has Occurred: '.date('m/d/Y H:i:s'));
    }

	/**
	 * Adds the server and request variables to the processed message
	 * @param string $message
	 * @param int    $level
	 * @param string $category
	 * @param int    $time
	 * @return string
	 */
	protected function formatLogMessage($message, $level, $category, $time) {
        $message .= PHP_EOL.'​.............................................................................................................................................​'
                   .PHP_EOL.'Server Variables: '.print_r($_SERVER, true)
                   .PHP_EOL.'​.............................................................................................................................................​'
                   .PHP_EOL.'Request Variables: '.print_r($_REQUEST, true);

        $message .= PHP_EOL.'​.............................................................................................................................................​';

        if(YII_IS_CONSOLE) {
            $message .= PHP_EOL.'In Yii Console mode, no session variable available.';
        } else {
            $message .= PHP_EOL.'Session Variables: '.print_r($_SESSION, true);
        }

        $message .= PHP_EOL.'​.............................................................................................................................................​'
                   .PHP_EOL.'Cookie Variables: '.print_r($_COOKIE, true)
                   .PHP_EOL.'=============================================================================================================================================';

        return parent::formatLogMessage($message, $level, $category, $time);
	}

    /**
     * Sends an email. Same as parent except for replace last mail line with Yii Mail
     * @param string $email single email address
     * @param string $subject email subject
     * @param string $message email content
     */
    protected function sendEmail($email,$subject,$message)
    {
        $this->utf8 = true;
        $headers=$this->getHeaders();
        if($this->utf8) {
            $subject='=?UTF-8?B?'.base64_encode($subject).'?=';
        }
        if(($from=$this->getSentFrom())!==null)
        {
            $matches=array();
            preg_match_all('/([^<]*)<([^>]*)>/iu',$from,$matches);
            if(isset($matches[1][0],$matches[2][0]))
            {
                $name=$this->utf8 ? '=?UTF-8?B?'.base64_encode(trim($matches[1][0])).'?=' : trim($matches[1][0]);
                $from=trim($matches[2][0]);
                $headers[]="From: {$name} <{$from}>";

                $from = array($from => $name);
            }
        }

        $message = str_replace('    ','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$message);
        if(!YII_DEBUG) {
            StmZendMail::easyMail($from, $email, $subject, $message, null, 'text/html');
        }
    }
}