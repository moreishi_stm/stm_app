<?php

/**
 * StmHtml
 * Extension of the CHtml component
 * @author Chris Willard <chris@seizethemarket.com>
 * @since 9/12/2012
 * @version 1.1
 */
class StmHtml extends CHtml
{
	/**
	 * Extention to the CHtml::value() method.  Allows for the checking of the existance of a attribute.
	 * @param  CActiveRecord $model
	 * @param  string        $attribute
	 * @param  string        $defaultValue
	 * @return string
	 */
	public static function value($model, $attribute, $defaultValue=StmFormatter::DEFAULT_VALUE)
	{
		// Bypass this check if there is a dot in the attribute name
		if (strpos($attribute, '.') === false) {
            if (!$model->$attribute)
                return $defaultValue;
        }
		return parent::value($model, $attribute, $defaultValue);
	}

	/**
	 * Renders a div list from a collection of models
	 * @param  array $models
	 * @param  string $attribute
	 * @param  array  $htmlOptions HTML options for each entry
	 * @return null;
	 */
	public static function renderListItems($models, $attribute, $htmlOptions=array())
	{
		if (!is_array($models))
			return StmFormatter::DEFAULT_VALUE;

		$listItems = CHtml::openTag('div', $containerHtmlOptions=array(
			'class'=>'listContainer',
		));
		foreach($models as $Model)
			$listItems .= CHtml::tag('div', $htmlOptions, $Model->$attribute);
		$listItems .= CHtml::closeTag('div');

		return $listItems;
	}

	/**
	 * booleanImage
	 * @param  string $field value of field
	 * @return string        html code with proper image/icon
	 */
	public static function booleanImage($field) {
		if ($field)
			$string = '<em class="icon i_stm_success"></em>';
		else
			$string = '<em class="icon i_stm_error"></em>';
		return $string;
	}

	/**
	 * [formatConditionalPrint description]
	 * @param  [type] $field        [description]
	 * @param  [type] $functionName [description]
	 * @return [type]               [description]
	 */
	public static function formatConditionalPrint($field, $functionName) {
		if ($field)
			$string = StmFormatter::$functionName($field);
		return $string;
	}

}
