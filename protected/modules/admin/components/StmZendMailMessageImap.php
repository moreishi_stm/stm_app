<?php
/**
 * Stm Zend Mail Message Imap
 *
 * Contains additional helper functions for use with mail messages
 */
class StmZendMailMessageImap extends Zend_Mail_Message
{
    /**
     * Email Part Full
     *
     * @var int
     */
    const EMAIL_PART_FULL = 0;

    /**
     * Email Part Mailbox
     *
     * @var int
     */
    const EMAIL_PART_MAILBOX = 1;

    /**
     * Email Part Domain
     *
     * @var int
     */
    const EMAIL_PART_DOMAIN = 2;

    /**
     * Email Part Domain
     *
     * @var int
     */
    const EMAIL_PART_PERSONAL_NAME = 3;

    /**
     * RFC Headers
     *
     * @var Object of headers
     */
    protected $_rfcHeaders;

    /**
     * Get Rfc Headers
     *
     * @return object RFC Headers returned from imap_rfc822_parse_headers
     * @throws Exception When the function required doesn't exist
     */
    protected function _getRfcHeaders()
    {
        // If we have already received headers, use those
        if(!$this->_rfcHeaders) {

            // This function is required, make sure it exists
            if(!function_exists('imap_rfc822_parse_headers')) {
                throw new Exception('imap_rfc822_parse_headers() does not exist, please make sure that the PHP IMAP extension is correctly installed.');
            }

            // Parse out headers
            $this->_rfcHeaders = imap_rfc822_parse_headers($this->_mail->getRawHeader($this->_messageNum));
        }

        // Return the headers
        return $this->_rfcHeaders;
    }

    /**
     * Get Email Address
     *
     * @param string $type The type to pull
     * @return string The address
     */
    protected function _getEmailAddress($type, $part = self::EMAIL_PART_FULL)
    {
        // Get the address object
        $addresses = $this->_getRfcHeaders()->$type;

        // Return requested address parts
        $addressString = '';
        if(is_array($addresses)) {

            foreach($addresses as $address) {

                $addressString .= ($addressString) ? ',' : '';

                switch($part) {
                    case self::EMAIL_PART_FULL:
                        $addressString .= $address->mailbox . '@' . $address->host;
                        break;
                    case self::EMAIL_PART_MAILBOX:
                        // skip loop as it's use is to pull just one, keep it like this until verify all uses in code
                        return $address->mailbox;
                        break;

                    case self::EMAIL_PART_PERSONAL_NAME:
                        // skip loop as it's use is to pull just one, keep it like this until verify all uses in code
                        return $address->personal;
                        break;

                    case self::EMAIL_PART_DOMAIN:
                        // skip loop as it's use is to pull just one, keep it like this until verify all uses in code
                        return $address->host;
                        break;
                    default:
                        throw new Exception('Unknown part passed!');
                        break;
                }
            }
        }
        return $addressString;
    }

    /**
     * Get To Address
     *
     * @return string The first to address found
     */
    public function getToAddress()
    {
        return $this->_getEmailAddress('to');
    }

    /**
     * Get To Mailbox
     *
     * @return string The mailbox of the first address found
     */
    public function getToMailbox()
    {
        return $this->_getEmailAddress('to', self::EMAIL_PART_MAILBOX);
    }

    /**
     * Get To Domain
     *
     * @return string The domain name of the first address found
     */
    public function getToDomain()
    {
        return $this->_getEmailAddress('to', self::EMAIL_PART_DOMAIN);
    }

    /**
     * Get Reply To Address
     *
     * @return string The first reply to address found
     */
    public function getReplyToAddress()
    {
        return $this->_getEmailAddress('reply_to');
    }

    /**
     * Get From Address
     *
     * @return string The first from address found
     */
    public function getFromAddress()
    {
        return $this->_getEmailAddress('from');
    }

    /**
     * Get From Name
     *
     * @return string for personal name
     */
    public function getFromName()
    {
        return $this->_getEmailAddress('from', self::EMAIL_PART_PERSONAL_NAME);
    }

    /**
     * Get First Part By Content Type
     *
     * @param string $type The Content Type to search for
     * @return string The contents of the first of type part found in the email message
     */
    protected function _getFirstPartByContentType($type)
    {
        // Search message for specified content type
        $partCount = 0;
        foreach(new RecursiveIteratorIterator($this) as $part) {

            // We have to keep track of this because we just do
            $partCount++;

            // If the message is malformed we don't want the entire application to blow up, so wrap this in an exception and if it fails just keep trying
            try {
                if (strtok($part->contentType, ';') == $type) {

                    // Decode quoted-printable, if required
                    $content = $part->getContent();
                    $headerExists = $part->headerExists('content-transfer-encoding');
                    if($headerExists && ($part->getHeader('content-transfer-encoding') == 'quoted-printable')) {
                        $content = quoted_printable_decode($content);
                        $charset = mb_detect_encoding($content, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                        if($charset !== 'UTF-8') {
                            $content = iconv($charset, 'UTF-8', $content);
                            //$content = Yii::app()->format->formatRemoveMicrosoftTags($content);
                            //@todo: notify on what type of encoding is needed????
                        }
                    }
                    //@todo: or has html in the type??
                    elseif($headerExists && ($this->getHeaders()['content-transfer-encoding'] == 'base64' || $part->getHeader('content-transfer-encoding') == 'base64')) {
                        $content = base64_decode($content);
                        if($type == 'text/html') {
                            // if the html has body tag
                            if(strpos($content,'<html') < 5 && strpos($content,'<body') !== false && substr_count ($content,'<body') == 1) {
                                //$content = strip_tags(str_replace(array('<br />','<br>','<br/>'),PHP_EOL, $content));
                                preg_match("/<body[^>]*>(.*?)<\/body>/is", $content, $matches);
                                if($matches) {
                                    //$content = Yii::app()->format->formatRemoveMicrosoftTags($matches[1]);
                                }
                                else {
                                    //$content = nl2br(Yii::app()->format->formatRemoveMicrosoftTags(strip_tags($content)));
                                }
                            }
                            else {
                            }
                        }
                    }

                    // Return content
                    return $content;
                }
            }
            catch (Zend_Mail_Exception $e) {
                // get message
                $error = $e->getMessage();
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') ERROR in retreiving email body. Error Message: '.$error, CLogger::LEVEL_ERROR);
            }
        }

        // @todo: this try is here for debugging, can possibly remove later??? - or keep cuz it did blow up here in email processing command - - CLee 1/5/2015
        try {
            // If we wanted text/plain and we don't have any parts, then the email body is what we want
            if ($partCount === 0 && strtok($this->contentType, ';') == 'text/plain') {
                if($content = $this->getContent()) {

                    // if encoding detected, notify and will need to code for it
                    $headerExists = $this->headerExists('content-transfer-encoding');


                    if($headerExists && ($this->getHeader('content-transfer-encoding') == 'quoted-printable')) {
                        $content = quoted_printable_decode($content);
                        $charset = mb_detect_encoding($content, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                        if(mb_detect_encoding($content) !== 'UTF-8') {
                            $content = iconv($charset, 'UTF-8', $content);
                            //@todo: notify on what type of encoding is needed????
                        }
                    }
                    //@todo: or has html in the type??
                    elseif($headerExists && ($this->getHeaders()['content-transfer-encoding'] == 'base64')) {
                        $content = base64_decode($content);
                    }
                    elseif($headerExists && (in_array($this->getHeaders()['content-transfer-encoding'], array('7bit','8bit')))) {
                        $charset = mb_detect_encoding($content, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                        if(mb_detect_encoding($content) !== 'UTF-8') {
                            $content = iconv($charset, 'UTF-8', $content);
                        }
                    }
                    elseif($headerExists) {

                        Yii::log(__CLASS__ . ' (' . __LINE__ . ') ERROR: text/plain type of content is encoded "'.$this->getHeaders()['content-transfer-encoding'].'" and our method does not have decoding. Insert encoding logic here.'.PHP_EOL.'Content Data: '.$content, CLogger::LEVEL_ERROR);

                    }
                    return $content;
                }
            }
        }
        catch (Zend_Mail_Exception $e) {
            Yii::log(__CLASS__ . ' (' . __LINE__ . ') Zend Mail Error: '.print_r($e->getMessage(), true).' Subject: '.$this->subject.' From: '.$this->getFromAddress(), CLogger::LEVEL_ERROR);
        }

        return $this->_getContentByContentType($type);
    }

    /**
     * Get Content By Content Type
     * Retrieves content based on requested type. Returns null if matching type does not exist.
     *
     * @param string $type The Content Type to search for
     * @return string The contents of the content found in the email message
     */
    protected function _getContentByContentType($type)
    {
        // all parts didn't have the body, check header "content" has the body vs the parts
        try {
            $content = $this->getContent();
            if($this->headerExists('content-type')) {
                $contentTypeHeader = $this->getHeaders()['content-type'];
            }
            elseif($this->headerExists('Content-Type')) {
                $contentTypeHeader = $this->getHeaders()['Content-Type'];
            }
            else {
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Email Message content-type/Content-Type unrecognized header value: '.print_r($this->getHeaders(), true).' Verify that this email was processed properly.'.PHP_EOL.'Content Data: '.$content, CLogger::LEVEL_ERROR);
                // content type header not exists, return null as the other stuff will break
                return null;
            }

            if(strpos($contentTypeHeader, 'text/html') !== false) {
                $contentType = 'text/html';
            }
            elseif(strpos($contentTypeHeader, 'text/plain') !== false) {
                $contentType = 'text/plain';
            }
            else {
                if(!empty($this->getHeaders()['content-type'])) {
                    $contentTypeArray = explode(';', $contentTypeHeader);
                    $contentType = trim($contentTypeArray[0]);
                }
                else {
                    // error log, move in here from couple lines below
                }

                // send error log of unknown type
                Yii::log(__CLASS__ . ' (' . __LINE__ . ') URGENT: Email Message content-type unrecognized header value: '.$this->getHeaders()['content-type'].' Verify that this email was processed properly. When done reviewing, may place this in the "else" conditional above.'.PHP_EOL.'Content Data: '.$content, CLogger::LEVEL_ERROR);

                // this means that content type is not explicity defined. check to see if content has any tags. If so, it is type html, otherwise text
                //$contentType = ($content == strip_tags($content))? 'text/plain' : 'text/html';
            }

            $headerExists = $this->headerExists('content-transfer-encoding');
            if($headerExists && ($this->getHeaders()['content-transfer-encoding'] == 'quoted-printable')) {
                $content = quoted_printable_decode($content);
                $charset = mb_detect_encoding($content, 'UTF-8,ASCII,ISO-8859-1,ISO-8859-15', TRUE);
                if(mb_detect_encoding($content) !== 'UTF-8') {
                    $content = iconv($charset, 'UTF-8', $content);
                }
            }
            elseif($headerExists && ($this->getHeaders()['content-transfer-encoding'] == 'base64' || $this->getHeaders()['content-transfer-encoding'] == 'base64')) {
                $content = base64_decode($content);
            }

            if($type == $contentType) {
                return $content;
            }
        }
        catch (Zend_Mail_Exception $e) {

            $error = $e->getMessage();
        }
    }

    /**
     * Get Message Body Text
     *
     * @return string Plain text message body, if available
     */
    public function getMessageBodyText()
    {
        return preg_replace('/\=\n/', '', $this->_getFirstPartByContentType('text/plain'));
    }

    /**
     * Get Message Body HTML
     *
     * @return string HTML message body, if available
     */
    public function getMessageBodyHtml()
    {
        return preg_replace('/\=\n/', '', $this->_getFirstPartByContentType('text/html'));
    }

    /**
     * Get Message Body
     *
     * @return string Returns HTML message body if found, text otherwise
     */
    public function getMessageBody($textToHtml=false)
    {
        $body = $this->getMessageBodyHtml();

        // if nothing in html body, look for text body
        if(empty($body)) {

            $body = $this->getMessageBodyText();

            if(empty($body)) {
                // if no text parts of email exist, get content in general as there are not "parts" in the mail
                $body = $this->getContent();
            }

            $body = ($textToHtml)? nl2br($body): $body;
        }

        return $body;
    }

    /**
     * Get Message Id
     *
     * @return string Unique message ID
     */
    public function getMessageId()
    {
        return $this->_getRfcHeaders()->message_id;
    }

    /**
     * Clear CC Header
     *
     * @return void
     */
    public function clearCCHeader()
    {
        $this->_headers['cc'] = null;
    }

    /**
     * Get Attachment By Content Id
     *
     * @param string $contentId The content ID to fish for through the parts
     * @return string The decoded content of the part
     * @throws Exception When no content ID has been passed
     */
    public function getAttachmentByContentId($contentId)
    {
        // Make sure we have a value for content ID
        if(empty($contentId)) {
            throw new Exception('Content ID is required');
        }

        // Iterate through all parts
        foreach(new RecursiveIteratorIterator($this) as $part) {

            // Get headers, because if we try to get a header and it doesn't exist an exception will be thrown.....
            $headers = $part->getHeaders();

            // Check to see if this is the right one
            if(preg_replace('/^\<|\>$/', '', $headers['content-id']) == $contentId) {

                // Handle different encodings
                switch($headers['content-transfer-encoding']) {

                    // Base64
                    case 'base64':
                        return base64_decode($part->getContent());
                    break;

                    // Return raw content
                    default:
                        return $part->getContent();
                    break;
                }
            }
        }
    }

    public function getMessageSize()
    {
        $partSizes = array();
        foreach(new RecursiveIteratorIterator($this) as $part) {
            $partSizes[] = $part->getSize();
            unset($part);
        }

        // returns bytes
        return array_sum($partSizes);
    }


    public function getAttachments()
    {
        $attachments = array();
        // Iterate through all parts
        foreach(new RecursiveIteratorIterator($this) as $part) {

            // Get headers, because if we try to get a header and it doesn't exist an exception will be thrown.....
            $headers = $part->getHeaders();

            // Check to see if this is the right one
            if(strpos($headers['content-disposition'], 'attachment') !== false || strpos($headers['content-disposition'], 'filename') !== false) {

                // Get filename
                list(, $filename) = explode('filename=', $headers['content-disposition']);
                //remove quotes, trim, get first character as it could be single or double quotes
                $filename = substr($filename, 1, -1);
                // Handle different encodings
                switch($headers['content-transfer-encoding']) {

                    // Base64
                    case 'base64':
                        $content = base64_decode($part->getContent());
                        $attachments[] = array('content'=>$content, 'filename' => $filename);
                        //get total file size ... 10mb max for SES, otherwise, send via Zimbra...
                        //x-attachment-id,

                        break;

                    // Return raw content
                    default:
                        $content = $part->getContent();
                        $attachments[] = array('content'=>$content, 'filename' => $filename);
                        break;
                }
            }
            unset($part);
        }
        return $attachments;
    }
}