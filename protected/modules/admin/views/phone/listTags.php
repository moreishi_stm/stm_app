<?
$this->breadcrumbs=array(
    'Tags'=>''
);

$module = Yii::app()->controller->module->name;

Yii::app()->clientScript->registerScript('phoneTagsListScript', <<<JS

        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("phones-tag-grid", {
                data: $(this).serialize()
            });
            return false;
        });
JS
);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/phone/addTags" class="button gray icon i_stm_add">Add Phone Tag</a>
</div>

<div id="content-header">
    <h1>My Phone Tags</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'phones-tag-search',
        )); ?>
<!--    <div class="g2">-->
<!--        <label class="g7">High Priority:</label>-->
<!--        <span class="g5">-->
<!--            --><?php //echo $form->dropDownList($model, 'is_priority', StmFormHelper::getYesNoList(), $htmlOptions=array('empty'=>'')); ?>
<!--        </span>-->
<!--    </div>-->
    <div class="g6">
        <label class="g3">Name:</label>
        <span class="g9"><?php echo $form->textField($model,'name');?></span>
    </div>
<!--    <div class="g3">-->
<!--        <label class="g4">Phone:</label>-->
<!--        <span class="g8">--><?php //echo $form->textField($model,'phone');?><!--</span>-->
<!--    </div>-->
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<!--    <div class="g4">-->
<!--        <label class="g5">Call Date:</label>-->
<!--        <span class="g7">-->
<!--            --><?php //$this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'hunt-group-phones-grid','isForm'=>false, 'submitButtonVisible'=> false, 'container'=> array('tag'=>'div','htmlOptions'=>array('class'=>'p-fl')))); //, 'updateNonGridElement'=>'#header-date' ?>
<!--        </span>-->
<!--    </div>-->
    <?php $this->endWidget(); ?>
</div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'phones-tag-grid',
    'dataProvider'=>$model->search(),
    'enableSorting'=>true,
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Name',
            'value'=>'$data->name',
        ),
//        array(
//            'type'=>'raw',
//            'name'=>'Is Priority',
//            'value'=>'StmFormHelper::getYesNoName($data->is_priority)',
//        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/admin/phone/editTags/".$data["id"]."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a>"',
            'htmlOptions'=>array('style'=>'width:80px;'),
        ),
    ),
));
?>