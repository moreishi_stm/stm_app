<?
$this->breadcrumbs=array(
    'Numbers'=>''
);

$module = Yii::app()->controller->module->name;

Yii::app()->clientScript->registerScript('phoneNumbersListScript', <<<JS

        $("#TelephonyPhones_call_hunt_group_id").change(function(){
            $('#listview-search form').submit();
            return false;
        });

        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("hunt-group-phones-grid", {
                data: $(this).serialize()
            });
            return false;
        });
JS
);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/voicemail/greetings" class="button gray icon i_stm_search">Manage Voicemail Greetings</a>
</div>
<div id="content-header">
    <h1>My Phone Numbers</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'phones-search',
        )); ?>
    <div class="g4">
        <label class="g5">Call Hunt Group:</label>
            <span class="g7">
                <?php echo $form->dropDownList($model, 'call_hunt_group_id', CHtml::listData(CallHuntGroups::model()->findAll(), 'id', 'name'), $htmlOptions=array('empty'=>'Select One')); ?>
            </span>
    </div>
    <div class="g3">
        <label class="g3">Name:</label>
        <span class="g9"><?php echo $form->textField($model,'user_description');?></span>
    </div>
    <div class="g3">
        <label class="g4">Phone:</label>
        <span class="g8"><?php echo $form->textField($model,'phone');?></span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <div class="g4">
        <label class="g5">Call Date:</label>
        <span class="g7">
            <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'hunt-group-phones-grid','isForm'=>false, 'defaultSelect'=> 'last_30_days', 'submitButtonVisible'=> false, 'container'=> array('tag'=>'div','htmlOptions'=>array('class'=>'p-fl')))); //, 'updateNonGridElement'=>'#header-date' ?>
        </span>
    </div>
    <div class="g3">
        <label class="g4">Tags:</label>
        <span class="g8">
            <?php echo $form->dropDownList($model, 'tagCollection', CHtml::listData(TelephonyPhoneTags::model()->findAll(), 'id', 'name'), $htmlOptions=array('class'=>'chzn-select g12','multiple'=>'multiple','data-placeholder'=>'Select Tags')); ?>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#TelephonyPhones_tagCollection')); ?>
        </span>
    </div>
    <div class="g3">
        <label class="g4">Dedicated Line:</label>
        <span class="g8">
            <?php echo $form->dropDownList($model, 'isDedicated', StmFormHelper::getYesNoList(), $htmlOptions=array('class'=>'g12','empty'=>'')); ?>
        </span>
    </div>
    <?php $this->endWidget(); ?>

</div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'hunt-group-phones-grid',
    'dataProvider'=>$dataProvider,
    'enableSorting'=>true,
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'value'=>'(($data["is_priority"]) ? "<em class=\"icon icon-only i_stm_star\"></em>" : "")', //"<span class=\"priority-task-flag\">!</span> "
            'htmlOptions'=>array('style'=>'width:20px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Phone',
            'value'=>'Yii::app()->format->formatPhone($data["phone"])',
            'htmlOptions'=>array('style'=>'width:120px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">Total:</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'call_count',
            'value'=>'$data["call_count"]',
            'footer' => '<span style="font-weight:bold;font-size:18px;">' . (($summaryData['summaryCallCount']) ? $summaryData['summaryCallCount'] : '0') . '</span>',
        ),
        array(
            'type'=>'raw',
            'name'=>'call_answered_count',
            'value'=>'$data["call_answered_count"]',
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">'. (($summaryData['summaryCallAnswered'] != '') ? $summaryData['summaryCallAnswered'] : '') .'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'call_answered_percent',
            'value'=>'$data["call_answered_percent"].(($data["call_answered_percent"]) ? "%" : "")',
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">'. (($summaryData['summaryAnsweredPercent'] != '') ? $summaryData['summaryAnsweredPercent'].'%' : '') .'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'# Logs',
            'value'=>'$data["call_log_count"]',
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">'. (($summaryData['summaryLogCount'] != '') ? $summaryData['summaryLogCount'] : '') .'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'% Log',
            'value'=>'(($data["call_log_percent"]) ? $data["call_log_percent"]."%" : "")',
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">'. (($summaryData['summaryLogPercent'] != '') ? $summaryData['summaryLogPercent'].'%' : '') .'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'Tags',
            'value'=>'Yii::app()->format->formatCommaDelimited(TelephonyPhones::model()->findByPk($data["id"])->tags, "name")',
        ),
        array(
            'type'=>'raw',
            'name'=>'name',
            'value'=>'$data["user_description"]',
        ),
        array(
            'type'=>'raw',
            'name'=>'Direct # for',
            'value'=>'$data["fullName"]',
        ),
        array(
            'type'=>'raw',
            'name'=>'Notes',
            'value'=>'$data["user_notes"]',
        ),
        array(
            'type'=>'raw',
            'name'=>'Hunt Group',
            'value'=>'CallHuntGroups::model()->findByPk($data["call_hunt_group_id"])->name',
            'htmlOptions'=>array('style'=>'width:200px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/editNumbers/".$data["id"]."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a>"',
            'visible' => Yii::app()->user->checkAccess('owner'),
            'htmlOptions'=>array('style'=>'width:80px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<a href=\"/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/callHistory/".$data["id"]."\" class=\"button gray icon i_stm_search grey-button\">Call History</a>"',
            'htmlOptions'=>array('style'=>'width:90px;'),
        ),///voicemail?id=1&type=phone
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<a href=\"/".Yii::app()->controller->module->id."/voicemail?id=".$data["id"]."&type=phone\" class=\"button gray icon i_stm_search grey-button\">Voicemail</a>"',
            'htmlOptions'=>array('style'=>'width:90px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/".Yii::app()->controller->module->id."/callHuntGroups".((Yii::app()->controller->module->id == "hq") ? "HQ": "")."/listPhone/".$data["call_hunt_group_id"]."\" class=\"button gray icon i_stm_search grey-button\">Hunt Group</a>"
			',
            'htmlOptions'=>array('style'=>'width:120px;text-align: center;'),
        ),
    ),
));
?>