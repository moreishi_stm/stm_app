<?
$this->breadcrumbs=array(
    'Tags'=>'/admin/phone/tags',
    'Add'=>''
);?>
<div id="content-header">
    <h1>Edit My Phone Number</h1>
</div>

<?
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=> 'My Phone Tag',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-container" >
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <div class="g1"></div>
        <div class="g8">
            <table class="container">
                <tr>
                    <th><?php echo $form->labelEx($model, 'name');?>:</th>
                    <td><?php echo $form->textField($model, 'name', $htmlOptions=array('placeholder'=>'Name')); ?>
                        <?php echo $form->error($model,'name'); ?>
                    </td>
                </tr>
<!--                <tr>-->
<!--                    <th>--><?php //echo $form->labelEx($model, 'is_priority');?><!--</th>-->
<!--                    <td>--><?php //echo $form->dropDownList($model, 'is_priority', StmFormHelper::getYesNoList(), $htmlOptions=array()); ?>
<!--                        --><?php //echo $form->error($model,'is_priority'); ?>
<!--                    </td>-->
<!--                </tr>-->
            </table>
        </div>
        <div class="g3"></div>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Phone Tag</button></div>
<?php $this->endWidget(); ?>
