<style type="text/css">

    body {
        padding-top: 15px;
    }

    header:first-of-type {
        display: none;
    }

    /*footer:last-of-type {*/
        /*display: none;*/
    /*}*/

    #content {
        min-height: auto;
    }

    #phone-number-container {
        text-align: center;
        max-width: 400px;
        margin: 0 auto;
    }

    #phone-number {
        width: 100%;
        font-size: 24px;
        box-sizing: border-box;
        margin-bottom: 10px;
    }

    .number-pad {
        display: table;
        width: 100%;
    }

    #number-pad>div {
        display: table-row;
    }

    #number-pad>div>button {
        display: table-cell;
        box-sizing: border-box;
        width: 130px;
        height: 75px;
        font-size: 18px;
    }

    #number-pad>div>button:first-of-type {
        margin-left: 0px;
    }

    #number-pad>div>button:last-of-type {
        margin-right: 0px;
    }

    #call-buttons button {
        box-sizing: border-box;
        width: 195px;
        font-size: 18px;
        height: 75px;
    }

    #call-buttons button:first-of-type {
        margin-left: 0px;
    }

    #call-buttons button:last-of-type {
        margin-right: 0px;
    }
</style>

<h1>Phone</h1>

<div id="phone-number-container">
    <input type="text" id="phone-number" placeholder="(123) 456 7890" value="">
    <div id="number-pad">
        <div>
            <button type="button" value="1">1</button>
            <button type="button" value="2">2</button>
            <button type="button" value="3">3</button>
        </div>
        <div>
            <button type="button" value="4">4</button>
            <button type="button" value="5">5</button>
            <button type="button" value="6">6</button>
        </div>
        <div>
            <button type="button" value="7">7</button>
            <button type="button" value="8">8</button>
            <button type="button" value="9">9</button>
        </div>
        <div>
            <button type="button" value="*">*</button>
            <button type="button" value="0">0</button>
            <button type="button" value="#">#</button>
        </div>
    </div>
    <div id="call-buttons">
        <button type="button" id="clear-number">Clear Number</button>
        <button type="button" id="make-call">Call</button>
    </div>
</div>

<input type="hidden" id="contact-id" value="<?=Yii::app()->user->id?>">

<!-- JavaScript -->
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<script type="text/javascript" src="https://static.twilio.com/libs/twiliojs/1.2/twilio.min.js"></script>
<!--<script type="text/javascript" src="https://s3.amazonaws.com/plivosdk/web/plivo.min.js"></script>-->
<script type="text/javascript">

    // Do this because of stuff not working, ask Brandon how we are including jQuery stuff now
    var j = jQuery.noConflict(true);

    //
    // Twilio Event Bindings and Functions
    //

    // Handle errors
    Twilio.Device.error(function (error) {
        console.log('Twilio Error!');
        console.log(error);
    });

    // Handle going offline!? Not sure what this is but let's find out
    Twilio.Device.offline(function (device) {
        console.log('Twilio Offline!?');
    });

    // Do this when a call disconnects?
    Twilio.Device.disconnect(function (conn) {
        console.log('Twilio Call Disconneted');
    });


    // Helper function to make the call
    var makeCall = function() {

        // AJAX post to retrieve up to date token for Twilio
        j.post('/admin/phone/generatetoken', function(data) {

            // Setup Twilio with the token
            Twilio.Device.setup(data.token);

            // Make the call
            Twilio.Device.connect({
                tocall: j('#phone-number').val(),
                contactId: j('#contact-id').val()
            });
        });
    };

    //
    // UI Event Bindings
    //

    // Bind delegate for number pad
    j('#number-pad').delegate('button', 'click', function(e) {

        // Append the digit to the phone number
        j('#phone-number').val(function(index, val) {
            return val + e.target.value;
        });
    });

    // Bind clear number button
    j('#clear-number').click(function() {
        j('#phone-number').val('');
    });

    // Bind call button event
    j('#make-call').click(function() {
        makeCall();
    });

    // Helper function to get URI variable
    var getURLParameter = function(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
    }

    // Set the phone number to call from the URL if we had one
//    console.log(getURLParameter('to'));

    // Set the phone number from whatever we had
    j('#phone-number').val(getURLParameter('to'));

    // If we wanna auto-dial, then auto-dial on load
    if(getURLParameter('autoDial') == '1') {

        // Hide the phone number display
        j('#phone-number').hide();

        // Make the call
        makeCall();
    }

</script>