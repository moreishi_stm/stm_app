<?
$this->breadcrumbs=array(
    'Call History'=>''
);

$domainSuffix = (YII_DEBUG) ? 'local': 'com';
$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";
Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);

$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerCss('callHistoryCssScript', <<<CSS
    .report-status-action-button {
        position: relative;
        cursor: pointer;
        display: inline-block;
        font-weight: normal !important;
        text-decoration: none !important;
    }
    .report-status-action-text {
        position: absolute;
        right: -22px;
        width: 120px;
        border: 1px #AAA solid;
        padding: 4px;
        background: #EFEFEF;
    }

    .report-status-action-text {
        display: none;
        color: #555;
        text-shadow: none;
    }

    .show .report-status-action-text {
        display: block;
        top: 14px;
    }

    .report-status-action-text:hover {
        text-decoration: underline !important;
    }
CSS
);

Yii::app()->clientScript->registerScript('callHistoryScript', <<<JS

    $("#status").change(function(){
        $('#listview-search form').submit();
        return false;
    });

    $('a.listen-recording').on('click', function() {
        var recordingSource = $(this).attr('data-source');
        $('#recording-controls').attr('data-src', recordingSource);
        $('#recording-source').attr('src', recordingSource);
        $('#recording-embed').attr('src', recordingSource);
        $("#recording-controls").trigger('load');

        $.fancybox({
            'type': 'inline',
            'autoSize': false,
            'content': $('#recording-container').html(),
            'width':600,
            'height':'auto',
            'scrolling':'no',
            'padding':40
        });
    });

    $('.report-status-action-button').live('click', function(){
        if($(this).hasClass('show')) {
            $(this).removeClass('show');
        }
        else {
            $(this).addClass('show');
        }
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update("call-history-grid", {
            data: $(this).serialize()
        });
        $('.view-voicemail-button').attr('href','/$module/voicemail?number=' + $('#TelephonyPhones_phone').val() + '&type=phone');
        $("a.listen-recording").fancybox({'width':600, 'height':'auto', 'autoSize': false, 'scrolling':'no', 'padding':40});

        return false;
    });
JS
);

/*$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
        'target' => 'a.listen-recording', 'config' => array(
            'width' => 600, 'height' => 'auto', 'autoSize' => false, 'scrolling' => 'no', 'autoDimensions' => false, 'padding' => '40',
        ),
    )
);*/

?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/voicemail?id=<?=$model->id?>&type=phone" class="button gray icon i_stm_search view-voicemail-button">View Voicemails</a>
</div>
<div id="content-header">
    <h1>Call History</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'call-history-search',
        )); ?>
    <div class="g4">
        <label class="g3">Phone #:</label>
            <span class="g9">
                <?php echo $form->dropDownList($model, 'phone', $phoneListData, $htmlOptions=array('empty'=>'','data-placeholder' => 'Select a Phone #'));
                $this->widget(
                    'admin_module.extensions.EChosen.EChosen',
                    array('target' => '#TelephonyPhones_phone',
                         'options' => array('allow_single_deselect' => true,
                                            'width' => '100%',
                                            'enable_split_word_search'=>true,
                                            'search_contains'=>true,
                        ),
                    )
                );
                ?>
            </span>
    </div>
    <div class="g2">
        <label class="g5">Status:</label>
        <span class="g7"><?php echo CHtml::dropDownList('status', null, array(0=> 'Missed', 1=> 'Accepted'), $htmlOptions=array('empty' => ''));?></span>
    </div>
    <div class="g3">
        <label class="g4">From Number:</label>
        <span class="g8"><?php echo CHtml::textField('fromNumber', null);?></span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <div class="g6">
        <label class="g2">Call Date:</label>
        <span class="g10">
            <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'hunt-group-phones-grid','isForm'=>false, 'submitButtonVisible'=> false, 'defaultSelect'=> 'last_30_days', 'container'=> array('tag'=>'div','htmlOptions'=>array('class'=>'p-fl')))); //, 'updateNonGridElement'=>'#header-date' ?>
        </span>
    </div>
    <div class="g4">
        <label class="g4">Is Priority:</label>
        <span class="g8"><?php echo CHtml::dropDownList('status', null, array(0=> 'No', 1=> 'Yes'), $htmlOptions=array('empty' => ''));?></span>
    </div>
    <?php $this->endWidget(); ?>

</div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'call-history-grid',
    'dataProvider'=>$dataProvider,
    'enableSorting'=>true,
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'called',
            'value'=>'Yii::app()->format->formatDateTime($data["added"])',
            'htmlOptions'=>array('style'=>'width:70px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">Total:</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'toNumber',
            'value'=>'Yii::app()->format->formatPhone(substr($data["to_number"], 1))',
            'htmlOptions'=>array('style'=>'width:110px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px;">'.(($summaryData['calls_count']) ? $summaryData['calls_count'] : '').'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'fromNumber',
            'value'=>'Yii::app()->format->formatPhone(substr($data["from_number"], 1))',
            'htmlOptions'=>array('style'=>'width:110px; font-weight: bold;'),
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">Answered:</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'Contact Matches',
            'value'=>'Yii::app()->controller->printContactMatches($data)',
        ),
//        array(
//            'type'=>'raw',
//            'name'=>'Answered by',
//            'value'=>'',
//            'htmlOptions'=>array('style'=>'width:150px;'),
//        ),
        array(
            'type'=>'raw',
            'name'=>'status',
            'value'=>'Yii::app()->controller->printStatus($data).(($data["answeredBy"]) ? "<br>(".$data["answeredBy"].")" : "")',
            'htmlOptions'=>array('style'=>'width:150px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px;">'.(($summaryData['answeredPercent']) ? '('.$summaryData['answeredPercent'].')' : '').'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'Follow-up Status',
            'value'=>'Yii::app()->controller->printFollowUpStatus($data)',
            'htmlOptions'=>array('style'=>'width:60px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px;">'.(($summaryData['answeredCount']) ? $summaryData['answeredCount'] : '').'</div>',
        ),
        array(
            'type' => 'raw',
            'name' => 'In Reports',
            'value' => 'Yii::app()->controller->printCallHistoryReportAction($data)',
            'htmlOptions' => array('style' => 'width:70px; text-align: center;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Follow-up Notes',
            'value'=>'Yii::app()->controller->printFollowupNotes($data)',
            'htmlOptions'=>array('style'=>'width:200px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<button class=\"add-activity-log-button\" ctid=\"".ComponentTypes::DIRECT_INCOMING_CALLS."\" data=\"".$data["id"]."\">Add Follow-up</button>"',
            'htmlOptions'=>array('style'=>'width:140px;'),
        ),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => '(($data["recording_url"]) ? "<div><a href=\"#recording-container\" data-source=\"".$data["recording_url"]."\" class=\"button gray icon i_stm_search grey-button listen-recording\">Listen</a></div>" : "")',
            'htmlOptions' => array('style' => 'width:100px'),
        ),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => '(($data["vm_url"]) ? "<div><a href=\"#recording-container\" data-source=\"".$data["vm_url"]."\" class=\"button gray icon i_stm_search grey-button listen-recording\">Voicemail</a></div>" : "")',
            'htmlOptions' => array('style' => 'width:100px'),
        ),

//        array(
//            'type' => 'raw',
//            'name' => 'Report Action',
//            'value' => '(($data["ignore"]) ? "<div><a href=\"javascript:void(0)\" class=\"button gray report-include-button icon i_stm_add grey-button\" data-id=\"".$data["id"]."\">Include</a></div>" : "<div><a href=\"javascript:void(0)\" class=\"button gray report-exclude-button icon i_stm_delete grey-button\" data-id=\"".$data["id"]."\">Exclude</a></div>")',
//            'htmlOptions' => array('style' => 'width:100px'),
//        ),
//        array(
//            'type' => 'raw',
//            'name' => '',
//            'value' => '
//				"<div><a href=\"/'.$module.'/dialer/recordingDownload/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">Download</a></div>"',
//            'htmlOptions' => array('style' => 'width:100px'),
//        ),
//        array(
//            'type'=>'raw',
//            'name'=>'',
//            'value'=>'
//				"<a href=\"/admin/callHuntGroups/listPhone/".$data["call_hunt_group_id"]."\" class=\"button gray icon i_stm_search grey-button\">View Hunt Group</a>"
//			',
//            'htmlOptions'=>array('style'=>'width:160px;text-align: center;'),
//        ),
    ),
));
?>
<div style="display: none">
    <div id="recording-container" style="padding: 40px;">
        <h2 style="margin-bottom: 20px;">Listen to Call Audio</h2>
        <audio controls title="Audio File" id="recording-controls" data-src="" style="width: 100%;">
            <source src="" type="audio/mpeg" id="recording-source">
            <embed height="50" width="650" src="" id="recording-embed">
        </audio>
    </div>
</div>

<?
// Checks to see if a activity log dialog already exists and creates one only if it doesn't exist.
Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
            'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
            'title' => 'Complete Task & Log Activity',
            'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        )
    );
}

Yii::import('admin_widgets.DialogWidget.CallReportingDialogWidget.CallReportingDialog');
if (!class_exists('CallReportingDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.CallReportingDialog.CallReportingDialog', array(
            'id' => CallReportingDialog::DIALOG_ID,
            'title' => 'Update Call Reporting Status',
            'triggerElement' => CallReportingDialog::TRIGGERS,
        )
    );
}
