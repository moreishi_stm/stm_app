<?
$this->breadcrumbs=array(
    'Lead Call History'=>''
);

$module = Yii::app()->controller->module->name;

Yii::app()->clientScript->registerScript('callHistoryScript', <<<JS

    $("#status").change(function(){
        $('#listview-search form').submit();
        return false;
    });

    $('a.listen-recording').live('click', function() {
        var recordingSource = $(this).data('source');
        $('#recording-controls').attr('data-src', recordingSource);
        $('#recording-source').attr('src', recordingSource);
        $('#recording-embed').attr('src', recordingSource);
        $("#recording-controls").trigger('load');

        $.fancybox({
            'type': 'inline',
            'content': '#recording-container',
            'width':'600', 'height':'100', 'scrolling':'no', 'autoDimensions':false, 'padding':'40'
        });
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update("call-history-grid", {
            data: $(this).serialize()
        });

        $("a.listen-recording").fancybox({'width':'600', 'height':'100', 'scrolling':'no', 'autoDimensions':false, 'padding':'40'});

        return false;
    });
JS
);

$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
        'target' => 'a.listen-recording', 'config' => array(
            'width' => '600', 'height' => '100', 'scrolling' => 'no', 'autoDimensions' => false, 'padding' => '40',
        ),
    )
);

?>
<div id="content-header">
    <h1>Lead Connect Call History</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'phones-search',
        )); ?>
    <div class="g2"></div>
<!--    <div class="g4">-->
<!--        <label class="g3">Phone #:</label>-->
<!--            <span class="g9">-->
<!--                --><?php //echo $form->dropDownList($model, 'phone', $phoneListData, $htmlOptions=array('empty'=>'','data-placeholder' => 'Select a Phone #'));
//                $this->widget(
//                    'admin_module.extensions.EChosen.EChosen',
//                    array('target' => '#TelephonyPhones_phone',
//                         'options' => array('allow_single_deselect' => true,
//                                            'width' => '100%',
//                                            'enable_split_word_search'=>true,
//                                            'search_contains'=>true,
//                        ),
//                    )
//                );
//                ?>
<!--            </span>-->
<!--    </div>-->
    <div class="g3">
        <label class="g6">Status:</label>
        <span class="g6"><?php echo CHtml::dropDownList('status', null, array(0=> 'Missed', 1=> 'Answered'), $htmlOptions=array('empty' => ''));?></span>
    </div>
<!--    <div class="g3">-->
<!--        <label class="g4">From Number:</label>-->
<!--        <span class="g8">--><?php //echo CHtml::textField('fromNumber', null);?><!--</span>-->
<!--    </div>-->
    <div class="g3">
        <label class="g3">Date:</label>
        <span class="g9">
            <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'hunt-group-phones-grid','isForm'=>false, 'submitButtonVisible'=> false, 'container'=> array('tag'=>'div','htmlOptions'=>array('class'=>'p-fl')))); //, 'updateNonGridElement'=>'#header-date' ?>
        </span>
    </div>
    <div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>

</div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'call-history-grid',
    'dataProvider'=>$dataProvider,
    'enableSorting'=>true,
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'called',
            'value'=>'Yii::app()->format->formatDateTime($data["added"])',
            'htmlOptions'=>array('style'=>'width:150px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">Total Calls:</div>',
        ),
//        array(
//            'type'=>'raw',
//            'name'=>'toNumber',
//            'value'=>'Yii::app()->format->formatPhone(substr($data["to_number"], 1))',
//            'htmlOptions'=>array('style'=>'width:150px;'),
//            'footer' => '<div style="font-weight:bold;font-size:18px;">'.(($summaryData['calls_count']) ? $summaryData['calls_count'] : '').'</div>',
//        ),
//        array(
//            'type'=>'raw',
//            'name'=>'fromNumber',
//            'value'=>'Yii::app()->format->formatPhone(substr($data["from_number"], 1))',
//            'htmlOptions'=>array('style'=>'width:150px; font-weight: bold;'),
//            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">Answered:</div>',
//        ),
        array(
            'type'=>'raw',
            'name'=>'status',
            'value'=>'$data["status"]',
            'htmlOptions'=>array('style'=>'width:100px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px;">'.(($summaryData['answeredCount']) ? $summaryData['answeredCount'] : '').'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'Answered by',
            'value'=>'$data["answeredBy"]',
            'htmlOptions'=>array('style'=>'width:150px;'),
            'footer' => '<div style="font-weight:bold;font-size:18px;">'.(($summaryData['answeredPercent']) ? '('.$summaryData['answeredPercent'].')' : '').'</div>',
        ),
        array(
            'type'=>'raw',
            'name'=>'Type',
            'value'=>'$data["componentDisplayName"]',
            'htmlOptions'=>array('style'=>'width:60px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Lead',
            'value'=>'$data["fullName"]',
            'htmlOptions' => array('style' => 'width:150px'),
        ),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => '"<div><a href=\"/'.$module.'/".$data["component_name"]."/".$data["component_id"]."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View ".$data["componentDisplayName"]."</a></div>"',
            'htmlOptions' => array('style' => 'width:80px'),
        ),
        array(
            'type' => 'raw',
            'name' => 'Recording',
            'value' => '(($data["recording_url"]) ? "<div><a href=\"#recording-container\" data-source=\"".$data["recording_url"]."\" class=\"button gray icon i_stm_search grey-button listen-recording\">Listen to Recording</a></div>" : "")',
            'htmlOptions' => array('style' => 'width:200px'),
        ),
//        array(
//            'type' => 'raw',
//            'name' => '',
//            'value' => '
//				"<div><a href=\"/'.$module.'/dialer/recordingDownload/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">Download</a></div>"',
//            'htmlOptions' => array('style' => 'width:100px'),
//        ),
//        array(
//            'type'=>'raw',
//            'name'=>'Follow-up Status',
//            'htmlOptions'=>array('style'=>'width:120px;'),
//            'value'=>'"TBD - coming soon..."',
//            'footer' => '<div style="font-weight:bold;font-size:18px; text-align: right;">%</div>',
//        ),
//        array(
//            'type'=>'raw',
//            'name'=>'Follow-up Notes',
//            'value'=>'"TBD - coming soon..."',
//        ),
//        array(
//            'type'=>'raw',
//            'name'=>'',
//            'value'=>'
//				"<a href=\"/admin/callHuntGroups/listPhone/".$data["call_hunt_group_id"]."\" class=\"button gray icon i_stm_search grey-button\">View Hunt Group</a>"
//			',
//            'htmlOptions'=>array('style'=>'width:160px;text-align: center;'),
//        ),
    ),
));
?>
<div style="display: none">
    <div id="recording-container">
        <h2 style="margin-bottom: 20px;">Listen to Call Audio</h2>
        <audio controls title="Audio File" id="recording-controls" data-src="" style="width: 100%;">
            <source src="" type="audio/mpeg" id="recording-source">
            <embed height="50" width="650" src="" id="recording-embed">
        </audio>
    </div>
</div>