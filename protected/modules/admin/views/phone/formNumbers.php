<?
Yii::app()->clientScript->registerScript('telephonyPhoneListScript', <<<JS
    $('#TelephonyPhones_call_route_type').change(function(){
        if($(this).val() == 'Hunt Group') {
            $('#ivr-row').hide();
            $('#call-hunt-group-row').show('slow');
        }
        else {
            $('#call-hunt-group-row').hide();
            $('#ivr-row').show('slow');
        }
    });
JS
);
$this->breadcrumbs=array(
    'Edit Number'=>''
);?>
<div id="content-header">
    <h1>Edit My Phone Number</h1>
</div>

<?
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'telephony-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=> 'My Phone Number',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-container" >
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <div class="g1"></div>
        <div class="g8">
            <table class="container">
                <tr>
                    <th>Phone Number:</th>
                    <td style="margin:10px 0 10px 0; color: #58F; font-size: 20px;"><?=Yii::app()->format->formatPhone($model->phone); ?>
                    </td>
                </tr>
                <? if($model->contact_id): ?>
                <tr>
                    <th>Dedicated to:</th>
                    <td><?php echo $form->dropDownList($model, 'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->contact_id)->findAll(),'id','fullName'), $htmlOptions=array()); ?>
                    </td>
                </tr>
                <? endif; ?>
                <tr>
                    <th><?php echo $form->labelEx($model, 'user_description');?>:</th>
                    <td><?php echo $form->textField($model, 'user_description', $htmlOptions=array('placeholder'=>'Name/Title')); ?>
                        <?php echo $form->error($model,'user_description'); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo $form->labelEx($model, 'is_priority');?>:
                    </th>
                    <td><?php echo $form->dropDownList($model, 'is_priority', StmFormHelper::getYesNoList(false), $htmlOptions=array()); ?>
                        <div class="stm-tool-tip question">
                            <span>If set to “Yes,” then an automatic report will notify you when there are any calls without activity logs.  Logs can be found by clicking the “Call History” button on the <a href="/admin/phone">Phone Numbers Page</a></span>
                        </div>
                        <?php echo $form->error($model,'is_priority'); ?>
                    </td>
                </tr>
<!--                 <tr>
                    <th><?php echo $form->labelEx($model, 'tagCollection');?></th>
                    <td><?php echo $form->dropDownList($model, 'tagCollection', Chtml::listData(TelephonyPhoneTags::model()->findAll(),'id','name'), $htmlOptions=array('class'=>'chzn-select g12','multiple'=>'multiple','data-placeholder'=>'Select Tags')); ?>
                        <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#TelephonyPhones_tagCollection')); ?>
                        <?php echo $form->error($model,'tagCollection'); ?>
                    </td>
                </tr> -->
                <tr>
                    <th><?php echo $form->labelEx($model, 'user_notes');?>:</th>
                    <td><?php echo $form->textField($model, 'user_notes', $htmlOptions=array('placeholder'=>'Notes')); ?>
                        <?php echo $form->error($model,'user_notes'); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo $form->labelEx($model, 'incoming_caller_id_display');?>:
                    </th>
                    <td><?php echo $form->dropDownList($model, 'incoming_caller_id_display', $model->callerIdTypeList, $htmlOptions=array()); ?>
                        <div class="stm-tool-tip question">
                            <span>Choose which Caller ID will display for incoming calls to this phone number. </span>
                        </div>
                        <?php echo $form->error($model,'incoming_caller_id_display'); ?>
                    </td>
                </tr>
                <tr>
                    <th># Rings to Voicemail:</th>
                    <td><?php echo $form->dropDownList($model, 'ring_timeout', TelephonyPhones::getRingSecondsList(), $htmlOptions=array()); ?>
                        <div class="stm-tool-tip question">
                            <span>Number of times the phone will ring before being sent to voicemail.</span>
                        </div>
                        <?php echo $form->error($model,'ring_timeout'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Voicemail Greeting:</th>
                    <td><?php echo $form->dropDownList($model, 'voicemail_greeting_id', CHtml::listData(CallRecordings::model()->findAll(), 'id', 'label'), $htmlOptions=array('empty'=>'Select Voicemail Greeting')); ?>
                        <?php echo $form->error($model,'voicemail_greeting_id'); ?>
                        <div class="stm-tool-tip question">
                            <span>This is a prerecorded voicemail greeting to play when a call goes to voicemail. </span>
                        </div>
                        <a href="/<?=Yii::app()->controller->module->id?>/voicemail/greetings" class="btn primary default">Manage Voicemails</a>
                    </td>
                </tr>
                <tr>
                    <th>Notification Emails:</th>
                    <td><?php echo $form->textField($model, 'notification_emails', $htmlOptions=array('placeholder'=>'Notification Emails (comma separated)')); ?>
                        <div class="stm-tool-tip question">
                            <span>A notification will be sent to this email address when a call is missed or a voicemail is received. You can add multiple emails separated by a comma.</span>
                        </div>
                        <?php echo $form->error($model,'notification_emails'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'call_route_type');?></th>
                    <td><?php echo $form->dropDownList($model, 'call_route_type', array('Hunt Group'=>'Hunt Group','IVR'=>'IVR'), $htmlOptions=array()); ?>
                        <div class="stm-tool-tip question">
                            <span>IVR is a pre-recorded hotline. A hunt group is a normal targeted call. Select a hunt group to direct incoming calls to the phone number of your choice. A hunt group can move one or more phone numbers in it to forward the call to. </span>
                        </div>
                        <?php echo $form->error($model,'call_route_type'); ?>
                    </td>
                </tr>
                <tr id="call-hunt-group-row" style="<?=($model->call_route_type == 'Hunt Group' || $model->call_route_type == '') ? '': 'display:none;'?>">
                    <th><?php echo $form->labelEx($model, 'call_hunt_group_id');?></th>
                    <td><?php echo $form->dropDownList($model, 'call_hunt_group_id', Chtml::listData(CallHuntGroups::model()->findAll(),'id','name'), $htmlOptions=array()); ?>
                        <div class="stm-tool-tip question">
                            <span>Select which Call Hunt Group will ring for incoming calls. Manage your hunt groups here. <a href="/admin/callHuntGroups">Call Hunt Groups</a></span>
                        </div>
                        <?php echo $form->error($model,'call_hunt_group_id'); ?>
                    </td>
                </tr>
                <tr id="ivr-row" style="<?=($model->call_route_type == 'IVR') ? '' : 'display:none;'?>">
                    <th><?php echo $form->labelEx($model, 'ivr_id');?></th>
                    <td><?php echo $form->dropDownList($model, 'ivr_id', Chtml::listData(Ivrs::model()->findAll(),'id','name'), $htmlOptions=array('empty'=>'')); ?>
                        <?php echo $form->error($model,'ivr_id'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="g3"></div>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Phone Number</button></div>
<?php $this->endWidget(); ?>
