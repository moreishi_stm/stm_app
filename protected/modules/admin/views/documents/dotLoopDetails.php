<?php
$this->breadcrumbs = array(
    'Dot Loop' => '',
	'Details' => '',
);
?>
<h1>Loop Details for <?php echo $loopInfo['loopName'];?></h1>
<h2>Status: <?php echo $loopInfo['loopStatus'];?></h2>

<?php
    $this->widget('admin_module.components.StmGridView', array(
        'id'            => 'loop-documents-grid',
        'dataProvider'  => $documentDataProvider,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type'  => 'raw',
                'name'  => 'Folder',
                'value' => '$data[folderName]',
                'htmlOptions'=>array('style'=>'width:120px'),
            ),
            array(
                'type'  => 'raw',
                'name'  => 'Document Name',
                'value' => '$data[documentName]',
            ),

            array(
                'type'  => 'raw',
                'name'  => 'Last Modified',
                'value' => 'Yii::app()->format->formatDate($data[lastModifiedDate])',
                'htmlOptions'=>array('style'=>'width:120px'),
            ),
            array(
                'type'  => 'raw',
                'name'  => 'Created',
                'value' => 'Yii::app()->format->formatDate($data[createdDate])',
                'htmlOptions'=>array('style'=>'width:120px'),
            ),
            array(
                'type'=>'raw',
                'name'=>'',
                'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/documents/dotLoopViewDocument/".$data["loopViewId"]."?documentId=".$data["documentId"]."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Document</a></div>"
			',
                'htmlOptions'=>array('style'=>'width:160px'),
            ),
        ),
    ));
