<?php
$this->breadcrumbs = array(
	'List' => '',
);
$module = Yii::app()->controller->module->name;
?>

<!-- Modal Template -->
<?php
echo '<script id="template-dialog-form" type="text/x-handlebars-template">';
	$model = new Documents();
    $model->componentTypeId = ComponentTypes::CONTACTS;
    $model->componentId = Yii::app()->user->id;
    $model->visibility_ma = Documents::VISIBILITY_TEAM;
    $model->document_type_id = DocumentTypes::TYPE_MISC;

	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'document-dialog-form',
			'action' => Yii::app()->createUrl("/".Yii::app()->controller->module->id."/documents/add", array()),
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'beforeValidate' => 'js:function(form) {
	            $(".loading-container").addClass("loading");
				return true;
            }',
				'afterValidate' => 'js:function(form, data, hasError) {
				if(hasError){
			    	$(".loading-container").removeClass("loading");
			    }
			    return true;
			}',
			),
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)
	);
	?>
	<fieldset>
		<section>
			<?php echo $form->labelEx($model, 'document_type_id'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'document_type_id', CHtml::listData(DocumentTypes::model()->byComponentType(ComponentTypes::CONTACTS)->orderByName()->findAll(), 'id', 'name'), $htmlOptions = array(
					'class' => 'documentTypes chzn-select',
					'data-placeholder' => 'Type Document Type Here...',
				)
				);

				$this->widget('admin_module.extensions.EChosen.EChosen', array(
						'target' => 'select.documentTypes',
						'options' => array(
							'allow_single_deselect' => true,
							'width' => '83%'
						),
					)
				);
				?>

			</div>
			<?php echo $form->error($model, 'document_type_id'); ?>
		</section>
		<section>
			<?php echo $form->labelEx($model, 'file_name'); ?>
			<div>
				<div id="fileReplace-container"><?php echo $form->checkBox($model, 'fileReplaceFlag', false, $htmlOptions = array('style' => 'margin-bottom:10px;font-size:16px;')) ?> Replace File</div>
				<div id="fileField-container">
					<? echo $form->fileField($model, 'file_name', $htmlOptions = array(
						'class' => 'g10',
					)
					); ?>
				</div>
			</div>
			<?php echo $form->error($model, 'file_name'); ?>
		</section>
		<section>
			<?php echo $form->labelEx($model, 'visibility_ma'); ?>
			<div>
				<?php echo $form->dropDownList($model, 'visibility_ma', array(
					Documents::VISIBILITY_COMPONENT => 'Don\'t Share with Client',
					Documents::VISIBILITY_PUBLIC => 'Share with Client'
				), $htmlOptions = array()
				);?>
			</div>
			<?php echo $form->error($model, 'visibility_ma'); ?>
		</section>
		<section>
			<?php echo $form->labelEx($model, 'description'); ?>
			<div>
				<?php echo $form->textField($model, 'description', $htmlOptions = array(
					'placeholder' => 'Description',
					'class' => 'g11',
				)
				); ?>
			</div>
			<?php echo $form->error($model, 'description'); ?>
		</section>
		<?php echo $form->hiddenField($model, 'componentTypeId'); ?>
		<?php echo $form->hiddenField($model, 'componentId'); ?>
		<?php echo $form->hiddenField($model, 'id'); ?>
	</fieldset>
	<div class="submit-button-row p-tc p-p10">
		<input id="document-dialog-submit-button" class="button wide" type="submit" value="{{title}}">
	</div>
	<div class="loading-container"><em></em></div>
	<? $this->endWidget(); ?>
<?='</script>'?>
<?
Yii::app()->clientScript->registerScript('documentListJs', <<<JS
    $('.delete-document-button').live('click', function(){

        if(confirm('Are you sure you want to delete this Document?')) {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            var id = $(this).attr('data-id');
            $.post('/$module/documents/delete/'+id, function(data) {

                $("div.loading-container.loading").remove();
                if(data.status=='success') {
                    Message.create("success","Document Deleted successfully.");
                    $.fn.yiiGridView.update("documents-grid", {
                        data: $(this).serialize()
                    });
                } else
                    Message.create("error","Error: Document did not Delete. Message: " + data.message);
            },"json");
        }
        return false;
    });

	// Bind click event for modal
	$('#add-document-button').click(function() {

        $.when(
            // Create modal
            stmModal.create({
                title: 'Add Document',
                contentVariables: {
                	title: 'Add Document'
                },
                content: $('#template-dialog-form').html(),
                height: 370,
                saveFormDataUrl: "/admin/documents/add",
            })
        ).then( function(){
            $('#Documents_fileReplaceFlag').val(1);
            $('#fileReplace-container').hide();
            $('#document-dialog-form #fileField-container').show();
            $('#document-dialog-form input#Documents_file_name').removeAttr('disabled');
        });
	});

	// Bind edit button
	$('.edit-document-button').on('click', function(e) {
        e.preventDefault();
        $.when(
            // Create modal
            stmModal.create({
                title: 'Edit Document',
                contentVariables: {
                	title: 'Edit Document'
                },
                content: $('#template-dialog-form').html(),
                height: 370,
                loadFormDataUrl: "/admin/documents/view/" + $(this).data('id'),
                saveFormDataUrl: "/admin/documents/edit/" + $(this).data('id'),
                loadFormDataMappings: {
                    'Documents[componentTypeId]': 'component_type_id',
                    //'Documents[componentId]': 'component_id',
                    'Documents[visibility_ma]': 'visibility_ma',
                    'Documents[description]': 'description',
                    'Documents[id]': 'id'
                }
            })
        ).then( function(){
            $('#Documents_fileReplaceFlag').val(0);
            $('#document-dialog-form #fileField-container').hide();
            $('#document-dialog-form input#Documents_file_name').attr('disabled','disabled');
        });
	});

    $('#document-dialog-form #Documents_fileReplaceFlag').live('click', function() {
        if ($(this).is(':checked')) {
            $('#Documents_fileReplaceFlag').val(1);
            $('#document-dialog-form #fileField-container').show('normal');
            $('#document-dialog-form input#Documents_file_name').removeAttr('disabled');
            return;
        } else {
            $('#Documents_fileReplaceFlag').val(0);
            $('#document-dialog-form #fileField-container').hide('normal');
            $('#document-dialog-form input#Documents_file_name').attr('disabled','disabled');
            return;
        }

    });
JS
, CClientScript::POS_END);?>

<?
// Documents Dialog Widget
//$this->widget('admin_widgets.DialogWidget.DocumentsDialogWidget.DocumentsDialogWidget', array(
//	'id'             => 'addDocument',
//	'title'          => 'Add Document',
//	'view'           => 'officeDocumentsDialog',
//	'triggerElement' => '#add-document-button, .edit-document-button', // Trigger for Add Document Button (See DocumentsPortlet.DocumentsPortlet.php)
//	 'parentModel'    => new Contacts,
//	 'componentType'  => $componentType,
//));
?>
<div id="listview-actions">
	<!-- <a href="/<?php echo Yii::app()->controller->module->id; ?>/documents/add" class="button gray icon i_stm_add">Add New Document</a> -->
	<button id="add-document-button" class="button gray icon i_stm_add">Add New Document</button>
	<!-- <a href="#" class="btnGray button i_stm_search icon">Search Contacts</a> -->
</div>
<div id="content-header">
	<h1>Team Documents</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBox',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'            => 'documents-grid',
	'dataProvider'  => $dataProvider,
	'itemsCssClass' => 'datatables',
	'columns' => array(
		'description',
		'file_name',
		array(
			'type'  => 'raw',
			'name'  => 'Type',
			'value' => '$data->documentType->name',
			'htmlOptions'=>array('style'=>'width:90px'),
		),
	    array(
			'type'  => 'raw',
	        'name'  => 'File Size',
	        'value' => 'Yii::app()->format->formatFileSize($data->file_size)',
			'htmlOptions'=>array('style'=>'width:90px'),
		),
		array(
			'type'  => 'raw',
			'name'  => 'Added by',
			'value' => '$data->addedBy->fullName',
			'htmlOptions'=>array('style'=>'width:120px'),
		),
	    array(
			'type'  => 'raw',
	        'name'  => 'Added',
	        'value' => 'Yii::app()->format->formatDateTime($data->added)."<br />".Yii::app()->format->formatDays($data->added, array("daysLabel"=>"days ago"))',
			'htmlOptions'=>array('style'=>'width:160px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"javascript:void(0)\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button edit-document-button\" data-id=\"".$data->id."\">Edit</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:80px'),
        ),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/documents/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:90px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"javascript:void(0)\" data-id=\"".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_delete grey-button delete-document-button\">Delete</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:90px'),
        ),
	),
));