<div class="button-container">
	<button id="add-document-button" class="text" type="button"><em class="icon i_stm_add"></em>Add Document</button>
</div>

<div class="ui-button-panel">
<?
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'documents-grid',
	'htmlOptions'=>array(
		'class'=>'documents-grid'
	 ),
	'template'=>'{items}',
	'enablePagination'=>false,
	'dataProvider'=>$dataProvider,
	'itemsCssClass'=>'datatables',
	'columns'=>array(
	    array(
			'type'=>'raw',
	    	'name'=>'Name',
			'value' => '"<b>".$data->documentType->name."</b><br/>".$data->description."<br/>&nbsp;&nbsp;&nbsp;&nbsp;<i>(File Name: ".$data->file_name.")</i>"',
	    ),
	     array(
			 'type'=>'raw',
	         'name'=>'Size / Type',
	         'value'=>'number_format(($data->file_size/1024))." Kb<br />".$data->file_extension',
			 'htmlOptions'=>array('style'=>'width:75px;'),
		     ),
	    array(
			'type'=>'raw',
	        'name'=>'Added',
	        'htmlOptions'=>array('style'=>'width:75px;'),
	        'value'=>'Yii::app()->format->formatDate($data->added)."<br>".Yii::app()->format->formatTime($data->added)',
		    ),
		array(
			'name'=>'',
			'type'=>'raw',
			'value' => '"<a class=\"btn btn-primary button default edit-document-button\" data-id=\"".$data->id."\">Edit</a>"',
			'htmlOptions'=>array(
				'style'=>'width:50px'
			),
		),
		array(
			'name'=>'',
			'type'=>'raw',
			'value' => '"<a href=\"/".Yii::app()->controller->module->id."/documents/".$data->id."\" class=\"btn btn-primary button default\" target=\"_blank\">View</a>"',
			'htmlOptions'=>array(
				'style'=>'width:50px'
			),
		),
	),
));
?>
</div>