<?php
$this->breadcrumbs = array(
    'Dot Loop' => '',
	'List' => '',
);
?>
<h1>Dot Loop List</h1>

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'            => 'documents-grid',
	'dataProvider'  => $dataProvider,
	'itemsCssClass' => 'datatables',
	'columns' => array(
        array(
            'type'  => 'raw',
            'name'  => 'Loop ID',
            'value' => '$data[loopId]',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Status',
            'value' => '$data[loopStatus]',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Loop Name',
            'value' => '$data[loopName]',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Last Updated',
            'value' => 'Yii::app()->format->formatDate($data[lastUpdated])',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
        array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/documents/dotLoopDetails/".$data["loopViewId"]."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Loop</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:120px'),
		),
	),
));
