<?php
  Yii::app()->clientScript->registerScript('search', "
    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('documents-grid', {
            data: $(this).serialize()
        });
        return false;
    });
  ");

  $form = $this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'id'     => 'document-list-search',
)); ?>
    <div class="g2"></div>
	<div class="g6">
		<label class="g3">Notes:</label>
		<span class="g9"><?php echo $form->textField($model,'description');?></span>
	</div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
