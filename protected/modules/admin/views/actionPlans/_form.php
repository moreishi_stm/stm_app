<?php
$customAssignToScript = <<<JS
	$('#has_dependency').click(function () {
		var onCompleteId = $('#ActionPlans_on_complete_plan_id');
		onCompleteId.slideToggle(function(){
			var style = $(this).attr('style');
			if($('#ActionPlans_on_complete_plan_id').attr('style') == undefined || $('#ActionPlans_on_complete_plan_id').attr('style').indexOf('display: none') ==-1) {
				$(this).removeAttr('disabled');
			} else {
//				$(this).val([]);
				$(this).attr('disabled','disabled');
			}
		});
	});

	$('#add-auto-responder').click(function(){
		var buttonContent = $(this).html();
		if($(this).find('em.icon').hasClass('i_stm_delete')) {
			if(confirm('Confirm deleting this Auto-responder?')) {
				$('#auto-responder-container').toggle('normal');
				buttonContent = buttonContent.replace('Delete Auto Responder','Add Auto Responder');
				buttonContent = buttonContent.replace('i_stm_delete','i_stm_add');
				$('#LeadRouteAutoresponder_type_ma').val('');
				$('#LeadRouteAutoresponder_component_id').val('');
				$(this).html(buttonContent);
			}
		} else {
			$('#auto-responder-container').toggle('normal');
			buttonContent = buttonContent.replace('Add Auto Responder','Delete Auto Responder');
			buttonContent = buttonContent.replace('i_stm_add','i_stm_delete');
			$('#LeadRouteAutoresponder_type_ma').removeAttr('disabled');
			$('#LeadRouteAutoresponder_component_id').removeAttr('disabled');
			$(this).html(buttonContent);
		}
	});
JS;

Yii::app()->clientScript->registerScript('customAssignToScript', $customAssignToScript);

$form = $this->beginWidget('CActiveForm', array(
	'id' =>' add-new-action-plan-form',
	'enableAjaxValidation' => false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'   => 'Action Plan Details',
		'handleIconCss' => 'i_wizard'
	));
	?>
	<div id="action-plans-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
	        <table class="container">
	        	<tr>
	        		<th class="narrow">Status:</th>
	        		<td>
	            		<?php echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive')); ?>
        				<?php echo $form->error($model,'status_ma'); ?>
	            	</td>
	        		<th class="narrow">Component Type:</th>
	        		<td>
						<?php if($this->action->id == 'add' || $crossComponentSafe) {
							echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll(), 'id', 'display_name'), $htmlOptions=array('empty'=>'Select One'));
							echo $form->error($model,'component_type_id');
						} else {
							echo $model->componentType->display_name;
							echo $form->hiddenField($model,'component_type_id');
						}
						?>
						<div class="stm-tool-tip question">
							<span>Remember! Each action plan will only show up for it’s respective type. Example: a sellers action plan will only show up for your sellers and recruits will only show up for recruits. </span>
						</div>
	            	</td>
	        	</tr>
	        	<tr>
	        		<th class="narrow">Name:</th>
	        		<td colspan="3">
			    		<?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Action Plan Name', 'class'=>'g9', ));?>
        				<?php echo $form->error($model,'name'); ?>
	            	</td>
	        	</tr>
                <?php if($this->action->id == 'edit'):?>
		        <tr>
			        <th class="narrow"><?php echo $form->labelEx($model, 'on_complete_plan_id'); ?>:</th>
			        <td colspan="3">
				        <?php echo CHtml::checkBox('has_dependency', (($model->on_complete_plan_id) ? true : false), $htmlOptions=array(
					    )) ;?>
				        <?php echo $form->dropDownList($model, 'on_complete_plan_id', CMap::mergeArray(array(''=>''),CHtml::listData(ActionPlans::model()->orderByName()->findAllByAttributes(array('account_id'=>Yii::app()->user->getAccountId(),'component_type_id'=>$model->component_type_id)), 'id', 'name')), $htmlOptions=array(
							'style' => 'display: '.(($model->on_complete_plan_id) ? "inline" : "none").';',
							'disabled'=>(($model->on_complete_plan_id) ? "" : "disabled"),
					    ));?>
				        <?php echo $form->error($model, 'on_complete_plan_id'); ?>
				        <div class="stm-tool-tip question">
						    <span>Choose an action plan to start automatically after this one ends. This can be used to create a repeating action plan, just be aware that if the first item activates in "0 days" then it will trigger instantly after this action plan ends. </span>
						</div>
			        </td>
		        </tr>
                <?php endif;?>
                <tr>
                    <th class="narrow">Operation Manuals:</th>
                    <td colspan="3">
                        <?php echo $form->dropDownList($model,'operation_manuals', CHtml::listData(OperationManuals::model()->findAll(),'id','name'),$htmlOptions=array('placeholder'=>'Action Plan Name', 'class'=>'g9', 'multiple' => 'multiple', 'data-placeholder' => 'Operation Manuals'));?>
	                    <div class="stm-tool-tip question">
						    <span>Choose an operation manual to be attached to this action plan. You can create operation manuals containing detailed instructions on how to use a resource or perform a task. Click here to <a href="/admin/operationManuals">manage your operation manuals</a>.</span>
						</div>
                        <?php echo $form->error($model,'operation_manuals'); ?>
                        <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ActionPlans_operation_manuals')); ?>
                    </td>
                </tr>
	        </table>

		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper">
		<?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Submit' : 'Save') . ' Action Plan', $htmlOptions = array(
			'class' => 'submit wide',
			'type' => 'submit',
		)); ?>
	</div>
<?php $this->endWidget(); ?>
