<?php
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'action-plans-edrips-grid',
	'htmlOptions'=>array(
		'class'=>'action-plans-edrips-grid'
	 ),
	'template'=>'{items}',
	'enablePagination'=>false,
	'dataProvider'=>$dataProvider,
	'itemsCssClass'=>'datatables',
	'columns'=>array(
	    array(
	        'name'=>'Type',
	        'value'=>'$data->transactionType->name',
        	'htmlOptions'=>array('style'=>'width:50px'),
		    ),
	    array(
	    	'name'=>'Name',
	    	'value'=>'$data->name',
	    ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{complete}',
        	'htmlOptions'=>array(
        		'style'=>'width:50px'
        	 ),
			'buttons'=>array(
				'complete'=>array(
					'label'=>'Apply',
					'raw'=>'true',
            		'url'=>'"/admin/actionPlans/".$data->id',
	            	'options'=>array(
	            		'class'=>'btn btn-primary button default',
	            	 ),
					'click'=>"function() {
						if (!confirm('Complete ?')) return false;
						$.fn.yiiGridView.update('contact-grid', {
							type:'POST',
							url:$(this).attr('href'),
							success:function(text,status) {
								$.fn.yiiGridView.update('contact-grid');
							}
						});
						return false;
					}",
				),
            ),
		),
	),
));