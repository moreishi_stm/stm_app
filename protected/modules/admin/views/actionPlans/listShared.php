<?php
/**
 * @important using external database
 */
StmFunctions::frontConnectDbByClientIdAccountId(30, 1);
$this->breadcrumbs = array(
    'Shared Library' => array('/admin/actionPlans'),
);
$module = Yii::app()->controller->module->name;
$js = <<<JS
        $("#ActionPlans_component_type_id, #ActionPlans_status_ma").change(function(){
            $('#listview-search form').submit();
        });

        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("action-plan-grid", {
                data: $(this).serialize()
            });
            return false;
        });

    // Override button click event
    $('body').delegate('.import-button', 'click', function(e) {

        // Prevent default behavior
        e.preventDefault();

        $('body').prepend('<div class="loading-container loading"><em></em></div>');

        // Retrieve elements
        var tr = $(this).closest('tr');
        var tds = $(tr).children('td');

        // Make sure we want to actually import this
        if(confirm('Are you sure you want to import Action Plan "' + $(tds[2]).html() + '"?')) {

            // do an ajax call and upon success forward to the edit for the new action plan
            $.post('/$module/actionPlans/copyShared/' + $(this).attr('data-id'), function(data) {

                // forward to edit new action plan
                if(data.status == 'success') {
                    window.location.replace("/$module/actionPlans/" + data.id);
                }
                else {
                    $("div.loading-container.loading").remove();
                    Message.create('error', 'Unable to save new action plan.  Error Message: ' + data.message);
                }
            }, 'json');
        }
    });
JS;

Yii::app()->clientScript->registerScript('actionPlanLibraryListScript', $js);
?>

<div id="content-header">
    <h1>Action Plan Shared Library</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $this->renderPartial('_listSearchBox', array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'action-plan-grid',
        'dataProvider' => $model->search(),
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Status',
                'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Type',
                'value' => 'Yii::app()->controller->action->printComponentTypeName($data)',
            ),
            'name',
            array(
                'type' => 'raw',
                'name' => 'Item Count',
                'value' => 'count($data->actionPlanItems)',
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '
                "<div>
                <a href=\"/".Yii::app()->controller->module->name."/actionPlans/copyShared/".$data->id."\" class=\"button gray icon i_stm_edit grey-button import-button\" data-id=\"".$data->id."\" target=\"_blank\">Import</a>
                </div>"
            ',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div>
                <a href=\"/".Yii::app()->controller->module->name."/actionPlans/library/".$data->id."\" class=\"button gray icon i_stm_search grey-button \" data-id=\"".$data->id."\" target=\"_blank\">View</a>
                </div>"
            ',
				'htmlOptions' => array('style' => 'width:80px'),
			)
        ),
    )
);
StmFunctions::frontConnectDbByClientIdAccountId(Yii::app()->user->clientId, Yii::app()->user->accountId);
?>

