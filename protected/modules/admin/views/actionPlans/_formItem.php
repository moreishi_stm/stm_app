<?php
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'contact-form',
			'enableAjaxValidation' => false,
		)
	);

	$specificUserOptionId = AssignmentTypes::SPECIFIC_USER;

	$customAssignToScript = <<<JS

	$('#ActionPlanItems_assign_to_type_ma').change(function (event) {
		var currentlySelectedItem = $('#ActionPlanItems_assign_to_type_ma option:selected').val();

		var userSelector = $('#ActionPlanItems_specific_user_id');

		if (currentlySelectedItem == $specificUserOptionId) {
			userSelector.show('normal');
		} else {
			userSelector.hide('normal');
		}
	});

	//Fire a change so the drop down is initially visible or hidden as appropriate
	$('#ActionPlanItems_assign_to_type_ma').change();
JS;

	Yii::app()->clientScript->registerScript('customAssignToScript', $customAssignToScript);

	$emailTaskTypeId = TaskTypes::AUTO_EMAIL_DRIP;
	$letterTaskId = TaskTypes::LETTER;
	$emailActionPlanCustomScript = <<<JS
	$('#ActionPlanItems_task_type_id').change(function (event) {

		var currentlySelectedItem = $('#ActionPlanItems_task_type_id option:selected').val();

		var emailTemplateSelector = $('#ActionPlanItems_email_template_id');

		if (currentlySelectedItem == $emailTaskTypeId) {
			emailTemplateSelector.removeAttr('disabled');
            emailTemplateSelector.chosen({enable_split_word_search:true, search_contains:true, width:"62%"}); //'enable_split_word_search'=>true,'search_contains'=>true,'width'=>'74%'
            $('#ActionPlanItems_email_template_id_chzn').hide().show("normal");
		} else {
            $('#ActionPlanItems_email_template_id_chzn').hide("normal");
            emailTemplateSelector.trigger("liszt:updated");
			emailTemplateSelector.hide('normal');
			emailTemplateSelector.attr('disabled','disabled');
		}
		currentlySelectedItem = $('#ActionPlanItems_task_type_id option:selected').val();
		// I re used the email selector because i didnt see any benefeit to creating a letter selector variable
		// Jamesk 4/16/2016
		emailTemplateSelector = $('#ActionPlanItems_letter_template_id');
		if (currentlySelectedItem == $letterTaskId) {
			emailTemplateSelector.removeAttr('disabled');
            emailTemplateSelector.chosen({enable_split_word_search:true, search_contains:true, width:"62%"}); //'enable_split_word_search'=>true,'search_contains'=>true,'width'=>'74%'
            $('#ActionPlanItems_letter_template_id_chzn').hide().show("normal");
		} else {
            $('#ActionPlanItems_letter_template_id_chzn').hide("normal");
            emailTemplateSelector.trigger("liszt:updated");
			emailTemplateSelector.hide('normal');
			emailTemplateSelector.attr('disabled','disabled');
		}
	});

	//Fire a change so the drop down is initially visible or hidden as appropriate
	$('#ActionPlanItems_task_type_id').change();
JS;

	Yii::app()->clientScript->registerScript('emailActionPlanCustomScript', $emailActionPlanCustomScript);

	$taskCompletionItem = ActionPlanItems::DUE_REFERENCE_UPON_COMPLETION_OF_TASK;

	$specificDateID = ActionPlanItems::DUE_REFERENCE_SPECIFIC_DATE_ID;

	$parentActionPlanItemSelectorScript = <<<JS
	$('#ActionPlanItems_due_reference_ma').change(function (event) {
		var currentlySelectedItem = $('#ActionPlanItems_due_reference_ma option:selected').val();
		var actionPlanItemSelector = $('tr#depends_on_row');

		if (currentlySelectedItem == $taskCompletionItem) {
			actionPlanItemSelector.show();
		} else {
			actionPlanItemSelector.hide();
			actionPlanItemSelector.find('select').prop('selectedIndex', 0);
		}

		if($(this).val() == {$specificDateID}) {
			$("#normalDays").hide();
			$("#datePickerContainer").show();
		} else {
			$("#datePickerContainer").hide();
			$("#normalDays").show();
		}
	});

	//Fire a change so the drop down is initially visible or hidden as appropriate
	$('#ActionPlanItems_due_reference_ma').change();
JS;

	Yii::app()->clientScript->registerScript('parentActionPlanItemSelectorScript', $parentActionPlanItemSelectorScript);




	$this->beginStmPortletContent(array(
			'handleTitle' => 'Action Plan Item Details',
			'handleIconCss' => 'i_wizard'
		)
	);

?>
<div id="action-plans-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container">
			<tr>
				<th class="narrow"><?php echo $form->labelEx($model, 'status_ma'); ?></th>
				<td>
					<?php
						echo $form->dropDownList($model, 'status_ma', array(
								1 => 'Active',
								0 => 'Inactive'
							)
						);
					?>
					<div class="stm-tool-tip question">
					    <span>If set to Inactive, this action item will not be executed in the action plan.</span>
					</div>
					<?php echo $form->error($model, 'status_ma'); ?>
				</td>
			</tr>
<!--            <tr>-->
<!--                <th class="narrow">--><?php //echo $form->labelEx($model, 'status_ma'); ?><!--</th>-->
<!--                <td>-->
<!--                    --><?php
//                    echo $form->dropDownList($model, 'status_ma', array(
//                            1 => 'Active',
//                            0 => 'Inactive'
//                        )
//                    );
//                    ?>
<!--                    --><?php //echo $form->error($model, 'status_ma'); ?>
<!--                </td>-->
<!--            </tr>-->
			<tr>
				<th class="narrow"><?php echo $form->labelEx($model, 'task_type_id'); ?></th>
				<td>
					<?php echo $form->dropDownList($model, 'task_type_id', TaskTypeComponentLu::getDropDownListByComponentTypeId($model->actionPlan->component_type_id, $addEmailDrip=true),
						$htmlOptions = array(
							'empty' => 'Select One'
						)
					); ?>
<!--                    --><?php //$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ActionPlanItems_email_template_id','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'width'=>'74%'))); ?>

					<?php
					echo $form->dropDownList(
						$model,
						'email_template_id',
						CHtml::listData(
							EmailTemplates::getByComponentTypeId($model->actionPlan->component_type_id),
							'id',
							'dropDownDisplay'
						),
						$htmlOptions = array('empty' => 'Select an email template')
					);
					echo $form->dropDownList(
						$model,
						'letter_template_id',
						CHtml::listData(
							LetterTemplates::getByComponentTypeId($model->actionPlan->component_type_id),
							'id',
							'dropDownDisplay'
						),
						$htmlOptions = array('empty' => 'Select letter template')
					);

					?>
					<div class="stm-tool-tip question">
					    <span>The type of task to apply. NOTE: "Phone" task is recognized by the Dialer and will automatically integrate to enable you to follow-up through the dialer 3x faster.</span>
					</div>
					<?php echo $form->error($model, 'task_type_id'); ?>
					<?php echo $form->error($model, 'email_template_id'); ?>
					<?php echo $form->error($model, 'letter_template_id'); ?>

				</td>
			</tr>
			<tr>
				<th class="narrow"><?php echo $form->labelEx($model, 'description'); ?></th>
				<td>
					<?php echo $form->textField($model, 'description', $htmlOptions = array(
							'placeholder' => 'Description',
                            'maxlength'=> 750,
							'class' => 'g9',
						)
					); ?>
					<?php echo $form->error($model, 'description'); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow"><?php echo $form->labelEx($model, 'due_days'); ?></th>
				<td>
					<span id="datePickerContainer"<?php if($model->due_reference_ma != $specificDateID):?> style="display: none;"<?php endif; ?>>
						<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
                                'attribute' => 'specific_due_date',
								// name of post parameter
								'options' => array(
									'showAnim' => 'fold',
									'dateFormat' => 'mm/dd',
									'numberOfMonths' => 2,
								),
								'htmlOptions' => array(
									'style' => 'width:90px;font-size: 12px;',
									'class' => 'g5',
								),
							)
						);
						?>
					</span>
					<span id="normalDays"<?php if($model->due_reference_ma == $specificDateID):?> style="display: none;"<?php endif ;?>>
						<?php echo $form->textField($model, 'due_days', $htmlOptions = array(
								'placeholder' => 'Days',
								'class' => 'g1',
							)
						); ?><label class="label"> days</label>

						<?php echo $form->dropDownList($model, 'due_days_type_ma', ActionPlanItems::model()->getDueDaysTypes(), $htmlOptions = array('empty' => 'Select One',)); ?>
					</span>
					<?php echo $form->dropDownList($model, 'due_reference_ma', $model->getDueReferences(), $htmlOptions = array('empty' => 'Select One',)); ?>
					<div class="stm-tool-tip question">
					    <span>When the task will show as due. If the task type is set to Email Drip (Auto), this determine when the email is sent out.</span>
					</div>
					<?php echo $form->error($model, 'due_days'); ?><?php echo $form->error($model, 'due_days_type_ma'); ?><?php echo $form->error($model, 'due_reference_ma'); ?>
				</td>
			</tr>
			<tr id="depends_on_row">
				<th class="narrow"><?php echo $form->labelEx($model, 'depends_on_id'); ?></th>
				<td>
					<?php echo $form->dropDownList($model, 'depends_on_id', CHtml::listData(ActionPlanItems::model()->byActionPlan($model->action_plan_id)->byExcludeId($model->id)->findAll(), 'id', 'dropDownDisplay'),
						$htmlOptions = array(
							'empty' => 'Select A Parent Task',
//                            'class' => 'chzn-select g10',
						)
					); ?>
					<?php echo $form->error($model, 'depends_on_id'); ?>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ActionPlanItems_depends_on_id','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'width'=>'74%'))); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow"><?php echo $form->labelEx($model, 'assign_to_type_ma'); ?></th>
				<td>
					<?php echo $form->dropDownList($model, 'assign_to_type_ma', ActionPlanItems::getAssignmentTypes($model->actionPlan->component_type_id), $htmlOptions = array()); ?>
					<?php echo $form->dropDownList($model, 'specific_user_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->specific_user_id)->findAll(), 'id', 'fullPhoneticName'),
						$htmlOptions = array('empty' => 'Select One')
					); ?>
					<div class="stm-tool-tip question">
					    <span>When this Action Item is triggered, the task will be applied to this user. </span>
					</div>
					<?php echo $form->error($model, 'assign_to_type_ma'); ?>
					<?php echo $form->error($model, 'specific_user_id'); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow"><?php echo $form->labelEx($model, 'if_weekend') ?></th>
				<td>
					<?php echo $form->dropDownList($model, 'if_weekend', ActionPlanItems::getIfWeekends(), $htmlOptions = array()); ?>
					<div class="stm-tool-tip question">
					    <span>The weekends are not always the best time to do a task (e.g. a phone call to a client/prospect). If this is the case for your task, use this option to move to to the next or previous weekday.</span>
					</div>
					<?php echo $form->error($model, 'if_weekend'); ?>
				</td>
			</tr>
            <tr>
                <th class="narrow">Operation Manuals:</th>
                <td colspan="3">
                    <?php echo $form->dropDownList($model,'operation_manual_id', CHtml::listData(OperationManuals::model()->findAll(),'id','name'),$htmlOptions=array('class'=>'g9', 'data-placeholder' => 'Select Operation Manuals','empty'=>''));?>
                    <div class="stm-tool-tip question">
					    <span>Choose an operation manual to be attached to this action plan. You can create operation manuals containing detailed instructions on how to use a resource or perform a task. Click here to <a href="/admin/operationManuals">manage your operation manuals</a>.</span>
					</div>
                    <?php echo $form->error($model,'operation_manual_id'); ?>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ActionPlanItems_operation_manual_id','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'allow_single_deselect' => true))); ?>
                </td>
            </tr>

            <!-- 	        	<tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'sort_order')?></th>
                    <td>
                        <?php echo $form->textField($model,'sort_order', $htmlOptions=array('placeholder'=>'Days', 'class'=>'g1', ));?>
                        <?php echo $form->error($model,'sort_order'); ?>
                    </td>
                </tr>
    -->            </table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
	<?php echo CHtml::htmlButton((($model->id) ? 'Save' : 'Submit') . ' Action Plan Item', $htmlOptions = array(
			'class' => 'submit wide',
			'type' => 'submit',
		)
	); ?>
</div>
<?php $this->endWidget(); ?>
