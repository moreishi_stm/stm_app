<?php
	$this->breadcrumbs = array(
		'List' => array('/admin/actionPlans'),
	);
    $module = Yii::app()->controller->module->name;
    $js = <<<JS
        $("#ActionPlans_component_type_id, #ActionPlans_status_ma").change(function(){
            $('#listview-search form').submit();
        });

        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("action-plan-grid", {
                data: $(this).serialize()
            });
            return false;
        });

        $( '.delete-action-plan-button' ).live( "click", function() {
            if(confirm('Are you sure you want to delete this Action Plan?')) {
                $("body").prepend("<div class='loading-container loading'><em></em></div>");
                var id = $(this).data('id');
                $.post('/$module/actionPlans/delete/'+id, function(data) {
                    $("div.loading-container.loading").remove();
                    if(data=='') {
                        Message.create("success","Action Plan Item deleted successfully.");
                        $.fn.yiiGridView.update("action-plan-grid", { data: $(this).serialize() });
                    } else {
                        Message.create("error","Error: Action Plan did not delete.");
                    }
                },"json");
            }
        });
JS;

    Yii::app()->clientScript->registerScript('actionPlanListScript', $js);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name;?>/actionPlans/add" class="button gray icon i_stm_add">Add Action Plan</a>
    <a href="/<?php echo Yii::app()->controller->module->name;?>/actionPlans/listShared" class="button gray icon i_stm_search">Action Plan Library</a>
    <div class="stm-tool-tip question left">
        <span>Use action plans from our library that are already set up for you!</span>
    </div>
</div>
<div id="content-header">
	<h1>Action Plans List</h1>
    <h4><a href="/admin/support/video/16">Click here for a tutorial on action plans</a></h4>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBox', array(
			'model' => $model,
		)
	); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'action-plan-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Status',
				'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)',
			),
			array(
				'type' => 'raw',
				'name' => 'Type',
				'value' => 'Yii::app()->controller->action->printComponentTypeName($data)',
			),
			'name',
            array(
                'type' => 'raw',
                'name' => 'Item Count',
                'value' => 'count($data->actionPlanItems)',
            ),
            array(
                'type' => 'raw',
                'name' => 'ID',
                'value' => '$data->id',
                'htmlOptions' => array('style' => 'width:20px'),
            ),
            array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->name."/actionPlans/".$data->id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a></div>"
            ',
				'htmlOptions' => array('style' => 'width:80px'),
			),
            array(
                'type'=>'raw',
                'name'=>'',
                'value'=>'
				"<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button delete-action-plan-button \" data-id=\"".$data->id."\">Delete</a></div>"
			',
                'htmlOptions'=>array('style'=>'width:90px'),
            ),
		),
	)
);
?>
