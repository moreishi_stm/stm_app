<?php if($index==1) {
    echo '<thead>';
        echo '<tr>';
            echo '<th></th>';
            echo '<th>Task Type</th>';
            echo '<th>Description</th>';
            echo '<th>Due</th>';
            echo '<th>Due Date Type</th>';
            echo '<th>Assigned to</th>';
            echo '<th>ID</th>';
            echo '<th></th>';
            echo '<th></th>';
        echo '</tr>';
    echo '</thead>';
}
?>
<tr class="<?php echo ($dependent) ? 'even' : 'even' ?> row" <?php echo ($dependent==true)? 'style="background-image:white;"':''?>>
    <td><?php echo ''; echo ($index!==false && ($dependent!=true))? ($index+1).')' : ''; ?></td>
    <td><?php echo $data->taskType->name;?></td>
    <td><?php
        if($data->task_type_id==TaskTypes::AUTO_EMAIL_DRIP) {
            $emailInfo = ' <br /><br /><a href="/'.Yii::app()->controller->module->id.'/emailTemplates/edit/'.$data->emailTemplate->id.'" target="_blank">Email Template Info:<br />Subject: <strong><u>'.$data->emailTemplate->subject.'</u> (View Email)</a></strong><br />Description: '.$data->emailTemplate->description;
        }
        echo $data->description.$emailInfo;
        if($data->operation_manual_id) {
            echo ' <br /><br /><a href="/'.Yii::app()->controller->module->id.'/operationManuals/'.$data->operationManual->id.'" target="_blank">Operation Manual: <u>'.$data->operationManual->name.'</u> (View Manual)</a><br />';
        }
        ?>
        <?php echo ($dependent)?'<br />(This task is triggered by another) See Reference Date':'';
        ?>
    </td>
    <td><?php
        if($data->due_days_type_ma == ActionPlanItems::DUE_REFERENCE_SPECIFIC_DATE_ID) {
            echo $data->specific_due_date;
        } else {
            echo $data->due_days.(($data->due_days)? " Days":" Day").' '.$data->getModelAttribute("getDueDaysTypes",$data->due_days_type_ma);
        }
        ?></td>
    <td><?php echo Yii::app()->controller->action->printReferenceDateGrid($data); ?></td>
    <td><?php echo $data->getModelAttribute("getAssignTos", $data->assign_to_type_ma);
            echo ($data->assign_to_type_ma == AssignmentTypes::SPECIFIC_USER)? ':<br>'.$data->specificUser->fullName.(($data->specificUser->phonetic_name)? ' ('.$data->specificUser->phonetic_name.')':''):'';
        ?>
    </td>
    <td><?php echo $data->id;?></td>

    <td style="width:80px;">
        <?php echo '<div><a href="/'.Yii::app()->controller->module->name.'/actionPlans/editItems/'.$data->id.'" class="button gray icon i_stm_edit grey-button">Edit</a></div>';?>
    </td>
    <td style="width:90px;">
        <?php
        $dependentCount = count($data->dependentItems);
        echo '<div><a href="javascript:void(0)" class="button gray icon i_stm_delete grey-button delete-action-plan-item-button " data-id="'.$data->id.'" data-dependents="'.$dependentCount.'">Delete</a></div>';?>
    </td>
</tr>
<?php
if($dependentItems = $data->dependentItems) {
    //****@todo: build out the array of actionplanitem ids with all dependencies, then call render partial separately so nesting max is not hit. ****
    foreach($dependentItems as $i =>$dependentItem) {
        // @todo: remove this sanity check after verify that no action plan items has depends_on_id AND due_reference_ma <> 6. Time combo existed on accident, looks like actions are fixed. Leaving this here for now just in case.

        //@todo: check to see if it's dependent on anything and if it's deleted.
        if ($dependentItem->due_reference_ma == ActionPlanItems::DUE_REFERENCE_UPON_COMPLETION_OF_TASK) {
            $this->renderPartial('_actionPlanItemRow', array('data'=>$dependentItem, 'dependent'=>true, 'index'=>false));
        }
    }
}

?>