<?php
$this->breadcrumbs = array(
	(($this->action->id == 'edit')?$model->name:'Add New Action Plan') => '/'.$this->module->id.'/actionPlans/'.$model->id,
);

if ($this->action->id == 'edit')
	$this->breadcrumbs = CMap::mergeArray($this->breadcrumbs, array(ucfirst($this->action->id) => ''));
?>

<div id="action-plans-header">
	<h1 class="name"><?php echo ($model->isNewRecord) ? 'Add New Action Plan' : $model->name;?></h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'crossComponentSafe' => $crossComponentSafe)); ?>
