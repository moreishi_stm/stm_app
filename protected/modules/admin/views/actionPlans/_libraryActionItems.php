<style>
    .grid-view .summary {
        margin: 0px 0px 5px;
        text-align: right;
    }
</style>
<?php
$module = Yii::app()->controller->module->name;
?>

<div class="p-clr p-p10" style="margin-bottom: -17px; position: relative; padding-top:30px;">
	<h2>Action Plan Items</h2>
</div>
<div id="listview-actions">
	<a data-id="<?php echo $id; ?>" id="copy-button" class="button gray icon i_stm_add">Copy</a>
</div>

<?php $this->widget('front_module.components.StmListView', array(
        'dataProvider' => $model,
        'template' => '{pager}{summary}{items}{pager}{summary}',
        'id' => 'action-plan-items-grid',
        'htmlOptions' => array('class'=>'grid-view'),
        'ajaxUpdate' => true,
        'itemsTagName'=>'table',
        'itemView' => '_libraryItemRow',
        'itemsCssClass' => 'datatables',
    )
);
?>
