<?php
	$this->breadcrumbs = array(
		$model->name => '',
	);
    $actionPlanId = $model->id;
    $module = Yii::app()->controller->module->name;
    Yii::app()->clientScript->registerScript('actionPlanViewScript', <<<JS
        $('#copy-button').click(function(){

            if(confirm('Are you sure you want to copy this Action Plan?')) {
                $("body").prepend("<div class='loading-container loading'><em></em></div>");
                var id = $(this).data('id');
                $.post('/$module/actionPlans/copy/'+id, function(data) {
                    $("div.loading-container.loading").remove();
                    if(data.status=='success') {
                        Message.create("success","Action Plan Item copied successfully.");

                        // go to the copied action plan
                        window.location = "/$module/actionPlans/" + data.message;
                    } else
                        Message.create("error","Error: Action Plan Item did copy. Message: " + data.message);
                },"json");
            }
        });
JS
);
?>
    <br/>

    <?if($_GET['copy']==1):?>
        <div class="g2"></div>
        <div class="g8" style="background: #c0ffc0;">
            <span class="g12" style="font-size: 15px; font-weight: bold; text-align: center; padding: 20px;">Action Plan Copied Successfully. Please update the Action Plan name.</span>
        </div>
    <?endif;?>

	<br/>

	<div class="p-tc p-f0 g12">

        <?php
        $this->beginStmPortletContent(array(
                'handleTitle' => 'Action Plan Details',
                'handleIconCss' => 'i_wizard',
                'handleButtons' => array(
                    array(
                        'label' => 'Edit',
                        'iconCssClass' => 'i_stm_edit',
                        'htmlOptions' => array('id' => 'edit-contact-button'),
                        'name' => 'edit-contact',
                        'type' => 'link',
                        'link' => '/'.Yii::app()->controller->module->name.'/actionPlans/edit/' . $model->id,
                    ),
                    array(
                        'label' => 'Copy',
                        'iconCssClass' => 'i_stm_add',
                        'htmlOptions' => array('id' => (Yii::app()->user->checkAccess('owner')? 'copy-button': ''), 'data-id'=>$model->id, 'style'=>(Yii::app()->user->checkAccess('owner')? '':'display:none;')),
                        'name' => 'copy-action-plan',
                        'type' => 'link',
                        'link' => 'javascript:void(0)',
                    )
                ),
            )
        );
        ?>
        <div id="action-plans-container">
            <div class="g12 p-mb5 rounded-text-box odd-static">
                <h2 class="p-pv10"><?php echo $model->name ?></h2>
                <table class="container p-mt10">
                    <tr>
                        <th class="narrow">Status:</th>
                        <td><?php echo StmFormHelper::getStatusBooleanName($model->status_ma) ?></td>
                        <th class="narrow">Component Type:</th>
                        <td>
                            <?php echo $model->componentType->display_name ?>
                            <div class="stm-tool-tip question">
                                <span>Each action plan will only show up for it’s respective type. Example: a sellers action plan will only show up for your sellers, and recruits will only show up for recruits. </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow">On Complete, start action plan:</th>
                        <td><?php echo $model->onCompleteActionPlan->name; ?></td>
                        <th class="narrow"></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th class="narrow">Operation Manuals:<br />(Click to view)</th>
                        <td>
                            <?php
                            if(!empty($model->operation_manuals)) {
                                foreach($model->operation_manuals as $operationManualId) {
                                    echo '<a href="/'.$this->module->id.'/operationManuals/'.$model->id.'" target="_blank">'.OperationManuals::model()->findByPk($operationManualId)->name.'</a><br />';
                                }
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <?php $this->endStmPortletContent(); ?>
	</div>
<?php
	// Contact-top-section
	echo $this->renderPartial('_viewBottom', array(
			'model' => $actionPlanItemsModel,
			'id' => $model->id,
		)
	);
?>