<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'action-plan-list-search',
)); ?>
    <div class="g3">
        <label class="g6">Status:</label>
            <span class="g6">
                <?php echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive'), $htmlOptions=array('empty'=>'Select One')); ?>
            </span>
    </div>
	<div class="g2">
		<label class="g5">Category:</label>
		<span class="g7">
    		<?php echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll(), 'id', 'display_name'), $htmlOptions=array('empty'=>'Select One')); ?>
		</span>
	</div>
	<div class="g4">
		<label class="g2">Name:</label>
		<span class="g10"><?php echo $form->textField($model,'name');?></span>
	</div>
	<div class="g3 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
