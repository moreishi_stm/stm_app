<?php
$this->breadcrumbs = array(
	$model->actionPlan->name => '/admin/actionPlans/'.$model->action_plan_id,
	ucfirst($this->action->id) => '',
);

if ($this->action->id == 'edit') {
	CMap::mergeArray($this->breadcrumbs, array($model->name => array('/admin/users/' . $model->id),));
}

?>
<div id="action-plans-header">
	<h1><?php echo $model->actionPlan->name; ?></h1>
</div>

<?php echo $this->renderPartial('_formItem', array('model' => $model)); ?>
