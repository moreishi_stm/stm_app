<div class="button-container">
	<button id="add-action-plan-button" class="text" type="button"><em class="icon i_stm_add"></em>Add Action Plan</button>
</div>

<div class="ui-button-panel">
	<?php
        $module = Yii::app()->controller->module->name;

		Yii::app()->clientScript->registerScript('unapplyActionPlanScript', <<<JS
	        $(".unapplyBtn").live("click",function (e) {
	            e.preventDefault();

	            if (confirm("Unapply this action plan?")) {

	            	$("body").prepend("<div class='loading-container loading'><em></em></div>");

                    $.post($(this).attr("href"), function(data) {

                        if(data.status=='success') {
                            Message.create("success", "Action Plan Unapplied Successfully!");
                            location.reload();
                        } else {
                            Message.create("error", "Error: Action Plan did not unapply. " + data.message);
        	                $("div.loading-container.loading").remove();
                        }
                    },"json");
	            }

	            return false;
	        });
JS
);

		$this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'mini-action-plans-grid',
				'htmlOptions' => array(
					'class' => 'mini-action-plans-grid'
				),
				'template' => '{items}',
				'dataProvider' => $dataProvider,
				'itemsCssClass' => 'datatables',
				'columns' => array(
					array(
						'name' => 'Type',
						'value' => '$data->componentType->display_name',
						'htmlOptions' => array('style' => 'width:50px'),
					),
					array(
						'name' => 'Name',
						'value' => '$data->name',
					),

					array(
						'class' => 'CButtonColumn',
						'template' => '{unapply}',
						'htmlOptions' => array(
							'style' => 'width:50px',
						),
						'buttons' => array(
							'unapply' => array(
								'label' => 'Unapply',
								'visible' => '!$data->getIsCompleted('.$parentComponentId.')',
								'options' => array(
									'class' => 'btn btn-primary button default unapplyBtn',
								),
								'url' =>'Yii::app()->createUrl("/'.$module.'/actionPlans/unapply", array("id"=>$data->id, "componentId"=> ' . $parentComponentId . ', "componentTypeId" => ' . $parentComponentTypeId . '))'
							),
						),
					),
				),
			)
		);
	?>
</div>