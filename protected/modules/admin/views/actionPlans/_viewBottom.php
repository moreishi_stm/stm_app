<style>
    .grid-view .summary {
        margin: 0px 0px 5px;
        text-align: right;
    }
</style>
<?php
$module = Yii::app()->controller->module->name;
$deleteScript = <<<JS
	$( '.delete-action-plan-item-button' ).live( "click", function() {
        if($(this).data('dependents') > '0') {
            alert('This has '+$(this).data('dependents')+' dependent Action Plan Item(s). Please remove before deleting.');
            return;
        }


		if(confirm('Are you sure you want to delete this Action Plan Item?')) { // Any dependent task will also be deleted. Please update dependent tasks before deleting.??????????
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var id = $(this).data('id');
			$.post('/$module/actionPlans/itemsDelete/'+id, function(data) {
				$("div.loading-container.loading").remove();
				if(data=='') {
					Message.create("success","Action Plan Item deleted successfully.");
                    $.fn.yiiListView.update("action-plan-items-grid");
				} else
					Message.create("error","Error: Action Plan Item did not delete.");
			},"json");
		}
	});
JS;

Yii::app()->clientScript->registerScript('actionPlanItemDeleteScript', $deleteScript);

?>

<div class="p-clr p-p10" style="margin-bottom: -17px; position: relative; padding-top:30px;">
	<h2>Action Plan Items</h2>
    <h4 style="padding-bottom:10px">Click on the “Add Action Item” button on the top right to create action plan items. Each action plan item will create tasks when you apply the plan. </h4>
</div>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name; ?>/actionPlans/addItem/<?php echo $id; ?>" class="button gray icon i_stm_add">Add Action Item</a>
    <a href="/<?php echo Yii::app()->controller->module->name; ?>/actionPlans/listShared" class="button gray icon i_stm_search">Action Plan Library</a>
</div>

<?php $this->widget('front_module.components.StmListView', array(
        'dataProvider' => $model->search(),
        'template' => '{pager}{summary}{items}{pager}{summary}',
        'id' => 'action-plan-items-grid',
        'htmlOptions' => array('class'=>'grid-view'),
        'ajaxUpdate' => true,
        'itemsTagName'=>'table',
        'itemView' => '_actionPlanItemRow',
        'itemsCssClass' => 'datatables',
    )
);
?>
