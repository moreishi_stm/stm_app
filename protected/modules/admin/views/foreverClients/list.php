<?php
$this->breadcrumbs = array(
	'List' => '',
);
Yii::app()->clientScript->registerCss('foreverClientListCss', <<<CSS
.email-body-container {
    width: 800px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 50px;
    margin-bottom: 50px;
    background: white;
    padding: 60px;
    font-size: 13px;
    color: black;
    border: 1px solid #DDD;
}
.email-body-container p, .email-body-container ul {
    font-size: 13px;
    line-height: inherit;
}
.email-body-container ul {
    margin-top: 1em !important;
    margin-bottom: 1em !important;
}
.email-body-container table {
    background: white;
}
form .button {
    font-size: 11px !important;
    min-height: 30px !important;
}
.ui_tpicker_minute {
    display: none;
}
.ui_tpicker_hour {
    margin-left: 73px !important;
}
CSS
);

$module = Yii::app()->controller->module->id;
$contactId = Yii::app()->user->id;
Yii::app()->clientScript->registerScript('search', <<<JS
    $('#forever-client-fields input, #forever-client-fields select').change(function(){
        $('#listview-search form').submit();
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('past-clients-grid', {
            data: $(this).serialize()
        });
        $('.email-blast-container').hide('normal');
        $('.email-blast-button').show('normal');
        $('.email-blast-container select, .email-blast-container input:not("#send-email-blast-button")').val('');
        $('.email-body-container').html('').hide('normal');
        $('.email-blast-details-container').hide();

        return false;
    });

    $('.export-button').click(function(){
        if(confirm("Are you sure you want to export this list?")) {
            $('body').prepend('<div class="loading-container loading"><em></em></div>');
            $('#export').val(1);

            window.open('/$module/foreverClients?' + $('#listview-search form').serialize());
            setTimeout(function () {
                $('div.loading-container.loading').remove();
                Message.create("notice", "Export is processing in the new window. Please wait...");
            }, 5000);
        }
        $('#export').val(0);
        return false;
    });

    $('.dialer-button').click(function(){
        $('#dialerProcess').val(1);

        if($('#dialerAction').val() == '') {
            alert('Please select option to Add or Remove from Dialer list.');
            $('#dialerProcess').val(0);
            return false;
        }

        if($('#dialerListId').val() == '') {
            alert('Please select a Dialer List or create one if needed.');
            $('#dialerProcess').val(0);
            return false;
        }

        $.fn.yiiGridView.update('past-clients-grid', {
            data: $('#listview-search form').serialize()
        });
        $('#dialerProcess').val(0);
        return false;
    });

    $('.email-blast-button').click(function(){
        $('.email-blast-container').show('normal');
        $('.email-blast-instructions').show('normal');
        $(this).hide('normal');
    });

    $('#EmailBlasts_email_template_id').change(function() {

        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.post('/admin/emailTemplates/view/' + $(this).val() + '?contactId=' + $contactId,  function (data){
            $('#EmailBlasts_email_subject').val(data.subject);
            $('#EmailBlasts_description').val(data.description);

            emailTemplateBody(data.content + data.signature, data.id);
            $('.email-blast-details-container').show('normal');
            $("div.loading-container.loading").remove();
        }, 'json');
    });

    $('#send-email-blast-button').click(function(){
        if($('#EmailBlasts_email_subject').val()== '') {
            alert('Enter a valid Email Subject.');
            return false;
        }

        if($('#EmailBlasts_sender_name').val()== '' || $('#EmailBlasts_sender_name').val().length < 6) {
            alert('Enter a valid Sender Name.');
            return false;
        }

        if($('#EmailBlasts_sender_email_id').val()== '') {
            alert('Select a From Email.');
            return false;
        }

        if($('#EmailBlasts_send_datetime').val()== '') {
            alert('Select a Send Date/Time.');
            return false;
        }

        if($('#EmailBlasts_send_datetime').val().length != 19) {
            alert('Select a valid Send Date/Time.');
            return false;
        }

        if(confirm('Confirm submitting the Forever Client Email Blast.')) {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");

            $.post('/admin/foreverClients/scheduleEmailBlast', $( "#listview-search form" ).serialize(), function (data){

                if(data.status=='success') {
                    Message.create('success', 'Successfully scheduled Email Blast.');
                } else if(data.status=='error') {
                    Message.create('error', data.message);
                }
                $("div.loading-container.loading").remove();
            }, 'json');
        }

        return false;
    });

    function emailTemplateBody(bodyHtml, templateId) {
            bodyHtml = '<h2>Preview Email Message</h2><hr><br><br>' + bodyHtml;
            bodyHtml += '<br><br><hr><div style="margin-top: 30px;"><a href="/admin/emailTemplates/edit/'+ templateId +'" class="button gray icon i_stm_edit" target="_blank">Edit Email Template</a><br>NOTE: Click to edit this email template. Refresh this page to update the Email Preview.';
            $('.email-body-container').html(bodyHtml);
            $('.email-body-container').show('normal');
    }
JS
);
?>
<h1>Forever Clients</h1>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">

    <?php $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
            'id' => 'past-clients-search',
        )
    ); ?>
    <div class="g12">
        <div id="forever-client-fields">

            <div class="g3">
                <label class="g3">Name:</label>
                <span class="g4"><?php echo CHtml::textField('firstName', null, $htmlOptions = array('placeholder' => 'First Name')); ?></span>
                <span class="g4 p-ml10"><?php echo CHtml::textField('lastName', null, $htmlOptions = array('placeholder' => 'Last Name')); ?></span>
            </div>
            <div class="g4">
                <label class="g4">Address:</label>
                <span class="g6"><?php echo CHtml::textField('address', null, $htmlOptions = array('placeholder' => 'Address')); ?></span>
            </div>
            <div class="g3">
                <label class="g4">Email:</label>
                <span class="g8"><?php echo CHtml::textField('email', null, $htmlOptions = array('placeholder' => 'Email')); ?></span>
            </div>
            <div class="g2 submit" style="text-align:center">
                <?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?>
            </div>


            <div class="g3">
                <label class="g3">Status:</label>
                <span class="g8"><?php echo CHtml::dropDownList('closingStatus', Closings::STATUS_TYPE_CLOSED_ID, Closings::getStatusTypes(), $htmlOptions = array()); ?></span>
            </div>
            <div class="g4">
                <label class="g4">Closing Date:</label>
                <span class="g8">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name' => 'closingDateFrom',
                            // name of post parameter
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'mm/dd/yy',
                                'numberOfMonths' => 2,
                            ),
                            'htmlOptions' => array(
                                'style' => 'width:90px;font-size: 12px;',
                                'class' => 'g5',
                            ),
                        )
                    );
                    ?>
                    <label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name' => 'closingDateTo',
                            // name of post parameter
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'mm/dd/yy',
                                'numberOfMonths' => 2,
                            ),
                            'htmlOptions' => array(
                                'style' => 'width:90px;font-size: 12px;',
                                'class' => 'g5',
                            ),
                        )
                    );
                    ?>
                </span>
            </div>
            <div class="g3">
                <label class="g4">Phone:</label>
                <span class="g8"><?php echo CHtml::textField('phone', null, $htmlOptions=array('placeholder' => 'Phone #')); ?></span>
            </div>
        <?php if(Yii::app()->user->checkAccess('owner')): ?>
            <?=CHtml::hiddenField('export','0'); ?>
            <div class="g2 submit" style="text-align:center">
                <?php echo CHtml::submitButton('EXPORT', array('class' => 'button export-button')); ?>
            </div>
        <?endif;?>
            <div class="g3">
                <label class="g3">Sort:</label>
                <span class="g8"><?php echo CHtml::dropDownList('sort', null, array('last_spoke_to_asc'=>'Last Spoke to: oldest first', 'last_spoke_to_desc'=>'Last Spoke to: newest first', 'last_called_asc'=>'Last Called: oldest first', 'last_called_desc'=>'Last Called: newest first', 'last_hand_written_note_asc'=>'Last Hand Written Note: oldest first', 'last_hand_written_note_desc'=>'Last Hand Written Note: newest first'), $htmlOptions = array('empty' => '')); ?></span>
            </div>
            <div class="g4">
                <label class="g4">Special Date:</label>
                        <span class="g8">
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'specialDateFrom',
                                    // name of post parameter
                                    'options' => array(
                                        'showAnim' => 'fold',
                                        'dateFormat' => 'mm/dd',
                                        'numberOfMonths' => 2,
                                    ),
                                    'htmlOptions' => array(
                                        'style' => 'width:90px;font-size: 12px;',
                                        'class' => 'g5',
                                    ),
                                )
                            );
                            ?>
                            <label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'specialDateTo',
                                    // name of post parameter
                                    'options' => array(
                                        'showAnim' => 'fold',
                                        'dateFormat' => 'mm/dd',
                                        'numberOfMonths' => 2,
                                    ),
                                    'htmlOptions' => array(
                                        'style' => 'width:90px;font-size: 12px;',
                                        'class' => 'g5',
                                    ),
                                )
                            );
                            ?>
                        </span>
            </div>
            <div class="g3">
                <label class="g4">Last Spoke to:</label>
                <span class="g8"><?php echo CHtml::dropDownList('last_spoke_to_days', null, array( 30=>'30 Days+', 60 => '60 Days+', 90 => '90 Days+'), $htmlOptions = array('empty' => '')); ?></span>
            </div>
        </div>
        <hr class="g12" />
        <div class="g12">
            <label class="g1">Dialer Action:</label>
        <span class="g10">
            <?php echo CHtml::dropDownList('dialerAction', null, array('add'=>'Add to Dialer List', 'remove'=>'Remove from Dialer List'), $htmlOptions=array('empty'=>'')); ?>
            <?php echo CHtml::dropDownList('dialerListId', null, CHtml::listData(CallLists::model()->findAll(array('condition'=>'preset_ma='.CallLists::CUSTOM_CONTACTS)), 'id','name'), $htmlOptions=array('empty'=>'')); ?>
            <?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button dialer-button','style'=>'font-size: 10px; padding: 8px 10px;'))?>
            <?php echo CHtml::hiddenField('dialerProcess', null, $htmlOptions=array('value'=>'0'))?>
        </span>
        </div>
        <?php if(Yii::app()->user->checkAccess('foreverClientEmails')): ?>
            <hr class="g12" />
            <div class="email-blast-start-container g100 p-fr p-tc">
                <?php echo CHtml::button('Start Email Blast', array('class' => 'button email-blast-button', 'style'=>'')); ?>
                <div class="g12 email-blast-instructions" style="display: none;">
                    <h4>NOTE: If you see this section you are part of a BETA group for the Forever Client Email Blast feature. <br>Contact support@seizethemarket.com with any questions or suggestions. We appreciate your feedback.<br>This feature is subject to change.</h4>
                    <br><br>
                    <h4>LIMIT: You are only allowed 1 email blast per day. Excessive email bounce or complaint rates will immediately disqualify you from this feature. </h4>
                    <br><br>
                    <h4 style="text-align: left; margin-left: 20%;">INSTRUCTIONS:<br>
                        1) Enter the criteria above for your Forever Clients. Leave blank to send to all.<br>
                        2) Review the list/count of clients shown below as your recipients.<br>
                        2) Click Start Email Blast button.<br>
                        3) Select the Email Template you want to use.<br>
                        4) Complete all the fields that display. The send date/time is limited to every hour.<br>
                        6) Hit the Submit button to schedule the email blast.<br>
                    </h4>
                </div>
            </div>
            <div class="email-blast-container g100" style="display: none;">
                <div class="g12"><h2>Email Blast</h2></div>
                <div class="g3"></div>
                <div class="g6">
                    <label class="g4">Contacts Email Template:</label>
                    <span class="g8"><?=$form->dropDownList($emailBlast,'email_template_id', CHtml::listData(EmailTemplates::model()->findAllByAttributes(array('component_type_id'=>ComponentTypes::CONTACTS)), 'id', 'description'), array('data-placeholder' => 'Email Template','style' => 'width:400px;','empty'=>''));?></span>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#EmailBlasts_email_template_id','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'allow_single_deselect' => true,'width'=>'100%'))); ?>
                </div>
                <div class="email-blast-details-container g12" style="display: none;">
                    <div class="g6">
                        <label class="g2">Subject:</label>
                        <span class="g10"><?=$form->textField($emailBlast,'email_subject', array('placeholder' => 'Subject'));?></span>
                    </div>
                    <div class="g6">
                        <label class="g2">Description:</label>
                        <span class="g10"><?=$form->textField($emailBlast,'description', array('placeholder' => 'Description'));?></span>
                    </div>
                    <div class="g4">
                        <label class="g3">From Name:</label>
                        <span class="g9"><?=$form->textField($emailBlast,'sender_name', array('placeholder' => 'Sender Name'));?></span>
                    </div>
                    <div class="g4">
                        <label class="g3">From Email:</label>
                        <span class="g9"><?=$form->dropDownList($emailBlast,'sender_email_id', CHtml::listData($emailModels, 'id', 'email'), array('empty' => 'Select From Email'));?></span>
                    </div>
                    <div class="g3">
                        <label class="g4">Send Date/Time:</label>
                        <span class="g8">
                            <?php $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                                    'model'=>$emailBlast,
                                    'attribute'=>'send_datetime',
                                    'mode'=>'datetime',
                                    'options'=>array(
                                        // 'showAnim'=>'fold',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'g12',
                                         'value'=>date('m/d/Y h:00 a', strtotime('+1 hour')),
                                         'readonly' => true,
                                    ),
                                ));

                            ?>
                        </span>
                    </div>
                    <div class="g1">
                        <?php echo CHtml::button('Submit', array('class' => 'button', 'id'=>'send-email-blast-button', 'style'=>'margin-right: 4%;')); ?>
                    </div>
                </div>
            </div>
        <?endif;?>

    </div>
    <?php $this->endWidget(); ?>

</div><!-- search-form -->

<div class="p-clr"></div>
<div class="email-body-container p-clr" style="display: none;">
    <?
//    if($emailTemplates->body) {
//        echo $emailTemplates->body;
//        echo '<div style="margin-top: 50px;"><a href="/admin/emailTemplates/edit/'.$emailTemplates->id.'" class="button gray icon i_stm_edit" target="_blank">Edit Email Template</a></div>';
//    }
    ?>
</div>

<?php
$this->widget('admin_module.components.StmGridView', array(
        'id' => 'past-clients-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'template' => '{pager}{summary}{items}{pager}{summary}',
        'summaryText'=>$start.'-'.$end.' of '.$count.' results.',
        'pager'=> array('id'=>'leadsPager','class' => 'admin_module.components.StmListPager',),
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Client Info',
                'value' => 'Yii::app()->controller->printContactInfo($data)',
            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Referrals',
//                'value' => '',
//            ),
            array(
                'type' => 'raw',
                'name' => 'Special Dates',
                'value' => '$data["special_dates"]',
            ),
            array(
                'type' => 'raw',
                'name' => 'Closing History',
                'value' => '$data["closing_history"]',
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Spoke to',
                'value' => '$data["last_spoke_to"]',
                'htmlOptions' => array('style' => 'width:70px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Called',
                'value' => '$data["last_called"]',
                'htmlOptions' => array('style' => 'width:70px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Hand Written Note',
                'value' => '$data["last_hand_written_note"]',
                'htmlOptions' => array('style' => 'width:70px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Add Task',
                'value' => '"<button class=\"add-task-button\" data=\"".$data[contact_id]."\" ctid=\"".ComponentTypes::CONTACTS."\">Add Task</button>"',
                'htmlOptions' => array('style' => 'width:85px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Log Phone Call',
                'value' => '"<button class=\"add-activity-log-button phone-call-button\" data=\"".$data[contact_id]."\" ctid=\"".ComponentTypes::CONTACTS."\">Log Phone Call</button>"',
                'htmlOptions' => array('style' => 'width:125px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Log Activity',
                'value' => '"<button class=\"add-activity-log-button\" data=\"".$data[contact_id]."\" ctid=\"".ComponentTypes::CONTACTS."\">Log Activity</button>"',
                'htmlOptions' => array('style' => 'width:115px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/admin/contacts/edit/".$data[contact_id]."\" class=\"button gray icon i_stm_edit grey-button\" target=\"_blank\">Edit</a></div>"',
                'htmlOptions' => array('style' => 'width:75px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/admin/contacts/".$data[contact_id]."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a></div>"',
                'htmlOptions' => array('style' => 'width:75px'),
            ),
        ),
    )
);

Yii::import('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget');
if (!class_exists('ClickToCallDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget', array(
            'id' => ClickToCallDialogWidget::CLICK_TO_CALL_DIALOG_ID,
            'title' => 'Click to Call',
            'triggerElement' => '.click-to-call-button',
        )
    );
}

Yii::import('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget');
$this->widget('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget', array(
        'id' => PhoneDialogWidget::PHONE_DIALOG_ID,
        'title' => 'Phone Actions',
        'triggerElement' => '.phone-status-button',
        //        'parentModel' => $model,
    )
);

// Checks to see if a activity log dialog already exists and creates one only if it doesn't exist.
Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
            'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
            'title' => 'Complete Task & Log Activity',
            'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        )
    );
}

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
if (!class_exists('TaskDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
            'id' => 'task-edit',
            'title' => 'Edit Task',
            'parentModel' => new Contacts,
            'triggerElement' => '.add-task-button',
        )
    );
}