<?
$this->breadcrumbs=array(
	'User'=>'',
);
?>
<div id="settings-header">
	<h1 class="name">User Settings</h1>
</div>
<?php echo $this->renderPartial('_form', array(
		'smsProvidedNumber' => $smsProvidedNumber,
		'model'                  =>$model,
        'isOnlyOwner'            =>$isOnlyOwner,
		'Contact'                =>$Contact,
		'AuthAssignment'         =>$AuthAssignment,
		'ContactAttributeValues' =>$ContactAttributeValues,
		'ContactAttributes'      =>$ContactAttributes,
		'SettingContactValues'   =>$SettingContactValues,
));