<?php

	Yii::app()->clientScript->registerScript('search', "
    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('contact-grid', { data: $(this).serialize() });
        return false;
    });
  "
	);

	$form = $this->beginWidget('CActiveForm', array(
			'action' => Yii::app()->createUrl($this->route),
			'method' => 'get',
			'id' => 'contact-list-search',
		)
	); ?>
<div class="g1"></div>
<div class="g3">
    <label class="g3">Email:</label>
    <span class="g9"><?php echo $form->textField($model->emails, 'email', $htmlOptions = array('placeholder' => 'Email')); ?></span>
</div>
<div class="g5">
	<label class="g3">Name:</label>
	<span class="g4"><?php echo $form->textField($model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')); ?></span>
	<span class="g4 p-ml10"><?php echo $form->textField($model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')); ?></span>
</div>
<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>
<?php $this->endWidget(); ?>
