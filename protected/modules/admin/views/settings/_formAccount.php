<style type="text/css">
    span.desc {
        display: inline-block; margin-top: 10px;
    }
</style>
<?php
    $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select.chosen','options'=>array('allow_single_deselect' => true)));


	$websiteContactLeadRouteId = SettingAccountValues::WEBSITE_FLYER_CONTACT_TYPE_LEAD_ROUTE;
	Yii::app()->clientScript->registerScript('accountSettingForm', '
	$("#website_flyer_contact_type").change(function(){
		if($(this).val() == ' . $websiteContactLeadRouteId . ') {
			$(".websiteContactSpecificContact").hide();
			$(".websiteContactSpecificContact").attr("disabled","disabled");
			$(".websiteContactLeadRoute").show("normal");
			$(".websiteContactLeadRoute").removeAttr("disabled");
		} else {
			$(".websiteContactLeadRoute").hide();
			$(".websiteContactLeadRoute").attr("disabled","disabled");
			$(".websiteContactSpecificContact").show("normal");
			$(".websiteContactSpecificContact").removeAttr("disabled");
		}
	});
'
	);

	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'contact-form',
			'enableAjaxValidation' => false,
		)
	);
	$this->beginStmPortletContent(array(
			'handleTitle' => 'Setting Details',
			'handleIconCss' => 'i_settings'
		)
	);
?>
	<div id="action-plans-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
	<table class="container">
    <tr>
        <th class="narrow">Logo:</th>
        <td>

            <!-- Upload new photo stuff -->

            <style type="text/css">
                #file-drop {
                    padding: 20px;
                    margin: 10px;
                    border: 2px dashed #000000;
                    font-weight: bold;
                    border-radius: 6px;
                    text-align: center;
                }
                #the-photo {
                    /*max-height: 350px;*/
                    /*width: auto;*/
                }
            </style>

            <?if($logoUrl) : ?>
                <img src="<?=$logoUrl?>">
            <?else : ?>
                <span>No Logo Present</span>
            <?endif;?>
            <div class="stm-tool-tip question">
                <span>The image to display at the top left of your <a href="/" target="_blank">front-end website</a>.</span>
            </div>
            <div>
                <button type="button" id="btn-upload-photo">Upload New Photo</button>

                <script id="template-upload-photo" type="text/x-handlebars-template">
                    <div id="upload-photo-container">
                        <div id="file-drop">Drag and Drop Photo Here</div>
                        <div style="font-size: 22px; text-align: center; margin-bottom: 4px;">- Or -</div>
                        <div style="text-align: center;">
                            <input type="file" id="file-browser" style="width: 95%;">
                        </div>
                        <div id="photo-container">
                            <img src="" id="the-photo" style="display: none;">
                        </div>
                        <button type="button" id="btn-save-profile-photo" style="display: inline-block;" disabled>Save Profile Photo</button>
                        <input type="hidden" id="contact_id" value="<?=$Contact->id?>">
                    </div>
                </script>
            </div>

            <? //Yii::app()->clientScript->registerCssFile('https://d1k75bnu87kt9p.cloudfront.net/assets/js/jquery/plugins/jcrop/css/jquery.Jcrop.min.css', CClientScript::POS_END);?>
            <? //Yii::app()->clientScript->registerScriptFile('https://d1k75bnu87kt9p.cloudfront.net/assets/js/jquery/plugins/jcrop/js/jquery.Jcrop.min.js', CClientScript::POS_END);?>
            <? Yii::app()->clientScript->registerScript('uploadPhotoJs', <<<JS

					// Bind upload photo button
					$('#btn-upload-photo').on('click', function(e) {

						// Prevent default behavior
						e.preventDefault();

						// Create modal
						$.when(
							stmModal.create({
								title: 'Upload New Photo',
								contentVariables: {

								},
								content: $('#template-upload-photo').html(),
							})
						).then( function(){

							// Hold crop values
							var cropValues;

							// Callback for retrieving coordintate values
							var getCoords = function(c) {
								cropValues = c;
							};

							// Helper function to setup cropper and stuff
							var fileHandler = function(file) {

								// Only do this for images
								if (file.type.match(/image.*/)) {

									// Read in file data
									var reader = new FileReader();
									reader.onload = function(e2) {

										// Update image source
										document.getElementById('the-photo').style.display = 'block';
										document.getElementById('the-photo').src = e2.target.result;

										// Use jCrop
										jQuery('#the-photo').Jcrop({
											minSize: [100, 100],
											setSelect: [30, 30, 300, 300],
											boxWidth: 300,
											boxHeight: 300,
											onSelect: getCoords,
											onChange: getCoords
										});
									}
									reader.readAsDataURL(file); // start reading the file data.

									// Enable upload button
									$('#btn-save-profile-photo').prop('disabled', false);
								}
							}

							// Create a drop zone
							var dropZone = document.getElementById('file-drop');

							// Show the copy icon when dragging over.  Seems to only work for chrome.
							dropZone.addEventListener('dragover', function(e) {
								e.stopPropagation();
								e.preventDefault();
								e.dataTransfer.dropEffect = 'copy';
							});

							// Get file data on drop
							var file;
							dropZone.addEventListener('drop', function(e) {

								// Stop default behavior
								e.stopPropagation();
								e.preventDefault();

								// Pull the first file that was dropped
								file = e.dataTransfer.files[0]; // Array of all files

								// Pass this off to file handler
								fileHandler(file);
							});

							// Bind file browse change event
							$('#file-browser').on('change', function(e) {
								file = this.files[0];
								fileHandler(file);
							});

							// Bind upload button
							$('#btn-save-profile-photo').on('click', function(e) {

								// Show loading container
								$("body").prepend("<div class='loading-container loading'><em></em></div>");

								// Create new form data object
								var formData = new FormData();

								// Append the file and other info
								formData.append('contact_id', $('#contact_id').val());
								formData.append('photo_file', file);
								formData.append('x', cropValues.x);
								formData.append('y', cropValues.y);
								formData.append('w', cropValues.w);
								formData.append('h', cropValues.h);

								// Make AJAX call
								jQuery.ajax({
									method: 'POST',
									url: "/admin/settings/saveAccountLogo",
									data: formData,
									contentType: false,
									processData: false,
									success: function(e) {
										location.reload(true);
									}
								});
							});
						});
					});

JS
                , CClientScript::POS_END);
            ?>
            <!-- End upload new photo stuff -->

        </td>
    </tr>
	<tr>
		<th class="narrow">Office Name:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('office_name')->id . ']', $htmlOptions = array(
					'placeholder' => 'Office Name',
					'class' => 'g6',
				)
			); ?>
            <div class="label">*Required to send out Email Drips</div>
		</td>
	</tr>
	<tr>
		<th class="narrow">Office Phone:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('office_phone')->id . ']', $htmlOptions = array(
					'placeholder' => 'Office Phone',
					'class' => 'g6',
				)
			); ?>
		</td>
	</tr>
	<tr>
		<th class="narrow">Office Fax:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('office_fax')->id . ']', $htmlOptions = array(
					'placeholder' => 'Office Fax',
					'class' => 'g6',
				)
			); ?>
		</td>
	</tr>
	<tr>
		<th class="narrow">Office Email:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('office_email')->id . ']', $htmlOptions = array(
					'placeholder' => 'Office Email',
					'class' => 'g6',
				)
			); ?>
		</td>
	</tr>
	<tr>
		<th class="narrow">Office Address:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('office_address')->id . ']', $htmlOptions = array(
					'placeholder' => 'Office Address',
					'class' => 'g6',
				)
			); ?>
            <div class="label">*Required to send out Email Drips</div>
		</td>
	</tr>
	<tr>
		<th class="narrow">City:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('office_city')->id . ']', $htmlOptions = array(
					'placeholder' => 'City',
					'class' => 'g3',
				)
			); ?>
			<?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('office_state')->id . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'),
				$htmlOptions = array(
					'empty' => 'State',
					'class' => 'g1',
				)
			); ?>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('office_zip')->id . ']', $htmlOptions = array(
					'placeholder' => 'Zip',
					'class' => 'g2',
				)
			); ?>
            <div class="label">*Required to send out Email Drips</div>
		</td>
	</tr>
	<tr>
		<th class="narrow">Team Email:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('email_all')->id . ']', $htmlOptions = array(
					'placeholder' => 'Team Email',
					'class' => 'g6'
				)
			); ?>
            <div class="stm-tool-tip question">
                <span>When a new closing, appointment, or listing occurs, a congratulatory email will be sent to this email address. This is where the Gamification alerts go to!</span>
            </div>
		</td>
	</tr>
    <tr>
        <th class="narrow">BombBomb API Key:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('bombbomb_api_key')->id . ']', $htmlOptions = array(
                    'placeholder' => 'BombBomb API Key',
                    'class' => 'g6'
                )
            ); ?>
            <div class="stm-tool-tip question p-fl">
                <span>
                    Bombbomb API Key: Copy this from your BombBomb Account.
                    <a href="/admin/support/video/84" target="_blank">Click for video tutorial</a>
                </span>
            </div>

        </td>
    </tr>
    <tr>
        <th class="narrow">Sly Broadcast API:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('sly_broadcast_email')->id . ']', $htmlOptions = array(
                    'placeholder' => 'Sly Broadcast Login Email',
                    'class' => 'g6'
                )
            ); ?>
            <?php echo CHtml::passwordField('pwNone','', $htmlOptions = array(
                    'placeholder' => 'None',
                    'class' => 'g6 hidden',
                    'disabled'=>'disabled' // this is done to trick browsers that want to remember email/pw combos in fields, putting this extra one throws it off to prevent auto-fill
                )
            ); ?>
            <?php echo $form->passwordField($SettingAccountValues, 'data[' . $model->getField('sly_broadcast_password')->id . ']', $htmlOptions = array(
                    'placeholder' => 'Sly Broadcast Login Password',
                    'class' => 'g5 p-mt5'
                )
            ); ?>
            <div class="stm-tool-tip question left">
                <span>The is the login for your Slybroadcast account. After filling out these fields, you may use Slybroadcast from the field at the bottom of your Buyers, Sellers, or Recruits list search.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow">Team Lead MLS Agent ID:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('agent_mls_num')->id . ']', $htmlOptions = array(
                    'placeholder' => 'Team Lead MLS Agent ID',
                    'class' => 'g6'
                )
            ); ?>
        </td>
    </tr>
	<tr>
		<th class="narrow">Website Footer Contact:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('website_footer_contact')->id . ']', $htmlOptions = array(
					'placeholder' => 'Website Footer Contact',
					'class' => 'g11'
				)
			); ?>
            <div class="stm-tool-tip question left">
                <span>Contact information to display at the bottom of your <a href="/" target="_blank">front-end website</a>.</span>
            </div>
		</td>
	</tr>
	<tr>
		<th class="narrow">Lead Pool Contact:</th>
		<td>
			<?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('lead_pool_contact_id')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins(false, $SettingAccountValues->data[$model->getField('lead_pool_contact_id')->id])->findAll(), 'id', 'fullName'),
				$htmlOptions = array('empty' => ''
				)); ?>
            <div class="stm-tool-tip question">
                <span>All unassigned buyer/seller leads will go into the shared office "Lead Pool," which will be managed by this user. This can be used as your centralized team/office contact or default assignment.</span>
            </div>
		</td>
	</tr>
    <tr>
        <th class="narrow">Recruit Lead Pool Contact:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('recruit_lead_pool_contact_id')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins(false, $SettingAccountValues->data[$model->getField('recruit_lead_pool_contact_id')->id])->findAll(), 'id', 'fullName'),
                $htmlOptions = array('empty' => ''
                )); ?>
            <div class="stm-tool-tip question">
                <span>All unassigned recruit leads will go into the shared office "Recruit Lead Pool," which will be managed by this user. This can be used as your centralized team/office recruit contact or default assignment.</span>
            </div>
        </td>
    </tr>
	<tr>
		<th class="narrow">Website Flyer Contact:</th>
		<td><?php
				$websiteContact = $SettingAccountValues->data[$model->getField('website_flyer_contact')->id];
				$websiteContact = CJSON::decode($websiteContact);
				echo CHtml::dropDownList('website_flyer_contact[type]', $websiteContact['type'], array(
						'1' => 'Specific Contact',
						'2' => 'Lead Route'
					), $htmlOptions = array(
						'placeholder' => 'Website Flyer Contact',
						'class' => 'g2'
					)
				);

				$isWebContactLeadRoute = ($websiteContact['type'] == SettingAccountValues::WEBSITE_FLYER_CONTACT_TYPE_LEAD_ROUTE) ? true : false;

				echo CHtml::dropDownList('website_flyer_contact[value]', $websiteContact['value'],
					CHtml::listData(Contacts::model()->byActiveAdmins(false, $websiteContact['value'])->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions = ((!$isWebContactLeadRoute)
						? array(
							'class' => 'g2 websiteContactSpecificContact',
							'empty' => 'Select One'
						)
						: array(
							'class' => 'g2 websiteContactSpecificContact',
							'disabled' => 'disabled',
							'style' => 'display:none;',
							'empty' => 'Select One'
						))
				);

				echo CHtml::dropDownList('website_flyer_contact[value]', $websiteContact['value'], CHtml::listData(LeadRoutes::model()->findAll(), 'id', 'name'), $htmlOptions = (($isWebContactLeadRoute)
						? array(
							'class' => 'g2 websiteContactLeadRoute',
							'empty' => 'Select One'
						)
						: array(
							'class' => 'g2 websiteContactLeadRoute',
							'disabled' => 'disabled',
							'style' => 'display:none;',
							'empty' => 'Select One'
						))
				);

			?>
            <div class="stm-tool-tip question">
                <span>Allows you to specify the default contact to display on a home details flyer from your website. If you select a lead route, the contact information will be drawn from the user assigned from the lead route. </span>
            </div>
		</td>
	</tr>
    <tr>
        <th class="narrow">Market Snapshot Contact:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('market_snapshot_contact_id')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins(false, $SettingAccountValues->data[$model->getField('market_snapshot_contact_id')->id])->findAll(), 'id', 'fullName'),
                $htmlOptions = array('empty' => ''
                )); ?>
            <div class="stm-tool-tip question">
                <span>Allows you to specify the default contact to display on a market snapshot. A market snapshot is a home value report email that goes out to seller leads. (Applicable only if you have this feature)</span>
            </div>
        </td>

    </tr>
	<tr>
		<th class="narrow">Max Home Views:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('views_max')->id . ']', $htmlOptions = array(
					'placeholder' => '#',
					'class' => 'g1',
				)
			); ?>
            <div class="stm-tool-tip question">
                <span>This is the max number of homes that can be viewed by a visitor on your website before they are prompted to register. We recommend you set this to 0. Anything higher has been found to reduce website registrations by up to 70%.</span>
            </div>
		</td>
	</tr>
<!-- 	<tr>
		<th class="narrow"><?php echo $model->getField('response_time_new_lead')->label; ?>:</th>
		<td>
			<?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('response_time_new_lead')->id . ']', $htmlOptions = array(
					'placeholder' => 'minutes',
					'class' => 'g1'
				)
			) ?>
            <div class="stm-tool-tip question">
                <span>Minutes an agent has to respond to an incoming lead before it is flagged as "not followed up," which will show up in accountability reports.</span>
            </div>
		</td>
	</tr> -->
    <tr>
        <th class="narrow"><?php echo $model->getField('new_lead_status_change_days')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('new_lead_status_change_days')->id . ']', $htmlOptions = array(
                    'placeholder' => '#',
                    'class' => 'g1'
                )
            ) ?>
            <div class="stm-tool-tip question">
                <span>Leave blank to disable. When enabled, new leads will be have their status updated to "Not Spoken To" after this many days (30 days recommended).</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('new_lead_status_change_days_email')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('new_lead_status_change_days_email')->id . ']', $htmlOptions = array('placeholder' => 'New Lead Status Notify Email Address','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
            <span>When new leads are auto-updated to "Not Spoken To" a notification will be sent to this email. (Separate multiple emails with comma)</span>
        </div>
        </td>
    </tr>
<!--    <tr>-->
<!--        <th class="narrow">Max Assigned Buyer/Seller Prospects:</th>-->
<!--        <td>-->
<!--            --><?php //echo $form->textField($SettingAccountValues, 'data[' . $model->getField('max_assigned_buyer_seller_prospects')->id . ']', $htmlOptions = array(
//                    'placeholder' => '#',
//                    'class' => 'g1',
//                )
//            ); ?>
<!--        </td>-->
<!--    </tr>-->
    <tr>
        <th class="narrow"><?php echo $model->getField('daily_task_executive_summary_recipients')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('daily_task_executive_summary_recipients')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName'),
                $htmlOptions = array('class' => 'g6 chosen','multiple' => 'multiple','data-placeholder' => 'Daily Task Executive Summary Recipients')
            );
             ?>
             <div class="stm-tool-tip question">
                <span>Each day, when the Executive Summary is generated, it will be sent to these users. The Executive Summary contains information about overdue tasks on your users.</span>
            </div>
        </td>
    </tr>
<!--    <tr>-->
<!--        <th class="narrow"></th>-->
<!--        <td>-->
<!--            --><?php //echo $form->textField($SettingAccountValues, 'data[' . $model->getField('daily_task_executive_summary_recipients')->id . ']', $htmlOptions = array('class' => 'g6','placeholder' => 'CC: Email Addresses')); ?><!--<span class="desc">Copy (CC) Additional Email Addresses</span>-->
<!--        </td>-->
<!--    </tr>-->
    <tr>
        <th class="narrow"><?php echo $model->getField('closing_pipeline_executive_summary_recipients')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('closing_pipeline_executive_summary_recipients')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName'),
                $htmlOptions = array('class' => 'g6 chosen','multiple' => 'multiple','data-placeholder' => 'Closings Pipeline Executive Summary Recipients')
            );
            ?>
            <div class="stm-tool-tip question">
                <span>Each day, when the Executive Summary is generated, it will be sent to these users. The Executive Summary contains information about overdue tasks on your users.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('appointments_executive_summary_recipients')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('appointments_executive_summary_recipients')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName'),
                $htmlOptions = array('class' => 'g6 chosen','multiple' => 'multiple','data-placeholder' => 'Appointments Executive Summary Recipients')
            );
            ?>
            <div class="stm-tool-tip question">
                <span>Sends the Appointments Executive Summary to these recipients. The Appointments Executive Summary contains the month-to-date appointments set, met, and signed.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('appointments_executive_summary_agents')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('appointments_executive_summary_agents')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName'),
                $htmlOptions = array('class' => 'g6 chosen','multiple' => 'multiple','data-placeholder' => 'Appointments Executive Summary Agents')
            );
            ?>
            <div class="stm-tool-tip question">
                <span>Specify which agents to report on in the Appointments Executive Summary. </span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('goals_executive_summary_recipients')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('goals_executive_summary_recipients')->id . ']', CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName'),
                $htmlOptions = array('class' => 'g6 chosen','multiple' => 'multiple','data-placeholder' => 'Goals Executive Summary Recipients')
            );
            ?>
            <div class="stm-tool-tip question">
                <span>Sends the Goals Executive Summary to these recipients. The Goals Executive Summary reports on your users' progress meeting the goals you have set up. </span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('zillow_source')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('zillow_source')->id . ']', CHtml::listData(Sources::model()->findAll(), 'id', 'name'),
                $htmlOptions = array('class' => 'g6 chosen','data-placeholder' => 'Select Zillow Source','empty'=>'')
            );
            ?>
            <div class="stm-tool-tip question">
                <span>The source to assign to incoming Zillow leads, if Zillow is setup in your <a href="/admin/integrations" target="_blank">Integrations</a>.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('trulia_source')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('trulia_source')->id . ']', CHtml::listData(Sources::model()->findAll(), 'id', 'name'),
                $htmlOptions = array('class' => 'g6 chosen','data-placeholder' => 'Select Trulia Source','empty'=>'')
            );
            ?>
            <div class="stm-tool-tip question">
                <span>The source to assign to incoming Trulia leads, if Trulia is setup in your <a href="/admin/integrations" target="_blank">Integrations</a>.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('realtordotcom_source')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('realtordotcom_source')->id . ']', CHtml::listData(Sources::model()->findAll(), 'id', 'name'),
                $htmlOptions = array('class' => 'g6 chosen','data-placeholder' => 'Select Realtor.com Source','empty'=>'')
            );
            ?>
            <div class="stm-tool-tip question">
                <span>The source to assign to incoming Realtor.com leads, if Realtor.com is set up in your <a href="/admin/integrations" target="_blank">Integrations</a>.</span>
            </div>
        </td>
    </tr>
    <? //if(YII_DEBUG || $SettingAccountValues->data[$model->getField('third_party_leads_email_cc')->id]): ?>
    <tr>
        <th class="narrow"><?php echo $model->getField('third_party_leads_email_cc')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('third_party_leads_email_cc')->id . ']', $htmlOptions = array('placeholder' => 'Email to Copy Third Party Leads','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
                <span>Will send a copy of any third party emails you have set up on your <a href="/admin/integrations" target="_blank">Integrations</a> page to this email address.</span>
            </div>
        </td>
    </tr>
    <? //endif; ?>
    <tr>
        <th class="narrow"><?php echo $model->getField('tasks_past_due_redirect')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('tasks_past_due_redirect')->id . ']', $htmlOptions = array('placeholder' => '# Task Past Due Redirect','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
                <span>Leave blank to disable. If the amount of overdue tasks for a given user goes over this number, the user will be blocked from accessing the rest of Seize The Market until they get that amount back under the limit. This is an automated accountability measure that takes the responsibility off of you by making the system notify the users, and is highly recommended. (This number limit can be overriden for individual users on their user page)</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('appointments_past_due_redirect')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('appointments_past_due_redirect')->id . ']', StmFormHelper::getYesNoList(false)); ?>
            <div class="stm-tool-tip question">
                <span>If enabled, then when a user has an appointment that needs to be updated, they will be blocked from accessing the rest of Seize The Market until they update the appointment. Keeping appointments updated is very important, as this data is used in business reports. This is an automated accountability measure that takes the responsibility off of you by making the system notify the users, and is highly recommended. (This can be overriden for individual users on their user page)</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('timeblocks_past_due_redirect')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('timeblocks_past_due_redirect')->id . ']', StmFormHelper::getYesNoList(false)); ?>
            <span class="label"><?php echo $model->getField('timeblocks_past_due_redirect_start_date')->label; ?>:</span>
            <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $SettingAccountValues,
                    'attribute' => 'data[' . $model->getField('timeblocks_past_due_redirect_start_date')->id . ']',
                    'options' => array(
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'placeholder' => 'Timeblock Redirect Start Date',
                        'style'=>'width: 25%;',
                    ),
                ));
            ?>
            <div class="stm-tool-tip question">
                <span>If enabled, then when a user has not yet filled out their timeblocks for every weekday, they will be blocked from accessing the rest of Seize The Market until they do. The start date specifies when the system will start looking for timeblocks. This is an automated accountability measure that takes the responsibility off of you by making the system notify the users, and is highly recommended. (This can be overriden for individual users on their user page)</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('appointments_dig_for_motivation_required')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('appointments_dig_for_motivation_required')->id . ']', StmFormHelper::getYesNoList(false)); ?>
            <div class="stm-tool-tip question">
                <span>If enabled, a field will appear when logging appointments, allowing users to record how they would rate the motivation of the individual they met with. </span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('new_opportunity_alert')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('new_opportunity_alert')->id . ']', StmFormHelper::getYesNoList(false)); ?>
            <span class="label"><?php echo $model->getField('new_opportunity_alert_email')->label; ?>:</span>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('new_opportunity_alert_email')->id . ']', $htmlOptions = array('placeholder' => 'Email','style' => 'width: 25%;')); ?><span class="label p-pt8"> </span>
            <div class="stm-tool-tip question">
                <span>If enabled, the given email address will receive an alert when a new opportunity is added for a buyer/seller.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('one_thing_daily_past_due_redirect')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('one_thing_daily_past_due_redirect')->id . ']', StmFormHelper::getYesNoList(false)); ?>
            <div class="stm-tool-tip question">
                <span>If enabled, then when a user has not yet added their One Thing entry for the day, they will be blocked from accessing the rest of Seize The Market until they do. (This can be overriden for individual users on their user page)</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('one_thing_report_email')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('one_thing_report_email')->id . ']', $htmlOptions = array('placeholder' => 'Email','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
                <span>One Thing entries will be sent to this email address. (Separate multiple emails with comma)</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('one_thing_report_email_subject')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('one_thing_report_email_subject')->id . ']', $htmlOptions = array('placeholder' => 'Subject','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
                <span>One Thing emails will have this subject line. Date, Name and % Goal will be added.</span>
            </div>
        </td>
    </tr>
<!--     <tr>
        <th class="narrow"><?php echo $model->getField('max_assigned_buyer_seller_prospects')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('max_assigned_buyer_seller_prospects')->id . ']', $htmlOptions = array('placeholder' => '# Max Assigned Buyers/Sellers','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
                <span>This is the max amount of leads an agent can have assigned to them at a time.</span>
            </div>
        </td>
    </tr>
 -->    <tr>
        <th class="narrow"><?php echo $model->getField('email_click_notification_emails')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('email_click_notification_emails')->id . ']', $htmlOptions = array('placeholder' => 'Email Click Notification Emails','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
                <span>All emails sent from Seize The Market will automatically track when somebody clicks on a link in the email. Notifications for when somebody clicks on a link will be sent to this email address. (Separate multiple emails with comma)</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('dialer_caller_id_name')->label; ?>:</th>
        <td>
            <?php echo $form->textField($SettingAccountValues, 'data[' . $model->getField('dialer_caller_id_name')->id . ']', $htmlOptions = array('placeholder' => 'Dialer Caller ID Name','class' => 'g3')); ?>
            <div class="stm-tool-tip question">
                <span>The default Caller ID name to show if the user doesn't set one in the dialer.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('dialer_caller_id')->label; ?>:</th>
        <td>
            <?php $this->widget('StmMaskedTextField', array(
                    'model' => $SettingAccountValues,
                    'attribute' => 'data[' . $model->getField('dialer_caller_id')->id . ']',
                    'mask' => '9999999999',
                    'htmlOptions' => array(
                        'placeholder' => 'Dialer Call ID',
                        'class' => 'g3',
                    ),
                )
            );

            ?>
            <div class="stm-tool-tip question">
                <span>The default Caller ID phone number to show if the user doesn't set one in the dialer</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('dialer_task_overdue_maxed_hide_all_shared_list')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('dialer_task_overdue_maxed_hide_all_shared_list')->id . ']', StmFormHelper::getYesNoList(false)); ?>
            <div class="stm-tool-tip question">
                <span>If enabled,your users will not be able to access the Lead Pool task list if their overdue tasks are above the limit you have set.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('hide_phone_numbers_search_results')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('hide_phone_numbers_search_results')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array()); ?>
            <div class="stm-tool-tip question">
                <span>If enabled, the phone numbers in your database will only display the last 4 digits. This is in case you need to hide the numbers for security purposes, or if you want to force everyone using your database to make calls through the dialer.</span>
            </div>
        </td>
    </tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('show_assigned_leads_only')->label; ?>:</th>
        <td>
            <?php echo $form->dropDownList($SettingAccountValues, 'data[' . $model->getField('show_assigned_leads_only')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array()); ?>
            <div class="stm-tool-tip question">
                <span>This will restrict users to only see leads assigned to them.</span>
            </div>
        </td>
    </tr>
	<tr>
		<th class="narrow"><?php echo $model->getField('email_signature')->label; ?>:</th>
		<td><span style="font-weight: bold;">****NOTE: Do NOT cut and paste from any other document. Thank you. =)****</span>
        <div class="stm-tool-tip question">
            <span>This will appear at the bottom of any outgoing emails from Seize the Market, and will be the default email signature for all of your users. This settings is overridden if a user's email signature is defined in their user settings.</span>
        </div>
			<?php
				/*$this->widget('admin_module.extensions.tinymce.ETinyMce', array(
						'name' => 'SettingAccountValues[data][' . $model->getField('email_signature')->id . ']',
						'value' => $SettingAccountValues->data[$model->getField('email_signature')->id],
						'useSwitch' => false,
						'options' => array(
							'theme' => 'advanced',
							'theme_advanced_toolbar_location' => 'top',
							//								'theme_advanced_toolbar_align'    =>'left',
							//								'theme_advanced_resizing'         =>'true',
							'theme_advanced_buttons1' => 'bold,italic,underline,separator,fontselect,fontsizeselect,separator,forecolor, backcolor,separator,justifyleft,justifycenter,justifyright,separator,bullist,numlist,separator,undo,redo,link,unlink,removeformat,code',
							'theme_advanced_buttons2' => '',
							'theme_advanced_buttons3' => '',
							'width' => '700',
							'height' => '250',
							//								'plugins'                         =>'advimage', //,'imagemanager','filemanager'
							//								'apply_source_formatting'         => false,
						),
					)
				);*/



            Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);

            $scope = (YII_DEBUG) ? "local" : "com";
            $s3BaseUrl = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";

            $s3Url = urlencode(base64_encode('s3://'.$s3BaseUrl.'/site-files/'.Yii::app()->user->clientId."/emailSignatures/account"));
            $emailSignatureSettingId = $model->getField('email_signature')->id;
            $js = <<<JS
tinymce.PluginManager.load('moxiemanager', 'http://www.seizethemarket.{$scope}/imagemanager/plugin.js?v=4');
tinymce.init({
    convert_urls: false,
    relative_urls: false,
    height : 300,
    selector: "#SettingAccountValues_data_$emailSignatureSettingId",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor textcolor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste moxiemanager"
    ],
    toolbar: "insertfile undo redo | styleselect | fontselect fontsizeselect forecolor backcolor bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    autosave_ask_before_unload: false,
    setup : function(ed) {
        ed.on('init', function() { this.getDoc().body.style.fontSize = '13px'; });
        ed.on('keyUp', function() { tinyMCE.triggerSave(); });
    },
    //moxiemanager_apiPageUrl: "http://www.seizethemarket.{$scope}/imagemanager/api.php,
    //moxiemanager_baseUrl: "http://www.seizethemarket.{$scope}/imagemanager"
    init_instance_callback: function () {
        moxman.Env.baseUrl = "http://www.seizethemarket.{$scope}/imagemanager";
        moxman.Env.apiPageName = "api.php?v=1&s3={$s3Url}";
        //moxman.path = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
        //moxman.rootpath = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
    },
    //moxiemanager_rootpath: 's3://{$s3BaseUrl}/site-files/{$s3Url}',
    //moxiemanager_path: '/site-files/{$s3Url}',
});
JS;

            //http://www.seizethemarket.local/imagemanager/api.php?
            //s3=czM6Ly9kZXYuc2l0ZXMuc2VpemV0aGVtYXJrZXQuY29tL3NpdGUtZmlsZXMvMS9jbXNQYWdlcy8%3D
            //&action=streamfile&path=%2FcmsPages
            //&name=one-story-craftsman-style-home-plans-l-d28baafe08f326b5.jpg
            //&loaded=0
            //&total=105057
            //&id=null
            //&csrf=41d9afb847906a6d5bafccc453a2c6a461204ab064876d1e1a0c198a554db7d2
            //&resolution=default

            Yii::app()->clientScript->registerScript('tinyMceNew', $js); ?>
            <textarea id="SettingAccountValues_data_<?=$emailSignatureSettingId;?>" name="SettingAccountValues[data][<?=$emailSignatureSettingId;?>]">
                <?=$SettingAccountValues->data[$emailSignatureSettingId];?>
            </textarea>
		</td>
	</tr>
	<tr>
		<th class="narrow">Top Links:</th>
		<td id="email-table-cell">
			<?php echo $form->hiddenField($SettingAccountValues, 'data[' . $model->getField('top_links')->id . ']'); ?>
			<?php echo $this->renderPartial('_addlTopLinkTemplate', array(
					'form' => $form,
					'containerHtmlOptions' => array(
						'style' => 'display: none',
						'id' => 'addlTopLinkTemplate'
					)
				)
			); ?>
			<div id="addlTopLinkRows" class="g12 p-p0" style="min-height: 0;">
				<?php
					if ($topLinks = $SettingAccountValues->data[$model->getField('top_links')->id]) {
						$topLinks = CJSON::decode($topLinks);
                        $rowNumber = 0;
						foreach ($topLinks as $i => $data) {
							echo $this->renderPartial('_addlTopLinkTemplate', array(
									// 'form'=>$form,
									'data' => $data,
									'i' => $rowNumber,
                                    'containerHtmlOptions' => array(
                                        'id'=>'addlTopLink-'.$rowNumber,
                                    ),
								)
							);
                            $rowNumber++;
						}
					}
				?>
			</div>
			<button type="button" id="add-top-link" class="text"><em class="icon i_stm_add"></em>Add Top Link</button>
		</td>
	</tr>
    <tr>
        <th class="narrow"><?php echo $model->getField('tracking_scripts')->label; ?>:</th>
        <td>
            <?php echo $form->textArea($SettingAccountValues, 'data[' . $model->getField('tracking_scripts')->id . ']', $htmlOptions = array('placeholder' => 'Tracking or Pixel Script','class' => 'g8','rows'=>4)); ?>
            <div class="stm-tool-tip question">
                <span>*** Advanced Setting, if you don't understand this, do not use it *** If there are any tracking pixel scripts you want to have in your pages, this is where they would go. These scripts may cause your site to no longer work properly. Be very careful. </span>
            </div>
        </td>
    </tr>
	</table>

	</div>
	</div>
<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper">
		<button type="submit" class="submit wide">Update Settings</button>
	</div>
<?php $this->endWidget(); ?>