<style type="text/css">
		/*@todo: move to css file, temporarily here for development*/
	.profilePhotoUpload {
		position: relative;
		display: block; /* or inline-block */
		font-weight: normal;
		width: 150px;
		padding: 7px 0;
		text-align: center;
		background: #A20000;
		border-bottom: 1px solid #ddd;
		color: #fff;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
		-moz-box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
		box-shadow: inset 0 1px 3px rgba(80, 43, 0, 0.2);
	}

	.profilePhotoUpload input[type="file"] {
		position: absolute;
		left: 0px;
		top: 0px;
		font-weight: normal;
		font-family: Arial;
		font-size: 118px;
		margin: 0px;
		padding: 0px;
		cursor: pointer;
		opacity: 0;
		width: 150px;
		max-height: 28px;
	}

	.profilePhotoUpload:hover {
		background: #C20000;
	}

	.qq-upload-list {
		display: none;
	}
    #upload-photo-container {
        text-align: center;
    }
    #btn-save-profile-photo {
        width: 96%;
        margin-top: 20px;
        padding: 15px;
    }
</style>
<?php
Yii::import('admin_widgets.DialogWidget.ProfilePhotoDialogWidget.ProfilePhotoDialogWidget');
$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerScript('verifySmsScript', <<<JS
	$('.verify-sms-button').click(function(){
        if(confirm('Please confirm that you have saved after entering your cell # on this page.')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var contactId = $(this).data('id');
			var cell_carrier = $("#cell_carriers_list option:selected").val();
			var cell_number = $("#cell_number_field").val();

			$.post('/$module/settings/verifySms/',{'contactId':contactId, 'cell_carrier': cell_carrier, 'cell_number': cell_number}, function(data) {
				if(data==null) {
					Message.create("success","Verification text sent. Check your phone for a text message.");
					$("#sms_verified_conatiner").slideDown();
				} else if(data['error']) {
					Message.create("error",data['error']);
				}
                $("div.loading-container.loading").remove();
			},"json");
		}
    });

	$('.verify-sms-code').click(function() {
		$("body").prepend("<div class='loading-container loading'><em></em></div>");
		var contactId = $(this).data('id');
		var smsVerificationCode = $('#sms_verification_code').val();

		if(smsVerificationCode == "" || smsVerificationCode == null || smsVerificationCode.length < 6) {
			Message.create("error","Error: Please enter a valid 6 digit verification code.");
			$("div.loading-container.loading").remove();
			return false;
		}

		$.post('/$module/settings/verifySmsCode/',{'contactId':contactId, 'smsVerificationCode': smsVerificationCode}, function(data) {
			if(data.status == "success") {
				Message.create("success","Cell number verified.");
				window.location.reload();
			} else if(data['error']) {
				Message.create("error",data['error']);
				$("div.loading-container.loading").remove();
			}
		},"json");
	});

	$('#sms_verification_code').on('keyup', function() {
		$(this).val($(this).val().replace(/[^0-9]/g, ''));
	});

	$('.change-sms-button').click(function() {
		$(this).hide();
		$("#register-stm-number").hide();
		$("#verifiedNote").hide();
		$('.verify-sms-button').show();
		$('#cell_carriers_list').removeAttr('disabled');
		$('#cell_number_field').removeAttr('disabled');
	});
JS
);


	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'contact-form',
			'enableAjaxValidation' => false,
			'htmlOptions' => array('enctype' => 'multipart/form-data','autocomplete'=>'off'),
		)
	);
	$this->beginStmPortletContent(array(
			'handleTitle' => 'Settings Details',
			'handleIconCss' => 'i_settings'
		)
	);

	Yii::import('admin_widgets.DialogWidget.ChooseSmsNumberDialogWidget.ChooseSmsNumberDialogWidget');
?>
<div id="settings-container">
<div class="g12 p-mb5 rounded-text-box odd-static">
<div class="g6 ">
	<table class="">
		<tr>
			<th class="narrow"><?php echo $model->getField('profile_photo')->label; ?>:</th>
			<td><? $image = (!empty($SettingContactValues[data][$model->getField('profile_photo')->id])) ?
					Yii::app()->user->getProfileImageBaseUrl() . DS . $Contact->id . DS . $SettingContactValues['data'][$model->getField('profile_photo')->id] : $this->getModule()->getCdnImageAssetsUrl().'profile_placeholder_1.jpg'; ?>
				<?php echo CHtml::image($image, null, array('class' => 'profile-photo')); ?>
				<?php /*$this->widget('admin_exts.EAjaxUpload.EAjaxUpload', array(
						//								'id'=>'SettingContactValues',
						'config' => array(
							'action' => '/'.Yii::app()->controller->module->id.'/settings/'.$Contact->id,
							'allowedExtensions' => explode(',', Settings::ALLOWED_PROFILE_PHOTO_FILE_EXTENSIONS),
							'sizeLimit' => Settings::PROFILE_PHOTO_SIZE_MAX_MB * 1024 * 1024,
							//									'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
							'onComplete' => 'js:function(id, fileName, responseJSON){
											if(responseJSON.success==true) {console.log(responseJSON);
												var photoSource = $("img.profile-photo").attr("src");
												var strLocation = photoSource.search("profile_photo");
												if(strLocation<0) {
													var fileName = "profile_placeholder_1.jpg";
												} else {
													var fileName = photoSource.substring(strLocation);
												}
												photoSource =  responseJSON.baseUrlPath + responseJSON.filename;
												var Dialog = $("#profile-photo-dialog");
												var DialogPhoto = $("img#dialog-profile-photo");

												DialogPhoto.attr("src",photoSource);
												DialogPhoto.attr("style","");
												DialogPhoto.data("naturalwidth",responseJSON.width);
												DialogPhoto.data("naturalheight",responseJSON.height);
                                                DialogPhoto.attr("width",responseJSON.width);
                                                DialogPhoto.attr("height",responseJSON.height);
                                                DialogPhoto.attr("data-ratio",responseJSON.ratio);

												$("#start_dialog-profile-photo").click();

												var parts = fileName.split(".");
												var fileType =  parts[parts.length - 1];
												$("#profile-photo-dialog #fileType").val(fileType);

												Dialog.dialog("open");
											}
										}',
							'messages' => array(
								'typeError' => "{file} has invalid extension. Only {extensions} are allowed.",
								'sizeError' => "{file} is too large, maximum file size is {sizeLimit}.",
								'minSizeError' => "{file} is too small, minimum file size is {minSizeLimit}.",
								'emptyError' => "{file} is empty, please select files again without it.",
								'onLeave' => "The files are being uploaded, if you leave now the upload will be cancelled."
							),
							'showMessage' => 'js:function(message){ Message.create("notice",message);}'
						)
					)
				); */?>
				<br>

				<!-- Upload new photo stuff -->

				<style type="text/css">
					#file-drop {
						padding: 20px;
						margin: 10px;
						border: 2px dashed #000000;
						font-weight: bold;
						border-radius: 6px;
						text-align: center;
					}
					#the-photo {
						/*max-height: 350px;*/
						/*width: auto;*/
					}
				</style>

				<button type="button" id="btn-upload-photo">Upload New Photo</button>

				<script id="template-upload-photo" type="text/x-handlebars-template">
                    <div id="upload-photo-container">
                        <div id="file-drop">Drag and Drop Photo Here</div>
                        <div style="font-size: 22px; text-align: center; margin-bottom: 4px;">- Or -</div>
                        <div style="text-align: center;">
                            <input type="file" id="file-browser" style="width: 95%;">
                        </div>
                        <div id="photo-container">
                            <img src="" id="the-photo" style="display: none;">
                        </div>
                        <button type="button" id="btn-save-profile-photo" style="display: inline-block;" disabled>Save Profile Photo</button>
                        <input type="hidden" id="contact_id" value="<?=$Contact->id?>">
                    </div>
				</script>

				<? //Yii::app()->clientScript->registerCssFile('https://d1k75bnu87kt9p.cloudfront.net/assets/js/jquery/plugins/jcrop/css/jquery.Jcrop.min.css', CClientScript::POS_END);?>
				<? //Yii::app()->clientScript->registerScriptFile('https://d1k75bnu87kt9p.cloudfront.net/assets/js/jquery/plugins/jcrop/js/jquery.Jcrop.min.js', CClientScript::POS_END);?>
				<? Yii::app()->clientScript->registerScript('uploadPhotoJs', <<<JS

					// Bind upload photo button
					$('#btn-upload-photo').on('click', function(e) {

						// Prevent default behavior
						e.preventDefault();

						// Create modal
						$.when(
							stmModal.create({
								title: 'Upload New Photo',
								contentVariables: {

								},
								content: $('#template-upload-photo').html(),
							})
						).then( function(){

							// Hold crop values
							var cropValues;

							// Callback for retrieving coordintate values
							var getCoords = function(c) {
								cropValues = c;
							};

							// Helper function to setup cropper and stuff
							var fileHandler = function(file) {

								// Only do this for images
								if (file.type.match(/image.*/)) {

									// Read in file data
									var reader = new FileReader();
									reader.onload = function(e2) {

										// Update image source
										document.getElementById('the-photo').style.display = 'block';
										document.getElementById('the-photo').src = e2.target.result;

										// Use JCrop (I'm sorry yahoo, as much as I want to use YUI for this, i just can't...)
										jQuery('#the-photo').Jcrop({
											minSize: [100, 100],
											setSelect: [30, 30, 300, 300],
											boxWidth: 300,
											boxHeight: 300,
											aspectRatio: .76,
											onSelect: getCoords,
											onChange: getCoords
										});
									}
									reader.readAsDataURL(file); // start reading the file data.

									// Enable upload button
									$('#btn-save-profile-photo').prop('disabled', false);
								}
							}

							// Create a drop zone
							var dropZone = document.getElementById('file-drop');

							// Show the copy icon when dragging over.  Seems to only work for chrome.
							dropZone.addEventListener('dragover', function(e) {
								e.stopPropagation();
								e.preventDefault();
								e.dataTransfer.dropEffect = 'copy';
							});

							// Get file data on drop
							var file;
							dropZone.addEventListener('drop', function(e) {

								// Stop default behavior
								e.stopPropagation();
								e.preventDefault();

								// Pull the first file that was dropped
								file = e.dataTransfer.files[0]; // Array of all files

								// Pass this off to file handler
								fileHandler(file);
							});

							// Bind file browse change event
							$('#file-browser').on('change', function(e) {
								file = this.files[0];
								fileHandler(file);
							});

							// Bind upload button
							$('#btn-save-profile-photo').on('click', function(e) {

								// Show loading container
								$("body").prepend("<div class='loading-container loading'><em></em></div>");

								// Create new form data object
								var formData = new FormData();

								// Append the file and other info
								formData.append('contact_id', $('#contact_id').val());
								formData.append('video_file', file);
								formData.append('x', cropValues.x);
								formData.append('y', cropValues.y);
								formData.append('w', cropValues.w);
								formData.append('h', cropValues.h);

								// Make AJAX call
								jQuery.ajax({
									method: 'POST',
									url: "/admin/settings/saveProfilePhoto",
									data: formData,
									contentType: false,
									processData: false,
									success: function(e) {
										location.reload(true);
									}
								});
							});
						});
					});

JS
					, CClientScript::POS_END);
				?>
				<!-- End upload new photo stuff -->

			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('gender')->label; ?>:</th>
			<td>
				<?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('gender')->id . ']', array(
						Settings::GENDER_FEMALE => 'Female',
						Settings::GENDER_MALE => 'Male'
					), $htmlOptions = array('empty' => 'Select One')
				); ?>
                <a href="/<?php echo Yii::app()->controller->module->name;?>/contacts/edit/<?php echo $Contact->id; ?>" target="_blank" class="button gray icon i_stm_edit">Edit Contact</a>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo 'Name:'; ?></th>
			<td>
				<?php echo $form->textField($Contact, 'first_name', $htmlOptions = array(
						'placeholder' => 'First Name',
                        'autocomplete'=>'off',
						'class' => 'g6'
					)
				) ?>
				<?php echo $form->textField($Contact, 'last_name', $htmlOptions = array(
						'placeholder' => 'Last Name',
                        'autocomplete'=>'off',
						'class' => 'g5',
                        'data-value'=>$Contact->last_name,
					)
				) ?>
			</td>
		</tr>
        <tr>
            <th class="narrow"><?php echo 'Password:'; ?></th>
            <td>
                <?php echo $form->passwordField($Contact, 'password', $htmlOptions = array(
                        'placeholder' => 'Password',
                        'autocomplete'=>'off',
                        'class' => 'g11'
                    )
                ) ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('anniversary_date')->label; ?>:</th>
            <td>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$SettingContactValues,
                        'attribute'=>'data[' . $model->getField('anniversary_date')->id . ']',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=>'mm/dd/yy',
                        ),
                        'htmlOptions'=>array(
                            'class'=>'g4',
                            'placeholder' => 'Work Anniversary Date',
                        ),
                    ));
                ?>
            </td>
        </tr>
        <?php if(Yii::app()->user->checkAccess('permissions') || Yii::app()->user->checkAccess('owner')): ?>
		<tr>
			<th class="narrow"><?php echo 'Permission:'; ?></th>
			<td>
				<?php
                //todo: need to move into action then pass as variable.
                $userTypeDropDownData = array();
                if(count( array_intersect($AuthAssignment->itemnameCollection,array('owner','agent')))) {
                    $userInData = array('owner','agent','recruitReferringAgent','noSellerAccess','noBuyerAccess','limitedClosingAccess','inactive','commissionEditAccess','hideTaskListCompleteButton','hideTaskListDeleteButton','dialerListRequeue','dialerimport','import');
                } elseif(array_intersect($AuthAssignment->itemnameCollection,array('recruitReferringAgent'))) {
                    $userInData = array('recruitReferringAgent','inactive');
                } elseif(array_intersect($AuthAssignment->itemnameCollection,array('lender'))) {
                    $userInData = array('lender','inactive');
                } else {
                    $userInData = array('owner','agent','lender','inactive');
                }

                if($isOnlyOwner !== true || !AuthAssignment::model()->countByAttributes(array('itemname'=>'owner','userid'=>$Contact->id))) {
                    echo $form->dropDownList($AuthAssignment, 'itemnameCollection', CHtml::listData(AuthItem::model()->byIn('name',$userInData)->findAll(), 'name', 'label'),
                        $htmlOptions = array('class' => 'g11','multiple'=>'multiple')
                    );
                    $this->widget('admin_module.extensions.EChosen.EChosen', array(
                                                                             'target' => 'select#AuthAssignment_itemnameCollection',
                                                                             'options'=>array(
//                                                                                 'allow_single_deselect'=>true,
                                                                             ),
                                                                       ));
                } else {
                    echo 'You are the only Owner for this account. There must be at least one Owner for this account so please assign another Owner in order to change this permission from Owner to a different one.';
                }
                ?>
			</td>
		</tr>
        <?php endif; ?>
		<tr>
			<th class="narrow"><?php echo $model->getField('title')->label; ?>:</th>
			<td>
				<?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('title')->id . ']', $htmlOptions = array(
						'placeholder' => 'Title',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('subtitle')->label; ?>:</th>
			<td>
				<?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('subtitle')->id . ']', $htmlOptions = array(
						'placeholder' => 'Subtitle',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('designations')->label; ?>:</th>
			<td>
				<?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('designations')->id . ']', $htmlOptions = array(
						'placeholder' => 'Designations',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('bio')->label; ?>:</th>
            <td>
                <?php echo $form->textArea($SettingContactValues, 'data[' . $model->getField('bio')->id . ']', $htmlOptions = array(
                        'placeholder' => 'Bio Description',
                        'class' => 'g11',
                        'rows' => '15'
                    )
                ) ?>
            </td>
        </tr>
	</table>
</div>
<div class="g6 ">
	<table class="">
        <?php if(array_intersect($AuthAssignment->itemnameCollection, array('owner','agent'))): ?>
		<tr>
			<th class="narrow"><?php echo $model->getField('agent_mls_num')->label; ?>:</th>
			<td>
				<?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('agent_mls_num')->id . ']', $htmlOptions = array(
						'placeholder' => 'Agent MLS #',
						'class' => 'g6'
					)
				) ?>
			</td>
		</tr>
        <?php endif; ?>
        <?php if(Yii::app()->user->checkAccess('sms')): //in_array(strtolower($_SERVER['SERVER_NAME']), array('www.christineleeteam.com','www.christineleeteam.local'))?>
        <tr>
            <th class="narrow">STM Provided SMS #:</th>
            <td><?php echo (!empty($smsProvidedNumber) ? $smsProvidedNumber : ((Yii::app()->user->checkAccess('owner'))?'<button id="register-stm-number" type="button">Get a STM Number</button>' : '')); ?></td>
        </tr>
        <?php endif; ?>
		<tr>
			<th class="narrow"><?php echo $model->getField('cell_number')->label; ?>:</th>
			<td>
				<?php if($SettingContactValues['data'][$model->getField('sms_verified')->id] != 1): ?>
					<?php $this->widget('StmMaskedTextField', array(
							'model' => $SettingContactValues,
							'attribute' => 'data[' . $model->getField('cell_number')->id . ']',
							'mask' => '(999) 999-9999',
							'htmlOptions' => $htmlOptions = array(
								'placeholder' => 'Cell #',
								'class' => 'g6',
								'id' => 'cell_number_field'
							),
						)
					); ?>
					<?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('cell_carrier_id')->id . ']', CHtml::listData(MobileGateways::model()->findAll(), 'id', 'carrier_name'),
						$htmlOptions = array('empty' => 'Select One', 'id' => 'cell_carriers_list')
					); ?>
					<?php echo CHtml::link('Send Verification Code','javascript:void(0)',$htmlOptions=array('class'=>'button gray icon i_stm_comment grey-button verify-sms-button','data-id'=>$Contact->id))?>
				<?php else: ?>
					<?php $this->widget('StmMaskedTextField', array(
							'model' => $SettingContactValues,
							'attribute' => 'data[' . $model->getField('cell_number')->id . ']',
							'mask' => '(999) 999-9999',
							'htmlOptions' => $htmlOptions = array(
								'placeholder' => 'Cell #',
								'class' => 'g6',
								'disabled' => 'disabled',
								'id' => 'cell_number_field'
							),
						)
					); ?>
					<?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('cell_carrier_id')->id . ']', CHtml::listData(MobileGateways::model()->findAll(), 'id', 'carrier_name'),
						$htmlOptions = array('empty' => 'Select One', 'disabled' => 'disabled', 'id' => 'cell_carriers_list')
					); ?>
					<span id="verifiedNote" style="color: green;">SMS VERIFIED</span>
					<?php echo CHtml::link('Change SMS Number','javascript:void(0)',$htmlOptions=array('class'=>'button gray icon i_stm_comment grey-button change-sms-button','data-id'=>$Contact->id))?>
					<?php echo CHtml::link('Send Verification Code','javascript:void(0)',$htmlOptions=array('class'=>'button gray icon i_stm_comment grey-button verify-sms-button','data-id'=>$Contact->id,'style' => 'display: none;'))?>
				<?php endif; ?>
			</td>
		</tr>

		<tr id="sms_verified_conatiner"<?php if($SettingContactValues['data'][$model->getField('sms_verified')->id] != 1): ?> style="display: table-row;"<?php else: ?> style="display: none;"<?php endif; ?>>
			<th class="narrow">SMS Verification Code:</th>
			<td>
				<?php echo Chtml::textField('sms_verification_code','',$htmlOptions = array(
						'class' => 'g6',
						'minlength' => 6,
						'maxlength'=>6,
						'id' => 'sms_verification_code'
					));
				/*$form->textField($SettingContactValues, 'data[' . $model->getField('sms_verification_code')->id . ']', $htmlOptions = array(
						'class' => 'g6',
						'minlength' => 6,
						'maxlength'=>6,
						'id' => 'sms_verification_code'
					)
				)*/ ?>
				<?php echo CHtml::link('Verify SMS Code','javascript:void(0)',$htmlOptions=array('class'=>'button gray icon i_stm_comment grey-button verify-sms-code','data-id'=>$Contact->id))?>
			</td>
		</tr>
        <?php if(array_intersect($AuthAssignment->itemnameCollection,array('owner','agent'))):?>
		<tr>
			<th class="narrow"><?php echo $model->getField('team_direct_phone')->label; ?>:</th>
			<td>
				<?php $this->widget('StmMaskedTextField', array(
						'model' => $SettingContactValues,
						'attribute' => 'data[' . $model->getField('team_direct_phone')->id . ']',
						'mask' => '(999) 999-9999',
						'htmlOptions' => $htmlOptions = array(
							'placeholder' => 'Phone #',
							'class' => 'g6'
						),
					)
				); ?>
			</td>
		</tr>
<!--        <tr>-->
<!--            <th class="narrow">--><?php //echo $model->getField('first_responder_alert_enabled')->label; ?><!--:</th>-->
<!--            <td>-->
<!--                --><?php //echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('first_responder_alert_enabled')->id . ']', StmFormHelper::getYesNoList(), $htmlOptions = array('empty' => 'Select One')); ?>
<!--            </td>-->
<!--        </tr>-->

<!--		<tr>-->
<!--			<th class="narrow">--><?php //echo $model->getField('agent_website')->label; ?><!--:</th>-->
<!--			<td>-->
<!--				--><?php //echo $form->textField($SettingContactValues, 'data[' . $model->getField('agent_website')->id . ']', $htmlOptions = array(
//						'placeholder' => 'Agent Website',
//						'class' => 'g6'
//					)
//				) ?><!--<span class="label p-pt8">.--><?php //echo str_replace('www.','',$_SERVER['SERVER_NAME']); ?><!--</span>-->
<!--			</td>-->
<!--		</tr>-->
		<tr>
			<th class="narrow">Dashboard Layout:</th>
			<td>
				<?php
                if(Yii::app()->user->client->type == 'Agent'):
                    echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('dashboard')->id . ']', $model->dashboardTypes, $htmlOptions = array('empty' => 'Select One'));
                else:
                    echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('dashboard')->id . ']', array(Settings::DASHBOARD_TYPE_GENERAL_ADMIN_VIEW => 'Task List (Default/Recommended)'), $htmlOptions = array('empty' => 'Select One'));
                endif;
                ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('default_assignment_type')->label; ?>:</th>
			<td>
				<?php
                    $assignmentTypes = array('buyer_agent','listing_agent','listing_manager','isa','closing_manager','showing_partner');
                    echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('default_assignment_type')->id . ']', CHtml::listData(AssignmentTypes::model()->byNames($assignmentTypes)->findAll(), 'id', 'display_name'),
						$htmlOptions = array(
							'class' => 'g6',
							'empty' => 'Select One'
						)
					);
				?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('primary_goal_id')->label; ?>:</th>
			<td>
				<?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('primary_goal_id')->id . ']', CHtml::listData(Goals::getListByContactId(Yii::app()->user->id), 'id', 'name'),
					$htmlOptions = array('class' => 'g6')
				);
				?>
			</td>
		</tr>
        <?php endif; ?>
        <?php if(Yii::app()->user->checkAccess('owner')):?>
        <tr>
            <th class="narrow"><?php echo $model->getField('profile_on_website')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('profile_on_website')->id . ']', StmFormHelper::getStatusBooleanList(), $htmlOptions = array('class' => 'g6'));
                ?>
                <div class="stm-tool-tip question p-fl">
                <span>
                    If active, your profile will be public to anyone that visits on your <a href="/team" target="_blank">Meet the Team</a> page.
                </span>
            </div>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('tasks_past_due_redirect')->label; ?>:</th>
            <td>
                <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('tasks_past_due_redirect')->id . ']', $htmlOptions = array('placeholder' => '# Task Past Due Redirect','class' => 'g6')); ?><span class="label p-pt8"> Enter Zero to disable.</span>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('appointments_past_due_redirect')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('appointments_past_due_redirect')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array('empty' => '')); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('new_opportunity_alert')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('new_opportunity_alert')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array('empty' => '')); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('timeblocks_past_due_redirect')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('timeblocks_past_due_redirect')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array('empty' => '')); ?>
                <span class="label"><?php echo $model->getField('timeblocks_past_due_redirect_start_date')->label; ?>:</span>
                <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $SettingContactValues,
                        'attribute' => 'data[' . $model->getField('timeblocks_past_due_redirect_start_date')->id . ']',
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'placeholder' => 'Start Date',
                            'style'=>'width: 25%;',
                        ),
                    ));
                ?>
            </td>
        </tr>

        <tr>
            <th class="narrow"><?php echo $model->getField('one_thing_daily_past_due_redirect')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('one_thing_daily_past_due_redirect')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array('empty' => '')); ?>
            </td>
        </tr>
<!--        <tr>-->
<!--            <th class="narrow">--><?php //echo $model->getField('hide_phone_numbers_search_results')->label; ?><!--:</th>-->
<!--            <td>-->
<!--                --><?php //echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('hide_phone_numbers_search_results')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array()); ?><!--<span class="label p-pt8"> All calls made through system. Click to Call required to dial leads.</span>-->
<!--            </td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th class="narrow">--><?php //echo $model->getField('appointments_past_due_redirect')->label; ?><!--:</th>-->
<!--            <td>-->
<!--                --><?php //echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('appointments_past_due_redirect')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array('empty' => '')); ?>
<!--            </td>-->
<!--        </tr>-->
        <?php endif; ?>
        <tr>
            <th class="narrow"><?php echo $model->getField('click_to_call_initial_number')->label; ?>:</th>
            <td>
                <?php $this->widget('StmMaskedTextField', array(
                        'model' => $SettingContactValues,
                        'attribute' => 'data[' . $model->getField('click_to_call_initial_number')->id . ']',
                        'mask' => '9999999999',
                        'htmlOptions' => array(
                            'placeholder' => 'Click to Call Your Initial #',
                            'class' => 'g6',
                        ),
                    )
                );
                ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('click_to_call_caller_id_name')->label; ?>:</th>
            <td>
                <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('click_to_call_caller_id_name')->id . ']', $htmlOptions = array('placeholder' => 'Click to Call Caller ID Name','class' => 'g6')); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('click_to_call_caller_id_number')->label; ?>:</th>
            <td>
                <?php $this->widget('StmMaskedTextField', array(
                        'model' => $SettingContactValues,
                        'attribute' => 'data[' . $model->getField('click_to_call_caller_id_number')->id . ']',
                        'mask' => '9999999999',
                        'htmlOptions' => array(
                            'placeholder' => 'Click to Caller ID #',
                            'class' => 'g6',
                        ),
                    )
                );
                ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('click_to_call_popup')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('click_to_call_popup')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array()); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $model->getField('click_to_call_skip_confirmation')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('click_to_call_skip_confirmation')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array()); ?><span class="label p-pt8"> Click to Call Initial Number must match the Verified Cell Number setting.</span>
            </td>
        </tr>
        <?php if(Yii::app()->user->checkAccess('owner')): ?>
        <tr>
            <th class="narrow"><?php echo $model->getField('show_assigned_leads_only')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('show_assigned_leads_only')->id . ']', StmFormHelper::getYesNoList(false), $htmlOptions = array()); ?><span class="label p-pt8">   If Yes, Agent can only see leads that are assigned to them.</span>
            </td>
        </tr>
        <?php endif; ?>
        <?php if(array_intersect($AuthAssignment->itemnameCollection,array('lender'))):?>
            <tr>
                <th class="narrow"><?php echo $model->getField('lender_license_number')->label; ?>:</th>
                <td>
                    <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('lender_license_number')->id . ']', $htmlOptions = array('placeholder' => 'Lender License #','class' => 'g11')) ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $model->getField('lender_company_name')->label; ?>:</th>
                <td>
                    <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('lender_company_name')->id . ']', $htmlOptions = array('placeholder' => 'Lender Company','class' => 'g11')) ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $model->getField('lender_address')->label; ?>:</th>
                <td>
                    <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('lender_address')->id . ']', $htmlOptions = array('placeholder' => 'Lender Address','class' => 'g11')) ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"></th>
                <td>
                    <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('lender_city')->id . ']', $htmlOptions = array('placeholder' => 'City','class' => 'g5')) ?>
                    <?php echo $form->dropDownList($SettingContactValues, 'data[' . $model->getField('lender_state_id')->id . ']', CHtml::listData(AddressStates::model()->findAll(),'id','short_name'),$htmlOptions = array('placeholder' => 'State','class' => 'g2','empty'=>'')) ?>
                    <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('lender_zip')->id . ']', $htmlOptions = array('placeholder' => 'Zip','class' => 'g4')) ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $model->getField('lender_office_phone')->label; ?>:</th>
                <td>
                    <?php $this->widget('StmMaskedTextField', array(
                            'model' => $SettingContactValues,
                            'attribute' => 'data[' . $model->getField('lender_office_phone')->id . ']',
                            'mask' => '(999) 999-9999',
                            'htmlOptions' => $htmlOptions = array(
                                    'placeholder' => 'Office #',
                                    'class' => 'g6'
                                ),
                        )
                    ); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $model->getField('lender_direct_phone')->label; ?>:</th>
                <td>
                    <?php $this->widget('StmMaskedTextField', array(
                            'model' => $SettingContactValues,
                            'attribute' => 'data[' . $model->getField('lender_direct_phone')->id . ']',
                            'mask' => '(999) 999-9999',
                            'htmlOptions' => $htmlOptions = array(
                                    'placeholder' => 'Direct #',
                                    'class' => 'g6'
                                ),
                        )
                    ); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $model->getField('lender_fax')->label; ?>:</th>
                <td>
                    <?php $this->widget('StmMaskedTextField', array(
                            'model' => $SettingContactValues,
                            'attribute' => 'data[' . $model->getField('lender_fax')->id . ']',
                            'mask' => '(999) 999-9999',
                            'htmlOptions' => $htmlOptions = array(
                                    'placeholder' => 'Fax #',
                                    'class' => 'g6'
                                ),
                        )
                    ); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $model->getField('lender_website')->label; ?>:</th>
                <td>
                    <?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('lender_website')->id . ']', $htmlOptions = array('placeholder' => 'Lender Website','class' => 'g11')) ?>
                </td>
            </tr>
        <?php endif; ?>


	</table>
</div>
<table class="">
	<tr>
		<th style="width:8.75%;"><?php echo $model->getField('email_signature')->label; ?>:</th>
		<td>****NOTE: Do NOT cut and paste from any other document. Thank you. =)****
			<?php
				/*$this->widget('admin_module.extensions.tinymce.ETinyMce', array(
						'name' => 'SettingContactValues[data][' . $model->getField('email_signature')->id . ']',
						'value' => $SettingContactValues->data[$model->getField('email_signature')->id],
						'useSwitch' => false,
						'options' => array(
							'theme' => 'advanced',
							'theme_advanced_toolbar_location' => 'top',
							'theme_advanced_buttons1' => 'bold,italic,underline,separator,fontselect,fontsizeselect,separator,forecolor, backcolor,separator,justifyleft,justifycenter,justifyright,separator,bullist,numlist,separator,undo,redo,link,unlink,removeformat,code',
							'theme_advanced_buttons2' => '',
							'theme_advanced_buttons3' => '',
                            'forced_root_block' => '',
                            'force_br_newlines' => true,
                            'force_p_newlines' => false,
                            'width' => '97%',
							'height' => '250',
							//								'apply_source_formatting'         => false,
						),
					)
				);*/
			Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);

$scope = (YII_DEBUG) ? "local" : "com";
$s3BaseUrl = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";

$s3Url = urlencode(base64_encode('s3://'.$s3BaseUrl.'/site-files/'.Yii::app()->user->clientId."/emailSignatures/".$Contact->id));
$emailSignatureSettingId = $model->getField('email_signature')->id;
$js = <<<JS
	tinymce.PluginManager.load('moxiemanager', 'http://www.seizethemarket.{$scope}/imagemanager/plugin.js?v=4');
	tinymce.init({
		convert_urls: false,
		relative_urls: false,
		height : 300,
		selector: "#SettingContactValues_data_$emailSignatureSettingId",
		plugins: [
			"advlist autolink lists link image charmap print preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste moxiemanager"
		],
		toolbar: "insertfile undo redo | styleselect | fontselect fontsizeselect forecolor backcolor bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		autosave_ask_before_unload: false,
		setup : function(ed) {
		    ed.on('init', function() { this.getDoc().body.style.fontSize = '13px'; });
		    ed.on('keyUp', function() { tinyMCE.triggerSave(); });
		},
		//moxiemanager_apiPageUrl: "http://www.seizethemarket.{$scope}/imagemanager/api.php,
		//moxiemanager_baseUrl: "http://www.seizethemarket.{$scope}/imagemanager"
		init_instance_callback: function () {
			moxman.Env.baseUrl = "http://www.seizethemarket.{$scope}/imagemanager";
			moxman.Env.apiPageName = "api.php?v=1&s3={$s3Url}";
			//moxman.path = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
			//moxman.rootpath = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
		},
		//moxiemanager_rootpath: 's3://{$s3BaseUrl}/site-files/{$s3Url}',
		//moxiemanager_path: '/site-files/{$s3Url}',
	});
JS;

		//http://www.seizethemarket.local/imagemanager/api.php?
		//s3=czM6Ly9kZXYuc2l0ZXMuc2VpemV0aGVtYXJrZXQuY29tL3NpdGUtZmlsZXMvMS9jbXNQYWdlcy8%3D
		//&action=streamfile&path=%2FcmsPages
		//&name=one-story-craftsman-style-home-plans-l-d28baafe08f326b5.jpg
		//&loaded=0
		//&total=105057
		//&id=null
		//&csrf=41d9afb847906a6d5bafccc453a2c6a461204ab064876d1e1a0c198a554db7d2
		//&resolution=default

				Yii::app()->clientScript->registerScript('tinyMceNew', $js); ?>
				<textarea id="SettingContactValues_data_<?=$model->getField('email_signature')->id;?>" name="SettingContactValues[data][<?=$model->getField('email_signature')->id;?>]">
					<?=$SettingContactValues->data[$model->getField('email_signature')->id];?>
				</textarea>
		</td>
	</tr>
</table>
<hr style="border-top-color:#BBB;"/>
<div class="g6 p-clr">
	<table class="">
		<tr>
			<th class="narrow"><?php echo $ContactAttributes->getField('books')->label; ?>:</th>
			<td>
				<?php echo $form->textField($ContactAttributeValues, 'data[' . $ContactAttributes->getField('books')->id . ']', $htmlOptions = array(
						'placeholder' => 'Favorite Books',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('favorite_quotes')->label; ?>:</th>
			<td>
				<?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('favorite_quotes')->id . ']', $htmlOptions = array(
						'placeholder' => 'Favorite Quotes',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $ContactAttributes->getField('restaurants')->label; ?>:</th>
			<td>
				<?php echo $form->textField($ContactAttributeValues, 'data[' . $ContactAttributes->getField('restaurants')->id . ']', $htmlOptions = array(
						'placeholder' => 'Favorite Restaurants',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $ContactAttributes->getField('movies')->label; ?>:</th>
			<td>
				<?php echo $form->textField($ContactAttributeValues, 'data[' . $ContactAttributes->getField('movies')->id . ']', $htmlOptions = array(
						'placeholder' => 'Favorite Movies',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
	</table>
</div>
<div class="g6 ">
	<table class="">
		<tr>
			<th class="narrow"><?php echo $ContactAttributes->getField('hobbies')->label; ?>:</th>
			<td>
				<?php echo $form->textField($ContactAttributeValues, 'data[' . $ContactAttributes->getField('hobbies')->id . ']', $htmlOptions = array(
						'placeholder' => 'Hobbies',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('words_describe_self')->label; ?>:</th>
			<td>
				<?php echo $form->textField($SettingContactValues, 'data[' . $model->getField('words_describe_self')->id . ']', $htmlOptions = array(
						'placeholder' => 'Words to Describe Self',
						'class' => 'g11'
					)
				) ?>
			</td>
		</tr>
		<tr>
			<th class="narrow"><?php echo $model->getField('memorable_moments')->label; ?>:</th>
			<td>
				<?php echo $form->textArea($SettingContactValues, 'data[' . $model->getField('memorable_moments')->id . ']', $htmlOptions = array(
						'placeholder' => 'Memorable Moments',
						'class' => 'g11',
						'rows' => 4
					)
				) ?>
			</td>
		</tr>
	</table>
</div>
</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
	<button type="submit" class="submit wide">Update Settings</button>
</div>
<?php $this->endWidget();

	$ProfilePhotoSetting = new SettingContactValues;
	$ProfilePhotoSetting->contact_id = $Contact->id;
	$this->widget('admin_widgets.DialogWidget.ProfilePhotoDialogWidget.ProfilePhotoDialogWidget', array(
			'id' => ProfilePhotoDialogWidget::PROFILE_PHOTO_DIALOG_ID,
			'title' => 'Profile Photo',
			'triggerElement' => '',
			'contactId' => $Contact->id,
			'parentModel' => $ProfilePhotoSetting,
			'photoPath' => $SettingContactValues['data'][$model->getField('profile_photo')->id],
		)
	);

	$this->widget('admin_widgets.DialogWidget.ChooseSmsNumberDialogWidget.ChooseSmsNumberDialogWidget', array(
			'id' => 'search-sms-numbers-dialog-'.$Contact->id,
			'title' => 'Configure SMS Number',
			'contactId' => $Contact->id,
            'triggerElement' => '#register-stm-number',
		)
	);