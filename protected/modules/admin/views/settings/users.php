<?php
	$this->breadcrumbs = array(
		'Users' => '',
	);

	Yii::app()->clientScript->registerScript('search', '
$("#listview-search form").submit(function() {
	$.fn.yiiGridView.update("users-grid", { data: $(this).serialize() });
	return false;
});
'
	);
?>
	<div id="listview-actions">
        <a href="/<?php echo Yii::app()->controller->module->name;?>/settings/usersLenderAdd" class="button gray icon i_stm_add">Add New Lender</a>
		<a href="/<?php echo Yii::app()->controller->module->name;?>/settings/usersAdd" class="button gray icon i_stm_add">Add New User</a>
	</div>
	<div id="content-header">
		<h1>User List</h1>
	</div>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $form=$this->beginWidget('CActiveForm', array(
												  'action'=>Yii::app()->createUrl($this->route),
												  'method'=>'get',
												  'id'=>'users-list-search',
												  )); ?>

        <div class="g2">
            <label class="g5">Status:</label>
            <span class="g7">
                <?php echo CHtml::dropDownList('authAssignmentIsActive', $authAssignmentIsActive, array(1=>'Active', 0=>'Inactive', 2=>'All Status')); ?>
            </span>
        </div>
        <div class="g3">
            <label class="g5">User Role:</label>
                <span class="g7">
                    <?php echo CHtml::dropDownList('role', null, array(''=>'','owner'=>'Owner', 'agent'=>'Agent', 'lender'=>'Lender'),$htmlOptions=array('class'=>'g9')); ?>
                </span>
        </div>
		<div class="g4">
			<label class="g2">Name:</label>
			<span class="g4 p-mr8"><?php echo $form->textField($model,'first_name',$htmlOptions=array('placeholder'=>'First Name'));?></span>
			<span class="g4"><?php echo $form->textField($model,'last_name',$htmlOptions=array('placeholder'=>'Last Name'));?></span>
		</div>
		<div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
	<?php $this->endWidget(); ?>
</div><!-- search-form -->


<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'users-grid',
		'dataProvider' => $DataProvider,
		'itemsCssClass' => 'datatables',
		'columns' => array(
			'first_name',
			'last_name',
			'primaryEmail',
			array(
				'type' => 'raw',
				'name' => 'Permission Role',
				'value' => 'Yii::app()->format->formatCommaDelimited($data->userGroups,"label")',
			),
			array(
				'type' => 'raw',
				'name' => 'Added Date',
				'value' => 'Yii::app()->format->formatDate($data->added)." &nbsp;(".Yii::app()->format->formatDays($data->added).")"',
			),
            'id',
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/settings/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit Settings</a></div>"',
				'htmlOptions' => array('style' => 'width:140px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Contact</a></div>"',
				'htmlOptions' => array('style' => 'width:140px'),
			),
		),
	)
);
