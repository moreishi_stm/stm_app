<?
$this->breadcrumbs=array(
	'Account'=>'',
);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/settings" class="button gray icon i_stm_edit">Edit User Settings</a>
</div>

<div id="settings-header">
	<h1 class="name">Account Settings</h1>
</div>
<?php echo $this->renderPartial('_formAccount', array(
		'model'=>$model,
		'Settings'=>$Settings,
		'SettingAccountValues'=>$SettingAccountValues,
		'logoUrl' => $logoUrl
));