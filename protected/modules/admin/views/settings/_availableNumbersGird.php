<?php 

if($count > 0) {
	$this->widget('admin_module.components.StmGridView', array(
		'id' => 'available-numbers-grid',
		'dataProvider' => $dataProvider,
		'itemsCssClass' => 'datatables',
		'pager'=> array('id'=>'numbersPerPage','class' => 'admin_module.components.StmListPager'),
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Use',
				'value' => 'Yii::app()->controller->action->makeAvailableNumbersRadio($data)',
				'htmlOptions' => array('style' => 'width:50px; text-align: center;'),
			),
			array(
				'type' => 'raw',
				'name' => 'Number',
				'value' => 'Yii::app()->controller->action->makeAvailableNumbersText($data)',
				'htmlOptions' => array('style' => 'text-align: center;'),
			)
		),
	));
} else {?>
<div>No results found</div>
<?php } ?>
