<?
$js = <<<JS
	    $('.addNewUserLender').live('click', function() {
	        if(confirm('Confirm adding '+ $(this).data('name') +' as a new lender.')) {
                //submit the form that it's in
                $('form#addUserLenderForm-'+$(this).data('id')).submit();
	        }
	            return false;
	    });
JS;
Yii::app()->clientScript->registerScript('addUserLender', $js);

$this->breadcrumbs=array(
	'User'=>'/'.Yii::app()->controller->module->id.'/settings/users',
	'Add Lender'=>'',
);

if($isMaxUsers=0) { ?>
    <h1>You have reached your max number of users.</h1>
    <h3>If you wish to add a new user, inactivate one of the users then add a new one.</h3>
<!--    <h3><a href="/--><?php //echo $this->module->id;?><!--/settings/users">Click here to review your list of users.</a></h3>-->
<?php
} else {
?>
    <div id="settings-header">
    	<h1 class="name">Add New Lender</h1>
    </div>

    <div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
        <?php $this->renderPartial('_listSearchBox', array(
                                                     'model' => $model,
                                                     )
        ); ?>
    </div><!-- search-form -->
<?php
}

$this->widget('admin_module.components.StmGridView', array(
        'id' => 'contact-grid',
        'template'=>'{summary}{items}{summary}{pager}',
        'dataProvider' => $DataProvider,
        'enableSorting'=>true,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            'first_name',
            'last_name',
            array(
                'type' => 'raw',
                'name' => 'User Role',
                'value' => 'Yii::app()->format->formatCommaDelimited($data->userGroups,"label")',
            ),
            'primaryEmail',
            array(
                'type' => 'raw',
                'name' => '',
                'value' => 'Yii::app()->controller->action->printUserButton($data)',
                'htmlOptions' => array('style' => 'width:120px'),
            ),
        ),
    )
);