<?php
$defaultHtmlOptions = array(
	'class'=>'topLinkRow g12 p-p0',
);
if ( isset($containerHtmlOptions) )
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
else
	$containerHtmlOptions = $defaultHtmlOptions;

// Determine which model to use
$inputHtmlOptions = array('placeholder'=>'Label', 'class'=>'g2');
$linkHtmlOptions = array('placeholder'=>'URL', 'class'=>'g5');
$sortOrderHtmlOptions = array('placeholder'=>'Order', 'class'=>'p-fl', 'style'=>'width:25px;');
$windowHtmlOptions = array('placeholder'=>'Window', 'class'=>'g2', 'style'=>'width:145px;');

if ( isset($data) ) {
    // for actual email records
	$index = "[$i]";
} else {
    // for the email template base
	$index = '[0]';
	$inputHtmlOptions = CMap::mergeArray($inputHtmlOptions, array(
		'disabled'=>'disabled',
	));
	$linkHtmlOptions = CMap::mergeArray($linkHtmlOptions, array(
		'disabled'=>'disabled',
	));
	$sortOrderHtmlOptions = CMap::mergeArray($sortOrderHtmlOptions, array(
		'disabled'=>'disabled',
	));
	$windowHtmlOptions = CMap::mergeArray($windowHtmlOptions, array(
		'disabled'=>'disabled',
	));
}

echo CHtml::openTag('div', $containerHtmlOptions);
?>
<div class="g10">
	<?php echo CHtml::textField('topLink'.$index.'[label]', $data['label'], $inputHtmlOptions); ?>
	<?php echo CHtml::textField('topLink'.$index.'[url]', $data['url'], $linkHtmlOptions); ?>
	<div class="p-fl p-p5 label">Order:</div>
	<?php echo CHtml::textField('topLink'.$index.'[sort_order]', $data['sort_order'], $sortOrderHtmlOptions); ?>
	<div class="p-fl p-p5 label">Click:</div>
	<?php  $data['window'] = (isset($data['window']))? $data['window'] : '';
		echo CHtml::dropDownList('topLink'.$index.'[window]', $data['window'], array('_blank'=>'Open New Window','_parent'=>'Same Window'), $windowHtmlOptions); ?>
</div>
<div class="g2 p-pl8">
	<div style="margin-top: -6px">
		<button type="button" class="text remove-top-link"><em class="icon i_stm_delete"></em>Remove</button>
	</div>
</div>
<?php echo CHtml::closetag('div'); ?>