<?php
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'meeting-notes-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Date / Name (Met by)',
            'value'=>'Yii::app()->format->formatDate($data->date)."<br><br><strong>".$data->contact->fullName."</strong><br>(".$data->metBy->fullName.")"',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Goal %',
            'value'=>'($data->percent_goal_accomplished)? $data->percent_goal_accomplished."%" : "-"',
            'htmlOptions'=>array('style'=>'width:50px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Action Items %',
            'value'=>'($data->percent_prev_action_items_accomplished)? $data->percent_prev_action_items_accomplished."%" : "-"',
            'htmlOptions'=>array('style'=>'width:50px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Accountability',
            'value'=>'nl2br($data->accountability)',
            'htmlOptions'=>array('style'=>'min-width:25%'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Knowledge / Growth',
            'value'=>'nl2br($data->knowledge)',
            'htmlOptions'=>array('style'=>'width:150px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Share / Help Others',
            'value'=>'nl2br($data->share)',
        ),
        array(
            'type'=>'raw',
            'name'=>'Challenges',
            'value'=>'nl2br($data->challenges)',
            'htmlOptions'=>array('style'=>'width:150px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Action Items',
            'value'=>'nl2br($data->action_items)',
            'htmlOptions'=>array('style'=>'min-width:25%'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'visible' => (in_array(Yii::app()->controller->action->id, array('list','index'))),
            'value'=>'"<div><a href=\"/admin/meetingNotes/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
            'htmlOptions'=>array('style'=>'width:100px'),
        ),
	),
));