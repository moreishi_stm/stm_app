<?
$this->breadcrumbs=array(
    ucwords(Yii::app()->controller->action->id) => ''
);
?>
<div id="listview-actions">
<!--    <a href="/admin/goals/add" class="button gray icon i_stm_add">Add New Goal</a>-->
</div>
<div id="content-header">
    <h1>Meeting Notes</h1>
</div>

<?php
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'one-thing-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=>$mode.' Meeting Notes',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-container" >
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <div class="g1"></div>
        <div id="goals-left" class="g9">
            <table class="container">
                <tr>
                    <th><?php echo $form->labelEx($model, 'date');?></th>
                    <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
                                'attribute'=>'date',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                ),
                                'htmlOptions'=>array(
                                    'placeholder'=>'Date',
                                    'class'=>'g3',
                                    'readonly'=>'readonly',
                                ),
                            )); ?>
                        <?php echo $form->error($model,'date'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'contact_id');?></th>
                    <td><?php echo $form->dropDownList($model, 'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'contact_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'met_by_id');?></th>
                    <td><?php echo $form->dropDownList($model, 'met_by_id', CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'met_by_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'percent_prev_action_items_accomplished');?></th>
                    <td><?php echo $form->dropDownList($model, 'percent_prev_action_items_accomplished', StmFormHelper::getDropDownList(array('start'=>0,'end'=>500,'increment'=>1,'label'=>'%','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'percent_prev_action_items_accomplished'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'percent_goal_accomplished');?></th>
                    <td><?php echo $form->dropDownList($model, 'percent_goal_accomplished', StmFormHelper::getDropDownList(array('start'=>0,'end'=>500,'increment'=>1,'label'=>'%','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'percent_goal_accomplished'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Accountability:<br>One Thing/Goals, Pipeline, Calendar, Timeblocking<span class="required">*</span></th>
                    <td><?php echo $form->textArea($model, 'accountability', $htmlOptions=array('rows'=>'12')); ?>
                        <?php echo $form->error($model,'accountability'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Action Items for Next Meeting<span class="required">*</span></th>
                    <td><?php echo $form->textArea($model, 'action_items', $htmlOptions=array('rows'=>'6')); ?>
                        <?php echo $form->error($model,'action_items'); ?>
                    </td>
                </tr>
                <tr>
                    <th>What did you learn this week? What changes have taken place in terms of your attitude, skills?<span class="required">*</span></th>
                    <td><?php echo $form->textArea($model, 'knowledge', $htmlOptions=array('rows'=>'3')); ?>
                        <?php echo $form->error($model,'knowledge'); ?>
                    </td>
                </tr>
                <tr>
                    <th>What would you share to help someone else on the team?</th>
                    <td><?php echo $form->textArea($model, 'share', $htmlOptions=array('rows'=>'3')); ?>
                        <?php echo $form->error($model,'share'); ?>
                    </td>
                </tr>
                <tr>
                    <th>What area(s) do you need the greatest help?<span class="required">*</span></th>
                    <td><?php echo $form->textArea($model, 'challenges', $htmlOptions=array('rows'=>'3')); ?>
                        <?php echo $form->error($model,'challenges'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="g3"></div>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Meeting Notes</button></div>
<?php $this->endWidget();
      $meetingNotes = new MeetingNotes();
      $meetingNotes->contact_id = $model->contact_id;
      $this->renderPartial('_grid', array('model' => $meetingNotes));
?>