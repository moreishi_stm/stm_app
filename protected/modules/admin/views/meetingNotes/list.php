<?
// Files needed for Sales graph
//Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
//Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');
//
//$js = <<<JS
//        $('.chart').wl_Chart({
//            flot: {
//                lines:{
////                    fill:true,
//                    fillColor: {
//                        colors: [ { opacity: .2 }, { opacity: .7 } ]
//                    }
//                },
//                // colors: ["#555555","#18CC18","#FFAB4A"], //blue 528EFF
//                yaxis: { tickDecimals: 0},
//                legend: {position: "ne", noColumns: 4}
//            },
//            xlabels:$xLabels, // ['1','2','3']
//            data:[{
//                //this 2nd row Goal values add on top of the Actual, so need to represent the difference between the two
//                label:'Goal Line',
//                lines: {fill: false, lineWidth: 1.5},
//                color: "#555555",
//                points: { radius: 0 },
//                data: $goalData //[[0,1],[1,1],[2,1],[3,0],[4,1],[5,1],[6,0],[7,0],[8,1],[9,1],[10,0],[11,2]]
//            },
//            {
//                label:'% Daily Performance',
//                lines: {fill: true, zero:true},
//                color: "#18CC18",
//                data: $dailyPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
//            },
////            {
////                label:'% Weekly Performance',
////                lines: {fill: false},
////                color: "#528EFF",
////                data: $weeklyPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
////            },
//            {
//                label:'% Monthly Performance',
//                lines: {fill: false},
//                color: "#528EFF", //FFAB4A
//                data: $monthlyPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
//            }]
//        });
//
//    	$(window).resize(function() {
//    	    var newWidth = $('#graph-container').width() - 5; console.log(newWidth);
//    		$('.chart').width(newWidth);
//    	})
//JS;
// example in _sales.php
//Yii::app()->clientScript->registerScript('oneThingGraph', $js);

$this->breadcrumbs=array(
	'List'=>'/admin/meetingNotes'
);

Yii::app()->clientScript->registerScript('searchMeetingNotes', "
        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update('meeting-notes-grid', {
                data: $(this).serialize()
            });
            return false;
        });
");
?>
<div id="listview-actions">
    <a href="/admin/meetingNotes/add" class="button gray icon i_stm_add">Add Meeting Notes</a>
</div>
<div id="content-header">
	<h1>Meeting Notes</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'meeting-notes-search',
        )); ?>
    <div class="g4"></div>
    <div class="g3">
        <label class="g2">Agent:</label>
        <span class="g10"><?php
            echo $form->dropDownList($model,'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions=array('class'=>'g12', 'empty'=>'Everyone'));
            $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => '#MeetingNotes_contact_id')); ?></span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div>

<?php
$this->renderPartial('_grid', array('model' => $model));