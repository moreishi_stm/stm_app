<?php
Yii::app()->clientScript->registerScript('search', <<<JS
    $('form .reset-button').click(function() {

        var componentTypeIdElement = '#LetterTemplates_component_type_id option[value="' + $('#LetterTemplates_component_type_id').val() + '"]';
        $(componentTypeIdElement).removeAttr('selected');
        $('#LetterTemplates_searchContent').val('');
        $('#LetterTemplates_body').val('');
        $('form#letter-templates-list-search').submit();
    });

    $('#listview-search form select').change(function() {
        $(this).submit();
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('letter-templates-grid', { data: $(this).serialize() });
        return false;
    });
JS
);

$form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'letter-templates-list-search',
)); ?>
<div class="g2">
	<label class="g5">Status:</label>
		<span class="g7">
    		<?php echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive'), $htmlOptions=array('empty'=>'Select One')); ?>
		</span>
</div>
<div class="g2">
	<label class="g5">Category:</label>
		<span class="g7">
    		<?php echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll(), 'id', 'display_name'), $htmlOptions=array('empty'=>'Select One')); ?>
		</span>
</div>
<div class="g2">
	<label class="g3">Name:</label>
	<span class="g9"><?php echo $form->textField($model,'name');?></span>
</div>
<div class="g3">
	<label class="g3">Description</label>
	<span class="g9"><?php echo $form->textField($model,'description');?></span>
</div>
<?php /**
 *
 * @TODO: it would be nice but there is not enough room
<div class="g2">
	<label class="g2">Body:</label>
	<span class="g6"><?php echo $form->textField($model,'body');?></span>
</div>
 */
  ?>
<div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<div class="g1 submit" style="text-align:center"><?php echo CHtml::resetButton('RESET', array('class'=>'button reset-button','style'=>'font-size: 11px; height: 30px;')); ?></div>
<?php $this->endWidget(); ?>
