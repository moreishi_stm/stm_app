<?php
$css = <<<CSS
    td.clear-formatting{
        background-color: white;
        font-family: Arial, Helvetica, sans-serif;
        padding: 30px;
        color: black;
        font-size: 13px;
    }
    tr:hover td.clear-formatting strong {
        color: inherit !important;
    }
    td.clear-formatting p, td.clear-formatting ul {
    font-size: 13px;
    line-height: inherit;
}
    td.clear-formatting ul {
        margin-top: 1em !important;
        margin-bottom: 1em !important;
    }

	p {
		margin-bottom: 18px;
	}
    span {
        font-family: inherit !important;
    }
    .email-view h1, .email-view h2, .email-view h3, .email-view h4, .email-view h5, .email-view h6{
        text-align: left;
    }
    .email-view table {
        background-image: none;
        float: none;
    }
    .email-view td {
        text-align: inherit;
        border-color: transparent;
    }
    .email-view tr:hover td strong {
        color: inherit !important;
    }
    .email-view p {
        margin-bottom: 1em;
        font-size: 100%;
        font-style: inherit;
        margin-top: 1em;
    }
CSS;
Yii::app()->clientScript->registerCss('editLetterTemplate', $css);

$this->breadcrumbs=array(
	$model->description=>'',
);
$this->beginStmPortletContent(array(
	'handleTitle'=>'Letter Template :: '.$model->description,
	'handleIconCss'=>'i_wizard',
	'handleButtons'    => array(
		array(
			'label'        =>'Edit',
			'iconCssClass' =>'i_stm_edit',
			'htmlOptions'  =>array('id'=>'edit-contact-button'),
			'name'         =>'edit-contact',
			'type'         =>'link',
			'link'		   =>'/'.Yii::app()->controller->module->name.'/letterTemplates/edit/'.$model->id,
		)
	),
));
?>
<div id="email-templates-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container p-mt10">
			<tr>
				<th>Status:</th>
				<td><?php echo StmFormHelper::getStatusBooleanName($model->status_ma)?><label class="label" style="padding-left: 100px;">Type:</label><?php echo $model->componentType->display_name?></td>
			</tr>
			<tr>
				<th>Description:</th>
				<td><?php echo $model->description;?></td>
			</tr>
		</table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<table style="width: 900px; float: none; clear: both; margin: 0 auto;">
    <tr>
        <th colspan="2"><h2>Letter Body</h2></th>
    </tr>
    <tr>
        <th>Body:</th>
        <td class="email-view clear-formatting"><?php echo $model->body;?></td>
    </tr>
</table>