<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->name.'/letterTemplates'),
);

?>
	<div id="listview-actions">
		<a href="/<?php echo Yii::app()->controller->module->name;?>/letterTemplates/add" class="button gray icon i_stm_add">Add New Letter Template</a>
		<!-- <a href="#" class="btnGray button i_stm_search icon">Search Contacts</a> -->
	</div>
	<div id="content-header">
		<h1>Letter Template List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox',array(
			'model'=>$model,
		)); ?>
	</div><!-- search-form -->
<?php
//die("<pre>".print_r($model->search(),1));
?>
<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'letter-templates-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'id',
		array(
			'type'  => 'raw',
			'name'  => 'Status',
			'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)',
		),
		'componentType.display_name',
		'name',
        'description',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/letterTemplates/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:80px'),
        ),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/letterTemplates/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:80px'),
		),
	),
));
