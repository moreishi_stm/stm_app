<?php
	$this->breadcrumbs = array(
		ucwords($this->action->id) => '',
	);

	if ($this->action->id == 'edit') {
		$this->breadcrumbs = array(
			$model->componentType->display_name => '/'.Yii::app()->controller->module->name.'/letterTemplates/' . $model->id,
			ucwords($this->action->id) => ''
		);
	}
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
