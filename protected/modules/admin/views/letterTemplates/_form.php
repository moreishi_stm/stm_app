<?php
$form = $this->beginWidget('CActiveForm', array(
		'id' => ' contact-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)
);
$this->beginStmPortletContent(array(
		'handleTitle' => 'Letter Template Details',
		'handleIconCss' => 'i_wizard'
	)
);
?>
	<div id="action-plans-container">
		<div class="g12 p-mb5 rounded-text-box odd-static">
			<table class="container">
				<tr>
					<th class="narrow">Status:</th>
					<td>
						<?php echo $form->dropDownList($model, 'status_ma', array(
							1 => 'Active',
							0 => 'Inactive'
						), $htmlOptions = array('class' => 'g2')
						); ?>
						<label class="label p-fl p-pr4" style="padding-left:100px;">Type:</label>
						<?php echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll(), 'id', 'display_name'), $htmlOptions = array(
							'empty' => 'Select One',
							'class' => 'g2'
						)
						); ?>
						<?php echo $form->error($model, 'status_ma'); ?>
					</td>
				</tr>
				<tr>
					<th class="narrow">Name:</th>
					<td>
						<?php echo $form->textField($model, 'name', $htmlOptions = array(
							'placeholder' => 'Letter Name',
							'class' => 'g7',
						)
						); ?>
						<?php echo $form->error($model, 'name'); ?>
					</td>
				</tr>
				<tr>
					<th class="narrow">Description:</th>
					<td>
						<?php echo $form->textArea($model, 'description', $htmlOptions = array(
							'placeholder' => 'Description',
							'class' => 'g7',
							'rows' => '4',
						)
						); ?>
						<?php echo $form->error($model, 'component_type_id'); ?><?php echo $form->error($model, 'description'); ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<hr/>
						<h3>Edit the Letter Below:</h3>
					</td>

				</tr>
				<tr>
					<th class="narrow">Body:</th>
					<td style="width: 800px;">
						<div style="font-weight: bold;">
							To have the first name of the contact appear in the letter, type in {{first_name}}.<br />
							Example: Hello {{first_name}}, how are you doing?<br /><br />
							To have the appointment date appear in the letter, type in {{appointment_date}}<br />
							Example: You have an appointment scheduled for {{appointment_date}}<br /><br />

							To have the seller address appear in the letter, the following fieles are available: {{seller_address}}, {{seller_city}}, {{seller_state}} and {{seller_zip}}<br /><br />

							WARNING: Do NOT copy from a Microsoft document as it adds invalid hidden tags.<br /><br />
						</div>
						<?php echo $form->error($model, 'body'); ?>
						<?php
						Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);

						$scope = (YII_DEBUG) ? "local" : "com";
						$s3BaseUrl = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";

						$s3Url = urlencode(base64_encode('s3://'.$s3BaseUrl.'/site-files/'.Yii::app()->user->clientId."/emailTemplates/".$model->id));

						$js = <<<JS
	tinymce.PluginManager.load('moxiemanager', 'http://www.seizethemarket.{$scope}/imagemanager/plugin.js?v=4');
	tinymce.init({
		selector: "#LetterTemplates_body",
		convert_urls: false,
		relative_urls: false,
		height : 800,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste moxiemanager, spellchecker"
		],
		image_advtab: true,

        cleanup_on_startup: false,
        trim_span_elements: false,
        verify_html : false,

		extended_valid_elements : "span[*|top|position],a[*],a*,div[*],span*,div[*]",
		cleanup: false,
		toolbar: "fontselect fontsizeselect spellchecker | forecolor forecolorpicker backcolor bold italic underline | cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image insertfile undo redo | preview | code",
		autosave_ask_before_unload: false,
		setup : function(ed) {
		    ed.on('init', function() { this.getDoc().body.style.fontSize = '13px'; });
		    ed.on('keyUp', function() { tinyMCE.triggerSave(); });
		},
		spellchecker_rpc_url: "http://www.seizethemarket.{$scope}/js/tinymce/plugins/spellchecker/spellchecker.php",
		//moxiemanager_apiPageUrl: "http://www.seizethemarket.{$scope}/imagemanager/api.php,
		//moxiemanager_baseUrl: "http://www.seizethemarket.{$scope}/imagemanager"
		init_instance_callback: function () {
			moxman.Env.baseUrl = "http://www.seizethemarket.{$scope}/imagemanager";
			moxman.Env.apiPageName = "api.php?v=1&s3={$s3Url}";
			//moxman.path = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
			//moxman.rootpath = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
		},


	});
JS;

						Yii::app()->clientScript->registerScript('tinyMceNew', $js); ?>
				<tr>
					<th class="narrow">Body:</th>
					<td>
						<?php echo $form->textArea($model, 'body', $htmlOptions = array(
							'placeholder' => 'The body of the letter',
							'class' => 'g7',
							'rows' => '4',
						)
						); ?>
						<?php echo $form->error($model, 'component_type_id'); ?><?php echo $form->error($model, 'body'); ?>
					</td>
				</tr>
				</td>
				</tr>

			</table>

		</div>
	</div>
<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper">
		<button type="submit" class="submit wide">Submit Letter Template</button>
	</div>
<?php $this->endWidget(); ?>