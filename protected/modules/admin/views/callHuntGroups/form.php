<?
$this->breadcrumbs=array(
    $this->action->id == 'edit' ? 'Edit' : 'Add'=>''
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'hunt-group-form',
        'enableAjaxValidation' => false
    )
);
?>
<h1><?=$this->action->id == 'edit' ? 'Edit' : 'Add'?> Hunt Group</h1>
<?
$this->beginStmPortletContent(array(
        'handleTitle' => 'Hunt Group Details',
        'handleIconCss' => 'i_wizard'
)
);
?>
<div id="action-plans-container">
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <table class="container">
            <tr>
                <th class="narrow"><?php echo $form->labelEx($model, 'name'); ?>:</th>
                <td>
                    <?php echo $form->textField($model, 'name', $htmlOptions = array(
                            'placeholder' => 'Name',
                            'class' => 'g7',
                        )
                    ); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $form->labelEx($model, 'description'); ?>:</th>
                <td>
                    <?php echo $form->textField($model, 'description', $htmlOptions = array(
                            'placeholder' => 'Description',
                            'class' => 'g7',
                        )
                    ); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $form->labelEx($model, 'voicemail_greeting_id'); ?>:</th>
                <td>
                    <?php echo $form->dropDownList($model, 'voicemail_greeting_id', CHtml::listData(CallRecordings::model()->findAll(array('order'=>'title ASC')), 'id', 'title'), $htmlOptions=array('empty'=>'','data-placeholder'=>'Select a Greeting'));?>
                    <div class="stm-tool-tip question">
                        <span>Voicemail greeting that plays when call is not answered for this call hunt group. </span>
                    </div>
                    <?php echo $form->error($model, 'voicemail_greeting_id'); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <button type="submit" class="submit wide">Save Hunt Group</button>
</div>
<?php $this->endWidget(); ?>