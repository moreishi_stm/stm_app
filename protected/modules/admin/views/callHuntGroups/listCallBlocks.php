<?
$this->breadcrumbs=array(
    'Call Block List'=>''
);?>
<div id="listview-actions">
    <a href="/<?=Yii::app()->controller->module->id?>/<?=Yii::app()->controller->id?>/addCallBlock" class="button gray icon i_stm_add">Add Call Block</a>
</div>
<div id="content-header">
    <h1>Call Blocks</h1>
</div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'hunt-group-phones-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        'phone:phone',
        'reason',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'($count = count($data->callBlockCalls)) ? "# Calls Blocked: ".$count:
				"<a href=\"/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/editCallBlock/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit Call Block</a>"
			',
            'htmlOptions'=>array('style'=>'width:200px;text-align: center;'),
        ),
    ),
));
?>