<?php
$this->breadcrumbs=array(
    $this->action->id == 'edit' ? 'Edit' : 'Add'=>''
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'call-block-form',
        'enableAjaxValidation' => false
    )
);
?>
    <h1><?=$this->action->id == 'editPhone' ? 'Edit' : 'Add'?> Call Block</h1>
<?
$this->beginStmPortletContent(array(
        'handleTitle' => 'Call Blocks',
        'handleIconCss' => 'i_wizard'
    )
);
?>
    <div id="call-block-container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'status_ma') ?></th>
                    <td>
                        <?php echo $form->dropDownList($model, 'status_ma', StmFormHelper::getStatusBooleanList(true), $htmlOptions = array(
                            )
                        ); ?>
                        <?php echo $form->error($model, 'status_ma');?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'phone') ?></th>
                    <td>
                        <?php $this->widget('StmMaskedTextField', array(
                                'model' => $model,
                                'attribute' => 'phone',
                                'mask' => '(999) 999-9999',
                                'htmlOptions' => array(
                                    'placeholder' => '(999) 123-4567',
                                    'class' => 'g7',
                                ),
                            )
                        ); ?>
                        <?php echo $form->error($model, 'phone'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'reason') ?></th>
                    <td>
                        <?php echo $form->textField($model, 'reason', $htmlOptions = array(
                                'placeholder' => 'Reason',
                                'class' => 'g7',
                            )
                        ); ?>
                        <?php echo $form->error($model, 'reason'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper">
        <button type="submit" class="submit wide">Save Call Block</button>
    </div>
<?php $this->endWidget(); ?>