<?
$this->breadcrumbs=array(
    'List'=>''
);?>
<div id="listview-actions">
    <a href="/<?=Yii::app()->controller->module->id?>/<?=Yii::app()->controller->id?>/addPhone/<?=$data->id?>" class="button gray icon i_stm_add">Add New Hunt Group Phone</a>
</div>
<div id="content-header">
    <h1>Hunt Group Phones</h1>
    <h4>This is a list of phone numbers that belong to this call hunt group. When an incoming call has this Call Hunt Group Assigned, all of these numbers (if they are active) will be called at the same time to take the call. Whoever picks up and presses “1” to accept the call first will be connected to the incoming call. If this call is not answered by anyone, it will be marked as unanswered in the call history. </h4>
</div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'hunt-group-phones-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Status',
            'value'=>'StmFormHelper::getStatusBooleanName($data->status_ma)',
        ),
        'description',
        'phone:phone',
        'updated:dateTime',
        array(
            'name'=>'Updated By',
            'value'=>'$data->updatedBy->fullName',
        ),
        array(
            'type'=>'raw',
            'name'=>'Hunt Group Name',
            'value'=>'$data->callHuntGroup->name',
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/editPhone/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit Hunt Group Phone</a>"
			',
            'htmlOptions'=>array('style'=>'width:200px;text-align: center;'),
        ),
    ),
));
?>