<?php
$this->breadcrumbs=array(
    $this->action->id == 'edit' ? 'Edit' : 'Add'=>''
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => ' hunt-group-phone-form',
        'enableAjaxValidation' => false
    )
);
?>
    <h1><?=$this->action->id == 'editPhone' ? 'Edit' : 'Add'?> Phone</h1>
    <h2>Call Hunt Group</h2>
<?
$this->beginStmPortletContent(array(
        'handleTitle' => 'Call Hunt Group Phone',
        'handleIconCss' => 'i_wizard'
    )
);
?>
    <div id="action-plans-container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'status_ma') ?></th>
                    <td>
                        <?php echo $form->dropDownList($model, 'status_ma', StmFormHelper::getStatusBooleanList(true), $htmlOptions = array(
                            )
                        ); ?>
                        <div class="stm-tool-tip question">
                            <span>If active, this number will be called for incoming calls assigned to this Hunt Group. </span>
                        </div>
                        <?php echo $form->error($model, 'status_ma');?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'phone') ?></th>
                    <td>
                        <?php echo $form->textField($model, 'phone', $htmlOptions = array(
                                'placeholder' => 'Phone',
                                'class' => 'g7',
                                'maxlength' => 10,
                            )
                        ); ?>
                        <div class="stm-tool-tip question">
                            <span>The phone number that will have incoming calls routed to it. Typically an office / cell phone. </span>
                        </div>
                        <?php echo $form->error($model, 'phone'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow">Description:</th>
                    <td>
                        <?php echo $form->textField($model, 'description', $htmlOptions = array(
                                'placeholder' => 'Name / Description',
                                'class' => 'g7',
                            )
                        ); ?>
                        <div class="stm-tool-tip question">
                            <span>Describe the phone number so you know where it goes to. E.g. John Smith - Cell</span>
                        </div>
                        <?php echo $form->error($model, 'description'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'call_hunt_group_id') ?></th>
                    <td>
                        <?php echo $form->dropDownList($model, 'call_hunt_group_id', CallHuntGroups::getHuntGroupList(), $htmlOptions = array(
                                'data-placeholder' => 'Select a Hunt Group',
                                'empty' => '',
                            )
                        ); ?>
                        <div class="stm-tool-tip question">
                            <span>Indicates which call hunt group this phone number is associated with. </span>
                        </div>
                        <?php echo $form->error($model, 'call_hunt_group_id');?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper">
        <button type="submit" class="submit wide">Save Hunt Group</button>
    </div>
<?php $this->endWidget(); ?>