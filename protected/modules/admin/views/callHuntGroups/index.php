<?
$this->breadcrumbs=array(
    'List'=>''
);?>
<div id="listview-actions">
    <a href="/<?=Yii::app()->controller->module->id?>/<?=Yii::app()->controller->id?>/add" class="button gray icon i_stm_add">Add New Hunt Group</a>
    <a href="/<?=Yii::app()->controller->module->id?>/<?=Yii::app()->controller->id?>/addCallBlock" class="button gray icon i_stm_add">Block a Number</a>
    <a href="/<?=Yii::app()->controller->module->id?>/<?=Yii::app()->controller->id?>/listCallBlock" class="button gray icon i_stm_search">View Blocked Numbers</a>
</div>
<div id="content-header">
    <h1>Call Hunt Groups</h1>
    <h4>A targetted phone number will be assigned a call hunt group containing number or your staff to ring if there is an incoming call. </h4>
</div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'hunt-group-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        'name',
        'description',
        array(
            'type'=>'raw',
            'name'=>'Phone #s',
            'value'=>'Yii::app()->controller->printHuntGroupPhones($data)',
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/listPhone/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View Phones</a>"
			',
            'htmlOptions'=>array('style'=>'width:120px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/addPhone/".$data->id."\" class=\"button gray icon i_stm_add grey-button\">Add Phone</a>"
			',
            'htmlOptions'=>array('style'=>'width:110px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a>"
			',
            'htmlOptions'=>array('style'=>'width:80px;'),
        ),
        /*array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/".Yii::app()->controller->module->id."/voicemail?id=" . $data->id . "&type=hunt-group" . "\" class=\"button gray icon i_stm_search grey-button\">Voicemails</a>"
			',
            'htmlOptions'=>array('style'=>'width:120px;'),
        ),*/
    ),
));
?>