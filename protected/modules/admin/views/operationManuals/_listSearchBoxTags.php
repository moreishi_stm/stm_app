<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'operation-manual-tags-list-search',
)); ?>
	<div class="g3"></div>
	<div class="g5">
		<label class="g3">Name:</label>
		<span class="g9"><?php echo $form->textField($model,'name');?></span>
	</div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
