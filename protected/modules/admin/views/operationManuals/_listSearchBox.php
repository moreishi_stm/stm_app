<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'operation-manuals-list-search',
)); ?>
    <div class="g2">
        <label class="g6">Status:</label>
            <span class="g6">
                <?php echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive'), $htmlOptions=array('empty'=>'Select One')); ?>
            </span>
    </div>
    <div class="g5">
        <label class="g3">Name/Content:</label>
        <span class="g9"><?php echo $form->textField($model,'name');?></span>
    </div>
	<div class="g3">
		<label class="g2">Tags:</label>
		<span class="g10"><?php echo $form->dropDownList($model->operationManualTagLu, 'idCollection',
												CHtml::listData(OperationManualTags::model()->findAll(), 'id', 'name'), array(
//													'empty' => 'All',
													'class' => 'chzn-select g12',
													'multiple' => 'multiple',
													'data-placeholder' => 'Tags'
												)); ?>
		</span>
		<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#OperationManualTagLu_idCollection')); ?>
	</div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
