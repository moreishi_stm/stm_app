<?php
$this->breadcrumbs = array(
    'List' => '',
);
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('search', <<<JS

    $("#listview-search form").submit(function() {
        $.fn.yiiGridView.update("operation-manuals-grid", {
            data: $(this).serialize()
        });
        return false;
    });

    $(".delete-operations-manual-button").live('click',function() {
        if(confirm('Are you sure you want to delete this Operations Manual?')) {

			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var id = $(this).data('id');
			$.post('/$module/operationManuals/delete/'+id, function(data) {

				$("div.loading-container.loading").remove();

				if(data.status=='success') {
					Message.create("success","Operations Manual deleted successfully.");
                    $.fn.yiiGridView.update("operation-manuals-grid", {
                        data: $('#listview-search form').serialize()
                    });
                } else {
					Message.create("error","Error: Operations Manual did not delete. " + data.errorMessage);
				}
			},"json");
        }
        return false;
    });
JS
);
?>
<div id="listview-actions">
	<a href="/<?php echo $this->module->name;?>/operationManuals/add" class="button gray icon i_stm_add">Add Operation Manual</a>
	<a href="/<?php echo $this->module->name;?>/operationManuals/tags" class="button gray icon i_stm_document_add">Tags</a>
	<a href="/<?php echo $this->module->name;?>/operationManuals/permissions" class="button gray icon i_stm_key">Permissions</a>
</div>
<div id="content-header">
	<h1>Operation Manuals List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBox', array(
			'model' => $model,
		)
	); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'operation-manuals-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Tags',
				'value' => 'Yii::app()->format->formatCommaDelimited($data->operationManualTags,"name")',
				'htmlOptions' => array('style' => 'width:120px'),
			),
			'name',
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->name."/operationManuals/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
            ',
				'htmlOptions' => array('style' => 'width:70px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->name."/operationManuals/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
            ',
				'htmlOptions' => array('style' => 'width:70px'),
			),
            array(
                'type' => 'raw',
                'name' => '',
                'visible' => Yii::app()->user->checkAccess('owner'),
                'value' => '
                "<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button delete-operations-manual-button\" data-id=\"".$data->id."\">Delete</a></div>"
            ',
                'htmlOptions' => array('style' => 'width:70px'),
            ),
		),
	)
);
?>
