<?php
	$this->breadcrumbs = array(
		$model->name => '',
	);

$id = $model->id;
$module = Yii::app()->controller->module->id;
$js = <<<JS
	$('#print-manual-button').live('click', function(){
			window.open('/$module/operationManuals/print/$id', 'Operation Manual', 'width=800,height=700,scrollbars=yes');
	});
JS;
Yii::app()->clientScript->registerScript('printOperationManualJs', $js);

Yii::app()->clientScript->registerCss('operationManualView', <<<CSS
    .operation-manual-container table {
        float: none;
        background-image: none;
    }
    .operation-manual-container table td, .operation-manual-container table th {
        padding: 0;
        border-width: 0;
    }
    .widget * p, .widget * li {
        font-size: 14px;
        font-family: arial, helvetica, sans-serif;
    }
    .widget * h1, .widget * h1 * span, .widget h1 span,
    .widget * h2, .widget * h2 * span, .widget h2 span,
    .widget * h3, .widget * h3 * span, .widget h3 span{
        text-align: left !important;
        font-weight: bold !important;
        color: #555 !important;
    }
    .widget * h1, .widget * h1 * span, .widget h1 span {
        font-size: 28px !important;
    }
    .widget * h2, .widget * h2 * span, .widget h2 span {
        font-size: 22px !important;
    }
    .widget * h3, .widget * h3 * span, .widget h3 span {
        font-size: 16px !important;
    }
    .widget ul {
        margin: 15px 0 10px 25px;
    }
    .widget ul li{
        margin-top: 2px;
    }
    .widget > div {
        background-image: none;
    }
    .operation-manual-container {
        padding: 40px !important;
        background: #FBFBFB;
    }
CSS
);

$this->beginStmPortletContent(array(
	'handleTitle'   => $model->name,
	'handleIconCss' => 'i_wizard',
	'handleButtons' => array(
		array(
			'label' => 'Edit',
			'iconCssClass' => 'i_stm_edit',
			'htmlOptions' => array('id' => 'edit-contact-button'),
			'name' => 'edit-operation-manual',
			'type' => 'link',
			'link' => '/'.$this->module->name.'/operationManuals/edit/'.$model->id
		),
        array(
            'label' => 'Print',
            'iconCssClass' => 'i_stm_print',
            'htmlOptions' => array('id' => 'print-manual-button'),
            'name' => 'edit-operation-manual',
            'type' => 'link',
            'link' => 'javascript:void(0)'
        )
    )
	));
?>
<div class="p-p10 operation-manual-container">
    <h4><a href="/<?php echo $this->module->name; ?>/operationManuals/<?php echo $model->id; ?>" class="button gray icon i_stm_search">Refresh</a></h4>
	<strong>Tags:</strong> <?php echo Yii::app()->format->formatCommaDelimited($model->operationManualTags,'name')?>
	<hr />
	<?php echo $model->content; ?>
    <h4><a href="/<?php echo $this->module->name; ?>/operationManuals/<?php echo $model->id; ?>" class="button gray icon i_stm_search">Refresh</a></h4>
</div>
<?php $this->endStmPortletContent(); ?>
