<?php
    $isEdit = ($this->action->id == 'edit')? true : false;
    $module = Yii::app()->controller->module->name;
	$this->breadcrumbs = array(
		($isEdit)? 'Edit': 'Add' => '',
	);

    if($isEdit) {
        $this->breadcrumbs = CMap::mergeArray($this->breadcrumbs, array($model->name=>'/'.$module.'/operationManuals/'.$model->id));
    }

$js = <<<JS
    $('.submit').click(function(){
        var submitType;
        if($(this).hasClass('save-continue')) {
            submitType = 'continue';
        } else {
            submitType = 'close';
        }
        $('#operation-manual-form').attr('data-aftersave', submitType);
    });
JS;
Yii::app()->clientScript->registerScript('operationsManualForm', $js);
?>

<?php
$flashMessageMode = ($this->action->id == 'edit')? 'Updated': 'Added';
$form = $this->beginWidget('CActiveForm', array(
											  'id' =>'operation-manual-form',
                                              'action' => array('/'.$module.'/operationManuals/'.Yii::app()->controller->action->id.'/'.$model->id),
                                              'enableAjaxValidation' => true,
                                              'enableClientValidation' => false,
                                              'clientOptions' => array(
                                                  'validateOnChange' => false,
                                                  'validateOnSubmit' => $isEdit,
                                                  'beforeValidate' => 'js:function(form) {
                                                            tinymce.triggerSave();
                                                            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                                            return true;
                                                        }',
                                                  'afterValidate' => 'js:function(form, data, hasError) {
                                                      if (!hasError && form.attr("data-aftersave")=="close") {
                                                          window.location = "/'.Yii::app()->controller->module->name.'/operationManuals/'.$model->id.'";
                                                          return false;
                                                      } else if(!hasError) {
                                                          $("div.loading-container.loading").remove();
                                                          Message.create("success", "Successfully '.$flashMessageMode.' Operations Manual!");
                                                          return false;
                                                      }
                                                      $("div.loading-container.loading").remove();
                                                  }',
                                              ),
                                        ));
	$this->beginStmPortletContent(array(
								  'handleTitle'   => 'Operation Manual',
								  'handleIconCss' => 'i_wizard'
								  ));
?>
	<div id="operation-manual-tag-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
			<table class="container">
				<tr>
					<th class="narrow"><?php echo $form->labelEx($model, 'status_ma'); ?>:</th>
					<td>
                        <?php echo $form->dropDownList($model, 'status_ma', StmFormHelper::getStatusBooleanList(), $htmlOptions=array('class'=>'g4')); ?>
						<?php echo $form->error($model,'status_ma'); ?>
					</td>
				</tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'name'); ?>:</th>
                    <td>
                        <?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Manual Name', 'class'=>'g11', ));?>
                        <?php echo $form->error($model,'name'); ?>
                    </td>
                </tr>
				<tr>
					<th class="narrow"><?php echo $form->labelEx($model, 'tagCollection'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'tagCollection', CHtml::listData(OperationManualTags::model()->findAll(), 'id', 'name'), $htmlOptions=array('class'=>'g11','multiple'=>'multiple','data-placeholder'=>'Select Tags'));
						$this->widget('admin_module.extensions.EChosen.EChosen', array(
																				 'target' => 'select#OperationManuals_tagCollection',
																				 'options'=>array(
																				 ),
																				 ));
						?>
						<?php echo $form->error($model,'tagCollection'); ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
                        <div id="submit-button-wrapper">
                            <?php if($isEdit): ?>
                                <?php echo CHtml::htmlButton('Save & Continue', $htmlOptions = array('class' => 'submit wide save-continue', 'type' => 'submit')); ?>
                                <?php echo CHtml::htmlButton('Save & Close', $htmlOptions = array('class' => 'submit wide', 'type' => 'submit')); ?>
                                <h4><a href="/<?php echo $this->module->name; ?>/operationManuals/edit/<?php echo $model->id; ?>">Refresh</a></h4>
                            <?php else: ?>
                                <?php echo CHtml::htmlButton('Save Operations Manual', $htmlOptions = array('class' => 'submit wide', 'type' => 'submit')); ?>
                            <?php endif; ?>
                        </div>
                        <?php echo $form->error($model,'content'); ?>
						<?php
						/*$this->widget('admin_module.extensions.tinymce.ETinyMce', array(
																				  'name' => 'OperationManuals[content]',
																				  'value' => $model->content,
																				  'useSwitch' => false,
																				  'options' => array(
																					  'theme' => 'advanced',
																					  'theme_advanced_toolbar_location' => 'top',
                                                                                      'theme_advanced_buttons1' => 'bold,italic,underline,strikethrough,separator,forecolor,backcolor,separator,justifyleft,justifycenter,justifyright,justifyfull,separator',
                                                                                      'theme_advanced_buttons2' => 'formatselect,fontselect,fontsizeselect,separator,bullist,numlist,separator,outdent,indent,blockquote,separator,undo,redo,link,unlink,separator,link,unlink,anchor,image,cleanup,help,code',
																					  'theme_advanced_buttons3' => '',
																					  'width' => '97%',
																					  'height' => '650',
													  //								'apply_source_formatting'         => false,
																				  ),
																				  )
						);*/
						Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);




$scope = (YII_DEBUG) ? "local" : "com";

$s3BaseUrl = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";

$s3Url = urlencode(base64_encode('s3://'.$s3BaseUrl.'/site-files/'.Yii::app()->user->clientId."/operationManuals/".$model->id));


$js = <<<JS
	tinymce.PluginManager.load('moxiemanager', 'http://www.seizethemarket.{$scope}/imagemanager/plugin.js?v=4');
	tinymce.init({
		selector: "#OperationManuals_content",
		convert_urls: false,
		relative_urls: false,
		height : 800,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste moxiemanager"
		],
		toolbar: "insertfile undo redo | styleselect | fontselect fontsizeselect forecolor backcolor bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		autosave_ask_before_unload: false,
		setup : function(ed) {
		    ed.on('init', function() { this.getDoc().body.style.fontSize = '13px'; });
		    ed.on('keyUp', function() { tinyMCE.triggerSave(); });
		},
		//moxiemanager_apiPageUrl: "http://www.seizethemarket.{$scope}/imagemanager/api.php,
		//moxiemanager_baseUrl: "http://www.seizethemarket.{$scope}/imagemanager"
		init_instance_callback: function () {
			moxman.Env.baseUrl = "http://www.seizethemarket.{$scope}/imagemanager";
			moxman.Env.apiPageName = "api.php?v=1&s3={$s3Url}";
			//moxman.path = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
			//moxman.rootpath = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
		},
		//moxiemanager_rootpath: 's3://{$s3BaseUrl}/site-files/{$s3Url}',
		//moxiemanager_path: '/site-files/{$s3Url}',
	});
JS;

		//http://www.seizethemarket.local/imagemanager/api.php?
		//s3=czM6Ly9kZXYuc2l0ZXMuc2VpemV0aGVtYXJrZXQuY29tL3NpdGUtZmlsZXMvMS9jbXNQYWdlcy8%3D
		//&action=streamfile&path=%2FcmsPages
		//&name=one-story-craftsman-style-home-plans-l-d28baafe08f326b5.jpg
		//&loaded=0
		//&total=105057
		//&id=null
		//&csrf=41d9afb847906a6d5bafccc453a2c6a461204ab064876d1e1a0c198a554db7d2
		//&resolution=default

				Yii::app()->clientScript->registerScript('tinyMceNew', $js); ?>
                <?php echo $form->textArea($model,'content', $htmlOptions=array('placeholder'=>'Manual Name', 'class'=>'g12'));?>
<!--				<textarea id="OperationManuals_content" name="OperationManuals[content]">-->
<!--					--><?//=$model->content;?>
<!--				</textarea>-->
					</td>
				</tr>
			</table>
		</div>
	</div>
<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper">
        <?php if($isEdit): ?>
            <?php echo CHtml::htmlButton('Save & Continue', $htmlOptions = array('class' => 'submit wide save-continue', 'type' => 'submit')); ?>
            <?php echo CHtml::htmlButton('Save & Close', $htmlOptions = array('class' => 'submit wide', 'type' => 'submit')); ?>
        <?php else: ?>
            <?php echo CHtml::htmlButton('Save Operations Manual', $htmlOptions = array('class' => 'submit wide', 'type' => 'submit')); ?>
        <?php endif; ?>
    </div>
<?php $this->endWidget(); ?>