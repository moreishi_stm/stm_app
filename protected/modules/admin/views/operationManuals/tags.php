<?php
$this->breadcrumbs = array(
	'Tags' => '',
);

Yii::app()->clientScript->registerScript('search', '
$("#listview-search form").submit(function() {
	$.fn.yiiGridView.update("operation-manual-tags-grid", {
		data: $(this).serialize()
	});
	return false;
});
');
?>
<div id="listview-actions">
	<a href="/<?php echo $this->module->name; ?>/operationManuals/addTag" class="button gray icon i_stm_add">Add New Tag</a>
</div>
<div id="content-header">
	<h1>Operation Manual Tags</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBoxTags', array(
			'model' => $model,
		)
	); ?>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'operation-manual-tags-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			'name',
			array(
				'type' => 'raw',
				'name' => 'Subcategory of',
				'value' => '$data->parent->name',
			),
			array(
				'type' => 'raw',
				'name' => 'Permission',
				'value' => 'Yii::app()->format->formatCommaDelimited($data->operationManualTagPermissions,"label")',
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->name."/operationManuals/editTag/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit Tags</a></div>"
            ',
				'htmlOptions' => array('style' => 'width:120px'),
			),
		),
	)
);
?>
