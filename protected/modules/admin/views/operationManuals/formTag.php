<?php
$this->breadcrumbs = array(
	'Tags' => '/'.$this->module->name.'/operationManuals/tags',
	($this->action->id == 'editTag')? 'Edit': 'Add' => '',
);
?>

<h1 class="name"><?php echo ($model->isNewRecord) ? 'Add New Tag' : $model->name;?></h1>

<?php
	$form = $this->beginWidget('CActiveForm', array(
											  'id' =>'add-operation-manual-tag-form',
											  'enableAjaxValidation' => false,
											  ));
	$this->beginStmPortletContent(array(
								  'handleTitle'   => 'Operation Manual Tag',
								  'handleIconCss' => 'i_wizard'
								  ));
?>
	<div id="operation-manual-tag-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
			<table class="container">
				<tr>
					<th><?php echo $form->labelEx($model, 'name'); ?>:</th>
					<td>
						<?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Tag Name', 'class'=>'g6', ));?>
						<?php echo $form->error($model,'name'); ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'parent_id'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'parent_id', CMap::mergeArray(array(''=>''),CHtml::listData(OperationManualTags::model()->exclude($model->id)->findAll(), 'id', 'name')), $htmlOptions=array('class'=>'g6'));
						$this->widget('admin_module.extensions.EChosen.EChosen', array(
																				 'target' => 'select#OperationManualTags_parent_id',
																				 'options'=>array(
																					 'allow_single_deselect'=>true,
																				 ),
																				 ));
						?>
						<?php echo $form->error($model,'name'); ?>
					</td>
				</tr>
				<tr>
					<th class="narrow"><?php echo $form->labelEx($model, 'tagPermissionCollection'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'tagPermissionCollection', CHtml::listData(AuthItem::model()->byTypes(CAuthItem::TYPE_ROLE)->findAll(), 'name', 'label'), $htmlOptions=array('class'=>'g6','multiple'=>'multiple','data-placeholder'=>'Select Roles'));
						$this->widget('admin_module.extensions.EChosen.EChosen', array(
																				 'target' => 'select#OperationManualTags_tagPermissionCollection',
																				 'options'=>array(
																				 ),
																				 ));
						?>
						<?php echo $form->error($model,'name'); ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper">
		<?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Submit' : 'Save') . ' Tag', $htmlOptions = array(
				'class' => 'submit wide',
				'type' => 'submit',
			)); ?>
	</div>
<?php $this->endWidget(); ?>