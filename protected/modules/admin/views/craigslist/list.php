<style type="text/css">
    #craigslist-grid tbody td {
        font-size: 24px;
    }
</style>
<?php
	$this->breadcrumbs = array(
		'List' => '',
	);

	Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('craigslist-grid', {
		data: $(this).serialize()
	});
	return false;
});
"
	);
?>
		<h1>Craigslist Poster</h1>
<h3><?php echo ($model->listing_agent_id)? '' : 'Agent MLS ID is missing in the settings';?></h3>
	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<? $form = $this->beginWidget('CActiveForm', array(
				'id' => 'craigslist-list-search',
			)
		);
		?>
<!--		<div class="g12">-->
<!--			<div class="g10">-->
<!--				<label class="g4">Search:</label>-->
<!--				<span class="g6">--><?php //echo $form->textField($model, 'listing_id'); ?><!--</span>-->
<!--			</div>-->
<!--			<div class="g2 submit" style="text-align:center">-->
<!--				--><?php //echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?>
<!--			</div>-->
<!--		</div>-->
		<? $this->endWidget(); ?>
	</div><!-- search-form -->
<?php
if($model->listing_agent_id)
	$this->widget('admin_module.components.StmGridView', array(
			'id' => 'craigslist-grid',
			'dataProvider' => $model->search(),
			'itemsCssClass' => 'datatables',
			'columns' => array(
				array(
					'type' => 'raw',
					'name' => 'Photo',
					'htmlOptions' => array('width' => '240px'),
					'value' => 'CHtml::image($data->getPhoto(1)->getUrl(), "", array("width"=>"480", "height"=>"300"))',
				),
				array(
					'type' => 'raw',
					'name' => 'Property',
					'value' => 'Yii::app()->format->formatDollars($data->price)."<br />".$data->formatMlsAddress($data, array("lineBreak"=>true))', //."<br />(".Yii::app()->format->formatProperCase($data->common_subdivision.")")
				),
				array(
					'type' => 'raw',
					'name' => 'Photo Count',
					'htmlOptions' => array('width' => '150px'),
					'value' => '$data->photo_count." Photos"',
				),
//				array(
//					'type' => 'raw',
//					'name' => 'Post Activity',
//					'htmlOptions' => array('width' => '220px'),
//					'value' => '"# Posts: <br />Last Posted: "',
//				),
				array(
					'type' => 'raw',
					'name' => '',
					'htmlOptions' => array(
						'style' => 'text-align: center; width: 180px;',
					),
                    'value' => 'Yii::app()->controller->printCraigslistButton($data)',
//					'value' => 'CHtml::link("Post to Craigslist", Yii::app()->controller->createUrl("/admin/craigslist/post", array("propertyTypeName" => strtolower($data->propertyType->name), "listingId" => $data->listing_id)),
//                $htmlOptions=array(
//                    "class" => "button gray icon i_stm_add grey-button",
//                    "target" => "_blank",
//                )
//            )',
				),
			),
		)
	);
