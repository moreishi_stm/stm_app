<table width="760">
	<tr>
		<td colspan="2" style="background-color: yellow; font-family: Arial; font-weight: bold; font-size:20px; padding:6px;">Keller Williams Jacksonville Realty <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?></td>
	</tr>
	<tr>
		<td style="font-family: Arial; font-size:14px;" width="410">
			<br/>
			<table width="300">
				<tr>
					<td colspan="3" style="text-align: right; font-weight: bold;font-size: 20px;">Offered at $<?php echo number_format($Property->price); ?></td>
				</tr>
			</table>
			<hr/>
			<table width="300">
				<tr>
					<td width="110"></td>
					<td>
						Sq Footage:<br/>
						Bedrooms:<br/>
						Baths:<br/>
						Year Built:<br/>
					</td>
					<td>
						<?php echo number_format($Property->sq_feet); ?><br/>
						<?php echo $Property->bedrooms; ?><br/>
						<?php echo $Property->baths_total; ?><br/>
						<?php echo $Property->year_built; ?><br/>
					</td>
				</tr>
			</table>
			<br/>
			<hr/>
			<span style="font-weight: bold; text-decoration: underline;">Description:</span>
			<?php echo $Property->public_remarks; ?>
		</td>
		<td>
			<a href="<?php echo $Property->getPropertyPageUrl(); ?>?utm_source=Craigslist&utm_content=1"><img src="<?php echo $Property->getPhoto(1)->getUrl(); ?>" width="340"
			                                                                                                  style="padding-left: 10px;"></a>
		</td>
	</tr>
</table>
<br/>
<table width="760">
	<?php
		if ($photoCount = $Property->photo_count) {
			for ($i = 1; $i <= $photoCount; $i++) {
				$isPhotoCountOdd = ($i % 2 == 0) ? false : true;
				echo ($isPhotoCountOdd) ? '<tr>' : '';

				echo '<td><a href="' . $Property->getPropertyPageUrl() . '?utm_source=Craigslist&utm_content=1"><img src="' . $Property->getPhoto($i)->getUrl() . '" width="360"></a></td>';

				echo ($isPhotoCountOdd) ? '' : '</tr>';
			}
		}
	?>
</table>
<table width="760">
	<tr>
		<td colspan="2" style="background-color: yellow; font-family: Arial; font-weight: bold; font-size:20px; padding:6px;"><?php echo Yii::app()->getUser()->getSettings()->office_name; ?> <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?></td>
	</tr>
</table>