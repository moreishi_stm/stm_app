<table width="760">
	<tr>
		<td colspan="2" style="background-color: yellow; font-family: Arial; font-weight: bold; font-size:20px; padding:6px;">Keller Williams Jacksonville Realty <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?></td>
	</tr>
	<tr>
		<td style="font-family: Arial; font-size:14px;" width="410">
			<br/>
			<table width="300">
				<tr>
					<td width="60"></td>
					<td colspan="2" style="text-align: left; font-weight: bold;font-size: 20px;">Offered at $<?php echo number_format($Property->price); ?></td>
				</tr>
			</table>
			<hr/>
			<table width="300">
				<tr>
					<td width="60"></td>
					<td>
						Sq Footage:<br/>
						Bedrooms:<br/>
						Baths:<br/>
						Year Built:<br/>
					</td>
					<td>
						<?php echo number_format($Property->sq_feet); ?><br/>
						<?php echo $Property->bedrooms; ?><br/>
						<?php echo $Property->baths_total; ?><br/>
						<?php echo $Property->year_built; ?><br/>
					</td>
				</tr>
			</table>
			<br/>
			<hr/>
			<span style="font-weight: bold; text-decoration: underline;">Description:</span>
			<?php echo $Property->public_remarks; ?>
		</td>
		<td>
			<a href="<?php echo $Property->getUrl(); ?>?utm_source=Craigslist&utm_content=2"><img src="<?php echo $Property->getPhoto(1)->getUrl(); ?>" width="400"
			                                                                                                  style="padding-left: 10px;"></a>
		</td>
	</tr>
</table>
<br/>
<table width="760">
	<?php
		if ($photoCount = $Property->photo_count) {
			$photoPerRow = 7;
			for ($i = 1; $i <= $photoCount; $i++) {
				$isNewRow = (($i - 1) % $photoPerRow == 0) ? true : false;
				echo ($isNewRow || $i == 1) ? '<tr>' : '';

				echo '<td><a href="' . $Property->getUrl() . '?utm_source=Craigslist&utm_content=2"><img src="' . $Property->getPhoto($i)->getUrl() . '" width="100"></a></td>';

				echo ($i % $photoPerRow == $photoPerRow && $i != 1) ? '</tr>' : '';
			}
		}
	?>
</table>
<table width="760">
	<tr>
		<td colspan="2" style="background-color: yellow; font-family: Arial; font-weight: bold; font-size:20px; padding:6px;"><?php echo Yii::app()->getUser()->getSettings()->office_name; ?> <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?></td>
	</tr>
</table>