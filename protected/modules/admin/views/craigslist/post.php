<?php
$action = $_POST['cgAction'];
$crypt = $_POST['cgCrypt'];

$craigslistJs = <<<JS
    /**
      * A object for manipulating form data from a cURL initialized craigslist posting page
      */
    var craigslistPostMapper = {

        /**
          * Set to true to insert arbitrary values into the craigslist content
          */
        useTestData : false,

        /**
          * Maps a internal name to either a input defined by the span tag text
          * or a jQuery selector
          */
        craigslistInputMap : {
            'title' : 'posting title',
            'location' : 'specific location',
            'description' : $('#PostingBody'),
            'price' : 'price',
            'bedrooms' : $('#Bedrooms'),
            'bathrooms' : $('#bathrooms'),
            'housing_type' : $('#housing_type'),
            'laundry' : 'laundry',
            'parking' : 'parking',
            'sqft' : 'sqft',
            'furnished' : 'furnished',
            'show_on_maps' : 'show on maps',
            'street' : 'street',
            'cross_street' : 'cross street',
            'city' : 'city',
            'postal' : 'postal',
            'region' : $('#region'),
//            'fromEmail' : $('#FromEMail'),
//            'confirmEmail' : $('#ConfirmEMail')
        },

        /**
          * Gathers the element from the craigslist generated content and processed the value supplied
          */
        insertValueToElement : function(value, element) {
            craigslistElement = craigslistPostMapper.craigslistInputMap[element];
            if (!(craigslistElement instanceof jQuery)) {
                craigslistElement = craigslistPostMapper.findInputFromLabelText(craigslistElement);
            }

            craigslistPostMapper.processValueForForm(value, craigslistElement);
        },

        /**
          * Determines how to utilize the supplied value based on the element type
          */
        processValueForForm : function(value, element) {
            if (element.is('select')) {
                element.val(value).attr('selected', true);
            } else if (element.is(':checkbox')) {
                if (value) {
					element.click();
                }
            } else if (element.is('input, textarea')) {
                element.val(value);
            }
        },

        /**
          * Returns the input based on either label text or a span masquerading as a label
          */
        findInputFromLabelText : function(craigslistElementText) {
            var inputFromCraigslist = null;
                inputFromWithinLabel = $('form').find('label:contains("'+ craigslistElementText +'")').find('input');
                if (inputFromWithinLabel.length) {
                    inputFromCraigslist = inputFromWithinLabel;
                } else {
                    inputFromCraigslist = $('form').find('span:contains("'+ craigslistElementText    +'")').nextAll('input, textarea, select');
                }

            return inputFromCraigslist;
        }
    }

    // Useful for testing purposes, inserts test data into the craigslist content
    if (craigslistPostMapper.useTestData) {
        for (elementKey in craigslistPostMapper.craigslistInputMap) {
            craigslistPostMapper.insertValueToElement('1', elementKey);
        }
    }

    var propertyData = $propertyJsonData;
    if (propertyData) {
        for (elementKey in propertyData) {
            var value = propertyData[elementKey];
            craigslistPostMapper.insertValueToElement(value, elementKey);
        }
    }
JS;
Yii::app()->clientScript->registerScript('craigslistJs', $craigslistJs);

/** Render the form from Craigslist */
echo $craigslistPostContent;
