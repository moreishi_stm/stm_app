<?php
//Yii::app()->clientScript->registerCss('internalDoNotCallReportCss',<<<CSS
//CSS
//);

Yii::app()->clientScript->registerScript('internalDoNotCallScript', <<<JS

//		$(".expandableDiv, .moreLink").live("click",function() {
//			var id = $(this).attr("data");
//			if($("#expandableDiv"+id).hasClass("min")) {
//				$("#expandableDiv"+id).removeClass("min");
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("More","Less"));
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("+","-"));
//			} else {
//				$("#expandableDiv"+id).addClass("min");
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("Less","More"));
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("-","+"));
//			}
//
//		});
//
    $("select").change(function(){
        $("#internal-do-not-call-form").submit();
        return false;
    });

    $('form').submit(function(){

        $.fn.yiiGridView.update("internal-do-not-call-grid", {
            data: $("form").serialize()
        });
    });
JS
);

	$form=$this->beginWidget('CActiveForm', array(
											'id'=>'internal-do-not-call-form',
											'action'=>'',
											'method'=>'get',
											));

?>
<div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'internal-do-not-call-grid',
																				   'isForm'=>false,
                                                                                   'returnSubmit' => false,
																				   'formSelector'=>'#internal-do-not-call-form',
                                                                                   'defaultSelect' => 'last_3_months',
 )); ?>
</div>
<?php $this->breadcrumbs = array(
	'Internal Do Not Call' => '',
);
//?>
<h1>Internal Do Not Call Report</h1>
<h3 id="date-range-label">
	<label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> -
	<label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label>
</h3>

<div class="g3">

    <div class="g3" style="text-align: right; margin-bottom: 25px;">
        <span style="font-weight: bold;">Name:</span>
    </div>
    <div class="g9">
        <?php echo CHtml::activeDropDownList($model, 'added_by', CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('class'=>'chzn-select','style'=>'width:100%;','empty'=>'')); ?>
    </div>
    <?php
        $this->widget('admin_module.extensions.EChosen.EChosen', array(
            'target' => 'select#CallListDoNotCall_added_by',
            'options' => array('allow_single_deselect' => true)
        ));
    ?>
</div>
<!--<div class="g3">-->
<!--    <div class="g5" style="text-align: right; margin-bottom: 25px;">-->
<!--        <span style="font-weight: bold;">Type:</span>-->
<!--    </div>-->
<!--    <div class="g7">-->
<!--        --><?php //echo CHtml::activeDropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->byIds(array(ComponentTypes::SELLERS, ComponentTypes::BUYERS))->findAll(), 'id', 'display_name'),array('class'=>'chzn-select','style'=>'width:100%;','empty'=>'','data-placeholder'=>'Seller/Buyer')); ?>
<!--    </div>-->
<!--</div>-->
<!---->
<!--<div class="g3">-->
<!--    <div class="g4" style="text-align: right; margin-bottom: 25px;">-->
<!--        <span style="font-weight: bold;">Sort & Filter:</span>-->
<!--    </div>-->
<!--    <div class="g8">-->
<!--        --><?php //echo CHtml::activeDropDownList($model, 'sortOrder', array('targetDateAsc'=>'Target Date (Most Recent)', 'addedDateAsc'=>'Added Date (Lowest)', 'addedDateDesc'=>'Added Date (Highest)'),array('class'=>'chzn-select','style'=>'width:100%;')); ?>
<!--    </div>-->
<!--</div>-->
<!---->
<!--<div class="g3">-->
<!--    <div class="g8" style="text-align: right; margin-bottom: 25px;">-->
<!--        <span style="font-weight: bold;">Has Appointments:</span>-->
<!--    </div>-->
<!--    <div class="g4">-->
<!--        --><?php //echo CHtml::activeDropDownList($model, 'hasAppointment', StmFormHelper::getYesNoList(),array('class'=>'chzn-select','style'=>'width:100%;','empty'=>'','data-placeholder'=>' ')); ?>
<!--    </div>-->
<!--</div>-->
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
		   'id'            =>'internal-do-not-call-grid',
		   'template'	   =>'{pager}{summary}{items}{pager}',
		   'dataProvider'  =>$model->search(),
		   'extraParams'   =>array('dateRange'=>$dateRange),
		   'itemsCssClass' =>'datatables',
		   'columns' => array(
               array(
                   'type'  => 'raw',
                   'name'  => 'Phone #',
                   'value' => 'Yii::app()->format->formatPhone($data->phone)',
                   'htmlOptions' => array('style'=>'width:100px;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Contact',
                   'value' => 'Yii::app()->controller->action->printContactInfo($data)',
                   'htmlOptions' => array('style'=>'width:150px;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Leads',
                   'value' => 'Yii::app()->controller->action->printLeads($data)',
                   'htmlOptions' => array('style'=>'width:200px;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Expire Date',
                   'value' => 'Yii::app()->format->formatDate($data->expire_date, "n/j/y")',
                   'htmlOptions' => array('style'=>'width:100px;text-align:left;'),
               ),
               array(
                    'type'  => 'raw',
                    'name'  => 'Added',
                    'value' => 'Yii::app()->format->formatDate($data->added, "n/j/y")',
                    'htmlOptions' => array('style'=>'width:100px;text-align:left;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Added By',
                   'value' => '$data->addedBy->fullName',
                   'htmlOptions' => array('style'=>'width:100px;text-align:left;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Updated Date',
                   'value' => '($data->updated != $data->added) ? Yii::app()->format->formatDate($data->updated, "n/j/y") : ""',
                   'htmlOptions' => array('style'=>'width:100px;text-align:left;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Updated By',
                   'value' => '($data->updated != $data->added) ? $data->updatedBy->fullName : ""',
                   'htmlOptions' => array('style'=>'width:100px;text-align:left;'),
               ),
		   ),
		   ));

$this->endWidget();