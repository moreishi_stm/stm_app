<?php Yii::app()->clientScript->registerScript('tracking-tasks-assignment-script', <<<JS
	    $('#appointment-form').submit(function(){
            $('#appointments-form').attr('action','/admin/tracking/appointments');
            updateGrid();
			return false;
	    });

		$(".apptFilter").change(function(){
            if(typeof $("option:selected", this).val() === "undefined"){
                $(this).val("");
            } else {
                $('option[value=""]', this).prop('selected', false);
            }
            $("select").trigger("liszt:updated");
            $('#appointments-form').attr('action','/admin/tracking/appointments');
            updateGrid();
			return false;
		});

        function updateGrid() {
            $.fn.yiiGridView.update("appointments-grid", {
                data: $("form#appointments-form").serialize(),
                complete: function(jqXHR, status) {

                    if (status=='success'){
                        var html = $.parseHTML(jqXHR.responseText);
                        var newAppointmentSummary = $('#appointment-summary', $(html)).html();
                        var newAppointmentSummary2 = $('#appointment-summary-buyerSeller', $(html)).html();
//                        var newSellerApptTab = $('.ui-tabs-nav li a[href="#sellers-appointments-tab"]', $(html)).html();

                        $('#appointment-summary').html(newAppointmentSummary);
                        $('#appointment-summary-buyerSeller').html(newAppointmentSummary2);
//                        $('.ui-tabs-nav li a[href="#sellers-appointments-tab"]').html(newSellerApptTab);
                    }
                }
            });
        }

        $('.hideTable').live('click',function(){
            var table = $(this).parents("table:first");
            var tableName;
            if($(this).hasClass('hideBuyerTable')) {
                tableName = 'buyerTable';
            }
            else if ($(this).hasClass('hideSellerTable')) {
                tableName = 'sellerTable';
            }

            var buttonContent = $(this).html();
            if(buttonContent.indexOf('Hide Table') > -1) {
                //replace hide with show
                var newButtonContent = buttonContent.replace('Hide Table', 'Show Table');
                newButtonContent = newButtonContent.replace('i_stm_delete', 'i_stm_add');

                $(this).html(newButtonContent);
                $('.'+tableName+' .agent-info').hide();

            } else {
                 //replace show with hide
                var newButtonContent = buttonContent.replace('Show Table', 'Hide Table');
                newButtonContent = newButtonContent.replace('i_stm_add', 'i_stm_delete');
                $(this).html(newButtonContent);
                $('#'+table.attr('id')+' .agent-info').show();

               //change button to delete

            }
        });

        $('#appointmentReportShowHeaderBottom').click(function(){
            if($(this).is(":checked")) {
                $('.appointment-header-bottom').show('normal');
            }
            else {
                $('.appointment-header-bottom').hide('normal');
            }
        });
JS
);

$css = <<<CSS
        #appointment-summary th, #appointment-summary td{
            text-align: center;
        }
        .datatables td:not(:first-child), .datatables th:not(:first-child) {
            text-align: center;
        }
        .datatables {
            float: none;
        }
        #appointment-summary-buyerSeller tr.sub-header th{
            font-size: 11px;
            width: 7.7%;
            text-align: center;
            vertical-align: middle;
        }
        #appointment-summary-total tr.sub-header th {
            font-size: 11px;
            width: 7.7%;
        }
        #appointment-summary-buyerSeller tr.agent-info, #appointment-summary-total tr.agent-info {
            font-size: 13px;
        }
        #appointment-summary-buyerSeller tr.agent-info td, #appointment-summary-buyerSeller tr.total th:not(:first-child), #appointment-summary-total tr.sub-header th, #appointment-summary-total tr.agent-info td, #appointment-summary-total tr.total th:not(:first-child){
            text-align: center;
        }
        #appointment-summary-buyerSeller tr .seller {
            background-color: #d2ffc7;
            background-image: none;
        }
        #appointment-summary-buyerSeller tr .buyer {
            background-color: #f9ffd0;
            background-image: none;
        }
        /*#appointment-detailed-summary-recruit tr .recruit {*/
            /*background-color: #f9ffd0;*/
            /*background-image: none;*/
        /*}*/
        #appointment-summary-buyerSeller tr.total, #appointment-summary-total tr.total{
            border-top: 2px solid #666;
        }
        #appointment-summary-total tr .total {
            background-color: #d4f0ff;
            background-image: none;
        }
        .pastDue {
            color: #E20000 !important;
            font-weight: bold !important;
            font-size: 13px !important;
        }
        #appointment-summary-buyerSeller .text-button {
            display: block;
            margin-left: 0;
        }

        /*new table css*/
        #appointments-tracking-widget .appointment-detailed-summary tr.header2 th {
            font-size: 13px;
        }
        #appointments-tracking-widget .appointment-detailed-summary tr.agent-info{
            font-size: 13px;
        }
        #appointments-tracking-widget .appointment-detailed-summary tr.sub-header th:not(:first-child){
            font-size: 11px;
        }
        #appointments-tracking-widget .appointment-detailed-summary tr.sub-header th:not(:first-child) {
            width: 7.7%;
        }
        #appointments-tracking-widget .appointment-detailed-summary tr.sub-header th.thin{
            width: 5%;
        }
        #appointments-tracking-widget .appointment-detailed-summary tr.agent-info td.half{
            width: 3.35%;
        }
        .appointment-detailed-summary tr.header2 th:not(:first-child),
        .appointment-detailed-summary tr.sub-header th:not(:first-child),
        .appointment-detailed-summary tr.total th:not(:first-child),
        .appointment-detailed-summary tr.agent-info td{
            text-align: center;
            vertical-align: middle;
            background-image: none;
        }
        .appointment-detailed-summary.seller tr.header2 th:not(:first-child),
        .appointment-detailed-summary.seller tr.sub-header th:not(:first-child),
        .appointment-detailed-summary.seller tr.total th:not(:first-child),
        .appointment-detailed-summary.seller tr.agent-info td{
            background-color: #d2ffc7;
        }
        .appointment-detailed-summary.buyer tr.header2 th:not(:first-child),
        .appointment-detailed-summary.buyer tr.sub-header th:not(:first-child),
        .appointment-detailed-summary.buyer tr.total th:not(:first-child),
        .appointment-detailed-summary.buyer tr.agent-info td{
            background-color: #f9ffd0;
        }
        .appointment-detailed-summary.recruit tr.header2 th:not(:first-child),
        .appointment-detailed-summary.recruit tr.sub-header th:not(:first-child),
        .appointment-detailed-summary.recruit tr.total th:not(:first-child),
        .appointment-detailed-summary.recruit tr.agent-info td{
            background-color: #d4f0ff;
        }
        .appointment-detailed-summary.total tr.header2 th:not(:first-child),
        .appointment-detailed-summary.total tr.sub-header th:not(:first-child),
        .appointment-detailed-summary.total tr.total th:not(:first-child),
        .appointment-detailed-summary.total tr.agent-info td{
            background-color: #f3ddff;
        }

        .appointment-detailed-summary tr.total th:not(:first-child) {
            border-top: 2px solid #666;
        }

        .appointment-detailed-summary td.right-border,
        .appointment-detailed-summary th.right-border {
            border-right: 2px solid #888;
        }


        .ui-tabs-panel {
            margin-top: 20px;
            margin-bottom: 40px;
        }
CSS;
Yii::app()->clientScript->registerCss('appointmentsTrackingCss', $css);

	$form=$this->beginWidget('CActiveForm', array(
											'id'=>'appointments-form',
											'action'=>'',
											'method'=>'get',
											'clientOptions'=> array(
                                                'beforeValidate'   => 'js:function() {
                                                    return true;
                                                }',
											),
											));

?>

    <div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'appointments-grid','isForm'=>false,'formSelector'=>'#appointments-form', 'updateNonGridElement'=>'#appointment-summary,#appointment-detailed-summary-seller, #appointment-detailed-summary-buyer, #appointment-detailed-summary-recruit, #appointment-detailed-summary-total, a[href=\"#sellers-appointments-tab\"], a[href=\"#buyers-appointments-tab\"], a[href=\"#recruits-appointments-tab\"], a[href=\"#total-appointments-tab\"]', 'returnSubmit' => 'false')); ?>
</div>
<?php $this->breadcrumbs = array(
	'Tasks' => '',
);
?>
<h1>Appointments Report</h1>
<h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label></h3>

<!--    <div style="width:850px;margin-left: auto;margin-right: auto; padding:20px;">-->
<!--        <table id="appointment-summary" class="p-f0" style="font-size: 16px;">-->
<!--            <tr>-->
<!--                <th style="width: 15%;"></th>-->
<!--                <th style="width: 15%;">Set Appointments</th>-->
<!--                <th style="width: 15%;">Met Appointments</th>-->
<!--                <th style="width: 15%;">Set to Met %</th>-->
<!--                <th style="width: 15%;">Signed Agreements</th>-->
<!--                <th style="width: 15%;">Met to Signed %</th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th>Total</th>-->
<!--                <td>--><?php //echo $setAppts; ?><!--</td>-->
<!--                <td>--><?php //echo $metAppts; ?><!--</td>-->
<!--                <td>--><?php //echo ($setAppts >0)? Yii::app()->format->formatPercentages($metAppts/$setAppts) : '-'; ?><!--</td>-->
<!--                <td>--><?php //echo $signed; ?><!--</td>-->
<!--                <td>--><?php //echo ($metAppts >0)? Yii::app()->format->formatPercentages($signed/$metAppts) : '-'; ?><!--</td>-->
<!--            </tr>-->
<!--        </table>-->
<!--    </div>-->

    <div style="width:95%;margin-left: auto;margin-right: auto; padding:20px;">
    <?php
    // ****************** LOOP THROUGH THE 3 TABLES ********************
    $reportTypes = array('seller','buyer','recruit','total');
    foreach($reportTypes as $reportType) {
        ob_start();

        ?>
        <!--SELLER Appointments Table-->
        <table id="appointment-detailed-summary-<?php echo $reportType; ?>" class="p-f0 <?php echo $reportType; ?> appointment-detailed-summary" style="font-size: 16px;">
            <tr class="header">
                <th style="width: 10%; text-align: center;"></th>
                <th style="width: 85%; text-align: center;" colspan="16"><?php echo strtoupper($reportType); ?> APPOINTMENTS</th>
                <th style="width: 5%; text-align: center;" colspan="2"><!--<a href="javascript:void(0)" class="text-button hideTable"><em class="icon i_stm_delete"></em>Hide Table</a>--></th>
                <?if(Yii::app()->user->client->type !== 'Brokerage'):?>
                <th style="text-align: center;" colspan="3">CLOSINGS</th>
                <?endif;?>
            </tr>
            <tr class="header2">
                <th></th>
                <th colspan="3">You Set & Met</th>
                <th colspan="3">Set for You by ISA</th>
                <th colspan="3">Set for Others</th>
                <th colspan="2">Set</th>
                <th colspan="2">Met</th>
                <th colspan="4">Signed Agreements</th>
                <th>Met to Signed</th>
                <?if(Yii::app()->user->client->type !== 'Brokerage'):?>
                <th colspan="3">Closings</th>
                <?endif;?>
            </tr>
            <tr class="sub-header">
                <th></th>
                <!--SELLER Appointments-->
                <th># Set<br>(Future*)</th>
                <th colspan="2">Met<br># &nbsp; &nbsp; &nbsp; %</th>
                <th># Set<br>(Future)</th>
                <th colspan="2">Met<br># &nbsp; &nbsp; &nbsp; %</th>
                <th># Set<br>(Future)</th>
                <th colspan="2">Met<br># &nbsp; &nbsp; &nbsp; %</th>
                <th class="thin">Set for You This Period Total</th>
                <th class="thin"># Set to Meet per Appt (Prev*)</th>
                <th class="thin">Met Total (Prev*)</th>
                <th class="thin">Set to Met %</th>
                <th class="thin">Set & Met<br>(Prior)</th>
                <th class="thin">Set by Others<br>(Prior)</th>
                <th class="thin">Set for Others<br>(Prior)</th>
                <th class="thin">Total<br>(Prior)</th>
                <th>%</th>
                <?if(Yii::app()->user->client->type !== 'Brokerage'):?>

                <th class="thin"># Closed</th>
<!--                <th class="thin">% Leads Closed</th>-->
                <th class="thin">% Appt Met Closed</th>
                <th class="thin">% Signed Closed</th>
                <?endif;?>
            </tr>

            <?php foreach($agents as $agent) {
                    // for cleaner referencing below
                    $agentData = $reportData[$reportType][$agent->id];
            ?>
                <tr class="agent-info">
                    <th><?php echo $agent->fullName; ?>
                        <?php  if(strpos($_SERVER['HTTP_HOST'], 'christineleeteam')):
//                            $pastDueTotalLabel = ($pastDueTotal = $agentAppointments[$agent->id]['pastDueSeller'] + $agentAppointments[$agent->id]['pastDueBuyer'])? '<div class="pastDue">'.$pastDueTotal.' PAST DUE!</div>' :'';
                            echo $pastDueLabel = ($pastDueCount = $agentData['pastDue'])? '<div class="pastDue">'.$pastDueCount.' PAST DUE!</div>' :'';
//                            $pastDueBuyerLabel = ($pastDueBuyer = $agentAppointments[$agent->id]['pastDueBuyer'])? '<div class="pastDue">'.$pastDueBuyer.' PAST DUE!</div>' :'';
                        endif;
                        ?>
                    </th>
                    <td><?php echo ($agentData['youSetForSelf'])?$agentData['youSetForSelf'] : '-'; ?> <?php echo ($agentData['setFutureTotal'])? '('.$agentData['setFutureTotal'].')' : ''; ?></td>
                    <td class="half"><?php echo ($agentData['youSetMet'])?$agentData['youSetMet'] : '-'; ?></td>
                    <td class="half right-border"><?php echo ($agentData['youSetForSelf'])?$agentData['youSetMetPercent'] : '-'; ?></td>

                    <td><?php echo ($agentData['setForYou'])?$agentData['setForYou'] : '-'; ?> <?php echo ($agentData['setForYouFuture'])? '('.$agentData['setForYouFuture'].')' : ''; ?></td>
                    <td class="half"><?php echo ($agentData['setForYouMet'])?$agentData['setForYouMet'] : '-'; ?></td>
                    <td class="half right-border"><?php echo ($agentData['setForYouMetPercent'])?$agentData['setForYouMetPercent'] : '-'; ?></td>

                    <td><?php echo ($agentData['setForOthers'])?$agentData['setForOthers'] : '-'; ?> <?php echo ($agentData['setForOthersFuture'])? '('.$agentData['setForOthersFuture'].')' : ''; ?></td>
                    <td class="half"><?php echo ($agentData['setForOthersMet'])?$agentData['setForOthersMet'] : '-'; ?></td>
                    <td class="half right-border"><?php echo ($agentData['setForOthersMetPercent'])?$agentData['setForOthersMetPercent'] : '-'; ?></td>

                    <td class="half"><?php echo ($agentData['setForYouTotal'])?$agentData['setForYouTotal'] : '-'; ?></td>
                    <td class="half right-border"><?php echo ($agentData['metPerDateTotal'])?$agentData['metPerDateTotal'] : '-'; ?> <?php echo ($agentData['setForYouTotalFuture'])? '('.$agentData['setForYouTotalFuture'].')' : ''; ?></td>


                    <td class="half"><?php echo ($agentData['metTotal'])?$agentData['metTotal'] : '-'; ?><?php echo ($agentData['metPrevTotal'])? ' ('.$agentData['metPrevTotal'].')' : ''; ?></td>
                    <td class="half right-border"><?php echo ($agentData['setMetTotalPercent'])?$agentData['setMetTotalPercent'] : '-'; ?></td>
                    <td><?php echo ($agentData['youSetMetSigned'])?$agentData['youSetMetSigned'] : '-'; ?> <?php echo ($agentData['youSetMetPrevSigned'])? '('.$agentData['youSetMetPrevSigned'].')' : ''; ?></td>
                    <td><?php echo ($agentData['setForYouSigned'])?$agentData['setForYouSigned'] : '-'; ?> <?php echo ($agentData['setForYouPrevSigned'])? '('.$agentData['setForYouPrevSigned'].')' : ''; ?></td>
                    <td><?php echo ($agentData['setForOthersSigned'])?$agentData['setForOthersSigned'] : '-'; ?> <?php echo ($agentData['setForOthersPrevSigned'])? '('.$agentData['setForOthersPrevSigned'].')' : ''; ?></td>
                    <td class="right-border"><?php echo ($agentData['signedTotal'])?$agentData['signedTotal'] : '-'; ?> <?php echo ($agentData['signedPrevTotal'])? '('.$agentData['signedPrevTotal'].')' : ''; ?></td>
                    <td class="right-border"><?php echo ($agentData['metSignedTotalPercent'])?$agentData['metSignedTotalPercent'] : '-'; ?></td>
                    <?if(Yii::app()->user->client->type !== 'Brokerage'):?>
                    <td><?php echo ($agentData['closed'])?$agentData['closed'] : '-'; ?></td>
                    <td><?php echo ($agentData['metClosedPercent'])?$agentData['metClosedPercent'] : '-'; ?></td>
                    <td><?php echo ($agentData['signedClosedPercent'])?$agentData['signedClosedPercent'] : '-'; ?></td>
                    <?endif;?>
                </tr>
            <?php }
            $TypeTotalData = $reportData[$reportType.'Total'];
            ?>

            <tr class="total">
                <th>TOTAL:</th>
                <th><?php echo ($TypeTotalData['youSetForSelf'])?$TypeTotalData['youSetForSelf'] : '-'; ?> <?php echo ($TypeTotalData['setFutureTotal'])? '('.$TypeTotalData['setFutureTotal'].')' : ''; ?></th>
                <th><?php echo ($TypeTotalData['youSetMet'])?$TypeTotalData['youSetMet'] : '-'; ?></th>
                <th class="right-border"><?php echo ($TypeTotalData['youSetForSelf'])?$TypeTotalData['youSetMetPercent'] : '-'; ?></th>
                <th><?php echo ($TypeTotalData['setForYou'])?$TypeTotalData['setForYou'] : '-'; ?> <?php echo ($TypeTotalData['setForYouFuture'])? '('.$TypeTotalData['setForYouFuture'].')' : ''; ?></th>
                <th><?php echo ($TypeTotalData['setForYouMet'])?$TypeTotalData['setForYouMet'] : '-'; ?></th>
                <th class="right-border"><?php echo ($TypeTotalData['setForYouMetPercent'])?$TypeTotalData['setForYouMetPercent'] : '-'; ?></th>
                <th><?php echo ($TypeTotalData['setForOthers'])?$TypeTotalData['setForOthers'] : '-'; ?> <?php echo ($TypeTotalData['setForOthersFuture'])? '('.$TypeTotalData['setForOthersFuture'].')' : ''; ?></th>
                <th><?php echo ($TypeTotalData['setForOthersMet'])?$TypeTotalData['setForOthersMet'] : '-'; ?></th>
                <th class="right-border"><?php echo ($TypeTotalData['setForOthersMetPercent'])?$TypeTotalData['setForOthersMetPercent'] : '-'; ?></th>
                <th class=""><?php echo ($TypeTotalData['setForYouTotal'])?$TypeTotalData['setForYouTotal'] : '-'; ?> <?php echo ($TypeTotalData['setForYouTotalFuture'])? '('.$TypeTotalData['setForYouTotalFuture'].')' : ''; ?></th>
                <th class="right-border"><?php echo ($TypeTotalData['metPerDateTotal'])?$TypeTotalData['metPerDateTotal'] : '-'; ?></th>
                <th><?php echo ($TypeTotalData['metTotal'])?$TypeTotalData['metTotal'] : '-'; ?></th>
                <th class="right-border"><?php echo ($TypeTotalData['setMetTotalPercent'])?$TypeTotalData['setMetTotalPercent'] : '-'; ?></th>
                <th><?php echo ($TypeTotalData['youSetMetSigned'])?$TypeTotalData['youSetMetSigned'] : '-'; ?> <?php echo ($TypeTotalData['youSetMetPrevSigned'])? '('.$TypeTotalData['youSetMetPrevSigned'].')' : ''; ?></th>
                <th><?php echo ($TypeTotalData['setForYouSigned'])?$TypeTotalData['setForYouSigned'] : '-'; ?> <?php echo ($TypeTotalData['setForYouPrevSigned'])? '('.$TypeTotalData['setForYouPrevSigned'].')' : ''; ?></th>
                <th><?php echo ($TypeTotalData['setForOthersSigned'])?$TypeTotalData['setForOthersSigned'] : '-'; ?> <?php echo ($TypeTotalData['setForOthersPrevSigned'])? '('.$TypeTotalData['setForOthersPrevSigned'].')' : ''; ?></th>
                <th class="right-border"><?php echo ($TypeTotalData['signedTotal'])?$TypeTotalData['signedTotal'] : '-'; ?> <?php echo ($TypeTotalData['signedPrevTotal'])? '('.$TypeTotalData['signedPrevTotal'].')' : ''; ?></th>
                <th class="right-border"><?php echo ($TypeTotalData['metSignedTotalPercent'])?$TypeTotalData['metSignedTotalPercent'] : '-'; ?></th>

                <?if(Yii::app()->user->client->type !== 'Brokerage'):?>
                <th><?php echo ($TypeTotalData['closed'])?$TypeTotalData['closed'] : '-'; ?></th>
                <th><?php echo ($TypeTotalData['metClosedPercent'])?$TypeTotalData['metClosedPercent'] : '-'; ?></th>
                <th><?php echo ($TypeTotalData['signedClosedPercent'])?$TypeTotalData['signedClosedPercent'] : '-'; ?></th>
                <?endif;?>
            </tr>
            <tr class="sub-header appointment-header-bottom" style="display: none;">
                <th></th>
                <!--SELLER Appointments-->
                <th># Set<br>(Future*)</th>
                <th colspan="2">Met<br># &nbsp; &nbsp; &nbsp; %</th>
                <th># Set<br>(Future)</th>
                <th colspan="2">Met<br># &nbsp; &nbsp; &nbsp; %</th>
                <th># Set<br>(Future)</th>
                <th colspan="2">Met<br># &nbsp; &nbsp; &nbsp; %</th>
                <th class="thin">Set for You This Period Total</th>
                <th class="thin"># Set to Meet per Appt (Prev*)</th>
                <th class="thin">Met Total (Prev*)</th>
                <th class="thin">Set to Met %</th>
                <th class="thin">Set & Met<br>(Prior)</th>
                <th class="thin">Set by Others<br>(Prior)</th>
                <th class="thin">Set for Others<br>(Prior)</th>
                <th class="thin">Total<br>(Prior)</th>
                <th>%</th>
                <?if(Yii::app()->user->client->type !== 'Brokerage'):?>
                <th class="thin"># Closed</th>
<!--                <th class="thin">% Leads Closed</th>-->
                <th class="thin">% Appt Met Closed</th>
                <th class="thin">% Signed Closed</th>
                <?endif;?>
            </tr>
            <tr class="header2 appointment-header-bottom" style="display: none;">
                <th></th>
                <th colspan="3">You Set & Met</th>
                <th colspan="3">Set for You by Others</th>
                <th colspan="3">Set for Others</th>
                <th colspan="2">Set</th>
                <th colspan="2">Met</th>
                <th colspan="4">Signed Agreements</th>
                <th>Met to Signed</th>
                <?if(Yii::app()->user->client->type !== 'Brokerage'):?>
                <th colspan="3">Closings</th>
                <?endif;?>
            </tr>
        </table>
    <?php
        ${$reportType.'TotalApptSet'} = ($TypeTotalData['setTotal'])? $TypeTotalData['setTotal'] : 0;
        ${$reportType.'TableContent'} = ob_get_contents();
        ob_end_clean();
    } ?>
    <br /><br /><br />

        <?php
        if(Yii::app()->user->client->type == 'Brokerage') {
            $tabs = array(
                'Recruit Appointments ('.$recruitTotalApptSet.')'=> array(
                    'content' => $recruitTableContent,
                    'id' => 'recruits-appointments-tab',
                ),
            );
        }
        else {
            $tabs = array(
                'Seller Appointments ('.$sellerTotalApptSet.')'=> array(
                    'content' => $sellerTableContent,
                    'id' => 'sellers-appointments-tab',
                ),
                'Buyer Appointments ('.$buyerTotalApptSet.')'=> array(
                    'content' => $buyerTableContent,
                    'id' => 'buyers-appointments-tab',
                ),
                'Recruit Appointments ('.$recruitTotalApptSet.')'=> array(
                    'content' => $recruitTableContent,
                    'id' => 'recruits-appointments-tab',
                ),
                'Total Appointments ('.$totalTotalApptSet.')'=> array(
                    'content' => $totalTableContent,
                    'id' => 'total-appointments-tab',
                ),
            );
        }


        $this->widget('zii.widgets.jui.CJuiTabs', array(
                'tabs' => $tabs,
                'id' => 'appointments-tracking-widget',
            )
        );
        ?>
        <div><?=CHtml::checkBox('appointmentReportShowHeaderBottom', false)?> Show header at bottom</div>
        <div>*Indicates a subsection of the total and is included in the total.</div>
    </div>
    <hr/>
<div class="g12" style="margin-bottom: -30px;">
    <div class="g2">
        <div class="g12">Appointment Set By:</div>
        <?php echo CHtml::dropDownList('set_by_id',null,CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('empty'=>'Anyone','class'=>'apptFilter chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Names'));
        ?>
    </div>
    <div class="g2">
        <div class="g12">Appointment Met By:</div>
        <?php echo CHtml::dropDownList('met_by_id',null,CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('empty'=>'Anyone','class'=>'apptFilter chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Names'));

        $this->widget('admin_module.extensions.EChosen.EChosen', array(
                'target' => 'select#set_by_id, select#met_by_id',
            ));
        ?>
    </div>
    <div class="g2" <?=(Yii::app()->user->client->type == 'Brokerage') ? 'style="display: none;"' : '';?>>
        <div class="g12">Buyer or Seller Appointment:</div>
    <?php echo CHtml::dropDownList('component_type_id',null,array(ComponentTypes::SELLERS=>'Sellers',ComponentTypes::BUYERS=>'Buyers',ComponentTypes::RECRUITS=>'Recruits'),array('empty'=>'','class'=>'apptFilter chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Buyer or Seller'));
    $this->widget('admin_module.extensions.EChosen.EChosen', array(
                                                             'target' => 'select#component_type_id',
                                                             ));
    ?>
    </div>
    <div class="g2">
        <div class="g12">Date Filter:</div>
        <?php echo CHtml::dropDownList('by_date_filter','set' ,array('set'=>'by Appt Set Date','met'=>'by Appt Met Date','both'=>'by Appt Set AND Met Date','either'=>'by Appt Set OR Met Date', 'signed'=>'by Signed Date'),array('class'=>'apptFilter chzn-select','style'=>'width:100%;'));
    //    $this->widget('admin_module.extensions.EChosen.EChosen', array(
    //                                                             'target' => 'select#by_set_met',
    //                                                             ));
        ?>
    </div>
    <div class="g1">
        <div class="g12">Past Due:</div>
        <?php echo CHtml::dropDownList('past_due', null, array(''=>'',1=>'Yes'),array('class'=>'apptFilter chzn-select','style'=>'width:100%;'));
        ?>
    </div>
    <div class="g2">
        <div class="g12">Status:</div>
        <?php echo CHtml::dropDownList('met_status', null, $DataProvider->model->getStatusTypes(),array('class'=>'apptFilter chzn-select','style'=>'width:100%;', 'empty'=>''));
        ?>
    </div>
    <div class="g1">
        <div class="g12">Signed:</div>
        <?php echo CHtml::dropDownList('signed', null, array(''=>'',1=>'Yes', 0=>'No'), array('class'=>'apptFilter chzn-select','style'=>'width:100%;'));
        ?>
    </div>
</div>
<div class="clear p-pb10 p-mb10"></div>
<!--<div class="g4">-->
<!--	--><?// echo CHtml::radioButton('met',true,array('value'=>0,'style'=>'font-size:16px;'))?><!-- <span style="font-size: 15px;">by Appt Set Date&nbsp;&nbsp;&nbsp;&nbsp;</span>--><?// echo CHtml::radioButton('met',false,array('value'=>1,'style'=>'font-size:16px;'))?><!-- <span style="font-size: 15px;">by Appt Meet Date</span>-->
<!--</div>-->
<?php
$this->endWidget();
$this->renderPartial('_appointments', array('DataProvider' => $DataProvider,'dateRange' => $dateRange));
