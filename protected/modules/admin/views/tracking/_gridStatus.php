<?=StmFormHelper::getBooleanImage($model->mostRecentClick->status_ma,"report-status-image-".$model->id)?>
<div class="report-status-action-button report-status-text-"<?=$model->mostRecentClick->id?>>&#9660;
    <div class="status-update-button report-status-action-text <?=(($model->mostRecentClick->status_ma) ? "mark-incomplete" : "mark-complete")?>" data-id="<?=$model->mostRecentClick->id?>"><?=(($model->mostRecentClick->status_ma) ? "Mark as Not Followed-Up" : "Mark as Followed-Up")?>
    </div>
</div>
<?if($model->mostRecentClick->status_ma):?>
    <div style="margin-top: 8px;"><?=$model->mostRecentClick->updatedBy->fullName?><br><?=Yii::app()->format->formatDateTime($model->mostRecentClick->updated)?></div>
<?endif;?>

<?if($model->mostRecentClick->updated == '0000-00-00 00:00:00' && !in_array($model->prevUpdated, array('0000-00-00 00:00:00', null))): ?>
    <div style="margin-top: 8px;">Prev Follow-up: <?=Yii::app()->format->formatDateTime($model->prevUpdated)?></div>
<?endif;?>
<? //echo print_r($model->mostRecentClick->attributes, true).PHP_EOL.PHP_EOL; echo print_r($model->attributes, true); echo $model->prevUpdated;