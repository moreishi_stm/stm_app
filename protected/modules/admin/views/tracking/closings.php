<?php
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');
//Yii::app()->clientScript->registerScriptFile($this->module->cdnAssetUrl . DS . 'js'.DS. 'curvedLines.js');

$showGraph  = ($ClosedGraphLabel && $ClosedGraphData) ? 1 : 1;
$thisYear = date('Y');
$lastYear = $thisYear-1;

extract($graphData);

Yii::app()->clientScript->registerCss('tracking-closings-css', <<<CSS
    .tickLabel {
        font-size: 11px;
    }
    div.chart .legend table td {
        font-size: 11px;
    }
    tr.net {
        background: #A9ECEC;
    }
CSS
);

Yii::app()->clientScript->registerScript('tracking-closings-script', <<<JS
    $("#closings-form").submit(function(){
        $(this).submit();
    });

    if($showGraph) {

       $('#closings-graph').wl_Chart({
            flot: {
                bars:{
                    align: "center",
                    lineWidth: 0,
                    fillColor: {
                        colors: [ { opacity: .9 }, { opacity: .9 } ]
                    }
                },
                yaxis: {
                    tickDecimals: 0,
//                    max: $ClosedGraphUnitDataMax
//                    axisLabel: '%',
//                    tickFormatter: function (val, axis) {
//                        return val;
//                    }
                },
                xaxis: {
//                    axisLabelPadding: 5,
                    tickLength: 0,
                    //labelWidth: 200,
//                    axisLabelFontSizePixels: 25,
//                    axisLabelUseCanvas: true,
                    ticks: $ClosedGraphLabel
                },
                legend: {position: "nw", noColumns: 12} //,
            },
            type: 'bars',
//            barWidth: 0.8,
            data:[
                {
                    color: "#45D914",//"#528EFF", //FFAB4A
                    label : 'Seller Closings',
                    data: $ClosedGraphSellerUnitData
                },
                {
                    color: "#669BFF",//"#528EFF", //FFAB4A
                    label:'Buyer Closings',
                    data: $ClosedGraphBuyerUnitData
                }
            ]
        });

       $('#closings-yoy-units-graph').wl_Chart({
            flot: {
                bars:{
                    align: "center",
                    lineWidth: 0,
                    fillColor: {
                        colors: [ { opacity: .9 }, { opacity: .9 } ]
                    }
                },
                yaxis: {
                    tickDecimals: 0
                    //max: $ClosedGraphUnitDataMax
//                    axisLabel: '%',
//                    tickFormatter: function (val, axis) {
//                        return val;
//                    }
                },
                xaxis: {
//                    axisLabelPadding: 5,
                    tickLength: 0,
                    //labelWidth: 200,
//                    axisLabelFontSizePixels: 25,
//                    axisLabelUseCanvas: true,
                    ticks:[
                       [0,'Jan'],[1,'Feb'],[2,'Mar'],[3,'Apr'],[4,'May'],[5,'Jun'],[6,'Jul'],[7,'Aug'],[8,'Sep'],[9,'Oct'],[10,'Nov'],[11,'Dec']
                    ]
                },
                legend: {position: "ne", noColumns: 12, sorted: "reverse"}
            },
            type: 'lines',
//            barWidth: 0.8,
            data:[
//                {
//                    label : '2013 Closings',
//                    color : '#E8D1FF',//FFD9FA
////                    lines: {
////                        lineWidth: 3
////                    },
//                    data: [{$ClosedAnnualUnitData[2013]}]
//                },
                {
                    label : '{$thisYear} Closed Units',
                    color: "#45D914",
                    lines: {
                        lineWidth: 4,
                        fill: true,
                        fillColor:{ colors: [ { opacity: .8 }, { opacity: .8 } ]   }
                    },
                    data: {$ClosedAnnualUnitDataThisYear}
                },
                {
                    label : '{$thisYear} Pending Units',
                    color: "#669BFF",
                    lines: {
                        lineWidth: 4,
                        fill: true,
                        fillColor:{ colors: [ { opacity: .8 }, { opacity: .8 } ]   }
                    },
                    data: {$PendingAnnualUnitDataThisYear}
                },
                {
                    label:'{$lastYear} Closed Units',
                    color: "#E861D8",//FFE48C
                    lines: {
                        lineWidth: 2
                    },
//                    curvedLines: {apply: true },
                    data: {$ClosedAnnualUnitDataLastYear}
                }
            ]
        });

       $('#closings-yoy-volume-graph').wl_Chart({
            flot: {
                bars:{
                    align: "center",
                    lineWidth: 0,
                    fillColor: {
                        colors: [ { opacity: .8 }, { opacity: .8 } ]
                    }
                },
                yaxis: {
                    tickDecimals: 0,
                    axisLabel: '$',
                    tickFormatter: function (val, axis) {
                        var suffix = '';

                        if(val > 10000 && val < 1000000) {

                            val = val / 1000;
                            suffix = 'k';
                        }
                        else if (val >= 1000000) {
                            val = val / 1000000;
                            suffix = 'M';
                        }
                        return '$' + val + suffix;
                    }
                },
                xaxis: {
//                    axisLabelPadding: 5,
                    tickLength: 0,
                    //labelWidth: 200,
//                    axisLabelFontSizePixels: 25,
//                    axisLabelUseCanvas: true,
                    ticks:[
                       [0,'Jan'],[1,'Feb'],[2,'Mar'],[3,'Apr'],[4,'May'],[5,'Jun'],[6,'Jul'],[7,'Aug'],[8,'Sep'],[9,'Oct'],[10,'Nov'],[11,'Dec']
                    ]
                },
                legend: {position: "ne", noColumns: 12} //, sorted: "reverse"
            },
            type: 'lines',
//            barWidth: 0.8,
            data:[
//                {
//                    label : '2013 Closings',
//                    color : '#E8D1FF',//FFD9FA
////                    lines: {
////                        lineWidth: 3
////                    },
//                    data: [{$ClosedAnnualVolumeData[2013]}]
//                },

                {
                    label : '{$thisYear} Closed Volume',
                    color: "#45D914",
//                    bars: {
//                        show: true,
//                        align: "right"
//                        barWidth: .8
//                        lineWidth:1
//                    },
                    lines: {
//                        show: false,
//                        points: { show: false, radius: 0 } //symbol: "triangle", fillColor: "#0062FF",
                        lineWidth: 4,
                        fill: true,
                        fillColor:{ colors: [ { opacity: .8 }, { opacity: .8 } ]   }
                    },
                    data: {$ClosedAnnualVolumeDataThisYear}
                },
                {
                    label : '{$thisYear} Pending Volume',
                    color: "#669BFF",
                    lines: {
                        lineWidth: 4,
                        fill: true,
                        fillColor:{ colors: [ { opacity: .8 }, { opacity: .8 } ]   }
                    },
                    data: {$PendingAnnualVolumeDataThisYear}
                },
               {
                    label:'{$lastYear} Closed Volume',
                    color: "#E861D8",//FFE48C
                    lines: {
//                        show: true,
                        lineWidth: 1
                    },
//                    bars: {
//                        show: false
//                        align: "left"
//                        barWidth: .4
//                        lineWidth:1
//                    },
                    data: {$ClosedAnnualVolumeDataLastYear}
                }
            ]
        });
    }
JS
);

	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'closings-form',
			'action' => '',
			'method' => 'post',
		)
	);

?>
	<div class="g12" style="position: absolute; top:2px;">
		<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
				'fromDateLabelSelector' => '#from-date-label',
				'toDateLabelSelector' => '#to-date-label',
				'gridName' => 'closings-grid',
				'isForm' => false,
				'formSelector' => '#closings-form',
				'defaultSelect' => $DateRangerSelect
			)
		); ?>
	</div>
    <div class="p-clr"></div>
    <div class="g2 p-fr">
        <?php   echo CHtml::dropDownList('contactId', $_POST['contactId'], CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Select Name','empty'=>''));
        $this->widget('admin_module.extensions.EChosen.EChosen', array(
                'target' => 'select#contactId, select#componentTypeId', 'options' => array('allow_single_deselect' => true) //,'width'=>'150px'
            ));
        ?>
    </div>
    <div class="g2 p-fr">
        <?php   echo CHtml::dropDownList('componentTypeId', $_POST['componentTypeId'], array(ComponentTypes::SELLERS => 'Sellers', ComponentTypes::BUYERS => 'Buyers'), array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Seller & Buyers','empty'=>'')); ?>
    </div>
    <div class="p-clr"></div>
<?php $this->breadcrumbs = array(
	'Closings' => ''
);
?>
	<h1>Closings Report</h1>
	<h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date'])); ?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y",
				strtotime($dateRange['to_date'])
			); ?></label></h3>
	<div style="width:600px;margin-left: auto;margin-right: auto; padding:40px;">
		<table class="p-f0" style="font-size: 16px;">
			<tr>
				<th></th>
				<th>Pending</th>
				<th>Closed</th>
				<th>Total</th>
			</tr>
			<tr>
				<th>Units</th>
				<td><?php echo $PendingDataProvider->totalItemCount; ?></td>
				<td><?php echo $ClosedDataProvider->totalItemCount; ?></td>
				<td><?php echo $ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount; ?></td>
			</tr>
            <tr>
                <th>Gross Commission</th>
                <td><?php echo Yii::app()->format->formatDollars($PendingGCI); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($ClosedGCI); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($PendingGCI + $ClosedGCI); ?></td>
            </tr>
            <tr>
                <th>Agent Commission</th>
                <td><?php echo Yii::app()->format->formatDollars($PendingAgentCommissionTotal); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($ClosedAgentCommissionTotal); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($PendingAgentCommissionTotal + $ClosedAgentCommissionTotal); ?></td>
            </tr>
            <tr>
                <th>Internal Referral Commission</th>
                <td><?php echo Yii::app()->format->formatDollars($PendingInternalReferralCommissionTotal); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($ClosedInternalReferralCommissionTotal); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($PendingInternalReferralCommissionTotal + $ClosedInternalReferralCommissionTotal); ?></td>
            </tr>
            <tr class="net">
                <th>Net Commission</th>
                <td><?php echo Yii::app()->format->formatDollars($PendingNetCommissionTotal); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($ClosedNetCommissionTotal); ?></td>
                <td><?php echo Yii::app()->format->formatDollars($PendingNetCommissionTotal + $ClosedNetCommissionTotal); ?></td>
            </tr>
			<tr>
				<th>Volume</th>
				<td><?php echo Yii::app()->format->formatDollars($PendingVolume); ?></td>
				<td><?php echo Yii::app()->format->formatDollars($ClosedVolume); ?></td>
				<td><?php echo Yii::app()->format->formatDollars($ClosedVolume + $PendingVolume); ?></td>
			</tr>
			<tr>
				<th>Avg Sales Price</th>
				<td><?php echo ($PendingDataProvider->totalItemCount)? Yii::app()->format->formatDollars($PendingVolume / $PendingDataProvider->totalItemCount) : '-'; ?></td>
				<td><?php echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars($ClosedVolume / $ClosedDataProvider->totalItemCount) : '-'; ?></td>
				<td><?php echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars(($ClosedVolume + $PendingVolume) / ($ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount)) : '-'; ?></td>
			</tr>
		</table>
	</div>

<?if(1) { ?>

    <div id="closings-graph-container" style="padding: 10px; float: left; width: 32%;">
        <h1>Monthly Closings</h1>
        <h2>Sellers & Buyers</h2>
        <table id="closings-graph" data-height="225" data-tooltip="false" data-stack="true" width="75%"></table>
        <h2><br></h2>
    </div>

    <div id="closings-yoy-units-graph-container" style="padding: 10px; float: left; width: 32%;">
        <h1>Closed # Units</h1>
        <h2>Year over Year Comparison</h2>
        <table id="closings-yoy-units-graph" data-height="225" data-tooltip="false" width="75%"></table>
        <h2><br></h2>
    </div>

    <div id="closings-yoy-volume-graph-container" style="padding: 10px; float: left; width: 32%;">
        <h1>Closed $ Volume</h1>
        <h2>Year over Year Comparison</h2>
        <table id="closings-yoy-volume-graph" data-height="225" data-tooltip="false" data-stack="false" width="75%"></table>
        <h2><br></h2>
    </div>
    <div class="p-clr"></div>

<? } ?>
	<div class="g12p-mr0">
		<?php
			$this->widget('zii.widgets.jui.CJuiTabs', array(
					'tabs' => array(
						'Scheduled to Close (' . $PendingDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($PendingVolume) => array(
							'content' => $this->renderPartial('_closings', array(
									'DataProvider' => $PendingDataProvider,
									'gridName' => 'pending',
                                    'gciTotal' => $PendingGCI,
									'volumeTotal' => $PendingVolume,
									'agentCommissionTotal' => $PendingAgentCommissionTotal,
									'netCommissionTotal' => $PendingNetCommissionTotal,
                                    'internalReferralCommissionTotal' => $PendingInternalReferralCommissionTotal,
								), true
							),
							'id' => 'pending-closings-tab',
						),
                        'All Pendings (' . $AllPendingDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($AllPendingVolume) => array(
                            'content' => $this->renderPartial('_closings', array(
                                   'DataProvider' => $AllPendingDataProvider,
                                   'gridName' => 'all-pending',
                                   'gciTotal' => $AllPendingGCI,
                                   'volumeTotal' => $AllPendingVolume,
                                   'agentCommissionTotal' => $AllPendingAgentCommissionTotal,
                                   'netCommissionTotal' => $AllPendingNetCommissionTotal,
                                   'internalReferralCommissionTotal' => $AllPendingInternalReferralCommissionTotal
                                ), true
                            ),
                            'id' => 'all-pending-closings-tab',
                        ),
						'Closed Contracts (' . $ClosedDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($ClosedVolume) => array(
							'content' => $this->renderPartial('_closings', array(
									'DataProvider' => $ClosedDataProvider,
									'gridName' => 'closed',
                                    'gciTotal' => $ClosedGCI,
									'volumeTotal' => $ClosedVolume,
                                    'agentCommissionTotal' => $ClosedAgentCommissionTotal,
                                    'netCommissionTotal' => $ClosedNetCommissionTotal,
                                    'internalReferralCommissionTotal' => $ClosedInternalReferralCommissionTotal
                                ), true
							),
							'id' => 'closed-contracts-tab',
						),
						'New Executed Contracts (' . $ExecutedDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($ExecutedVolume) => array(
							'content' => $this->renderPartial('_closings', array(
									'DataProvider' => $ExecutedDataProvider,
									'gridName' => 'executed',
                                    'gciTotal' => $ExecutedGCI,
									'volumeTotal' => $ExecutedVolume,
                                    'agentCommissionTotal' => '',
                                    'netCommissionTotal' => ''
                                    ), true
							),
							'id' => 'new-executed-contracts-tab',
						),
						'Fall-outs (' . $FalloutDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($FalloutVolume) => array(
							'content' => $this->renderPartial('_closings', array(
									'DataProvider' => $FalloutDataProvider,
									'gridName' => 'fallout',
                                    'gciTotal' => $FalloutGCI,
									'volumeTotal' => $FalloutVolume,
                                    'agentCommissionTotal' => '',
                                    'netCommissionTotal' => ''
                                    ), true
							),
							'id' => 'fall-outs-tab',
						),
					),
					'id' => 'closings-tracking-widget',
				)
			);
		?>
	</div>
<?php $this->endWidget();