<? Yii::app()->clientScript->registerCss('tracking-closings-summary-css', <<<CSS
    table.closings-summary {
        margin-top: 20px !important;
    }
    .month-total th, .summary-closed-total th, .summary-pending-total th {
        background-image: none;
        background: #A9ECEC;
        border-bottom: 2px solid black;
    }
    .month-total-header th, .summary-closed-total-header th, .summary-pending-total-header th{
        border-top: 2px solid #666;
    }
    .quarter-total th, .summary-total th {
        background-image: none;
        background: #A9ECEC;
        border-top: 2px solid black;
        border-bottom: 2px solid black;
    }
    .summary-header th{
        border-top: 2px solid black;
    }
   .month-total-header th, .summary-closed-total-header th, .summary-pending-total-header th{
        background: #EDEDED;
    }
    .summary-total th{
        border-top: 2px solid black;
        border-bottom: 2px solid black;
    }
    .summary-header th:first-child, .quarter-total th:first-child, .month-total th:first-child, .month-sellers td:first-child, .month-buyers td:first-child, .summary-total th:first-child, .summary-pending-total th:first-child, .summary-closed-total th:first-child, .summary-sellers-total td:first-child, .summary-buyers-total td:first-child, .month-total-header th:first-child, .summary-closed-total-header th, .summary-pending-total-header th {
        border-left: 2px solid black;
    }
    .summary-header th:last-child, .quarter-total th:last-child, .month-total th:last-child, .month-sellers td:last-child, .month-buyers td:last-child, .summary-total th:last-child, .summary-pending-total th:last-child, .summary-closed-total th:last-child, .summary-sellers-total td:last-child, .summary-buyers-total td:last-child, .month-total-header th:last-child, .summary-closed-total-header th, .summary-pending-total-header th {
        border-right: 2px solid black;
    }
    /*.summary-pending-total th {*/
        /*border-bottom: 2px solid black;*/
    /*}*/
    .divider td{
        height: 40px;
        border: none;
        background-image: none;
    }
    .year {
        padding: 1px 8px;
        font-size: 24px;
        height: auto;
    }
CSS
);
$summaryHeader =
    '<tr class="summary-header">
        <th>Quarter</th>
        <th>Month</th>
        <th>Type</th>
        <th>Status</th>
        <th>Units</th>
        <th>Volume</th>
        <th>Gross Commission</th>
        <th>Agent Commission</th>
        <th>Internal Referral $</th>
        <th>Commission Subtotal</th>
        <th>Avg Sales Price</th>
        <th>Avg Commission $</th>
        <th>Avg Commission %</th>
        <th>Net External Referrals</th>
        <th>Net Commission</th>
    </tr>';
?>
<?php $this->breadcrumbs = array(
    'Closings Executive Summary' => ''
);
?>
<h1>Closings Executive Summary Report</h1>
<h1 class="p-tc">Year: <?=date('Y')?><?//=CHtml::dropDownList('year', date('Y'), StmFormHelper::getDropDownList(array('start'=>2015, 'end'=>date('Y'))), $htmlOptions=array('class'=>'year'))?></h1>
<table class="closings-summary">
    <?
    foreach($ClosingsSummaryData as $month => $row) {
        if(in_array($month, array(1, 4, 7, 10))) {
            echo $summaryHeader;
        }
        ?>
        <tr class="month-total-header">
            <th colspan="15"><?=date('M', strtotime($month.'/1/2000'))?></th>
        </tr>
        <? if($PendingsSummaryData[$month][ComponentTypes::SELLERS]['units']):?>
        <tr class="month-sellers">
            <td></td>
            <td></td>
            <td>Sellers</td>
            <td>Pending</td>
            <td><?=$PendingsSummaryData[$month][ComponentTypes::SELLERS]['units']?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['volume'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['grossCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['agentCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['internalReferralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['netCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['averagePrice'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['avgCommission'])?></td>
            <td><?=(($PendingsSummaryData[$month][ComponentTypes::SELLERS]['grossCommission'])? round($row[ComponentTypes::SELLERS]['grossCommission'] / $PendingsSummaryData[$month][ComponentTypes::SELLERS]['volume'] * 100, 2)."%" : "")?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['referralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::SELLERS]['totalCommission'])?></td>
        </tr>
        <? endif; ?>
        <tr class="month-sellers">
            <td></td>
            <td></td>
            <td>Sellers</td>
            <td>Closed</td>
            <td><?=$row[ComponentTypes::SELLERS]['units']?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['volume'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['grossCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['agentCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['internalReferralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['netCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['averagePrice'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['avgCommission'])?></td>
            <td><?=(($row[ComponentTypes::SELLERS]['grossCommission'])? round($row[ComponentTypes::SELLERS]['grossCommission'] / $row[ComponentTypes::SELLERS]['volume'] * 100, 2)."%" : "")?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['referralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::SELLERS]['totalCommission'])?></td>
        </tr>
        <? if($PendingsSummaryData[$month][ComponentTypes::BUYERS]['units']):?>
        <tr class="month-buyers">
            <td></td>
            <td></td>
            <td>Buyers</td>
            <td>Pending</td>
            <td><?=$PendingsSummaryData[$month][ComponentTypes::BUYERS]['units']?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['volume'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['grossCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['agentCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['internalReferralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['netCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['averagePrice'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['avgCommission'])?></td>
            <td><?=(($PendingsSummaryData[$month][ComponentTypes::BUYERS]['grossCommission'])? round($row[ComponentTypes::BUYERS]['grossCommission'] / $PendingsSummaryData[$month][ComponentTypes::BUYERS]['volume'] * 100, 2)."%": "")?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['referralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($PendingsSummaryData[$month][ComponentTypes::BUYERS]['totalCommission'])?></td>
        </tr>
        <? endif; ?>
        <tr class="month-buyers">
            <td></td>
            <td></td>
            <td>Buyers</td>
            <td>Closed</td>
            <td><?=$row[ComponentTypes::BUYERS]['units']?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['volume'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['grossCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['agentCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['internalReferralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['netCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['averagePrice'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['avgCommission'])?></td>
            <td><?=(($row[ComponentTypes::BUYERS]['grossCommission'])? round($row[ComponentTypes::BUYERS]['grossCommission'] / $row[ComponentTypes::BUYERS]['volume'] * 100, 2)."%": "")?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['referralCommission'])?></td>
            <td><?=Yii::app()->format->formatDollars($row[ComponentTypes::BUYERS]['totalCommission'])?></td>
        </tr>
        <tr class="month-total">
            <th colspan="3"><?=date('M', strtotime($month.'/1/2000'))?> Total</th>
            <th></th>
            <th><?=($monthlyUnitTotals = $row['total']['units'] + $PendingsSummaryData[$month]['total']['units']) ? $monthlyUnitTotals : ""?></th>
            <th><?=Yii::app()->format->formatDollars($row['total']['volume'] + $PendingsSummaryData[$month]['total']['volume'])?></th>
            <th><?=Yii::app()->format->formatDollars($row['total']['grossCommission'] + $PendingsSummaryData[$month]['total']['grossCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($row['total']['agentCommission'] + $PendingsSummaryData[$month]['total']['agentCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($row['total']['internalReferralCommission'] + $PendingsSummaryData[$month]['total']['internalReferralCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($row['total']['netCommission'] + $PendingsSummaryData[$month]['total']['netCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars(($monthlyUnitTotals) ? ($row['total']['volume'] + $PendingsSummaryData[$month]['total']['volume']) / $monthlyUnitTotals : 0)?></th>
            <th><?
                // get the proportionate weight of closed vs. pendings to calculate
                $monthlyAvgCommission = (($monthlyUnitTotals) ? $row['total']['avgCommission'] * $row['total']['units'] / $monthlyUnitTotals : 0) + (($monthlyUnitTotals) ? $PendingsSummaryData[$month]['total']['avgCommission'] * $PendingsSummaryData[$month]['total']['units'] / $monthlyUnitTotals : 0);
                echo Yii::app()->format->formatDollars($monthlyAvgCommission)?></th>
            <th><?=(($row['total']['grossCommission'] || $PendingsSummaryData[$month]['total']['grossCommission']) ? round(($row['total']['grossCommission'] + $PendingsSummaryData[$month]['total']['grossCommission']) / ($row['total']['volume'] + $PendingsSummaryData[$month]['total']['volume']) * 100, 2)."%" : "")?></th>
            <th><?=Yii::app()->format->formatDollars($row['total']['referralCommission'] + $PendingsSummaryData[$month]['total']['referralCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($row['total']['totalCommission'] + $PendingsSummaryData[$month]['total']['totalCommission'])?></th>
        </tr>
        <?if($month%3==0):?>
        <tr class="quarter-total">
            <th>Q<?=date('m', strtotime($month.'/1/2000'))/3?> Total</th>
            <th></th>
            <th>Total</th>
            <th></th>
            <th><?=$quarterUnitTotals = $ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['units'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['units']?></th>
            <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['volume'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['volume'])?></th>
            <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['grossCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['grossCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['agentCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['agentCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['internalReferralCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['internalReferralCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['netCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['netCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars(($quarterUnitTotals) ? (($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['volume'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['volume']) / $quarterUnitTotals) : 0)?></th>
            <th><? $quarterAvgCommission = (($quarterUnitTotals) ? $ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['avgCommission'] * $ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['units'] / $quarterUnitTotals : 0) + (($quarterUnitTotals) ? $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['avgCommission'] * $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['units'] / $quarterUnitTotals : 0);

                echo Yii::app()->format->formatDollars($quarterAvgCommission);
                ?>
            </th>
            <th><?=(($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['volume']) ? round(($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['grossCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['grossCommission']) / ($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['volume'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['volume']) * 100, 2)."%" : "")?></th>
            <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['referralCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['referralCommission'])?></th>
            <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['q'.($month/3)]['totalCommission'] + $PendingsSummaryQuarterAnnualTotals['q'.($month/3)]['totalCommission'])?></th>
        </tr>
        <tr class="divider"><td colspan="11"></td></tr>
        <?endif;?>
    <? } ?>
    <tr class="summary-header">
        <th></th>
        <th></th>
        <th>Type</th>
        <th>Status</th>
        <th>Units</th>
        <th>Volume</th>
        <th>Gross Commission</th>
        <th>Agent Commission</th>
        <th>Internal Referral $</th>
        <th>Commission Subtotal</th>
        <th>Avg Sales Price</th>
        <th>Avg Commission $</th>
        <th>Avg Commission %</th>
        <th>Net External Referrals</th>
        <th>Net Commission</th>
    </tr>
    <tr class="summary-closed-total-header">
        <th colspan="15">Annual Closed</th>
    </tr>
    <tr class="summary-sellers-total">
        <td></td>
        <td></td>
        <td>Sellers</td>
        <td>Closed</td>
        <td><?=$ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units']?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['agentCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['internalReferralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['netCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['averagePrice'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['avgCommission'])?></td>
        <td><?=(($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission']) ? round($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'] / $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'] * 100, 2)."%" : "")?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['referralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['totalCommission'])?></td>
    </tr>
    <tr class="summary-sellers-total">
        <td></td>
        <td></td>
        <td>Buyers</td>
        <td>Closed</td>
        <td><?=$ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units']?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['agentCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['interalReferralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['netCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['averagePrice'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['avgCommission'])?></td>
        <td><?=(($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission']) ? round($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'] / $ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'] * 100, 2)."%" : "")?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['referralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['totalCommission'])?></td>
    </tr>
    <tr class="summary-closed-total">
        <th colspan="3">Annual Closed Total</th>
        <th></th>
        <th><?=$ClosingsSummaryQuarterAnnualTotals['annual']['total']['units']?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['volume'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['agentCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['internalReferralCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['netCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['averagePrice'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['avgCommission'])?></th>
        <th><?=(($ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission']) ? round($ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] / $ClosingsSummaryQuarterAnnualTotals['annual']['total']['volume'] * 100, 2)."%" : "")?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['referralCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['totalCommission'])?></th>
    </tr>
    <tr class="summary-pending-total-header">
        <th colspan="15">Annual Pending</th>
    </tr>
    <tr class="summary-buyers-total">
        <td></td>
        <td></td>
        <td>Buyers</td>
        <td>Pending</td>
        <td><?=$PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['units']?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['agentCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['interalReferralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['netCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['averagePrice'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['avgCommission'])?></td>
        <td><?=(($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission']) ? round($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['grossCommission'] / $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['volume'] * 100, 2)."%" : "")?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['referralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::BUYERS]['totalCommission'])?></td>
    </tr>
    <tr class="summary-sellers-total">
        <td></td>
        <td></td>
        <td>Sellers</td>
        <td>Pending</td>
        <td><?=$PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['units']?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['agentCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['internalReferralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['netCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['averagePrice'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['avgCommission'])?></td>
        <td><?=(($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission']) ? round($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['grossCommission'] / $PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['volume'] * 100, 2)."%" : "")?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['referralCommission'])?></td>
        <td><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual'][ComponentTypes::SELLERS]['totalCommission'])?></td>
    </tr>
    <tr class="summary-pending-total">
        <th colspan="3">Annual Pending Total</th>
        <th></th>
        <th><?=$PendingsSummaryQuarterAnnualTotals['annual']['total']['units']?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['volume'])?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['agentCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['internalReferralCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['netCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['averagePrice'])?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['avgCommission'])?></th>
        <th><?=(($PendingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission']) ? round($PendingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] / $PendingsSummaryQuarterAnnualTotals['annual']['total']['volume'] * 100, 2)."%" : "")?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['referralCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($PendingsSummaryQuarterAnnualTotals['annual']['total']['totalCommission'])?></th>
    </tr>
    <tr class="summary-total">
        <th colspan="4">Annual Total</th>
        <th><?=$annualTotalUnits = $ClosingsSummaryQuarterAnnualTotals['annual']['total']['units'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['units']?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['volume'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['volume'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['agentCommission'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['agentCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['internalReferralCommission'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['internalReferralCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['netCommission'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['netCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars(($annualTotalUnits) ? ($ClosingsSummaryQuarterAnnualTotals['annual']['total']['volume'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['volume']) / $annualTotalUnits : 0)?></th>
        <th><? $annualAvgCommission = (($annualTotalUnits) ? $ClosingsSummaryQuarterAnnualTotals['annual']['total']['avgCommission'] * $ClosingsSummaryQuarterAnnualTotals['annual']['total']['units'] / $annualTotalUnits : 0) + (($annualTotalUnits) ? $PendingsSummaryQuarterAnnualTotals['annual']['total']['avgCommission'] * $PendingsSummaryQuarterAnnualTotals['annual']['total']['units'] / $annualTotalUnits : 0);
            echo Yii::app()->format->formatDollars($annualAvgCommission)?></th>
        <th><?=(($ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission']) ? round(($ClosingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['grossCommission']) / ($ClosingsSummaryQuarterAnnualTotals['annual']['total']['volume'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['volume']) * 100, 2)."%" : "")?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['referralCommission'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['referralCommission'])?></th>
        <th><?=Yii::app()->format->formatDollars($ClosingsSummaryQuarterAnnualTotals['annual']['total']['totalCommission'] + $PendingsSummaryQuarterAnnualTotals['annual']['total']['totalCommission'])?></th>
    </tr>
</table>
