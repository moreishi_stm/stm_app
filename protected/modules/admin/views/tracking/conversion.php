<?php
	Yii::app()->clientScript->registerScript('tracking-tasks-assignment-script', '
		$("#assignment_type_id, #contact_id").change(function(){
			$.fn.yiiGridView.update("conversion-grid", { data: $(this).serialize() });
			return false;
		});
	');

	$form=$this->beginWidget('CActiveForm', array(
											'id'=>'conversion-form',
											'action'=>'',
											'method'=>'get',
											));

?>
<div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'conversion-grid','isForm'=>false,'formSelector'=>'#conversion-form')); ?>
</div>
<?php $this->breadcrumbs = array(
	'Conversion' => '',
);
?>
<h1>Conversion Report</h1>
<h3 id="date-range-label">
    <label id="from-date-label" style="font-weight: bold;">
        <?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?>
    </label>
</h3>

<div class="p-fr p-pb10 g3">
	<?php echo CHtml::dropDownList('assignment_type_id', $defaultAssignmentTypes, CHtml::listData(AssignmentTypes::model()->byActiveAdmin()->findAll(), 'id', 'display_name'),array('empty'=>'Everyone','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Roles'));
	?>
</div>
<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#assignment_type_id, select#contact_id',)); ?>
<div class="p-fr p-pb10 g2">
    <?php echo CHtml::dropDownList('contact_id',null,CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'),array('empty'=>'Everyone','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Name'));
    ?>
</div>
<?php $this->widget('admin_module.components.StmGridView', array(
                                                           'id'            =>'conversion-grid',
                                                           'dataProvider'  =>$DataProvider,
														   'template'	   =>'{items}',
														   'extraParams'   =>array('dateRange'=>$dateRange),
                                                           'itemsCssClass' =>'datatables',
                                                           'columns' => array(
	                                                           'fullName',
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Lead Gen Hours',
		                                                           'value' => '$data->getActualLeadGen($this->grid->extraParams["dateRange"])',
		                                                           'htmlOptions' => array('style'=>'width:65px;'),
	                                                           ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Dials',
                                                                   'value' => '$data->getCountDials($this->grid->extraParams["dateRange"])',
                                                                   'htmlOptions' => array('style'=>'width:40px;'),
                                                               ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Dials / Hour',
                                                                   'value' => '$data->getDialsPerHour($this->grid->extraParams["dateRange"])',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Contacts New & Follow-up',
                                                                   'value' => '"<div class=\"g4\">".$data->getCountNewContacts($this->grid->extraParams["dateRange"])."</div><div class=\"g4\"> + </div><div class=\"g4\">".$data->getCountFollowupContacts($this->grid->extraParams["dateRange"])."</div>"',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
//	                                                           array(
//		                                                           'type'  => 'raw',
//		                                                           'name'  => 'New Contacts',
//		                                                           'value' => '$data->getCountNewContacts($this->grid->extraParams["dateRange"])',
//		                                                           'htmlOptions' => array('style'=>'width:55px;'),
//	                                                           ),
//															   array(
//																   'type'  => 'raw',
//																   'name'  => 'Follow-up Contacts',
//		                                                           'value' => '$data->getCountFollowupContacts($this->grid->extraParams["dateRange"])',
//																   'htmlOptions' => array('style'=>'width:65px;'),
//															   ),
															   array(
																   'type'  => 'raw',
																   'name'  => 'Contacts Total',
		                                                           'value' => '"<a href=\"/admin/tracking/activities/".$data->id."\" target=\"_blank\">".$data->getCountTotalContacts($this->grid->extraParams["dateRange"])."</a>"',
																   'htmlOptions' => array('style'=>'width:55px;'),
															   ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Dials / Contacts',
                                                                   'value' => '$data->getDialsPerContact($this->grid->extraParams["dateRange"])',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
															   array(
																   'type'  => 'raw',
																   'name'  => 'Ask Referral',
																   'value' => '$data->getCountAskForReferrals($this->grid->extraParams["dateRange"])',
																   'htmlOptions' => array('style'=>'width:55px;'),
															   ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Opportunities',
                                                                   'value' => '$data->getCountOpportunities($this->grid->extraParams["dateRange"])',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Appt Set',
                                                                   'value' => '$data->getCountSetAppointments($this->grid->extraParams["dateRange"])',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Appoin\'t Set For & Met',
                                                                   'value' => '"<div class=\"g4\">".$data->getCountSetForAppointments($this->grid->extraParams["dateRange"])."</div><div class=\"g3\"> / </div><div class=\"g4\">".$data->getCountMetAppointments($this->grid->extraParams["dateRange"])."</div>"',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
															   array(
																   'type'  => 'raw',
																   'name'  => '% Appts Set-Met (exclude future appts)',
																   'value' => '$data->getPercentageSetToMetAppointments($this->grid->extraParams["dateRange"])',
																   'htmlOptions' => array('style'=>'width:55px;'),
															   ),
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Contracts Executed',
		                                                           'value' => '((Yii::app()->controller->module->id!=="hq")?$data->getCountExecutedContracts($this->grid->extraParams["dateRange"]):"")',
		                                                           'htmlOptions' => array('style'=>'width:55px;'),
	                                                           ),
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Closings',
		                                                           'value' => '((Yii::app()->controller->module->id!=="hq")?$data->getCountClosings($this->grid->extraParams["dateRange"]):"")',
		                                                           'htmlOptions' => array('style'=>'width:55px;'),
	                                                           ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Contacts / Hour',
                                                                   'value' => '$data->getContactsPerHour($this->grid->extraParams["dateRange"])',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
															   array(
																   'type'  => 'raw',
																   'name'  => 'Contacts /Set Appt',
																   'value' => '$data->getContactsPerSetAppointment($this->grid->extraParams["dateRange"])',
																   'htmlOptions' => array('style'=>'width:60px;'),
															   ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Hours / Set Appt',
                                                                   'value' => '$data->getHoursPerSetAppointment($this->grid->extraParams["dateRange"])',
                                                                   'htmlOptions' => array('style'=>'width:60px;'),
                                                               ),
//															   array(
//																   'type'  => 'raw',
//																   'name'  => 'Met Appts',
//																   'value' => '$data->getCountMetAppointments($this->grid->extraParams["dateRange"])',
//																   'htmlOptions' => array('style'=>'width:60px;'),
//															   ),
															   array(
																   'type'  => 'raw',
																   'name'  => 'Signed Agrmts / Met',
																   'value' => '"<div class=\"g3\">".$data->getCountSignedAppointments($this->grid->extraParams["dateRange"])."/</div><div class=\"g3\">".$data->getCountMetAppointments($this->grid->extraParams["dateRange"])."</div><div class=\"g6\">(".Yii::app()->format->formatPercentages($data->getCountSignedAppointmentsPercentage($this->grid->extraParams["dateRange"]),array("decimalPlaces"=>0)).")</div>"',
																   'htmlOptions' => array('style'=>'width:75px;'),
															   ),
//															   array(
//																   'type'  => 'raw',
//																   'name'  => 'Appt/Sales',
//																   'value' => '$data->getAppointmentsPerSale($this->grid->extraParams["dateRange"])',
//																   'htmlOptions' => array('style'=>'width:60px;'),
//															   ),
                                                           ),
                                                           ));
	$this->endWidget();