<?php
Yii::app()->clientScript->registerScript('agentAccountability-script', '
		$("#assignment_type_id, #contact_id").change(function(){
			$.fn.yiiGridView.update("accountability-grid", { data: $(this).serialize() });
			return false;
		});
	');
?>

<div class="g12" style="position: absolute; top:2px;">
    <?php $this->widget(
        'admin_module.components.widgets.DateRanger.DateRanger', array('fromDateLabelSelector' => '#from-date-label',
                                                                       'toDateLabelSelector'   => '#to-date-label',
                                                                       'gridName'              => 'accountability-grid',
                                                                       'defaultSelect'         => 'this_month',
        )
    ); ?>
</div>
<?php
$this->breadcrumbs=array(
    'No Activity'=>'',
);
?>
<h1>No Activity</h1>
<h3 id="date-range-label">
    <label id="from-date-label" style="font-weight: bold;">
        <?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?>
    </label>
</h3>
<div class="p-fr p-pb10 g2">
    <?php echo CHtml::dropDownList('assignment_type_id',null,CHtml::listData(AssignmentTypes::model()->byActiveAdmin()->findAll(), 'id', 'display_name'),array('empty'=>'Everyone','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Roles'));
    ?>
</div>
<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#assignment_type_id, select#contact_id',)); ?>
<div class="p-fr p-pb10 g2">
    <?php echo CHtml::dropDownList('contact_id',null,CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'),array('empty'=>'Everyone','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Name'));
    ?>
</div>
<?php echo $this->renderPartial('../transactions/_listBuyers',array('DataProvider'=>$dataProvider)); ?>