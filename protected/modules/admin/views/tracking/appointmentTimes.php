<?php
// Files needed for Sales graph
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');

Yii::app()->clientScript->registerScript('appointmentTimesScript', <<<JS

    $('#appointment-set-times-graph').wl_Chart({
        flot: {
            bars:{
                align: "center",
                lineWidth: 0,
                fillColor: {
                    colors: [ { opacity: .7 }, { opacity: .7 } ]
                }
            },
            yaxis: {
                tickDecimals: 0,
                max: $maxPercent,
                axisLabel: '%',
                tickFormatter: function (val, axis) {
                    return val + "%";
                }
            },
            xaxis: {
                tickLength: 0,
                ticks:[
                    [0,'6am'],
                    [1,'7am'],
                    [2,'8am'],
                    [3,'9am'],
                    [4,'10am'],
                    [5,'11am'],
                    [6,'12pm'],
                    [7,'1pm'],
                    [8,'2pm'],
                    [9,'3pm'],
                    [10,'4pm'],
                    [11,'5pm'],
                    [12,'6pm'],
                    [13,'7pm'],
                    [14,'8pm'],
                    [15,'9pm'],
                    [16,'10pm'],
                    [17,'11pm']
                ]
            },
            legend: {position: "nw", noColumns: 7}
        },
        //colors: ["#FF0000","#FFFF00","#00FF00"],
        type: 'bars',
                barWidth: 0.8,
        // xlabels: [""],//$xLabels, // ['1','2','3']
        data:[
            {
                color: "#528EFF",//"#528EFF", //FFAB4A
                data: [ $dataString ]
            }
        ]
    });

    $('select#set_by_id, select#component_type_id').on('change', function(){
        $('#appointment-times-form').submit();
    });

    $('form#appointment-times-form').submit(function(){
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
    });
JS
);

//$css = <<<CSS
//CSS;
//Yii::app()->clientScript->registerCss('appointmentsTimeofDayTrackingCss', $css);

$this->breadcrumbs = array(
    'Appointment Times' => '',
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'appointment-times-form',
        'action' => '/admin/tracking/appointmentTimes',
        'method' => 'GET',
    )
);
?>

<div class="p-fr date-ranger-container" style="min-width: 200px; width: inherit;">
    <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
            'fromDateLabelSelector' => '#from-date-label',
            'toDateLabelSelector' => '#to-date-label',
            'isForm' => false,
            'gridName' => null,
            'formSelector' => '#appointment-times-form',
            'defaultSelect' => $dateRangerSelect
        )
    ); ?>

</div>
<div class="g2 p-fr">
    <?php echo CHtml::dropDownList('component_type_id', $_GET['component_type_id'], CHtml::listData(ComponentTypes::model()->byIds(array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS))->findAll(), 'id', 'display_name'),array('empty'=>'','class'=>'chzn-select g12','data-placeholder'=>'Default Sellers & Buyers'));
    ?>
</div>
<div class="g3 p-fr">
    <?php echo CHtml::dropDownList('set_by_id', $_GET['set_by_id'],CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), array('empty'=>'','class'=>'chzn-select g12','data-placeholder'=>'Select Name'));
    $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select', 'options' => array('allow_single_deselect' =>true)));
    ?>
</div>
<? $this->endWidget(); ?>


<div style="padding: 10px; clear: both; width: 100%;">
    <h1>Appointments Set Time of Day</h1>

    <h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label></h3>

    <table id="appointment-set-times-graph" class="chart"   data-height="400" data-tooltip="false" width="75%"></table>
    <h2><br></h2>
</div>