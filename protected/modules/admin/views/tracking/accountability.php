<?php
Yii::app()->clientScript->registerScript('agentAccountability-script', '
		$("#assignment_type_id, #contact_id").change(function(){
			$.fn.yiiGridView.update("accountability-grid", { data: $(this).serialize() });
			return false;
		});
	');
?>

<div class="g12" style="position: absolute; top:2px;">
    <?php $this->widget(
        'admin_module.components.widgets.DateRanger.DateRanger', array('fromDateLabelSelector' => '#from-date-label',
                                                                       'toDateLabelSelector'   => '#to-date-label',
                                                                       'gridName'              => 'accountability-grid',
                                                                       'defaultSelect'         => 'this_month',
        )
    ); ?>
</div>
<?php
$this->breadcrumbs=array(
    'Agent Accountability'=>'',
);
?>
<h1>1st Responder Ranking</h1>
<h3 id="date-range-label">
    <label id="from-date-label" style="font-weight: bold;">
        <?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?>
    </label>
</h3>
<div class="p-fr p-pb10 g2">
    <?php echo CHtml::dropDownList('assignment_type_id',null,CHtml::listData(AssignmentTypes::model()->byActiveAdmin()->findAll(), 'id', 'display_name'),array('empty'=>'Everyone','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Roles'));
    ?>
</div>
<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#assignment_type_id, select#contact_id',)); ?>
<div class="p-fr p-pb10 g2">
    <?php echo CHtml::dropDownList('contact_id',null,CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'),array('empty'=>'Everyone','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Name'));
    ?>
</div>
<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'accountability-grid',
        'dataProvider' => $DataProvider,
        'template'	   =>'{items}',
        'itemsCssClass' => 'datatables',
        'extraParams'   =>array('dateRange'=>$dateRange, 'newLeadCount'=>null),
        'columns' => array(
            'fullName',
            array(
                'type' => 'raw',
                'name' => 'Last Login',
                'value' => 'Yii::app()->format->formatDays($data->last_login, array("nearestMinHourDays"=>true,"nearestMinHourDays"=>true))." ago"',
            ),
            array(
                'type' => 'raw',
                'name' => 'New Leads',
                'value' => 'CHtml::link(Yii::app()->controller->action->printCountAndPercent($this->grid->extraParams["newLeadCount"] = $data->getCountLeadsByDateRange($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"]),"/admin/buyers/list/assignmentId/".$data->id."/dateSearchFilter/added_desc/includeTrash/1/fromSearchDate/".$this->grid->extraParams["dateRange"]["from_date"]."/toSearchDate/".$this->grid->extraParams["dateRange"]["to_date"], $htmlOptions=array("target"=>"_blank"))',
                'htmlOptions'=> array('style'=>'width: 8%'),
            ),
            array(
                'type' => 'raw',
                'name' => 'All Leads',
                'value' => '$data->getCountLeads()',
                'htmlOptions'=> array('style'=>'width: 5%'),
            ),
            array(
                'type' => 'raw',
                'name' => 'No Saved Searches',
                'value' => 'CHtml::link(Yii::app()->controller->action->printCountAndPercent($data->getCountNoSavedSearches($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"]),"/admin/buyers/list/searchNoSavedSearches/1/assignmentId/".$data->id."/dateSearchFilter/added_desc/includeTrash/0/fromSearchDate/".$this->grid->extraParams["dateRange"]["from_date"]."/toSearchDate/".$this->grid->extraParams["dateRange"]["to_date"], $htmlOptions=array("target"=>"_blank"))',
                'htmlOptions'=> array('style'=>'width: 10%'),
            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Response Time by Phone / Email',
//                'value' => 'TBD', //'Yii::app()->controller->action->printCountAndPercent($data->getCountCalledLeads($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"])',
//                'htmlOptions'=> array('style'=>'width: 10%'),
//            ),
            array(
                'type' => 'raw',
                'name' => 'No Activity',
                'value' => 'CHtml::link(Yii::app()->controller->action->printCountAndPercent($data->getCountNoActivityLeads($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"]),"/admin/buyers/list/searchNoActivity/1/assignmentId/".$data->id."/dateSearchFilter/added_desc/includeTrash/0/fromSearchDate/".$this->grid->extraParams["dateRange"]["from_date"]."/toSearchDate/".$this->grid->extraParams["dateRange"]["to_date"], $htmlOptions=array("target"=>"_blank"))',
            ),
            array(
                'type' => 'raw',
                'name' => 'No Emails',
                'value' => 'Yii::app()->controller->action->printCountAndPercent($data->getCountNoEmailsLeads($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"])',
            ),
            array(
                'type' => 'raw',
                'name' => 'No Calls',
                'value' => 'CHtml::link(Yii::app()->controller->action->printCountAndPercent($data->getCountNotCalledLeads($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"]),"/admin/buyers/list/searchNoCalls/1/assignmentId/".$data->id."/dateSearchFilter/added_desc/includeTrash/0/fromSearchDate/".$this->grid->extraParams["dateRange"]["from_date"]."/toSearchDate/".$this->grid->extraParams["dateRange"]["to_date"], $htmlOptions=array("target"=>"_blank"))',
            ),
            array(
                'type' => 'raw',
                'name' => 'Called',
                'value' => 'Yii::app()->controller->action->printCountAndPercent($data->getCountCalledLeads($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"])',
            ),
            array(
                'type' => 'raw',
                'name' => 'Leads Spoke To',
                'value' => 'Yii::app()->controller->action->printCountAndPercent($data->getCountSpokeToLeads($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"])',
            ),

            array(
                'type' => 'raw',
                'name' => 'New Lead Appts Set',
                'value' => 'Yii::app()->controller->action->printCountAndPercent($data->getCountLeadSetAppointment($this->grid->extraParams["dateRange"]), $this->grid->extraParams["newLeadCount"])',
            ),
            array(
                'type' => 'raw',
                'name' => 'Total Appts Set',
                'value' => '$data->getCountSetAppointments($this->grid->extraParams["dateRange"])',
            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Appts Met # (%)',
//                'value' => '$data->getActualLeadGen($this->grid->extraParams["dateRange"])', //# and %
//            ),
        ),
    )
);