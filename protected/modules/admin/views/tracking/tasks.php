<?php
	Yii::app()->clientScript->registerScript('tracking-tasks-assignment-script', '
		$("#activity-insights-form #assignment_type_id").change(function(){
			$.fn.yiiGridView.update("activity-insights-grid", { data: $(this).serialize() });
			return false;
		});
	');

	$form=$this->beginWidget('CActiveForm', array(
											'id'=>'activity-insights-form',
											'action'=>'',
											'method'=>'get',
											));

?>
<style type="text/css">
	.datatables td:not(:first-child), .datatables th:not(:first-child) {
		text-align: center;
	}
</style>

<div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('fromDateLabelSelector'=>'#from-date-label',
																				   'toDateLabelSelector'=>'#to-date-label',
																				   'gridName'=>'activity-insights-grid',
																				   'isForm'=>false,
																				   'formSelector'=>'#activity-insights-form',
																				   'defaultSelect' => 'month_to_date',
 )); ?>
</div>
<?php $this->breadcrumbs = array(
	'Tasks' => '',
);
?>
<h1>Activity Insights</h1>
<h3 id="date-range-label">
	<label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> -
	<label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label>
</h3>

<div class="p-fr p-pb10 g2">
	<?php echo CHtml::dropDownList('assignment_type_id',null,CHtml::listData(AssignmentTypes::model()->byActiveAdmin()->findAll(), 'id', 'display_name'),array('empty'=>'All','class'=>'chzn-select g12','multiple'=>'multiple','data-placeholder'=>'Select Roles'));
		$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#assignment_type_id'));
	?>
</div>
<?php $this->widget('admin_module.components.StmGridView', array(
                                                           'id'            =>'activity-insights-grid',
														   'template'	   =>'{items}',
                                                           'dataProvider'  =>$DataProvider,
														   'extraParams'   =>array('dateRange'=>$dateRange),
                                                           'itemsCssClass' =>'datatables',
                                                           'columns' => array(
	                                                           'fullName',
//	                                                           array(
//		                                                           'type'  => 'raw',
//		                                                           'name'  => 'New Follow-up',
//		                                                           'value' => '$data->getCountNewTasks($this->grid->extraParams["dateRange"])',
//		                                                           'htmlOptions' => array('style'=>'width:65px;'),
//	                                                           ),
//	                                                           array(
//		                                                           'type'  => 'raw',
//		                                                           'name'  => '+ Updated Follow-up',
//		                                                           'value' => '$data->getCountUpdatedTasks($this->grid->extraParams["dateRange"])',
//		                                                           'htmlOptions' => array('style'=>'width:75px;'),
//	                                                           ),
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'New/Updated Tasks',
		                                                           'value' => '$data->getScheduledFollowupTasks($this->grid->extraParams["dateRange"])',
		                                                           'htmlOptions' => array('style'=>'width:140px;'),
	                                                           ),
//                                                               array(
//                                                                   'type'  => 'raw',
//                                                                   'name'  => 'Completed Tasks',
//                                                                   'value' => '$data->getCountCompletedTasks($this->grid->extraParams["dateRange"])',
//                                                                   'htmlOptions' => array('style'=>'width:85px;'),
//                                                               ),
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Future Tasks',
		                                                           'value' => '"<strong>".$data->getCountFutureTasks($this->grid->extraParams["dateRange"])."</strong>"',
		                                                           'htmlOptions' => array('style'=>'width:90px;'),
	                                                           ),
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Overdue Tasks',
		                                                           'value' => '"<span style=\"color: #D20000; font-weight: bold;\">".$data->getCountOverdueTasks($this->grid->extraParams["dateRange"])."</span>"',
		                                                           'htmlOptions' => array('style'=>'width:120px;'),
	                                                           ),
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Activities Logged',
		                                                           'value' => '$data->getCountActivitiesLogged($this->grid->extraParams["dateRange"])',
		                                                           'htmlOptions' => array('style'=>'width:120px;'),
	                                                           ),
                                                               array(
                                                                   'type'  => 'raw',
                                                                   'name'  => 'Activities per Weekday',
                                                                   'value' => '"<strong>".$data->getCountActivitiesLoggedPerWeekday($this->grid->extraParams["dateRange"])."</strong>"',
                                                                   'htmlOptions' => array('style'=>'width:120px;'),
                                                               ),
                                                           ),
                                                           ));

$this->endWidget();
