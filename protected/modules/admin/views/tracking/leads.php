<div class="g12" style="position: absolute; top:2px;">
    <?php $this->widget(
        'admin_module.components.widgets.DateRanger.DateRanger', array('fromDateLabelSelector' => '#from-date-label',
                                                                       'toDateLabelSelector'   => '#to-date-label',
                                                                       'gridName'              => 'leads-grid',
                                                                       'defaultSelect'         => 'last_12_months',
                                                                       //																				   'isForm'=>false,
                                                                       //																				   'formSelector'=>'#activity-insights-form',
        )
    ); ?>
</div>
<?php $this->breadcrumbs = array(
    'Leads' => '',
);
?>
<h1><?php echo $leadType; ?> Leads Report</h1>
<h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date(
            "m/d/Y", strtotime($dateRange['from_date'])
        ); ?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date(
            "m/d/Y", strtotime($dateRange['to_date'])
        ); ?></label></h3>
<!--<h3 id="total-lead-count">Total Lead Count: --><?php //echo $totalCount; ?><!--</h3>-->
<?php $this->widget(
    'admin_module.components.StmGridView', array(
        'id'            => 'leads-grid',
        'dataProvider'  => $DataProvider,
        'itemsCssClass' => 'datatables',
        'columns'       => array(
            array(
                'type'        => 'raw',
                'name'        => 'Date',
                'value'       => '$data[dateLabel]',
                'htmlOptions' => array('style' => 'width:25%;'),
                'footer'      => '<span style="font-weight:bold;font-size:18px;">Total Count:</span>',
            ),
            array(
                'type'        => 'raw',
                'name'        => 'Buyer Leads',
                'value'       => '$data[buyers]',
                'htmlOptions' => array('style' => 'width:15%;'),
                'footer'      => '<span style="font-weight:bold;font-size:18px;">' . $buyerLeadCount . '</span>',
            ),
            array(
                'type'        => 'raw',
                'name'        => 'Buyer Leads %',
                'value'       => '$data[buyerLeadPercent]',
                'htmlOptions' => array('style' => 'width:15%;'),
                'footer'      => '<span style="font-weight:bold;font-size:18px;">'.(($buyerLeadCount)? Yii::app()->format->formatPercentages($buyerLeadCount/($buyerLeadCount+$sellerLeadCount)): '-').'</span>',
            ),
            array(
                'type'        => 'raw',
                'name'        => 'Seller Leads',
                'value'       => '$data[sellers]',
                'htmlOptions' => array('style' => 'width:15%;'),
                'footer'      => '<span style="font-weight:bold;font-size:18px;">' . $sellerLeadCount .'</span>',
            ),
            array(
                'type'        => 'raw',
                'name'        => 'Seller Leads %',
                'value'       => '$data[sellerLeadPercent]',
                'htmlOptions' => array('style' => 'width:15%;'),
                'footer'      => '<span style="font-weight:bold;font-size:18px;">' .(($sellerLeadCount)? Yii::app()->format->formatPercentages($sellerLeadCount/($buyerLeadCount+$sellerLeadCount)): '-').'</span>',
            ),
            array(
                'type'        => 'raw',
                'name'        => 'Count',
                'value'       => '$data[buyers]+$data[sellers]',
                'htmlOptions' => array('style' => 'width:15%;'),
                'footer'      => '<span style="font-weight:bold;font-size:18px;">' . $totalCount . '</span>',
            ),
        ),
    )
);