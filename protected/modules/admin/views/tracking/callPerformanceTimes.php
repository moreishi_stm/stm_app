<?php
// Files needed for graph
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/flot/flot-plugin-barNumbers-enhanced/jquery.flot.barnumbers.enhanced.min.js');
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/flot/flot-plugin-valuelabels/jquery.flot.valuelabels.js');

$tickTimes = "[0,'6am'],
              [1,'7am'],
              [2,'8am'],
              [3,'9am'],
              [4,'10am'],
              [5,'11am'],
              [6,'12pm'],
              [7,'1pm'],
              [8,'2pm'],
              [9,'3pm'],
              [10,'4pm'],
              [11,'5pm'],
              [12,'6pm'],
              [13,'7pm'],
              [14,'8pm'],
              [15,'9pm'],
              [16,'10pm'],
              [17,'11pm']";

$numbersBarScript = "";

Yii::app()->clientScript->registerScript('callTimesScript', <<<JS

    var tickTimes = "";
    createGraph("appointments-percent-times-graph", "$appointmentsPercentDataString", $appointmentsPercentMax, "%", true, 0);
    createGraph("spoke-tos-percent-times-graph", "$spokeToPercentDataString", $spokeToPercentMax, "%", true, 0);
    createGraph("appointments-times-graph", "$appointmentsDataString", $appointmentsMax, "", true, 0);
    createGraph("dials-times-graph", "$dialsDataString", $dialsMax, "", true, 0);
    createGraph("spoke-tos-times-graph", "$spokeTosDataString", $spokeTosMax, "", true, 0);

    function createGraph(containerId, dataString, maxValue, yLabel, showNumbers, yOffsetValue)
    {
        $('#' + containerId).wl_Chart({
            flot: {
                bars:{
                    numbers:{
                        show: showNumbers,
                        font: '7pt Verdana,Tahoma,Arial,Helvetica,sans-serif,"Comic Sans MS"',
                        fontColor: '#666',
                        yOffset: 4,
                        threshold: 1,
                        xOffset: -30,
                        yAlign: function(y) { return (y) ? y + yOffsetValue : null; }
                    },
                    align: "center",
                    lineWidth: 0,
                    fillColor: {
                        colors: [ { opacity: .7 }, { opacity: .7 } ]
                    }
                },
               yaxis: {
                    tickDecimals: 0,
                    max: maxValue,
                    axisLabel: yLabel,
                    tickFormatter: function (val, axis) {
                        return val + yLabel;
                    }
                },
                xaxis: {
                    tickLength: 0,
                    ticks:[ $tickTimes ]
                },
                legend: {position: "nw", noColumns: 7}
            },
            type: "bars",
                    barWidth: 0.8,
            data:[
                {
                    color: "#528EFF",
                    data: JSON.parse(dataString)
                }
            ]
        });
    }

    $('select#set_by_id, select#component_type_id').on('change', function(){
        $('#call-times-form').submit();
    });

    $('form#call-times-form').submit(function(){
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
    });
JS
);

//$css = <<<CSS
//CSS;
//Yii::app()->clientScript->registerCss('appointmentsTimeofDayTrackingCss', $css);

$this->breadcrumbs = array(
    'Call Perfomance' => '',
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'call-times-form',
        'action' => '/admin/tracking/'.Yii::app()->controller->action->id,
        'method' => 'GET',
    )
);
?>

<div class="p-fr date-ranger-container" style="min-width: 200px; width: inherit;">
    <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
            'fromDateLabelSelector' => '#from-date-label',
            'toDateLabelSelector' => '#to-date-label',
            'isForm' => false,
            'gridName' => null,
            'formSelector' => '#appointment-times-form',
            'defaultSelect' => $dateRangerSelect
        )
    ); ?>

</div>
<div class="g2 p-fr">
    <?php echo CHtml::dropDownList('component_type_id', $_GET['component_type_id'], CHtml::listData(ComponentTypes::model()->byIds(array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS))->findAll(), 'id', 'display_name'),array('empty'=>'','class'=>'chzn-select g12','data-placeholder'=>'Default Sellers & Buyers'));
    ?>
</div>
<div class="g3 p-fr">
    <?php echo CHtml::dropDownList('contact_id', $_GET['contact_id'],CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), array('empty'=>'','class'=>'chzn-select g12','data-placeholder'=>'Select Name'));
    $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select', 'options' => array('allow_single_deselect' =>true)));
    ?>
</div>
<? $this->endWidget(); ?>


<h1 id="date-range-label p-clr g12" style="clear:both; padding: 20px 0 0 0; font-size: 30px; line-height: 37px;">Calls Performance by Times Report</h1>
<h2 style="font-size: 24px; padding-bottom: 20px;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?>- <?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></h2>
<hr/>
<div style="padding: 10px; padding-bottom:40px; clear: both; width: 100%;">
    <h1>Appointments % (per Spoke To) by Time of Day</h1>
    <h1 style="padding: 0 0 20px 0; font-size: 15px;">NOTE: % Data with less than 2 counts and 100% values will be ignored due to insufficient sample size or outlier data.</h1>

    <table id="appointments-percent-times-graph" class="chart"   data-height="400" data-tooltip="false" width="75%"></table>
</div>
<hr/>
<div style="padding: 10px; padding-bottom:40px; clear: both; width: 100%;">
    <h1>Outbound Spoke To % (per Dial) by Time of Day</h1>
    <h1 style="padding: 0 0 20px 0; font-size: 15px;">NOTE: % Data with less than 10 counts and 100% values will be ignored due to insufficient sample size or outlier data.</h1>

    <table id="spoke-tos-percent-times-graph" class="chart"   data-height="400" data-tooltip="false" width="75%"></table>
</div>
<hr/>
<div style="padding: 10px; padding-bottom:40px; clear: both; width: 100%;">
    <h1>Appointments by Time of Day</h1>

    <table id="appointments-times-graph" class="chart"   data-height="400" data-tooltip="false" width="75%"></table>
</div>
<hr/>
<div style="padding: 10px; padding-bottom:40px; clear: both; width: 100%;">
    <h1>Outbound Dials by Time of Day</h1>

    <table id="dials-times-graph" class="chart"   data-height="400" data-tooltip="false" width="75%"></table>
</div>
<hr/>
<div style="padding: 10px; padding-bottom:40px; clear: both; width: 100%;">
    <h1>Outbound Spoke Tos by Time of Day</h1>

    <table id="spoke-tos-times-graph" class="chart"   data-height="400" data-tooltip="false" width="75%"></table>
</div>