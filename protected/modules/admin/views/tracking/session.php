<?php $this->breadcrumbs = array(
	'Traffic Tracker' => '/admin/tracking/trafficDetails',
	'Session: '.$model->session_id => '',
);
?>
<h1 style="padding-top: 20px;">Session Activity</h1>
<h3><?php echo $model->session_id;?></h3>
<?php $this->widget('admin_module.components.StmGridView', array(
                                                           'id'            =>'sessions-grid',
                                                           'dataProvider'  =>$model->search(),
                                                           'template'      =>'{pager}{summary}{items}{pager}{summary}',
                                                           'itemsCssClass' =>'datatables',
                                                           'columns' => array(
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Added / Contact',
		                                                           'value' => 'Yii::app()->format->formatDateTime($data->added,array("break"=>true))."<br />".$data->contact_id',
		                                                           'htmlOptions' => array('style'=>'width:75px;'),
	                                                           ),
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => '# Home Views / IP',
		                                                           'value' => '"# Home Views: ".$data->home_detail_view_count."<br />".$data->ip',
		                                                           'htmlOptions' => array('style'=>'width:125px;'),
	                                                           ),
	                                                           'utmz:googleAnalyticsCookie',
	                                                           array(
		                                                           'type'  => 'raw',
		                                                           'name'  => 'Domain / Page / Referrer',
		                                                           'value' => '$data->domain."<br />Page: ".wordwrap($data->page, 80, "<br />\n",true)."<br />Referrer: ".wordwrap($data->referrer, 80, "<br />\n",true)',
	                                                           ),
                                                           ),
                                                           ));