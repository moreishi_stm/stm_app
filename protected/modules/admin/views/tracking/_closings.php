<?php
    $this->widget('admin_module.components.StmGridView', array(
		'id' => $gridName . '-closings-grid',
		//'closings-grid',
		'dataProvider' => $DataProvider,
		'template' => '{items}',
		'extraParams' => array('volumeTotal' => $volumeTotal),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Status',
				'value' => '"<a href=\"/admin/closings/".$data->id."\" target=\"_blank\">".$data->getModelAttribute("getStatusTypes", $data->closing_status_ma)."</a>"',
			),
			array(
				'type' => 'raw',
				'name' => 'Type',
                'value' => '"<div><a href=\"/admin/".$data->transaction->componentType->name."/".$data->transaction->id."\" target=\"_blank\">".$data->transaction->componentType->singularName."</a></div>"',
			),
			array(
				'type' => 'raw',
				'name' => 'Contract Executed',
				'value' => 'Yii::app()->format->formatDate($data->contract_execute_date)',
				'htmlOptions' => array('style' => 'width:75px;'),
			),
			array(
				'type' => 'raw',
				'name' => 'Contract Closing Date',
				'value' => 'Yii::app()->format->formatDate($data->contract_closing_date)',
				'htmlOptions' => array('style' => 'width:85px;'),
                'footer' => '<span style="font-weight:bold;font-size:18px;float:right;">' . (($volumeTotal) ? 'Total: ' : '') . '</span>',
			),
            array(
                'type' => 'raw',
                'name' => 'Actual Closing Date',
                'value' => 'Yii::app()->format->formatDateTime($data->actual_closing_datetime)',
                'htmlOptions' => array('style' => 'width:85px;'),
                'footer' => '<span style="font-weight:bold;font-size:18px;">' . (($volumeTotal) ? $DataProvider->totalItemCount : '') . '</span>',
            ),
			array(
				'type' => 'raw',
				'name' => 'Price',
				'value' => 'Yii::app()->format->formatDollars($data->price)',
				'htmlOptions' => array('style' => 'width:85px;'),
                'footer' => '<span style="font-weight:bold;font-size:18px;float:right;">' . (($gciTotal && Yii::app()->user->checkAccess('owner')) ? 'GCI: ' : '') . '</span>',
			),
            array(
                'type' => 'raw',
                'name' => 'Gross Commission',
                'value' => 'Yii::app()->format->formatDollars($data->commission_amount)',
                'htmlOptions' => array('style' => 'width:85px;'),
                'footer' => '<span style="font-weight:bold;font-size:18px;">' . (($gciTotal && Yii::app()->user->checkAccess('owner')) ? Yii::app()->format->formatDollars($gciTotal) : '') . '</span>',
            ),
			array(
				'type' => 'raw',
				'name' => '% Commisson',
				'value' => '(($data->price) ? " ".round(($data->commission_amount/$data->price)*100,2)."%" : "")',
				'htmlOptions' => array('style' => 'width:48px;')
			),
            array(
                'type' => 'raw',
                'name' => 'Agent Commission Expense',
                'value' => 'Yii::app()->format->formatDollars($data->getAgentCommission())',
                'footer' => '<span style="font-weight:bold;font-size:18px;">' . (($agentCommissionTotal && Yii::app()->user->checkAccess('owner')) ? Yii::app()->format->formatDollars($agentCommissionTotal) : '') . '</span>',
                'htmlOptions' => array('style' => 'width:85px;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Referral $',
                'value' => 'Yii::app()->format->formatDollars($data->getAgentInternalReferralCommission())',
                'footer' => '<span style="font-weight:bold;font-size:18px;">' . (($internalReferralCommissionTotal && Yii::app()->user->checkAccess('owner')) ? Yii::app()->format->formatDollars($internalReferralCommissionTotal) : '') . '</span>',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Referral %',
                'value' => '($data->getAgentInternalReferralCommission() && $data->commission_amount) ? round( $data->getAgentInternalReferralCommission() / $data->commission_amount * 100)."%": ""',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Referral Type',
                'value' => '($data->getAgentInternalReferralCommission()) ? "Internal" : ""',
                'htmlOptions' => array('style' => 'width:50px;'),
            ),
			array(
				'type' => 'raw',
				'name' => 'Net Commission',
				'value' => 'Yii::app()->format->formatDollars($data->netCommission)',
                'footer' => '<span style="font-weight:bold;font-size:18px;">' . (($netCommissionTotal && Yii::app()->user->checkAccess('owner')) ? Yii::app()->format->formatDollars($netCommissionTotal) : '') . '</span>',
				'htmlOptions' => array('style' => 'width:85px;'),
			),
			array(
				'type' => 'raw',
				'name' => 'Source',
				'value' => '$data->transaction->source->name',
				'htmlOptions' => array('style' => 'width:85px;'),
			),
			array(
				'type' => 'raw',
				'name' => 'Name',
				'value' => '$data->contact->fullName',
				'footer' => '<span style="font-weight:bold;font-size:18px;float:right;">' . (($volumeTotal) ? 'Volume: ' : '') . '</span>',
			),
			array(
				'type' => 'raw',
				'name' => 'Address',
				'value' => '"<div><a href=\"/admin/closings/".$data->id."\" target=\"_blank\">".Yii::app()->format->formatAddress($data->transaction->address)."</a></div>"',
				'footer' => '<span style="font-weight:bold;font-size:18px;">' . Yii::app()->format->formatDollars($volumeTotal) . '</span>',
			),
			array(
				'type' => 'raw',
				'name' => 'Assignments',
				'value' => '"<div>".Yii::app()->format->formatGridValue("", $data->transaction->printGridviewAssignments())."</div>"',
			),
		),
	)
);