<?php
$module = Yii::app()->controller->module->id;

$this->breadcrumbs = array('Pipeline Report' => '');

$js = <<<JS

        $(".apptFilter").change(function(){
            if(typeof $("option:selected", this).val() === "undefined"){
                $(this).val("");
            } else {
                $('option[value=""]', this).prop('selected', false);
            }
            $("select").trigger("liszt:updated");
            $('#pipeline-report-form').attr('action','/admin/tracking/pipeline');
            updateGrid();
			return false;
		});

		$("#printIt").click(function() {
		    $(this).attr('href', '/admin/tracking/pipelineprint/?'+$('#pipeline-report-form').serialize());
            return true;
		});

        function updateGrid() {
            $.fn.yiiGridView.update("pipeline-report-grid", {
                data: $("form#pipeline-report-form").serialize(),
                complete: function(jqXHR, status) {

                    if (status=='success'){

                    }
                }
            });
        }

        $("#emailIt").click(function() {
            stmModal.create({
                title: 'Email Pipeline Report',
                content: $('#pipelineEmailModal').html(),
                height: 800,
                width: 750,
                successCallback: function(response) {
                    Message.create('success', 'Email sent.');
                }
            });
        });
JS;
Yii::app()->clientScript->registerScript('pipeline-report-form-script', $js);

$form=$this->beginWidget('CActiveForm', array('id'=>'pipeline-report-form', 'method'=>'get'));

Yii::app()->clientScript->registerCss('pipelineReportCss', <<<CSS
.pipeline-summary {
        margin: 30px 0;
}
.pipeline-summary label {
    text-align: center;
    width: 80%;
    font-size: 15px;
}
.pipeline-summary .count {
    background: #ffe1c0;
    padding: 15px;
    font-size: 22px;
    font-weight: bold;
    text-align: center;
}
CSS
);

?>

<script id="pipelineEmailModal" type="text/x-handlebars-template">
    <form action="/admin/tracking/pipelineemail" method="POST">
        <div id="pipelineEmailModal-container" class="stm-modal-body">
            <h3>Emails</h3>
            <input type="textbox" id="emailTo" placeholder="List of emails (comma separated)" />
        </div>
        <div class="stm-modal-footer">
            <button type="submit" id="sendEmail">Send Email</button>
        </div>
    </form>
</script>

    <h1>Pipeline Report</h1>

    <div class="g12" style="margin-bottom: 20px;">
        <div class="g2">
            <div class="g12">Appointment Set By:</div>
            <?php
                echo CHtml::dropDownList('set_by_id',null,CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('empty'=>'Anyone','class'=>'apptFilter chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Names'));
            ?>
            <?php
            $this->widget('admin_module.extensions.EChosen.EChosen', array(
                'target' => 'select#set_by_id',
            ));
            ?>
        </div>
        <div class="g2">
            <div class="g12">Production Status:</div>
            <?php echo CHtml::dropDownList('production_status', null, array(0=>'Descending', 1=>'Ascending'),array('class'=>'apptFilter chzn-select')); ?>
        </div>

        <div class="g2">
            <div class="g12">Rating:</div>
            <?php echo CHtml::dropDownList('rating', null, array(''=>'Select One',0=>'Descending', 1=>'Ascending'),array('class'=>'apptFilter chzn-select')); ?>
        </div>
        <div class="g2 p-fr p-tr">
            <a href="javascript:void(0);" id="printIt" class="btn btn-primary default" target="_blank">Print</a>
            <a href="javascript:void(0);" id="emailIt" class="btn btn-primary default">Email</a>
        </div>
    </div>

    <div class="clear"></div>

<div class="g12 pipeline-summary">
    <?php
    $i = 0;
    foreach($totals as $col => $total):?>
        <div class="g2">
            <label><?=$col;?></label>
            <div class="g12">
                <div class="g2"><?php if($i == 0):?><label>Leads</label><?php endif ;?></div>
                <div class="count g6"><?=$total['leads'];?></div>
            </div>
            <div class="g12">
                <div class="g2"><?php if($i == 0):?><label>Appts.</label><?php endif ;?></div>
                <div class="count g6"><?=$total['appt'];?></div>
            </div>
        </div>
    <?php
        $i++;
    endforeach;
    ?>
</div>

<?php

$this->widget('admin_module.components.StmGridView', array(
    'id'            =>'pipeline-report-grid',
    'template'	   =>'{pager}{summary}{items}{summary}{pager}',
    'dataProvider'  => $dataProvider,
    'enableSorting'=>true,
    'itemsCssClass' =>'datatables',
    'columns' => array(
        /*array(
            'type'  => 'raw',
            'name'  => 'ID',
            'value' => '$data->id',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),*/
        array(
            'type'  => 'raw',
            'name'  => 'Full Name',
            'value' => array($this, 'formatRecruitName'),//'Yii::app()->controller->formatRecruitName($data)',
            'htmlOptions' => array('style'=>'min-width:100px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => '1st Appt Date',
            'value' => 'Yii::app()->format->formatDate($data->appointment->set_for_datetime)',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Lead Recruiter',
            'value' => array($this, 'getLeadRecruiter'), //'Yii::app()->controller->getLeadRecruiter($data)',
            'htmlOptions' => array('style'=>'min-width:100px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Rating',
            'value' => array($this, 'getRating'),//'Yii::app()->controller->getRating($data)',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Production Status',
            'value' => array($this, 'getCapStatus'), //'Yii::app()->controller->getCapStatus($data)',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Lead Source',
            'value' => '$data->source->name',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Join By Date',
            'value' => 'Yii::app()->format->formatDate($data->target_date)',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Phone',
            'value' => 'Yii::app()->format->formatPrintPhones($data->contact->phones)',
            'htmlOptions' => array('style'=>'min-width:100px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Email',
            'value' => 'Yii::app()->format->formatPrintEmails($data->contact->emails)',
            'htmlOptions' => array('style'=>'min-width:100px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Next Follow-up',
            'value' => array($this, 'getNextStep'), //'Yii::app()->controller->getNextStep($data)',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Notes',
            'value' => array($this, 'formatNotes'), //'Yii::app()->controller->formatNotes($data)',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
    ),
));

$this->endWidget();
