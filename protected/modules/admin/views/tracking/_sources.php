<?php
if(!$gridType) {

    if($graph) { ?>

        <div id="closings-source-graph-container" style="padding: 10px; clear: both; width: 100%;">
<!--            <h1>--><?//=date('Y')?><!-- Key Performance Indicators</h1>-->
            <table id="closings-source-graph" data-height="400" data-tooltip="false" width="75%"></table>
            <h2><br></h2>
        </div>

    <? }


    $this->widget('admin_module.components.StmGridView', array(
            'id'            =>$gridName.'-sources-grid',//'closings-grid',
            'dataProvider'  =>$DataProvider,
            'template'	   =>'{items}',
            'extraParams'   =>array('allTotal'=>$allTotal),
            'itemsCssClass' =>'datatables',
            'columns' => array(
                array(
                    'type'=>'raw',
                    'name'=>'Source',
                    'value'=>'$data->parentSource',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:right;">'.(($countTotal)? 'Total: ' :'').'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'# Closings',
                    'value'=>'$data->countTotal',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;">'.(($countTotal)? $countTotal :'').'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'$ Volume',
                    'value'=>'Yii::app()->format->formatDollars($data->volumeTotal)',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:right;">'.(($volumeTotal)? 'Volume: ' :'').'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'% Business',
                    'value'=>'number_format($data->businessPercent*100)."%"',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;">'.Yii::app()->format->formatDollars($volumeTotal).'</span>',
                ),
//                array(
//                    'type'=>'raw',
//                    'name'=>'$ Spent',
//                    'value'=>'',
//                ),
//                array(
//                    'type'=>'raw',
//                    'name'=>'$/Closing',
//                    'value'=>'',
//                ),
            ),
        ));
}
elseif ($gridType == 'typeAppointmentsDataProvider'){

    $this->widget('admin_module.components.StmGridView', array(
            'id'            =>$gridName.'-sources-grid',//ex. 'closings-grid',
            'dataProvider'  =>$DataProvider,
            'template'	   =>'{items}',
            'extraParams'   =>array(),
            'itemsCssClass' =>'datatables',
            'columns' => array(
                array(
                    'type'=>'raw',
                    'name'=>'Source',
                    'value'=>'$data["source_name"]',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:right;">'.(($countTotal)? 'Total: ' :'').'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'Appointment Count',
                    'value'=>'($data["countTotal"]) ? $data["countTotal"] : "<span style=\"color: #D20000; font-weight: bold;\">Missing Appointments!</span>"',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;">'.(($countTotal)? $countTotal :'-').'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'% Appointment Source',
                    'value'=>'($data["businessPercent"]) ? number_format($data["businessPercent"], 1)."%" : "<span style=\"color: #D20000; font-weight: bold;\">Missing Appointments!</span>"',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:right;">'.(($countTotal)? 'Closings: ' :'').'</span>',
//                    'footer'=>'<span style="font-weight:bold;font-size:18px;">'.Yii::app()->format->formatDollars($volumeTotal).'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'# Closings from Source (not Appts)',
                    'value'=>'($data["total_closings"]) ? $data["total_closings"] : "-"',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:left;">'.(($closingCountTotal)? $closingCountTotal : '').'</span>',
                ),

                array(
                    'type'=>'raw',
                    'name'=>'$ Volume',
                    'value'=>'($data["total_closing_volume"])?Yii::app()->format->formatDollars($data["total_closing_volume"]) : "-"',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:left;">'.(($closingVolumeTotal)? Yii::app()->format->formatDollars($closingVolumeTotal) : '').'</span>',
                ),
//                array(
//                    'type'=>'raw',
//                    'name'=>'$ Spent',
//                    'value'=>'',
//                ),
//                array(
//                    'type'=>'raw',
//                    'name'=>'$/Closing',
//                    'value'=>'',
//                ),
            ),
        ));
}
elseif ($gridType == 'typeLeadsDataProvider'){

    $this->widget('admin_module.components.StmGridView', array(
            'id'            =>$gridName.'-sources-grid',//ex. 'closings-grid',
            'dataProvider'  =>$DataProvider,
            'template'	   =>'{items}',
            'extraParams'   =>array(),
            'itemsCssClass' =>'datatables',
            'columns' => array(
                array(
                    'type'=>'raw',
                    'name'=>'Source',
                    'value'=>'$data["source_name"]',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:right;">'.(($countTotal)? 'Total: ' :'').'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'Lead Count',
                    'value'=>'($data["countTotal"]) ? $data["countTotal"] : "<span style=\"color: #D20000; font-weight: bold;\">Missing Appointments!</span>"',
                    'footer'=>'<span style="font-weight:bold;font-size:18px;">'.(($countTotal)? $countTotal :'-').'</span>',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'% Lead Source',
                    'value'=>'number_format($data["businessPercent"], 1)."%"',
//                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:right;">'.(($countTotal)? 'Closings: ' :'').'</span>',
                    //                    'footer'=>'<span style="font-weight:bold;font-size:18px;">'.Yii::app()->format->formatDollars($volumeTotal).'</span>',
                ),
//                array(
//                    'type'=>'raw',
//                    'name'=>'# Closings from Source (not Appts)',
//                    'value'=>'($data["total_closings"]) ? $data["total_closings"] : "-"',
//                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:left;">'.(($closingCountTotal)? $closingCountTotal : '').'</span>',
//                ),
//
//                array(
//                    'type'=>'raw',
//                    'name'=>'$ Volume',
//                    'value'=>'($data["total_closing_volume"])?Yii::app()->format->formatDollars($data["total_closing_volume"]) : "-"',
//                    'footer'=>'<span style="font-weight:bold;font-size:18px;float:left;">'.(($closingVolumeTotal)? Yii::app()->format->formatDollars($closingVolumeTotal) : '').'</span>',
//                ),
//                array(
//                    'type'=>'raw',
//                    'name'=>'$ Spent',
//                    'value'=>'',
//                ),
//                array(
//                    'type'=>'raw',
//                    'name'=>'$/Closing',
//                    'value'=>'',
//                ),
            ),
        ));
}
