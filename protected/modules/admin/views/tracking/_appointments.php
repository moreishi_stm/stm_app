<?php $this->widget('admin_module.components.StmGridView', array(
			   'id'            =>'appointments-grid',
			   'template'	   =>'{pager}{summary}{items}{summary}{pager}',
			   'dataProvider'  =>$DataProvider,
			   'itemsCssClass' =>'datatables',
			   'columns' => array(
				   array(
					   'type'  => 'raw',
					   'name'  => 'Set on',
					   'value' => 'Yii::app()->format->formatDateTime($data->set_on_datetime, array("dateFormat"=>"M j","timeFormat"=>"g:i a","mySqlFormat"=>true))',
					   'htmlOptions' => array('style'=>'width:60px;'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Set for',
					   'value' => 'Yii::app()->format->formatDateTime($data->set_for_datetime, array("dateFormat"=>"M j","timeFormat"=>"g:i a","mySqlFormat"=>true))',
					   'htmlOptions' => array('style'=>'width:55px;'),
				   ),
                   array(
                       'type'  => 'raw',
                       'name'  => 'Set for Days',
                       'value' => 'Yii::app()->format->formatDaysDiff($data->set_on_datetime, $data->set_for_datetime)',
                       'htmlOptions' => array('style'=>'width:50px;'),
                   ),
                   'componentType.display_name',
                   array(
                       'type'  => 'raw',
                       'name'  => 'Source',
                       'value' => '$data->componentModel->source->name',
                   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Name',
					   'value' => '"<a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=_blank>".$data->contact->fullName."</a>"',
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Set By',
					   'value' => '$data->setBy->fullName',
					   //		                                                           'htmlOptions' => array('style'=>'width:85px;'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Met By',
					   'value' => '$data->metBy->fullName',
//		                                                           'htmlOptions' => array('style'=>'width:85px;'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Set via',
					   'value' => '$data->taskType->name',
//                       'htmlOptions' => array('style'=>'width:85px;'),
				   ),
                   array(
                       'type'  => 'raw',
                       'name'  => 'Met Status',
                       'value' => '$data->printMetStatusIcon().(($data->met_status_ma == Appointments::MET_STATUS_CANCEL || $data->met_status_ma == Appointments::MET_STATUS_RESCHEDULE)? ": ".$data->met_status_reason : "")',
//                       'htmlOptions' => array('style'=>'width:70px;'),
                   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Signed',
                       'value' => '$data->printSignedStatusIcon()',
					   'htmlOptions' => array('style'=>'width:125px;'),
				   ),
                   array(
                       'type'=>'raw',
                       'name'=>'',
                       'value'=>'"<div><a href=\"/admin/".$data->componentType->name."/edit/".$data->component_id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
                       'htmlOptions'=>array('style'=>'width:80px'),
                   ),
				   array(
					   'type'=>'raw',
					   'name'=>'',
					   'value'=>'"<div><a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
					   'htmlOptions'=>array('style'=>'width:80px'),
				   ),
			   ),
			   ));
