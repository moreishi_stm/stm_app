<?php
	Yii::app()->clientScript->registerScript('tracking-contacts-script', '

		$(".expandableDiv, .moreLink").live("click",function() {
			var id = $(this).attr("data");
			if($("#expandableDiv"+id).hasClass("min")) {
				$("#expandableDiv"+id).removeClass("min");
				$("#moreLink"+id).html($("#moreLink"+id).html().replace("More","Less"));
				$("#moreLink"+id).html($("#moreLink"+id).html().replace("+","-"));
			} else {
				$("#expandableDiv"+id).addClass("min");
				$("#moreLink"+id).html($("#moreLink"+id).html().replace("Less","More"));
				$("#moreLink"+id).html($("#moreLink"+id).html().replace("-","+"));
			}

		});

		$("#tracking-contacts-form #spoke_to, #tracking-contacts-form #not_spoke_to, #tracking-contacts-form #contact_id, #tracking-contacts-form #assigned_by_contact_id, #tracking-contacts-form #task_type_id #notes").change(function(){
			$("#tracking-contacts-form").submit();
			return false;
		});
	');

	$form=$this->beginWidget('CActiveForm', array(
											'id'=>'tracking-contacts-form',
											'action'=>'',
											'method'=>'get',
											));

?>
<style type="text/css">
	/*.datatables td:not(:first-child), .datatables th:not(:first-child) {*/
		/*text-align: center;*/
	/*}*/
</style>
<style type="text/css">
	.activityDescription{max-height: 200px;}
	.expandableDiv.min{max-height: 100px; overflow-y: hidden;}
	.expandableDiv:hover{cursor: pointer;}
</style>

<div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'tracking-contacts-grid',
																				   'isForm'=>false,
																				   'formSelector'=>'#tracking-contacts-form',
                                                                                   'returnSubmit' => 'false',
 )); ?>
</div>
<?php $this->breadcrumbs = array(
	'Tasks' => '',
);
?>
<h1>Activity Details Report</h1>
<h3 id="date-range-label">
	<label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> -
	<label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label>
</h3>
<div class="g1" style="text-align: right;">
    <span style="font-weight: bold;">Activities for:</span>
</div>
<div class="g2">
    <?php echo CHtml::dropDownList('contact_id',$contactIds,CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Select Names')); ?>
</div>
<div class="g1" style="text-align: right;">
    <span style="font-weight: bold;">Assigned by:</span>
</div>
<div class="g2">
<?php   echo CHtml::dropDownList('assigned_by_contact_id', null, CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Assigned by','empty'=>''));

?>
<?php
    $this->widget('admin_module.extensions.EChosen.EChosen', array(
        'target' => 'select#contact_id, select#assigned_by_contact_id, select#task_type_id',
        'options' => array('allow_single_deselect' => true)
    ));
?>
</div>
<div class="g1" style="text-align: right;">
    <span style="font-weight: bold;">Task Type:</span>
</div>
<div class="g2">
    <?php echo CHtml::dropDownList('task_type_id', null, CHtml::listData(TaskTypes::model()->byComponentType(ComponentTypes::CONTACTS)->findAll(), 'id', 'name'),array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Select Task Type','empty'=>'')); ?>
</div>

<div class="g3 p-pt10 p-pl10">
    <?php echo CHtml::radioButton('spoke_to',true,array('value'=>2))?>Both&nbsp;&nbsp;&nbsp;<?php echo CHtml::radioButton('spoke_to',false,array('value'=>1))?>Spoke to&nbsp;&nbsp;&nbsp;<? echo CHtml::radioButton('spoke_to',false,array('value'=>0))?>Not Spoke to
</div>
<div class="g1 p-clr" style="text-align: right;">
    <span style="font-weight: bold;">Notes:</span>
</div>
<div class="g2">
    <?php echo CHtml::textField('notes', null, array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Notes')); ?>
</div>
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
		   'id'            =>'tracking-contacts-grid',
		   'template'	   =>'{pager}{summary}{items}{pager}',
		   'dataProvider'  =>$DataProvider,
           'enableSorting'=>true,
		   'extraParams'   =>array('dateRange'=>$dateRange),
		   'itemsCssClass' =>'datatables',
		   'columns' => array(
//	                                                           'id',
			   array(
				   'type'  => 'raw',
				   'name'  => 'date',
				   'value' => 'Yii::app()->format->formatDate($data->activity_date, "n/j/y")."<br />".Yii::app()->format->formatTime($data->activity_date)',
				   'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
			   ),
			   array(
				   'type'  => 'raw',
				   'name'  => 'Type / From',
				   'value' => '$data->taskType->name."<br />".$data->addedBy->fullName',
				   'htmlOptions' => array('style'=>'min-width:100px;text-align:left;'),
			   ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Is Lead Gen',
                   'value' => '(($data->lead_gen_type_ma) ? "<em class=\"icon i_stm_success\"></em>".(($data->lead_gen_type_ma && $data->lead_gen_type_ma==ActivityLog::LEAD_GEN_NEW_ID) ? "<br />New" : "<br />Follow-up") : "")',
                   'htmlOptions' => array('style'=>'min-width:50px;'),
               ),
			   array(
				   'type'  => 'raw',
				   'name'  => 'Name',
				   'value' => '"<a href=\"/".Yii::app()->controller->module->id."/".$data->originalComponentType->name."/".$data->originalComponentId."\" target=\"_blank\">".$data->getContact()->fullName."</a><br/>(".(($data->component_type_id == ComponentTypes::TASKS)? Tasks::model()->findByPk($data->component_id)->componentType->singularName: $data->componentType->display_name).")"',
				   'htmlOptions' => array('style'=>'min-width:100px;text-align:left;'),
			   ),
			   array(
				   'type'  => 'raw',
				   'name'  => 'Description',
				   'value' => '(($data->is_spoke_to)? "<div style=\"display:inline-block; float:left;\"><em class=\"icon_large i_stm_spoke_to_2\" style=\"margin-right: 0; height:36px;\"></em></div>":"").
						ActivityLogPortlet::formatActivityNote($data->id, $data->note,array("nl2br"=>true,"maxRows"=>4,"maxHeight"=>"100px;"))',
				   'htmlOptions' => array(
					   'class' => 'activityDescription'
				   ),
//				   'value' => '(($data->is_spoke_to)? "<div style=\"display:inline-block; float:left;\"><em class=\"icon_large i_stm_spoke_to_2\" style=\"margin-right: 0; height:36px;\"></em></div>":"").$data->note',
//				   'htmlOptions' => array('style'=>'text-align:left;'),
			   ),
		   ),
		   ));

$this->endWidget();
