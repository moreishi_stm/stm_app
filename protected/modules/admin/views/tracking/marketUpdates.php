<?php
$module = Yii::app()->controller->module->name;
$js = <<<JS
        $('#market-update-tracking-form').submit(function() {
                $("body").prepend("<div class='loading-container loading'><em></em></div>");
        });
JS;

Yii::app()->clientScript->registerScript('marketUpdatesTrackingScript', $js);

	$form=$this->beginWidget('CActiveForm', array(
											'id'=>'market-update-tracking-form',
											'action'=>'',
											'method'=>'post',
											));
?>
<style type="text/css">
	td:not(:first-child), th:not(:first-child) {
		text-align: center;
	}
</style>

<div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('fromDateLabelSelector'=>'#from-date-label','toDateLabelSelector'=>'#to-date-label','gridName'=>'appointments-grid','defaultSelect'=>$defaultSelect,'isForm'=>false,'formSelector'=>'#appointments-form')); ?>
</div>
<?php $this->breadcrumbs = array(
	'Market Updates' => '',
);
?>
<h1>Market Updates Report</h1>
<h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label></h3>

<div style="width:750px;margin-left: auto;margin-right: auto; padding:20px;">
	<table class="p-f0" style="font-size: 16px;">
		<tr>
			<th></th>
			<th style="width: 25%;">Sent Market Updates</th>
			<th style="width: 25%;">Clicks</th>
			<th style="width: 25%;">Conversion</th>
		</tr>
		<tr>
			<th>Unique Contacts</th>
			<td><?php echo $data['countSentUnique']; ?></td>
			<td><?php echo Yii::app()->format->formatNumber($data['countClicksUnique']); ?></td>
			<td><?php echo ($data['countSentUnique'])? Yii::app()->format->formatPercentages($data['countClicksUnique'] / $data['countSentUnique']) : '-'; ?></td>
		</tr>
		<tr>
			<th>Total Count</th>
			<td><?php echo Yii::app()->format->formatNumber($data['countSent']); ?></td>
			<td><?php echo Yii::app()->format->formatNumber($data['countClicks']); ?></td>
			<td><?php echo ($data['countSent'])? Yii::app()->format->formatPercentages($data['countClicks'] / $data['countSent']) : '-'; ?></td>
		</tr>
	</table>
</div>

	<!--<div class="p-fr p-pb10">--><?php //$x = AssignmentTypes::model()->byActiveAdmin()->findAll();?>
<!--	--><?php //echo CHtml::dropDownList('contact_id',null,CHtml::listData(Contacts::model()->byAdmins()->findAll(), 'id', 'fullName'),array('empty'=>'All','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:200px;','data-placeholder'=>'Select Names'));
//		$this->widget('admin_module.extensions.EChosen.EChosen', array(
//																 'target' => 'select#contact_id',
//																 ));
//	?>
<!--</div>-->
<?php
$this->endWidget();

if(strpos($_SERVER['SERVER_NAME'], 'christineleeteam') !== false || strpos($_SERVER['SERVER_NAME'], 'myashevillerealestate') !== false) {
    $this->renderPartial('../savedHomeSearch/_listSent', array('dataProvider' => $dataProvider));
}