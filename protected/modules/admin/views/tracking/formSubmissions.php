<?php
Yii::app()->clientScript->registerCss('emailClickReportCssScript', <<<CSS
    .report-status-action-button {
        position: relative;
        cursor: pointer;
        display: inline-block;
        font-weight: normal !important;
        text-decoration: none !important;
    }
    .report-status-action-text {
        position: absolute;
        left: -8px;
        width: 180px;
        border: 1px #AAA solid;
        padding: 6px;
        background: #EFEFEF;
        display: none;
        color: #555;
        font-size: 13px;
        text-shadow: none;
        text-align: left;
    }

    .report-status-action-text:hover {
        text-decoration: underline !important;
    }

    .show .report-status-action-text {
        display: block;
        top: 14px;
    }

CSS
);
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('formSubmissionsScript', <<<JS

    //$('.report-status-action-button').live('click', function(){
    //    if($(this).hasClass('show')) {
    //        $(this).removeClass('show');
    //    }
    //    else {
    //        $(this).addClass('show');
    //    }
    //});

//    $('.status-update-button').live('click', function(){
//        var action;
//        if($(this).hasClass('mark-complete')) {
//            action = 'complete';
//        } else if($(this).hasClass('mark-incomplete')) {
//            action = 'incomplete';
//        }
//
//        $('body').prepend('<div class="loading-container loading"><em></em></div>');
//
//        $.post('/$module/tracking/formSubmissions/' + $(this).attr('data-id'), {action: action }, function(data) {
//            $("div.loading-container.loading").remove();
//
//            // refresh grid
//            if(data.status == 'success') {
//                $.fn.yiiGridView.update('email-click-tracking-grid', {
//                    data: $(this).serialize()
//                });
//            }
//            else {
//                Message.create('error', 'Error!.');
//            }
////            $("#smsMessage").val('');
//
//        }, 'json');
//    });



    //$('#listview-search form').submit(function() {
    $('#form-submissions-form').submit(function() {
        $.fn.yiiGridView.update("form-submissions-grid", {
            data: $(this).serialize()
        });

        //$("a.listen-recording").fancybox({'width':600, 'height':'auto', 'autoSize': false, 'scrolling':'no', 'padding':40});

        return false;
    });
JS
);
$form=$this->beginWidget('CActiveForm', array('id'=>'form-submissions-form'));
?>
<?php $this->breadcrumbs = array(
    'Form Submissions' => '',
);
?>
    <h1>Form Submissions Report</h1>

    <div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">

        <? $form = $this->beginWidget('CActiveForm', array('method' => 'get')); ?>
        <div class="g4">
            <label class="g6 p-tr">Type:</label>
            <span class="g6"><?php echo $form->dropDownList($model, 'type', array('New'=>'New','Resubmit'=>'Resubmit'), $htmlOptions = array('class'=>'g10','empty'=>'')); ?></span>
        </div>

        <div class="g6">
            <label class="g3 p-tr">Step:</label>
            <span class="g9"><?php echo $form->dropDownList($model, 'submission_step', array('Partial'=>'Partial','Full'=>'Full'), $htmlOptions = array('empty'=>'')); ?></span>
        </div>

        <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>

        <?php $this->endWidget(); ?>
    </div>

<?php $this->widget('admin_module.components.StmGridView', array(
    'id'            =>'form-submissions-grid',
    'template'	   =>'{pager}{summary}{items}{summary}{pager}',
    'dataProvider'  =>$model->searchForSellerGrid(),
    'enableSorting'=>true,
    'itemsCssClass' =>'datatables',
    'columns' => array(
        array(
            'type'  => 'raw',
            'name'  => 'Type',
            'value' => '$data->type',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Submission Step',
            'value' => '$data->submission_step',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'First Name',
            'value' => '(FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::FIRST_NAME_ID]) ? FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::FIRST_NAME_ID] : ""',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Last Name',
            'value' => '(FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::LAST_NAME_ID]) ? FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::LAST_NAME_ID]: ""',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Email',
            'value' => 'FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::EMAIL_ID]',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Phone',
            'value' => '(FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::PHONE_ID]) ? Yii::app()->format->formatPhone(FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::PHONE_ID]): ""',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Address',
            'value' => 'FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::ADDRESS_ID]."<br />".FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::CITY_ID].", ".AddressStates::getShortNameById(FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::STATE_ID])." ".FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::ZIP_ID]',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Beds/Baths',
            'value' => '(FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::BEDROOMS_ID]) ? FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::BEDROOMS_ID]."/".FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::BATHS_ID] : ""',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Condition',
            'value' => 'FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::CONDITION_ID]',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Source',
            'value' => '($data->source_id) ? Sources::model()->findByPk($data->source_id)->name : ""', //@todo: don't know why $data->source->name not working
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Source Description',
            'value' => 'FormSubmissionValues::getValuesBySubmissionId($data->id)[FormFields::SOURCE_DESCRIPTION_ID]',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Referrer',
            'value' => '$data->referrer',
            'htmlOptions'=>array('style'=>'width:180px'),
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Submit Date',
            'value' => 'Yii::app()->format->formatDate($data->added, "n/j/y")."<br />"',
            'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
        ),
        'id',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'($data->component_id&&$data->submission_step=="Full"?"<a href=\"/admin/sellers/".$data->component_id."\" class=\"button gray icon i_stm_search grey-button\">View Seller</a>":"")',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
    ),
));

$this->endWidget();
