<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => $gridName . '-listings-grid',
		'dataProvider' => $DataProvider,
		'template' => '{items}',
		'extraParams' => array('volumeTotal' => $volumeTotal),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			'id',
			array(
				'type'  => 'raw',
				'name'  => 'Contact Info',
				'value' => '"<strong>".CHtml::link($data->contact->fullName, array("/admin/sellers/".$data->id), array("target"=>"_blank"))."</strong><br />"
						.Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />"
						.Yii::app()->format->formatPrintPhones($data->contact->phones)',
			),
			array(
				'type'=>'raw',
				'name'=>'Property',
				'value'=>'
				"<strong><div><span class=\"list-view-price\">".Yii::app()->format->formatGridValue("", Yii::app()->format->formatDollars($data->getFieldValue("price_current")))."</span></div>".
				"<div><u>".Yii::app()->format->formatAddress($data->address, array(streetOnly=>true))."</u></strong><br />".Yii::app()->format->formatAddress($data->address, array(CityStZipOnly=>true))."</div>".
				"<div>".$br = (Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false))? Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false).Yii::app()->format->formatGridValue(" / Baths:", $data->getFieldValue("baths", 0),false) : Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false).
				"</div>".
				"<div>".Yii::app()->format->formatGridValue("SF:", $data->getFieldValue("sq_feet", 0),false)."</div>".
				"<div>".Yii::app()->format->formatGridValue("Submit:", Yii::app()->format->formatDateDays($data->contact->added))."</div>"
			',
			),
			array(
				'type'=>'raw',
				'name'=>'Profile',
				'value'=>'
				"<div>".Yii::app()->format->formatGridValue("Status:", $data->status->name)."</div>".
				"<div>".Yii::app()->format->formatGridValue("Source:", $data->source->name)."</div>".
				"<div>".Yii::app()->format->formatGridValue("Assigned to:", $data->assignments[0]->contact->fullName)."</div>".
				"<div>".Yii::app()->format->formatGridValue("Appt:", $data->getFieldValue("appt_timedate"))."</div>".
				"<div>".Yii::app()->format->formatGridValue("Met:", $data->getFieldValue("appt_met"))."</div>"
			',
			),
			array(
				'type'=>'raw',
				'name'=>'',
				'value'=>'
				"<div><a href=\"/admin/sellers/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Details</a></div>"
			',
				'htmlOptions'=>array('style'=>'width:120px'),
			),
		),
	)
);