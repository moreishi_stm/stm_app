<?php $this->breadcrumbs = array(
	'Traffic Details' => '',
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('traffic-details-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Traffic Details</h1>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBoxTrafficDetails',array(
	                                            'model'=>$model,
	                                            )); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
       'id'            =>'traffic-details-grid',
       'template'      =>'{pager}{summary}{items}{pager}{summary}',
       'dataProvider'  =>$DataProvider,
       'itemsCssClass' =>'datatables',
       'columns' => array(
           array(
               'type'  => 'raw',
               'name'  => 'Added',
               'value' => 'Yii::app()->format->formatDateTime($data->added,array("break"=>true))',
               'htmlOptions' => array('style'=>'width:85px;'),
           ),
           array(
               'type'  => 'raw',
               'name'  => 'Home Detail Views / Contact / IP',
               'value' => '"# Home Detail Views: ".$data->home_detail_view_count."<br /># Total Views: ".$data->countBySessionId($data->session_id)."<br />".$data->contact_id."<br />".$data->ip',
	           'htmlOptions'=>array('style'=>'width:175px;'),
           ),
           'utmz:googleAnalyticsCookie',
           array(
               'type'  => 'raw',
               'name'  => 'Domain / Page / Referrer',
               'value' => '$data->domain."<br />Page: ".wordwrap($data->page,80,"<br />",true)."<br />Referrer: ".wordwrap($data->referrer,50,"<br />",true)',
           ),
           array(
               'type'=>'raw',
               'name'=>'',
               'value'=>'
"<div><a href=\"/admin/tracking/session/".$data->session_id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Session</a></div>"
',
               'htmlOptions'=>array('style'=>'width:120px'),
           ),
       ),
));