<?php
Yii::app()->clientScript->registerCss('abcReportCss',<<<CSS
    .status {
        color: #FFF;
        padding: 4px 7px;
        font-weight: bold;
        font-size: 13px;
        border-radius: 13px;
        display: inline-block;
        margin-right: 4px;
    }
    .statusA {
        background-color: #D20000;
    }
    .statusB {
        background-color: #FFA500;
    }
    .statusC {
        background-color: dodgerblue;
    }
    .statusD {
        background-color: mediumpurple;
    }

    .summary-table .totalDivider{
        border-top: 2px solid #444;
    }
CSS
);

Yii::app()->clientScript->registerScript('tracking-contacts-script', <<<JS

//		$(".expandableDiv, .moreLink").live("click",function() {
//			var id = $(this).attr("data");
//			if($("#expandableDiv"+id).hasClass("min")) {
//				$("#expandableDiv"+id).removeClass("min");
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("More","Less"));
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("+","-"));
//			} else {
//				$("#expandableDiv"+id).addClass("min");
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("Less","More"));
//				$("#moreLink"+id).html($("#moreLink"+id).html().replace("-","+"));
//			}
//
//		});
//
    $("select").change(function(){
        $("#opportunities-tracking-form").submit();
        return false;
    });

    $('form').submit(function(){

        $.fn.yiiGridView.update("opportunities-tracking-grid", {
            data: $("form").serialize(),
            complete: function(jqXHR, status) {
                if (status=='success'){
                    var html = $.parseHTML(jqXHR.responseText);
                    var summaryTable = $('#summary-table-container', $(html)).html();
                    $('#summary-table-container').html(summaryTable);
                }
            }
        });
    });
JS
);

$form=$this->beginWidget('CActiveForm', array(
        'id'=>'opportunities-tracking-form',
        'action'=>'',
        'method'=>'get',
    ));

?>
<!--<style type="text/css">-->
<!--	/*.datatables td:not(:first-child), .datatables th:not(:first-child) {*/-->
<!--		/*text-align: center;*/-->
<!--	/*}*/-->
<!--</style>-->
<!--<style type="text/css">-->
<!--	.activityDescription{max-height: 200px;}-->
<!--	.expandableDiv.min{max-height: 100px; overflow-y: hidden;}-->
<!--	.expandableDiv:hover{cursor: pointer;}-->
<!--</style>-->
<!---->
<div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'opportunities-tracking-grid',
																				   'isForm'=>false,
                                                                                   'returnSubmit' => false,
																				   'formSelector'=>'#opportunities-tracking-form',
                                                                                   'defaultSelect' => 'last_3_months',
 )); ?>
</div>
<?php $this->breadcrumbs = array(
	'Opportunities' => '',
);
//?>
<h1>Opportunities Report</h1>
<h3 id="date-range-label">
	<label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> -
	<label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label>
</h3>
<div  id="summary-table-container" class="" style="width: 800px; margin-left: auto; margin-right: auto;">
    <table class="p-f0 summary-table" style="font-size: 16px; display: <?=(!empty($tableSummaryData['agents'])) ? 'block;' : 'none;' ?>">
        <tr>
            <th class="p-tc" style="width: 420px;"></th>
            <th class="p-tc" style="width: 100px;">Type</th>
            <th class="p-tc" style="width: 70px;">A</th>
            <th class="p-tc" style="width: 70px;">B</th>
            <th class="p-tc" style="width: 70px;">C</th>
            <th class="p-tc" style="width: 70px;">D</th>
        </tr>

        <?
        foreach($tableSummaryData['agents'] as $name => $row) {

            echo '<tr>';
            echo '<th>'.$name.'</th>';

            if(isset($row[ComponentTypes::SELLERS])) {

                echo '<td class="p-tc">Sellers</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::SELLERS][TransactionStatus::HOT_A_LEAD_ID].'</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::SELLERS][TransactionStatus::B_LEAD_ID].'</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::SELLERS][TransactionStatus::C_LEAD_ID].'</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::SELLERS][TransactionStatus::D_NURTURING_SPOKE_TO_ID].'</td>';
            }

            if(isset($row[ComponentTypes::BUYERS])) {

                // is seller data existed, start new row
                if(isset($row[ComponentTypes::SELLERS])) {
                    echo '</tr>';
                    echo '<tr>';
                    echo '<th></th>';
                }
                echo '<td class="p-tc">Buyers</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::BUYERS][TransactionStatus::HOT_A_LEAD_ID].'</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::BUYERS][TransactionStatus::B_LEAD_ID].'</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::BUYERS][TransactionStatus::C_LEAD_ID].'</td>';
                echo '<td class="p-tc">'.$row[ComponentTypes::BUYERS][TransactionStatus::D_NURTURING_SPOKE_TO_ID].'</td>';

            }
            echo '</tr>';

        }

        // print totals
        echo '<tr class="totalDivider">';
        echo '<th class="totalDivider">TOTALS:</th>';
        echo '<td class="p-tc totalDivider">Sellers</td>';
        echo '<td class="p-tc totalDivider">'.$tableSummaryData['totals'][ComponentTypes::SELLERS][TransactionStatus::HOT_A_LEAD_ID].'</td>';
        echo '<td class="p-tc totalDivider">'.$tableSummaryData['totals'][ComponentTypes::SELLERS][TransactionStatus::B_LEAD_ID].'</td>';
        echo '<td class="p-tc totalDivider">'.$tableSummaryData['totals'][ComponentTypes::SELLERS][TransactionStatus::C_LEAD_ID].'</td>';
        echo '<td class="p-tc totalDivider">'.$tableSummaryData['totals'][ComponentTypes::SELLERS][TransactionStatus::D_NURTURING_SPOKE_TO_ID].'</td>';
        echo '</tr>';

        echo '<tr>';
        echo '<th></th>';
        echo '<td class="p-tc">Buyers</td>';
        echo '<td class="p-tc">'.$tableSummaryData['totals'][ComponentTypes::BUYERS][TransactionStatus::HOT_A_LEAD_ID].'</td>';
        echo '<td class="p-tc">'.$tableSummaryData['totals'][ComponentTypes::BUYERS][TransactionStatus::B_LEAD_ID].'</td>';
        echo '<td class="p-tc">'.$tableSummaryData['totals'][ComponentTypes::BUYERS][TransactionStatus::C_LEAD_ID].'</td>';
        echo '<td class="p-tc">'.$tableSummaryData['totals'][ComponentTypes::BUYERS][TransactionStatus::D_NURTURING_SPOKE_TO_ID].'</td>';
        echo '</tr>';

        ?>
<!--        <tr>-->
<!--            <th>Opportunities Found</th>-->
<!--            <td>--><?php ////echo $PendingDataProvider->totalItemCount; ?><!--</td>-->
<!--            <td>--><?php ////echo $ClosedDataProvider->totalItemCount; ?><!--</td>-->
<!--            <td>--><?php ////echo $ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount; ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>Dialer Hours</th>-->
<!--            <td>--><?php ////echo Yii::app()->format->formatDollars($PendingVolume); ?><!--</td>-->
<!--            <td>--><?php ////echo Yii::app()->format->formatDollars($ClosedVolume); ?><!--</td>-->
<!--            <td>--><?php ////echo Yii::app()->format->formatDollars($ClosedVolume + $PendingVolume); ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>Opps per Hour</th>-->
<!--            <td>--><?php ////echo ($PendingDataProvider->totalItemCount)? Yii::app()->format->formatDollars($PendingVolume / $PendingDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars($ClosedVolume / $ClosedDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars(($ClosedVolume + $PendingVolume) / ($ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount)) : '-'; ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>Hot "A" - 30 Days</th>-->
<!--            <td>--><?php ////echo ($PendingDataProvider->totalItemCount)? Yii::app()->format->formatDollars($PendingVolume / $PendingDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars($ClosedVolume / $ClosedDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars(($ClosedVolume + $PendingVolume) / ($ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount)) : '-'; ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>B Opportunities - 60 days</th>-->
<!--            <td>--><?php ////echo ($PendingDataProvider->totalItemCount)? Yii::app()->format->formatDollars($PendingVolume / $PendingDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars($ClosedVolume / $ClosedDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars(($ClosedVolume + $PendingVolume) / ($ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount)) : '-'; ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>C Opportunities - 90 days</th>-->
<!--            <td>--><?php ////echo ($PendingDataProvider->totalItemCount)? Yii::app()->format->formatDollars($PendingVolume / $PendingDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars($ClosedVolume / $ClosedDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars(($ClosedVolume + $PendingVolume) / ($ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount)) : '-'; ?><!--</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>D Opportunities - 90+ days</th>-->
<!--            <td>--><?php ////echo ($PendingDataProvider->totalItemCount)? Yii::app()->format->formatDollars($PendingVolume / $PendingDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars($ClosedVolume / $ClosedDataProvider->totalItemCount) : '-'; ?><!--</td>-->
<!--            <td>--><?php ////echo ($ClosedDataProvider->totalItemCount)? Yii::app()->format->formatDollars(($ClosedVolume + $PendingVolume) / ($ClosedDataProvider->totalItemCount + $PendingDataProvider->totalItemCount)) : '-'; ?><!--</td>-->
<!--        </tr>-->
    </table>
</div>
<hr/>
<div class="g3">

    <div class="g3" style="text-align: right; margin-bottom: 25px;">
        <span style="font-weight: bold;">Name:</span>
    </div>
    <div class="g9">
        <?php echo CHtml::activeDropDownList($model, 'added_by', CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('class'=>'chzn-select','style'=>'width:100%;','empty'=>'')); ?>
    </div>
    <?php
        $this->widget('admin_module.extensions.EChosen.EChosen', array(
            'target' => 'select#Opportunities_component_type_id, select#Opportunities_added_by, select#Opportunities_hasAppointment, select#Opportunities_sortOrder', //, select#task_type_id
            'options' => array('allow_single_deselect' => true)
        ));
    ?>
</div>
<div class="g3">
    <div class="g5" style="text-align: right; margin-bottom: 25px;">
        <span style="font-weight: bold;">Type:</span>
    </div>
    <div class="g7">
        <?php echo CHtml::activeDropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->byIds(array(ComponentTypes::SELLERS, ComponentTypes::BUYERS))->findAll(), 'id', 'display_name'),array('class'=>'chzn-select','style'=>'width:100%;','empty'=>'','data-placeholder'=>'Seller/Buyer')); ?>
    </div>
</div>

<div class="g3">
    <div class="g4" style="text-align: right; margin-bottom: 25px;">
        <span style="font-weight: bold;">Sort & Filter:</span>
    </div>
    <div class="g8">
        <?php echo CHtml::activeDropDownList($model, 'sortOrder', array('targetDateAsc'=>'Target Date (Most Recent)', 'addedDateAsc'=>'Added Date (Lowest)', 'addedDateDesc'=>'Added Date (Highest)'),array('class'=>'chzn-select','style'=>'width:100%;')); ?>
    </div>
</div>

<div class="g3">
    <div class="g8" style="text-align: right; margin-bottom: 25px;">
        <span style="font-weight: bold;">Has Appointments:</span>
    </div>
    <div class="g4">
        <?php echo CHtml::activeDropDownList($model, 'hasAppointment', StmFormHelper::getYesNoList(),array('class'=>'chzn-select','style'=>'width:100%;','empty'=>'','data-placeholder'=>' ')); ?>
    </div>
</div>
<!--<div class="g1" style="text-align: right;">-->
<!--    <span style="font-weight: bold;">Task Type:</span>-->
<!--</div>-->
<!--<div class="g2">-->
<!--    --><?php //echo CHtml::dropDownList('task_type_id', null, CHtml::listData(TaskTypes::model()->byComponentType(ComponentTypes::CONTACTS)->findAll(), 'id', 'name'),array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Select Task Type','empty'=>'')); ?>
<!--</div>-->
<!---->
<!--    <div class="g3 p-pt10 p-pl10">-->
<!--    --><?php //echo CHtml::radioButton('spoke_to',true,array('value'=>2))?><!--Both&nbsp;&nbsp;&nbsp;--><?php //echo CHtml::radioButton('spoke_to',false,array('value'=>1))?><!--Spoke to&nbsp;&nbsp;&nbsp;--><?// echo CHtml::radioButton('spoke_to',false,array('value'=>0))?><!--Not Spoke to-->
<!--</div>-->
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
		   'id'            =>'opportunities-tracking-grid',
		   'template'	   =>'{pager}{summary}{items}{pager}',
		   'dataProvider'  =>$model->search(),
		   'extraParams'   =>array('dateRange'=>$dateRange),
		   'itemsCssClass' =>'datatables',
		   'columns' => array(
			   array(
				   'type'  => 'raw',
				   'name'  => 'Oppotunity Status / Target Date',
				   'value' => 'Yii::app()->controller->action->printStatus($data)',
				   'htmlOptions' => array('style'=>'width:80px;text-align:left;'),
			   ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Next Follow-up / Added By & Date',
                   'value' => 'Yii::app()->format->formatDate($data->next_followup_date, "n/j/y")."<br><br>".$data->addedBy->fullName."<br>(".Yii::app()->format->formatDate($data->added, "n/j/y").")"',
                   'htmlOptions' => array('style'=>'width:100px;text-align:left;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Lead Status / Action Plans',
                   'value' => 'Yii::app()->controller->action->printTransaction($data)',
                   'htmlOptions' => array('style'=>'width:300px;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Appointment',
                   'value' => 'Yii::app()->controller->action->printAppointment($data->appointmentId)',
                   'htmlOptions' => array('style'=>'width:200px;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Activity Log Note',
                   'value' => '$data->activityLog->note',
               ),
			   array(
				   'type'  => 'raw',
				   'name'  => 'View',
				   'value' => '"<a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a>"',
				   'htmlOptions' => array('style'=>'width:100px;text-align:left;'),
			   ),
		   ),
		   ));

$this->endWidget();