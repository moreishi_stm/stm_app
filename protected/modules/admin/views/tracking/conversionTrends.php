<?php
//Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
//Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');
$domainSuffix = (YII_DEBUG) ? 'local' : 'com';
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/flot.js');
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/wl_Chart.js');
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/flot/flot-plugin-tooltip/js/jquery.flot.tooltip.min.js');
//Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.'.$domainSuffix.'/assets/js/flot/flot-plugin-curvedLines/curvedLines.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCss('conversionTrendsCss', <<<CSS
    .graph-container {
        padding: 10px;
        width: 48%;
        float: left;
    }
    .graph-container .chart{
        clear: both;
    }
    .tickLabel {
        font-size: 11px;
    }
    .legend-container {
        margin-right: 1%;
    }
    .legend-container table{
        width: auto;
        max-width: 90%;
        float: right;
        background-image: none;
        margin: 8px;
    }
    .legendColorBox, .legendLabel {
        border: none;
    }
    .legendColorBox div:first-child {
        max-width: 14px;
    }
    .flotTip {
			border: 1px solid #fdd !important;
			padding: 2px !important;
			background-color: #fee !important;
			opacity: 0.80;
			font-size: 14px !important;
			border-radius: 0 !important;
			padding: 8px !important;
    }
    .divider {
        padding: 20px;
    }
CSS
);
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('conversionTrendsJs', <<<JS
    $('select').change(function(){
        updateGraph();
    });

    function updateGraph(data) {
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.post('/$module/tracking/conversionTrends',
            $('#conversion-trends-form').serialize(),
            function(data) {
                var graphSettings = [
                    {
                        'id' : 'executiveSummarySetAppointments',
                        'toolTipText' : ''
                    },
                    {
                        'id' : 'executiveSummaryMetAppointments',
                        'toolTipText' : ''
                    },
                    {
                        'id' : 'executiveSummarySigned',
                        'toolTipText' : ''
                    },
                    {
                        'id' : 'executiveSummarySetMetConversion',
                        'toolTipText' : ''
                    },
                    {
                        'id' : 'executiveSummaryMetSignedConversion',
                        'toolTipText' : ''
                    },
                    {
                        'id' : 'apptSet',
                        'toolTipText' : ' Appt Set'
                    },
                    {
                        'id' : 'apptMet',
                        'toolTipText' : ' Appt Met'
                    },
                    {
                        'id' : 'isaApptSigned',
                        'toolTipText' : ' Signed'
                    },
                    {
                        'id' : 'apptSetMetConversion',
                        'toolTipText' : '% Set-Met'
                    },
                    {
                        'id' : 'apptMetSignedConversion',
                        'toolTipText' : '% Met-Signed'
                    }
                ];

                $.each(graphSettings , function( index, graph ) {

                    $('#' + graph.id + '-graph').wl_Chart({
                        colors: [ "#9AE39A", "#ED9F9F", "#95EDED", "#E8E8B3", "#EB94EB", "#A59EF0" ],
                        flot: {
                            lines: {
                                lineWidth: 3
                            },
                            yaxes: [
                                {
                                    tickDecimals: 0
                                },
                                {
                                    tickDecimals: 0,
                                    position: 'right',
                                    tickFormatter: function(val, axis) {
                                        return (val) ? val + '%' : '';
                                    }
                                }
                            ],
                            xaxis: {
                                tickLength: 0,
                                ticks: {$xAxisTicks}
                            },
                            legend: {
                                noColumns: 5,
                                container: $('#' + graph.id + '-legend-container')
                            },
                            grid:  { hoverable: true }, //important! flot.tooltip requires this
                            tooltip: {
                                show: true,
                                content: "<strong>%s</strong><br>%y" + graph.toolTipText
                            }
                        },
                        type: 'lines',
                        data: data[ graph.id ] //ex. apptSet, apptMet
                    });

                });

                $("div.loading-container.loading").remove();
            },
            'json'
        );
    }

    // do this once to load graph
    updateGraph();
JS
);


$this->breadcrumbs = array(
	'Conversion Trends' => '',
);
?>
<h1>Conversion Trends Report</h1>
<h3 id="date-range-label">
    <label id="from-date-label" style="font-weight: bold;">
        <?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?>
    </label>
</h3>
<?
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'conversion-trends-form',
        'action'=>'',
        )
    );?>
<div class="p-fr p-pb10 g3">
    <div class="g4 p-tr">Specific Users:</div>
    <div class="g8"><?php echo CHtml::dropDownList('contactIds',null, $contactIdsListData, array('empty'=>'Everyone','class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Select Name'));?></div>

</div>
<div class="p-fr p-pb10 g3">
    <div class="g8 p-tr">Active Users Only:</div>
    <div class="g4"><?php echo CHtml::dropDownList('activeAdminsOnly',null, StmFormHelper::getYesNoList(), array('empty'=>'','class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>' '));?></div>
</div>
<div class="p-fr p-pb10 g3">
    <div class="g3 p-tr">Types:</div>
    <div class="g9"><?php echo CHtml::dropDownList('componentTypeIds', array(ComponentTypes::SELLERS, ComponentTypes::BUYERS),array(ComponentTypes::SELLERS=>'Sellers',ComponentTypes::BUYERS=>'Buyers',ComponentTypes::RECRUITS=>'Recruits'),array('empty'=>'','class'=>'apptFilter chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Buyer, Seller, Recruit'));?></div>
</div>

<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select','options'=>array('allow_single_deselect' => true,))); ?>
<? $this->endWidget(); ?>
<div class="p-clr divider"></div>

<div class="graph-container">
    <h1>Signed Agreements Summary</h1>
    <div id="executiveSummarySigned-legend-container" class="legend-container"></div>
    <table id="executiveSummarySigned-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="graph-container">
    <h1>Met-Signed Conversion % Summary</h1>
    <div id="executiveSummaryMetSignedConversion-legend-container" class="legend-container"></div>
    <table id="executiveSummaryMetSignedConversion-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="p-clr divider"></div>

<div class="graph-container">
    <h1>Set Appointments Summary</h1>
    <div id="executiveSummarySetAppointments-legend-container" class="legend-container"></div>
    <table id="executiveSummarySetAppointments-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="graph-container">
    <h1>Met Appointments Summary</h1>
    <div id="executiveSummaryMetAppointments-legend-container" class="legend-container"></div>
    <table id="executiveSummaryMetAppointments-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="p-clr divider"></div>

<div class="graph-container">
    <h1>Set-Met Conversion % Summary</h1>
    <div id="executiveSummarySetMetConversion-legend-container" class="legend-container"></div>
    <table id="executiveSummarySetMetConversion-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="graph-container">
    <h1>Signed-Contract Conversion % Summary</h1>
    <h3>Coming Soon...</h3>
    <div id="executiveSummarySignedContractConversion-legend-container" class="legend-container"></div>
    <table id="executiveSummarySignedContractConversion-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="p-clr divider"></div>

<div class="graph-container">
    <h1>Appointments Set</h1>
    <div id="apptSet-legend-container" class="legend-container"></div>
    <table id="apptSet-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="graph-container">
    <h1>Appointments Met</h1>
    <div id="apptMet-legend-container" class="legend-container"></div>
    <table id="apptMet-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="p-clr divider"></div>

<div class="graph-container">
    <h1>Set to Met %</h1>
    <div id="apptSetMetConversion-legend-container" class="legend-container"></div>
    <table id="apptSetMetConversion-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="graph-container">
    <h1>ISA Appointments Signed</h1>
    <div id="isaApptSigned-legend-container" class="legend-container"></div>
    <table id="isaApptSigned-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>

<div class="p-clr divider"></div>

<div class="graph-container">
    <h1>Met to Signed %</h1>
    <div id="apptMetSignedConversion-legend-container" class="legend-container"></div>
    <table id="apptMetSignedConversion-graph" data-height="250" data-tooltip="false" data-stack="false"></table>
</div>