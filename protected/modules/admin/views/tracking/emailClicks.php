<?php
Yii::app()->clientScript->registerCss('emailClickReportCssScript', <<<CSS
    .report-status-action-button {
        position: relative;
        cursor: pointer;
        display: inline-block;
        font-weight: normal !important;
        text-decoration: none !important;
    }
    .report-status-action-text {
        position: absolute;
        left: -8px;
        width: 180px;
        border: 1px #AAA solid;
        padding: 6px;
        background: #EFEFEF;
        display: none;
        color: #555;
        font-size: 13px;
        text-shadow: none;
        text-align: left;
    }

    .report-status-action-text:hover {
        text-decoration: underline !important;
    }

    .show .report-status-action-text {
        display: block;
        top: 14px;
    }

CSS
);
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('callHistoryScript', <<<JS

//    $("#status").change(function(){
//        $('#listview-search form').submit();
//        return false;
//    });

    $('.report-status-action-button').live('click', function(){
        if($(this).hasClass('show')) {
            $(this).removeClass('show');
        }
        else {
            $(this).addClass('show');
        }
    });

    $('.status-update-button').live('click', function(){
        var action;
        if($(this).hasClass('mark-complete')) {
            action = 'complete';
        } else if($(this).hasClass('mark-incomplete')) {
            action = 'incomplete';
        }

        $('body').prepend('<div class="loading-container loading"><em></em></div>');

        $.post('/$module/tracking/emailClickStatusUpdate/' + $(this).attr('data-id'), {action: action }, function(data) {
            $("div.loading-container.loading").remove();

            // refresh grid
            if(data.status == 'success') {
                $.fn.yiiGridView.update('email-click-tracking-grid', {
                    data: $(this).serialize()
                });
            }
            else {
                Message.create('error', 'Error with updated Email Follow-up Click Status.');
            }
//            $("#smsMessage").val('');

        }, 'json');
    });

//    $('#listview-search form').submit(function() {
//        $.fn.yiiGridView.update("call-history-grid", {
//            data: $(this).serialize()
//        });
//
//        $("a.listen-recording").fancybox({'width':600, 'height':'auto', 'autoSize': false, 'scrolling':'no', 'padding':40});
//
//        return false;
//    });
JS
);
$form=$this->beginWidget('CActiveForm', array('id'=>'email-clicks-form'));
?>

<div class="g12" style="position: absolute; top:2px;">
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'email-click-tracking-grid',
																				   'isForm'=>false,
                                                                                   'returnSubmit' => false,
																				   'formSelector'=>'#email-clicks-form',
 )); ?>
</div>
<?php $this->breadcrumbs = array(
	'Email Clicks' => '',
);
?>
<h1>Email Clicks Report</h1>
<h3 id="date-range-label">
	<label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> -
	<label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label>
</h3>



<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">

    <? $form = $this->beginWidget('CActiveForm', array('method' => 'get',)); ?>
    <div class="g4">
        <label class="g6 p-tr">Email Template Type:</label>
        <span class="g6"><?php echo $form->dropDownList($model, 'componentTypeId', CHtml::listData(ComponentTypes::model()->byIds(array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::CONTACTS, ComponentTypes::RECRUITS, ComponentTypes::CLOSINGS, ComponentTypes::REFERRALS))->findAll(), 'id', 'display_name'), $htmlOptions = array('class'=>'g10','empty'=>'')); ?></span>
    </div>

    <div class="g6">
        <label class="g3 p-tr">Email Template:</label>
        <span class="g9"><?php echo $form->dropDownList($model, 'email_template_id', CHtml::listData(EmailTemplates::model()->findAll(), 'id', 'subject'), $htmlOptions = array('empty'=>'')); ?></span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->





<?php $this->widget('admin_module.components.StmGridView', array(
		   'id'            =>'email-click-tracking-grid',
		   'template'	   =>'{pager}{summary}{items}{summary}{pager}',
		   'dataProvider'  =>$model->search(),
           'enableSorting'=>true,
		   'itemsCssClass' =>'datatables',
		   'columns' => array(
			   array(
				   'type'  => 'raw',
				   'name'  => 'date',
				   'value' => 'Yii::app()->format->formatDate($data->mostRecentClick->added, "n/j/y")."<br />".Yii::app()->format->formatTime($data->mostRecentClick->added)',
				   'htmlOptions' => array('style'=>'min-width:60px;text-align:left;'),
			   ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Contact',
                   'value' => 'Yii::app()->controller->renderPartial("../contacts/_gridContactInfoModel", array("data"=>$data->contact))',//$data->contact->fullName
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Contact Components',
                   'value' => '"<a name=\"".$data->id."\">".$data->contact->existingComponentRecordButton(ComponentTypes::CONTACTS)',
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Email Subject',
                   'value' => '$data->emailMessage->subject',
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Follow-up Status',
                   'value' => 'Yii::app()->controller->renderPartial("_gridStatus", array("model"=>$data))',
                   'htmlOptions' => array('style'=>'text-align:center;'),
               ),
               array(
                   'type'  => 'raw',
                   'name'  => 'Url',
                   'value' => 'EmailClickTracking::printGridUrl($data->mostRecentClick->url)',
               ),
		   ),
		   ));

$this->endWidget();
