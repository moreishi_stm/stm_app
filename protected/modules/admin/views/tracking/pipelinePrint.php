<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="print" />

        <!-- Optional theme -->
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" media="print" />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta charset="utf-8" />
        <style media="print" type="text/css">
            @media print {
                * {
                    text-shadow: none !important;
                    box-shadow: none !important;
                    -webkit-print-color-adjust: exact;
                }

                .table-striped>tbody>tr:nth-of-type(odd),
                .table-striped>tbody>tr:nth-of-type(odd) td{
                    background-color: #f9f9f9 !important;
                    -webkit-print-color-adjust: exact !important;
                }

                th {
                    text-align: center;
                }
                .pipeline-summary {
                    width: 100%;
                    margin: 20px 0;
                }
                .pipeline-summary label.lbl {
                    text-align: center;
                    font-size: 13px;
                    height: 32px;
                }
                .pipeline-summary .count {
                    background-color: #ffe1c0 !important;
                    padding: 15px;
                    font-size: 22px;
                    font-weight: bold;
                    text-align: center;
                }
                .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6,
                .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3,
                .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11,
                .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9,
                .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6,
                .col-xs-7, .col-xs-8, .col-xs-9 {
                    padding: 2px 4px;
                }

                .printShift {
                    margin: 0 0 0 -100px;
                }
            }
        </style>
        <style type="text/css">
            .pipeline-summary {
                margin: 20px 0;
            }
            .pipeline-summary label.lbl {
                text-align: center;
                width: 80%;
                font-size: 13px;
                height: 32px;
            }
            .pipeline-summary .count {
                background-color: #ffe1c0 !important;
                padding: 15px;
                font-size: 22px;
                font-weight: bold;
                text-align: center;
            }
            .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6,
            .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3,
            .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11,
            .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9,
            .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6,
            .col-xs-7, .col-xs-8, .col-xs-9 {
                padding: 2px 4px;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: center;">
                <h1><?=$title;?></h1>
                <h3>As of <?=date("m/d/Y", time()); ?></h3>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pipeline-summary">
                <?php
                $i = 0;
                foreach($totals as $col => $total):?>
                    <div class="item col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <label class="lbl"><?=$col;?></label>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php if($i == 0):?>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>Leads</label></div>
                            <?php endif; ?>
                            <div class="count<?php if($i == 0):?> col-lg-8 col-md-8 col-sm-8 col-xs-8<?php endif; ?>"><?=$total['leads'];?></div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php if($i == 0):?>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>Appts.</label></div>
                            <?php endif ;?>
                            <div class="count<?php if($i == 0):?> col-lg-8 col-md-8 col-sm-8 col-xs-8<?php endif; ?>"><?=$total['appt'];?></div>
                        </div>
                    </div>
                <?php
                    $i++;
                endforeach;
                ?>
            </div>

        </div>

        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 printShift">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>1st Appt Date</th>
                                <th>Lead Recruiter</th>
                                <th>Rating</th>
                                <th>Production Status</th>
                                <th>Lead Source</th>
                                <th>Join By Date</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Next Step</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $allData = $dataProvider->getData();
                            foreach($allData as $data): ?>
                            <tr>
                                <td><?= $data->contact->first_name." ".$data->contact->last_name;?></td>
                                <td><?=Yii::app()->format->formatDate($data->appointment->set_for_datetime); ?></td>
                                <td><?=Yii::app()->controller->getLeadRecruiter($data); ?></td>
                                <td><?=Yii::app()->controller->getRating($data); ?></td>
                                <td><?=Yii::app()->controller->getCapStatus($data); ?></td>
                                <td><?=$data->source->name;?></td>
                                <td><?=Yii::app()->format->formatDate($data->target_date); ?></td>
                                <td><?=Yii::app()->format->formatPrintPhones($data->contact->phones); ?></td>
                                <td><?=Yii::app()->format->formatPrintEmails($data->contact->emails); ?></td>
                                <td><?=Yii::app()->controller->getNextStep($data); ?></td>
                                <td><?=Yii::app()->controller->formatNotes($data); ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>