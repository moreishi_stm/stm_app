<style>
    #create-link-container input[type="text"] {
        font-size: 15px;
    }
    /*#create-link-container .urlPrefix {*/
        /*font-size: 15px;*/
        /*color: #69F;*/
        /*font-weight: bold;*/
        /*margin-top: 9px;*/
    /*}*/
    #create-link-container .label {
        font-size: 14px;
        font-weight: bold;
    }
    #create-link-container .link-row {
        margin-top: 20px;
    }
    #create-link-container .link-container {
        text-align: left;
        color: #69F;
        font-size: 20px;
        margin-bottom: 10px;
    }
</style>
<?php
$this->breadcrumbs=array(
	'Create Link'=>'',
);
$module = Yii::app()->controller->module->name;
$js = <<<JS
	$( '.create-link-button' ).click(function() {
        if($('#url').val()=='') {
            alert('Please enter a URL Destination');
            return false;
        }
        if($('#campaign_name').val()=='') {
            alert('Please enter a Campaign Name');
            return false;
        }
        var createUrl='';

        var urlText = $('#url').val();
        var urlSeparator = (urlText.indexOf("?") != -1) ?  '&' : '?';

        urlText = urlText.replace('http://','');
        createUrl = 'http://' + urlText + urlSeparator + 'utm_source=' + $('#source').val() + '&utm_campaign=' + $('#campaign_name').val();
//        createUrl = createUrl.replace(/\s{1,}/g, '+');
        createUrl = encodeURI(createUrl);
        $('.link-container').html(createUrl);
        $('.link-row').show('normal');
	});
JS;

Yii::app()->clientScript->registerScript('createLinkSource', $js);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name; ?>/sources/add" class="button gray icon i_stm_add">Add New Source</a>
</div>
<h1>Create Link</h1>

<div id="create-link-container" class="g12">
    <div class="g12">
        <div class="g4 p-tr label">Url Destination:</div>
        <div class="g4"><?php echo CHtml::textField('url', null, $htmlOptions=array('placeholder'=>'Ex. www.domain.com/page_name'))?></div>
    </div>
    <div class="g12">
        <div class="g4 p-tr label">Source:</div>
        <div class="g4"><?php echo CHtml::dropDownList('source', null, CHtml::listData(Sources::model()->findAll(),'name','name'),$htmlOptions=array())?></div>
                        <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#source','options'=>array('allow_single_deselect'=>true,'width'=>'100%','enable_split_word_search'=>true,'search_contains'=>true))); ?>
    </div>
<!--    <div class="g12">-->
<!--        <div class="g4 p-tr label">Medium:</div>-->
<!--        <div class="g4">--><?php //echo CHtml::textField('campaign_medium', null, $htmlOptions=array('placeholder'=>'Ex. cpc, banner email'))?><!--</div>Marketing medium: cpc, banner, email-->
<!--    </div>-->
<!--    <div class="g12">-->
<!--        <div class="g4 p-tr label">Campaign Term:</div>-->
<!--        <div class="g4">--><?php //echo CHtml::textField('campaign_term', null, $htmlOptions=array('placeholder'=>'Paid keyword terms: running+shoes'))?><!--</div>Note the keywords for this ad.-->
<!--    </div>-->
<!--    <div class="g12">-->
<!--        <div class="g4 p-tr label">Campaign Content:</div>-->
<!--        <div class="g4">--><?php //echo CHtml::textField('campaign_content', null, $htmlOptions=array('placeholder'=>'Ex. logolink or textlink'))?><!--</div> Used for A/B testing, ads that point to same URL.-->
<!--    </div>-->
    <div class="g12">
        <div class="g4 p-tr label">Campaign Name:</div>
        <div class="g4"><?php echo CHtml::textField('campaign_name', null, $htmlOptions=array('placeholder'=>'Ex. Spring Sale'))?></div>Product, promo, code or slogan
    </div>
    <div class="g12">
        <div class="g4 p-tr label"></div>
        <div class="g6"><?php echo CHtml::submitButton('Create Link', $htmlOptions=array('class'=>'button create-link-button', 'style'=>'width: 66%;'))?></div>
    </div>
    <div class="g12 link-row" style="display:none;">
        <div class="g4 p-tr label">Link to Copy & Paste:</div>
        <div class="g8">
            <h3 class="link-container">http://sample.com</h3>
            <span class="link-message">Complete the form above and hit Create Link and a link will appear.</span>
        </div>
    </div>
</div>