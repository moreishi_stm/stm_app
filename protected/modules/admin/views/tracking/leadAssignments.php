<?php $this->breadcrumbs = array(
    'Lead Assignments' => '',
);
?>
<h1>Lead Assignments Report</h1>
<?php $this->widget(
    'admin_module.components.StmGridView', array(
        'id'            => 'lead-assignments-grid',
        'dataProvider'  => $DataProvider,
        'itemsCssClass' => 'datatables',
        'columns'       => array(
//            array(
//                'type'        => 'raw',
//                'name'        => 'Date',
//                'value'       => '$data[dateLabel]',
//                'htmlOptions' => array('style' => 'width:25%;'),
//                'footer'      => '<span style="font-weight:bold;font-size:18px;">Total Count:</span>',
//            ),
//            array(
//                'type'        => 'raw',
//                'name'        => 'Buyer Leads',
//                'value'       => '$data[buyers]',
//                'htmlOptions' => array('style' => 'width:15%;'),
//                'footer'      => '<span style="font-weight:bold;font-size:18px;">' . $buyerLeadCount . '</span>',
//            ),
//            array(
//                'type'        => 'raw',
//                'name'        => 'Buyer Leads %',
//                'value'       => '$data[buyerLeadPercent]',
//                'htmlOptions' => array('style' => 'width:15%;'),
//                'footer'      => '<span style="font-weight:bold;font-size:18px;">'.(($buyerLeadCount)? Yii::app()->format->formatPercentages($buyerLeadCount/($buyerLeadCount+$sellerLeadCount)): '-').'</span>',
//            ),
//            array(
//                'type'        => 'raw',
//                'name'        => 'Seller Leads',
//                'value'       => '$data[sellers]',
//                'htmlOptions' => array('style' => 'width:15%;'),
//                'footer'      => '<span style="font-weight:bold;font-size:18px;">' . $sellerLeadCount .'</span>',
//            ),
//            array(
//                'type'        => 'raw',
//                'name'        => 'Seller Leads %',
//                'value'       => '$data[sellerLeadPercent]',
//                'htmlOptions' => array('style' => 'width:15%;'),
//                'footer'      => '<span style="font-weight:bold;font-size:18px;">' .(($sellerLeadCount)? Yii::app()->format->formatPercentages($sellerLeadCount/($buyerLeadCount+$sellerLeadCount)): '-').'</span>',
//            ),
//            array(
//                'type'        => 'raw',
//                'name'        => 'Count',
//                'value'       => '$data[buyers]+$data[sellers]',
//                'htmlOptions' => array('style' => 'width:15%;'),
//                'footer'      => '<span style="font-weight:bold;font-size:18px;">' . $totalCount . '</span>',
//            ),
        ),
    )
);