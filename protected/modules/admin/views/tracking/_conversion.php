<table>
    <thead>
        <tr>
            <th></th><th>Conversion</th><th>Metric 1</th><th>Metric 2</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Contacts per Appointment</td><td>12</td><td>142 Contacts</td><td>14 Appts</td>
        </tr>
        <tr>
            <td>Appointment to Agreement</td><td>93%</td><td>14 Appts</td><td>12 Agrmts</td>
        </tr>
        <tr>
            <td>Agreement to Contract</td><td>87%</td><td>12 Agrmts</td><td>8 Contracts</td>
        </tr>
        <tr>
            <th>CLOSING METRICS</th><th></th><th></th><th></th>
        </tr>
        <tr>
            <td>Contract to Close</td><td>75%</td><td>8 Contracts</td><td rowspan="3">6 Closed</td>
        </tr>
        <tr>
            <td>Appointments to Close</td><td>43%</td><td>14 Appts</td>
        </tr>
        <tr>
            <td>Agreement to Close</td><td>50%</td><td>12 Agrmts</td>
        </tr>
    </tbody>
</table>
