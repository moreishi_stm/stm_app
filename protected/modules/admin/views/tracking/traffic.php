<?
$this->breadcrumbs=array(
    'Web Traffic Report'
);

// Files needed for Sales graph
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl.DS.'flot.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl.DS.'wl_Chart.js');

?>
<script>
    $(function() {
        $('.chart').wl_Chart({
            flot: {
                lines:{
                    // fill:true,
                    // fillColor: {
                    //     colors: [ { opacity: .7 }, { opacity: .7 } ],
                    // },
                },
                legend: {
                    show: true,
                    container: $("#legend"),
                    position: 'nw',
                },

                colors: ['#42DB42', '#FF6B95', '#79EDED', '#F0F07F', '#F28AF2', '#9990F5'],
                //     colors: ["#18CC18","#DE4949"],
            },
        });
    });
</script>

<div id="content-header">
    <h1>Web Traffic Report</h1>
</div>
    <div id="legend" class="legend" style="width:94px; padding-top:20px;"></div>
<div class="g10 p-fr" style="text-align:center; padding-bottom:20px; ">
    <table class="chart" data-type="lines" data-orientation="vertical" data-legend="false" data-tilt=".75" data-height="200" data-width="750" data-tooltip="false">
        <thead>
            <tr>
                <th></th>
                <th>Adwords</th>
                <th>AOL</th>
                <th>Craigslist</th>
                <th>Direct Traffic</th>
                <th>Facebook</th>
                <th>Unknown</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>June</th>
                <td>48</td>
                <td>125</td>
                <td>159</td>
                <td>147</td>
                <td>154</td>
                <td>114</td>
            </tr>
            <tr>
                <th>July</th>
                <td>8</td>
                <td>27</td>
                <td>25</td>
                <td>79</td>
                <td>47</td>
                <td>59</td>
            </tr>
            <tr>
                <th>August</th>
                <td>28</td>
                <td>56</td>
                <td>98</td>
                <td>112</td>
                <td>87</td>
                <td>26</td>
            </tr>
            <tr>
                <th>October</th>
                <td>38</td>
                <td>43</td>
                <td>69</td>
                <td>54</td>
                <td>16</td>
                <td>16</td>
            </tr>
        </tbody>
    </table>
</div>


<table>
    <thead>
        <tr>
            <th>Source</th><th>Visitors</th><th>Form Views</th><th>Leads</th><th>Visit-to-Form</th><th>Form-to-Lead</th><th>Lead-to-Visit</th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td>Adwords</td>
            <td>846</td>
            <td>573</td>
            <td>173</td>
            <td>68%</td>
            <td>39%</td>
            <td>21%</td>
        </tr>
        <tr class="even">
            <td>AOL</td>
            <td>152</td>
            <td>38</td>
            <td>12</td>
            <td>43%</td>
            <td>31%</td>
            <td>24%</td>
        </tr>
        <tr class="odd">
            <td>Craigslist</td>
            <td>217</td>
            <td>104</td>
            <td>23</td>
            <td>36%</td>
            <td>26%</td>
            <td>19%</td>
        </tr>
        <tr class="even">
            <td>Direct Traffic</td>
            <td>426</td>
            <td>254</td>
            <td>102</td>
            <td>31%</td>
            <td>24%</td>
            <td>15%</td>
        </tr>
        <tr class="odd">
            <td>Facebook</td>
            <td>85</td>
            <td>41</td>
            <td>21</td>
            <td>12%</td>
            <td>21%</td>
            <td>12%</td>
        </tr>
        <tr class="even">
            <td>Unknown</td>
            <td>32</td>
            <td>14</td>
            <td>8</td>
            <td>35%</td>
            <td>28%</td>
            <td>21%</td>
        </tr>
        <tr class="odd">
            <td>Yahoo</td>
            <td>153</td>
            <td>53</td>
            <td>21</td>
            <td>46%</td>
            <td>21%</td>
            <td>24%</td>
        </tr>
        <tr class="even">
            <td>Zillow</td>
            <td>23</td>
            <td>12</td>
            <td>7</td>
            <td>54%</td>
            <td>18%</td>
            <td>19%</td>
        </tr>
        <tr>
            <th>Totals:</th><th>4835</th><th>2746</th><th>1424</th><th>54%</th><th>24%</th><th>18%</th>
        </tr>
    </tbody>
</table>