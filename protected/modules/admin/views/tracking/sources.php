<?php
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');
//Yii::app()->clientScript->registerCss('sourceTrackingScriptCss', <<<CSS
//div.tickLabels div.xAxis div.tickLabel
//{
//    text-align: right !important;
//    top: 470px !important;
//
//    transform: rotate(-60deg);
//    -ms-transform:rotate(-60deg);  /*IE 9*/
//    -moz-transform:rotate(-60deg); /* Firefox */
//    -webkit-transform:rotate(-60deg); /* Safari and Chrome */
//    -o-transform:rotate(-60deg); /* Opera */
//    /*rotation-point:60% 60%; *//* CSS3 */
//    /*rotation:270deg; *//* CSS3 */
//}
//CSS
//);
$showGraph  = ($ClosedGraphLabel && $ClosedGraphData) ? 1 : 0;
Yii::app()->clientScript->registerScript('sourceTrackingScript', <<<JS

//		$("#sources-form").submit(function(){
//			$(this).submit();
//			console.log('here');
//			return false;
//		});

    if($showGraph) {

       $('#closings-source-graph').wl_Chart({
            flot: {
                bars:{
                    align: "center",
                    lineWidth: 0,
                    fillColor: {
                        colors: [ { opacity: .7 }, { opacity: .7 } ]
                    }
                },
                yaxis: {
                    tickDecimals: 0,
                    max: $ClosedGraphDataMax,
                    axisLabel: '%',
                    tickFormatter: function (val, axis) {
                        return val + "%";
                    }
                },
                xaxis: {
//                    axisLabelPadding: 5,
                    tickLength: 0,
                    //labelWidth: 200,
//                    axisLabelFontSizePixels: 25,
//                    axisLabelUseCanvas: true,
                    ticks:[
                       $ClosedGraphLabel
                    ]
                },
                legend: {position: "nw", noColumns: 7}
            },
            type: 'bars',
                    barWidth: 0.8,
            data:[
                {
                    color: "#528EFF",//"#528EFF", //FFAB4A
                    data: [
                        $ClosedGraphData
                    ]
                }
            ]
        });
    }
JS
, CClientScript::POS_END);

$form = $this->beginWidget('CActiveForm', array(
        'id' => 'sources-form',
        'action' => '',
        'method' => 'post',
    )
);

?>
	<div class="g12" style="position: absolute; top:2px;">
		<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
//				'fromName' => 'from_date',
//				'toName' => 'to_date',
                'custom_from_date' => $DateRanger->custom_from_date,
                'custom_to_date' => $DateRanger->custom_to_date,
				'fromDateLabelSelector' => '#from-date-label',
				'toDateLabelSelector' => '#to-date-label',
				'gridName' => 'closings-grid',
				'isForm' => false,
//				'formSelector' => '#sources-form',
				'defaultSelect' => $DateRanger->defaultSelect
			)
		); ?>
	</div>
<?php $this->breadcrumbs = array(
	'Source of Business' => ''
);
?>
	<h1>Source of Business Report</h1>
	<h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date'])); ?></label> - <label id="to-date-label"
	                                                                                                                                                                   style="font-weight: bold;"><?php echo date("m/d/Y",
				strtotime($dateRange['to_date'])
			); ?></label></h3>

	<br/>    <br/>
	<div class="g12p-mr0">
		<?php
			$this->widget('zii.widgets.jui.CJuiTabs', array(
					'tabs' => array(
						'Closed Contracts (' . $ClosedTotal . ') ' . Yii::app()->format->formatDollars($ClosedVolume) => array(
							'content' => $this->renderPartial('_sources', array(
									'DataProvider' => $ClosedDataProvider,
									'gridName' => 'closed',
									'countTotal' => $ClosedTotal,
									'volumeTotal' => $ClosedVolume,
                                    'graph' => true,
								), true
							),
							'id' => 'closed-contracts-tab',
						),
						'Pending Contracts (' . $PendingTotal . ') ' . Yii::app()->format->formatDollars($PendingVolume) => array(
							'content' => $this->renderPartial('_sources', array(
									'DataProvider' => $PendingDataProvider,
									'gridName' => 'closed',
									'countTotal' => $PendingTotal,
									'volumeTotal' => $PendingVolume
								), true
							),
							'id' => 'pending-closings-tab',
						),
						'Fall-out Contracts (' . $FalloutTotal . ') ' . Yii::app()->format->formatDollars($FalloutVolume) => array(
							'content' => $this->renderPartial('_sources', array(
									'DataProvider' => $FalloutDataProvider,
									'gridName' => 'closed',
									'countTotal' => $FalloutTotal,
									'volumeTotal' => $FalloutVolume
								), true
							),
							'id' => 'fallout-contracts-tab',
						),
						'Appointments Set (' . $AppointmentsTotal . ') ' . Yii::app()->format->formatDollars($AppointmentsVolume) => array(
                            'content' => $this->renderPartial('_sources', array(
                                    'DataProvider' => $AppointmentsDataProvider,
                                    'gridName' => 'appointments',
                                    'countTotal' => $AppointmentsTotal,
                                    'volumeTotal' => $AppointmentsVolume,
                                    'closingCountTotal' => $ClosedTotal,
                                    'closingVolumeTotal' => $ClosedVolume,
                                    'gridType' => 'typeAppointmentsDataProvider',
                                ), true
                            ),
							'id' => 'appointments-tab',
						),
						'New Leads ('.$LeadsTotal.')' => array(
                            'content' => $this->renderPartial('_sources', array(
                                        'DataProvider' => $LeadsDataProvider,
                                        'gridName' => 'leads',
                                        'countTotal' => $LeadsTotal,
                                        'volumeTotal' => $LeadsVolume,
                                        'gridType' => 'typeLeadsDataProvider',
                                    ), true
                                ),
							'id' => 'leads-tab',
						),
						//							  'Buyers Closed' => array(
						//								  'content' => 'Coming Soon',
						//								  'id' => 'closed-buyers-tab',
						//							  ),
						//							  'Sellers Closed' => array(
						//								  'content' => 'Coming Soon',
						//								  'id' => 'closed-sellers-tab',
						//							  ),
					),
					'id' => 'sources-tracking-widget',
				)
			);
		?>
	</div>
<?php $this->endWidget();