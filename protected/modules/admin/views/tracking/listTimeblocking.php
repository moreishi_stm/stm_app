<?php Yii::app()->clientScript->registerScript(
    'search', "
	$('#Timeblockings_contact_id, #Timeblockings_timeblock_type_ma').change(function(){
		$.fn.yiiGridView.update('timeblocking-grid', { data: $('form#timeblock-list-form').serialize() });
	});

    $('form#timeblock-list-form').submit(function() {
        $.fn.yiiGridView.update('timeblocking-grid', { data: $(this).serialize() });
        return false;
    });

    $(document).on('click', 'a.delete-timeblock', function() {
        $.getJSON($(this).attr('href'), function(data) {
            if (data.success == true) {
                Message.create('success', 'Successfully removed timeblock entry!');
            } else {
                Message.create('error', 'Could not remove timeblock entry.');
            }

            $.fn.yiiGridView.update('timeblocking-grid');
        });

        return false;
    });
  "
);

$this->breadcrumbs = array('List' => '',);
?>

    <div id="listview-actions">
        <a href="javascript:void(0)" class="button gray icon i_stm_add" id="add-timeblock">Add New Timeblock</a>
    </div>
    <div id="content-header">
        <h1>Timeblocking List</h1>

        <h3 id="date-range-label">
            <label id="from-date-label" style="font-weight: bold;"><?php echo date(
                    "m/d/Y", strtotime($dateRange['from_date'])
                ); ?></label> -
            <label id="to-date-label" style="font-weight: bold;"><?php echo date(
                    "m/d/Y", strtotime($dateRange['to_date'])
                ); ?></label>
        </h3>
    </div>
    <? if($missingDates): ?>
    <h4 style="color: #D20000;">Missing Timeblock for the following weekdays: <br><?=$missingDates?></h4>
    <? endif; ?>

<?php    $form = $this->beginWidget(
    'CActiveForm', array('id'                   => 'timeblock-list-form',
                         'action'               => array('/admin/tracking/addTimeblocking'),
                         'enableAjaxValidation' => true,
                         'clientOptions'        => array('validateOnChange' => false,
                                                         'validateOnSubmit' => true,))
);
?>
    <div class="g12 p-fr">
        <?php
        $this->widget(
            'admin_module.components.widgets.DateRanger.DateRanger',
            array('fromDateLabelSelector' => '#from-date-label',
                  'toDateLabelSelector'   => '#to-date-label',
                  'gridName'              => 'timeblocking-grid',
                  'isForm'                => false,
                  'container'             => null)
        );
        echo CHtml::activeDropDownList(
            $model, 'timeblock_type_ma', CMap::mergeArray(array('' => ''), $model->getTypes()),
            $htmlOptions = array('class' => 'p-fr')
        );
        if (Yii::app()->user->checkAccess('timeblockUpdateOthers')) {
            echo CHtml::activeDropDownList(
                $model, 'contact_id', CHtml::listData(
                    Contacts::model()->byActiveAdmins(false, $model->contact_id)->orderByName()->findAll(), 'id',
                    'fullName'
                ), $htmlOptions = array('class' => 'p-fr')
            );
        }
        ?>
    </div>
<?php $this->endWidget(); ?>

    <div style="clear:both;"></div>
<?php $this->widget(
    'admin_module.components.StmGridView', array('id'            => 'timeblocking-grid',
                                                 'dataProvider'  => $DataProvider,
                                                 'itemsCssClass' => 'datatables',
                                                 'columns'       => array(array('type'  => 'raw',
                                                                                'name'  => 'Type',
                                                                                'value' => '$data->getModelAttribute("getTypes", $data->timeblock_type_ma)',),
                                                     'date:date',
                                                     'start_time:time',
                                                     'end_time:time',
                                                     array('type'  => 'raw',
                                                           'name'  => 'Hours',
                                                           'value' => 'number_format($data->getHours(), 2)',),
                                                     array('class'           => 'CLinkColumn',
                                                           'label'           => 'Edit',
                                                           'urlExpression'   => 'Yii::app()->createUrl("/admin/tracking/viewTimeblock", array(
                    "id" => $data->id,
                ))',
                                                           'htmlOptions'     => array('style' => 'text-align: center; width:75px;',),
                                                           'linkHtmlOptions' => array('class' => 'edit-timeblock button gray icon i_stm_edit',),),
                                                     array('class'           => 'CLinkColumn',
                                                           'label'           => 'Delete',
                                                           'urlExpression'   => 'Yii::app()->createUrl("/admin/tracking/deleteTimeblock", array(
					"id" => $data->id,
				))',
                                                           'htmlOptions'     => array('style' => 'text-align: center; width:75px;',),
                                                           'linkHtmlOptions' => array('class' => 'delete-timeblock button gray icon i_stm_delete',),),),)
);

// Timeblock Dialog Widget
$this->widget(
    'admin_widgets.DialogWidget.TimeblockDialogWidget.TimeblockDialogWidget', array('id'             => 'timeblocking',
                                                                                    'title'          => 'Add Timeblock',
                                                                                    'triggerElement' => '#add-timeblock, a.edit-timeblock',)
);
