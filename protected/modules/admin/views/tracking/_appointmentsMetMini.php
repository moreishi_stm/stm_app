<?php $this->widget('admin_module.components.StmGridView', array(
			   'id'            =>'appointments-grid',
			   'template'	   =>'{items}{pager}',
			   'dataProvider'  =>$DataProvider,
			   'extraParams'   =>array('dateRange'=>$dateRange),
			   'itemsCssClass' =>'datatables',
			   'columns' => array(
				   array(
					   'type'  => 'raw',
					   'name'  => 'Set on',
					   'value' => 'Yii::app()->format->formatDateTime($data->set_on_datetime, array("dateFormat"=>"M j","timeFormat"=>"h:i a","mySqlFormat"=>true))',
					   'htmlOptions' => array('style'=>'width:65px;'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Meet Date',
					   'value' => 'Yii::app()->format->formatDateTime($data->set_for_datetime, array("dateFormat"=>"M j","timeFormat"=>"h:i a","mySqlFormat"=>true))',
					   'htmlOptions' => array('style'=>'width:65px;'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Met',
					   'value' => 'StmHtml::booleanImage(($data->met_status_ma == Appointments::MET_STATUS_MET) ? true : false)',//'$data->getMetYesNo()',
					   'htmlOptions' => array('style'=>'width:30px;','class'=>'p-tc'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Signed',
					   'value' => 'StmHtml::booleanImage(($data->is_signed) ? true : false)', //'StmFormHelper::getYesNoName($data->is_signed)',
					   'htmlOptions' => array('style'=>'width:40px;','class'=>'p-tc'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Name',
					   'value' => '"<a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=_blank>".$data->contact->fullName."</a>"',
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Set via',
					   'value' => '$data->taskType->name',
					   'htmlOptions' => array('style'=>'width:50px;'),
				   ),
				   array(
					   'type'=>'raw',
					   'name'=>'',
					   'value'=>'"<div><a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
					   'htmlOptions'=>array('style'=>'width:80px'),
				   ),
			   ),
			   ));
