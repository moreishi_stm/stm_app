<?php
	Yii::app()->clientScript->registerScript('listings-tracking-script', '
		$("#listings-form").submit(function(){
			$(this).submit();
		});
	');

	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'listings-form',
			'action' => '',
			'method' => 'post',
		)
	);

?>
	<div class="g12" style="position: absolute; top:2px;">
		<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
				'fromDateLabelSelector' => '#from-date-label',
				'toDateLabelSelector' => '#to-date-label',
				'gridName' => 'listings-grid',
				'isForm' => false,
				'formSelector' => '#listings-form',
				'defaultSelect' => $DateRangerSelect
			)
		); ?>
	</div>
<?php $this->breadcrumbs = array(
	'Closings' => ''
);
?>
	<h1>Listings Report</h1>
	<h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date'])); ?></label> -
		<label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date'])); ?></label>
	</h3>

	<div class="p-fl" style="margin-bottom: 60px; width: 100%;">
		<div id="listing-pipeline-container" class="p-fl g100">
			<table class="p-mb10" style="font-size: 16px;">
				<tr>
					<th style="width: 20%;"></th>
					<th style="width: 20%;"># Quantity</th>
					<th style="width: 20%;">$ Volume</th>
					<th style="width: 20%;">Avg Sales Price</th>
					<th style="width: 20%;">Ratio</th>
				</tr>
				<tr>
					<th>Appointments</th>
					<td><?php echo $apptMetCount.' Met / '.$apptSetCount.' Set'; ?></td>
					<td></td>
					<td></td>
					<td><?php echo ($apptMetCount && $apptSetCount)? Yii::app()->format->formatPercentages($apptMetCount / $apptSetCount).' Met-to-Set' : '-'; ?></td>
				</tr>
				<tr>
					<th>New Listings Signed</th>
					<td><?php echo $newListingsCount; ?></td>
					<td><?php echo Yii::app()->format->formatDollars($newListingsVolume, '$0'); ?></td>
					<td><?php echo ($newListingsCount)? Yii::app()->format->formatDollars($newListingsVolume / $newListingsCount) : '-'; ?></td>
					<td><?php echo ($apptListingConversion) ? $apptListingConversion.'% Appt-to-List': ''; ?></td>
				</tr>
				<tr>
					<th>Executed</th>
					<td><?php echo $executedContractCount; ?></td>
					<td><?php echo Yii::app()->format->formatDollars($executedContractVolume, '$0'); ?></td>
					<td><?php echo ($executedContractCount)? Yii::app()->format->formatDollars($executedContractVolume / $executedContractCount) : '-'; ?></td>
					<td><?php echo ($executedContractCount && $newListingsCount)? Yii::app()->format->formatPercentages($executedContractCount / $newListingsCount).' of New Listings' : '-'; ?></td>
				</tr>
				<tr>
					<th>Scheduled to Close</th>
					<td><?php echo $scheduledClosingsCount; ?></td>
					<td><?php echo Yii::app()->format->formatDollars($scheduledClosingsVolume, '$0'); ?></td>
					<td><?php echo ($scheduledClosingsCount)? Yii::app()->format->formatDollars($scheduledClosingsVolume / $scheduledClosingsCount) : '-'; ?></td>
					<td></td>
				</tr>
				<tr>
					<th>Closed</th>
					<td><?php echo $closedCount; ?></td>
					<td><?php echo Yii::app()->format->formatDollars($closedVolume, '$0'); ?></td>
					<td><?php echo ($closedCount)? Yii::app()->format->formatDollars($closedVolume / $closedCount) : '-'; ?></td>
					<td><?php echo ($closedCount && $newListingsCount)? Yii::app()->format->formatPercentages($closedCount / $newListingsCount).' of New Listings' : '-'; ?></td>
				</tr>
<!--				<tr>-->
<!--					<th>Expired/Withdrawn</th>-->
<!--					<td>--><?php //echo $expiredWithdrawnListingsCount; ?><!--</td>-->
<!--					<td>--><?php //echo Yii::app()->format->formatDollars($expiredWithdrawnListingsVolume, '-'); ?><!--</td>-->
<!--					<td>--><?php //echo ($expiredWithdrawnListingsCount && $expiredWithdrawnListingsVolume)? Yii::app()->format->formatDollars($expiredWithdrawnListingsVolume / $expiredWithdrawnListingsCount) : '-'; ?><!--</td>-->
<!--					<td>--><?php //echo ($expiredWithdrawnListingsCount && $newListingsCount)? Yii::app()->format->formatPercentages($expiredWithdrawnListingsCount / $newListingsCount).' of New Listings' : '-'; ?><!--</td>-->
<!--				</tr>-->
				<tr>
					<th>Contract Fall-out</th>
					<td><?php echo ($falloutDataProvider->totalItemCount)? $falloutDataProvider->totalItemCount: '0'; ?></td>
					<td><?php echo Yii::app()->format->formatDollars($falloutVolume, '-'); ?></td>
					<td><?php echo ($falloutDataProvider->totalItemCount)? Yii::app()->format->formatDollars($falloutVolume / $falloutDataProvider->totalItemCount) : '-'; ?></td>
					<td><?php echo ($executedContractCount && $falloutDataProvider->totalItemCount)? Yii::app()->format->formatPercentages($falloutDataProvider->totalItemCount / $executedContractCount).' of New Listings' : '-'; ?></td>
				</tr>
			</table>
		</div>
		<div id="listing-conversion-container" class="p-fl g100">
			<table class="p-mt10" style="font-size: 16px;">
				<tr>
					<th></th>
					<th>Appt Sets</th>
					<th>Appt Met</th>
					<th>Listings Taken</th>
					<th>% Appt-Listing</th>
				</tr>
				<tr>
					<th style="width: 20%;">Total</th>
					<td style="width: 20%;"><?php echo $apptSetCount; ?></td>
					<td style="width: 20%;"><?php echo $apptMetCount; ?></td>
					<td style="width: 20%;"><?php echo $listingsTakenCount; ?></td>
					<td style="width: 20%;"><?php echo $apptListingConversion; ?>%</td>
				</tr>
			</table>
		</div>
	</div>
    <div class="p-clr"></div>
	<div class="g12p-mr0">
		<?php
			$this->widget('zii.widgets.jui.CJuiTabs', array(
					'tabs' => array(
						'New Listings (' . $newListingsDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($newListingsVolume) => array(
							'content' => $this->renderPartial('_listings', array(
									'DataProvider' => $newListingsDataProvider,
									'gridName' => 'new',
									'volumeTotal' => $newListingsVolume
								), true
							),
							'id' => 'new-listings-tab',
						),
						'Appointments (' . $apptMetDataProvider->totalItemCount . ') ' => array(
							'content' => $this->renderPartial('_appointments', array('DataProvider' => $apptMetDataProvider,'dateRange' => $dateRange), true),
							'id' => 'listing-appointments-tab',
						),
						'Executed Contracts (' . $executedContractDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($executedContractVolume) => array(
							'content' => $this->renderPartial('_closings', array('DataProvider' => $executedContractDataProvider,'dateRange' => $dateRange), true),
							'id' => 'listing-pending-tab',
						),
						'Closed (' . $closedDataProvider->totalItemCount . ') ' . Yii::app()->format->formatDollars($closedVolume) => array(
							'content' => $this->renderPartial('_closings', array('DataProvider' => $closedDataProvider,'dateRange' => $dateRange), true),
							'id' => 'closed-listings-tab',
						),
//						'Expired/Withdrawn (' . $expiredWithdrawnListingsDataProvider->totalItemCount . ') ' => array(
//							'content' => $this->renderPartial('_listings', array(
//								   'DataProvider' => $expiredWithdrawnListingsDataProvider,
//								   'gridName' => 'expired-withdrawn',
//								   'volumeTotal' => $newListingsVolume
//								   ), true),
//							'id' => 'expired-listings-tab',
//						),
						'Fall-outs (' . $falloutDataProvider->totalItemCount . ') ' => array(
							'content' => $this->renderPartial('_closings', array('DataProvider' => $falloutDataProvider,'gridName' => 'fallout','volumeTotal' => $falloutVolume), true),
							'id' => 'fallout-closings-tab',
						),
					),
					'id' => 'listings-tracking-widget',
				)
			);
		?>
	</div>
<?php $this->endWidget();