<?php Yii::app()->clientScript->registerScript('appointments-pastdue-script', <<<JS
		$("select").change(function(){
            $.fn.yiiGridView.update("appointments-grid", { data: $("form#appointments-form").serialize()});

			return false;
		});

		$("form").submit(function(){
            $.fn.yiiGridView.update("appointments-grid", { data: $("form#appointments-form").serialize()});

			return false;
		});
JS
);

$this->breadcrumbs = array(
	'Tasks' => '',
);
?>
    <h1>Past Due Appointments</h1>
    <?php if($DataProvider->totalItemCount): ?>
    <h3>Please correct the past due appointments below.</h3>
    <h4 style="color: #D20000;">PAST DUE = Met status needs to be updated or a missing reason for a cancel or reschedule.</h4>
    <?php else: ?>
    <h3>Congrats! You have no past due appointments!</h3>
    <?php endif; ?>
    <form id="appointments-form" action="">
        <div class="g12" style="margin-bottom: -30px;">
            <div class="g4"></div>
            <div class="g4">
                <?php echo CHtml::dropDownList('met_by_id', Yii::app()->user->id, CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullName'),array('class'=>'chzn-select','style'=>'width:100%;','data-placeholder'=>'Select Name','empty'=>''));

                $this->widget('admin_module.extensions.EChosen.EChosen', array(
                        'target' => 'select#met_by_id',
                        'options' => array('allow_single_deselect' => true)
                    ));
                ?>
            </div>
            <div class="g1"><?=CHtml::submitButton('Submit', $htmlOptions=array('style'=>'font-size: 12px;'))?></div>
        </div>
    </form>
    <div class="clear p-pb10 p-mb10"></div>

<?php $this->renderPartial('_appointments', array('DataProvider' => $DataProvider)); ?>
<div class="p-clr"></div>
<h1 style="margin-top: 40px;">Video Tutorial: How to Update Past Due Appointments</h1>
<div class="p-tc">
    <iframe src="https://player.vimeo.com/video/<?=$vimeoCode;?>" width="800" height="487" frameborder="0" style="margin-right: auto; margin-left: auto;"></iframe>
</div>
