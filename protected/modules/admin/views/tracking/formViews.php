<?php $this->breadcrumbs = array(
	'Form View' => '',
);
?>
<h1>Form View</h1>
<?php $this->widget('admin_module.components.StmGridView', array(
	'id'            =>'form-views-grid',
	'template'      =>'{pager}{summary}{items}{pager}{summary}',
	'dataProvider'  =>$model->search(),
	'itemsCssClass' =>'datatables',
	'columns' => array(
           array(
	           'type'  => 'raw',
	           'name'  => 'Added',
		           'value' => 'Yii::app()->format->formatDateTime($data->added,array("break"=>true))',
		           'htmlOptions' => array('style'=>'width:75px;'),
	       ),
	       array(
	           'type'  => 'raw',
	           'name'  => '# Traffic / Form Views',
	           'value' => '"# Form Views: ".$data->viewsBySessionId($data->session_id)."<br /># Total Views: ".CHtml::link($data->trafficViewsBySessionId($data->session_id),"/admin/tracking/session/".$data->session_id)',
	       ),
	       'utmz:googleAnalyticsCookie',
	//	                                                           array(
	//		                                                           'type'  => 'raw',
	//		                                                           'name'  => 'Utmz',
	//		                                                           'value' => 'formatGoogleAnalyticsCookie',//wordwrap($data->utmz,30,"<br />",true)
	//	                                                           ),
	       array(
	           'type'  => 'raw',
	           'name'  => 'Domain / Page',
	           'value' => '$data->domain."<br />".$data->page',
	       ),
	       //update `form_views` set page = mid(url, locate("/",url)+1), domain = mid(url, 1, locate("/",url)-1) WHERE 1
	       //update `form_views` set url = replace(url,"//","/") WHERE 1
	       array(
	           'type'  => 'raw',
	           'name'  => 'Referrer',
	           'value' => 'substr($data->referrer,0,strpos($data->referrer,"/"))."<br />".wordwrap(substr($data->referrer,strpos($data->referrer,"/")+1),50,"<br />",true)', //wordwrap($data->referrer,50,"<br />",true)
	       ),
	       array(
	           'type'=>'raw',
	           'name'=>'',
	           'value'=>'
	"<div><a href=\"/admin/tracking/session/".$data->session_id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Session</a></div>"
	',
	           'htmlOptions'=>array('style'=>'width:120px'),
	       ),
	   ),
	));