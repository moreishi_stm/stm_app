<?php $this->widget('admin_module.components.StmGridView', array(
			   'id'            =>'appointments-grid',
			   'template'	   =>'{items}{pager}',
			   'dataProvider'  =>$DataProvider,
			   'extraParams'   =>array('dateRange'=>$dateRange),
			   'itemsCssClass' =>'datatables',
			   'columns' => array(
				   array(
					   'type'  => 'raw',
					   'name'  => 'Date',
					   'value' => 'Yii::app()->format->formatDate($data->set_for_datetime, "M j")',
					   'htmlOptions' => array('style'=>'width:50px;'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Time',
					   'value' => 'Yii::app()->format->formatTime($data->set_for_datetime)',
					   'htmlOptions' => array('style'=>'width:65px;'),
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Name',
					   'value' => '"<a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=_blank>".$data->contact->fullName."</a>"',
				   ),
				   array(
					   'type'  => 'raw',
					   'name'  => 'Set via',
					   'value' => '$data->taskType->name',
					   'htmlOptions' => array('style'=>'width:50px;'),
				   ),
//				   array(
//					   'type'  => 'raw',
//					   'name'  => 'Set by',
//					   'value' => '$data->setBy->fullName',
//					   'htmlOptions' => array('style'=>'width:50px;'),
//				   ),
				   array(
					   'type'=>'raw',
					   'name'=>'',
					   'value'=>'"<div><a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
					   'htmlOptions'=>array('style'=>'width:80px'),
				   ),
			   ),
			   ));
