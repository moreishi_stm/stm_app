<?php
	// Begin the widget layout view
	echo CHtml::openTag('div', $containerHtmlOptions);

	// Render the handle of the portlet
	echo CHtml::tag('h3', $handleHtmlOptions, $handleContent);

	// Render the content inside of the layout
	echo $content;

	// End the widget layout view
	echo CHtml::closeTag('div');
