<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <?php echo $this->getModule()->registerScripts(); ?>
    <title><?php echo $this->title; ?></title>
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="img/icon.png">
	<link rel="apple-touch-startup-image" href="img/startup.png">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	<link rel="shortcut icon" href="http://cdn.seizethemarket.<?=(YII_DEBUG) ? 'local': 'com'?>/assets/images/favicon.ico?v=3" type="image/x-icon">

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
</head>

<body>

	<div class="container g12" id="content">

		<?php $this->renderPartial('/layouts/_topNavigation'); ?>

		<div id="content-container">

			<?php $this->renderPartial('/layouts/_leftSideArea'); ?>

<!--			<section style="position: relative;">-->
				<?php $this->widget('admin_widgets.StmBreadcrumbs', array('links'=>$this->breadcrumbs,)); ?>
				<?php $this->widget('admin_widgets.FlashMessageWidget.FlashMessageWidget'); ?>
				<div id="<?php echo $this->pageColor?>">
					<?php echo $content; ?>
				</div>
<!--			</section>-->

		</div>
	</div>

	<?php $this->renderPartial('/layouts/_footer');

	$js = <<<JS
		$(function() {
			$("#mobile #nav > li > a").click(function(event) {
				console.log($(this));

				event.stopPropagation();
				event.preventDefault();

				var parent = $(this).parent();

				if(parent.hasClass('open')) {
					parent.removeClass('open');
					parent.css('height', parent.attr('data-height'));
					return;
				}

				var currentHeight = parent.css('height');
				parent.attr('data-height', currentHeight);
				parent.addClass('open');
				parent.css('height','auto');
			});
		});
JS;

	Yii::app()->getClientScript()->registerScript('mobileNavJs', $js);
	?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-38180271-3', 'auto');
        ga('send', 'pageview');

    </script>
</body>

</html>
