<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta charset="utf-8">

	<title>Login</title>

	<?php echo $this->getModule()->registerScripts(); ?>
    <?php foreach($this->module->cdnCssAssets as $cssFile) {
        echo '<link rel="stylesheet" type="text/css" href="http://cdn.seizethemarket.com/assets/css/'.$cssFile.'">';
    }
    ?>
    <?php foreach($this->module->cdnJsAssets as $jsFile) {
        echo '<script type="text/javascript" src="http://cdn.seizethemarket.com/assets/js/'.$jsFile.'"></script>';
    }
    ?>
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="img/icon.png">
	<link rel="apple-touch-startup-image" href="img/startup.png">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">

	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->

	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
</head>

<body>

<div class="container" id="page">

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->

</body>
</html>
