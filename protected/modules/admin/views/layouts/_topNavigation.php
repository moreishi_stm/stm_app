<header>
    <div id="pageoptions">
        <!-- Handlebars Template for Quick Search -->
        <script id="template-result" type="text/x-handlebars-template">
            {{#each results}}
            <article class="result-{{component_type}}">
                <a href="/admin/{{component_type}}/{{link_id}}">
                <h2><div class="component-type">{{component_type_label}}</div>{{first_name}} {{last_name}}</h2>
                {{#if has_email}}<div class="component-type"></div>{{email}}<br>{{/if}}
                {{#if has_phone}}<div class="component-type"></div>{{phone}}<br>{{/if}}
                {{#if has_address}}
                        <h2><div class="component-type"></div>{{address}}<br>
                        <h2><div class="component-type"></div>{{city}}, {{state_short}} {{zip}}
                {{/if}}
                </a>
            </article>
            {{/each}}
        </script>
    <?php
        $topLinkItems = Yii::app()->user->topLinks;
        //this should be DB driven via settings "top links"
        $this->widget('zii.widgets.CMenu', array(
            'items'=> $topLinkItems,
            'id' => 'top-links-container',
            'encodeLabel'=>false,
        ));

        $menuConfig = array(
            // DASHBOARD
            array(
                'label'=>'Dashboard',
                'url'=>array('/admin/dashboard'),
                'itemOptions'=>array('class'=>'mnuDashboard first'),
                'submenuOptions'=>array('class'=>'ddWrapper dashboard'),

                // if transactions is not enabled, this indicates the TL package and removes the dropdown - @todo: prolly need better way - CLee 4/14/16
                'items'=>(Yii::app()->user->checkAccess('transactions')) ? array(
                    //column 1
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'GO TO',
                                'items'=>array(
                                    array('label'=>'My Dashboard', 'url'=>array('/admin/dashboard/index')),
                                    array('label'=>'My Tasks', 'url'=>array('/admin/tasks/index')),
                                    array('label'=>'My Goals', 'url'=>array('/admin/goals/index')),
                                    array('label'=>'My Meeting Notes', 'url'=>array('/admin/meetingNotes')),
                                    array('label'=>'My One Thing Tracker', 'url'=>array('/admin/goals/oneThing')),
                                    array('label'=>'My Profile', 'url'=>array('/admin/settings/index')),
                                    array('label'=>'My Timeblocking', 'url'=>array('/admin/tracking/timeblocking')),
                                    array('label'=>'My Badges', 'url'=>array('/admin/badges/myBadges')),
                                    array('label'=>'My Referrals', 'url'=>array('/admin/referrals/index')),
                                    array('label'=>'Operations Manual', 'url'=>array('/admin/operationManuals')),
                                    // array('label'=>'My Call Sessions', 'url'=>array('/admin/callsession/index')),
                                    // array('label'=>'Web Traffic Report', 'url'=>array('/admin/tracking/traffic')),
                                    // array('label'=>'Agent Lead Report', 'url'=>array('/admin/tracking/agentLeads')),
                                ),
                            ),
                        ),
                    ),
                    //column 2
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'Contacts',
                                'items'=>array(
                                    array('label'=>'Search Contacts', 'url'=>array('/admin/contacts')),
                                    array('label'=>'Add Contact', 'url'=>array('/admin/contacts/add')),
                                    array('label'=>'Forever Clients', 'url'=>array('/admin/foreverClients')),
                                    array('label'=>'Special Dates', 'url'=>array('/admin/contacts/specialDates')),
                                    array('label'=>'Keep In Touch', 'url'=>array('/admin/contacts/keepInTouch')),
                                ),
                            ),
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'Companies',
                                'items'=>array(
                                    array('label'=>'Search Companies', 'url'=>array('/admin/companies')),
                                    array('label'=>'Add Company', 'url'=>array('/admin/companies/add')),
                                ),
                            ),
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'Projects',
                                'items'=>array(
                                    array('label'=>'View Projects', 'url'=>array('/admin/projects')),
                                    array('label'=>'Add Project', 'url'=>array('/admin/projects/add')),
                                ),
                            ),
                        ),
                    ),
                ): null,
            ),
            // BUYERS
            array(
                'label'=>'Buyers',
                'url'=>array('/admin/contacts/leads/type/2'),
                'visible'=>(Yii::app()->user->checkAccess('transactions')),
                'itemOptions'=>array('class'=>'mnuBuyers'),
                'submenuOptions'=>array('class'=>'ddWrapper buyers'),
                'items'=>array(
                    //column 1
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            //                                        array(
                            //                                            'itemOptions'=>array('class'=>'ddTitle'),
                            //                                            'label'=>'MY LEADS',
                            //                                            'items'=>array(
                            //                                                array('label'=>'My Buyers', 'url'=>array('/admin/contacts/leads/type/2/myLeads/'.Yii::app()->user->id)),
                            //                                                array('label'=>'My A, B, C Buyers', 'url'=>array('/admin/buyers/abc')),
                            //                                            ),
                            //                                        ),
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'BUYERS',
                                'items'=>array(
                                    array('label'=>'All Buyers', 'url'=>array('/admin/contacts/leads/type/2')),
                                    array('label'=>'My A, B, C Buyers', 'url'=>array('/admin/buyers/abc')),
                                    array('label'=>'Add New Buyer', 'url'=>array('/admin/contacts/add/buyers')),
                                    array('label'=>'Reverse Prospecting', 'url'=>array('/admin/buyers/reverseProspect')),
                                    array('label'=>'Prospect Count', 'url'=>array('/admin/buyers/reverseProspectCount')),
                                ),
                            ),
                        ),
                    ),
                    //column 2
                    //                                array(
                    //                                    'template'=>'',
                    //                                    'visible'=>(!Yii::app()->user->checkAccess('lender')),
                    //                                    'itemOptions'=>array('class'=>'ddColumnWrapper'),
                    //                                    'items'=>array(
                    //                                        array(
                    //                                            'itemOptions'=>array('class'=>'ddTitle'),
                    //                                            'label'=>'TOOLBOX',
                    //                                            'items'=>array(
                    //                                                array('label'=>'Activity Insights', 'url'=>array('/admin/tracking/tasks')),
                    //                                                array('label'=>'Appointments Report', 'url'=>array('/admin/tracking/appointments', 'id'=>Yii::app()->user->id)),
                    //                                                array('label'=>'Buyer Storyboards', 'url'=>array('/admin/buyerStoryboards')),
                    //                                                array('label'=>'Lead Routing', 'url'=>array('/admin/leadRouting')),
                    //                                                array('label'=>'My Timeblocking', 'url'=>array('/admin/tracking/timeblocking')),
                    //                                                array('label'=>'Sent Market Updates', 'url'=>array('/admin/savedHomeSearch/sentBy', 'id'=>Yii::app()->user->id)),
                    //                                            ),
                    //                                        ),
                    //                                    ),
                    //                                ),
                ),
            ),
            // SELLERS
            array(
                'label'=>'Sellers',
                'url'=>array('/admin/contacts/leads/type/3'),
                'visible'=>(Yii::app()->user->checkAccess('transactions')),
                'itemOptions'=>array('class'=>'mnuSellers'),
                'submenuOptions'=>array('class'=>'ddWrapper sellers'),
                'items'=>array(
                    //column 1
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'SELLERS',
                                'items'=>array(
                                    //                                                array('label'=>'My Sellers', 'url'=>array('/admin/contacts/leads/type/3/myLeads/'.Yii::app()->user->id)),
                                    array('label'=>'All Sellers', 'url'=>array('/admin/contacts/leads/type/3')),
                                    array('label'=>'Add New Seller', 'url'=>array('/admin/contacts/add/sellers')),
                                    array('label'=>'Prospect Count', 'url'=>array('/admin/buyers/reverseProspectCount')),
                                ),
                            ),
                        ),
                    ),
                    //column 2
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'LISTING TOOLS',
                                'items'=>array(
                                    array('label'=>'Active Listings', 'url'=>array('/admin/contacts/leads/type/3/transactionStatus/8'), 'linkOptions'=>array('class'=>'ddItem')),
                                    array('label'=>'Listing Storyboards', 'url'=>array('/admin/listingStoryboards')),
                                    array('label'=>'Coming Soon Listings', 'url'=>array('/admin/sellers/comingsoon')),
                                    //                                                array('label'=>'Showing Feedback', 'url'=>array('/admin/sellers')),
                                    //												array('label'=>'Expired/Cancelled', 'url'=>array('/admin/sellers/expiredCancel')),
                                    array('label'=>'Craigslist Posts', 'url'=>array('/admin/craigslist')),
                                    array('label'=>'Lockboxes', 'url'=>array('/admin/lockboxes')),
                                    array('label'=>'Yard Signs', 'url'=>array('/admin/yardSigns')),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            // RECRUITS
            array(
                'label'=>'Recruits',
                'visible'=>(Yii::app()->user->checkAccess('menuRecruits')),
                'url'=>array('/admin/recruits'),
                'itemOptions'=>array('class'=>'mnuClosings'),
            ),
            array(
                'label'=>'My Market Center',
                'visible'=>(Yii::app()->user->checkAccess('menuRecruitMyMarketCenter')),
                'url'=>array('/admin/recruits?Recruits[myMarketCenter]=1'),
                'itemOptions'=>array('class'=>'mnuBuyers'),
            ),
            // DIALER
            array(
                'label'=>'Dialer',
                'visible'=>(Yii::app()->user->checkAccess('menuDialer')),
                'url'=>array('/admin/dialer'),
                'itemOptions'=>array('class'=>'mnuDialer'),
            ),
            // CLOSINGS
            array(
                'label'=>'Closings',
                'visible'=>(!Yii::app()->user->checkAccess('lender') && !Yii::app()->user->checkAccess('menuHideClosings') && Yii::app()->user->checkAccess('closings')),
                'url'=>array('/admin/closings'),
                'itemOptions'=>array('class'=>'mnuClosings'),
                //'submenuOptions'=>array('class'=>'ddWrapper closings'),
            ),
            //                        array(
            //                            'label'=>'Documents',
            //                            'url'=>array('/admin/documents'),
            //                            'visible'=>(!Yii::app()->user->checkAccess('lender') && Yii::app()->user->checkAccess('menuDocuments')),
            //                            'itemOptions'=>array('class'=>'mnuDocuments'),
            //                            'submenuOptions'=>array('class'=>'ddWrapper documents'),
            //                            'items'=>array(
            //                                //column 1
            //                                array(
            //                                    'template'=>'',
            //                                    'itemOptions'=>array('class'=>'ddColumnWrapper'),
            //                                    'items'=>array(
            //                                        array(
            //                                            'itemOptions'=>array('class'=>'ddTitle'),
            //                                            'label'=>'TRANSACTION',
            //                                            'items'=>array(
            //                                                array('label'=>'Purchase & Sale', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Addendum', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'HOA Disclosure', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Condo Disclosure', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Short Sale Disclosure', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Loyalty Agreement', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Referral Agreement', 'url'=>array('/admin/tasks')),
            //                                            ),
            //                                        ),
            //                                    ),
            //                                ),
            //                                //column 2
            //                                array(
            //                                    'template'=>'',
            //                                    'itemOptions'=>array('class'=>'ddColumnWrapper'),
            //                                    'items'=>array(
            //                                        array(
            //                                            'itemOptions'=>array('class'=>'ddTitle'),
            //                                            'label'=>'TEAM DOCS',
            //                                            'items'=>array(
            //                                                array('label'=>'Contract Checklist', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Buyer Agent Scripts', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Core Values', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'DISC', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Team Application', 'url'=>array('/admin/tasks')),
            //                                            ),
            //                                        ),
            //                                    ),
            //                                ),
            //                                //column 3
            //                                array(
            //                                    'template'=>'',
            //                                    'itemOptions'=>array('class'=>'ddColumnWrapper'),
            //                                    'items'=>array(
            //                                        array(
            //                                            'itemOptions'=>array('class'=>'ddTitle'),
            //                                            'label'=>'ACCOUNTABILITY',
            //                                            'items'=>array(
            //                                                array('label'=>'DAR Form', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Ideal Weekly Schedule', 'url'=>array('/admin/tasks')),
            //                                                array('label'=>'Goal Worksheet', 'url'=>array('/admin/tasks')),
            //                                            ),
            //                                        ),
            //                                        array(
            //                                            'itemOptions'=>array('class'=>'ddTitle'),
            //                                            'label'=>'MY DOCS',
            //                                            'items'=>array(
            //                                                array('label'=>'Manage Documents', 'url'=>array('/admin/documents')),
            //												array('label'=>'Operations Manual', 'url'=>array('/admin/operationManuals')),
            //                                            ),
            //                                        ),
            //                                    ),
            //                                ),
            //                            ),
            //                        ),

            // REPORTS
            array(
                'label'=>((Yii::app()->user->checkAccess('lender'))? 'Appointments': 'Tools'),
                'url'=>(Yii::app()->user->checkAccess('lender'))? '/admin/tracking/appointments': '#',
                'visible'=>(!Yii::app()->user->checkAccess('menuHideTools')), //!Yii::app()->user->checkAccess('hideToolsMenu') &&  - got rid of this from david cohen and replaced with menuHideTools
                'itemOptions'=>array('class'=>'mnuReports'),
                'submenuOptions'=>array('class'=>'ddWrapper reports'),
                'items'=>array(
                    //column 1
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'REPORTS',
                                'items'=>array(
                                    //                                                array('label'=>'Goals', 'url'=>array('/admin/goals')),
                                    array('label'=>'1st Responder Ranking', 'url'=>array('/admin/tracking/accountability', 'visible'=>Yii::app()->user->checkAccess('transactions'))),
                                    array('label'=>'Activity Insights', 'url'=>array('/admin/tracking/tasks')),
                                    array('label'=>'Activity Details', 'url'=>array('/admin/tracking/activities')),
                                    array('label'=>'Appointments Report', 'url'=>array('/admin/tracking/appointments')),
                                    array('label'=>'Appointments Past Due', 'url'=>array('/admin/tracking/appointmentsPastDue')),
                                    array('label'=>'Appointments Time of Day', 'url'=>array('/admin/tracking/appointmentTimes')),
                                    array('label'=>'Call Performance by Time Report', 'url'=>array('/admin/tracking/callPerformanceTimes')),
                                    array('label'=>'Call Trends (Inbound) by Time Report', 'url'=>array('/admin/tracking/callsInboundTimes')),
                                    array('label'=>'Closings Report', 'url'=>array('/admin/tracking/closings'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Closings Executive Summary', 'url'=>array('/admin/tracking/closingsExecutiveSummary'), 'visible'=>(Yii::app()->user->checkAccess('owner') && Yii::app()->user->checkAccess('transactions'))),
                                    array('label'=>'Conversion Report', 'url'=>array('/admin/tracking/conversion')),
                                    array('label'=>'Dialer List Removal Report', 'url'=>array('/admin/tracking/dialerListRemoval')),
                                    array('label'=>'Dialer Session Report', 'url'=>array('/admin/dialer/sessions'), 'visible'=>(Yii::app()->user->checkAccess('dialer'))),
                                    array('label'=>'Email Click Report', 'url'=>array('/admin/tracking/emailClicks')),
                                    array('label'=>'Internal Do Not Call Report', 'url'=>array('/admin/tracking/internalDoNotCall')),
                                    array('label'=>'Leads Report', 'url'=>array('/admin/tracking/leads')),
                                    array('label'=>'Listings Report', 'url'=>array('/admin/tracking/listings'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Opportunities Report', 'url'=>array('/admin/tracking/opportunities'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Pipeline Report', 'url'=>array('/admin/tracking/pipeline'), 'visible'=>Yii::app()->user->getClient()->type == 'Brokerage'),
                                    array('label'=>'Market Updates ', 'url'=>array('/admin/tracking/marketUpdates'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'ROI Report', 'url'=>array('/admin/accounting/roiReport'), 'visible'=>(Yii::app()->user->checkAccess('owner') && Yii::app()->user->checkAccess('transactions')),),
                                    array('label'=>'Saved Searches ', 'url'=>array('/admin/savedHomeSearch'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Sources Report', 'url'=>array('/admin/tracking/sources')),
                                    array('label'=>'Timeblocking Report', 'url'=>array('/admin/tracking/timeblocking'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                ),
                            ),
                        ),
                    ),
                    //column 2
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            //										array(
                            //											'itemOptions'=>array('class'=>'ddTitle'),
                            //											'label'=>'TOOLS',
                            //											'items'=>array(
                            //												array('label'=>'Merge Buyers', 'url'=>array('/admin/buyers/merge')),
                            //												array('label'=>'Merge Sellers', 'url'=>array('/admin/sellers/merge')),
                            //											),
                            //										),
                        ),
                    ),
                    //column 3
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'TELEPHONY',
                                'items'=>array(
                                    array('label'=>'Blocked Phone Numbers', 'url'=>array('/admin/callHuntGroups/listCallBlock'), 'visible'=>Yii::app()->user->checkAccess('owner')),
                                    array('label'=>'Call Hunt Groups', 'url'=>array('/admin/callHuntGroups'), 'visible'=>Yii::app()->user->checkAccess('owner')),
                                    array('label'=>'Dialer', 'url'=>array('/admin/dialer')),
                                    array('label'=>'Dialer Import', 'url'=>array('/admin/dialerimport')),
                                    array('label'=>'Dialer Call List', 'url'=>array('/admin/dialer/callLists')),
                                    array('label'=>'Dialer Call Recordings', 'url'=>array('/admin/dialer/recordings')),
                                    array('label'=>'Dialer Voicemails', 'url'=>array('/admin/dialer/viewVoicemailGreetings')),
                                    array('label'=>'IVR', 'url'=>array('/admin/ivr'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Phone Numbers', 'url'=>array('/admin/phone/numbers')),
                                    array('label'=>'Phone Voicemail Greetings', 'url'=>array('/admin/voicemail/greetings')),
                                    array('label'=>'Lead Connect Calls', 'url'=>array('/admin/phone/leadCallHistory')),
                                    array('label'=>'SMS / Text Messages', 'url'=>array('/admin/sms')),
                                ),
                            ),
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'RESOURCES',
                                'items'=>array(
                                    array('label'=>'Audio Library', 'url'=>array('/admin/audio'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'BombBomb', 'url'=>array('/admin/bombBomb'),'visible'=>(Yii::app()->user->checkAccess('transactions') || Yii::app()->user->checkAccess('bombBomb'))),
                                    array('label'=>'Flash Cards', 'url'=>array('/admin/flashcards'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Documents', 'url'=>array('/admin/documents'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Import Tool', 'url'=>array('/admin/import'), 'visible'=>(Yii::app()->user->checkAccess('owner'))),
                                    array('label'=>'Integrations', 'url'=>array('/admin/integrations')),
                                    array('label'=>'Mass Update by Email', 'url'=>array('/admin/contacts/massUpdateByEmail')),
                                    array('label'=>'Recruits', 'url'=>array('/admin/recruits'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'ROI Expenses', 'url'=>array('/admin/accounting/roi'), 'visible'=>(Yii::app()->user->checkAccess('transactions') && Yii::app()->user->checkAccess('owner'))),
                                    //                                                array('label'=>'Market Trends', 'url'=>array('/admin/marketTrends')),
                                    array('label'=>'Transfer Assignments', 'url'=>array('/admin/contacts/transfer'), 'visible'=>Yii::app()->user->checkAccess('owner')),
                                    array('label'=>'Transfer Tasks', 'url'=>array('/admin/tasks/transfer'), 'visible'=>Yii::app()->user->checkAccess('owner')),
                                    array('label'=>'Transfer Saved Searches', 'url'=>array('/admin/savedHomeSearch/transfer', 'visible'=>Yii::app()->user->checkAccess('owner'))),
                                ),
                            ),
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'WEB CONTENT',
                                'items'=>array(
                                    //                                                array('label'=>'Blog', 'url'=>array('/admin/widgets')),
                                    //                                                array('label'=>'Web Sites', 'url'=>array('/admin/settings')),
                                    array('label'=>'Web Pages (CMS)', 'url'=>array('/admin/cms')),
                                    array('label'=>'Comments (CMS)', 'url'=>array('/admin/cms/comments'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Menu & Navigation (CMS)', 'url'=>array('/admin/cms/menus')),
                                    array('label'=>'Tags (CMS)', 'url'=>array('/admin/cms/tags')),
                                    array('label'=>'Widgets (CMS)', 'url'=>array('/admin/widgets'), 'visible'=>(Yii::app()->user->checkAccess('transactions')&& Yii::app()->user->checkAccess('widgets'))),
                                    //                                                array('label'=>'Form Views', 'url'=>array('/admin/tracking/formViews')),
                                    //                                                array('label'=>'Traffic', 'url'=>array('/admin/tracking/trafficDetails')),
                                    array('label'=>'Create Tracking Link', 'url'=>array('/admin/tracking/createLink'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'House Values Sites', 'url'=>array('/admin/cms/houseValues'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Landing Pages', 'url'=>array('/admin/cms/landingPages')),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'label'=>'Settings',
                'visible'=>(!Yii::app()->user->checkAccess('menuHideSettings')),
                'url'=>array('/admin/settings'),
                'itemOptions'=>array('class'=>'mnuSettings last'),
                'submenuOptions'=>array('class'=>'ddWrapper settings'),
                'items'=>array(
                    //column 1
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'SETTINGS',
                                'items'=>array(
                                    array('label'=>'Action Plans', 'url'=>array('/admin/actionPlans')),
                                    array('label'=>'Automatic Triggers', 'url'=>array('/admin/triggers'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Contact Tags', 'url'=>array('/admin/contactTypes')),
                                    array('label'=>'Company Types', 'url'=>array('/admin/companyTypes'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Email Templates', 'url'=>array('/admin/emailTemplates')),
                                    array('label'=>'Featured Areas', 'url'=>array('/admin/featuredAreas'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Featured Area Types', 'url'=>array('/admin/featuredAreas/types'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Letter Templates', 'url'=>array('/admin/letterTemplates')),
                                    array('label'=>'Lead Routing', 'url'=>array('/admin/leadRouting')),
                                    array('label'=>'Operations Manuals', 'url'=>array('/admin/operationManuals'), 'visible'=>Yii::app()->user->checkAccess('operationManuals')),
                                    array('label'=>'Phone Tags', 'url'=>array('/admin/phone/tags'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Recruit Tags', 'url'=>array('/admin/recruitTypes')),
                                    array('label'=>'SMS Templates', 'url'=>array('/admin/smsTemplates'), 'visible'=>(Yii::app()->user->checkAccess('smsTemplates'))),
                                    array('label'=>'Sources', 'visible'=>Yii::app()->user->checkAccess('sources'), 'url'=>array('/admin/sources')),
                                    array('label'=>'Transaction Tags', 'url'=>array('/admin/transactionTags'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                ),
                            ),
                        ),
                    ),
                    //column 2
                    array(
                        'template'=>'',
                        'visible'=>(!Yii::app()->user->checkAccess('lender')),
                        'itemOptions'=>array('class'=>'ddColumnWrapper'),
                        'items'=>array(
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'USERS',
                                'items'=>array(
                                    array('label'=>'My Settings', 'url'=>array('/admin/settings')),
                                    array('label'=>'Account Settings', 'url'=>array('/admin/settings/account'),'visible'=>(Yii::app()->user->checkAccess('permissions') || Yii::app()->user->checkAccess('owner'))),
                                    array('label'=>'Goals', 'url'=>array('/admin/goals'), 'visible'=>Yii::app()->user->checkAccess('transactions')),
                                    array('label'=>'Users', 'url'=>array('/admin/settings/users'),'visible'=>(Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('permissions'))),
                                ),
                            ),
                            array(
                                'itemOptions'=>array('class'=>'ddTitle'),
                                'label'=>'ACCOUNT',
                                'items'=>array(
                                    array('label'=>'Features', 'url'=>array('/admin/console')),
                                    array('label'=>'Support', 'url'=>array('/admin/support')),
                                    array('label'=>'Logout', 'url'=>array('/admin/logout')),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );
    ?>
    </div>
    <div id="main-nav-container">
        <div id="logo">
            <a href="/admin/dashboard">
                <?php echo CHtml::image('http://cdn.seizethemarket.'.((YII_DEBUG) ? 'local' : 'com').'/assets/images/logo_admin'.(($_SERVER['']) ? '' : '').'.png?v=2', 'Seize the Market', $htmlOptions=array(
                'class'=>'logo'
            )); ?>
            </a>
        </div>
       <div>
            <nav id="regular">
            <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'=> $menuConfig,
                    'encodeLabel'=>false,
                    'id'=>'nav',
                ));
            ?>
            </nav>

		   <?php /* Added Bkozak: 03/14/2015 - new mobile menu */ ?>
		   <nav id="mobile">
               <div id="mobile-header">
                   <div class="nav-header">
                       Seize The Market
                   </div>
                   <div class="pull-right">
                       <button type="button" id="mobile-menu-expand" class="gray">Menu</button>
                   </div>
               </div>
            <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'=> $menuConfig,
                    'encodeLabel'=>false,
                    'id'=>'nav',
                ));
            ?>
            </nav>
        </div>
    </div>
</header>
