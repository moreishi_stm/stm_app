<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Image Manager</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css">

<!--        <link rel="stylesheet" href="/assets/image-manager/site/main.css">-->

        <style type="text/css">
            .freeze {
                position: fixed;
                top: 0px;
                left: 0px;
                right: 0px;
                padding-bottom: 20px;
                z-index: 1000;
                background: rgba(255, 255, 255, 0.8);
            }

            .freeze-inner {
                margin: 0px 10px;
            }

            #photos {
                /* Prevent vertical gaps */
                line-height: 0;

                -webkit-column-count: 5;
                -webkit-column-gap:   0px;
                -moz-column-count:    5;
                -moz-column-gap:      0px;
                column-count:         5;
                column-gap:           0px;
            }

            #photos img {
                width: 100% !important;
                height: auto !important;
            }

            #photos .image-check {
                position: absolute;
                top: 5px;
                right: 5px;
            }

            #photos .image {
                /* Just in case there are inline attributes */
                position: relative;
            }

            #photos .image h4 {
                margin: 1px 1px 4px 1px;
                color: rgba(255, 255, 255, 0.6)
            }

            #photos .info {
                position: absolute;
                bottom: 0px;
                left: 0px;
                right: 0px;
                width: 100%;
                background: rgba(0, 0, 0, 0.6);
                color: white;
                margin: 0px;
            }

            #photos .more-info {
                height: 0px;
                overflow: hidden;
            }

            @media (max-width: 1200px) {
                #photos {
                    -moz-column-count:    4;
                    -webkit-column-count: 4;
                    column-count:         4;
                }
            }
            @media (max-width: 1000px) {
                #photos {
                    -moz-column-count:    3;
                    -webkit-column-count: 3;
                    column-count:         3;
                }
            }
            @media (max-width: 800px) {
                #photos {
                    -moz-column-count:    2;
                    -webkit-column-count: 2;
                    column-count:         2;
                }
            }
            @media (max-width: 400px) {
                #photos {
                    -moz-column-count:    1;
                    -webkit-column-count: 1;
                    column-count:         1;
                }
            }
        </style>

    </head>
    <body class='yui3-skin-sam'>
        <?=$content?>
        <script src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js"></script>
        <script src="http://cdn.seizethemarket.com/assets/js/jquery/jquery-1.10.2.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
<!--        <script src="/assets/image-manager/site/main.js"></script>-->



        <!-- gc/gc-widgets/gc-widgets.js -->
        <script type="text/javascript">

            // Widgets module
            YUI.add('gc-widgets', function(Y) {

                // Make sure namespace exists
                Y.Gc = Y.Gc || {};

                // Define Gc Widgets
                Y.Gc.Widgets = {

                    /**
                     * Loading Bar
                     *
                     * Container for loading bar functionality
                     */
                    LoadingBar: {

                        // Awesome loader found here http://preloaders.net/

                        // Our bar object definition
                        Bar: Y.Base.create('panel', Y.Panel, [], {}, {
                            ATTRS: {
                                headerContent: {value: null},
                                bodyContent: {value: '<div style="text-align: center;"><img src="http://cdn.seizethemarket.com/assets/images/loading-bar.gif" /><br>Please Wait...</div>'},
                                width: {value: 300},
                                centered: {value: true},
                                modal: {value: true},
                                zIndex: {value: 10000},
                                buttons: {value: []},
                                hideOn: {value: []},
                                render: {value: true}
                            }
                        }),

                        // Internal instance of loading bar
                        instance: null,

                        // The number of times the widget has been requested to display
                        iCount: 0,

                        // Shows loading bar
                        show: function() {
                            Y.Gc.Widgets.LoadingBar.iCount++;
                            if(Y.Gc.Widgets.LoadingBar.iCount === 1) {
                                Y.Gc.Widgets.LoadingBar.instance = new Y.Gc.Widgets.LoadingBar.Bar();
                            }
                        },

                        // Hides loading bar
                        hide: function() {
                            Y.Gc.Widgets.LoadingBar.iCount--;
                            if(Y.Gc.Widgets.LoadingBar.iCount === 0) {
                                Y.Gc.Widgets.LoadingBar.instance.destroy();
                            }
                        }
                    },

                    /**
                     * Get Modal Template
                     *
                     * Returns the handlebars template used for generating Bootstrap style modals
                     * @returns string Handlebars template for bootstrap modal
                     */
                    GetModalTemplate: function() {
                        return "\n\
				<div class='modal fade' id='gc-modal'>\n\
					<div class='modal-dialog'>\n\
						<div class='modal-content'>\n\
							<div class='modal-header'>\n\
								<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>\n\
								<h4 class='modal-title'>{{title}}</h4>\n\
							</div>\n\
							<div class='modal-body'>\n\
								<p>{{{content}}}</p>\n\
							</div>\n\
							<div class='modal-footer'>\n\
								{{#each buttons}}\n\
								<button type='button' class='{{./class}}'{{#if ./close}} data-dismiss='modal'{{/if}}>{{./label}}</button>\n\
								{{/each}}\n\
							</div>\n\
						</div>\n\
					</div>\n\
				</div>\n\
			";
                    },

                    /**
                     * Notification
                     *
                     * Generates a twitter bootstrap style dialog box
                     * @param object config Configuration info for the dialog
                     * @returns void
                     */
                    Dialog: function(config) {

                        // Compile the template in to a node with our data we modelled above
                        var mContainer = Y.Node.create(Y.Handlebars.render(Y.Gc.Widgets.GetModalTemplate(), {
                            title: config.title ? config.title : '',
                            content: config.content ? '<p>' + config.content + '</p>' : '',
                            buttons: config.buttons ? config.buttons : [{label: 'Continue', class: 'btn btn-success', close: true}]
                        }));

                        // Append container to body
                        Y.one('body').appendChild(mContainer);

                        // Setup modal dialog
                        jQuery('#' + mContainer.getAttribute('id')).modal({
                            show : true,
                            backdrop : true
                        });
                    },

                    /**
                     * Server Error
                     *
                     * Called when a server error has occurred via an AJAX call
                     * @param object config Configuration info for the dialog
                     * @returns void
                     */
                    ServerError: function(config) {

                        // Construct content template for modal
                        var template = "\n\
				<p>{{message}}</p>\n\
				{{#if exception}}\n\
				<hr>\n\
				<h5>Message</h4>\n\
				{{exception.message}}\n\
				<h5>Stack Trace</h4>\n\
				<pre>{{exception.stackTrace}}</pre>\n\
				<h5>Request Parameters</h4>\n\
				<pre>{{{exception.requestParams}}}</pre>\n\
				{{/if}}\n\
			";

                        // Compile the template in to a node with our data we modelled above
                        var mContainer = Y.Node.create(Y.Handlebars.render(Y.Gc.Widgets.GetModalTemplate(), {
                            title: 'Epic Error',
                            buttons: config.buttons ? config.buttons : [{label: 'Continue', class: 'btn btn-success', close: true}],
                            content: Y.Handlebars.render(template, {
                                message: config.message,
                                exception: config.exception ? config.exception : false
                            })
                        }));

                        // Append container to body
                        Y.one('body').appendChild(mContainer);

                        // Setup modal dialog
                        jQuery('#' + mContainer.getAttribute('id')).modal({
                            show : true,
                            backdrop : true
                        });
                    }
                };
            }, null, {
                requires: ['node', 'handlebars', 'panel']
            });
        </script>



        <!-- main.js -->
        <script type="text/javascript">

            // YUI Config
            YUI_config = {
                groups: {
                    gc: {
                        filter: 'raw',
                        base: '/lib/gc/',
                        patterns: {
                            'gc-': {}
                        }
                    }
                }
            };

            // Prerequisites module
            YUI.add('gc-prereqs', function(Y) {

                // Bind callbacks for AJAX errors, and loading bar
                Y.on('io:start', Y.Gc.Widgets.LoadingBar.show);
                Y.on('io:end', Y.Gc.Widgets.LoadingBar.hide);
                Y.on('io:failure', function(id, o) {

                    // Attempt to parse out response from server
                    try {
                        var results = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        Y.Gc.Widgets.Dialog({
                            title: 'Epic Error',
                            content: 'Error parsing response from server, please try again. If the problem continues contact support.'
                        });
                        return;
                    }

                    // Display the server error modal
                    Y.Gc.Widgets.ServerError({
                        message: results.message,
                        exception: results.exception
                    });
                });

            }, null, {
                condition: {
                    trigger: 'io-base'
                },
                requires: ['io-base', 'json-parse', 'gc-widgets']
            });
        </script>

<!--        <script src="/assets/image-manager/site/image-manager.js"></script>--


        <!-- image-manager.js -->
        <script type="application/javascript">

            // Use YUI3 things
            YUI().use('node', 'event-mouseenter', 'app', 'app-transitions', 'transition', 'anim', 'view', 'handlebars', 'model', 'model-list', 'io', 'json-parse', 'uploader', 'gc-widgets', function(Y) {

                // Retrieve our config parameters
                var config = Y.JSON.parse(Y.one('#config').get('value'));

                // Make sure this exists, then create some base objects we need
                Y.GcImageManager = Y.GcImageManager || {};

                // Model for all models to extend
                Y.GcImageManager.BaseModel = Y.Base.create('imageManagerBaseModel', Y.Model, [], {});

                // Model list for all model lists to extend
                Y.GcImageManager.BaseModelList = Y.Base.create('imageManagerBaseModelList', Y.ModelList, [], {});

                // View for all views to extend
                Y.GcImageManager.BaseView = new Y.Base.create('baseView', Y.View, [], {

                    // Renders the view and passes along some data
                    renderWithData: function(results) {
                        this.get('container').setHTML(Y.Handlebars.render(Y.one(this.get('containerTemplate')).getHTML(), results));
                    },

                    // Renders the view
                    render: function() {
                        this.get('container').setHTML(Y.Handlebars.render(Y.one(this.get('containerTemplate')).getHTML()));
                    }
                });

                // Represents a directory or image
                Y.GcImageManager.FileModel = Y.Base.create('imageManagerImageModel', Y.GcImageManager.BaseModel, [], {

                    // Inserts the image (only valid when image manager is used for TinyMCE or form context)
                    insertImage: function() {

                        // Generate the image URL
                        var imgUrl = config.outputImageLocation + this.get('fullPath');

                        // Check whether or not we are using TinyMCE
                        if(config.mce) {
                            // Get MCE dialog params and set the image url in the parent dialog, then close.
                            var params = top.tinymce.activeEditor.windowManager.getParams();
                            var inputElement = window.parent.document.getElementById(params.input);

                            inputElement.value = imgUrl;
                            top.tinymce.activeEditor.windowManager.close();
                        }
                        else if(window.opener) {
                            window.opener.document.getElementById(config.eleId).value = imgUrl;
                            window.close();
                        }
                    },

                    // Deletes the image
                    deleteImage: function() {
                        Y.io('/admin/imageManager/delete', {
                            method: 'POST',
                            data: {
                                directory: this.get('fullPath')
                            },
                            on: {
                                success: function(id, o) {
                                    // Attempt to parse out server response
                                    try {
                                        var results = Y.JSON.parse(o.responseText)
                                    }
                                    catch(e) {
                                        callback('Unable to parse response from server');
                                        return;
                                    }

                                    // Handle different result types
                                    switch(results.status) {
                                        case 'success':
                                            App.set('directory', App.get('directory'));
                                            break;
                                        default:
                                            admin.dialog('Server Error', 'An unknown error has occurred while deleting.');
                                            break;
                                    }
                                }
                            }
                        });
                    }
                }, {
                    ATTRS: {
                        path: {value: null},
                        fullPath: {
                            valueFn: function() {
                                return App.get('directory') == '/' ? this.get('path') : '/' + App.get('directory') + this.get('path');
                            }
                        },
                        isDirectory: {value: false},
                        uid: {
                            getter: function() {
                                return this.get('clientId');
                            }
                        },
                        filename: {
                            getter: function() {
                                return this.get('path').split("/").pop();
                            }
                        }
                    }
                });

                // Represents a list of images
                Y.GcImageManager.FileModelList = Y.Base.create('imageManagerImageModelList', Y.GcImageManager.BaseModelList, [], {
                    model: Y.GcImageManager.FileModel,
                    sync: function(action, options, callback) {

                        // Handle different actions
                        switch(action) {

                            // Use to load data from server
                            case 'read':

                                // AJAX call to return directory listing
                                Y.io('/admin/imageManager/list', {
                                    method: 'POST',
                                    data: {
                                        directory: options.directory || '/'
                                    },
                                    on: {
                                        success: function(id, o) {

                                            // Attempt to parse out server response
                                            try {
                                                var results = Y.JSON.parse(o.responseText)
                                            }
                                            catch(e) {
                                                callback('Unable to parse response from server');
                                                return;
                                            }

                                            // Handle different result types
                                            switch(results.status) {
                                                case 'success':
                                                    callback(null, results.results);
                                                    break;
                                                default:
                                                    callback('Unknown response status received from server');
                                                    break;
                                            }
                                        }
                                    }
                                });
                                break;
                            default:
                                callback('Unsupported sync action: ' + action);
                                break;
                        }
                    }
                });

                // Thumbnail view
                Y.GcImageManager.ThumbnailView = Y.Base.create('imageManagerThumbnailView', Y.GcImageManager.BaseView, [], {
                    initializer: function() {

                        // The container we will use for this view
                        this.set('containerTemplate', '#thumbnail-view');

                        // Re-render when model data changes
                        App.get('fileModelList').after(['add', 'remove', 'reset', '*:change'], function(e) {
                            this.renderWithData({
                                imageLocation: config.imageLocation,
                                files: e.currentTarget.toJSON(),
                                insert: (config.mce || window.opener) ? true : false
                            });
                        }, this);

                        // Load model data
                        App.set('directory', '/');
                    },
                    render: function() {
                        this.renderWithData({
                            imageLocation: config.imageLocation,
                            files: App.get('fileModelList').toJSON()
                        });
                    },
                    events: {

                        // Photo container events
                        '#photos .image': {
                            dblclick: function(e) {

                                // Get the image model and handle directories and files differently
                                var model = App.get('fileModelList').getByClientId(e.currentTarget.getData('uid'));
                                if(model.get('isDirectory')) {
                                    App.set('directory', App.get('directory') == '/' ? model.get('filename') : App.get('directory') + '/' + model.get('filename'));
                                    return;
                                }

                                // Insert image
                                model.insertImage();
                            },
                            mouseenter: function(e) {
                                var ele = e.currentTarget.one('.more-info');
                                new Y.Anim({
                                    node: ele,
                                    easing: 'bounceOut',
                                    duration: 0.33,
                                    to: {
                                        height: 50
                                    }
                                }).run();
                            },
                            mouseleave: function(e) {
                                var ele = e.currentTarget.one('.more-info');
                                new Y.Anim({
                                    node: ele,
                                    easing: 'bounceIn',
                                    duration: 0.33,
                                    to: {
                                        height: 0
                                    }
                                }).run();
                            }
                        }
                    }
                });

                // Our app object
                Y.GcImageManager.App = Y.Base.create('imageManagerApp', Y.App, [], {
                    views: {
                        thumbnailView: {
                            type: Y.GcImageManager.ThumbnailView,
                            preserve: true
                        }
                    }
                }, {
                    ATTRS: {
                        directory: {
                            getter: function(val) {
                                return val ? val : '/';
                            },
                            setter: function(value) {

                                // Make sure we have something
                                if(!value) {
                                    return '/';
                                }

                                // Re-load data
                                App.get('fileModelList').load({
                                    directory: value
                                });

                                return value;
                            }
                        },
                        parentDirectory: {
                            getter: function() {

                                // We are at the top
                                if(this.get('directory').indexOf('/') === -1) {
                                    return '/';
                                }

                                var dir = this.get('directory').replace(/\/[^/]*$/i,'');

                                return dir ? dir : '/';
                            }
                        },
                        fileModelList: {
                            valueFn: function() {
                                return new Y.GcImageManager.FileModelList();
                            }
                        },
                        routes: {
                            value: [
                                {
                                    path: '/thumbnail-view', callbacks: function() {
                                    this.showView('thumbnailView');
                                }
                                }
                            ]
                        }
                    }
                });

                // Instantiate our app
                App = new Y.GcImageManager.App({
                    viewContainer: '#app',
                    serverRouting: false,
                    transitions: true
                });

                // Render the app
                App.render();
                App.navigate('/#/thumbnail-view');

                // Do this for the freeze bar
                Y.all('.freeze').on('mouseenter', function(e) {
                    e.currentTarget.transition({
                        duration: 0.33,
                        backgroundColor: 'rgba(255, 255, 255, 1)'
                    });
                });

                Y.all('.freeze').on('mouseleave', function(e) {
                    e.currentTarget.transition({
                        duration: 0.33,
                        backgroundColor: 'rgba(255, 255, 255, 0.8)'
                    });
                });

                // Parent folder button
                Y.one('#parent-folder').on('click', function(e) {
                    App.set('directory', App.get('parentDirectory'));
                });

                // Create folder button
                Y.one('#create-folder').on('click', function(e) {

                    // Template
                    var template = "\n\
                <div class='modal' id='create-folder-modal'>\n\
                    <div class='modal-header'>\n\
                        <a class='close' data-dismiss='modal'>x</a>\n\
                        <h3>Create Folder</h3>\n\
                    </div>\n\
                    <div class='modal-body'>\n\
                        <label for='new-folder-name'>New Folder Name<label>\n\
                        <input type='text' id='new-folder-name' class='input-xlarge'>\n\
                    </div>\n\
                    <div class='modal-footer'>\n\
                    <button type='button' class='btn modal-cancel'>Cancel</a>\n\
                    <button type='button' class='btn btn-success create-folder-confirm'>Continue</a>\n\
                    </div>\n\
                </div>\n\
            ";

                    // Compile the template in to a node with our data we modelled above
                    var mContainer = Y.Node.create(Y.Handlebars.render(template));

                    // Append container to body and setup modal dialog
                    Y.one('body').appendChild(mContainer);
                    $('#' + mContainer.getAttribute('id')).modal({
                        show : true,
                        backdrop : true
                    });
                });

                // Event listener for create folder buttons
                Y.one('body').delegate('click', function() {

                    // New directory (folder) name
                    var newName = Y.one('#new-folder-name').get('value');

                    // Get rid of the modal
                    $('.modal').modal('hide');
                    Y.all('.modal').remove();

                    Y.io('/admin/imageManager/createdirectory', {
                        method: 'POST',
                        data: {
                            directory: App.get('directory'),
                            name: newName
                        },
                        on: {
                            success: function(id, o) {

                                // Attempt to parse out server response
                                try {
                                    var results = Y.JSON.parse(o.responseText)
                                }
                                catch(e) {
                                    admin.dialog('Error', 'Unable to parse response from server');
                                    return;
                                }

                                // Handle different result types
                                switch(results.status) {
                                    case 'success':
                                        // Do nothing
                                        break;
                                    default:
                                        admin.dialog('Error', 'Unknown response status received from server');
                                        break;
                                }

                                // Reload the current directory listing
                                App.set('directory', App.get('directory'));
                            }
                        }
                    });
                }, '.create-folder-confirm');

                // Insert button
                Y.one('body').delegate('click', function(e) {
                    var model = App.get('fileModelList').getByClientId(e.currentTarget.getData('uid'));
                    model.insertImage();
                }, '.insert-image');

                // Delete button
                Y.one('body').delegate('click', function(e) {
                    var model = App.get('fileModelList').getByClientId(e.currentTarget.getData('uid'));
                    model.deleteImage();
                }, '.delete-image');

                // Open folder button
                Y.one('body').delegate('click', function(e) {
                    var model = App.get('fileModelList').getByClientId(e.currentTarget.getData('uid'));
                    App.set('directory', App.get('directory') == '/' ? model.get('filename') : App.get('directory') + '/' + model.get('filename'));
                }, '.open-folder');

                // Generic modal cancel button
                Y.one('body').delegate('click', function(e) {
                    $('.modal').modal('hide');
                    Y.all('.modal').remove();
                }, '.modal-cancel');

                // Upload button
                Y.one('#upload').on('click', function(e) {

                    // Template
                    var template = "\n\
                <div class='modal' id='upload-modal'>\n\
                    <div class='modal-header'>\n\
                        <a class='close' data-dismiss='modal'>x</a>\n\
                        <h3>Upload</h3>\n\
                    </div>\n\
                    <div class='modal-body'>\n\
                        <div id='uploader'></div>\n\
                        <div style='margin-top: 10px;'>\n\
                            <b>Selected Files</b>\n\
                            <hr style='margin: 3px;'>\n\
                        </div>\n\
                        <div id='upload-list' style='height: 275px; overflow-y: scroll;'></div>\n\
                    </div>\n\
                    <div class='modal-footer'>\n\
                    <button type='button' class='btn modal-cancel'>Cancel</a>\n\
                    <button type='button' class='btn btn-success upload-confirm'>Upload</a>\n\
                    </div>\n\
                </div>\n\
            ";

                    // Compile the template in to a node with our data we modelled above
                    var mContainer = Y.Node.create(Y.Handlebars.render(template));

                    // Append container to body and setup modal dialog
                    Y.one('body').appendChild(mContainer);
                    $('#' + mContainer.getAttribute('id')).modal({
                        show : true,
                        backdrop : true
                    });

                    // YUI Uploader
                    Y.Uploader.SELECT_FILES_BUTTON = '<button type="button" class="btn btn-info" role="button" aria-label="{selectButtonLabel}" tabindex="{tabIndex}" style="width: auto !important; height: auto !important;"><i class="icon-th-list icon-white"></i>{selectButtonLabel}</button>';
                    var uploader = new Y.Uploader({
                        multipleFiles: true,
                        fileFieldName: 'Filedata[]'
                    });

                    // When files are selected
                    uploader.on('fileselect', function(e) {

                        // Get a list of the files
                        var fileList = [];
                        Y.Array.each(uploader.get('fileList'), function(o) {
                            fileList.push(o.get('name'));
                        });

                        // Template for file uploads
                        var template = "\n\
                    <ul>\n\
                        {{#each files}}\n\
                        <li>{{.}}</li>\n\
                        {{/each}}\n\
                    </ul>\n\
                ";

                        // Render template
                        Y.one('#upload-list').setHTML(Y.Node.create(Y.Handlebars.render(template, {
                            files: fileList
                        })));
                    });

                    // When uploading starts
                    uploader.on('uploadstart', function(e) {
                        Y.Gc.Widgets.LoadingBar.show();
                    });

                    // When uploading completes
                    uploader.on('alluploadscomplete', function(e) {
                        Y.Gc.Widgets.LoadingBar.hide();
                        $('.modal').modal('hide');
                        Y.all('.modal').remove();
                        App.set('directory', App.get('directory'));
                    });

                    // Render uploader
                    uploader.render('#uploader');

                    // Upload confirm button
                    Y.one('.upload-confirm').on('click', function(e) {
                        uploader.uploadAll('/admin/imageManager/upload', {
                            directory: App.get('directory')
                        });
                    });
                });
            });
        </script>
    </body>
</html>