<?php
    /** Strip out all of the css assets and javascript assets that are module defined
     *  This should in effect leave on the core script technologies (jquery, jquery-ui) */
    $this->getModule()->cssAssets = array();
    $this->getModule()->javaScriptAssets = array();

    /** Register the remaining scripts (core) */
    $this->getModule()->registerScripts();

    echo $content;
