<?php $this->beginContent('/layouts/admin'); ?>
	<div id="sidebar">
<?
		$this->widget('admin_widgets.StickyNotesWidget.StickyNotesWidget');
		$this->widget('admin_widgets.StickyTasksWidget.StickyTasksWidget');
?>
	</div><!-- sidebar -->
	<?php echo $content; ?>
<?php $this->endContent(); ?>