<?php $this->breadcrumbs = array(
	'My Badges' => '',
);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/badges/addPoint/1" class="button gray icon i_stm_add">Send a Thank You</a>
</div>
<h1>
	<?php
		if (empty($BadgePointReceivedDataProvider->totalItemCount)) {
			echo 'You have no badges.';
		} elseif ($BadgePointReceivedDataProvider->totalItemCount == 1) {
			echo 'You have 1 ' . $Badge->name . ' Badge.';
		} else {
			echo 'You have ' . $BadgePointReceivedDataProvider->totalItemCount . ' "' . $Badge->name . '" Badges.';
		}
	?>
</h1>
<?php
	$this->beginStmPortletContent(array(
			'handleTitle' => 'Your Badge Collection!',
			'handleIconCss' => 'i_wizard'
		)
	);
?>
<div id="action-plans-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container">
			<tr>
				<td colspan="2" class="p-tc">
					<?php for ($i = 0; $i < $BadgePointReceivedDataProvider->totalItemCount; $i++) {
						echo '<img src="'.$this->getModule()->getCdnImageAssetsUrl().'badges/' . $Badge->image_filename . '" height="50"> &nbsp;';
					}?>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="p-tc">
					You have given out <?php echo $BadgePointSentDataProvider->totalItemCount . ' "' . $Badge->name; ?>" badges.
				</td>
			</tr>
		</table>
	</div>
</div>
<?php $this->endWidget(); ?>

<?php
$badgeImage = '<img style="float: left;" src="'.$this->getModule()->getCdnImageAssetsUrl().'badges/' . $Badge->image_filename . '">';

$this->widget('zii.widgets.jui.CJuiTabs', array(
		   'tabs' => array(
			   'Sent Badges (' . $BadgePointSentDataProvider->totalItemCount . ')' => array(
				   'content' => $this->renderPartial('_listSent', array(
																  'DataProvider' => $BadgePointSentDataProvider,
																  'Badge'=>$Badge,
																  'badgeImage'=>$badgeImage,
																  ), true
				   ),
				   'id' => 'sent-badges-tab',
			   ),
			   'Received Badges (' . $BadgePointReceivedDataProvider->totalItemCount . ')' => array(
				   'content' => $this->renderPartial('_listReceived', array(
																  'DataProvider' => $BadgePointReceivedDataProvider,
																  'Badge'=>$Badge,
																  'badgeImage'=>$badgeImage,
																  ), true
				   ),
				   'id' => 'received-badges-tab',
			   ),
		   ),
		   'id' => 'badges-tab-widget',
		   )
		);
?>
