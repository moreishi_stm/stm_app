<?php
$this->widget('admin_module.components.StmGridView', array(
		'id' => 'badges-grid',
		'dataProvider' => $DataProvider,
		'itemsCssClass' => 'datatables',
		'extraParams'   =>array('badgeImage'=>$badgeImage),
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Date',
				'value' => 'Yii::app()->format->formatDateTime($data->added)',
				'htmlOptions' => array('style' => 'width: 170px;'),
			), //
			array(
				'type' => 'raw',
				'name' => 'Badge Type',
				'value' => '$this->grid->extraParams["badgeImage"]."<br /> &nbsp;Thank You"',
				//@todo: needs to be dynamic once we figure out our final design
				'htmlOptions' => array('style' => 'width: 150px; vertical-align:middle;'),
			),
			array(
				'type' => 'raw',
				'name' => 'To',
				'value' => '$data->contact->fullName',
				'htmlOptions' => array('style' => 'width: 150px;'),
			),
			array(
				'type' => 'raw',
				'name' => 'Personal Note',
				'value' => '$data->note',
			),
		),
	)
);
?>
