<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->id.'/badges'),
);

Yii::app()->clientScript->registerScript('search', "
$('#badges-form').submit(function() {
	$.fn.yiiGridView.update('badges-grid', { data: $(this).serialize() });
	return false;
});
");

$form=$this->beginWidget('CActiveForm', array(
										'id'=>'badges-form',
										'action'=>'',
										'method'=>'get',
										));

?>
<!--<div id="listview-actions">-->
<!--	<a href="/<?php //echo Yii::app()->controller->module->id; ?>/badges/add" class="button gray icon i_stm_add">Add New Badge</a>-->
<!--</div>-->
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
			'fromName'=>'from_date',
			'toName'=>'to_date',
//			'fromDateLabelSelector' => '',
//			'toDateLabelSelector'   => '',
			'gridName'              => 'tasks-grid',
			'isForm'                => false,
			'formSelector'          => '#badges-form',
			'defaultSelect'         => 'this_month',
			'datePresetList'        => array('all',
										  'today',
										  'last_1_week',
										  'this_month',
										  'this_year',
										  'custom'
			),
			'container'             => array('tag'         => 'div',
										  'htmlOptions' => array('class' => 'p-fr','style'=>'position: absolute;top: 10px;right: 5px;')
			)
			));
?>
<h1>Badges List</h1>
<h3 id="date-range-label"><label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date']));?></label> - <label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date']));?></label></h3>

<div class="p-clr"></div>
<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'badges-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'To',
				'value' => 'Yii::app()->format->formatDate($data->added)',
				'htmlOptions' => array('style' => 'width: 100px;'),
			),
			array(
				'type' => 'raw',
				'name' => 'To',
				'value' => '$data->contact->fullName',
				'htmlOptions' => array('style' => 'width: 150px;'),
			),
			array(
				'type' => 'raw',
				'name' => 'From',
				'value' => '$data->addedBy->fullName',
				'htmlOptions' => array('style' => 'width: 150px;'),
			),
			'note',
//			array(
//				'type' => 'raw',
//				'name' => 'Badge Type',
//				'value' => '"Thank You"',
//				//@todo: needs to be dynamic once we figure out our final design
//				'htmlOptions' => array('style' => 'width: 150px;'),
//			),
		),
	)
);
$this->endWidget();
?>
