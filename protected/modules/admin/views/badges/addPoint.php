<?php $this->breadcrumbs = array(
	'Add Point' => '',
);

	Yii::app()->clientScript->registerScript('add-badge-point', '
$("form#add-new-badge-point-form").submit(function() {
	$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
});
'
	);
?>
	<h1>Send a <?php echo $Badge->name; ?> Badge</h1>
<?php
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'add-new-badge-point-form',
			'enableAjaxValidation' => false,
		)
	);
	$this->beginStmPortletContent(array(
			'handleTitle' => 'Award a Badge!',
			'handleIconCss' => 'i_wizard'
		)
	);
?>
	<div id="action-plans-container">
		<div class="g12 p-mb5 rounded-text-box odd-static">
			<table class="container">
				<tr>
					<td colspan="2" class="p-tc"><img src="<?php echo $this->getModule()->getCdnImageAssetsUrl(); ?>badges/<?php echo $Badge->image_filename; ?>" height="50">
					</td>
				</tr>
				<tr>
					<th>From:</th>
					<td><?php echo Yii::app()->user->fullName; ?>
					</td>
				</tr>
				<tr>
					<th>To:</th>
					<td>
						<?php
							echo $form->dropDownList($model, 'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName'), array(
									'empty' => ' ',
									'class' => 'chzn-select g6',
									'data-placeholder' => 'Select Name'
								)
							);
							$this->widget('admin_module.extensions.EChosen.EChosen', array(
									'target' => 'select',
									'options' => array(
										'allow_single_deselect' => true,
									),
								)
							);?>
						<?php echo $form->error($model, 'contact_id'); ?>
					</td>
				</tr>
				<tr>
					<th class="narrow">Note:</th>
					<td>
						<?php echo $form->textArea($model, 'note', $htmlOptions = array(
								'placeholder' => 'Write a personal note...',
								'class' => 'g6',
								'rows' => 10
							)
						); ?>
						<?php echo $form->error($model, 'note'); ?>
					</td>
				</tr>
			</table>
			<?php $model->points_earned = 1;
				echo $form->hiddenField($model, 'points_earned');?>
		</div>
	</div>
<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper">
		<?php echo CHtml::htmlButton('Send Badge', $htmlOptions = array(
				'class' => 'submit wide',
				'type' => 'submit',
			)
		); ?>
	</div>
<?php $this->endWidget(); ?>