<style type="text/css">

    #ivr-info-container {
        margin-bottom: 50px;
    }

    #ivr-form-container {
        max-width: 650px;
        margin: 0 auto;
        text-align: center;
    }

    .ivr-error {
        background-color: lightcoral;
        border-radius: 4px;
        padding: 10px;
        margin: 20px 0px;
        color: white;
    }
    .ivr-error,
    .ivr-error p
    {
        font-size: 16px;
    }
</style>

<?
Yii::app()->clientScript->registerScript('ivrRecordScript', <<<JS
    $('button#call-now-button').click(function(){

        var phone = $('#phone').val().replace( /\D+/g, '');
        if(phone=='') {
            alert('Please enter a phone #');
            return false;
        }
        if(phone.length != 10) {
            alert('Please enter a valid phone #');
            return false;
        }

        $("body").prepend('<div class="loading-container loading"><em></em></div>');

        setTimeout(function(){ $("div.loading-container.loading").remove(); }, 10000);

        $.post('', { phone: phone}, function(data){
            if(data.status=='success') {
                Message.create('success','Calling your phone now...');
            }
            else if(data.status=='error') {
                Message.create('error','Unexpected Error. Please try again or contact support. Error Message: ' + data.errorMessage);
            }
        },'json');

        return false;
    });
JS
);

$this->breadcrumbs=array(
    'Record'=>''
);?>

<section id="ivr-info-container">
    <h1>Record <?=$label?> Greeting</h1>
    <p style="font-size: 18px; text-align: center; margin-top: 20px;">
        The system will call your phone number to record your greeting.
        <ol style="margin-left: 33%; font-size: 15px; margin-top: 20px;">
            <li>Enter your 10 digit phone number and press the "Call" button.</li>
            <li>Record your greeting after the tone as instructed.</li>
            <li>Hangup after you complete your recording.</li>
        </ol>
    </p>

    <?if($phoneFail) : ?>
    <div class="ivr-error">
        <b>Error!</b>
        <p>The phone number you entered is not a valid phone number. Please make sure you enter 10 numeric digits only.</p>
    </div>
    <?endif;?>
</section>

<section id="ivr-form-container">
    <form action="/admin/ivr/record?id=<?=$ivr->id?>&type=<?=$theType?>" method="post">
        <?$this->widget('StmMaskedTextField', array(
                'name' => 'phone',
                'mask' => '(999) 999-9999',
                'htmlOptions' => array(
                    'placeholder' => '(999) 999-9999',
                    'class' => 'g8',
                    'style' => 'font-size: 45px;',
                ),
            )
        );?>
        <button id="call-now-button" type="submit" class="g3" style="font-size: 15px; padding: 14px; float: left;">Call Now to Record</button>
    </form>
    <h1 style="margin-top: 50px; float: left;">
        <a href="/admin/<?=$controller?>/<?=$action?>/<?=$_GET['id']?>">Return to <?=$label?></a>
    </h1>
</section>