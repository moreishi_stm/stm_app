<?
$this->breadcrumbs=array(
    $this->action->id == 'edit' ? 'Edit' : 'Add'=>''
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'ivr-source-form',
        'enableAjaxValidation' => false
    )
);
?>
    <h1><?=$this->action->id == 'editSource' ? 'Edit' : 'Add'?> IVR Source</h1>
<?
$this->beginStmPortletContent(array(
        'handleTitle' => 'IVR Source Details',
        'handleIconCss' => 'i_wizard'
    )
);
?>
    <div id="action-plans-container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th><label>Extension #</label>:</th>
                    <td>
                        <?php $this->widget('StmMaskedTextField', array(
                                'model' => $model,
                                'attribute' => 'extension_suffix',
                                'mask' => '99',
                                'htmlOptions' => array(
                                    'placeholder' => '99',
                                    'class' => 'g1',
                                ),
                            )
                        );
                        ?>
                        <?php echo $form->error($model, 'extension_suffix'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'source_id'); ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($model, 'source_id', CHtml::listData(Sources::model()->findAll(array('order'=>'name ASC')), 'id', 'name'), $htmlOptions=array('empty'=>'','data-placeholder'=>'Select a Source'));?>
                        <?php echo $form->error($model, 'source_id'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper">
        <button type="submit" class="submit wide">Save IVR Source</button>
    </div>
<?php $this->endWidget(); ?>