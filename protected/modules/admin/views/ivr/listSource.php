<?
$this->breadcrumbs=array(
    'List Source'=>''
);?>
    <div id="listview-actions">
        <a href="/admin/ivr/addSource" class="button gray icon i_stm_add">Add Extension Source</a>
    </div>
    <div id="content-header">
        <h1>IVR Sources</h1>
    </div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'ivr-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        'source.name',
        'extension_suffix',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/admin/ivr/editSource/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a>"
			',
            'htmlOptions'=>array('style'=>'width:80px;'),
        ),
    ),
));
?>