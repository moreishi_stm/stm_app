<?
$this->breadcrumbs=array(
    'List'=>''
);?>
    <div id="listview-actions">
        <a href="/admin/ivr/addExtension/id/<?=$model->ivr->id?>" class="button gray icon i_stm_add">Add IVR Extension</a>
    </div>
    <div id="content-header">
        <h1>IVR Extensions for <?=$model->ivr->name?></h1>
    </div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'ivr-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        'name',
        'description',
        'extension',
        array(
            'type'  =>  'raw',
            'name'  =>  'Assigned Hunt Group',
            'value' =>  '$data->callHuntGroup->name'
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
                "<a href=\"/admin/voicemail?id=" . $data->id . "&type=extension" . "\" class=\"button gray icon i_stm_search grey-button\">Voicemails</a>" .
				"<a href=\"/admin/ivr/editExtension/" . $data->id . "\" class=\"button gray icon i_stm_edit grey-button\">Edit</a>"
			',
            'htmlOptions'=>array('style'=>'width:190px;'),
        ),
    ),
));
?>