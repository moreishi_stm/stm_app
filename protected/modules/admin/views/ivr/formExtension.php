<?
require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmAws.S3')."/FileManagement.php");
$fileExists = (file_exists(\StmAws\S3\FileManagement::getBaseUrlByType(\StmAws\S3\FileManagement::SITE_FILES_BUCKET_TYPE).'/'.$clientId.'/ivrExtension/'.$model->id.'.mp3')) ? '1' : '0';
Yii::app()->clientScript->registerScript('ivrRecordScript', <<<JS
    $('#record-button').click(function(){
        if($fileExists) {
            if(!confirm('Are you sure you want to replace your current recording?')) {
                return false;
            }
        }
    });
JS
);

$this->breadcrumbs=array(
    $this->action->id == 'editExtension' ? 'Edit' : 'Add'=>''
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'ivr-extension-form',
        'enableAjaxValidation' => false
    )
);
?>
    <h1><?=$this->action->id == 'editExtension' ? 'Edit' : 'Add'?> IVR Extension</h1>
<?
$this->beginStmPortletContent(array(
        'handleTitle' => 'IVR Extension Details',
        'handleIconCss' => 'i_wizard'
    )
);
?>
    <div id="action-plans-container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'name'); ?>:</th>
                    <td>
                        <?php echo $form->textField($model, 'name', $htmlOptions = array(
                                'placeholder' => 'Name',
                                'class' => 'g7',
                            )
                        ); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'extension'); ?>:</th>
                    <td>
                        <?php echo $form->textField($model, 'extension', $htmlOptions = array(
                                'placeholder' => 'Ext #',
                                'class' => 'g1',
                                'maxLength' => 3,
                            )
                        ); ?> <span class="label" style="padding-top: 10px; padding-left: 6px;">Must be a 2 or 3 digit numbers.</span>
                        <?php echo $form->error($model, 'extension'); ?>
                    </td>
                </tr>
                <?if($this->action->id == 'editExtension'):?>
                <tr>
                    <th class="narrow">
                        <label>Recording:</label>
                    </th>
                    <td>
                        <?if($model->record_url):?>
                        <audio controls="controls" style="display: block;" class="g7">
                            <source src="<?=\StmAws\S3\FileManagement::getBaseUrlByType(\StmAws\S3\FileManagement::SITE_FILES_BUCKET_TYPE)?>/<?=$clientId?>/ivrExtension/<?=$model->id?>.mp3">
                            Error, your web browser does not support the required audio playback functionality. Please upgrade your browser to preview the recording.
                        </audio>
                        <?endif;?>
                        <a id="record-button" href="/admin/ivr/record/<?=$model->id?>?type=extension" class="btn">Record New</a>
                    </td>
                </tr>
                <?endif;?>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'description'); ?>:</th>
                    <td>
                        <?php echo $form->textField($model, 'description', $htmlOptions = array(
                                'placeholder' => 'Description',
                                'class' => 'g7',
                            )
                        ); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'call_hunt_group_id'); ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($model, 'call_hunt_group_id', CHtml::listData(CallHuntGroups::model()->findAll(array('order'=>'name ASC')), 'id', 'name'), $htmlOptions=array('empty'=>'','data-placeholder'=>'Select a Hunt Group'));?>
                        <?php echo $form->error($model, 'call_hunt_group_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'speech'); ?>:</th>
                    <td>
                        <?php echo $form->textArea($model, 'speech', $htmlOptions = array(
                                'class' => 'g11',
                                'rows' => '20'
                            )
                        ); ?>
                        <?php echo $form->error($model, 'speech'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'do_speech'); ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($model, 'do_speech', StmFormHelper::getYesNoList(false)); ?>
                        <?php echo $form->error($model, 'do_speech'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper">
        <button type="submit" class="submit wide">Save IVR</button>
    </div>
<?php $this->endWidget(); ?>