<?
$this->breadcrumbs=array(
    'List'=>''
);?>
    <div id="listview-actions">
        <a href="/admin/ivr/listSource" class="button gray icon i_stm_search">IVR Source Numbers</a>
        <a href="/admin/ivr/add" class="button gray icon i_stm_add">Add New IVR</a>
    </div>
    <div id="content-header">
        <h1>IVR</h1>
    </div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'ivr-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        'name',
        'description',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/admin/ivr/listExtension/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">Extensions</a>"
			',
            'htmlOptions'=>array('style'=>'width:120px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/admin/ivr/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Settings</a>"
			',
            'htmlOptions'=>array('style'=>'width:80px;'),
        ),
    ),
));
?>