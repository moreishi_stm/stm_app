<?php
$this->breadcrumbs=array(
	'Campaigns'=>'/'.Yii::app()->controller->module->name.'/sources/campaigns',
	'List'=>'',
);

?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name;?>/sources/addCampaigns" class="button gray icon i_stm_add">Add New Campaign</a>
	<a href="/<?php echo Yii::app()->controller->module->name;?>/sources" class="button gray icon i_stm_search">View Sources</a>
</div>
<div id="content-header">
	<h1>Campaign List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBoxCampaigns',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'campaigns-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'name',
		'description',
        // array(
        //   'type'=>'raw',
        //   'name'=> '# Campaigns',
        //   'value'=> 'count($data->sourceCampaigns)'
        // ),
	    array(
			'type'=>'raw',
	        'name'=>'Source',
	        'value'=>'$data->source->name',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'buttons'=>array(
			    'view'=>array(
					'url' =>'Yii::app()->controller->createUrl("/".Yii::app()->controller->module->name."/sources/editCampaigns",array("id"=>$data->id,))',
	            	'imageUrl'=>$this->module->imageAssetsUrl.'/search.png',
			    ),
            ),
		),
	),
));
