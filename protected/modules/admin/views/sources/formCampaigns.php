<?php
$action = ($this->action->id == 'addCampaigns') ? 'Add' : 'Edit';
$this->breadcrumbs=array(
    'Campaigns'=>'/'.Yii::app()->controller->module->id.'/sources/campaigns',
    $action=>'',
);
?>
<div id="featured-areas-header">
	<h1 class="name">Add New Campaign</h1>
</div>
<?php echo $this->renderPartial('_formCampaigns', array('model'=>$model)); ?>