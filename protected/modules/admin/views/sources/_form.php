<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableAjaxValidation'=>false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'=>'Sources of Business',
		'handleIconCss'=>'i_strategy'
	));
	?>
	<div id="action-plans-container">
		<div class="g12 p-mb5 rounded-text-box odd-static" style="padding:30px 0 30px 0;">
	        <table class="container">
	        	<tr>
	        		<th>Name:</th>
	        		<td colspan="3">
			    		<?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Source Name', 'class'=>'g6', ));?>
						<div class="p-clr p-pt10">
							<?php echo CHtml::checkBox('isSubsource');?>
							<span class="label">Subsource of:</span>
							<?php echo $form->dropDownList($model, 'parent_id', CMap::mergeArray(array(''=>''),CHtml::listData(Sources::model()->findAll(), 'id', 'name')), $htmlOptions = array('style'=>'width:250px;','data-placeholder'=>' ','disabled'=>$subSourceDisabled,'class'=>'chzn-select'
								));

							$this->widget('admin_module.extensions.EChosen.EChosen', array(
																					 'target' => 'select#Sources_parent_id',
																					 'options'=>array('allow_single_deselect'=>true),
																					 ));

							?>
						</div>
        				<?php echo $form->error($model,'name'); ?><?php echo $form->error($model,'parent_id'); ?>

					</td>
	        	</tr>
	        	<tr>
	        		<th>Description:</th>
	        		<td colspan="3">
			    		<?php echo $form->textField($model,'description', $htmlOptions=array('placeholder'=>'Description', 'class'=>'g6', ));?>
        				<?php echo $form->error($model,'description'); ?>
	            	</td>
	        	</tr>
<!--                <tr>-->
<!--                    <th># Days to Follow-up:</th>-->
<!--                    <td colspan="3">-->
<!--                        --><?php //echo $form->textField($model,'follow_up_days', $htmlOptions=array('placeholder'=>'Follow-up Days', 'class'=>'g6', ));?>
<!--                        --><?php //echo $form->error($model,'follow_up_days'); ?>
<!--                    </td>-->
<!--                </tr>-->
	        </table>
		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Source</button></div>
<?php $this->endWidget(); ?>