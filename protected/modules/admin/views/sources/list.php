<?php
$this->breadcrumbs=array(
	'List'=>'',
);
$module = Yii::app()->controller->module->name;
$deleteScript = <<<JS
	$( '.delete-source-button' ).live( "click", function() {
		if(confirm('Are you sure you want to delete this Source?')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var id = $(this).data('id');
			$.post('/$module/sources/delete/'+id, function(data) {
				$("div.loading-container.loading").remove();
				if(data.deleted) {
					Message.create("success","Source deleted successfully.");
					$.fn.yiiGridView.update("sources-grid", { data: $(this).serialize() });
				} else
					Message.create("error","Error: Source did not delete.");
			},"json");
		}
	});

	$( '.undelete-source-button' ).live( "click", function() {
		if(confirm('Are you sure you want to Un-Delete this Source?')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var id = $(this).data('id');
			$.post('/$module/sources/undelete/'+id, function(data) {
				$("div.loading-container.loading").remove();
				if(data.undeleted) {
					Message.create("success","Source Undeleted successfully.");
					$.fn.yiiGridView.update("sources-grid", { data: $(this).serialize() });
				} else
					Message.create("error","Error: Source did not Undelete.");
			},"json");
		}
	});
JS;

Yii::app()->clientScript->registerScript('sourceDeleteScript', $deleteScript);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name; ?>/sources/add" class="button gray icon i_stm_add">Add New Source</a>
</div>
<div id="content-header">
	<h1>Sources List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBox',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'sources-grid',
	'dataProvider'=>$model->searchList(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Name',
            'value'=>'$data["name"]',
        ),
        array(
			'type'=>'raw',
			'name'=>'Subsource of',
			'value'=>'$data["parent_name"]',
		),
        array(
            'type'=>'raw',
            'name'=>'Description',
            'value'=>'$data["description"]',
        ),
//        array(
//            'type'=>'raw',
//            'name'=>'Follow-up Days',
//            'value'=>'$data["follow_up_days"]',
//        ),
		array(
			'type'=>'raw',
			'name'=>'',
            'visible' => !$model->is_deleted,
			'value'=>'($data["is_deleted"]==0)? "<div><a href=\"/".Yii::app()->controller->module->name."/sources/".$data["id"]."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>" : ""',
			'htmlOptions'=>array('style'=>'width:90px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'($data["is_deleted"]==0)?"<div><a href=\"javascript:void()\" class=\"button gray icon i_stm_delete grey-button delete-source-button \" data-id=\"".$data["id"]."\">Delete</a></div>" : "<div><a href=\"javascript:void()\" class=\"button gray icon i_stm_add grey-button undelete-source-button \" data-id=\"".$data["id"]."\">Un-Delete</a></div>"',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
	),
));
