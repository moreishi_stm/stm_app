<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableAjaxValidation'=>false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'=>'Sources of Business',
		'handleIconCss'=>'i_strategy'
	));
	?>
	<div id="action-plans-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
	        <table class="container">
	        	<tr>
	        		<th class="narrow"><?php echo $form->labelEx($model, 'source_id')?></th>
	        		<td colspan="3">
						<?php echo $form->dropDownList($model, 'source_id', CHtml::listData(Sources::getList(), 'id', 'name'),$htmlOptions=array(
							'empty'=>'Select One'
						)); ?>
        				<?php echo $form->error($model,'source_id'); ?>
	            	</td>
	        	</tr>
	        	<tr>
	        		<th class="narrow">Name:</th>
	        		<td colspan="3">
			    		<?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Campaign Name', 'class'=>'g9', ));?>
        				<?php echo $form->error($model,'name'); ?>
	            	</td>
	        	</tr>
	        	<tr>
	        		<th class="narrow">Description:</th>
	        		<td colspan="3">
			    		<?php echo $form->textField($model,'description', $htmlOptions=array('placeholder'=>'Description', 'class'=>'g9', ));?>
        				<?php echo $form->error($model,'description'); ?>
	            	</td>
	        	</tr>
	        </table>

		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Source</button></div>
<?php $this->endWidget(); ?>