<?
Yii::app()->clientScript->registerScript('search', <<<JS
	$('#listview-search form').submit(function() {
	    $.fn.yiiGridView.update('sources-grid', {
	        data: $(this).serialize()
	    });
	    return false;
	});

	$('#Sources_is_deleted').change(function() {
	    if($(this).is(":checked")) {
	        $(this).val(1);
	    }
	    else {
            $(this).val(0);
	    }
	    $.fn.yiiGridView.update('sources-grid', {
	        data: $("#listview-search form").serialize()
	    });
	    return false;
	});
JS
);

 $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sources-list-search',
)); ?>
	<div class="g3">
	</div>
	<div class="g4">
		<label class="g2">Name:</label>
		<span class="g10"><?php echo $form->textField($model,'name');?></span>
	</div>
    <div class="g3">
        <label class="g6">Deleted:</label>
        <span class="g6"><?php echo $form->checkBox($model, 'is_deleted', array('style'=>'margin-top: 10px;','value'=>0, 'checked'=>false));?></span>
    </div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
