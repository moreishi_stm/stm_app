<?
$this->breadcrumbs=array(
    'List'
);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/accounting/addExpense" class="button gray icon i_stm_add">Add ROI Expense</a>
    <a href="/<?php echo Yii::app()->controller->module->id; ?>/accounting/roiReport" class="button gray icon i_stm_search">View ROI Report</a>
</div>
<div id="content-header">
	<h1>ROI Expenses</h1>
</div>
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
			   'id' => 'referrals-grid',
			   'dataProvider' => $model->search(),
			   'itemsCssClass' => 'datatables',
			   'columns' => array(
				   'date:date',
				   'source.name',
				   'amount:dollars',
				   'notes',
				   'reference_number',
				   array(
					   'type' => 'raw',
					   'name' => '',
					   'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/accounting/editExpense/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\" target=\"_blank\">Edit</a></div>"',
					   'htmlOptions' => array('style' => 'width:80px'),
				   ),
			   ),
			   )
);