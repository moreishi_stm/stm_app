<div class="g12" style=" top:2px;">
    <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'roi-grid','isForm'=>true)); ?>
</div>
<?
$this->breadcrumbs=array(
    'List'
);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/accounting/roi" class="button gray icon i_stm_search">ROI Expenses</a>
</div>

<div id="content-header">
	<h1>ROI Report</h1>
</div>
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
			   'id' => 'roi-grid',
			   'dataProvider' => $dataProvider,
			   'itemsCssClass' => 'datatables',
			   'columns' => array(
				   array(
					   'type' => 'raw',
					   'name' => 'Source of Business',
					   'value' => '$data["name"]',
				   ),
                   array(
                       'type' => 'raw',
                       'name' => 'Amount',
                       'value' => 'Yii::app()->format->formatDollars($data["amount"])',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '# Seller Leads',
                       'value' => '$data["sellerLeadCount"]',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '# Buyer Leads',
                       'value' => '$data["buyerLeadCount"]',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '# Total Leads',
                       'value' => '$data["sellerLeadCount"] + $data["buyerLeadCount"]',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '$ per Lead',
                       'value' => '(($data["sellerLeadCount"] + $data["buyerLeadCount"]) ? Yii::app()->format->formatDollars($data["amount"] / ($data["sellerLeadCount"] + $data["buyerLeadCount"])) : "")',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '# Seller Closings',
                       'value' => '$data["sellerClosingCount"]',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '# Buyer Closings',
                       'value' => '$data["buyerClosingCount"]',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '# Total Closings',
                       'value' => '$data["sellerClosingCount"] + $data["buyerClosingCount"]',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '$ per Closings',
                       'value' => '(($data["sellerClosingCount"] + $data["buyerClosingCount"]) ? Yii::app()->format->formatDollars($data["amount"] / ($data["sellerClosingCount"] + $data["buyerClosingCount"])) : "")',
                   ),
			   ),
			   )
);