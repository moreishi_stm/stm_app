<?
$this->widget('admin_module.extensions.moneymask.MMask', array(
														 'element' => '#AccountingTransactions_amount',
														 'currency' => 'PHP',
														 'config' => array(
															 'precision' => 2,
															 'symbol' => '$',
															 'showSymbol' => true,
															 'symbolStay' => true,
														 )
														 ));
$actionType = ($this->action->id == 'editExpense') ? 'Edit' : 'Add';
$this->breadcrumbs=array(
    $actionType.' Expense' => '',
);
?>
<?php $form = $this->beginWidget('CActiveForm', array(
												'id' =>' add-referral-form',
												'enableAjaxValidation' => false,
												));
$this->beginStmPortletContent(array(
							  'handleTitle'   => 'Add Expense',
							  'handleIconCss' => 'i_wizard'
							  ));
?>
<div id="referral-container" >
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<div class="g6">
			<table class="container">
				<tr>
					<th><?php echo $form->labelEx($model, 'date'); ?>:</th>
					<td>
						<?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
																			  'model' => $model,
																			  'attribute' => 'date',
																			  'options' => array(
																				  'showAnim' => 'fold',
																			  ),
																			  'htmlOptions' => array(
																				  'class' => 'g6',
																				  'placeholder'=>'Expense Date'
																			  ),
																			  ));
						?>
						<?php echo $form->error($model,'date'); ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'amount'); ?></th>
					<td>
						<?php echo $form->textField($model, 'amount', $htmlOptions=array('class'=>'g6','placeholder'=>'Amount')); ?>
						<?php echo $form->error($model,'amount'); ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="g6">
			<table class="container">
				<tr>
					<th class="narrow">Reference #:</th>
					<td>
						<?php echo $form->textField($model, 'reference_number', $htmlOptions=array('class'=>'g6','placeholder'=>'Reference Number')); ?>
						<?php echo $form->error($model,'reference_number'); ?>
					</td>
				</tr>
				<tr>
					<th class="narrow"><?php echo $form->labelEx($model, 'source_id'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'source_id', CMap::mergeArray(array(''=>''),Sources::optGroupList($model->source_id)), $htmlOptions=array('style' => 'width:325px;',
																																						'data-placeholder' => 'Select a Source',
							)); ?>
						<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#AccountingTransactions_source_id',
																							 'options'=>array('allow_single_deselect'=>true),
																					   )); ?>
						<?php echo $form->error($model,'source_id'); ?>
					</td>
				</tr>
			</table>
		</div>
		<table class="container">
			<tr>
				<th class='narrow'>
					<?php echo $form->labelEx($model, 'notes'); ?>:
				</th>
				<td>
					<?php echo $form->textArea($model, 'notes', $htmlOptions = array(
							'class' => 'g11',
							'rows' => '8'
						)
					); ?>
					<?php echo $form->error($model, 'notes'); ?>
                    <?php echo $form->hiddenField($model,'remove'); ?>
				</td>
			</tr>
		</table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper">
		<?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Add New' : 'Save') . ' Expense', $htmlOptions = array(
				'class' => 'submit wide',
				'type' => 'submit',
			)); ?>
	</div>
<?php $this->endWidget(); ?>