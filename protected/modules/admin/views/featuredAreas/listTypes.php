<?php
$this->breadcrumbs=array(
	'List'=>array('index'),
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('featured-area-types-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/featuredAreas/addTypes" class="button gray icon i_stm_add">Add Featured Area Type</a>
</div>

<h1>Featured Areas Types</h1>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBoxTypes',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'featured-area-types-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'name',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
                "<div><a href=\"/admin/featuredAreas/editTypes/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
            ',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
	),
));