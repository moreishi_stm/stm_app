<?php
$this->breadcrumbs=array(
    'Edit'=>'',
    $model->name=>'',
);


$form=$this->beginWidget('CActiveForm', array(
    'id'=>'featured-areas-form',
    'enableAjaxValidation'=>false,
)); ?>
	<!-- ====== FEATURED AREA INFO =============================================================================================== -->
	<div id="top-data" class="row">
		<div class="g100 p-p0 p-m0">
			<? $this->beginStmPortletContent(array(
				'handleTitle'=>'Featured Area Type',
				'handleIconCss'=>'i_map'
			)); ?>
			<div id="featured-area-container">
				<div class="g12 p-mb5 rounded-text-box odd-static">
					<div class="g7 p-fl">
                        <table class="container">
			            	<tr><th>Name:</th>
				            	<td>
						            <?php echo $form->textField($model, 'name', $htmlOptions=array('class'=>'g12'))?>
				            	</td>
			            	</tr>
			            </table>
			        </div>
				</div>
			</div>
		</div>
	</div>
    <!-- ====== END SECTIONS =============================================================================================== -->
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit</button></div>

    <?php $this->endWidget(); ?>

<?php $this->endStmPortletContent(); ?>
