<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'featued-areas-list-search',
)); ?>
	<div class="g3">
		<label class="g3">Name:</label>
		<span class="g9"><?php echo $form->textField($model,'name');?></span>
	</div>
	<div class="g3">
		<label class="g3">Type:</label>
		<span class="g9"><?php echo $form->textField($model,'typesCollection');?></span>
	</div>
	<div class="g3">
		<label class="g3">Url:</label>
		<span class="g9"><?php echo $form->textField($model,'url');?></span>
	</div>
	<div class="g3 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
