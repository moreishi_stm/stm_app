<?php
$this->breadcrumbs=array(
    'Edit'=>'',
	$model->name=>array('/admin/featuredAreas/'.$model->id),
);
$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);
$js = <<<JS
    $("#FeaturedAreasCriteriaForm_neighborhood").ajaxChosen({
        type: 'POST',
        url: '/admin/savedHomeSearch/autocomplete',
        dataType: 'json'
    }, function (data) {
        var results = [];

        $.each(data, function (i, val) {
            results.push({ value: val.value, text: val.text });
        });

        return results;
    });
JS;

Yii::app()->clientScript->registerScript('featuredAreaEditScript', $js);

echo $this->renderPartial('_form', array(
	'model'=>$model,
    'featuredAreasModel'=>$featuredAreasModel,
	'termComponentLu'=>$termComponentLu,
	'componentId' => $componentId,
	'componentName' => $componentName,
	'imageData' => $imageData,
	'baseCdnPath' => $baseCdnPath,
	'searchableFields' => MlsFieldMaps::getSearchableColumns(Yii::app()->user->board->id)
));
