<?php
$this->breadcrumbs=array(
	'List'=>array('index'),
);
Yii::app()->clientScript->registerScript('search', <<<JS

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('featured-area-grid', {
            data: $(this).serialize()
        });
        return false;
    });

    $( '.delete-featured-area-button' ).live( "click", function() {
        if(confirm('Are you sure you want to delete this Featured Area?')) {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            var id = $(this).data('id');
            $.post('/admin/featuredAreas/delete/'+id, function(data) {
                $("div.loading-container.loading").remove();
                if(data=='') {
                    Message.create("success","Featured Area deleted successfully.");
                    $.fn.yiiGridView.update("featured-area-grid", { data: $(this).serialize() });
                } else
                    Message.create("error","Error: Featured Area did not delete.");
            },"json");
        }
    });
JS
);
?>
<div id="listview-actions">
    <a href="/<?php echo $this->module->name;?>/featuredAreas/add/<?php echo Yii::app()->user->accountId; ?>" class="button gray icon i_stm_add">Add Featured Area</a>
</div>

<div id="feature-area-header">
	<h1>Featured Areas</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBox',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'featured-area-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'name',
		array(
			'name'=>'Type',
            'value' => 'Yii::app()->format->formatCommaDelimited($data->types,"name")',
		),
		'url',
		array(
			'name'=>'Home Count',
			'value'=>'Yii::app()->format->formatInteger(floatval($data->count_homes))',
		),
		array(
			'name'=>'Price Range',
			'value'=>'$data->priceRange',
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
                "<div><a href=\"/area/".$data->url."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Web Page</a></div>"
            ',
            'htmlOptions'=>array('style'=>'width:140px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
                "<div><a href=\"/admin/featuredAreas/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
            ',
            'htmlOptions'=>array('style'=>'width:80px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
                "<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button delete-featured-area-button\" data-id=\"".$data->id."\">Delete</a></div>"
            ',
            'htmlOptions'=>array('style'=>'width:80px'),
        ),
	),
));