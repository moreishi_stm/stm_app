<?php
$this->breadcrumbs=array(
	'Comments'=>array('/admin/cms'),
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('comment-grid', {
		data: $(this).serialize()
	});

	return false;
});
");
?>
<div id="content-header">
    <?php echo CHtml::tag('h2', $htmlOptions=array(), $this->getPageTitle()); ?>
</div>

<?php
    $this->renderPartial('_commentGrid', array('dataProvider' => $model->search()));
?>
