<?php
$formType =  (strpos($this->action->id,'Edit') !== false)?'Edit':'Add';

$this->breadcrumbs = array(
	'Tags' => '',
	'Edit' => '',//array('/admin/cms/domain', 'id' => $model->domain_id),
);
?>

<h1 class="p-pv10">CMS Tags</h1>

<?php
	$this->beginStmPortletContent(array(
								  'handleTitle' => 'Edit CMS Tag',
								  'handleIconCss' => 'i_wizard',
								  ));

	$form = $this->beginWidget('CActiveForm', array(
											  'id' => 'contact-form',
											  'enableAjaxValidation' => false,
											  ));
?>
	<div id="action-plans-container">
		<div class="g12 p-mb5 rounded-text-box odd-static">
			<table class="container">
				<tr>
					<th><?php echo $form->labelEx($model, 'name'); ?></th>
					<td><?php echo $form->textField($model, 'name', $htmlOptions = array(
								'placeholder' => 'Tag Name', 'class' => 'g6',
							));?>
						<?php echo $form->error($model, 'name'); ?>
					</td>
				</tr>
                <?php if($formType == 'Edit'): ?>
                <tr>
                    <th><?php echo $form->labelEx($model, 'url'); ?></th>
                    <td><?php echo $form->textField($model, 'url', $htmlOptions = array(
                                'placeholder' => 'Tag URL', 'class' => 'g6',
                            ));?><span class="label">(Change URL with caution. May affect SEO if links and content established.)</span>
                        <?php echo $form->error($model, 'name'); ?>
                    </td>
                </tr>
                <?php endif; ?>
                <tr>
                    <th><?php echo $form->labelEx($model, 'parent_id'); ?></th>
                    <td>
                        <?php echo $form->dropDownList($model,'parent_id', CMap::mergeArray(array(''=>''), $model->parentDropDownList()), $htmlOptions=array('placeholder'=>'Tag Name', 'class'=>'g6', 'data-placeholder' => 'Select Subcategory',
                            ));

                        $this->widget('admin_module.extensions.EChosen.EChosen', array(
                                'target' => 'select#CmsTags_parent_id',
                                'options'=>array(
                                    'allow_single_deselect'=>true,
                                ),
                            ));
                        ?>
                        <?php echo $form->error($model, 'parent_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'is_popular_topic_link'); ?></th>
                    <td>
                        <?php echo $form->dropDownList($model,'is_popular_topic_link', StmFormHelper::getYesNoList(), $htmlOptions=array('class'=>'g2')); ?><span class="label">(Link on Popular Topics list)</span>
                        <?php echo $form->error($model, 'is_popular_topic_link'); ?>
                    </td>
                </tr>
			</table>
			<div id="submit-button-wrapper">
				<?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Add CMS Tag' : 'Save CMS Tag'), $htmlOptions = array(
						'class' => 'submit wide',
						'type' => 'submit',
					)); ?>
			</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
<?php $this->endStmPortletContent(); ?>