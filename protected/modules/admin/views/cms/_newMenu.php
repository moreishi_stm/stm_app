<?php
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'new-menu-form',
    'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'onsubmit'=>"return sendNewMenuCreate();",/* Disable normal form submit */
	),
)); ?>
<div class="buildMenuContents">
	<?php /*<label class="menu-name-label howto open-label" for="menu-name">
		<span>Menu Name</span>
		<input name="name" id="menu_name" type="text" class="menu-name regular-text menu-item-textbox" title="Enter menu name here" placeholder="Top Nav">
	</label>
	<label class="menu-name-label howto open-label" for="menu-name">
		<span>Css Classes</span>
		<input name="css_classes" id="css_classes" type="text" class="menu-name regular-text menu-item-textbox" title="Enter css classes here" placeholder="myCssClass">
	</label>
	<div class="publishing-action">
		<input type="submit" name="save_menu" id="save_menu_btn" class="button" value="Create Menu">
	</div>*/ ?>

	Welcome to your Menu Control System.<br><br>
    You can configure your Top Navigation and Bottom Navigation (Footer). You may add links in 2 ways.<br><br>
    1) Click on the "Top Nav" or "Bottom Nav" tab. The current menu configuration will appear in this boxed area.<br>
    2) Select/Add links from a list of web pages you have created from the CMS module (see left "Pages" section) or<br>
    3) Add a Custom Link by typing in a text and url (see left "Custom Links" section).<br><br>
    This menu control systems gives you 100% control over the content of your Mega Dropdown Navigation Menu.<br><br>
    Enjoy!

</div>
<?php $this->endWidget(); ?>

<script>
var canOpen = false;
var tabTemplate = "<li><a href='#menus-tab#{href}'>#{label}</a></li>";
var tabs;
var currentSelectedMenuId = 0;
function addTab(label, id) {
	li = $( tabTemplate.replace( /#\{href\}/g, id ).replace( /#\{label\}/g, label ) ),
	tabContentHtml = '';

	tabs.find( ".ui-tabs-nav" ).append( li );
	tabs.append(" <div id=\"menus-tab"+id+"\" style=\"display: none;\"><div class=\"menuLinksContainer\">\
	<div class=\"NoLinks\">There are currently no links. Please add a custom link or select links from the pages on the left.</div>\
	<ol id=\"menus-tab"+id+"-menu-links\" class=\"menuLinksList sortable ui-sortable mjs-nestedSortable-branch\"></ol>\
	<button class=\"saveMenuBtn\" data-selector=\"#menus-ta"+id+"-menu-links\" data-menu-id=\""+id+"\">Save Menu</button>\
</div></div></div>" );
	tabs.tabs( "refresh" );
}

function sendNewMenuCreate() {
	$("body").prepend("<div class='loading-container loading'><em></em></div>");

	var data = $("#new-menu-form").serialize();

	$.ajax({
		type: 'POST',
		url: '/admin/cms/checkMenuNameAndSaveAction',
		'data': data,
		dataType: "json",
		success:function(data){
			data = JSON.parse(data);
			if(typeof data.results == 'undefined' || data.results != true) {
				$("div.loading-container.loading").remove();
				if(typeof data.message != 'undefined') {
					Message.create("error", data.message);
				}  else {
					Message.create("error", "Error occured. Please try again.");
				}
				return false;
			}

			if(typeof data.name == 'undefined') {
				Message.create("error", "Error occured. Please try again.");
				return false;
			}

			if(typeof data.id == 'undefined') {
				Message.create("error", "Error occured. Please try again.");
				return false;
			}

			addTab(data.name, data.id);
			$("div.loading-container.loading").remove();
		},
		error: function(data) { // if error occured
			$("div.loading-container.loading").remove();
			Message.create("error", "Error occured. Please try again.");
		},

		dataType:'html'
	});

	return false;
}
</script>

