<?php
$this->breadcrumbs = array(
    $model->domain->name => array(
        '/'.$this->module->id.'/cms/domain', 'id' => $model->domain_id
    ), $model->url       => '#',
);
?>

    <h1 class="p-pv10">Content Management System (CMS)</h1>

<?php
$this->beginStmPortletContent(
    array(
        'handleTitle' => $this->pageTitle, 'handleIconCss' => 'i_wizard',
    )
);
$this->renderPartial(
    '_form', array(
        'model' => $model, 'Template' => $Template, 'viewPageLink'=> $viewPageLink
    )
);
$this->endStmPortletContent();

$this->renderPartial('_commentGrid', array('dataProvider' => $commentDataProvider));

