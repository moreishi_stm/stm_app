<table id="cms-editor" class="container" style="border-top: 1px solid #CCC;">
    <?php foreach ($Template->fields as $Field) {
        // This prevents us from trying to render a field template that may not exist due to the absence of a 'type'
        if (!$Field->type) {
            continue;
        }

        $title = CHtml::tag('h4', $htmlOptions=array(), $Field->title);
        $fieldEditor = $this->renderPartial("/cms/field_templates/{$Field->type}", array('model'=>$Field, 'Template'=>$Template), $return=true);

        echo CHtml::openTag('tr');
        echo CHtml::tag('td', $htmlOptions = array(), $title);
        echo CHtml::tag('td', $htmlOptions = array(), $fieldEditor);
        echo CHtml::closeTag('tr');
    }
    ?>
</table>
<div id="submit-button-wrapper">
    <button type="submit" class="submit wide">Save Page</button>
</div>