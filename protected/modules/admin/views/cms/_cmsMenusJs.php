<?php
Yii::app()->clientScript->registerScript('menuAccordian', <<<JS

    // this fn sends the ajax request to save the updated link
	var editCustomLinkFormSubmit = function(event) {
		$('body').prepend("<div class='loading-container loading'><em></em></div>");
		
		event.preventDefault();
		event.stopPropagation();
		
		var action = $(this).attr('action');
		var form = $(this);
		var form_data = $(this).serializeArray();
		
		$.ajax({
            type: 'POST',
            url: action,
            'data': form_data,
            dataType: 'json',
            success:function(data){
                //data = JSON.parse(data);
                if(typeof data.results == 'undefined' || data.results != true) {
                    $('div.loading-container.loading').remove();
                    if(typeof data.message != 'undefined') {
                        Message.create('error', data.message);
                    }  else {
                        Message.create('error', 'Error occured. Please try again.');
                    }
                    return false;
                }

                // btoa() is a way to base64 encode data. Since links can have " ; ' & * () it needs to be encoded to NOT break the HTML
		        var elm = $("li[data-link-id='"+data['link_id']+"']");
		        if(elm.attr('data-custom')) {
		           elm.attr('data-custom',btoa('{"url":"'+data['url']+'","title":"'+data['title']+'"}'));
		        } else {
		            elm.attr('data-name',btoa(data['title']));
		        }

				$("#link_title_"+data['link_id']).html(data['title']);
				$("#link_url_"+data['link_id']).html(data['url']);

				$(form).find('.cancelEdit').click();
		
                Message.create('success', 'Menu Saved.');

                $('.menuLinksList').nestedSortable('refresh');
                $('div.loading-container.loading').remove();
            },
            error: function(data) { // if error occured
                $('div.loading-container.loading').remove();
                Message.create('error', 'Error occured. Please try again.');
            }
        });
	};

	// This fn show and hides the editable fields
	var editLinkFn = function(e) {
		var id = $(this).attr('data-link-id');
		
		var editArea = $('#editContents_'+id);
		
		if(editArea.attr('data-open') == 1) {
			$('#contents_'+id).slideDown();
			editArea.slideUp();
			editArea.removeAttr('data-open');
		} else {
			$('#contents_'+id).slideUp();
			editArea.slideDown();
			editArea.attr('data-open',1);
		}
	};
		
    var removeLinkFn = function(event) {

        $('body').prepend("<div class='loading-container loading'><em></em></div>");

        event.stopPropagation();

        var that = $(this);

        if($(this).closest('li').find('ol > li').length > 0) {
            if(!confirm('This has links under it and will be moved up level upon deletion. Are you sure you want to delete?')) {
                $('div.loading-container.loading').remove();
                return false;
            }
        }

        var linkId = $(this).attr('data-link-id');

        if(!linkId) {
            linkId = $(this).data('link-id');
        }

        if(!linkId) {
            $(this).parents('.menuLinksContainer').effect('highlight', {color: '#88BE71'}, 3000);
            $(this).closest('li').remove();
            $('.menuLinksList').nestedSortable('refresh');
            Message.create('success', 'Menu Removed.');
            $('div.loading-container.loading').remove();

            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/admin/cms/removeMenuLink',
            'data': {'linkId': linkId},
            dataType: 'json',
            success:function(data){
                //data = JSON.parse(data);
                if(typeof data.results == 'undefined' || data.results != true) {
                    $('div.loading-container.loading').remove();
                    if(typeof data.message != 'undefined') {
                        Message.create('error', data.message);
                    }  else {
                        Message.create('error', 'Error occured. Please try again.');
                    }
                    return false;
                }
                Message.create('success', 'Menu Removed.');
                location.reload();

                return false;
            },
            error: function(data) { // if error occured
                $('div.loading-container.loading').remove();
                Message.create('error', 'Error occurred. Please try again.');
            }
        });
    };

    tabs = jQuery('#menus-tab-widget').tabs({
        activate: function(event, ui) {
            var target = event.currentTarget;
            var href_id = jQuery(target).attr('href');

            if(href_id != '#new-menu-tab') {
                canOpen = true;
                currentSelectedMenuId = href_id;
            } else {
                canOpen = false;
                jQuery('.accordion-section-title').parent().siblings('.open').removeClass('open').end();
                jQuery('.accordion-section-content input:checkbox').removeAttr('checked');
            }
        }
    });
    tabs.tabs( 'refresh' );

    jQuery('.accordion-section-title').click(function() {
        if(!canOpen) {
            Message.create('error','You must select a menu from the tabs on the right before you can add links');
            return false;
        }
        //$(this).parent().siblings('.open').find('input:checkbox').removeAttr('checked');
        $(this).parent().siblings('.open').removeClass('open').end();
        jQuery(this).parent().addClass('open');
    });

    var pageInc = 0;
    jQuery('.addLinksToMenu').click(function() {
        if(currentSelectedMenuId == 0) {
            Message.create('error','Please select a menu from a tab on the right');
            return false;
        }

        var domainId = $(this).data('domain-id');
        var checkboxes = $('#selectPages_'+domainId+' input:checked');

        if(checkboxes.length > 0) {
            $(currentSelectedMenuId+'-menu-links .NoLinks').hide();
            checkboxes.map(function() {

                //var selectedMenuLink = $(currentSelectedMenuId+'-menu-links');
                var selectedMenuAppend = '<li id="page-'+$(this).val()+'-'+pageInc+'"\
					data-selector="#page-'+$(this).val()+'-'+pageInc+'"\
					data-name="'+btoa($(this).data('title'))+'" data-page-id="'+$(this).val()+'"\
					data-entity-type="cms_page" data-entity-id="'+$(this).val()+'" class="mjs-nestedSortable-branch">\
                    <div class="menuDiv">\
						<span class="disclose"><span></span></span>\
						CMS Page ('+$(this).val()+')\
                        <i class="fa fa-remove removeLink"></i>\
                        <div class="clearfix"></div>\
                        <div class="menuEdit">\
							<strong>Title:</strong> '+$(this).data('title')+'<br />\
							<strong>URL:</strong> '+$(this).data('url')+'\
						</div>\
                    </div>\
                </li>';

                $(currentSelectedMenuId+'-menu-links').append(selectedMenuAppend);

                $('#page-'+$(this).val()+'-'+pageInc+' > div.menuDiv > i.removeLink').click(removeLinkFn);
				$('#page-'+$(this).val()+'-'+pageInc).find('.disclose').on('click', function() {
					$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
				});
                pageInc++;
            });

            $('.menuLinksList').nestedSortable('refresh');
        }
        $('.accordion-section').find('input:checkbox').removeAttr('checked');
        $(currentSelectedMenuId + ' button.saveMenuBtn').click();
    });

    jQuery('.addFeaturedToMenu').click(function() {
        if(currentSelectedMenuId == 0) {
            Message.create('error','Please select a menu from a tab on the right');
            return false;
        }

        var checkboxes = $('#selectFeaturedAreas input:checked');
		
        if(checkboxes.length > 0) {
            $(currentSelectedMenuId+'-menu-links .NoLinks').hide();
            checkboxes.map(function() {
		
                var currentSelectedMenuLink = $(currentSelectedMenuId+'-menu-links');
                var currentSelectedMenuAppend = '<li id="page-'+$(this).val()+'-'+pageInc+'"\
					data-selector="#page-'+$(this).val()+'-'+pageInc+'" data-name="'+btoa($(this).data('title'))+'"\
					data-page-id="'+$(this).val()+'" data-entity-type="featured_area" data-entity-id="'+$(this).val()+'"\
					class="mjs-nestedSortable-branch">\
                    <div class="menuDiv">\
						<span class="disclose"><span></span></span>\
						Featured Area ('+$(this).val()+')\
                        <i class="fa fa-remove removeLink"></i>\
                        <div class="clearfix"></div>\
                        <div class="menuEdit">\
							<strong>Title:</strong> '+$(this).data('title')+'<br />\
							<strong>URL:</strong> '+$(this).data('url')+'\
						</div>\
                    </div>\
                </li>';

                $(currentSelectedMenuId+'-menu-links').append(currentSelectedMenuAppend);

                $('#page-'+$(this).val()+'-'+pageInc+' > div.menuDiv > i.removeLink').click(removeLinkFn);
				$('#page-'+$(this).val()+'-'+pageInc).find('.disclose').on('click', function() {
					$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
				});
                pageInc++;
            });

            $('.menuLinksList').nestedSortable('refresh');
        }
        $('.accordion-section').find('input:checkbox').removeAttr('checked');
        $(currentSelectedMenuId + ' button.saveMenuBtn').click();
    });

    // This is an override fn to allow selcting all the attributes from the LI element
    // NestedSortable is the drag and drop framework for jQuery
    $.mjs.nestedSortable.prototype.toArray = function(options) {

        var o = $.extend({}, this.options, options),
            sDepth = o.startDepthCount || 0,
            ret = [],
            left = 1;

        if (!o.excludeRoot) {
            ret.push({
                "item_id": o.rootID,
                "parent_id": null,
                "depth": sDepth,
                "left": left,
                "right": ($(o.items, this.element).length + 1) * 2
            });
            left++;
        }

        $(this.element).children(o.items).each(function() {
            left = _recursiveArray(this, sDepth + 1, left);
        });

        ret = ret.sort(function(a, b) { return (a.left - b.left); });

        return ret;

        function _recursiveArray(item, depth, _left) {

            var right = _left + 1,
                id,
                pid,
                parentItem;

            if ($(item).children(o.listType).children(o.items).length > 0) {
                depth++;
                $(item).children(o.listType).children(o.items).each(function() {
                    right = _recursiveArray($(this), depth, right);
                });
                depth--;
            }

            id = ($(item).attr(o.attribute || "id")).match(o.expression || (/(.+)[-=_](.+)/));

            if (depth === sDepth + 1) {
                pid = o.rootID;
            } else {
                parentItem = ($(item).parent(o.listType)
                                        .parent(o.items)
                                        .attr(o.attribute || "id"))
                                        .match(o.expression || (/(.+)[-=_](.+)/));
                pid = parentItem[2];
            }

            if (id) {
            var attributes = {};

                $.each($(item).get(0).attributes, function(i, attrib){
                    attributes[attrib.name.replace('data-','').replace('-','_')] = attrib.value;
                });

                ret.push($.extend({}, {
                    "item_id": id[2],
                    "parent_id": pid,
                    "depth": depth,
                    "left": _left,
                    "right": right
                }, attributes));
            }

            _left = right + 1;

            return _left;
        }
    };

    // This is an override fn to allow selcting all the attributes from the LI element
    // NestedSortable is the drag and drop framework for jQuery
    $.mjs.nestedSortable.prototype.toHierarchy = function(options) {

        var o = $.extend({}, this.options, options),
            ret = [];

        $(this.element).children(o.items).each(function() {
            var level = _recursiveItems(this);
            ret.push(level);
        });

        return ret;

        function _recursiveItems(item) {
            var id = ($(item).attr(o.attribute || "id") || "").match(o.expression || (/(.+)[-=_](.+)/)),
                currentItem;

            if (id) {
                currentItem = {
                    "id": id[2]
                };

                if ($(item).children(o.listType).children(o.items).length > 0) {
                    currentItem.children = [];
                    $(item).children(o.listType).children(o.items).each(function() {
                        var level = _recursiveItems(this);
                        currentItem.children.push(level);
                    });
                }
                var attributes = {};

                $.each($(item).get(0).attributes, function(i, attrib){
                    attributes[attrib.name.replace('data-','').replace('-','_')] = attrib.value;
                });
                currentItem = $.extend({}, currentItem, attributes);
                return currentItem;
            }
        }
    };

    // This is an override fn to allow auto-save when a Drag is completed
    // NestedSortable is the drag and drop framework for jQuery
    $.mjs.nestedSortable.prototype._mouseStop =  function(event) {
        // mjs - if the item is in a position not allowed, send it back
        if (this.beyondMaxLevels) {

            this.placeholder.removeClass(this.options.errorClass);

            if (this.domPosition.prev) {
                $(this.domPosition.prev).after(this.placeholder);
            } else {
                $(this.domPosition.parent).prepend(this.placeholder);
            }

            this._trigger("revert", event, this._uiHash());

        }

        // mjs - clear the hovering timeout, just to be sure
        $("." + this.options.hoveringClass)
            .mouseleave()
            .removeClass(this.options.hoveringClass);

        this.mouseentered = false;
        if (this.hovering) {
            window.clearTimeout(this.hovering);
        }
        this.hovering = null;

        this._relocate_event = event;
        this._pid_current = $(this.domPosition.parent).parent().attr("id");
        this._sort_current = this.domPosition.prev ? $(this.domPosition.prev).next().index() : 0;
        $.ui.sortable.prototype._mouseStop.apply(this, arguments); //asybnchronous execution, @see _clear for the relocate event.

        $(currentSelectedMenuId + ' button.saveMenuBtn').click();
    };

    $('.menuLinksList').nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '> div',
		isTree: true,
    });

    $('.saveMenuAssignment').click(function() {

        var selector = $(this).data('selector');
        var selectorElement = $(selector);

        var menu_id = selectorElement.val();
        /*if(menu_id < 1) {
            Message.create('error', 'Please select an option from the menu');
            return false;
        }*/

        var position_id = $(this).data('menu-position-id');
        var domain_id = $(this).data('domain-id');

        var data = {
            'position_id': position_id,
            'menu_id': menu_id,
            'domain_id': domain_id,
            'selector': selector
        };

        $.ajax({
            type: 'POST',
            url: '/admin/cms/assignMenuPosition',
            'data': data,
            dataType: 'json',
            success:function(data){
                //data = JSON.parse(data);
                if(typeof data.results == 'undefined' || data.results != true) {
                    $('div.loading-container.loading').remove();
                    if(typeof data.message != 'undefined') {
                        Message.create('error', data.message);
                    }  else {
                        Message.create('error', 'Error occured. Please try again.');
                    }
                    return false;
                }

                if(data.deleted) {
                    Message.create('success', 'The menu has been unassigned.');
                    $(parentTd).parent().effect('highlight', {color: '#88BE71'}, 3000);
                    $('div.loading-container.loading').remove();
                    return false;
                }

                var parentTd = selectorElement.parent();
                selectorElement.remove();

                $(parentTd).html('<strong>Assigned Menu:</strong> '+data.menuName);

                $(parentTd).parent().effect('highlight', {color: '#88BE71'}, 3000);

                $('div.loading-container.loading').remove();
            },
            error: function(data) { // if error occured
                $('div.loading-container.loading').remove();
                Message.create('error', 'Error occured. Please try again.');
            }
        });
    });

    jQuery('.updateAssignment').click(function(event) {
        event.preventDefault();

        var selector = $(this).data('selector');

        var selectorElement = $(selector);

        $(this).hide();
        selectorElement.removeClass('hidden');
        selectorElement.show();
    });

    var customPageInc = 99999;
    jQuery('#submit-customlink').click(function() {

        var custom_url = $('#add_custom_url').val();
        var custom_label = $('#add_custom_label').val();
        var customMenuAppend = '<li id="page-'+customPageInc+'" data-selector="#page-'+customPageInc+'" data-custom="'+btoa('{"url":"'+custom_url+'","title":"'+custom_label+'"}')+'" data-entity-type="custom" data-entity-id="0" class="mjs-nestedSortable-branch">'+
       '<div class="menuDiv">'+
			'<span class="disclose"><span></span></span>'+
			'Custom URL'+
			'<i class="fa fa-remove removeLink"></i> <i class="fa fa-pencil-square-o editLink" data-link-id=""></i>'+
			'<div class="clearfix"></div>'+
			'<div class="menuEdit">'+
				'<div class="contents" id="contents_ZZZ">'+
					'<strong>Title:</strong> <span id="link_title_ZZZ">'+custom_label+'</span><br />'+
					'<strong>URL:</strong> <span id="link_url_ZZZ">'+custom_url+'</span>'+
				'</div>'+
				'<div class="editContents" id="editContents_ZZZ">'+
					'<form action="/admin/cms/updateCustomLink" method="post">'+
						'<input type="hidden" name="link_id" value="" />'+
						'<input type="text" name="title" value="'+custom_label+'" />'+
						'<input type="text" name="url" value="'+custom_url+'" />'+
						'<div>'+
							'<input type="submit" class="button" value="Save" />'+
							'<input type="button" class="button gray cancelEdit" data-link-id="" value="Cancel" />'+
						'</div>'+
					'</form>'+
				'</div>'+
			'</div>'+
        '</div>'+
        '</li>';

        $(currentSelectedMenuId+'-menu-links .NoLinks').hide();
        $(currentSelectedMenuId+'-menu-links').append(customMenuAppend);
        $('#page-'+customPageInc+' > div.menuDiv > i.removeLink').click(removeLinkFn);
		$('#page-'+customPageInc+' > div.menuDiv > i.editLink').click(editLinkFn);
		$('#page-'+customPageInc).find(".cancelEdit").click(editLinkFn);
		$('#page-'+customPageInc).find(".editContents form").submit(editCustomLinkFormSubmit);
		$('#page-'+customPageInc).find('.disclose').on('click', function() {
			$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		});
        $('.menuLinksList').nestedSortable('refresh');

        $('#add_custom_url').val('http://');
        $('#add_custom_label').val('');
        customPageInc++;

        $('.accordion-section').find('input:checkbox').removeAttr('checked');
        $(currentSelectedMenuId + ' button.saveMenuBtn').click();
    });

    /*jQuery('#testSerialize').click(function() {
        serialized = $(currentSelectedMenuId+'-menu-links').nestedSortable('serialize');
        console.log('serialized');
        console.log(serialized);

        hiered = $(currentSelectedMenuId+'-menu-links').nestedSortable('toHierarchy', {startDepthCount: 0});
        console.log('hiered');
        console.log(hiered);

        arraied = $(currentSelectedMenuId+'-menu-links').nestedSortable('toArray', {startDepthCount: 0});
        console.log('arraied');
        console.log(arraied);
    });*/

    jQuery('.saveMenuBtn').click(function() {
        $('body').prepend("<div class='loading-container loading'><em></em></div>");
        var menu_items = $($(this).data('selector')).nestedSortable('toHierarchy', {startDepthCount: 0});
		
        var menuItemsToSend = [];
        var memberfilter = [];

        /*$.each(menu_items, function(i, v) {
            $.each(v, function(_i, _v) {
                if(_i != 'nestedSortableItem') {
                    memberfilter.push(_i);
                }
            });
        });*/

        $.each(menu_items, function(i, v) {
            menuItemsToSend.push(JSON.stringify(v));//, memberfilter));
        });

        $.ajax({
            type: 'POST',
            url: '/admin/cms/saveMenuLinks',
            'data': {'data': menuItemsToSend,"menu_id": $(this).data('menu-id')},
            dataType: 'json',
            success:function(data){
                //data = JSON.parse(data);
                if(typeof data.results == 'undefined' || data.results != true) {
                    $('div.loading-container.loading').remove();
                    if(typeof data.message != 'undefined') {
                        Message.create('error', data.message);
                    }  else {
                        Message.create('error', 'Error occured. Please try again.');
                    }
                    return false;
                }
                Message.create('success', 'Menu Saved.');
                $('#menus-tab'+data.menuId).effect('highlight', {color: '#88BE71'}, 3000);
                $('#menus-tab'+data.menuId).find('.menuLinksContainer').effect('highlight', {color: '#88BE71'}, 3000);

                $.each(data['links'], function(i, v) {
                    if(!v['result']) {
                        Message.create('error', 'Could not save link titled: "'+v['title']+'"');
                    } else {
                        $(v['selector']).attr('data-link-id',v['id']);
                        $(v['selector']).find('i.removeLink').attr('data-link-id',v['id']);
						$(v['selector']).find('i.editLink').attr('data-link-id',v['id']);
						$(v['selector']).find('#contents_ZZZ').attr('id','contents_'+v['id']);
						$(v['selector']).find('#link_title_ZZZ').attr('id','link_title_'+v['id']);
						$(v['selector']).find('#link_url_ZZZ').attr('id','link_url_'+v['id']);
						$(v['selector']).find('#editContents_ZZZ').find("input[name='link_id']").val(v['id']);
						$(v['selector']).find('#editContents_ZZZ').find('.cancelEdit').attr('data-link-id',v['id']);
						$(v['selector']).find('#editContents_ZZZ').attr('id','editContents_'+v['id']);
                    }
                });

                $('.menuLinksList').nestedSortable('refresh');
                $('div.loading-container.loading').remove();
            },
            error: function(data) { // if error occured
                $('div.loading-container.loading').remove();
                Message.create('error', 'Error occured. Please try again.');
            }
        });
    });
		
	jQuery(".editContents form").submit(editCustomLinkFormSubmit);

    jQuery("i.removeLink").click(removeLinkFn);
		
	jQuery("i.editLink").click(editLinkFn);
	jQuery(".cancelEdit").click(editLinkFn);
		
	$('.disclose').on('click', function() {
		$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
	});
JS
);