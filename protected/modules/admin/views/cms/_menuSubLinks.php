<?php if (isset($links) && !empty($links)): ?>
	<ol>
		<?php foreach ($links as $child):
			$inc->i++; ?>
			<li id="page-<?=$inc->i;?>" data-selector="#page-<?=$inc->i;?>" data-name="<?=base64_encode($child['name']);?>" class="mjs-nestedSortable-branch" data-link-id="<?= $child['id']; ?>">
				<div class="menuDiv">
					<span class="disclose"><span></span></span>
					<?=ucwords(str_replace('_',' ', $child['entity_type'])); ?><?php if(!empty($child['entity_id'])):?> (<?=$child['entity_id']; ?>)<?php endif; ?>
					<i class="fa fa-remove removeLink clearfix" data-link-id="<?=$child['id'];?>"></i><?php if($child['entity_type'] == 'custom'):?> <i class="fa fa-pencil-square-o editLink" data-link-id="<?=$child['id'];?>"></i><?php endif; ?>
					<div class="clearfix"></div>
					<div class="menuEdit">
						<div class="contents" id="contents_<?=$child['id'];?>">
							<strong>Title:</strong> <span id="link_title_<?=$child['id'];?>"><?=$child['name'];?></span><br />
							<strong>URL:</strong> <span id="link_url_<?=$child['id'];?>"><?=$child['url']; ?></span>
						</div>
						<div class="editContents" id="editContents_<?=$child['id'];?>">
							<form action="/admin/cms/updateCustomLink" method="post">
								<input type="hidden" name="link_id" value="<?=$child['id'];?>" />
								<input type="text" name="title" value="<?=$child['name'];?>" />
								<input type="text" name="url" value="<?=$child['url'];?>" />
								<div>
									<input type="submit" class="button" value="Save" />
									<input type="button" class="button gray cancelEdit" data-link-id="<?=$child['id'];?>" value="Cancel" />
								</div>
							</form>
						</div>
					</div>
				</div>
			<?php if(isset($menuLinkItems[$child['id']])  && !empty($menuLinkItems[$child['id']])) {
				ksort($menuLinkItems[$child['id']]);
				$this->renderPartial('_menuSubLinks', array('links' => $menuLinkItems[$child['id']], 'inc' => &$inc, 'menuLinkItems' => $menuLinkItems));
			} ?>
			</li>
	<?php endforeach; ?>
	</ol>
<?php endif; ?>