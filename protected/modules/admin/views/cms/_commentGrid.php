<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since : 12/18/13
 */

Yii::app()->clientScript->registerScript(
    'commentGrid', "
$('a.delete-comment').live('click', function() {
    $.getJSON($(this).attr('href'), function(data) {
        if (data.success) {
            Message.create('success', 'Comment successfully removed.');
        } else {
            Message.create('error', 'Comment could not be removed.');
        }

        $.fn.yiiGridView.update('comment-grid', {
            data: $('#listview-search form').serialize()
        });
    });

    return false;
});
"
);

$this->beginStmPortletContent(
    array(
        'handleTitle'          => 'View Comments',
        'handleIconCss'        => 'i_wizard',
        'containerHtmlOptions' => array(
            'style' => 'margin-top: 20px',
        )
    )
);

$this->widget(
    'admin_module.components.StmGridView', array(
        'id'            => 'comment-grid',
        'dataProvider'  => $dataProvider,
        'itemsCssClass' => 'datatables',
        'emptyText'     => 'No comments found.',
        'columns'       => array(
            'name',
            'content.url',
            'comment',
            'added:dateTime',
            'updated:dateTime',
            array(
                'class'           => 'CLinkColumn',
                'label'           => 'Delete',
                'urlExpression'   => 'array("/admin/cms/deleteComment", "id"=>$data->id)',
                'htmlOptions'     => array('style' => 'text-align: center',),
                'linkHtmlOptions' => array('class' => 'button gray icon i_stm_delete delete-comment',),
            ),
        ),
    )
);

$this->endStmPortletContent();
