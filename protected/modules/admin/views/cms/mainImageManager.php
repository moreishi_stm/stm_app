<?php
$cdnUrl = (YII_DEBUG) ? "http://dev.sites.seizethemarket.com/" : "http://sites.seizethemarket.com/";
$cdnBucket = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";
$clientId = Yii::app()->user->clientId;
?>
<div id="content-header">
	<h1>Home Page Image Manager</h1>
</div>

<div class="container">
		<div style="text-align: center;">
			<img src="<?=$currentImage;?>" id="currentImage" style="height: 25%;<?php if(empty($currentImage)):?>display: none;<?php endif; ?>" />
			<div>
				<a href="/admin/cms/deleteMainImage/" id="imageDeleter" data-id="<?php $currentImageId;?>" style="<?php if(empty($currentImage)):?>display: none;<?php endif; ?>" class="btn">Delete</a>
			</div>
		</div>
		<br />
		<br />
		<h3>Upload an image</h3>
		
		<div id="my-awesome-dropzone" action="/admin/imageManager/upload" class="dropzone" style="display: block; min-height: 100px; width: 100%; background-color: #CCC;">
			<div class="dropzone-previews"></div>
			<div class="fallback"> <!-- this is the fallback if JS isn't working -->
				<input name="Filedata" type="file" id="Filedata" multiple />
			</div>
		</div>
		<?php /*<button type="submit" id="submit-all" class="btn btn-primary btn-xs hidden">Upload the file</button>*/ ?>
</div>

<?php
$js = <<<JS
	Dropzone.options.myAwesomeDropzone = {
        autoProcessQueue: true,
        uploadMultiple: false,
        parallelUploads: 5,
        maxFiles: 25,
		addRemoveLinks: true,
		params: {
			directory: "{$clientId}/homePageBanner/",
			cdnUrl: "{$cdnUrl}",
			cdnBucket: "{$cdnBucket}"
		},
		paramName: 'Filedata',
        init: function() {
          var myDropzone = this;
        },
		error: function(file, response) {
			Message.create("Error",response);
		},
		complete: function(file) {
			var fileData = file;
			this.removeFile(file);

			var serverData = JSON.parse(fileData.xhr.response);

			if(typeof serverData['results'] == 'undefined' || serverData['results'] != true) {
				$('div.loading-container.loading').remove();
				if(typeof serverData.message != 'undefined') {
					Message.create('error', serverData.message);
				}  else {
					Message.create('error', 'Error occured. Please try again.');
				}
				return false;
			}
			
			$.ajax({
				type: 'POST',
				url: '/admin/cms/assignmainimage/',
				'data': {'imageData': serverData},
				dataType: 'json',
				success:function(data){
					if(typeof data.results == 'undefined' || data.results != true) {
						$('div.loading-container.loading').remove();
						if(typeof data.message != 'undefined') {
							Message.create('error', data.message);
						}  else {
							Message.create('error', 'Error occured. Please try again.');
						}
						return false;
					}

					$('#imageDeleter').attr('data-id', data.id);
					$('#imageDeleter').css('dispaly','block');
			
					$('#currentImage').attr('src',data.value);
					$('#currentImage').css('dispaly','block');
			
					Message.create('success', 'Image Added.');
					return false;
				},
				error: function(data) { // if error occured
					$('div.loading-container.loading').remove();
					Message.create('error', 'Error occurred. Please try again.');
				}
			});
		}
	};
JS;
Yii::app()->clientScript->registerScript('dragNDropImagesForMainHomePageImage', $js);