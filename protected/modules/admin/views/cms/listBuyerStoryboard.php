<?php
	$this->breadcrumbs = array(
        'List' => array('/'.$this->module->name.'/listingStoryboards'),
	);
?>
<!--	<div id="listview-actions">-->
<!--		<a href="/--><?php //echo $this->module->name;?><!--/contacts/add" class="button gray icon i_stm_add">Add New Storyboard</a>-->
<!--	</div>-->
	<div id="content-header">
		<h1>
            <img src="http://cdn.seizethemarket.com/assets/images/buyer_storyboard.png" alt="Buyer Storyboard &trade;" title="Buyer Storyboard &trade;" width="228px" height="70px">
        </h1>
	</div>

<!--	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">-->
        <?php //$this->renderPartial('_listSearchBox', array(
//				'model' => $model,
//			)
//		); ?>
<!--	</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'contact-grid',
        'template'=>'{summary}{items}{summary}{pager}',
		'dataProvider' => $DataProvider,
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Seller Name',
                'value' => '$data->contact->fullName."<br />Assigned to: ".$data->printTransactionAssignments()',
            ),
            'title',
            'url',
            array(
                'type' => 'raw',
                'name' => 'Added',
                'value' => 'Yii::app()->format->formatDate($data->added)."<br />".$data->addedBy->fullName',
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/cms/editListingStoryboard/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/storyboard/".$data->url."/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
		),
	)
);
