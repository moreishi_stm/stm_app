<?php
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('landingPagesSearch', <<<JS
    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('house-values-landing-pages-grid', { data: $(this).serialize() });
        return false;
    });
JS
);
$form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'domain-pages-search',
));
?>
    <div class="g12">
        <div class="g2"></div>
        <div class="g6">
            <label class="g4">Title/Url:</label>
            <span class="g8"><?php echo $form->textField($model,'title');?></span>
        </div>
        <div class="g2 submit" style="text-align:center">
            <?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?>
        </div>
    </div>
<? $this->endWidget(); ?>