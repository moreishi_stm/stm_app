<? $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'domain-pages-search',
));
?>
    <div class="g12">
        <div class="g2">
            <label class="g4">Status:</label>
            <span class="g8"><?php echo $form->dropDownList($model,'status_ma', StmFormHelper::getStatusBooleanList(true), $htmlOptions=array());?></span>
        </div>
        <div class="g3">
            <label class="g3">Type:</label>
            <span class="g9"><?php echo $form->dropDownList($model,'type_ma', $model->pageTypeList, $htmlOptions=array('empty'=>''));?></span>
        </div>
        <div class="g3">
            <label class="g3">Title/Url:</label>
            <span class="g9"><?php echo $form->textField($model,'title');?></span>
        </div>
        <div class="g2">
            <label class="g4">Tags:</label>
            <span class="g8"><?php echo $form->textField($model,'id');?></span>
        </div>
        <div class="g2 submit" style="text-align:center">
            <?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?>
        </div>
    </div>
<? $this->endWidget(); ?>