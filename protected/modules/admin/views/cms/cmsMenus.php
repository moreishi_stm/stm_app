<?php $this->renderPartial('_cmsMenusCss'); ?>

    <div class="domainGrid">
	<div class="domainGridContainer">
		<h3>Menu Assignment</h3>
		<table class="datatables" style="float: none;">
			<thead>
				<th>Domains</th>
				<th>Menu Position</th>
				<th>Menu</th>
				<th>Actions</th>
			</thead>
			<tbody>
				<?php
				$i = 0;

				$optionsHtml = "";
				$menusById = array();
				if(!empty($menus)) {
					foreach($menus as $menu) {
						$optionsHtml .= '<option value="'.$menu->id.'">'.$menu->name.'</option>';
						$menusById[$menu->id] = $menu->name;
					}
				}

				foreach($domainAvailablePositions as $domainId => $positions): ?>
				<?php foreach($positions as $positionId => $position):
					$hideSelector = false; ?>
				<tr<?php if($i%2):?> class="even"<?php endif; ?>>
					<td>
						<?=$domains[$domainId]->name; ?>
					</td>
					<td>
						<?=$position; ?>
					</td>
					<td>
						<?php if(!empty($domainMenus) && isset($domainMenus[$domainId][$positionId]) && !empty($domainMenus[$domainId][$positionId])):
							$hideSelector = true; ?>
						<strong>Selected Menu:</strong> <?=ucwords($menusById[$domainMenus[$domainId][$positionId]['menu_id']])?><br /> <a href="#" class="updateAssignment" data-selector="#menu_assignment_<?=$domainId;?>_<?=$positionId;?>" style="color: blue; text-decoration: none; font-weight: normal;">Update</a>
						<?php endif; ?>
						<?php if(!empty($menus)): ?>
						<select name="menu_assignment[<?=$domainId;?>][<?=$positionId;?>]" id="menu_assignment_<?=$domainId;?>_<?=$positionId;?>"<?php if($hideSelector):?> class="hidden"<?php endif; ?>>
							<option value="0" selected="selected">Unassigned</option>
							<?=$optionsHtml; ?>
						</select>
						<?php else: ?>
						<span style="color: #D20000; font-weight: bold;">No Menus Available for assignment.</span>
						<?php endif; ?>
					</td>
					<td>
						<button title="Update Assignment" data-menu-position-id="<?=$positionId;?>" data-selector="#menu_assignment_<?=$domainId;?>_<?=$positionId;?>" data-domain-id="<?=$domainId;?>" class="saveMenuAssignment"><i class="fa fa-2x fa-save" style="color: #00BD00; text-shadow: none;"></i></button>
					</td>
				</tr>
				<?php $i++;
					endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<div class="menuManagementPage">
	<h3>Menu Management</h3>
	<hr />
	<div class="menuManagement clearfix">
		<div class="left selectPages">
			<div id="accordian">
				<div class="accordion-section">
					<h3 class="accordion-section-title">Pages</h3>
					<div class="accordion-section-content">
						<div class="list">
							<?php
							$i=0;
							if(!empty($cmsPagesByDomain)):
								foreach($cmsPagesByDomain as $domainName => $pagesData): ?>
								<h4><?=$domainName; ?></h4>
								<ul class="scroll-bar" id="selectPages_<?=$pagesData['domain_id'];?>">
									<?php foreach($pagesData['pages'] as $pageId => $page): ?>
									<li>
										<label class="menu-item-title">
											<input type="checkbox" name="pages[]" value="<?=$page['id']; ?>" data-title="<?=$page['title']; ?>" data-url="<?=$page['url'];?>" /><?=$page['title']; ?>
										</label>
									</li>
									<?php endforeach; ?>
								</ul>
								<span class="list-controls">
									<a href="#" data-domain-id="<?=$page['domain_id']; ?>" class="select-all">Select All: <?=$domainName; ?></a>
								</span>
								<span class="add-to-menu">
									<input type="submit" value="Add to Menu" data-domain-id="<?=$page['domain_id']; ?>" name="submit-pages-to-menu<?=$page['domain_id']; ?>" id="submit-pages-to-menu<?=$page['domain_id']; ?>" class="button addLinksToMenu">
								</span>
							<?php $i++; endforeach; ?>
							<?php endif; ?>
						</div>
						<div class="clearfix" style="margin-bottom: 10px;"></div>
					</div>
				</div>
                <div class="accordion-section">
                    <h3 class="accordion-section-title">Featured Areas</h3>
                    <div class="accordion-section-content">
                        <div class="list">
                            <?php
                            if(!empty($featuredAreas)): ?>
                                <ul class="scroll-bar" id="selectFeaturedAreas">
                                <?php foreach($featuredAreas as $domainName => $featuredArea): ?>
								<li>
									<label class="menu-item-title">
										<input type="checkbox" name="featuredareas[]" value="<?=$featuredArea['id']; ?>" data-title="<?=$featuredArea['name']; ?>" data-url="<?=$featuredArea['url']; ?>" /><?=$featuredArea['name'];?>
									</label>
								</li>
                                <?php $i++; endforeach; ?>
                                </ul>
                                <span class="add-to-menu">
                                    <input type="submit" value="Add to Menu" data-featured-area-id="<?=$featuredArea['id']; ?>" name="submit-featured-areas-to-menu" id="submit-featured-areas-to-menu" class="button addFeaturedToMenu">
                                </span>
                            <?php endif; ?>
                        </div>
                        <div class="clearfix" style="margin-bottom: 10px;"></div>
                    </div>
                </div>
				<div class="accordion-section">
					<h3 class="accordion-section-title">Custom Links</h3>
					<div class="accordion-section-content">
						<div class="list">
							<div class="customlinkdiv clearfix" id="customlinkdiv">
								<div class="clearfix">
									<label class="howto" for="custom-menu-item-name">
										<span>Link Text</span>
										<input name="add_custom_label" id="add_custom_label" type="text" class="regular-text menu-item-textbox input-with-default-title" title="Menu Item">
									</label>
								</div>

                                <div class="clearfix">
                                    <label class="howto" for="custom-menu-item-url">
                                        <span>URL</span>
                                        <input name="add_custom_url" id="add_custom_url" type="text" class="code menu-item-textbox" value="http://">
                                    </label>
                                </div>

                                <div class="button-controls clearfix">
									<span class="add-to-menu">
										<input type="submit" class="button right" value="Add to Menu" name="add-custom-menu-item" id="submit-customlink">
										<span class="spinner"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="left buildMenu">
			<?php
			$tabData = array();
			$tabData['Create Menu'] = array(
				'content' => $this->renderPartial('_newMenu', array('tabsId' => '#menus-tab-widget'), true),
				'id' => 'new-menu-tab',
			);
			$inc = new \stdClass();
			$inc->i = 0;
			foreach($menus as $menu) {
				$inc->i++;
				ksort($menuLinks[$menu->id]);
				$tabData[$menu->name] = array(
					'content' => $this->renderPartial('_menuLinks', array('menu_id' => $menu->id, 'menuLinkItems' => $menuLinks[$menu->id], 'inc' => $inc), true),
					'id' => 'menus-tab'.$menu->id,
				);
			}
			$this->widget('zii.widgets.jui.CJuiTabs', array(
				'tabs' => $tabData,
				'id' => 'menus-tab-widget',
			));
			?>
			<?php /*<button id="testSerialize">Serialize To Console</button>*/ ?>
		</div>
	</div>
</div>

<?php
$this->renderPartial('_cmsMenusJs');