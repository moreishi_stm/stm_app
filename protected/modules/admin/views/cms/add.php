<?php
$this->breadcrumbs = array('Add' => '#',);

Yii::app()->clientScript->registerScript(
    'cmsUrl', <<<'JS'
$('#CmsContents_title').keyup(function(e) {

	var title = $('#CmsContents_title').val().replace(/\s/g, '-');
	url = title.replace(/-+/g, '-').replace(/[^a-zA-Z0-9-]+/g,'').toLowerCase();

	$('#CmsContents_url').val(url);
});
JS
);
?>
    <div id="content-header" class="p-pv10">
        <h1>Content Management System (CMS)</h1>
    </div>

<?php

$this->beginStmPortletContent(
    array(
        'handleTitle' => $this->pageTitle, 'handleIconCss' => 'i_wizard',
    )
);

$this->renderPartial(
    '_form', array(
        'model' => $model, 'Template' => $Template
    )
);

$this->endStmPortletContent();

