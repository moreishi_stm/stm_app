<?php
	/**
if ($model->outertext) {
	echo CHtml::tag('div', $htmlOptions=array(), $model->outertext);
}
	 **/

$dataThumbnail = $model->getDataAttr('thumbnail');
$dataType      = $model->getDataAttr('type');

$srcField = CHtml::textField($model->name . '[src]', (($model->src) ? $model->src : ''), $htmlOptions = array(
	'class' => 'g10 p-f0',
	'placeholder' => 'Enter a video code/url'
));

echo CHtml::tag('div', $htmlOptions=array(
	'style'=>'clear: both; padding-top: 10px;',
), $srcField);
