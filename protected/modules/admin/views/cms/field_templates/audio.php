<?php
$audio = '<audio controls title="Audio File" class="stm-audio" id="audio-1" data-src="'.$model->{"data-src"}.'">
                <source src="'.$model->{"data-src"}.'" type="audio/mpeg"></source>
                <embed height="50" width="650" src="'.$model->{"data-src"}.'"></embed>
            </audio>';


echo CHtml::tag('div', $htmlOptions=array(), $audio);

echo CHtml::tag('div', $htmlOptions=array(
        'class'=>'g12',
        'style'=>'margin-top: 10px;',
    ), CHtml::tag('h4', $htmlOptions=array(
            'style'=>'text-align: left;',
        ), 'Select an MP3 file (100Mb max):'));
echo CHtml::fileField($model->name, $model->{"data-src"}, $htmlOptions=array(
        'class'=>'g3',
        'style'=>'clear:both;',
    ));
echo CHtml::hiddenField($model->name, $model->{"data-src"});