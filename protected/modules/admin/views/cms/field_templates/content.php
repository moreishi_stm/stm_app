<?php
    /*Yii::app()->controller->widget('admin_module.extensions.tinymce.ETinyMce', array(
        'name'           => $model->name,
        'value'          => $model->content,
        'useSwitch'      => false,
        'htmlOptions'=>array(
        	'width' => '300px',
        ),
        'options' => array(
            'theme'                           =>'advanced',
            'theme_advanced_toolbar_location' =>'top',
            'theme_advanced_toolbar_align'    =>'left',
            'theme_advanced_resizing'         =>'true',
            'theme_advanced_buttons1' => 'bold,italic,underline,strikethrough,separator,forecolor,backcolor,separator,justifyleft,justifycenter,justifyright,justifyfull,separator',
            'theme_advanced_buttons2' => 'fontselect,fontsizeselect,separator,bullist,numlist,separator,outdent,indent,blockquote,separator,undo,redo,link,unlink,separator,link,unlink,anchor,image,cleanup,help,code',
            'theme_advanced_buttons3' => 'tablecontrols',
            'height'                          =>'300',
            'width'                           =>'83%',
            'plugins'                         =>'advimage,table', //,'imagemanager','filemanager'
            'apply_source_formatting'         => true,
            'cleanup_on_startup'              =>false,
            'forced_root_block'               => '',
            'force_br_newlines'               => true,
            'force_p_newlines'                => true,
            'remove_linebreaks'               => false,
        ),
    ));*/

Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);

$scope = (YII_DEBUG) ? "local" : "com";
$s3BaseUrl = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";

$s3Url = urlencode(base64_encode('s3://'.$s3BaseUrl.'/site-files/'.Yii::app()->user->clientId."/cmsPages/"));
$fieldName = str_replace(array('[',']'),array('_',''),$model->name);

$js = <<<JS
	tinymce.PluginManager.load('moxiemanager', 'http://www.seizethemarket.{$scope}/imagemanager/plugin.js?v=4');
	tinymce.init({
		convert_urls: false,
		relative_urls: false,
		height : 400,
		selector: "#{$fieldName}",
		plugins: [
			"advlist autolink lists link image charmap print preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table paste moxiemanager" //spellchecker contextmenu
		],
		toolbar: "insertfile undo redo | styleselect | fontselect fontsizeselect forecolor backcolor bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image spellchecker", //spellchecker
		autosave_ask_before_unload: false,
		forced_root_block: false,
		setup : function(ed) {
		    ed.on('init', function() {
		    	this.getDoc().body.style.fontSize = '13px';
			});
		    ed.on('keyUp', function() { tinyMCE.triggerSave(); });
		},
		spellchecker_rpc_url: "http://www.seizethemarket.{$scope}/js/tinymce/plugins/spellchecker/spellchecker.php",
		//moxiemanager_apiPageUrl: "http://www.seizethemarket.{$scope}/imagemanager/api.php,
		//moxiemanager_baseUrl: "http://www.seizethemarket.{$scope}/imagemanager"
		init_instance_callback: function () {
			moxman.Env.baseUrl = "http://www.seizethemarket.{$scope}/imagemanager";
			moxman.Env.apiPageName = "api.php?v=1&s3={$s3Url}";
			//moxman.path = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
			//moxman.rootpath = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
		}
		//moxiemanager_rootpath: 's3://{$s3BaseUrl}/site-files/{$s3Url}',
		//moxiemanager_path: '/site-files/{$s3Url}',
//		external_plugins: { "nanospell": "http://www.seizethemarket.{$scope}/js/tinymce/plugins/nanospell/plugin.js" },
//	 	nanospell_server:"php"
	});

JS;

		//http://www.seizethemarket.local/imagemanager/api.php?
		//s3=czM6Ly9kZXYuc2l0ZXMuc2VpemV0aGVtYXJrZXQuY29tL3NpdGUtZmlsZXMvMS9jbXNQYWdlcy8%3D
		//&action=streamfile&path=%2FcmsPages
		//&name=one-story-craftsman-style-home-plans-l-d28baafe08f326b5.jpg
		//&loaded=0
		//&total=105057
		//&id=null
		//&csrf=41d9afb847906a6d5bafccc453a2c6a461204ab064876d1e1a0c198a554db7d2
		//&resolution=default

Yii::app()->clientScript->registerScript('tinyMceNew_'.$fieldName, $js);
?>
<textarea id="<?=str_replace(array('[',']'),array('_',''),$model->name);?>" name="<?=$model->name;?>">
	<?=$model->content;?>
</textarea>