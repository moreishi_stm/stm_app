<?php
	$this->breadcrumbs = array(
        'House Values Sites' => '',
	);
?>
<?php if(!$isMaxed): ?>
<div id="listview-actions">
    <a href="/<?php echo $this->module->name;?>/cms/addHouseValues/<?php echo Yii::app()->user->primaryDomain->id; ?>" class="button gray icon i_stm_add">Add House Values Site</a>
</div>
<?php endif; ?>

<h1>House Values Sites</h1>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
        <?php $this->renderPartial('_listSearchBoxLandingPages', array(
				'model' => $DataProvider->model,
			)
		); ?>
	</div>

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'house-values-landing-pages-grid',
        'template'=>'{summary}{items}{summary}{pager}',
		'dataProvider' => $DataProvider,
		'itemsCssClass' => 'datatables',
		'columns' => array(
//            array(
//                'type' => 'raw',
//                'name' => 'Seller Name',
//                'value' => '$data->contact->fullName."<br />Assigned to: ".$data->printTransactionAssignments()',
//            ),
            'title',
            'url',
//            array(
//                'type' => 'raw',
//                'name' => 'Added',
//                'value' => 'Yii::app()->format->formatDate($data->added)."<br />".$data->addedBy->fullName',
//            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/cms/editHouseValues/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/houseValues/".$data->url."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">Live View</a></div>"
			',
				'htmlOptions' => array('style' => 'width:110px'),
			),
		),
	)
);
