<?php
Yii::app()->clientScript->registerScript(
    'editTemplateContent', <<<JS
    editContentBaseUrl = $('a#edit_template_content').attr('href');

    $('a#edit_template_content').click(function() {
        var usersSelectedTemplate = $('select#CmsContent_template_name option:selected').val();
        $(this).attr('href', editContentBaseUrl+'/'+'template'+'/'+usersSelectedTemplate);
    });

    $('#edit_template').click(function() {
        $(this).hide();
        $('#cms_template').fadeIn('slow');
        return false;
    });

    $('#cms_template').change(function() {
        var selectedTemplate = $(this).val();

        url = window.location.href.replace(/\/template\/\w+\/?\/status\/\w+\/?/, '');

        if (selectedTemplate) {
            window.location.href = url+'/template/'+selectedTemplate+'/status/changed';
        } else {
            window.location.href = url;
        }
    });

    $('.cancel-template-change').click(function() {
        $('#edit_template').show();
        $('#cms_template').hide();
        $.fancybox.close();
    });

    $('.confirm-template-change').click(function() {
        $.fancybox.close();
    });
JS
);

$css
    = <<<'CSS'
	div#edit_template_warning em.icon {
		top: 5px;
		padding-right: 10px;
	}

	div#edit_template_warning h4 {
		color: #9d0a10;
		font-weight: bold;
		font-size: 25px;
	}

	div#edit_template_warning p {
		padding-top: 20px;
		font-size: 20px;
		line-height: 25px;
	}

	span.template-name {
	    padding: 5px;
	    border-radius: 5px;
	    background: #FFF;
	    border: 1px solid #999999;
	    margin: 5px;
	}
CSS;
Yii::app()->clientScript->registerCss('editTemplate', $css);

$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
        'target' => 'a#edit_template', 'config' => array(
            'width' => '600', 'height' => 'auto', 'scrolling' => 'no', 'autoDimensions' => false, 'padding' => '40',
        ),
    )
);

$form = $this->beginWidget(
    'CActiveForm', array(
        //	'id' => 'contact-form',
        'id'          => 'page-content-form', 'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )
);
?>
    <div style="display: none">
        <div id="edit_template_warning">
            <h4><em class="icon i_alert red"></em>WARNING: Changing Your Template</h4>

            <p>
                Changing templates may cause you to lose some data for fields that no longer exist. You will need to
                update the content of this page. To change the template, select a different template name from the drop
                down menu that will appear. <a class="confirm-template-change"
                                               style="color: blue; text-decoration: underline;">Click here to confirm
                    and continue</a>.<br/><br/><span class="g100 p-tc">OR</span><br/><br/>To keep the existing template
                <a class="cancel-template-change" style="color: blue; text-decoration: underline;">click here to
                    cancel</a>.
            </p>
        </div>
    </div>
    <div id="action-plans-container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'status_ma');?></th>
                    <td>
                        <div class="g4">
                            <?php echo $form->dropDownList(
                                $model, 'status_ma', $model->statusTypes
                            ); ?>
                            <?php echo $form->error($model, 'status_ma'); ?>
                        </div>
                    <?php if(Yii::app()->user->checkAccess('widgets')): ?>
                        <div class="g2 p-tr">
                            <?php echo $form->labelEx(
                                $model, 'published', $htmlOptions = array('class' => 'label')
                            );?>
                        </div>
                        <div class="g6">
                            <?php
                            $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                                    'model'=>$model,
                                    'attribute'=>'published',
                                    // additional javascript options for the date picker plugin
                                    'mode'=>'datetime',
                                    'options'=>array(
                                        // 'showAnim'=>'fold',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'g5',
                                        // 'value'=>date('m/d/Y h:i a'),
                                        //																							   'readonly'=>true,
                                    ),
                                ));
                            ?>


                        <?php if(strpos($this->action->id, 'edit') !== false): ?>
                            <a href="/<?php echo $viewPageLink; ?>" class="button gray icon i_stm_search" style="margin-left:100px;" target="_blank">View Page</a>
                        <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'template_name'); ?></th>
                    <td>
                        <div class="g4">
                            <?php
                            echo CHtml::tag(
                                'span', $htmlOptions = array('class' => 'template-name'),
                                Yii::app()->format->properCase($model->template_name)
                            );
                            echo CHtml::link(
                                'Edit Template', '#edit_template_warning', $htmlOptions = array(
                                    'class' => 'btn', 'id' => 'edit_template',
                                )
                            );

                            $templates = $model->templates[$this->getAction()->templateType];
                            $templates = array_combine($templates, $templates);

                            echo $form->dropDownList($model, 'template_name', $templates, $htmlOptions = array('style' => 'display: none', 'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'id'    => 'cms_template'));
                            echo $form->error($model, 'template_name'); ?>
                        </div>
                        <div class="g2 p-tr">
                            <?php echo $form->labelEx(
                                $model, 'cms_widget_group_id', $htmlOptions = array('class' => 'label')
                            );?>
                        </div>
                        <div class="g6">
                            <?php echo $form->dropDownList(
                                $model, 'cms_widget_group_id', CHtml::listData(CmsWidgetGroups::model()->findAll(), 'id', 'name'), $htmlOptions = array('class'=>'g5')
                            ); ?>
                            <?php echo $form->error($model, 'cms_widget_group_id'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'title'); ?></th>
                    <td>
                        <div class="g4">
                            <?php echo $form->textField($model, 'title', $htmlOptions = array('placeholder' => 'Title', 'class' => ''));?>
                            <?php
//                          ?>
                            <?php echo $form->error($model, 'title'); ?>
                        </div>
                        <div class="g2 p-tr">
                            <?php echo $form->labelEx(
                                $model, 'cmsTagsToAdd', $htmlOptions = array('class' => 'label')
                            );?>
                        </div>
                        <div class="g6">
                            <?php echo $form->dropDownList(
                                $model, 'cmsTagsToAdd', CHtml::listData(
                                    CmsTags::model()->byAccount(Yii::app()->user->account->id)->findAll(), 'id', 'name'
                                ), $htmlOptions = array(
                                    'data-placeholder' => 'Select Tags', 'multiple' => 'multiple', 'class' => 'g8'
                                )
                            );
                            $this->widget(
                                'admin_module.extensions.EChosen.EChosen', array(
                                    'target' => 'select#CmsContents_cmsTagsToAdd', 'options' => array(),
                                )
                            );
                            ?><a href="javascript:void(0)" class="text-button add-cms-tag-button" style="display:inline-block;"><em class="icon i_stm_add"></em>Add New Tag</a>

                            <?php echo $form->error($model, 'cmsTagsToAdd'); ?>
                        </div>

                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'url'); ?></th>
                    <td>
                        <div class="g4">
                            <?php echo $form->textField(
                                $model, 'url', $htmlOptions = array(
                                    'placeholder' => 'URL'
                                )
                            );?>
                            <?php echo $form->error($model, 'url'); ?>
                        </div>
                    </td>
                </tr>
                <?php if ($model->isStoryboardType()): ?>
                    <tr>
                        <th class="narrow"><label>Enable Client Access</label></th>
                        <td><?php echo $form->dropDownList(
                                $model, 'enableClientAccess', StmFormHelper::getYesNoList(), $htmlOptions = array()
                            );?>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'seo_description'); ?></th>
                    <td>
                        <div class="g10">
                            <?php echo $form->textArea($model, 'seo_description', $htmlOptions = array('placeholder' => 'SEO Description'));?>
                            <?php echo $form->error($model, 'seo_description'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'seo_keywords'); ?></th>
                    <td>
                        <div class="g10">
                            <?php echo $form->textArea($model, 'seo_keywords', $htmlOptions = array('placeholder' => 'SEO Keywords', 'rows'=>2, 'style'=> 'min-height: 35px;'));?>
                            <?php echo $form->error($model, 'seo_keywords'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'meta_tags'); ?></th>
                    <td>
                        <div class="g10">
                            <?php echo $form->textArea($model, 'meta_tags', $htmlOptions = array('placeholder' => 'Additional Meta Tags'));?>
                            <?php echo $form->error($model, 'meta_tags'); ?>
                            <div class="label">This is an advanced option. These Meta Tags are in addition to the SEO Description and Keywords. DO NOT use this unless you understand Meta Tags as it can cause unintended results for your page output and SEO. <br>Content should be the entire meta tag (ex. &lt;meta name="some_goes_here" content="content_goes_here"&gt;&lt;/meta&gt;)</div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'display_tags'); ?></th>
                    <td>
                        <div class="g10">
                            <?php echo $form->dropDownList(
                                $model, 'display_tags', array('1' => 'Yes', '0' => 'No')
                            ); ?>
                            <?php echo $form->error($model, 'display_tags'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'allow_comments'); ?></th>
                    <td>
                        <div class="g10">
                            <?php echo $form->dropDownList(
                                $model, 'allow_comments', array('1' => 'Yes', '0' => 'No')
                            ); ?>
                            <?php echo $form->error($model, 'allow_comments'); ?>
                        </div>
                    </td>
                </tr>
            </table>
            <?php
            $this->renderPartial(
                '_content', array(
                    'model' => $model, 'Template' => $Template,
                )
            );
            ?>
        </div>
    </div>
<?php
$this->endWidget();

Yii::import('admin_widgets.DialogWidget.CmsTagDialogWidget.CmsTagDialogWidget');
if (!class_exists('TaskDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.CmsTagDialogWidget.CmsTagDialogWidget', array(
            'id' => 'cms-tag-dialog',
            'title' => 'Add Cms Tag',
            'triggerElement' => '.add-cms-tag-button',
        )
    );
}
