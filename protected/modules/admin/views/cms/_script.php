<?php Yii::app()->clientScript->registerScript('editTemplateContent', <<<'JS'
editContentBaseUrl = $('a#edit_template_content').attr('href');

$('a#edit_template_content').click(function() {
var usersSelectedTemplate = $('select#CmsContent_template_name option:selected').val();
$(this).attr('href', editContentBaseUrl+'/'+'template'+'/'+usersSelectedTemplate);
});

$('#edit_template').click(function() {
$(this).hide();
$('#cms_template').fadeIn('slow');
return false;
});

$('#cms_template').change(function() {
var selectedTemplate = $(this).val();

url = window.location.href.replace(/\/template\/\w+\/?\/status\/\w+\/?/, '');

if (selectedTemplate) {
window.location.href = url+'/template/'+selectedTemplate+'/status/changed';
} else {
window.location.href = url;
}
});
JS
);

Yii::app()->clientScript->registerScript('cmsUrl',  <<<'JS'
$('#CmsContents_title').keyup(function(e) {
	if ((e.keyCode>=37 && e.keyCode<=40) || e.keyCode==16)
		return;

	var url = $('#CmsContents_title').val();
	var title = url;

	title = title.replace(/  /g, ' ');
	url = url.replace( / /g, '-' ).toLowerCase();
	url = url.replace(/[^a-zA-Z0-9-]+/g,'');

	$('#CmsContents_title').val(title);
	$('#CmsContents_url').val(url);

	if ($('#CmsContents_url').val().indexOf('--') >= 0) {
		url = url.replace(/--/g,'-')
		$('#CmsContents_url').val(url);
	}
});
JS
);