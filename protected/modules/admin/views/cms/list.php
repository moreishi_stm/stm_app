<?php
$this->breadcrumbs=array(
	'Domains'=>array('/admin/cms'),
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('domain-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="content-header">
	<h1>My Web Sites</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <? $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
        'id'=>'domain-list-search',
    ));
    ?>
        <div class="g12">
            <div class="g10">
                <label class="g4">Domain:</label>
                <span class="g6"><?php echo $form->textField($model,'name');?></span>
            </div>
            <div class="g2 submit" style="text-align:center">
                <?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?>
            </div>
        </div>
    <? $this->endWidget(); ?>
</div><!-- search-form -->

<?php
$this->widget('admin_module.components.StmGridView', array(
    'id'=>'domain-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        // 'id',
        array(
            'type'=>'raw',
            'name'=>'Status',
            'value'=>'StmFormHelper::getStatusBooleanName($data->is_active)',
        ),
        'name',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
                "<div><a href=\"/".Yii::app()->controller->module->id."/cms/domain/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View Details</a></div>"
            ',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
    ),
));