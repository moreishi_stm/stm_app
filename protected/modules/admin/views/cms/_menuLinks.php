<div class="menuLinksContainer">
	<button class="saveMenuBtn" data-selector="#menus-tab<?=$menu_id;?>-menu-links" data-menu-id="<?=$menu_id;?>" style="position: absolute; top: 0; right: 0;">Save Menu</button>
	<div class="NoLinks<?php if(count($menuLinkItems[0]) > 0):?> hidden<?php endif; ?>">There are currently no links. Please add a custom link or select links from the pages on the left.</div>
	<ol id="menus-tab<?=$menu_id;?>-menu-links" class="menuLinksList sortable ui-sortable mjs-nestedSortable-branch">
		<?php
		foreach($menuLinkItems[0] as $link):
			$inc->i++; ?>
			<li id="page-<?=$inc->i;?>" data-selector="#page-<?=$inc->i;?>" data-name="<?=base64_encode($link['name']); ?>" class="mjs-nestedSortable-branch" data-link-id="<?=$link['id'];?>">
				<div class="menuDiv">
					<span class="disclose"><span></span></span>
					<?=ucwords(str_replace('_',' ', $link['entity_type'])); ?><?php if(!empty($link['entity_id'])):?> (<?=$link['entity_id']; ?>)<?php endif; ?>
					<i class="fa fa-remove removeLink clearfix" data-link-id="<?=$link['id'];?>"></i> <?php if($link['entity_type'] == 'custom'):?><i class="fa fa-pencil-square-o editLink" data-link-id="<?=$link['id'];?>"></i><?php endif; ?>
					<div class="clearfix"></div>
					<div class="menuEdit">
						<div class="contents" id="contents_<?=$link['id'];?>">
							<strong>Title:</strong> <span id="link_title_<?=$link['id'];?>"><?=$link['name'];?></span><br />
							<strong>URL:</strong> <span id="link_url_<?=$link['id'];?>"><?=$link['url']; ?></span>
						</div>
						<div class="editContents" id="editContents_<?=$link['id'];?>">
							<form action="/admin/cms/updateCustomLink" method="post">
								<input type="hidden" name="link_id" value="<?=$link['id'];?>" />
								<input type="text" name="title" value="<?=$link['name'];?>" />
								<input type="text" name="url" value="<?=$link['url'];?>" />
								<div>
									<input type="submit" class="button" value="Save" />
									<input type="button" class="button gray cancelEdit" data-link-id="<?=$link['id'];?>" value="Cancel" />
								</div>
							</form>
						</div>
					</div>
				</div>
			<?php if(isset($menuLinkItems[$link['id']])  && !empty($menuLinkItems[$link['id']])) {
				ksort($menuLinkItems[$link['id']]);
				$this->renderPartial('_menuSubLinks', array('links' => $menuLinkItems[$link['id']], 'inc' => &$inc, 'menuLinkItems' => $menuLinkItems));
			} ?>
			</li>
		<?php endforeach; ?>
	</ol>
</div>