<?php
$this->breadcrumbs = array(
    $model->name => array('/admin/cms/domain/' . $model->id),
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('domain-pages-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
    <div id="listview-actions">
        <?php
        if($this->module->id=='admin') {
            echo CHtml::link('Add New Page', array('/'.$this->module->id.'/cms/addPage', 'id' => $model->id), array(
                    'class' => 'button gray icon i_stm_add',
                ));
            echo CHtml::link('Add New Blog', array('/'.$this->module->id.'/cms/addBlog', 'id' => $model->id), array(
                    'class' => 'button gray icon i_stm_add',
                ));
        }
        else {
            echo CHtml::link('Add New Page', array('/'.$this->module->id.'/cms/addBlog', 'id' => $model->id), array(
                    'class' => 'button gray icon i_stm_add',
                ));
        }
        ?>
    </div>
    <div id="content-header">
        <h1>Site Pages for <u><?php echo $model->name?></u></h1>
    </div>

    <div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
        <? $this->renderPartial('_listSearchBox', array('model' => $CmsContentModel)); ?>
    </div><!-- search-form -->

<?php
$this->widget('admin_module.components.StmGridView', array(
    'id' => 'domain-pages-grid',
    'dataProvider' => $CmsContentModel->search(),
    'itemsCssClass' => 'datatables',
    'columns' => array(
        array(
            'type' => 'raw',
            'name' => 'Status',
            'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)',
        ),
        'title',
        'url',
        array(
            'type' => 'raw',
            'name' => 'Widget Group',
            'value' => '$data->cmsWidgetGroup->name',
        ),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->id."/cms/edit".ucwords($data->pageType)."/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit Page</a></div>"
            ',
            'htmlOptions' => array('style' => 'width:120px'),
        ),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => '
                "<div><a href=\"/".(($data->isStoryboard)? "storyboard/":"").$data->url.(($data->isStoryboard)? "/".$data->id :"")."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Online</a></div>"
            ',
            'htmlOptions' => array('style' => 'width:120px'),
        ),
    ),
));