<?php
    $this->widget('admin_module.extensions.placeholder.Placeholder');

?>
<style type="text/css">
    #listingStandard-template .storyboard-header {
        width: 100%;
        margin-left: auto;
        margin-right: auto;
    }
    #listingStandard-template .listingstoryboard-logo {
        width: 228px;
        height: 70px;
    }
    #listingStandard-template .label {
        font-weight: bold;
        font-size: 15px;
        display: inline;
    }
    #listingStandard-template .seller-response {
        padding: 10px 0;
    }
    #listingStandard-template .inline, #lifestyleStandard-template .inline p {
        display: inline;
    }
    #listingStandard-template #thank-you-message {
        background-color: #a0ffa0;
        text-align: justify;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        padding: 20px 10px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

        -moz-box-shadow:    0px 2px 2px 0px #ccc;
        -webkit-box-shadow: 0px 2px 2px 0px #ccc;
        box-shadow:         0px 2px 2px 0px #ccc;
    }

    #listingStandard-template #video-container iframe.stmcms {
        margin-top: 30px;
        border: 13px solid black;
        border-radius: 8px;
    }
    #listingStandard-template #video-container #tv-base{
        background: url('http://cdn.seizethemarket.com/assets/images/tv_base.png');
        width: 347px;
        height: 60px;
        margin-left: auto;
        margin-right: auto;
        top: -7px;
        position: relative;
    }
</style>
<div id="listingStandard-template">
    <div class="g12">
        <div style="text-align: center;">
            <div class="storyboard-header">
                <div class="g4" style="padding-top: 12px;">
                    <img class="listingstoryboard-logo" src="http://cdn.seizethemarket.com/assets/images/listing_storyboard.png" alt="Listing Storyboard &trade;" title="Listing Storyboard &trade;">
                </div>
                <div class="g8">
                    <h1 style="line-height: 30px; font-size: 25px; text-align: right;">
                        <?php echo $Address->address; ?><br />
                        <?php echo $Address->city; ?>, <?php echo AddressStates::getShortNameById($Address->state_id); ?> <?php echo $Address->zip; ?>
                    </h1>
                    <h3 style="text-align: right; font-size: 20px;">
                        <span style="color:#08C000;"><?php echo ($price = $Transaction->getFieldValue('price_current'))? Yii::app()->format->formatDollars($price): 'Call for Pricing'; ?></span>
                        <?php echo ($beds = $Transaction->getFieldValue('bedrooms'))? '&nbsp;|&nbsp; '.$beds.' Beds': ''; ?>
                        <?php if ($baths = $Transaction->getFieldValue('baths')) {
                                    echo '&nbsp;|&nbsp; '.$baths;
                                    if($halfBaths = $Transaction->getFieldValue('half_baths')) {
                                        if($halfBaths == 1) {
                                            echo '&frac12;';
                                        } else {
                                            echo '.'.$halfBaths;
                                        }
                                    }
                                    echo ' Baths';
                              }

                        ?>
                        <?php echo ($baths = $Transaction->getFieldValue('sq_feet'))? '&nbsp;|&nbsp; '.number_format($baths).' Sq.Feet': ''; ?>
                    </h3>
                </div>
            </div>

            <?php if (($fields['video-main'] != 'photo_file.png') && !empty($fields['video-main'])): ?>
                <div id="video-container">
                    <iframe id="video-main" width="700" height="390" allowfullscreen="true" frameborder="0" class="stmcms video-thumbnail" title="Video" placeholder="Enter a video url" src="" data-type="youtube"></iframe>
                    <div id="tv-base">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="p-clr"></div>
    <h3 style="font-family: Georgia, 'Times New Roman'; font-size: 40px; font-weight: normal; margin-top: 20px;">"Seller Says..."
        <div class="p-fr" style="position: relative; top:-5px;">
            <a href="http://www.facebook.com/sharer/sharer.php?u=http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
                <img src="http://cdn.seizethemarket.com/assets/images/facebook_share.png" alt="Facebook Share Listing Storyboard &trade;" title="Facebook Share Listing Storyboard &trade;" >
            </a>
        </div>
    </h3>
    <hr style="clear:both;">
    <?php if ($fields['photo-1'] != 'photo_file.png'): ?>
        <div class="p-fr p-ml10">
            <img id="photo-1" title="Photo #1 on Right" src="photo_file.png" class="stmcms" style="width: 500px;"/>
        </div>
    <?php endif;

    if (!empty($fields['about-home'])): ?>
        <div class="seller-response">
            <span class="label">About the Home:</span>
            <div id="about-home" class="stmcms inline" title="About the Home"></div>
        </div>
    <?php endif;

    if (!empty($fields['grocery'])):
        ?>
        <div class="seller-response">
            <span class="label">Favorite Grocery Store:</span>
            <div id="grocery" class="stmcms inline" title="Favorite Grocery Store"></div>
        </div>
    <?php endif;

    if ($fields['photo-2'] != 'photo_file.png'): ?>
        <div class="p-fl p-mr10">
            <img id="photo-2" title="Photo #2 on Left" src="photo_file.png" class="stmcms" width="500px"/>
        </div>
    <?php endif;

    if (!empty($fields['restaurant'])):
        ?>
        <div class="seller-response">
            <span class="label">Favorite Restaurant:</span>
            <div id="restaurant" class="stmcms inline" title="Favorite Restaurant"></div>
        </div>
    <?php endif;

    if (!empty($fields['love-area'])):
        ?>
        <div class="seller-response">
            <span class="label">What I Love About the Area:</span>
            <div id="love-area" class="stmcms inline" title="What I Love About the Area"></div>
        </div>
    <?php endif;

    if (!empty($fields['hobbies'])):
        ?>
        <div class="seller-response">
            <span class="label">Hobbies:</span>
            <div id="hobbies" class="stmcms inline" title="Hobbies"></div>
        </div>
    <?php endif;

    if (!empty($fields['utilities'])):
        ?>
        <div class="seller-response">
            <span class="label">Utilities:</span>
            <div id="utilities" class="stmcms inline" title="Utilities"></div>
        </div>
    <?php endif;

    if (!empty($fields['improvements'])):
        ?>
        <div class="seller-response">
            <span class="label">Improvements & Upgrades:</span>
            <div id="improvements" class="stmcms inline" title="Improvements & Upgrades"></div>
        </div>
    <?php endif;

    ?>
    <div class="g12" style="background-color:#FCF4E1; text-align: center; border-top: 2px solid black; border-bottom: 2px solid black; margin: 20px 0 20px 0;padding: 15px;">
        <div class="g5"><img style="width: 263px;" src="/assets/a4dc7b64/sneak_preview.png" alt=""/></div>
        <div id="form" class="g5">
            <?php
            $formId           = Forms::FORM_STORYBOARD_SNEAK_PREVIEW;
            $SubmissionValues = new FormSubmissionValues($formId);
            $FormFields       = new FormFields;
            if(isset($_GET['err'])) {
                Yii::app()->user->setFlash('error', $_GET['err']);
            }

            if (!Yii::app()->user->isGuest) {

                $SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;
                $SubmissionValues->data[$FormFields->getField('last_name')->id]  = Yii::app()->user->lastName;
                $SubmissionValues->data[$FormFields->getField('email')->id]      = Yii::app()->user->contact->getPrimaryEmail();
                $SubmissionValues->data[$FormFields->getField('phone')->id]      = Yii::app()->user->contact->getPrimaryPhone();
            }

            $form = $this->beginWidget('CActiveForm', array(
                                                      'id'                     => 'sneak-preview-form',
                                                      'action'                 => array('/front/forms/videoForm/formId/' . $formId),
                                                      'enableAjaxValidation'   => true,
                                                      'enableClientValidation' => false,
                                                      'clientOptions'          => array(
                                                          'validateOnChange'   => false,
                                                          'validateOnSubmit'   => true,
                                                          'beforeValidate'     => 'js:function(form) {
											if($("#name-container").hasClass("hidden")) {
												$("div#name-container").hide().removeClass("hidden").show("normal");
												return false;
											} else {
												$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
												return true;
											}
										}',
                                                          'afterValidate'    => 'js:function(form, data, hasErrors) {
										$("div.loading-container.loading").remove();
										if (!hasErrors && (typeof data.error == "undefined")) {
											$("div#form").addClass("hidden");
											$("div#thank-you-message").hide().removeClass("hidden").show("normal");
										} else if (typeof data.error != "undefined") {
											Message.create("error",data.error);
										}
										return false;
									}')
                                                      ));
            ?>
            <span class="g12 p-tl" style="font-size:18px; font-style: italic; font-weight: bold;">See this home before the rest of the market!</span>
            <span class="p-tl" style="font-size: 18px; display:block;"><?php echo $Address->address;?></span>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                  'model' => $SubmissionValues,
                                                                  'attribute' => 'data['. $FormFields->getField('contact_date')->id .']',
                                                                  'options' => array('showAnim' => 'fold'),
                                                                  'htmlOptions' => array('placeholder' => 'Date','class' =>'g5'),
                                                                  )); ?>
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('contact_time')->id.']', $htmlOptions=array(
                    'placeholder'=>'Time ex. 12:00pm','class'=>'g5',
                ));?>
            <div id="name-container" class="hidden p-clr">
                <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array(
                        'placeholder'=>'First Name','class'=>'g5 p-clr',
                    ));?>
                <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array(
                        'placeholder'=>'Last Name','class'=>'g5',
                    ));?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>
                <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array(
                        'placeholder'=>'Email','class'=>'g5 p-clr',
                    ));?>
                <?php $this->widget('StmMaskedTextField', array(
                                                          'model' => $SubmissionValues,
                                                          'attribute' => 'data['.$FormFields->getField('phone')->id.']',
                                                          'mask' => '(999) 999-9999',
                                                          'id' => 'FormSubmissionValues_data_4',
                                                          'htmlOptions' => array('class'=>'g5',),
                                                          )); ?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('email')->id.']',$htmlOptions=array('class'=>'errorMessage g5', 'style'=>'clear:none;')); ?>
                <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('phone')->id.']',$htmlOptions=array('class'=>'errorMessage g5','style'=>'clear:none;')); ?>
                <?php echo $form->error($SubmissionValues, 'id', $htmlOptions=array('class'=>'hidden','style'=>'clear:none;')); ?>
            </div>

            <input class="button g12" type="submit" value="Schedule a Sneak Preview" style="font-size: 16px; padding: 12px; width: 86%;"/>
            <div class="g12 p-tl" style="font-size: 17px;">or confirm by phone at <?php echo Yii::app()->user->settings->office_phone;?>.</div>
            <?php $this->endWidget(); ?>
        </div>
        <div id="thank-you-message" class="hidden g5">
            <p>
            <h3>Thank You!</h3>
            We received your request for a sneak preview! <br /><br /> We will call you shortly to confirm the appointment. Feel free to call us at <strong><?php echo Yii::app()->user->settings->office_phone;?></strong> for immediate assistance. <br /><br />We look forward to helping you. Have a great day! =)<br /><br /><a href="/area/all">Click here to continue your Home Search.</a>
            </p>
        </div>
    </div>
    <iframe width="100%" height="370" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.mx/?q=<?php echo str_replace(' ','+',$Address->address);?>,+<?php echo $Address->city;?>,+<?php echo AddressStates::getShortNameById($Address->state_id);?>&hl=en&t=h&amp;z=19&amp;output=embed&iwloc=near"></iframe>
</div>
<div>
    <a href="http://www.facebook.com/sharer/sharer.php?u=http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
        <img src="http://cdn.seizethemarket.com/assets/images/facebook_share.png" alt="Facebook Share Listing Storyboard &trade;" title="Facebook Share Listing Storyboard &trade;" >
    </a>
</div>
<?php
$this->widget(
'front_module.components.widgets.CmsCommentWidget.CmsCommentWidget'
);
?>
