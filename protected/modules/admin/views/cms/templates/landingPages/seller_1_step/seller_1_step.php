<?php
    $formId = Forms::FORM_HOUSE_VALUES;

    $formName = ($_GET['status']) ? $formId : $formId.'_Part1_details';

    $SubmissionValues = new FormSubmissionValues($formName);

    if($_GET['status']=='success') {
        $SubmissionValues->formPart = 'success';
    } else {
        $SubmissionValues->formPart = '_all_details';
    }


    if(isset($_GET['address']) && isset($_GET['city']) && isset($_GET['state']) && isset($_GET['zip'])  && ($_GET['status']=='success' || $_GET['status']==2)) {
        $SubmissionValues->data = array(FormFields::ADDRESS_ID=>$_GET['address'], FormFields::CITY_ID=>$_GET['city'], FormFields::STATE_ID=>$_GET['state'], FormFields::ZIP_ID=>$_GET['zip']);
    }

    $fields = $Template->loadedFields;
    $SubmissionValues->data[FormFields::SOURCE_DESCRIPTION_ID] = $fields['source-description'];
?>

<style type="text/css">
    body, div, #houseValues-template, h1, h2, h3, h4 { margin:0; padding: 0;}
    .loading-container.loading {
        display: block;
        position: fixed;
        background-color: #FFFFFF;
        background: white;
        background-image: none;
        opacity: .9;
        filter: Alpha(Opacity=90);
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000 !important;
    }

    .loading-container.loading em {
        display: block;
        background: url(http://cdn.seizethemarket.com/assets/images/loading3.gif) no-repeat;
        opacity: 1 !important;
        position: relative;
        top: 38%;
        margin-left: auto;
        margin-right: auto;
        height: 256px;
        width: 256px;
    }
    div.inline { display: inline-block; vertical-align: top;}
    #subtitle-2 {
        margin-top: 15px;
    }
    #subtitle-3 {
        font-size: 18px;
        font-weight: normal;
        margin-bottom: 20px;
    }
    .top-area {
        padding-bottom: 100px;
        background-color: #999;
        background-repeat: repeat-x;
        background-size: 100% 900px;
        background-image: -moz-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #444444), color-stop(100%, #999999));
        background-image: -webkit-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: -o-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: -ms-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: linear-gradient(to bottom, #444444 0%, #999999 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#444444', endColorstr='#999999', GradientType=0);
    }
    .top-box {
        position: relative;
        top: 50px;
        margin-left: auto;
        margin-right: auto;
        padding-bottom: 40px;
        width: 900px;
        background: rgba(255, 255, 255, 0.95);
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-box-shadow: 4px 4px 4px #666;
        -webkit-box-shadow: 4px 4px 4px #666;
        box-shadow: 4px 4px 4px #666;
    }
    .video-container{
        padding-top: 40px;
        text-align: center;
        border-right: 1px solid #C6C6C6;
    }
    .video-container img {
        width: 750px;
        height: 420px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -moz-box-shadow: 4px 4px 4px #666;
        -webkit-box-shadow: 4px 4px 4px #666;
        box-shadow: 4px 4px 4px #666;
    }
    .header-container {
        display: inline-block;
        vertical-align: top;
        padding: 20px 0 0 20px;
    }
    .form-container {
        display: inline-block;
        margin-top: 20px;
        margin-left: 50px;
        width: 790px;
    }
    .form-container input, .form-container select {
        font-size: 25px;
    }
    .form-container select {
        height: 45px;
    }
    .form-container form .address {
        /*width: 460px;*/
    }

    .form-container form .city {
        display: inline-block;
        /*width: 180px;*/
    }

    .form-container form .state {
        width:97%;
    }

    .form-container form .zip {
        width: 90%;
    }
    .form-container form .firstName, .form-container form .lastName, .form-container form .email, .form-container form .phone {
        width: 195px;
    }
    .form-box .center {
        text-align: center;
    }
    #submit-button{
        font-size: 18px !important;
        height: 45px;
        margin-left: 50px;
    }
    div.arrow {
        background: url('http://cdn.seizethemarket.com/assets/images/cmsTemplates/houseValues/standard/arrow.png') repeat scroll 0% 0% transparent;
        height: 131px;
        width: 234px;
        position: relative;
        top: 11px;
    }
    div.error select {
        background: none repeat scroll 0% 0% #FFB8B8;
        border: 1px solid #FF5757;
    }
    .row.submit {
        margin-top: 20px;
        padding-bottom: 40px;
    }
    a#forward-url {
        color: #1b83e6;
        font-size: 20px;
        font-weight: bold;
    }
    a#forward-url:hover {
        color: #D20000;
    }
    .bonus-area {
        width: 900px;
        height: 250px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 20px;
    }
    .bonus-area ul {
        padding: 0;
        min-height: 250px;
    }
    .bonus-area ul li {
        display: inline-block;
        width: 260px;
        height: 235px;
        padding-right: 50px;
        float: left;
        list-style: none;
    }
    .bonus-area ul li:last-child {
        padding-right: 0;
    }
    .bonus-area h3 {
        font-size: 24px;
        margin-bottom: 8px;
        line-height: 1.2;
    }
    .bonus-area .tagline {
        color: #D20000;
        font-weight: bold;
        font-size: 14px;
        margin-top: 15px;
        bottom: 0;
    }
    .bonus-area .forwardLink {
        font-size: 34px;
        color: black;
        position: relative;
        top: 100px;
        display: block;
        text-align: center;
    }
    .bonus-area .forwardMessage {
        font-size: 12px;
        color: #888;
        top: 120px;
        position: relative;
        text-align: center;
    }
    .footer {
        font-size: 12px;
        color: #888;
        margin-top: 25px;
        left: 0;
        text-align: center;
        clear: both;
        background-color: #e4e4df;
        padding: 14px;
    }
</style>

<div id="houseValues-template">
    <div class="top-area">
        <div class="top-box">
            <div class="video-container">
                <?php if(!empty($fields['main-video'])) { ?>
                    <iframe width="750" height="420" id="main-video" class="stmcms" placeholder="Enter a video url" src="" data-type="youtube" />
                <?php } else { ?>
                    <?php if(!empty($fields['photo-main'])): ?>
                        <img id="photo-main" title="Main Photo (size: 750 x 420. Displays if no video)" src="http://cdn.seizethemarket.com/assets/default-placeholder.png" class="stmcms"/>
                    <?php endif; ?>
                <?php } ?>
            </div>
            <div class="header-container">
                <div class="form-container">
                    <?php
                        $successForwardUrl = $fields['forward-url'];
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'houseValues-form',
                            /*@todo: form id needs to change??? - what about triple guarantee?*/
                            'action' => array('/front/forms/houseValues/formId/' . $formId),
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                            'clientOptions' => array(
                                'validateOnChange' => false,
                                'validateOnSubmit' => true,
                                'beforeValidate' => 'js:function(form, attribute) {
                                    $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                    return true;
                                }',
                                'afterValidate' => 'js:function(form, data, hasErrors) {
                                    if (!hasErrors) {
                                        window.location = "http://'.$_SERVER["SERVER_NAME"].'/l/'.$cmsUrl.'" + "/status/success";
                                    } else {
                                        $("div.loading-container.loading").remove();
                                    }
                                    return false;
                                }',
                            ),
                        ));
                    ?>
                        <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_DESCRIPTION_ID.']'); ?>
                        <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>

                            <h5 id="source-description" class="stmcms" title="Source Description" style="display: none;"></h5>
                            <h1 id="main-title" class="stmcms" title="Main Title"></h1>
                        <?php if($SubmissionValues->formPart != 'success'): ?>
                            <h2 id="header-subtitle" class="stmcms inline" title="Subtitle 1"></h2>
                            <h3 id="subtitle-2" class="stmcms" title="Subtitle 2"></h3>
                            <h4 id="subtitle-3" class="stmcms" title="Subtitle 3"></h4>

                            <div class="inline g6" style="border-right: 1px solid #e6e6e6; width: 47.5%; padding-right: .5%; margin-right: .5%;">
                                <div class="inline g12">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']', $htmlOptions = array('class' => 'address g97', 'placeholder' => 'Address'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
                                </div>
                                <div class="inline g6">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']', $htmlOptions = array('class' => 'city g95', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'City', 'value' => $opt['city']));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
                                </div>
                                <div class="inline g3">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array('empty' => 'State', 'class' => 'state g2'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
                                </div>
                                <div class="inline g3">
                                    <?php $this->widget('StmMaskedTextField', array(
                                            'stmMaskedTextFieldClass'=>'houseValuesZip',
                                            'model' => $SubmissionValues,
                                            'attribute' => 'data[' . $FormFields->getField('zip')->id . ']',
                                            'mask' => '99999',
                                            'id' => 'FormSubmissionValues_data_'.FormFields::ZIP_ID,
                                            'htmlOptions' => array('class' => 'zip', 'placeholder' => 'Zip'),
                                        )); ?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
                                </div>
                                <div class="inline g6">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array('class' => 'g95', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'First Name'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
                                </div>
                                <div class="inline g6">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array('class' => 'g95', 'placeholder' => 'Last Name'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
                                </div>

                            </div>
                            <div class="inline g6">
                                <div class="inline g6">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']', StmFormHelper::getBedroomList(' Bedrooms'), $htmlOptions = array('class' => 'g95', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'Bedrooms','empty'=>'Select Bedrooms'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']'); ?>
                                </div>
                                <div class="inline g6">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']', StmFormHelper::getHouseValuesBathList(), $htmlOptions = array('class' => 'g95', 'placeholder' => 'Baths', 'empty'=>'Select Baths'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']'); ?>
                                </div>                                <div class="inline g6">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']', $htmlOptions = array('class' => 'city g95', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'Sq. Feet'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']'); ?>
                                </div>
                                <div class="inline g6">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition'). ']', array('Excellent'=>'Excellent','Average'=>'Average','Below Average'=>'Below Average','Poor'=>'Poor'), $htmlOptions = array('class' => 'g95', 'placeholder' => 'Baths', 'empty'=>'Select Condition'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition') . ']'); ?>
                                </div>
                                <div class="inline g6 p-clr">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'g95', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'Email'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
                                </div>
                                <div class="inline g6">
                                    <?php $this->widget('StmMaskedTextField', array(
                                            'stmMaskedTextFieldClass'=>'houseValuesPhone',
                                            'model' => $SubmissionValues,
                                            'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                                            'mask' => '(999) 999-9999',
                                            'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                                            'htmlOptions' => array('class' => 'g95', 'placeholder' => 'Phone'),
                                        )); ?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($SubmissionValues->formPart == 'success'): ?>
                            <br/>
                            <h2>Your information has been successfully submitted.</h2>
                            <br/>
                            <h2 id="success-message-2" class="stmcms" title="Success Message 2"></h2>
                            <br/>
                            <h2>Thank you and have a great day!</h2>
                            <br/>
                            <br/>
                            <?php if(!empty($fields['forward-url'])): ?>
                                <div class="forwardMessage">You will be forwarded in 8 seconds...</div>
                                <a id="forward-url" href="" class="stmcms inline" title="Forward Url After Submit"><div id="forward-url-text" class="stmcms" title="Forward URL Text"></div></a>
                                <?php Yii::app()->clientScript->registerScript('completeForwarder', 'setTimeout(function(){ window.location = "' . $fields['forward-url'] . '";}, 10000)'); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        </div>
                    </div>
                    <?php if($SubmissionValues->formPart != 'success'):?>
                    <div class="row center submit">
                        <?php if (!empty($fields['submit-button'])) { ?>
                            <button id="submit-button" type="submit" class="stmcms submit g11" title="Submit Button Text" ></button>
                        <?php } else { ?>
                            <button id="submit-button" type="submit" class="submit g11" title="Submit Button Text" >Submit Now</button>
                        <?php } ?>
                    </div>
                    <?php endif; ?>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<div class="bonus-area">
    <ul>
        <li>
            <h3 id="bonus-title-1" class="stmcms" title="Bonus Title #1:"></h3>
            <span id="bonus-content-1" class="stmcms" title="Bonus Content #1"></span>

            <h5 id="bonus-tagline-1" class="stmcms tagline" title="Bonus Tag Line #1">- FREE Inside...</h5>
        </li>
        <li>
            <h3 id="bonus-title-2" class="stmcms" title="Bonus Title #2"></h3>
            <span id="bonus-content-2" class="stmcms" title="Bonus Title #2"></span>

            <h5 id="bonus-tagline-2" class="stmcms tagline" title="Bonus Tag Line #2">- FREE Inside...</h5>
        </li>
        <li>
            <h3 id="bonus-title-3" class="stmcms" title="Bonus Title #3"></h3>
            <span id="bonus-content-3" class="stmcms" title="Bonus Title #3"></span>

            <h5 id="bonus-tagline-3" class="stmcms tagline" title="Bonus Tag Line #3">- FREE Inside...</h5>
        </li>
    </ul>
</div>

<div class="footer">
    <div class="credits">© Copyright <?php echo date('Y');?>. Not intended to solicit currently listed properties.  <?php echo Yii::app()->user->accountSettings['office_name']; ?>.</div>
</div>