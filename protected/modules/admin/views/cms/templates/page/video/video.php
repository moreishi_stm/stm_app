<style>
    /**
     * Template Name: Video Page
     * Author: Seize the Market
     * Date: 11/21/2012
     * Description: Video Form Page.
     */

    #video-tmp .top-container{
        /*background-color: #FAFAFA;*/
        width: 940px;
        float: left;
        margin-top: 20px;
        background: whiteSmoke;
        -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 0 0 10px rgba(0,0,0,0.15);
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
        border: 1px solid #DDD;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        padding: 10px;

    /*
        margin-bottom: 15px;
    */
    }
    #video-tmp h1{
        text-align: left;
        color: #C20000;
    }
    #video-tmp h3{
        color: #C20000;
        font-weight: bold;
        font-size: 24px;
        text-align: center;
        line-height: 1.2;
    }

    #video-tmp .video-container{
        width: 590px;
        float: left;
        text-align: center;
        border-right: 1px solid #C6C6C6;
    }
    #video-tmp .form-container{
        width: 300px;
        float: left;
        padding: 20px;
        text-align: center;
    }

    #video-tmp .form-description{
        margin: 3px;
    }
    #video-tmp .form-description p {
        text-align: justify;
    }
    #video-tmp .form-description p span, #video-tmp #content p span{
        font-size: 15px !important;
        margin: 3px;
    }

    #video-tmp input{
        font-size: 14px;
        padding: 4px;
    }
    #video-tmp input#first_name, #video-tmp input#last_name{
        width: 120px;
        margin: 3px -1px;
    }
    #video-tmp input#email{
        width: 247px;
    }

    #video-tmp hr{
        margin: 20px 0;
    }
    #video-tmp button{
        font-size: 18px;
        width: 98%;
        margin-top: 25px;
    }
    #video-tmp .video-thumbnails{
        list-style: none;
        float: left;
    }
    #video-tmp .video-thumbnails .video-thumbnails-item{
        position: relative;
        float:left;
        border: 3px solid transparent;
        width: 200px;
        padding: 4px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        margin: 10px;
        text-align: center;
    }
    #video-tmp .video-thumbnails .video-thumbnails-item:hover{
        border: 3px solid #C20000;
        background: #FFEBEB;
    }
    #video-tmp .video-thumbnails .video-thumbnails-item a{
        text-align: center;
        /*float: left;*/
    }
    #video-tmp .video-thumbnails .video-thumbnails-item a h5{
        clear:both;
        float: left;
        width: 200px;
        font-size: 15px;
        cursor: pointer;
        color: #444;
    }
    #video-tmp .video-thumbnails .video-thumbnails-item img{
        width: 200px;
        margin-left: auto;
        margin-right: auto;
        display: block;
    }
    #video-tmp .video-thumbnails a .play-button {
        background-image: url(http://cdn.seizethemarket.com/assets/images/play_button.png);
        position: absolute;
        top: 45px;
        left: 29px;
        width: 152px;
        height: 42px;
        background-position: 0px 44px;
    }
    #video-tmp #content p{
        width: 600px;
    }
    #video-tmp .form-container button{
        color: #07136B;
        font-style: italic;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFC26', endColorstr='#FFD903');
        background: -webkit-gradient(linear, left top, left bottom, from(#FFFC26), to(#FFD903));
        background: -moz-linear-gradient(top, #FFFC26, #FFD903);
        background: -o-linear-gradient(top, #FFFC26, #FFD903);
        border-color: #FFEE00 #CFA723 #CFA723 #FAD900;
        border-width: 2px;

        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

        -moz-box-shadow:    0px 2px 2px 0px #ccc;
        -webkit-box-shadow: 0px 2px 2px 0px #ccc;
        box-shadow:         0px 2px 2px 0px #ccc;
    }

    #video-tmp .form-container div#email-input input:last-child ,#video-tmp .form-container div#phone-input input:last-child {
        width: 98%;
        clear: both;
    }

    #video-tmp .form-container div#name-inputs input:last-child {
        width: 47%;
        float: left;
    }
    #video-tmp #thank-you-message {
        background-color: #a0ffa0;
        text-align: justify;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        padding: 20px 10px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
        box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

        -moz-box-shadow:    0px 2px 2px 0px #ccc;
        -webkit-box-shadow: 0px 2px 2px 0px #ccc;
        box-shadow:         0px 2px 2px 0px #ccc;
    }

	@media screen and (max-width: 1000px) {
		#video-tmp .video-thumbnails .video-thumbnails-item {
			width: 23%;
			min-width: 152px;
			border: none;
			padding: 0;
			margin: 2px;
		}
		#video-tmp .video-thumbnails .video-thumbnails-item:last-of-type,
		#video-tmp .video-thumbnails .video-thumbnails-item:nth-of-type(4){
			margin-right: 0px;
		}

		#video-tmp .video-thumbnails .video-thumbnails-item a .play-button {
			left: 0;
		}
		#video-tmp .video-thumbnails .video-thumbnails-item img {
			width: 100%;
			float: none;
		}
	}

	@media screen and (max-width: 660px) {
		#video-tmp .video-thumbnails .video-thumbnails-item {
			width: 98%;
			min-width: 152px;
			border: none;
			padding: 0;
			margin: 0 2px 0 0;
		}
	}
</style>
<div id="video-tmp">
	<div class="top-container">
		<div class="video-container">
			<iframe width="560" height="315" id="main-video" class="stmcms" placeholder="Enter a video url" src="" data-type="youtube" />
		</div>
		<div class="form-container">
			<div id="form">
				<?php
					$formId           = Forms::FORM_VIDEO;
					$SubmissionValues = new FormSubmissionValues($formId);
					$FormFields       = new FormFields;
					if(isset($_GET['err'])) {
						Yii::app()->user->setFlash('error', $_GET['err']);
					}

					if (!Yii::app()->user->isGuest) {

						$SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;
						$SubmissionValues->data[$FormFields->getField('last_name')->id]  = Yii::app()->user->lastName;
						$SubmissionValues->data[$FormFields->getField('email')->id]      = Yii::app()->user->contact->getPrimaryEmail();
						$SubmissionValues->data[$FormFields->getField('phone')->id]      = Yii::app()->user->contact->getPrimaryPhone();
					}

					$form = $this->beginWidget('CActiveForm', array(
						'id'=>'video-form',
						'action' => array('/front/forms/videoForm/formId/'.$formId),
						'enableAjaxValidation'   => true,
						'enableClientValidation' => false,
						'clientOptions' => array(
							'validateOnChange' => false,
							'validateOnSubmit' => true,
							'beforeValidate'    => 'js:function(form) {
									$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
									return true;
								}',
							'afterValidate' => 'js:function(form, data, hasErrors) {
								$("div.loading-container.loading").remove();
								if (!hasErrors) {
									Message.create("success", "Your request was sent successfully!");
									$("div#form").addClass("hidden");
									$("div#thank-you-message").hide().removeClass("hidden").show();
								}

								return false;
							}',
						)
					));


				?>
				<h3 id="form-title" class="stmcms" title="Form Title" placeholder="Add Your Title"></h3>
				<span id="form-description" class="stmcms form-description" title="Form Description">
					Insert body paragraph text.
				</span>
				<div id="name-inputs">
					<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array(
						'placeholder'=>'First Name','class'=>'g6',
					));?>
					<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array(
						'placeholder'=>'Last Name','class'=>'g5',
					));?>
					<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']'); ?>
					<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']'); ?>
				</div>
				<div class="row">
					<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array(
						'placeholder'=>'E-mail','class'=>'g6',
					));?>
					<?php $this->widget('StmMaskedTextField', array(
															  'model' => $SubmissionValues,
															  'attribute' => 'data['.$FormFields->getField('phone')->id.']',
															  'mask' => '(999) 999-9999',
															  'id' => 'FormSubmissionValues_data_4',
															  'htmlOptions' => array('class'=>'g5',),
															  )); ?>
					<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('email')->id.']'); ?>
					<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('phone')->id.']'); ?>
					<?php echo CHtml::hiddenField('url',$_SERVER['REQUEST_URI']); ?>
				</div>

                <?php if (!empty($fields['submit-button'])) { ?>
                    <button id="submit-button" type="submit" class="stmcms submit" title="Submit Button Text" >Get Free Report</button>
                <?php } else { ?>
                    <button id="submit-button" type="submit" class="submit" title="Submit Button Text" >Get Free Report</button>
                <?php } ?>
				<?php $this->endWidget(); ?>
			</div>
			<div id="thank-you-message" class="hidden">
				<p>
					<h3>Thank You!</h3> We received your request. You will receive the information shortly. <br /><br /> Check your email for the information or call <strong><?php echo Yii::app()->user->settings->office_phone;?></strong> for personal assistance. <br /><br />We look forward to helping you. Have a great day! =)<br /><br /><a href="/area/all">Click here to continue your Home Search.</a>
				</p>
			</div>
		</div>
	</div>

	<div class="video-thumbnails">
		<div class="video-thumbnails-item">
			<a href="#" id="video-link-1" class="stmcms" title="Video #1 Link">
                <em class="play-button"></em>
				<iframe id="video-1" width="220" height="" class="stmcms video-thumbnail" title="Suggested Video #1" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
				<h5 class="stmcms" title="Suggested Video Title #1" id="video-1-title" placeholder="Video Title"></h5>
			</a>
		</div>
		<div class="video-thumbnails-item">
			<a href="#" id="video-link-2" class="stmcms" title="Video #2 Link">
                <em class="play-button"></em>
				<iframe  id="video-2" width="220" height="" class="stmcms video-thumbnail" title="Suggested Video #2" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
				<h5 class="stmcms" title="Suggested Video Title #2" id="video-2-title" placeholder="Video Title"></h5>
			</a>
		</div>
		<div class="video-thumbnails-item">
			<a href="#" id="video-link-3" class="stmcms" title="Video #3 Link">
                <em class="play-button"></em>
				<iframe  id="video-3" width="220" height="" class="stmcms video-thumbnail" title="Suggested Video #3" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
				<h5 class="stmcms" title="Suggested Video Title #3" id="video-3-title" placeholder="Video Title"></h5>
			</a>
		</div>
		<div class="video-thumbnails-item">
			<a href="#" id="video-link-4" class="stmcms" title="Video #4 Link">
                <em class="play-button"></em>
				<iframe  id="video-4" width="220" height="" class="stmcms video-thumbnail" title="Suggested Video #4" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
				<h5 class="stmcms" title="Suggested Video Title #4" id="video-4-title" placeholder="Video Title"></h5>
			</a>
		</div>
	</div>

	<hr />

	<h1 id="content-title" class="stmcms" title="Content Title" placeholder="Add Content Title"></h1>
	<div id="content" class="stmcms" title="Content">
		<p>Insert body paragraph text.</p>
	</div>
</div>