
<div class="g12">
	<div id="logo">
		<img title="Logo" src="photo_file.png" class="stmcms" id="logo-photo"/>
	</div>
	<div id="title-container">
		<h1 id="vendor-category" class="stmcms" title="Vendor Category">A/C, Roofer, Handyman, etc.</h1>
		<hr />
		<h2 class="stmcms" id="vendor-name" title="Vendor Name">A/C, Roofer, Handyman, etc.</h2>
		<div class="stmcms" id="phone" title="Phone">(999) 999-9999</div>
		<div id="website"><a href="http://www.website.com" title="Website">www.website.com</a></div>
	</div>
	<div class="stmcms" id="bio" title="Vendor Info &amp; Bio">
		<p>Insert a little bio about your vendor.</p>
	</div>

	<div id="services-container">
		<h3 class="stmcms" id="services-title" title="Services Title">Description of Services</h3>
		<div class="stmcms" id="services" title="Service Description">
			<p>Insert body paragraph text.</p>
		</div>
	</div>
</div>