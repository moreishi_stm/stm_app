<h1 class="stmcms" id="header" title="Page Header" style="padding-top: 30px; line-height: 1; font-size: 35px; height: auto;">Add Your Header</h1>

	<?php if (($fields['photo-1'] != 'photo_file.png') && (!empty($fields['photo-1']) && strpos($fields['photo-1'], 'photo_file.png') == false)): ?>
        <div class="g12">
            <div id="photo-1-container" style="text-align: center;">
                <img title="Main Photo" src="photo_file.png" class="stmcms" id="photo-1" width="600px"/>
            </div>
        </div>

	<?php endif; ?>

<div class="stmcms" id="content" title="Content">
    <p>Insert body paragraph text.</p>
</div>
