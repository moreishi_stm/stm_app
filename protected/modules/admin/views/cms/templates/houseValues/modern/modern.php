<style type="text/css">
    body {
    <?php if($SubmissionValues->formPart == '1_details' || $SubmissionValues->formPart == 'success'): ?>
        background-image: url(http://cdn.seizethemarket.com/assets/standard2/assets/slider/slider2.jpg);
        background-color: black;
        padding-top: 150px;
        background-position: top center;
        background-repeat: no-repeat;
    <? elseif($SubmissionValues->formPart == '2'): ?>
        background-image: none;
        background-color: white;
    <? endif; ?>
        overflow: auto;
    }
    <?php if($SubmissionValues->formPart == '1_details'): ?>
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: black;
        opacity: 1;
    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: black;
        opacity: 1;
    }
    ::-ms-input-placeholder { /* IE 10+ */
        color: black;
        opacity: 1;
    }
    ::-moz-placeholder { /* Firefox 18- */
        color: black;
        opacity: 1;
    }
    <? endif; ?>
    .row {
        margin-right: inherit;
    }
    .loading-container.loading {
        display: block;
        position: fixed;
        background-color: #FFFFFF;
        background: white;
        background-image: none;
        opacity: .9;
        filter: Alpha(Opacity=90);
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000 !important;
    }
    .loading-container.loading em {
        display: block;
        background: url(http://cdn.seizethemarket.com/assets/images/loading3.gif) no-repeat;
        opacity: 1 !important;
        position: relative;
        top: 38%;
        margin-left: auto;
        margin-right: auto;
        height: 256px;
        width: 256px;
    }
    .title-container {
        margin: 15px 15px 25px 15px;
        text-align: center;
    }
    .title-container.success {
        margin: 15px 15px 25px 15px;
        padding: 20px;
        background-color: rgba(0, 0, 0, 0.4);;
        text-align: center;
    }
    h1 {
        font-size: 60px;
        font-weight: bold;
        color: white;
        font-family: arial,helvetica,sans-serif;
        line-height: 60px;
    }
    .title-container-2 h1 {
        color: black;
        text-align: center;
        font-weight: bold;
        font-size: 38px;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        max-width: 1000px;
        margin: 17px auto;
        line-height: 50px;
        padding-bottom: 0;
    }
    h3 {
        margin-top: 10px;
        font-size: 30px;
        color: rgb(255, 255, 255);
        font-family: arial,helvetica,sans-serif;
        font-weight: bold;
        line-height: 35px;
    }
    .form-container {
        text-align: center;
        width: 620px;
        background: #FAFAFC;
        border-radius: 6px;
        padding: 13px 15px;
        margin: 0 auto;
        position: relative;
        background: -moz-linear-gradient(top, #ffffff 0%, #F4F4F9 47%, #E9E9F3 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(47%,#F4F4F9), color-stop(100%,#E9E9F3));
        background: -webkit-linear-gradient(top, #ffffff 0%,#F4F4F9 47%,#E9E9F3 100%);
        background: -o-linear-gradient(top, #ffffff 0%,#F4F4F9 47%,#E9E9F3 100%);
        background: -ms-linear-gradient(top, #ffffff 0%,#F4F4F9 47%,#E9E9F3 100%);
        background: linear-gradient(to bottom, #ffffff 0%,#F4F4F9 47%,#E9E9F3 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#E9E9F3',GradientType=0 );
    }

    .form-container-2 {
        background: #EBEBEB;
        padding: 15px 0 15px;
    }
    .form-container-2 .container {
        max-width: 1050px;
    }
    .form-container-2 h3 {
        font-size: 25px;
        font-weight: bold;
        color: #4CB07A;
        text-align: center;
        font-family: "Times New Roman";
    }
    .full-address {
        font-size: 15px !important;
        width: 390px;
        padding: 13px 10px;
        margin: 5px 5px 5px 0 !important;
        border: 1px solid #DCDCDC !important;
        border-radius: 5px 5px 5px 5px;
        height: inherit !important;
        color: black !important;
        opacity: 1;
    }
    .submit {
        background: #ffe27a;
        border: 1px solid #ffffff;
        font-size: 25px;
        padding: 12px 13px;
        font-weight: bold;
        font-family:'Tahoma';
    }
    .form-container-2 input {
        padding: 10px;
        font-size: 15px;
        height: inherit;
        color: black;
    }
    .form-container-2 .submit {
        background: rgb(255, 233, 36) none repeat scroll 0% 0%;
        border: 1px solid;
        border-radius: 5px;
        font-size: 32px;
        padding: 26px 38px;
        font-weight: bold;
        font-family: "Tahoma";
    }
    .form-container-2 .half.map {
        width: 49%;
        min-height: 400px;
    }
    .form-container-2 .half.fields {
        width: 46%;
    }
    #ValueReportFormGeoLocation_container{
        position: relative;
        display: inline-block;
    }
    #ValueReportFormGeoLocation_container .results{
        position: absolute;
        text-align: left;
        color: black;
        top: 51px;
        left: 0;
        z-index:100;
        background-color: #fff;
        display:none;
        border: 1px solid #ddd;
        min-width: 390px;
    }
    #ValueReportFormGeoLocation_container .no-results {
        padding: 6px 8px;
    }
    #ValueReportFormGeoLocation_container .results li{
        padding:8px;
        z-index:999;
    }
    #ValueReportFormGeoLocation_container .results li.selected,
    #ValueReportFormGeoLocation_container .results li:hover{
        background:#eee;
        cursor: pointer;
    }
    #FormSubmissionValues_data_14,
    #FormSubmissionValues_data_16,
    #FormSubmissionValues_data_17,
    #FormSubmissionValues_data_18{
        display:none;
    }
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        margin: 0;
        max-width: none;
        width: 100%;
        padding: 0;
        background: rgba(0,0,0,0.8);
        border: 0;
        height: auto;
        color: #9C95A1;
        font-size: 12px;
        font-family: Arial, Helvetica, sans-serif;
    }
    .footer-2 {
        position: inherit;
        max-width: 1050px;
        margin: 0 auto;
        padding: 35px 0;
        color: #9C95A1;
        font-size: 12px;
        background: white;
    }
    .footer div {
        padding: 5px 10px;
    }
    .footer .policy-terms a {
        text-decoration: underline;
    }
    @media (max-width: 1020px) {
        .form-container-2 .container {
            width: 100%;
        }
        .form-container-2 .half.map {
            display: none;
        }
        .form-container-2 .half.fields {
            float: none !important;
            margin: 0 auto;
            min-width: 390px;
            width: 49%;
        }
        .form-container-2 input:not([type="submit"]) {
            width: 100%;
        }
    }
    @media (max-width: 420px) {
        body {
            padding-top: 0;
        }
        .row {
            margin: 0;
        }
        .title-container {
            margin: 0;
            padding: 15px;
        }
        .title-container h1 {
            font-size: 60px;
            line-height: 1;
        }
        .title-container h3 {
            font-size: 30px;
            line-height: 1.3;
        }
        .title-container-2 h1 {
            font-size: 22px;
            line-height: 1.3;
        }
        .form-container {
            width: 100%;
            background: none;
            padding: 0 10px;
        }
        .form-container-2 h3 {
            margin-top: 0;
            line-height: 1.1;
            padding-bottom: 5px;
        }
        .form-container-2 input {
            width: 90% !important;
            margin: 10px auto;
        }
        .form-container-2 .half.fields {
            min-width: 100%;
        }
        .form-container-2 .submit {
            padding: 26px 0;
        }
        .container {
            width: 100%;
            max-width: 100% !important;
            min-width: inherit;
            margin: 0;
            padding: 0;
        }
        table, tbody, tr, td, #ValueReportFormGeoLocation_container, input {
            width: 100%;
            display: block;
        }
        .full-address {
            width: 100%;
        }
        .footer:not(.footer-2) {
            display: none;
        }
        .footer .credits, .footer .policy-terms {
            width: 49%;
        }
    }
</style>

<div class="<?=($SubmissionValues->formPart == '1_details') ? "container" : ""?>">
<?php if($SubmissionValues->formPart == '1_details'): ?>
    <div class="title-container">
        <h1>
            Find Out What Your<br>
            Home Is Worth For FREE
        </h1>
        <h3>
            Enter Your Address Below To Find Out Now:
        </h3>
    </div>
<?php elseif($SubmissionValues->formPart == 2): ?>
    <div class="title-container-2">
        <h1>
            Property Found At: <?=$_GET['address'] . ', ' . $_GET['city'] . ', '. AddressStates::getShortNameById($_GET['state']) . ' ' . $_GET['zip']?>
        </h1>
    </div>
<?php elseif($SubmissionValues->formPart == 'success'): ?>
    <div class="title-container success">
        <h1>
            Thank You
        </h1>
        <h3>
            Thanks for requesting your FREE Home Evaluation.
            <br>
            We're putting it together now and will be in touch very shortly.
            <br>
            <br>
            Sincerely,
            <br>
            <br>
            <?php echo Yii::app()->user->settings->office_name; ?>
        </h3>
    </div>
<?endif;?>
<?php if($SubmissionValues->formPart != 'success'): ?>

    <div class="form-container<?=($SubmissionValues->formPart == 2) ? "-2" : ""?>">
        <?php
        $successForwardUrl = $fields['forward-url'];
        $form = $this->beginWidget('CActiveForm', array(
                'id' => 'houseValues-form',
                'action' => array('/front/forms/houseValues/formId/' . $formId),
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'beforeValidate' => 'js:function(form, attribute) {

                        if (document.getElementById("FormSubmissionValues_data_3")) {

                            if($("#FormSubmissionValues_data_3").val() == "") {
                                alert("Please Enter Your Email Address.");
                                return false;
                            }
                            if($("#FormSubmissionValues_data_52").val() == "") {
                                alert("Please Enter Your Name.");
                                return false;
                            }
                            if($("#FormSubmissionValues_data_52").val() == "") {
                                alert("Please Enter Your Name.");
                                return false;
                            }
                            if($("#FormSubmissionValues_data_52").val().length < 5) {
                                alert("Please Enter Your First & Last Name.");
                                return false;
                            }
                            if ($("#FormSubmissionValues_data_52").val().indexOf(" ") === -1) {
                                alert("Please Enter Your First & Last Name.");
                                return false;
                            }
                            if($("#FormSubmissionValues_data_4").val() == "") {
                                alert("Please Enter Your Phone Number.");
                                return false;
                            }
                            else {
                                var phone = $("#FormSubmissionValues_data_4").val().replace(/\D/g,"");
                                if(phone.length < 10) {
                                    alert("Please Enter a Complete Phone Number.");
                                    return false;
                                }
                            }

                        }

                        $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                        return true;
                    }',
                    'afterValidate' => 'js:function(form, data, hasErrors) {

                        if (!hasErrors && data.status =="successPart1_details") {
                            window.location = "http://'.$_SERVER["SERVER_NAME"].'/houseValues/'.$houseValuesActionId.'/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/2/fsid/" + data.form_submission_id;
                        } else if (!hasErrors && "'.$_GET["status"].'" == "2") {
                            window.location = "http://'.$_SERVER["SERVER_NAME"].'/houseValues/'.$houseValuesActionId.'/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/success/fsid/" + data.form_submission_id;
                        } else if(data != "") {

                            if("'.$SubmissionValues->formPart.'" == "1_details") {
                                alert("Please enter a valid address.");
                            }

                            $("div.loading-container.loading").remove();
                        }
                        return false;
                    }',
                ),
            ));

        ?>
        <select id="source-id" class="stmcms" title="Source" data-id="<?=$fields['source-id']?>" style="display: none;"></select>
        <h5 id="source-description" class="stmcms" title="Source Description" style="display: none;"></h5>
        <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_DESCRIPTION_ID.']', $htmlOptions=array('value' => $fields['source-description'])); ?>
        <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_ID.']', $htmlOptions=array('value' => $fields['source-id'])); ?>

        <?php if($SubmissionValues->formPart == '1_details'): ?>
        <table>
            <tr>
                <td>
                    <div id="ValueReportFormGeoLocation_container">
                        <input class="full-address" placeholder="Enter your address, city, state and zip code" name="ValueReportFormGeoLocation" id="ValueReportFormGeoLocation" type="text" autocomplete="off">
                        <div class="errorMessage" id="ValueReportFormGeoLocation_error" style="display:none"></div>
                        <div class="results"></div>
                    </div>
                </td>
                <td>
                    <div>
                        <input class="submit" value="Find Out Now" type="submit">
                        <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
                    </div>
                </td>
            </tr>
        </table>
        <? endif; ?>
        <div class="hidden">
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']', $htmlOptions = array('class' => 'address', 'placeholder' => 'Address'));?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']', $htmlOptions = array('class' => 'city', 'placeholder' => 'City', 'value' => $opt['city']));?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array('empty' => 'State')); ?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']', $htmlOptions = array());?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
        </div>
        <?php if($SubmissionValues->formPart == 2): ?>
            <div class="container">
                <div id="map-container" class="half map pull-left">
                </div>
                <div class="half fields pull-right">
                    <h3>
                        Where should we send your evaluation?
                    </h3>
                    <div>
                        <div>
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'col-lg-12', 'placeholder' => 'Enter Your Email Address'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
                        </div>
                        <div>
                            <?php //echo CHtml::textField('fullName', null, $htmlOptions = array('class' => 'col-lg-12', 'placeholder' => 'Enter Your Name'));?>
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('full_name')->id . ']', $htmlOptions = array('class' => 'col-lg-12', 'placeholder' => 'Enter Your Name'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('full_name')->id . ']'); ?>
                        </div>
                        <div>
                            <?php $this->widget('StmMaskedTextField', array(
                                    'stmMaskedTextFieldClass'=>'houseValuesPhone',
                                    'model' => $SubmissionValues,
                                    'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                                    'mask' => '(999) 999-9999',
                                    'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                                    'htmlOptions' => array('class' => 'col-lg-12', 'placeholder' => 'Enter Your Phone Number'),
                                )); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
                            <?php echo $form->hiddenField($SubmissionValues, 'data[' . $FormFields->getField('form_submission_id')->id . ']'); ?>
                        </div>
                        <div class="text-center">
                            <input class="submit" value="Send Me My Value" type="submit">
                            <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php elseif($SubmissionValues->formPart == 'success' && !empty($fields['forward-url'])): ?>
            <a id="forward-url" href="" class="stmcms hidden" title="Forward Url After Submit"></a>
            <?php Yii::app()->clientScript->registerScript('completeForwarder', 'setTimeout(function(){ window.location = "' . $fields['forward-url'] . '";}, 10000)'); ?>
        <? endif; ?>

        <?php $this->endWidget(); ?>
    </div>
<?endif;?>
</div>

<div class="footer <?=($SubmissionValues->formPart == 2) ? "footer-2 container": ""?>">
    <div id="footer-credits" class="stmcms credits pull-left" title="Footer Credits">© Copyright <?php echo date('Y');?>.  <?php echo Yii::app()->user->settings->office_name; ?>.</div>
    <div class="policy-terms pull-right">
        <span style="float:right;">
            <a href="javascript:void(0)" onclick="return false;">Privacy Policy</a> |
            <a href="javascript:void(0)" onclick="return false;">Terms of Use</a>
        </span>
    </div>
</div>

<?php if($SubmissionValues->formPart == "1_details"):
    Yii::app()->clientScript->registerScript('houseValuesTemplateModern1','

    if (document.getElementById("ValueReportFormGeoLocation_container")) {

        var ValueReportFormGeoLocation_results = [];
        var ValueReportFormGeoLocation_selected = false;
        function stmSetLocation(id){
            var selected = ValueReportFormGeoLocation_results[ id ];
            var street_number;
            var route;
            var city;
            var state;
            var zip;

			ValueReportFormGeoLocation_selected = id;
            $("#ValueReportFormGeoLocation").val(selected.formatted_address);
            $("#ValueReportFormGeoLocation_container .results").hide();
            $.each(selected.address_components,function( key, section){
                if(section.types[0] == "street_number"){
					street_number = section.long_name;
				} else if (section.types[0] == "route"){
					route = section.short_name;
				} else if (section.types[0] == "locality"){
					city = section.long_name;
				} else if (section.types[0] == "administrative_area_level_1"){
					state = section.short_name;
				} else if (section.types[0] == "postal_code"){
					zip = section.long_name;
				}
            });
            $("#FormSubmissionValues_data_14").val(street_number+" "+route);
            $("#FormSubmissionValues_data_16").val(city);

            $.when( $("#FormSubmissionValues_data_17 option").removeAttr("selected") ).done(function() {
                $("#FormSubmissionValues_data_17 option:contains(" + state + ")").attr("selected", "selected");
            });

            $("#FormSubmissionValues_data_18").val(zip);
        }


        $("#ValueReportFormGeoLocation_container .results").mouseleave(function(){
            $(this).hide();
        });

        $("#ValueReportFormGeoLocation").on("input",function (e) {
            if (e.which == 40){ return; }
            $("#ValueReportFormGeoLocation_container .results").show();
            ValueReportFormGeoLocation_results = [];
            ValueReportFormGeoLocation_selected = false;
            $("#ValueReportFormGeoLocation_container .results").html("<div class=\"no-results\">No Results</div>");
            var string = $.trim($(this).val());
            if(string && (string.length > 6) ){
                $("#ValueReportFormGeoLocation_container .results").html("Searching....");
                $.get("http://maps.googleapis.com/maps/api/geocode/json?components=country:US",
                    {
                        "format": "json",
                        "address": string,
                        "sensor" : true,
                    },
                    function(result){
                        if(result.status && (result.status == "OK") ){
                            $("#ValueReportFormGeoLocation_container .results").html(" ");
                            var country;
                            var result_count = 0;
                            $.each( result.results, function( index, element ){
                                if(result_count == 0){
                                    $("#ValueReportFormGeoLocation_container .results").html("No Results");
                                }
                                if(element.formatted_address){
                                    country = "";
                                    $.each(element.address_components,function( key, section){
                                        if(section.types[0] == "country"){
                                            country = section.short_name;
                                        }
                                    });

                                    if(result_count == 0){ $("#ValueReportFormGeoLocation_container .results").html("<ul></ul>"); }
                                    result_count++;
                                    $("#ValueReportFormGeoLocation_container .results ul").append("<li onclick=\'stmSetLocation("+index+")\'>"+element.formatted_address+"</li>");
                                    ValueReportFormGeoLocation_results[index] = element;
                                }
                            });
                        }

                    }
                );
            }
        });
        $( "#ValueReportFormGeoLocation" ).on( "keydown", function( event ) {
            if(event.which == 40 || event.which == 38 || event.which == 13){
                event.preventDefault();
                if(event.which == 13 && !ValueReportFormGeoLocation_selected ){
					return $( "#ValueReportFormGeoLocation_container .results ul li.selected" ).click();
				} else if (ValueReportFormGeoLocation_selected && event.which == 13 ) {
					$( this ).closest("form").submit();
				}
                if($( "#ValueReportFormGeoLocation_container .results ul" ).html()){
                    if(! $( "#ValueReportFormGeoLocation_container .results .selected" ).html() ){
                        $( "#ValueReportFormGeoLocation_container .results ul li" ).first().addClass( "selected" );
                    } else {
                        var found = false;
                        var used = false;
                        var elements;
                        if(event.which == 38){
                            elements = $("#ValueReportFormGeoLocation_container .results ul li").get().reverse();
                        } else {
                            elements = $( "#ValueReportFormGeoLocation_container .results ul li" );
                        }
                        $.each( elements , function( index ) {
                            if(found && !used){
                                $( this ).addClass("selected");
                                used = true;
                                return;
                            }
                            if (!found && !used && $( this ).hasClass("selected")){
                                $( this ).removeClass("selected");
                                found = true;
                            }
                        });
                    }
                }

            }
        });
    }
' , CClientScript::POS_END);
elseif($SubmissionValues->formPart == 2): ?>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

<?    Yii::app()->clientScript->registerScript('houseValuesTemplateModern2','

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        "address": "'.$_GET["address"] . ", " . $_GET["city"] . ", " . AddressStates::getShortNameById($_GET["state"]) . " " . $_GET["zip"].'"
    }, function (results) {
        var mapSubject = new google.maps.Map(document.getElementById("map-container"), {
            zoom: 30,
            disableDefaultUI: true,
            scrollwheel: false,
            zoomControl: true,
            center: results[0].geometry.location,
            mapTypeId: google.maps.MapTypeId.SATELLITE
        });
        var markerSubject = new google.maps.Marker({
            position: results[0].geometry.location,
            icon: "http://cdn.seizethemarket.com/assets/images/map-house-marker.png",
            map: mapSubject,
            title: ""
        });
    });
    if(typeof fbq != "undefined") {
        fbq("track", "Lead");
    }
' , CClientScript::POS_END);

elseif($SubmissionValues->formPart == 'success'):

    Yii::app()->clientScript->registerScript('houseValuesTemplateModernSuccessStep','

    if(typeof fbq != "undefined") {

        fbq("track", "CompleteRegistration");
    }

' , CClientScript::POS_END);
endif; ?>