<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
		'id' => 'saved-home-search-list-search',
	)
); ?>
<div class="g2"></div>
<div class="g4">
	<label class="g4">Email:</label>
	<span class="g8">
		<?php echo $form->textField($model->toEmail, 'email', $htmlOptions = array('placeholder' => 'Email')); ?>
	</span>
</div>
<div class="g2">
	<label class="g6">Clicked:</label>
	<span class="g6">
		<?php echo $form->dropDownList($model, 'isClicked', StmFormHelper::getYesNoList(), $htmlOptions = array('empty'=> ' '));
		?>
	</span>
</div>
<div class="g3">
    <label class="g4">From:</label>
	<span class="g8">
		<?php echo $form->dropDownList($model, 'contactId',
            CMap::mergeArray(array('' => ''), CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName')),
            $htmlOptions = array('data-placeholder' => ' ')
        );
//        $this->widget('admin_module.extensions.EChosen.EChosen', array(
//                                                                 'target' => '#EmailMessage_isClicked',
//                                                                 'options' => array(
//                                                                     'width' => '175px',
//                                                                     'allow_single_deselect' => true
//                                                                 )
//                                                                 )
//        );
        ?>
	</span>
</div>
<!--<div class="g5">-->
<!--	<label class="g3">Last Viewed:</label>-->
<!--	<span class="g7">-->
<!--		--><?php //$this->widget('zii.widgets.jui.CJuiDatePicker', array(
//				'model' => $model,
//				'attribute' => 'processed_datetime',
//				// name of post parameter
//				'options' => array(
//					'showAnim' => 'fold',
//					'dateFormat' => 'mm/dd/yy',
//					'numberOfMonths' => 2,
//				),
//				'htmlOptions' => array(
//					'style' => 'width:90px;font-size: 12px;',
//					'class' => 'g5',
//				),
//			)
//		);
//		?>
<!--		<label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>-->
<!--		--><?php //$this->widget('zii.widgets.jui.CJuiDatePicker', array(
//				'model' => $model,
//				'attribute' => 'processed_datetime',
//				// name of post parameter
//				'options' => array(
//					'showAnim' => 'fold',
//					'dateFormat' => 'mm/dd/yy',
//					'numberOfMonths' => 2,
//				),
//				'htmlOptions' => array(
//					'style' => 'width:90px;font-size: 12px;',
//					'class' => 'g5',
//				),
//			)
//		);
//		?>
<!--</div>-->
<div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>

<?php $this->endWidget(); ?>
