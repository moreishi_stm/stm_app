<?php
	$this->breadcrumbs = array(
		'List' => '',
	);

	Yii::app()->clientScript->registerScript('savedHomeSearchList', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('saved-home-search-grid', {
		data: $(this).serialize()
	});
	return false;
});
"
	);
?>
	<h1>Saved Home Search List</h1>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox', array('model' => $model)); ?>
	</div>
<?php
	$this->widget('admin_module.components.StmGridView', array(
			'id' => 'saved-home-search-grid',
			'template' => '{pager}{summary}{items}{summary}{pager}',
			'dataProvider' => $model->search(),
			'itemsCssClass' => 'datatables',
			'columns' => array(
				array(
					'name' => 'Name / Frequency',
					'value' => '"<a href=\"/admin/contacts/".$data->contact->id."\" target=\"_blank\">".$data->contact->fullName."</a><br />Search: <strong>".$data->name."</strong><br />".$data->getModelAttribute("getFrequencyTypes", $data->frequency)',
					//$data->priceRange.$data->printBedsBathSqFeetRow().$data->printBooleanFieldsRow()
					'type' => 'raw',
				),
				array(
					'name' => 'Agent / Updated by / Added by',
					'type' => 'raw',
					'value' => '"Sending From: ".$data->agent->fullName."<br />Updated: ".$data->updatedBy->fullName." - ".Yii::app()->format->formatDate($data->updated, "n/j/y")." (".Yii::app()->format->formatTime($data->updated).")<br />
			Added: ".
			$data->addedBy->fullName." - ".Yii::app()->format->formatDate($data->added, "n/j/y")." (".Yii::app()->format->formatTime($data->added).")"',
				),
				array(
					'name' => 'Updated / Added',
					'value' => '"<strong><u>Search Criteria:</u></strong>".$data->printSearchCriteria()',
					'type' => 'raw',
				),
                array(
					'name' => '',
					'type' => 'raw',
					'value' => '"<strong>Result Count: ".$data->getCountPropertyResults()."</strong><br /><br />".
                        "<a href=\"/admin/savedHomeSearch/sentTo/".$data->contact->id."\" class=\"button gray icon i_stm_mail grey-button\" target=\"_blank\" data=\"".$data->id."\" style=\"width: 80px;\">Emails Sent</a>".
                        "<a class=\"edit-saved-home-search-button button gray icon i_stm_edit grey-button\" data=\"".$data->id."\" style=\"width: 80px;\">Edit</a>".
                        "<a href=\"/admin/savedHomeSearch/preview/".$data->id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\" data=\"".$data->id."\" style=\"width: 80px;\">Preview</a>"',
					'htmlOptions' => array(
						'style' => 'width:120px'
					),
				),
			),
		)
	);

	$this->widget('admin_widgets.DialogWidget.HomeSearchDialogWidget.HomeSearchDialogWidget',
        array(
			'id' => 'home-search-dialog',
			'contactId' => $model->contact->id,
			'title' => 'Add New Saved Search',
			'triggerElement' => '.edit-saved-home-search-button',
		)
	);
?>