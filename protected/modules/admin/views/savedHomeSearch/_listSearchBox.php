<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
		'id' => 'saved-home-search-list-search',
	)
); ?>
<div class="g5">
	<label class="g3">Contact:</label>
	<span class="g4">
		<?php echo $form->textField($model, 'contactFirstName', $htmlOptions = array('placeholder' => 'First Name')); ?>
	</span>
	<span class="g4 p-ml10">
		<?php echo $form->textField($model, 'contactLastName', $htmlOptions = array('placeholder' => 'Last Name')); ?>
	</span>
</div>
<div class="g3">
	<label class="g4">Added by:</label>
	<span class="g8">
		<?php echo $form->dropDownList($model, 'added_by', CMap::mergeArray(array('' => ''), CHtml::listData(SavedHomeSearches::model()->byDistinct('added_by')->findAll(), 'addedBy.id', 'addedBy.fullName')),
			$htmlOptions = array('data-placeholder' => ' ',)
		);
		?>
	</span>
</div>
<div class="g3">
	<label class="g4">Updated by:</label>
	<span class="g8">
		<?php echo $form->dropDownList($model, 'updated_by',
			CMap::mergeArray(array('' => ''), CHtml::listData(SavedHomeSearches::model()->byDistinct('updated_by')->findAll(), 'updatedBy.id', 'updatedBy.fullName')),
			$htmlOptions = array('data-placeholder' => ' ')
		);
			$this->widget('admin_module.extensions.EChosen.EChosen', array(
					'target' => '#SavedHomeSearches_added_by,#SavedHomeSearches_updated_by',
					'options' => array(
						'width' => '175px',
						'allow_single_deselect' => true
					)
				)
			);
		?>
	</span>
</div>
<div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>
<div class="g4">
	<label class="g4">Added Date:</label>
	<span class="g8">
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'attribute' => 'addedFromDate',
				// name of post parameter
				'options' => array(
					'showAnim' => 'fold',
					'dateFormat' => 'mm/dd/yy',
					'numberOfMonths' => 2,
				),
				'htmlOptions' => array(
					'style' => 'width:90px;font-size: 12px;',
					'class' => 'g5',
				),
			)
		);
		?>
		<label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'attribute' => 'addedToDate',
				// name of post parameter
				'options' => array(
					'showAnim' => 'fold',
					'dateFormat' => 'mm/dd/yy',
					'numberOfMonths' => 2,
				),
				'htmlOptions' => array(
					'style' => 'width:90px;font-size: 12px;',
					'class' => 'g5',
				),
			)
		);
		?>
</div>
<div class="g4">
	<label class="g6">Update Date:</label>
	<span class="g6">
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'attribute' => 'updatedFromDate',
				// name of post parameter
				'options' => array(
					'showAnim' => 'fold',
					'dateFormat' => 'mm/dd/yy',
					'numberOfMonths' => 2,
				),
				'htmlOptions' => array(
					'style' => 'width:90px;font-size: 12px;',
					'class' => 'g5',
				),
			)
		);
		?>
		<label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'attribute' => 'updatedToDate',
				// name of post parameter
				'options' => array(
					'showAnim' => 'fold',
					'dateFormat' => 'mm/dd/yy',
					'numberOfMonths' => 2,
				),
				'htmlOptions' => array(
					'style' => 'width:90px;font-size: 12px;',
					'class' => 'g5',
				),
			)
		);
		?>
</div>
<div class="g3">
    <label class="g4">Assigned to:</label>
	<span class="g8">
		<?php echo $form->dropDownList($model, 'agent_id',
            CMap::mergeArray(array('' => ''), CHtml::listData(SavedHomeSearches::model()->byDistinct('agent_id')->findAll(), 'agent.id', 'agent.fullName')),
            $htmlOptions = array('data-placeholder' => ' ')
        );
        $this->widget('admin_module.extensions.EChosen.EChosen', array(
                'target' => '#SavedHomeSearches_agent_id',
                'options' => array(
                    'width' => '175px',
                    'allow_single_deselect' => true
                )
            )
        );
        ?>
	</span>
</div>

<!--<div class="g3">-->
<!--    <label class="g4">Result Count Max:</label>-->
<!--    <span class="g2">-->
<!--        --><?php //echo $form->textField($model, 'resultCountMax', $htmlOptions = array('placeholder' => '#')); ?>
<!--    </span>-->
<!--</div>-->
<?php $this->endWidget(); ?>
