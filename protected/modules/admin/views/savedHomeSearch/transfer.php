<?php
$this->breadcrumbs = array(
    'Transfer' => '',
);
$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerScript('search', <<<JS
	$('.transfer-search-button').live('click',function(){
        if(confirm('Confirm transfer of Saved Home Searches from '+ $(this).attr('data-from-name') +' to '+ $(this).attr('data-to-name') +'.')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var fromId = $(this).attr('data-from-id');
			var toId = $(this).attr('data-to-id');
			$.post('/$module/savedHomeSearch/transfer/',{'fromId':fromId,'toId':toId}, function(data) {
				if(data==null) {
					Message.create("success","Tasks transferred successfully.");
                    $.fn.yiiGridView.update('transfer-saved-home-search-grid', {
                        data: $(this).serialize(),
                        complete: function(jqXHR, status) {
                            if (status=='success'){
                                $('select.transferTo').chosen({allow_single_deselect: true, width: '90%'});
                            }
                        }
                    });
				} else {
    				$("div.loading-container.loading").remove();
					Message.create("error","Error: Saved Home Searches did not transfer.");
				}
			},"json");
		}
    });

    var transferDropdown = $('select.transferTo');
    transferDropdown.live('change', function(){
        if($(this).val()!='') {
            $('.transfer-button-container').hide('normal');
            var fromId = $(this).attr('data-id');
            var toId = $(this).val();
            var toName = $(this).find(':selected').text();
            var transferButton = $('#transfer-button-container-'+fromId + ' .transfer-search-button');

            $('#transfer-button-container-'+fromId).show('normal');
            transferButton.attr('data-to-id', toId);
            transferButton.attr('data-to-name', toName);
        }
    });
JS
);
?>
    <style>
        .chzn-results {
            text-shadow: none;
        }
    </style>
	<h1>Transfer Saved Home Searches</h1>

<?php
$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select.transferTo','options'=>array('allow_single_deselect'=>true,'width'=>'90%')));

$this->widget('admin_module.components.StmGridView', array(
        'id'            =>'transfer-saved-home-search-grid',
        'template'	   =>'{items}',
        'dataProvider'  =>$dataProvider,
        'extraParams'   =>array('dateRange'=>$dateRange,'contactsList'=>$contactsList),
        'itemsCssClass' =>'datatables',
        'columns' => array(
            array(
                'type'  => 'raw',
                'name'  => 'Name',
                'value' => '"<a href=\"/admin/savedHomeSearch/list/agent_id/".$data->id."\" target=\"_blank\">".$data->fullName."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# Saved Home Searches',
                'value' => 'SavedHomeSearches::model()->countByAttributes(array("agent_id"=>$data->id))',
            ),
            array(
                'type' => 'raw',
                'name' => 'Transfer To',
                'value' => 'CHtml::dropDownList("contactId",null, $this->grid->extraParams["contactsList"],$htmlOptions=array("empty"=>"","data-placeholder"=>"Select Transfer To","id"=>"transfer-to-".$data->id, "data-id"=>$data->id, "class"=>"transferTo"))',
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div id=\"transfer-button-container-".$data->id."\" class=\"transfer-button-container\"  style=\"display:none;\">".CHtml::link("Transfer Searches","javascript:void(0)", $htmlOptions=array("class"=>"button gray icon i_stm_edit grey-button transfer-search-button","data-from-id"=>$data->id,"data-from-name"=>$data->fullName,"data-to-id"=>$data->id,"data-to-name"=>$data->fullName))."</div>"',
                'htmlOptions' => array('style' => 'width:180px'),
            ),
        ),
    ));