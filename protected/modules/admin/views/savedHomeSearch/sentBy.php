<?php
	$this->breadcrumbs = array(
		'Sent Emails' => '',
	);
?>

<h1>Sent Market Updates List</h1>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $this->renderPartial('_listSearchBoxSent', array('model' => $model)); ?>
</div>

<?php $this->renderPartial('_listSent', array('dataProvider' => $dataProvider)); ?>