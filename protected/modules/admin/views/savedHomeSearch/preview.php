<?php
$this->breadcrumbs=array(
	$model->contact->fullName =>array('/admin/contacts/'.$model->contact->id),
	$model->name =>'',
);
$searchId = $model->id;
Yii::app()->clientScript->registerScript('closeHomeSearchPreview', <<<JS
	$(".close-home-search-preview-button").click(function() {
		window.close();
	});

	$("#send-now-button").click(function() {
	    $(this).hide();
		$('#email-homes-container').show('normal');
	});

	$('#send-email-button').click(function(){
	    // validation
	    if($('#subject').val() == '') {
	        alert("Subject cannot be blank.");
	        return false;
	    }

	    if($('#message').val() == '') {
	        alert("Message cannot be blank.");
	        return false;
	    }

	    if($('#emailId').val() == '') {
	        alert("Please select an email address.");
	        return false;
	    }

        $("body").prepend("<div class='loading-container loading'><em></em></div>");

        $.post('/admin/savedHomeSearch/send/' + $searchId, {subject: $('#subject').val(), emailId: $('#emailId').val(), message: $('#message').val() }, function(data) {
            $("div.loading-container.loading").remove();
            if(data.status == 'success') {
                Message.create("success","Homes sent successfully.");
                $('#email-homes-container').hide();

            } else {
                Message.create("error","Error: Homes did not get sent. " + data.message);
            }
        },"json");

	    return false;
	});
JS
); ?>

<div style="width:700px; margin-left: auto;margin-right: auto;">
<h1 style="margin:20px;">Home Search Preview</h1>
    <h2><?=($count) ? $count." Homes Found" : ""?></h2>
    <div style="position: absolute; top:10px; right: 10px;">
        <a class="btn btn-primary close-home-search-preview-button pull-right">Close</a>
    </div>
<hr />
<a id="send-now-button" class="btn btn-primary btn-lg g12" style="font-size: 18px; line-height: 1.3333333; padding: 10px 0; margin: 20px 0 40px 0;">Send Homes to Client</a>
<div id="email-homes-container" class="g12 p-clr" style="display: none; margin-bottom: 40px;">
    <label class="message" style="font-weight: bold;">Subject:</label>
    <div class="g12 p-mb10">
        <?=CHtml::textField('subject', 'Homes for Sale (Custom Search) - '.date('m/d/Y'), $htmlOptions=array('class'=>'g12'))?>
    </div>
    <label class="message" style="font-weight: bold;">Email:</label>
    <div class="g12 p-mb10">
        <?
            $emails = array();
            if($r = $model->contact->emails) {

                foreach($r as $emailModel) {
                    $emails[$emailModel->id] = $emailModel->email;
                }
            }
        ?>
        <?=CHtml::dropDownList('emailId', null, $emails, $htmlOptions=array('class'=>'g12'))?>
    </div>
    <label class="message g12" style="font-weight: bold;">Message:</label>
    <?
    $scope = (YII_DEBUG) ? "local" : "com";
    Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);
    Yii::app()->clientScript->registerScript('tinyMceNewEmailDialogWidget', <<<JS
	tinymce.init({
		selector: "#message",
		convert_urls: false,
		relative_urls: false,
		height : 200,
		plugins: [
			"advlist autolink lists link charmap print preview anchor textcolor paste",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu, spellchecker"
		],
        cleanup_on_startup: false,
        trim_span_elements: false,
        verify_html : false,
		extended_valid_elements : "span[*|top|position],a[*],a*,div[*],span*,div[*]",
		cleanup: false,
		toolbar: "fontselect fontsizeselect spellchecker | forecolor forecolorpicker backcolor bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link undo redo | preview | code",
		autosave_ask_before_unload: false,
		setup : function(ed) {
		    ed.on('init', function() { this.getDoc().body.style.fontSize = '13px'; });
		    ed.on('keyUp', function() { tinyMCE.triggerSave(); });
		},
		spellchecker_rpc_url: "http://www.seizethemarket.{$scope}/js/tinymce/plugins/spellchecker/spellchecker.php"
	});
JS
        ,CClientScript::POS_END);

        $message =   "Hi ".$model->contact->first_name.",<br><br>"
                    ."Here are some homes that you may be interested in. Let me know which ones you like and would like to see.<br><br>"
                    ."Thanks!";
    ?>
    <div class="g12">
        <?=CHtml::textArea('message', $message, $htmlOptions=array('rows'=>8,'class'=>'g12'))?>
    </div>
    <label class="g12">NOTE: Your signature will automatically be applied.</label>
    <a id="send-email-button" class="btn btn-primary btn-lg g12" style="font-size: 18px; line-height: 1.3333333; padding: 10px 0; margin-top: 20px;">Send Now</a>
</div>
<div class="g12">
<?php
	echo $htmlString;
//	$this->widget('front_module.components.StmListView', array(
//														 'dataProvider' => $DataProvider,
//														 'template' => '{pager}{summary}{items}{pager}{summary}',
//														 'id' => 'home-list',
//														 'itemView' => '../../../front/views/site/_homeListRow',
//														 'itemsCssClass' => 'datatables',
//														 )
//	);
?>
</div>
</div>