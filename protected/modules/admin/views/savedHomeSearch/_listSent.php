<?php
$domainSuffix = (YII_DEBUG) ? 'local': 'com';
$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";
Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);

Yii::app()->clientScript->registerScript('searchSavedHomeSearchEmailListSent',<<<JS
	$("#listview-search form").submit(function() {
		$.fn.yiiGridView.update("email-messages-grid", {
			data: $(this).serialize()
		});
        //$(".view-email").fancybox();
		return false;
	});

    $(".view-email").on("click",function() {
//        if (!$(this).data("fancybox")) {
//            $(".view-email").fancybox();
//            $(this).click();
//            return false;
//        }

        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.getJSON("/admin/savedHomeSearch/email/" + $(this).attr("data-mid"), function(data) {
            $("#email-message").html(data.body);
            $("#email-message").prepend("<h4 class='p-tc'>Subject: "+data.subject+"</h4><br /><br />");
            if(data.bcc) {
                $("#email-message").prepend("<h4 class='p-tc'>BCC: "+data.bcc+"</h4>");
            }
            if(data.cc) {
                $("#email-message").prepend("<h4 class='p-tc'>CC: "+data.cc+"</h4>");
            }
            $("#email-message").prepend("<h4 class='p-tc'>To: "+data.to+"</h4>");
            $("#email-message").prepend("<h5 class='p-tc'>"+data.sent+"</h5>");
            $("div.loading-container.loading").remove();

            $.fancybox({
                'type': 'inline',
                'content' : $('#email-message-container').html(),
                'width': 800,
                'height': '100%',
                'autoSize':true,
                'scrolling':'auto',
                'autoDimensions':false,
                'padding': 40
            });
        });
        return false;
    });
JS
, CClientScript::POS_END);

/*$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
        'target' => '.view-email', 'config' => array(
            'width' => '800', 'height' => '600', 'scrolling' => 'auto', 'autoDimensions' => false, 'padding' => '40',
        ),
    )
);*/
?>
<div style="display: none">
    <div id="email-message-container">
        <div id="email-message">
        </div>
    </div>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
            'id' => 'email-messages-grid',
            'dataProvider' => $dataProvider,
            'pager' => array('maxButtonCount'=>10),
            'rowHtmlOptionsExpression' => 'array("data-mid" => $data->id)',
            'itemsCssClass' => 'datatables',
            'columns' => array(
                array(
                    'type' => 'raw',
                    'name' => 'Sent',
                    'value' => 'Yii::app()->format->formatDate($data->processed_datetime,"n/j/y")',
                    'htmlOptions' => array('style'=>'width:90px;'),
                ),
                array(
                    'type' => 'raw',
                    'name' => 'name',
                    'value' => '"<a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->toEmail->contact->id."\" target=\"_blank\">".$data->toEmail->contact->fullName."</a>"',
                ),
                'toEmail.email',
                array(
                    'type' => 'raw',
                    'name' => 'from',
                    'value' => '$data->fromEmail->contact->fullName',
                ),
//                array(
//                    'type' => 'raw',
//                    'name' => '# Homes Sent',
//                    'value' => '(($count = $data->getCountHomesSent())?$count:"")',
//                    'htmlOptions' => array('style'=>'width:60px;'),
//                ),
                array(
                    'type' => 'raw',
                    'name' => '#_clicks',
                    'value' => '(($count = $data->getClickCount())?$count:"")',
                    'htmlOptions' => array('style'=>'width:60px;'),
                ),
                array(
                    'type' => 'raw',
                    'name' => 'last_viewed',
                    'value' => '(($lastClicked = $data->getLastClicked())?Yii::app()->format->formatDateTime($lastClicked):"")',
                    'htmlOptions' => array('style'=>'width:150px;'),
                ),
                array(
                    'type' => 'raw',
                    'name' => '',
                    'value' => '"<a href=\"#email-message-container\" class=\"view-email\" data-mid=\"".$data->id."\">View Email</a>"',
                    'htmlOptions' => array('style' => 'width:80px'),
                ),
            ),
            )
    );
?>