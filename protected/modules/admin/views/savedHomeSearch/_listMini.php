<style type="text/css">
    #saved-home-search-grid a.btn.widget {
        margin: 5px !important;
    }
</style>
<?php Yii::app()->clientScript->registerScript('deleteSavedSearch', "
    $(document).on('click', 'a.delete-savedsearch', function() {
        $.getJSON($(this).attr('href'), function(data) {
            if (data.success == true) {
                Message.create('success', 'Successfully removed saved search!');
            } else {
                Message.create('error', 'Could not remove saved search.');
            }

            $.fn.yiiGridView.update('saved-home-search-grid');
        });

        return false;
    });
  "
); ?>
<div class="button-container">
	<button id="add-home-search-button" class="text" type="button" data-id="<?php echo $model->contact_id ?>" data-ctid="<?php echo $componentTypeId; ?>" data-cid="<?php echo $componentId; ?>"><em class="icon i_stm_add"></em>Add Home Search</button>
</div>
<div class="ui-button-panel">
	<?
		$this->widget('admin_module.components.StmGridView', array(
				'id' => 'saved-home-search-grid',
				'htmlOptions' => array(
					'class' => 'saved-home-search-grid'
				),
				'template' => '{items}',
				'dataProvider' => $model->search(),
				'itemsCssClass' => 'datatables',
				'columns' => array(
					array(
						'name' => 'Name',
						'value' => '"<strong><u>".$data->name."</u></strong><br />".$data->getModelAttribute("getFrequencyTypes", $data->frequency)',
						'type' => 'raw',
                        'htmlOptions' => array(
                            'style' => 'width:100px'
                        ),
					),
					array(
						'name' => 'Updated / Added',
						'value' => '"<strong>Agent:</strong> ".$data->agent->fullName."<br /><strong><u>Search Criteria:</u></strong>".$data->printSearchCriteria()."<br />Updated: ".$data->updatedBy->fullName." ".Yii::app()->format->formatDate($data->updated, "n/j/y")." ".Yii::app()->format->formatTime($data->updated)."<br />
			Added: ".
			$data->addedBy->fullName." ".Yii::app()->format->formatDate($data->added, "n/j/y")." ".Yii::app()->format->formatTime($data->added)',
						'type' => 'raw',
					),
//					array(
//						'name' => '',
//						'type' => 'raw',
//						'value' => '',
//						'htmlOptions' => array(
//							'style' => 'width:100px'
//						),
//					),
					array(
						'name' => '',
						'type' => 'raw',
						'value' => '"<strong>Result Count: ".$data->getCountPropertyResults()."</strong><br /><br />".
                                    "<a class=\"btn btn-primary default button edit-saved-home-search-button g9\" data=\"".$data->id."\">Edit</a>".
                                    "<a href=\"/admin/savedHomeSearch/preview/".$data->id."\" class=\"btn btn-primary button default view-saved-home-search-button g9\" target=\"_blank\">Preview</a>".
						            "<a href=\"/admin/savedHomeSearch/sentTo/".$data->contact->id."\" class=\"btn btn-primary button default g9\" target=\"_blank\">Sent Emails</a>".
                                    "<a href=\"/admin/savedHomeSearch/delete/".$data->id."\" class=\"btn btn-primary button default delete-savedsearch g9\">Delete</a>"',
						'htmlOptions' => array(
							'style' => 'width:100px'
						),
					),
//					array(
//						'name' => '',
//						'type' => 'raw',
//						'value' => '"<a href=\"/admin/savedHomeSearch/preview/".$data->id."\" class=\"btn widget view-saved-home-search-button\" target=\"_blank\">Preview</a>".
//						            "<a href=\"/admin/savedHomeSearch/sentTo/".$data->contact->id."\" class=\"btn widget\" target=\"_blank\">Sent Emails</a>"',
//						'htmlOptions' => array(
//							'style' => 'width:100px'
//						),
//					),
				),
			)
		);
	?>
</div>