<div class="p-clr p-p10" style="margin-bottom: -17px; position: relative; padding-top:30px;">
	<h2>Project Items</h2>
</div>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/projects/addItem/<?php echo $id; ?>" class="button gray icon i_stm_add">Add Project Item</a>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
	'id' => 'action-plan-grid',
	'dataProvider' => $model->search(),
	'itemsCssClass' => 'datatables',
	'columns' => array(
		array(
			'type' => 'raw',
			'name' => 'Priority',
			'value' => '$data->printPriorityFlag()',
			'htmlOptions'=>array('style'=>'width:60px'),
		),
		array(
			'type' => 'raw',
			'name' => 'Due Date',
			'value' => 'Yii::app()->format->formatDate($data->due_date)',
			'htmlOptions'=>array('style'=>'width:150px'),
		),
		'description',
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/projects/items/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:90px'),
		),
//		array(
//			'type'=>'raw',
//			'name'=>'',
//			'value'=>'
//				"<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button delete-action-plan-item-button \" data-id=\"".$data->id."\">Delete</a></div>"
//			',
//			'htmlOptions'=>array('style'=>'width:90px'),
//		),
	),
)); ?>
