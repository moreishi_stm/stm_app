<?php
	$this->breadcrumbs = array(
		'List' => '',
	);

	Yii::app()->clientScript->registerScript('search', '
$("#listview-search form").submit(function() {
	$.fn.yiiGridView.update("project-grid", {
		data: $(this).serialize()
	});
	return false;
});
'
	);
?>
<div id="listview-actions">
	<a href="/<?php echo $this->module->name;?>/projects/add" class="button gray icon i_stm_add">Add New Project</a>
</div>
<div id="content-header">
	<h1>Projects List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBox', array('model' => $model)); ?>
</div><!-- search-form -->
<?php echo $this->renderPartial('_list',array('model'=>$model));

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
        'id' => TaskDialogWidget::TASK_DIALOG_ID,
        'title' => 'Create New Task',
        'triggerElement' => '.add-task-button, .edit-task-button, .delete-task-button',
        'parentModel' => $model,
    )
);

Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
            'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
            'title' => 'Add New Activity Log',
            'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
            'parentModel' => $model,
        )
    );
}
