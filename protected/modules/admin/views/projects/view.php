<style type="text/css">
    .datatables button {
        font-size: 9px;
    }
</style>
<?php
$this->breadcrumbs = array(
    $model->name => '',
);
?>
<div class="p-tc p-f0 g100">
    <?php
    $this->beginStmPortletContent(array(
            'handleTitle' => 'Project Details',
            'handleIconCss' => 'i_wizard',
            'handleButtons' => array(
                array(
                    'label' => 'Edit',
                    'iconCssClass' => 'i_stm_edit',
                    'htmlOptions' => array('id' => 'edit-project-button'),
                    'name' => 'edit-project',
                    'type' => 'link',
                    'link' => '/'.$this->module->name.'/projects/edit/' . $model->id,
                )
            ),
        )
    );
    ?>
    <div>
        <div class="rounded-text-box odd-static">
            <h2 class="p-pv10"><?php echo $model->name ?></h2>
            <table class="container p-mt10">
                <tr>
                    <th class="narrow">Status:</th>
                    <td><?php echo $model->statusName; ?></td>
                </tr>
                <tr>
                    <th class="narrow">Due Date:</th>
                    <td style="width: 25%;"><?php echo Yii::app()->format->formatDate($model->due_date) ?></td>
                    <th class="narrow">Priority:</th>
                    <td><?php echo StmFormHelper::getYesNoName($model->is_priority); ?></td>
                </tr>
                <tr>
                    <th class="narrow">Subcategory of:</th>
                    <td><?php echo $model->parent->name; ?></td>
                </tr>
                <tr>
                    <th class="narrow">Notes:</th>
                    <td><?php echo nl2br($model->notes); ?></td>
                </tr>
            </table>
        </div>
    </div>
    <h2>Sub-Projects</h2>
    <?php echo ($model->hasChild())? $this->renderPartial('_list',array('model'=>$parentModel)): '';?>

    <?php $this->endStmPortletContent(); ?>
</div>

<!--<div class="p-tc p-f0 g12">-->
<!--	--><?php
//	$this->beginStmPortletContent(array(
//								  'handleTitle' => 'Sub-Project List',
//								  'handleIconCss' => 'i_wizard',
//								  'handleButtons' => array(
//									  array(
//										  'label' => 'Edit',
//										  'iconCssClass' => 'i_stm_edit',
//										  'htmlOptions' => array('id' => 'edit-project-button'),
//										  'name' => 'edit-project',
//										  'type' => 'link',
//										  'link' => '/admin/projects/edit/' . $model->id,
//									  )
//								  ),
//								  )
//	);
//	?>
<!--	<div>-->
<!--		<div class="g12 p-mb5 rounded-text-box odd-static">-->
<!--			<h2 class="p-pv10">--><?php //echo $model->name ?><!--</h2>-->
<!--			<table class="container p-mt10">-->
<!--				<tr>-->
<!--					<th class="narrow">Due Date:</th>-->
<!--					<td>--><?php //echo Yii::app()->format->formatDate($model->due_date) ?><!--</td>-->
<!--					<th class="narrow">Priority:</th>-->
<!--					<td>--><?php //echo StmFormHelper::getYesNoName($model->is_priority); ?><!--</td>-->
<!--				</tr>-->
<!--				<tr>-->
<!--					<th class="narrow">Subcategory of:</th>-->
<!--					<td>--><?php //echo $model->parent->name; ?><!--</td>-->
<!--				</tr>-->
<!--			</table>-->
<!--		</div>-->
<!---->
<!--	</div>-->
<!--	--><?php //$this->endStmPortletContent(); ?>
<!--</div>-->

<?php $this->beginStmPortletContent(array(
        'handleTitle' => 'Project Tasks',
        'handleIconCss' => 'i_mouse',
    ));


$tasksContent = '<a id="add-task-button" class="text-button p-fr" data="'.$model->id.'" ctid="'.ComponentTypes::PROJECTS.'"><em class="icon i_stm_add"></em>Add Task</a>';
$tasksContent .= '<div class="p-clr"></div>';
$tasksContent .= $this->renderPartial('../tasks/_listGrid', array(
        'dataProvider' => $dataProvider,
        'overdueCount' => $overdueCount
    ),true
);

$actionPlanProvider = ActionPlans::getAppliedPlans($model->componentType->id, $model->id);
$actionPlansContent = $this->renderPartial('../actionPlans/_listMiniAppliedPlans', array(
        'dataProvider' => $actionPlanProvider,
        'parentComponentId' => $model->id,
        'parentComponentTypeId' => ComponentTypes::PROJECTS,
    ), true
);

$this->widget('zii.widgets.jui.CJuiTabs', array(
        'tabs' => array(
            'Tasks' => array(
                'content' => $tasksContent,
                'id' => 'tasks-tab',
            ),
            'Action Plans' => array(
                'content' => $actionPlansContent,//$actionPlansContent
                'id' => 'action-plans-tab',
            ),
        ),
        'options'=>array('active'=>0),
        'id' => 'projects-tab',
    )
);

$this->endStmPortletContent();
?>
<div id="" class="g12 p-mh0 p-mv5">
    <?php
    $this->widget('admin_widgets.ActivityLogPortlet.ActivityLogPortlet', array(
            'parentModel' => $model,
        ));
    ?>
</div>
<?php
Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
        'id' => TaskDialogWidget::TASK_DIALOG_ID,
        'title' => 'Create New Task',
        'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button, .delete-task-button',
        'parentModel' => $model,
    )
);

$this->widget('admin_widgets.DialogWidget.ActionPlanDialogWidget.ActionPlanDialogWidget', array(
        'id' => 'action-plan-dialog',
        'parentModel' => $model,
        'title' => 'Add Action Plan',
        'triggerElement' => '#add-action-plan-button',
    )
);