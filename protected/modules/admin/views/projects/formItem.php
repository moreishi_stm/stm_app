<?php
$this->breadcrumbs = array(
	$model->project->name => '/'.Yii::app()->controller->module->id.'/projects/'.$model->project_id,
	ucfirst($this->action->id) => '',
);

if ($this->action->id == 'edit') {
	CMap::mergeArray($this->breadcrumbs, array($model->name => array('/'.Yii::app()->controller->module->id.'/projects/' . $model->id),));
}

?>
<h1><?php echo $model->project->name; ?></h1>

<?php echo $this->renderPartial('_formItem', array('model' => $model)); ?>
