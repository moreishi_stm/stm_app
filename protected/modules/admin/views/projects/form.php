<?php
$this->breadcrumbs = array(
	(($this->action->id == 'edit')?$model->name:'Add New Project') => (($this->action->id == 'edit')?'/'.Yii::app()->controller->module->id.'/projects/'.$model->id:''),
);

if ($this->action->id == 'edit')
	$this->breadcrumbs = CMap::mergeArray($this->breadcrumbs, array(ucfirst($this->action->id) => ''));
?>

<div id="action-plans-header">
	<h1 class="name"><?php echo ($model->isNewRecord) ? 'Add New Project' : $model->name;?></h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
