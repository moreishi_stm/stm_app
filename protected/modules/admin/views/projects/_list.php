<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'project-grid',
        'dataProvider' => $model->search(),
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Priority',
                'value' => '$data->printPriorityFlag()',
                'htmlOptions' => array('style' => 'width:50px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Status / Owner',
                'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)."<br />(".$data->contact->fullName.")"',
                'htmlOptions' => array('style' => 'width:100px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Due Date',
                'value' => 'Yii::app()->format->formatDate($data->due_date)',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
            'name',
            array(
                'type' => 'raw',
                'name' => 'Subcategory of',
                'value' => '$data->parent->name',
            ),
            array(
                'type' => 'raw',
                'name' => 'Notes',
                'value' => 'nl2br($data->notes)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Next Task',
                'value' => 'Tasks::formatNextTaskDate($data->nextTaskDate)',
                'htmlOptions' => array('style' => 'width:120px'),
            ),
            array(
                'name' => '',
                'type' => 'raw',
                'value' => '"<button class=\"add-task-button\" data=\"".$data->id."\" ctid=\"".ComponentTypes::PROJECTS."\">Add Task</button>"',
                'htmlOptions' => array(
                    'style' => 'width:90px'
                ),
            ),
            array(
                'name' => '',
                'type' => 'raw',
                'value' => '"<button class=\"add-activity-log-button\" data=\"".$data->id."\" ctid=\"".ComponentTypes::PROJECTS."\">Add Activity Log</button>"',
                'htmlOptions' => array(
                    'style' => 'width:135px'
                ),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/projects/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/projects/".$data->id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
        ),
    )
);