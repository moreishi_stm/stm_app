<?php
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'project-item-form',
			'enableAjaxValidation' => false,
		)
	);

	$projectItemScript = <<<JS
$('#ProjectItems_is_priority').click(function(){
	if($(this).checked) {
		$(this).val(0);
	} else {
		$(this).val(1);
	}
});
JS;

	Yii::app()->clientScript->registerScript('projectItemScript', $projectItemScript);


	$this->beginStmPortletContent(array(
			'handleTitle' => 'Project Item Details',
			'handleIconCss' => 'i_wizard'
		)
	);

?>
<div id="project-item-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container">
			<tr>
				<th><?php echo $form->labelEx($model, 'due_date'); ?></th>
				<td>
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
																		  'model'=>$model,
																		  'attribute'=>'due_date',
																		  'options'=>array(
																			  'showAnim'=>'fold',
																			  'dateFormat'=>'mm/dd/yy',
																		  ),
																		  'htmlOptions'=>array(
																			  'style'=>'width:120px;',
																		  ),
																		  ));
					?>
					<?php echo $form->error($model,'due_date'); ?>
				</td>
				<th class="narrow"><?php echo $form->labelEx($model, 'is_priority'); ?>:</th>
				<td style="width:45%;">
					<?php echo $form->checkbox($model, 'is_priority', $htmlOptions=array('style'=>'font-size:16px;','value'=>1)) ;?>
					<?php echo $form->error($model, 'is_priority'); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow"><?php echo $form->labelEx($model, 'description'); ?></th>
				<td colspan="3">
					<?php echo $form->textArea($model, 'description', $htmlOptions = array(
							'rows' => '4',
							'placeholder' => 'Description',
							'class' => 'g9',
						)
					); ?>
					<?php echo $form->error($model, 'description'); ?>
				</td>
			</tr>

		</table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
	<?php echo CHtml::htmlButton((($model->id) ? 'Update' : 'Add') . ' Project Item', $htmlOptions = array(
			'class' => 'submit wide',
			'type' => 'submit',
		)
	); ?>
</div>
<?php $this->endWidget(); ?>
