<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'project-list-search',
)); ?>
    <div class="g2">
        <label class="g6">Status:</label>
        <span class="g6">
    		<?php echo $form->dropDownList($model, 'status_ma', StmFormHelper::getStatusBooleanList(true), $htmlOptions=array('empty'=>'Select One')); ?>
        </span>
    </div>
	<div class="g3">
		<label class="g2">Name:</label>
		<span class="g10"><?php echo $form->textField($model,'name');?></span>
	</div>
    <div class="g3">
        <label class="g4">Assigned to:</label>
            <span class="g8">
                <?php echo $form->dropDownList($model, 'assignedTo', $model->getAssignedToDropDownValues(), array(
                        'empty' => '',
                        'class' => 'chzn-select  g12',
                        'multiple' => 'multiple',
                        'data-placeholder' => 'Assigned to'
                    )); ?>
                <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Projects_assignedTo')); ?>
            </span>
    </div>
    <div class="g3">
        <label class="g7">My Projects Only:</label>
        <span class="g5">
            <?php echo $form->dropDownList($model, 'showOthersProjects', StmFormHelper::getYesNoList()); ?>
        </span>
    </div>
	<div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
