<?php

$form = $this->beginWidget('CActiveForm', array(
        'id' =>' add-new-project-form',
        'enableAjaxValidation' => false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'   => 'Project Details',
        'handleIconCss' => 'i_wizard'
    ));
?>
<div id="action-plans-container" >
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <table class="container">
            <tr>
                <th>Status:</th>
                <td colspan="3">
                    <?php echo $form->dropDownList($model, 'status_ma', CMap::mergeArray(StmFormHelper::getStatusBooleanList(true), array(2=>'Complete'))); ?>
                    <?php if($model->contact_id == Yii::app()->user->id || Yii::app()->user->checkAccess('owner')): ?>
                        <span class="label" style="margin-left: 150px;">Owner:</span>
                        <?php echo $form->dropDownList($model, 'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(),'id','fullName')); ?>
                    <?php endif; ?>
                    <?php echo $form->error($model,'status_ma'); ?>
                    <?php if($model->contact_id == Yii::app()->user->id || Yii::app()->user->checkAccess('owner')): ?>
                        <?php echo $form->error($model,'contact_id'); ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <th>Name:</th>
                <td colspan="3">
                    <?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Project Name', 'class'=>'g6', ));?>
                    <?php echo $form->error($model,'name'); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form->labelEx($model, 'due_date'); ?></th>
                <td>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,
                            'attribute'=>'due_date',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=>'mm/dd/yy',
                            ),
                            'htmlOptions'=>array(
                                'style'=>'width:120px;',
                            ),
                        ));
                    ?>
                    <?php echo $form->error($model,'due_date'); ?>
                </td>
                <th class="narrow"><?php echo $form->labelEx($model, 'is_priority'); ?>:</th>
                <td style="width:45%;">
                    <?php echo $form->checkbox($model, 'is_priority', $htmlOptions=array('style'=>'font-size:16px;','value'=>1)) ;?>
                    <?php echo $form->error($model, 'is_priority'); ?>
                </td>
            </tr>
            <tr>
                <th>Subcategory of:</th>
                <td colspan="3">
                    <?php echo $form->dropDownList($model,'parent_id', CMap::mergeArray(array(''=>''), $model->parentDropDownList()), $htmlOptions=array('placeholder'=>'Project Name', 'class'=>'g6', 'data-placeholder' => 'Select Subcategory',
                        ));

                    $this->widget('admin_module.extensions.EChosen.EChosen', array(
                            'target' => 'select#Projects_parent_id',
                            'options'=>array(
                                'allow_single_deselect'=>true,
                            ),
                        ));
                    ?>
                    <?php echo $form->error($model,'parent_id'); ?>
                </td>
            </tr>
            <tr>
                <th>Notes:</th>
                <td colspan="3">
                    <?php echo $form->textArea($model,'notes', $htmlOptions=array('placeholder'=>'Notes', 'class'=>'g6', 'rows'=>10));?>
                    <?php echo $form->error($model,'notes'); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Submit' : 'Save') . ' Project', $htmlOptions = array(
            'class' => 'submit wide',
            'type' => 'submit',
        )); ?>
</div>
<?php $this->endWidget(); ?>
