<table>
    <thead>
        <tr>
            <th>Close Date</th><th>Name<br>Address</th><th>Price</th><th>Lender</th><th>Source</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>7/1/2012</td><td><a href="#"><strong><u>Larry Duke</u></strong></a><br>1742 Clearwater St</td><td>$313,000</td><td>First Option</td><td>Referral</td>
        </tr>
        <tr>
            <td>7/1/2012</td><td><a href="#"><strong><u>Jane Smith</u></strong></a><br>2974 Hartley Rd</td><td>$485,000</td><td>Cash</td><td>Web</td>
        </tr>
        <tr>
            <td>7/4/2012</td><td><a href="#"><strong><u>Jack Lane</u></strong></a><br>123 Main Street</td><td>$635,000</td><td>Cash</td><td>Web</td>
        </tr>
        <tr>
            <td>7/15/2012</td><td><a href="#"><strong><u>John Smith</u></strong></a><br>11472 Pilgrim Way</td><td>$497,000</td><td>First Option</td><td>Web</td>
        </tr>
    </tbody>
</table>
