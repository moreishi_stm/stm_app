<?php
$css
    = <<<CSS
    div div.chzn-container {
        margin-top: 4px;
    }

    select.contacts + div.chzn-container {
        /** display: none; **/
    }
CSS;

Yii::app()->clientScript->registerCss('closingStyle', $css);
$closingStatusFallout = Closings::STATUS_TYPE_FALLOUT_ID;
$closingStatusClosed = Closings::STATUS_TYPE_CLOSED_ID;
$js
    = <<<JS
    $('#closing-form').submit(function(){
        $("body").prepend('<div class="loading-container loading"><em></em></div>');
    });
	$('#Closings_closing_status_ma').change(function(){
		if($('#Closings_closing_status_ma').val() == $closingStatusFallout) {
			$('.fallout-reason-container').show('normal');
			$('.post-closing-container').hide('normal');
			$('#Closings_post_close_status_ma').val('');
		} else if($('#Closings_closing_status_ma').val() == $closingStatusClosed) {
			$('.post-closing-container').show('normal');
			$('.fallout-reason-container').hide('normal');
			$('#Closings_fallout_date').val('');
			$('#Closings_fallout_reason_other').val('');
			$('#Closings_fallout_reason_ma').val('');
		} else {
			$('.post-closing-container').hide('normal');
			$('.fallout-reason-container').hide('normal');
			$('#fallout-reason-other-container').hide('normal');
			$('#Closings_fallout_reason_other').val('');
			$('#Closings_fallout_reason_ma').val('');
		}
	});

	$('#Closings_fallout_reason_ma').change(function(){
		if($('#Closings_fallout_reason_ma').val() ==3) {
			$('#fallout-reason-other-container').show('normal');
		} else {
			$('#fallout-reason-other-container').hide('normal');
			$('#Closings_fallout_reason_other').val('');
		}
	});
JS;
Yii::app()->clientScript->registerScript('closingForm', $js);

$this->widget(
    'admin_module.extensions.EChosen.EChosen', array(
        'target' => 'select.inspectorComboBox, select.contacts',
    )
);

$this->widget(
    'admin_module.extensions.EChosen.EChosen',
    array('target' => '#Closings_other_agent_id, #Closings_title_company_id, #Closings_lender_id')
);

$this->widget(
    'admin_module.extensions.moneymask.MMask', array(
        'element' => '#Closings_price, #Closings_loan_amount, #Closings_ernest_money_deposit', 'currency' => 'PHP', 'config' => array(
            'precision' => 0, 'symbol' => '$', 'showSymbol' => true, 'symbolStay' => true,
        )
    )
);

$this->widget(
    'admin_module.extensions.moneymask.MMask', array(
        'element' => '#Closings_commission_amount, #Closings_referral_commission', 'currency' => 'PHP',
        'config'  => array(
            'precision' => 2, 'symbol' => '$', 'showSymbol' => true, 'symbolStay' => false,
        )
    )
);

$this->widget(
    'admin_module.extensions.moneymask.MMask', array(
        'element' => '#Closings_commission_percent', 'currency' => 'PHP', 'config' => array(
            'precision' => 2, 'showSymbol' => false,
        )
    )
);
?>

<?php $form = $this->beginWidget(
    'CActiveForm', array(
        'id' => 'closing-form', 'enableAjaxValidation' => false,
    )
); ?>
<?php $this->beginStmPortletContent(
    array(
        'handleTitle' => $this->title, 'handleIconCss' => 'i_price_tag'
    )
); ?>
<div id="closing-container">
<div class="g12 p-mb5 rounded-text-box notes odd-static">
<div id="closing-left" class="g6">
    <table class="">
        <tr>
            <th><?php echo $form->labelEx($model, 'closing_status_ma'); ?>:</th>
            <td>
                <?php echo $form->dropDownList(
                    $model, 'closing_status_ma', $model->statusTypes, $htmlOptions = array(
                        'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g5',
                    )
                );?>
                <?php echo $form->error($model, 'closing_status_ma'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'is_forever_client'); ?>:</th>
            <td>
                <?php echo $form->dropDownList($model, 'is_forever_client', StmFormHelper::getYesNoList(), $htmlOptions = array('class' => 'g5'));?><span class="label"> <a href="/admin/foreverClients">See Forever Client Module</a></span>
                <?php echo $form->error($model, 'is_forever_client'); ?>
            </td>
        </tr>
        <tr class="post-closing-container" <?php echo ($model->closing_status_ma == Closings::STATUS_TYPE_CLOSED_ID)
            ? '' : 'style="display:none;"'; ?>>
            <th><?php echo $form->labelEx($model, 'post_close_status_ma'); ?>:</th>
            <td>
                <?php echo $form->dropDownList(
                    $model, 'post_close_status_ma', $model->postCloseStatusTypes, $htmlOptions = array(
                        'class' => 'g5',
                    )
                );?>
            </td>
        </tr>
        <tr class="fallout-reason-container" <?php echo ($model->closing_status_ma == Closings::STATUS_TYPE_FALLOUT_ID)
            ? '' : 'style="display:none;"'; ?>>
            <th><?php echo $form->labelEx($model, 'fallout_reason_ma'); ?>:</th>
            <td>
                <?php echo $form->dropDownList(
                    $model, 'fallout_reason_ma', $model->falloutReasons, $htmlOptions = array(
                        'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g5',
                    )
                );?>
                <div id="fallout-reason-other-container" <?php echo ($model->fallout_reason_other) ? ''
                    : 'style="display:none;"'; ?> class="g7 p-pt0">
                    <?php echo $form->textField(
                        $model, 'fallout_reason_other', $htmlOptions = array('class' => 'g12')
                    ); ?>
                </div>
                <?php echo $form->error($model, 'fallout_reason_ma'); ?>
            </td>
        </tr>
        <tr class="fallout-reason-container" <?php echo ($model->closing_status_ma == Closings::STATUS_TYPE_FALLOUT_ID)
            ? '' : 'style="display:none;"'; ?>>
            <th><?php echo $form->labelEx($model, 'fallout_date'); ?>:</th>
            <td>
                <?php
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'model'          => $model, 'attribute' => 'fallout_date', 'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'fallout_date'); ?>
            </td>
        </tr>
        <tr>
            <th>Auto Email Updates:</th>
            <td>
                <?php echo $form->dropDownList($model, 'email_frequency_ma', Transactions::getEmailFrequencyTypes(), $htmlOptions = array('class' => 'g5'));?>

                <a href="#email-client-update-container" id="preview-email-client-update-button" class="text-button" style="margin-left:0;"><em class="icon i_stm_search"></em><span>Preview Email</span></a>

            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'mls_id'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'mls_id', $htmlOptions = array(
                        'class' => 'g5',
                    )
                ); ?>
                <?php echo $form->error($model, 'mls_id'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'price'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'price', $htmlOptions = array(
                        'class' => 'g5',
                    )
                ); ?>
                <?php echo $form->error($model, 'price'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'ernest_money_deposit'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'ernest_money_deposit', $htmlOptions = array(
                        'class' => 'g5',
                    )
                ); ?>
                <?php echo $form->error($model, 'ernest_money_deposit'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'financing_type_ma'); ?>:</th>
            <td>
                <?php echo $form->dropDownList(
                    $model, 'financing_type_ma', $model->financingTypes, $htmlOptions = array(
                        'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g5',
                    )
                );?>
                <?php echo $form->error($model, 'financing_type_ma'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'loan_amount'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'loan_amount', $htmlOptions = array(
                        'class' => 'g5',
                    )
                ); ?>
                <?php echo $form->error($model, 'loan_amount'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'sale_type_ma'); ?>:</th>
            <td>
                <?php echo $form->dropDownList(
                    $model, 'sale_type_ma', $model->saleTypes, $htmlOptions = array(
                        'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g5',
                    )
                ); ?>
                <?php echo $form->error($model, 'sale_type_ma'); ?>
            </td>
        </tr>
        <tr>
            <th>Address:</th>
            <td>
                <div class="g12">
                    <?php
                    $contactAddresses = $model->transaction->contact->addresses;
                    $contactAddresses = (count($contactAddresses) > 0) ? CHtml::listData(
                        $contactAddresses, 'id', 'fullAddress'
                    ) : array();

                    echo $form->dropDownList(
                        $model->transaction, 'address_id', $contactAddresses, $htmlOptions = array(
                            'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g8 address-dropdown'
                        )
                    );
                    ?><a href="javascript:void(0);" id="add-address-button" class="text-button p-fl"><em
                            class="icon i_stm_add"></em>Add New</a>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'closing_location'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'closing_location', $htmlOptions = array(
                        'class' => 'g12'
                    )
                ); ?>
                <?php echo $form->error($model, 'closing_location'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'closing_gift'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'closing_gift', $htmlOptions = array(
                        'class' => 'g12'
                    )
                ); ?>
                <?php echo $form->error($model, 'closing_gift'); ?>
            </td>
        </tr>    </table>
</div>
<div id="closing-right" class="g6">
    <table class="">
        <tr>
            <th class="narrow top-align">Assignments:</th>
            <td id="assignment-table-cell">
                <?php echo $this->renderPartial(
                    '../transactions/_addlAssignmentTemplate', array(
                        'form'        => $form, 'model' => $Assignment, 'componentTypeId' => ComponentTypes::CLOSINGS,
                        'componentId' => $model->id, 'containerHtmlOptions' => array(
                            'style' => 'display: none', 'id' => 'addlAssignmentTemplate'
                        )
                    )
                ); ?>
                <div id="addlAssignmentRows" class="g12 p-p0" style="min-height: 0;">
                    <?php
                    if ($model->assignments) {
                        foreach ($model->assignments as $i => $Assignment) {
                            echo $this->renderPartial(
                                '../transactions/_addlAssignmentTemplate', array(
                                    'form'            => $form, 'model' => $Assignment,
                                    'componentTypeId' => ComponentTypes::CLOSINGS, 'componentId' => $model->id,
                                    'i'               => $i,
                                )
                            );
                        }
                    } else {
                        echo $this->renderPartial(
                            '../transactions/_addlAssignmentTemplate', array(
                                'form'            => $form, 'model' => $Assignment,
                                'componentTypeId' => ComponentTypes::CLOSINGS, 'componentId' => $model->id, 'i' => $i,
                                'enabled'         => true,
                            )
                        );
                    }
                    ?>
                </div>
                <button type="button" id="add-assignment" class="text p-m0"><em class="icon i_stm_add"></em>Add Assignment</button>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'contract_execute_date'); ?>:</th>
            <td>
                <?php
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'model'          => $model, 'attribute' => 'contract_execute_date', 'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'contract_execute_date'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'contract_closing_date'); ?>:</th>
            <td>
                <?php
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'model'          => $model, 'attribute' => 'contract_closing_date', 'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'contract_closing_date'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'inspection_contingency_date'); ?>:</th>
            <td>
                <?php
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'model'          => $model, 'attribute' => 'inspection_contingency_date', 'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'inspection_contingency_date'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'appraisal_contingency_date'); ?>:</th>
            <td>
                <?php
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'model'          => $model, 'attribute' => 'appraisal_contingency_date', 'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'appraisal_contingency_date'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'financing_contingency_date'); ?>:</th>
            <td>
                <?php
                $this->widget(
                    'zii.widgets.jui.CJuiDatePicker', array(
                        'model'          => $model, 'attribute' => 'financing_contingency_date', 'options' => array(
                            'showAnim' => 'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g5',
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'financing_contingency_date'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'actual_closing_datetime'); ?>:</th>
            <td>
                <?php
                $this->widget(
                    'admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                        'model'          => $model, 'attribute' => 'actual_closing_datetime', 'mode' => 'datetime',
                        'options'        => array( // 'showAnim'=>'fold',
                        ), 'htmlOptions' => array(
                            'class' => 'g7',
                            // 'value'=>date('m/d/Y h:i a'),
                            //								'readonly' => true,
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'actual_closing_datetime'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'is_referral'); ?>:</th>
            <td>
                <?php echo $form->dropDownList(
                    $model, 'is_referral', StmFormHelper::getBooleanList(), $htmlOptions = array(
                        'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g5',
                    )
                );?>
                <?php echo $form->error($model, 'is_referral'); ?>
            </td>
        </tr>
        <tr id="referral_commission" style="display: none;">
            <th><?php echo $form->labelEx($model, 'referral_commission'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'referral_commission', $htmlOptions = array(
                        'class' => 'g4', 'disabled' => 'disabled',
                    )
                );?>
                <?php echo $form->error($model, 'referral_commission'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'commission_amount'); ?>:</th>
            <td>
                <?php echo $form->textField(
                    $model, 'commission_amount', $htmlOptions = array(
                        'class' => 'g5'
                    )
                ); ?>
                <?php echo $form->error($model, 'commission_amount'); ?>
            </td>
        </tr>
        <? if(Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('commissionEditAccess')):?>
        <tr>
            <th class="narrow top-align">Commissions:</th>
            <td id="expense-table-cell">
                <?php echo $this->renderPartial('_addlClosingExpenseTemplate', array(
                        'form'        => $form,
                        'model' => $Expense,
                        'componentTypeId' => ComponentTypes::CLOSINGS,
                        'componentId' => $model->id,
                        'transactionId'     => $model->transaction_id,
                        'containerHtmlOptions' => array(
                            'style' => 'display: none', 'id' => 'addlClosingExpenseTemplate'
                        )
                    )
                ); ?>
                <div id="addlExpenseRows" class="g12 p-p0" style="min-height: 0;">
                    <?php
                    if (!empty($model->expenseCollection)) {
                        foreach ($model->expenseCollection as $i => $Expense) {
                            echo $this->renderPartial('_addlClosingExpenseTemplate', array(
                                    'form'            => $form,
                                    'model'           => $Expense,
                                    'componentTypeId' => ComponentTypes::CLOSINGS,
                                    'componentId'     => $model->id,
                                    'transactionId'     => $model->transaction_id,
                                    'i'               => $i,
                                )
                            );
                        }
                    }
                    ?>
                </div>
                <button type="button" id="add-expense" class="text p-m0"><em class="icon i_stm_add"></em>Add Expense</button>
            </td>
        </tr>
        <? endif; ?>
    </table>
</div>
<table class="">
    <tr>
        <th class='narrow'>
            <?php echo $form->labelEx($model, 'notes'); ?>:
        </th>
        <td>
            <?php echo $form->textArea(
                $model, 'notes', $htmlOptions = array(
                    'class' => 'g11', 'rows' => '8'
                )
            ); ?>
            <?php echo $form->error($model, 'notes'); ?>
        </td>
    </tr>
</table>
</div>

<?php //============================================ Closing Vendors/Companies Area =========================================================?>
<div class="g12 p-mb5 rounded-text-box odd-static">
    <div id="closing-details-left" class="g6">
        <table class="">
            <tr>
                <th>
                    <?php echo $form->labelEx($model, 'title_company_id'); ?>:
                </th>
                <td>
                    <div>
                        <div class="g12">
                            <?php echo $form->dropDownList(
                                $model, 'title_company_id', CHtml::listData(
                                    Companies::model()->byType(Companies::TITLE_COMPANIES)->findAll(
                                        array('condition'=>'status_ma=1','order' => 't.name ASC')
                                    ), 'id', 'name'
                                ), $htmlOptions = array(
                                    'empty' => 'Select a Title Company', 'class' => 'g12 company', 'ajax' => array(
                                        'type'      => 'POST', 'url' => CController::createUrl(
                                            '/admin/closings/companyContacts', array(
                                                'type' => 'title',
                                            )
                                        ), 'update' => '#TitleCompanyAssignmentGroup_assigneeGroup',
                                        'complete'  => new CJavaScriptExpression('function() {
                                		var titleCompanyId = $("select#Closings_title_company_id").val();
                                		if(titleCompanyId){
                                            var titleCompanyName = $("select#Closings_title_company_id option[value="+titleCompanyId+"]").html();
                                            var titleAssignmentGroupHtml = $("#TitleCompanyAssignmentGroup_assigneeGroup");

                                            if(titleAssignmentGroupHtml.html()=="")
                                                titleAssignmentGroupHtml.attr("data-placeholder","No Contacts for "+ titleCompanyName);
                                            else
                                                titleAssignmentGroupHtml.attr("data-placeholder","Select "+titleCompanyName+" Contact");

                                            titleAssignmentGroupHtml.trigger("liszt:updated");
                                		}
                                    }'),
                                    ),
                                )
                            ); ?>
                        </div>

                        <?php

                        echo $form->dropDownList(
                            $titleCompanyAssignmentGroup, 'assigneeGroup', array(), $htmlOptions = array(
                                'data-placeholder' => 'Add Contacts', 'multiple' => 'multiple',
                                'class'            => 'contacts g12',
                            )
                        );
                        ?>
                    </div>
                    <?php echo $form->error($model, 'title_company_id'); ?>
                </td>
            </tr>
            <tr>
                <th>
                    <?php echo $form->labelEx($model, 'other_agent_id'); ?>:
                </th>
                <td>
                    <div class="g12">
                        <?php echo $form->dropDownList(
                            $model, 'other_agent_id', CHtml::listData(
                                Companies::model()->byType(Companies::REAL_ESTATE_BROKERAGES)->orderByName()->findAll(),
                                'id', 'name'
                            ), $htmlOptions = array(
                                'empty'            => 'Select a Other Agent', 'class' => 'g12 company',
                                'data-placeholder' => 'Select Cooperating Agent', 'ajax' => array(
                                    'type'      => 'POST', 'url' => CController::createUrl(
                                        '/admin/closings/companyContacts', array(
                                            'type' => 'otheragent',
                                        )
                                    ), 'update' => '#CoBrokerAssignmentGroup_assigneeGroup',
                                    'complete'  => new CJavaScriptExpression('function() {
                                		var brokerageId = $("select#Closings_other_agent_id").val();
                                		if(brokerageId) {
                                            var brokerageName = $("select#Closings_other_agent_id option[value="+brokerageId+"]").html();
                                            var coBrokerAssignmentGroupHtml = $("#CoBrokerAssignmentGroup_assigneeGroup");
                                            if(coBrokerAssignmentGroupHtml.html()=="")
                                                coBrokerAssignmentGroupHtml.attr("data-placeholder","No Contacts for "+ brokerageName);
                                            else
                                                coBrokerAssignmentGroupHtml.attr("data-placeholder","Select "+brokerageName+" Contact");

                                            coBrokerAssignmentGroupHtml.trigger("liszt:updated");
                                		}
                                    }'),
                                ),
                            )
                        );
                        ?>
                    </div>
                    <?php echo $form->dropDownList(
                        $coBrokerAssignmentGroup, 'assigneeGroup', array(), $htmlOptions = array(
                            'data-placeholder' => 'Add Contact', 'multiple' => 'multiple', 'class' => 'contacts g12',
                        )
                    ); ?>
                </td>
            </tr>
        </table>
    </div>
    <div id="closing-details-right" class="g6">
        <table class="">
            <tr>
                <th class="narrow">
                    <?php echo $form->labelex($model, 'lender_id'); ?>:
                </th>
                <td>
                    <div>
                        <div class="g12">
                            <?php echo $form->dropDownList(
                                $model, 'lender_id', CHtml::listData(
                                    Companies::model()->byType(Companies::LENDERS)->findAll(
                                        array('order' => 't.name ASC')
                                    ), 'id', 'name'
                                ), $htmlOptions = array(
                                    'empty' => 'Select a Lender', 'class' => 'g10 company', 'ajax' => array(
                                        'type'      => 'POST', 'url' => CController::createUrl(
                                            '/admin/closings/companyContacts', array(
                                                'type' => 'lender',
                                            )
                                        ), 'update' => '#LenderAssignmentGroup_assigneeGroup',
                                        'complete'  => new CJavaScriptExpression('function() {
											var lenderId = $("select#Closings_lender_id").val();
											if(lenderId) {
                                                var lenderName = $("select#Closings_lender_id option[value="+lenderId+"]").html();
                                                if($("select#LenderAssignmentGroup_assigneeGroup").html()=="")
                                                    $("#LenderAssignmentGroup_assigneeGroup").attr("data-placeholder","No Contacts for "+ lenderName);
                                                else
                                                    $("#LenderAssignmentGroup_assigneeGroup").attr("data-placeholder","Select "+lenderName+" Contact");

                                                $("#LenderAssignmentGroup_assigneeGroup").trigger("liszt:updated");
											}
                                        }'),
                                    ),
                                )
                            ); ?>
                        </div>
                        <?php echo $form->dropDownList(
                            $lenderAssignmentGroup, 'assigneeGroup', array(), $htmlOptions = array(
                                'data-placeholder' => 'Add Contacts', 'multiple' => 'multiple',
                                'class'            => 'contacts g10',
                            )
                        ); ?>
                    </div>
                </td>
            </tr>
            <?php
                $inspectorOptions = Companies::getInspectorOptions();
                if ($inspectorOptions):
            ?>
            <tr>
                <th class="narrow">
                    <label>Inspectors:</label>
                </th>
                <td>
                    <div class="g12">
                        <?php
                            /** @var $form CActiveForm */
                            echo $form->dropDownList(
                                $inspectorAssignmentGroup, 'assigneeGroup',
                                CHtml::listData($inspectorOptions, 'id', 'label', 'group'), $htmlOptions = array(
                                    'data-placeholder' => 'Add Inspectors', 'class' => 'inspectorComboBox g10',
                                    'multiple'         => 'multiple',
                                )
                            );
                        ?>
                    </div>
                </td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
</div>
<?php echo $form->hiddenField($model, 'id'); ?>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <button type="submit" class="submit wide">Submit Closing</button>
</div>
<?php
$this->endWidget();

$this->widget(
    'admin_widgets.DialogWidget.AddressDialogWidget.AddressDialogWidget', array(
        'id'          => 'address-dialog-widget', 'title' => 'Add Address', 'triggerElement' => '#add-address-button',
        'parentModel' => $model, 'transaction' => $model->transaction, 'contactId' => $model->transaction->contact->id,
    )
);

$js
    = <<<JS
    $('#Closings_is_referral').change(processIsReferral).each(processIsReferral);

    function processIsReferral() {
    jReferralCommission = $('#referral_commission');
    if ($(this).val() == 1) {
    jReferralCommission.show();
    jReferralCommission.find('input').attr('disabled', false);

    }
    else {
    jReferralCommission.fadeOut();
    jReferralCommission.find('input').attr('disabled', true).val('');
    }
    }
    $("Closings_title_company_id").on("change", function() {

    });

    $("#Closings_title_company_id").trigger("change");
    $("#Closings_lender_id").trigger("change");
    $("#Closings_other_agent_id").trigger("change");
JS;
Yii::app()->clientScript->registerScript('closingScript', $js);