<div id="contact-header" class="g12">
	<?php
		// if ($TransactionType->id != ComponentTypes::BUYERS)
		// 	echo '<br><span class="address">'.$model->address.'</span>';
	?>
	<div class="submit-login-summary">
		<div class="p-fr p-rel">
			<span class="grey-textbox"><?php echo Yii::app()->format->formatDateDays($model->transaction->contact->last_login, array('break'=>false,'default'=>'n/a')); ?></span><em class="bubble">Last Login</em>
		</div>
		<div class="p-fr p-rel">
			<span class="grey-textbox"><?php echo $model->transaction->contact->addedDateTime; ?></span><em class="bubble">Submit Date</em>
		</div>
	</div>
</div>

<div class="btn-bar">
	<a href="#contacts">
		Info
	</a>
	<a href="#transactions">
		Closing
	</a>
	<a href="#activity-details">
		Activity Details
	</a>
	<a href="#activity-log-portlet">
		Activity Log
	</a>
    <button class="text add-task-button" type="button" data="<?=$model->id?>" ctid="<?=$model->componentType->id?>"><em class="icon i_stm_add"></em>Add Task</button>
    <button class="text add-activity-log-button" type="button" data="<?=$model->id?>" ctid="<?=$model->componentType->id?>"><em class="icon i_stm_add"></em>Add Activity</button>
</div>

<div id="top-data" class="res-section res-group p-0">
	<div class="res-col span_4_of_12 p-0 p-fl">
		<?php
		$this->widget('admin_widgets.ContactPortlet.ContactPortlet', array(
			'model' => $model->transaction->contact,
			'view' => 'mini',
            'originComponentTypeId' => $model->componentType->id,
            'originComponentId' => $model->id,
			'handleTitle' => 'Contact Info',
			'handleButtons' => array(
				array(
					'label' => 'Edit',
					'iconCssClass' => 'i_stm_edit',
					'htmlOptions' => array('id' => 'edit-contact-button'),
					'name' => 'edit-contact',
					'type' => 'link',
					'link' => '/admin/contacts/edit/' . $model->transaction->contact->id,
				)
			),
		));
		?>
	</div>
	<div class="res-col span_8_of_12 p-0 p-fr" id="">
		<?php
		$this->widget('admin_widgets.ClosingPortlet.ClosingPortlet', array(
			'model' => $model,
			'handleTitle' => 'Closing :: '.$model->transaction->contact->fullName,
		));
		?>
	</div>
</div>

<?php
$this->widget('admin_widgets.ActivityDetailsPortlet.ActivityDetailsPortlet', array(
	'model'         => $model,
	'componentType' => $this->componentType,
));

?>
<?php
    $this->widget('admin_widgets.ActivityLogPortlet.ActivityLogPortlet', array(
        'parentModel' => $model
    ));
?>
