<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
		'id' => 'closing-list-search',
	)
); ?>
<div class="g12">
	<div class="g3">
		<label class="g3">Name:</label>
		<span class="g4"><?php echo $form->textField($model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')); ?></span>
		<span class="g4 p-ml10"><?php echo $form->textField($model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')); ?></span>
	</div>
	<div class="g4">
		<label class="g4">Address:</label>
		<span class="g8"><?php echo $form->textField($model->transaction->address, 'address'); ?></span>
	</div>
	<div class="g3">
		<label class="g4">Email:</label>
		<span class="g8"><?php echo $form->textField($model->transaction->contact->emails, 'email'); ?></span>
	</div>
	<div class="g2 submit" style="text-align:center">
		<?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?>
	</div>
	<div class="g3">
		<label class="g3">Status:</label>
		<span class="g8"><?php echo $form->dropDownList($model, 'closing_status_ma', Closings::getStatusTypes(), $htmlOptions = array('empty' => 'All')); ?></span>

	</div>
	<div class="g4">
		<label class="g4">Closing Date:</label>
		<span class="g8">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
																  'model' => $model,
																  'attribute' => 'closingDateFrom',
																  // name of post parameter
																  'options' => array(
																	  'showAnim' => 'fold',
																	  'dateFormat' => 'mm/dd/yy',
																	  'numberOfMonths' => 2,
																  ),
																  'htmlOptions' => array(
																	  'style' => 'width:90px;font-size: 12px;',
																	  'class' => 'g5',
																  ),
																  )
			);
			?>
			<label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
																  'model' => $model,
																  'attribute' => 'closingDateTo',
																  // name of post parameter
																  'options' => array(
																	  'showAnim' => 'fold',
																	  'dateFormat' => 'mm/dd/yy',
																	  'numberOfMonths' => 2,
																  ),
																  'htmlOptions' => array(
																	  'style' => 'width:90px;font-size: 12px;',
																	  'class' => 'g5',
																  ),
																  )
			);
			?>
		</span>
	</div>
    <? if(Yii::app()->user->checkAccess('owner') || (!Yii::app()->user->checkAccess('noBuyerAccess') && !Yii::app()->user->checkAccess('noSellererAccess'))):?>
    <div class="g3">
        <div class="g4 text-right"><label>Assigned to:</label></div>
        <div class="g7">
            <?php echo $form->dropDownList($model, 'assignmentIds',
                CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->transaction->assignmentId)->byAssignmentComponentType(array(ComponentTypes::BUYERS, ComponentTypes::SELLERS, ComponentTypes::CLOSINGS))->orderByName()->findAll(), 'id','fullName'), $htmlOptions = array('empty' => ''));

            $this->widget(
                'admin_module.extensions.EChosen.EChosen', array(
                    'target'  => 'select#Buyers_assignmentId',
                    'options' => array('allow_single_deselect' => true,'width' => '100%'
                    )
                )
            );
            ?>
        </div>
    </div>
    <? endif; ?>
    <? if(Yii::app()->user->checkAccess('owner')):?>
	<div class="g2 submit" style="text-align:center">
        <?    echo CHtml::hiddenField('export','0');?>
		<button type="button" class="export-button button">EXPORT</button>
	</div>
    <? endif; ?>
</div>
<?php $this->endWidget(); ?>
<?php
$css = <<<CSS
#listview-search .button{
	padding: 6px 12px;
	font-variant: small-caps;
	min-height: 30px;
	font-size: 11px;
	margin: 0;
	border-radius: 0;
	font-weight:700;
}
CSS;
Yii::app()->clientScript->registerCss('closingListSearchBoxCss', $css);
?>