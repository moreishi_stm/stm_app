<div class="g6">
    <table class="">
        <tbody>
            <tr>
                <th>Status:</th>
                <td><?php echo StmHtml::value($model, 'statusType');?></td>
            </tr>
            <tr>
                <th>Forever Client:</th>
                <td><?php echo StmFormHelper::getYesNoName($model->is_forever_client, true);?><span class="label"> &nbsp;&nbsp;<a href="/admin/foreverClients">[See Forever Client Module]</a></span></td>
            </tr>
            <tr>
                <th>MLS#:</th>
                <td><?php echo StmHtml::value($model, 'mls_id');?></td>
            </tr>
            <tr>
                <th style="vertical-align: middle;">Auto Email Updates:</th>
                <td>
<!--                    --><?php //echo $model->getModelAttribute("getEmailFrequencyTypes", $model->email_frequency_ma);?>
                    <a href="#email-client-update-container" id="preview-email-client-update-button" class="text-button" style="margin-left:0; display: inline-block;"><em class="icon i_stm_search"></em><span>Preview Email</span></a>

                </td>
            </tr>
			<tr>
				<th>Referral:</th>
				<td><?php echo Yii::app()->format->formatBoolean(StmHtml::value($model, 'is_referral', 0));?></td>
			</tr>
            <tr>
                <th>Price:</th>
                <td><?php echo Yii::app()->format->formatDollars(StmHtml::value($model, 'price', null));?></td>
            </tr>
            <tr>
                <th>Ernest Money Deposit:</th>
                <td><?php echo '$'.StmHtml::value($model, 'ernest_money_deposit', null);?></td>
            </tr>
            <tr>
                <th>Sale Type:</th>
                <td><?php echo StmHtml::value($model, 'saleType');?></td>
            </tr>
            <tr>
                <th>Loan Amount:</th>
                <td><?php echo Yii::app()->format->formatDollars(StmHtml::value($model, 'loan_amount', null));?></td>
            </tr>
            <tr>
                <th>Financing Type:</th>
                <td><?php echo $model->getModelAttribute('getFinancingTypes',$model->financing_type_ma)?></td>
            </tr>
            <tr>
                <th class="wide">Contract Date:</th>
                <td><?php echo StmHtml::value($model, 'contract_execute_date');?></td>
            </tr>
            <tr>
                <th>Inspection Contingency Date:</th>
                <td><?php echo StmHtml::value($model, 'inspection_contingency_date');?></td>
            </tr>
            <tr>
                <th>Appraisal Contingency Date:</th>
                <td><?php echo StmHtml::value($model, 'appraisal_contingency_date');?></td>
            </tr>
            <tr>
                <th>Financing Contingency Date:</th>
                <td><?php echo StmHtml::value($model, 'financing_contingency_date');?></td>
            </tr>
            <tr>
                <th>Closing Date:</th>
                <td><?php echo StmHtml::value($model, 'contract_closing_date');?></td>
            </tr>
            <tr>
                <th>Actual Closing Date:</th>
                <td><?php echo StmHtml::value($model, 'actual_closing_datetime');?></td>
            </tr>
			<tr>
				<th>Closing Location:</th>
				<td><?php echo StmHtml::value($model, 'closing_location');?></td>
			</tr>
            <tr>
                <th>Closing Gift:</th>
                <td><?php echo StmHtml::value($model, 'closing_gift');?></td>
            </tr>
            <tr>
                <th>Commission:</th>
                <td><?php
                        if($model->commission_amount) {
                            echo '$'.StmHtml::value($model, 'commission_amount');
                            if($model->price) {
                                echo ' ('.(round(($model->commission_amount/$model->price) * 100, 2).'%)');
                            }
                            else {
                                echo 'Missing Price';
                            }
                        }
                    ?>
                </td>
            </tr>
            <? if(Yii::app()->user->checkAccess('owner')):?>
            <tr>
                <th>Expenses:</th>
                <td><?php
                    if($model->expenses) {
                        foreach($model->expenses as $expense) {
                            $expenseString .= ($expenseString)? '<br>': '';
                            $expenseString .= '$'.$expense->amount;

                            if($model->commission_amount) {
                                $expenseString .= ' ('.round(($expense->amount/$model->commission_amount) * 100, 2).'%)';
                            }

//                            if($expense->accounting_account_id == AccountingAccounts::REFERRAL_FEE) {
//                                $expenseString .= ' Referral Fee';
//                            }
                        }
                        echo $expenseString;
                    }
                    ?>
                </td>
            </tr>
            <? endif; ?>
        </tbody>
    </table>
</div>

<div class="g6">
    <table class="">
        <tbody>
			<tr>
				<th>Assigned to:</th>
				<td>
					<?php
					if ($Assignments = $model->assignments) {
						foreach ($Assignments as $Assignment) {
							echo '<span style="font-weight:bold; display:inline-block;">' . $Assignment->contact->fullName . ' (' . $Assignment->assignmentType->display_name . ')</span>';
						}
					}
					?>
				</td>
			</tr>
<!--            <tr>-->
<!--                <th class="wide">Commission Amount:</th>-->
<!--                <td>--><?//=StmHtml::value($model, 'commission_amount');?><!--</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th class="wide">Commission Percent:</th>-->
<!--                <td>-->
                    <?
//                        $commissionPercent = StmHtml::value($model, 'commission_percent', null);
//                        if ($commissionPercent)
//                            $commissionPercent = ($commissionPercent * 100).'%';
//                        else
//                            $commissionPercent = StmFormatter::DEFAULT_VALUE;
//
//                        echo $commissionPercent;
                    ?>
<!--                </td>-->
<!--            </tr>-->
			<tr><th class="p-pb0">Title:</th><td class="p-pb0"><?php echo $model->titleCompany->name;?><br /><?php echo Yii::app()->format->formatPhone($model->titleCompany->primaryPhone);?></td></tr>
			<tr><th>Title Agent:</th><td><?php echo $model->titleAgent->fullName;?><br /><?php echo Yii::app()->format->formatPhone($model->titleAgent->primaryPhone);?><br /><?php echo $model->titleAgent->primaryEmail;?></td></tr>
			<?php if($model->titleProcessor): ?>
				<tr><th>Title Processor:</th><td><?php echo $model->titleProcessor->fullName;?><br /><?php echo Yii::app()->format->formatPhone($model->titleProcessor->primaryPhone);?><br /><?php echo $model->titleProcessor->primaryEmail;?></td></tr>
			<?php endif; ?>
            <?php if($model->getCoBrokeringAgent()): ?>
                <tr><th>Co-Broker Agent:</th><td><?php echo $model->getCoBrokeringAgent()->fullName;?><br /><?php echo Yii::app()->format->formatPhone($model->getCoBrokeringAgent()->primaryPhone);?><br /><?php echo $model->getCoBrokeringAgent()->primaryEmail;?></td></tr>
            <?php endif; ?>
			<tr>
				<th>Financing:</th>
				<td><?php echo StmHtml::value($model, 'financingType');?></td>
			</tr>
			<tr>
				<th class="p-pb0">Lender:</th>
				<td class="p-pb0"><?php echo $model->lender->name;?><br />
                    <?php echo Yii::app()->format->formatPhone($model->lender->primaryPhone);?>
                </td>
			</tr>
			<tr>
				<th>Loan Officer:</th>
				<td><?php echo $model->loanOfficer->fullName;?><br />
                    <?php echo ($model->loanOfficer->primaryPhone) ? Yii::app()->format->formatPhone($model->loanOfficer->primaryPhone).'<br />' : '';?>
                    <?php echo $model->loanOfficer->primaryEmail;?>
                </td>
			</tr>
            <?if($model->loanProcessor):?>
            <tr>
                <th>Loan Processor:</th>
                <td><?php echo $model->loanProcessor->fullName;?><br />
                    <?php echo ($model->loanProcessor->primaryPhone) ? Yii::app()->format->formatPhone($model->loanProcessor->primaryPhone).'<br />' : '';?>
                    <?php echo ($model->loanProcessor->primaryEmail) ? $model->loanProcessor->primaryEmail : '';?>
                </td>
            </tr>
            <? endif; ?>
        </tbody>
    </table>
</div>

<div class="g12">
    <table class="">
        <tbody>
            <tr>
                <th class="narrow">Notes:</th>
                <td><?php echo Yii::app()->format->formatTextArea($model->notes);?></td>
            </tr>
        </tbody>
    </table>
</div>
