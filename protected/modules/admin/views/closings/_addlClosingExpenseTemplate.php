<?php
$defaultHtmlOptions = array(
    'class' => 'expenseRow g12 p-p0',
);
if($model->remove) {
    $defaultHtmlOptions['style'] = 'display:none;';
}

if (isset($containerHtmlOptions)) {
    $containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
    $containerHtmlOptions = $defaultHtmlOptions;
}

// Determine which model to use
$inputHtmlOptions = array(
    'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g3',
);

if (isset($model)) {
    // for actual assignments records
    $index           = "[$i]";
    $ExpenseModel = $model;
} else {
    // for the assignments template base
    $ExpenseModel                    = new AccountingTransactions;
    $index                              = '[0]';
    if ($enabled != true) {
        $inputHtmlOptions = CMap::mergeArray(
            $inputHtmlOptions, array(
                'disabled' => 'disabled',
            )
        );
    }
}

echo CHtml::openTag('div', $containerHtmlOptions);

?>
<div class="g12">
    <?php   echo $form->textField($ExpenseModel, $index . 'amount', CMap::mergeArray($inputHtmlOptions, array('class' => 'g3 expenseAmount'))); ?>
    <?php
        echo $form->dropDownList($ExpenseModel, $index . 'accounting_account_id', array(AccountingAccounts::SALES_COMMISSION=>'Sales Commission',AccountingAccounts::CLOSING_BONUS=>'Closing Bonus',AccountingAccounts::REFERRAL_FEE_INTERNAL => 'Internal Referral Fee'), $inputHtmlOptions);
        $contactIds = Yii::app()->db->createCommand("select distinct a.assignee_contact_id from transactions t LEFT JOIN assignments a ON a.component_type_id=t.component_type_id AND a.component_id=t.id AND a.assignment_type_id NOT IN(".AssignmentTypes::LOAN_OFFICER.",".AssignmentTypes::LOAN_PROCESSOR.") AND t.id={$transactionId} AND a.is_deleted=0")->queryColumn();
            $criteria = new CDbCriteria();
            if(empty($contactIds)) {
                $contactIds = array(0);
            }

            $criteria->addInCondition('id', $contactIds);
        echo $form->dropDownList($ExpenseModel, $index . 'agent_id', CHtml::listData(Contacts::model()->orderByName()->findAll($criteria), 'id', 'fullName'), $inputHtmlOptions);
         ?>
    <button type="button" class="text remove-expense p-fl"><em class="icon icon-only i_stm_delete"></em></button>
    <?php echo $form->error($ExpenseModel, $index . 'amount'); ?>
    <?php echo $form->error($ExpenseModel, $index . 'agent_id'); ?>
    <?php echo $form->error($ExpenseModel, $index . 'accounting_account_id'); ?>
    <?php echo $form->hiddenField($ExpenseModel, $index . 'id', $inputHtmlOptions); ?>
    <?php echo $form->hiddenField($ExpenseModel, $index . 'remove', CMap::mergeArray(array('class' => 'remove'), $inputHtmlOptions)); ?>
</div>
<?php echo CHtml::closetag('div'); ?>
