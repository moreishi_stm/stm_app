<?php
	$this->widget('admin_module.components.StmGridView', array(
			'id' => 'closing-grid',
			'dataProvider' => $model->search(),
			'itemsCssClass' => 'datatables',
			'columns' => array(
				'id',
				array(
					'type' => 'raw',
					'name' => 'Client Info',
					'value' => '"<strong>".$data->transaction->contact->fullName."</strong><br />".Yii::app()->format->formatPrintEmails($data->transaction->contact->emails)."<br />".Yii::app()->format->formatPrintPhones($data->transaction->contact->phones)',
				),
				array(
					'type' => 'raw',
					'name' => 'Property Info',
					'value' => '"".Yii::app()->format->formatDollars($data->price)." &nbsp;&nbsp;[".$data->closingSideName."]".
					"<br /><strong>".Yii::app()->format->formatAddress($data->transaction->address)."</strong>"
					',
				),
				array(
					'type' => 'raw',
					'name' => 'Other Contact Info',
					'value' => '"<br />Title Company: ".$data->titleCompany->name
                    ."<br />Lender: ".$data->lender->name
                    ."<br />Agent: <strong>".$data->transaction->printGridviewAssignments()."</strong>"
					."<br />Co-Broker Agent: ".$data->otherAgent[0]->contact->fullName',
				),
				array(
					'type' => 'raw',
					'name' => 'Closing Info',
					'value' => '"<strong>Closing Date: ".Yii::app()->format->formatDate($data->contract_closing_date)."</strong>"
				."<br />Contract Date: ".Yii::app()->format->formatDate($data->contract_execute_date)."<br />Inspection Contingency Date: ".Yii::app()->format->formatDate($data->inspection_contingency_date)',
				),
                array(
                    'type' => 'raw',
                    'name' => '',
                    'value' => '"<div><a href=\"/admin/closings/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
                    'htmlOptions' => array('style' => 'width:80px'),
                ),
				array(
					'type' => 'raw',
					'name' => '',
					'value' => '"<div><a href=\"/admin/closings/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
					'htmlOptions' => array('style' => 'width:80px'),
				),
			),
		)
	);