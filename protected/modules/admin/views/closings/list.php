<?php
$this->breadcrumbs = array(
	'List' => '',
);

Yii::app()->clientScript->registerScript('closingListJs', <<<JS

    $('.export-button').click(function(){
        if(confirm("Are you sure you want to export this list? A new window will open for the export.")) {
            $('body').prepend('<div class="loading-container loading"><em></em></div>');
            $('#export').val(1);

            window.open('/admin/closings?' + $('#listview-search form').serialize());
            setTimeout(function () {
                $('div.loading-container.loading').remove();
                Message.create("notice", "Export is processing in the new window. Please wait...");
            }, 5000);
        }
        $('#export').val(0);
        return false;
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('closing-grid', {
            data: $(this).serialize()
        });
        return false;
    });
JS
);
?>
	<div id="content-header">
		<h1>Closing List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox', array('model' => $model)); ?>
	</div><!-- search-form -->

<?php $this->renderPartial('_list', array('model' => $model)); ?>