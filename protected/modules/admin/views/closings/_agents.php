<div class="g6 p-mt10">
    <table class="container">
        <tbody>
            <tr><th>Agent:</th><td>Nancy New<br />(904) 258-4738<br />(904) 888-1234 Ext. 1256<br />nancy@me.com</td></tr>
        </tbody>
    </table>
</div>

<div class="g6 p-mt10">
    <table class="container">
        <tbody>
            <tr><th class="p-pb0">Co-Broker:</th><td class="p-pb0">ABC Mortgage<br />(904) 555-1212</td></tr>
            <tr><th>Agent:</th><td>John Smith<br />(904) 888-1234 Ext. 1256<br />john@smith.com</td></tr>
            <tr><th>Processor:</th><td>Betty Boop<br />(904) 888-1234 Ext. 1256<br />betty@boop.com</td></tr>
        </tbody>
    </table>
</div>

