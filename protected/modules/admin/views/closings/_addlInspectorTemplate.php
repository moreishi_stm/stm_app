<?php
$defaultHtmlOptions = array(
	'class' => 'inspectorRow g9',
);

if (isset($containerHtmlOptions)) {
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
	$containerHtmlOptions = $defaultHtmlOptions;
}

if (isset($CompanyAssignment)) {
	$Assignment = $CompanyAssignment;
} else {
	$Assignment = new CompanyAssignmentLu();
}

$modelAttributeName = "[inspectors][$index]company_id";

echo CHtml::openTag('div', $containerHtmlOptions);

echo $form->dropDownList($Assignment,  $modelAttributeName, CHtml::listData(Companies::model()->byType(Companies::INSPECTORS)->findAll(), 'id', 'name'), $htmlOptions=array(
	'empty'    => 'Select an Inspector',
	'disabled' => (isset($disabled) && $disabled) ? 'disabled' : null,
));
?>
<button type="button" class="text remove-inspector"><em class="icon icon-only i_stm_delete"></em></button>
<?php echo CHtml::closetag('div'); ?>
