<?php
$this->breadcrumbs=array(
    'Edit'
);

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'pipeline-form',
    'enableAjaxValidation' => false,
));

echo $form->errorSummary($model);

$this->endWidget();