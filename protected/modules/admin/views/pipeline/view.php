<style>
    .stages-container {
        overflow-x: auto;
    }
    .scroll-container {
        overflow-x: auto;
        height: 600px;
        margin-left: 8px;
        position: relative;
    }

    .stage {
        border: 1px solid #dee3e4;
        margin: 0 4px;
        padding: 0;
        height: 100%;
        border-radius: 3px;
        background: #fff;
        min-width: 206px;
    }

    .stage-header {
        background-color: #e9eced;
        border-bottom: 1px solid #dee3e4;
        padding: 4px 4px 0px 4px;
        width: 100%;
        display: block;
        position: relative;
    }

    .stage-header h3 {
        width: 80%;
        text-align: left;
        display: inline-block;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        line-height:19px;
    }

    .stage-header .total-assignments {
        width:20%;
        display: inline-block;
        text-align: right;
    }

    .stage .stage-footer {
        padding-top: 8px;
        padding-bottom: 8px;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
        border-top: 1px solid #dee3e4;
        position: absolute;
        bottom:0;
        width: 100%;
        color: #545c5f;
        font-weight: bold;
        background: #faffd2;
    }

    .stage .stage-items {
        width: 100%;
        min-width: 200px;
        text-align: center;
        overflow-y: auto;
        margin: 0px;
        background: #FFF;
    }

    .stage .stage-items ul {
        position: relative;
        list-style: none;
    }

    .stage .stage-items ul li {
        white-space: normal;
        border: 1px solid #e6e6e6;
        text-align: left;
        margin: 7px auto;
        padding: 5px 8px;
        width: 190px;
        border-radius: 2px;
        box-shadow: 0 0 9px 0 rgba(0,0,0,0.05);
        min-height: 30px;
        background: #d0ffd0;
    }

    .stage .stage-items ul li i {
        display: none;
        position: absolute;
        top: 2px;
        right: 2px;
    }
</style>
<script>
    var stages = [];
</script>

<div class="stages-container">
    <div class="scroll-container">
        <?php foreach($stages as $stage) :?>
        <script>
            stages.push('<?=$stage->id;?>');
        </script>
        <div class="col-xs-11 col-sm-2 col-md-2 col-lg-1 stage" id="stage-<?=$stage->id;?>">
            <div class="stage-header"><h3><?=$stage->name; ?></h3><h3 class="total-assignments">0</h3></div>
            <div class="stage-items"><ul></ul></div>
            <div class="stage-footer">
                <strong>Probability: </strong><?=$stage->probability;?>%
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php

$module = Yii::app()->controller->module->id;
$js = <<<JS

var stage_items = {};

$("body").prepend("<div class='loading-container loading'><em></em></div>");

var makeListItem = function(item) {
    return '<li><div><i class="fa fa-2x fa-reorder"></i><div><a href="/{$module}/contacts/'+item.component_id+'">'+item.first_name+' '+item.last_name+'</a></div></li>';
};

var addAssignments = function(data) {
    var list = $("#stage-"+data.pipeline_stage_id);
    var list_container = $(list).find('.stage-items');

    if(!stage_items[data.pipeline_stage_id]) {
        stage_items[data.pipeline_stage_id] = [];
    }

    $.each(data.items, function(index, item) {
        list_container.find('ul').append(makeListItem(item));
        stage_items[data.pipeline_stage_id].push(item.id);
    });

    //console.log(stage_items);

    var totalAssignmentsLabel = stage_items[data.pipeline_stage_id].length;
    totalAssignmentsLabel = (totalAssignmentsLabel > 0) ? totalAssignmentsLabel : '-';
    $(list).find(".total-assignments").html(totalAssignmentsLabel);
};


$('.scroll-container').css('width', (stages.length * 13) + '%');


var deferreds = [];

$.each(stages, function(index, value) {
    deferreds.push(
        $.ajax({
            type: 'POST',
            url: '/{$module}/pipeline/getAssignments/'+value,
            dataType: "json",
            success:function(data){
                if(typeof data.status == 'undefined' || data.status != 'success') {
                    if(typeof data.messages != 'undefined') {
                        $.each(data.messages, function(index,value) {
                            Message.create("error", data.message);
                        });
                    }  else {
                        Message.create("error", "Error occurred. Please try again.");
                    }
                    return false;
                }

                addAssignments(data);
            },
            error: function(data) { // if error occured
                $("div.loading-container.loading").remove();
                Message.create("error", "Error occurred. Please try again.");
            }
        })
    );
});

$.when.apply($, deferreds).then(function(){
    $("div.loading-container.loading").remove();
}).fail(function(){
    $("div.loading-container.loading").remove();
}).always(function(){
    $("div.loading-container.loading").remove();
});

JS;
Yii::app()->clientScript->registerScript('pipeline-load-stage-assignments', $js);
