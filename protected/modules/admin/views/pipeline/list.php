<?php

$this->widget('admin_module.components.StmGridView', array(
    'id'            =>'pipelines-grid',
    'template'	   =>'{pager}{summary}{items}{summary}{pager}',
    'dataProvider'  => $pipelines->search(),
    'enableSorting'=>true,
    'itemsCssClass' =>'datatables',
    'columns' => array(
        array(
            'type'  => 'raw',
            'name'  => 'ID',
            'value' => '$data->id',
        ),
        array(
            'type'  => 'raw',
            'name'  => ' Name',
            'value' => '$data->name',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'component_type_id',
            'value' => 'ucwords(ComponentTypes::getNameById($data->component_type_id))',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Created',
            'value' => 'Yii::app()->format->formatDate($data->added)',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'Actions',
            'value' => array($this, 'printActions'),
        ),
    ),
));


