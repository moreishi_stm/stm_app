<html>
<head>
    <title>
        New Nurture Opportunity Alert
    </title>
</head>
<body>
    <div style="font-weight:bold;font-size:25px;">
        Congrats! <?=$model->addedBy->fullName?> got a New "<?=$leadStatusName?>" Nurture Opportunity with a Target Date of: <?=date('m/d/Y', strtotime($model->target_date))?>
        <br/>
        <img src="<?=Yii::app()->controller->getModule()->getCdnImageAssetsUrl()?>opportunities/<?=$photo?>" alt="Congrats on Signed Agreement!">
        <br/>
        <br/>
        <span style="color: blue; font-style:italic;"><?=$phrase?></span>
        <br/>
        <br/>
        <a href="http://www.<?=$domainName?>/admin/<?=$model->componentType->name?>/<?=$model->component_id?>">http://www.<?=$domainName?>/admin/<?=$model->componentType->name?>/<?=$model->component_id?></a>
    </div>
    <br/>
    <br/>
    <img src="<?=$domainName; ?>/images/logo_admin.png" border="0" style="width:200px;" alt="Team Logo" />

</body>
</html>