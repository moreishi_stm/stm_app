<html>
    <head>
    </head>
    <body style="font-family: sans-serif;">
        <div style="width: 680px; border: 0px solid #e7e7e7; padding: 6px; background: #FFF;">
            <div style="border-bottom: 1px solid #CCC; padding: 5px; background: #FFF;">
                <div style="float: right; margin-bottom: 30px;">
                    <img src="<?php echo $httpDomain; ?>/images/logo_admin.png" border="0" style="width:175px;"/>
                </div>
                <p style="font-size: 18px; font-weight: bold;  width: 100%; padding-bottom: 12px;">Hello <?php echo Yii::app()->format->formatProperCase($contact->first_name); ?>,</p>

                <p style="font-size: 18px; width: 460px; line-height: 20px;">
                <?if($message):?>
                    <?=$message?>
                <?else:?>
                    Just checking in with your latest real estate market update. If you're thinking about selling, buying or just want to know how the market is, this report will help.
                <?endif;?>
                    <br><br>
                    <span style="font-weight: bold;">Here is your <?=date('F Y')?> housing trends report for</span>
                    <br>
                    <a href="<?=$marketSnahpshotLink?>" style="font-size: 24px; font-weight: bold; color: #2e84be; line-height: 42px;"><?=$address?>, <?=$city?>, <?=$state?> <?=$zip?></a>
                    <br><br>
                </p>
                <div style="clear: both;"></div>
                <table style="margin-bottom: 30px;">
                    <tr>
                        <td style="width: 35%; vertical-align: middle;">
                            <?if($dom):?>
                            <div style="background: #2e84be; padding: 10px; margin-bottom: 10px;">
                                <a href="<?=$marketSnahpshotLink?>" style=" text-decoration: none;  color: white; ">
                                    <div style="text-align:center; font-size: 12px; margin-bottom:10px;">DAYS ON MARKET</div>
                                    <div style="font-weight: bold; font-size: 38px; text-align:center; padding: 8px;"><?=$dom?></div>
                                    <div style="text-align:right; font-size: 12px; ">more trends >></div>
                                </a>
                            </div>
                            <?endif;?>
                            <div style="background: #2e84be; padding: 10px; margin-bottom: 10px;">
                                <a href="<?=$marketSnahpshotLink?>" style=" text-decoration: none;  color: white; ">
                                    <div style="text-align:center; font-size: 12px; margin-bottom:10px;">AVERAGE SOLD PRICE</div>
                                    <div style="font-weight: bold; font-size: 38px; text-align:center; padding: 8px;"><?=$avgSalesPrice?></div>
                                    <div style="text-align:right; font-size: 12px;">more trends >></div>
                                </a>
                            </div>
                        </td>
                        <td style="width: 65%;">
                            <a href="<?=$marketSnahpshotLink?>"><img src="http://cdn.seizethemarket.com/assets/images/market_snapshot_graph_2.jpg" style="width: 100%;" alt=""/></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a href="<?=$marketSnahpshotLink?>" style="text-decoration: none;"><div style="border: 1px solid #2e84be; text-align: center; font-size: 25px; background: #c1e5f0; padding: 20px; clear:both; color: #2e84be; line-height: 30px;">View your <?=date('F Y')?> report for<br><?=$address?>, <?=$city?>, <?=$state?> <?=$zip?></div></a>
                        </td>
                    </tr>
                </table>
                <p style="font-size: 18px; line-height: 20px;">
                    <br><br>
                    If you have any questions, reply to this email<?php echo ($phoneNumberDisplay) ? ' or call me at '.$phoneNumberDisplay : ""; ?>.

                    <br/><br/>Thanks!<br/><br/>
                    <span style="font-size: 20px; font-weight: bold; line-height: 25px;">
                        <?php echo $agent->getFullName(); ?>
                    </span>
                    <br/>
                    Real Estate Expert
                    <?php echo ($phoneNumberDisplay) ? '<br/>Call'.$phoneNumberDisplay : ""; ?>
                    <br/><br/>
                </p>
            </div>

            <div style="text-align:center; font-size: 10px; padding-top: 25px; line-height: 13px; color: #666;">
                <?php echo $officeName;?>
                <br />
                <?php echo $officeAddress;?>
                <br />
                <a href="<?php echo $httpDomainPrefix?>/optout/<?php echo $emailId;?>" style="text-decoration: none; color: #666;">Click here to opt-out from email communication.</a>
            </div>
        </div>
    </body>
</html>