Hi <?php echo $contact->first_name; ?>!<br />
<br />
Here is your password reminder you requested.<br />
<br />
<strong>Login: </strong>	<?php echo $email; ?><br />
<strong>Password: </strong>	<?php echo $contact->password; ?><br />
<br />
<span><a href="<?php echo 'http://'.$domainName.'/login'; ?>" style="color:#D20000; font-weight:bold;">Click Here</a> to Login and view ALL Homes on the market!</span><br />
<br />
If you have any questions, call us at <?php echo Yii::app()->getUser()->getSettings()->office_phone; ?>.<br />
<br />
<br />
Enjoy your home search!<br />
<br />
<br />
<span style="font-size:18px; font-weight:bold; font-family:Arial;"><?php echo Yii::app()->getUser()->getSettings()->office_name; ?></span>
<?php echo $signature; ?>
