<?php $httpDomainPrefix = 'http://www.' . $domain->name; ?>
<html>
    <head>
    </head>
    <body>
        <div style="width: 680px; border: 0px solid #e7e7e7; padding: 6px; background: #FFF;">
            <div style="border-bottom: 1px solid #CCC; padding: 5px; background: #FFF;">
                <div style="float: right">
                    <img src="<?php echo $httpDomainPrefix; ?>/images/logo_admin.png" border="0" style="width:175px;"/>
                </div>
                <p style="font-size: 15px; font-weight: bold;  width: 460px; padding-bottom: 12px;">Hello <?php echo Yii::app()->format->formatProperCase($contact->first_name); ?>,</p>

                <p style="font-size: 13px; width: 460px;">
                    Here are the new or recently updated properties that match your custom search.
                    <br><br>
                    If you have any questions or want to see any properties, reply to this email or call me at <?php echo $phoneNumberDisplay; ?>.

                    <br/><br/>Thanks!<br/><br/>
                    <span style="font-size: 20px; font-weight: bold;">
                        <?php echo $agent->getFullName(); ?>
                    </span>
                    <br/>
                    Real Estate Expert
                    <br/>
                    Call <?php echo $phoneNumberDisplay; ?>
                    <br/><br/>
                </p>
            </div>
            <div style="clear: both; background: #e7e7e7; padding: 5px;">
                <?php foreach ($updatedProperties as $index => $Property):
                    $Property->setActiveDomain($domain)->setActiveMlsBoard($mlsBoard);
                    $propertyLink = $Property->getUrl();
                    $background = ($index % 2 == 0) ? '#FFFFFF' : '#E1E8FF';
                    ?>
                    <div style="display: inline-block; background-color: <?php echo $background; ?>; padding:6px;">
                        <div style="width:260px; display:inline-block; vertical-align:top; padding-right:8px;">
                            <a href="<?php echo $propertyLink . '?utm_source=Market%20Updates&utm_medium=Email&utm_content=View%20Photos&utm_campaign=' . $savedSearchId; ?>">
                                <img src="<?php echo (!empty($Property->firstPhotoUrl))? $Property->firstPhotoUrl : $Property->getPhotoUrl($photoNumber=1, $domain, $mlsBoard->getPrefixedName(), $mlsBoard->has_photo_url); ?>" width="260" alt="View Home Photos - click here">
                            </a>
                        </div>
                        <div style="width:120px; display:inline-block; vertical-align:top; position:relative;">
                            <span style="color:#444; font-size:17px; font-weight:bold; line-height: 15px;">
                                <?php echo Yii::app()->format->formatDollars($Property->price) ?>
                            </span>
                            <span style="color:#222;font-size:12px;display:inline-block;padding-top:6px;">
                                ($<?php echo $Property->pricePerSf; ?>/SF)<br/>
                                MLS#: <?php echo $Property->listing_id; ?><br/>
                            </span>
                            <span style="padding-top: 15px; display:block;">
                                <a style="color:#D20000;font-weight:bold;font-size:13px;" href="<?php echo $propertyLink . '?utm_source=Market%20Updates&utm_medium=Email&utm_content=View%20Photos&utm_campaign=' . $savedSearchId; ?>">View Details</a>
                            </span>
                        </div>
                        <div style="width:130px;color:#222;font-size:12px;vertical-align:top;display:inline-block;position:relative;">
                            <?php echo $Property->bedrooms; ?> Bedrooms<br/>
                            <?php echo $Property->baths_full; ?> Baths<br/>
                            <?php echo $Property->sq_feet; ?> Sq. Feet<br/>
                            <?php echo $Property->year_built; ?> Yr Built<br/>
                            <?php echo ($Property->pool_yn) ? '<span style="color:#36F;font-weight:bold;font-size:13px;">Pool</span><br />' : ''; ?>
                            <?php echo ($Property->waterfront_yn) ? '<span style="color:#36F;font-weight:bold;font-size:13px;">Waterfront</span><br />' : ''; ?>
                            <span style="padding-top: 15px; display:block;">
                                <a style="color:#30BF00;font-weight:bold;font-size:13px;"
                                   href="<?php echo $propertyLink . '?utm_source=Market%20Updates&utm_medium=Email&utm_content=View%20Photos&utm_campaign=' . $savedSearchId; ?>">
                                    View <?php echo $Property->photo_count; ?> Photos
                                </a>
                            </span>
                        </div>
                        <div style="width:126px; color:#222; font-size:12px; vertical-align:top; display:inline-block; position:relative;">
                            <?php echo Yii::app()->format->formatProperCase($Property->common_subdivision) ?><br/>
                            <?php echo Yii::app()->format->formatProperCase($Property->city) ?>, <br/><?php echo substr($Property->zip, 0, 5) ?><br/>
                            <?php echo Yii::app()->format->formatDays($Property->status_change_date); ?><br/>
                            <span style="padding-top: 15px; display:block;">
                                <a style="color:blue;font-weight:bold;font-size:13px;"
                                   href="<?php echo $propertyLink . '?utm_source=Market%20Updates&utm_medium=Email&utm_content=View%20Map&utm_campaign=' . $savedSearchId; ?>#map-tab">
                                    View Map
                                </a>
                            </span>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div style="text-align:center; padding: 5px;">
                    <span style="font-size: 16px;">Viewing <strong><?php echo $propertyLimit; ?></strong> out of <strong><?php echo $propertyCount; ?></strong> homes.</span><br/>
                    <a style="font-size: 20px; font-weight: bold; color: #D20000;" href="<? echo $httpDomainPrefix.'/marketUpdates/'.$savedSearchId; ?>">Click to view more homes!</a>
                </div>
            </div>
            <div style="margin-top: 15px; text-align:center; position: relative;">
                <div style="font-size: 20px; font-weight: bold; padding-bottom: 20px;">
                    <a href="<?php echo $httpDomainPrefix; ?>/value">
                        You may be Surprised at What Your Home is Worth.
                    </a>
                </div>
                <div style="width:680px; height: 187px; background: url('http://cdn.seizethemarket.com/assets/images/emails/house_values_bg.jpg') repeat scroll 0% 0% transparent;">
                    <a href="<?php echo $httpDomainPrefix; ?>/value" style="display: inline-block; text-align:right; text-decoration: none; width: 680px; height: 187px;">
                        <span style="display: inline-block; color: black; font-weight: bold; font-size: 20px; width: 450px; text-align: center;">New <?php echo date('F Y');?> House Values Report</span>
                    </a>
                </div>
            </div>
            <div style="text-align:center; font-size: 10px; padding-top: 25px;">
                <?php echo $officeName;?>
                <br />
                <?php echo $address;?>
                <br />
                <a href="<?php echo $httpDomainPrefix?>/optout/<?php echo $emailId;?>">Click here to opt-out from email communication.</a>
            </div>
        </div>
    </body>
</html>