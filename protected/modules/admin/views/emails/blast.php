<?php
$this->breadcrumbs=array(
	'Blast'=>'',
);
Yii::app()->clientScript->registerCss('emailBlastCss', <<<CSS
.email-body-container {
    width: 800px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 50px;
    margin-bottom: 50px;
    background: white;
    padding: 60px;
    font-size: 13px;
    color: black;
    border: 1px solid #DDD;
}
.email-body-container table {
    background: white;
}
form .button {
    font-size: 11px !important;
    min-height: 30px !important;
}
.ui_tpicker_minute {
    display: none;
}
.ui_tpicker_hour {
    margin-left: 73px !important;
}
CSS
);
Yii::app()->clientScript->registerScript('emailblast', <<<JS

    $('#listview-search form').submit(function() {
        $('#email-blast-grid div.keys').attr('title','');

        $.fn.yiiGridView.update('email-blast-grid', {
            data: $(this).serialize(),
            complete: function(jqXHR, status) {

                var html = $.parseHTML(jqXHR.responseText);
                var emailBodyContainer = $('.email-body-container', $(html)).html();

                $('div.email-body-container').html(emailBodyContainer);

                if($("div#email-blast-grid div.summary").length > 0) {
                    $("#send-emails-button").show('normal');
                    $("#save-draft-button").show('normal');
                }
                else {
                    $("#send-emails-button").hide('normal');
                    $("#save-draft-button").hide('normal');
                }
            }
        });

        return false;
    });

    $('#send-emails-button, #save-draft-button').click(function() {

        if($("div#email-blast-grid div.summary").length <1) {
            Message.create("warning","Please find contacts to send emails to.");
            return false;
        }

        if($('#EmailBlasts_email_template_id').val() == '') {
            Message.create("error","Please select an Email Template.");
            return false;
        }

        if($('#EmailBlasts_sender_name').val() == '') {
            Message.create("error","Please enter Sender Name.");
            return false;
        }

        if($('#EmailBlasts_sender_name').val().length < 10) {
            Message.create("error","Please enter a valid Sender Name.");
            return false;
        }

        if($('#EmailBlasts_email_subject').val() == '') {
            Message.create("error","Please enter a Subject.");
            return false;
        }

        if($('#EmailBlasts_description').val() == '') {
            Message.create("error","Please enter a Description.");
            return false;
        }

        if($('#EmailBlasts_send_datetime').val() == '') {
            Message.create("error","Please enter a Send Date.");
            return false;
        }

        if($(this).attr('id')=='send-emails-button' && confirm('Confirm sending emails now.')) {

            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            $.post('/admin/emails/blastSend', $( "#emails-blast-form" ).serialize(), function (data){

                Message.create(data.status, data.message);

                $("div.loading-container.loading").remove();
            },'json');
        }
        else if($(this).attr('id')=='save-draft-button') {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            $.post('/admin/emails/blastSend?draft=1', $( "#emails-blast-form" ).serialize(), function (data){

                Message.create(data.status, data.message);

                $("div.loading-container.loading").remove();
            },'json');
        }
        return false;
    });

    $('#draftIdToSend').change(function() {
        if($(this).val() !== '') {
            $('#send-draft-button').show('normal');
        }
        else {
            $('#send-draft-button').hide('normal');
        }
    });

    $('#send-draft-button').click(function() {

        if($('#draftIdToSend').val() == '') {
            alert('Select a draft to send.');
            return false;
        }

        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.post('/admin/emails/sendDraft/' + $('#draftIdToSend').val(), function (data){

            Message.create(data.status, data.message);

            $("div.loading-container.loading").remove();
        });

        return false;
    },'json');

    $('#EmailBlasts_email_template_id').change(function() {

        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.post('/admin/emails/emailTemplate', $( "#emails-blast-form" ).serialize(), function (data){
            $('#EmailBlasts_email_subject').val(data.subject);
            $('#EmailBlasts_description').val(data.description);

            emailTemplateBody(data.body, data.id);

        }, 'json');
        $("div.loading-container.loading").remove();
    });

    function emailTemplateBody(bodyHtml, templateId) {
            bodyHtml += '<div style="margin-top: 50px;"><a href="/admin/emailTemplates/edit/'+ templateId +'" class="button gray icon i_stm_edit" target="_blank">Edit Email Template</a>';
            $('.email-body-container').html(bodyHtml);
    }
JS
);
?>
<div id="listview-actions">
	<a href="/admin/emails/blastSent" class="button gray icon i_stm_search">Send History</a>
</div>
<h1>Email Blast</h1>
<?if($error):?>
    <h3>Error: <?=$error?></h3>
<?else:?>
    <h3>Search Contacts to send emails to.</h3>

    <div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <? $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
        'id'=>'emails-blast-form',
    ));
    ?>
        <div class="g12">
            <div class="g3">
                <label class="g3">Source:</label>
                <span class="g9"><?=$form->dropDownList($contacts->source,'idCollection', CHtml::listData(Sources::model()->findAll(), 'id', 'name'), array('multiple' => 'multiple','data-placeholder' => 'Sources','style' => 'width:100%;'));?></span>
            </div>
            <div class="g3">
                <label class="g3">Tags:</label>
                <span class="g9"><?=$form->dropDownList($contacts->contactTypeLu,'idCollection', CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name'), array('multiple' => 'multiple','data-placeholder' => 'Tags','style' => 'width:100%;'));?></span>
            </div>
            <div class="g3">
                <label class="g3">Template:</label>
                <span class="g9"><?=$form->dropDownList($emailBlast,'email_template_id', CHtml::listData(EmailTemplates::model()->findAllByAttributes(array('component_type_id'=>ComponentTypes::CONTACTS)), 'id', 'description'), array('data-placeholder' => 'Email Template','style' => 'width:100%;','empty'=>''));?></span>
            </div>
            <div class="g1 submit" style="text-align:center">
                <?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?>
            </div>
            <div class="g1 submit" style="text-align:center">
                <?php echo CHtml::submitButton('SEND', array('class'=>'button','id'=>'send-emails-button','style'=>'display:none;')); ?>
            </div>
            <div class="g1 submit" style="text-align:center">
                <?php echo CHtml::submitButton('SAVE DRAFT', array('class'=>'button','id'=>'save-draft-button','style'=>'display:none;')); ?>
            </div>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#EmailBlasts_email_template_id, select#draftIdToSend','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'allow_single_deselect' => true))); ?>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => '#Sources_idCollection, #ContactTypeLu_idCollection, #Sources_excludeIds, #ContactTypeLu_excludeIds','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'allow_single_deselect' => true))); ?>


            <div class="g3 p-clr">
                <label class="g4">Exclude Source:</label>
                <span class="g8"><?=$form->dropDownList($contacts->source, 'excludeIds', CHtml::listData(Sources::model()->findAll(), 'id', 'name'), array('multiple' => 'multiple','data-placeholder' => 'Sources','style' => 'width:100%;'));?></span>
            </div>
            <div class="g3">
                <label class="g4">Exclude Tags:</label>
                <span class="g8"><?=$form->dropDownList($contacts->contactTypeLu, 'excludeIds', CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name'), array('multiple' => 'multiple','data-placeholder' => 'Tags','style' => 'width:100%;'));?></span>
            </div>

            <div class="email-blast-container g100">
                <div class="g3">
                    <label class="g3">Subject:</label>
                    <span class="g9"><?=$form->textField($emailBlast,'email_subject', array('placeholder' => 'Subject'));?></span>
                </div>
                <div class="g3">
                    <label class="g3">Description:</label>
                    <span class="g9"><?=$form->textField($emailBlast,'description', array('placeholder' => 'Description'));?></span>
                </div>
                <div class="g3">
                    <label class="g3">From Name:</label>
                    <span class="g9"><?=$form->textField($emailBlast,'sender_name', array('placeholder' => 'Sender Name'));?></span>
                </div>
                <div class="g3">
                    <label class="g3">From Email:</label>
                    <span class="g9"><?=$form->dropDownList($emailBlast,'sender_email_id', CHtml::listData(Emails::model()->findAll(array('condition'=>'contact_id='.Yii::app()->user->id)), 'id', 'email'), array('empty' => 'Select From Email'));
                        //(Emails::model()->findAll(array('condition'=>'email="christine_l@christineleeteam.com" OR email="careers@christineleeteam.com" OR email="mandy@referco.com"')
                        ?></span>
                </div>
            </div>

            <div class="g3">
                <label class="g3">Last Login:</label>
                <span class="g9">
                    <?//=$form->dropDownList($contacts,'last_login', CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name'), array('multiple' => 'multiple','data-placeholder' => 'Tags','style' => 'width:100%;'));?>
                    <?php $this->widget(
                        'zii.widgets.jui.CJuiDatePicker', array(
                            'model'       => $contacts,
                            'attribute'   => 'fromSearchDate',
                            // name of post parameter
                            'options'     => array(
                                'showAnim'       => 'fold',
                                'dateFormat'     => 'yy-mm-dd',
                                'numberOfMonths' => 2,
                            ),
                            'htmlOptions' => array(
                                'style' => 'width: 80px; float: left; font-size: 12px;',
                            ),
                        )
                    );
                    ?>
                    <?php $this->widget(
                        'zii.widgets.jui.CJuiDatePicker', array(
                            'model'       => $contacts,
                            'attribute'   => 'toSearchDate',
                            // name of post parameter
                            'options'     => array(
                                'showAnim'       => 'fold',
                                'dateFormat'     => 'yy-mm-dd',
                                'numberOfMonths' => 2,
                            ),
                            'htmlOptions' => array(
                                'style' => 'width: 80px; float: left; font-size: 12px;',
                            ),
                        )
                    );
                    ?>
                </span>
            </div>
            <div class="g3">
                <label class="g5">Send Date/Time:</label>
                <span class="g7">
                    <?php $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                            'model'=>$emailBlast,
                            'attribute'=>'send_datetime',
                            // additional javascript options for the date picker plugin
                            'mode'=>'datetime',
                            'options'=>array(
                                // 'showAnim'=>'fold',
                            ),
                            'htmlOptions'=>array(
                                'class'=>'g12',
                                // 'value'=>date('m/d/Y h:i a'),
                                //																							   'readonly'=>true,
                            ),
                        ));

                    ?>
                </span>
            </div>
            <div class="g3">
                <label class="g3">Send Draft:</label>
                <span class="g7">
                    <?=CHtml::dropDownList('draftIdToSend', null, CHtml::listData(EmailBlasts::model()->findAllByAttributes(array('status'=> 'Draft')), 'id', 'description'), array('data-placeholder' => 'Select Draft','style' => 'width:100%;','empty'=>''));?>
                </span>
            </div>
<!--            <div class="g11">-->
<!--                <label class="g1">Emails:</label>-->
<!--                <span class="g11">-->
<!--                    --><?//=CHtml::textArea('emailsPerLine', null, array('placeholder' => 'Emails 1 per Line','style' => 'width:100%;','rows'=>8));?>
<!--                </span>-->
<!--            </div>-->
            <div class="g1 submit" style="text-align:center">
                <?php echo CHtml::submitButton('SEND DRAFT', array('class'=>'button','id'=>'send-draft-button','style'=>'display:none;')); ?>
            </div>
        </div>
    <? $this->endWidget(); ?>
</div><!-- search-form -->
<div class="p-clr"></div>
<div class="email-body-container p-clr" style="width: 800px; margin-left: auto; margin-right: auto; margin-top: 50px; margin-bottom: 50px;">
    <?
    if($emailTemplates->body) {
        echo $emailTemplates->body;
        echo '<div style="margin-top: 50px;"><a href="/admin/emailTemplates/edit/'.$emailTemplates->id.'" class="button gray icon i_stm_edit" target="_blank">Edit Email Template</a></div>';
    }
    ?>
</div>
<?
$this->widget('admin_module.components.StmGridView', array(
        'id' => 'email-blast-grid',
        'dataProvider' => $contacts->search(),
        'itemsCssClass' => 'datatables',
        'template' => '{pager}{summary}{items}{pager}{summary}',
        'columns' => array(
            'fullName',
            'primaryEmail',
            'primaryPhone:phone',
            'added:date',
            'last_login:date',
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->id."/contacts/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
        ),
    )
);

endif;