<html>
    <head>
        <title>
            <?=$model->contact->fullName?>'s One Thing Report <?=date('n/j/Y', strtotime($model->date))?>
        </title>
    </head>
    <body>
        <h1>
            <?=date('n/j/Y - l', strtotime($model->date))?><br/>
            <?=$model->contact->fullName?>'s One Thing Report
        </h1>
        <h2>
            Today's Goal Accomplished: <?=$dailyGoalPercent?>%<br>
            Monthly Goal Accomplished: <?=$monthlyGoalPercent?>%
        </h2>

        <h2 style="margin-bottom: 0; text-decoration: underline; display: inline-block;">Today's One Thing:</h2>
        <?=$model->goal_description?>

        <br/>
        <br/>
        <h2 style="margin-bottom: 0; text-decoration: underline; display: inline-block;">Today's Results:</h2>
        <?=$model->results_description?>

        <br/>
        <br/>
        <h2 style="margin-bottom: 0; text-decoration: underline; display: inline-block;">Challenges:</h2>
        <?=$model->challenges?>

        <br/>
        <br/>
        <h2 style="margin-bottom: 0; text-decoration: underline; display: inline-block;">Tomorrow Must Do's:</h2>
        <?=$model->tomorrow_must_do?>

        <br/>
        <br>
        <br>
        <img src="<?php echo $domainName; ?>/images/logo_admin.png" border="0" style="width:200px;" alt="Team Logo" />

    </body>
</html>