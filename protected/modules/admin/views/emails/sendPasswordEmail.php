<span style='font-family:Arial; font-size:10pt'>
	Hi <?php echo $contact->first_name; ?>,

	<p>Here is your account information!</p>

	<ul>
		<b><u>Account Features:</b></u>
		<li>Save &amp; Watch your Favorite homes</li>
		<li>Access to your Recently Viewed homes</li>
		<li>FULL Access to every home on the market!</li>
		<li>Manage Your Profile</li>
	</ul>

	<i>Your login information is:</i><br>
	&nbsp;&nbsp;&nbsp;&nbsp;Email: <strong><?php echo $contact->getPrimaryEmail(); ?></strong><br />
	&nbsp;&nbsp;&nbsp;&nbsp;Password: <strong><?php echo $contact->password; ?></strong>
	<br />
	<br />
	<span>
		<a href="<?php echo 'http://'.$domainName.'/login'; ?>" style="color:#D20000; font-weight:bold;">Click Here</a> to Login and view ALL Homes on the market!
		<br />
		<br />
		You can also <a href="<?php echo 'http://'.$domainName.'/myAccount'; ?>" style="color:#D20000; font-weight:bold;">Click Here</a> to update your Profile!

	</span>
	<br />
	If you have any questions, call us at <?=$officePhone?> to get immediate live service.
	<br />
	Look forward to helping you.
	<br />
	<br />
	<span style="font-size:18px; font-weight:bold; font-family:Arial;"><?=$officeName; ?></span>
</span>