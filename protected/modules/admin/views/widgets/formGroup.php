<?php
$formType =  (strpos($this->action->id,'edit') !== false)?'Edit':'Add';
$this->breadcrumbs=array(
    'Groups'=>array('/'.Yii::app()->controller->module->id.'/widgets'),
    $formType=>'',
);

if($formType == 'Edit') {
    $this->breadcrumbs[$model->name] = '';
}
?>
<h1><?php echo $formType; ?> Widget Group</h1>

<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'featured-areas-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<!-- ====== FEATURED AREA INFO =============================================================================================== -->
	<div id="top-data" class="row">
		<div class="g100 p-p0 p-m0">
			<? $this->beginStmPortletContent(array(
				'handleTitle'=>'Widget Group Details',
				'handleIconCss'=>'i_strategy'
			)); ?>
			<div id="featured-area-container">
				<div class="g12 p-mb5 rounded-text-box odd-static">
                    <div class="g5">
                        <table class="container">
                            <tr>
                                <th>
                                    <?php echo $form->labelEx($model, 'status_ma')?>:
                                </th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'status_ma', StmFormHelper::getStatusBooleanList(), $htmlOptions=array()); ?>
                                    <?php echo $form->error($model,'status_ma'); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?php echo $form->labelEx($model, 'name')?>:
                                </th>
                                <td>
                                    <?php echo $form->textField($model, 'name', $htmlOptions=array('placeholder'=>'Widget Group Name', 'class'=>'g12')); ?>
                                    <?php echo $form->error($model,'name'); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?php echo $form->labelEx($model, 'description')?>:
                                </th>
                                <td>
                                    <?php echo $form->textArea($model, 'description', $htmlOptions=array('placeholder'=>'Description', 'class'=>'g12', 'rows'=>'3')); ?>
                                    <?php echo $form->error($model,'description'); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="g7">
                        <table class="container">
                            <tr>
                                <th class="narrow"><?php echo $form->labelEx($model, 'widgetsToAdd')?>:</th>
                                <td>
                                    <?php $this->widget('admin_widgets.StmMultiSelectWidget', array(
                                            'model'=>$model,
                                            'attribute'=>'widgetsToAdd',
                                            'listData'=>CHtml::listData(CmsWidgets::model()->findAll(
                                                        array('with'=>'cmsWidgetGroupLu',
                                                              'order'=>'cmsWidgetGroupLu.sort_order asc'
                                                        )
                                                    ), 'id', 'name'),
                                        )); ?>
                                    <?php echo $form->error($model,'widgetsToAdd'); ?>
                                </td>
                            </tr>
                        </table>
                    </div>

				</div>
			</div>
		</div>
	</div>
<!-- ====== END SECTIONS =============================================================================================== -->
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Widget</button></div>

	<?php $this->endWidget(); ?>

</div><!-- form -->
<?php $this->endStmPortletContent(); ?>
<hr />
<? $this->renderPartial('_listWidgets', array('WidgetModel' =>$WidgetModel)); ?>