<?php
$this->breadcrumbs=array(
	'Groups'=>array('/'.Yii::app()->controller->module->id.'/widgets/groups'),
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('domain-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="content-header">
	<h1>Widget Groups</h1>
</div>

<!-- <div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <? $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
        'id'=>'domain-list-search',
    ));
    ?>
        <div class="g12">
            <div class="g10">
                <label class="g4">Name:</label>
                <span class="g6"><?php echo $form->textField($model,'name');?></span>
            </div>
            <div class="g2 submit" style="text-align:center">
                <?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?>
            </div>
        </div>
    <? $this->endWidget(); ?>
</div> --><!-- search-form -->

<?php
$this->widget('admin_module.components.StmGridView', array(
    'id'=>'domain-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Status',
            'value'=>'StmFormHelper::getStatusBooleanName($data->status_ma)',
        ),
        'name',
        'description',
        array(
            'type'=>'raw',
            'name'=>'Widgets',
            'value'=>'Yii::app()->format->formatCommaDelimited($data->cmsWidgets, "name")',
        ),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/widgets/editGroup/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
            'htmlOptions' => array('style' => 'width:80px'),
        ),
    ),
));