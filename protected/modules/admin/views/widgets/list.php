<?php
$this->breadcrumbs=array(
	'All'=>array('/'.Yii::app()->controller->module->id.'/widgets'),
);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->id; ?>/widgets/addGroup" class="button gray icon i_stm_add">Add Widget Groups</a>
</div>
<? $this->renderPartial('_listWidgetGroups', array('model'=>$WidgetGroupsModel)); ?>
<hr />
<? $this->renderPartial('_listWidgets', array('WidgetModel'=>$model)); ?>