<?php
Yii::import('front_module.FrontModule');

$this->breadcrumbs=array(
    'View'=>array('/'.Yii::app()->controller->module->id.'/widgets'),
    $model->name=>array('/'.Yii::app()->controller->module->id.'/widgets/'.$model->name),
);
?>

<h1><?php echo $model->name;?> Widget</h1>

<div class="g12" style="margin-left: auto; margin-right: auto; margin-top: 20px;">
    <div class="g5"></div>
    <div style="width: 240px; float:left;">
        <?
        $this->widget('front_module.components.widgets.'.$model->folder_name.'.'.$model->folder_name);
        ?>
    </div>
</div>
