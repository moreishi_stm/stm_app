<?php
$this->breadcrumbs = array(
	'List' => '',
);
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('smsChatScript', <<<JS
    $('#send-sms-message-button').click(function(){

        if($('#smsMessage').val() == '') {
            alert('Please enter an SMS message');
            return false;
        }

        $('body').prepend('<div class="loading-container loading"><em></em></div>');

        $.post('/$module/sms/send/$id', $("#smsMessage").serialize(), function(data) {
            $("div.loading-container.loading").remove();

            if(!data.status) {
                $('.loading-container.loading').remove();
                Message.create("error", "Error: SMS could not be sent. Please try again.");
                return false;
            }

            if(data.status == "error") {
                $('.loading-container.loading').remove();
                Message.create("error", "Error: " + data.message);
                return false;
            }

            Message.create("success", "Successfully sent SMS!");

            // refresh grid
            $.fn.yiiGridView.update('sms-chat-grid', {
                data: $(this).serialize()
            });
            $("#smsMessage").val('');

        }, "json");
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update("sms-chat-grid", {
            data: $(this).serialize()
        });
        return false;
    });

	$("#listview-search form select").on("change",function(e){
		$("#listview-search form").submit();
	});
JS
);

?>
<style type="text/css">
    .datatables td:not(:first-child), .datatables th:not(:first-child) {
        text-align: center;
    }
    .button.gray.icon {
        float: left;
    }
    #ChatFilterForm input{
        max-width: 240px;
    }
    #ChatFilterForm button{
        border-radius: 0;
        padding: 6px;
        margin-top: 0;
    }
    #red{
        max-width: 800px;
        margin-left: auto;
        margin-right: auto;
    }
    #sms-chat-grid tr {
        background: none;
        background-image: none !important;
        background-color: transparent !important;
        text-shadow: none !important;
    }
    #sms-chat-grid th, #sms-chat-grid td{
        border: 0 !important;
    }
    #send-sms-message-button {
        padding-left: 0 !important;
        padding-right: 0;
    }
    .chat-bubble {
        min-width: 40%;
        max-width: 75%;
        padding: 16px;
        background-color: #d1eaff;
        color: #444;
        float: right;
    }
    .chat-bubble.fromLead {
        background-color: #ffffb3;
        float: left;
    }
    .chat-datetime {
        font-size: 12px;
    }
    .chat-message {
        font-size: 20px;
    }
    @media (max-width: 450px) {
        body {
            width: 100%;
            margin: 0;
        }
        body > #content.container {
            width: 100% !important;
        }
        #ChatFilterForm > div {
            width: 100%;
        }
        #ChatFilterForm input, #ChatFilterForm select {
            width: 70% !important;
        }
        #ChatFilterForm button[type="submit"] {
            width: 97% !important;
        }
        .chat-message {
            font-size: 15px;
        }
    }
</style>

<!--	<h1>SMS / Text Message Chat</h1>-->
	<h1><?=$display = SmsMessages::getPhoneNameDisplay($id)?></h1>
    <h3><?=Yii::app()->format->formatPhone(substr($id, 1))?></h3>
	<div class="g12">
		<div class="g12"><?=CHtml::textArea('smsMessage', null, $htmlOptions=array('rows'=>3, 'placeholder' => 'Type text message here...', 'class'=>'g12', 'style' => 'font-size: 18px;'))?></div>
		<div class="g12"><a href="javascript:void(0)" id="send-sms-message-button" class="g100 button gray icon i_stm_search grey-button" style="font-size: 18px;">Send Text Message</a></div>
	</div>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <form id="ChatFilterForm" action="/<?php echo $module;?>/sms/chat/<?php echo $id;?>" method="GET">
        <div class="g6">
            <label class="g3">Search:</label>
            <input class="g8" title="Search Messages" name="SmsMessageSearch[q]" type="text" value="" placeholder="Enter Text...">
        </div>
        <div class="g3">
            <label class="g3">Sort:</label>
            <select class="g9" name="SmsMessageSearch[sort]">
                <option value="new">Newest Top</option>
                <option value="old">Newest Bottom</option>
            </select>
        </div>
        <div class="g1">
            <button type="submit">Search</button>
        </div>
    </form>
</div>
<div id="SmsChatLogTable">
<?php
$this->widget('admin_module.components.StmGridView', array(
	'id' => 'sms-chat-grid',
	'template' => '{items}',
	'dataProvider' => $DataProvider,
	'extraParams' => array('dateRange' => $dateRange),
	'itemsCssClass' => 'datatables',
	'columns' => array(
//		array(
//			'type' => 'raw',
//			'name' => 'Sent To',
//			'value' => 'Yii::app()->format->formatPhone($data["to_phone_number"])',
//			'htmlOptions' => array('style' => 'width:140px;'),
//		),
//		array(
//			'type' => 'raw',
//			'name' => 'Sent From',
//			'value' => 'Yii::app()->format->formatPhone($data["from_phone_number"])',
//			'htmlOptions' => array('style' => 'width:140px;'),
//		),
//		array(
//			'type' => 'raw',
//			'name' => 'Sent / Received Date',
//			'value' => 'Yii::app()->format->formatDate($data["processed_datetime"], "n/j/y")." ".Yii::app()->format->formatTime($data["processed_datetime"])',
//			'htmlOptions' => array('style' => 'width:140px;'),
//		),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => 'Yii::app()->controller->printChatBubble($data, '.$id.')',
        ),
//		array(
//			'type' => 'raw',
//			'name' => 'Message',
//			'value' => '$data["content"]',
//		),
	),
));
?>
</div>

<div class="g12">
<!--    <h2 class="p-tl">Contact Matches by Phone</h2>-->
<!--    <h5 class="p-tl">Below are contacts and related sellers, buyers for this phone number.</h5>-->
<!--    --><?//=Yii::app()->controller->printContactMatches($id, $break=false)?>
</div>