<style>
	#SmsListFilterForm label{
		font-weight: 900;
	}

	#SmsListFilterForm button{
		border-radius: 0;
		padding: 6px;
		margin-top: 0;
	}
	.datatables td:not(:first-child), .datatables th:not(:first-child) {
		text-align: center;
	}
    #blue{
        max-width: 800px;
        margin-left: auto;
        margin-right: auto;
    }
    .datatables tbody:hover{
        cursor: pointer;
    }
    @media screen and (max-width: 900px) {
        .view-column {
            display: none;
        }
    }
</style>
<?php
Yii::app()->clientScript->registerScript('sms-activity-script', <<<JS

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update("sms-outbound-activity-grid", {
            data: $(this).serialize()
        });
        return false;
    });

	$("#listview-search form select").on("change",function(e){
		$("#listview-search form").submit();
	});
JS
);

$this->breadcrumbs = array(
	'List' => '/admin/sms'
);
?>
<h1>Sms Activity</h1>
<h3 id="date-range-label">
	<label id="from-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['from_date'])); ?></label> -
	<label id="to-date-label" style="font-weight: bold;"><?php echo date("m/d/Y", strtotime($dateRange['to_date'])); ?></label>
</h3>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<form id="SmsListFilterForm" action="/<?php echo $module;?>sms" method="post">
		<div class="g4" style="padding: 0 24px;">
			<label>Phone:</label>
			<input style="width:196px;" name="SmsListFilterForm[q]" type="text" value=" " placeholder="Search by Contact Phone">
		</div>
		<div class="g3" style="padding: 0 24px;">
			<label>Sort By:</label>

			<select name="SmsListFilterForm[sort]">
				<option value="new">Newest First</option>
				<option value="old">Oldest First</option>
			</select>

		</div>
		<div class="g1" style="padding: 0 24px;">
			<button type="submit">Search</button>
		</div>
	</form>
</div>

<?php


$form = $this->beginWidget('CActiveForm', array(
	'id' => 'sms-activity-form',
	'action' => '',
	'method' => 'post',
));
?>

<div class="g12" style="position: absolute; top:2px;">
	<?php
	$this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
		'fromDateLabelSelector' => '#from-date-label',
		'toDateLabelSelector' => '#to-date-label',
		'gridName' => 'sms-outbound-activity-grid',
		'isForm' => false,
		'formSelector' => '#sms-activity-form',
		'defaultSelect' => 'month_to_date',
	));
	?>
</div>
<div id="SmsActivityTable">
	<?php
	$this->widget('admin_module.components.StmGridView', array(
		'id' => 'sms-outbound-activity-grid',
		'template' => '{items}',
		'dataProvider' => $DataProvider,
		'extraParams' => array('dateRange' => $dateRange),
        'selectionChanged'=>"function(id){window.location='/admin/sms/chat/' + $.fn.yiiGridView.getSelection(id);}",
		'itemsCssClass' => 'datatables',
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Phone',
				'value' => '"<strong style=\"font-size: 14px;\">".SmsMessages::getPhoneNameDisplay($data["phone"])."</strong><br>".SmsMessages::getLastMessage($data["phone"], '.$stmNumber->phone.')',
			),
            array(
                'type' => 'raw',
                'name' => 'Last Message',
                'value' => '((strpos($data["processed_datetime"], date("Y-m-d")) === false) ? Yii::app()->format->formatDate($data["processed_datetime"], "n/j/Y") : Yii::app()->format->formatTime($data["processed_datetime"]))',
                'htmlOptions' => array('style' => 'width:100px;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'View',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/".Yii::app()->controller->id."/chat/".$data["phone"]."\" class=\"button gray icon i_stm_search grey-button\">View (".SmsMessages::getMessageCount($data["phone"], '.$stmNumber->phone.').")</a></div>"',
                'htmlOptions' => array('style' => 'width:110px;','class'=>'view-column'),
                'headerHtmlOptions' => array('class'=>'view-column'),
            ),
//			array(
//				'type' => 'raw',
//				'name' => 'Contact Matches',
//				'value' => 'Yii::app()->controller->printContactMatches($data["phone"])',
//				'htmlOptions' => array('style' => 'width:140px;'),
//			),
		),
	));

	$this->endWidget();
	?>
</div>