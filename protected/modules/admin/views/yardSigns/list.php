<?php
	$this->breadcrumbs = array(
		'List' => array('/admin/yardSigns'),
	);
    $module = Yii::app()->controller->module->name;
    $js = <<<JS
//        $("#ActionPlans_component_type_id, #ActionPlans_status_ma").change(function(){
//            $('#listview-search form').submit();
//        });

        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("yard-signs-grid", {
                data: $(this).serialize()
            });
            return false;
        });

//        $( '.delete-action-plan-button' ).live( "click", function() {
//            if(confirm('Are you sure you want to delete this Action Plan?')) {
//                $("body").prepend("<div class='loading-container loading'><em></em></div>");
//                var id = $(this).data('id');
//                $.post('/$module/actionPlans/delete/'+id, function(data) {
//                    $("div.loading-container.loading").remove();
//                    if(data=='') {
//                        Message.create("success","Action Plan Item deleted successfully.");
//                        $.fn.yiiGridView.update("action-plan-grid", { data: $(this).serialize() });
//                    } else {
//                        Message.create("error","Error: Action Plan did not delete.");
//                    }
//                },"json");
//            }
//        });
JS;

    Yii::app()->clientScript->registerScript('yardSignListScript', $js);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name;?>/yardSigns/add" class="button gray icon i_stm_add">Add Yard Sign</a>
</div>
<div id="content-header">
	<h1>Yard Signs Inventory</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'yard-signs-list-search',
        )); ?>
    <div class="g2">
        <label class="g6">Status:</label>
            <span class="g6">
                <?php echo $form->dropDownList($model, 'status_ma', $model->getStatusList(), $htmlOptions=array('empty'=>'Select One')); ?>
            </span>
    </div>
    <div class="g2">
        <label class="g5">Type:</label>
		<span class="g7">
                <?php echo $form->dropDownList($model, 'type_ma', $model->getTypesList(), $htmlOptions=array('empty'=>'Select One')); ?>
		</span>
    </div>
    <div class="g3">
        <label class="g6">Internal Sign ID:</label>
        <span class="g6"><?php echo $form->textField($model,'internal_sign_id');?></span>
    </div>
    <div class="g3">
        <label class="g4">Notes:</label>
        <span class="g8"><?php echo $form->textField($model,'notes');?></span>
    </div>
    <div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'yard-signs-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
            'internal_sign_id',
            array(
                'type' => 'raw',
                'name' => 'Status',
                'value' => '$data->getStatusList()[$data->status_ma]',
            ),
            array(
                'type' => 'raw',
                'name' => 'Type',
                'value' => '$data->getTypesList()[$data->type_ma]',
            ),
            'ivr_extension',
            'notes',
            'updated:date',
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/yardSigns/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
		),
	)
);
?>
