<?php
$action = ($this->action->id == 'add') ? 'Add' : 'Edit';
$this->breadcrumbs=array(
    $action=>'',
);

?>
    <div id="featured-areas-header">
        <h1 class="name">Add New Yard Sign</h1>
    </div>

<?php
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'yard-sign-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=>'Yard Signs',
        'handleIconCss'=>'i_strategy'
    ));
?>
    <div id="yard-sign-container">
        <div class="g12 p-mb5 rounded-text-box odd-static" style="padding:30px 0 30px 0;">
            <table class="container">
<!--                <tr>-->
<!--                    <th>Name:</th>-->
<!--                    <td colspan="3">-->
<!--                        --><?php //echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Source Name', 'class'=>'g6', ));?>
<!--                        <div class="p-clr p-pt10">-->
<!--                            --><?php //echo CHtml::checkBox('isSubsource');?>
<!--                            <span class="label">Subsource of:</span>-->
<!--                            --><?php //echo $form->dropDownList($model, 'parent_id', CMap::mergeArray(array(''=>''),CHtml::listData(Sources::model()->findAll(), 'id', 'name')), $htmlOptions = array('style'=>'width:250px;','data-placeholder'=>' ','disabled'=>$subSourceDisabled,'class'=>'chzn-select'
//                                ));
//
//                            $this->widget('admin_module.extensions.EChosen.EChosen', array(
//                                    'target' => 'select#Sources_parent_id',
//                                    'options'=>array('allow_single_deselect'=>true),
//                                ));
//
//                            ?>
<!--                        </div>-->
<!--                        --><?php //echo $form->error($model,'name'); ?><!----><?php //echo $form->error($model,'parent_id'); ?>
<!---->
<!--                    </td>-->
<!--                </tr>-->
                <tr>
                    <th>Status:</th>
                    <td colspan="3">
                        <?php echo $form->dropDownList($model,'status_ma', $model->getStatusList(), $htmlOptions=array('empty'=>'', 'class'=>'g6', ));?>
                        <?php echo $form->error($model,'status_ma'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Type:</th>
                    <td colspan="3">
                        <?php echo $form->dropDownList($model,'type_ma', $model->getTypesList(), $htmlOptions=array('empty'=>'', 'class'=>'g6', ));?>
                        <?php echo $form->error($model,'type_ma'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Internal Sign ID:</th>
                    <td colspan="3">
                        <?php echo $form->textField($model,'internal_sign_id', $htmlOptions=array('placeholder'=>'Internal Sign ID', 'class'=>'g6', ));?>
                        <?php echo $form->error($model,'internal_sign_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Sign Rider IVR Extension:</th>
                    <td colspan="3">
                        <?php echo $form->textField($model,'ivr_extension', $htmlOptions=array('placeholder'=>'Sign Rider IVR Extension', 'class'=>'g6', ));?>
                        <?php echo $form->error($model,'ivr_extension'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Notes:</th>
                    <td colspan="3">
                        <?php echo $form->textarea($model,'notes', $htmlOptions=array('placeholder'=>'Notes', 'class'=>'g6','rows'=>8 ));?>
                        <?php echo $form->error($model,'notes'); ?>
                    </td>
                </tr>
<!--                <tr>-->
<!--                    <th># Days to Follow-up:</th>-->
<!--                    <td colspan="3">-->
<!--                        --><?php //echo $form->textField($model,'follow_up_days', $htmlOptions=array('placeholder'=>'Follow-up Days', 'class'=>'g6', ));?>
<!--                        --><?php //echo $form->error($model,'follow_up_days'); ?>
<!--                    </td>-->
<!--                </tr>-->
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Yard Sign</button></div>
<?php $this->endWidget(); ?>