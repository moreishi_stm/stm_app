<?php
Yii::app()->clientScript->registerScript('search', <<<JS
    $('form .reset-button').click(function() {

        var componentTypeIdElement = '#SMSTemplates_component_type_id option[value="' + $('#SMSTemplates_component_type_id').val() + '"]';
        $(componentTypeIdElement).removeAttr('selected');
        $('#SMSTemplates_searchContent').val('');
        $('#SMSTemplates_body').val('');
        $('form#sms-templates-list-search').submit();
    });

    $('#listview-search form select').change(function() {
        $(this).submit();
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('sms-templates-grid', { data: $(this).serialize() });
        return false;
    });
JS
);

$form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sms-templates-list-search',
)); ?>
<div class="g2">
	<label class="g5">Status:</label>
		<span class="g7">
    		<?php echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive'), $htmlOptions=array('empty'=>'Select One')); ?>
		</span>
</div>
<div class="g2">
	<label class="g5">Category:</label>
		<span class="g7">
    		<?php
            $criteria = new CDbCriteria();
            if(Yii::app()->user->checkAccess('transactions')) {
                $criteria->compare('id', ComponentTypes::RECRUITS);
            }
            echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll($criteria), 'id', 'display_name'), $htmlOptions=array('empty'=>'Select One'));
            ?>
		</span>
</div>
<div class="g3">
	<label class="g3">Content:</label>
	<span class="g9"><?php echo $form->textField($model,'searchContent');?></span>
</div>
<div class="g3">
	<label class="g3">Body:</label>
	<span class="g9"><?php echo $form->textField($model,'body');?></span>
</div>
<div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<div class="g1 submit" style="text-align:center"><?php echo CHtml::resetButton('RESET', array('class'=>'button reset-button','style'=>'font-size: 11px; height: 30px;')); ?></div>
<?php $this->endWidget();
