<?php
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'sms-template-form',
			'enableAjaxValidation' => false,
		)
	);
	$this->beginStmPortletContent(array(
			'handleTitle' => 'SMS Template Details',
			'handleIconCss' => 'i_wizard'
		)
	);
?>
<div id="action-plans-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container">
			<tr>
				<th class="narrow">Status:</th>
				<td>
					<?php echo $form->dropDownList($model, 'status_ma', array(
							1 => 'Active',
							0 => 'Inactive'
						), $htmlOptions = array('class' => 'g2')
					); ?>
					<label class="label p-fl p-pr4" style="padding-left:100px;">Type:</label>
					<?php echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll(), 'id', 'display_name'), $htmlOptions = array(
							'empty' => 'Select One',
							'class' => 'g2'
						)
					); ?>
					<?php echo $form->error($model, 'status_ma'); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow">Description:</th>
				<td>
					<?php echo $form->textField($model, 'description', $htmlOptions = array(
							'placeholder' => 'Description',
							'class' => 'g7',
						)
					); ?>
					<?php echo $form->error($model, 'component_type_id'); ?><?php echo $form->error($model, 'description'); ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr/>
					<h3>Edit the SMS Message Below:</h3>
				</td>

			</tr>
			<tr>
				<th class="narrow">Name:</th>
				<td>
					<?php echo $form->textField($model, 'name', $htmlOptions = array(
							'placeholder' => 'Name',
							'class' => 'g7',
						)
					); ?>
					<?php echo $form->error($model, 'name'); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow">Body:</th>
				<td style="width: 800px;">
                    <div style="font-weight: bold;">
                        To have the first name of the contact appear in the email, type in {{first_name}}.<br />
                        Example: Hello {{first_name}}, how are you doing?<br /><br />
						To have the appointment date appear in the email, type in {{appointment_date}}<br />
						Example: You have an appointment scheduled for {{appointment_date}}<br /><br />

						To have the seller address appear in the email, the following fields are available: {{seller_address}}, {{seller_city}}, {{seller_state}} and {{seller_zip}}<br /><br />

						To have the recruit information appear in the email, the following fields are available: {{recruit_units}}, {{recruit_volume}}, {{recruit_volume_change}} and {{recruit_volume_change_percent}}<br /><br />

						WARNING: Do NOT copy from a Microsoft document as it adds invalid hidden tags.<br /><br />
                    </div>
                    <?php echo $form->error($model, 'body'); ?>
					<?php echo $form->textArea($model, 'body'); ?>
				</td>
			</tr>
		</table>

	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
	<button type="submit" class="submit wide">Submit SMS Template</button>
</div>
<?php $this->endWidget(); ?>