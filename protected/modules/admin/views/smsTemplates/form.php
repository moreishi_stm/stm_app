<?php
	$this->breadcrumbs = array(
		ucwords($this->action->id) => '',
	);

	if ($this->action->id == 'edit') {
		$this->breadcrumbs = array(
			$model->componentType->display_name => '/'.Yii::app()->controller->module->name.'/smsTemplates/' . $model->id,
			ucwords($this->action->id) => '',
			$model->name => '/'.Yii::app()->controller->module->name.'/smsTemplates/' . $model->id,
		);
	}
	
	echo $this->renderPartial('_form', array('model' => $model));
