<?php
$css = <<<CSS
    td.clear-formatting{
        background-color: white;
        font-family: Arial, Helvetica, sans-serif;
    }
CSS;
Yii::app()->clientScript->registerCss('editSmsTemplate', $css);

$this->breadcrumbs=array(
	$model->description=>'',
);
$this->beginStmPortletContent(array(
	'handleTitle'=>'Sms Template :: '.$model->description,
	'handleIconCss'=>'i_wizard',
	'handleButtons'    => array(
		array(
			'label'        =>'Edit',
			'iconCssClass' =>'i_stm_edit',
			'htmlOptions'  =>array('id'=>'edit-contact-button'),
			'name'         =>'edit-contact',
			'type'         =>'link',
			'link'		   =>'/'.Yii::app()->controller->module->name.'/smsTemplates/edit/'.$model->id,
		)
	),
));
?>
<style type="text/css">
	p {
		margin-bottom: 18px;
	}
    span {
        font-family: inherit !important;
    }
</style>
<div id="email-templates-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container p-mt10">
			<tr>
				<th>Status:</th>
				<td><?php echo StmFormHelper::getStatusBooleanName($model->status_ma)?><label class="label" style="padding-left: 100px;">Type:</label><?php echo $model->componentType->display_name?></td>
			</tr>
			<tr>
				<th>Description:</th>
				<td><?php echo $model->description;?></td>
			</tr>
		</table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<table style="width: 800px; float: none; clear: both; margin: 0 auto;">
    <tr>
        <th colspan="2"><h2>Sms Message</h2></th>
    </tr>
    <tr>
        <th class="">Name:</th>
        <td><h2 style="text-align: left;"><?php echo $model->name;?></h2></td>
    </tr>
    <tr>
        <th>Message:</th>
        <td class="clear-formatting"><?php echo $model->body;?></td>
    </tr>
</table>