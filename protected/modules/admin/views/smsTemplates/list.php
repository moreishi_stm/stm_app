<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->name.'/smsTemplates'),
);

?>
	<div id="listview-actions">
		<a href="/<?php echo Yii::app()->controller->module->name;?>/smsTemplates/add" class="button gray icon i_stm_add">Add New SMS Template</a>
		<!-- <a href="#" class="btnGray button i_stm_search icon">Search Contacts</a> -->
	</div>
	<div id="content-header">
		<h1>SMS Template List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox',array(
			'model'=>$model,
		)); ?>
	</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'sms-templates-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'id',
		array(
			'type'  => 'raw',
			'name'  => 'Status',
			'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)',
		),
		'componentType.display_name',
        'name',
        'description',
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/smsTemplates/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:80px'),
        ),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/smsTemplates/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:80px'),
		),
	),
));
