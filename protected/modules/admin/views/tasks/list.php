<?php
	$this->breadcrumbs = array(
		'List' => '',
	);
?>
	<h1>My Tasks</h1>

<?php echo $this->renderPartial('_list', array(
		'dataProvider' => $dataProvider,
		'overdueCount' => $overdueCount,
        'contactId'=> $contactId,
        'hideTaskListCompleteButton' => $hideTaskListCompleteButton,
        'hideTaskListDeleteButton' => $hideTaskListDeleteButton,
	)
);

	// Checks to see if a activity log dialog already exists and creates one only if it doesn't exist.
	Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
	if (!class_exists('ActivityLogDialog', false)) {
		$this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
				'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
				'title' => 'Complete Task & Log Activity',
				'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
			)
		);
	}

	Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
	if (!class_exists('TaskDialogWidget', false)) {
		$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
				'id' => 'task-edit',
				'title' => 'Edit Task',
				'parentModel' => new Contacts,
				'triggerElement' => '.edit-task-button',
			)
		);
	}
?>