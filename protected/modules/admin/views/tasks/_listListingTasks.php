<?php
$this->widget('admin_module.components.StmGridView', array(
    'id'=>'listing-tasks-grid',
    'dataProvider'=>$tasksData,
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Due Date',
            'value'=>'Yii::app()->format->formatDate($data->due_date)."<br />".$data->taskType->name',
            'htmlOptions'=>array('style'=>'width:90px;')
            ),
        array(
            'type'=>'raw',
            'name'=>'Description',
            'value'=>'"<strong><u><a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=\"_blank\">".Yii::app()->format->formatAddress($data->transaction->address, array(\'streetOnly\'=>true))."</a></u></strong><br />".$data->description',
        ),
        array(
            'type'=>'raw',
            'name'=>'Client Info',
            'value'=>'$data->contact->fullName."<br />".Yii::app()->format->formatPrintPhones($data->contact->phones)',
            ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<button class=\"edit-task-button\" data=\"$data->id\" ctid=\"$data->component_type_id\">Edit</button>"',
            'htmlOptions'=>array('style'=>'width:50px')
            ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<button class=\"complete-task-button\" data=\"$data->id\" ctid=\"$data->component_type_id\">Complete</button>"',
            'htmlOptions'=>array('style'=>'width:70px')
            ),
        array(
            'type' => 'raw',
            'name' => '',
            'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->name."/".$data->componentType->name."/".$data->component_id."\" class=\"button\" target=\"_blank\">View</a></div>"
            ',
            'htmlOptions' => array('style' => 'width:60px'),
        ),
    ),
));

// Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
// $this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
//     'id'             => TaskDialogWidget::TASK_DIALOG_ID,
//     'title'          => 'Create New Task',
//     'triggerElement' => '#add-task-button, .edit-task-button, .delete-task-button',
//     'parentModel'    => new Sellers,
// ));