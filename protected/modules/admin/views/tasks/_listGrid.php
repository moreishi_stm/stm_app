<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('leadSearchScript',<<<JS
	 $('#task-grid .task-button-tip').tipsy({live: true, gravity: 's', title: 'data-tooltip'});
JS
, CClientScript::POS_END);

	$this->widget('admin_module.components.StmGridView', array(
			'id' => 'task-grid',
			'dataProvider' => $dataProvider,
			'itemsCssClass' => 'datatables',
			'enableSorting' => false,
			'enablePagination' => true,
			'columns' => array(
				array(
					'header'=>'html',
					'id'=>'task_ids',
					'class'=>'CCheckBoxColumn',
					'selectableRows' => '50',
//					'selectableRows'=>2,
                    'headerHtmlOptions' => array('style'=>(( $_GET["task_types"] == TaskTypes::LETTER ) ?  "" : "display: none;")),
					'value'=>'$data->id',
					'headerTemplate'=>'<label></label>',
					'htmlOptions'=>array('style'=>"width: 20px; ".(( $_GET["task_types"] == TaskTypes::LETTER ) ?  "" : "display: none;"),'class'=>'taskIds'),
				),
				array(
					'type' => 'raw',
					'name' => 'Priority / Overdue',
					'value' => '$data->printPriorityFlag().$data->printOverdueFlag(false)',
					'htmlOptions' => array(
						'class' => 'p-tc',
						'width' => '30px'
					),
				),
//				array(
//					'type' => 'raw',
//					'name' => 'Overdue',
//					'value' => '$data->printOverdueFlag()',
//					'htmlOptions' => array(
//						'class' => 'p-pl5',
//					),
//				),
				array(
					'type' => 'raw',
					'name' => 'Due/Type',
					'value' => 'Yii::app()->format->formatDate($data->due_date)."<br />".$data->taskType->name."<br />(".$data->componentType->display_name.")".(($data->taskRecursion) ? "<div style=\"font-weight:bold; color: blue; text-shadow: none !important; font-style: italic;\">Recurring</div>" : "")',
					'htmlOptions' => array(
						'class' => 'p-tl',
						'width' => '110px'
					),
				),
				array(
					'name' => 'status_ma',
					'value' => '$data->componentStatus',
				),
                array(
                    'type' => 'raw',
                    'name' => 'Name',
                    'value' => '$data->printGridContactInfo()',
                    'htmlOptions' => array(
                        'style' => 'width:200px;'
                    ),
                ),
                array(
                    'type' => 'raw',
                    'name' => 'Description',
                    'value' => 'nl2br($data->description).(($data->operation_manual_id) ? "<br><br><a href=\"/".Yii::app()->controller->module->id."/operationManuals/".$data->operation_manual_id."\" target=\"_blank\">(View Operation Manual)</a>":"").(($data->actionPlanAppliedLu && $data->actionPlanAppliedLu->actionPlanItem->operation_manual_id) ? "<br><br><a href=\"/".Yii::app()->controller->module->id."/operationManuals/".$data->actionPlanAppliedLu->actionPlanItem->operationManual->id."\" target=\"_blank\">(View Operation Manual)</a>":"")',
                ),
//                array(
//                    'name' => '',
//                    'type' => 'raw',
//                    'value' => '($data->task_type_id==TaskTypes::PHONE) ? "<a href=\"#click-to-call-container\" class=\"button show-click-to-call-modal\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\" cid=\"".$data->component_id."\">Call</a>" : ""',
//                    'htmlOptions' => array(
//                        'style' => 'width:45px;'
//                    ),
//                    'visible' => Yii::app()->user->checkAccess('clickToCall')
//                ),
                array(
                    'name' => 'CID',
                    'type' => 'raw',
                    'value' => '$data->component_type_id',
                ),
				array(
					'name' => '',
					'type' => 'raw',
					'value' => '"<a class=\"action-button orange fa fa-pencil task-button-tip edit-task-button\" data-tooltip=\"Edit Task\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\"></a>"',
					'htmlOptions' => array(
						'style' => 'width:45px;'
					),
				),
				array(
					'name' => '',
					'type' => 'raw',
					'value' => '"<a class=\"action-button green fa fa-check task-button-tip complete-task-button\" data-tooltip=\"Complete Task\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\"></a>"',
					'htmlOptions' => array(
						'style' => 'width:45px;'.(($hideTaskListCompleteButton) ? ' display:none;' : '')
					),
				),
                array(
                    'name' => '',
                    'type' => 'raw',
                    'value' => '"<a class=\"action-button green fa fa-plus-circle task-button-tip add-activity-log-button\" data-tooltip=\"Add Activity Log\" data=\"".$data->component_id."\" ctid=\"".$data->component_type_id."\"></a>"',
                    'htmlOptions' => array(
                        'style' => 'width:4px'
                    ),
                ),
				array(
					'name' => '',
					'type' => 'raw',
					'value' => '"<a class=\"action-button red fa fa-times task-button-tip delete-task-button\" data-tooltip=\"Delete Task\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\"></a>"',
					'htmlOptions' => array(
						'style' => 'width:4px;'.(($hideTaskListDeleteButton) ? ' display:none;' : '')
					),
				)
			)
		)
	);