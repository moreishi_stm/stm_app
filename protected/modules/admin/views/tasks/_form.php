<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'activity_task_group_lu'); ?>
		<?php echo $form->textField($model,'activity_task_group_lu'); ?>
		<?php echo $form->error($model,'activity_task_group_lu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'task_type_lu'); ?>
		<?php echo $form->textField($model,'task_type_lu'); ?>
		<?php echo $form->error($model,'task_type_lu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'assigned_to_id'); ?>
		<?php echo $form->textField($model,'assigned_to_id'); ?>
		<?php echo $form->error($model,'assigned_to_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'assigned_by_id'); ?>
		<?php echo $form->textField($model,'assigned_by_id'); ?>
		<?php echo $form->error($model,'assigned_by_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action_plan_id'); ?>
		<?php echo $form->textField($model,'action_plan_id'); ?>
		<?php echo $form->error($model,'action_plan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'record_id'); ?>
		<?php echo $form->textField($model,'record_id'); ?>
		<?php echo $form->error($model,'record_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_priority'); ?>
		<?php echo $form->textField($model,'is_priority'); ?>
		<?php echo $form->error($model,'is_priority'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'due_date'); ?>
		<?php echo $form->textField($model,'due_date'); ?>
		<?php echo $form->error($model,'due_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'complete_date'); ?>
		<?php echo $form->textField($model,'complete_date'); ?>
		<?php echo $form->error($model,'complete_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_deleted'); ?>
		<?php echo $form->textField($model,'is_deleted'); ?>
		<?php echo $form->error($model,'is_deleted'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'added'); ?>
		<?php echo $form->textField($model,'added'); ?>
		<?php echo $form->error($model,'added'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'added_by'); ?>
		<?php echo $form->textField($model,'added_by'); ?>
		<?php echo $form->error($model,'added_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
		<?php echo $form->error($model,'updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->