<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'task_id'); ?>
		<?php echo $form->textField($model,'task_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activity_task_group_lu'); ?>
		<?php echo $form->textField($model,'activity_task_group_lu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'task_type_lu'); ?>
		<?php echo $form->textField($model,'task_type_lu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'assigned_to_id'); ?>
		<?php echo $form->textField($model,'assigned_to_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'assigned_by_id'); ?>
		<?php echo $form->textField($model,'assigned_by_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'action_plan_id'); ?>
		<?php echo $form->textField($model,'action_plan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'record_id'); ?>
		<?php echo $form->textField($model,'record_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_priority'); ?>
		<?php echo $form->textField($model,'is_priority'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'due_date'); ?>
		<?php echo $form->textField($model,'due_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'complete_date'); ?>
		<?php echo $form->textField($model,'complete_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_deleted'); ?>
		<?php echo $form->textField($model,'is_deleted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'added'); ?>
		<?php echo $form->textField($model,'added'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'added_by'); ?>
		<?php echo $form->textField($model,'added_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->