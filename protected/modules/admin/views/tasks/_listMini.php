<?
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('leadSearchScript',<<<JS
	 $('#task-grid .task-button-tip').tipsy({live: true, gravity: 's', title: 'data-tooltip'});
JS
, CClientScript::POS_END);
?>
<div class="button-container">
	<button id="add-task-button" class="text" type="button" data="<?php echo $parentModel->id; ?>"
	        ctid="<?php echo $componentTypeId; ?>"><em class="icon i_stm_add"></em>Add Task
	</button>
    <button id="add-task-button" class="text phone-call-button" type="button" data="<?php echo $parentModel->id; ?>"
            ctid="<?php echo $componentTypeId; ?>"><em class="icon i_stm_add"></em>Add Phone Task
    </button>
</div>
<div class="scroll-content-container">
	<?php
		$this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'task-grid',
				'htmlOptions' => array(
					'class' => 'task-grid'
				),
				'template' => '{items}{pager}',
				'enablePagination' => false,
				'dataProvider' => $dataProvider,
				'itemsCssClass' => 'datatables',
				'columns' => array(
					array(
						'type' => 'raw',
						'name' => 'Due/Assign',
						'value' => '($data->depends_on_id && !$data->isDue) ? "<strong>Not Due</strong>" : Yii::app()->format->formatDate($data->due_date)."<br>".$data->assignedTo->fullName .
						  "<input type=\"hidden\" class=\"task_id\" value=\"".$data->id."\">" .
						  "<input type=\"hidden\" class=\"task_type_id\" value=\"".$data->task_type_id."\">"',
						'htmlOptions' => array(
							'style' => 'width:85px'
						),
					),
					array(
						'name' => 'Type',
						'value' => '$data->printPriorityFlag().$data->taskType->name.(($data->taskRecursion) ? "<div style=\"font-weight:bold; color: blue; text-shadow: none !important; font-style: italic;\">Recurring</div>" : "")',
						'type' => 'raw',
					),
					array(
						'name' => 'Description',
						'value' => '(($data->depends_on_id)? "[Triggered Task]: ".$data->description."<br />[Upon task completion of]: ".$data->dependsOnTask->description: $data->description).(($data->actionPlanItem)? "<br><br>(Action Plan: ".$data->actionPlanItem[0]->actionPlan->name.")": "").(($data->operation_manual_id) ? "<br><br><a href=\"/".Yii::app()->controller->module->id."/operationManuals/".$data->operation_manual_id."\" target=\"_blank\">(View Operation Manual)</a>":"").(($data->actionPlanAppliedLu && $data->actionPlanAppliedLu->actionPlanItem->operation_manual_id) ? "<br><br><a href=\"/".Yii::app()->controller->module->id."/operationManuals/".$data->actionPlanAppliedLu->actionPlanItem->operationManual->id."\" target=\"_blank\">(View Operation Manual)</a>":"")',
						'type' => 'raw',
					),
					array(
						'name' => '',
						'type' => 'raw',
						'value' => '(($data->task_type_id==TaskTypes::AUTO_EMAIL_DRIP)? false : "<a class=\"action-button orange fa fa-pencil task-button-tip edit-task-button\" data-tooltip=\"Edit Task\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\"></a>")',
						'htmlOptions' => array(
							'style' => 'width:45px; text-align: center;'.(($data->task_type_id==TaskTypes::AUTO_EMAIL_DRIP)?' display:hidden;':''),
						),
					),
					array(
						'name' => '',
						'type' => 'raw',
						'value' => '(($data->task_type_id==TaskTypes::AUTO_EMAIL_DRIP)? false : "<a class=\"action-button green fa fa-check task-button-tip complete-task-button\"  data-tooltip=\"Complete Task\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\"></a>")',
						'htmlOptions' => array(
							'style' => 'width:45px; text-align: center;'
						),
					),
					array(
						'name' => '',
						'type' => 'raw',
						'value' => '(($data->task_type_id==TaskTypes::AUTO_EMAIL_DRIP)? false : "<a class=\"action-button red fa fa-times task-button-tip delete-task-button\" data-tooltip=\"Delete Task\" data=\"".$data->id."\" ctid=\"".$data->component_type_id."\"></a>")',
						'htmlOptions' => array(
                            'style' => 'width:45px; text-align: center;'
						),
					)
				)
			)
		);
	?>
</div>