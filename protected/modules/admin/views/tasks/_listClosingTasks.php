<?php
$this->widget('admin_module.components.StmGridView', array(
    'id'=>'closing-tasks-grid',
    'dataProvider'=>$tasksData,
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Due Date',
            'value'=>'Yii::app()->format->formatDate($data->due_date)."<br />".$data->taskType->name',
            ),
        array(
            'type'=>'raw',
            'name'=>'Description',
            'value'=>'"<strong><u><a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=\"_blank\">".$data->contact->fullName."</a></u></strong>"." &nbsp;&nbsp;(".Closings::model()->findByPk($data->component_id)->closingSideName.") ".Yii::app()->format->formatDollars(Closings::model()->findByPk($data->component_id)->price)."<br />".$data->description',
        ),
        array(
            'type'=>'raw',
            'name'=>'Client Info',
            'value'=>'Yii::app()->format->formatPrintEmails($data->transaction->contact->emails)."<br />".Yii::app()->format->formatPrintPhones($data->transaction->contact->phones)',
            ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<button class=\"edit-task-button\" data=\"$data->id\" ctid=\"$data->component_type_id\">Edit</button>"',
            'htmlOptions'=>array('style'=>'width:50px')
            ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<button class=\"complete-task-button\" data=\"$data->id\" ctid=\"$data->component_type_id\">Complete</button>"',
            'htmlOptions'=>array('style'=>'width:70px')
            ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
                "<div><a href=\"/admin/".$data->componentType->name."/".$data->component_id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Details</a></div>"
            ',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
    ),
));

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
    'id' => TaskDialogWidget::TASK_DIALOG_ID,
    'title' => 'Create New Task',
    'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button',
    'parentModel' => new Closings,
));

Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
        'id'             => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
        'title'          => 'Add New Activity Log',
        'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        'parentModel'    => new Closings,
    ));
}