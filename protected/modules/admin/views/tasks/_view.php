<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->task_id), array('view', 'id'=>$data->task_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activity_task_group_lu')); ?>:</b>
	<?php echo CHtml::encode($data->activity_task_group_lu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_type_lu')); ?>:</b>
	<?php echo CHtml::encode($data->task_type_lu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('assigned_to_id')); ?>:</b>
	<?php echo CHtml::encode($data->assigned_to_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('assigned_by_id')); ?>:</b>
	<?php echo CHtml::encode($data->assigned_by_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action_plan_id')); ?>:</b>
	<?php echo CHtml::encode($data->action_plan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('record_id')); ?>:</b>
	<?php echo CHtml::encode($data->record_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('is_priority')); ?>:</b>
	<?php echo CHtml::encode($data->is_priority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('due_date')); ?>:</b>
	<?php echo CHtml::encode($data->due_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('complete_date')); ?>:</b>
	<?php echo CHtml::encode($data->complete_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_deleted')); ?>:</b>
	<?php echo CHtml::encode($data->is_deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('added')); ?>:</b>
	<?php echo CHtml::encode($data->added); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('added_by')); ?>:</b>
	<?php echo CHtml::encode($data->added_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	*/ ?>

</div>