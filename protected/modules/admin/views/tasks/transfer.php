<?php
$this->breadcrumbs = array(
    'Transfer' => '',
);
$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerScript('search', <<<JS
	$('.transfer-task-button').live('click',function(){
        if(confirm('Confirm all tasks transfer from '+ $(this).attr('data-from-name') +' to '+ $(this).attr('data-to-name') +'.')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var fromId = $(this).attr('data-from-id');
			var toId = $(this).attr('data-to-id');
			$.post('/$module/tasks/transfer/',{'fromId':fromId,'toId':toId}, function(data) {
				if(data==null) {
					Message.create("success","Tasks transferred successfully.");
                    $.fn.yiiGridView.update('transfer-tasks-grid', {
                        data: $(this).serialize(),
                        complete: function(jqXHR, status) {
                            if (status=='success'){
                                $('select.transferTo').chosen({allow_single_deselect: true, width: '90%'});
                            }
                        }
                    });
				} else {
    				$("div.loading-container.loading").remove();
					Message.create("error","Error: Tasks did not transfer.");
				}
			},"json");
		}
    });

    var transferDropdown = $('select.transferTo');
    transferDropdown.live('change', function(){
        if($(this).val()!='') {
            $('.transfer-button-container').hide('normal');
            var fromId = $(this).attr('data-id');
            var toId = $(this).val();
            var toName = $(this).find(':selected').text();
            var transferButton = $('#transfer-button-container-'+fromId + ' .transfer-task-button');
            $('#transfer-button-container-'+fromId).show('normal');
            transferButton.attr('data-to-id', toId);
            transferButton.attr('data-to-name', toName);
        }
    });
JS
);
?>
    <style>
        .chzn-results {
            text-shadow: none;
        }
    </style>
	<h1>Transfer Tasks</h1>

<?php
$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select.transferTo','options'=>array('allow_single_deselect'=>true,'width'=>'90%')));

$this->widget('admin_module.components.StmGridView', array(
        'id'            =>'transfer-tasks-grid',
        'template'	   =>'{items}',
        'dataProvider'  =>$dataProvider,
        'extraParams'   =>array('dateRange'=>$dateRange,'contactsList'=>$contactsList),
        'itemsCssClass' =>'datatables',
        'columns' => array(
            array(
                'type'  => 'raw',
                'name'  => 'Name',
                'value' => '"<a href=\"/admin/contacts/".$data->id."\" target=\"_blank\">".$data->fullName."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# Future Tasks',
                'value' => '$data->getCountFutureTasks($this->grid->extraParams["dateRange"])',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# Overdue Tasks',
                'value' => '"<strong>".$data->getCountOverdueTasks($this->grid->extraParams["dateRange"])',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# Total Tasks to Transfer',
                'value' => '$data->getCountIncompleteTasks($this->grid->extraParams["dateRange"])',
            ),//
            array(
                'type' => 'raw',
                'name' => 'Transfer To',
                'value' => 'CHtml::dropDownList("contactId",null, $this->grid->extraParams["contactsList"],$htmlOptions=array("empty"=>"","data-placeholder"=>"Select Transfer To","id"=>"transfer-to-".$data->id, "data-id"=>$data->id, "class"=>"transferTo"))',
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div id=\"transfer-button-container-".$data->id."\" class=\"transfer-button-container\"  style=\"display:none;\">".CHtml::link("Transfer Tasks","javascript:void(0)", $htmlOptions=array("class"=>"button gray icon i_stm_edit grey-button transfer-task-button","data-from-id"=>$data->id,"data-from-name"=>$data->fullName,"data-to-id"=>$data->id,"data-to-name"=>$data->fullName))."</div>"',
                'htmlOptions' => array('style' => 'width:160px'),
            ),
        ),
    ));