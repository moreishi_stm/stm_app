<?php
$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerScript('tasksearch', <<<JS

	$('select#task_types, select#contact_id, select#component_type_id, select#order_by, select#added_by, select#DateRanger_date_preset').change(function(){
        if($('select#DateRanger_date_preset').val() != 'custom') {
            updateTaskGrid($(this).serialize());
        }
	});

	$('select#date_preset').change(function(){
		if($('select#date_preset').val() == 'custom')
			$('#custom-date-container').show('slow');
		else {
			$('#custom-date-container').hide('slow');
			$('#from_date').val('');
			$('#to_date').val('');
		}
	});

	$('.task-list-type-buttons.filter').click(function(){
	    var taskType = $(this).data('id');
	    var ele = $('select#task_types');
        ele.val(taskType);
        ele.trigger("liszt:updated");
        updateTaskGrid(ele.serialize());
	});

	$('.date-ranger-go-button').click(function(){
		updateTaskGrid($('form#task-list-search').serialize());
		return false;
	});

    $('form#task-list-search').submit(function() {
    	updateTaskGrid($('form#task-list-search').serialize());
        return false;
    });

    function updateTaskGrid(serializedData) {
		$.fn.yiiGridView.update('task-grid', {
				data: serializedData,
				complete: function(jqXHR, status) {
					if (status=='success'){
						var html = $.parseHTML(jqXHR.responseText);
						var newOverdueLabel = $('.overdue-task-label', $(html)).html();
						$('.overdue-task-label').html(newOverdueLabel);
					}
				}
		});
    }
    $("#massPrint").on("click",function(){
		var params = $('form#task-list-search').serializeArray();
		params.push({name:"massUpdate",value:1});

		$("#task-grid .taskIds").children("input:checked").each(function(i,e){
		    params.push({name:"taskIds[]",value: $(this).val() });
			console.log($(this).val());
		});



		var url = $('form#task-list-search').attr("action")+"/?";
		start = true;
		$.each(params,function(k,data){
			if(!start){url = url + "&";}
			url = url+ data.name+"="+encodeURIComponent(data.value);
			start = false;
		});


		return window.open(url);

	});

	$("#task_types").on("change",function(el){
		if( $(this).val() == 66 ){
			$("td.taskIds").show();
			return $("#massPrint").show();
		} else{
			$("td.taskIds").hide();
			return $("#massPrint").hide();
		}

	});


JS
);

Yii::app()->clientScript->registerCss('taskCss', <<<CSS
    .task-list-type-buttons {
        padding: 15px;
        font-size: 20px;
        font-weight: bold;
        display: block;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        background-color: #ffeaea;
        border: 2px solid #F9DCDB;
        color: #3f3f3f;
        text-align: center;
    }
    .task-list-type-buttons:hover {
        border-color: #d23f4f;
        text-decoration: none;
    }
    .task-list-type-buttons div{
        font-size: 12px;
    }
    .dialer-button > div {
        margin-bottom: 30px;
    }
    .goal-summary {
        background-color: #77d85c;
        color: white;
        padding: 20px;
    }
    .goal-summary .count {
        font-size: 30px;
    }
    .goal-summary .label {
        font-size: 13px;
    }
    .goal-graph-bg {
        border-radius: 35px;
        width: 100%;
        background: #EFEFEF;
    }
    .goal-graph {
        background-color: #77d85c;
        padding: 13px;
        color: white;
        text-align: center;
        margin-top: 15px;
        font-size: 27px;
        border-top-left-radius: 35px;
        border-bottom-left-radius: 35px;
    }
CSS
);
?>
<script type="text/javascript">

</script>

    <div class="container dialer-button">
        <?if(1): //Yii::app()->user->checkAccess('dialer')?>
            <div class="col-lg-12 no-padding">
                <a href="/<?=Yii::app()->controller->module->id?>/dialer" class="task-list-type-buttons center" data-id="<?=TaskTypes::PHONE?>">
                    <div>Call 3x Faster vs. snail dialing</div>
                    <i class="fa fa-phone"></i>
                    STM Power Dialer
                </a>
            </div>
        <?else:?>
            <a href="javascript:void(0)" class="task-list-type-buttons filter center" data-id="<?=TaskTypes::PHONE?>"><div>View Only</div>Phone Tasks</a>
        <?endif;?>
        <? /*
        <a href="javascript:void(0)" class="task-list-type-buttons filter" data-id="<?=TaskTypes::TODO?>"><div>View Only</div>To Dos</a>
        <a href="javascript:void(0)" class="task-list-type-buttons filter" data-id="<?=TaskTypes::EMAIL_MANUAL?>"><div>View Only</div>Emails Tasks</a>
        <a href="javascript:void(0)" class="task-list-type-buttons filter" data-id="<?=TaskTypes::TEXT_MESSAGE?>"><div>View Only</div>SMS/Text Tasks</a>
        */ ?>
    </div>
    <? if(0 && Yii::app()->user->settings->primary_goal_id): ?>
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 goal-summary">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                <i class="fa fa-signal fa-5x"></i>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-tr">
                <div class="count">
                    1 of 40
                </div>
                <div class="label">
                    Met Appointment Goal
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="vertical-align: middle;">
            <div class="goal-graph-bg">
                <div class="goal-graph" style="width: 60%;">
                    50%
                </div>
            </div>
        </div>
    </div>
    <? endif; ?>

	<div class="g12 p-fr">
		<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'task-list-search',
				'action' => Yii::app()->createUrl($this->route),
				'method' => 'get',
			)
		);

            $this->widget('admin_module.extensions.EChosen.EChosen', array(
                    'target' => 'select#task_types, select#component_type_id, select#contact_id, select#order_by, select#added_by, select#letter_template_id',
                    'options' => array('allow_single_deselect' => true)
                )
            );

            ?>
            <?php
			$typesList = TaskTypes::model()->byComponentType(ComponentTypes::CONTACTS)->findAll();
			//idk why it doesnt work with the query?
			//$typesList[] = TaskTypes::model()->findByPk(TaskTypes::LETTER);

            $listData = CHtml::listData($typesList, 'id', 'name');
            $listData = CMap::mergeArray(array('' => 'All TaskTypes'), $listData);
            echo CHtml::dropDownList('task_types', null, $listData, $htmlOptions = array(
                    'class' => 'p-fl p-mh8',
                    'style' => 'font-size:13px;'
                )
            );
			//die("sss<pre>".print_r($listData,1));


            if(Yii::app()->user->checkAccess('transactions')) {
                $listData = CHtml::listData(ComponentTypes::model()->byIds(array(ComponentTypes::CONTACTS, ComponentTypes::BUYERS, ComponentTypes::SELLERS, ComponentTypes::CLOSINGS, ComponentTypes::RECRUITS, ComponentTypes::REFERRALS, ComponentTypes::PROJECTS))->findAll(), 'id', 'display_name');
            }
            // team leader package
            else {
                $listData = CHtml::listData(ComponentTypes::model()->byIds(array(ComponentTypes::CONTACTS, ComponentTypes::RECRUITS, ComponentTypes::PROJECTS))->findAll(), 'id', 'display_name');
            }


            $listData = CMap::mergeArray(array('' => 'All Categories'), $listData);
            echo CHtml::dropDownList('component_type_id', null, $listData, $htmlOptions = array(
                    'class' => 'p-fl p-mh8',
                    'style' => 'font-size:13px; width: 150px;'
                )
            );

            $listData = CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins()->findAll(), 'id', 'fullPhoneticName');
            echo CHtml::dropDownList('contact_id', Yii::app()->user->id, $listData, $htmlOptions = array(
                    'class' => 'p-fl p-mh8',
                    'style' => 'font-size:13px; width: 200px;'
            ));

            echo CHtml::dropDownList('added_by', null, $listData, $htmlOptions = array(
                    'class' => 'p-fl p-mh8',
                    'style' => 'font-size:13px; width: 200px;',
                    'data-placeholder' => 'Assigned by',
                    'empty' => '',
                ));

            echo CHtml::dropDownList('order_by', null, array('due_date_asc'=>'Order by Due Date','component'=>'Order by Client/Project Name'), $htmlOptions = array(
                    'class' => 'p-fl p-mh8',
                    'style' => 'font-size:13px; width: 200px;'
                ));
            ?>
            <label class="overdue-task-label" style="margin-left: 15px;"><?echo ($overdueCount)? $overdueCount. ' Overdue Tasks' : '';?></label>

            <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array(
                                   'fromDateLabelSelector' => '',
                                   'toDateLabelSelector'   => '',
                                   'gridName'              => 'tasks-grid',
                                   'isForm'                => false,
                                   'formSelector'          => '#task-list-search',
                                   'updateNonGridElement'  =>'.overdue-task-label',
                                   'custom_from_date'      => (($_COOKIE['TaskDateRanger']['custom_from_date'])? $_COOKIE['TaskDateRanger']['custom_from_date'] : ''),
                                   'custom_to_date'        => (($_COOKIE['TaskDateRanger']['custom_to_date'])? $_COOKIE['TaskDateRanger']['custom_to_date'] : ''),
                                   'defaultSelect'         => (($_COOKIE['TaskDateRanger']['date_preset'])? $_COOKIE['TaskDateRanger']['date_preset'] : 'up_to_next_1_week'),
                                   'datePresetList'        => array('all',
                                                                    'up_to_today',
                                                                    'up_to_next_1_week',
                                                                    'up_to_next_2_weeks',
                                                                    'up_to_this_month',
                                                                    'custom'
                                   ),
                                   'container'             => array('tag'         => 'div',
                                                                    'htmlOptions' => array('class' => 'p-fr')
                                   )
                                   ));
            ?>
		<?php $this->endWidget(); ?>
	</div>
	<div class="g12">
		<button id="massPrint" type="button" style="display:none;">Mass Print</button>
	</div>
<?php $this->renderPartial('../tasks/_listGrid',array(
                                                    'dataProvider'=>$dataProvider,
                                                    'hideTaskListCompleteButton' => $hideTaskListCompleteButton,
                                                    'hideTaskListDeleteButton' => $hideTaskListDeleteButton,
                                                ));
?>
<div></div>
<?
    Yii::import('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget');
    if (!class_exists('ClickToCallDialogWidget', false)) {
        $this->widget('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget', array(
                'id' => ClickToCallDialogWidget::CLICK_TO_CALL_DIALOG_ID,
                'title' => 'Click to Call',
                'triggerElement' => '.click-to-call-button',
            )
        );
    }

	Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
	if (!class_exists('TaskDialogWidget', false)) {
		$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
				'id' => TaskDialogWidget::TASK_DIALOG_ID,
				'title' => 'Create New Task',
				'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button, .delete-task-button',
				'parentModel' => new Contacts,
			)
		);
	}

	Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
	if (!class_exists('ActivityLogDialog', false)) {
		$this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
				'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
				'title' => 'Add New Activity Log',
				'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
				'parentModel' => new Contacts,
			)
		);
	}