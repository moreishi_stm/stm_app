<style type='text/css'>
    .yui3-datatable-paginator {
        background: none;
    }
</style>

<? $this->breadcrumbs = array('Import' => '') ?>
<h1>Import Contacts</h1>

<section style='max-width: 1200px; margin: 25px auto;'>

    <h3>Delimiter</h3>
    <div style='text-align: center; margin: 25px 0px;'>
        <label for='field-delimiter'>Field Delimiter</label>
        <select id='field-delimiter' style='width: 250px;'>
            <option value="1">Comma</option>
            <option value='2'>Tab</option>
        </select>
    </div>

<!--    <h3>Contacts CSV Data</h3>-->
<!--    <textarea id='contacts-input' placeholder='Paste your contacts CSV data here'></textarea>-->
<!--    <button type='button' id="contacts-btn-load">Preview Raw Data</button>-->

<!--    <div style='font-size: 2em; text-align: center; margin-bottom: 20px;'>- Or -</div>-->

    <form action='/admin/contacts/import' method='POST' enctype='multipart/form-data' id='contacts-form-upload' style='text-align: center;'>
        <select name="ignoreFirstRow">
            <option value="1">Yes, Ignore First Row/Header</option>
            <option value="0">No, Include First Row</option>
        </select>
        <input type='file' name='contacts-file' id='contacts-file' style='width: 400px;'>
        <input type='hidden' name='method' id='contacts-method' value='process-file'>
        <button type='submit' id='contacts-btn-upload'>Preview File Data</button>
    </form>

    <hr style='margin: 25px 0px;'>

    <h3>Contacts Import Preview</h3>
    <div id='contacts-datatable'></div>

    <button type='button' id='contacts-btn-continue'>Continue and Import</button>
</section>

<!-- JavaScript -->
<script src="http://yui.yahooapis.com/3.17.2/build/yui/yui-min.js"></script>
<script type="text/javascript">

    // Use YUI things
    YUI().use('node', 'datatable', 'datatable-message', 'datatable-paginator', 'datasource-local', 'datasource-jsonschema', 'datatable-datasource', 'io', 'json', function(Y) {

        // Hide continue button
        Y.one('#contacts-btn-continue').hide();

        // Maintain scope
        var dataSource;
        var dataTable;

        // Loading container functionality
        var loadingContainer = {
            show: function() {
                Y.one('body').appendChild(Y.Node.create('<div id="contacts-loading-container" class="loading-container loading"><em></em></div>'));
            },
            hide: function() {
                Y.all('.loading-container').remove();
            }
        };

        // Create table function
        var createTable = function(rawData) {

            // Make sure raw data is set
            rawData = rawData || [];
//console.log(rawData);
            // Destroy data table if we have one already
            if(dataTable) {
                dataTable.destroy();
            }

            // Destroy data source if we have one already
            if(dataSource) {
                dataSource.destroy();
            }

            // Show the continue button
            Y.one('#contacts-btn-continue').show();

            // Create a datasource
            dataSource = new Y.DataSource.Local({
                source: rawData
            });

            //@todo: ******** THIS BREAKS IF THE DATA SET HAS A COMMA IN IT + FOR SOME REASON THE file_get_content()  is ignoring new line returns ***********

            // Add a schema to the datasource
            dataSource.plug(Y.Plugin.DataSourceJSONSchema, {
                schema: {
//                    resultDelimiter: "\n",
//                    resultListLocator: "data",
//                    fieldDelimiter: Y.one('#field-delimiter').get('value') == 1 ? ',' : "\t",
                    resultFields: [
                        'first_name',
                        'last_name',
                        'phone_1',
                        'phone_2',
                        'phone_3',
                        'phone_4',
                        'email',
                        'email_2',
                        'email_3',
                        'email_4',
                        'address',
                        'city',
                        'state',
                        'zip',
                        'source',
                        'contactTags',
                        'notes',
                        'added'
                    ]
                }
            });

            // Create datatable
            dataTable = new Y.DataTable({
                columns: [
                    {key: 'first_name', label: 'First Name'},
                    {key: 'last_name', label: 'Last Name'},
                    {key: 'phone_1', label: 'Phone 1'},
                    {key: 'phone_2', label: 'Phone 2'},
                    {key: 'phone_3', label: 'Phone 3'},
                    {key: 'phone_4', label: 'Phone 4'},
                    {key: 'email', label: 'Email'},
                    {key: 'email_2', label: 'Email 2'},
                    {key: 'email_3', label: 'Email 3'},
                    {key: 'email_4', label: 'Email 4'},
                    {key: 'address', label: 'Address'},
                    {key: 'city', label: 'City'},
                    {key: 'state', label: 'State'},
                    {key: 'zip', label: 'Zip'},
                    {key: 'source', label: 'Source'},
                    {key: 'contactTags', label: 'Contact Tags'},
                    {key: 'notes', label: 'Notes'},
                    {key: 'added', label: 'Added'}
                ],
                sortable: true,
                rowsPerPage: 50,
                paginatorLocation: ['footer']
            });

            // Configure a message for no data
            dataTable.set('strings.emptyMessage',"No contacts could be read from data.");

            // Add the datasource to the datatable
            dataTable.plug(Y.Plugin.DataTableDataSource, {
                datasource: dataSource
            });

            // Render datatable and load data
            dataTable.render('#contacts-datatable');
            dataTable.datasource.load();
        };

        // Handle load button event
//        Y.one('#contacts-btn-load').on('click', function() {
//            createTable(Y.one('#contacts-input').get('value'));
//        });

        // Create the table initially for display purposes
        createTable();

        // Bind event for continue and import button
        Y.one('#contacts-btn-continue').on('click', function() {

            // Get data from table
            var data = dataTable.get('data').toJSON();

            // Make sure we have data to POST
            if(!data.length) {
                return;
            }

            // Make AJAX call to POST data
            Y.io('/admin/contacts/import', {
                method: 'POST',
                timeout: 360000,
                data: {
                    contacts: Y.JSON.stringify(data),
                    method: 'import'
                },
                on: {
                    start: function() {
                        loadingContainer.show();
                    },
                    end: function() {
                        loadingContainer.hide();
                    },
                    success: function(id, o) {

                        // Attempt to parse JSON response
                        try {
                            var responseData = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Unable to read response from server.');
                            return;
                        }

                        // Handle different types of responses
                        switch(responseData.status) {
                            case 'success':
                                alert('Import completed successfully');
                                //clear data table
                                createTable();
                            break;
                            case 'error':
                                alert('An error occurred: ' + responseData.message);
                            break;
                        }
                    },
                    failure: function() {
                        // Display message of failure
                        alert('An unknown error has occurred, please check data and try again.');
                    }
                }
            });
        });

        // Bind upload event
        Y.one('#contacts-form-upload').on('submit', function(e) {

            // Stop the default submit action
            e.preventDefault();

            // Make the call
            Y.io('/admin/contacts/import', {
                method: 'POST',
                timeout: 180000, //milliseconds
                form: {
                    id: 'contacts-form-upload',
                    upload: true
                },
                on: {
                    start: function(id, o) {
                        loadingContainer.show();
                    },
                    end: function(id, o) {
                        loadingContainer.hide();
                    },
                    complete: function(id, o) {
                        // Attempt to parse JSON response
                        try {
                            var responseData = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Unable to read response from server. There may be some invalid characters or column format.');
                            return;
                        }
                        // Handle different types of responses
                        switch(responseData.status) {
                            case 'success':
                                createTable(responseData.data);
                            break;
                            case 'error':
                                alert('An error occurred: ' + responseData.message);
                            break;
                        }
                    }
                }
            });
        });
    });
</script>