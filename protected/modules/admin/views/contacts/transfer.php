<?php
$this->breadcrumbs = array(
    'Transfer' => '',
);
$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerScript('transferLeadsScript', <<<JS

	$('.transfer-button').live('click',function(){
        var fromId = $(this).data('from-id');
        if($('#transfer-status-' + fromId).val() =='') {
            alert('Please select a Lead Status.');
            return false;
	    }

        if($('#transfer-to-' + fromId).val() == '') {
            alert('Please select user to Transfer To.');
            return false;
	    }

        if(confirm('Confirm all Assignments transfer from '+ $(this).data('from-name') +' to '+ $('#transfer-button-'+fromId).attr('data-to-name') +'.')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var toId = $(this).attr('data-to-id');
			var statusId = $(this).attr('data-status-id');
			$.post('/$module/contacts/transfer/',{'fromId':fromId,'toId':toId ,'transactionStatusId': statusId}, function(data) {
				if(data==null) {
					Message.create("success","Leads transferred successfully.");
                    $.fn.yiiGridView.update('transfer-leads-grid', {
                        data: $(this).serialize(),
                        complete: function(jqXHR, status) {
                            if (status=='success'){
                                $('select.transferTo').chosen({allow_single_deselect: true, width: '90%'});
                                $('select.transferStatusId').chosen({allow_single_deselect: true, width: '90%'});
                            }
                        }
                    });
				} else {
    				$("div.loading-container.loading").remove();
					Message.create("error","Error: Leads did not transfer.");
				}
			},"json");
		}
    });

    $('select.transferTo, select.transferStatusId').live('change', function(){
        if($(this).val()!='') {
            $('.transfer-button-container').hide('normal');
            var fromId = $(this).data('id');
            var toId = $(this).val();
            var transferToElement = $('#transfer-to-' + fromId);
            var transferStatusElement = $('#transfer-status-' + fromId);
            var toName = transferToElement.find(':selected').text();
            var statusId = transferStatusElement.val();

            if(transferStatusElement.val() !='' && transferToElement.val() != '') {
                var transferButton = $('#transfer-button-container-'+fromId + ' .transfer-button');
                $('#transfer-button-container-'+fromId).show('normal');
                transferButton.attr('data-to-id', toId);
                transferButton.attr('data-to-name', toName);
                transferButton.attr('data-status-id', statusId);
            }
            else {
                $('#transfer-button-container-'+fromId).hide('normal');
            }
        }
    });
JS
);
?>
    <style>
        .chzn-results {
            text-shadow: none;
        }
    </style>
	<h1>Transfer Leads</h1>

<?php
$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select.transferTo, select.transferStatusId','options'=>array('allow_single_deselect'=>true,'width'=>'90%')));

$this->widget('admin_module.components.StmGridView', array(
        'id'            =>'transfer-leads-grid',
        'template'	   =>'{items}',
        'dataProvider'  =>$dataProvider,
        'extraParams'   =>array('dateRange'=>$dateRange,'contactsList'=>$contactsList, 'nurtureListData'=>$nurtureListData),
        'itemsCssClass' =>'datatables',
        'columns' => array(
            array(
                'type'  => 'raw',
                'name'  => 'Name',
                'value' => '"<a href=\"/admin/contacts/leads/type/1/myLeads/".$data->id."\" target=\"_blank\">".$data->fullName."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# A Leads',
                'value' => '"<a href=\"/admin/contacts/leads/type/1/transactionStatus/".TransactionStatus::NEW_LEAD_ID."/myLeads/".$data->id."\" target=\"_blank\">".Yii::app()->controller->action->getLeadData($data->id, TransactionStatus::NEW_LEAD_ID)."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# B Leads',
                'value' => '"<a href=\"/admin/contacts/leads/type/1/transactionStatus/".TransactionStatus::B_LEAD_ID."/myLeads/".$data->id."\" target=\"_blank\">".Yii::app()->controller->action->getLeadData($data->id, TransactionStatus::B_LEAD_ID)."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# C Leads',
                'value' => '"<a href=\"/admin/contacts/leads/type/1/transactionStatus/".TransactionStatus::C_LEAD_ID."/myLeads/".$data->id."\" target=\"_blank\">".Yii::app()->controller->action->getLeadData($data->id, TransactionStatus::C_LEAD_ID)."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# D Leads',
                'value' => '"<a href=\"/admin/contacts/leads/type/1/transactionStatus/".TransactionStatus::D_NURTURING_SPOKE_TO_ID."/myLeads/".$data->id."\" target=\"_blank\">".Yii::app()->controller->action->getLeadData($data->id, TransactionStatus::D_NURTURING_SPOKE_TO_ID)."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => '# E Leads',
                'value' => '"<a href=\"/admin/contacts/leads/type/1/transactionStatus/".TransactionStatus::NURTURING_ID."/myLeads/".$data->id."\" target=\"_blank\">".Yii::app()->controller->action->getLeadData($data->id, TransactionStatus::NURTURING_ID)."</a>"',
            ),
            array(
                'type'  => 'raw',
                'name'  => 'Lead Status',
                'value' => 'CHtml::dropDownList("transactionStatusId",null, $this->grid->extraParams["nurtureListData"], $htmlOptions=array("empty"=>"","data-placeholder"=>"Select Transfer Status","id"=>"transfer-status-".$data->id, "data-id"=>$data->id, "class"=>"transferStatusId"))',
            ),
            array(
                'type' => 'raw',
                'name' => 'Transfer To',
                'value' => 'CHtml::dropDownList("contactId",null, $this->grid->extraParams["contactsList"],$htmlOptions=array("empty"=>"","data-placeholder"=>"Select Transfer To","id"=>"transfer-to-".$data->id, "data-id"=>$data->id, "class"=>"transferTo"))',
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div id=\"transfer-button-container-".$data->id."\" class=\"transfer-button-container\"  style=\"display:none;\">".CHtml::link("Transfer Tasks","javascript:void(0)", $htmlOptions=array("id"=>"transfer-button-".$data->id,"class"=>"button gray icon i_stm_edit grey-button transfer-button","data-from-id"=>$data->id,"data-from-name"=>$data->fullName,"data-to-id"=>$data->id,"data-to-name"=>$data->fullName, "data-status-id"=>""))."</div>"',
                'htmlOptions' => array('style' => 'width:160px'),
            ),
        ),
    ));