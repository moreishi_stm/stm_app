<?php
	$this->breadcrumbs = array(
		'Keep In Touch' => array('/'.$this->module->name.'/contacts'),
	);
Yii::app()->clientScript->registerScript('keepInTouchList', <<<JS
$('.complete-button').live('click',function() {
        $.fn.yiiGridView.update('activity-log-grid', { data: $(this).serialize() });
        return false;
    });
JS
);
?>
<h1>Keep In Touch</h1>
<h3>Set It & Don't Forget It</h3>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBoxKeepInTouch', array(
				'model' => $model,
			)
		); ?>
	</div>

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'activity-log-grid',
        'template'=>'{summary}{items}{summary}{pager}',
		'dataProvider' => $dataProvider,
        'enableSorting'=>true,
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Contact Info',
                'value' => '"<a href=\"/admin/contacts/".$data->id."\" target=\"_blank\">".$data->first_name.(($data->spouse_first_name)? " & ".$data->spouse_first_name."": "")." ".$data->last_name."</a><br>".Yii::app()->format->formatPrintPhones($data->phones).((Yii::app()->format->formatPrintPhones($data->phones))? "<br>":"").Yii::app()->format->formatPrintEmails($data->emails)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Closed Transactions',
                'value' => 'Yii::app()->controller->action->printClosings($data)',
            ),
			array(
				'type' => 'raw',
				'name' => 'Tags',
				'value' => 'Yii::app()->format->formatCommaDelimited($data->types,"name")',
			),
            array(
                'type' => 'raw',
                'name' => 'Source',
                'value' => '$data->source->name',
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Spoke To',
                'value' => 'Yii::app()->format->formatDays($data->lastSpokeDate(), array("defaultValue"=>"<div style=\"color: #D20000; font-weight: bold;\">NONE</div>"))',
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Called',
                'value' => 'Yii::app()->format->formatDays($data->lastCalledDate(), array("defaultValue"=>"<div style=\"color: #D20000; font-weight: bold;\">NONE</div>"))',
            ),
            array(
                'name' => '',
                'type' => 'raw',
                'value' => '"<button class=\"add-activity-log-button lead-gen-follow-up-phone-call-button\" data=\"".$data->id."\" ctid=\"".ComponentTypes::CONTACTS."\" data-ttid=\"".TaskTypes::PHONE."\">Add Call</button>"',
                'htmlOptions' => array(
                    'style' => 'width:85px'
                ),
            ),
            array(
                'name' => '',
                'type' => 'raw',
                'value' => '"<button class=\"add-task-button\" data=\"".$data->id."\" ctid=\"".ComponentTypes::CONTACTS."\">Add Task</button>"',
                'htmlOptions' => array(
                    'style' => 'width:85px'
                ),
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
		),
	)
);

// Checks to see if a activity log dialog already exists and creates one only if it doesn't exist.
Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
            'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
            'title' => 'Complete Task & Log Activity',
            'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        )
    );
}

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
if (!class_exists('TaskDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
            'id' => 'task-edit',
            'title' => 'Edit Task',
            'parentModel' => new Contacts,
            'triggerElement' => '.add-task-button',
        )
    );
}
