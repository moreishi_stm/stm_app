<?php
	$this->breadcrumbs = array(
		'List' => array('/'.$this->module->name.'/contacts'),
	);
?>
	<div id="listview-actions">
		<a href="/<?php echo $this->module->name;?>/contacts/add" class="button gray icon i_stm_add">Add New Contacts</a>
	</div>
	<div id="content-header">
		<h1>Contacts List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox', array(
				'model' => $model,
			)
		); ?>
	</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'contact-grid',
        'template'=>'{summary}{items}{summary}{pager}',
		'dataProvider' => $DataProvider,
        'enableSorting'=>true,
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'status',
                'value' => 'StmFormHelper::getStatusBooleanName($data->contact_status_ma, "Trash")',
            ),
			'first_name',
            'last_name',
            'spouse_first_name',
            'spouse_last_name',
            array(
                'type' => 'raw',
                'name' => 'Contact Info',
                'value' => 'Yii::app()->controller->action->printContactInfo($data)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Seller/Buyer Leads',
                'value' => 'Yii::app()->controller->action->printContactLeads($data)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Special Dates & Relationships',
                'value' => 'Yii::app()->controller->action->printSpecialDatesRelationships($data)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Spoke',
                'value' => 'Yii::app()->format->formatDateTimeDays($data->lastSpokeDate(), array("break"=>true))',
            ),
			array(
				'type' => 'raw',
				'name' => 'Tags',
				'value' => 'Yii::app()->format->formatCommaDelimited($data->types,"name")',
			),
            array(
                'type' => 'raw',
                'name' => 'Source',
                'value' => '$data->source->name."<br />Added: ".Yii::app()->format->formatDate($data->added)',
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
				'htmlOptions' => array('style' => 'width:80px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
				'htmlOptions' => array('style' => 'width:80px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a class=\"button gray icon i_stm_add grey-button moveToAccount\" data-contact-id=\"$data->id\">Move To New Account</a></div>"',
				'htmlOptions' => array('style' => 'width:80px'),
                'visible' => $isMultiAccountOwner
			),
		),
	)
);

$this->widget('admin_widgets.DialogWidget.MoveContactsAccountDialogWidget.MoveContactsAccountDialogWidget', array(
		'id' => 'move-contact-to-account-dialog',
		'parentModel' => $model,
		'title' => 'Move Contact To Account',
		'triggerElement' => '.moveToAccount',
	)
);
