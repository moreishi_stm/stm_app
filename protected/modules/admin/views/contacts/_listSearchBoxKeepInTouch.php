<?php
    $module = Yii::app()->controller->module->id;
	Yii::app()->clientScript->registerScript('search', <<<JS
    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('activity-log-grid', { data: $(this).serialize() });
        return false;
    });
JS
);

	$form = $this->beginWidget('CActiveForm', array(
			'action' => Yii::app()->createUrl($this->route),
			'method' => 'get',
			'id' => 'contact-list-search',
		)
	); ?>
<div class="g2"></div>
<div class="g6">
	<label class="g3">Contact Types:</label>
		<span class="g9"><?php echo $form->dropDownList($model->contactTypeLu, 'idCollection', CHtml::listData(ContactTypes::model()->findAll(array('condition'=>'follow_up_days>=1')), 'id', 'name'), array(
					'empty' => '',
					'class' => 'chzn-select g12',
//					'multiple' => 'multiple',
					'data-placeholder' => 'Select One'
					)
			); ?>
		</span>
	<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ContactTypeLu_idCollection')); ?>
</div>
<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('Generate Call List', array('class' => 'button')); ?></div>
<?php $this->endWidget(); ?>
