<?php
    $module = Yii::app()->controller->module->id;
	Yii::app()->clientScript->registerScript('search', <<<JS
    $('form#special-dates-list-search').submit(function() {
        $.fn.yiiGridView.update('contact-grid', { data: $(this).serialize() });
        $.fn.yiiGridView.update('work-anniversary-grid', { data: $(this).serialize() });
        return false;
    });
JS
);

	$form = $this->beginWidget('CActiveForm', array(
			'action' => Yii::app()->createUrl($this->route),
			'method' => 'get',
			'id' => 'special-dates-list-search',
		)
	); ?>
<!--<div class="g4">-->
<!--	<label class="g3">Name:</label>-->
<!--	<span class="g4">--><?php //echo $form->textField($model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')); ?><!--</span>-->
<!--	<span class="g4 p-ml10">--><?php //echo $form->textField($model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')); ?><!--</span>-->
<!--</div>-->
<!--<div class="g3">-->
<!--	<label class="g3">Email:</label>-->
<!--	<span class="g9">--><?php //echo $form->textField($model->emails, 'email', $htmlOptions = array('placeholder' => 'Email')); ?><!--</span>-->
<!--</div>-->
<!--<div class="g3">-->
<!--	<label class="g3">Phone:</label>-->
<!--        <span class="g9">-->
<!--          --><?php //$this->widget('StmMaskedTextField', array(
//		          'model' => $model->phones,
//		          'attribute' => 'phone',
//		          'mask' => '(999) 999-9999',
//		          //Makes the full 10 digit phone number optional (enables partial search)
//		          'htmlOptions' => array(
//			          'placeholder' => '(999) 123-1234',
//			          'class' => 'phoneField'
//		          )
//	          )
//          ); ?>
<!--        </span>-->
<!--</div>-->
        <?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'contact-grid',
                                                                                           'updateNonGridElement' => '#work-anniversary-grid',
                                                                                           'datePresetList'        => array('today',
                                                                                                                            'this_week',
                                                                                                                            'this_month',
                                                                                                                            'last_1_week',
                                                                                                                            'last_2_weeks',
                                                                                                                            'last_30_days',
                                                                                                                            'last_month',
                                                                                                                            'this_year',
                                                                                                                            'custom'
                                                                                           ),

            )); ?>
<!--            --><?php //echo CHtml::dropDownList('specialDateRange', null, array(0=>'Today',7=>'1 Week',14=>'2 Weeks',30=>'1 Month',60=>'2 Months'), array(
//					'class' => 'g12',
//				)
//			); ?>
<!--	--><?php //$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ContactTypeLu_idCollection')); ?>
<!--<div class="g2 submit" style="text-align:center">--><?php //echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?><!--</div>-->
<!--<div class="g3">-->
<!--	<label class="g4">Sources:</label>-->
<!--		<span class="g8">--><?php //echo $form->dropDownList($model->source, 'idCollection', CHtml::listData(Sources::model()->findAll(), 'id', 'name'), array(
//					'empty' => 'All',
//					'class' => 'chzn-select g12',
//					'multiple' => 'multiple',
//					'data-placeholder' => 'Sources'
//					)
//			); ?>
<!--		</span>-->
<!--	--><?php //$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Sources_idCollection')); ?>
<!--</div>-->
<!--<div class="g3">-->
<!--    <label class="g4">Status:</label>-->
<!--		<span class="g8">--><?php //echo $form->dropDownList($model, 'contact_status_ma', StmFormHelper::getStatusBooleanList(true,'Trash'), array('class' => 'g12')); ?><!--</span>-->
<!--</div>-->
<!--<div class="g3">-->
<!--    <div class="g4 text-right"><label>Assigned to:</label></div>-->
<!--    <div class="g7">-->
<!--        --><?php //echo $form->dropDownList(
//            $model, 'assignmentId',
//            CHtml::listData(
//                Contacts::model()->byActiveAdmins(false, $model->assignmentId)
//                    ->byAssignmentComponentType(ComponentTypes::BUYERS)->orderByName()->findAll(), 'id',
//                'fullName'
//            ), $htmlOptions = array('empty' => '')
//        );
//
//        $this->widget(
//            'admin_module.extensions.EChosen.EChosen', array(
//                'target'  => 'select#Buyers_assignmentId',
//                'options' => array(
//                    'allow_single_deselect' => true,
//                    'width'                 => '84%'
//                )
//            )
//        );
//        ?>
<!--    </div>-->
<!--</div>-->
<!--<div class="g1"></div>-->
<!--<div class="g4">-->
<!--    <div class="g3 text-right"><label>Sort:</label></div>-->
<!--    <div class="g3">-->
<!--        --><?php //echo $form->dropDownList(
//            $model, 'dateSearchFilter', $model->dateSearchTypes, $htmlOptions = array(
//                'empty' => '',
//                'class'=>'g12',
//            )
//        ); ?>
<!--    </div>-->
<!--    <div class="g6">-->
<!--        --><?php //$this->widget(
//            'zii.widgets.jui.CJuiDatePicker', array(
//                'model'       => $model,
//                'attribute'   => 'fromSearchDate',
//                // name of post parameter
//                'options'     => array(
//                    'showAnim'       => 'fold',
//                    'dateFormat'     => 'mm/dd/yy',
//                    'numberOfMonths' => 2,
//                ),
//                'htmlOptions' => array(
//                    'style' => 'width: 80px; float: left; font-size: 12px;',
//                ),
//            )
//        );
//        ?>
<!--        <label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>-->
<!--        --><?php //$this->widget(
//            'zii.widgets.jui.CJuiDatePicker', array(
//                'model'       => $model,
//                'attribute'   => 'toSearchDate',
//                // name of post parameter
//                'options'     => array(
//                    'showAnim'       => 'fold',
//                    'dateFormat'     => 'mm/dd/yy',
//                    'numberOfMonths' => 2,
//                ),
//                'htmlOptions' => array(
//                    'style' => 'width: 80px; float: left; font-size: 12px;',
//                ),
//            )
//        );
//        ?>
<!--    </div>-->
<!--</div>-->

<?php $this->endWidget(); ?>
