<?php
	$this->breadcrumbs = array(
		'Add ' . $componentTitle => '',
	);
?>
<?php
	//	if(!$ContactToAddValidation)
	$this->renderPartial('_addForm', array(
			'model' => $model,
			'ContactToAdd' => $ContactToAdd,
            'componentTypeId' => $componentTypeId,
			'componentTitle' => $componentTitle,
			'DataProvider' => $DataProvider
		)
	);
?>
	<div class="loading-container"><em></em></div>
<?php $this->renderPartial('_addList', array(
		'model' => $model,
		'ContactToAdd' => $ContactToAdd,
		'ContactToAddValidation' => $ContactToAddValidation,
		'componentName' => $componentName,
		'componentTitle' => $componentTitle,
		'componentTypeId' => $componentTypeId,
		'exactEmailMatch' => $exactEmailMatch,
        'exactLastNamePhoneMatch' => $exactLastNamePhoneMatch,
		'DataProvider' => $DataProvider
	)
); ?>