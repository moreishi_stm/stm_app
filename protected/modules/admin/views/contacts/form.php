<?php Yii::app()->clientScript->registerScript('contacts-form', <<<JS
		$("#showSpouseLastName").change(function(){
			if ($(this).is(':checked')) {
                $('#Contacts_spouse_last_name').attr('disabled',false);
		    } else {
                $('#Contacts_spouse_last_name').val('').attr('disabled',true);
		    }
		});
		$("#Contacts_spouse_first_name").change(function(){
			if ($(this).val() !== '') {
                $('#Contacts_spouse_last_name').attr('placeholder',$('#Contacts_last_name').val());
		    } else {
                $('#Contacts_spouse_last_name').attr('placeholder', 'Spouse Last Name');
		    }
		});
JS
);

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'contact-form',
	'enableAjaxValidation' => true,
    'clientOptions'=>array(
        'beforeValidate'=>'js:function(form) {
            $(".email-address").removeClass("error");
        }',
    ),
    'htmlOptions'=>array(
        'autocomplete'=>'off',
    ),
)); ?>
<style>
	table {
		border-collapse: collapse;
		border-spacing: 0;
	}
	td,
	th {
		padding: 0;
	}


	table {
		background-color: transparent;
	}
	caption {
		padding-top: 8px;
		padding-bottom: 8px;
		color: #777777;
		text-align: left;
	}
	th {
		text-align: left;
	}
	.table {
		width: 100%;
		max-width: 100%;
		margin-bottom: 20px;
	}
	.table > thead > tr > th,
	.table > tbody > tr > th,
	.table > tfoot > tr > th,
	.table > thead > tr > td,
	.table > tbody > tr > td,
	.table > tfoot > tr > td {
		padding: 8px;
		line-height: 1.42857143;
		vertical-align: top;
		border-top: 1px solid #dddddd;
	}
	.table > thead > tr > th {
		vertical-align: bottom;
		border-bottom: 2px solid #dddddd;
	}
	.table > caption + thead > tr:first-child > th,
	.table > colgroup + thead > tr:first-child > th,
	.table > thead:first-child > tr:first-child > th,
	.table > caption + thead > tr:first-child > td,
	.table > colgroup + thead > tr:first-child > td,
	.table > thead:first-child > tr:first-child > td {
		border-top: 0;
	}
	.table > tbody + tbody {
		border-top: 2px solid #dddddd;
	}
	.table .table {
		background-color: #ffffff;
	}
	.table-condensed > thead > tr > th,
	.table-condensed > tbody > tr > th,
	.table-condensed > tfoot > tr > th,
	.table-condensed > thead > tr > td,
	.table-condensed > tbody > tr > td,
	.table-condensed > tfoot > tr > td {
		padding: 5px;
	}
	.table-bordered {
		border: 1px solid #dddddd;
	}
	.table-bordered > thead > tr > th,
	.table-bordered > tbody > tr > th,
	.table-bordered > tfoot > tr > th,
	.table-bordered > thead > tr > td,
	.table-bordered > tbody > tr > td,
	.table-bordered > tfoot > tr > td {
		border: 1px solid #dddddd;
	}
	.table-bordered > thead > tr > th,
	.table-bordered > thead > tr > td {
		border-bottom-width: 2px;
	}
	.table-striped > tbody > tr {
		background-color: #FFF;
	}
	.table-striped > tbody > tr:nth-of-type(odd) {
		background-color: #f9f9f9;
	}
	.table-hover > tbody > tr:hover {
		background-color: #f5f5f5;
	}
	table col[class*="col-"] {
		position: static;
		float: none;
		display: table-column;
	}
	table td[class*="col-"],
	table th[class*="col-"] {
		position: static;
		float: none;
		display: table-cell;
	}
	.table > thead > tr > td.active,
	.table > tbody > tr > td.active,
	.table > tfoot > tr > td.active,
	.table > thead > tr > th.active,
	.table > tbody > tr > th.active,
	.table > tfoot > tr > th.active,
	.table > thead > tr.active > td,
	.table > tbody > tr.active > td,
	.table > tfoot > tr.active > td,
	.table > thead > tr.active > th,
	.table > tbody > tr.active > th,
	.table > tfoot > tr.active > th {
		background-color: #f5f5f5;
	}
	.table-hover > tbody > tr > td.active:hover,
	.table-hover > tbody > tr > th.active:hover,
	.table-hover > tbody > tr.active:hover > td,
	.table-hover > tbody > tr:hover > .active,
	.table-hover > tbody > tr.active:hover > th {
		background-color: #e8e8e8;
	}
	.table > thead > tr > td.success,
	.table > tbody > tr > td.success,
	.table > tfoot > tr > td.success,
	.table > thead > tr > th.success,
	.table > tbody > tr > th.success,
	.table > tfoot > tr > th.success,
	.table > thead > tr.success > td,
	.table > tbody > tr.success > td,
	.table > tfoot > tr.success > td,
	.table > thead > tr.success > th,
	.table > tbody > tr.success > th,
	.table > tfoot > tr.success > th {
		background-color: #dff0d8;
	}
	.table-hover > tbody > tr > td.success:hover,
	.table-hover > tbody > tr > th.success:hover,
	.table-hover > tbody > tr.success:hover > td,
	.table-hover > tbody > tr:hover > .success,
	.table-hover > tbody > tr.success:hover > th {
		background-color: #d0e9c6;
	}
	.table > thead > tr > td.info,
	.table > tbody > tr > td.info,
	.table > tfoot > tr > td.info,
	.table > thead > tr > th.info,
	.table > tbody > tr > th.info,
	.table > tfoot > tr > th.info,
	.table > thead > tr.info > td,
	.table > tbody > tr.info > td,
	.table > tfoot > tr.info > td,
	.table > thead > tr.info > th,
	.table > tbody > tr.info > th,
	.table > tfoot > tr.info > th {
		background-color: #d9edf7;
	}
	.table-hover > tbody > tr > td.info:hover,
	.table-hover > tbody > tr > th.info:hover,
	.table-hover > tbody > tr.info:hover > td,
	.table-hover > tbody > tr:hover > .info,
	.table-hover > tbody > tr.info:hover > th {
		background-color: #c4e3f3;
	}
	.table > thead > tr > td.warning,
	.table > tbody > tr > td.warning,
	.table > tfoot > tr > td.warning,
	.table > thead > tr > th.warning,
	.table > tbody > tr > th.warning,
	.table > tfoot > tr > th.warning,
	.table > thead > tr.warning > td,
	.table > tbody > tr.warning > td,
	.table > tfoot > tr.warning > td,
	.table > thead > tr.warning > th,
	.table > tbody > tr.warning > th,
	.table > tfoot > tr.warning > th {
		background-color: #fcf8e3;
	}
	.table-hover > tbody > tr > td.warning:hover,
	.table-hover > tbody > tr > th.warning:hover,
	.table-hover > tbody > tr.warning:hover > td,
	.table-hover > tbody > tr:hover > .warning,
	.table-hover > tbody > tr.warning:hover > th {
		background-color: #faf2cc;
	}
	.table > thead > tr > td.danger,
	.table > tbody > tr > td.danger,
	.table > tfoot > tr > td.danger,
	.table > thead > tr > th.danger,
	.table > tbody > tr > th.danger,
	.table > tfoot > tr > th.danger,
	.table > thead > tr.danger > td,
	.table > tbody > tr.danger > td,
	.table > tfoot > tr.danger > td,
	.table > thead > tr.danger > th,
	.table > tbody > tr.danger > th,
	.table > tfoot > tr.danger > th {
		background-color: #f2dede;
	}
	.table-hover > tbody > tr > td.danger:hover,
	.table-hover > tbody > tr > th.danger:hover,
	.table-hover > tbody > tr.danger:hover > td,
	.table-hover > tbody > tr:hover > .danger,
	.table-hover > tbody > tr.danger:hover > th {
		background-color: #ebcccc;
	}
	.table-responsive {
		overflow-x: auto;
		min-height: 0.01%;
	}
	@media screen and (max-width: 767px) {
		.table-responsive {
			width: 100%;
			margin-bottom: 15px;
			overflow-y: hidden;
			-ms-overflow-style: -ms-autohiding-scrollbar;
			border: 1px solid #dddddd;
		}
		.table-responsive > .table {
			margin-bottom: 0;
		}
		.table-responsive > .table > thead > tr > th,
		.table-responsive > .table > tbody > tr > th,
		.table-responsive > .table > tfoot > tr > th,
		.table-responsive > .table > thead > tr > td,
		.table-responsive > .table > tbody > tr > td,
		.table-responsive > .table > tfoot > tr > td {
			white-space: nowrap;
		}
		.table-responsive > .table-bordered {
			border: 0;
		}
		.table-responsive > .table-bordered > thead > tr > th:first-child,
		.table-responsive > .table-bordered > tbody > tr > th:first-child,
		.table-responsive > .table-bordered > tfoot > tr > th:first-child,
		.table-responsive > .table-bordered > thead > tr > td:first-child,
		.table-responsive > .table-bordered > tbody > tr > td:first-child,
		.table-responsive > .table-bordered > tfoot > tr > td:first-child {
			border-left: 0;
		}
		.table-responsive > .table-bordered > thead > tr > th:last-child,
		.table-responsive > .table-bordered > tbody > tr > th:last-child,
		.table-responsive > .table-bordered > tfoot > tr > th:last-child,
		.table-responsive > .table-bordered > thead > tr > td:last-child,
		.table-responsive > .table-bordered > tbody > tr > td:last-child,
		.table-responsive > .table-bordered > tfoot > tr > td:last-child {
			border-right: 0;
		}
		.table-responsive > .table-bordered > tbody > tr:last-child > th,
		.table-responsive > .table-bordered > tfoot > tr:last-child > th,
		.table-responsive > .table-bordered > tbody > tr:last-child > td,
		.table-responsive > .table-bordered > tfoot > tr:last-child > td {
			border-bottom: 0;
		}
	}
</style>
	<!-- ====== CONTACT INFO =============================================================================================== -->
	<div id="top-data" class="row">
		<div class="g100 p-p0 p-m0">
			<?php
			$existingContact = ($model->id > 0);
			$title = ($existingContact) ? 'Edit Contact :: '.$model->fullName : 'Add New Contact';

			$this->beginStmPortletContent(array(
				'handleTitle' => $title,
				'handleIconCss' => 'i_user'
			)); ?>
			<div id="contact-container">
				<div class="g12 p-mb5 rounded-text-box notes odd-static">
					<div id="contact-left" class="g6 ">
						<table class="">
                            <tr>
                                <th class="narrow"><?php echo $form->labelEx($model, 'contact_status_ma')?></th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'contact_status_ma', StmFormHelper::getStatusBooleanList(true, $inactiveLabel='Trash'),
                                        $htmlOptions = array(
                                            'data-placeholder'=>'Select a Status',
                                            'empty'=>'',
                                            'class' => 'g5',
                                        )); ?>
                                    <?php echo $form->error($model, 'contact_status_ma');
//                                    $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Contacts_contact_status_ma','options'=>array('allow_single_deselect'=>true)));
                                    ?>
                                </td>
                            </tr>
							<tr>
								<th class="narrow"><?php echo $form->labelEx($model, 'source_id')?></th>
								<td>
									<?php echo $form->dropDownList($model, 'source_id', Sources::optGroupList($model->source_id),
										$htmlOptions = array(
											'data-placeholder'=>'Select a Source',
											'empty'=>'',
											'style' => 'width:83%;',
										)); ?>
									<?php echo $form->error($model, 'source_id');
										  $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Contacts_source_id','options'=>array('allow_single_deselect'=>true)));
									?>
								</td>
							</tr>
							<tr>
								<th class="narrow">
									<?php echo $form->label($model, 'contactFullName')?>
									<?php echo CHtml::tag('span', $htmlOptions = array(
										'class' => 'required',
									), '*'); ?>
								</th>
								<td>
									<?php echo $form->textField($model, 'first_name', $htmlOptions = array(
										'placeholder' => 'First Name', 'class' => 'g5', 'autocomplete'=>'off'
									)); ?>
									<?php echo $form->textField($model, 'last_name', $htmlOptions = array(
										'placeholder' => 'Last Name', 'class' => 'g5', 'autocomplete'=>'off'
									)); ?>
									<?php echo $form->error($model, 'contactFullName'); ?>
								</td>
							</tr>
                            <tr>
                                <th class="narrow">
                                    <?php echo $form->label($model, 'phonetic_name')?>:
                                </th>
                                <td>
                                    <?php echo $form->textField($model, 'phonetic_name', $htmlOptions = array(
                                            'placeholder' => 'Phonetic Name', 'class' => 'g10', 'autocomplete'=>'off'
                                        )); ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="narrow">
                                    <label>Spouse:</label>
                                </th>
                                <td>
                                    <?php echo $form->textField($model, 'spouse_first_name', $htmlOptions = array(
                                            'placeholder' => 'Spouse First Name', 'class' => 'g5', 'autocomplete'=>'off'
                                        )); ?>
                                    <?php echo $form->textField($model, 'spouse_last_name', $htmlOptions = array('placeholder' => 'Spouse Last Name', 'class' => 'g5', 'autocomplete'=>'off','disabled'=>'1')); ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="narrow">
                                    <label>Spouse Phonetic Name:</label>
                                </th>
                                <td>
                                    <?php echo $form->textField($model, 'spouse_phonetic_name', $htmlOptions = array(
                                            'placeholder' => 'Spouse Phonetic Name', 'class' => 'g10', 'autocomplete'=>'off', 'value' => (empty($model->spouse_phonetic_name)) ? ' ' : $model->spouse_phonetic_name//,'value'=>' '
                                        )); ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="narrow">
                                </th>
                                <td>
                                    <?php echo CHtml::checkBox('showSpouseLastName', 0,$htmlOptions=array('uncheckValue'=>'0','style'=>'font-size:16px')); ?> <span class="label">Spouse Last Name is different</span>
                                </td>
                            </tr>
							<tr>
								<th class="narrow top-align">Password:</th>
								<td>
									<?php echo $form->passwordField($model, 'password', $htmlOptions = array(
											'placeholder' => 'Password', 'class' => 'g5', 'autocomplete'=>'off',
										)); ?>
									<?php echo $form->error($model, 'password'); ?>
								</td>
							</tr>
							<tr>
								<th class="narrow"><em class="icon icon-only i_stm_star"></em></th>
								<td><span class="label">= Indicates Primary Email, Phone, Address</span>
								</td>
							</tr>
							<tr>
								<th class="narrow top-align">Emails:</th>
								<td id="email-table-cell">
									<?php echo $this->renderPartial('_addlEmailTemplate', array(
										'form' => $form,
										'containerHtmlOptions' => array(
											'style' => 'display: none',
											'id' => 'addlEmailTemplate'
										)
									)); ?>
									<div id="addlEmailRows" class="g12 p-p0" style="min-height: 0;">
										<?php
										if ($model->contactEmails) {
											foreach ($model->contactEmails as $i => $ContactEmail) {
												echo $this->renderPartial('_addlEmailTemplate', array(
													'form' => $form,
													'model' => $ContactEmail,
													'i' => $i,
												));
											}
										}
										?>
									</div>
									<button type="button" id="add-email" class="text p-m0"><em class="icon i_stm_add"></em>Add Email</button>
								</td>
							</tr>
							<tr>
								<th class="narrow top-align">Address:</th>
								<td id="address-table-cell">
									<?php echo $this->renderPartial('_addlAddressTemplate', array(
										'form' => $form,
										'containerHtmlOptions' => array(
											'style' => 'display: none',
											'id' => 'addlAddressTemplate'
										)
									)); ?>
									<div id="addlAddressRows" class="g12 p-p0" style="min-height: 0;">
										<?php
										if ($model->contactAddresses) {
											foreach ($model->contactAddresses as $i => $ContactAddress) {
												echo $this->renderPartial('_addlAddressTemplate', array(
													'form' => $form,
													'model' => $ContactAddress,
													'i' => $i,
												));
											}
										}
										?>
									</div>
									<button type="button" id="add-address" class="text"><em class="icon i_stm_add"></em>Add Address</button>
								</td>
							</tr>
							<tr>
								<th class="narrow top-align">Phone:</th>
								<td id="phone-table-cell">
									<?php echo $this->renderPartial('_addlPhoneTemplate', array(
										'form' => $form,
										'containerHtmlOptions' => array(
											'style' => 'display: none',
											'id' => 'addlPhoneTemplate'
										)
									)); ?>
									<div id="addlPhoneRows" class="g12 p-p0" style="min-height: 0;">
										<?php
										if ($model->contactPhones) {
											foreach ($model->contactPhones as $i => $ContactPhone) {
												echo $this->renderPartial('_addlPhoneTemplate', array(
													'form' => $form,
													'model' => $ContactPhone,
													'i' => $i+1,
												));
											}
										}
										?>
									</div>
									<button type="button" id="add-phone" class="text"><em class="icon i_stm_add"></em>Add Phone</button>
								</td>
							</tr>
							<tr <?=(!YII_DEBUG && Yii::app()->controller->module->id == 'admin') ? "style='display:none;'": "";?>>
								<th>Pipeline:</th>
								<td>
									<?php if($model->pipelineComponentLu):?>
										<table class="table table-striped table-bordered">
											<thead>
												<tr>
													<th style="text-align: left; color: #333333; font-size: 12px; font-weight: bold;">Pipeline</th>
													<th style="text-align: left; color: #333333; font-size: 12px; font-weight: bold;">Stage</th>
												</tr>
											</thead>
											<tbody id="currentPipelineAssignments">
											<?php foreach($model->pipelineComponentLu as $pcl):?>
												<tr>
													<td><a href="/<?=Yii::app()->controller->module->id;?>/pipeline/view/<?=$pcl->pipeline_id;?>" target="_blank"><?=$pcl->pipelineStage->pipeline->name;?></a></td>
													<td><a href="/<?=Yii::app()->controller->module->id;?>/pipeline/view/<?=$pcl->pipeline_id;?>" target="_blank"><?=$pcl->pipelineStage->name; ?></a></td>
												</tr>

											<?php endforeach; ?>
											</tbody>
										</table>
									<?php endif; ?>
									<button type="button" id="pipeline-dialog-button" name="pipeline-dialog-button" class="text assign-pipeline-to-component" data-ctid="<?=$model->componentType->id;?>" data-cid="<?=$model->id;?>"><em class="icon i_stm_add"></em>Assign Pipeline</button></button>
									<?php
									Yii::import('admin_widgets.DialogWidget.PipelineDialogWidget.PipelineDialogWidget');
									$this->widget('admin_widgets.DialogWidget.PipelineDialogWidget.PipelineDialogWidget', array(
											'id' => PipelineDialogWidget::PIPELINE_DIALOG_ID,
											'title' => 'Pipeline',
											'triggerElement' => ".assign-pipeline-to-component",
											'componentTypeId' => $model->componentType->id,
											'updateContainer' => "#currentPipelineAssignments"
										)
									);
									?>
								</td>
							</tr>
						</table>
					</div>
					<div id="contact-right" class="g6">
						<table class="">
							<tr>
								<th class="narrow">Tags:</th>
								<td colspan="2">
									<?php echo $form->dropDownList($model, 'contactTypesToAdd', CHtml::listData(ContactTypes::model()->findAll(array('order'=>'name ASC')), 'id', 'name'),
										$htmlOptions = array('class'=>'chzn-select','multiple'=>'multiple','style'=>'width:83%;','data-placeholder'=>'Select Contact Types'));
										$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Contacts_contactTypesToAdd'));
									?>
									<?php echo $form->error($model, 'contactTypesToAdd'); ?>
								</td>
							</tr>
							<?php foreach (ContactAttributes::model()->findAll(array('condition'=>'t.id<1000')) as $ContactAttributes): ?>
								<tr>
									<th class="narrow"><?php echo $ContactAttributes->label;?>:</th>
									<td>
										<?php
										$ContactAttributeValue = $model->getAttributeValue($ContactAttributes->id);

										switch ($ContactAttributes->data_type_id) {
											case DataTypes::TEXT_INPUT_ID:
												echo $form->textField($ContactAttributeValue, "[$ContactAttributeValue->contact_attribute_id]value",
													$htmlOptions = array(
														'class' => 'g10',
													));
												break;
											case DataTypes::DATE_ID:
											case DataTypes::DATETIME_ID:
												$this->widget('zii.widgets.jui.CJuiDatePicker', array(
													'model' => $ContactAttributeValue,
													'attribute' => "[$ContactAttributeValue->contact_attribute_id]value",
													'options' => array(
														'showAnim' => 'fold',
													),
													'htmlOptions' => array(
														'class' => 'g3',
													),
												));
												break;
                                            case DataTypes::BOOLEAN_ID:
                                                echo $form->dropDownList($ContactAttributeValue, "[$ContactAttributeValue->contact_attribute_id]value", StmFormHelper::getYesNoList(),
                                                    $htmlOptions = array(
                                                        'class' => 'g2',
                                                        'empty'=>'',
                                                    ));
                                                break;
										}
										?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
			</div>
			<div id="contact-bottom">
				<div class="g12 p-mb5 rounded-text-box notes odd-static">
                <?php if(Yii::app()->controller->module->id == 'hq') {
                     Yii::app()->controller->printHqAttributes($model, $form);
                    }
                ?>
					<table class="">
						<tr>
							<th class="narrow">Notes:</th>
							<td>
								<?php echo $form->textArea($model, 'notes', $htmlOptions = array(
									'placeholder' => 'Notes',
									'class' => 'g10',
									'rows'=>8,
								)); ?>
                                <?php echo CHtml::hiddenField('returnUrl',$returnUrl) ?>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

	<?php
	$buttonAction = ($existingContact) ? 'Update' : 'Submit';
	?>
	<!-- ====== END SECTIONS =============================================================================================== -->
	<div id="submit-button-wrapper">
		<button type="submit" class="submit wide"><?php echo $buttonAction?> Contact</button>
	</div>

<?php $this->endWidget(); ?>

<?php $this->endStmPortletContent(); ?>
