<?php
$module = Yii::app()->controller->module->id;
$uri = $_SERVER['REQUEST_URI'];

Yii::app()->clientScript->registerScript('contactsListSearch', <<<JS
        $('.export-button').click(function(){
            if(confirm("Are you sure you want to export this contact list?")) {
                $('body').prepend('<div class="loading-container loading"><em></em></div>');
                $('#export').val(1);

                window.open('/$module/contacts?' + $('#listview-search form').serialize());
                setTimeout(function () {
                    $('div.loading-container.loading').remove();
                    Message.create("notice", "Contact is processing in the new window. Please wait...");
                }, 5000);
            }
            $('#export').val(0);
            return false;
        });

        $('#listview-search form').submit(function() {

             // clear out mass update
            $('#MassAction_action').val('');
            $("select#MassAction_options").val(null).trigger('liszt:updated');
            $("#MassAction_action_tag_options").hide();

            $('#dialerProcess').val(0);

            // reset key so pagination gets reset to 1
            $('div.keys').attr('title','$uri');

            $.fn.yiiGridView.update('contact-grid', { data: $(this).serialize() });

            $('#dialerAction').val('');
            $('#dialerListId').val('');

            return false;
        });

        $('.dialer-button').click(function(){
            $('#dialerProcess').val(1);

            if($('#dialerAction').val() == '') {
                alert('Please select option to Add or Remove from Dialer list.');
                $('#dialerProcess').val(0);
                return false;
            }

            if($('#dialerListId').val() == '') {
                alert('Please select a Dialer list or create one if needed.');
                $('#dialerProcess').val(0);
                return false;
            }

            $.fn.yiiGridView.update('contact-grid', {
                data: $('#listview-search form').serialize()
            });
            $('#dialerProcess').val(0);
            return false;
        });

        $('.broadcast-button').click(function(){

            if(!$('#broadcastCallerId').val().match(/^\d{10}/)) {
                alert('Please enter a valid Caller ID.');
                $('#broadcastProcess').val(0);
                return false;
            }

            if($('#broadcastAudioFile').val() == '') {
                alert('Please select valid audio file.');
                $('#broadcastProcess').val(0);
                return false;
            }

            if($('#broadcastDeliveryDate').val().length == '') {
                alert('Please select valid date and time.');
                $('#broadcastProcess').val(0);
                return false;
            }

            if(confirm('Are you sure you want to continue?')) {

                $('#broadcastProcess').val(1);

                $.ajax({
                    data: $('#listview-search form').serialize(),
                    dataType: 'json',
                    success: function(o) {

                        // Show flash message
                        Message.create(o.status, o.message);
                    }
                });

                $('#broadcastCallerId').val('');
                $('#broadcastDeliveryDate').val('');
                $('#broadcastAudioFile').val('');

                $('#broadcastProcess').val(0);
            }

            return false;
        });

    $("#MassAction_action").on("change",function(){
		var option = $( this ).val();
		$("#MassAction_action_tag_options").hide();
		$("#MassAction_action_plan_options").hide();

		if(option == "addTag" || option == "removeTag"){
			$("#MassAction_action_tag_options").show();
		}else if (option == "applyActionPlan"){
			$("#MassAction_action_plan_options").show();
		}
	});


	$( "form .ajaxMassAction" ).on("click", function( event ) {
		 event.preventDefault();

         if(confirm("Confirm proceeding with Mass Action.")) {

             $("body").prepend("<div class='loading-container loading'><em></em></div>");
             var element_id = $(this).closest('form').attr('id');
             $.get(
                $("#"+element_id).attr("action"),
                $( "#"+element_id ).serializeArray(),
                function(data) {
                    if(data.status == 200){
                        Message.create('success','Mass Update was successful.');

                         // clear out mass update
                        $('#MassAction_action').val('');
                        $("select#MassAction_options").val(null);
                        $("select#MassAction_plan_options").val(null).trigger('liszt:updated');

                        $("#MassAction_action_tag_options").hide();
                        $("#MassAction_action_plan_options").hide();
                    }
                    else {
                        Message.create('error','Error: Mass Update had an error.');
                    }
                    $("div.loading-container.loading").remove();
                }
            );
         }
	});
JS
);

$form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
		'id' => 'contact-list-search',
	)
); ?>
	<div class="g4">
		<label class="g3">Name:</label>
		<span class="g4"><?php echo $form->textField($model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')); ?></span>
		<span class="g4 p-ml10"><?php echo $form->textField($model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')); ?></span>
	</div>
	<div class="g3">
		<label class="g3">Email:</label>
		<span class="g9"><?php echo $form->textField($model->emails, 'email', $htmlOptions = array('placeholder' => 'Email')); ?></span>
	</div>
	<div class="g3">
		<label class="g3">Phone:</label>
        <span class="g9">
          <?php $this->widget('StmMaskedTextField', array(
				  'model' => $model->phones,
				  'attribute' => 'phone',
				  'mask' => '(999) 999-9999',
				  //Makes the full 10 digit phone number optional (enables partial search)
				  'htmlOptions' => array(
					  'placeholder' => '(999) 123-1234',
					  'class' => 'phoneField'
				  )
			  )
		  ); ?>
        </span>
	</div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>
	<div class="g4">
		<label class="g3">Address:</label>
		<span class="g9"><?php echo $form->textField($model->addresses, 'address', $htmlOptions = array('placeholder' => 'Address')); ?></span>
	</div>
	<div class="g3">
		<label class="g3">Zip:</label>
		<span class="g9"><?php echo $form->textField($model->addresses, 'zip', $htmlOptions = array('placeholder' => 'Zip Code')); ?></span>
	</div>
	<div class="g3">
		<label class="g3">Sources:</label>
		<span class="g9"><?php echo $form->dropDownList($model->source, 'idCollection', CHtml::listData(Sources::model()->findAll(), 'id', 'name'), array(
					'empty' => 'All',
					'class' => 'chzn-select g12',
					'multiple' => 'multiple',
					'data-placeholder' => 'Sources'
				)
			); ?>
		</span>
		<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Sources_idCollection')); ?>
	</div>

<?php if(Yii::app()->user->checkAccess('owner')):
	echo CHtml::hiddenField('export','0');
	?>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('Export', array('class' => 'button export-button')); ?></div>
<?php endif; ?>

	<div class="g4">
		<label class="g3">Status:</label>
		<span class="g9"><?php echo $form->dropDownList($model, 'contact_status_ma', StmFormHelper::getStatusBooleanList(true,'Trash'), array('class' => 'g12')); ?></span>
	</div>
	<!--<div class="g3">-->
	<!--    <div class="g4 text-right"><label>Assigned to:</label></div>-->
	<!--    <div class="g7">-->
	<!--        --><?php //echo $form->dropDownList(
//            $model, 'assignmentId',
//            CHtml::listData(
//                Contacts::model()->byActiveAdmins(false, $model->assignmentId)
//                    ->byAssignmentComponentType(ComponentTypes::BUYERS)->orderByName()->findAll(), 'id',
//                'fullName'
//            ), $htmlOptions = array('empty' => '')
//        );
//
//        $this->widget(
//            'admin_module.extensions.EChosen.EChosen', array(
//                'target'  => 'select#Buyers_assignmentId',
//                'options' => array(
//                    'allow_single_deselect' => true,
//                    'width'                 => '84%'
//                )
//            )
//        );
//        ?>
	<!--    </div>-->
	<!--</div>-->
	<!--<div class="g1"></div>-->
	<div class="g3">
		<label class="g3">Contact Tags:</label>
		<span class="g9"><?php echo $form->dropDownList($model->contactTypeLu, 'idCollection', CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name'), array(
					'empty' => 'All',
					'class' => 'chzn-select  g12',
					'multiple' => 'multiple',
					'data-placeholder' => 'Contact Tags'
				)
			); ?>
		</span>
		<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#ContactTypeLu_idCollection')); ?>
	</div>

	<div class="g4">
		<div class="g2 text-right"><label>Sort:</label></div>
		<div class="g4">
			<?php echo $form->dropDownList(
				$model, 'dateSearchFilter', $model->dateSearchTypes, $htmlOptions = array(
				'empty' => '',
				'class'=>'g12',
			)
			); ?>
		</div>
		<div class="g6">
			<?php $this->widget(
				'zii.widgets.jui.CJuiDatePicker', array(
					'model'       => $model,
					'attribute'   => 'fromSearchDate',
					// name of post parameter
					'options'     => array(
						'showAnim'       => 'fold',
						'dateFormat'     => 'mm/dd/yy',
						'numberOfMonths' => 2,
					),
					'htmlOptions' => array(
						'style' => 'width: 80px; float: left; font-size: 12px;',
					),
				)
			);
			?>
			<label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
			<?php $this->widget(
				'zii.widgets.jui.CJuiDatePicker', array(
					'model'       => $model,
					'attribute'   => 'toSearchDate',
					// name of post parameter
					'options'     => array(
						'showAnim'       => 'fold',
						'dateFormat'     => 'mm/dd/yy',
						'numberOfMonths' => 2,
					),
					'htmlOptions' => array(
						'style' => 'width: 80px; float: left; font-size: 12px;',
					),
				)
			);
			?>
		</div>
	</div>
	<div class="g4">
		<label class="g3">Mass Action</label>
				<span class="g9">
					<select id="MassAction_action" name="MassAction[action]">
						<option>Select an action</option>
						<option value="addTag">Apply Tag</option>
						<option value="removeTag">Remove Tag</option>
						<!--						<option value="applyActionPlan">Apply Action Plan</option>-->
					</select>
				</span>
		<span class="g3">&nbsp;</span>
				<span id="MassAction_action_tag_options" class="g9" style="display: none">
					<?php
					echo CHtml::dropDownList(
						'MassAction[options]',
						$models->typesCollection,
						CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name'),
						array(
							'empty' => ' ',
							'class' => 'chzn-select',
							'multiple' => 'multiple',
							'style' => 'width:100%;',
							'data-placeholder' => 'Recruit Tags',
							'id' => "MassAction_options"
						)
					);
					?>
					<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#MassAction_options','options' => array('width'=>'80%'))); ?>
					<button type="button" class="ajaxMassAction">Go</button>
                </span>
		<span id="MassAction_action_plan_options" class="g3 massActionOptions" style="display: none">
					<?php
					echo CHtml::dropDownList(
						'MassAction[plan_options]',
						$models->typesCollection,
						CHtml::listData(
							ActionPlans::model()
								->byComponentType(ComponentTypes::CONTACTS)
								->byStatus(StmFormHelper::ACTIVE)
								->orderByName()
								->findAll(),
							'id',
							'name'
						),
						array(
							'empty' => 'Action Plan',
							'class' => 'chzn-select',
							'style' => 'width:100%;',
							'data-placeholder' => 'Action Plan',
							'id' => "MassAction_plan_options"
						)
					);
					?>
			<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#MassAction_plan_options','options' => array('width'=>'80%'))); ?>
			<button type="button" class="ajaxMassAction">Go</button>
                </span>

	</div>
	<div class="g2">
		<div class="g11 text-right"><label>No Action Plan:</label></div>
		<div class="g1">
			<?php echo CHtml::checkbox('noActionPlanCheckbox',false ,$htmlOptions=array(
				'value'=>1,
				'style'=>'font-size: 16px; position: relative; top: 10px;',
			)); ?>
			<?php echo CHtml::hiddenField('noActionPlan', null); ?>
		</div>
	</div>
	<div class="g2">
		<div class="g11 text-right"><label>No Tasks:</label></div>
		<div class="g1">
			<?php echo CHtml::checkbox('noTasksCheckbox',false ,$htmlOptions=array(
				'value'=>1,
				'style'=>'font-size: 16px; position: relative; top: 10px;',
			)); ?>
			<?php echo CHtml::hiddenField('noTasks', null); ?>
		</div>
	</div>


<?if(Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('dialerimport')):?>
	<hr class="g12" />
	<div class="g12">
		<label class="g1">Dialer Action:</label>
        <span class="g10">
            <?php echo CHtml::dropDownList('dialerAction', null, array('add'=>'Add to Dialer List', 'remove'=>'Remove from Dialer List'), $htmlOptions=array('empty'=>'')); ?>
			<?php echo CHtml::dropDownList('dialerListId', null, CHtml::listData(CallLists::model()->findAll(array('condition'=>'preset_ma='.CallLists::CUSTOM_CONTACTS)), 'id','name'), $htmlOptions=array('empty'=>'')); ?>
			<?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button dialer-button','style'=>'font-size: 10px; padding: 8px 10px;'))?>
			<?php echo CHtml::hiddenField('dialerProcess', null, $htmlOptions=array('value'=>'0'))?>
        </span>
	</div>
<?endif;?>

<? if(Yii::app()->user->checkAccess('slybroadcast')):?>
	<hr style="padding: 0; min-height: 5px;" class="g12">
	<div class="g12">
		<label class="g1">Slybroadcast:</label>
        <span class="g10">

            <?php echo CHtml::textField('broadcastCallerId', null, $htmlOptions=['placeholder' => 'Caller ID', 'style' => 'width: 250px;']);?>

			<div style='display: inline-block;'>
				Audio File<br>
				<? echo CHtml::dropDownList('broadcastAudioFile', null, CHtml::listData(VoicemailBroadcastMessages::model()->findAll(), 'id', 'name'), $htmlOptions = array(
					//'class' => 'g8',
					'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
				));?>

			</div>

			<?php $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'name' => 'broadcastDeliveryDate',
					'defaultTime' => '12:00 am',
					'options' => array(
						'showAnim' => 'fold',
					),
					'htmlOptions' => array(
						'style' => 'width:185px;',
						'placeholder' => 'Set on Date & Time'
					),
				)
			); ?>

			<div style='display: inline-block;'>
				<label>Mobile Only:</label>
				<?php echo CHtml::checkbox('broadcastMobileOnlyCheckbox',false ,$htmlOptions=array(
					'value'=>0,
					'style'=>'font-size: 16px; position: relative; top: 10px;',
				)); ?>
				<?php echo CHtml::hiddenField('broadcastMobileOnly', null); ?>
			</div>

			<?php echo CHtml::hiddenField('broadcastProcess', null, $htmlOptions=array('value'=>'0'))?>
			<?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button broadcast-button','style'=>'font-size: 10px; padding: 8px 10px;'))?>

			<a id="add-call-list-button" class="text-button" href="/admin/contacts/voicemailBroadcasts" style="display: inline-block;"><em class="icon i_stm_add"></em>Add Audio File</a>
        </span>
	</div>
<? endif;?>

<?php $this->endWidget(); ?>