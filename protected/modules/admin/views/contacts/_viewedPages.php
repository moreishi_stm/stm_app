<?php
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'homes-viewed-grid',
	'htmlOptions'=>array(
		'class'=>'homes-viewed-grid'
	 ),
	'template'=>'{items}',
	'enablePagination'=>false,
	'dataProvider'=>$dataProvider,
	'itemsCssClass'=>'datatables',
	'columns'=>array(
	    array(
			'type'=>'raw',
	    	'name'=>'Date / Time',
	    	'value'=>'Yii::app()->format->formatDateTime($data->added,true)',
	    ),
	    array(
			'type'=>'raw',
	        'name'=>'Price / MLS#',
	        'value'=>'"<b>".Yii::app()->format->formatDollars($data->property->price)."</b><br>".$data->property->listing_id',
		    ),
	    array(
			'type'=>'raw',
	        'name'=>'Size',
	        'value'=>'$data->property->bedrooms."BR / ".$data->property->baths_full." Baths <br>".number_format($data->property->sq_feet)." SF"',
		    ),
	    array(
	        'type'=>'raw',
	        'name'=>'Address / Subdivision',
	        'value'=>'"<b><u>".$data->property->common_subdivision."</u></b><br>".$data->property->streetAddress."<br>".$data->property->cityStZip',
		    ),
		),
	)
);