<?php
	$this->breadcrumbs = array(
		'Opportunities' => array('/'.$this->module->name.'/contacts/opportunities'),
	);
?>
	<div id="listview-actions">
		<a href="/<?php echo $this->module->name;?>/contacts/add/sellers" class="button gray icon i_stm_add">Add New Seller</a>
        <a href="/<?php echo $this->module->name;?>/contacts/add/buyers" class="button gray icon i_stm_add">Add New Buyer</a>
	</div>
	<div id="content-header">
		<h1>Opportunities List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox', array(
				'model' => $model,
			)
		); ?>
	</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'contact-grid',
        'template'=>'{summary}{items}{summary}{pager}',
		'dataProvider' => $DataProvider,
        'enableSorting'=>true,
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'status',
                'value' => 'StmFormHelper::getStatusBooleanName($data->contact_status_ma, "Trash")',
            ),
            array(
                'type' => 'raw',
                'name' => 'Tags',
                'value' => 'Yii::app()->format->formatCommaDelimited($data->types,"name")',
            ),
            array(
                'type' => 'raw',
                'name' => 'Contact Info',
                'value' => 'Yii::app()->controller->action->printContactInfo($data)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Special Dates & Relationships',
                'value' => 'Yii::app()->controller->action->printSpecialDatesRelationships($data)',
            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Last Spoke',
//                'value' => 'Yii::app()->format->formatDateTimeDays($data->lastSpokeDate(), array("break"=>true))',
//            ),
            array(
				'type' => 'raw',
				'name' => 'Opportunities',
				'value' => '$data->existingComponentRecordButton(' . ComponentTypes::CONTACTS . ')',
			),
            array(
                'type' => 'raw',
                'name' => 'Activities',
                'value' => 'Yii::app()->controller->action->printActivityInfo($data)',
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
		),
	)
);
