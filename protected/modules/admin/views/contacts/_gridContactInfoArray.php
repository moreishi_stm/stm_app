<?
$spouseLastDiff = (!empty($data['lastName']) && !empty($data['spouseFirstName']) && !empty($data['spouseLastName']) && (strtolower($data['spouseLastName']) !== strtolower($data['lastName']))) ? true : false;
$name = $data['firstName'];
if(!empty($data['spouseFirstName'])) {
    $name .= ($spouseLastDiff) ? ' '.$data['lastName'].' & '  : ' & '.$data['spouseFirstName'].' '.$data['lastName'] ;
    $name .= ($spouseLastDiff) ? $data['spouseFirstName'].' '.$data['spouseLastName'] : '';
}
else {
    $name .= ' '.$data['lastName'];
}
$url = '/admin/'.$data['componentTypeName'].'/'.$data['id'];
echo '<a href="'.$url.'" target="_blank">'. $name . '</a>'.
    '<a href="'.$url.'" target="_blank" class="action-button blue fa fa-search lead-button-tip" data-tooltip="View Details"></a>'.
    '<a href="'.$url.'" target="_blank" class="action-button orange fa fa-pencil lead-button-tip" data-tooltip="Edit Details"></a>';

$data['phones'] = Yii::app()->db->createCommand("select phone from phones where contact_id=".$data['contact_id']." AND is_deleted=0")->queryColumn();
if(!empty($data['phones'])) {
    foreach($data['phones'] as $phone) {
        $phoneString .= (empty($phoneString))? Yii::app()->format->formatPhone($phone) : '<br />'.Yii::app()->format->formatPhone($phone);
    }
    echo '<br />'.$phoneString;
}

if($data['emails'] = Yii::app()->db->createCommand("select email from emails where contact_id=".$data['contact_id']." AND is_deleted=0")->queryColumn()) {
    foreach($data['emails'] as $email) {
        $emailString .= (empty($emailString))? $email : '<br />'.$email;
    }
    echo '<br />'.$emailString;
}

if($data['address']) {
    echo '<br /><span style="font-weight: bold; text-decoration: underline;">'.$data['address'].'</span>';
    echo '<br />'.$data['city'].', '.AddressStates::getShortNameById($data['stateId']).' '.$data['zip'].'';
}
if($data['component_type_id']==ComponentTypes::SELLERS && $data['transactionStatusId']==TransactionStatus::ACTIVE_LISTING_ID) {
    $listingDate = TransactionFieldValues::model()->findByAttributes(array('transaction_id'=> $data['id'],'transaction_field_id'=>TransactionFields::SELLER_LISTING_DATE))->value;
    if($listingDate) {
        $unformattedlistingDate = date('Y-m-d', strtotime($listingDate));
        if($isFuture = ($unformattedlistingDate > date('Y-m-d'))) {
            $formattedListingDate = 'Coming Soon in '.Yii::app()->format->formatDays($unformattedlistingDate, array('isFuture'=>true));
        } else {
            $formattedListingDate = Yii::app()->format->formatDays($unformattedlistingDate).' on market';
        }
    } else {
        $formattedListingDate = '<span style="color: #D20000; font-weight: bold;">Missing Listing Date</span>';
    }
    echo '<br />'.$formattedListingDate;
    echo '<br />Showings: '.ActivityLog::model()->countByAttributes(array('component_type_id'=>$data['component_type_id'], 'component_id'=>$data['id'], 'task_type_id'=>TaskTypes::SHOWING));
}