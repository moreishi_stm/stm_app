<?php
$this->breadcrumbs = array(
	$model->fullName,
);

$this->title = $model->fullName;

?>
<div id="contact-header">
	<div class="submit-login-summary">
		<div class="p-fr p-rel">
			<span class="grey-textbox">
				<?php echo Yii::app()->format->formatDateDays($model->last_login); ?>
			</span><em class="bubble">Last Login</em>
		</div>
		<div class="p-fr p-rel">
			<span class="grey-textbox">
				<?php echo $model->addedDateTime; ?>
			</span><em class="bubble">Submit Date</em>
		</div>
	</div>
</div>

<!--<div class="res-section res-group">-->
<!--    <div class="res-col span_6_of_12">-->
<!--        Left-->
<!--    </div>-->
<!--    <div class="res-col span_6_of_12">-->
<!--        Right-->
<!--    </div>-->
<!--</div>-->

<div class="btn-bar">
    <a href="#relationships">
        Relationships
    </a>
    <a href="#activity-details">
        Tasks
    </a>
    <a href="#activity-log-portlet">
        Activity Log
    </a>
    <button class="text add-task-button" type="button" data="<?=$model->id?>" ctid="<?=$model->componentType->id?>"><em class="icon i_stm_add"></em>Add Task</button>
    <button class="text add-activity-log-button" type="button" data="<?=$model->id?>" ctid="<?=$model->componentType->id?>"><em class="icon i_stm_add"></em>Add Activity</button>
</div>

<div id="top-data" class="res-section res-group p-0">
	<div class="res-col span_8_of_12 p-0 p-fl">
		<?php
            $this->widget('admin_widgets.ContactPortlet.ContactPortlet', array(
                'model' => $model,
                'handleTitle' => $model->fullName,
            ));
		?>
                        </div>
                        <div class="res-col span_4_of_12 p-0 p-fr" id="">
                            <div id="relationships" class="widget g12 p-p0">
                                <h3 class="handle"><em class="i_folder_love"></em>Relationships
                                    <div class="handleButtons"><a id="edit-contact-button" class="text-button add-relationship-button" href="javascript:void(0)"><em class="icon i_stm_add"></em>Add</a></div>
                                </h3>
                                <div class="g99" style="min-height:325px; ">
                                    <div>
                                        <div class="rounded-text-box odd-static g12" style="min-height: 315px;">
                                            <div class="g12">
                                                <table class="container">
                                                    <?php
                                                    foreach($model->relationshipsTo as $relationship) {
                                    echo '<tr>';
                                        echo '<th style="width: 25%; vertical-align: middle;">('.$relationship->originRelationshipType->name.')</th>';
                                        echo '<td><a href="/'.$this->module->id.'/contacts/'.$relationship->relatedContact->id.'" target="_blank">'.$relationship->relatedContact->fullName.'</a></td>';
                                        echo '<td style="width: 80px;"><a class="text-button edit-relationship-button" href="javascript:void(0)" style="display: inline-block; position:relative; right:0;" data-id="'.$relationship->id.'"><em class="icon i_stm_edit"></em>Edit</a></td>';
                                    echo '</tr>';
                                }
                                foreach($model->relationshipsFrom as $relationship) {
                                    echo '<tr>';
                                    echo '<th style="width: 25%; vertical-align: middle;">('.$relationship->relatedRelationshipType->name.')</th>';
                                    echo '<td><a href="/'.$this->module->id.'/contacts/'.$relationship->originContact->id.'" target="_blank">'.$relationship->originContact->fullName.'</a></td>';
                                    echo '<td style="width: 80px;"><a class="text-button edit-relationship-button" href="javascript:void(0)" style="display: inline-block; position:relative; right:0;"  data-id="'.$relationship->id.'"><em class="icon i_stm_edit"></em>Edit</a></td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<?php
// middle-sectionc
$this->renderPartial('_viewMiddle', array('model' => $model));

$this->widget('admin_widgets.ActivityLogPortlet.ActivityLogPortlet', array(
        'parentModel' => $model,
    ));

Yii::import('admin_widgets.DialogWidget.RelationshipDialogWidget.RelationshipDialogWidget');
$this->widget('admin_widgets.DialogWidget.RelationshipDialogWidget.RelationshipDialogWidget', array(
                                                                              'id' => RelationshipDialogWidget::RELATIONSHIP_DIALOG_ID,
                                                                              'title' => 'Add Relationship',
                                                                              'triggerElement' => RelationshipDialogWidget::RELATIONSHIP_TRIGGERS,
                                                                              'contactModel' => $model,
                                                                              )
);