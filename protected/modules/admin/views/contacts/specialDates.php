<?php
	$this->breadcrumbs = array(
		'Special Dates' => array('/'.$this->module->name.'/contacts'),
	);
?>
	<div id="content-header">
<!--		<h1>Special Dates</h1>-->
	</div>

<!--	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">-->
		<?php $this->renderPartial('_listSearchBoxSpecialDates', array(
				'model' => $model,
			)
		); ?>
<!--	</div>-->
<div class="p-clr"></div>
<?php
    echo '<h2>Work Anniversaries</h2>';
    $this->widget('admin_module.components.StmGridView', array(
            'id' => 'work-anniversary-grid',
            'template'=>'{summary}{items}{summary}{pager}',
            'dataProvider' => $dataProvider2,
            'enableSorting'=>true,
            'itemsCssClass' => 'datatables',
            'columns' => array(
                'contact.first_name',
                'contact.last_name',
                array(
                    'type' => 'raw',
                    'name' => 'Work Anniversary',
                    'value' => 'Yii::app()->controller->action->printWorkAnniversary($data)',
                ),
                array(
                    'type' => 'raw',
                    'name' => '',
                    'value' => '
                    "<div><a href=\"/".Yii::app()->controller->module->name."/contacts/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
                ',
                    'htmlOptions' => array('style' => 'width:80px'),
                ),
                array(
                    'type' => 'raw',
                    'name' => '',
                    'value' => '
                    "<div><a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
                ',
                    'htmlOptions' => array('style' => 'width:80px'),
                ),
            ),
        )
    );

echo '<h2>Contacts / Clients / Sphere Special Dates</h2>';

$this->widget('admin_module.components.StmGridView', array(
		'id' => 'contact-grid',
        'template'=>'{summary}{items}{summary}{pager}',
		'dataProvider' => $dataProvider,
        'enableSorting'=>true,
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Contact Info',
                'value' => '"<a href=\"/admin/contacts/".$data->id."\" target=\"_blank\">".$data->first_name.(($data->spouse_first_name)? " & ".$data->spouse_first_name."": "")." ".$data->last_name."</a><br>".Yii::app()->format->formatPrintPhones($data->phones).((Yii::app()->format->formatPrintPhones($data->phones))? "<br>":"").Yii::app()->format->formatPrintEmails($data->emails)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Special Dates & Relationships',
                'value' => 'Yii::app()->controller->action->printSpecialDatesRelationships($data)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Spoke To',
                'value' => 'Yii::app()->format->formatDays($data->lastSpokeDate(), array("defaultValue"=>"<div style=\"color: #D20000; font-weight: bold;\">NONE</div>"))',
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Called',
                'value' => 'Yii::app()->format->formatDays($data->lastCalledDate(), array("defaultValue"=>"<div style=\"color: #D20000; font-weight: bold;\">NONE</div>"))',
            ),
            array(
                'type' => 'raw',
                'name' => 'Add Task',
                'value' => '"<button class=\"add-task-button\" data=\"".$data->id."\" ctid=\"".ComponentTypes::CONTACTS."\">Add Task</button>"',
                'htmlOptions' => array('style' => 'width:85px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Log Activity',
                'value' => '"<button class=\"add-activity-log-button\" data=\"".$data->id."\" ctid=\"".ComponentTypes::CONTACTS."\">Log Activity</button>"',
                'htmlOptions' => array('style' => 'width:115px'),
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
				'htmlOptions' => array('style' => 'width:80px'),
			),
		),
	)
);

Yii::import('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget');
if (!class_exists('ClickToCallDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget', array(
            'id' => ClickToCallDialogWidget::CLICK_TO_CALL_DIALOG_ID,
            'title' => 'Click to Call',
            'triggerElement' => '.click-to-call-button',
        )
    );
}

Yii::import('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget');
$this->widget('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget', array(
        'id' => PhoneDialogWidget::PHONE_DIALOG_ID,
        'title' => 'Phone Actions',
        'triggerElement' => '.phone-status-button',
        //        'parentModel' => $model,
    )
);

// Checks to see if a activity log dialog already exists and creates one only if it doesn't exist.
Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
            'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
            'title' => 'Complete Task & Log Activity',
            'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        )
    );
}

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
if (!class_exists('TaskDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
            'id' => 'task-edit',
            'title' => 'Edit Task',
            'parentModel' => new Contacts,
            'triggerElement' => '.add-task-button',
        )
    );
}