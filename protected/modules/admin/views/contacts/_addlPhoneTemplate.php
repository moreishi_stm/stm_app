<?php
$defaultHtmlOptions = array(
	'class' => 'phoneRow g12 p-p0',
);
if (isset($containerHtmlOptions)) {
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
	$containerHtmlOptions = $defaultHtmlOptions;
}

// Determine which model to use
$inputHtmlOptions = array('placeholder' => '(999) 123-1234', 'class' => 'phoneField');
$phoneTypeOwnerOptions = array('class'=>'g2');
if (isset($model)) { // for actual phone records
	$index = "[$i]";
	$PhoneModel = $model;
	$phoneFieldId = 'Phones_' . $i . '_phone';
} else { // for the phone template base
	$PhoneModel = new Phones;
	$index = '[0]';
	$inputHtmlOptions = CMap::mergeArray($inputHtmlOptions, array(
		'disabled' => 'disabled',
	));
	$phoneFieldId = 'Phones_0_phone';
	$phoneTypeOwnerOptions =  CMap::mergeArray($phoneTypeOwnerOptions, array(
		'disabled' => 'disabled',
	));
}


echo CHtml::openTag('div', $containerHtmlOptions);
?>
<div class="g11">
	<?php $this->widget('StmMaskedTextField', array(
		'model' => $PhoneModel,
		'attribute' => $index . 'phone',
		'mask' => '(999) 999-9999',
		'id' => $phoneFieldId,
		'htmlOptions' => CMap::mergeArray($inputHtmlOptions,array('class'=>'g4')),
	)); ?>
	<?php echo $form->dropDownList($PhoneModel, $index . 'phone_type_ma', $PhoneModel->getPhoneTypes(), $phoneTypeOwnerOptions); ?>
	<?php echo $form->dropDownList($PhoneModel, $index . 'owner_ma', $PhoneModel->getPhoneOwnerTypes(), $phoneTypeOwnerOptions); ?>
	<?php echo $form->textField($PhoneModel, $index . 'extension',CMap::mergeArray(array('placeholder' => 'Ext.'), $phoneTypeOwnerOptions)); ?>

	<div class="g1" style="margin-top: -6px">
		<?php
		switch ($PhoneModel->is_primary) {
			case true:
				$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star'), null); //Remove Primary
				$primaryButtonCssClass = 'remove-primary-phone';
				break;

			case false:
			default:
				$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star_4'), null); //Make Primary
				$primaryButtonCssClass = 'make-primary-phone';
				break;
		}
		echo CHtml::htmlButton($primaryButtonText, $htmlOptions = array(
			'class' => "text $primaryButtonCssClass",
		))
		?>
	</div>
	<div class="g1" style="margin-top: -6px">
		<button type="button" class="text remove-phone"><em class="icon icon-only i_stm_delete"></em></button>
	</div>

	<?php echo $form->hiddenField($PhoneModel, $index . 'id', $inputHtmlOptions); ?>
	<?php echo $form->hiddenField($PhoneModel, $index . 'is_primary', $inputHtmlOptions); ?>
	<?php echo $form->hiddenField($PhoneModel, $index . 'remove', CMap::mergeArray(array('class' => 'remove'), $inputHtmlOptions)); ?>
	<?php echo $form->error($PhoneModel, $index . 'phone'); ?>
</div>
<?php echo CHtml::closetag('div'); ?>
