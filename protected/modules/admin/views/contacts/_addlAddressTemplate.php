<?php
$defaultHtmlOptions = array(
	'class' => 'addressRow g12 p-p0',
);
if (isset($containerHtmlOptions)) {
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
	$containerHtmlOptions = $defaultHtmlOptions;
}

// Determine which model to use
if (isset($model)) { // for actual email records
	$AddressModel = $model;
	$index = "[$i]";
} else { // for the email template base
	$AddressModel = new Addresses;
	$index = '[0]';
}

echo CHtml::openTag('div', $containerHtmlOptions);                            k
?>
<div class="g12">
	<div class="g10">
		<?php echo $form->textField($AddressModel, $index . 'address', $htmlOptions = array(
			'placeholder' => 'Address',
			'style' => 'width: 93%'
		)); ?>
		<?php echo $form->error($AddressModel, $index . 'address'); ?>
	</div>
	<div class="g1" style="margin-top: -6px">
		<?php
		switch ($AddressModel->isPrimary) {
			case true:
				$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star'), null); //Remove Primary Button
				$primaryButtonCssClass = 'remove-primary-address';
				break;

			case false:
			default:
				$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star_4'), null); //Make Primary
				$primaryButtonCssClass = 'make-primary-address';
				break;
		}
		echo CHtml::htmlButton($primaryButtonText, $htmlOptions = array(
			'class' => "text icon-only $primaryButtonCssClass",
		))
		?>
	</div>
	<div class="g1" style="margin-top: -6px">

		<button type="button" class="text icon-only remove-address"><em class="icon icon-only i_stm_delete"></em>
		</button>
	</div>
</div>
<div class="g4">
	<?php echo $form->textField($AddressModel, $index . 'city', $htmlOptions = array(
		'placeholder' => 'City',
	)); ?>
	<?php echo $form->error($AddressModel, $index . 'city'); ?>
</div>
<div class="g2 p-ml10 p-tc">
	<?php echo $form->dropDownList($AddressModel, $index . 'state_id', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name asc')), 'id', 'short_name'), $htmlOptions = array(
		'empty' => 'State',
		'style' => 'max-width:90%',
	)); ?>
	<?php echo $form->error($AddressModel, $index . 'state_id'); ?>
</div>
<div class="g3 p-ml0">
	<?php echo $form->textField($AddressModel, $index . 'zip', $htmlOptions = array(
		'placeholder' => 'Zip',
	)); ?>
	<?php echo $form->error($AddressModel, $index . 'zip'); ?>
	<?php echo $form->hiddenField($AddressModel, $index . 'isPrimary'); ?>
	<?php echo $form->hiddenField($AddressModel, $index . 'id'); ?>
	<?php echo $form->hiddenField($AddressModel, $index . 'remove', $htmlOptions = array('class' => 'remove')); ?>

</div>
<div class="g12 p-pl8">
	<div class="g4" style="margin-top: -6px">


	</div>
</div>
<?php echo CHtml::closetag('div'); ?>
