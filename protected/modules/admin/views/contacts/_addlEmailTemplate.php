<?php
$defaultHtmlOptions = array(
	'class' => 'emailRow g12 p-p0',
);
if (isset($containerHtmlOptions)) {
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
	$containerHtmlOptions = $defaultHtmlOptions;
}

// Determine which model to use
$inputHtmlOptions = array('placeholder' => 'E-mail');

if (isset($model)) {
	// for actual email records
	$index = "[$i]";
	$EmailModel = $model;
} else {
	// for the email template base
	$EmailModel = new Emails;
	$index = '[placeholder]';
	$inputHtmlOptions = CMap::mergeArray($inputHtmlOptions, array(
		'disabled' => 'disabled',
	));
}

echo CHtml::openTag('div', $containerHtmlOptions);
?>
<div class="g9">
	<?php echo $form->textField($EmailModel, $index . 'email', CMap::mergeArray(array('class' => 'email-address g8','autocomplete'=>'off'), $inputHtmlOptions)); ?>
    <?php echo $form->dropDownList($EmailModel, $index . 'email_status_id', array(''=>'',EmailStatus::OPT_OUT=>'Opt-Out'), CMap::mergeArray(array('class' => 'g3','autocomplete'=>'off'), $inputHtmlOptions)); ?>
    <?php $statusAlert = '';
        if($EmailModel->email_status_id == EmailStatus::OPT_OUT) {
            $statusAlert = 'opted-out';
        } elseif($EmailModel->email_status_id == EmailStatus::HARD_BOUNCE) {
            $statusAlert = 'bounced';
        }
        if($statusAlert) {
            echo '<div class="errorMessage">This email has '.$statusAlert.' and will not receive emails.</div>';
        }
    ?>
	<?php echo $form->error($EmailModel, $index . 'email', $htmlOptions=array(
		'validateOnType' => true,
	)); ?>
	<?php echo $form->hiddenField($EmailModel, $index . 'id', $inputHtmlOptions); ?>
	<?php echo $form->hiddenField($EmailModel, $index . 'is_primary', $inputHtmlOptions); ?>
	<?php echo $form->hiddenField($EmailModel, $index . 'remove', CMap::mergeArray(array('class' => 'remove'), $inputHtmlOptions)); ?>
</div>
<div class="g2 p-pl8">
	<div class="g6" style="margin-top: -6px">
		<?php
		switch ($EmailModel->is_primary) {
			case true:
				//By default we'll not show the remove primary button it will be implied by
				//marking another email address as primary.
				$primaryButtonText = CHtml::tag('em', $htmlOptions = array(
					'class' => 'icon icon-only i_stm_star', 'style' => 'dislay:none;'
				), null); //Remove Primary
				$primaryButtonCssClass = 'remove-primary-email';
				break;
			case false:
				$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star_4'), null); //Make Primary
				$primaryButtonCssClass = 'make-primary-email';
				break;
		}
		echo CHtml::htmlButton($primaryButtonText, $htmlOptions = array(
			'class' => "text $primaryButtonCssClass",
		))
		?>
	</div>
	<div class="g6" style="margin-top: -6px">
		<button type="button" class="text remove-email"><em class="icon icon-only i_stm_delete"></em></button>
	</div>
</div>
<?php echo CHtml::closetag('div'); ?>
