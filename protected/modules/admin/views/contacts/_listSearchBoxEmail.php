<?php
    $module = Yii::app()->controller->module->id;
	Yii::app()->clientScript->registerScript('emailSearch', <<<JS
    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('emails-grid', { data: $(this).serialize() });
        return false;
    });
JS
);

	$form = $this->beginWidget('CActiveForm', array(
			'action' => Yii::app()->createUrl($this->route),
			'method' => 'get',
		)
	); ?>
<!--<div class="g4">-->
<!--	<label class="g3">Name:</label>-->
<!--	<span class="g4">--><?php //echo $form->textField($model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')); ?><!--</span>-->
<!--	<span class="g4 p-ml10">--><?php //echo $form->textField($model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')); ?><!--</span>-->
<!--</div>-->
<div class="g2"></div>
<div class="g3">
    <label class="g4">Status:</label>
    <span class="g8"><?php echo $form->dropDownList($model, 'email_status_id', CHtml::listData(EmailStatus::model()->findAll(),'id','name'), array('class' => 'g12')); ?></span>
</div>
<div class="g3">
	<label class="g3">Email:</label>
	<span class="g9"><?php echo $form->textField($model, 'email', $htmlOptions = array('placeholder' => 'Email')); ?></span>
</div>
<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>
<?php $this->endWidget(); ?>
