<?php
$this->breadcrumbs = array(
    'Mass Update by Email' => '',
);

$inputHtmlOptions = array(
    'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g5',
);
?>
<h1>Mass Update by Email</h1>
<h3 style="margin: 50px;">Enter the list of emails (1 per line) in the box below and select values in the dropdown that match the action you would like to take. You will receive a Summary Report in your email once it is complete confirming the status of each update.</h3>

<?
$form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'post',
        'id' => 'reconcile-contacts-form',
    )
); ?>
<div class="g12 p-mb5 rounded-text-box odd-static">
    <div style="padding: 10px;">
        <div class="g3">
            <label class="g3">Action:</label>
            <span class="g9">
                <select name="action" id="action">
                    <option value="-1" selected="selected">Select One</option>
                    <option value="0">Assign Users</option>
					<option value="1">Update Lead Status</option>
					<option value="2">Add Tag</option>
					<option value="3">Remove Tag</option>
                </select>
            </span>
        </div>

        <div class="g3">
            <label class="g3">Lead Type:</label>
            <span class="g9">
                <select name="componentType" id="componentType">
                    <option value="0" selected="selected">Select One</option>
                    <?if(Yii::app()->user->checkAccess('transactions')):?>
                    <option value="<?php echo ComponentTypes::BUYERS;?>">Buyer</option>
                    <option value="<?php echo ComponentTypes::SELLERS;?>">Seller</option>
                    <?endif;?>
                    <option value="<?php echo ComponentTypes::RECRUITS;?>">Recruit</option>
					<option value="<?php echo ComponentTypes::CONTACTS;?>">Contact</option>
                </select>
            </span>
        </div>

        <div class="g3" id="buyer_assignment_type_container" style="display:none;">
            <label class="g4">Assignments:</label>
            <span class="g8">
                <?php echo CHtml::dropDownList(
                    'buyer_assignment_type_id',
                    $_GET['assignment_type_id'],
                    CHtml::listData(AssignmentTypes::model()->findByComponentTypeId(ComponentTypes::BUYERS), 'id', 'display_name'),
                    $inputHtmlOptions
                );?>
            </span>
        </div>

        <div class="g3" id="sellers_assignment_type_container" style="display:none;">
            <label class="g3">Assignments:</label>
            <span class="g9">
                <?php echo CHtml::dropDownList(
                    'seller_assignment_type_id',
                     $_GET['assignment_type_id'],
                    CHtml::listData(AssignmentTypes::model()->findByComponentTypeId(ComponentTypes::SELLERS), 'id', 'display_name'),
                    $inputHtmlOptions
                );?>
            </span>
        </div>

        <div class="g3" id="recruits_assignment_type_container" style="display:none;">
            <label class="g3">Assignments:</label>
            <span class="g9">
                <?php echo CHtml::dropDownList(
                    'recruit_assignment_type_id',
                    $_GET['assignment_type_id'],
                    CHtml::listData(AssignmentTypes::model()->findByComponentTypeId(ComponentTypes::RECRUITS), 'id', 'display_name'),
                    $inputHtmlOptions
                );?>
            </span>
        </div>

        <div class="g3" id="assign_to">
            <label class="g4">Value:</label>
            <span class="g8">
                 <?php echo CHtml::dropDownList(
                     'assign_to_user',
                     $_GET['contact_id'],
                     CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), array('empty'=>'','class'=>'chzn-select g12','data-placeholder'=>'Select Name', 'empty' => 'Select Name', 'style' => 'display: none')
                 );

                echo CHtml::dropDownList('assign_to_lead_status', $transactionStatusSelected, $transactionStatus,
                    $htmlOptions = array('empty'            => '',
                        'class'            => 'chzn-select',
                        'data-placeholder' => 'Status',
                        'style' => 'display: none;',
                        'empty' => "Select One"
                    )
                ); ?>
                <span id="tag_container" style="display: none;">
                     <?php
                     echo CHtml::dropDownList(
                         'MassAction[options]',
                         $models->typesCollection,
                         $tagOptions,
                         array(
                             'empty' => ' ',
                             'class' => 'chzn-select',
                             'multiple' => 'multiple',
                             'style' => 'width:100%;',
                             'data-placeholder' => 'Select Tags',
                             'id' => "MassAction_tag_options"
                         )
                     );
                     ?>
                     <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#MassAction_tag_options','options' => array('width'=>'80%'))); ?>
                </span>
            </span>
        </div>

        <div class="clearfix""></div>

        <div calss="g12">
            <span>* PLEASE ENTER 1 EMAIL PER LINE</span>
            <textarea name="email_list" id="email_list" rows=30></textarea>
        </div>

        <div calss="g12">
            <?php echo CHtml::submitButton('Go', $htmlOptions=array('class'=>'p-fr button gray date-ranger-go-button','style'=>'font-size:10px;top:-4px;')); ?>
        </div>
    </div>
</div>
<?php $this->endWidget();

$message = "";
if(!empty($messages)) {
    $message = "";
}

$sellers = ComponentTypes::SELLERS;
$buyers = ComponentTypes::BUYERS;
$recruits = ComponentTypes::RECRUITS;

Yii::app()->clientScript->registerScript('assignToChanger', <<<JS
    $('#action').on('change', function() {
    	$("#componentType").removeClass("error");
    	$("#tag_container").hide();
		//$("#tags_container a.search-choice-close").click();

        if($(this).val() == 0) {
            $("#assign_to_user").show();
            $("#assign_to_lead_status").hide();
        } else if($(this).val() == 1) {
            $("#assign_to_user").hide();
            $("#assign_to_lead_status").show();
            $("#buyer_assignment_type_container").hide();
            $("#sellers_assignment_type_container").hide();
            $("#recruits_assignment_type_container").hide();
        } else if ( $(this).val() == 2 || $(this).val() == 3){
            $("#assign_to_user").hide();
        	$("#tag_container").show();
            $("#assign_to_lead_status").hide();
            $("#buyer_assignment_type_container").hide();
            $("#sellers_assignment_type_container").hide();
            $("#recruits_assignment_type_container").hide();
            if($('#MassAction_tag_options').val() != null) {
        	    massActionTagsUpdate();
            }
        } else {
            $("#assign_to_user").hide();
            $("#assign_to_lead_status").hide();
            $("#buyer_assignment_type_container").hide();
            $("#sellers_assignment_type_container").hide();
            $("#recruits_assignment_type_container").hide();
        }
    });

    $('#componentType').on("change", function() {
        if($('#action').val() == 0) {
            if($(this).val() == {$buyers}) {
                $("#buyer_assignment_type_container").show();
                $("#sellers_assignment_type_container").hide();
                $("#recruits_assignment_type_container").hide();
            }

            if($(this).val() == {$sellers}) {
                $("#sellers_assignment_type_container").show();
                $("#buyer_assignment_type_container").hide();
                $("#recruits_assignment_type_container").hide();
            }

            if($(this).val() == {$recruits}) {
                $("#sellers_assignment_type_container").hide();
                $("#buyer_assignment_type_container").hide();
                $("#recruits_assignment_type_container").show();
            }
        } else if ( $('#action').val() == 2 || $('#action').val() == 3 ){
        	massActionTagsUpdate();
        } else {
            $("#buyer_assignment_type_container").hide();
            $("#sellers_assignment_type_container").hide();
        }
    });

    $("#reconcile-contacts-form").submit(function() {

        if($("#email_list").val().length < 1) {
            Message.create("error", "Please enter at min. 1 email.");
            return false;
        }

        if($("#action").val() == "-1") {
            Message.create("error", "Please select an Action.");
            return false;
        }

        if($("#componentType").val() == 0) {
            Message.create("error", "Please select a Lead Type.");
            return false;
        }

        if($("#action").val() == 0) {
            if($("#componentType").val() == {$buyers}) {
                if($("#buyer_assignment_type_id").val() == "") {
                    Message.create("error", "Please select an Assignment.");
                    return false;
                }
            }

            if($("#componentType").val() == {$sellers}) {
                if($("#seller_assignment_type_id").val() == "") {
                    Message.create("error", "Please select an Assignment.");
                    return false;
                }
            }

            if($("#assign_to_user").val() == "") {
                Message.create("error", "Please select a Value.");
                return false;
            }
        }

        if($("#action").val() == 1) {
            if($("#assign_to_lead_status").val() == "") {
                Message.create("error", "Please select a Value.");
                return false;
            }
        }
    });

	function massActionTagsUpdate(){
		$("#componentType").removeClass("error");
		//$("#tags_container a.search-choice-close").click();

		var componentType = $("#componentType").val();
		if(!componentType || componentType == 0 ){
			Message.create("error", "Please select an Lead Type.");
			$("#componentType").addClass("error");
		}else if (componentType == {$buyers} || componentType == {$sellers}){
			Message.create("error", "You can not add tags to buyers or sellers at this time.");
			$("#componentType").val(0);
			$("#componentType").addClass("error");
		}else{
			$.post( "/admin/contacts/getTags", { "componentId": componentType }, function( data ) {
				console.log( data.status ); // John
				if(data.tags){
					var html = '';
					$.each(data.tags , function ( index , tag ) {
						html = html + '<option value="'+index+'">'+tag+'</option>';
					});
					$("#MassAction_tag_options").html(html).trigger("liszt:updated");
				}
			}, "json");
		}
	}

JS
);
?>