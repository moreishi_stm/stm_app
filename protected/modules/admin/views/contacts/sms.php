<!--<script type="javascript" src="https://s3.amazonaws.com/plivosdk/web/plivo.min.js"></script>-->
<!--<script src="https://static.twilio.com/libs/twiliojs/1.1/twilio.js"></script>-->

<?php
	$moduleName = Yii::app()->controller->module->id;
    $js = <<<JS
		$('#sms-form').submit(function() {

			dialNow();

			$('#call-container').show("normal");
			$('#hang-up').click(function(){
				$('#call-container').removeClass('green').addClass('blue');
				$('#call-status').html('Call Ended:');
				$(this).hide();
				$('#redial').show();
			});
			$('#redial').click(function(){
				$('#call-container').removeClass('blue').addClass('green');
				$('#call-status').html('Making outgoing call:');
				$(this).hide();
				$('#hang-up').show();
				dialNow();
			});

			$.post('/$moduleName/contacts/sms', $('#sms-form').serialize(), function(data) {
				if(data=='success') {
					Message.create('success', 'Text message successfully sent!');
				} else if (data=='error') {
					Message.create('error', 'Error! Text message not sent.');
				}
			});
			return false;
		});

		function dialNow(){
			// Assumes dial on submit
			Plivo.onWebrtcNotSupported = webrtcNotSupportedAlert;
			Plivo.onReady = onReady;
			Plivo.init();

			var username = 'testFrom130903042324';
			var pass = 'testFrom';
			Plivo.conn.login(username, pass);

			var dest = "clee130902023454@phone.plivo.com";
			Plivo.conn.call(dest);
		}

		function dialTwilio() {
			Twilio.Device.setup(token);
			Twilio.Device.connect();
			//you decide what happens next
		}

		function dialPlivo() {

		}
JS;

//	Yii::app()->clientScript->registerScript('smsScript', $js);


	$this->breadcrumbs = array(
		'SMS' => '',
	);

?>
<h1>Send SMS</h1>
<div id="call-container" class="alert i_phone_hook green" style="display: none;"><span id="call-status">Making outgoing call:</span> Christine Lee (904) 280-3800.
	<a href="javascript:void(0)" id="hang-up" class="p-ml10 button gray icon i_stm_delete grey-button">Hang Up</a>
	<a href="javascript:void(0)" id="redial" class="p-ml10 button gray icon i_stm_add grey-button" style="display: none;">Redial</a>
</div>
<?php $form = $this->beginWidget('CActiveForm', array(
												'id' => 'sms-form',
												'enableAjaxValidation' => false,
												));
$this->beginStmPortletContent(array(
							  'handleTitle' => 'Send SMS',
							  'handleIconCss' => 'i_user'
							  ));

?>
<div class="g12 p-mb5 rounded-text-box notes odd-static">

	<table class="container">
		<tr>
			<th>Phone #:</th>
			<td>
				<?php echo CHtml::textField('to_phone', null, $htmlOptions = array('placeholder' => 'Phone #', 'class' => 'g5')); ?>
			</td>
		</tr>
	</table>
</div>

<?php
$this->endStmPortletContent();
echo '<div class="p-mc p-tc">'.CHtml::submitButton('Send SMS', $htmlOptions=array('class'=>'submit wide','id'=>'sms-submit-button')).'</div>';
$this->endWidget();