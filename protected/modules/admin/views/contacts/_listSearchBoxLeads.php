<?php
$form = $this->beginWidget(
    'CActiveForm', array(
        'method' => 'get',
        'id'     => 'transaction-list-search',
    )
);
?>
<div class="g12">
    <div class="g4">
        <div class="g3 text-right"><label>Lead Type:</label></div>
        <div class="g9" style="padding-top: 12px; font-size: 13px;">
            <?php echo CHtml::radioButton('transactionType',((empty($_GET['type']) || $_GET['type']==1)? true: false) ,$htmlOptions=array(
                    'value'=>1,
                    'uncheckValue'=>"",
                    'style'=>'margin-left: 20px;',
                    'id'=>'transactionTypeBoth',
                    'class'=>'transactionType'
                )); ?> Both
            <?php echo CHtml::radioButton('transactionType', (($_GET['type']==ComponentTypes::SELLERS)? true: false) ,$htmlOptions=array(
                    'value'=>ComponentTypes::SELLERS,
                    'uncheckValue'=>"",
                    'style'=>'margin-left: 20px;',
                    'id'=>'transactionTypeSellers',
                    'class'=>'transactionType'
                )); ?> Sellers
            <?php echo CHtml::radioButton('transactionType', (($_GET['type']==ComponentTypes::BUYERS)? true: false) ,$htmlOptions=array(
                    'value'=>ComponentTypes::BUYERS,
                    'uncheckValue'=>"",
                    'style'=>'margin-left: 20px;',
                    'id'=>'transactionTypeBuyers',
                    'class'=>'transactionType'
                )); ?> Buyers
            <?php echo CHtml::hiddenField('transactionTypeId', $_GET['type']); ?>


        </div>
    </div>
    <div class="g8">
        <div class="g1 text-right"><label>Status:</label></div>
        <div class="g11">
            <?php echo CHtml::dropDownList('transactionStatusIds', $transactionStatusSelected, CHtml::listData(TransactionStatus::model()->byComponentType(array(ComponentTypes::BUYERS, ComponentTypes::SELLERS))->defaultSort()->findAll(), 'id', 'name'),
                $htmlOptions = array('empty'            => '',
                                     'class'            => 'chzn-select',
                                     'multiple'         => 'multiple',
                                     'style'            => 'width:100%;',
                                     'data-placeholder' => 'Status'
                )
            );
            ?>
            <a class="status-unselect-all" href="javascript:void(0)">Unselect All</a>
            <a class="status-select-abc" style="margin-left: 12px;" href="javascript:void(0)">Select A, B, C</a>
            <a class="status-select-a" style="margin-left: 12px;" href="javascript:void(0)">Select A</a>
            <a class="status-select-b" style="margin-left: 12px;" href="javascript:void(0)">Select B</a>
            <a class="status-select-c" style="margin-left: 12px;" href="javascript:void(0)">Select C</a>
            <a class="status-select-listed" style="margin-left: 12px;" href="javascript:void(0)">Select Listed</a>
            <a class="status-select-coming-soon" style="margin-left: 12px;" href="javascript:void(0)">Select Coming Soon</a>
        </div>
        <?php $this->widget(
            'admin_module.extensions.EChosen.EChosen', array('target' => 'select#transactionStatusIds','options' => array('width'=>'100%')
            )
        ); ?>
    </div>
</div>
<div class="g12">
    <div class="g4">
        <div class="g3 text-right"><label>Name:</label></div>
        <div class="g4">
            <?php echo CHtml::textField('firstName', null, $htmlOptions = array('placeholder' => 'First Name')); ?>
        </div>
        <div class="g4 p-ml8">
            <?php echo CHtml::textField('lastName', null, $htmlOptions = array('placeholder' => 'Last Name')); ?>
        </div>
    </div>
    <div class="g3">
        <div class="g3 text-right"><label>Email:</label></div>
        <div class="g9">
            <?php echo CHtml::textField('email', null, $htmlOptions = array('placeholder' => 'Email')); ?>
        </div>
    </div>
    <div class="g3">
        <div class="g4 text-right"><label>Phone:</label></div>
        <div class="g8">
            <?php $this->widget(
                'StmMaskedTextField', array(
                    'name'   => 'phone',
                    'mask'        => '(999) 999-9999',
                    //Makes the full 10 digit phone number optional (enables partial search)
                    'htmlOptions' => array(
                        'placeholder' => '(999) 123-1234',
                        'class'       => 'phoneField g11'
                    )
                )
            ); ?>
        </div>
    </div>
    <div class="g1 submit">
        <?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?>
    </div>
</div>

<div class="g12">
    <div class="g4">
        <div class="g3 text-right"><label>Address:</label></div>
        <div class="g8">
            <?php echo CHtml::textField('address', null, $htmlOptions = array('placeholder' => 'Address')); ?>
        </div>
    </div>
    <div class="g6" style="margin-left: 2%;">
        <div class="g1 text-right"><label>Sort:</label></div>
        <div class="g5">
            <?php echo CHtml::dropDownList('dateSearchFilter', null, $this->action->dateSearchTypes, $htmlOptions = array(
                    'empty' => '',
                    'style'=>'width:93%;',
                )
            ); ?>
        </div>
        <div class="g6">
            <?php $this->widget(
                'zii.widgets.jui.CJuiDatePicker', array(
                    'name'   => 'fromSearchDate',
                    'options'     => array(
                        'showAnim'       => 'fold',
                        'dateFormat'     => 'mm/dd/yy',
                        'numberOfMonths' => 2,
                    ),
                    'htmlOptions' => array(
                        'style' => 'width: 40%; float: left; font-size: 12px;',
                        'placeholder'=>'From Date',
                    ),
                )
            );
            ?>
            <label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
            <?php $this->widget(
                'zii.widgets.jui.CJuiDatePicker', array(
                    'name'   => 'toSearchDate',
                    'options'     => array(
                        'showAnim'       => 'fold',
                        'dateFormat'     => 'mm/dd/yy',
                        'numberOfMonths' => 2,
                    ),
                    'htmlOptions' => array(
                        'style' => 'width: 40%; float: left; font-size: 12px;',
                        'placeholder'=>'To Date',
                    ),
                )
            );
            ?>
        </div>
    </div>
    <?php if(Yii::app()->user->checkAccess('owner')):
        echo CHtml::hiddenField('export','0');
        ?>
        <div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('Export', array('class' => 'button export-button gray')); ?></div>
    <?php endif; ?>
</div>
<div class="g12">
    <div class="g4">
        <div class="g3 text-right"><label>Assigned to:</label></div>
        <div class="g9">
            <?php

            $transactionStatusIds = TransactionStatus::NEW_LEAD_ID.','.TransactionStatus::HOT_A_LEAD_ID.','.TransactionStatus::B_LEAD_ID.','.TransactionStatus::C_LEAD_ID.','.TransactionStatus::D_NURTURING_SPOKE_TO_ID.','.TransactionStatus::NURTURING_ID;

            if((!Yii::app()->user->checkAccess('owner') && Yii::app()->user->settings->show_assigned_leads_only)) {
                $AssignmentIdListData = array(Yii::app()->user->id => Yii::app()->user->contact->fullName);
            }
            else {
                $contactIds = Yii::app()->db->createCommand("SELECT DISTINCT c.id FROM contacts c JOIN auth_assignment a ON c.id=a.userid AND a.itemname='inactive' JOIN assignments aa ON aa.component_type_id IN (2,3) AND aa.assignment_type_id IN (".AssignmentTypes::BUYER_AGENT.','.AssignmentTypes::CO_BUYER_AGENT.','.AssignmentTypes::LISTING_AGENT.','.AssignmentTypes::CO_LISTING_AGENT.") JOIN transactions t ON t.id=aa.component_id AND t.transaction_status_id IN ({$transactionStatusIds})")->queryColumn();

                $AssignmentIdListData = CHtml::listData(Contacts::model()->byActiveAdmins(false, $contactIds)->byAssignmentComponentType(array(ComponentTypes::BUYERS,ComponentTypes::SELLERS))->orderByName()->findAll(), 'id','fullPhoneticName');
            }
            echo CHtml::dropDownList('assignmentId',($_GET['myLeads'])? $_GET['myLeads'] : null,  $AssignmentIdListData, $htmlOptions = array('empty'=> '','data-placeholder'=>' '));

            $this->widget('admin_module.extensions.EChosen.EChosen', array(
                    'target'  => 'select#assignmentId',
                    'options' => array(
                        'allow_single_deselect' => true,
                        'width'                 => '93%'
                    )
                )
            );
            ?>
        </div>
    </div>
    <div class="g3">
        <div class="g3 text-right"><label>Source:</label></div>
        <div class="g9">
            <?php echo CHtml::dropDownList('sourceId', null,  CHtml::listData(Sources::model()->findAll(), 'id','name'), $htmlOptions = array('empty'=> '', 'data-placeholder'=>' '));

            $this->widget('admin_module.extensions.EChosen.EChosen', array('target'  => 'select#sourceId', 'options' => array('allow_single_deselect' => true, 'width' => '100%'))); ?>
        </div>
    </div>
    <div class="g1">
        <div class="g11 text-right"><label>No Action Plan:</label></div>
        <div class="g1">
            <?php echo CHtml::checkbox('noActionPlanCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('noActionPlan', null); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g11 text-right"><label>Live Now:</label></div>
        <div class="g1">
            <?php echo CHtml::checkbox('liveNowCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('liveNow', null); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g10 text-right"><label>Opt Out Emails:</label></div>
        <div class="g2">
            <?php echo CHtml::checkbox('optOutBadEmailsCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('optOutBadEmails', null); ?>
        </div>
    </div>

</div>
<div class="g12">
    <div class="g1">
        <div class="g11 text-right"><label>Has Phone:</label></div>
        <div class="g1">
            <?php echo CHtml::checkbox('hasPhoneCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('hasPhone', null); ?>
        </div>
    </div>
    <div class="g2" style="margin-left: 40px;">
        <div class="g8 text-right"><label>No Phone Call:</label></div>
        <div class="g4">
            <?php echo CHtml::checkbox('noPhoneCallCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('noPhoneCall', null); ?>
        </div>
    </div>
    <div class="g1">
        <div class="g11 text-right"><label>No Activity:</label></div>
        <div class="g1">
            <?php echo CHtml::checkbox('noActivityCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('noActivity', null); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g11 text-right"><label>No Saved Search:</label></div>
        <div class="g1">
            <?php echo CHtml::checkbox('noSavedSearchCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('noSavedSearch', null); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g10 text-right"><label>Not Spoke to:</label></div>
        <div class="g2">
            <?php echo CHtml::checkbox('notSpokeToCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('notSpokeTo', null); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g10 text-right"><label>Last Follow-up Days:</label></div>
        <div class="g2">
            <?php echo CHtml::dropDownList('lastActivity', null, array(''  => '', 7 => '7+', 14 => '14+', 30 => '30+', 45 => '45+', 60 => '60+', 90 => '90+', 120 => '120+', 150 => '150+', 180 => '180+'), $htmlOptions = array()); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g10 text-right"><label>Has Appointment:</label></div>
        <div class="g2">
            <?php echo CHtml::checkbox('hasAppointmentCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('hasAppointment', null); ?>
        </div>
    </div>
    <div class="g4">
        <div class="g3 text-right"><label>Dialer List:</label></div>
        <div class="g8">
            <? $callListCriteria = new CDbCriteria();
                $callListCriteria->addInCondition('preset_ma', array(CallLists::CUSTOM_SELLERS, CallLists::CUSTOM_BUYERS));
            $callListCriteria->addCondition('is_deleted=0');
            ?>
            <?php echo CHtml::dropDownList('callListId', null, CHtml::listData(CallLists::model()->findAll($callListCriteria), 'id', 'name'), $htmlOptions = array('empty'=>' ','class'=>'g100', 'data-placeholder'=>' ')); ?>
        </div>
    </div>
    <div class="g4">
        <div class="g3 text-right"><label>Transaction Tags:</label></div>
        <div class="g9">
            <?php echo CHtml::dropDownList('tags', null, CHtml::listData(TransactionTags::model()->findAll(array('order'=>'name')), 'id', 'name'), $htmlOptions = array('empty'=>' ','class'=>'g100', 'data-placeholder'=>' ','multiple' => 'multiple')); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g10 text-right"><label>No Tasks:</label></div>
        <div class="g2">
            <?php echo CHtml::checkbox('noTasksCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('noTasks', null); ?>
        </div>
    </div>
    <div class="g2">
        <div class="g10 text-right"><label>No Assignment:</label></div>
        <div class="g2">
            <?php echo CHtml::checkbox('noAssignmentCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
            <?php echo CHtml::hiddenField('noAssignment', null); ?>
        </div>
    </div>

    <div class="g4">
        <div class="g3 text-right"><label>Applied Action Plans:</label></div>
        <div class="g8">
            <?
            $actionPlans2Criteria = new CDbCriteria();
            $actionPlans2Criteria->addInCondition('component_type_id', array(ComponentTypes::SELLERS, ComponentTypes::BUYERS));
            $actionPlans2Criteria->order = 'name';

            $actionPlanResults = Yii::app()->db->createCommand()
                ->select('ap.id, ap.name')
                ->from('action_plans ap')
                ->where("(SELECT count(*) FROM tasks tsk LEFT JOIN action_plan_applied_lu lu ON tsk.id=lu.task_id LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id LEFT JOIN action_plans ap2 ON ap2.id=i.action_plan_id WHERE tsk.component_type_id IN (2,3) AND tsk.complete_date IS NULL AND tsk.is_deleted=0) > 0")
                ->order('name ASC')
                ->queryAll();
            $actionPlanAppliedList = array_column($actionPlanResults,'name','id');
            ?>
            <?php echo CHtml::dropDownList('actionPlanId', null, $actionPlanAppliedList, $htmlOptions = array('empty'=>' ', 'class'=>'g100', 'data-placeholder'=>' ')); ?>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => '#massActionComponentId, #actionPlanId, #callListId, #tags','options' => array('enable_split_word_search'=>true, 'allow_single_deselect' => true))); ?>
        </div>
    </div>

    <?php if(Yii::app()->user->checkAccess('owner')):?>
    <div class="g4">
        <div class="g3 text-right"><label>Mass Action:</label></div>
        <div class="g9">
            <?php
			echo CHtml::dropDownList(
				'massActionType',
				null,
				array('unapplyActionPlans'=>'Unapply Action Plans',"addTag"=>"Add Tags","removeTag"=>"Remove Tags","updateLeadStatus"=>"Update Status"), //'applyActionPlans'=>'Apply Action Plans',
				$htmlOptions = array(
					'empty'=>'Select an action',
					'class'=>'g9',
					'data-placeholder'=>'Select an action'
				));
			?>
        </div>
		<div class="g12" id="MassAction_action_plan_options" style="display:none;">
			<div class="g3">&nbsp;</div>
			<span class="g9">
				<?php
				$actionPlansCriteria = new CDbCriteria();
				$actionPlansCriteria->addInCondition('component_type_id', array(ComponentTypes::SELLERS, ComponentTypes::BUYERS));
				$actionPlansCriteria->order = 'name';
				?>
				<?php echo CHtml::hiddenField('massActionProcess', null, $htmlOptions=array('value'=>'0'))?>

                <?php echo CHtml::dropDownList('massActionComponentId', null, CHtml::listData(ActionPlans::model()->findAll($actionPlansCriteria), 'id', 'name'), $htmlOptions = array('empty'=>'', 'data-placeholder'=>'Select an Action Plan')); ?>
				<?php
				$this->widget('admin_module.extensions.EChosen.EChosen',
					array(
						'target' => 'select#massActionComponentId',
						'options' =>
							array(
                                'allow_single_deselect' => true,
                                )
					)
				); ?>
				<?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button mass-action-button ajaxMassAction g1','style'=>'font-size: 10px; padding: 8px 10px;'))?>
			</span>
		</div>
		<div id="MassAction_tag_options" class="g12" style="display: none;">
			<div class="g3">&nbsp;</div>
			<span class="g9">
				<?php
				echo CHtml::dropDownList(
					'MassAction[tags]',
					array(),
					CHtml::listData(TransactionTags::model()->findAll(), 'id', 'name'),
					array(
						'empty' => ' ',
						'class' => 'chzn-select',
						'multiple' => 'multiple',
						'style' => '',
						'data-placeholder' => 'Transaction Tags',
						'id' => "MassAction_tags"
					)
				);
				?>
				<?php $this->widget('admin_module.extensions.EChosen.EChosen',
					array('target' => 'select#MassAction_tags','options' => array('width'=>'80%')
					)
				); ?>
				<button type="button" class="ajaxMassAction gray button">Go</button>
		</span>
		</div>
		<div id="MassAction_action_status_options" class="g12" style="display: none;">
			<div class="g3">&nbsp;</div>
			<span class="g8 massActionHidden">
					<?php
					echo CHtml::dropDownList(
						'MassAction[status_options]',
						$models->typesCollection,
						CHtml::listData(TransactionStatus::model()->byComponentType(ComponentTypes::RECRUITS)->defaultSort()->findAll(), 'id', 'name'),
						array(
							'empty' => '',
							'class' => 'chzn-select',
							'style' => 'width:100%;',
							'data-placeholder' => 'Status',
							'id' => "MassAction__status_options"
						)
					);
					?>
				<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#MassAction__status_options','options' => array('width'=>'80%'))); ?>
				<button type="button" class="ajaxMassAction">Go</button>
			</span>
		</div>




    </div>
    <?endif;?>

<hr class="g12" style="padding: 0; min-height: 5px;" />
    <div class="g12" style="font-weight: bold; font-size: 20px; text-align: center; color: #666;">Reverse Prospecting</div>
    <div class="g3">
        <div class="g2 text-right"><label>Zip:</label></div>
        <div class="g10">
            <?php echo CHtml::textField('reverseZip', null, $htmlOptions=['placeholder' => 'Zip Codes (Ex. 11111,22222,33333)','class'=>'g12']); ?>
        </div>
    </div>
    <div class="g3">
        <div class="g2 text-right"><label>City:</label></div>
        <div class="g10">
            <?php echo CHtml::textField('reverseCity', null, $htmlOptions=['placeholder' => 'Exact City (Ex. City1,City2,City3)','class'=>'g12']); ?>
        </div>
    </div>
    <div class="g3">
        <div class="g2 text-right"><label>Price:</label></div>
        <span class="g4"><?php echo CHtml::textField('reversePriceMin', null, $htmlOptions=['placeholder' => 'Price Min']);?></span>
        <span class="g1 p-tc p-mt10">to</span>
        <span class="g4"><?php echo CHtml::textField('reversePriceMax', null, $htmlOptions=['placeholder' => 'Price Max']);?></span>
    </div>

    <div class="g2">
        <?php echo CHtml::dropDownList('reverseDays', null, array(null=>'Viewed within 120 Days', '90'=>'Viewed within 90 Days', '60'=>'Viewed within 60 Days', '30'=>'Viewed within 30 Days'), ['class'=>'g12']);?>
    </div>
    <div class="g1 submit">
        <?php echo CHtml::submitButton('SEARCH', array('class' => 'button gray')); ?>
    </div>
    <?if(Yii::app()->user->checkAccess('dialerimport') && in_array($_GET['type'], array(ComponentTypes::SELLERS, ComponentTypes::BUYERS))):?>
    <hr class="g12" style="padding: 0; min-height: 5px;" />
    <div class="g12">
        <label class="g1">Dialer Action:</label>
        <span class="g10">
            <?php echo CHtml::dropDownList('dialerAction', null, array('add'=>'Add to Dialer List', 'remove'=>'Remove from Dialer List'), $htmlOptions=array('empty'=>'')); ?>
            <?php echo CHtml::dropDownList('dialerListId', null, CHtml::listData($dialerLists[$_GET['type']], 'id', 'name'), $htmlOptions=array('empty'=>'Select a Dialer List'));?>
            <?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button dialer-button','style'=>'font-size: 10px; padding: 8px 10px;'))?>
            <?php echo CHtml::hiddenField('dialerProcess', null, $htmlOptions=array('value'=>'0'))?>
            <?php echo CHtml::dropDownList('sellerCallListTemplate', null, CHtml::listData($dialerLists[ComponentTypes::SELLERS], 'id', 'name'), $htmlOptions=['name'=>'dialerListId','empty'=>'','style'=>'display:none;','disabled'=>'disabled'])?>
            <?php echo CHtml::dropDownList('buyerCallListTemplate', null, CHtml::listData($dialerLists[ComponentTypes::BUYERS], 'id', 'name'), $htmlOptions=['name'=>'dialerListId','empty'=>'','style'=>'display:none;','disabled'=>'disabled'])?>
            <a id="add-call-list-button" class="text-button" href="/admin/dialer/addCallLists" style="display: inline-block;"><em class="icon i_stm_add"></em>Add Call List</a>
        </span>
    </div>
    <?endif;?>

    <? if(Yii::app()->user->checkAccess('slybroadcast')):?>
    <hr style="padding: 0; min-height: 5px;" class="g12">
    <div class="g12">
        <? if(!SettingAccountValues::model()->getSettingsByAccountSettingId(Yii::app()->user->accountId, Settings::SETTING_SLY_BROADCAST_EMAIL_ID) || !SettingAccountValues::model()->getSettingsByAccountSettingId(Yii::app()->user->accountId, Settings::SETTING_SLY_BROADCAST_PASSWORD_ID)):
            echo '<h3 class="g12">Sly Broadcast API info in Account Settings is missing. Please update your settings to proceed.</h3>';
            else: ?>
            <label class="g1">Slybroadcast:</label>
            <span class="g10">

            <?php echo CHtml::textField('broadcastCallerId', null, $htmlOptions=['placeholder' => 'Caller ID', 'style' => 'width: 250px;']);?>

            <div style='display: inline-block;'>
                Audio File<br>
                <? echo CHtml::dropDownList('broadcastAudioFile', null, CHtml::listData(VoicemailBroadcastMessages::model()->findAll(), 'id', 'name'), $htmlOptions = array(
                    //'class' => 'g8',
                    'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                ));?>

            </div>

            <?php $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'name' => 'broadcastDeliveryDate',
                    'defaultTime' => '12:00 am',
                    'options' => array(
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:185px;',
                        'placeholder' => 'Set on Date & Time'
                    ),
                )
            ); ?>

            <div style='display: inline-block;'>
                <label>Mobile Only:</label>
                <?php echo CHtml::checkbox('broadcastMobileOnlyCheckbox',false ,$htmlOptions=array(
                    'value'=>0,
                    'style'=>'font-size: 16px; position: relative; top: 10px;',
                )); ?>
                <?php echo CHtml::hiddenField('broadcastMobileOnly', null); ?>
            </div>

            <?php echo CHtml::hiddenField('broadcastProcess', null, $htmlOptions=array('value'=>'0'))?>
            <?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button broadcast-button','style'=>'font-size: 10px; padding: 8px 10px;'))?>

            <a id="add-call-list-button" class="text-button" href="/admin/contacts/voicemailBroadcasts" style="display: inline-block;"><em class="icon i_stm_add"></em>Add Audio File</a>
        </span>
        <? endif;?>
    </div>
    <? endif;?>
</div>
<?php $this->endWidget(); ?>