<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
	$this->breadcrumbs = array(
		'Leads' => array(
			'/admin/contacts/leads' //. $model->transactionType
		)
	);
    $module = Yii::app()->controller->module->id;
    $uri = $_SERVER['REQUEST_URI'];
	Yii::app()->clientScript->registerScript('leadSearchScript',<<<JS
	 $('.action-plan-status.exists, .lead-button-tip').tipsy({live: true, gravity: 's', title: 'data-tooltip'});

    $('.export-button').click(function(){
        if(confirm("Are you sure you want to export this leads list?")) {
            $('body').prepend('<div class="loading-container loading"><em></em></div>');
            $('#export').val(1);

            window.open('/$module/contacts/leads?' + $('#listview-search form').serialize());
            setTimeout(function () {
                $('div.loading-container.loading').remove();
                Message.create("notice", "Contact is processing in the new window. Please wait...");
            }, 5000);
        }
        $('#export').val(0);
        return false;
    });

    $('form#transaction-list-search').submit(function() {

        // clear mass action fields
        $('#massActionType').val('');
        $("select#massActionComponentId, select#MassAction_tags").val(null).trigger('liszt:updated');
        $("#MassAction_action_plan_options, #MassAction_tag_options").hide();

        if(($('#fromSearchDate').val() !='' || $('#toSearchDate').val() !='') && $('#dateSearchFilter').val() == '') {
            alert('Please select a Sort filter from the drop down list.');
            return false;
        }
        // reset key so pagination gets reset to 1
        $('div.keys').attr('title','$uri');

        $.fn.yiiGridView.update('leads-grid', {
            data: $(this).serialize(),
            complete: function(jqXHR, status) {
//                if (status=='success'){
//                    var html = $.parseHTML(jqXHR.responseText);
//                    var newOverdueLabel = $('.overdue-task-label', $(html)).html();
//                    $('.overdue-task-label').html(newOverdueLabel);
//                }
            }
        });
        $('#dialerProcess').val(0);
        $('#dialerAction').val('');
        $('#dialerListId').val('');

        $('#massActionProcess').val(0);
        $('#massActionType').val('');
        $('#massActionComponentId').val(null).trigger('liszt:updated');
        return false;
    });

    $('.transactionType').change(function(){
        $('#transactionTypeId').val($(this).val());
        $('form#transaction-list-search').submit();
    });

    $('form#transaction-list-search input[type="checkbox"]').change(function(){
        var checkboxName = $(this).prop('name').replace('Checkbox', '');
        if($(this).is(':checked')) {
            $('#'+checkboxName).prop('value',1);
        } else {
            $('#'+checkboxName).prop('value',0);
        }
        $('form#transaction-list-search').submit();
    });

    $('form#transaction-list-search .transactionType').change(function(){
        if($(this).hasClass('transactionType')) {
            if($(this).val()=='3') {
                $('title, #content-header h1').html('Seller Lead List');
                $('#red, #yellow').prop('id','green');
                $('#dialerListId').html($('#sellerCallListTemplate').html());
            } else if($(this).val()=='2') {
                $('title, #content-header h1').html('Buyer Lead List');
                $('#red, #green').prop('id','yellow');
                $('#dialerListId').html($('#buyerCallListTemplate').html());
            } else {
                $('title, #content-header h1').html('Leads List');
                $('#yellow, #green').prop('id','red');
                $('#dialerListId').html('');
            }
        }
        $('form#transaction-list-search').submit();
    });

    $('.dialer-button').click(function(){
        $('#dialerProcess').val(1);

        if($('#dialerAction').val() == '') {
            alert('Please select option to Add or Remove from Dialer list.');
            $('#dialerProcess').val(0);
            return false;
        }

        if($('#dialerListId').val() == '') {
            alert('Please select a Dialer list.');
            $('#dialerProcess').val(0);
            return false;
        }

        $('form#transaction-list-search').submit();

        $('#dialerProcess').val(0);
    	return false;
    });

    $('.broadcast-button').click(function(){

        if(!$('#broadcastCallerId').val().match(/^\d{10}/)) {
            alert('Please enter a valid Caller ID.');
            $('#broadcastProcess').val(0);
            return false;
        }

        if($('#broadcastAudioFile').val() == '') {
            alert('Please select valid audio file.');
            $('#broadcastProcess').val(0);
            return false;
        }

        if($('#broadcastDeliveryDate').val().length == '') {
            alert('Please select valid date and time.');
            $('#broadcastProcess').val(0);
            return false;
        }

        if(confirm('Are you sure you want to continue?')) {

                $('#broadcastProcess').val(1);

                $.ajax({
                    data: $('form#transaction-list-search').serialize(),
                    dataType: 'json',
                    success: function(o) {

                        // Show flash message
                        Message.create(o.status, o.message);
                    }
                });

                $('#broadcastCallerId').val('');
                $('#broadcastDeliveryDate').val('');
                $('#broadcastAudioFile').val('');

                $('#broadcastProcess').val(0);
            }

        return false;
    });

    $('.status-unselect-all').click(function(){
        $('select#transactionStatusIds').val(null).trigger('liszt:updated');
    });

    $('.status-select-abc').click(function(){
        $('select#transactionStatusIds').val(["2", "3", "4"]).trigger('liszt:updated');
    });

    $('.status-select-a').click(function(){
        $('select#transactionStatusIds').val(["2"]).trigger('liszt:updated');
    });

    $('.status-select-b').click(function(){
        $('select#transactionStatusIds').val(["3"]).trigger('liszt:updated');
    });

    $('.status-select-c').click(function(){
        $('select#transactionStatusIds').val(["4"]).trigger('liszt:updated');
    });

    $('.status-select-listed').click(function(){
        $('select#transactionStatusIds').val(["8"]).trigger('liszt:updated');
    });

    $('.status-select-coming-soon').click(function(){
        $('select#transactionStatusIds').val(["7"]).trigger('liszt:updated');
    });

    $('#dateSearchFilter').change(function(){
        if($(this).val() == 'status_asc') {
            // clear date value fields and hide
            $('#fromSearchDate, #toSearchDate').val().hide('normal');
        }
        else {
            // show date value fields
            $('#fromSearchDate, #toSearchDate').show('normal');
        }
    });

	$("#massActionType").on("change",function(){
		var option = $( this ).val();
		if(option == "addTag" || option == "removeTag"){
			$("#MassAction_action_plan_options").hide();
			$("#MassAction_tag_options").show();
			return;
		}else if (option == "unapplyActionPlans" || option == "applyActionPlans") {
			$("#MassAction_tag_options").hide();
			$("#MassAction_action_plan_options").show();
			return;
		}else{
			$("#MassAction_tag_options").hide();
			$("#MassAction_action_plan_options").hide();
		}
	});

	$( "form .ajaxMassAction" ).on("click", function( event ) {
		event.preventDefault();
		var message = '';

		$('#massActionProcess').val(1);
		if($('#massActionType').val() == '') {
            alert('Please select a Mass Action from the dropdown list');
            $('#massActionProcess').val(0);
            return false;
        }

        // validate action plan mass action, verify an action plan is selected
        if(($('#massActionType').val() == 'unapplyActionPlans') && ($('#massActionComponentId').val() == '')) {
            alert('Please select an option from Mass Action.');
            $('#massActionProcess').val(0);
            return false;
        }

        if(($('#massActionType').val() == 'unapplyActionPlans')) {
            message = '**** IMPORTANT: There is a limit of 200 mass actions. Repeat as needed. Please WAIT until first process is complete to start the next one. ****';
        }

//        if(($('#massActionType').val() == 'updateLeadStatus')) {
//			return $("#MassAction_action_status_options").show();
// 		}

        if(confirm("Confirm proceeding with Mass Action." + message)) {

            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            var element_id = $(this).closest('form').attr('id');
            $.get(
                $("#"+element_id).attr("action"),
                $( "#"+element_id ).serializeArray(),
                function(data) {
                    // reset mass action flag
                    $('#massActionProcess').val(0);

                    if(data.status == 200){
                        Message.create('success','Mass Update was successful. ' + data.message);
                        Message.create('success','Refreshing search results.');

                         // clear mass action fields
                        $('#massActionType').val('');
                        $("select#massActionComponentId, select#MassAction_tags").val(null).trigger('liszt:updated');
                        $("#MassAction_action_plan_options, #MassAction_tag_options").hide();

                        // refresh grid
                        $.fn.yiiGridView.update('leads-grid', {
                            data: $('#listview-search form').serialize()
                        });

                    } else if(data.status == 400){
                        Message.create('warning', data.message);
                    }else{
                        Message.create('error','Error: Mass Update had an error. ' + data.message);
                    }
                    $("div.loading-container.loading").remove();
                }
            );
        } else {
            // reset mass action flag
            $('#massActionProcess').val(0);
        }
	});
JS
, CClientScript::POS_END);


Yii::app()->clientScript->registerCss('contactLeadsCss', <<<CSS
    .follow-up-flag-definition {
        display: inline-block;
    }
    #leads-grid a {
        font-size: 16px;
    }
    .status {
        color: #FFF;
        font-weight: bold;
        font-size: 18px;
        border-radius: 18px;
        display: inline-block;
        margin-right: 4px;
        width: 30px;
        height: 26px;
        padding-top: 4px;
        text-shadow: none;
    }
    .statusN {
        background-color: #fc3dfc;
    }
    .statusA {
        background-color: #d20000; /*0dd71a*/
    }
    .statusB {
        background-color: #ffab00;
    }
    .statusC {
        background-color: #2c0; /*43a2ff*/
    }
    .statusD {
        background-color: #43a2ff; /*a25af0*/
    }
    .statusE {
        background-color: #888;
    }
    #leads-grid .action-plan-status.fa {
        font-size: 25px !important;
    }
    #leads-grid .action-plan-status.fa-check {
        color: #AAA; /*00c000*/
    }
    #leads-grid .action-plan-status.fa-times {
        color: #D20000;
    }
    .tipsy-inner{
        font-size: 10px;
    }
    #leads-grid th a.sort-link {
        font-size: 11px !important;
    }
    #massActionComponentId_chzn {
        float: left;
        width: 74.5% !important;
    }
CSS
);
?>
<!--	<div style="position: absolute; left:27%; top: 11px;">--><?php //echo Transactions::FOLLOW_UP_FLAG; ?>
<!--		<div class="follow-up-flag-definition"> = New Lead Follow-up time exceeded, No future Task, No activity in past --><?php //echo str_replace('-','',ActivityLog::DEFAULT_NO_ACTIVITY_DAYS); ?><!--.</div>-->
<!--	</div>-->
	<div id="content-header">
		<h1>Leads List</h1>
	</div>
	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php
			$this->renderPartial('_listSearchBoxLeads', array(
					'model' => $model,
                    'transactionStatusSelected'=>$transactionStatusSelected,
                    'dialerLists' => $dialerLists,
				)
			);
		?>
	</div>
<?php
$this->renderPartial('_leads', array(
        'dataProvider' => $dataProvider,
        'count'=>$count,
        'start'=>$start,
        'end'=>$end,
    )
);