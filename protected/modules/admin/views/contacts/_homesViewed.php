<?php
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'homes-viewed-grid',
	'htmlOptions'=>array(
		'class'=>'homes-viewed-grid'
	 ),
	'template'=>'{items}',
	'enablePagination'=>false,
	'dataProvider'=>$dataProvider,
	'itemsCssClass'=>'datatables',
	'columns'=>array(
	    array(
			'type'=>'raw',
	    	'name'=>'Date / Time',
	    	'value'=>'Yii::app()->format->formatDateTime($data["added"],array("break"=>true))',
	    ),
		array(
			'type'=>'raw',
			'name'=>'# Views',
			'value'=>'$data["listing_view_count"]',
            'htmlOptions' => array('style' => 'width:50px; text-align:center;'),
		),
	    array(
			'type'=>'raw',
	        'name'=>'Price / MLS#',
	        'value'=>'"<b>".Yii::app()->format->formatDollars($data["price"])."</b><br>".$data["listing_id"]',
		    ),
	    array(
			'type'=>'raw',
	        'name'=>'Size',
	        'value'=>'($data["bedrooms"]) ? $data["bedrooms"]."BR / ".$data["baths_full"]." Baths <br>".number_format($data["sq_feet"])." SF" : " "',
		    ),
	    array(
	        'type'=>'raw',
	        'name'=>'Address / Subdivision',
            'value'=>'(($data["common_subdivision"]) ? "<strong><u>".$data["common_subdivision"]."</u></strong><br>": "").CHtml::link(StmFormatter::formatProperCase(StmFormatter::formatRemoveExtraSpaces($data["streetAddress"])), "/home/".$data["state"]."/".strtolower(StmFormatter::formatLink($data["city"]))."/".strtolower(StmFormatter::formatLink($data["streetAddress"]))."/".$data["listing_id"], $htmlOptions=array(\'target\'=>\'_blank\'))."<br>".Yii::app()->format->formatProperCase($data["city"]).", ".strtoupper($data["state"])." ".$data["zip"]',
		    ),
		),
	)
);