<? $this->breadcrumbs = array('Import' => '') ?>
<h1>Import Contacts</h1>

<section style='max-width: 1200px; margin: 25px auto;'>

    <h3>Password: <?=CHtml::textField('pw', null, $htmlOptions=array('style'=>'width: 200px; font-size: 20px;','placeholder'=>'Password'))?></h3>

    <form action='/admin/contacts/initialImport' method='POST' enctype='multipart/form-data' id='contacts-form-upload' style='text-align: center; margin-top: 25px;'>
        <h2>Component Type: <?=CHtml::dropDownList('componentTypeId', null, array(1=>'Contacts', 2=>'Buyers', 3=>'Sellers', 5=>'Closings'))?></h2><br>
        <input type='file' name='contacts-file' id='contacts-file' style='width: 400px;'>
        <input type='hidden' name='method' id='contacts-method' value='process-file'>
        <button type='submit' id='contacts-btn-upload'>Import Now</button>
    </form>
</section>