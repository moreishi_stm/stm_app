<div class="button-container">
	<?php $contactId = Yii::app()->request->getQuery('id'); ?>
	<a href="/admin/sellers/add/<? echo $contactId; ?>" id="add-seller-button" class="text-button"><em class="icon i_stm_add"></em>Add Seller</a>
	<a href="/admin/buyers/add/<? echo $contactId; ?>" id="add-buyer-button" class="text-button"><em class="icon i_stm_add"></em>Add Buyer</a>
	<a href="/admin/referrals/add/<? echo $contactId; ?>" id="add-referral-button" class="text-button"><em class="icon i_stm_add"></em>Add Referral</a>
</div>
<div class="ui-button-panel">
	<?php
	$this->widget('admin_module.components.StmGridView', array(
		'id' => 'transaction-grid',
		'htmlOptions' => array(
			'class' => 'transaction-grid'
		),
		'template' => '{items}',
		'enablePagination' => false,
		'dataProvider' => $dataProvider,
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Status',
                'htmlOptions' => array('style' => 'width:80px'),
                'value' => '$data["status"]',
            ),
			array(
                'type' => 'raw',
                'name' => 'Details',
                'value' => '$data["details"]',
//                'htmlOptions'=>array('style'=>'width:150px'),
            ),
			array(
                'type' => 'raw',
                'name' => 'Assigned to',
                'value' => '$data["assigned_to"]',
                'htmlOptions'=>array('style'=>'width:110px'),
            ),
			array(
                'type' => 'raw',
                'name' => 'Submit Date',
                'value' => '$data["submit_date"]',
                'htmlOptions'=>array('style'=>'width:90px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '$data["button"]',
                'htmlOptions'=>array('style'=>'width:50px'),
            ),
		),
	));

//            array(
//                'type' => 'raw',
//                'name' => 'Status',
//                'htmlOptions' => array('style' => 'width:80px'),
//                'value' => '$data->componentType->singularName."<br />".$data->status->name',
//            ),
//			array(
//                'type' => 'raw',
//                'name' => 'Address',
//                'value' => '(($data->address)? "<a href=\"/admin/".strtolower($data->componentType->name)."/".$data->id."\">".Yii::app()->format->formatAddress($data->address)."</a><br />":"")',
//            ),
//			array(
//                'type' => 'raw',
//                'name' => 'Assigned to',
//                'value' => 'StmHtml::renderListItems($data->assignments, "contactName")',
//            ),
//			array(
//                'type' => 'raw',
//                'name' => 'Submit Date',
//                'value' => 'Yii::app()->format->formatDateDays($data->added,array("break"=>true))',
//                'htmlOptions'=>array('style'=>'width:90px'),
//            ),
//			array(
//                'class' => 'CButtonColumn',
//                'template' => '{complete}',
//                'htmlOptions' => array(
//                    'style' => 'width:50px'
//                ),
//                'buttons' => array(
//                    'complete' => array(
//                        'label' => 'View',
//                        'raw' => 'true',
//                        'url' => '"/admin/".strtolower($data->componentType->name)."/".$data->id',
//                        'options' => array(
//                            'class' => 'btn widget',
//                            'target'=>'_blank',
//                        ),
//                    ),
//                ),
//            ),
	?>
</div>