<?php
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'contact-search-form',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'beforeValidate' => 'js:function(form) {
							var firstName = $("#Contacts_first_name").val();
							var lastName = $("#Contacts_last_name").val();

							if(firstName !="")
								$("#Contacts_contactFullName").val(firstName);

							if(lastName !="")
								$("#Contacts_contactFullName").val(lastName);

							$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
							return true;
						}',
				'afterValidate' => 'js:function(form, data, hasError) {
							$("div.loading-container.loading").remove();
							if (!hasError) {
								$.fn.yiiGridView.update("contact-grid", {
									data: $(form).serialize(),
									complete: function(jqXHR, status) {
										if (status=="success"){
											var html = $.parseHTML(jqXHR.responseText);
											var exactEmailMatch = $("#add-new-contact-container", $(html)).data("exact-email-match");
											var exactLastNamePhoneMatch = $("#add-new-contact-container", $(html)).data("exact-last-name-phone-match");

                                            $("#exact-match-container").hide();
                                            $("#last-name-phone-match-container").hide();

											if(exactEmailMatch == true || exactLastNamePhoneMatch == true) {
                                                if(exactEmailMatch == true) {
                                                    $("#add-new-contact-container").hide();
                                                    $("#exact-match-container").show();
                                                }

                                                if(exactLastNamePhoneMatch == true) {
                                                    $("#add-new-contact-container").hide();
                                                    $("#last-name-phone-match-container").show();
                                                }
                                            }
											else {
												$("#add-new-contact-container").show();
												$("#exact-match-container").hide();
												$("#last-name-phone-match-container").hide();
											}

											$("#contact-grid-container").show();

											//populate fields into the form below to add new contacts
											$("#contact-add-form #ContactToAdd_source_id").val($("#Contacts_source_id").val());
											$("#contact-add-form #ContactToAdd_first_name").val($("#Contacts_first_name").val());
											$("#contact-add-form #ContactToAdd_last_name").val($("#Contacts_last_name").val());
											$("#contact-add-form #ContactToAdd_contactEmailAdd").val($("#Contacts_contactEmailAdd").val());
											$("#contact-add-form #ContactToAdd_contactPhoneAdd").val($("#Contacts_contactPhoneAdd").val());

											$("#contact-add-form #Addresses_address").val($("#Addresses_address").val());
											$("#contact-add-form #Addresses_city").val($("#Addresses_city").val());
											$("#contact-add-form #Addresses_state_id").val($("#Addresses_state_id").val());
											$("#contact-add-form #Addresses_zip").val($("#Addresses_zip").val());
										}
									}
								});
							}
							return false;
						}',
			)
		)
	); ?>

<div class="g100 p-p0 p-m0">
<?php
	$existingContact = ($model->id > 0);

	$this->beginStmPortletContent(array(
			'handleTitle' => 'Add New ' . $componentTitle,
			'handleIconCss' => 'i_user'
		)
	); ?>
	<div id="contact-container">
		<div class="g12 p-mb5 rounded-text-box notes odd-static">
			<div id="contact-left" class="g6" style="margin-left: 25%;">
				<table class="container">
					<tr>
						<th class="narrow"><?php echo $form->labelEx($model, 'source_id') ?></th>
						<td>
							<?php echo $form->dropDownList($model, 'source_id', Sources::optGroupList(), $htmlOptions = array(
									'data-placeholder' => 'Select a Source',
									'empty' => '',
								)
							); ?>
							<?php echo $form->error($model, 'source_id');
								$this->widget('admin_module.extensions.EChosen.EChosen', array(
										'target' => 'select#Contacts_source_id',
										'options' => array(
											'allow_single_deselect' => true,
											'width' => '84%'
										)
									)
								);
							?>
						</td>
					</tr>
					<tr>
						<th class="narrow">
							<?php echo $form->labelEx($model, 'contactFullName') ?>
						</th>
						<td>
							<?php echo $form->textField($model, 'first_name', $htmlOptions = array(
									'placeholder' => 'First Name',
									'class' => 'g5'
								)
							); ?>
							<?php echo $form->textField($model, 'last_name', $htmlOptions = array(
									'placeholder' => 'Last Name',
									'class' => 'g5'
								)
							); ?>
							<?php echo $form->hiddenField($model, 'contactFullName'); ?>
							<?php echo $form->error($model, 'contactFullName'); ?>
						</td>
					</tr>
					<tr>
						<th class="narrow"><?php echo $form->labelEx($model, 'contactEmailAdd') ?></th>
						<td>
							<?php echo $form->textField($model, 'contactEmailAdd', $htmlOptions = array(
									'placeholder' => 'Email',
									'class' => 'g10'
								)
							);
								echo $form->error($model, 'contactEmailAdd');
							?>
						</td>
					</tr>
					<tr>
						<th class="narrow"><?php echo $form->labelEx($model, 'contactPhoneAdd') ?></th>
						<td>
							<?php $this->widget('StmMaskedTextField', array(
									'model' => $model,
									'attribute' => 'contactPhoneAdd',
									'mask' => '(999) 999-9999',
									'htmlOptions' => array(
										'placeholder' => '(999) 123-1234',
										'class' => 'phoneField g5',
									),
								)
							);
								echo $form->error($model, 'contactPhoneAdd');
							?>
						</td>
					</tr>
                    <?if($componentTypeId==ComponentTypes::SELLERS):?>
                        <tr>
                            <th class="narrow"><label>Address</label></th>
                            <td>
                                <?php echo $form->textField($model->addressToAdd, 'address', $htmlOptions = array(
                                        'placeholder' => 'Address',
                                        'class' => 'g10'
                                    )
                                );
                                echo $form->error($model->addressToAdd, 'address');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th class="narrow"><label>City, St Zip</label></th>
                            <td>
                                <div class="g5">
                                    <?php echo $form->textField($model->addressToAdd, 'city', $htmlOptions = array(
                                            'placeholder' => 'City',
                                            'class' => 'g12'
                                        )
                                    );
                                    echo $form->error($model->addressToAdd, 'city');
                                    ?>
                                </div>
                                <div class="g2 p-tc">
                                    <?php echo $form->dropDownList($model->addressToAdd, 'state_id', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name asc')), 'id', 'short_name'), $htmlOptions = array(
                                            'empty' => 'State',
                                            'class' => 'g12',
                                        )
                                    );
                                    echo $form->error($model->addressToAdd, 'state_id');
                                    ?>
                                </div>
                                <div class="g3">
                                    <?php echo $form->textField($model->addressToAdd, 'zip', $htmlOptions = array(
                                            'placeholder' => 'Zip',
                                            'class' => 'g12'
                                        )
                                    );
                                    echo $form->error($model->addressToAdd, 'zip');
                                    ?>
                                </div>
                            </td>
                        </tr>
                    <?endif;?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
	<div style="text-align: center;"><input class="wide button" type="submit" style="font-size: 12px;" value="CHECK FOR EXISTING RECORDS"></div>
<?php $this->endWidget();

Yii::app()->clientScript->registerScript('lockContact',"
    // Bind change event to lock out adding a contact if a check for existing contacts has already been ran
    jQuery('#contact-left').delegate('input', 'input', function(e) {
        if(jQuery('#add-new-contact-container')) {
            jQuery('#add-new-contact-container').hide();
        }
    });
");
