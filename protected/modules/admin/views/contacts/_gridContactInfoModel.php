<?
$spouseLastDiff = (!empty($data->last_name) && !empty($data->spouse_first_name) && !empty($data->spouse_last_name) && (strtolower($data->spouse_last_name) !== strtolower($data->last_name))) ? true : false;
$name = $data->first_name;
if(!empty($data->spouse_first_name)) {
    $name .= ($spouseLastDiff) ? ' '.$data->last_name.' & ' :  ' & '.$data->spouse_first_name.' '.$data->last_name ;
    $name .= ($spouseLastDiff) ? $data->spouse_first_name.' '.$data->spouse_last_name : '';
}
else {
    $name .= ' '.$data->last_name;
}

echo '<strong><u>'. $name . '</u></strong>';

$data->phones = Yii::app()->db->createCommand("select phone from phones where contact_id=".$data->id." AND is_deleted=0")->queryColumn();
if(!empty($data->phones)) {
    foreach($data->phones as $phone) {
        $phoneString .= (empty($phoneString))? Yii::app()->format->formatPhone($phone) : '<br />'.Yii::app()->format->formatPhone($phone);
    }
    echo '<br />'.$phoneString;
}

if($data->emails = Yii::app()->db->createCommand("select email from emails where contact_id=".$data->id." AND is_deleted=0")->queryColumn()) {
    foreach($data->emails as $email) {
        $emailString .= (empty($emailString))? $email : '<br />'.$email;
    }
    echo '<br />'.$emailString;
}