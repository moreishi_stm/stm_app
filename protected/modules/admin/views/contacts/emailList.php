<?php
	$this->breadcrumbs = array(
		'Emails' => array('/'.$this->module->name.'/contacts'),
	);
?>
    <h1>Email List</h1>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBoxEmail', array(
				'model' => $model,
			)
		); ?>
	</div>

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'emails-grid',
        'template'=>'{pager}{summary}{items}{summary}{pager}',
		'dataProvider' => $model->search(),
        'enableSorting'=>true,
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Name',
                'value' => '$data->contact->fullName',
            ),
            array(
                'type' => 'raw',
                'name' => 'Transactions',
                'value' => '$data->contact->existingComponentRecordButton(ComponentTypes::BUYERS)',
            ),
            'email',
            'emailStatus.name',
            'contact.last_login:date',
		),
	)
);
