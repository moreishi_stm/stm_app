<style type="text/css">
    .stm-alert {
        border-radius: 5px;
        font-size: 20px;
        padding: 10px;
        color: white;
        border: 1px solid;
    }

    .stm-alert.red {
        background-color: #f2dede;
        border-color: #ebccd1;
        color: #a94442
    }

    .stm-alert.green {
        background-color: #dff0d8;
        border-color: #d6e9c6;
        color: #3c763d;
    }
</style>

<?
$this->breadcrumbs=array(
    'Voicemail Broadcast Audio List'=>''
);?>

<div id="content-header">
    <h1>Voicemail Broadcasts</h1>
    <h3>Enter a name and select a file to upload a new voice broadcast file.</h3>
</div>

<?if($message) : ?>
<div class="stm-alert <?if($success === true) : ?>green<?else : ?>red<?endif;?>">
    <b>Hey!</b><br>
    <p><?=$message?></p>
</div>
<?endif;?>

<section style="max-width: 400px; margin-left: auto; margin-right: auto;">

    <form enctype="multipart/form-data" method="POST">
        Name: <input type="text" name="name">
        Audio WAV File: <input type="file" name="audio_file">
        <br><br>
        <button type="submit" value="Upload">Upload</button>
    </form>

</section>

<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'vm-broadcasts-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        'name',
        array(
            'type'  =>  'raw',
            'name'  =>  'Preview',
            'value' =>  array($this, 'html5Audio')
        )
    ),
));
?>