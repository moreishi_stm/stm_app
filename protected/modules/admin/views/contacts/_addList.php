<style type="text/css">
	.instructions {
		background-color: #B20000;
		padding: 8px;
		width: 80%;
		margin-right: auto;
		margin-left: auto;
		color: white;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}

	#add-new-contact-container, #exact-match-container, #last-name-phone-match-container {
		clear: both;
		padding: 14px;
		background-color: #FFF888;
		color: black;
		width: 55%;
		margin-top: 20px;
	}
</style>
<h3 id="add-new-contact-container" class="instructions" data-exact-email-match="<?php echo ($exactEmailMatch) ? 'true' : 'false'; ?>" data-exact-last-name-phone-match="<?php echo ($exactLastNamePhoneMatch) ? 'true' : 'false'; ?>" style="display:none">
	Check for matching records below OR
	<div class="p-pt10">
		<div style="<?php echo ($ContactToAddValidation) ? '' : 'display: none;'; ?>">

			<?php $form = $this->beginWidget('CActiveForm', array(
					'id' => 'contact-add-form',
					'enableAjaxValidation' => false,
					'enableClientValidation' => false,
				)
			);
				echo $form->hiddenField($ContactToAdd, 'source_id', $htmlOptions = array(
						'id' => 'ContactToAdd_source_id',
						'name' => 'ContactToAdd[source_id]'
					)
				);
				echo $form->hiddenField($ContactToAdd, 'first_name', $htmlOptions = array(
						'id' => 'ContactToAdd_first_name',
						'name' => 'ContactToAdd[first_name]'
					)
				);
				echo $form->hiddenField($ContactToAdd, 'last_name', $htmlOptions = array(
						'id' => 'ContactToAdd_last_name',
						'name' => 'ContactToAdd[last_name]'
					)
				);
				echo $form->hiddenField($ContactToAdd, 'contactEmailAdd', $htmlOptions = array(
						'id' => 'ContactToAdd_contactEmailAdd',
						'name' => 'ContactToAdd[contactEmailAdd]'
					)
				);
				echo $form->hiddenField($ContactToAdd, 'contactPhoneAdd', $htmlOptions = array(
						'id' => 'ContactToAdd_contactPhoneAdd',
						'name' => 'ContactToAdd[contactPhoneAdd]'
					)
				);
                echo $form->hiddenField($ContactToAdd->addressToAdd, 'address', $htmlOptions = array(
                        'id' => 'Addresses_address',
                        'name' => 'Addresses[address]'
                    )
                );
                echo $form->hiddenField($ContactToAdd->addressToAdd, 'city', $htmlOptions = array(
                        'id' => 'Addresses_city',
                        'name' => 'Addresses[city]'
                    )
                );
                echo $form->hiddenField($ContactToAdd->addressToAdd, 'state_id', $htmlOptions = array(
                        'id' => 'Addresses_state_id',
                        'name' => 'Addresses[state_id]'
                    )
                );
                echo $form->hiddenField($ContactToAdd->addressToAdd, 'zip', $htmlOptions = array(
                        'id' => 'Addresses_zip',
                        'name' => 'Addresses[zip]'
                    )
                );
			?>
		</div>
		<input type="submit" class="button gray icon i_stm_add grey-button" value="No Matches Found. Add New Contact <?php echo ($componentTitle == 'Contact')?'': ' and '.$componentTitle; ?>."/>
		<?php $this->endWidget(); ?>
	</div>
</h3>

<h3 id="exact-match-container" class="instructions" style="display:none">
	Exact Email match found! Please see below.
</h3>

<h3 id="last-name-phone-match-container" class="instructions" style="display:none">
    Exact Last Name & Phone match found! Please see below.
</h3>

<div id="contact-grid-container" style="display: none;">
	<?php
		$this->widget('admin_module.components.StmGridView', array(
				'id' => 'contact-grid',
				'template' => '{pager}{summary}{items}{summary}{pager}',
				'dataProvider' => $DataProvider,
				'itemsCssClass' => 'datatables',
				'columns' => array(
					'id',
					'first_name',
					'last_name',
					array(
						'type' => 'raw',
						'name' => 'Contact Info',
						'value' => 'Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />".Yii::app()->format->formatPrintPhones($data->contact->phones)."<br>".Yii::app()->format->formatPrintAddresses($data->contact->addresses)',
					),
					array(
						'type' => 'raw',
						'name' => 'Existing Records',
						'value' => '$data->existingComponentRecordButton(' . $componentTypeId . ')',
					),
					array(
						'type' => 'raw',
						'name' => '',
                        'value' => 'Yii::app()->controller->action->printGridColumnAddButton('.$componentTypeId.', '.$componentName.', '.$componentTitle.', $data)',
						'htmlOptions' => array('style' => 'width:150px'),
					),
					array(
						'type' => 'raw',
						'name' => '',
						'value' => '"<div><a href=\"/".Yii::app()->controller->module->id."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Contact</a></div>"',
						'htmlOptions' => array('style' => 'width:130px'),
					),
				),
			)
		);
	?>
</div>