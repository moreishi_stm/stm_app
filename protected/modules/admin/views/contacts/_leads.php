<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'leads-grid',
        'dataProvider' => $dataProvider,
        'template' => '{pager}{summary}{items}{pager}{summary}',
        'itemsCssClass' => 'datatables',
        'enableSorting'=>true,
        'summaryText'=>$start.'-'.$end.' of '.$count.' results.',
        'pager'=> array('id'=>'leadsPager','class' => 'admin_module.components.StmListPager',),
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'ID / Type',
                'value' => 'Yii::app()->controller->action->printGridIdTypeInfo($data)',
                'htmlOptions' => array('style' => 'width:50px; text-align: center;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Contact Info',
                'value' => 'Yii::app()->controller->renderPartial("_gridContactInfoArray", array("data"=>$data))',
            ),
            array(
                'type' => 'raw',
                'name' => 'Status',
                'value' => 'Yii::app()->controller->action->printGridStatus($data)',
                'htmlOptions' => array('style' => 'width: 80px; text-align: center;'),
            ),
            array(
                'type' => 'raw',
				'name' => 'Homes Viewed',
				'value' => 'Yii::app()->controller->action->contactViewedListingCount($data)',
				'htmlOptions' => array('style' => 'width: 50px;text-align: center;'),
            ),
			array(
				'type' => 'raw',
				'name' => 'Saved Homes',
				'value' => 'Yii::app()->controller->action->contactSavedHomesCount($data)',
				'htmlOptions' => array('style' => 'width: 50px;text-align: center;'),
			),
			array(
				'type' => 'raw',
				'name' => 'Saved Searches',
				'value' => 'Yii::app()->controller->action->contactSavedSearchesCount($data)',
				'htmlOptions' => array('style' => 'width: 50px; text-align: center;'),
			),
//			array(
//				'type' => 'raw',
//				'name' => 'Home Activity',
//				'value' => 'Yii::app()->controller->action->printGridEngagement($data)',
//				'htmlOptions' => array('style' => 'width: 200px; text-align: center;'),
//			),
            array(
                'type' => 'raw',
                'name' => 'Last Activity',
                'value' => 'Yii::app()->controller->action->printGridLastActivity($data)',
                'htmlOptions' => array('style' => 'text-align: center;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Next Task',
                'value' => 'Yii::app()->controller->action->printGridNextTask($data)',
                'htmlOptions' => array('style' => 'text-align: center;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Assigned to',
                'value' => 'Yii::app()->controller->action->printGridAssignments($data)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Action Plan',
                'value' => 'Yii::app()->controller->action->printGridActionPlan($data)',
                'htmlOptions' => array('style' => 'max-width: 250px; text-align: center;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Login',
                'value' => 'Yii::app()->controller->action->printGridLastLogin($data)',
                'htmlOptions' => array('style' => 'width: 80px; text-align: center;'),

            ),
            array(
                'type' => 'raw',
                'name' => 'Added',
                'value' => 'Yii::app()->controller->action->printGridActivityInfo($data)',
            ),
        ),
    )
);