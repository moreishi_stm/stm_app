<?php
$this->breadcrumbs=array(
	'List'=>array('/admin/contacts'),
);

?>
<div id="listview-actions">
	<a href="/admin/contacts/add" class="button gray icon i_stm_add">Add New Contacts</a>
	<!-- <a href="#" class="btnGray button i_stm_search icon">Search Contacts</a> -->
</div>
<div id="content-header">
	<h1>Contacts List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBox',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div style="clear:both; padding-top:20px;">
	<h2>Enter Name, Email or Phone above for Contact Results.</h2><br />
	<h3>You currently have <?php echo $model->totalCount;?> Contacts.</h3>
</div>
