<?php
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'transactionGrid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		array(
			'type'=>'raw',
			'name'=>'Contact Info',
			'value'=>'$data->contact->fullName."<br />".Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />".Yii::app()->format->formatPrintPhones($data->contact->phones)',
		),
		array(
			'type'=>'raw',
			'name'=>'Property',
			'value'=>'
				"<div><strong><a href=\"/admin/".$data->componentType->name."/".$data->id."\" target=\"_blank\">".Yii::app()->format->formatAddress($data->address, array(\'streetOnly\'=>true))."</a></strong><br />".Yii::app()->format->formatAddress($data->address, array(\'CityStZipOnly\'=>true))."</div>".
				"<div>".Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0)).
					$br = (Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0)))? Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0)).Yii::app()->format->formatGridValue(" / Baths:", $data->getFieldValue("baths", 0)) : Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0)).
				"</div>".
				"<div>".Yii::app()->format->formatGridValue("SF:", $data->getFieldValue("sq_feet", 0))."</div>".
				"<div>Submit: ".Yii::app()->format->formatDateDays($data->contact->added)."</div>"',
		),
		array(
			'type'=>'raw',
			'name'=>'Listing',
			'value'=>'"<strong>".Yii::app()->format->formatDollars($data->getFieldValue("price"))."</strong> ".(($data->getFieldValue("price_original")) ? "<em>(".Yii::app()->format->formatDollars($data->getFieldValue("price_original")).")</em>" : "")
				."<br />MLS #: ".$data->getFieldValue("mls_num")
				."<br />IVR Ext: ".$data->getFieldValue("ivr_num")
				."<br />Listed: ".$data->getFieldValue("listing_date")
				."<br />Last Reduced: ".Yii::app()->format->formatDateDays($data->getFieldValue("last_reduced"))',
		),
		array(
			'type'=>'raw',
			'name'=>'Activity',
			'value'=>'"Last Login: ".Yii::app()->format->formatDateTime($data->contact->last_login).
				"<br />Follow-up:"."<br />Status: ".$data->status->name.
				"<br />".Yii::app()->format->formatHomeViewSaved($data->contact->id).
				"<br />Assigned to: ".$data->assignments[0]->contact->fullName',
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<div><a href=\"/admin/sellers/".$data->id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a></div>"',
            'htmlOptions'=>array('style'=>'width:90px'),
        ),
	),
));

?>