<?php
$defaultHtmlOptions = array(
    'class' => 'assignmentRow g12 p-p0',
);
if($model->remove) {
    $defaultHtmlOptions['style'] = 'display:none;';
}

if (isset($containerHtmlOptions)) {
    $containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
    $containerHtmlOptions = $defaultHtmlOptions;
}

// Determine which model to use
$inputHtmlOptions = array(
    'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE, 'class' => 'g5',
);

if (isset($model)) {
    // for actual assignments records
    $index           = "[$i]";
    $AssignmentModel = $model;
} else {
    // for the assignments template base
    $AssignmentModel                    = new Assignments;
    $AssignmentModel->component_type_id = $componentTypeId;
    $AssignmentModel->component_id      = $componentId;
    $index                              = '[0]';
    if ($enabled != true) {
        $inputHtmlOptions = CMap::mergeArray(
            $inputHtmlOptions, array(
                'disabled' => 'disabled',
            )
        );
    }
}

echo CHtml::openTag('div', $containerHtmlOptions);
//
?>
<div class="g12">
    <?
    if(!$AssignmentModel->is_protected || (Yii::app()->user->checkAccess('owner'))):?>
        <?php echo $form->dropDownList(
            $AssignmentModel, $index . 'assignee_contact_id', CHtml::listData(
                Contacts::model()->byActiveAdmins($together = false, $contactId = $model->contact->id)->orderByName()
                    ->findAll(), 'id', 'fullName'
            ), $inputHtmlOptions
        ); ?>
        <?php echo $form->dropDownList(
            $AssignmentModel, $index . 'assignment_type_id',
            CHtml::listData(AssignmentTypes::model()->findByComponentTypeId($componentTypeId), 'id', 'display_name'),
            $inputHtmlOptions
        ); ?>
    <? else:?>
        <?=$AssignmentModel->contact->fullName.': '.$AssignmentModel->assignmentType->display_name?>. This assignment is protected.
    <? endif;?>

    <?
    if(Yii::app()->user->checkAccess('owner') && in_array($componentTypeId, array(ComponentTypes::CONTACTS,ComponentTypes::SELLERS,ComponentTypes::BUYERS))){
        echo $form->dropDownList($AssignmentModel, $index . 'is_protected',
            array(0=>'Normal/Transferable',1=>'Protected Status'),
            $inputHtmlOptions
        );
    }
    ?>

    <button type="button" class="text remove-assignment p-fl"><em class="icon icon-only i_stm_delete"></em></button>
    <?php echo $form->error($AssignmentModel, $index . 'assignee_contact_id'); ?>
    <?php echo $form->error($AssignmentModel, $index . 'assignment_type_id'); ?>
    <?php echo $form->hiddenField($AssignmentModel, $index . 'id', $inputHtmlOptions); ?>
    <?php echo $form->hiddenField(
        $AssignmentModel, $index . 'remove', CMap::mergeArray(array('class' => 'remove'), $inputHtmlOptions)
    ); ?>
</div>
<?php echo CHtml::closetag('div'); ?>
