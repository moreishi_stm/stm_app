<?php
	Yii::app()->clientScript->registerScript('abcList', "
	$('select#contact_id').change(function(){
		window.location = '/admin/buyers/abc/'+this.value;
	});
"
	);

	$this->breadcrumbs = array(
		'A, B, C' => '',
	);
?>
	<h1>A <?php echo Yii::app()->format->formatProperCase($model->transactionType) ?></h1>
	<div class="p-tr">
		<?
			$listData = CHtml::listData(Contacts::model()->orderByName()->byActiveAdmins(false, $model->contact_id)->findAll(), 'id', 'fullName');
			echo CHtml::dropDownList('contact_id', $userId, $listData, $htmlOptions = array(
					'class' => 'p-mh8',
					'style' => 'font-size:13px;'
				)
			);

			$this->renderPartial('_listBuyersABC', array(
					'model' => $model,
					'DataProvider' => $HotAProvider
				)
			);
		?>
	</div>
	<h1>B <?php echo Yii::app()->format->formatProperCase($model->transactionType) ?></h1>
<?
	$this->renderPartial('_listBuyersABC', array(
			'model' => $model,
			'DataProvider' => $BProvider
		)
	);
?>
	<h1>C <?php echo Yii::app()->format->formatProperCase($model->transactionType) ?></h1>
<?
	$this->renderPartial('_listBuyersABC', array(
			'model' => $model,
			'DataProvider' => $CProvider
		)
	);
