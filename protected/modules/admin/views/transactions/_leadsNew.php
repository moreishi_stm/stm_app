<?php
$this->widget('admin_module.components.StmGridView', array(
    'id'=>'new-leads-grid',
    'dataProvider'=>$dataProvider,
    'itemsCssClass'=>'datatables',
    'summaryText' => '',
    'enablePagination'=>false,
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Name',
            'value'=>'CHtml::link($data->contact->fullName, array("/admin/buyers/".$data->id), array("target"=>"_blank"))',
        ),
        array(
            'type'=>'raw',
            'name'=>'Price / Location',
            'value'=>'$data->buyerPriceRange
            			.CHtml::tag("div", $htmlOptions=array(), $data->getFieldValue("location"))',
        ),
//        'source.name',
        array(
            'type'=>'raw',
            'name'=>'Assigned to',
            'value'=>'$data->assignments[0]->contact->fullName', // @todo: need to refactor this to pull from the primary assignment
        ),
        array(
            'type'=>'raw',
            'name'=>'Submit',
            'value'=>'Yii::app()->format->formatDaysTime($data->added, array("break"=>true,"zeroDaysLabelToday"=>true))',
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}',
            'viewButtonOptions' => array("target" => "_blank"),
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>$this->module->imageAssetsUrl.'/search.png',
                    'url' =>'Yii::app()->controller->createUrl("buyers/".$data->id)',
                ),
            ),
            'htmlOptions'=>array('style'=>'width: 20px')
        ),
    ),
));