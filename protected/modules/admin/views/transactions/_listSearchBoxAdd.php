<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'transaction-list-search',
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
			'beforeValidate' => 'js:function(form) {
	            $(".loading-container").addClass("loading");
				return true;
            }',
			'afterValidate' => 'js:function(form, data, hasError) {
			$(".loading-container").removeClass("loading");
			if (!hasError) {
				$.fn.yiiGridView.update("contact-grid", {
					data: $(form).serialize(),
					complete: function(jqXHR, status) {
						if (status=="success"){
							$("#contact-grid-container").show();
							$("#add-new-contact-container").show();
							$("#instructions-top").css({"background-color":"yellow", "color":"black"});
							$("#instructions-top").html("Step 2: Review for an existing transaction below or click on Add New Contact.");

							//populate fields into the form below to add new contacts
//							$("#").val($("#Contacts_first_name").val());
						}
					}
				});
			}
			return false;
		}',
		),
	)
);
?>
<div class="g12">
	<div class="g3">
		<label class="g3">Name:</label>
			<span class="g8">
				<?php echo $form->textField($model, 'first_name', $htmlOptions = array('placeholder' => 'First Name')); ?>
				<?php echo $form->error($model, 'first_name'); ?>
			</span>
	</div>
	<div class="g3">
			<span class="g9">
				<?php echo $form->textField($model, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')); ?>
				<?php echo $form->error($model, 'last_name'); ?>
			</span>
		<label class="g3">Email:</label>
	</div>
	<div class="g2">
			<span class="g12">
				<?php echo $form->textField($model->emails, 'email', $htmlOptions = array('placeholder' => 'Email')); ?>
				<?php echo $form->error($model->emails, 'email'); ?>
			</span>
	</div>
	<div class="g3">
		<label class="g3">Phone:</label>
			<span class="g8">
			  <?php $this->widget('StmMaskedTextField', array(
					  'model' => $model->phones,
					  'attribute' => 'phone',
					  'mask' => '(999) 999-9999',
					  //Makes the full 10 digit phone number optional (enables partial search)
					  'htmlOptions' => array(
						  'placeholder' => '(999) 123-1234',
						  'class' => 'phoneField'
					  )
				  )
			  ); ?>
			  <?php echo $form->error($model->phones, 'phone'); ?>
			</span>
	</div>
	<div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>
</div>
<? $this->endWidget(); ?>
