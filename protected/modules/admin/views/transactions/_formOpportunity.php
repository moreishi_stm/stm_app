<tr>
    <th colspan="2">
        <hr/>
    </th>
</tr>
<? if(!$Opportunity->isNewRecord):

Yii::app()->clientScript->registerCss('opportunityFormCss', <<<CSS
    #transactions .container td.opportunity-data {
        color: #37F;
    }

CSS
);
?>
    <tr>
        <th></th>
        <td><u>OPPORTUNITY:</u></td>
    </tr>
    <tr>
        <th>Opportunity Found by:</th>
        <td class="opportunity-data"><?=$Opportunity->addedBy->fullName?> @ <?=Yii::app()->format->formatDate($Opportunity->added)?></td>
    </tr>
    <tr>
        <th>Opportunity Target <?=($Opportunity->component_type_id == ComponentTypes::SELLERS) ? 'List' : 'Buy'?> Date:</th>
        <td class="opportunity-data"><?=Yii::app()->format->formatDate($Opportunity->target_date)?></td>
    </tr>
    <tr>
        <th>Next Follow-up Date:</th>
        <td class="opportunity-data"><?=Yii::app()->format->formatDate($Opportunity->target_date)?></td>
    </tr>
<? return;
endif; ?>

<?Yii::app()->clientScript->registerScript('opportunityFormScript', <<<JS

	$('#opportunity-button').click(function(){
		var buttonContent = $(this).html();
		if($(this).find('em.icon').hasClass('i_stm_delete')) {
			if(confirm('Confirm deleting this Nurture/Opportunity?')) {
				$('.opportunity-containers').toggle('normal');
				buttonContent = buttonContent.replace('Delete Nurture/Opportunity','Add Opportunity');
				buttonContent = buttonContent.replace('i_stm_delete','i_stm_add');
				$('#Opportunity_toDelete').val('1');
			    $('.opportunity-info').hide();
				$(this).html(buttonContent);
			}
		} else {
			$('.opportunity-containers').toggle('normal');
			buttonContent = buttonContent.replace('Add Opportunity','Delete Opportunity');
			buttonContent = buttonContent.replace('i_stm_add','i_stm_delete');
			$('#Opportunity_toDelete').val('');
			$('.opportunity-info').show();
			$(this).html(buttonContent);
		}
	});
JS
);

$display = (($Opportunity->isNewRecord && $Opportunity->isEmpty))?'style="display:none;"': ''; // || $Opportunity->toDelete
?>
<tr>
    <th>Opportunity:</th>
    <td>
        <?php echo ($display)?'<button type="button" class="text" id="opportunity-button"><em class="icon icon-only i_stm_add"></em>Add Nurture/Opportunity</button>'
            :'<button type="button" class="text" id="opportunity-button" style="'.((Yii::app()->user->checkAccess('owner'))? '' : 'display: none;').'"><em class="icon icon-only i_stm_delete"></em>Delete Nurture/Opportunity</button>';
        ?>
        <span class="label opportunity-info" style="display: none;">
            <u>NEW NUTURE/OPPORTUNITY DEFINITION:</u><br>
            1) Confirmed Contact Information<br>
            2) Sale Opportunity in the next 12 months<br>
            3) Willing to consider hiring us<br>
            4) Agreed upon follow-up date<br>
            5) Identified motivation to move<br>
            <br>
            *All Nurture/Opportunity fields must be completed.<br>
        </span>
    </td>
</tr>
<!--<tr --><?php //echo $appointmentDisplay; ?><!-- class='appointment-containers'>-->
<!--    <th>Appt Status:</th>-->
<!--    <td>-->
<!--        --><?php //echo $form->dropDownList($Appointment, 'met_status_ma', $Appointment->getMetStatusTypes(), $htmlOptions = array(
//                'style' => 'width:125px;',
//                'empty' => ' ',
//                'class' => 'p-fl'
//            )
//        ); ?>
<!--        --><?// $x = TaskTypes::model()->byComponentType(ComponentTypes::APPOINTMENTS)->findAll(); ?>
<!--        --><?php //echo $form->dropDownList($Appointment, 'set_activity_type_id', CHtml::listData(TaskTypes::model()->byComponentType(ComponentTypes::APPOINTMENTS)->findAll(), 'id', 'name'),
//            $htmlOptions = array(
//                'style' => 'width:125px;',
//                'empty' => ' ',
//            )
//        ); ?>
<!--        <span class="label strong g2 p-tr p-pr5">Set Method:</span>-->
<!--        --><?php //echo $form->error($Appointment, 'met_status_ma'); ?>
<!--        --><?php //echo $form->error($Appointment, 'set_activity_type_id'); ?>
<!--    </td>-->
<!--</tr>-->
<!--<tr class='appointment-cancel-reschedule-reason' style="--><?php //echo ($Appointment->met_status_ma == Appointments::MET_STATUS_CANCEL || $Appointment->met_status_ma == Appointments::MET_STATUS_RESCHEDULE)? '' : 'display: none;';?><!--">-->
<!--    <th>Cancel/Reschedule Reason:</th>-->
<!--    <td>-->
<!--        --><?php //echo $form->textField($Appointment, 'met_status_reason', $htmlOptions = array(
//                'empty' => ' ',
//                'class' => 'p-fl g8'
//            )
//        ); ?>
<!--        --><?php //echo $form->error($Appointment, 'met_status_reason'); ?>
<!--    </td>-->
<!--</tr>-->
<tr <?php echo $display; ?> class='opportunity-containers p-mt5'>
    <th>Target Move Date:</th>
    <td>
        <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $Opportunity,
                'attribute' => 'target_date',
                'options' => array(
                    'showAnim' => 'fold',
                ),
                'htmlOptions' => array(
                    'class' => 'g8',
                    'placeholder' => 'Target Date'
                ),
            ));
        ?>
        <?php echo $form->error($Opportunity, 'target_date'); ?>
    </td>
</tr>
<tr <?php echo $display; ?> class='opportunity-containers p-mt5'>
    <th>Next Follow-up Date:</th>
    <td>
        <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $Opportunity,
                'attribute' => 'next_followup_date',
                'options' => array(
                    'showAnim' => 'fold',
                ),
                'htmlOptions' => array(
                    'class' => 'g8',
                    'placeholder' => 'Next Followup Date'
                ),
            ));
        ?>
        <?php echo $form->error($Opportunity, 'next_followup_date'); ?>
        <?php echo $form->hiddenField($Opportunity, 'toDelete'); ?>
    </td>
</tr>
<tr <?php echo $display; ?> class='opportunity-containers p-mt5'>
    <th>How well did you Dig for Motivation:</th>
    <td>
        <?php echo $form->dropDownList($Opportunity, 'dig_motivation_rating', StmFormHelper::getDropDownList(array('start'=>1, 'end'=>10)), $htmlOptions = array(
                'empty' => ' ',
                'class' => 'p-fl'
            )
        ); ?> <span class="p-fl p-pl10">(1 low - 10 high)<br>Tip: How can you go for the appointment now?</span>

        <?php echo $form->error($Opportunity, 'dig_motivation_rating'); ?>

    </td>
</tr>
<tr <?php echo $display; ?> class='opportunity-containers p-mt5'>
    <th>Follow-up Phone Task Description:</th>
    <td>
        <?php echo $form->textField($Opportunity, 'followUpTaskDescription', $htmlOptions = array('placeholder' => 'Follow-up Task Description','class' => 'g12')); ?>
        <span class="label">*Will automatically add Phone Task</span>
        <?php echo $form->error($Opportunity, 'followUpTaskDescription'); ?>
    </td>
</tr>
<tr <?php echo $display; ?> class='opportunity-containers p-mt5'>
    <th>Task Assigned to:</th>
    <td>
        <?php echo $form->dropDownList($Opportunity, 'followUpTaskAssignedToId', CHtml::listData(Contacts::model()->byActiveAdmins(false)->orderByName()->findAll(), 'id', 'fullName'),
            $htmlOptions = array('empty' => 'Select Task Assign to')
        ); ?>
        <?php echo $form->error($Opportunity, 'followUpTaskAssignedToId'); ?>

    </td>
</tr>
<tr <?php echo $display; ?> class='opportunity-containers p-mt5'>
    <th>Activity Log Note:</th>
    <td>
        <?php echo $form->textArea($Opportunity, 'activityLogNote', $htmlOptions = array('placeholder' => 'Activity Log Note','class' => 'g12','rows'=> 3)); ?>
        <span class="label">*Will automatically add Activity Log</span>
        <?php echo $form->error($Opportunity, 'activityLogNote'); ?>
    </td>
</tr>