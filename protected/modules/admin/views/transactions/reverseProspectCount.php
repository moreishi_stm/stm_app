<?php
/**
 * Reverse search example: Criteria = $500,000 - $600,00, 4 BR, 2 Baths, Zip Code: 32082
 * This means: Find buyers that have viewed homes (viewed_pages table) that meet the above criteria
 */

$this->breadcrumbs = array(
	'Reverse Prospect' => '#',
);

Yii::app()->clientScript->registerScript('search', <<<JS
    $('form#transaction-list-search').submit(function() {
        $('body').prepend('<div class="loading-container loading"><em></em></div>');
        $.post('reverseProspectCount', $("form#transaction-list-search").serialize(), function(data) {
            $("div.loading-container.loading").remove();
            if(data !="error") {
                $('#d30').html(data.d30);
                $('#d60').html(data.d60);
                $('#d90').html(data.d90);
            }
        }, "json");
        return false;
    });
JS
);

?>
<h1 style="line-height: 1.2;">Reverse Prospect Count</h1><hr />
<h3 class="p-pb10">Enter criteria below to find buyers that fit your needs:</h3>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBoxReverse',array(
		'model'=>$MlsProperties,
	)); ?>
</div>
<div id="reverse-prospect-counts">
    <table style="width: 50%; font-size: 16px; margin-left: 25%; text-align: center;">
        <tr>
            <th>30 days</th>
            <th>60 days</th>
            <th>90 days</th>
        </tr>
        <tr>
            <td id="d30"> -</td>
            <td id="d60"> -</td>
            <td id="d90"> -</td>
        </tr>
    </table>
</div>