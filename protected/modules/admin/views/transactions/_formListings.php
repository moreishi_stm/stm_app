<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'contact-form',
			'enableAjaxValidation' => false,
		)
	); ?>

	<!-- ====== SELLERS INFO =============================================================================================== -->
	<div id="top-data" class="row">
		<div class="g100 p-p0 p-m0" id="transactions">
			<? $this->beginStmPortletContent(array(
					'handleTitle' => 'Listing Information',
					'handleIconCss' => 'i_house'
				)
			); ?>
			<div id="transaction-container">
				<div class="g12 p-mb5 rounded-text-box odd-static">
					<div id="transaction-seller-left" class="g6">
						<table class="container">
							<tr>
								<th><?php echo $form->labelEx($model, 'contact_id'); ?>:</th>
								<td>
									<?php echo CHtml::tag('span', $htmlOptions = array(), $model->contact->fullName); ?>
									<?php echo $form->hiddenField($model, 'contact_id'); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $form->labelEx($model, 'transaction_status_id'); ?>:</th>
								<td>
									<?php echo $form->dropDownList($model, 'transaction_status_id', CHtml::listData(TransactionStatus::model()->findAll(), 'id', 'name'), $htmlOptions = array(
											'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
										)
									); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $form->labelEx($model, 'sale_type_ma'); ?>:</th>
								<td>
									<?php echo $form->dropDownList($model, 'sale_type_ma', Transactions::getSaleTypes(), $htmlOptions = array(
											'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
										)
									); ?>
									<?php echo $form->error($model, 'sale_type_ma'); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $form->labelEx($model->assignment, 'contact_id'); ?>:</th>
								<td>
									<?php echo $form->dropDownList($model->assignment, 'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->assignment)->findAll(), 'id', 'fullName'),
										$htmlOptions = array(
											'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
										)
									); ?>
									<?php echo $form->error($model->assignment, 'contact_id'); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $form->labelEx($model, 'source_id'); ?>:</th>
								<td>
									<?php echo $form->dropDownList($model, 'source_id', CHtml::listData(Sources::model()->findAll(), 'id', 'name'), $htmlOptions = array(
											'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
										)
									); ?>
									<?php echo $form->error($model, 'source_id'); ?>
								</td>
							</tr>
							<tr>
								<th>Address:</th>
								<td>
									<div class="g12">
										<?php
											$contactAddresses = ($model->tempContact->addresses) ? CHtml::listData($model->tempContact->addresses, 'id', 'fullAddress') : array();
											echo $form->dropDownList($model->transactionAddress, 'address_id', $contactAddresses, $htmlOptions = array(
													'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
												)
											);
										?>
									</div>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('bedrooms')->label; ?>:</th>
								<td>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('bedrooms')->id . ']', $htmlOptions = array('class' => 'g1')); ?>

									<?php echo CHtml::tag('span', $htmlOptions = array('class' => 'label strong g2 p-tr'), $TransactionFields->getField('baths')->label . ':'); ?>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('baths')->id . ']', $htmlOptions = array('class' => 'g1')); ?>

									<?php echo CHtml::tag('span', $htmlOptions = array('class' => 'label strong g2 p-tr'), $TransactionFields->getField('sq_feet')->label . ':'); ?>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('sq_feet')->id . ']', $htmlOptions = array('class' => 'g2')); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('subdivision')->label; ?>:</th>
								<td>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('subdivision')->id . ']', $htmlOptions = array('class' => 'g11')); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('features')->label; ?>:</th>
								<td>
									<?php echo $form->textArea($TransactionFieldValues, 'data[' . $TransactionFields->getField('features')->id . ']', $htmlOptions = array(
											'class' => 'g11',
											'placeholder' => 'Home Features',
											'rows' => '4'
										)
									); ?>
								</td>
							</tr>
						</table>
					</div>
					<!-- ====== TRANSACTION FIELDS =============================================================================================== -->
					<div id="transaction-seller-right" class="g5 custom-form label-wide">
						<table class="container">
							<tr>
								<th><?php echo $TransactionFields->getField('mls_num')->label; ?>:</th>
								<td>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('mls_num')->id . ']', $htmlOptions = array('class' => 'g4')); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('ivr_num')->label; ?>:</th>
								<td>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('ivr_num')->id . ']', $htmlOptions = array('class' => 'g4')); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('lockbox_num')->label; ?>:</th>
								<td>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('lockbox_num')->id . ']', $htmlOptions = array('class' => 'g4')); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('listing_date')->label; ?></th>
								<td>
									<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
											'model' => $TransactionFieldValues,
											'attribute' => 'data[' . $TransactionFields->getField('listing_date')->id . ']',
											'options' => array(
												'showAnim' => 'fold',
											),
											'htmlOptions' => array(
												'class' => 'g4',
											),
										)
									);
									?>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('upfront')->label; ?>:</th>
								<td>
									<?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('upfront')->id . ']', $htmlOptions = array('class' => 'g6')); ?>
								</td>
							</tr>
							<tr>
								<th><?php echo $TransactionFields->getField('showing_instructions')->label; ?>:</th>
								<td>
									<?php echo $form->textArea($TransactionFieldValues, 'data[' . $TransactionFields->getField('showing_instructions')->id . ']', $htmlOptions = array(
											'class' => 'g11',
											'placeholder' => 'Showing Instructions',
											'rows' => '4'
										)
									); ?>
								</td>
							</tr>
							<tr>
								<th>Motivation:</th>
								<td>
									<?php echo $form->textArea($model, 'motivation', $htmlOptions = array(
											'class' => 'g11',
											'placeholder' => 'Motivation',
											'rows' => '4'
										)
									); ?>
								</td>
							</tr>
							<tr>
								<th>Notes:</th>
								<td>
									<?php echo $form->textArea($model, 'notes', $htmlOptions = array(
											'class' => 'g11',
											'placeholder' => 'Notes',
											'rows' => '4'
										)
									); ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ====== END SECTIONS =============================================================================================== -->
<div id="submit-button-wrapper">
	<button type="submit" class="submit wide">Submit Listing</button>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php $this->endStmPortletContent(); ?>