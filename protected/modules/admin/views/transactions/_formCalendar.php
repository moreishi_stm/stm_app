<?
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl.DS.'calendar.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl.DS.'wl_Calendar.js');
Yii::app()->clientScript->registerCssFile($this->module->cssAssetsUrl.DS.'jquery.fullcalendar.css');

$appointmentExists = ($Appointment->id) ? 'true' : 'false';
Yii::app()->clientScript->registerScript('appointmentCalendarJs', <<<JS
    if('$appointmentExists' == 'true' && $('#Appointments_google_calendar_id').val() != undefined) {

        $('#googleCalendarDropdown').html($('#Appointments_google_calendar_id').html());

        $('div#calendar').wl_Calendar({
            "handleWindowResize" : true,
            events: {
                url: '/admin/contacts/getCalendarEvents?calendarId=' + $('#Appointments_google_calendar_id').val()
            }
        });
    }

    $('#appointment-button').click(function(){

        $('#calendar-container').show('normal', function(){

            $('#googleCalendarDropdown').html($('#Appointments_google_calendar_id').html());

            $('div#calendar').wl_Calendar({
                "handleWindowResize" : true,
                events: {
                    url: '/admin/contacts/getCalendarEvents?calendarId=' + $('#Appointments_google_calendar_id').val()
                }
            });
        });
    });

    $('#Appointments_google_calendar_id, #googleCalendarDropdown').change(function(){

        if($(this).attr('id') == 'googleCalendarDropdown') {

            $('#Appointments_google_calendar_id').val($(this).val());
        } else if($(this).attr('id') == 'Appointments_google_calendar_id') {

            $('#googleCalendarDropdown').val($(this).val());
        }

        $('div#calendar').wl_Calendar('removeEvents');
        $('div#calendar').wl_Calendar('addEventSource', '/admin/contacts/getCalendarEvents?calendarId=' + $('#Appointments_google_calendar_id').val());
    });
JS
);
?>
<? //if($Appointment->google_calendar_event_id): ?>
<div id="calendar-container" class="g12 p-mb5 rounded-text-box notes odd-static" style="<?=(($Appointment->id)? '' : 'display: none;')?>">
    <div class="12">
        <div class="g3"></div>
        <div class="g6">
            <select name="googleCalendarDropdown" id="googleCalendarDropdown" style="font-size: 20px;" class="g12"></select>
        </div>
    </div>
    <div id="calendar" style="margin: 1%"></div>
</div>
<? //endif; ?>