<?php
	Yii::import('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker');

	$this->widget('admin_module.extensions.moneymask.MMask', array(
			'element' => '#TransactionFieldValues_data_1, #TransactionFieldValues_data_2, #TransactionFieldValues_data_19',
			'currency' => 'PHP',
			'config' => array(
				'precision' => 0,
				'symbol' => '$',
				'showSymbol' => true,
			)
		)
	);

Yii::app()->clientScript->registerScript('addBuyer', <<<JS
    $("form#contact-form").submit(function() {
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
    });
JS
);
?>
<div id="c2"></div>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'contact-form',
		'enableAjaxValidation' => false,
	)
); ?>
    <!-- ====== BUYER INFO =============================================================================================== -->
    <div id="top-data" class="row">
        <div class="g100 p-p0 p-m0" id="transactions">
        <?php
            $title = ($this->action->id == 'edit') ? 'Edit Buyer :: ' . $model->contact->fullName : 'Add New Buyer';
            $this->beginStmPortletContent(array(
                    'handleTitle' => $title,
                    'handleIconCss' => 'i_shopping_cart'
                )
            ); ?>
        <div id="transaction-container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
        <div id="transaction-left" class="g6">
        <table class="">
        <tr>
            <th><?php echo $form->labelEx($model, 'contact_id'); ?>:</th>
            <td>
                <?php echo CHtml::tag('span', $htmlOptions = array('class' => 'contact-name'), $model->contact->fullName); ?>
                <?php echo $form->hiddenField($model, 'contact_id'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model, 'source_id'); ?>:</th>
            <td>
                <?php
                    echo $form->dropDownList($model, 'source_id', Sources::optGroupList($model->source_id), $htmlOptions = array(
                            'data-placeholder' => 'Select a Source'
                        )
                    );

                    $this->widget('admin_module.extensions.EChosen.EChosen', array(
                            'target' => 'select#' . CHtml::activeId($model, 'source_id'),
                            'options' => array(
                                'width' => '325px',
                            ),
                        )
                    );
                ?>
                <?php echo $form->error($model, 'source_id'); ?>
            </td>
        </tr>
        <tr>
            <th>Transaction Tags:</th>
            <td>
                <?php echo $form->dropDownList($model->transactionTagLu, 'idCollection', CHtml::listData(TransactionTags::model()->findAll(), 'id', 'name'), array(
                        'class' => 'chzn-select  g11',
                        'multiple' => 'multiple',
                        'data-placeholder' => 'Transaction Tags'
                    )
                ); ?>
                <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#TransactionTagLu_idCollection')); ?>
            </td>
        </tr>
        <!--<tr>-->
        <!--	<th>--><?php //echo $form->labelEx($model, 'internal_referrer_id'); ?><!--:</th>-->
        <!--	<td>-->
        <!--		--><?php //echo $form->dropDownList($model, 'internal_referrer_id',
        //			CMap::mergeArray(array('' => ''), CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->internal_referrer_id)->orderByName()->findAll(), 'id', 'fullName')), $htmlOptions = array(
        //				'style' => 'width:175px;',
        //				'data-placeholder' => ' ',
        //			)
        //		);
        //
        //			$this->widget('admin_module.extensions.EChosen.EChosen', array(
        //					'target' => 'select#Buyers_internal_referrer_id',
        //					'options' => array('allow_single_deselect' => true),
        //				)
        //			);
        //
        //		?>
        <!--		--><?php //echo $form->dropDownList($model, 'internal_referrer_assignment_type_id',
        //			CMap::mergeArray(array('' => ''), CHtml::listData(AssignmentTypes::model()->byTimeblockTypes()->findAll(), 'id', 'display_name')), $htmlOptions = array(
        //				'style' => 'width:145px;',
        //				'data-placeholder' => ' ',
        //			)
        //		);
        //
        //			$this->widget('admin_module.extensions.EChosen.EChosen', array(
        //					'target' => 'select#Buyers_internal_referrer_assignment_type_id',
        //					'options' => array('allow_single_deselect' => true),
        //				)
        //			);
        //
        //		?>
        <!--		--><?php //echo $form->error($model, 'internal_referrer_id'); ?>
        <!--	</td>-->
        <!--</tr>-->
        <tr>
            <th>Location:</th>
            <td>
                <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('location')->id . ']', $htmlOptions = array('class' => 'g11')) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $TransactionFields->getField('price_min')->label; ?>:</th>
            <td>
                <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('price_min')->id . ']', $htmlOptions = array(
                        'class' => 'g4',
                        'placeholder' => StmFormHelper::MONEY_PLACEHOLDER,
                    )
                )?>
                <div class="g1" align="center">-</div>
                <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('price_max')->id . ']', $htmlOptions = array(
                        'class' => 'g4',
                        'placeholder' => StmFormHelper::MONEY_PLACEHOLDER,
                    )
                )?>
            </td>
        </tr>
        <tr>
            <th><?php echo $TransactionFields->getField('motivation')->label; ?>:</th>
            <td>
                <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('motivation')->id . ']', $htmlOptions = array('class' => 'g11')) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $TransactionFields->getField('other_agent')->label; ?>:</th>
            <td>
                <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('other_agent')->id . ']', StmFormHelper::getBooleanList(), $htmlOptions = array(
                        'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                    )
                );?>
            </td>
        </tr>
        <tr>
            <th>Mortgage:</th>
            <td>
                <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('prequal_amount')->id . ']', $htmlOptions = array(
                        'class' => 'g6',
                        'placeholder' => StmFormHelper::MONEY_PLACEHOLDER,
                    )
                )?>
                <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $TransactionFieldValues,
                        'attribute' => 'data[' . $TransactionFields->getField('prequal_date')->id . ']',
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'class' => 'g4',
                            'placeholder' => 'Date'
                        ),
                    )
                );
                ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $TransactionFields->getField('lender')->label; ?>:</th>
            <td>
                <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('lender')->id . ']', $htmlOptions = array('class' => 'g11')) ?>
            </td>
        </tr>
        </tr>
        <?php
        $this->renderPartial('_formOpportunity',array('form'=>$form,'Opportunity'=>$Opportunity, 'model'=>$model));
        ?>
        <tr>
        <?php $this->renderPartial('_formAppointment',array('form'=>$form,'Appointment'=>$Appointment, 'model'=>$model));?>
        </table>
        </div>
        <div id="transaction-right" class="g6">
            <table class="">
                <tr>
                    <th><?php echo $form->labelEx($model, 'transaction_status_id'); ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($model, 'transaction_status_id', CHtml::listData($model->getStatusList(), 'id', 'name'), $htmlOptions = array(
                                'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                            )
                        ); ?>
                        <?php echo $form->error($model, 'transaction_status_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow top-align">Assignments:<span class="required">*</span></th>
                    <td id="assignment-table-cell">
                        <?php echo $this->renderPartial('_addlAssignmentTemplate', array(
                                'form' => $form,
                                'model' => $Assignment,
                                'componentTypeId' => $model->component_type_id,
                                'componentId' => $model->id,
                                'containerHtmlOptions' => array(
                                    'style' => 'display: none',
                                    'id' => 'addlAssignmentTemplate'
                                )
                            )
                        ); ?>
                        <div id="addlAssignmentRows" class="g12 p-p0" style="min-height: 0;">
                            <?php
                                if ($model->assignments) {
                                    foreach ($model->assignments as $i => $Assignment) {
                                        if($Assignment->assignment_type_id != AssignmentTypes::LOAN_OFFICER) {
                                            echo $this->renderPartial('_addlAssignmentTemplate', array(
                                                    'form' => $form,
                                                    'model' => $Assignment,
                                                    'componentTypeId' => $model->component_type_id,
                                                    'componentId' => $model->id,
                                                    'i' => $i,
                                                )
                                            );
                                        }
                                    }
                                } else {
                                    echo $this->renderPartial('_addlAssignmentTemplate', array(
                                                 'form' => $form,
                                                 'model' => $Assignment,
                                                 'componentTypeId' => $model->component_type_id,
                                                 'componentId' => $model->id,
                                                 'i' => $i,
                                                 'enabled'=>true,
                                             )
                                        );
                                }
                            ?>
                        </div>
                        <button type="button" id="add-assignment" class="text p-m0"><em class="icon i_stm_add"></em>Add Assignment</button>
                    </td>
                </tr>
                <tr>
                    <th class="narrow top-align">Preferred Lender:</th>
                    <td>
                        <?php
                        //@todo: this moves to action....
                        if(!($LenderAssignment = $model->assignmentLender)) {
                            $LenderAssignment = new Assignments;
                            $LenderAssignment->component_type_id = $model->component_type_id;
                            $LenderAssignment->component_id      = $model->id;
                            $LenderAssignment->assignment_type_id= AssignmentTypes::LOAN_OFFICER;
                        }

                        echo $form->dropDownList($LenderAssignment, '[lender]assignee_contact_id', CHtml::listData(
                                Contacts::model()->byActiveAdmins($together = false, $contactId = $LenderAssignment->contact->id)->byAdmins($together = false, $contactId = $LenderAssignment->contact->id, $authItemName = 'lender')->orderByName()
                                    ->findAll(), 'id', 'fullName'
                            ), $htmlOptions=array('empty'=>'Select Lender','class' => 'g5')
                        );
        //                echo $form->hiddenField($LenderAssignment, '[lender]component_type_id');
        //                echo $form->hiddenField($LenderAssignment, '[lender]component_id');
                        echo $form->hiddenField($LenderAssignment, '[lender]assignment_type_id');
                        echo $form->hiddenField($LenderAssignment, '[lender]id');
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>Target Date:</th>
                    <td>
                        <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model' => $TransactionFieldValues,
                                'attribute' => 'data[' . $TransactionFields->getField('target_date')->id . ']',
                                'options' => array(
                                    'showAnim' => 'fold',
                                ),
                                'htmlOptions' => array(
                                    'class' => 'g4',
                                ),
                            )
                        );
                        ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $TransactionFields->getField('best_time_to_look')->label; ?>:</th>
                    <td>
                        <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('best_time_to_look')->id . ']', $htmlOptions = array('class' => 'g11')) ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $TransactionFields->getField('own_rent')->label; ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('own_rent')->id . ']', array(
                                'Own' => 'Own',
                                'Rent' => 'Rent'
                            ), $htmlOptions = array(
                                'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                            )
                        );?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $TransactionFields->getField('home_to_sell')->label; ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('home_to_sell')->id . ']', StmFormHelper::getBooleanList(), $htmlOptions = array(
                                'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                            )
                        );?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $TransactionFields->getField('bedrooms')->label; ?>:</th>
                    <td>
                        <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('bedrooms')->id . ']', $htmlOptions = array('class' => 'g1 p-f0')) ?>

                        <span class="label p-vt"><?php echo $TransactionFields->getField('baths')->label; ?>:</span>
                        <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('baths')->id . ']', $htmlOptions = array('class' => 'g1 p-f0')) ?>

                        <span class="label p-vt"><?php echo $TransactionFields->getField('sq_feet')->label; ?>:</span>
                        <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('sq_feet')->id . ']', $htmlOptions = array('class' => 'g3 p-f0')) ?>
                    </td>
                </tr>
                <tr>
                    <th>Features:</th>
                    <td>
                        <?php echo $form->textArea($TransactionFieldValues, 'data[' . $TransactionFields->getField('features')->id . ']', $htmlOptions = array(
                                'class' => 'g12',
                                'placeholder' => 'Home Features',
                                'rows' => '6'
                            )
                        );?>
                    </td>
                </tr>
            </table>
        </div>
        </div>
        </div>
        <div id="notes-container">
            <div class="g12 p-mb5 rounded-text-box notes odd-static">
                <table class="">
                    <tr>
                        <th style="width:17%;">Notes:</th>
                        <td>
                            <?php echo $form->textArea($model, 'notes', $htmlOptions = array(
                                    'placeholder' => 'Notes',
                                    'class' => 'g12',
                                    'rows' => 10,
                                )
                            ); ?>
                            <?php echo $form->error($model, 'notes'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <?php if($apiKey = ApiKeys::model()->findByAttributes(array('type'=>'google','contact_id'=>Yii::app()->user->id))):
            $this->renderPartial('_formCalendar', array('Appointment'=>$Appointment));
         endif; ?>
        </div>
    </div>
</div>
<!-- ====== END SECTIONS =============================================================================================== -->
<div id="submit-button-wrapper">
	<button id="submit-button" type="submit" class="submit wide"><?php echo ($model->isNewRecord) ? 'Submit' : 'Update'; ?> Buyer</button>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php $this->endStmPortletContent(); ?>
