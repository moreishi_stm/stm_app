<?php $form=$this->beginWidget('CActiveForm', array(
	'method'=>'get',
	'id'=>'transaction-list-search',
));
?>
	<div class="g12">
		<div class="g2">
			<label class="g4">MLS #:</label>
			<span class="g8"><?php echo $form->textField($model,'listing_id');?></span>
		</div>
		<div class="g2">
			<label class="g4">Zip:</label>
			<span class="g8"><?php echo $form->textField($model,'zip');?></span>
		</div>
		<div class="g3">
			<label class="g4">Street Name:</label>
			<span class="g8"><?php echo $form->textField($model,'street_name');?></span>
		</div>
		<div class="g3">
			<label class="g2">Price:</label>
			<span class="g4"><?php echo $form->textField($model,'price_min');?></span>
			<span class="g1 p-tc p-mt10">to</span>
			<span class="g4"><?php echo $form->textField($model,'price_max');?></span>
		</div>
        <div class="g1">
            <span class="g4"><?php echo $form->dropDownList($model,'reverseProspectDays', array('3'=>'Within 90 Days', '2'=>'Within 60 Days', '1'=>'Within 30 Days'));?></span>
        </div>
		<div class="g1 submit" style="text-align:center">
			<?php echo CHtml::submitButton('Search', array('class'=>'button')); ?>
		</div>
	</div>
<? $this->endWidget(); ?>
