<?
$this->widget('admin_module.extensions.moneymask.MMask', array(
	'element' => '#TransactionFieldValues_data_56, #TransactionFieldValues_data_57, #TransactionFieldValues_data_70',
	'currency' => 'PHP',
	'config' => array(
		'precision' => 0,
		'symbol' => '',
		'showSymbol' => true,
	)
));
$this->widget('admin_module.extensions.moneymask.MMask', array(
	'element' => '#TransactionFieldValues_data_64',
	'currency' => 'PHP',
	'config' => array(
		'precision' => 0,
		'symbol' => '',
		'showSymbol' => true,
	)
));
?>
	<tr>
		<th>Current List Price:</th>
		<td>
			<?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('price_current')->id.']', $htmlOptions=array('class'=>'g4'))?>
			<span class="label strong g3 p-tr">Original Price:</span>
			<?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('price_original')->id.']', $htmlOptions=array('class'=>'g4'))?>
		</td>
	</tr>
	<tr>
		<th>MLS #:</th>
		<td>
			<?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('mls_number')->id.']', $htmlOptions=array('class'=>'g4'))?>
			<span class="label strong g3 p-tr">Recommend $:</span>
			<?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('price_recommend')->id.']', $htmlOptions=array('class'=>'g4'))?>
		</td>
	</tr>
	<tr>
		<th>IVR Ext#:</th>
		<td><?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('ivr_extension')->id.']', $htmlOptions=array('class'=>'g4'))?>
			<span class="label strong g3 p-tr">Lockbox #:</span>
			<?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('lockbox_number')->id.']', $htmlOptions=array('class'=>'g4'))?>
		</td>
	</tr>
	<tr>
		<th>Listing Date:</th>
		<td>
			<?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'model' => $TransactionFieldValues,
					'attribute' => 'data['.$TransactionFields->getField('listing_date')->id.']',
					'options' => array(
						'showAnim' => 'fold',
					),
					'htmlOptions' => array(
						'class' => 'g4',
					),
				));
			?>
            <span class="label strong g3 p-tr">Listing Expire Date:</span>
            <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('listing_expire_date')->id.']',
                    'options' => array(
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )); //listing expire: 66/listing_expire_date | prev expire: 53/expired_date
            ?>
		</td>
	</tr>
    <tr>
        <th>Upfront Fee:</th>
        <td>
            <?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('upfront_fee')->id.']', $htmlOptions=array('class'=>'g4'))?>
        </td>
    </tr>
    <tr>
        <th>Showing Instructions:</th>
        <td>
            <?php echo $form->textArea($TransactionFieldValues, 'data[' . $TransactionFields->getField('showing_instructions')->id . ']', $htmlOptions = array(
                    'class' => 'g12',
                    'placeholder' => 'Showing Instructions',
                    'rows' => '5'
                )
            )?>
        </td>
    </tr>
    <tr class="certified-pre-owned">
        <th>Certified Pre-Owned:</th>
        <td>
            <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('certified_pre_owned')->id . ']', array('No'=>'No','Full'=>'Full','Partial'=>'Partial'), $htmlOptions = array(
                    'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,'class'=>'g4'
                )
            );
            ?>
            <?php echo $form->error($TransactionFieldValues, 'data[' . $TransactionFields->getField('certified_pre_owned')->id . ']'); ?>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            <hr/>
        </th>
    </tr>
<? if(in_array($model->transaction_status_id, array(TransactionStatus::COMING_SOON_ID, TransactionStatus::ACTIVE_LISTING_ID, TransactionStatus::UNDER_CONTRACT_ID,TransactionStatus::CLOSED_ID, TransactionStatus::EXPIRED_ID, TransactionStatus::CANCELLED_ID))): ?>
    <tr>
        <th colspan="2">
            <h2>Home Inspections</h2>
        </th>
    </tr>
    <tr>
        <th>Home Inspection Ordered Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('home_inspection_ordered_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
			<span class="label strong g3 p-tr">Home Inspection Date:</span>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('home_inspection_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <th>Home Inspection Received Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('home_inspection_received_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            <hr/>
        </th>
    </tr>
    <tr>
        <th colspan="2">
            <h2>Appraisal</h2>
        </th>
    </tr>
    <tr>
        <th>Appraisal Ordered Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('appraisal_ordered_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
            <span class="label strong g3 p-tr">Appraisal Inspection Date:</span>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('appraisal_inspection_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <th>Appraisal Received Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('appraisal_received_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            <hr/>
        </th>
    </tr>
    <tr>
        <th colspan="2">
            <h2>Staging</h2>
        </th>
    </tr>
    <tr>
        <th>Staging Ordered Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('staging_ordered_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
            <span class="label strong g3 p-tr">Staging Appointment Date:</span>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('staging_inspection_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <th>Staging Report Received Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('staging_report_received_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            <hr/>
        </th>
    </tr>
    <tr>
        <th colspan="2">
            <h2>Photography</h2>
        </th>
    </tr>
    <tr>
        <th>Photography Ordered Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('photography_ordered_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
            <span class="label strong g3 p-tr">Photography Appointment Date:</span>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('photography_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <th>Photography Received Date:</th>
        <td>
            <? $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'model'          => $TransactionFieldValues,
                    'attribute' => 'data['.$TransactionFields->getField('photography_received_date')->id.']',
                    'mode' => 'datetime',
                    'options'        => array(
                        'showAnim'=>'fold',
                    ), 'htmlOptions' => array(
                        'class' => 'g4',
                    ),
                )
            );
            ?>
        </td>
    </tr>
<? endif; ?>