<?php
$js = <<<JS
	$('#appointment-button').click(function(){
		var buttonContent = $(this).html();
		if($(this).find('em.icon').hasClass('i_stm_delete')) {
			if(confirm('Confirm deleting this Appointment?')) {
				$('.appointment-containers').toggle('normal');
				buttonContent = buttonContent.replace('Delete Appointment','Add Appointment');
				buttonContent = buttonContent.replace('i_stm_delete','i_stm_add');
				$('#Appointments_toDelete').val('1');
			    $('.appointment-info').hide();
				$(this).html(buttonContent);
			}
		} else {
			$('.appointment-containers').toggle('normal');
			buttonContent = buttonContent.replace('Add Appointment','Delete Appointment');
			buttonContent = buttonContent.replace('i_stm_add','i_stm_delete');
			$('#Appointments_toDelete').val('');
			$('.appointment-info').show();
			$(this).html(buttonContent);
		}
	});

    $("#Appointments_location_ma").change(function(){
        if($(this).val()==3)
            $("#other-location-container").show("normal");
        else {
            $("#other-location-container").hide("normal");
            $("#Appointments_location_other").val("");
        }
    });

    $("#Appointments_is_signed").change(function(){
        if($(this).val()==0) {
            $("#not-signed-reason-container").show("normal");
            $("#signed-date-container").hide("normal");
        }
        else {
            $("#not-signed-reason-container").hide("normal");
            $("#Appointments_not_signed_reason").val("");
            $("#signed-date-container").show("normal");
        }
    });

    $("#Appointments_not_signed_reason").change(function(){
        if($(this).val()==4)
            $("#not-signed-reason-other-container").show("normal");
        else {
            $("#not-signed-reason-other-container").hide("normal");
            $("#Appointments_not_signed_reason_other").val("");
        }
    });

	$('#Appointments_met_status_ma').change(function(){
        var apptCancelRescheduleReason = $('.appointment-cancel-reschedule-reason');
	    if($(this).val()==3 || $(this).val()==4) {
            apptCancelRescheduleReason.show('normal');
	    } else {
	        if(apptCancelRescheduleReason.val() !== '' && confirm("Do you want to remove the Appointment Cancel/Reschedule reason?")) {
                apptCancelRescheduleReason.val('');
	        }
            apptCancelRescheduleReason.hide('normal');
	    }
	});

//    $("#Appointments_set_for_datetime").change(function(){
//        if(document.getElementById("Appointments_googleCalendarData_event_start") && $(this).val() !=='') {
//            var googleEventStart = $('#Appointments_googleCalendarData_event_start');
//            // if google calendar element exists and is blank
//            if(googleEventStart.val() !==undefined && googleEventStart.val() =='') {
//                googleEventStart.val($(this).val());
//            }
//        }
//    });
JS;
Yii::app()->clientScript->registerScript('transactionAppointmentFormScript', $js);

$appointmentDisplay = (($Appointment->isNewRecord || $Appointment->isEmpty) || $Appointment->toDelete)?'style="display:none;"': '';
?>
<tr>
	<th>Appointment:</th>
	<td>
		<?php echo ($appointmentDisplay)?'<button type="button" class="text" id="appointment-button"><em class="icon icon-only i_stm_add"></em>Add Appointment</button>'
			:'<button type="button" class="text" id="appointment-button" style="'.((Yii::app()->user->checkAccess('owner'))? '' : 'display: none;').'"><em class="icon icon-only i_stm_delete"></em>Delete Appointment</button>';
		?>
        <span class="label appointment-info" style="display: none;">*Appt fields must be complete or empty in its entirety.</span>
	</td>
</tr>
<tr <?php echo $appointmentDisplay; ?> class='appointment-containers'>
	<th>Appt Status:</th>
	<td>
        <?php echo $form->dropDownList($Appointment, 'met_status_ma', $Appointment->getMetStatusTypes(), $htmlOptions = array(
                'style' => 'width:125px;',
                'empty' => ' ',
                'class' => 'p-fl'
            )
        ); ?>
        <? $x = TaskTypes::model()->byComponentType(ComponentTypes::APPOINTMENTS)->findAll(); ?>
		<?php echo $form->dropDownList($Appointment, 'set_activity_type_id', CHtml::listData(TaskTypes::model()->byComponentType(ComponentTypes::APPOINTMENTS)->findAll(), 'id', 'name'),
			$htmlOptions = array(
				'style' => 'width:125px;',
                'empty' => ' ',
			)
		); ?>
		<span class="label strong g2 p-tr p-pr5">Set Method:</span>
        <?php echo $form->error($Appointment, 'met_status_ma'); ?>
        <?php echo $form->error($Appointment, 'set_activity_type_id'); ?>
	</td>
</tr>
<? if(Yii::app()->user->settings->appointments_dig_for_motivation_required):?>
<tr class='appointment-containers' <?php echo $appointmentDisplay; ?>>
    <th>How well did you Dig for Motivation:</th>
    <td>
        <?php echo $form->dropDownList($Appointment, 'dig_motivation_rating', StmFormHelper::getDropDownList(array('start'=>1, 'end'=>10)), $htmlOptions = array(
                'empty' => ' ',
                'style' => 'width:125px;',
                'class' => 'p-fl'
            )
        ); ?> <span class="p-fl p-pl10 p-pt5">(1 low - 10 high)</span>
        <?php echo $form->error($Appointment, 'dig_motivation_rating'); ?>
    </td>
</tr>
<? endif; ?>
<tr class='appointment-cancel-reschedule-reason' style="<?php echo ($Appointment->met_status_ma == Appointments::MET_STATUS_CANCEL || $Appointment->met_status_ma == Appointments::MET_STATUS_RESCHEDULE)? '' : 'display: none;';?>">
    <th>Cancel/Reschedule Reason:</th>
    <td>
        <?php echo $form->textField($Appointment, 'met_status_reason', $htmlOptions = array(
                'empty' => ' ',
                'class' => 'p-fl g8'
            )
        ); ?>
        <?php echo $form->error($Appointment, 'met_status_reason'); ?>
    </td>
</tr>
<tr <?php echo $appointmentDisplay; ?> class='appointment-containers p-mt5'>
	<th>Appointment Set by:</th>
	<td>
		<?php echo $form->dropDownList($Appointment, 'set_by_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $Appointment->set_by_id)->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions = array(
				'style' => 'width:125px;',
				'empty' => ''
			)
		); ?>
		<?php $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
											'model' => $Appointment,
											'attribute' => 'set_on_datetime',
                                            'defaultTime' => '12:00 am',
											'options' => array(
												'showAnim' => 'fold',
											),
											'htmlOptions' => array(
												'style' => 'width:185px;',
												'placeholder' => 'Set on Date & Time'
											),
											)
		);
		?>
		<?php echo $form->error($Appointment, 'set_by_id'); ?><?php echo $form->error($Appointment, 'set_on_datetime'); ?>
	</td>
</tr>
<tr <?php echo $appointmentDisplay; ?> class='appointment-containers'>
	<th>Appointment for:</th>
	<td>
		<?php echo $form->dropDownList($Appointment, 'met_by_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $Appointment->met_by_id)->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions = array(
				'style' => 'width:125px;',
				'empty' => ''
			)
		); ?>
		<?php
		$this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
											'model' => $Appointment,
											'attribute' => 'set_for_datetime',
											'defaultTime' => '12:00 am',
											'options' => array(
												'showAnim' => 'fold',
											),
											'htmlOptions' => array(
												'style' => 'width:185px;',
												'placeholder' => 'Set for Date & Time'
											),
											)
		);
		?>
		<?php echo $form->error($Appointment, 'met_by_id'); ?><?php echo $form->error($Appointment, 'set_for_datetime'); ?>
	</td>
</tr>
<tr <?php echo $appointmentDisplay; ?> class='appointment-containers'>
	<th>Appt Location:</th>
	<td>
		<?php echo $form->dropDownList($Appointment, 'location_ma', $Appointment->getLocationTypes(), $htmlOptions = array(
				'style' => 'width:125px;',
				'empty' => '',
				'class' => 'p-fl'
			)
		); ?>
		<div id="other-location-container" style="<?php echo ($Appointment->location_ma != 3) ? 'display:none;' : ''; ?>" class="g6 p-p0 p-pl4"><?php echo $form->textField($Appointment,
				'location_other', $htmlOptions = array(
					'placeholder' => 'Appointment Location',
					'class' => 'g12 p-f0'
				)
			)?></div>
		<?php echo $form->error($Appointment, 'location_ma'); ?>
	</td>
</tr>
<tr <?php echo $appointmentDisplay; ?> class='appointment-containers'>
	<th>Agreement Signed:</th>
	<td>
		<?php echo $form->dropDownList($Appointment, 'is_signed', StmFormHelper::getBooleanList(), $htmlOptions = array(
				'style' => 'width:125px;',
				'empty' => '',
				'class' => 'p-fl'
			)
		);?>
		<div id="not-signed-reason-container" style="<?php echo ($Appointment->is_signed == 0) ? '': 'display:none;'; ?>" class="p-fl p-p0 p-pl4">
			<?php echo $form->dropDownList($Appointment, 'not_signed_reason', $Appointment->getNotSignedReasonTypes(), $htmlOptions = array(
					'style' => 'width:125px;',
					'empty' => '',
					'class' => 'p-fl'
				)
			); ?>
		</div>
        <div id="signed-date-container" style="<?php echo ($Appointment->is_signed == 1) ? '' : 'display:none;' ; ?>" class="g4 p-p0 p-pl4">
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$Appointment,
                    'attribute'=>'signed_date',  // name of post parameter
                    'name'=>'Appointments[signed_date]',
                    'options'=>array(
                        'numberOfMonths'=>2,
                        'showAnim'=>'fold',
                        'dateFormat'=>'mm/dd/yy',
                    ),
                    'htmlOptions'=>array(
                        'style'=>'font-size: 12px;',
                        'class'=>'g11 p-fr',
                        'placeholder'=>'Signed Date',
                    ),
                ));
            ?>
            <?php echo $form->error($Appointment, 'signed_date'); ?>
        </div>
		<div id="not-signed-reason-other-container" style="<?php echo ($Appointment->not_signed_reason != 4) ? 'display:none;' : ''; ?>" class="g4 p-p0 p-pl4">
			<?php echo $form->textField($Appointment, 'not_signed_reason_other', $htmlOptions = array(
					'placeholder' => 'Reason Not Signed',
					'class' => 'g12 p-f0'
				)
			) ?>
		</div>
		<?php echo $form->error($Appointment, 'is_signed'); ?><?php echo $form->error($Appointment, 'not_signed_reason'); ?>
		<?php echo $form->hiddenField($Appointment, 'toDelete'); ?>
	</td>
</tr>
<?php if($apiKey = ApiKeys::model()->findByAttributes(array('type'=>'google','contact_id'=>Yii::app()->user->id))): ?>
    <tr <?php echo $appointmentDisplay; ?> class='appointment-containers'>
        <th>Google Calendar:</th>
        <td>
            <?php


            // if the user has permission for the calendar display data, if not display error message
            if((!$Appointment->google_calendar_id || ($Appointment->google_calendar_id && !empty($Appointment->googleCalendarData)))) {

                if(empty($Appointment->googleCalendarData)) {
                    switch($model->componentType->id) {
                        case ComponentTypes::BUYERS:
                            $eventName = 'Buyer Consult - '.$model->contact->fullName;
                            break;
                        case ComponentTypes::SELLERS:
                            if($model->address_id && ($address = Addresses::model()->findByPk($model->address_id))) {
                                $evenLocation = Yii::app()->format->formatAddress($address, array('lineBreak'=>false));
                            }
                            $eventName = 'Listing Appt - '.$model->contact->fullName;
                            break;
                        case ComponentTypes::RECRUITS:
                            $eventName = 'Recruit Appt - '.$model->contact->fullName;
                            break;
                    }

                    $eventDescription = 'Note: '.PHP_EOL.PHP_EOL.'CONTACT: '.PHP_EOL.$model->contact->fullName.PHP_EOL.Yii::app()->format->formatPrintEmails($model->contact->emails, $delimiter=', ', $defaultValue=null).PHP_EOL.Yii::app()->format->formatPrintPhones($model->contact->phones, $delimiter=', ', $defaultValue=null, $primaryLabel=null).PHP_EOL.PHP_EOL.'http://'.$_SERVER['SERVER_NAME'].str_replace('/edit/','/',$_SERVER['REQUEST_URI']).PHP_EOL.'(Appt Set by: '.Yii::app()->user->contact->fullName.' @ '.date('m/d/Y g:iA').')';
                }

                Yii::import('admin_module.components.StmOAuth2.Google', true);
                $stmGoogle = new \StmOAuth2\Google();
                $googleCalendarList = array();

                $success = false;
                try {
                    $listCalendars = $stmGoogle->listCalendars();
                    $success = true;
                }
                catch (\Google_Auth_Exception $e) {
                    echo "Your Google Calendar token is invalid. Please try again or re-do your Integration under Tools => Integration.";
                }
                catch (\Exception $e) {
                    \Yii::log(__CLASS__.' (:'.__LINE__.") Google Calendar List error: " . $e->getMessage(), \CLogger::LEVEL_ERROR);
                }

                if($success) {
                    foreach($listCalendars as $calendar) {
                        $googleCalendarList[$calendar->id] = 'Calendar - '.$calendar->summary;
                        if($calendar->primary) {
                            $calendarDefault = $calendar->id;
                        }
                    }
                }

                $Appointment->google_calendar_id = ($Appointment->google_calendar_id)? $Appointment->google_calendar_id : $calendarDefault;
                if($calendarDefault):
                ?>
                    <?php echo $form->dropDownList($Appointment, 'googleCalendarData[add_event]', array(1=>'Yes, Sync to Google Calendar Now',0=>'No, Ignore Google Calendar'), $htmlOptions = array('class'=>'g12')); ?>
                    <?php echo $form->textField($Appointment, 'googleCalendarData[event_name]', $htmlOptions = array('placeholder' => 'Google Calendar Event Name','class' => 'p-fl g12', 'value'=>$eventName)); ?>
                    <?php echo $form->error($Appointment, 'googleCalendarData[event_name]'); ?>
                    <?php echo $form->textField($Appointment, 'googleCalendarData[event_location]', $htmlOptions = array('placeholder' => 'Google Calendar Location','class' => 'p-fl g12','value'=>$evenLocation)); ?>
                    <?php echo $form->textArea($Appointment, 'googleCalendarData[event_description]', $htmlOptions = array('placeholder' => 'Google Calendar Notes/Description','class' => 'p-fl g12','rows'=>8, 'value'=>$eventDescription)); ?>
                    <?php
                    // @todo: find cleaner way. Currently empty state in googleCalendarData affects the display of this form
                    if(!$Appointment->googleCalendarData['event_duration']) {
                        $Appointment->googleCalendarData['event_duration'] = 90;
                    }
                    echo $form->dropDownList($Appointment, 'googleCalendarData[event_duration]', array('60'=>'1 Hour Appointment Time Block','90'=>'1.5 Hour Appointment Time Block','120'=>'2 Hour Appointment Time Block'), $htmlOptions = array('class'=>'g12')); ?>
                    <?php echo $form->dropDownList($Appointment, 'google_calendar_id', $googleCalendarList, $htmlOptions = array('placeholder' => 'Calendar Name','class' => 'p-fl g12')); ?>
                    <?php echo $form->hiddenField($Appointment, 'google_calendar_event_id'); ?>

            <?php
                endif;
            }
            // Google Calendar event exists but current user does not have permission
            elseif($Appointment->google_calendar_id && empty($Appointment->googleCalendarData)) { ?>

                <span class="contact-name">Google Calendar appointment exists, but you do not have permission to access this event.</span>

            <?php }
            ?>
        </td>
    </tr>

<?php endif; ?>
