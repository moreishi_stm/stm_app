<?php
$module = Yii::app()->controller->module->id;
$transactionType = Yii::app()->request->getQuery('transactionType');
Yii::app()->clientScript->registerScript('exportTransactionsList', <<<JS
    $('.export-button').click(function(){
        if(confirm("Are you sure you want to export this list?")) {
            $('body').prepend('<div class="loading-container loading"><em></em></div>');
            $('#export').val(1);

            window.open('/$module/$transactionType?' + $('#listview-search form').serialize());
            setTimeout(function () {
                $('div.loading-container.loading').remove();
                Message.create("notice", "Export is processing in the new window. Please wait...");
            }, 5000);
        }
        $('#export').val(0);
        return false;
    });
JS
);

$form = $this->beginWidget(
    'CActiveForm', array(
        'method' => 'get',
        'id'     => 'transaction-list-search',
    )
);

switch ($model->transactionType) {
    case Transactions::BUYERS: //To add later: Reverse search: zip, price (min & max), addr, mls
        ?>
        <div class="g12">
            <div class="g4">
                <div class="g3 text-right"><label>Name:</label></div>
                <div class="g4"><?php echo $form->textField(
                        $model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')
                    ); ?></div>
                <div class="g1"></div>
                <div class="g4"><?php echo $form->textField(
                        $model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')
                    ); ?></div>
            </div>
            <div class="g3">
                <div class="g3 text-right"><label>Email:</label></div>
                <span class="g9"><?php echo $form->textField($model->contact->emails, 'email'); ?></span>
            </div>
            <div class="g3">
                <div class="g4 text-right"><label>Phone:</label></div>
                <div class="g8">
                  <?php $this->widget(
                      'StmMaskedTextField', array(
                          'model'       => $model->contact->phones,
                          'attribute'   => 'phone',
                          'mask'        => '(999) 999-9999',
                          //Makes the full 10 digit phone number optional (enables partial search)
                          'htmlOptions' => array(
                              'placeholder' => '(999) 123-1234',
                              'class'       => 'phoneField'
                          )
                      )
                  ); ?>
                </div>
            </div>
            <div class="g1"></div>
            <div class="g1 submit" style="text-align: right;">
                <?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?>
            </div>
        </div>

        <div class="g12">
            <div class="g4">
                <div class="g3 text-right"><label>Status:</label></div>
                <div class="g9">
                    <?php echo $form->dropDownList(
                        $model, 'transaction_status_id', CHtml::listData($model->getStatusList(), 'id', 'name'),
                        $htmlOptions = array('empty'            => '',
                                             'class'            => 'chzn-select',
                                             'multiple'         => 'multiple',
                                             'style'            => 'width:100%;',
                                             'data-placeholder' => 'Status'
                        )
                    ); ?>
                </div>
                <?php $this->widget(
                    'admin_module.extensions.EChosen.EChosen', array('target' => 'select#Buyers_transaction_status_id')
                ); ?>
            </div>
            <div class="g3">
                <div class="g4 text-right"><label>Assigned to:</label></div>
                <div class="g7">
                    <?php echo $form->dropDownList(
                        $model, 'assignmentId',
                        CHtml::listData(
                            Contacts::model()->byActiveAdmins(false, $model->assignmentId)
                                ->byAssignmentComponentType(ComponentTypes::BUYERS)->orderByName()->findAll(), 'id',
                            'fullName'
                        ), $htmlOptions = array('empty' => '')
                    );

                    $this->widget(
                        'admin_module.extensions.EChosen.EChosen', array(
                            'target'  => 'select#Buyers_assignmentId',
                            'options' => array(
                                'allow_single_deselect' => true,
                                'width'                 => '100%'
                            )
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="g2">
                <div class="g6 text-right"><label>Has Phone:</label></div>
                <div class="g6">
                    <?php echo $form->dropDownList(
                        $model, 'hasPhone', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                    ); ?>
                </div>
            </div>
            <div class="g2">
                <div class="g8 text-right"><label>Last Follow-up Days:</label></div>
                <div class="g3">
                    <?php echo $form->dropDownList(
                        $model, 'lastActivityDaysSearch',
                        array(''  => '', 7 => '7+', 14 => '14+', 30 => '30+', 45 => '45+', 60 => '60+', 90 => '90+',
                              120 => '120+', 150 => '150+', 180 => '180+'), $htmlOptions = array()
                    ); ?>
                </div>
            </div>
            <?php if(Yii::app()->user->checkAccess('owner')):
                echo CHtml::hiddenField('export','0');
                ?>
                <div class="g1 submit" style="text-align:right; margin-left: 2%;"><?php echo CHtml::submitButton('Export', array('class' => 'button export-button')); ?></div>
            <?php endif; ?>
        </div>
        <div class="g12">
            <div class="g5">
                <div class="g2 text-right"><label>Sort:</label></div>
                <div class="g4">
                    <?php echo $form->dropDownList(
                        $model, 'dateSearchFilter', $model->dateSearchTypes, $htmlOptions = array(
                            'empty' => '',
                        )
                    ); ?>
                </div>
                <div class="g6">
                    <?php $this->widget(
                        'zii.widgets.jui.CJuiDatePicker', array(
                            'model'       => $model,
                            'attribute'   => 'fromSearchDate',
                            // name of post parameter
                            'options'     => array(
                                'showAnim'       => 'fold',
                                'dateFormat'     => 'mm/dd/yy',
                                'numberOfMonths' => 2,
                            ),
                            'htmlOptions' => array(
                                'style' => 'width: 80px; float: left; font-size: 12px;',
                            ),
                        )
                    );
                    ?>
                    <label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
                    <?php $this->widget(
                        'zii.widgets.jui.CJuiDatePicker', array(
                            'model'       => $model,
                            'attribute'   => 'toSearchDate',
                            // name of post parameter
                            'options'     => array(
                                'showAnim'       => 'fold',
                                'dateFormat'     => 'mm/dd/yy',
                                'numberOfMonths' => 2,
                            ),
                            'htmlOptions' => array(
                                'style' => 'width: 80px; float: left; font-size: 12px;',
                            ),
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="g2" style="margin-left: 40px;">
                <div class="g6 text-right"><label>No Phone Call:</label></div>
                <div class="g6">
                    <?php echo $form->dropDownList(
                        $model, 'searchNoCalls', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                    ); ?>
                </div>
            </div>
            <div class="g2">
                <div class="g5 text-right"><label>No Activity:</label></div>
                <div class="g7">
                    <?php echo $form->dropDownList(
                        $model, 'searchNoActivity', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                    ); ?>
                </div>
            </div>
            <div class="g2">
                <div class="g7 text-right"><label>No Saved Search:</label></div>
                <div class="g5">
                    <?php echo $form->dropDownList(
                        $model, 'searchNoSavedSearches', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                    ); ?>
                </div>
            </div>
        </div>
        <?
        break;

    case Transactions::SELLERS:
        ?>
        <div class="g12">
            <div class="g3">
                <label class="g3">Name:</label>
                <span class="g4"><?php echo $form->textField(
                        $model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')
                    ); ?></span>
                <span class="g4 p-ml10"><?php echo $form->textField(
                        $model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')
                    ); ?></span>
            </div>
            <div class="g3">
                <label class="g4">Address:</label>
                <span class="g8"><?php echo $form->textField($model, 'addressSearch'); ?></span>
            </div>
            <div class="g3">
                <label class="g4">Email:</label>
                <span class="g8"><?php echo $form->textField($model->contact->emails, 'email'); ?></span>
            </div>
            <div class="g3">
                <label class="g4">Phone:</label>
                    <span class="g7">
                      <?php $this->widget(
                          'StmMaskedTextField', array(
                              'model'       => $model->contact->phones,
                              'attribute'   => 'phone',
                              'mask'        => '(999) 999-9999',
                              //Makes the full 10 digit phone number optional (enables partial search)
                              'htmlOptions' => array(
                                  'placeholder' => '(999) 123-1234',
                                  'class'       => 'phoneField'
                              )
                          )
                      ); ?>
                    </span>
            </div>
            <div class="g3">
                <label class="g3">Status:</label>
					<span class="g8"><?php echo $form->dropDownList(
                            $model, 'transaction_status_id', CHtml::listData($model->getStatusList(), 'id', 'name'),
                            $htmlOptions = array('empty'            => '',
                                                 'class'            => 'chzn-select',
                                                 'multiple'         => 'multiple',
                                                 'style'            => 'width:205px;',
                                                 'data-placeholder' => 'Status'
                            )
                        ); ?></span>
                <?php $this->widget(
                    'admin_module.extensions.EChosen.EChosen', array('target' => 'select#Sellers_transaction_status_id')
                ); ?>
            </div>
            <div class="g3">
                <label class="g4">Assigned:</label>
            		<span class="g8"><?php echo $form->dropDownList(
                            $model, 'assignmentId',
                            CHtml::listData(
                                Contacts::model()->byActiveAdmins(false, $model->assignmentId)
                                    ->byAssignmentComponentType(ComponentTypes::SELLERS)->findAll(), 'id', 'fullName'
                            ), $htmlOptions = array(
                                'empty' => '',
                                'class' => 'g12'
                            )
                        );
                        $this->widget(
                            'admin_module.extensions.EChosen.EChosen', array(
                                'target'  => 'select#Sellers_assignmentId',
                                'options' => array(
                                    'allow_single_deselect' => true,
                                    'width'                 => '84%'
                                )
                            )
                        );
                        ?>
            </div>
            <div class="g3">
                <label class="g4">Source:</label>
					<span class="g8"><?php echo $form->dropDownList(
                            $model, 'source_id', CHtml::listData(
                                Sources::model()->byComponentTypeIds(array(ComponentTypes::SELLERS))->findAll(), 'id',
                                'name'
                            ),
                            $htmlOptions = array(
                                'empty'            => '',
                                'class'            => 'chzn-select g12',
                                'multiple'         => 'multiple',
                                'style'            => 'width:205px;',
                                'data-placeholder' => 'Sources'
                            )
                        ); ?></span>
                <?php $this->widget(
                    'admin_module.extensions.EChosen.EChosen', array('target' => 'select#Sellers_source_id')
                ); ?>
            </div>
            <div class="g2">
                <label class="g6">MLS#:</label>
                <span class="g6"><?php echo $form->textField($model, 'mlsSearch'); ?></span>
            </div>
            <div class="g4">
                <label class="g2">Sort:</label>
                    <span class="g3">
                        <?php echo $form->dropDownList(
                            $model, 'dateSearchFilter', $model->dateSearchTypes, $htmlOptions = array(
                                'empty' => '',
                                'class' => 'g12'
                            )
                        ); ?>
                    </span>
					<span class="g7">
						<?php $this->widget(
                            'zii.widgets.jui.CJuiDatePicker', array(
                                'model'       => $model,
                                'attribute'   => 'fromSearchDate',
                                // name of post parameter
                                'options'     => array(
                                    'showAnim'       => 'fold',
                                    'dateFormat'     => 'mm/dd/yy',
                                    'numberOfMonths' => 2,
                                ),
                                'htmlOptions' => array(
                                    'style' => 'width:90px;font-size: 12px;',
                                    'class' => 'g5',
                                ),
                            )
                        );
                        ?>
                        <label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
                        <?php $this->widget(
                            'zii.widgets.jui.CJuiDatePicker', array(
                                'model'       => $model,
                                'attribute'   => 'toSearchDate',
                                // name of post parameter
                                'options'     => array(
                                    'showAnim'       => 'fold',
                                    'dateFormat'     => 'mm/dd/yy',
                                    'numberOfMonths' => 2,
                                ),
                                'htmlOptions' => array(
                                    'style' => 'width:90px;font-size: 12px;',
                                    'class' => 'g5',
                                ),
                            )
                        );
                        ?>
					</span>
            </div>
            <div class="g2" style="">
                <label class="g9">Has Phone:</label>
                    <span class="g3">
                        <?php echo $form->dropDownList(
                            $model, 'hasPhone', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                        ); ?>
                    </span>
            </div>
            <div class="g2" style="">
                <label class="g9">Last Follow-up Days:</label>
                    <span class="g3">
                        <?php echo $form->dropDownList(
                            $model, 'lastActivityDaysSearch',
                            array(''  => '', 7 => '7+', 14 => '14+', 30 => '30+', 45 => '45+', 60 => '60+', 90 => '90+',
                                  120 => '120+', 150 => '150+', 180 => '180+'), $htmlOptions = array()
                        ); ?>
                    </span>
            </div>
            <div class="g2" style="">
                <label class="g9">No Saved Search:</label>
                    <span class="g3">
                        <?php echo $form->dropDownList(
                            $model, 'searchNoSavedSearches', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                        ); ?>
                    </span>
            </div>
            <div class="g1 submit" style="text-align:center">
                <label class="g7"></label>
                <span class="g5"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></span>
            </div>
            <div class="g4"></div>
            <div class="g2" style="">
                <label class="g9">No Activity:</label>
                    <span class="g3">
                        <?php echo $form->dropDownList(
                            $model, 'searchNoActivity', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                        ); ?>
                    </span>
            </div>

            <div class="g2" style="">
                <label class="g9">No Phone Call:</label>
                    <span class="g3">
                        <?php echo $form->dropDownList(
                            $model, 'searchNoCalls', array(1 => 'Yes'), $htmlOptions = array('empty' => '')
                        ); ?>
                    </span>
            </div>
            <div class="g2"></div>
            <?php if(Yii::app()->user->checkAccess('owner')):
                echo CHtml::hiddenField('export','0');
                ?>
                <div class="g1 submit" style="text-align:right; margin-left: 2.5%;"><?php echo CHtml::submitButton('Export', array('class' => 'button export-button')); ?></div>
            <?php endif; ?>
        </div>
        <?
        break;
}

$this->endWidget(); ?>
