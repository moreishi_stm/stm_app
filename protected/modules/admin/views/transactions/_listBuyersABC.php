<?php //notes, motivation
	$this->widget('admin_module.components.StmGridView', array(
			'id' => 'transactionGrid',
			'template' => '{pager}{summary}{items}{pager}{summary}',
			'dataProvider' => $DataProvider,
			'itemsCssClass' => 'datatables',
			'columns' => array(
				array(
					'type' => 'raw',
					'name' => 'Contact Info',
					'value' => 'CHtml::link($data->contact->fullName, array("/admin/buyers/".$data->id), array("target"=>"_blank"))."<br />".Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />".Yii::app()->format->formatPrintPhones($data->contact->phones)',
				),
				array(
					'type' => 'raw',
					'name' => 'Profile',
					'value' => '
				"<div><strong><span class=\"list-view-price\">".$data->buyerPriceRange."</span></strong></div>".
				"<div><strong>Target Date: </strong>".Yii::app()->format->formatGridValue("", Yii::app()->format->formatDate($data->getFieldValue("target_date")))."</div>".
				"<div>".Yii::app()->format->formatGridValue("Signed Agreement:", StmFormHelper::getYesNoName($data->appointment->is_signed, $displayEmpty=true))."</div>".
				"<div><strong>Prequal: </strong>".StmFormHelper::getYesNoName($data->getFieldValue("prequal_date"), $displayEmpty=true)."</div>".
				"<div><strong>Lender: </strong>".Yii::app()->format->formatGridValue("", $data->getFieldValue("lender"))."</div>"
			',
				),
				array(
					'type' => 'raw',
					'name' => 'Other Details',
					'value' => '
				"<div><strong>Area: </strong>".Yii::app()->format->formatGridValue("", $data->getFieldValue("location"))."</div>".
				"<div><strong>Motivation: </strong>".Yii::app()->format->formatGridValue("", $data->getFieldValue("motivation"))."</div>".
				"<div><strong>Source: </strong>".$data->source->name."</div>".
				"<div>".Yii::app()->format->formatGridValue("Last Activity: ", Yii::app()->format->formatDays($data->lastActivityLog->activity_date, array("defaultValue"=>"None", "daysLabel"=>"days ago"))).
				"<div>".Yii::app()->format->formatGridValue("Next Task:", Yii::app()->format->formatDays($data->nextTask->due_date, array("defaultValue"=>"None", "daysLabel"=>"days","isFuture"=>true)))."</div>"
//				"<div>".Yii::app()->format->formatGridValue("Assigned to:", $data->printGridviewAssignments())."</div>"
			',
				),
				array(
					'type' => 'raw',
					'name' => '',
					'value' => '
				"<div><a href=\"/admin/buyers/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Details</a></div>"
			',
					'htmlOptions' => array('style' => 'width:120px'),
				),
			),
		)
	);
?>
