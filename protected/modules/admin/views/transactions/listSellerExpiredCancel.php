<?php
	$this->breadcrumbs = array(
		'List' => array(
			'/admin\/' . $model->transactionType
		)
	);

	Yii::app()->clientScript->registerScript('search', "
$('form#transaction-list-search').submit(function() {
	$.fn.yiiGridView.update('expiredCancel', {
		data: $(this).serialize()
	});
	return false;
});
"
	);
?>
	<h1>Expired & Cancelled Listings</h1>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php
//			$this->renderPartial('_listSearchBox', array(
//					'model' => $model,
//				)
//			);
		?>
	</div><!-- search-form -->
<?php
	$this->_getGridExpiredCancel();
