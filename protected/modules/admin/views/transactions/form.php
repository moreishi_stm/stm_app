<?php
	 $this->breadcrumbs=array(
		 $model->contact->fullName=>'/admin/contacts/'.$model->contact->id,
		 (($this->action->id =='edit')?'Edit':'Add New').' Transaction'=>'',
	 );

	$countExisting = count($existingOpenTransactions);
	if($countExisting) {
		echo '<h3 style="color:#C20000;">'.$countExisting.' open '.$model->componentType->singularName .' Transaction exists. Verify that you still want to add a New Transaction.
			<br /><a href="/admin/contacts/'.$model->contact->id.'">Click here to view the contact and inspect existing transactions.</a></h3>';
	}

	$this->renderPartial($this->formPartialView, array(
		'model' => $model,
		'Appointment'=>$Appointment,
        'Opportunity'=>$Opportunity,
		'TransactionFields' => $TransactionFields,
		'TransactionFieldValues' => $TransactionFieldValues,
		'existingOpenTransactions' => $existingOpenTransactions,
	));