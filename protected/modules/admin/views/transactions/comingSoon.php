<?php
$this->breadcrumbs = array(
	'Coming Soon' => '',
	'List' => '',
);

Yii::app()->clientScript->registerScript('search', "
$('form#transaction-list-search').submit(function() {
	$.fn.yiiGridView.update('transactionGrid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="content-header">
	<h1>Coming Soon Listings</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php
//	$this->renderPartial('_listSearchBox', array(
//		'model' => $model,
//	));
	?>
</div><!-- search-form -->
<?php
	$this->renderPartial('_listSellers', array('DataProvider' => $DataProvider));
	//$this->_getGridViewTransactionGrid();
