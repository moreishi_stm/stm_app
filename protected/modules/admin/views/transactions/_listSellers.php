<?php

$this->widget('admin_module.components.StmGridView', array(
	'id'            => 'transactionGrid',
	'template'=>'{pager}{summary}{items}{summary}{pager}',
	'dataProvider'  => $DataProvider,
	'itemsCssClass' => 'datatables',
	'columns' => array(
	    'id',
		array(
			'type'  => 'raw',
			'name'  => 'Contact Info',
			'value' => '"<strong>".CHtml::link($data->contact->fullName, array("/admin/sellers/".$data->id), array("target"=>"_blank"))."</strong><br />"
						.Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />"
						.Yii::app()->format->formatPrintPhones($data->contact->phones)',
		),
		array(
			'type'=>'raw',
			'name'=>'Property',
			'value'=>'Yii::app()->controller->action->printPropertyInfo($data)',
		),
		array(
			'type'=>'raw',
			'name'=>'Profile',
			'value'=>'
				"<div>".Yii::app()->format->formatGridValue("Status:", $data->status->name)."</div>".
				"<div>".Yii::app()->format->formatGridValue("Source:", $data->source->name)."</div>".
				"<div><strong>".Yii::app()->format->formatGridValue("Assigned to:", $data->assignments[0]->contact->fullName)."</strong></div>".
				"<div>".Yii::app()->format->formatGridValue("Appt:", $data->getFieldValue("appt_timedate"))."</div>".
				"<div>".Yii::app()->format->formatGridValue("Met:", $data->getFieldValue("appt_met"))."</div>".
				"<div>".Yii::app()->format->formatGridValue("Next Task: ", Yii::app()->format->formatDays($data->nextTask->due_date, array("defaultValue"=>"None","isFuture"=>true)))."</div>"
//				"<div>".Yii::app()->format->formatGridValue("Follow-up:", $data->followUpFlag)."</div>"
			',
		),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/admin/sellers/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:80px'),
		),
	)
));

?>
