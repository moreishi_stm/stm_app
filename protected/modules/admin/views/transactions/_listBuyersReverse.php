<?php
	$this->widget('admin_module.components.StmGridView', array(
			'id' => 'reverseProspect',
			'template' => '{pager}{summary}{items}{pager}{summary}',
			'dataProvider' => $DataProvider,
			'itemsCssClass' => 'datatables',
			'columns' => array(
				'id',
				array(
					'type' => 'raw',
					'name' => 'Contact Info',
					'value' => 'CHtml::link($data->contact->fullName, array("/admin/buyers/".$data->id), array("target"=>"_blank"))."<br />".Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />".Yii::app()->format->formatPrintPhones($data->contact->phones)',
				),
				array(
					'type' => 'raw',
					'name' => 'Profile',
					'value' => '
				"<div><strong><span class=\"list-view-price\">".$data->buyerPriceRange."</span></strong></div>".
				"<div>".Yii::app()->format->formatGridValue("Area:", $data->getFieldValue("areas"))."</div>".
				"<div>".Yii::app()->format->formatGridValue("Status:", $data->status->name)."</div>".
				"<div><strong>".Yii::app()->format->formatGridValue("Assigned to:", $data->printGridviewAssignments())."</strong></div>".
				"<div>".Yii::app()->format->formatHomeViewSaved($data->contact->id)."</div>"
			',
				),
				array(
					'type' => 'raw',
					'name' => 'Activity',
					'value' => '
				"<div>".Yii::app()->format->formatGridValue("Last Login:", Yii::app()->format->formatDateTimeDays($data->contact->last_login, array("nearestMinHourDays"=>true)))."</div>".
				"<div>".Yii::app()->format->formatGridValue("Submit:", Yii::app()->format->formatDateTimeDays($data->contact->added))."</div>".
				"<div>".Yii::app()->format->formatGridValue("Last Activity: ", Yii::app()->format->formatDays($data->lastActivityLog->activity_date, array("defaultValue"=>"None", "daysLabel"=>"days ago"))).
				"</div>"
			',
				),
				array(
					'type' => 'raw',
					'name' => '',
					'value' => '
				"<div><a href=\"/admin/buyers/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Details</a></div>"
			',
					'htmlOptions' => array('style' => 'width:125px'),
				),
			),
		)
	);
?>
