<style type="text/css">
	.instructions {
		background-color: #B20000;
		padding: 8px;
		width: 80%;
		margin-right: auto;
		margin-left: auto;
		color: white;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}

	#add-new-contact-container {
		clear: both;
		padding: 14px;
		background-color: #FFF888;
		color: black;
	}
</style>
<?php
	$this->breadcrumbs = array(
		'Add New' => '',
	);

	//Yii::app()->clientScript->registerScript('search', '
	//
	//');
?>

<h1>
	Add New <?php echo $transactionTypeSingular; ?>
</h1>
<h3 id="instructions-top" class="instructions">Step 1: Enter the Contact information and click Submit.</h3>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBoxAdd', array('model' => $model)); ?>
</div>

<div id="contact-grid-container" style="display: none;">
	<?php
		if (!$_GET['Contacts']) {
			$model->id = 0;
		}

		$this->widget('admin_module.components.StmGridView', array(
				'id' => 'contact-grid',
				'dataProvider' => $DataProvider,
				'itemsCssClass' => 'datatables',
				'columns' => array(
					'id',
					'first_name',
					'last_name',
					'primaryEmail',
					array(
						'type' => 'raw',
						'name' => 'Existing Transactions',
						'value' => '$data->existingTransactionsGridView',
					),
					array(
						'type' => 'raw',
						'name' => '',
						'value' => '
					"<div><a href=\"/admin/' . $transactionTypeName . '/add/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_add grey-button\">Add ' . $transactionTypeSingular . ' Transaction</a></div>"
				',
						'htmlOptions' => array('style' => 'width:120px'),
					),
					array(
						'type' => 'raw',
						'name' => '',
						'value' => '"<div><a href=\"/admin/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Contact</a></div>"
				',
						'htmlOptions' => array('style' => 'width:100px'),
					),
				),
			)
		);
	?>
</div>
<h3 id="add-new-contact-container" class="instructions" style="display:none">
	If existing transaction is not found click this button.
	<div class="p-pt10">
		<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'transaction-list-search',
				'enableAjaxValidation' => false,
				'enableClientValidation' => false,
			),
			));
		?>
		<a href="javascript:void(0);" target="_blank" class="button gray icon i_stm_add grey-button">Add New Contact</a>
		<?php $this->endWidget(); ?>
	</div>
</h3>