<?php
$this->widget('admin_module.components.StmGridView', array(
    'id'=>'contact-grid',
    'dataProvider'=>$dataProvider,
    'itemsCssClass'=>'datatables',
    'summaryText' => '',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Name',
            'value'=>'CHtml::link($data->contact->fullName, array("/admin/buyers/".$data->id), array("target"=>"_blank"))',
			'htmlOptions'=>array('style'=>'width: 150px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Price / Location',
            'value'=>'Yii::app()->format->formatDollars($data->getFieldValue("price_min"))."<br />".$data->getFieldValue("location")',
			'htmlOptions'=>array('style'=>'width: 150px;'),
        ),
//        'source.name',
        array(
            'type'=>'raw',
            'name'=>'Signed',
            'value'=>'StmHtml::booleanImage($data->getFieldValue("agreement_signed"))',
            'htmlOptions'=>array('class'=>'p-tc','style'=>'width: 40px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Pre-qual / Lender',
            'value'=>'StmHtml::booleanImage(intval($data->getFieldValue("prequal_amount")))." "
            		.StmHtml::formatConditionalPrint(intval($data->getFieldValue("prequal_amount")), "formatDollars")',
			'htmlOptions'=>array('style'=>'width: 80px;'),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}',
			'htmlOptions'=>array('style'=>'width:30px;'),
            'viewButtonOptions' => array("target" => "_blank"),
            'buttons'=>array(
                'view'=>array(
                    'imageUrl'=>$this->module->imageAssetsUrl.'/search.png',
                    'url' =>'Yii::app()->controller->createUrl("buyers/".$data->id)',
                ),
            ),
        ),
    ),
));