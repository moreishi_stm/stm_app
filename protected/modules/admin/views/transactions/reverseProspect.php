<?php
/**
 * Reverse search example: Criteria = $500,000 - $600,00, 4 BR, 2 Baths, Zip Code: 32082
 * This means: Find buyers that have viewed homes (viewed_pages table) that meet the above criteria
 */

$this->breadcrumbs = array(
	'Reverse Prospect' => '#',
);

Yii::app()->clientScript->registerScript('search', "
$('form#transaction-list-search').submit(function() {
	$.fn.yiiGridView.update('reverseProspect', { data: $(this).serialize() });
	return false;
});
");
?>
<h1 style="line-height: 1.2;">Reverse Prospect</h1><hr />
<h3 class="p-pb10">Enter criteria below to find buyers that fit your needs:</h3>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBoxReverse',array(
		'model'=>$MlsProperties,
	)); ?>
</div>

<?php

$this->_getGridViewReverseProspect();

	if (isset($_GET['MlsProperties'])) {
} else {?>
<!--	<div id="reverseProspect" class="grid-view">-->
<!--		<table class="datatables">-->
<!--			<thead>-->
<!--			<tr>-->
<!--				<th id="transactionGrid_c0">ID</th><th id="transactionGrid_c1">Contact  Info</th><th id="transactionGrid_c2">Profile</th><th id="transactionGrid_c3">Activity</th><th id="transactionGrid_c4"></th></tr>-->
<!--			</thead>-->
<!--			<tbody>-->
<!--			<tr><td colspan="5" class="empty"><span class="empty">No results found.</span></td></tr>-->
<!--			</tbody>-->
<!--		</table><div class="keys" style="display:none" title="/admin/buyers/reverseProspect"></div>-->
<!--	</div>-->
<?php }