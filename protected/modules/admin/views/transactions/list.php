<?php
	$this->breadcrumbs = array(
		'List' => array(
			'/admin\/' . $model->transactionType
		)
	);

	Yii::app()->clientScript->registerScript('search', "
$('form#transaction-list-search').submit(function() {
	$.fn.yiiGridView.update('transactionGrid', {
		data: $(this).serialize()
	});
	return false;
});
"
	);
?>
	<style type="text/css">
		.follow-up-flag-definition {
			display: inline-block;
		}
	</style>
	<div style="position: absolute; left:27%; top: 11px;"><?php echo Transactions::FOLLOW_UP_FLAG; ?>
		<div class="follow-up-flag-definition"> = New Lead Follow-up time exceeded, No future Task, No activity in past <?php echo str_replace('-','',ActivityLog::DEFAULT_NO_ACTIVITY_DAYS); ?>.</div>
	</div>
	<div id="content-header">
		<h1><?php echo Yii::app()->format->formatProperCase(Yii::app()->request->getQuery('transactionType')); ?> List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?php
			$this->renderPartial('_listSearchBox', array(
					'model' => $model,
				)
			);
		?>
	</div><!-- search-form -->
<?php
	$this->_getGridViewTransactionGrid();
