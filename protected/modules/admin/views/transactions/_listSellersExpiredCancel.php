<?php

$this->widget('admin_module.components.StmGridView', array(
	'id'            => 'expiredCancel',
	'template'=>'{pager}{summary}{items}{summary}{pager}',
	'dataProvider'  => $DataProvider,
	'itemsCssClass' => 'datatables',
	'columns' => array(
	    'id',
		array(
			'type'  => 'raw',
			'name'  => 'Contact Info',
			'value' => '"<strong>".CHtml::link($data->contact->fullName, array("/admin/sellers/".$data->id), array("target"=>"_blank"))."</strong><br />"
						.Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />"
						.Yii::app()->format->formatPrintPhones($data->contact->phones)',
		),
		array(
			'type'=>'raw',
			'name'=>'Property',
			'value'=>'
				"<div><strong><u>".Yii::app()->format->formatAddress($data->address, array(streetOnly=>true))."</u></strong><br />".Yii::app()->format->formatAddress($data->address, array(CityStZipOnly=>true))."</div>".
				"<div>".$br = (Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false))? Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false).Yii::app()->format->formatGridValue(" / Baths:", $data->getFieldValue("baths", 0),false) : Yii::app()->format->formatGridValue("BR:", $data->getFieldValue("bedrooms", 0),false).
				"</div>".
				"<div>".Yii::app()->format->formatGridValue("SF:", $data->getFieldValue("sq_feet", 0),false)."</div>".
				"<div>".Yii::app()->format->formatGridValue("Submit:", Yii::app()->format->formatDateDays($data->contact->added))."</div>"
			',
		),
		array(
			'type'=>'raw',
			'name'=>'Profile',
			'value'=>'
				"<div>".Yii::app()->format->formatGridValue("Status:", $data->status->name)."</div>".
				"<div>".Yii::app()->controller->action->printExpiredCancelDate($data)."</div>".
				"<div>".Yii::app()->format->formatGridValue("Source:", $data->source->name)."</div>".
				"<div><strong>".Yii::app()->format->formatGridValue("Assigned to:", $data->assignments[0]->contact->fullName)."</strong></div>"
			',
		),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/admin/sellers/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:90px'),
		),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/admin/sellers/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:90px'),
		),
	)
));

?>
