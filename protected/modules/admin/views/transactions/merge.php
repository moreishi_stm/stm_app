<?php
	$this->breadcrumbs = array(
		'Merge Transactions' => '',
	);
	$js = <<<JS
	$("#merge-transaction-form").submit(function(){
		$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");

		var transactionId = $("#transactionId").val();
		$.post("/admin/$TransactionType/merge", {"id" : transactionId}, function(data) {
			if(data=="") {
				Message.create("error","Error: Could not find transactions.");
			} else {
				$("#primary-merge-id").html(transactionId);
				$("#primaryId").val(transactionId);
				$("#merge-portlet").show("normal");
				$("#merge-submit-button").removeClass("hidden");
				loadMergeData(data, "primaryMerge");

				if(data.toDeleteMerge) {
					loadMergeData(data, "toDeleteMerge");

					if(data.conflictFields.primary || data.conflictFields.toDelete) {
						loadConflictFields(data.conflictFields);
					}
				} else {
					Message.create("notice","Select a Transaction to merge and Delete.");
				}
			}
		},"json");
		equalHeight();
		$("div.loading-container.loading").remove();
		return false;
	});

	$("#confirm-merge-form").submit(function(){
		var errorFlag = false;
		$("select").each(function(i) {
			if($(this).val() == '') {
				$(this).addClass('error');
				if(!errorFlag)
					$(this).focus();
				errorFlag = true;
			}
		});

		if(errorFlag) {
			alert('Please make a Merge Action Selection');
			return false;
		}
	});

	$("#toDeleteId").change(function(){
		$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
		$(".transaction-details").remove();
		$.post("/admin/$TransactionType/merge", {"toDeleteId" : $("#toDeleteId").val(), "primaryId": $("#transactionId").val()}, function(data) {
			if(data=="") {
				Message.create("error","Error: Could not find transactions.");
			} else {
				if(typeof data.toDeleteMerge != null) {
					loadMergeData(data, "toDeleteMerge");
					if(data.conflictFields == null) {
						//@todo: show merge complete button as no conflicts exist

					} else if((typeof data.conflictFields.primary != 'undefined') || (typeof data.conflictFields.toDelete != 'undefined')) {
						loadConflictFields(data.conflictFields);
					}
				} else {
					Message.create("error","Error: Transaction was not found.");
				}
			}
		},"json");
		$("div.loading-container.loading").hide("normal").remove();
	});

	function loadMergeData(data, mergeType) {
		$.each( data[mergeType], function( key, value ) {
			if(key != "appointments") {
				$("#"+key+" ."+mergeType).html(value);
			} else {
				var appointmentContent = "";
				if(value !== "") {
					if(typeof data[mergeType]['appointments'] !== null) {
						$.each( data[mergeType]['appointments'], function( key2, value2 ) {
							if(value2 != null && value2 != '') {
								appointmentContent += (appointmentContent == '') ? '': '<br />';
								appointmentContent +=  '<span class="label">'+key2+ '</span>: ' +value2;
							}
						});
					}
				} else {
					appointmentContent = "None";
				}
				$("#"+key+" ."+mergeType).html(appointmentContent);
				if(mergeType == "toDeleteMerge") {
					if(($("#appointments .primaryMerge").html() != "None") && ($("#appointments .toDeleteMerge").html() != "None")) {
						$("#appointments .mergeAction").html('<select name="mergeAction['+key+']" id="mergeAction_'+key+'" data-id="'+key+'"><option></option><option>Use Primary</option><option>Ignore Primary</option></select>');
					}
				}
			}
		});

		if(mergeType == "primaryMerge") {
			$("#toDeleteId").html(data.toDeletedropdownList);
		} else {
			Message.create("warning","Review the Transaction to merge and Delete.");
		}

		$("."+mergeType+".view-details").attr("href","/admin/$TransactionType/"+data[mergeType].transactionId);
	}

	function equalHeight() {
		var primaryHeight = $("#primary-container").height();
		var toDeleteHeight = $("#to-delete-container").height();
		if(primaryHeight > toDeleteHeight){
			$("#to-delete-container").attr("style", "height:"+primaryHeight+"px");
		} else if(primaryHeight < toDeleteHeight) {
			$("#primary-container").attr("style", "height:"+toDeleteHeight+"px");
		}
	}

	function loadConflictFields(conflictFields) {

		if(typeof conflictFields != null) {

			if(conflictFields.primary) {
				$("#transaction-details-header").show("normal");
				$.each(conflictFields.primary, function( key, value ) {
					addTransactionDetailRow(conflictFields, key);
					if(typeof conflictFields.toDelete[key] != 'undefined') {
						delete conflictFields.toDelete[key];
					}
				});
			}
			if(conflictFields.toDelete) {
				$.each(conflictFields.toDelete, function( key, value ) {
					addTransactionDetailRow(conflictFields, key);
				});
			}
		}
	}

	function addTransactionDetailRow(conflictFields, key) {

		var label = '';
		var primaryValue = '';
		var toDeleteValue = '';

		if(typeof conflictFields.primary[key] != 'undefined') {
			primaryValue = conflictFields.primary[key].value;
		}

		if(typeof conflictFields.toDelete[key] != 'undefined') {
			if(label == '')
				label = conflictFields.toDelete[key].label;
			toDeleteValue = conflictFields.toDelete[key].value;
		}

		var newRow = '<tr class="transaction-details" data-fid="field-'+key+'">'+
			'<th class="narrow">'+label+':</th>'+
			'<td class="primary">'+primaryValue+'</td>'+
			'<td class="toDelete">'+toDeleteValue+'</td>'+
			'<td class="mergeAction">'+
				'<select name="mergeAction['+key+']" id="mergeAction_'+key+'" data-id="'+key+'">'+
					'<option></option>'+
					'<option value="1">Merge</option>'+
					'<option value="2">Use Primary</option>'+
					'<option value="3">Ignore Primary</option>'+
				'</select>'+
			'</td>'+
		'</tr>';

		$("#merge-table").append(newRow);
	}
//@todo: have some trailing record that there was a merge on this record
JS;

	Yii::app()->clientScript->registerScript('mergeTransaction', $js);
?>
<h1>Merge <?php echo ucwords($TransactionType); ?> Transaction</h1>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $form=$this->beginWidget('CActiveForm', array(
												  'action'=>'',
												  'method'=>'get',
												  'id'=>'merge-transaction-form',
												  )); ?>
	<div class="g8">
		<label class="g10">Enter Transaction ID you want to merge:</label>
		<span class="g2"><?php echo CHtml::textField('transactionId');?></span>
	</div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SUBMIT', array('class'=>'button')); ?></div>
	<?php $this->endWidget(); ?>
</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
										  'id' =>'confirm-merge-form',
										  'enableAjaxValidation' => false,
										  ));
$this->beginStmPortletContent(array(
									   'handleTitle'   => 'Transactions',
									   'handleIconCss' => 'i_wizard',
									   'id'=>'merge-portlet',
									   'containerHtmlOptions'=>array('style'=>'display:none;'),
									   ));
?>
<style type="text/css">

</style>

<div id="primary-container" class="g12 p-mb5 p-mr1 rounded-text-box odd-static">
	<table id="merge-table" class="container">
		<tr style="vertical-align: middle; height: 45px;">
			<th class="narrow" style="vertical-align: middle;">Transaction ID:</th>
			<th style="width:30%;background-color: #ACF19A; text-align: left; vertical-align: middle;"><span id="primary-merge-id" style="font-weight: bold;"></span>
				<?php echo CHtml::hiddenField('primaryId'); ?>
			</th>
			<th class="p-tl" style="width:30%;background-color: #FFBABA;vertical-align: middle; text-align: left;">
				Merge & <span style="color:#C20000;font-weight: bold;text-decoration: underline">DELETE</span>:
				<?php echo $form->dropDownList($model,'id',array(), $htmlOptions = array('class'=>'g6 p-f0','id'=>'toDeleteId'));?>
			</th>
			<th style="width:27.5%;background-color: #BACBFF;vertical-align: middle; text-align: left;">Merge Status / Action Required</th>
		</tr>
		<tr>
			<th class="narrow">Page Link:</th>
			<td><span class="primaryMerge"><a class="primaryMerge view-details text-button" href="#" target="_blank"><em class="icon i_stm_search"></em>View Details</a></span></td>
			<td><span class="toDeleteMerge"><a class="toDeleteMerge view-details text-button" href="#" target="_blank"><em class="icon i_stm_search"></em>View Details</a></span></td>
			<td><span class="mergeAction"></span></td>
		</tr>
		<tr id="createDate">
			<th class="narrow">Create Date:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="source">
			<th class="narrow">Source:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="addressId">
			<th class="narrow">Address ID:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td> <!--i_stm_success-->
		</tr>
		<tr id="closings">
			<th class="narrow">Closings:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="activityLog">
			<th class="narrow">Activity Log:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="tasks">
			<th class="narrow">Tasks:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="assignments">
			<th class="narrow">Assignments:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="documents">
			<th class="narrow">Documents:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="appointments">
			<th class="narrow">Appointments:</th>
			<td class="primaryMerge"></td>
			<td class="toDeleteMerge"></td>
			<td class="mergeAction"></td>
		</tr>
		<tr id="transaction-details-header" style="display:none;">
			<th class="narrow"></th>
			<td colspan="4" class="p-tc" style="background-color: #CCC;"><span class="label">Below are Transaction Details that have different values. Please select a merge action.</span></td>
		</tr>
	</table>
</div>
<?php $this->endStmPortletContent(); ?>

<div id="submit-button-wrapper">
	<?php echo CHtml::htmlButton('CONFIRM MERGE', $htmlOptions = array(
			'id' => 'merge-submit-button',
			'class' => 'submit wide hidden',
			'type' => 'submit',
		)); ?>
</div>
<?php $this->endWidget(); ?>

