<?php
	Yii::import('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
	$this->widget('admin_module.extensions.moneymask.MMask', array(
			'element' => '#TransactionFieldValues_data_55',
			'currency' => 'PHP',
			'config' => array(
				'precision' => 0,
				'symbol' => '',
				'showSymbol' => true,
			)
		)
	);
$hasMarketSnapshop = (Yii::app()->user->settings->market_snapshot) ? 1 : 0;
$statusCancel = TransactionStatus::CANCELLED_ID;
$statusListing = TransactionStatus::ACTIVE_LISTING_ID;
$pendingListing = TransactionStatus::UNDER_CONTRACT_ID;
$js = <<<JS
    $("form#contact-form").submit(function() {
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        if($hasMarketSnapshop == 1 && validateMarketSnapshotFields() == false) {
            $("div.loading-container.loading").remove();
            Message.create('error','');
            return false;
        }
    });
	$('#Sellers_transaction_status_id').change(function(){
		if($('#Sellers_transaction_status_id').val() == $statusCancel) {
			$('.cancel-date-container').show('normal');
		} else {
			$('.cancel-date-container').hide('normal');
			$('#TransactionFieldValues_data_69').val('');
		}

        if($('#Sellers_transaction_status_id').val() == $statusListing || $('#Sellers_transaction_status_id').val() == $pendingListing) {
            $('#email-frequency-container').show('normal');
        }
        else {
            $('#email-frequency-container').hide('normal');
        }
	});

	$('#TransactionFieldValues_data_102, #Sellers_address_id, #TransactionFieldValues_data_47, #TransactionFieldValues_data_48, #TransactionFieldValues_data_50, #Sellers_mls_property_type_id').change(function(){
        validateMarketSnapshotFields();
	});

	function validateMarketSnapshotFields()
	{
        var missingFields = '';

	    if($('#TransactionFieldValues_data_102').val() == '') {
	        return;
	    }

	    if($('#Sellers_address_id').val() == '') {
	        missingFields += (missingFields == '') ? '' : ', ';
            missingFields += 'Address';
	    }

	    if($('#TransactionFieldValues_data_47').val() == '') {
	        missingFields += (missingFields == '') ? '' : ', ';
            missingFields += 'Bedrooms';
	    }

	    if($('#TransactionFieldValues_data_48').val() == '') {
	        missingFields += (missingFields == '') ? '' : ', ';
            missingFields += 'Baths';
	    }

	    if($('#TransactionFieldValues_data_50').val() == '') {
	        missingFields += (missingFields == '') ? '' : ', ';
            missingFields += 'Sq. Feet';
	    }

	    if($('#Sellers_mls_property_type_id').val() == '') {
	        missingFields += (missingFields == '') ? '' : ', ';
            missingFields += 'Property Type';
	    }

	    if(missingFields != '') {
            alert(missingFields + ' is required for Market Snapshot. Market Snapshot frequency has been cleared. Please try again after providing the required fields.');
            //$('#TransactionFieldValues_data_102').val('');
	        return false;
	    }
	    else {
	        return true;
	    }
	}

	$('.propertyFeatures').bind('keyup',function(){
		if($(this).hasClass('topText')) {
			var topText = $('.propertyFeatures.topText').val();
			$('textarea.propertyFeatures.bottomText').val(topText);
		} else {
			var bottomText = $('.propertyFeatures.bottomText').val();
			$('textarea.propertyFeatures.topText').val(bottomText);
		}
	});
JS;
Yii::app()->clientScript->registerScript('transactionFormScript', $js);
?>
<style type="text/css">
    #transactions #seller-interview th.narrow { padding-top: 16px; }
</style>
<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'contact-form',
		'enableAjaxValidation' => false,
	)
); ?>
<!-- ====== SELLERS INFO =============================================================================================== -->
<div id="top-data" class="row">
<div class="g100 p-p0 p-m0" id="transactions">
<?php
	if(1) {

	}

	$title = ($this->action->id == 'edit') ? 'Edit Seller :: ' . $model->contact->fullName : 'Add New Seller';

	$this->beginStmPortletContent(array(
			'handleTitle' => $title,
			'handleIconCss' => 'i_key_2'
		)
	);
?>
<div id="transaction-container">
<div class="g12 p-mb5 rounded-text-box odd-static">
    <div id="transaction-seller-left" class="g5">
        <table class="">
            <tr>
                <th><?php echo $form->labelEx($model, 'contact_id'); ?>:</th>
                <td>
                    <?php echo CHtml::tag('span', $htmlOptions = array('class' => 'contact-name'), $model->contact->fullName); ?>
                    <?php echo $form->hiddenField($model, 'contact_id'); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form->labelEx($model, 'transaction_status_id'); ?>:</th>
                <td>
                    <?php echo $form->dropDownList($model, 'transaction_status_id', CHtml::listData($model->getStatusList(), 'id', 'name'), $htmlOptions = array(
                            'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                        )
                    );
                    ?>
                    <?php echo $form->error($model, 'transaction_status_id'); ?>
                </td>
            </tr>
            <tr class="cancel-date-container" <?php echo ($model->transaction_status_id == TransactionStatus::CANCELLED_ID || $model->transaction_status_id == TransactionStatus::EXPIRED_ID) ? '' : 'style="display:none;"'; ?>>
                <th><?php echo $TransactionFields->getField('cancel_date')->label; ?>:</th>
                <td>
                    <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                          'model' => $TransactionFieldValues,
                                                                          'attribute' => 'data['.$TransactionFields->getField('cancel_date')->id.']',
                                                                          'options' => array(
                                                                              'showAnim' => 'fold',
                                                                          ),
                                                                          'htmlOptions' => array(
                                                                              'class' => 'g6',
                                                                          ),
                                                                          ));
                    echo $form->error($TransactionFieldValues, 'data[' . $TransactionFields->getField('cancel_date')->id . ']'); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form->labelEx($model, 'source_id'); ?>:</th>
                <td>
                    <?php echo $form->dropDownList($model, 'source_id', Sources::optGroupList($model->source_id), $htmlOptions = array(
                            'style' => 'width:325px;',
                            'data-placeholder' => 'Select a Source'
                        )
                    );

                        $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Sellers_source_id'));
                    ?>
                    <?php echo $form->error($model, 'source_id'); ?>
                </td>
            </tr>
            <tr id="email-frequency-container"  <?php echo (in_array($model->transaction_status_id, array(TransactionStatus::ACTIVE_LISTING_ID, TransactionStatus::COMING_SOON_ID, TransactionStatus::UNDER_CONTRACT_ID)))? '': 'style="display:none;"';?>>
                <th><?php echo $form->labelEx($model, 'email_frequency_ma'); ?>:</th>
                <td>
                    <?php echo $form->dropDownList($model, 'email_frequency_ma', $model->getEmailFrequencyTypes(), $htmlOptions = array(
                            'style' => 'width:325px;',
                        )
                    );

                    $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Sellers_email_frequency_ma','options'=>array('width' => '325px')));
                    ?>
                    <?php echo $form->error($model, 'email_frequency_ma'); ?>
                </td>
            </tr>
            <tr>
                <th>Transaction Tags:</th>
                <td>
                    <?php echo $form->dropDownList($model->transactionTagLu, 'idCollection', CHtml::listData(TransactionTags::model()->findAll(), 'id', 'name'), array(
                            'class' => 'chzn-select  g11',
                            'multiple' => 'multiple',
                            'data-placeholder' => 'Transaction Tags'
                        )
                    ); ?>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#TransactionTagLu_idCollection')); ?>
                </td>
            </tr>
    <!--		<tr>-->
    <!--			<th>--><?php //echo $form->labelEx($model, 'internal_referrer_id'); ?><!--:</th>-->
    <!--			<td>-->
    <!--				--><?php //echo $form->dropDownList($model, 'internal_referrer_id',
    //					CMap::mergeArray(array('' => ''), CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->internal_referrer_id)->findAll(), 'id', 'fullName')), $htmlOptions = array(
    //						'style' => 'width:150px;',
    //						'data-placeholder' => ' ',
    //					)
    //				);
    //
    //					$this->widget('admin_module.extensions.EChosen.EChosen', array(
    //							'target' => 'select#Sellers_internal_referrer_id',
    //							'options' => array('allow_single_deselect' => true),
    //						)
    //					);
    //
    //				?>
    <!--				--><?php //echo $form->dropDownList($model, 'internal_referrer_assignment_type_id',
    //					CMap::mergeArray(array('' => ''), CHtml::listData(AssignmentTypes::model()->byTimeblockTypes()->findAll(), 'id', 'display_name')), $htmlOptions = array(
    //						'style' => 'width:145px;',
    //						'data-placeholder' => ' ',
    //					)
    //				);
    //
    //					$this->widget('admin_module.extensions.EChosen.EChosen', array(
    //							'target' => 'select#Sellers_internal_referrer_assignment_type_id',
    //							'options' => array('allow_single_deselect' => true),
    //						)
    //					);
    //
    //				?>
    <!--				--><?php //echo $form->error($model, 'internal_referrer_id'); ?>
    <!--			</td>-->
    <!--		</tr>-->
            <tr>
                <th><?php echo $TransactionFields->getField('sale_type_ma')->label; ?>:</th>
                <td>
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('sale_type_ma')->id . ']', Transactions::getSaleTypes(), $htmlOptions = array(
                            'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                        )
                    ); ?>
                    <?php echo $form->error($TransactionFieldValues, 'data[' . $TransactionFields->getField('sale_type_ma')->id . ']'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow top-align">Assignments:<span class="required">*</span></th>
                <td id="assignment-table-cell">
                    <?php echo $this->renderPartial('_addlAssignmentTemplate', array(
                            'form' => $form,
                            'model' => $Assignment,
                            'componentTypeId' => $model->component_type_id,
                            'componentId' => $model->id,
                            'containerHtmlOptions' => array(
                                'style' => 'display: none',
                                'id' => 'addlAssignmentTemplate'
                            )
                        )
                    ); ?>
                    <div id="addlAssignmentRows" class="g12 p-p0" style="min-height: 0;">
                        <?php
                            if ($model->assignments) {
                                foreach ($model->assignments as $i => $Assignment) {
                                    echo $this->renderPartial('_addlAssignmentTemplate', array(
                                            'form' => $form,
                                            'model' => $Assignment,
                                            'componentTypeId' => $model->component_type_id,
                                            'componentId' => $model->id,
                                            'i' => $i,
                                        )
                                    );
                                }
                            } else {
                                echo $this->renderPartial('_addlAssignmentTemplate', array(
                                     'form' => $form,
                                     'model' => $Assignment,
                                     'componentTypeId' => $model->component_type_id,
                                     'componentId' => $model->id,
                                     'i' => $i,
                                     'enabled'=>true,
                                     )
                                );
                            }
                        ?>
                    </div>
                    <button type="button" id="add-assignment" class="text p-m0"><em class="icon i_stm_add"></em>Add Assignment</button>
                </td>
            </tr>
            <? if(Yii::app()->user->settings->market_snapshot == 1): ?>
            <tr>
                <th>Market Snapshot:</th>
                <td>
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('market_snapshot')->id . ']', array(1 => 'Weekly', 2 => 'Every 2 Weeks', 4 => 'Monthly'), $htmlOptions = array('class' => 'g8', 'empty' => 'None')); ?>
                    <?php echo $form->error($TransactionFieldValues, 'data[' . $TransactionFields->getField('market_snapshot')->id . ']'); ?>
                </td>
            </tr>
            <tr>
                <th>Property Type:</th>
                <td>
                    <?php echo $form->dropDownList($model, 'mls_property_type_id', CHtml::listData(MlsPropertyTypeBoardLu::model()->findAllByAttributes(array('mls_board_id'=>Yii::app()->user->board->id,'status_ma'=>'1')), 'propertyType.id','propertyType.name'), $htmlOptions = array('class' => 'g8', 'empty' => '')); ?>
                    <?php echo $form->error($model, 'mls_property_type_id'); ?>
                </td>
            </tr>
            <? endif;?>
            <tr>
                <th>Address:</th>
                <td>
                    <div class="g12">
                        <?php
                        $contactAddresses = $model->tempContact->addresses;
                        $contactAddresses = (count($contactAddresses) > 0) ? CHtml::listData($contactAddresses, 'id', 'fullAddress') : array();

                        echo $form->dropDownList($model, 'address_id', $contactAddresses, $htmlOptions = array(
                                'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                                'class' => 'g8 address-dropdown'
                            )
                        );
                        ?>
                        <?php if(!$model->isNewRecord): ?>
                            <a href="javascript:void(0);" id="add-address-button" class="text-button p-fl"><em
                                class="icon i_stm_add"></em>Add New</a>
                        <?php endif; ?>
                </div>
                    <?php if($model->isNewRecord): ?>
                        Note: You can add a new address from here AFTER you save this new Seller.
                    <?php endif; ?>

                </td>
            </tr>
            <tr>
                <th><?php echo $TransactionFields->getField('bedrooms')->label; ?>:</th>
                <td>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('bedrooms')->id . ']', $htmlOptions = array('class' => 'g2')); ?>
                    <span class="label strong g2 p-tr">Baths:</span>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('baths')->id . ']', $htmlOptions = array('class' => 'g2')); ?>

                    <span class="label strong g3 p-tr">Half Baths:</span>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('half_baths')->id . ']', $htmlOptions = array('class' => 'g2')); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $TransactionFields->getField('sq_feet')->label; ?>:</th>
                <td>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('sq_feet')->id . ']', $htmlOptions = array('class' => 'g3')); ?>
                    <span class="label strong g2 p-tr"></span>
                    <span class="label strong g4 p-tr">Year Built:</span>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('year_built')->id . ']', $htmlOptions = array('class' => 'g2')); ?>
                </td>
            </tr>
            <tr>
                <th>Subdivision:</th>
                <td>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('area')->id . ']', $htmlOptions = array('class' => 'g12')); ?>
                </td>
            </tr>
            <tr>
                <th>Features:</th>
                <td>
                    <?php echo CHtml::textArea('property_features', $TransactionFieldValues->data[$TransactionFields->getField('features')->id], $htmlOptions=array('class'=>'g12 propertyFeatures topText', 'placeholder'=>'Home Features', 'rows'=>'4'));?>
                </td>
            </tr>
            <tr>
                <th>Notes:</th>
                <td>
                    <?php echo $form->textArea($model, 'notes', $htmlOptions = array(
                            'class' => 'g12',
                            'placeholder' => 'Notes',
                            'rows' => '5'
                        )
                    )?>
                </td>
            </tr>
            <?php
                    $this->renderPartial('_formOpportunity',array('form'=>$form,'Opportunity'=>$Opportunity, 'model'=>$model));
            ?>
            <tr>
                <th colspan="2">
                    <hr/>
                </th>
            </tr>
            <?php $this->renderPartial('_formAppointment',array('form'=>$form,'Appointment'=>$Appointment, 'model'=>$model));?>
            <tr>
                <th colspan="2">
                    <hr/>
                </th>
            </tr>
            <tr>
                <th colspan="2">
                    <h2>Previous Expired Listing Info</h2>
                </th>
            </tr>
            <tr>
                <th>Prev Expired Price:</th>
                <td>
                    <?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('expired_price')->id.']', $htmlOptions=array('class'=>'g4'))?>
                    <span class="label strong g3 p-tr">Prev Listing Office:</span>
                    <?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('expired_listing_office')->id.']', $htmlOptions=array('class'=>'g4'))?>
            </tr>
            <tr>
                <th>Prev Expired Date:</th>
                <td>
                    <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model' => $TransactionFieldValues,
                            'attribute' => 'data['.$TransactionFields->getField('expired_date')->id.']',
                            'options' => array(
                                'showAnim' => 'fold',
                            ),
                            'htmlOptions' => array(
                                'class' => 'g4',
                            ),
                        ));
                    ?>
                    <span class="label strong g3 p-tr">Prev Listing Agent:</span>
                    <?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('expired_listing_agent')->id.']', $htmlOptions=array('class'=>'g4'))?>
                </td>
            </tr>
            <tr>
                <th>Prev MLS #:</th>
                <td>
                    <?php echo $form->textField($TransactionFieldValues, 'data['.$TransactionFields->getField('expired_mls_number')->id.']', $htmlOptions=array('class'=>'g4'))?>
                </td>
            </tr>
            <tr>
                <th>Prev Agent Remarks:</th>
                <td>
                    <?php echo $form->textArea($TransactionFieldValues, 'data[' . $TransactionFields->getField('expired_agent_remarks')->id . ']', $htmlOptions = array(
                            'class' => 'g12',
                            'placeholder' => 'Previous Agent Remarks',
                            'rows' => '5'
                        )
                    )?>
                </td>
            </tr>
            <tr>
                <th colspan="2">
                    <hr/>
                </th>
            </tr>
        </table>
    </div>

    <!-- ====== TRANSACTION FIELDS TOP-RIGHT ========================================================================================= -->
    <div id="transaction-seller-right" class="g7">
        <table class="">
            <?php
            $this->renderPartial('_formSellers_listing_fields', array(
                                                                'model' => $model,
                                                                'form' => $form,
                                                                'TransactionFields' => $TransactionFields,
                                                                'TransactionFieldValues' => $TransactionFieldValues,
                                                                )
            );
            ?>
        </table>
    </div>
    <div class="p-clr"></div>
    <div id="submit-button-wrapper">
        <button id="submit-button" type="submit" class="submit wide"><?php echo ($model->isNewRecord) ? 'Submit' : 'Update'; ?> Seller</button>
    </div>
    <hr/>
    <!-- ====== TRANSACTION FIELDS =============================================================================================== -->
    <div id="seller-interview" class="g12 custom-form">
        <h2>Seller Interview</h2>
        <table class="">
            <tr>
                <th class="narrow">1)</th>
                <td>
                    <?php echo $TransactionFields->getField('motivation')->label; ?>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('motivation')->id . ']', $htmlOptions = array('class' => 'g10')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">2)</th>
                <td>
                    <?php echo $TransactionFields->getField('where_moving_to')->label; ?>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('where_moving_to')->id . ']', $htmlOptions = array('class' => 'g8')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">3)</th>
                <td>
                    How soon do you need to be there?
                    <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model' => $TransactionFieldValues,
                                'attribute' => 'data[' . $TransactionFields->getField('target_date')->id . ']',
                                'options' => array(
                                    'showAnim' => 'fold',
                                ),
                                'htmlOptions' => array(
                                    'style' => 'width: 120px;',
                                ),
                            )
                        );
                    ?>
                    <?php echo $form->error($TransactionFieldValues, 'data[' . $TransactionFields->getField('where_moving_to')->id . ']'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">4)</th>
                <td>
                    What'll happen if your house doesn't sell?
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('if_not_sell')->id . ']', $htmlOptions = array('class' => 'g7')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">5)</th>
                <td>
                    <?php echo $TransactionFields->getField('idea_of_sales')->label; ?>
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('idea_of_sales')->id . ']', array(
                            0 => 'No',
                            1 => 'Yes'
                        ), $htmlOptions = array(
                            'empty' => 'Select One'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">6)</th>
                <td>
                    <?php echo $TransactionFields->getField('idea_of_value')->label; ?>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('idea_of_value')->id . ']', $htmlOptions = array('class' => 'g5')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">7)</th>
                <td>
                    <?php echo $TransactionFields->getField('must_get_price')->label; ?>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('must_get_price')->id . ']', $htmlOptions = array('class' => 'g6')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">8)</th>
                <td>
                    <?php echo $TransactionFields->getField('mortgage_amount_1')->label; ?>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('mortgage_amount_1')->id . ']', $htmlOptions = array('class' => 'g3')); ?>
                    2nd Mtg / Home Equity Line (HELOC):
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('mortgage_amount_2')->id . ']', $htmlOptions = array('class' => 'g3')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">9)</th>
                <td>
                    <?php echo $TransactionFields->getField('payments_current')->label; ?>
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('payments_current')->id . ']', array(
                            0 => 'No',
                            1 => 'Yes'
                        ), $htmlOptions = array(
                            'empty' => 'Select One'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">10)</th>
                <td>
                    <?php echo $TransactionFields->getField('is_sole_owner')->label; ?>
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('is_sole_owner')->id . ']', StmFormHelper::getYesNoList(), $htmlOptions = array(
                            'empty' => 'Select One'
                        )
                    );
                    ?>
                    <label>Are there any other decision makers? </label>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('ownership_comment')->id . ']', $htmlOptions = array('class' => 'g3')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">11)</th>
                <td>
                    <label class="p-fl p-pr10">
                        Tell me a little bit about your house?<br/>
                                                        <span class="p-fl p-pt8" style="font-size:11px; font-style:italic">BR, Bath, Sq Feet, Yr Built, # Garage,<br/>
                                                        Pool-Screened, View (Woods, Water)<br/>
                                                        Upgrades/Features</span>
                    </label>

                    <div class="g8">
                        <?php echo $form->textArea($TransactionFieldValues, 'data[' . $TransactionFields->getField('features')->id . ']', $htmlOptions = array(
                                'class' => 'g12 break propertyFeatures bottomText',
                                'rows' => '4'
                            )
                        );
                        ?>
                    </div>

                </td>
            </tr>
            <tr>
                <th class="narrow">12)</th>
                <td>
                    Are you working with another agent?
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('other_agent')->id . ']', StmFormHelper::getYesNoList(), $htmlOptions = array(
                            'empty' => 'Select One'
                        )
                    );
                    ?>
                    Signed with them?
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('other_agent_signed')->id . ']', StmFormHelper::getYesNoList(), $htmlOptions = array(
                            'empty' => 'Select One'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">13)</th>
                <td>
                    <?php echo $TransactionFields->getField('agent_quality_1')->label; ?>
                    <div class="break">
                        <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('agent_quality_1')->id . ']', $htmlOptions = array('class' => 'g3')); ?>
                        <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('agent_quality_2')->id . ']', $htmlOptions = array('class' => 'g3')); ?>
                        <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('agent_quality_3')->id . ']', $htmlOptions = array('class' => 'g3')); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="narrow">14)</th>
                <td>
                    <?php echo $TransactionFields->getField('interview_others')->label; ?>
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('interview_others')->id . ']', StmFormHelper::getYesNoList(), $htmlOptions = array(
                            'empty' => ''
                        )
                    );
                    ?>
                    <?php echo $TransactionFields->getField('interview_last')->label; ?>
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data[' . $TransactionFields->getField('interview_last')->id . ']', StmFormHelper::getYesNoList(), $htmlOptions = array(
                            'empty' => ''
                        )
                    );
                    ?><br/>
                    <?php echo $TransactionFields->getField('interview_who')->label; ?>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('interview_who')->id . ']', $htmlOptions = array('class' => 'g6')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">15)</th>
                <td>
                    <?php echo $TransactionFields->getField('concerns')->label; ?>
                    <?php echo $form->textField($TransactionFieldValues, 'data[' . $TransactionFields->getField('concerns')->id . ']', $htmlOptions = array('class' => 'g5')); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">16)</th>
                <td>
                    <?php echo $TransactionFields->getField('trial_close')->label; ?>: If we show you how we are (name the qualities the want in agent), are you ready to hire us and put your home on the market today?
                    <?php echo $form->dropDownList($TransactionFieldValues, 'data['.$TransactionFields->getField('trial_close')->id.']', StmFormHelper::getYesNoList(),$htmlOptions=array('empty'=>'')); ?>
                </td>
            </tr>
        </table>
        </div>
    </div>
    <?php
        if($apiKey = ApiKeys::model()->findByAttributes(array('type'=>'google','contact_id'=>Yii::app()->user->id))):
            $this->renderPartial('_formCalendar', array('Appointment'=>$Appointment));
        endif;
    ?>

</div>
</div>
</div>
</div>
<!-- ====== END SECTIONS =============================================================================================== -->
<div id="submit-button-wrapper">
	<button id="submit-button" type="submit" class="submit wide"><?php echo ($model->isNewRecord) ? 'Submit' : 'Update'; ?> Seller</button>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php $this->endStmPortletContent(); ?>
<?php $this->widget(
    'admin_widgets.DialogWidget.AddressDialogWidget.AddressDialogWidget', array(
        'id'          => 'address-dialog-widget', 'title' => 'Add Address', 'triggerElement' => '#add-address-button',
        'parentModel' => $model, 'transaction' => $model, 'contactId' => $model->contact->id,
    )
);
?>