<style type="text/css">
    /* Datatable */
    #data-container {
        display: table;
        width: 100%;
    }
    #data-container>div {
        display: table-row;
    }

    #data-container>div>div {
        display: table-cell;
        border: 1px solid black;
        border-right: none;
        border-bottom: none;
        padding: 4px;
    }
    #data-container>div>div:last-of-type {
        border-right: 1px solid black;
    }
    #data-container>div:last-of-type>div {
        border-bottom: 1px solid black;
    }
    #data-container>div>div select {
        width: 100%;
    }
    .data-row-header {
        background-color: #DDD;
        font-size: 15px;
    }
    .data-column-header {
        font-size: 15px;
    }
    .fieldmap.blank {
        background-color: #DDD;
    }

    /* Form */
    #appointments-form-upload label {
        display: block;
        text-align: left;
    }
    #appointments-form-upload select,#appointments-form-upload input {
        width: 200px;
    }
    /*#appointments-form-upload .half{*/
        /*width: 295px;*/
    /*}*/
    #appointments-form-upload .form-group {
        margin-bottom: 10px;
    }
    #appointments-form-upload .form-group>div {
        display: inline-block
    }

    #data-upload-container {
        display: inline-block;
    }
    #mappings-container {
        float: right;
        text-align: left;
    }


</style>

<h1 style="margin: 50px 0px">Import</h1>

<section style='max-width: 1200px; margin: 25px auto;'>


    <div id="data-upload-container">

        <h3>Data Upload</h3>
        <h3>Make a selections below.</h3>
        <form action='/admin/importAppointments' method='POST' enctype='multipart/form-data' id='appointments-form-upload' style='width: 625px; margin: 0 auto;'>
            <div class="form-group">
                <input type='file' name='appointments-file' id='appointments-file' class='error' style='width: 400px;'>
                <input type='hidden' name='method' id='contacts-method' value='process-file'>
                <input type="hidden" name="type" id="type" value="appointments" />
            </div>
            <button type='submit' id='btn-upload'>Upload & Process File</button>
        </form>
    </div>

    <div style="clear: both;"></div>

    <div id="data-container"></div>


    <div style="text-align: center">
        <button type='button' id='btn-continue' style="display: none;">Continue and Import</button>
    </div>

    <input type="hidden" name="componentTypeId" id="componentTypeId" value="<?=ComponentTypes::CONTACTS?>" />
</section>


<script src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js"></script>

<?php
$js = <<<JS
 // Use YUI3 things
    YUI().use('node', 'io', 'json', 'handlebars', function(Y) {

        var loadingContainer = {
            show: function() {
                Y.one('body').appendChild(Y.Node.create('<div id="contacts-loading-container" class="loading-container loading"><em></em></div>'));
            },
            hide: function() {
                Y.all('.loading-container').remove();
            }
        };

        // Bind continue button
        Y.one('#btn-upload').on('click', function(e) {

            // Prevent default method
            e.preventDefault();
            // Make AJAX call to POST data
            Y.io('/admin/import/importAppointments', {
                method: 'POST',
                timeout: 360000,
                 form: {
                    id: 'appointments-form-upload',
                    upload: true
                },
                data: {
                    type: "appointments"
                },
                on: {
                    start: function() {
                        loadingContainer.show();
                    },
                    end: function() {
                        loadingContainer.hide();
                    },
                    complete: function(id, o) {
                        // Attempt to parse JSON response
                        try {
                            var responseData = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Unable to read response from server.');
                            return;
                        }

                        loadingContainer.hide();

                        // Handle different types of responses
                        switch(responseData.status) {
                            case 'success':
                                Message.create('success','Import Completed Successfully!');
                                $('#appointments-form-upload').reset();
                            break;
                            case 'error':
                                Message.create('error','Error: ' + responseData.message);
                            break;
                        }
                    },
                    failure: function() {
                        // Display message of failure
                        alert('An unknown error has occurred, please check data and try again.');
                    }
                }
            });
        });

    });
JS;

Yii::app()->clientScript->registerScript('importAppointmentsJs', $js, CClientScript::POS_END);