<?php
$this->breadcrumbs = array(
    'Upload' => array('/admin/import'),
);

$module = Yii::app()->controller->module->name;
$controller = Yii::app()->controller->id;
?>
    <style type="text/css">
        /* Datatable */
        #data-container {
            display: table;
            width: 100%;
        }
        #data-container>div {
            display: table-row;
        }

        #data-container>div>div {
            display: table-cell;
            border: 1px solid black;
            border-right: none;
            border-bottom: none;
            padding: 4px;
        }
        #data-container>div>div:last-of-type {
            border-right: 1px solid black;
        }
        #data-container>div:last-of-type>div {
            border-bottom: 1px solid black;
        }
        #data-container>div>div select {
            width: 100%;
        }
        .data-row-header {
            background-color: #DDD;
            font-size: 15px;
        }
        .data-column-header {
            font-size: 15px;
        }
        .fieldmap.blank {
            background-color: #DDD;
        }

        /* Form */
        #contacts-form-upload label {
            display: block;
            text-align: left;
        }
        #contacts-form-upload select,#contacts-form-upload input {
            width: 400px;
        }
        /*#contacts-form-upload .half{*/
        /*width: 295px;*/
        /*}*/
        #contacts-form-upload .form-group {
            margin-bottom: 10px;
        }
        #contacts-form-upload .form-group>div {
            display: inline-block
        }

        #data-upload-container {
            display: inline-block;
        }
        #mappings-container {
            float: right;
            text-align: left;
        }
        #successScheduled {
            max-width: 800px;
            padding: 30px;
            margin-left: auto;
            margin-right: auto;
        }
        #successScheduled h1 {
            margin-bottom: 0;
        }
        #successScheduled a {
            font-size: 20px;
            margin-top: 20px;
            display: block;
        }

        .warning {
            background-color: #ffede8;
            padding: 20px;
            text-align: center;
            font-weight: bold;
            font-size: 15px;
        }
        .warning h2 {
            margin-top: 15px;
        }
        .warning h2 a {
            font-size: 20px;
        }
    </style>
    <div id="listview-actions">
        <a href="/<?php echo Yii::app()->controller->module->name;?>/import/files" class="button gray icon i_stm_search">View Import Results</a>
    </div>

    <h1>Import Tool</h1>

    <div id="instructions-container" class="warning">
        **** WARNING: Visually verify the field maps every time as columns in your file may unexpectedly change. <br>Incorrect field mapping can cause major issues and it is very costly and/or time consuming to correct. Please be careful when importing. ****
        <h2>
            <a href="/admin/support/video/75">
                **** CLICK HERE for the IMPORT VIDEO TUTORIAL ****<br>
                Required training prior to using this tool
            </a>
            <br>
            <br>
            <a href="/admin/support/video/123">
                **** CLICK HERE for the NEW IMPORT VIDEO TUTORIAL ****<br>
            </a>
        </h2>
    </div>

    <div id="successScheduled" class="alert green" style="display: none;">
        <h1>Import Successfully Scheduled<br />
            <a href="/admin/import/files">NEXT STEP: View Import Results</a>
        </h1>
    </div>

    <section id="previewSection" style='max-width: 1200px; margin: 25px auto;'>


        <div id="data-upload-container">
            <h3>Data Upload</h3>
            <h3>Step 1: Enter a List Name and make the selections below.</h3>
            <form action='/<?=$module?>/<?=$controller?>/' method='POST' enctype='multipart/form-data' id='contacts-form-upload' style='width: 625px; margin: 0 auto;'>
                <div class="form-group">
                    <input type="text" id="importDescription" name="importDescription" style="width: 402px;" placeholder="Import Description" class="error"/>
                </div>
<!--                <h3 class="p-mt10 p-tl">Step 2: Select Options as needed.</h3>-->
                <div class="form-group hidden">
                    <input type="checkbox" id="importAsNotes" name="importAsNotes"  style="width: auto;" value="<?=ComponentTypes::IMPORT_NOTES;?>" />
                    <span>Import as notes?</span>
                </div>
<!--                <div class="form-group">-->
<!--                    <input type="checkbox" id="ignoreDupePhones" name="ignoreDupePhones"  style="width: auto;" value="1" />-->
<!--                    <span>Ignore duplicate Phones?</span>-->
<!--                </div>-->
<!--                <div class="form-group">-->
<!--                    <input type="checkbox" id="ignoreDupeAddress" name="ignoreDupeAddress"  style="width: auto;" value="1" />-->
<!--                    <span>Ignore duplicate Address?</span>-->
<!--                </div>-->
                <!--            <div class="form-group">-->
                <!--                <div>-->
                <!--                    <select id="isNewList" name="isNewList" class='error'>-->
                <!--                        <option value="">*** Select One ***</option>-->
                <!--                        <option value="1">Add to Existing List</option>-->
                <!--                        <option value="2">Create a New List</option>-->
                <!--                    </select>-->
                <!--                    <select id="callListId" name="callListId" class='error hidden' disabled="disabled">-->
                <!--                        <option value="">*** Select One ***</option>-->
                <!--                        --><?//foreach($callLists as $callList) : ?>
                <!--                        <option value="--><?//=$callList->id?><!--" data-psid="--><?//=$callList->preset_ma?><!--" data-ma="--><?//=$componentMapper[$callList->preset_ma]?><!--">--><?//=$callList->name?><!--</option>-->
                <!--                        --><?//endforeach;?>
                <!--                    </select>-->
                <!--                    <input type="text" id="listName" placeholder="List Name" class='error hidden' disabled="disabled">-->
                <!--                </div>-->
                <!--            </div>-->
                <div class="form-group">
                    <div>
                        <label>Import Type</label>
                        <select id="componentTypeId" name="componentTypeId" class='error'>
                            <option value="">*** Select One ***</option>
                            <option value="<?=ComponentTypes::CONTACTS?>">Contacts</option>
                            <?if($module != 'hq'):?>
                                <?if(Yii::app()->user->checkAccess('transactions')):?>
                                    <option value="<?=ComponentTypes::BUYERS?>">Buyers</option>
                                    <option value="<?=ComponentTypes::SELLERS?>">Sellers</option>
<!--                                    <option value="--><?//=ComponentTypes::CLOSINGS?><!--">Closings</option>-->
                                <?endif;?>
                                <option value="<?=ComponentTypes::RECRUITS?>">Recruits</option>
                            <?endif;?>
                        </select>
                    </div>
                    <!--                <div>-->
                    <!--                    <label>Source</label>-->
                    <!--                    <select id="sourceId" name="sourceId" class='error'>-->
                    <!--                        <option value="">*** Select One ***</option>-->
                    <!--                        --><?//foreach($sources as $source) : ?>
                    <!--                        <option value="--><?//=$source->id?><!--">--><?//=$source->name?><!--</option>-->
                    <!--                        --><?//endforeach;?>
                    <!--                    </select>-->
                    <!--                </div>-->
                </div>
<!--                <div class="form-group">-->
<!--                    <input type="text" id="importDescription" name="importDescription" style="width: 402px;" placeholder="Import Description" class="error"/>-->
<!--                </div>-->
                <!--            <div class="form-group">-->
                <!--                <div>-->
                <!--                    <label>Header Row</label>-->
                <!--                    <select id="ignoreFirstRow" name="ignoreFirstRow" class='error'>-->
                <!--                        <option value="">*** Select One ***</option>-->
                <!--                        <option value="1">Ignore First Header Row</option>-->
                <!--                        <option value="0">Include First Row</option>-->
                <!--                    </select>-->
                <input type='hidden' name='ignoreFirstRow' id='ignoreFirstRow' value='0'>
                <!--                </div>-->
                <!--            </div>-->
                <div>
                    <label>Select File</label>
                    <input type='file' name='contacts-file' id='contacts-file' class='error' style='width: 400px;'>
                    <input type='hidden' name='method' id='contacts-method' value='process-file'>
                </div>
                <div>
                    <button type='submit' id='contacts-btn-upload'>Preview File Data</button>
                </div>
            </form>
        </div>

        <div id="mappings-container" style="display: none;">

            <h3>Load Field Maps Template</h3>
            <form id="form-load-template">
                <select name="mapping" style="width: 100%;">
                    <?foreach($templates as $templateName => $templateMappings) : ?>
                        <option value="<?=$templateName?>" data-template='<?=$templateMappings?>'><?=$templateName?></option>
                    <?endforeach;?>
                </select>
                <br>
                <button type="submit">Load Field Maps</button>
                <button type="button" id="update-field-maps">Update Field Maps</button>
            </form>

            <h3 style="margin-top: 20px;">Save Field Maps as New Template</h3>
            <form method="POST" action="/<?=$module?>/<?=$controller?>/savetemplate" id="form-save-template">
                <label>List Name</label>
                <input type="text" placeholder="Field Map Template Name" name="name">
                <button type="submit">Save New Field Maps</button>
            </form>
        </div>
        <div style="clear: both;"></div>

        <div id="field-map-header" style="margin: 50px; display: none;">
            <h3 style="font-size: 20px;">Field Mapping Instructions:</h3>
            <h4>
                1) If you have a saved Field Map Template, select from top right and click load.<br>
                2) Select the Field Maps below from the drop downs in the first column.<br>
                3) Select the correct Component Type and Source.
            </h4>
        </div>
        <div id="data-container"></div>


        <div style="text-align: center">
            <button type='button' id='btn-continue' style="display: none;">Schedule Import</button>
        </div>
        <input type="hidden" id="mappings" value='<?=CJSON::encode($mappings);?>'>
    </section>

    <!-- Handlebars templates -->
    <script id="data-row-template" type="text/x-handlebars-template">
        <div>
            <div class="data-row-header" style="width: 120px;">
                {{#unless skipSelect}}
                {{{fieldsSelect}}}
                {{else}}
                <b>Field Maps</b>
                {{/unless}}
            </div>
            {{#each columns}}
            <div>
                {{{.}}}
            </div>
            {{/each}}
        </div>
    </script>

    <script id="data-row-fields-select" type="text/x-handlebars-template">
        <select class="fieldmap blank">
            <option value="-">- Omit -</option>
            {{#each fields}}
            <option value="{{@key}}">{{this}}</option>
            {{/each}}
        </select>
    </script>

    <script src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js"></script>

<?php
$js = <<<JSS
    // Use YUI3 things
    YUI().use('node', 'io', 'json', 'handlebars', 'anim', function(Y) {

        var import_id;

        // Form change background updates
        //@todo: Use YUI3 instead of jQuery
        $('#listName, #ignoreFirstRow, #componentTypeId, #sourceId, #contacts-file, #isNewList, #listName, #importDescription').live('change', function(){
            if($(this).val() != '') {
                $(this).removeClass('error');
            }
            else {
                $(this).addClass('error');
            }

            if($(this).attr('id') == 'ignoreFirstRow') {
                if($('#data-container').html() !== '') {
                    $('#data-container').html('');
                    $('#contacts-file').val('');
                    $('#field-map-header, #btn-continue').hide();
                }
            }
        });

        //@todo: Use YUI3 instead of jQuery
        $('.fieldmap').live('change', function(){
            if($(this).val() != '-') {
                $(this).removeClass('blank');
            }
            else {
                $(this).addClass('blank');
            }
        });

//        //@todo: Use YUI3 instead of jQuery
//        $('#isNewList').live('change', function(){
//
//            // if creating a new list
//            if($(this).val() == '2') {
//                $('#listName').removeClass('hidden');
//                $('#callListId').addClass('hidden');
//                $('#listName').attr('disabled',false);
//                $('#callListId').attr('disabled','disabled');
//                $('#componentTypeId').attr('disabled',false);
//                $('#componentTypeId').addClass('error');
//                $('#componentTypeId').val('');
//            }
//            // if adding to existing list
//            else {
//                $('#listName').addClass('hidden');
//                $('#callListId').removeClass('hidden');
//                $('#listName').attr('disabled','disabled');
//                $('#callListId').attr('disabled',false);
//                $('#componentTypeId').attr('disabled','disabled');
//            }
//        });

        // Set the component type id when we select a pre-existing list
//        Y.one('#callListId').on('change', function() {
//            var option = Y.one('#callListId option[value="' + Y.one('#callListId').get('value') + '"]');
//            Y.one('#componentTypeId').set('value', option.getData('ma'));
//            Y.one('#componentTypeId').removeClass('error');
//        });

        // Retrieve mappings
        var mappings = Y.JSON.parse(Y.one('#mappings').get('value'));

        // Data that will be imported
        var rows;

        // Loading container functionality
        var loadingContainer = {
            show: function() {
                Y.one('body').appendChild(Y.Node.create('<div id="contacts-loading-container" class="loading-container loading"><em></em></div>'));
            },
            hide: function() {
                Y.all('.loading-container').remove();
            }
        };

        // Helper function to get mappings
        var getMappings = function() {

            // Build mappings from select options
            var eles = Y.all('#data-container select'), selectedMappings = [];
            for(var i = 0; i < eles.size(); i++) {

                // Check for duplicate field mappings
                if((selectedMappings.indexOf(eles.item(i).get('value')) !== -1) && (eles.item(i).get('value') !== '-')) {
                    alert('A duplicate field mapping has been detected, please make sure there are no duplicates before continuing.');
                    return false;
                }

                // Catalog field mapping
                selectedMappings.push(eles.item(i).get('value'));
            }

            // Check to see if we omitted everything
            var indices = [];
            var idx = selectedMappings.indexOf('-');
            while(idx != -1) {
                indices.push(idx);
                idx = selectedMappings.indexOf('-', idx + 1);
            }

            // Make sure we have mappings
            if(indices.length == selectedMappings.length) {
                alert('Basic field maps such as First Name, Last Name, Phone 1 is required to save.');
                return false;
            }

            // check to see if first name, last name, phone1 or any address is mapped
            var firstNameExists = false;
            var lastNameExists = false;
            var fullNameExists = false;
            var sourceExists = false;
            var phone1Exists = false;
            var email1Exists = false;
            var addressExists = false;
            var cityExists = false;
            var stateExists = false;
            var zipExists = false;

            for(i = 0; i < eles.size(); i++) {

                if(eles.item(i).get('value') == 'first_name') {
                    firstNameExists = true;
                }
                if(eles.item(i).get('value') == 'last_name') {
                    lastNameExists = true;
                }
                if(eles.item(i).get('value') == 'phone_1') {
                    phone1Exists = true;
                }
                if(eles.item(i).get('value') == 'email_1') {
                    email1Exists = true;
                }
                if(eles.item(i).get('value') == 'full_name') {
                    fullNameExists = true;
                }
                if(eles.item(i).get('value') == 'source') {
                    sourceExists = true;
                }

                if(eles.item(i).get('value') == 'address') {
                    addressExists = true;
                }
                if(eles.item(i).get('value') == 'city') {
                    cityExists = true;
                }
                if(eles.item(i).get('value') == 'state') {
                    stateExists = true;
                }
                if(eles.item(i).get('value') == 'zip') {
                    zipExists = true;
                }
            }

            // check to see if first name is mapped
            if(firstNameExists !== true && fullNameExists !== true) {
                alert('First Name OR full name must be field mapped.');
                return false;
            }

            // check to see if last name is mapped
            if(lastNameExists !== true && fullNameExists !== true) {
                alert('Last Name OR full name must be field mapped.');
                return false;
            }

            // check to see if Phone 1 & Email 1 are both missing
            if(phone1Exists !== true && email1Exists !== true) {
                alert('Phone 1 or Email 1 must be field mapped.');
                return false;
            }

            // check to see if source is mapped
            if(sourceExists !== true) {
                alert('Source must be field mapped. If you don\'t have a column in your file for source, please create one and populate it with the source name. This will ensure you can track your business properly.');
                return false;
            }

            // check to see if all address fields mapped if one is mapped
            if((addressExists || cityExists || stateExists || zipExists) && !(addressExists && cityExists && stateExists && zipExists)) {

                alert('All address fields such as Address, City, State, Zip must be mapped to import addresses. Any missing values address columns in the import will cause the address not to save.');
                return false;
            }

            // Return mappings
            return selectedMappings;
        };

        // Preview File Data
        Y.one('#contacts-form-upload').on('submit', function(e) {

            // Prevent default method
            e.preventDefault();

            if(!Y.one('#importDescription').get('value')) {
                alert('Please enter an Import Description.');
                return;
            }

            // Check things for new vs existing lists
//            switch(Y.one('#isNewList').get('value')) {
//                case '1':
//
//                    // Make sure we chose an existing list
//                    if(!Y.one('#callListId').get('value')) {
//                        alert('You must select an existing call list to continue.');
//                        return;
//                    }
//
//                    break;
//                case '2':
//
//                    // Make sure list name isn't empty
//                    if(Y.one('#listName').get('value') == '') {
//                        alert('Please enter a list name.');
//                        return;
//                    }
//
//                    break;
//
//                default:
//                    alert('Please select a list option');
//                    return;
//                    break;
//            }

            // AJAX call to process data
            Y.io('/$module/$controller/previewfile', {
                method: 'POST',
                form: {
                    id: 'contacts-form-upload',
                    upload: true
                },
                on: {
                    start: function() {
                        loadingContainer.show();
                        Y.one('#data-container').setHTML('');
                    },
                    end: function() {
                        loadingContainer.hide();
                    },
                    complete: function(id, o) {

                        // Attempt to parse out JSON
                        try {
                            var response = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('An unknown error has occurred, please try again later.');
                            return;
                        }

                        // Handle different response types
                        switch(response.status) {

                            // Do this if we had a proper return
                            case 'success':

                                // Grab first 4 rows of data
                                rows = response.data;
                                var row, i, j, results = [];
                                var limit = rows.length < 4 ? rows.length : 4;

                                // no row of data exists
                                if(limit == 0) {
                                    loadingContainer.hide();
                                    Message.create('warning','No data was found. Please try again.');
                                    return;
                                }

                                for(i = 0; i < limit; i++) {

                                    // Check to make sure this index exists
                                    if(rows.indexOf(i) !== -1) {
                                        break;
                                    }

                                    // Iterate each column for the row
                                    row = rows[i];
                                    for(j = 0; j < row.length; j++) {

                                        // Make sure we have a sub-array to handle this
                                        if(!results[j]) {
                                            results[j] = [];
                                        }

                                        // Catalog the current piece of data
                                        results[j].push(row[j]);
                                    }
                                }

                                // Generate DOM elements from template
                                var eleRow, eleFieldsSelect;

                                // Figure out how many columns we are making
                                var columns = [];
                                for(i = 1; i <= results[0].length; i++) {
                                    columns.push('<b class="data-column-header">Sample Row ' + i + '</b>');
                                }

                                eleRow = Y.Node.create(Y.Handlebars.render(Y.one('#data-row-template').getHTML(), {
                                    skipSelect: true,
                                    columns: columns
                                }));

                                // Append the resulting node to the container
                                Y.one('#data-container').appendChild(eleRow);

                                // Iterate results and add rows to view
                                var  _fields = mappings[Y.one('#componentTypeId').get('value') ? Y.one('#componentTypeId').get('value') : 1];
                                if(Y.one("#importAsNotes").get("checked")) {
                                    _fields = mappings[Y.one("#importAsNotes").get('value')];
                                }

                                for(i = 0; i < results.length; i++) {

                                    // Generate select box data
                                    eleFieldsSelect = Y.Handlebars.render(Y.one('#data-row-fields-select').getHTML(), {
                                        fields: _fields
                                    });

                                    // Generate DOM elements from template
                                    eleRow = Y.Node.create(Y.Handlebars.render(Y.one('#data-row-template').getHTML(), {
                                        fieldsSelect: eleFieldsSelect,
                                        columns: results[i]
                                    }));

                                    // Append the resulting node to the container
                                    Y.one('#data-container').appendChild(eleRow);
                                }

                                // Show mappings
//                                Y.one('#data-upload-container').setStyle('display', 'inline-block');
                                Y.one('#mappings-container').show();
                                Y.one('#btn-continue').show();
                                Y.one('#field-map-header').show();
//                                Y.one('#mappings-container').setStyle('display', 'block');
                                import_id = response.import_id;
                            break;

                            case 'error':
                                alert(response.message);
                                return;
                            break;

                            default:
                                alert('An unknown error has occurred');
                            break;
                        }
                    }
                }
            });
        });

        // Bind continue button
        Y.one('#btn-continue').on('click', function(e) {

            // Prevent default method
            e.preventDefault();

            // Check things for new vs existing lists
//            switch(Y.one('#isNewList').get('value')) {
//                case '1':
//
//                    // Make sure we chose an existing list
//                    if(!Y.one('#callListId').get('value')) {
//                        alert('You must select an existing call list to continue.');
//                        return;
//                    }
//
//                break;
//                case '2':
//
//                    // Make sure list name isn't empty
//                    if(Y.one('#listName').get('value') == '') {
//                        alert('Please enter a list name.');
//                        return;
//                    }
//
//                break;
//                default:
//                    alert('Please select a list option');
//                    return;
//                break;
//            }

            // Make sure we have data to POST
            if(rows == undefined) {
                alert('Please select file, preview and map the columns to a field.');
                return;
            }
            if(!rows.length) {
                return;
            }

            // Run helper function to get mappings
            var selectedMappings = getMappings();
            if(selectedMappings === false) {
                return;
            }

            // Make AJAX call to POST data
            Y.io('/$module/$controller/import', {
                method: 'POST',
                timeout: 360000,
                data: {
                    rows: Y.JSON.stringify(rows),
                    mappings: Y.JSON.stringify(selectedMappings),
                    componentTypeId: Y.one('#componentTypeId').get('value'),
//                    listName: Y.one('#listName').get('value'),
//                    sourceId: Y.one('#sourceId').get('value'),
                    importDescription: Y.one('#importDescription').get('value'),
//                    listId: Y.one('#isNewList').get('value') === '1' ? Y.one('#callListId').get('value') : ''
                    'import_id': import_id
                },
                on: {
                    start: function() {
                        loadingContainer.show();
                    },
                    end: function() {
                        loadingContainer.hide();
                    },
                    success: function(id, o) {

                        // Attempt to parse JSON response
                        try {
                            var responseData = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Unable to read response from server.');
                            return;
                        }

                        loadingContainer.hide();

                        // Handle different types of responses
                        switch(responseData.status) {
                            case 'success':
                                Message.create('success','Import Scheduled Successfully!');
                                Y.one('#field-map-header').hide();
                                Y.one('#btn-continue').hide();
                                Y.one('#data-container').setHTML('');
                                Y.one('#instructions-container').hide();
                                Y.one("#previewSection").hide();
                                Y.one('#successScheduled').show();
                            break;
                            case 'error':
                                alert('An error occurred: ' + responseData.message);
                            break;
                        }
                    },
                    failure: function() {
                        // Display message of failure
                        alert('An unknown error has occurred, please check data and try again.');
                    }
                }
            });
        });

        // Hande template saving
        Y.one('#form-save-template').on('submit', function(e) {

            // Stop default event
            e.preventDefault();

            // Make sure we have name entry
            if(!e.currentTarget.one('input[name="name"]').get('value')) {
                alert('Field Map Template Name is required.');
                return;
            }

            // Run helper function to get mappings
            var selectedMappings = getMappings();
            if(selectedMappings === false) {
                return;
            }

            // Make AJAX call to POST data
            Y.io('/$module/$controller/savetemplate', {
                method: 'POST',
                timeout: 360000,
                data: {
                    name: e.currentTarget.one('input[name="name"]').get('value'),
                    mappings: Y.JSON.stringify(selectedMappings)
                },
                on: {
                    start: function() {
                        loadingContainer.show();
                    },
                    end: function() {
                        loadingContainer.hide();
                    },
                    success: function(id, o) {

                        // Attempt to parse JSON response
                        try {
                            var responseData = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Unable to read response from server.');
                            return;
                        }

                        // Handle different types of responses
                        switch(responseData.status) {
                            case 'success':

                                alert('Mappings saved successfully!');

                                // Remove pre-existing mappings from select box
                                var ele = Y.one('#form-load-template select[name="mapping"]');
                                if(ele.children) {
                                    ele.children.remove();
                                }

                                ele.appendChild(Y.Node.create("<option value='" + responseData.results.name + "' data-template='" + responseData.results.mappings + "'>" + responseData.results.name + "</option>"));

                                // Select the current item we just made
                                ele.set('value', responseData.name);

                                // Clear value for name
                                e.currentTarget.one('input[name="name"]').set('value', '');

                                break;
                            case 'error':
                                alert('An error occurred with saving a new Field Map Template. ' + responseData.errors);
                                break;
                        }
                    },
                    failure: function() {
                        // Display message of failure
                        alert('An unknown error has occurred, please check data and try again.');
                    }
                }
            });
        });

        // Bind load template event
        Y.one('#form-load-template').on('submit', function(e) {

            // Prevent default event facade
            e.preventDefault();

            loadingContainer.show();

            // Get template data
            var ele = Y.one('#form-load-template select[name="mapping"]');
            var template = ele.one('option[value="' + ele.get('value') + '"]').getData('template');

            // Decode template data
            try {
                var mappings = Y.JSON.parse(template);
            }
            catch(e) {
                Message.create('error','Error: template data corrupt.');
                return;
            }

            // Iterate over each and apply mapping
            Y.all('#data-container select').each(function(ele, i) {
                if(mappings[i]) {
                    ele.set('value', mappings[i]);
                    if(mappings[i] == '-') {
                        ele.addClass('blank');
                    }
                    else {
                        ele.removeClass('blank');
                    }
                }
            });
            Message.create('success','Successfully loaded Field Maps from template.');
            setTimeout(function(){loadingContainer.hide()}, 1000);
        });

        // Bind update field maps event
        Y.one('#update-field-maps').on('click', function(e) {

            // Prevent default event facade
            e.preventDefault();

            // Run helper function to get mappings
            var selectedMappings = getMappings();
            if(selectedMappings === false) {
                return;
            }

            // Make AJAX call to POST data
            Y.io('/$module/$controller/updatetemplate', {
                method: 'POST',
                timeout: 360000,
                data: {
                    template_id: Y.one('#form-load-template select[name="mapping"]').get('value'),
                    mappings: Y.JSON.stringify(selectedMappings)
                },
                on: {
                    start: function() {
                        loadingContainer.show();
                    },
                    end: function() {
                        loadingContainer.hide();
                    },
                    success: function(id, o) {

                        // Attempt to parse JSON response
                        try {
                            var responseData = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Unable to read response from server.');
                            return;
                        }

                        // Handle different types of responses
                        switch(responseData.status) {
                            case 'success':
                                alert('Mappings updated successfully!');

                                // Remove pre-existing mappings from select box, rebuild from scratch
                                var ele = Y.one('#form-load-template select[name="mapping"]');
//                                var ele = Y.one('#form-load-template select[name="mapping"] option[value="' + responseData.templateName + '"]');
                                ele.setHTML('');

                                // Update select box with mapping data
                                Y.Array.each(responseData.results, function(result) {
                                    ele.appendChild(Y.Node.create("<option value='" + result.name + "' data-template='" + result.mappings + "'>" + result.name + "</option>"));
                                });

                                // Select the current item we just made
                                ele.set('value', responseData.templateName);
//                                ele.setAttribute('data-template', responseData.mappings);

                                break;
                            case 'error':
                                alert('An error occurred with saving a new Field Map Template. ' + responseData.errors);
                                break;
                        }
                    },
                    failure: function() {
                        // Display message of failure
                        alert('An unknown error has occurred, please check data and try again.');
                    }
                }
            });
        })
    });

JSS;

Yii::app()->clientScript->registerScript('importViewJS', $js, CClientScript::POS_END);
