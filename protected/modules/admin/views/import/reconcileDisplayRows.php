<div class='loading-container loading'><em></em></div>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/u/dt/dt-1.10.12,fc-3.2.2,fh-3.1.2,r-2.1.0,rr-1.1.2,sc-1.4.2/datatables.min.css"/>

<style>
    #CsvImportFileRowsContainer {
        min-height: 200px;
        overflow-y: auto;
        overflow: hidden;
    }
    #CsvImportFileRowsTable thead tr th {
        /*background-color: #ccc;*/
        width: 100%;
        color: #000;
        text-shadow: none;
        text-align: center;
    }
    .csvimportFileRow {
        cursor: pointer;
    }

    .csvimportFileRow td {
        text-align: center;
        border-top: 0;
    }
     #phone-status-form {
         padding-top: 20px;
     }
    #phone-status-form .phone-action-button {
        padding: 15px;
        width: 70%;
        font-size: 20px;
        font-weight: bold;
        display: inline-block;
        -webkit-border-radius:6px;
        -moz-border-radius:6px;
        border-radius:6px;
        margin-bottom: 10px;
    }

    #phone-status-form .deleteBadNumber-button {
        background-color: #ffeaea;
        border: 2px solid #F9DCDB;
    }
    #phone-status-form .deleteBadNumber-button:hover {
        border-color: #d23f4f;
        text-decoration: none;
    }

    #phone-status-form .click-to-call-button {
        background-color: #e3ffd4;
        border: 2px solid #d6efc7;
    }
    #phone-status-form .click-to-call-button:hover {
        border-color: #73d273;
        text-decoration: none;
    }
    #phone-status-form #phone-action-do-not-call-container {
        background-color: #DFEBFF;
        border: 2px solid #C7DAEF;
    }
    #phone-status-form #phone-action-do-not-call-container select {
        font-size: 15px;
    }

    .selected-match {
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 7px #5cb85c;
        box-shadow: 0 1px 7px #5cb85c;
    }

    div.alert-danger {
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
    }
    div.alert {
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
    }

    tr.odd td.sorting_1, tr.even td.sorting_1 {
        color: #000;
        background: #fff;
    }
    tr.even td.sorting_1 {
        background: none #F5F5F5;
    }
    #red tr.odd, #red tr.odd th, #red tr.odd-static, #red tr.odd-static th, #red .widget.number-widget > div {
        background: none #fff;
    }
    #red .pager .page.selected a, #red .pager ul li a:hover, #red button.text:hover, #red a.text-button:hover, #red tr:hover.odd, #red tr:hover.even, #red tr:hover.odd th, #red tr:hover.even th, #red tr:hover.odd a:not(.button), #red tr:hover.even a:not(.button), #red tr:hover.odd a:not(.button):visited, #red tr:hover.even a:not(.button):visited, #red .widget.number-widget ul li a:hover, #red .ui-tabs-active {
        background: none #fff;
        text-shadow: none;
        color: #000;
    }

    #red tbody.results tr.trHover {
        background: none #efefef;
    }
    .selectAllCb, .unselectAllCb {
        display: block;
    }

    .DTFC_ScrollWrapper {
        height: calc(100vh - 325px) !important;
    }
    .dataTables_scrollBody {
        max-height: calc(100vh - 425px) !important;
    }
    .dataTables_paginate.paging_simple_numbers {
        width: inherit;
    }
    .dataTables_scrollBody {
        border-bottom: 1px solid #CCC !important;
        border-top: 1px solid #CCC !important;
    }
    table.dataTable thead th {
        border-bottom: 0;
    }
    .DTFC_LeftBodyWrapper {
        border-top: 1px solid #CCC;
        border-bottom: 1px solid #CCC;
    }
    .DTFC_LeftBodyWrapper td {
        border-right: 0;
        border-left: 0;
    }
    .DTFC_LeftHeadWrapper th{
        border-left: 0;
        border-right: 0;
    }

    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody td {
        border-right: 0;
        border-left: 0;
    }
    .dataTables_scrollHead th {
        border-right: 0;
    }
    #rowCollisionsInfo th, #rowCollisionsInfo td {
        border-right: 0;
        border-left: 0;
    }
    #rowCollisionsInfo tr:not(:last-child) td {
        border-bottom: 0;
    }
    table#rowCollisionsInfo.dataTable.no-footer {
        border-bottom: 0;
    }
    #rowCollisionsInfo thead tr, #rowCollisionsInfo tr.even td {
        background: #F5F5F5;
    }
    #selectedImportRowInfo, #selectedImportRowInfo td {
        text-align: center;
    }
</style>

<?php
$rowsReconciled = 0;
$reconciledRows = array();
?>
<h1>Import Reconcile</h1>
<div style="background-color: #ffede8; padding: 20px 0; color: #333;">
    <h3>
        <a class="instructionsButton" href="javascript:void(0)" style="font-size: 20px;">**** IMPORTANT: CLICK HERE for Instructions ****</a>
    </h3>
    <h3 class="instructionsContainer" style="max-width: 800px; text-align: left; margin-right: auto; margin-left: auto; display: none; color: #333;">
        <br>INSTRUCTIONS: Review the duplicates detected from your import file and choose an action to take.
        <br><br>Step 1: Select an action for each row with a Red "?" in the Status column by clicking on the row.
        <br><br>Step 2: A pop-up will appear when you click on a row and you can review the import data as well as the detected duplicates.
        <br><br>Step 3: Click an action button to A) Ignore/No Action B) Create New Record or C) Merge with an Existing Record by clicking in the table of potential matches.
        <br><br>Step 4: You can also take Mass Action by checking multiple the rows and clicking one of the Mass Action buttons above the table to the left. This is not recommended unless you are 100% positive about your data. WARNING: Once the action is taken, reversing can be very time consuming and costly. Please be very careful with Mass Action!
        <br><br>Step 5: When all rows have an action a green button will appear to Complete Reconciliation. This will perform the actions you have selected.
        <br><br><a href="/admin/support/video/123">Click here for Video Tutorial</a>
    </h3>
</div>

<div style="margin-top: 20px;">
    <div class="p-tl" style="font-size: 14px;">Mass Action for detected duplicate rows below: </div>
    <button type="button" name="ignoreAllCB" id="ignoreAllCB" class="btn btn-primary button default" style=" margin: 0 auto; display: inline-block; margin-bottom: 10px;" data-reconcile-id="ALL">Ignore / No Action</button>
    <button type="button" name="markAsNewCB" id="markAsNewCB" class="btn btn-primary button default" style=" margin: 0 auto; display: inline-block; margin-bottom: 10px;" data-reconcile-id="ALL">Create New Record</button>
</div>
<div id="scheduleReconcileContainer" class="p-tc">
    <button type="button" name="scheduleReconcile" id="scheduleReconcile" class="btn btn-lg hidden" style="width: 100%; margin: 0 auto; display: block; background-color: #5cb85c; border: none; margin-bottom: 8px; font-size: 18px; max-width: 500px;" data-reconcile-file-id="<?=$reconcile_file_id;?>">NEXT STEP: Complete Reconciliation</button>
</div>

<div class="g12">
    <div id="CsvImportFileRowsContainer">
        <table id="CsvImportFileRowsTable">
            <thead>
            <?php
            $firstRow = json_decode($ImportReconcileData[0]->data, true);
                $colCount = count($firstRow);

            $subHeaders = '';

            array_walk($firstRow, function($item, $key) use (&$subHeaders) {
                $subHeaders .= '<th>'.$key.'</th>';
            });
            ?>
                <tr>
                    <th rowspan="2" class="no-sort selectAllCbClicker"><span class="selectAllCb">Select All</span><span class="unselectAllCb hidden">Unselect All</span></th>
                    <th rowspan="2">Reconcile Status<div class="stm-tool-tip question"><span>Click the rows with the Red "?" to choose a Reconcile Action.</span></div></th>
                    <th rowspan="2">Reconcile Action<div class="stm-tool-tip question"><span>Reconcile Action is how you choose to handle the duplicates found. You can choose to Ignore/No Action, Merge into existing contact or Create a New Contact. Click on the row you wish to make a reconcile for the duplicates found.</span></div></th>
                    <th rowspan="2">Potential Duplicates<div class="stm-tool-tip question"><span>Number of potential duplicate contacts found for review.</span></div></th>
                    <th colspan="<?=$colCount;?>">Import Data<div class="stm-tool-tip question" style="display: inline-block;"><span>Below is the data from your import file to help you determine whether the record is a duplicate or not. Click on the row you wish to reveiw to choose your Reconcile Action.</span></div></th>
                </tr>
                <tr>
                    <?= $subHeaders; ?>
                </tr>
            </thead>
            <tbody class="results">
                <?php foreach($ImportReconcileData as $reconcileLine): ?>
                <?php
                    $collisionsByType = json_decode($reconcileLine->collisions, true);

                    $matches = 0;
                    foreach($collisionsByType as $type => $collisions) {
                        $matches += count($collisions);
                    }

                    $csvRowData = json_decode($reconcileLine->data, true);
                    $cols = count($csvRowData);

                    $itemsHTML = '';
                    array_walk($csvRowData, function($item, $key) use (&$itemsHTML) {
                        $itemsHTML .= "<td><div style='height: 100px; display: block; overflow-y: auto;'>".$item."</div></td>";
                    });
                    ?>
                <tr class="csvimportFileRow displayRow-<?=$reconcileLine->id; ?>" data-reconcile-data-id="<?=$reconcileLine->id; ?>" data-row-action="<?=$reconcileLine->action; ?>">
                    <td class="no-sort"><input type="checkbox" name="applyToAll[<?=$reconcileLine->id;?>]" id="applyToAll-<?=$reconcileLine->id;?>" class="applyToAll" value="<?=$reconcileLine->id;?>" /></td>
                    <td class="passFail">
                        <?php if($reconcileLine->action == 'Not Selected'): ?>
                            <i class="fa fa-2x fa-question-circle" style="color: #C50000;"></i>
                        <?php else:
                            $rowsReconciled++;
                            $reconciledRows[] = $reconcileLine->id;
                        ?>
                            <i class="fa fa-2x fa-check-circle" style="color: #5cb85c;"></i>
                        <?php endif; ?>
                    </td>

                    <?php

                    $bgcolor = "inherit";
                    $color = "";
                    if($reconcileLine->action == "New") {
                        $bgcolor = "#5cb85c";
                        $color = "color: #FFF;";
                    }

                    if($reconcileLine->action == "Merge") {
                        $bgcolor = "#FFFAED";
                    }

                    if($reconcileLine->action == "Ignore") {
                        $bgcolor = "#FBE9E6";
                    }

                    ?>

                    <td class="reconcoleLine-action-<?=$reconcileLine->id; ?> apply" style="background-color: <?=$bgcolor;?>;<?=$color;?>"><?=($reconcileLine->action == 'Not Selected') ? 'Click to Reconcile' : $reconcileLine->action; ?></td>
                    <td><?=$matches; ?></td>
                    <?=$itemsHTML; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
</div>

<script id="template-collision-modal" type="text/x-handlebars-template">
    <div id="collisionRowsContainer" class="stm-modal-body">
        <div style="margin-bottom: 20px;">
            INSTRUCTIONS: Choose an action by clicking on one of the blue Action Buttons below: <br>
            A) Ignore/No Action B) Create New Record C) Merge (button located in the table below)
        </div>
        <div id="rowIgnored" class="alert alert-danger hidden">Current Selected Row is Ignored. Click a match below or "Create New Record" to update the selected row.</div>

        <button type="button" name="ignoreAll" id="ignoreAll" class="btn btn-lg hidden" style="width: 49.5%; margin: 0 auto; display: inline-block; margin-bottom: 20px;" data-reconcile-id="">Ignore/No Action</button>
        <button type="button" name="markAsNew" id="markAsNew" class="btn btn-lg hidden" style="width: 49.5%; margin: 0 auto; display: inline-block; margin-bottom: 20px;" data-reconcile-id="">Create New Record</button>

        <h3>Import Row</h3>
        <table style="margin-bottom: 20px; position: relative; width: 100%; display: block; overflow-x: scroll;">
            <tr id="selectedImportRowInfo"></tr>
        </table>

        <h3>Matches</h3>
        <table id="rowCollisionsInfo"></table>
    </div>
</script>
<?php

$reconcileRowsCount = count($ImportReconcileData);

$js = <<<JS

var totalRowsToReconcile = {$reconcileRowsCount};
var reconciledRows = [];
var view_only = {$view_only};
var file_id = {$reconcile_file_id};
JS;


foreach($reconciledRows as $reconcileRowID) {
    $js .= "
    reconciledRows.push(".$reconcileRowID.");";
}

$js .= <<<JS
$('.instructionsButton').click(function(){
    $('.instructionsContainer').toggle('normal');
});

var table = $('#CsvImportFileRowsTable').DataTable( {
    scrollY:        "300px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         false,
    fixedColumns: {
        leftColumns: 3,
        topColumns: 1
    },
    columnDefs: [{
        targets: 'no-sort',
        orderable: false
    }]
});

var allChecked = false;
if(!view_only) {
    $(".selectAllCbClicker").on('click', function() {
        if(!allChecked) {
            $(".applyToAll").prop("checked", true);
            $(".selectAllCb").hide();

            $(".unselectAllCb").removeClass('hidden');
            $(".unselectAllCb").show();
            allChecked = true;
        } else {
            $(".applyToAll").removeProp("checked");
            $(".unselectAllCb").hide();
            $(".selectAllCb").show();
            allChecked = false;
        }
    });
} else {
    $(".applyToAll").prop("disabled", true);
}

$(".applyToAll").live('click', function(ev) {
    ev.stopPropagation();
});

$("#CsvImportFileRowsContainer").resizable();

$('.csvimportFileRow').hover(
    function () {
        $(this).addClass("trHover");
    },
    function () {
        $(this).removeClass('trHover');
    }
);

var checkScheduleReconcile = function() {
    if(view_only) return false;

    if(totalRowsToReconcile == reconciledRows.length) {
        $("#scheduleReconcile").removeClass('hidden');
        return true;
    }

    return false;
};

var checkRowReconcile = function(id) {
    if($.inArray(id, reconciledRows) == -1) {
        reconciledRows.push(id);
    }

    return checkScheduleReconcile();
};

var rowsLoaded = [];
$('.csvimportFileRow').die('click').live('click',function() {
    $("body").prepend("<div class='loading-container loading'><em></em></div>");

    $(".csvimportFileRow").find("td:not('.apply')").css('background','inherit');
    $(".csvimportFileRow").find("td").css("color", "#333");
    $(this).find('td').css('background','#edf5ff');

    var currentSelectedRowID = $(this).attr('data-reconcile-data-id');
    var currentSelectedRowAction = $(this).attr('data-row-action');

    var clickedElm = $(this);

    $.ajax({
        type: 'POST',
		url: '/admin/import/getColissionData/'+currentSelectedRowID,
		dataType: "json",
		success:function(response) {
		    $("div.loading-container.loading").remove();

		    if(response.status == 'error') {
		        Message.create("error", 'Could not load info. Please try again.');
		        return;
		    }

            var matches = [];
            var i = 0;

            $.each(response.matches, function(contactId, contactData) {
                 matches[i] = {"contact_id": contactId};
                 //matches[i]['DT_RowId'] = 'dt_row_'+contactId;
                 matches[i]['name'] = contactData['name'];
                 matches[i]['selected'] = contactData['selected'];
                 matches[i]['actions'] = '<button type="button" class="btn btn-lg mergeWith" style="width: 90%; margin: 0 auto; display: block;" data-pk="' + contactId + '">Merge</button>';


                $.each(contactData.data, function(key, values) {
                    matches[i][key] = '';
                    $.each(values, function(index, value) {
                        matches[i][key] += value + '<br />';
                    });
                });

                i++;
            });

            stmModal.create({
                title: 'Import Duplicate: Choose an Action below',
                content: $('#template-collision-modal').html(),
                height: 800,
                width: 750
            });

            if(currentSelectedRowAction != 'Ignore') {
                $("#rowIgnored").addClass('hidden');

                if(!view_only) {
                    $("#ignoreAll").attr('data-reconcile-id',currentSelectedRowID);
                    $("#ignoreAll").removeClass('hidden');

                    if(currentSelectedRowAction != "New") {
                        $("#markAsNew").removeClass('hidden');
                        $("#markAsNew").attr("data-reconcile-id", currentSelectedRowID);
                    } else {
                        $("#markAsNew").addClass('hidden');
                        $("#markAsNew").attr("data-reconcile-id", '');
                    }
                }
            } else {
                $("#rowIgnored").removeClass('hidden');
                if(!view_only) {
                    $("#ignoreAll").addClass('hidden');
                    $("#markAsNew").removeClass('hidden');
                    $("#markAsNew").attr("data-reconcile-id", currentSelectedRowID);
                }
            }

            $("#selectedImportRowInfo").html($(clickedElm).html());
            $("#selectedImportRowInfo").find("td:first-child").remove();
            $("#selectedImportRowInfo").find("td:first-child").remove();
            $("#selectedImportRowInfo").find("td:first-child").remove();
            $("#selectedImportRowInfo").find("td:first-child").remove();

		    $("#rowCollisionsInfo").DataTable({
		        data: matches,
		        columns: [
		            {title: "Contact ID", data: "contact_id"},
		            {title: "Name", data: "name"},
		            {title: "Emails", data: "emails"},
		            {title: "Phones", data: "phones"},
		            {title: "Addresses", data: "addresses"},
		            {title: "Actions", data: "actions"}
                ],
                createdRow: function(row, data, index) {
                    if(data.selected)  {
                        $(row).find("td").css('background-color', '#5cb85c');
                        $(row).find("td").css("color", "#FFF");
                        $(row).find(".mergeWith").hide();
                    };
                }
		    });

		    stmModal.windowResizeCallback();

            if(view_only) {
                return;
            }

		    $(".mergeWith").die('click').live('click', function() {
		        $("body").prepend("<div class='loading-container loading'><em></em></div>");

		        var that = $(this);
		        var id = $(this).attr('data-pk');
		        $.ajax({
		            type: 'POST',
		            url: '/admin/import/updateCollision/'+currentSelectedRowID,
		            dataType: "json",
		            data: {
		                action: 'Merge',
		                pk: id
		            },
		            success:function(data) {
                        $("div.loading-container.loading").remove();

                        if(data.status == 'error') {
                            Message.create("error", 'Could not load info. Please try again.');
                            $(".displayRow-"+currentSelectedRowID).find('.passFail').html('<i class="fa fa-2x fa-question-circle" style="color: #C50000;"></i>');
                            return;
                        }

                        Message.create('success', 'Row Updated.');
                        checkRowReconcile(currentSelectedRowID);
                        clickedElm.attr('data-row-action', 'Merge');
                        $("#ignoreAll").attr('data-reconcile-id',currentSelectedRowID);
                        $("#ignoreAll").removeClass('hidden');
                        $("#rowIgnored").addClass('hidden');

                        $(".reconcoleLine-action-"+currentSelectedRowID).html('Merge');
                        $(".displayRow-"+currentSelectedRowID).find('.passFail').html('<i class="fa fa-2x fa-check-circle" style="color: #5cb85c;"></i>');

                        $("#ignoreAll").removeClass('hidden');
                        $('#rowIgnored').addClass('hidden');
                        $('#markAsNew').removeClass('hidden');

                        $("#rowCollisionsInfo").find("td").css("background-color", "inherit");
                        $("#rowCollisionsInfo").find("td").css("color", "#333333");
                        $(".mergeWith").show();

                        $(that).hide();
                        $(that).closest('tr').find('td').css('background-color', '#5cb85c');
                        $(that).closest('tr').find("td").css("color", "#FFF");

                        stmModal.destroy();
		            },
                    error: function(data) { // if error occurred
                        $("div.loading-container.loading").remove();
                        Message.create("error", "Error occurred. Please try again.");
                    }
		        });
		    });
		},
		error: function(data) { // if error occured
			$("div.loading-container.loading").remove();
			Message.create("error", "Error occured. Please try again.");
		},
    });
});

$("#ignoreAll, #ignoreAllCB").live('click', function() {

    if(view_only) return;

    var id = $(this).attr('data-reconcile-id');
    var _data = {};

    if(id == "ALL") {
        var ids = [];
        $(".applyToAll").each(function() {
            if($(this).prop("checked")) {
                ids.push($(this).val());
            }
        });

        if(ids.length < 1) {
            alert("Please select at least 1 row.");
            return false;
        }

        _data['ids'] = $.unique(ids);
        _data['file_id'] = file_id;
    }

    $("body").prepend("<div class='loading-container loading'><em></em></div>");

    $.ajax({
        type: 'POST',
		url: '/admin/import/ignoreCollision/'+id,
		dataType: "json",
		data: _data,
		success:function(data){

		    // Destroy modal
            stmModal.destroy();

		    $("div.loading-container.loading").remove();

		    if(data.status == 'error') {
                Message.create("error", 'Could not load. Please try again.');
                return;
            }

            if(id !== "ALL") {
                checkRowReconcile(id);

                $(".reconcoleLine-action-"+id).html('Ignore');
                $(".reconcoleLine-action-"+id).parent().attr('data-row-action', 'Ignore');
                $(".reconcoleLine-action-"+id).css('background-color','#FBE9E6');
                $(".reconcoleLine-action-"+id).css('color','#555');
                $(".displayRow-"+id).find('.passFail').html('<i class="fa fa-2x fa-check-circle" style="color: #5cb85c;"></i>');
            } else {
                $.each(data.ids, function(index, value) {
                    checkRowReconcile(value);

                    $(".reconcoleLine-action-"+value).html('Ignore');
                    $(".reconcoleLine-action-"+value).parent().attr('data-row-action', 'Ignore');
                    $(".reconcoleLine-action-"+value).css('background-color','#FBE9E6');
                    $(".reconcoleLine-action-"+value).css('color','#555');
                    $("#applyToAll-"+value).removeProp('checked');
                    $(".displayRow-"+value).find('.passFail').html('<i class="fa fa-2x fa-check-circle" style="color: #5cb85c;"></i>');
                });
            }

            Message.create('success', 'Row(s) Ignored.');
		},
		error: function(data) { // if error occured
			$("div.loading-container.loading").remove();
			Message.create("error", "Error occured. Please try again.");
		},
    });
});

$("#markAsNew, #markAsNewCB").live('click', function() {
    if(view_only) return;

    var currentSelectedRowID = $(this).attr('data-reconcile-id');
    var ids = _ids = [];

    if(currentSelectedRowID == "ALL") {
         $(".applyToAll").each(function() {
            if($(this).prop("checked")) {
                ids.push($(this).val());
            }
        });

        _ids = $.unique(ids);

        if(_ids.length < 1) {
            alert("Please select at least 1 row.");
            return false;
        }
    }

    $("body").prepend("<div class='loading-container loading'><em></em></div>");

    $.ajax({
        type: 'POST',
        url: '/admin/import/updateCollision/'+currentSelectedRowID,
        dataType: "json",
        data: {
            action: 'New',
            ids: _ids,
            'file_id': file_id
        },
		success:function(data){

		    // Destroy modal
            stmModal.destroy();

		    $("div.loading-container.loading").remove();

		    if(data.status == 'error') {
                Message.create("error", 'Could not load. Please try again.');
                return;
            }

            if(data.id !== "ALL") {
                checkRowReconcile(data.id);

                $(".reconcoleLine-action-"+currentSelectedRowID).html('New');
                $(".reconcoleLine-action-"+currentSelectedRowID).parent().attr('data-row-action', 'New');
                $(".reconcoleLine-action-"+currentSelectedRowID).css('background-color','#5cb85c');
                $(".reconcoleLine-action-"+currentSelectedRowID).css('color','#FFF');
                $(".displayRow-"+currentSelectedRowID).find('.passFail').html('<i class="fa fa-2x fa-check-circle" style="color: #5cb85c;"></i>');
            } else {
                $.each(data.ids, function(index, value) {
                    checkRowReconcile(value);

                    $(".reconcoleLine-action-"+value).html('New');
                    $(".reconcoleLine-action-"+value).parent().attr('data-row-action', 'New');
                    $(".reconcoleLine-action-"+value).css('background-color','#5cb85c');
                    $(".reconcoleLine-action-"+value).css('color','#FFF');
                    $("#applyToAll-"+value).removeProp('checked');
                    $(".displayRow-"+value).find('.passFail').html('<i class="fa fa-2x fa-check-circle" style="color: #5cb85c;"></i>');
                });
            }
            Message.create('success', 'Row(s) Marked as New.');

		},
		error: function(data) { // if error occured
			$("div.loading-container.loading").remove();
			Message.create("error", "Error occured. Please try again.");
		},
    });
});

$("#scheduleReconcile").click(function() {
    if(view_only) return;

     $("body").prepend("<div class='loading-container loading'><em></em></div>");

    if(!checkScheduleReconcile()) {
        $("div.loading-container.loading").remove();
        Message.create("error", 'There are one or more rows that need to be reconciled.');
        return;
    }

    var id = $(this).attr('data-reconcile-file-id');

    $.ajax({
        type: 'POST',
		url: '/admin/import/scheduleReconcileImport/'+id,
		dataType: "json",
		success:function(data){
		    if(data.status == 'error') {
		        $("div.loading-container.loading").remove();
                Message.create("error", 'Could not load. Please try again.');
                return;
            }

            window.location.href = '/admin/import/files/';
		},
		error: function(data) { // if error occured
			$("div.loading-container.loading").remove();
			Message.create("error", "Error occured. Please try again.");
		},
    });
});

checkScheduleReconcile();

setTimeout(function() {
    $("div.loading-container.loading").remove();
}, 500);

JS;


Yii::app()->clientScript->registerScript('importAppointmentsJs', $js, CClientScript::POS_END);