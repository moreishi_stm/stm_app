<?php
$this->breadcrumbs = array(
    'Results' => array('/admin/import/files'),
);
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/import" class="button gray icon i_stm_add">New Import</a>
    <a href="/<?php echo Yii::app()->controller->module->name;?>/dialerimport" class="button gray icon i_stm_add">New Dialer Import</a>
</div>

<h1>Import Results</h1>
<h3 style="max-width: 900px; text-align: left; margin-left: auto; margin-right: auto;">
    Below are the imports from your file upload. When the import is processed, the status will update to "Error", "Reconcile", or "Completed".

    <br><br>If duplicates are detected, a blue "Reconcile"  button will appear in the Actions column. Click on the button and follow the instructions on the next screen.

    <br><br>FYI Workflow Status: New => Scheduled => Processing => [ "Error", "Reconcile", "Completed" ]

    <br><br><a href="/admin/support/video/123" class="btn primary default"><i class="fa fa-video-camera"></i> Click here for Video Tutorial</a>
</h3>
<?

$this->widget('admin_module.components.StmGridView', array(
        'id' => 'task-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'enableSorting' => false,
        'enablePagination' => true,
        'columns' => array(
            'id',
            array(
                'type' => 'raw',
                'name' => 'Description',
                'value' => '$data->title',
            ),
            array(
                'type' => 'raw',
                'name' => 'File Name',
                'value' => '$data->file_name',
            ),
            array(
                'type' => 'raw',
                'name' => 'Status',
                'value' => '(($data->status == "Error") ? "See Import Log" : $data->status)',
            ),
            array(
                'type' => 'raw',
                'name' => 'File Errors',
                'value' => '$data->error_message',
            ),
            array(
                'type' => 'raw',
                'name' => 'New Rows',
                'value' => '$data->new',
            ),
            array(
                'type' => 'raw',
                'name' => 'Collisions',
                'value' => '$data->collisions',
            ),
            array(
                'type' => 'raw',
                'name' => 'Collisions Reconciled',
                'value' => '$data->collisions_reconciled',
            ),
            array(
                'type' => 'raw',
                'name' => 'Reconciled Errors',
                'value' => '$data->reconcile_errors',
            ),
            array(
                'type' => 'raw',
                'name' => 'Errors',
                'value' => '$data->errors',
            ),
            array(
                'type' => 'raw',
                'name' => 'Added',
                'value' => 'Yii::app()->format->formatDateTime($data->added)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Scheduled',
                'value' => 'Yii::app()->format->formatDateTime($data->scheduled)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Scanned',
                'value' => 'Yii::app()->format->formatDateTime($data->scanned)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Actions',
                'value' => 'Yii::app()->controller->printGridActions($data)',
            ),
        )
    )
);