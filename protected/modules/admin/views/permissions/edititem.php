<?
$this->breadcrumbs=array(
    Yii::app()->format->formatProperCase($authItem->label)=>'',
    'Edit'
);
?>
<div id="content-header">
	<?php
		echo CHtml::tag('h1', $htmlOptions=array(
			'class' => 'p-mt10',
		), 'Editing '.Yii::app()->format->formatProperCase($authItem->label).' '.$authItem->getTypeName());
	?>
</div>

<?php
	$this->renderPartial('_form', array(
		'type' => $authItem->getTypeName(),
		'authItem' => $authItem,
	));

	$this->renderPartial('_inheritedPermissions', array(
		'authItem' => $authItem,
	));
?>
