<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/26/13
 * @var PermissionsController $this
 * @var AuthItem $authItem
 *
 */
$this->breadcrumbs = array(
	'Role' => '/'.Yii::app()->controller->module->id.'/permissions/list/role',
	'Add' => '',
);
?>
<div id="content-header">
	<?php echo CHtml::tag('h1', $htmlOptions=array(
			'class' => 'p-mt10',
		), 'Add New '.$authItemTypeStr); ?>
</div>

<?php
	$this->renderPartial('_form', array(
		'type' => $authItemTypeStr,
		'authItem' => $authItem,
	));
?>
