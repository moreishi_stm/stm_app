<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/27/13
 * @var CActiveDataProvider $authItemProvider
 */
$linkToAjaxJs = <<<JS

	$("form#permissions-options-form").change(function(){
		$.fn.yiiGridView.update("auth-item-grid", { data: $(this).serialize() + "&updatePermissionsFlag=0" });
	});

	$(".update-permission-button").click(function(){
		var params = $.extend($("form#permissions-options-form").serialize(), {authNames : $.fn.yiiGridView.getChecked("auth-item-grid","authNames").toString(), updatePermissionsFlag : 1});
		$.fn.yiiGridView.update("auth-item-grid", { data: params });
	});

JS;
Yii::app()->getClientScript()->registerScript('ajaxLinkJs', $linkToAjaxJs);

$this->breadcrumbs = array(
	'Assign' => '',
);
?>
<style type="text/css">
	#permissions-options-form .chzn-container-single {
		margin-right: 8px;
	}
</style>
<h1 class="p-mb10" style="margin-bottom: 20px;">Assign Permissions</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
											  'action'=>Yii::app()->createUrl($this->route),
											  'method'=>'get',
											  'id'=>'permissions-options-form',
											  )); ?>
	<div class="g12 p-f0 p-mc p-pv10">
		<?php
		echo CHtml::dropDownList('assignStatus', null, array(''=>'',1=>'Assigned',0=>'Unassigned'), $htmlOptions=array('data-placeholder' => 'Select Status','style'=>'width: 150px'));

		echo CHtml::dropDownList('authItemType', CAuthItem::TYPE_TASK, array(''=>'',CAuthItem::TYPE_TASK=>'Task',CAuthItem::TYPE_OPERATION=>'Operation'), $htmlOptions=array('data-placeholder' => 'Select Type','style'=>'width: 150px'));

		$taskList = CHtml::listData($tasks, 'name', 'label');
		echo CHtml::dropDownList('authTask', null, CMap::mergeArray(array(''=>''),$taskList), $htmlOptions=array('data-placeholder' => 'Select a Task','style'=>'width: 200px'));

		$operationList = CHtml::listData($operations, 'name', 'label');
		echo CHtml::dropDownList('authOperation', null, CMap::mergeArray(array(''=>''),$operationList), $htmlOptions=array('data-placeholder' => 'Select an Operation'));

		$roleList = CHtml::listData($roles, 'name', 'label');
		echo CHtml::dropDownList('roleSelector', $roleSelected, CMap::mergeArray(array(''=>''),$roleList), $htmlOptions=array('data-placeholder' => 'Select a Role'));

		$this->widget('admin_module.extensions.EChosen.EChosen', array(
																 'target' => 'form#permissions-options-form select',
																 'options'=>array(
																	 'allow_single_deselect'=>true,
																	 'width'=>'19.2%'
																 ),
																 ));
		?>
	</div>
<?php $this->endWidget(); ?>
<div class="p-clr p-tc p-mt10">
    <?php echo CHtml::tag('a', $htmlOptions=array('href'=>'javascript:void(0)','class'=>'btn wide update-permission-button'), $content='UPDATE PERMISSIONS'); ?>
</div>
<div id="content-body">
	<?php
		$this->widget('StmGridView', array(
			'id' => 'auth-item-grid',
			'dataProvider' => $authItemProvider,
			'emptyText' => 'Select a Role To Adjust Permissions',
			'selectableRows'=>2,
			'columns' => array(
//				array(
//					'name'=>'',
//					'value'=>'CHtml::checkBox("cid[]", ($data->findChild(Yii::app()->user->getState(AssignAction::STATE_ROLE))) ? true : false, array("value"=>$data->name,"id"=>"cid_".$data->name))',
//					'type'=>'raw',
//					'htmlOptions'=>array('width'=>5),
//				),
				array(
					'class'=>'CCheckBoxColumn',
					'id'=>'cid',
					'checkBoxHtmlOptions' => array(
						'name' => 'authNames[]',
					),
					'value'=>'$data->name',
					'checked'=>'($data->findChild(Yii::app()->user->getState(AssignAction::STATE_ROLE))) ? true : false',
				),
				'label:properCase',
				'typeName:properCase',
				'description',
				'bizrule',
			),
		));
	?>
</div>
<div class="p-clr p-tc">
	<?php echo CHtml::tag('a', $htmlOptions=array('href'=>'javascript:void(0)', 'class'=>'btn wide update-permission-button'), $content='UPDATE PERMISSIONS'); ?>
</div>

