<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/25/13
 */

/**
 * Handles the auto generation process via ajax
 */
$generateActionOperationsJs = <<<JS
    $('a#generate-action-perms').live('click',function() {
        Message.create('notice', 'Auto-generating permissions, please wait...');
        var linkLocation = $(this).attr('href');
        $.getJSON(linkLocation, function(response) {console.log("hi");
            if (response.isSuccessful) {
                Message.create('success', 'Successfully generated action permissions!');
            } else {
                Message.create('error', 'Could not automatically generate permissions!');
            }

            $.fn.yiiGridView.update('authitem-grid');
        });
        return false;
    });

    $('.deletePermission').live('click',function() {
        if(confirm('Are you sure you want to delete this permission?')) {
            $("body").prepend('<div class="loading-container loading"><em></em></div>');
            var linkLocation = $(this).attr('href');
            $.getJSON(linkLocation, function(response) {
                if (response.isSuccessful) {
                    Message.create('success', 'Successfully deleted permissions!');
                    $.fn.yiiGridView.update('authitem-grid');
                } else {
                    Message.create('error', 'Error: Permission was not deleted.');
                }
            })
                .fail(function() {
                    Message.create('error', 'Could not automatically generate permissions!');
                });

            $("div.loading-container.loading").remove();
        }

        return false;
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('authitem-grid', { data: $(this).serialize() });
        return false;
    });
JS;
Yii::app()->getClientScript()->registerScript('autoGeneratePermsJs', $generateActionOperationsJs);

$this->breadcrumbs = array(
	'List' => '',
);

?>
<div id="listview-actions">
    <?php

    echo CHtml::link('Generate Permissions', array('/'.Yii::app()->controller->module->name.'/permissions/generate'), $htmlOptions=array(
        'id' => 'generate-action-perms',
        'class' => 'button gray icon i_stm_add',
    ));

	echo CHtml::link('Add Task', array('/'.Yii::app()->controller->module->name.'/permissions/add', 'type'=>'task'), $htmlOptions=array(
		'class' => 'button gray icon i_stm_add',
	));
    echo CHtml::link('Add Operation', array('/'.Yii::app()->controller->module->name.'/permissions/add', 'type'=>'operation'), $htmlOptions=array(
            'class' => 'button gray icon i_stm_add',
        ));
    echo CHtml::link('Assign', array('/'.Yii::app()->controller->module->name.'/permissions/assign', 'type'=>'task'), $htmlOptions=array(
            'class' => 'button gray icon i_stm_search',
        ));
    ?>
</div>
    <h1 class="p-mt10">Permissions List</h1>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBox', array(
												 'model' => new AuthItem,
												 )
	); ?>
</div>

<?php
$this->widget('StmGridView', array(
    'id' => 'authitem-grid',
	'dataProvider' => $authItemProvider,
	'columns' => array(
		'label:properCase',
		'description:properCase',
		'bizrule',
		array(
			'class' => 'CLinkColumn',
			'label' => 'Edit',
			'urlExpression' => 'array("/".Yii::app()->controller->module->name."/permissions/edit", "type"=>$data->name)',
			'htmlOptions' => array(
				'style' => 'text-align: center',
			),
			'linkHtmlOptions' => array(
				'class' => 'button gray icon i_stm_edit',
			),
		),
		array(
			'class' => 'CLinkColumn',
			'label' => 'Delete',
			'urlExpression' => 'array("/".Yii::app()->controller->module->name."/permissions/delete", "type"=>$data->name)',
			'htmlOptions' => array(
				'style' => 'text-align: center',
			),
			'linkHtmlOptions' => array(
				'class' => 'button gray icon i_stm_delete deletePermission',
			),
		),
	),
));

