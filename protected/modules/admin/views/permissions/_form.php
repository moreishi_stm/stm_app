<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/26/13
 * @var PermissionsController $this
 * @var AuthItem $authItem
 */

/** @var CActiveForm $form  */
$form = $this->beginWidget('CActiveForm', array(
		'id' => 'authitem-form',
		'enableAjaxValidation' => false,
));

$this->beginStmPortletContent(array(
		'handleTitle' => (($authItem->isNewRecord) ? 'Add' : 'Edit').' '.$type,
		'handleIconCss' => 'i_user'
));
?>
<div id="authitem-form-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container">
			<tr>
				<th class="narrow">
					<?php echo $form->labelEx($authItem, 'name'); ?>
				</th>
				<td>
					<?php echo $form->textField($authItem, 'name', $htmlOptions = array(
							'class' => 'g6'
						)); ?>
					<?php echo $form->error($authItem, 'name'); ?>
				</td>
				<th class="narrow">
					<?php echo $form->labelEx($authItem, 'description'); ?>
				</th>
				<td colspan="2">
					<?php echo $form->textArea($authItem, 'description', $htmlOptions = array(
							'class' => 'g9'
						)); ?>
					<?php echo $form->error($authItem, 'description'); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow">
					<label for="AuthItem_label" class="required">Label <span class="required">*</span></label>
				</th>
				<td	>
					<?php echo $form->textField($authItem, 'label', $htmlOptions = array(
							'class' => 'g6'
						)); ?>
					<?php echo $form->error($authItem, 'label'); ?>
				</td>
				<th class="narrow">
					<?php echo $form->labelEx($authItem, 'bizrule'); ?>
				</th>
				<td colspan="2">
					<?php echo $form->textArea($authItem, 'bizrule', $htmlOptions = array(
							'class' => 'g9'
						)); ?>
					<?php echo $form->error($authItem, 'bizrule'); ?>
				</td>
			</tr>
		</table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
	<?php echo CHtml::htmlButton((($authItem->isNewRecord) ? 'Add' : 'Save').' '.$type, $htmlOptions = array(
		'class' => 'submit wide',
		'type' => 'submit',
	)); ?>
</div>
<?php $this->endWidget(); ?>
