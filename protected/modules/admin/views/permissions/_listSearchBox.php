<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>'',
	'method'=>'get',
	'id'=>'permissions-list-search',
)); ?>
	<div class="g2">
	</div>
	<div class="g4">
		<label class="g2">Name:</label>
		<span class="g10"><?php echo $form->textField($model,'name');?></span>
	</div>
    <div class="g3">
        <label class="g2">Type:</label>
        <span class="g10"><?php echo $form->dropDownList($model,'type', array(CAuthItem::TYPE_OPERATION=>'Operation', CAuthItem::TYPE_TASK=>'Task'), $htmlOptions=array('class'=>'g8','empty'=>''));?></span>
    </div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
