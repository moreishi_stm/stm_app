<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 6/27/13
 * @var AuthItemChild $data
 * @var PermissionsController $this
 */

$this->renderPartial('_assignEntry', array(
	'authItem' => $data->itemChild,
    'type' => 'child',
));
