<?php
	/**
	 * @author: Chris Willard <chriswillard.dev@gmail.com>
	 * @since : 6/27/13
	 * @var AuthItem $data
	 */

	echo CHtml::tag('li', $htmlOptions = array(), $authItem->label . ' &nbsp;&nbsp;<a href="javascript:void(0)" class="remove-permission" data-id="'.$authItem->name.'" data-type="'.$type.'">(Remove)</a>');
