<?php
	/**
	 *
	 * @author: Chris Willard <chriswillard.dev@gmail.com>
	 * @since : 6/26/13
	 * @var PermissionsController $this
	 * @var AuthItem              $authItem
	 */
    $module = Yii::app()->controller->module->name;
    $currentPermission = $_GET['name'];
	// Specify the width on the tasks and operation dropdown
	$tasksAndOperationsCss = <<< TASKS_OPERATIONS_CSS
	section {
		padding: 10px;
	}

	select.tasksAndOperations + div.chzn-container {
		width: 200px !important;
	}
TASKS_OPERATIONS_CSS;
	Yii::app()->getClientScript()->registerCss('tasksAndOperationsCss', $tasksAndOperationsCss);

	// Prevent the form from submitting on submit
	$tasksAndOperationsJs = <<<JS
        $('.remove-permission').click(function(){
            if(confirm('Are you sure you want to delete this parent permission?')) {
                $("body").prepend("<div class='loading-container loading'><em></em></div>");
                var id = $(this).data('id');
                $.post('/$module/permissions/deleteParentChild?targetname='+id+'&type='+$(this).data('type') + '&subjectname=$currentPermission', function(data) {
                    if(data.status=='success') {
                        Message.create("success","Permission deleted successfully.");
                        window.location.reload();
                    } else {
                        Message.create("error","Error: Permission did not delete.");
                    }
                    $("div.loading-container.loading").remove();
                },"json");
            }
        });
JS;
Yii::app()->getClientScript()->registerScript('tasksAndOperationsJs', $tasksAndOperationsJs);

	$this->widget('admin_exts.EChosen.EChosen', array(
			'target' => 'select.tasksAndOperations',
		)
	);

	/** @var CActiveForm $form */
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'authitem-form-shared-permissions',
			'enableAjaxValidation' => false,
		)
	);

	$this->beginStmPortletContent(array(
			'handleTitle' => 'Inherited Permissions',
			'handleIconCss' => 'i_user'
		)
	);
?>
<div id="authitem-form-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<section>
			<strong>Parent(s):</strong>
			<?php
				$this->widget('zii.widgets.CListView', array(
                        'id' => 'parent-grid',
						'dataProvider' => new CActiveDataProvider('AuthItemChild', array(
							'criteria' => array(
								'scopes' => array(
									array('byChild' => array('child' => $authItem->name)),
								),
							),
                            'pagination'=>array(
                                'pageSize'=>100,
                            )
						)),
						'itemView' => '_parentsEntry',
						'summaryText' => '',
					)
				);
			?>
		</section>
		<section>
			<strong>Children:</strong>
			<?php
				$this->widget('zii.widgets.CListView', array(
                        'id' => 'child-grid',
						'dataProvider' => new CActiveDataProvider('AuthItemChild', array(
							'criteria' => array(
								'scopes' => array(
									array('byParent' => array('parent' => $authItem->name)),
								),
							),
                            'pagination'=>array(
                                'pageSize'=>100,
                            )
						)),
						'itemView' => '_childrenEntry',
						'summaryText' => '',
					)
				);
			?>
		</section>
		<section>
			<?php
				$tasksAndOperations = AuthItem::model()->byTypes(array(
						CAuthItem::TYPE_TASK,
						CAuthItem::TYPE_OPERATION
					)
				                      )->byNotAssigned($authItem->name)->findAll();
				$tasksAndOperationsListData = CHtml::listData($tasksAndOperations, 'name', 'label', 'typeNamePlural');
				echo CHtml::dropDownList('tasksAndOperations', null, $tasksAndOperationsListData, $htmlOptions = array(
						'class' => 'tasksAndOperations',
					)
				);

				echo CHtml::htmlButton('Add Child', $htmlOptions = array(
						'id' => 'add-child',
						'class' => 'submit wide',
						'type' => 'submit',
					)
				);
			?>
		</section>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<?php $this->endWidget(); ?>
