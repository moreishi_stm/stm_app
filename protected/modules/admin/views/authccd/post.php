<form name="login-form" id="login-form-ccd" action="http://www.<?=$domain;?>/admin" method="post">
	<input type="hidden" name="LoginForm[email]" id="LoginForm_email" value="<?=$username; ?>" />
	<input type="hidden" name="LoginForm[password]" id="LoginForm_password" value="<?=$credentials;?>" />
</form>

<h1>Please wait...</h1>

<?php
Yii::app()->clientScript->registerScript('autchCCD', <<<JS
	$("body").prepend("<div class='loading-container loading'><em></em></div>");
	setTimeout(function() {
		$("#login-form-ccd").submit();
	}, 500);
JS
);