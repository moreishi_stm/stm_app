<style type="text/css">
    h1 {
        margin: 50px 0px;
    }
    #form-call-now {
        margin-top: 30px;
        text-align: center;
        display: none;
    }
    #form-call-now label {
        display: block;
    }
    #from {
        max-width: 150px;
    }

    #after-confirm {
        max-width: 500px;
        font-size: 15px;
        font-weight: bold;
        background-color: #00a500;
        color: white;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-top: 30px;
    }

    /* Details container */
    #container-details h3 {
        text-align: left;
    }

    #container-details .live-activity-buttons {
        text-align: center;
    }

    #dt thead tr th div{
        font-weight: bold;
        font-size: 14px;
    }

    /* Label styling for status badges and more */
    .label {
        display: inline-block;
        padding: 5px;
        font-weight: bold;
        color: white;
        border-radius: 3px;
    }
    .label.label-default {
        background-color: #777;
    }
    .label.label-primary {
        background-color: #428bca;
    }
    .label.label-success {
        background-color: #5cb85c;
    }
    .label.label-info {
        background-color: #5bc0de;
    }
    .label.label-warning {
        background-color: #f0ad4e;
    }
    .label.label-danger {
        background-color: #d9534f;
    }
    select optgroup{
        /*background:#000;*/
        color:black;
        /*font-style:normal;*/
        font-weight:bold;
    }
    select option{
        border: 0;
        padding-left: 20px;
    }</style>

<div id="listview-actions">
    <a href="/<?php echo $this->module->name;?>/dialer/sessions" class="button gray icon i_stm_search">Call Sessions</a>
    <a href="/<?php echo $this->module->name;?>/dialer/callFinder" class="button gray icon i_stm_search" target="_blank">Call Finder</a>
    <a href="/<?php echo $this->module->name;?>/dialer/recordings" class="button gray icon i_stm_search" target="_blank">Call Recordings</a>
</div>

<h1>Dialer</h1>

<?php if($hasActiveSession): ?>
    <h3>Active Call Session already exists. Please hang up that session to continue and refresh this page.</h3>
<?php else: ?>
    <form style="display: table; width: 100%; margin-bottom: 20px;">
        <div style="text-align: center;">
            <span style="font-size: 14px;">Select a Super Easy Call List:</span>
            <!-- Used to be #session-->
            <select id="preset" style="width: 300px; max-width: 350px; display: inline-block;">
                <option value=""></option>
                <?php if(strpos($_SERVER['SERVER_NAME'], 'www.seizethemarket.') !== false):?>

                    <optgroup label="Phone Tasks">
                        <option value="<?php echo CallLists::MY_CONTACTS_TASKS;?>" data-tableType="task">My Contact Phone Tasks</option>
                    </optgroup>
                    <optgroup label="Lead Pool Contacts (Shared List)">
                        <option value="<?php echo CallLists::LEAD_POOL_CONTACTS_TASKS;?>" data-tableType="task">Lead Pool Contacts</option>
                    </optgroup>

                <?php else: ?>

                    <optgroup label="Phone Tasks">
                        <option value="<?php echo CallLists::MY_SELLERS_TASKS;?>" data-tableType="task">My Seller Phone Tasks</option>
                        <option value="<?php echo CallLists::MY_BUYERS_TASKS;?>" data-tableType="task">My Buyer Phone Tasks</option>
                        <option value="<?php echo CallLists::MY_RECRUITS_TASKS;?>" data-tableType="task">My Recruiting Phone Tasks</option>
                    </optgroup>
                    <?php if(Yii::app()->user->settings->lead_pool_contact_id):?>
                        <optgroup label="Lead Pool Phone Tasks (Shared List)">
                            <option value="<?php echo CallLists::LEAD_POOL_SELLERS_TASKS;?>" data-tableType="task">Lead Pool Seller Phone Tasks</option>
                            <option value="<?php echo CallLists::LEAD_POOL_BUYERS_TASKS;?>" data-tableType="task">Lead Pool Buyer Phone Tasks</option>
                        </optgroup>
                    <?php endif; ?>
                    <optgroup label="Seller Lead Pool (Shared List)">
                        <option value="<?php echo CallLists::ALL_NEW_SELLER_PROSPECTS;?>" data-tableType="time">New Sellers Leads</option>
                        <option value="<?php echo CallLists::ALL_NURTURING_SELLERS;?>" data-tableType="time">Nurturing Sellers Leads</option>
                    </optgroup>
                    <optgroup label="Buyer Lead Pool (Shared List)">
                        <option value="<?php echo CallLists::ALL_NEW_BUYER_PROSPECTS;?>" data-tableType="time">New Buyers Leads</option>
                        <option value="<?php echo CallLists::ALL_NURTURING_BUYERS;?>" data-tableType="time">Nurturing Buyers Leads</option>
                    </optgroup>

                <?php endif; ?>



                <!--                --><?//foreach($callLists as $callList) : ?>
                <!--                <option value="--><?//=$callList->id?><!--">--><?//=$callList->name?><!-- | Total: --><?//=$callList->getDetails()->total?><!-- | Called: --><?//=$callList->getDetails()->num_called?><!-- | Created By: --><?//=$callList->contact->first_name?><!-- --><?//=$callList->contact->last_name?><!-- | Created On: --><?//=$callList->added?><!--</option>-->
                <!--                --><?//endforeach;?>
            </select>
            <button type="button" id="btn-load-list">Load Call List</button>
        </div>
        <div id="after-confirm" style="display: none;">Enter your phone # and click the "Start Calling" button. <br>The phone # entered will be called to start the session.</div>

        <!--    <div style="display: table-row;">-->
        <!--        <div style="display: table-cell;"></div>-->
        <!--        <div style="display: table-cell;">&nbsp;</div>-->
        <!--        <div style="display: table-cell;"><label for="name" style="display: block;">List Name</label></div>-->
        <!--    </div>-->
        <!--    <div>-->
        <!--        <div style="display: table-cell; padding: 0px 10px;">-->
        <!--            - Or --->
        <!--        </div>-->
        <!--        <div style="display: table-cell;">-->
        <!--            <input type="text" id="name" style="width: 50%">-->
        <!--            <select id="component-type-id" style="max-width: 150px; width: 50%%;">-->
        <!--                <option value="">- Select a Type -</option>-->
        <!--                <option value="--><?//=ComponentTypes::RECRUITS?><!--">Recruits</option>-->
        <!--                <option value="--><?//=ComponentTypes::CONTACTS?><!--">Contacts</option>-->
        <!--                <option value="--><?//=ComponentTypes::BUYERS?><!--">Buyers</option>-->
        <!--                <option value="--><?//=ComponentTypes::SELLERS?><!--">Sellers</option>-->
        <!--                <option value="--><?//=ComponentTypes::CLOSINGS?><!--">Closings</option>-->
        <!--                <option value="--><?//=ComponentTypes::PROJECTS?><!--">Projects</option>-->
        <!--            </select>-->
        <!--            <button type="button" id="btn-new">Create New</button>-->
        <!--        </div>-->
        <!--    </div>-->
    </form>
<?php endif; ?>

<form method='POST' id='form-call-now'>
    <div id="call-filter-row" style="margin-bottom: 25px;">
        <label style="font-size: 14px; display: inline-block;">FILTERS:</label>
        <select id='maxDialerCallCount' name='maxDialerCallCount'>
            <option value=""></option>
            <option value="1">No dialer calls from this list</option>
            <option value="5">Less than 5 dialer calls from this list</option>
            <option value="10">Less than 10 dialer calls from this list</option>
            <option value="15">Less than 15 dialer calls from this list</option>
            <option value="20">Less than 20 dialer calls from this list</option>
            <option value="25">Less than 25 dialer calls from this list</option>
            <option value="30">Less than 30 dialer calls from this list</option>
        </select>
        <select id='maxDialerCallDays' name='maxDialerCallDays'>
            <option value=""></option>
            <option value="7">in the last 7 Days</option>
            <option value="14">in the last 14 Days</option>
            <option value="30">in the last 30 Days</option>
            <option value="45">in the last 45 Days</option>
            <option value="60">in the last 2 Months</option>
            <option value="90">in the last 3 Months</option>
            <option value="180">in the last 6 Months</option>
            <option value="270">in the last 9 Months</option>
            <option value="365">in the last 1 Year</option>
        </select>
        <label style="font-size: 14px; display: block;">(Automatically ordered by the most engaged leads first.)</label>
    </div>
    <div>
        <label for='from' style="font-size: 14px; display: inline-block;">Your Phone:</label>
        <?php $this->widget('StmMaskedTextField', array(
            'name' => 'from',
            'mask' => '(999) 999-9999',
            'id' => 'from',
            'value' => $_COOKIE['dialerNumber'],
        )); ?>
        <label for='num_to_call' style="font-size: 14px; display: inline-block;"># Simultaneous Lines to Dial:</label>
        <select id='num_to_call' name='num_to_call'>
            <option value="1">1 line</option>
            <option value="2">2 lines</option>
            <option value="3">3 lines</option>
            <option value="4">4 lines</option>
            <option value="5">5 lines</option>
            <option selected value="6">6 lines</option>
        </select>
        <button type='submit' id='btn-start'>Start Calling</button>
    </div>
    <div style="text-align: center;">
        <label style="font-size: 14px; display: inline-block;">Caller ID:</label>
        <?php
        echo CHtml::textField('callerIdName', $callerIdName, $htmlOptions=array('style'=>'width: 200px; display: inline-block;','placeholder'=>'Caller ID Name'));
        $this->widget('StmMaskedTextField', array(
            'name' => 'callerId',
            'mask' => '(999) 999-9999',
            'id' => 'callerIdNumber',
            'value' => $callerIdNumber,
            'htmlOptions' => $htmlOptions=array('style'=>'width: 200px; display: inline-block; margin-right: 180px;','placeholder'=>'(999) 999-9999')
        )); ?>
    </div>
</form>

<a name="viewTop"></a>
<div id='container-details' style="margin: 30px 0px; font-size: 18px;"></div>
<div id="dt"></div>

<? Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
        'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
        'title' => 'Add New Activity Log',
        'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        'parentModel' => new Contacts
    ));
} ?>

<? Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
    'id' => TaskDialogWidget::TASK_DIALOG_ID,
    'title' => 'Create New Task',
    'triggerElement' => '.add-task-button, .edit-task-button, .delete-task-button',
    'parentModel' => new Contacts
)); ?>

<input type="hidden" id="nodeUrl" value="<?=$nodeUrl?>">

<!-- Templates -->
<script id="template-details-incall" type="text/x-handlebars-template">
    <hr>
    <h1>{{phone}}</h1>
    <div class='live-activity-buttons'>
        <button type='button' data-type='live-answer'>Live Answer</button>
        <button type='button' data-type='no-answer'>No Answer</button>
        <button type='button' data-type='busy'>Busy [???]</button>
        <button type='button' data-type='remove-bad-number'>Remove Bad Number</button>
        <button type='button' data-type='fax'>Fax Line</button>
    </div>
    <hr>
</script>

<script id="template-details-postcall" type="text/x-handlebars-template">
    <hr>
    <div style='display: table; width: 100%;'>
        <div style='display: table-row'>
            <div style='display: table-cell; width: 36%; padding-left: 2%; padding-right: 2%;'>
                <h3>Name</h3>
                <a href='/admin/{{component_name}}/{{component_id}}' target='_blank'>{{first_name}} {{spouse_first_name}} {{last_name}}</a><br>
                {{formatPhone phone}}<br>
                <a href='/admin/{{component_name}}/{{component_id}}' class='button gray icon i_stm_search grey-button' target='_blank'>View Details</a><br><br>
                <h3>Task Description</h3>
                <i>{{description}}</i><br><br>
                <hr/>
                <h3>Address</h3>
                {{address}}<br>
                {{city}}, {{state}} {{zipcode}}<br><br>
                <div class='task-buttons'>
                    <h3>Add New Task (defaults to Phone)</h3>
                    <button type='button' class='add-task-button phone-call-button' data-days='1' data='{{component_id}}' ctid='{{component_type_id}}'>Tomorrow</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='2' data='{{component_id}}' ctid='{{component_type_id}}'>2 Days</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='5' data='{{component_id}}' ctid='{{component_type_id}}'>5 Days</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='7' data='{{component_id}}' ctid='{{component_type_id}}'>1 Week</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='30' data='{{component_id}}' ctid='{{component_type_id}}'>1 Month</button>
                    <button type='button' class='add-task-button phone-call-button' data='{{component_id}}' ctid='{{component_type_id}}'>Custom Date</button>
                </div>
            </div>
            <div style='display: table-cell; width: 50%; padding-right: 10%;'>
                <div style='margin-top: 20px;'>
                    <button type='button' id="vm-mark-no-answer" data-id='{{activity_log_id}}' style='font-size: 16px;'>Got Voicemail, Hangup, Mark as No Answer</button>
                    <h3>Activity Log Notes</h3>
                    <form method='POST' class='activity-form'>
                        <textarea rows='8' style='width: 100%;' name='ActivityLog[note]'></textarea><br>
                        <select multiple='multiple' name='ActivityLog[lead_gen_typ_ma]' style='width: 100%; max-width: 250px; height: 100%;'>
                            <option selected value=''>- Select Lead Gen Type -</option>
                            <option value='1'>New</option>
                            <option value='2'>Followup</option>
                            <option value='0'>None</option>
                        </select>
                        <select multiple='multiple' name='ActivityLog[is_spoke_to]' style='width: 100%; max-width: 250px; height: 100%'>
                            <option selected value=''>- Select Spoke To -</option>
                            <option value='1'>Yes</option>
                            <option value='0'>No</option>
                        </select>
                        <select multiple='multiple' name='ActivityLog[asked_for_referral]' style='width: 100%; max-width: 250px; height: 100%'>
                            <option selected value=''>- Select Asked for Referral -</option>
                            <option value='1'>Yes</option>
                            <option value='0'>No</option>
                        </select>
                        <br>
                        <input type='hidden' name='ActivityLog[component_id]' value='{{component_id}}'>
                        <input type='hidden' name='ActivityLog[component_type_id]' value='{{component_type_id}}'>
                        <input type='hidden' name='ActivityLog[activity_date]' value='{{activity_date}}'>
                        <input type='hidden' name='ActivityLog[task_type_id]' value='42'>
                        <input type='hidden' name='ActivityLog[task_id]' value='{{task_id}}'>
                        <input type='hidden' name='ActivityLog[activity_date]' value='<?=date('Y-m-d H:i:s')?>'>
                        <input type='hidden' name='is_dialer' value='true'>
                        <input type='hidden' name='call_id' value='{{call_id}}'>
                        <button type='submit' style='font-size: 16px;'>Complete Task & Save Activity Log</button>
                        <button type='button' class="close-notes-area" style='font-size: 16px;'>Close</button>
                    </form>
                    <div class='activity-success-message' style="text-align: center; margin: 30px 0px; display: none;">
                        <div>- Activity Log Entry Saved! -</div>
                        <button type='button' class='activity-log-add-another'>Add Another</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</script>

<!-- JavaScript things -->
<script src="http://yui.yahooapis.com/3.17.2/build/yui/yui-min.js"></script>
<script type="text/javascript">

// Use YUI things
YUI().use('node', 'datatable', 'datasource-io', 'datasource-jsonschema', 'datatable-datasource', 'datatype-number'/*, 'gallery-datatable-footerview'*/, 'io', 'io-form', 'json', 'handlebars','node-event-simulate', 'cookie', function(Y) {

    // The selected table type
    var tableType;

    // Callable methods the server has access to
    var DialerClientFunctions = {

        /**
         * Has Action
         *
         * Used to determine if this object has a given action directly in it, will not return true for inherited functions
         * @param string action Action name to check against
         */
        hasAction: function(action) {
            return (this.hasOwnProperty(action) && action != 'hasAction');
        },

        /**
         * System Ready
         *
         * Called once the server has sorted out this websocket connection and is ready for requests
         * @param params Object
         */
        systemReady: function(params) {
            console.log('System Ready...');
        },

        /**
         * Update Data
         *
         * Updates data in data table, will clear any existing data
         * @param params Object
         */
        updateData: function(params) {

            console.log('Update data called!')

            // Update the datatable
            table.datasource.load({
                request: 'id=' + encodeURIComponent(callListId) + '&callsession=' + encodeURIComponent(callSessionId) + '&presetId=' + encodeURIComponent(presetId)
            });
        }
    };

    // Create new Web Socket connection to server
    var ws = new WebSocket('ws://' + Y.one('#nodeUrl').get('value') + '/dialer?session=' + Y.Cookie.get('PHPSESSID'));

    // Do this when the connection is opened
    ws.onopen = function(e) {
        console.log('Web socket connection opened');
    };

    // Do this when the connetion is closed
    ws.onclose = function(e) {
        console.log('Web socket connection closed');
    }

    // Do this when an error occurs
    ws.onerror = function(e) {
        console.log('A web socket error has occurred!');
        console.log(e);
    };

    // Do this when a message is received
    ws.onmessage = function(e) {

        // Attempt to parse JSON data
        try {
            var request = Y.JSON.parse(e.data);
        }
        catch(e) {
            console.log('Invalid JSON returned! Terminating connection!');
            console.log(e);
            ws.close();
        }

        // Make sure that our action exists before we try to run it
        if(!DialerClientFunctions.hasAction([request.action])) {
            console.log('Invalid action requested (' + request.action + ')! Terminating connection!');
            ws.close();
            return;
        }

        // Run action
        DialerClientFunctions[request.action](request.params ? request.params : {});
    };

    // Generic send command funtion to be used with Web Socket server
    ws.socketCommand = function(action, params) {
        ws.send(Y.JSON.stringify({
            action: action,
            params: params
        }));
    };

    // The selected call list ID
    var callListId;

    // The selected call list ID
    var presetId;

    // The currently running call session ID
    var callSessionId;

    // Polling handle
    var polling;

    // The current phone ID
    var currentPhoneId;

    // Custom formatter for phone
    var formatDate = function(o) {
        var date = new Date(o.value);
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
    }

    // Custom formatter for phone
    var formatPhone = function(o) {
        return o.value.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }

    Y.Handlebars.registerHelper("formatPhone", function(phone) {
        phone = phone.replace(/[^0-9]/g, '');
        phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        return phone;
    });

    // Custom formatter for status
    var formatStatus = function(o) {

        // Handle different kinds of statuses
        var label = '';
        switch(o.value) {
            case 'Queued':
                label = 'default';
                break;
            case 'Complete':
                label = 'info';
                break;
            case 'Answered':
                label = 'success';
                break;
            case 'No Answer':
                label = 'danger';
                break;
            case 'Ringing':
                label = 'warning';
                break;
            case 'Skipped':
                label = 'danger';
                break;
            default:
                return o.value;
                break;
        }

        // Return formatted label
        return '<span class="label label-' + label + '">' + o.value + '</span>'
    }

    // Custom formatter for options column
    var formatContactInfo = function(o) {
        return "<a href='/admin/" + o.data.component_type_name + "/" + o.data.component_id + "' style='text-decoration: underline; font-weight: bold;' target='_blank'>"+ o.data.contactInfo  + "</a><br>" + o.data.phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }

    // Custom formatter for options column
    var formatTaskButtons = function(o) {
        return "<button type='button' class='complete-task-button' data='" + o.data.task_id + "' ctid='"+ o.data.component_type_id +"'>Complete</button>&nbsp;"+
            "<button type='button' class='edit-task-button' data='" + o.data.task_id + "' ctid='"+ o.data.component_type_id +"'>Edit</button>&nbsp;"+
            "<button type='button' class='delete-task-button' data='" + o.data.task_id + "' ctid='"+ o.data.component_type_id +"'>Delete</button>&nbsp;"+
            "<button type='button' class='add-task-button' data='" + o.data.component_id + "' ctid='"+ o.data.component_type_id +"'>Add</button>&nbsp;";
    }

    // Custom formatter for options column
    var formatContactTaskButtons = function(o) {
        return "<button type='button' class='add-task-button' data='" + o.data.component_id + "' ctid='"+ o.data.component_type_id +"'>Add</button>";
    }

    // Custom formatter for options column
    var formatLogButtons = function(o) {
        return "<button type='button' class='log-activity add-activity-log-button' data='" + o.data.component_id + "' ctid='"+ o.data.component_type_id+"'>Log Activity</button>";
    }

    // Custom formatter for options column
    var formatQueue = function(o) {

        var label = '';
        var className = ''
        switch(o.data.status) {
            case 'Queued':
                label = 'Skip';
                className = 'skip-queue';
                break;
            case 'No Answer':
                label = 'Add to Queue';
                className = 'add-to-queue';
                break;
            case 'Skipped':
                label = 'Add to Queue';
                className = 'add-to-queue';
                break;
            case 'Ringing':
                label = 'Hangup All & Next';
                className = 'hangup-queue';
                break;
            case 'Complete':
                label = 'Add to Queue';
                className = 'add-to-queue';
                break;
            default:
                return;
                break;
        }
        return '<button class="' + className + '" data-id="' + o.data.call_list_phone_id + '" type="button">' + label + '</span>'
    }

    // Configurations for our various datatable configurations
    var configs = {
        resultFields: {
            task: [
                'call_list_phone_id',
                'last_called',
                'phone',
                'contactInfo',
                'status',
                'fullName',
                'description',
                'component_id',
                'component_type_id',
                'component_type_name',
                'component_name',
                'due_date',
                'task_id',
                'activity_log_id',
                'last_spoke_to_date'
            ],
            time: [
                'call_list_phone_id',
                'last_login',
                'last_called',
                'dialer_count',
                'phone',
                'contactInfo',
                'status',
                'fullName',
                'description',
                'component_id',
                'component_type_id',
                'component_type_name',
                'component_name',
                'added',
                'last_activity_date',
//                    'activity_count',
                'activity_log_id',
                'last_spoke_to_date'
            ]
        },
        columns: {
            task: [
//                {key: 'phone', label: 'Phone', formatter: formatPhone, width: '120px'},
//                {key: 'contactInfo', label: 'Contact Info', allowHTML: true, width: '180px'},
                {key: 'contactInfo', label: 'Contact Info', formatter: formatContactInfo, allowHTML: true, width: '180px'},
                {key: 'description', label: 'Description'},
                {key: 'due_date', label: 'Due Date', width: '80px'},
                {key: 'last_called', label: 'Last Call<br><span style="font-size: 10px;">For this Task</span>', width: '80px'},
                {key: 'status', label: 'Status', formatter: formatStatus, allowHTML: true, width: '90px'},
                {key: 'queue', label: 'Queue', formatter: formatQueue, allowHTML: true, width: '145px'},
                {key: 'taskButtons', label: 'Tasks', formatter: formatTaskButtons, allowHTML: true, width: '300px'},
                {key: 'logButtons', label: 'Activity Logs', formatter: formatLogButtons, allowHTML: true, width: '120px'}
            ],
            time: [
                {key: 'contactInfo', label: 'Contact Info<br><span style="font-size: 10px;">Click Name for Details</span>', formatter: formatContactInfo, allowHTML: true},
                {key: 'last_spoke_to_date', label: 'Last Spoke to', width: '80px'},
                {key: 'last_login', label: 'Last Login', width: '80px'},
                {key: 'last_called', label: 'Last Call<br><span style="font-size: 10px;">For this Task</span>', width: '80px'},
                {key: 'dialer_count', label: '# Dialer Calls', width: '80px'},
//                    {key: 'activity_count', label: '# Total Touches', width: '80px'},
                {key: 'last_activity_date', label: 'Last Activity', formatter: formatDate, width: '80px'},
//                    {key: 'added', label: 'Submit Date', width: '80px'},
                {key: 'status', label: 'Status', formatter: formatStatus, allowHTML: true, width: '90px'},
                {key: 'queue', label: 'Queue', formatter: formatQueue, allowHTML: true, width: '145px'},
                {key: 'taskButtons', label: 'Tasks', formatter: formatContactTaskButtons, allowHTML: true, width: '80'},
                {key: 'logButtons', label: 'Activity Logs', formatter: formatLogButtons, allowHTML: true, width: '120px'}
            ]
        }
    };


    // Hanle scope for datatable and datasource references
    var table, datasource;

    // Function to create datatable
    var createDataTable = function(tableType) {

        // Destroy datasource if it already exists
        if(datasource) {
            datasource.destroy();
        }

        // Destroy datatable if it already exists
        if(table) {
            table.destroy();
        }

        // Create datasource
        datasource = new Y.DataSource.IO({
            source: '/admin/dialer2/load'
        });

        // Bind event to update statistics when we receive data
        datasource.after('response', function(e) {

            // If we have this, populate the div with details
            //@todo: need to add scenario for if a call is hung up AND not started the next dial yet. This is the time for note taking
            if(e.response.meta.active) {

                // Only do this if the phone ID has changed
                if(currentPhoneId != e.response.meta.active.phone_id) {
                    Y.one('#container-details').setHTML(Y.Handlebars.render(Y.one('#template-details-postcall').getHTML(), e.response.meta.active));
                    currentPhoneId = e.response.meta.active.phone_id;
                }
            }
            else {
                //                Y.one('#container-details').setHTML('');
            }

            // Call session has ended. Stop polling and display start button if the calls session is not in progress. Inform user that session has ended. @todo: supply summary data.
            if(!e.response.meta.in_progress) {

                // Cancel polling, if it's currently in progress
                if(polling) {
                    polling.cancel();
                    polling = null;
                    setTimeout(Y.one('#btn-load-list').simulate('click'), 1500); //@todo: manually clear list
                }

                Y.one('#form-call-now, #after-confirm').show();
                Message.messagePosition = 'top-center';
                Message.create('notice','Call session has ended. You can reload a list and start another session.');

                // remove loading gif if exists
                if(Y.one('div.loading-container.loading')) {
                    $("div.loading-container.loading").remove();                                                                                    //@todo: this is jquery needs to be YUI
                }
            }
        });

        // Use POST instead of GET
        datasource.set('ioConfig', {method: 'POST'});

        // Plug JSON support in to our datasource
        datasource.plug(Y.Plugin.DataSourceJSONSchema, {
            schema: {
                metaFields: {
                    active: 'meta.active',
                    in_progress: 'meta.in_progress'
                },
                resultListLocator: 'results',
                resultFields: configs.resultFields[tableType]
            }
        });

        // Create a new data table
        table = new Y.DataTable({
            columns: configs.columns[tableType],
            sortable: true
//                scrollable: 'y',
//                height: '500px'

//                footerView:   Y.FooterView,
//                footerConfig: {
//                    fixed:   true,
//                    heading: {
//                        colspan:    3,
//                        content:    "{row_count} Records of ??",
//                        className:  "align-left"
//                    },
//                    columns: [
//                        { key:'NumClients', content:"{avg} avg",  className:"clientsClass", formatter:fmtComma2 },
//                        { key:'SalesTTM',   content:"{sum}", className:"salesClass" }
//                    ]
//                }
        });

        // Plug data source in to data table
        table.plug(Y.Plugin.DataTableDataSource, {
            datasource: datasource
        });

        // Render the datatable
        table.render('#dt');
    };

    // Bind click event for resume button
    Y.one('#btn-load-list').on('click', function(e) {

        // clear call answer box in case there's any content present
        Y.one('#container-details').setHTML('');

        // Add loading gif
        if(!Y.one('div.loading-container.loading')) {
            $("body").prepend('<div class="loading-container loading"><em></em></div>');                //@todo: this is jquery needs to be YUI
        }

        // Set call list ID
        // callListId = Y.one('#session').get('value'); //@todo: not using right now, replaced with presetId

        // Set call list preset ID
        presetId = Y.one('#preset').get('value');

        // Stop if we don't have a call list ID
        if(!presetId) { //!callListId == "undefined"
            Message.messagePosition = 'top-center';
            Message.create('warning','Please select a Call List.');
            $("div.loading-container.loading").remove();                                                //@todo: this is jquery needs to be YUI
            return;
        }

        // Create datatable with the current type we want
        createDataTable(Y.one("#preset option:checked").getData("tableType"));     // This is where you drop in the table configuration name, had default in there

        var filterString = '';
        if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
            filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
        }

        // Reload datatable
        table.datasource.load({
            request: 'presetId=' + encodeURIComponent(presetId) +'&initialize=1' + filterString
        });

        // display flash message
//            Message.messagePosition = 'top-center';
//            Message.create('success','Successfully loaded Call List.');
        $('#after-confirm, #form-call-now').show();                                                     //@todo: NOTE: Y.one doesn't work here - ask Nicole why

        if(presetId == <?php echo CallLists::ALL_NURTURING_SELLERS;?> || presetId == <?php echo CallLists::ALL_NURTURING_BUYERS;?>) {
            $('#call-filter-row').show('normal');
        }
        else {
            $('#call-filter-row').hide('normal');
            Y.one('[name="maxDialerCallCount"]').set('value', '');
            Y.one('[name="maxDialerCallDays"]').set('value', '');
        }

        // position screen to top of call data
        window.location.hash='viewTop';                                                                 // @todo: NICOLE  - this is NOT working - HELP!!!!!!!!!!!!***********************

        // remove loading gif
        setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);                    //@todo: this is jquery needs to be YUI

    });

    // Bind click event for new button
//        Y.one('#btn-new').on('click', function(e) {
//
//            // Make sure a name was given
//            if(Y.one('#name').get('value') === '') {
//                alert('Please enter a name to continue');
//                return;
//            }
//
//            // Make sure our type has been chosen
//            if(Y.one('#component-type-id').get('value') === '') {
//                alert('Please choose a type to continue');
//                return;
//            }
//
//            // Create new call list
//            Y.io('/admin/dialer/create', {
//                method: 'POST',
//                data: {
//                    name: Y.one('#name').get('value'),
//                    component_type_id: Y.one('#component-type-id').get('value')
//                },
//                on: {
//                    success: function(id, o) {
//
//                        // Attempt to parse out JSON
//                        try {
//                            var response = Y.JSON.parse(o.responseText);
//                        }
//                        catch(e) {
//                            alert('Error parsing response from server!');
//                        }
//
//                        // Handle different response types
//                        switch(response.status) {
//                            case 'success':
//
//                                // Set call list id
//                                callListId = response.call_list_id;
//
//                                // Reload datatable
//                                table.datasource.load({
//                                    request: 'id=' + encodeURIComponent(callListId)
//                                });
//                            break;
//                            default:
//                                alert('An unknown error has occurred!');
//                            break;
//                        }
//                    }
//                }
//            });
//        });

    // Bind start button event
    Y.one('#form-call-now').on('submit', function(e) {

        // Prevent default
        e.preventDefault();

        // reset phone id value, not resetting will cause active call details to not show if you have to redial the same number
        currentPhoneId=null;

        // position screen to top of call data
        window.location.hash='viewTop';

        // Stop if we don't have a call list ID
        if(!presetId) { //!callListId == "undefined"
            Message.messagePosition = 'top-center';
            Message.create('notice','Please select a Call List and confirm.');
            return;
        }

        // Stop if we don't have a phone number
        if(!Y.one('#from').get('value') || Y.one('#from').get('value').replace(/\D/g,'').length < 10) {                                 //@todo: this is jquery needs to be YUI
            Message.messagePosition = 'top-center';
            Message.create('notice','Please enter your valid 10 digit phone number to call.');
            return;
        }

        // Stop if we don't have a caller ID #
        if(!Y.one('#callerIdNumber').get('value') || Y.one('#callerIdNumber').get('value').replace(/\D/g,'').length < 10) {                                 //@todo: this is jquery needs to be YUI
            Message.messagePosition = 'top-center';
            Message.create('notice','Please enter a valid 10 digit Caller ID Number.');
            return;
        }

        // Stop if we don't have a caller ID name
        if(!Y.one('#callerIdName').get('value')) {                                 //@todo: this is jquery needs to be YUI
            Message.messagePosition = 'top-center';
            Message.create('notice','Please a valid Caller ID Name.');
            return;
        }

        // Stop if max count filter has value, but days filter empty, display notice to make selection
        if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value') == '') {                                 //@todo: this is jquery needs to be YUI
            Message.messagePosition = 'top-center';
            Message.create('notice','Please select filter for number of Days/Months.');
            return;
        }

        // save the number in a cookie
        document.cookie="dialerNumber="+Y.one('#from').get('value').replace(/\D/g,'');                                                  //@todo: this is jquery needs to be YUI
        document.cookie="callerIdNumber="+Y.one('#callerIdNumber').get('value').replace(/\D/g,'');                                                  //@todo: this is jquery needs to be YUI
        document.cookie="callerIdName="+Y.one('#callerIdName').get('value').replace(/\D/g,'');                                                  //@todo: this is jquery needs to be YUI

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                    //@todo: this is jquery needs to be YUI

        // Stop polling, if it is already running
        if(polling) {
            polling.cancel();
            polling = null;
        }

        // Make the call
        Y.io('/admin/dialer/start', {
            method: 'POST',
            data: {
                preset_id: presetId,
                call_list_id: callListId,
                phone_number: Y.one('#from').get('value'),
                num_to_call:  Y.one('#num_to_call').get('value'),
                callerIdName:  Y.one('#callerIdName').get('value'),
                callerIdNumber:  Y.one('#callerIdNumber').get('value')
            },
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':

                            // Hide start button
                            Y.one('#form-call-now, #after-confirm').hide();

                            Y.one('#container-details').setHTML('');
                            Message.messagePosition = 'top-center';
                            Message.create('success','We are calling your phone now. Please answer to continue.');
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 8000);                                //@todo: this is jquery needs to be YUI

                            // Set call session ID
                            callSessionId = response.call_session_id;

                            // Update the datatable every second
                            //@todo: need to stop it if the call session is over.
//                                polling = Y.later(2000, table.datasource, 'load', [{
//                                    request: 'id=' + encodeURIComponent(callListId) + '&callsession=' + encodeURIComponent(callSessionId) + '&presetId=' + encodeURIComponent(presetId)
//                                }], true);

                            // Notify Node that we started a session
                            ws.socketCommand('startSession', {
                                call_list_id: response.call_list_id,
                                call_session_id: response.call_session_id
                            });

                            break;

                        case 'sessionExists':
                            Message.messagePosition = 'top-center';
                            Message.create('error','A session is already in progress. Please end that session and try again.');
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1200);                                //@todo: this is jquery needs to be YUI

                            break;

                        default:
                            Message.messagePosition = 'top-center';
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 0);                                   //@todo: this is jquery needs to be YUI
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }
                }
            }
        });
    });

    // Handle form submit
    Y.one('#container-details').delegate('submit', function(e) {

        // Prevent default
        e.preventDefault();

        // Make sure we selected these items
        if(e.currentTarget.one('[name="ActivityLog[lead_gen_typ_ma]"]').get('value') === '') {
            alert('Please select a lead gen type to continue');
            return;
        }

        if(e.currentTarget.one('[name="ActivityLog[is_spoke_to]"]').get('value') === '') {
            alert('Please notate whether or not you have spoke to the client');
            return;
        }

        if(e.currentTarget.one('[name="ActivityLog[note]"]').get('value') === '') {
            alert('Please enter a valid note');
            return;
        }

        // Create new call list
        Y.io('/admin/activityLog/add', {
            method: 'POST',
            form: {
                id: e.currentTarget
            },
            on: {
                success: function(id, o) {

                    // Hide form and show success message
                    e.currentTarget.hide();
                    e.currentTarget.ancestor().one('.activity-success-message').show();
                }
            }
        });
    }, '.activity-form');

    // Bind click even for close notes area button
    Y.one('#container-details').delegate('click', function() {

        // check to see if there's any data in the notes section as it may be a mistake to click to close button
        if(Y.one('[name="ActivityLog[note]"]').get('value')) {
            if(!confirm('You have information in the activity log notes. Are you sure want to close this area?')) {
                return false;
            }
        }
        Y.one('#container-details').setHTML('');

    }, 'button.close-notes-area');

    // Bind dialer max call count filter drop down changes, update the days dropdown if empty
    Y.one('#maxDialerCallCount').on('change', function() {

        if(Y.one('#maxDialerCallCount').get('value') && !Y.one('#maxDialerCallDays').get('value')) {
            Y.one('#maxDialerCallDays').set('selectedIndex', 3);
        }

    });

    // Bind add another activity log button
    Y.one('#container-details').delegate('click', function() {

        // Clear data
        Y.one('[name="ActivityLog[note]"]').set('value', '');
        Y.one('[name="ActivityLog[lead_gen_typ_ma]"]').set('value', '');
        Y.one('[name="ActivityLog[is_spoke_to]"]').set('value', '');

        // Swap containers
        Y.one('.activity-form').show();
        Y.one('.activity-success-message').hide();

    }, 'button.activity-log-add-another');

    // Bind click even for voicemail button. This will update the activity log as answered to voicemail.
    Y.one('#container-details').delegate('click', function() {

        // prevent double clicking
        this.setAttribute('disabled','disabled');

        // this prevents someone from accidentally clicking on voicemail and eliminating a legitimate call.
        // @todo: Need to re-do flow later so maybe it asks for the confirmation FIRST before showing other stuff. That way it will prevent overriding a legitimate call.
        if(confirm("Confirm logging this call as Voicemail.")) {

            $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI
            // Log as voicemail, hangup  and update status
            Y.io('/admin/dialer/gotVoicemail/' + this.getAttribute('data-id'), {
                method: 'POST',
                on: {
                    success: function(id, o) {

                        // Attempt to parse out JSON
                        try {
                            var response = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Error parsing response from server!');
                        }

                        // Handle different response types
                        switch(response.status) {
                            case 'success':
                                Message.messagePosition = 'top-center';
                                Message.create('success','Successfully logged voicemail.');
                                Y.one('#container-details').setHTML('');
                                setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);

                                break;
                            default:
                                Message.messagePosition = 'top-center';
                                Message.create('error','Voicemail log did not save. An unknown error has occurred!');
                                break;
                        }

                        // re-enables button that was disabled to prevent double clicking
                        Y.one('button#vm-mark-no-answer').setAttribute('disabled',false);
                    }
                }
            });
        }

    },'button#vm-mark-no-answer');

    // Bind click even for add to queue button. This will add the phone number on call list to the Queue to be dialed
    Y.one('#dt').delegate('click', function() {
        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                                    //@todo: this is jquery needs to be YUI
        // Add this call list phone back into the queue
        Y.io('/admin/dialer/addToQueue/' + this.getAttribute('data-id'), {
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            // if polling is not active, refresh the data table
                            if(polling==null) {
                                Y.one('#btn-load-list').simulate('click');
                            }
                            else {
                                if(Y.one('div.loading-container.loading')) {
                                    $("div.loading-container.loading").remove();                                                                                    //@todo: this is jquery needs to be YUI
                                }
                            }

                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully added to Queue.');
                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }
                }
            }
        });

    },'button.add-to-queue');

    // Bind click even for skip queue button. This will skip the phone number on call list to the Queue to be dialed
    Y.one('#dt').delegate('click', function() {

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                                            //@todo: this is jquery needs to be YUI
        // Add this call list phone back into the queue
        Y.io('/admin/dialer/skipQueue/' + this.getAttribute('data-id'), {
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            // if polling is not active, refresh the data table
                            if(polling==null) {
                                Y.one('#btn-load-list').simulate('click');
                            }
                            else {
                                $("div.loading-container.loading").remove();
                            }

                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully skipped Queue.');
                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }
                }
            }
        });
        //setTimeout(function(){ $("div.loading-container.loading").remove(); }, 100);

    },'button.skip-queue');

    // Bind click even for pause queue button. This will hangup and the phone number on call list and pause the session
    Y.one('#dt').delegate('click', function() {
        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                                        //@todo: this is jquery needs to be YUI
        // Add this call list phone back into the queue
        var callerIdNumber = Y.one('#callerIdNumber').get('value').replace(/\D/g, '');
        Y.io('/admin/dialer/hangupCallListPhone/id/' + this.getAttribute('data-id') + '/callerIdNumber/' + callerIdNumber + '/callerIdName/' + Y.one('#callerIdName').get('value') + '/num_to_call/' + Y.one('#num_to_call').get('value'), { //hangupSpecificCall ... need to retrieve call_id
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully Ended Call.');
                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }

                    $("div.loading-container.loading").remove();
                }
            }
        });

    },'button.hangup-queue');
});
</script>