<?php
$this->breadcrumbs=array(
    'Zip Codes' => '/'.$this->module->id.'/leadRouting/listZipCodes',
    'Agents' => '/'.$this->module->id.'/leadRouting/listAgentZipCodes',
    $contact->fullName => '',
);

$form=$this->beginWidget('CActiveForm', array(
        'id'=>'zip-code-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=>'Zip Codes to '.$type.' for '.$contact->fullName,
        'handleIconCss'=>'i_strategy'
    ));
?>
    <div id="action-plans-container" >
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th style="width: 45%;">Name:</th>
                    <td colspan="3">
                        <?php echo $contact->fullName; ?>
                    </td>
                </tr>
                <tr>
                    <th style="width: 45%;">Mode:</th>
                    <td colspan="3">
                        Zip Codes to <?php echo $type; ?>
                    </td>
                </tr>                <tr>
                    <th style="width: 45%;">Zip Codes:</th>
                    <td colspan="3">
                        <?php $this->widget('admin_widgets.StmMultiSelectWidget', array(
                                'model' => $leadRouteZipLu,
                                'attribute' => 'data',
                                'listData' => CHtml::listData(ZipCodes::model()->findAll(), 'id', 'zip'),
                            )
                        ); ?>
                        <?php echo $form->error($leadRouteZipLu, 'data'); ?>
                        <?php echo CHtml::hiddenField('postTrigger', 1)?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper"><button type="submit" class="submit wide"><?php echo $action; ?> Zip Code</button></div>
<?php $this->endWidget(); ?>