<?php
	$css = <<<CSS
	div#route-options > table tr th {
		width: 100px;
	}
	div#lead-container table tr>th {
		width: 100px;
	}
	div#lead-container table tr>td textarea {
		height: 100%;
	}
	div#lead-container table.container td {
		vertical-align: top;
	}

	.auto-responder-copy:first-child {
		display: none;
	}
CSS;
	Yii::app()->clientScript->registerCss('leadRouteCss', $css);

    $actionPlanAutoResponderTypeId = LeadRouteAutoresponder::TYPE_ACTION_PLAN;
    $emailAutoResponderTypeId  = LeadRouteAutoresponder::TYPE_EMAIL;
	$js = <<<JS
	toggleTypeVisibility();
	$('#LeadRoutes_type_ma').change(toggleTypeVisibility);

	/**
	 * Toggle the visibility of the type editor for a lead route
	 */
	function toggleTypeVisibility() {

		leadRouteTypeVal = $('#LeadRoutes_type_ma').val();
		if (leadRouteTypeVal == 1) {
			$('#shifts').fadeIn();
			$('#roundrobin').hide();
		} else {
			$('#shifts').hide();
			$('#roundrobin').fadeIn();
		}
	}

	$('.auto-responder-types').change(toggleAutoResponderTypes);
	$('.auto-responder-types').each(toggleAutoResponderTypes);

    /**
     * Controls what auto responder options are available based on the selects with the '.auto-responder-types' class
     */
	function toggleAutoResponderTypes() {
        var autoResponderSelectorParent = $(this).parent();
        autoResponderSelectorParent.children('select:not(.auto-responder-types)').hide().prop('disabled', true);

        switch ($(this).val()) {
            case '$actionPlanAutoResponderTypeId':
                autoResponderSelectorParent.children('select.actionPlanOptions').show().prop('disabled', false);
            break;

            case '$emailAutoResponderTypeId':
                autoResponderSelectorParent.children('select.emailOptions').show().prop('disabled', false);
            break;

            default:

            break;
        }
	}
JS;
	Yii::app()->clientScript->registerScript('leadRoutesJs', $js);

	$this->widget('admin_widgets.DialogWidget.AddShiftDialogWidget.AddShiftDialogWidget', array(
			'id' => 'add-shift',
			'title' => 'Add New Shift',
			'triggerElement' => '.add-new-shift',
		)
	);

    $this->widget('admin_exts.EChosen.EChosen', array(
        'target' => 'select.chosen',
//        'options'=>array('allow_single_deselect'=>true),
    ));
?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'lead-route-form',
			'enableAjaxValidation' => false,
		)
	); ?>

	<!-- ====== FEATURED AREA INFO =============================================================================================== -->
	<div id="top-data" class="row">
		<div class="g100 p-p0 p-m0">
			<? $this->beginStmPortletContent(array(
					'handleTitle' => 'Lead Route',
					'handleIconCss' => 'i_shuffle'
				)
			); ?>
			<div id="lead-container">
				<div class="g12 p-mb5 rounded-text-box odd-static">
					<div class="g12 p-fl">
						<table class="container">
							<tr>
                                <th>
                                    <?php echo $form->labelEx($model, 'name') ?>:
                                </th>
                                <td>
                                    <?php echo $form->textField($model, 'name', $htmlOptions = array(
                                            'placeholder' => 'Lead Group Name',
                                            'class' => 'g8'
                                        )
                                    ); ?>
                                    <?php echo $form->error($model, 'name'); ?>
                                </td>
                                <th>
                                    <?php echo $form->labelEx($model, 'overflow_action_ma') ?>:
                                </th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'overflow_action_ma', LeadRoutes::getOverflowTypesList(), $htmlOptions = array(
                                            'empty' => 'Select One',
                                            'class' => 'chosen g8',
                                        )
                                    ); ?>
                                    <?php echo $form->error($model, 'overflow_action_ma'); ?>
                                </td>
								<th>
									<?php echo $form->labelEx($model, 'description', $htmlOptions = array('class' => '')) ?>
								</th>
                                <td>
                                    <?php echo $form->textArea($model, 'description', $htmlOptions = array(
                                            'placeholder' => 'Description',
                                            'class' => 'g12',
                                            'rows' => '3',
//                                            'style' => 'width:200px;'
                                        )
                                    ); ?>
                                    <?php echo $form->error($model, 'description'); ?>
                                </td>
							</tr>
							<tr>
                                <th>
                                    <?php echo $form->labelEx($model, 'type_ma') ?>:
                                </th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'type_ma', LeadRoutes::getGroupTypesList(), $htmlOptions = array('empty' => 'Select One', 'class' => 'chosen g9', 'style'=>'width: 90%;')); ?>
                                    <?php echo $form->error($model, 'type_ma'); ?>
                                </td>
								<th>
									<?php echo $form->labelEx($model, 'default_contact_id') ?>:
								</th>
								<td>
									<?php echo $form->dropDownList($model, 'default_contact_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->default_contact_id)->findAll(), 'id', 'fullName', 'primaryContactType.name'),
										$htmlOptions = array(
											'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
                                            'class' => 'chosen g8',
										)
									); ?>
									<?php echo $form->error($model, 'default_contact_id'); ?>
								</td>
                                <th>
                                    <?php echo $form->labelEx($model, 'apply_task_overdue_limit') ?>:
                                </th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'apply_task_overdue_limit', StmFormHelper::getYesNoList(),
                                        $htmlOptions = array(
                                            'class' => 'g3',
                                        )
                                    ); ?>
                                </td>
							</tr>
                            <tr>
                                <th>Triggered Routes:</th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'triggeredRouteId', CHtml::listData(LeadRoutes::model()->findAllByAttributes(array('context_ma'=>LeadRoutes::CONTEXT_LENDER_ID)), 'id', 'name'),
                                        $htmlOptions = array(
                                            'empty' => 'No Triggered Routes',
                                            'class' => 'chosen chzn-select  g8',
                                        )
                                    ); ?>

                                </td>
                                <th>
                                    <?php echo $form->labelEx($model, 'send_sms') ?>:<br>(Before Lead Connect Call)
                                </th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'send_sms', StmFormHelper::getYesNoList(),
                                        $htmlOptions = array(
                                            'class' => 'g3',
                                        )
                                    ); ?>
                                </td>                                <th>
                                    <?php echo $form->labelEx($model, 'cc_contact_ids') ?>:
                                </th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'cc_contact_ids', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->default_contact_id)->findAll(), 'id', 'fullName'),
                                        $htmlOptions = array(
                                            'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
//                                            'multiple' => 'multiple',
                                            'class' => 'chosen chzn-select g12',
                                            'multiple' => 'multiple',
                                            'data-placeholder' => 'Select Contacts to Email CC'
                                        )
                                    ); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Enable Lead Connect Calls:</th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'make_calls', StmFormHelper::getYesNoList(),
                                        $htmlOptions = array(
                                            'class' => 'g3',
                                        )
                                    ); ?>
                                    <div class="stm-tool-tip question">
                                        <span>If enabled, the current assignee (determined by Shift or Round Robin) will be called immediately upon the arrival of a new lead to connect to a phone call with the new lead. </span>
                                    </div>
                                    <span class="label">(If Yes, recommend "No" for Send SMS Before Lead Connect Call)</span>
                                </td>
                                <th>Call Hunt Group:</th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'call_hunt_group_id', CHtml::listData(CallHuntGroups::model()->findAll(), 'id', 'name'),
                                        $htmlOptions = array(
                                            'empty' => 'No Triggered Routes',
                                            'class' => 'g12',
                                        )
                                    ); ?>
                                </td>
                                <th>
                                    Is Cascading Round Robin:

                                </th>
                                <td>
                                    <?php echo $form->dropDownList($model, 'is_cascading', StmFormHelper::getYesNoList(),
                                        $htmlOptions = array(
                                            'class' => 'g3',
                                        )
                                    ); ?>
                                    <div class="stm-tool-tip question">
                                        <span>If Lead Connect Calls are enabled, and the current assignee does not pick up, the next person on the list in your Round Robin will be called. The process will continue until somebody picks up. </span>
                                    </div>
                                    <span class="label">(Only Applicable for Round Robin)</span>
                                </td>
                            </tr>
							<tr>
								<th>Auto-responders:
								</th>
								<td colspan="5">
                                    <span class="label">NOTE: Make sure the Action Plan does not have an assigned to of "Current User". This will prevent the Action Plan from applying.</span>
									<div id="auto-responder-container" class="g100">
										<?php $this->widget('admin_exts.jqrelcopy.JQRelcopy',array(
											'id' => 'auto-responder-copy',
											'removeText' => '<em class="icon i_stm_delete"></em> Remove Auto Responder',
											'removeHtmlOptions' => array(
												'class' => 'text-button'
											),
											'options' => array(
												'copyClass'=>'auto-responder-new-copy',
												'limit'=>6, // should be equal to what you intend + 1, due to the initial element being hidden
												'afterClone'=>'$(this).each(function() {
													rowIndex = (typeof rowIndex === "undefined") ? $(".auto-responder-copy").length : rowIndex + 1;
													$(this).children().each(function() {
														$(this).prop("disabled", false);
														if ($(this).is("select")) {
															var name = $(this).attr("name");
															var nameWithId = name.replace("[]", "["+rowIndex+"]");
															$(this).attr("name", nameWithId);
														}
													});
												});',
											),
										)); ?>
										<div class="row auto-responder-copy">
											<?php $this->renderPartial('_autoResponderEntry', array(
												'autoResponder' => new LeadRouteAutoresponder,
												'form' => $form,
                                                'route' => $model,
												'rowId' => null,
												'htmlOptions' => array(
													'disabled' => 'disabled',
												),
											)); ?>
										</div>
										<?php
											if ($model->autoResponders):
												$rowCount = 0;
												foreach ($model->autoResponders as $autoResponder):
										?>
												<div class="auto-responder-copy auto-responder-new-copy">
													<?php
														$this->renderPartial('_autoResponderEntry', array(
															'autoResponder' => $autoResponder,
                                                            'route' => $model,
															'form' => $form,
															'rowId' => ++$rowCount,
															'htmlOptions' => array(),
														));

														echo JQRelcopy::getRemoveLink('<em class="icon i_stm_delete"></em> Remove Auto Responder', array(
															'class' => 'text-button',
														));
													?>
												</div>
										<?php
												endforeach;
											endif;
										?>
										<button type="button" class="text p-m0" rel=".auto-responder-copy" id="auto-responder-copy"><em class="icon i_stm_add"></em>Add Auto Responder</button>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div id="route-options" class="g12 p-fl">
						<table class="container">
							<tr id="roundrobin">
								<th class="narrow">Contacts:</th>
								<td>
									<?php
                                    $this->widget('admin_widgets.StmMultiSelectWidget', array(
											'model' => $model,
                                            'jQuerySelector'=> '#LeadRoutes_contactsToAdd',
											'attribute' => 'contactsToAdd',
											'listData' => CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->contactsToAdd)->findAll(), 'id', 'fullName'), //$userList
											//update to Users Only
										)
									); ?>
									<?php echo $form->error($model, 'contactsToAdd'); ?>
								</td>
							</tr>
							<tr id="shifts" style="display: none">
								<td valign="top" colspan="2">
									<div id="calendar-wrapper">
										<?php $this->renderPartial('_formRouteCalendar'); ?>
									</div>
								</td>
							</tr>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>
	<!-- ====== END SECTIONS =============================================================================================== -->
	<div id="submit-button-wrapper">
		<button type="submit" class="submit wide">Save Route</button>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->
<?php $this->endStmPortletContent(); ?>
