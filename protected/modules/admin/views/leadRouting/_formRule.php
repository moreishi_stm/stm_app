<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'featured-areas-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<!-- ====== FEATURED AREA INFO =============================================================================================== -->
	<div id="top-data" class="row">
		<div class="g100 p-p0 p-m0">
			<? $this->beginStmPortletContent(array(
				'handleTitle'=>'Lead Route Rule',
				'handleIconCss'=>'i_shuffle'
			)); ?>
			<div id="lead-rule-container">
				<div class="g12 p-mb5 rounded-text-box odd-static">
					<div class="g9 p-fl">
			            <table class="container">
			            	<tr>
			            		<th>
			            			<?php echo $form->labelEx($model, 'name')?>:
			            		</th>
			            		<td>
        							<?php echo $form->textField($model, 'name', $htmlOptions=array('placeholder'=>'Lead Group Name', 'class'=>'g10')); ?>
        							<?php echo $form->error($model,'name'); ?>
			            		</td>
			            	</tr>
			            	<tr>
			            		<th>
			            			<?php echo $form->labelEx($model, 'field_ma')?>:
			            		</th>
			            		<td>
						    		<?php echo $form->dropDownList($model, 'field_ma', LeadRouteRules::getFieldsList(), $htmlOptions=array('empty'=>'Select One')); ?>
        							<?php echo $form->error($model,'field_ma'); ?>
			            		</td>
			            	</tr>
			            	<tr>
			            		<th>
			            			<?php echo $form->labelEx($model, 'operator_ma')?>:
			            		</th>
			            		<td>
						    		<?php echo $form->dropDownList($model, 'operator_ma', LeadRouteRules::getOperatorsList(), $htmlOptions=array('empty'=>'Select One')); ?>
        							<?php echo $form->error($model,'operator_ma'); ?>
			            		</td>
			            	</tr>			            	<tr>
			            		<th>
			            			<?php echo $form->labelEx($model, 'value')?>:
			            		</th>
			            		<td>
        							<?php echo $form->textField($model, 'value', $htmlOptions=array('placeholder'=>'Value', 'class'=>'g10')); ?>
        							<?php echo $form->error($model,'value'); ?>
			            		</td>
			            	</tr>
			            </table>
			        </div>
				</div>
			</div>
		</div>
	</div>
<!-- ====== END SECTIONS =============================================================================================== -->
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Contact</button></div>

	<?php $this->endWidget(); ?>

</div><!-- form -->
<?php $this->endStmPortletContent(); ?>
