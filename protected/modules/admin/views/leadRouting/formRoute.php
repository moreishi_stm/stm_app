<?php
$this->breadcrumbs=array(
	$model->name=>array('/'.$this->module->id.'/leadRouting/index'),
    'Edit'=>'#',
);
?>
<div id="featured-areas-header">
	<h1 class="name"><?php echo $model->name;?></h1>
</div>
<?php echo $this->renderPartial('_formRoute', array('model'=>$model)); ?>