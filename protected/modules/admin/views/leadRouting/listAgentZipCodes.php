<?php
$this->breadcrumbs=array(
    'Zip Codes' => '/'.$this->module->id.'/leadRouting/listZipCodes',
    'Agents' => '',
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('zip-code-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="listview-actions">
    <a href="/<?php echo $this->module->name;?>/leadRouting/addZipCodes" class="button gray icon i_stm_add">Add Zip Code</a>
    <a href="/<?php echo $this->module->name;?>/leadRouting/listAgentZipCodes" class="button gray icon i_stm_search">Agent Zip Codes</a>
</div>
<div id="lead-routing-header">
	<h1>Lead Routes</h1>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'zip-code-grid',
	'dataProvider'=>$dataProvider,
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'fullName',
        array(
            'type'=>'raw',
            'name'=>'Included Zip Codes',
            'value'=>'Yii::app()->controller->printZips($data->id, "Include")',
        ),
        array(
            'type'=>'raw',
            'name'=>'Excluded Zip Codes',
            'value'=>'Yii::app()->controller->printZips($data->id, "Exclude")',
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/leadRouting/includeAgentZipCodes/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Include Zip Codes</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:160px'),
        ),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/leadRouting/excludeAgentZipCodes/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Exclude Zip Codes</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:160px'),
		),
	),
));