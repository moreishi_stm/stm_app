<?php
$this->breadcrumbs=array(
    'Zip Codes'=>'',
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('zip-code-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="listview-actions">
    <a href="/<?php echo $this->module->name;?>/leadRouting/addZipCodes" class="button gray icon i_stm_add">Add Zip Code</a>
    <a href="/<?php echo $this->module->name;?>/leadRouting/listAgentZipCodes" class="button gray icon i_stm_search">Agent Zip Codes</a>
</div>
<div id="lead-routing-header">
	<h1>Lead Routes</h1>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'zip-code-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'zip',
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/leadRouting/editZipCodes/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:80px'),
		),
	),
));