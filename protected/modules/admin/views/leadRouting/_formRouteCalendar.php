<?php
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl.DS.'calendar.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl.DS.'wl_Calendar.js');
Yii::app()->clientScript->registerCssFile($this->module->cssAssetsUrl.DS.'jquery.fullcalendar.css');
$this->widget('admin_module.extensions.tipsy.Tipsy', array(
	'gravity' => 'w',
	'items'   => array(
		array(
			'id'       => '.slot-numbers-template',
			'fallback' => 'Click to add a shift to this day.'
		),
	),
));

$css = <<<CSS
	div#calendar-wrapper {
		padding: 15px;
		margin: 5px;
		border: 1px solid #CCC;
		border-radius: 5px;
		background: #fcfff4;
	}
	div#calendar-wrapper a.button {
		background-position: 10px 8px;
		padding-right: 1px;
	}
	/** Make sure the tds in the calendar are evenly spaced **/
	div#calendar-wrapper tbody tr td {
		width: 14.3%;
	}
	td.ui-state-highlight {
		border: 1px solid #AAA;
	}
	div.fc-event {
		clear: both;
	}
	span.fc-event-title {
		font-size: 9px;
		padding-top: 4px;
		display: block; /** Allows the full name to go on a new line **/
	}
	div.slot-numbers-template {
		font-size: 9px;
		padding: 2px 0 0 2px;
		float: left;
	}
CSS;
Yii::app()->clientScript->registerCss('calendarCss', $css);

$calendarEventUrl = $this->createUrl('/admin/shift/get/'.Yii::app()->request->getQuery('id'));
$js = <<<JS
	$('div.calendar').wl_Calendar({
		eventClick: function(event, jsEvent, view) {
			callUpdateShiftDialog(event);
		},

		eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {

		},

		eventAfterRender: function(event, element, view) {

		},

		events: {
			url: '$calendarEventUrl'
		},

		timeFormat: 'h:mmtt{ - h:mmtt}'

		// Uncommenting this when we have the ability to drag and edit shifts
		// editable: true
	});


	$('div.calendar .fc-view tbody tr td').each(function() {
		contentDiv = $(this).find('div:first');
		$('div#slot-numbers-template:hidden').clone().prependTo(contentDiv);

		slotNumberContent = contentDiv.find('div#slot-numbers-template');
		slotNumberContent.attr({
			'class' : 'slot-numbers-template'
		}).removeAttr('id style');
		slotNumberContent.show();
	});

	$('.calendar').ready(function() {

		var calendar = $(this);

		var addNewShiftBtn = calendar.find('.fc-button-today').clone();

		/** Create and inject the "Add New Shift" button **/
		addNewShiftBtn.find('.fc-button-content').text('Add New Shift');
		addNewShiftBtn.removeClass('ui-state-disabled fc-button-today').addClass('ui-state-default add-new-shift');
		addNewShiftBtn.hover(function() {
			$(this).addClass('ui-state-hover');
		}, function() {
			$(this).removeClass('ui-state-hover');
		});

		/** Inject the "Add New Shift" button **/
		calendar.find('td.fc-header-right').prepend('<span class="fc-header-space"></span>');
		calendar.find('td.fc-header-right').prepend(addNewShiftBtn);
	});

	// After everything is redrawn, then re-init tipsy to show the hover overs
	$('div.calendar').fullCalendar('rerenderEvents');
	initTipsy();
JS;

Yii::app()->clientScript->registerScript('calendarJs', $js);
?>
<div id="slot-numbers-template" style="display: none">
	<a href="#" class="button gray icon i_stm_add add-new-shift"></a>
</div>
<div class="calendar"></div>
