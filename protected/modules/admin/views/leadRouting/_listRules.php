<?
Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('featured-area-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="lead-routing-rules-header">
	<h1>Lead Routing Rules</h1>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'featured-area-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		array(
			'name'=>'Lead Route Name',
			'value'=>'$data->leadRoute->name',
		),
		'name',
		array(
			'name'=>'Field',
			'value'=>'$data->field',
		),
		array(
			'name'=>'Operator',
			'value'=>'$data->operator',
		),
		'value',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'buttons'=>array(
			    'view'=>array(
					'url' =>'Yii::app()->controller->createUrl("leadRouting/rules/".$data->id)',
	            	'imageUrl'=>$this->getModule()->getCdnImageAssetsUrl().'search.png',
			    ),
            ),
		),
	),
));