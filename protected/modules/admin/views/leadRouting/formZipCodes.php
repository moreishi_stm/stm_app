<?php
$action = ($this->action->id == 'add') ? 'Add' : 'Edit';
$this->breadcrumbs=array(
    'Zip Codes' => '/'.$this->module->id.'/leadRouting/listZipCodes',
    $action => '',
);

$form=$this->beginWidget('CActiveForm', array(
        'id'=>'zip-code-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=>'Zip Code',
        'handleIconCss'=>'i_strategy'
    ));
?>
    <div id="action-plans-container" >
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th style="width: 45%;">Name:</th>
                    <td colspan="3">
                        <?php echo $form->textField($model,'zip', $htmlOptions=array('placeholder'=>'Zip Code', 'class'=>'g3', ));?>
                        <?php echo $form->error($model,'zip'); ?>
                    </td>
                </tr>
            </table>

        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper"><button type="submit" class="submit wide"><?php echo $action; ?> Zip Code</button></div>
<?php $this->endWidget(); ?>