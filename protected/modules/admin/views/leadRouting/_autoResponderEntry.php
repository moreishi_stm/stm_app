<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 10/29/13
 * @param LeadRouteAutoresponder $autoResponder
 * @param CActiveForm $form
 * @param array $htmlOptions
 * @param int $rowId
 */
echo $form->dropDownList($autoResponder, "[$rowId]type_ma", LeadRouteAutoresponder::getTypes(), CMap::mergeArray($htmlOptions, array(
	'empty' => '',
	'style' => 'width: 10%;',
    'class' => 'auto-responder-types',
)));

$emailTemplateListData = CHtml::listData(EmailTemplates::model()->findAllByAttributes(array('component_type_id'=>$route->component_type_id)), 'id', 'subject');
echo $form->dropDownList($autoResponder, "[$rowId]component_id", $emailTemplateListData, CMap::mergeArray($htmlOptions, array(
	'empty' => '',
	'style' => 'width: 25%;',
    'class' => 'emailOptions',
)));

$actionPlanListData = CHtml::listData(ActionPlans::model()->orderByName()->byStatus(1)->findAllByAttributes(array(
    'component_type_id' => $route->component_type_id,
)), 'id', 'name');
echo $form->dropDownList($autoResponder, "[$rowId]component_id", $actionPlanListData, CMap::mergeArray($htmlOptions, array(
    'empty' => '',
    'style' => 'width: 25%;',
    'class' => 'actionPlanOptions',
)));

if (!$autoResponder->getIsNewRecord()) {
	echo $form->hiddenField($autoResponder, "[$rowId]id");
}
