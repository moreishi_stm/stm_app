<?
Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('featured-area-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="listview-actions">
    <a href="/<?php echo $this->module->name;?>/leadRouting/listZipCodes" class="button gray icon i_stm_search">Zip Codes</a>
    <a href="/<?php echo $this->module->name;?>/leadRouting/listAgentZipCodes" class="button gray icon i_stm_search">Agent Zip Codes</a>
</div>
<div id="lead-routing-header">
	<h1>Lead Routes</h1>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'featured-area-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		array(
			'name'=>'Type',
			'value'=>'$data->groupType',
		),
		'name',
		'description',
        array(
            'type'=>'raw',
            'name'=>'Auto Lead Connect Calls',
            'value'=>'StmFormHelper::getYesNoName($data->make_calls)',
            'htmlOptions'=>array('style'=>'width:100px'),
            'visible' => (Yii::app()->user->checkAccess('callHuntGroups')) ? true : false,
        ),
        array(
            'type'=>'raw',
            'name'=>'Lead Connect Call Hunt Group',
            'value'=>'$data->callHuntGroup->name',
            'visible' => (Yii::app()->user->checkAccess('callHuntGroups')) ? true : false,
        ),
		array(
			'type'=>'raw',
			'name'=>'Route Options',
			'value'=>'$data->populateLeadRouteOptionsHumanReadable()'
		),
        array(
            'type'=>'raw',
            'name'=>'Send SMS Before Call',
            'value'=>'StmFormHelper::getYesNoName($data->send_sms)',
            'htmlOptions'=>array('style'=>'width:100px'),
        ),
		array(
			'name'=>'People',
			'value'=>'Lead::model()->byLeadRouteId($data->id)->count()',
		),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/leadRouting/routes/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:80px'),
		),
	),
));