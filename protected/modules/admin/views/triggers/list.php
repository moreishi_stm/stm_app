<?php
	$this->breadcrumbs = array(
		'List' => '',
	);
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('search', <<<JS
    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('triggers-grid', {
            data: $(this).serialize()
        });
    	return false;
    });

    $( '.delete-trigger-button' ).live( "click", function() {
        if(confirm('Are you sure you want to delete this Automatic Trigger?')) {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            var id = $(this).data('id');
            $.post('/$module/triggers/delete/'+id, function(data) {
                $("div.loading-container.loading").remove();
                if(data.status=='success') {
                    Message.create("success","Trigger deleted successfully.");
                    $.fn.yiiGridView.update("triggers-grid", { data: $(this).serialize() });
                } else {
                    Message.create("error","Error: Trigger did not delete.");
                }
            },"json");
        }
        return false;
    });
JS
);
?>
<style type="text/css">
	#recruit-grid strong.disc {
		width: 25px;
		display: inline-block;
	}

	#recruit-grid div.disc-data, #recruit-grid div.ava-data {
		display: inline-block;
	}

	#recruit-grid .disc-data span {
		width: 35px;
		font-weight: normal;
		display: inline-block;
	}

	#recruit-grid .disc-data i {
		font-weight: normal;
		display: inline-block;
		float: left;
	}
</style>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/triggers/add" class="button gray icon i_stm_add">Add New Trigger</a>
</div>
<div id="content-header">
	<h1>Triggers List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?$form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'id' => 'triggers-list-search',
    )
); ?>
    <div class="g3">
        <label class="g3">Status:</label>
        <span class="g9">
            <?php echo $form->dropDownList($model, 'status_ma', StmFormHelper::getStatusBooleanList(true)); ?>
        </span>
    </div>
    <div class="g3">
        <label class="g3">Type:</label>
        <span class="g9">
            <?php echo $form->dropDownList($model, 'component_type_id', array(ComponentTypes::SELLERS => 'Sellers', ComponentTypes::BUYERS => 'Buyers')); ?>
        </span>
    </div>

    <div class="g3">
        <label class="g3">Description:</label>
        <span class="g9">
            <?php echo $form->textField($model, 'description', $htmlOptions=array('placeholder'=>'Description')); ?>
        </span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?></div>

<?php $this->endWidget(); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'triggers-grid',
		'dataProvider' => $model->search(),
        'template' => '{pager}{summary}{items}{summary}{pager}',
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Status',
                'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Type',
                'value' => '$data->componentType->singularName',
            ),
			array(
				'type' => 'raw',
				'name' => 'Event',
				'value' => '$data->event',
			),
            array(
                'type' => 'raw',
                'name' => 'Event Data',
                'value' => '$data->getEventDataLabel($data->event)',
            ),
            array(
                'type' => 'raw',
                'name' => 'Action',
                'value' => '$data->action',
            ),
            array(
                'type' => 'raw',
                'name' => 'Action Plan Name',
                'value' => '$data->actionPlan->name',
            ),
            array(
                'type' => 'raw',
                'name' => 'Description',
                'value' => '$data->description',
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->id."/triggers/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->id."/triggers/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
				'htmlOptions' => array('style' => 'width:80px'),
			),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->id."/triggers/delete/".$data->id."\" class=\"button gray icon delete-trigger-button i_stm_delete grey-button\" data-id=\"".$data->id."\" >Delete</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
		),
	)
);
?>
