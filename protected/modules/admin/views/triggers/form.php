<?php $this->breadcrumbs = array(
    (($this->action->id == 'edit')?'Edit':'Add') => '/'.Yii::app()->controller->module->id.'/triggers/'.$model->id,
);
?>

<h1><?=($this->action->id == 'edit')?'Edit':'Add New'?> Trigger</h1>

<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => ' trigger-form',
        'enableAjaxValidation' => false,
    ));
    $this->beginStmPortletContent(array(
        'handleTitle' => 'Trigger Details',
        'handleIconCss' => 'i_wizard'
    ));
    ?>
    <div>
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'event'); ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($model, 'event', $model->eventsList, $htmlOptions = array('class' => 'g3','empty'=>'')); ?>
                        <span id="event-data-container"><?php echo $form->dropDownList($model, 'event_data', $model->getEventDataList((($model->event) ? $model->event : current($model->eventsList))), $htmlOptions = array('class' => 'g5','empty'=>'')); ?></span>
                        <?php echo $form->error($model, 'event'); ?>
                        <?php echo $form->error($model, 'event_data'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'action'); ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($model, 'action', $model->actionsList, $htmlOptions = array('class' => 'g3','empty'=>'')); ?>
                        <span id="action-plan-container"><?php echo $form->dropDownList($model, 'action_plan_id', $currentActionPlanList, $htmlOptions = array('class' => 'g5','empty'=>'')); ?></span>
                        <?php echo $form->error($model, 'action_plan_id'); ?>
                        <?php echo $form->error($model, 'action'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow">Description:</th>
                    <td>
                        <?php echo $form->textField($model, 'description', $htmlOptions=array('placeholder'=>'Description','class' => 'g8')); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper">
        <button type="submit" class="submit wide">Submit Trigger</button>
    </div>
<?php $this->endWidget(); ?>

<? $yuiScript = 'http://yui.yahooapis.com/3.17.2/build/yui/yui-min.js'; ?>
<script src="<?=$yuiScript?>"></script>
<script type="text/javascript">
    // Use YUI things
    YUI().use('node', 'json', 'handlebars','node-event-simulate', function(Y) {

        Y.one('#Triggers_event').on('change', function(e) {
            if(e.currentTarget.get('value') == '<?=Triggers::EVENT_SELLER_STATUS_UPDATE?>') {
                Y.one('#action-plan-container').setHTML(Y.Handlebars.render(Y.one('#template-seller-action-plans').getHTML()));
                Y.one('#event-data-container').setHTML(Y.Handlebars.render(Y.one('#template-seller-lead-status').getHTML()));
            }
            else if(e.currentTarget.get('value') == '<?=Triggers::EVENT_BUYER_STATUS_UPDATE?>') {
                Y.one('#action-plan-container').setHTML(Y.Handlebars.render(Y.one('#template-buyer-action-plans').getHTML()));
                Y.one('#event-data-container').setHTML(Y.Handlebars.render(Y.one('#template-buyer-lead-status').getHTML()));
            }
            else if(e.currentTarget.get('value') == '<?=Triggers::EVENT_BUYER_APPOINTMENT_MET_STATUS?>') {
                Y.one('#action-plan-container').setHTML(Y.Handlebars.render(Y.one('#template-buyer-action-plans').getHTML()));
                Y.one('#event-data-container').setHTML(Y.Handlebars.render(Y.one('#template-appointment-status').getHTML()));
            }
            else if(e.currentTarget.get('value') == '<?=Triggers::EVENT_SELLER_APPOINTMENT_MET_STATUS?>') {
                Y.one('#action-plan-container').setHTML(Y.Handlebars.render(Y.one('#template-seller-action-plans').getHTML()));
                Y.one('#event-data-container').setHTML(Y.Handlebars.render(Y.one('#template-appointment-status').getHTML()));
            }
            else if(e.currentTarget.get('value') == '<?=Triggers::EVENT_CLOSING_STATUS;?>') {
                Y.one('#action-plan-container').setHTML(Y.Handlebars.render(Y.one('#template-closing-action-plans').getHTML()));
                Y.one('#event-data-container').setHTML(Y.Handlebars.render(Y.one('#template-closing-status').getHTML()));
            }
        });
    });
</script>

<script id="template-seller-action-plans" type="text/x-handlebars-template">
    <?php echo $form->dropDownList($model, 'action_plan_id', CHtml::listData(ActionPlans::model()->findAllByAttributes(array('component_type_id'=>ComponentTypes::SELLERS)),'id','name'), $htmlOptions = array('class' => 'g5','empty'=>'Select a Seller Action Plan'));?>
</script>

<script id="template-buyer-action-plans" type="text/x-handlebars-template">
    <?php echo $form->dropDownList($model, 'action_plan_id', CHtml::listData(ActionPlans::model()->findAllByAttributes(array('component_type_id'=>ComponentTypes::BUYERS)),'id','name'), $htmlOptions = array('class' => 'g5','empty'=>'Select a Buyer Action Plan'));?>
</script>

<script id="template-closing-action-plans" type="text/x-handlebars-template">
    <?php echo $form->dropDownList($model, 'action_plan_id', CHtml::listData(ActionPlans::model()->findAllByAttributes(array('component_type_id'=>ComponentTypes::CONTACTS)),'id','name'), $htmlOptions = array('class' => 'g5','empty'=>'Select a Closing Action Plan'));?>
</script>

<script id="template-buyer-lead-status" type="text/x-handlebars-template">
    <?php echo $form->dropDownList($model, 'event_data', $model->getEventDataList(Triggers::EVENT_SELLER_STATUS_UPDATE), $htmlOptions = array('class' => 'g5','empty'=>'Select a Buyer Action Plan'));?>
</script>

<script id="template-seller-lead-status" type="text/x-handlebars-template">
    <?php echo $form->dropDownList($model, 'event_data', $model->getEventDataList(Triggers::EVENT_BUYER_STATUS_UPDATE), $htmlOptions = array('class' => 'g5','empty'=>'Select an Appointment Status'));?>
</script>

<script id="template-appointment-status" type="text/x-handlebars-template">
    <?php echo $form->dropDownList($model, 'event_data', $model->getEventDataList(Triggers::EVENT_SELLER_APPOINTMENT_MET_STATUS), $htmlOptions = array('class' => 'g5','empty'=>'Select an Appointment Status'));?>
</script>

<script id="template-closing-status" type="text/x-handlebars-template">
    <?php echo $form->dropDownList($model, 'event_data', $model->getEventDataList(Triggers::EVENT_CLOSING_STATUS), $htmlOptions = array('class' => 'g5','empty'=>'Select a Closing Status'));?>
</script>