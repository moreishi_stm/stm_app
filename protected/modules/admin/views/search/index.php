<style type="text/css">
    #search-form {
        max-width: 450px;
        margin: 0 auto;
        text-align: center;
    }


    #search-form button {
        display: inline;
    }

    #search-form input {
        width: auto;
    }

    #results-container {
        margin: 0 auto;
        max-width: 283px;
    }

    #search-results {
        position: absolute;
    }

    #search-results article {
        padding: 5px;
        color: black;
    }

    #search-results article h2 {
        text-align: left;
        color: black;
    }

    #search-results article h2 a {
        color: black;
    }

    .result-buyers {
        background-color: #ffff66;
    }

    .result-sellers {
        background-color: #a3d977;
    }

    .result-recruits {
        background-color: #99d2f2;
    }

    .result-closings {
        background-color: #ef8d22;
    }

    .result-contacts {
        background-color: white;
    }

</style>

<h1>Search</h1>

<div class="stm-tool-tip question">
    <div>
        Hey! This is a tooltip! <a href="#" target="_blank">Test Link</a>
    </div>
</div>

<section id="autocomplete-search">
    <div id="search-form">
        <form method="POST" action="">
            <input type="text" name="query" id="query" placeholder="Enter Search Terms">
            <button type="submit" id="search-submit">Search</button>
        </form>
    </div>
    <div id="results-container" style="position: relative;">
        <div id="search-results" style="display: none; position: absolute; top: 0px; left: 0px; z-index: 1000;"></div>
    </div>
</section>

<!-- Handlebars Templates -->
<script id="template-result" type="text/x-handlebars-template">
    {{#each results}}
    <article class="result-{{component_type}}">
        <h2><a href="/admin/{{component_type}}/{{link_id}}">{{last_name}}, {{first_name}}</a> [{{component_type}}]</h2>
        {{#if has_email}}{{email}}<br>{{/if}}
        {{#if has_phone}}{{phone}}<br>{{/if}}
        {{#if has_address}}
        {{address}}<br>
        {{city}}, {{state_short}} {{zip}}
        {{/if}}
    </article>
    {{/each}}
</script>

<!-- JavaScript -->
<? Yii::app()->clientScript->registerScript('searchFunctionality', <<<JS
    (function(){

        // Bind search button functionality
        $('#search-form form').submit(function(e) {

            // Prevent default event facade
            e.preventDefault();

            // Make AJAX call for search results
            $.post('/admin/search/results', {
                query: $('#query').val()
            }, function(results) {

                // Parse template with results
                var template = Handlebars.compile($('#template-result').html());
                var html = template({
                    results: results
                });

                // Populate results and slide down
                $('#search-results').html(html);
                $('#search-results').show();
            });
        });

        // Hide the search container when we click elsewhere
        $('html').click(function() {
           $('#search-results').hide();
        });

        $('#autocomplete-search').click(function(e) {
            e.stopPropagation();
        });

        $('#query').focus(function(e){
            $('#search-results').show();
        });
    }());
JS
);