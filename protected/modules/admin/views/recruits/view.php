<?php
	$this->breadcrumbs = array(
		$model->contact->fullName,
	);

	$this->title = $model->contact->fullName;

?>
<style type="text/css">
	#recruit-portlet div.disc-data {
		display: inline-block;
	}

	#recruit-portlet .disc-data span {
		width: 35px;
		font-weight: normal;
		display: inline-block;
	}

	#recruit-portlet .disc-data i {
		font-weight: normal;
		display: inline-block;
		float: left;
	}
</style>

<div id="contact-header">
	<div class="submit-login-summary">
		<div class="p-fr p-rel">
			<span class="grey-textbox"><?php echo Yii::app()->format->formatDateDays($model->contact->last_login, array(
						'break' => false,
						'default' => 'n/a'
					)
				); ?></span><em class="bubble">Last Login</em>
		</div>
		<div class="p-fr p-rel">
			<span class="grey-textbox"><?php echo $model->contact->addedDateTime ?></span><em class="bubble">Submit Date</em>
		</div>
	</div>
</div>

<div class="btn-bar">
	<a href="#contacts">
		Info
	</a>
	<a href="#recruits">
		Details
	</a>
	<a href="#activity-details">
		Activity Details
	</a>
	<a href="#activity-log-portlet">
		Activity Log
	</a>
    <button class="text add-task-button" type="button" data="<?=$model->id?>" ctid="<?=$model->componentType->id?>"><em class="icon i_stm_add"></em>Add Task</button>
    <button class="text add-activity-log-button" type="button" data="<?=$model->id?>" ctid="<?=$model->componentType->id?>"><em class="icon i_stm_add"></em>Add Activity</button>
</div>

<div id="top-data" class="res-section res-group p-0">
	<div class="res-col span_4_of_12 p-0 p-fl">
		<?php
			$this->widget('admin_widgets.ContactPortlet.ContactPortlet', array(
					'model' => $model->contact,
					'view' => 'mini',
                    'originComponentTypeId' => $model->componentType->id,
                    'originComponentId' => $model->id,
					'handleTitle' => 'Contact Info',
					'handleButtons' => array(
						array(
							'label' => 'Edit',
							'iconCssClass' => 'i_stm_edit',
							'htmlOptions' => array('id' => 'edit-contact-button'),
							'name' => 'edit-contact',
							'type' => 'link',
							'link' => '/'.Yii::app()->controller->module->id.'/contacts/edit/' . $model->contact->id,
						)
					),
				)
			);
		?>
	</div>
	<div class="res-col span_8_of_12 p-0 p-fr">
		<?php
			$this->beginStmPortletContent(array(
					'handleTitle' => 'Recruit Details',
					'handleIconCss' => 'i_wizard',
					'id' => 'recruits',
					'handleButtons' => array(
						array(
							'label' => 'Edit',
							'iconCssClass' => 'i_stm_edit',
							'htmlOptions' => array('id' => 'edit-contact-button'),
							'name' => 'edit-contact',
							'type' => 'link',
							'link' => '/'.Yii::app()->controller->module->id.'/recruits/edit/' . $model->id,
						)
					),

				)
			);
		?>
		<div>
			<div class="rounded-text-box odd-static">
				<div class="g6">
					<table id="recruit-portlet" class="container">
						<tbody>
						<tr>
							<th>Name:</th>
							<td><?php echo $model->contact->fullName; ?></td>
						</tr>
						<tr>
							<th>Status:</th>
							<td>
								<?php echo $model->statusName; ?>
							</td>
						</tr>
						<tr>
							<th>Type:</th>
							<td>
								<?php echo Yii::app()->format->formatCommaDelimited($model->types, "name"); ?>
							</td>
						</tr>
						<tr>
							<th>Licensed:</th>
							<td>
                                <?php echo StmFormHelper::getYesNoName($model->is_licensed); ?>
                                <?php echo ($model->licensed_date) ? ' - '.Yii::app()->format->formatDate($model->licensed_date) : '' ; ?>
                                <?php echo ($model->license_exam_approval_date) ? ', Exam Eligibility Date: '.Yii::app()->format->formatDate($model->license_exam_approval_date) : '' ; ?>
                            </td>
						</tr>
                        <tr>
							<th>Application:</th>
							<td><?php echo StmFormHelper::getYesNoName($model->application_status_ma); ?></td>
						</tr>
						<tr>
							<th>Met:</th>
							<td><?php echo StmFormHelper::getYesNoName($model->met_status_ma); ?></td>
						</tr>
						<tr>
							<th>DISC Profile:</th>
							<td>
								<?php echo
									'<span style="width:30px;display:inline-block;">' . $model->getDISCProfile() . '</span> <i>' . $model->printDISCDetails($opts = array('parentheses' => true)) . '</i>'; ?>
                                <?php echo ($model->disc_date) ? ' - '.Yii::app()->format->formatDate($model->disc_date) : '' ; ?>
							</td>
						</tr>
						<tr>
							<th>AVA Profile:</th>
							<td>
								<?php echo $model->printAVAProfile(); ?>
                                <?php echo ($model->ava_date) ? ' - '.Yii::app()->format->formatDate($model->ava_date) : '' ; ?>
							</td>
						</tr>
                        <? if($Appointment):?>
                        <tr>
                            <th colspan="2" style="text-align: center;"><br><u>APPOINTMENT</u></th>
                        </tr>
                        <tr>
                            <th>Appt Set by:</th>
                            <td colspan="3">
                                <?php echo $model->appointment->setBy->fullName; ?>
                                <?php if ($Appointment->set_on_datetime > 0) {
                                    echo '<br/><label class="label p-p5">@</label>';
                                } ?>
                                <?php echo Yii::app()->format->formatDateTime($Appointment->set_on_datetime); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Appt Set Method:</th>
                            <td colspan="3">
                                <?php echo $Appointment->taskType->name; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Appt Set for:</th>
                            <td colspan="3">
                                <?php echo $model->appointment->metBy->fullName; ?>
                                <?php if ($Appointment->set_for_datetime > 0) {
                                    echo '<br/><label class="label p-p5">@</label>';
                                } ?>
                                <?php echo Yii::app()->format->formatDateTime($Appointment->set_for_datetime); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Appt Location:</th>
                            <td><?php echo $Appointment->getModelAttribute("getLocationTypes", $Appointment->location_ma); ?><?php if ($Appointment->location_other) {
                                    echo ': ' . $Appointment->location_other;
                                } ?></td>
                        </tr>
                        <tr>
                            <th>Appt Status:</th>
                            <td><?php echo $Appointment->getModelAttribute("getMetStatusTypes", $Appointment->met_status_ma); ?>
                                <?php echo ($Appointment->met_status_ma == Appointments::MET_STATUS_CANCEL || $Appointment->met_status_ma == Appointments::MET_STATUS_RESCHEDULE)? ' Reason: '.$Appointment->met_status_reason : ''; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Signed:</th>
                            <td>
                                <?php echo StmFormHelper::getYesNoName($model->appointment->is_signed); ?><?php if ($Appointment->not_signed_reason) {
                                    echo ': ' . $Appointment->getModelAttribute("getNotSignedBuyerReasonTypes", $Appointment->not_signed_reason);
                                } ?><?php if ($Appointment->not_signed_reason_other) {
                                    echo ': ' . $Appointment->not_signed_reason_other;
                                } ?>
                            </td>
                        </tr>
                        <? endif;?>
						<tr>
							<th colspan="2" style="text-align: center;"><br><u>AGENT PROFILE / PRODUCTION</u></th>
						</tr>
						<tr>
							<th>Current Brokerage:</th>
							<td><?php echo $model->current_brokerage; ?></td>
						</tr>
                        <tr>
                            <th>Brokerage MLS ID:</th>
                            <td><?php echo $model->brokerage_mls_id; ?>
                                <span class="label" style="margin-left: 8px;">License #: </span> <?php echo $model->brokerage_license_number; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Agent MLS ID:</th>
                            <td>
                                <?php echo $model->agent_mls_id; ?>
                                <span class="label" style="margin-left: 8px;">License #: </span> <?php echo $model->agent_license_number; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Agent 12 mo Volume:</th>
                            <td>
                                <?php echo $model->annual_volume; ?>
                                <span class="label" style="margin-left: 8px;">% Change: </span> <?php echo ($model->annual_volume_change) ? $model->annual_volume_change.'%' : ''; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Agent 12 mo Units:</th>
                            <td>
                                <?php echo $model->annual_units; ?>
                                <span class="label" style="margin-left: 8px;">% Change: </span> <?php echo ($model->annual_units_change) ? $model->annual_units_change.'%' : ''; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Data Last Updated:</th>
                            <td>
                                <?php echo Yii::app()->format->formatDate($model->annual_data_updated); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Website:</th>
                            <td><?php echo $model->website; ?></td>
                        </tr>
						</tbody>
					</table>
				</div>
				<div class="g6">
					<table class="container">
						<tbody>
                        <tr>
                            <th>Assignments:</th>
                            <td>
                                <? if($model->assignments) {
                                    foreach($model->assignments as $i => $assignment) {
                                        echo ($i >0) ? '<br>': '';
                                        echo $assignment->contact->fullName .' ('.$assignment->assignmentType->display_name.')';
                                    }
                                }
                                ?>
                            </td>
                        </tr>
						<tr>
							<th>Rating:</th>
							<td><?php echo ($model->rating > 1) ? $model->rating . '  of 10 Star' : ''; ?></td>
						</tr>
						<tr>
							<th>Source:</th>
							<td><?php echo $model->source->name; ?></td>
						</tr>
						<tr>
							<th>Referred by:</th>
							<td><?php echo $model->referredBy->fullName; ?></td>
						</tr>
						<tr>
							<th>Top 3 Strengths:</th>
							<td><?php echo $model->strengths; ?></td>
						</tr>
						<tr>
							<th>Top 3 Weaknesses:</th>
							<td><?php echo $model->weaknesses; ?></td>
						</tr>
						<tr>
							<th>Communication Skills:</th>
							<td><?php echo $model->communication_skills; ?></td>
						</tr>
						<tr>
							<th>Experience:</th>
							<td><?php echo $model->experience; ?></td>
						</tr>
						<tr>
							<th>Education:</th>
							<td><?php echo $model->education; ?></td>
						</tr>
						<tr>
							<th colspan="2" style="text-align: center;"><br><u>EMPLOYMENT</u></th>
						</tr>
						<tr>
							<th>Current Employer:</th>
							<td><?php echo $model->current_employer; ?></td>
						</tr>
						<tr>
							<th>Yrs on Current Job:</th>
							<td><?php echo $model->years_current_job; ?></td>
						</tr>
						<tr>
							<th>Yrs on Previous Job:</th>
							<td><?php echo $model->years_previous_job; ?></td>
						</tr>
						<tr>
							<th>Avg Years on Job:</th>
							<td><?php echo $model->average_years_job; ?></td>
						</tr>
                        <tr>
                            <th colspan="2" style="text-align: center;"><br><u>GOALS, DREAMS & MOTIVATION</u></th>
                        </tr>
                        <tr>
                            <th>Goal:</th>
                            <td><?php echo $model->goal; ?></td>
                        </tr>
                        <tr>
                            <th>Dream:</th>
                            <td><?php echo $model->dream_goal; ?></td>
                        </tr>
                        <tr>
                            <th>Motivation:</th>
                            <td><?php echo $model->motivation; ?></td>
                        </tr>
<!--                        <tr>-->
<!--                            <th>Pain Points:</th>-->
<!--                            <td>--><?php //echo $model->pain_points; ?><!--</td>-->
<!--                        </tr>-->
						</tbody>
					</table>
				</div>
				<div class="g12">
					<table class="container">
						<tbody>
						<tr class="p-p10">
							<th class="narrow">Notes:</th>
							<td><?php echo nl2br($model->notes); ?></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php $this->endStmPortletContent(); ?>
	</div>
</div>
<?php
	//$this->widget('admin_widgets.ActivityDetailsPortlet.ActivityDetailsPortlet', array(
	//	'model'         => $model,
	//	'componentType' => $model->getComponentType(),
	//));
	$this->widget('admin_widgets.ActivityDetailsPortlet.ActivityDetailsPortlet', array(
			'model' => $model,
			'componentType' => ComponentTypes::model()->findByPk(ComponentTypes::RECRUITS),
		)
	);
?>

<div id="" class="g12 p-mh0 p-mv5">
	<?php
		$this->widget('admin_widgets.ActivityLogPortlet.ActivityLogPortlet', array(
				'parentModel' => $model
			)
		);
	?>
</div>