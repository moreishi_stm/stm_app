<?php



Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);

$module = Yii::app()->controller->module->id;
$this->breadcrumbs = array(
	'List' => '',
);
$hasEmailBlastPermission = (Yii::app()->user->checkAccess('emails')) ? 1 : 0;
$cookieExpire = date('Y-m-d H:i:s'); //Thu, 18 Dec 2013 12:00:00 UTC
Yii::app()->clientScript->registerScript('recruitSearch', <<<JS

    $('table tr').live("click",function(e){
        if (e.target instanceof HTMLInputElement || e.target instanceof HTMLAnchorElement){
            return;
        }
        window.location = '/admin/recruits/' + $(this).attr("data-id");
    });

    $('form#recruit-list-search input[type="checkbox"]').change(function(){
        var checkboxName = $(this).prop('name').replace('Checkbox', '');
        if($(this).is(':checked')) {
            $('#'+checkboxName).prop('value',1);
        } else {
            $('#'+checkboxName).prop('value',0);
        }
        $('form#recruit-list-search').submit();
    });

	 $('#recruit-grid .recruit-button-tip').tipsy({live: true, gravity: 's', title: 'data-tooltip'});
	 $('#recruit-grid .recruit-button-tip-activity').tipsy({live: true, gravity: 'e', title: 'data-tooltip'});

    $('#listview-search form').submit(function() {

        // clear out mass update
        $('#MassAction_action').val('');
		$(".massActionChzn").val('').trigger('liszt:updated');
        $("#MassAction_action_tag_options").hide();

        $('#dialerProcess').val(0);

        $.fn.yiiGridView.update('recruit-grid', {
            data: $(this).serialize()
        });

        $('#dialerAction').val('');
        $('#dialerListId').val('');
    	return false;
    });

    $('#email-blast-preview-button').click(function() {

        if($('#EmailBlasts_email_template_id').val() == '') {
            alert('Please select an Email Template');
            return false;
        }

        $('#recruit-grid div.keys').attr('title','');

        $.fn.yiiGridView.update('recruit-grid', {
            data: $('#recruit-list-search').serialize(),
            complete: function(jqXHR, status) {

                var html = $.parseHTML(jqXHR.responseText);
                var emailBodyContainer = $('.email-body-container', $(html)).html();

                $('div.email-body-container').html(emailBodyContainer);

                if($("div#recruit-grid div.summary").length > 0) {
                    $("#send-emails-row").show('normal');
//                    $("#save-draft-button").show('normal');
                }
                else {
                    $("#send-emails-button").hide('normal');
//                    $("#save-draft-button").hide('normal');
                }
            }
        });

        return false;
    });

    $('.dialer-button').click(function(){
        $('#dialerProcess').val(1);

        if($('#dialerAction').val() == '') {
            alert('Please select option to Add or Remove from Dialer list.');
            $('#dialerProcess').val(0);
            return false;
        }

        if($('#dialerListId').val() == '') {
            alert('Please select a Dialer list.');
            $('#dialerProcess').val(0);
            return false;
        }

        $.fn.yiiGridView.update('recruit-grid', {
            data: $('#listview-search form').serialize()
        });
        $('#dialerProcess').val(0);
    	return false;
    });

//    $('#dialerAction, #dialerListId').change(function(){
//        if($('#dialerAction').val() != '' && $('#dialerListId').val() != '') {
//            $('.dialer-button').show('normal');
//        }
//        else {
//            $('.dialer-button').hide('normal');
//        }
//    });

    $('.status-unselect-all').click(function(){
        $('select#Recruits_status_id').val(null).trigger('liszt:updated');
    });
    $('.status-select-all').click(function(){
        $('#Recruits_status_id option').prop('selected', true).trigger('liszt:updated');
    });

    $('.status-select-abc').click(function(){
        $('select#Recruits_status_id').val(["2", "3", "4"]).trigger('liszt:updated');
    });

    $('.status-select-a').click(function(){
        $('select#Recruits_status_id').val(["2"]).trigger('liszt:updated');
    });

    $('.status-select-b').click(function(){
        $('select#Recruits_status_id').val(["3"]).trigger('liszt:updated');
    });

    $('.status-select-c').click(function(){
        $('select#Recruits_status_id').val(["4"]).trigger('liszt:updated');
    });

    $('.broadcast-button').click(function(){

        if(!$('#broadcastCallerId').val().match(/^\d{10}/)) {
            alert('Please enter a valid Caller ID.');
            $('#broadcastProcess').val(0);
            return false;
        }

        if($('#broadcastAudioFile').val() == '') {
            alert('Please select valid audio file.');
            $('#broadcastProcess').val(0);
            return false;
        }

        if($('#broadcastDeliveryDate').val().length == '') {
            alert('Please select valid date and time.');
            $('#broadcastProcess').val(0);
            return false;
        }

        if(confirm('Are you sure you want to continue?')) {

            $('#broadcastProcess').val(1);

            $.ajax({
                data: $('#listview-search form').serialize(),
                dataType: 'json',
                success: function(o) {

                    // Show flash message
                    Message.create(o.status, o.message);
                }
            });

            $('#broadcastCallerId').val('');
            $('#broadcastDeliveryDate').val('');
            $('#broadcastAudioFile').val('');

            $('#broadcastProcess').val(0);
        }

        return false;
    });

    $('.advanced-search-button').click(function(){
        $('.advanced-search-container').toggle(0, function(){
            var isVisible;
            if($(".advanced-search-container").is(":visible")) {
                isVisible = 1;
                $('#advanced-search-icon').removeClass('fa-angle-down').addClass('fa-angle-up');
                $('.advanced-search-button span.text').html('Hide Advanced Search Options');
            }
            else {
                isVisible = 0;
                $('#advanced-search-icon').removeClass('fa-angle-up').addClass('fa-angle-down');
                $('.advanced-search-button span.text').html('Show Advanced Search Options');
            }

            var expireDate = new Date();
            expireDate.setMonth( expireDate.getMonth() + 12 );
            document.cookie = "recruit_advanced_search_visible=" + isVisible + "; expires=" + expireDate.toUTCString();
        });
    });


    $('#EmailBlasts_email_template_id').change(function() {

        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.post('/admin/emails/emailTemplate', $( "#recruit-list-search" ).serialize(), function (data){
            $('#EmailBlasts_email_subject').val(data.subject);
            $('#EmailBlasts_description').val(data.description);
            emailTemplateBody(data.body, data.id);
            $('#sms-preview-container').hide();
            $('#email-preview-container').show();

        }, 'json');
        $("div.loading-container.loading").remove();
    });

    function emailTemplateBody(bodyHtml, templateId) {
            bodyHtml += '<div style="margin-top: 50px;"><a href="/admin/emailTemplates/edit/'+ templateId +'" class="button gray icon i_stm_edit" target="_blank">Edit Email Template</a>';
            $('.email-body-container').html(bodyHtml);
    }


    $('#send-emails-button, #save-draft-button').click(function() {

        if($("div#recruit-grid div.summary").length <1) {
            Message.create("warning","Please find recruits to send emails to.");
            return false;
        }

        if($('#EmailBlasts_email_template_id').val() == '') {
            Message.create("error","Please select an Email Template.");
            return false;
        }

        if($('#EmailBlasts_sender_name').val() == '') {
            Message.create("error","Please enter Sender Name.");
            return false;
        }

        if($('#EmailBlasts_sender_name').val().length < 8) {
            Message.create("error","Please enter a valid Sender Name.");
            return false;
        }

        if($('#EmailBlasts_sender_email_id').val() == '') {
            Message.create("error","Please enter From Email.");
            return false;
        }

        if($('#EmailBlasts_email_subject').val() == '') {
            Message.create("error","Please enter a Subject.");
            return false;
        }

        if($('#EmailBlasts_description').val() == '') {
            Message.create("error","Please enter a Description.");
            return false;
        }

        if($('#EmailBlasts_send_datetime').val() == '') {
            Message.create("error","Please enter a Send Date.");
            return false;
        }

        if($(this).attr('id')=='send-emails-button' && confirm('Confirm sending emails now.')) {

            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            $.get('/admin/recruits?blastSend=1', $( "#recruit-list-search" ).serialize(), function (data){

                if(data && data.status == 'success') {
                    Message.create('success','Email Blast sent successfully.');
                }
                else {
                    var statusMessage = (data && data.message) ? data.message : "";
                    Message.create('error','Error: Email Blast did not send. ' + statusMessage);
                }
                $("div.loading-container.loading").remove();
            },"json");
        }
//        else if($(this).attr('id')=='save-draft-button') {
//            $("body").prepend("<div class='loading-container loading'><em></em></div>");
//            $.post('/admin/emails/list?blastDraft=1', $( "#emails-blast-form" ).serialize(), function (data){
//
//                $("div.loading-container.loading").remove();
//            });
//        }
        return false;
    });



    $('#start-sms-blast-button-container').click(function(){
        $('#start-sms-blast-button-container').hide();
        $('#start-sms-blast-row').show('normal');
    });
    $('#start-email-blast-button-container').click(function(){
        $('#start-email-blast-button-container').hide();
        $('#start-email-blast-row').show('normal');
    });

    $('#sms-blast-preview-button').click(function() {

        if($('#SmsBlasts_sms_template_id').val() == '') {
            alert('Please select an Sms Template');
            return false;
        }

        $('#recruit-grid div.keys').attr('title','');

        $.fn.yiiGridView.update('recruit-grid', {
            data: $('#recruit-list-search').serialize(),
            complete: function(jqXHR, status) {

                var html = $.parseHTML(jqXHR.responseText);
                var emailBodyContainer = $('.sms-body-container', $(html)).html();

                $('div.sms-body-container').html(emailBodyContainer);

                if($("div#recruit-grid div.summary").length > 0) {

                    // check for max limit for sms
                    var summaryString = $("div#recruit-grid div.summary").html();
                    var matches = summaryString.match(/of (\d+) results/);
                    var matchCount;

                    if(matches) {
                        matchCount = matches[1];
                        if(matchCount > {$smsBlastLimit}) {
                            alert("Your current search has " + matchCount + " results. Your current Mass SMS limit is 250 per send. Please adjust your search criteria and try again.")
                            return false;
                        }
                    }

                    $("#send-sms-row").show('normal');
    //                    $("#sms-save-draft-button").show('normal');
                }
                else {
                    $("#send-sms-button").hide('normal');
    //                    $("#sms-save-draft-button").hide('normal');
                }
            }
        });

        return false;
    });


	$('#SmsBlasts_sms_template_id').change(function() {
		if(!$(this).val()){return;}
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $.post(
        	'/admin/smsTemplates/'+$(this).val(),
        	{"returnSmsModel":1}, function (data){
        		$('#email-preview-container').hide();
            	if(!data.id){
            		Message.create('error','Error: Failed fetching the sms template.');
            		return;
            	}
            	$('#SmsBlasts_description').val(data.description);
            	bodyHtml = data.body
				bodyHtml += '<div style="margin-top: 50px;"><a href="/admin/smsTemplates/edit/'+ data.id +'" class="button gray icon i_stm_edit" target="_blank">Edit Sms Template</a>';
            	$('.sms-body-container').html(bodyHtml);

            	$('#sms-preview-container').show();

        }, 'json');
        $("div.loading-container.loading").remove();
    });

    $('#send-sms-button, #save-draft-button').click(function() {

        if($("div#recruit-grid div.summary").length <1) {
            Message.create("warning","Please find recruits to send emails to.");
            return false;
        }

        if($('#SmsBlasts_sms_template_id').val() == '') {
            Message.create("error","Please select an Email Template.");
            return false;
        }

        if($('#SmsBlasts_description').val() == '') {
            Message.create("error","Please enter a Description.");
            return false;
        }

        if($(this).attr('id')=='send-sms-button' && confirm('Confirm sending messages now.')) {

            // @todo: re-update grid to make sure count is within limit

            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            $.get('/admin/recruits?saveSmsBlast=1', $( "#recruit-list-search" ).serialize(), function (data){
				$("div.loading-container.loading").remove();
                if(data && data.status == 'success') {
                    Message.create('success','SMS Blast scheduled successfully.');
                }
                else {
                    var statusMessage = (data && data.message) ? data.message : "";
                    Message.create('error','Error: SMS Blast did not save. ' + statusMessage);
                }
            },"json");
        }
        return false;
    });

    $("#MassAction_action").on("change",function(){
		var option = $( this ).val();
		$(".massActionOptions").hide();
		$(".massActionChzn").val('').trigger('liszt:updated');

		if(option == "addTag" || option == "removeTag"){
			return $("#MassAction_action_tag_options").show();
		} else if (option == "applyActionPlan" || option == "unapplyActionPlan"){
			return $("#MassAction_action_plan_options").show();
		} else if (option == "status"){
			return $("#MassAction_action_status_options").show();
 		}
	});

	$( "form .ajaxMassAction" ).on("click", function( event ) {

		var message = '';
		event.preventDefault();

		if($("#MassAction_action").val() == '') {
		    alert("Please select a Mass Action.");
		    return false;
		}

        // validate action plan mass action, verify an action plan is selected
        if((($('#MassAction_action').val() == 'applyActionPlan' || $('#MassAction_action').val() == 'unapplyActionPlan'))) {

            if(($('#MassAction_plan_options').val() == '')) {
                alert('Please select an Action Plan.');
                return false;
            }

            if(($('#MassAction_action').val() == 'unapplyActionPlans') || ($('#MassAction_action').val() == 'applyActionPlans')) {
                message = '**** IMPORTANT: There is a limit of 250 mass actions for Action Plans. Repeat as needed. Please WAIT until first process is complete to start the next one. ****';
            }
        }

        // validate status mass action, verify a status is selected
        if(($('#MassAction_action').val() == 'addTag' || $('#MassAction_action').val() == 'removeTag')) {

            if($('#MassAction_options').val().length == 0) {
                alert('Please select at least 1 Tag.');
                return false;
            }

            // check if first item is empty then it means the whole thing is empty
            if($('#MassAction_options').val().length == 1 && $('#MassAction_options').val()[0] == "") {
                alert('Please select at least 1 Tag.');
                return false;
            }
        }

        // validate status mass action, verify a status is selected
        if((($('#MassAction_action').val() == 'status' && $('#MassAction_status_options').val() == ''))) {

            alert('Please select a Status.');
            return false;
        }

        if(confirm("Confirm proceeding with Mass Action. " + message)) {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            var element_id = $(this).closest('form').attr('id');
            $.get(
                $("#"+element_id).attr("action"),
                $( "#"+element_id ).serializeArray(),
                function(data) {
                    if(data.status == 200){
                        Message.create('success','Mass Update was successful.');

                        // clear out mass update
                        $('#MassAction_action').val('');
                        $("#MassAction_options, #MassAction_plan_options, #MassAction_status_options").val(null).trigger('liszt:updated');
                        $("#MassAction_action_tag_options, #MassAction_action_plan_options, #MassAction_action_status_options").hide();

                        // refresh grid
                        $.fn.yiiGridView.update('recruit-grid', {
                            data: $('#listview-search form').serialize()
                        });
                    }
                    else {
                        Message.create('error','Error: Mass Update had an error.');
                    }

                    $("div.loading-container.loading").remove();
                }
            );
        }
	});

	$("#export_csv_button").on("click",function(e){
		e.preventDefault();
		if(confirm("Are you sure you want to export this list? A new window will open for the export.")) {
            $('body').prepend('<div class="loading-container loading"><em></em></div>');
            $("#export_csv").val("csv");
			var url = $( "#recruit-list-search" ).attr("action") + "?" + $( "#recruit-list-search" ).serialize();
            window.open(url);
            setTimeout(function () {
                $('div.loading-container.loading').remove();
                Message.create("notice", "Export is processing in the new window. Please wait...");
            }, 5000);
        }
         return $("#export_csv").val(0);
	});
JS
	, CClientScript::POS_END);
?>
	<style type="text/css">
		#recruit-grid strong.disc {
			width: 25px;
			display: inline-block;
		}

		#recruit-grid div.disc-data, #recruit-grid div.ava-data {
			display: inline-block;
		}

		#recruit-grid .disc-data span {
			width: 35px;
			font-weight: normal;
			display: inline-block;
		}

		#recruit-grid .disc-data i {
			font-weight: normal;
			display: inline-block;
			float: left;
		}
		.datatables tbody:hover{
			cursor: pointer;
		}
		.email-body-container,
		.sms-body-container{
			width: 800px;
			margin-left: auto;
			margin-right: auto;
			margin-top: 50px;
			margin-bottom: 50px;
			background: white;
			padding: 60px;
			font-size: 13px;
			color: black;
			border: 1px solid #DDD;
		}
		.email-body-container table{
			background: white;
		}
	</style>
	<div id="listview-actions">
		<?if(!Yii::app()->user->checkAccess(AuthItem::ROLE_RECRUIT_REFERRING_AGENT)):?>
			<a href="/<?php echo Yii::app()->controller->module->id; ?>/contacts/add/recruits" class="button gray icon i_stm_add">Add</a>
		<? endif;?>
	</div>
	<div id="content-header">
		<h1>Recruits List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box" style="padding-bottom: 0;">
		<?php $this->renderPartial('_listSearchBox', array(
				'model' => $model,
				'emailBlast' => $emailBlast,
				'smsBlast' => $smsBlast,
                'stmSmsNumber' => $stmSmsNumber
			)
		); ?>
	</div><!-- search-form -->
<? if(Yii::app()->user->checkAccess('emails')):?>
	<div id="email-preview-container" style="<?=($emailTemplates->body) ? "" : "display: none;"?>">
		<div class="p-clr"></div>
		<div class="email-body-container p-clr" style="width: 800px; margin-left: auto; margin-right: auto; margin-top: 50px; margin-bottom: 50px;">
			<?
			if($emailTemplates->body) {
				echo $emailTemplates->body;
				echo '<div style="margin-top: 50px;"><a href="/admin/emailTemplates/edit/'.$emailTemplates->id.'" class="button gray icon i_stm_edit" target="_blank">Edit Email Template</a></div>';
			}
			?>
		</div>
	</div>
<?endif;?>
<? if(Yii::app()->user->checkAccess('emails')):?>
	<div id="sms-preview-container" style=" <?php echo ($smsTemplate->body) ? "" : "display: none;" ;?>" >
	<div class="p-clr"></div>
	<div class="sms-body-container p-clr" style="width: 800px; margin-left: auto; margin-right: auto; margin-top: 50px; margin-bottom: 50px;">
		<?
		if($smsTemplate->body) {
			echo $smsTemplate->body;
			echo '<div style="margin-top: 50px;"><a href="/admin/smsTemplates/edit/'.$smsTemplate->id.'" class="button gray icon i_stm_edit" target="_blank">Edit SMS Template</a></div>';
		}
		?>
	</div>
	</div>
<?endif;?>

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'recruit-grid',
		'dataProvider' => $model->search(),
		'template' => '{pager}{summary}{items}{summary}{pager}',
		'itemsCssClass' => 'datatables',
        'rowHtmlOptionsExpression' => 'array("data-id"=>$data->id)',
//		'selectionChanged'=>"function(id){window.location='/admin/recruits/' + $.fn.yiiGridView.getSelection(id);}",
		'columns' => array(
			array(
				'type' => 'raw',
				'name' => 'Status',
				'value' => '$data->statusName',
				'htmlOptions' => array('style' => 'max-width:100px'),
			),
			array(
				'type' => 'raw',
				'name' => 'Contact',
				'value' => 'Yii::app()->controller->action->printGridContact($data)',
				'htmlOptions' => array('style' => 'max-width:200px'),
			),
			array(
				'type' => 'raw',
				'name' => '$ Volume',
				'value' => '($data->annual_volume) ? Yii::app()->format->formatDollars($data->annual_volume) : ""',
				'htmlOptions' => array('style' => 'width:70px'),
			),
			array(
				'type' => 'raw',
				'name' => '% Volume Change',
				'value' => '($data->annual_volume_change_percent <> 0) ? $data->annual_volume_change_percent."%" : ""',
				'htmlOptions' => array('style' => 'width:50px'),
			),
			array(
				'type' => 'raw',
				'name' => '$ Volume Change',
				'value' => '($data->annual_volume_change <> 0) ? Yii::app()->format->formatDollars($data->annual_volume_change) : ""',
				'htmlOptions' => array('style' => 'width:90px'),
			),
			array(
				'type' => 'raw',
				'name' => '# Units',
				'value' => '$data->annual_units',
				'htmlOptions' => array('style' => 'width:50px'),
			),
			array(
				'type' => 'raw',
				'name' => 'Avg Sales $',
				'value' => '($data->annual_volume > 0 && $data->annual_units > 0) ? Yii::app()->format->formatDollars($data->annual_volume / $data->annual_units) : ""',
				'htmlOptions' => array('style' => 'width:80px'),
			),
			array(
				'type' => 'raw',
				'name' => 'Info',
				'value' => 'Yii::app()->controller->printAgentInfo($data)',
				//                'htmlOptions' => array('style' => 'width:400px'),
			),
			array(
				'type' => 'raw',
				'name' => 'Description',
				'value' => 'Yii::app()->controller->printDescription($data)',
				'htmlOptions' => array('style' => 'width:300px'),
			),
			array(
				'name' => 'Add Task',
				'type' => 'raw',
				'value' => '"<a class=\"action-button orange fa fa-calendar recruit-button-tip add-task-button\" data-tooltip=\"Add Task\" data=\"".$data->id."\" ctid=\"".ComponentTypes::RECRUITS."\"></a>"',
				'htmlOptions' => array(
					'style' => 'width:4px'
				),
			),
			array(
				'name' => 'Add Log',
				'type' => 'raw',
				'value' => '"<a class=\"action-button green fa fa-sticky-note recruit-button-tip-activity add-activity-log-button\" data-tooltip=\"Add Activity Log\" data=\"".$data->id."\" ctid=\"".ComponentTypes::RECRUITS."\"></a>"',
				'htmlOptions' => array(
					'style' => 'width:4px'
				),
			),
		),
	)
);

Yii::import('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget');
if (!class_exists('ClickToCallDialogWidget', false)) {
	$this->widget('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget', array(
			'id' => ClickToCallDialogWidget::CLICK_TO_CALL_DIALOG_ID,
			'title' => 'Click to Call',
			'triggerElement' => '.click-to-call-button',
		)
	);
}

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
if (!class_exists('TaskDialogWidget', false)) {
	$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
			'id' => TaskDialogWidget::TASK_DIALOG_ID,
			'title' => 'Create New Task',
			'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button, .delete-task-button',
			'parentModel' => new Contacts,
		)
	);
}

Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
	$this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
			'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
			'title' => 'Add New Activity Log',
			'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
			'parentModel' => new Contacts,
		)
	);
}