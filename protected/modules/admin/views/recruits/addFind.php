<?php
$this->breadcrumbs = array(
	'Add New' => '',
);

Yii::app()->clientScript->registerScript('search', "
$('form#transaction-list-search').submit(function() {
	$.fn.yiiGridView.update('contact-grid', { data: $(this).serialize() });
	return false;
});
");
?>

<h1>
	Add New Recruit
</h1>
<h3>Note: Contact must exist to add a Recruit.<br />
	Step 1: Search below to find a Contact to add a Recruit.<br />Step 2: Choose an Existing Recruit or Add a new one.
</h3>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('../transactions/_listSearchBoxAdd', array('model' => $model)); ?>
</div>

<?php
if(!$_GET['Contacts'])
	$model->id=0;

$this->widget('admin_module.components.StmGridView', array(
	'id'=>'contact-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'id',
		'first_name',
		'last_name',
		'primaryEmail',
		array(
			'type'=>'raw',
			'name'=>'# Existing Recruits',
			'value'=>'count($data->recruit)',
		),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/recruits/add/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_add grey-button\">Add New Recruit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:120px'),
		),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'"<div><a href=\"/".Yii::app()->controller->module->id."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Contact</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:100px'),
		),
	),
));