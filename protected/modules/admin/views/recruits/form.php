<?
$moduleName = Yii::app()->controller->module->id;
//Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->jsAssetsUrl . DS . 'maskedinput' . DS . 'jquery.maskedinput.1.4.1.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->jsAssetsUrl . DS . 'stm_AssignmentsGui.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('recruitFormScriptJs', <<<JS

		$("select#Recruits_referred_by_id").ajaxChosen({
			type: 'POST',
			url: '/$moduleName/contacts/autocomplete',
			dataType: 'json'
		}, function (data) {
			var results = [];

			$.each(data, function (i, val) {
                results.push({ value: val.value, text: val.text+' ('+val.email+')' });
			});

			return results;
		});

		$("select#Recruits_referred_by_ids").ajaxChosen({
			type: 'POST',
			url: '/$moduleName/contacts/autocomplete',
			dataType: 'json'
		}, function (data) {
			var results = [];

			$.each(data, function (i, val) {
                results.push({ value: val.value, text: val.text+' ('+val.email+')' });
			});

			return results;
		});

//        $.mask.definitions['~']='[-?0-9]';
//		$('#Recruits_annual_volume').mask('$?999999999');
//		$('#Recruits_annual_volume_change_percent').mask('?~9999.99%');
JS
, CClientScript::POS_END);

$this->breadcrumbs = array(
	$model->contact->fullName=>'/'.Yii::app()->controller->module->id.'/recruits/'.$model->id,
);

if ($this->action->id == 'edit')
	$this->breadcrumbs = CMap::mergeArray($this->breadcrumbs, array(ucfirst($this->action->id) => ''));
?>

<h1 class="name"><?php echo $model->contact->fullName;?></h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
        'id' => ' recruit-form',
        'enableAjaxValidation' => false,
    )
);
$this->beginStmPortletContent(array(
        'handleTitle' => 'Recruit Details',
        'handleIconCss' => 'i_wizard'
    )
);
?>
<div id="action-plans-container">
<div class="g12 p-mb5 rounded-text-box odd-static">
<div class="g6">
    <table class="">
        <tr>
            <th>Name:</th>
            <td>
                <?php echo $form->textField($model->contact, 'first_name', $htmlOptions = array('class' => 'g6','placeholder' => 'First Name')); ?>
                <?php echo $form->textField($model->contact, 'last_name', $htmlOptions = array('class' => 'g5','placeholder' => 'Last Name')); ?>
                <?php echo $form->error($model, 'first_name'); ?><?php echo $form->error($model, 'last_name'); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><em class="icon icon-only i_stm_star"></em></th>
            <td><span class="label">= Indicates Primary Email, Phone, Address</span>
            </td>
        </tr>
        <tr>
            <th class="narrow top-align">Emails:</th>
            <td id="email-table-cell">
                <?php echo $this->renderPartial('../contacts/_addlEmailTemplate', array(
                        'form' => $form,
                        'containerHtmlOptions' => array(
                            'style' => 'display: none',
                            'id' => 'addlEmailTemplate'
                        )
                    )); ?>
                <div id="addlEmailRows" class="g12 p-p0" style="min-height: 0;">
                    <?php
                    if ($model->contactEmails) {
                        foreach ($model->contactEmails as $i => $ContactEmail) {
                            echo $this->renderPartial('../contacts/_addlEmailTemplate', array(
                                    'form' => $form,
                                    'model' => $ContactEmail,
                                    'i' => $i,
                                ));
                        }
                    }
                    ?>
                </div>
                <button type="button" id="add-email" class="text p-m0"><em class="icon i_stm_add"></em>Add Email</button>
            </td>
        </tr>
        <tr>
            <th class="narrow top-align">Address:</th>
            <td id="address-table-cell">
                <?php echo $this->renderPartial('../contacts/_addlAddressTemplate', array(
                        'form' => $form,
                        'containerHtmlOptions' => array(
                            'style' => 'display: none',
                            'id' => 'addlAddressTemplate'
                        )
                    )); ?>
                <div id="addlAddressRows" class="g12 p-p0" style="min-height: 0;">
                    <?php
                    if ($model->contactAddresses) {
                        foreach ($model->contactAddresses as $i => $ContactAddress) {
                            echo $this->renderPartial('../contacts/_addlAddressTemplate', array(
                                    'form' => $form,
                                    'model' => $ContactAddress,
                                    'i' => $i,
                                ));
                        }
                    }
                    ?>
                </div>
                <button type="button" id="add-address" class="text"><em class="icon i_stm_add"></em>Add Address</button>
            </td>
        </tr>
        <tr>
            <th class="narrow top-align">Phone:</th>
            <td id="phone-table-cell">
                <?php echo $this->renderPartial('../contacts/_addlPhoneTemplate', array(
                        'form' => $form,
                        'containerHtmlOptions' => array(
                            'style' => 'display: none',
                            'id' => 'addlPhoneTemplate'
                        )
                    )); ?>
                <div id="addlPhoneRows" class="g12 p-p0" style="min-height: 0;">
                    <?php
                    if ($model->contactPhones) {
                        foreach ($model->contactPhones as $i => $ContactPhone) {
                            echo $this->renderPartial('../contacts/_addlPhoneTemplate', array(
                                    'form' => $form,
                                    'model' => $ContactPhone,
                                    'i' => $i+1,
                                ));
                        }
                    }
                    ?>
                </div>
                <button type="button" id="add-phone" class="text"><em class="icon i_stm_add"></em>Add Phone</button>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $form->labelEx($model, 'status_id'); ?>:</th>
            <td>
                <?php echo $form->dropDownList(
                    $model, 'status_id', CHtml::listData(TransactionStatus::model()->byComponentType(ComponentTypes::RECRUITS)->defaultSort()->findAll(), 'id', 'displayName'),
                    $htmlOptions = array('empty'            => 'Select One', 'class' => 'g11'
                    )
                ); ?>
                <?php echo $form->error($model, 'status_id'); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow"><?php echo $form->labelEx($model, 'source_id'); ?>:</th>
            <td>
                <?php echo $form->dropDownList($model, 'source_id', Sources::optGroupList($model->source_id), $htmlOptions = array(
                        'class' => 'sourceId g11',
                        'data-placeholder' => 'Select a Source'
                    )
                );
                $this->widget('admin_module.extensions.EChosen.EChosen', array(
                        'target' => 'select.sourceId',
                        'options' => array('allow_single_deselect' => true),
                    )
                );
                ?>
                <?php echo $form->error($model, 'source_id'); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow">Tags:</th>
            <td>
                <?php echo $form->dropDownList($model, 'typesCollection', CHtml::listData(RecruitTypes::model()->findAll(), 'id', 'name'), $htmlOptions = array(
                        'class' => 'chzn-select g11',
                        'multiple' => 'multiple',
                        'data-placeholder' => 'Recruit Tags'
                    )
                );
                ?>
                <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => '#Recruits_typesCollection'));
                ?>

                <?php echo $form->error($model, 'typesCollection'); ?>
            </td>
        </tr>
        <tr>
            <th>Met:</th>
            <td>
                <?php echo $form->dropDownList($model, 'met_status_ma', StmFormHelper::getYesNoList(), $htmlOptions = array(
                        'class' => 'g4',
                        'empty' => 'Select One'
                    )
                ); ?>
                <?php echo $form->error($model, 'met_status_ma'); ?>
            </td>
        </tr>
        <tr>
            <th>Licensed:</th>
            <td>
                <?php echo $form->dropDownList($model, 'is_licensed', StmFormHelper::getYesNoList(), $htmlOptions = array(
                        'class' => 'g4',
                        'empty' => 'Select One'
                    )
                ); ?>
                <?php echo $form->error($model, 'is_licensed'); ?>
                <span class="label p-fl">Licensed Date:</span>
                <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'licensed_date',
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'class' => 'g4',
                            'placeholder' => 'Licensed Date'
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'licensed_date'); ?>
            </td>
        </tr>
        <tr>
            <th>Target Join Date:</th>
            <td>
                <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'target_date',
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'class' => 'g4',
                            'placeholder' => 'Target Date'
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'target_date'); ?>
            </td>
        </tr>
        <tr>
            <th>Production Status:</th>
            <td>
                <?php echo $form->dropDownList($model, 'recruit_production_status_id', CHtml::listData(RecruitProductionStatuses::model()->findAll(), 'id', 'name'), $htmlOptions = array(
                        'class' => 'g11',
                        'empty' => '',
                        'data-placeholder' => 'Production Status'
                    )
                ); ?>
                <?php echo $form->error($model, 'recruit_production_status_id'); ?>
                <?php $this->widget('admin_module.extensions.EChosen.EChosen', array(
                        'target' => '#Recruits_recruit_production_status_id',
                        'options' => array('allow_single_deselect' => true),
                    )
                );
                ?>
            </td>
        </tr>
        <tr>
            <th>Hire Date:</th>
            <td>
                <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'hire_date',
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'class' => 'g4',
                            'placeholder' => 'Hire Date'
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'hire_date'); ?>
            </td>
        </tr>
        <?if(0):?>
        <tr>
            <th>Referred by:</th>
            <td>
                <?php echo $form->dropDownList($model, 'referred_by_id', array('' => ''), $htmlOptions = array(
                        'class' => 'g4',
                        'style' => 'width:350px;',
                        'data-placeholder' => 'Type Name if applicable'
                    )
                ); ?>
                <?php echo $form->error($model, 'referred_by_id'); ?>
                <?php $this->widget('admin_module.extensions.EChosen.EChosen', array(
                        'target' => '#Recruits_referred_by_id',
                        'options' => array('allow_single_deselect' => true),
                    )
                );
                ?>
            </td>
        </tr>
        <tr>
            <th>Application:</th>
            <td>
                <?php echo $form->dropDownList($model, 'application_status_ma', StmFormHelper::getYesNoList(), $htmlOptions = array(
                        'class' => 'g4',
                        'empty' => 'Select One'
                    )
                ); ?>
                <?php echo $form->error($model, 'application_status_ma'); ?>
            </td>
        </tr>
        <tr>
            <th class="narrow">DISC:</th>
            <td>
                <div class="g10">
                    <?php echo $form->dropDownList($model, 'disc_D', StmFormHelper::getDropDownList(array(
                                'start' => 1,
                                'end' => 99,
                                'increment' => 1,
                                'label' => ' (D)'
                            )
                        ), $htmlOptions = array(
                            'empty' => 'D',
                            'class' => 'g3'
                        )
                    ); ?>
                    <?php echo $form->dropDownList($model, 'disc_I', StmFormHelper::getDropDownList(array(
                                'start' => 1,
                                'end' => 99,
                                'increment' => 1,
                                'label' => ' (I)'
                            )
                        ), $htmlOptions = array(
                            'empty' => 'I',
                            'class' => 'g3'
                        )
                    ); ?>
                    <?php echo $form->dropDownList($model, 'disc_S', StmFormHelper::getDropDownList(array(
                                'start' => 1,
                                'end' => 99,
                                'increment' => 1,
                                'label' => ' (S)'
                            )
                        ), $htmlOptions = array(
                            'empty' => 'S',
                            'class' => 'g3'
                        )
                    ); ?>
                    <?php echo $form->dropDownList($model, 'disc_C', StmFormHelper::getDropDownList(array(
                                'start' => 1,
                                'end' => 99,
                                'increment' => 1,
                                'label' => ' (C)'
                            )
                        ), $htmlOptions = array(
                            'empty' => 'C',
                            'class' => 'g3'
                        )
                    ); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>DISC Date:</th>
            <td>
                <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'disc_date',
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'class' => 'g4',
                            'placeholder' => 'Date'
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'disc_date'); ?>
            </td>
        </tr>


        <tr>
            <th class="narrow">AVA:</th>
            <td>
                <?php echo $form->dropDownList($model, 'ava_vector_1', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => ' (V1)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V1',
                        'class' => 'g2',
                        'style' => 'width:47px;'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_1_asterisk', array(
                        0 => '',
                        1 => '*'
                    ), $htmlOptions = array(
                        'class' => 'p-fl',
                        'style' => 'width:37px;'
                    )
                ); ?>

                <?php echo $form->dropDownList($model, 'ava_vector_2', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => ' (V2)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V2',
                        'class' => 'g2',
                        'style' => 'width:47px;'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_2_asterisk', array(
                        0 => '',
                        1 => '*'
                    ), $htmlOptions = array(
                        'class' => 'p-fl',
                        'style' => 'width:37px;'
                    )
                ); ?>

                <?php echo $form->dropDownList($model, 'ava_vector_3', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => ' (V3)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V3',
                        'class' => 'g2',
                        'style' => 'width:47px;'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_3_asterisk', array(
                        0 => '',
                        1 => '*'
                    ), $htmlOptions = array(
                        'class' => 'p-fl',
                        'style' => 'width:37px;'
                    )
                ); ?>

                <?php echo $form->dropDownList($model, 'ava_vector_4', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => ' (V4)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V4',
                        'class' => 'g2',
                        'style' => 'width:47px;'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_4_asterisk', array(
                        0 => '',
                        1 => '*'
                    ), $htmlOptions = array(
                        'class' => 'p-fl',
                        'style' => 'width:37px;'
                    )
                ); ?>

                <?php echo $form->dropDownList($model, 'ava_vector_5', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => ' (V5)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V5',
                        'class' => 'g2',
                        'style' => 'width:47px;'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_5_asterisk', array(
                        0 => '',
                        1 => '*'
                    ), $htmlOptions = array(
                        'class' => 'p-fl',
                        'style' => 'width:37px;'
                    )
                ); ?>
            </td>
        </tr>
        <tr>
            <th>AVA Date:</th>
            <td>
                <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'ava_date',
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'class' => 'g4',
                            'placeholder' => 'Date'
                        ),
                    )
                );
                ?>
                <?php echo $form->error($model, 'ava_date'); ?>
            </td>
        </tr>
        <?endif;?>
        <tr>
            <th colspan="2" style="text-align: center;"><br><u>AGENT PROFILE/PRODUCTION</u></th>
        </tr>
        <tr>
            <th>Current Brokerage Name:</th>
            <td>
                <?php echo $form->textField($model, 'current_brokerage', $htmlOptions = array('class' => 'g11','placeholder' => 'Current Brokerage')); ?>
                <?php echo $form->error($model, 'current_brokerage'); ?>
            </td>
        </tr>
        <tr>
            <th>Office Address:</th>
            <td>
                <?php echo $form->textField($model, 'office_address', $htmlOptions = array('class' => 'g11','placeholder' => 'Office Address')); ?>
                <?php echo $form->error($model, 'office_address'); ?>
            </td>
        </tr>
        <tr>
            <th>City/Zip:</th>
            <td>
                <?php echo $form->textField($model, 'office_city', $htmlOptions = array('class' => 'g8','placeholder' => 'Office City')); ?>
                <?php echo $form->textField($model, 'office_zip', $htmlOptions = array('class' => 'g3','placeholder' => 'Office Zip')); ?>
                <?php echo $form->error($model, 'office_city'); ?><?php echo $form->error($model, 'office_zip'); ?>
            </td>
        </tr>
        <tr>
            <th>Brokerage MLS ID:</th>
            <td>
                <?php echo $form->textField($model, 'brokerage_mls_id', $htmlOptions = array('class' => 'g3','placeholder' => 'MLS ID Brokerage')); ?>
                <?php echo $form->error($model, 'brokerage_mls_id'); ?>
                <span class="label p-fl">Brokerage License #:</span>
                <?php echo $form->textField($model, 'brokerage_license_number', $htmlOptions = array('class' => 'g4','placeholder' => 'License # Brokerage')); ?>
                <?php echo $form->error($model, 'brokerage_license_number'); ?>
            </td>
        </tr>
        <tr>
            <th>Agent MLS ID:</th>
            <td>
                <?php echo $form->textField($model, 'agent_mls_id', $htmlOptions = array('class' => 'g3','placeholder' => 'MLS ID Agent')); ?>
                <?php echo $form->error($model, 'agent_mls_id'); ?>
                <span class="label p-fl">Agent License #:</span>
                <?php echo $form->textField($model, 'agent_license_number', $htmlOptions = array('class' => 'g4','placeholder' => 'License # Brokerage')); ?>
                <?php echo $form->error($model, 'agent_license_number'); ?>
            </td>
        </tr>
        <tr>
            <th>Agent Website:</th>
            <td>
                <?php echo $form->textField($model, 'website', $htmlOptions = array('class' => 'g11','placeholder' => 'Agent Website')); ?>
                <?php echo $form->error($model, 'website'); ?>
            </td>
        </tr>
        <tr>
            <th>Last 12 Months Volume:</th>
            <td>
                <div class="g4">
                    <?php echo $form->textField($model, 'annual_volume', $htmlOptions = array('class' => 'g11','placeholder' => 'Volume 12 mo')); ?>
        <!--            --><?php //$this->widget('StmMaskedTextField', array(
        //                    'model' => $model,
        //                    'attribute' => 'annual_volume',
        //                    'mask' => '$[9][9][9][9][9][9][9][9][9]',
        //                    'stmMaskedTextFieldClass' => '12MoVolume',
        //                    'htmlOptions' => array('class'=>'g11 12MoVolume', 'placeholder'=>'Volume 12 mo'),
        //                )); ?>
                    <?php echo $form->error($model, 'annual_volume'); ?>
                </div>
                <span class="label p-tr g3">% Change Volume:</span>
                <div class="g4">
                    <?php echo $form->textField($model, 'annual_volume_change_percent', $htmlOptions = array('class' => 'g11','placeholder' => 'Volume 12 mo')); ?>
        <!--            --><?php //$this->widget('StmMaskedTextField', array(
        //                    'model' => $model,
        //                    'attribute' => 'annual_volume_change_percent',
        //                    'charMap' => array('~'=>'[+-9]') ,
        //                    'mask' => '~[9][9][9][9].[9][9]%',
        //                    'stmMaskedTextFieldClass' => '12MoVolumeChangePercent',
        //                    'htmlOptions' => array('class'=>'g11', 'placeholder'=>'% Change Volume'),
        //                )); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>Last 12 Months Units:</th>
            <td>
                <div class="g4">
                    <?php $this->widget('StmMaskedTextField', array(
                            'model' => $model,
                            'attribute' => 'annual_units',
                            'mask' => '[9][9][9][9]',
                            'stmMaskedTextFieldClass' => '12MoUnits',
                            'htmlOptions' => array('class'=>'g11', 'placeholder'=>'Units 12 mo'),
                        )); ?>
                    <?php echo $form->error($model, 'annual_units'); ?>
                </div>
                <span class="label p-tr g3">$ Volume Change:</span>
                <div class="g4">
                    <?php echo $form->textField($model, 'annual_volume_change', $htmlOptions = array('class' => 'g11','placeholder' => 'Volume Change')); ?>
        <!--            --><?php //$this->widget('StmMaskedTextField', array(
        //                    'model' => $model,
        //                    'attribute' => 'annual_volume_change',
        //                    'mask' => '$[9][9][9][9][9][9][9][9][9]',
        //                    'stmMaskedTextFieldClass' => '12MoVolumeChange',
        //                    'htmlOptions' => array('class'=>'g11', 'placeholder'=>'Volume Change'),
        //                )); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th>Data Last Updated:</th>
            <td>
                <div class="g4">
                    <?$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model' => $model,
                            'attribute' => 'annual_data_updated',
                            'options' => array(
                                'showAnim' => 'fold',
                            ),
                            'htmlOptions' => array(
                                'class' => 'g11',
                                'placeholder' => '12 mo Updated'
                            ),
                        )
                    );
                    ?>
                    <?php echo $form->error($model, 'annual_updated'); ?>
                </div>
                <span class="label p-tr g3">Ranking:</span>
                <div class="g4">
                    <?php echo $form->textField($model, 'ranking', $htmlOptions = array('class' => 'g11','placeholder' => 'Ranking #')); ?>
                    <?php echo $form->error($model, 'ranking'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: center;"><br><u>GOALS, DREAMS & MOTIVATION</u></th>
        </tr>
        <tr>
            <th>Hot Button / Motivation:</th>
            <td>
                <?php echo $form->textArea($model, 'motivation', $htmlOptions = array('class' => 'g11', 'rows'=> 4)); ?>
                <?php echo $form->error($model, 'motivation'); ?>
            </td>
        </tr>
        <tr>
            <th>Goal:</th>
            <td>
                <?php echo $form->textField($model, 'goal', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'goal'); ?>
            </td>
        </tr>
        <!--<tr>-->
        <!--	<th>Dream Goal:</th>-->
        <!--	<td>-->
        <!--		--><?php //echo $form->textField($model, 'dream_goal', $htmlOptions = array('class' => 'g11')); ?>
        <!--		--><?php //echo $form->error($model, 'dream_goal'); ?>
        <!--	</td>-->
        <!--</tr>-->
        <tr>
            <th>Challenges:</th>
            <td>
                <?php echo $form->textArea($model, 'challenges', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'challenges'); ?>
            </td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: center;"><br><u>SPOUSE CONTACT INFO</u></th>
        </tr>        <tr>
            <th>Spouse Name:</th>
            <td>
                <?php echo $form->textField($model, 'spouse_first_name', $htmlOptions = array('class' => 'g6','placeholder' => 'Spouse First Name')); ?>
                <?php echo $form->textField($model, 'spouse_last_name', $htmlOptions = array('class' => 'g5','placeholder' => 'Spouse Last Name')); ?>
                <?php echo $form->error($model, 'spouse_first_name'); ?><?php echo $form->error($model, 'spouse_last_name'); ?>
            </td>
        </tr>
        <tr>
            <th>Spouse Email:</th>
            <td>
                <?php echo $form->textField($model, 'spouse_email', $htmlOptions = array('class' => 'g11','placeholder' => 'Spouse Email')); ?>
                <?php echo $form->error($model, 'spouse_email'); ?>
            </td>
        </tr>
        <tr>
            <th>Spouse Phone:</th>
            <td>
                <?php echo $form->textField($model, 'spouse_phone', $htmlOptions = array('class' => 'g4','placeholder' => 'Spouse Phone')); ?>
                <?php echo $form->error($model, 'spouse_phone'); ?>
            </td>
        </tr>
    </table>
</div>
<div class="g6">
    <table class="">
		<tr>
			<th><?php
				//die("<pre>".print_r($model->attributes,1));
				echo $form->labelEx($model, 'Referrers'); ?>:</th>
			<td>
				<?php
				if(empty($refferedByDataArray)){$refferedByDataArray = array();}
				echo $form->dropDownList($model, 'referred_by_ids', $refferedByDataArray, $htmlOptions = array(
					'data-placeholder' =>'Type Name Here...',
					'class'=>'g12',
					'multiple' => 'multiple'
				));
				echo $form->error($model, 'referred_by_ids');

				$this->widget('admin_module.extensions.EChosen.EChosen', array(
					'target' => 'select#Referrals_referred_by_ids',
					'options'=>array(
						'allow_single_deselect'=>true,
					),
				));
				?>
				<span class="label">(Note: Agent or Contact. Must be existing Contact)</span>
			</td>
		</tr>
        <tr>
            <th class="narrow top-align">Assignments:</th>
            <td id="assignment-table-cell">
                <?php echo $this->renderPartial(
                    '../transactions/_addlAssignmentTemplate', array(
                        'form'        => $form, 'model' => $Assignment, 'componentTypeId' => ComponentTypes::RECRUITS,
                        'componentId' => $model->id, 'containerHtmlOptions' => array(
                            'style' => 'display: none', 'id' => 'addlAssignmentTemplate'
                        )
                    )
                ); ?>
                <div id="addlAssignmentRows" class="g12 p-p0" style="min-height: 0;">
                    <?php
                    if ($model->assignments) {
                        foreach ($model->assignments as $i => $Assignment) {
                            echo $this->renderPartial(
                                '../transactions/_addlAssignmentTemplate', array(
                                    'form'            => $form, 'model' => $Assignment,
                                    'componentTypeId' => ComponentTypes::RECRUITS, 'componentId' => $model->id,
                                    'i'               => $i,
                                )
                            );
                        }
                    }
                    ?>
                </div>
                <button type="button" id="add-assignment" class="text p-m0"><em class="icon i_stm_add"></em>Add Assignment</button>
            </td>
        </tr>
        <?if(0):?>
        <tr>
            <th class="narrow">Overall Rating:</th>
            <td>
                <?php echo $form->dropDownList($model, 'rating', array(
                        '10.0' => '10 Star',
                        '9.5' => '9.5 Star',
                        '9.0' => '9 Star',
                        '8.5' => '8.5 Star',
                        '8.0' => '8 Star',
                        '7.5' => '7.5 Star',
                        '7.0' => '7 Star',
                        '6.0' => '6 Star',
                        '5.0' => '5 Star',
                    ), $htmlOptions = array(
                        'class' => 'g4',
                        'empty' => 'Select One'
                    )
                ); ?>
                <?php echo $form->error($model, 'rating'); ?>
            </td>
        </tr>
        <tr>
            <th>Strengths:</th>
            <td>
                <?php echo $form->textField($model, 'strengths', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'strengths'); ?>
            </td>
        </tr>
        <tr>
            <th>Weaknesses:</th>
            <td>
                <?php echo $form->textField($model, 'weaknesses', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'weaknesses'); ?>
            </td>
        </tr>
        <tr>
            <th>Communication Skills:</th>
            <td>
                <?php echo $form->textField($model, 'communication_skills', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'communication_skills'); ?>
            </td>
        </tr>
        <tr>
            <th>Experience:</th>
            <td>
                <?php echo $form->textField($model, 'experience', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'experience'); ?>
            </td>
        </tr>
        <tr>
            <th>Education:</th>
            <td>
                <?php echo $form->textField($model, 'education', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'education'); ?>
            </td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: center;"><br><u>EMPLOYMENT</u></th>
        </tr>
        <tr>
            <th>Current Employers:</th>
            <td>
                <?php echo $form->textField($model, 'current_employer', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'current_employer'); ?>
            </td>
        </tr>
        <tr>
            <th>Years on Current Job:</th>
            <td>
                <?php echo $form->textField($model, 'years_current_job', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'years_current_job'); ?>
            </td>
        </tr>
        <tr>
            <th>Years on Prev Job:</th>
            <td>
                <?php echo $form->textField($model, 'years_previous_job', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'years_previous_job'); ?>
            </td>
        </tr>
        <tr>
            <th>Avg Years on Job:</th>
            <td>
                <?php echo $form->textField($model, 'average_years_job', $htmlOptions = array('class' => 'g11')); ?>
                <?php echo $form->error($model, 'average_years_job'); ?>
            </td>
        </tr>
        <?endif;?>
        <?php $this->renderPartial('../transactions/_formAppointment',array('form'=>$form,'Appointment'=>$Appointment, 'model'=>$model));?>
    </table>
</div>
<div class="g12">
    <table class="">
        <tr>
            <th class="narrow" style="width: 17.5%;">Notes:</th>
            <td>
                <?php echo $form->textArea($model, 'notes', $htmlOptions = array(
                        'placeholder' => 'Notes',
                        'class' => 'g11',
                        'rows' => '15'
                    )
                );?>
                <?php echo $form->error($model, 'notes'); ?>
            </td>
        </tr>
    </table>
</div>
</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <button type="submit" class="submit wide">Submit Recruit</button>
</div>
<?php $this->endWidget(); ?>