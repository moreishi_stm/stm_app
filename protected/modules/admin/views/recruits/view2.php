<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);

$this->breadcrumbs = array(
    $model->contact->fullName => '',
);
Yii::app()->clientScript->registerCss('recruitMiniView2Css', <<<CSS
    .icon.icon-only.i_stm_star {
        top: 8px;
    }
    .text-button.phone-status-button {
        top: -4px;
        position: relative;
        margin-left: 0;
    }
    #documents-tab .ui-button-panel {
        height: 430px;
    }
CSS
);
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'ajax_chosen.js', CClientScript::POS_END);

$moduleName = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('recruitView2ScriptJs', <<<JS

	 $('.recruit-button-tip').tipsy({live: true, gravity: 's', title: 'data-tooltip'});

    $('.remove-from-call-list').live('click', function() {

        if(!confirm('Confirm delete from the Dialer Call List.')) {
            return;
        }

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI

        $.post('/$moduleName/dialer/removeFromCallList', {
            callListId : $(this).data('id'),
            contactId : $(this).data('contactid'),
            componentTypeId : $(this).data('ctid'),
            componentId : $(this).data('cid')
        }, function(data) {
            if(data.status=='success') {

                Message.create('success','Successfully removed from the Dialer.');
                $('#call-list-tag-' + data.callListId ).remove();
                $.fn.yiiGridView.update("activity-log-grid");
            } else if (data.status=='error') {
                Message.create('error','Error removing from Call List.');
            }
        });
    });
JS
, CClientScript::POS_END);

	$this->title = $model->contact->fullName;
?>
<h1><?=$model->contact->fullName?></h1>
<div id="contact-header">
	<div class="submit-login-summary">
		<div class="p-fr p-rel">
			<span class="grey-textbox"><?php echo Yii::app()->format->formatDateDays($model->contact->last_login, array(
						'break' => false,
						'default' => 'n/a'
					)
				); ?></span><em class="bubble">Last Login</em>
		</div>
		<div class="p-fr p-rel">
			<span class="grey-textbox"><?php echo $model->contact->addedDateTime ?></span><em class="bubble">Submit Date</em>
		</div>
	</div>
</div>

<div class="btn-bar" style="margin-bottom: 20px;">
    <button class="text add-task-button" type="button" data="<?=$model->id?>" ctid="<?=$componentTypeId?>"><em class="icon i_stm_add"></em>Add Task</button>
    <button class="text add-activity-log-button" type="button" data="<?=$model->id?>" ctid="<?=$componentTypeId?>"><em class="icon i_stm_add"></em>Add Activity</button>
</div>

<div id="top-data" class="res-section res-group p-0">
	<div class="res-col span_8_of_12 p-0 p-fr">
        <?php
        $this->widget('zii.widgets.jui.CJuiTabs', array(
                'tabs' => array(
                    'Notes / Activities' => array(
                        'content' => ActivityLogPortlet::renderContentOnly($model),
                        'id' => 'activity-log-tab',
                    ),
                    'Tasks (' . $tasksCount . ')' => array(
                        'content' => $tasksContent,
                        'id' => 'tasks-tab',
                    ),
                    'Action Plans' => array(
                        'content' => $actionPlansContent,
                        'id' => 'action-plans-tab',
                    ),
                    'Docs' => array(
                        'ajax' => '/admin/documents/listMini/componentTypeId/' . $model->componentType->id.'/componentId/'.$model->id,
                        'id' => 'documents-tab',
                    ),
                ),
//                'options'=>array('active'=>$leftActiveTab),
                'id' => 'activity-details-widget',
            )
        );
        ?>
    </div>
	<div class="res-col span_4_of_12 p-0 p-fl">
		<?php
			$this->beginStmPortletContent(array(
					'handleTitle' => 'Recruit Details',
					'handleIconCss' => 'i_wizard',
					'id' => 'recruits',
					'handleButtons' => array(
						array(
							'label' => 'Edit',
							'iconCssClass' => 'i_stm_edit',
							'htmlOptions' => array('id' => 'edit-contact-button'),
							'name' => 'edit-contact',
							'type' => 'link',
							'link' => '/'.Yii::app()->controller->module->id.'/recruits/edit/' . $model->id,
						)
					),

				)
			);
		?>
		<div>
			<div class="rounded-text-box odd-static">
				<div class="g12">
					<table id="recruit-portlet" class="">
						<tbody>
                        <tr>
                            <th>Name:</th>
                            <td><?php echo $model->contact->fullName; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td>
                                <?php
                                if ($emails = $model->contact->emails) {
                                    foreach ($emails as $key => $EmailModel) {
                                        if ($EmailModel->email) {
                                            $primaryText = ($model->contact->getPrimaryEmail() == $EmailModel->email) ? ' <em class="icon icon-only i_stm_star"></em>' : '';

                                            echo '<div style="font-weight: bold;">';
                                            echo $EmailModel->email . $primaryText;
                                            $statusAlert = '';
                                            if($EmailModel->email_status_id == EmailStatus::OPT_OUT) {
                                                $statusAlert = 'Opted-out';
                                            } elseif($EmailModel->email_status_id == EmailStatus::HARD_BOUNCE) {
                                                $statusAlert = 'Bounced';
                                            }
                                            if($EmailModel->getIsUniversallyUndeliverable()) {
                                                $statusAlert .= ($statusAlert)? ', ': '';
                                                $statusAlert .= 'Undeliverable';
                                            }
                                            if($statusAlert) {
                                                echo '<div class="errorMessage" style="display:inline-block;">&nbsp;'.$statusAlert.'</div>';
                                            }
                                            echo '</div>';

                                            if(Yii::app()->user->checkAccess('bombBomb') && Yii::app()->user->settings->bombbomb_api_key) {
                                                echo ($EmailModel->isBombBomb)? '<em class="bombBomb"></em>':
                                                    '<a class="text-button add-bombBomb-email" href="javascript:void(0)" style="margin-left:0; display:inline-block;" data-id="'.$key.'"><em class="icon i_stm_add"></em>Add to BombBomb</a>';
                                                echo $this->bombBombLists();
                                                echo '<a href="javascript:void(0);" id="bombBombList-button-'.$key.'" class="text-button bombBombList-button hidden" data-id="'.$key.'"  data-email="'.$EmailModel->email.'">Add</a>';
                                            }
                                        }
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Phone:</th>
                            <td><?php
                                if ($phones = $model->contact->phones) {
                                    $hidePhones = (Yii::app()->user->settings->hide_phone_numbers_search_results) ? true : false;
                                    $primaryPhone = $model->contact->getPrimaryPhone();
                                    foreach ($phones as $PhoneModel) {
                                        $primaryText = ($primaryPhone == $PhoneModel->phone) ? ' <em class="icon icon-only i_stm_star"></em>' : '';

                                        if(Yii::app()->user->clientId == 0) {
                                            //$primaryText .= ' <button class="text click2callHelper" type="button" data-cid="'.$data->id.'" ctid="'.ComponentTypes::RECRUITS.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'" style="margin-left: 0;"><em class="icon i_stm_phone" style="width: 20px;"></em>CLICK-2-CALL</button>';
                                        }
                                        // this is for the dialog popup
                                        elseif(Yii::app()->user->checkAccess('clickToCall')) {
                                            $primaryText .= '<a class="action-button green  fa fa-volume-control-phone recruit-button-tip click-to-call-button" data-tooltip="Click to Call" data-cid="'.$model->id.'" ctid="'.ComponentTypes::RECRUITS.'" data-phone="'.Yii::app()->format->formatPhone($PhoneModel->phone).'"></a>';
                                            //                            $string .= ' <button class="text click-to-call-button" type="button" data-cid="'.$this->component_id.'" ctid="'.$this->componentType->id.'" data-phone="'.Yii::app()->format->formatPhone($phone->phone).'" style="margin-left: 0;"><em class="icon i_stm_phone" style="width: 20px;"></em>CLICK-2-CALL</button>';
                                        }

                                        // action button
                                        $primaryText .= '<a style="padding:4px 8px; display: inline;" class="text-button phone-status-button" href="javascript:void(0)" data-id="'.$PhoneModel->id.'" data-ctid="'.$componentTypeId.'" data-cid="'.$model->id.'" data-phone="'.Yii::app()->format->formatPhone($PhoneModel->phone).'"><em class="icon i_stm_add"></em>Action</a>';

                                        if ($PhoneModel->phone) {
                                            if ($PhoneModel->extension) {
                                                $opt = array('extension' => $PhoneModel->extension);
                                            }
                                            echo (($hidePhones)? '***-***-**'.substr($phone->phone, -2, 2): Yii::app()->format->formatPhone($PhoneModel->phone, $opt).$primaryText);
                                            if($AreaCodeInfo = $PhoneModel->getAreaCodeInfo()) {
                                                $phoneText = '<br /><span style="font-weight: normal; color: #555;">';
                                                $phoneText .= ($AreaCodeInfo->city)?$AreaCodeInfo->city.', ':'';
                                                $phoneText .= $AreaCodeInfo->state.'</span><br />';
                                                echo $phoneText;
                                            }
                                            echo '<br />';
                                        }
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <?php if ($addresses = $model->contact->addresses) { ?>
                            <tr>
                                <th>Address:</th>
                                <td>
                                    <div class="contact-portlet-address">
                                        <?php
                                        if ($addresses) {
                                            $primaryAddress = $model->contact->getPrimaryAddress();
                                            foreach ($addresses as $AddressModel) {
                                                if ($AddressModel->address) {
                                                    $primaryText = ($primaryAddress->id == $AddressModel->id) ? ' <em class="icon icon-only i_stm_star"></em>' : '';
                                                    echo '<p>' . Yii::app()->format->formatAddress($AddressModel) . $primaryText.'</p>';
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
<!--                        <tr>-->
<!--                            <th>Contact Notes:</th>-->
<!--                            <td>--><?php //echo Yii::app()->format->formatTextArea($model->contact->notes); ?><!--</td>-->
<!--                        </tr>-->
                        <tr><td colspan="2"><hr/></td></tr>
                        <tr>
                                <th>Status:</th>
                                <td>
                                    <?php echo $model->statusName; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Met:</th>
                                <td><?php echo StmFormHelper::getYesNoName($model->met_status_ma); ?></td>
                            </tr>
                            <tr>
                                <th>Agent 12 mo Volume:</th>
                                <td>
                                    <?php echo Yii::app()->format->formatDollars($model->annual_volume); ?>
                                    <span class="label" style="margin-left: 8px;">$ Change: </span> <?php echo ($model->annual_volume_change) ? Yii::app()->format->formatDollars($model->annual_volume_change) : ''; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Agent 12 mo Units:</th>
                                <td>
                                    <?php echo ($model->annual_units) ? $model->annual_units : ''; ?>
                                    <span class="label" style="margin-left: 8px;">% Change: </span> <?php echo ($model->annual_volume_change_percent) ? $model->annual_volume_change_percent.'%' : ''; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Ranking #:</th>
                                <td>
                                    <?php echo ($model->ranking) ? $model->ranking : ''; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Production Data Updated:</th>
                                <td>
                                    <?php echo ($model->annual_data_updated) ? Yii::app()->format->formatDate($model->annual_data_updated) : ''; ?>
                                </td>
                            </tr>

                            <tr>
                                <th>Current Brokerage:<?=($model->office_city) ? '<br>City:' : ""?></th>
                                <td>
                                    <?php echo $model->current_brokerage; ?>
                                    <?php echo (($model->current_brokerage) ? "<br>" : "").$model->office_city; ?><?php echo (($model->office_city) ? "<span class='label' style='margin-left: 10px;'>Zip: </span> " : "").$model->office_zip; ?>
                                </td>
                            </tr>
<!--                            <tr class="p-p10">-->
<!--                                <th class="narrow">Recruit Notes:</th>-->
<!--                                <td>--><?php //echo nl2br($model->notes); ?><!--</td>-->
<!--                            </tr>-->
                            <?if(Yii::app()->user->checkAccess('dialer') && in_array($componentTypeId, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS))):?>
                                <tr>
                                    <th>Dialer Lists:</th>
                                    <td>
                                        <div id="call-list-tag-container">
                                            <?php

                                            switch($componentTypeId) {
                                                case ComponentTypes::SELLERS:
                                                    $preset = CallLists::CUSTOM_SELLERS;
                                                    break;

                                                case ComponentTypes::BUYERS:
                                                    $preset = CallLists::CUSTOM_BUYERS;
                                                    break;

                                                case ComponentTypes::RECRUITS:
                                                    $preset = CallLists::CUSTOM_RECRUITS;
                                                    break;

                                                default:
                                                    throw new Exception(__CLASS__.' (:'.__LINE__.') Invalid Component Type Id');
                                                    break;
                                            }

                                            $dialerLists = Yii::app()->db->createCommand()
                                                ->select('clp.call_list_id call_list_id, l.name name, c.id contact_id')
                                                ->from('call_list_phones clp')
                                                ->join('call_lists l', 'l.id=clp.call_list_id')
                                                ->join('phones p', 'p.id=clp.phone_id')
                                                ->join('contacts c', 'c.id=p.contact_id')
                                                ->where('c.id='.$model->contact->id)
                                                ->andWhere('l.preset_ma='.$preset)
                                                ->andWhere('clp.is_deleted=0')
                                                ->andWhere('l.is_deleted=0')
                                                ->andWhere('clp.hard_deleted IS NULL')
                                                ->group('clp.call_list_id')
                                                ->queryAll();

                                            foreach($dialerLists as $dialerList) {
                                                ?>
                                                <div id="call-list-tag-<?=$dialerList['call_list_id']?>" class="call-list-tag"><span class="label"><?=$dialerList['name']?></span><span class="remove-from-call-list" data-id="<?=$dialerList['call_list_id']?>" data-contactid="<?=$dialerList['contact_id']?>" data-ctid="<?=$componentTypeId?>" data-cid="<?=$componentTypeId?>">X</span></div>
                                            <?
                                            }
                                            ?>
                                        </div>
                                        <button class="text add-to-dialer-button" style="display: inline-block; margin-left: -4px;" data-ctid="<?=$componentTypeId?>" data-cid="<?=$model->id?>" data-contactid="<?=$model->contact->id?>"><em class="icon i_stm_add"></em>Add to Dialer</button>
                                    </td>
                                </tr>
                            <? endif;?>

                            <? if($Appointment):?>
                                <tr>
                                    <th colspan="2" style="text-align: center;"><br><u>APPOINTMENT</u></th>
                                </tr>
                                <tr>
                                    <th>Appt Set by:</th>
                                    <td colspan="3">
                                        <?php echo $model->appointment->setBy->fullName; ?>
                                        <?php if ($Appointment->set_on_datetime > 0) {
                                            echo '<br/><label class="label p-p5">@</label>';
                                        } ?>
                                        <?php echo Yii::app()->format->formatDateTime($Appointment->set_on_datetime); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Appt Set Method:</th>
                                    <td colspan="3">
                                        <?php echo $Appointment->taskType->name; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Appt Set for:</th>
                                    <td colspan="3">
                                        <?php echo $model->appointment->metBy->fullName; ?>
                                        <?php if ($Appointment->set_for_datetime > 0) {
                                            echo '<br/><label class="label p-p5">@</label>';
                                        } ?>
                                        <?php echo Yii::app()->format->formatDateTime($Appointment->set_for_datetime); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Appt Location:</th>
                                    <td><?php echo $Appointment->getModelAttribute("getLocationTypes", $Appointment->location_ma); ?><?php if ($Appointment->location_other) {
                                            echo ': ' . $Appointment->location_other;
                                        } ?></td>
                                </tr>
                                <tr>
                                    <th>Appt Status:</th>
                                    <td><?php echo $Appointment->getModelAttribute("getMetStatusTypes", $Appointment->met_status_ma); ?>
                                        <?php echo ($Appointment->met_status_ma == Appointments::MET_STATUS_CANCEL || $Appointment->met_status_ma == Appointments::MET_STATUS_RESCHEDULE)? ' Reason: '.$Appointment->met_status_reason : ''; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Signed:</th>
                                    <td>
                                        <?php echo StmFormHelper::getYesNoName($model->appointment->is_signed); ?><?php if ($Appointment->not_signed_reason) {
                                            echo ': ' . $Appointment->getModelAttribute("getNotSignedBuyerReasonTypes", $Appointment->not_signed_reason);
                                        } ?><?php if ($Appointment->not_signed_reason_other) {
                                            echo ': ' . $Appointment->not_signed_reason_other;
                                        } ?>
                                    </td>
                                </tr>
                            <? endif;?>
                            <tr><td colspan="2"><hr/></td></tr>
                            <tr>
                                <th>Source:</th>
                                <td><?php echo $model->source->name; ?></td>
                            </tr>
                            <tr>
                                <th>Tags:</th>
                                <td><?php echo Yii::app()->format->formatCommaDelimited($model->types, "name"); ?></td>
                            </tr>
                            <tr>
                                <th>Assignments:</th>
                                <td>
                                    <?
                                    $assignmentsString = '';
                                    if($model->assignments) {
                                        foreach($model->assignments as $i => $assignment) {
                                            $assignmentsString .= ($assignmentsString) ? '<br>': '';
                                            $assignmentsString .= $assignment->contact->fullName .' ('.$assignment->assignmentType->display_name.')';
                                        }
                                    }
                                    if($model->referrers) {
                                        foreach($model->referrers as $i => $referrer) {
                                            $assignmentsString .= ($assignmentsString) ? '<br>': '';
                                            $assignmentsString .= $referrer->contact->fullName .' ('.$referrer->assignmentType->display_name.')';
                                        }
                                    }
                                    echo $assignmentsString;
                                    ?>
                                </td>
                            </tr>
                        <tr>
                            <th>Spouse Info:</th>
                            <td><?php
                                $spouseString = $model->spouse_first_name." ".$model->spouse_last_name;
                                $spouseString .= ($model->spouse_email) ? '<br>'.$model->spouse_email : "";
                                $spouseString .= ($model->spouse_phone) ? '<br>'.$model->spouse_phone : "";
                                echo $spouseString;
                                ?>
                            </td>
                        </tr>
                        </tbody>
					</table>
				</div>
			</div>
		</div>
		<?php $this->endStmPortletContent(); ?>
	</div>
</div>

<?php
    // ====== DIALOGS ===============================================================================================
	// Document Dialog
	$this->widget('admin_widgets.DialogWidget.DocumentsDialogWidget.DocumentsDialogWidget', array(
            'id' => 'add-document-dialog',
            'parentModel' => $model,
            'title' => 'Add New Document',
            'componentType' => $model->componentType,
            'triggerElement' => '#add-document-button, .edit-document-button',
            // Trigger for Add Document Button (See DocumentsPortlet.DocumentsPortlet.php)
        )
    );

	// Action Plans
	$this->widget('admin_widgets.DialogWidget.ActionPlanDialogWidget.ActionPlanDialogWidget', array(
            'id' => 'action-plan-dialog',
            'parentModel' => $model,
            'title' => 'Add Action Plan',
            // 'componentType'  => $componentType,
            'triggerElement' => '#add-action-plan-button',
            // Trigger for Add Action Plan
        )
    );

	Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
	$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
            'id' => TaskDialogWidget::TASK_DIALOG_ID,
            'title' => 'Create New Task',
            'triggerElement' => '#add-task-button, .add-task-button, .edit-task-button, .delete-task-button',
            'parentModel' => $model,
        )
    );

    if(in_array($componentTypeId, array(ComponentTypes::SELLERS, ComponentTypes::BUYERS, ComponentTypes::RECRUITS)) && Yii::app()->user->checkAccess('dialer')) {
        Yii::import('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget');
        if (!class_exists('DialerListDialogWidget', false)) {

            Yii::app()->clientScript->registerScript('dialerAddToListJs', <<<JS
            function postAddToDialerList(data) {
                // return if there's no added data for some reason
                if(data.added == undefined) {
                    Message.create("error", 'No phones were added to the Call List. Please verify that phone numbers exist for this contact.');
                    return;
                }
                var callListTag = '<div id="call-list-tag-'+ data.added.call_list_id +'" class="call-list-tag"><span class="label">'+ data.added.name +'</span><span class="remove-from-call-list" data-id="'+ data.added.call_list_id +'" data-pid="' + data.added.phoneId + '" data-ctid="' + data.added.componentTypeId + '" data-cid="' + data.added.componentId + '" data-contactid="' + data.added.contactId + '">X</span></div>';
                $('#call-list-tag-container').append(callListTag);
            }
JS
            );
            $this->widget('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget', array(
                    'id' => DialerListDialogWidget::DIALOG_ID,
                    'title' => 'Add to Dialer',
                    'triggerElement' => '.add-to-dialer-button',
                    'postAddFunction' => 'postAddToDialerList',
                )
            );
        }
    }

    Yii::import('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget');
    if (!class_exists('ClickToCallDialogWidget', false)) {
        $this->widget('admin_widgets.DialogWidget.ClickToCallDialogWidget.ClickToCallDialogWidget', array(
                'id' => ClickToCallDialogWidget::CLICK_TO_CALL_DIALOG_ID,
                'title' => 'Click to Call',
                'triggerElement' => '.click-to-call-button',
            )
        );
    }

    Yii::import('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget');
    $this->widget('admin_widgets.DialogWidget.PhoneDialogWidget.PhoneDialogWidget', array(
            'id' => PhoneDialogWidget::PHONE_DIALOG_ID,
            'title' => 'Phone Actions',
            'triggerElement' => '.phone-status-button',
            //        'parentModel' => $model,
        )
    );