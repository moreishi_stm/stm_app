<?
$this->breadcrumbs = array(
	'Send Referral',
);
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'recruit-form',
        'action'               => array('/admin/recruits/sendReferral/formId/' . Forms::RECRUIT_REFERRAL),
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnChange' => false,
            'validateOnSubmit' => true,
            'beforeValidate' => 'js:function(form, attribute) {
                    $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                    return true;
                }',
            'afterValidate' => 'js:function(form, data, hasErrors) {

                if (!hasErrors && data.status =="success") {

                    $("#form-table, #submit-button-wrapper").hide();
                    $("#success-message-container").show("normal");
                }

                $("div.loading-container.loading").remove();
                return false;
            }',
        ),
    )
);
$this->beginStmPortletContent(array(
        'handleTitle' => 'New Agent Referral',
        'handleIconCss' => 'i_wizard'
    )
);
?>
<div id="referral-container">
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <div class="g3"></div>
        <div class="g6">
            <table id="form-table" class="container">
                <tr>
                    <th class="narrow"><label class="required">Name <span class="required">*</span></label></th>
                    <td>
                        <div class="g4">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('first_name').']', $htmlOptions=array('placeholder'=> 'First Name', 'class' => 'g100')); ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('first_name').']'); ?>
                        </div>
                        <div class="g4">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('last_name').']', $htmlOptions=array('placeholder'=> 'Last Name', 'class' => 'g100')); ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('last_name').']'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><label class="required">Email <span class="required">*</span></label></th>
                    <td>
                        <div>
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('email').']', $htmlOptions=array('placeholder'=> 'Email', 'class' => 'g8')); ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('email').']'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><label class="required">Phone <span class="required">*</span></label></th>
                    <td>
                        <div>
                            <?php $this->widget('StmMaskedTextField', array(
                                    'model' => $SubmissionValues,
                                    'attribute' => 'data[' . $FormFields->getFieldIdByName('phone') . ']',
                                    'mask' => '(999) 999-9999',
                                    'htmlOptions' => $htmlOptions = array('placeholder' => 'Phone', 'class' => 'g8'),
                                ));
                            ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('phone').']'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow">Brokerage:</th>
                    <td>
                        <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('current_brokerage').']', $htmlOptions=array('placeholder'=> 'Brokerage', 'class' => 'g8')); ?>
                        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('current_brokerage').']'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><label class="required">Licensed <span class="required">*</span></label></th>
                    <td>
                        <div>
                            <?php echo $form->dropDownList($SubmissionValues,  'data['.$FormFields->getFieldIdByName('is_licensed').']', StmFormHelper::getYesNoList(), $htmlOptions=array('empty'=> '', 'class' => 'g8'));
                            //array('Not Licensed'=>'Not Licensed','In School'=>'In School','New'=>'New','1/4 Cap'=>'1/4 Cap','1/2 Cap'=>'1/2 Cap','Capper'=>'Capper','Mega'=>'Mega') ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('is_licensed').']'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><label class="required">Connection <span class="required">*</span></label></th>
                    <td>
                        <div>
                            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getFieldIdByName('relationship').']', array('Co-op'=>'Co-op','Networking'=>'Networking','Personal Sphere'=>'Personal Sphere','Family'=>'Family','Other'=>'Other'), $htmlOptions=array('empty'=> 'How do you know them?', 'class' => 'g8')); ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('relationship').']'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow"><label class="required">Expecting Call <span class="required">*</span></label></th>
                    <td>
                        <div>
                            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getFieldIdByName('is_expecting_call').']', array(1=>'Yes',0=>'No'), $htmlOptions=array('empty'=> '', 'class' => 'g8')); ?>
                            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('is_expecting_call').']'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="narrow">Notes:</th>
                    <td>
                        <?php echo $form->textArea($SubmissionValues, 'data['.$FormFields->getFieldIdByName('comments').']', $htmlOptions=array('placeholder'=> 'Comments', 'class' => 'g8','rows'=>4)); ?>
                        <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('comments').']'); ?>
                        <?php //echo $form->hiddenField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('referred_by').']'); ?>

                    </td>
                </tr>
            </table>
        </div>
        <div id="success-message-container" style="display: none; height: 200px; padding-top: 60px;" class="g12">
            <h1>Thank you! Your Referral has been successfully submitted.</h1>
            <div style="text-align: center; padding-top: 30px;">
                <a href="/admin/recruits/referral" class="button gray icon i_stm_add">Refer a New Lead</a>
                <a href="/admin/recruits" class="button gray icon i_stm_search">View All Recruits</a>
            </div>
        </div>
    </div>
</div>

<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <button type="submit" class="submit wide">Submit</button>
</div>
<?php $this->endWidget(); ?>
