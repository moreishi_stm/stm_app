<?php
$form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
		'id' => 'recruit-list-search',
	)
); ?>
	<div class="g3 p-clr">
		<label class="g4">Name:</label>
		<span class="g4"><?php echo $form->textField($model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name','class'=>'g11')); ?></span>
		<span class="g4"><?php echo $form->textField($model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name','class'=>'g12')); ?></span>
	</div>
	<div class="g3">
		<label class="g2">Email:</label>
		<span class="g10"><?php echo $form->textField($model->contact->emails, 'email'); ?></span>
	</div>
	<div class="g3">
		<label class="g4">Phone:</label>
        <span class="g8">
          <?php $this->widget('StmMaskedTextField', array(
				  'model' => $model->contact->phones,
				  'attribute' => 'phone',
				  'mask' => '(999) 999-9999',
				  //Makes the full 10 digit phone number optional (enables partial search)
				  'htmlOptions' => array(
					  'placeholder' => '(999) 123-1234',
					  'class' => 'phoneField'
				  )
			  )
		  ); ?>
        </span>
	</div>

	<div class="g1 submit" style="text-align:center">
		<?php echo CHtml::submitButton('SEARCH', array('class' => 'button',"id"=>"recruitSubmitButton")); ?>
	</div>
<? if(Yii::app()->user->checkAccess('owner')): ?>
    <div class="g2 submit p-fr" style="font-variant: small-caps;min-height: 30px;font-size: 11px;">
        <input type="hidden" name="export" value="" id="export_csv">
        <?php echo CHtml::submitButton('EXPORT', array('class' => 'gray button',"id"=>"export_csv_button")); ?>
    </div>
<? endif; ?>

	<div class="advanced-search-container" style="<?=($_COOKIE['recruit_advanced_search_visible'] == 1) ? "" : "display: none;" ?>">
		<div class="g10 p-clr">
			<div class="g1 text-right"><label>Status:</label></div>
			<div class="g11">
				<?php echo $form->dropDownList(
					$model, 'status_id', CHtml::listData(TransactionStatus::model()->byComponentType(ComponentTypes::RECRUITS)->defaultSort()->findAll(), 'id', 'displayName'),
					$htmlOptions = array(
                        'empty'            => '',
						'class'            => 'chzn-select',
						'multiple'         => 'multiple',
						'style'            => 'width:100%;',
						'data-placeholder' => 'Status'
					)
				); ?>
				<a class="status-unselect-all" href="javascript:void(0)">Unselect All</a>
                <a class="status-select-all" style="margin-left: 12px;" href="javascript:void(0)">Select All</a>
<!--				<a class="status-select-abc" style="margin-left: 12px;" href="javascript:void(0)">Select A, B, C</a>-->
<!--				<a class="status-select-a" style="margin-left: 12px;" href="javascript:void(0)">Select A</a>-->
<!--				<a class="status-select-b" style="margin-left: 12px;" href="javascript:void(0)">Select B</a>-->
<!--				<a class="status-select-c" style="margin-left: 12px;" href="javascript:void(0)">Select C</a>-->
				<?php $this->widget(
					'admin_module.extensions.EChosen.EChosen', array('target' => 'select#Recruits_status_id','options' => array('width'=>'100%'))
				); ?>
                <?=$form->hiddenField($model, 'myMarketCenter')?>
			</div>
		</div>
		<?if(!Yii::app()->user->checkAccess(AuthItem::ROLE_RECRUIT_REFERRING_AGENT)):?>
			<div class="g3 p-clr">
				<label class="g4">12 mo $ Volume:</label>
				<span class="g3"><?php echo $form->textField($model, 'annualVolumeMin', $htmlOptions=array('placeholder'=>'Volume Min')); ?></span>
				<span class="g1 p-tc">to</span>
				<span class="g3"><?php echo $form->textField($model, 'annualVolumeMax', $htmlOptions=array('placeholder'=>'Volume Max')); ?></span>
			</div>
			<!--            <div class="g4">-->
			<!--                <label class="g4">12 Mo # Units:</label>-->
			<!--                <span class="g3">--><?php //echo $form->textField($model, 'annualUnitsChangeMin', $htmlOptions=array('placeholder'=>'Units Min')); ?><!--</span>-->
			<!--                <span class="g1 p-tc">to</span>-->
			<!--                <span class="g3">--><?php //echo $form->textField($model, 'annualUnitsChangeMax', $htmlOptions=array('placeholder'=>'Units Max')); ?><!--</span>-->
			<!--            </div>-->
            <div class="g4">
                <label class="g4">Source:</label>
                <span class="g8">
                    <?php echo $form->dropDownList($model, 'source_id', CHtml::listData(Sources::model()->findAll(),'id', 'name'), $htmlOptions=array('empty'=>'', 'class'=>'g11')); ?>
                </span>
            </div>
			<div class="g4">
				<label class="g4">License Date:</label>
                <span class="g8">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'model' => $model,
							'attribute' => 'fromLicenseDate',
							// name of post parameter
							'options' => array(
								'showAnim' => 'fold',
								'dateFormat' => 'mm/dd/yy',
								'numberOfMonths' => 2,
							),
							'htmlOptions' => array(
								'style' => 'font-size: 12px;',
								'class' => 'g5',
							),
						)
					);
					?>
					<label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'model' => $model,
							'attribute' => 'toLicenseDate',
							// name of post parameter
							'options' => array(
								'showAnim' => 'fold',
								'dateFormat' => 'mm/dd/yy',
								'numberOfMonths' => 2,
							),
							'htmlOptions' => array(
								'style' => 'font-size: 12px;',
								'class' => 'g5',
							),
						)
					);
					?>
                </span>
			</div>

			<div class="g3 p-clr">
				<label class="g4">Change % Volume:</label>
				<span class="g3"><?php echo $form->textField($model, 'annualVolumeChangePercentMin', $htmlOptions=array('placeholder'=>'%')); ?></span>
				<span class="g1 p-tc">to</span>
				<span class="g3"><?php echo $form->textField($model, 'annualVolumeChangePercentMax', $htmlOptions=array('placeholder'=>'%')); ?></span>
			</div>
			<!--            <div class="g4">-->
			<!--                <label class="g4">Change # Units:</label>-->
			<!--                <span class="g3">--><?php //echo $form->textField($model, 'annualUnitsChangeMin', $htmlOptions=array('placeholder'=>'#')); ?><!--</span>-->
			<!--                <span class="g1 p-tc">to</span>-->
			<!--                <span class="g3">--><?php //echo $form->textField($model, 'annualUnitsChangeMax', $htmlOptions=array('placeholder'=>'#')); ?><!--</span>-->
			<!--            </div>-->
            <div class="g4">
                <label class="g4">Tag:</label>
                <span class="g8"><?php echo $form->dropDownList($model, 'typesCollection', CHtml::listData(RecruitTypes::model()->findAll(array('order'=>'name')), 'id', 'name'), array(
                            'class' => 'chzn-select',
                            'multiple' => 'multiple',
                            'style' => 'width:100%;',
                            'data-placeholder' => 'Recruit Tags'
                        )
                    ); ?>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Recruits_typesCollection','options' => array('width'=>'91%'))); ?>
                </span>
            </div>
			<div class="g4">
				<label class="g4">Added Date:</label>
                <span class="g8">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'model' => $model,
							'attribute' => 'addedFromDate',
							// name of post parameter
							'options' => array(
								'showAnim' => 'fold',
								'dateFormat' => 'mm/dd/yy',
								'numberOfMonths' => 2,
							),
							'htmlOptions' => array(
								'style' => 'font-size: 12px;',
								'class' => 'g5',
							),
						)
					);
					?>
					<label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'model' => $model,
							'attribute' => 'addedToDate',
							// name of post parameter
							'options' => array(
								'showAnim' => 'fold',
								'dateFormat' => 'mm/dd/yy',
								'numberOfMonths' => 2,
							),
							'htmlOptions' => array(
								'style' => 'font-size: 12px;',
								'class' => 'g5',
							),
						)
					);
					?>
                </span>
			</div>
			<div class="g3 p-clr">
				<label class="g4">Brokerage Name:</label>
				<span class="g7"><?php echo $form->textField($model, 'current_brokerage', $htmlOptions=array('placeholder'=>'Brokerage Name')); ?></span>
			</div>
            <div class="g4">
                <label class="g4">Exclude Tag:</label>
                <span class="g8"><?php echo $form->dropDownList($model, 'excludeTypesCollection', CHtml::listData(RecruitTypes::model()->findAll(array('order'=>'name')), 'id', 'name'), array(
                            'class' => 'chzn-select',
                            'multiple' => 'multiple',
                            'style' => 'width:100%;',
                            'data-placeholder' => 'Exclude Tags'
                        )
                    ); ?>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Recruits_excludeTypesCollection','options' => array('width'=>'91%'))); ?>
                </span>
            </div>
			<div class="g4">
				<label class="g4">Order by:</label>
				<label class="g8">
					<?php echo $form->dropDownList($model, 'dateSearchFilter', array('status_asc' => 'Status: Hottest first'/*,'volume_desc' => 'High to Low Volume'*/), $htmlOptions = array(
						'empty' => '',
						'class'=>'g11',
					)
					); ?>
				</label>
			</div>

			<div class="g3 p-clr">
				<label class="g4">City:</label>
				<span class="g7"><?php echo $form->textField($model, 'office_city', $htmlOptions=array('placeholder'=>'City')); ?></span>
			</div>
            <div class="g4">
                <label class="g4">Exclude Brokerages:</label>
                <span class="g7"><?php echo $form->textField($model, 'excludeBrokerageName', $htmlOptions=array('placeholder'=>'Exclude Brokerages (comma separated)')); ?></span>
            </div>
			<div class="g4">
				<label class="g4">DISC:</label>
                <span class="g8">
                    <?php echo $form->dropDownList($model, 'disc_D', StmFormHelper::getDropDownList(array(
							'start' => 1,
							'end' => 99,
							'increment' => 1,
							'label' => '+ (D)'
						)
					), $htmlOptions = array(
						'empty' => 'D',
						'class' => 'g3'
					)
					); ?>
					<?php echo $form->dropDownList($model, 'disc_I', StmFormHelper::getDropDownList(array(
							'start' => 1,
							'end' => 99,
							'increment' => 1,
							'label' => '+ (I)'
						)
					), $htmlOptions = array(
						'empty' => 'I',
						'class' => 'g3'
					)
					); ?>
					<?php echo $form->dropDownList($model, 'disc_S', StmFormHelper::getDropDownList(array(
							'start' => 1,
							'end' => 99,
							'increment' => 1,
							'label' => '+ (S)'
						)
					), $htmlOptions = array(
						'empty' => 'S',
						'class' => 'g3'
					)
					); ?>
					<?php echo $form->dropDownList($model, 'disc_C', StmFormHelper::getDropDownList(array(
							'start' => 1,
							'end' => 99,
							'increment' => 1,
							'label' => '+ (C)'
						)
					), $htmlOptions = array(
						'empty' => 'C',
						'class' => 'g3'
					)
					); ?>

                </span>
			</div>

			<div class="g3 p-clr">
				<label class="g4">Zip:</label>
				<span class="g7"><?php echo $form->textField($model, 'office_zip', $htmlOptions=array('placeholder'=>'Zip1, Zip2, Zip3')); ?></span>
			</div>
			<div class="g4">
				<label class="g4">Dialer List:</label>
				<label class="g8">
					<? $callListCriteria = new CDbCriteria();
					$callListCriteria->addInCondition('preset_ma', array(CallLists::CUSTOM_RECRUITS));
					$callListCriteria->addCondition('is_deleted=0');
					$callListCriteria->order = 'name ASC';
					?>
					<?php echo $form->dropDownList($model, 'callListId', CHtml::listData(CallLists::model()->findAll($callListCriteria), 'id', 'name'), $htmlOptions = array('empty'=>' ', 'class'=>'g11')); ?>
				</label>
			</div>
            <div class="g4">
                <label class="g4">Assigned to:</label>
                <span class="g8"><?echo $form->dropDownList($model, 'assignmentId', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->assignmentId)->byAssignmentComponentType(ComponentTypes::RECRUITS)->orderByName()->findAll(), 'id','fullName'), $htmlOptions = array('empty' => '', 'class'=>'g11'));?>
                </span>
            </div>
			<div class="g3">
				<label class="g4">Met Status:</label>
				<span class="g7"><?php echo $form->dropDownList($model, 'met_status_ma', array( 1=> 'Met', 0 => 'Not Met'), $htmlOptions=array('empty'=>'')); ?></span>
			</div>
			<div class="g4">
				<label class="g4">Mass Action</label>
				<span class="g8">
					<select id="MassAction_action" name="MassAction[action]">
						<option>Select an action</option>
						<option value="addTag">Tags: Apply</option>
						<option value="removeTag">Tags: Remove</option>
						<option value="applyActionPlan">Action Plans: Apply</option>
                        <option value="unapplyActionPlan">Action Plan: Remove</option>
                        <option value="status">Update Status</option>
					</select>
				</span>
				<span id="MassAction_action_tag_options" class="g8 massActionOptions" style="display: none">
					<?php
					echo CHtml::dropDownList(
						'MassAction[options]',
						$models->typesCollection,
						CHtml::listData(RecruitTypes::model()->findAll(), 'id', 'name'),
						array(
                            'id'               => "MassAction_options",
                            'empty'            => '',
                            'class'            => 'chzn-select massActionChzn',
                            'multiple'         => 'multiple',
                            'style'            => 'width:100%;',
                            'data-placeholder' => ' ',
                        )
					);
					?>
					<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#MassAction_options','options' => array('width'=>'80%'))); ?>
					<button type="button" class="ajaxMassAction">Go</button>
                </span>
				<span id="MassAction_action_plan_options" class="g8 massActionOptions" style="display: none">
					<?php
					echo CHtml::dropDownList(
						'MassAction[plan_options]',
						$models->typesCollection,
						CHtml::listData(
							ActionPlans::model()
								->byComponentType(ComponentTypes::RECRUITS)
								->byStatus(StmFormHelper::ACTIVE)
								->orderByName()
								->findAll(),
							'id',
							'name'
						),
						array(
                            'empty' => '',
							'class' => 'chzn-select massActionChzn',
							'style' => 'width:100%;',
							'data-placeholder' => 'Select an Action Plan',
							'id' => "MassAction_plan_options"
						)
					);
					?>
					<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#MassAction_plan_options','options' => array('width'=>'80%'))); ?>
					<button type="button" class="ajaxMassAction">Go</button>
                </span>
				<span id="MassAction_action_status_options" class="g8 massActionOptions" style="display: none">
					<?php
                    echo CHtml::dropDownList(
                        'MassAction[status_options]',
                        $models->typesCollection,
                        CHtml::listData(TransactionStatus::model()->byComponentType(ComponentTypes::RECRUITS)->defaultSort()->findAll(), 'id', 'displayName'),
                        array(
                            'empty' => '',
                            'class' => 'chzn-select massActionChzn',
                            'style' => 'width:100%;',
                            'data-placeholder' => 'Status',
                            'id' => "MassAction_status_options"
                        )
                    );
                    ?>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#MassAction_status_options','options' => array('width'=>'80%'))); ?>
                    <button type="button" class="ajaxMassAction">Go</button>
                </span>
			</div>
            <div class="g4">
                <label class="g4">Set Appointment:</label>
                <label class="g4">
                    <?php echo $form->dropDownList($model, 'hasAppointment', StmFormHelper::getYesNoList(), $htmlOptions = array(
                            'empty' => '',
                            'style'=>'width:93%; float: left;',
                        )
                    ); ?>
                </label>
            </div>
            <div class="g3 p-clr">
                <div class="g4 text-right"><label>No Action Plan:</label></div>
                <div class="g7">
                    <?php echo CHtml::checkbox('noActionPlanCheckbox',false ,$htmlOptions=array(
                            'value'=>1,
                            'style'=>'font-size: 16px; position: relative; top: 10px;',
                        )); ?>
                    <?php echo CHtml::hiddenField('noActionPlan', null); ?>
                </div>
            </div>
            <div class="g4">
                <div class="g4 text-right"><label>No Tasks:</label></div>
                <div class="g8">
                    <?php echo CHtml::checkbox('noTasksCheckbox',false ,$htmlOptions=array(
                            'value'=>1,
                            'style'=>'font-size: 16px; position: relative; top: 10px;',
                        )); ?>
                    <?php echo CHtml::hiddenField('noTasks', null); ?>
                </div>
            </div>
            <div class="g4">
                <div class="g4 text-right">
                    <label>Action Plan Applied:</label>
                </div>
                <div class="g8">
                    <?
                    $actionPlans2Criteria = new CDbCriteria();
                    $actionPlans2Criteria->addInCondition('component_type_id', array(ComponentTypes::RECRUITS));
                    $actionPlans2Criteria->order = 'name';

                    $actionPlanResults = Yii::app()->db->createCommand()
                        ->select('ap.id, ap.name')
                        ->from('action_plans ap')
                        ->where("(SELECT count(*) FROM tasks tsk LEFT JOIN action_plan_applied_lu lu ON tsk.id=lu.task_id LEFT JOIN action_plan_items i ON i.id=lu.action_plan_item_id LEFT JOIN action_plans ap2 ON ap2.id=i.action_plan_id WHERE tsk.component_type_id IN (".ComponentTypes::RECRUITS.") AND tsk.complete_date IS NULL AND tsk.is_deleted=0) > 0")
                        ->order('name ASC')
                        ->queryAll();
                    $actionPlanAppliedList = array_column($actionPlanResults,'name','id');
                    ?>
                    <?php echo $form->dropDownList($model, 'actionPlanId', $actionPlanAppliedList, $htmlOptions = array('empty'=>' ', 'class'=>'g12', 'data-placeholder'=>' ')); ?>
                    <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => '#Recruits_actionPlanId','options' => array('enable_split_word_search'=>true, 'allow_single_deselect' => true))); ?>
                </div>
            </div>

			<?/*    <div class="g4">
            <label class="g3">AVA:</label>
            <span class="g9">
                <?php echo $form->dropDownList($model, 'ava_vector_1', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => '+ (V1)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V1',
                        'class' => 'g2'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_2', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => '+ (V2)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V2',
                        'class' => 'g2'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_3', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => '+ (V3)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V3',
                        'class' => 'g2'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_4', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => '+ (V4)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V4',
                        'class' => 'g2'
                    )
                ); ?>
                <?php echo $form->dropDownList($model, 'ava_vector_5', StmFormHelper::getDropDownList(array(
                            'start' => 1,
                            'end' => 9,
                            'increment' => 1,
                            'label' => '+ (V5)'
                        )
                    ), $htmlOptions = array(
                        'empty' => 'V5',
                        'class' => 'g2'
                    )
                ); ?>
            </span>
        </div>
?*/?>
			<hr class="g12" />
			<div class="g12">
				<label class="g1">Dialer Action:</label>
            	<span class="g10">
					<?php echo CHtml::dropDownList('dialerAction', null, array('add'=>'Add to Dialer List', 'remove'=>'Remove from Dialer List'), $htmlOptions=array('empty'=>'')); ?>
					<?php echo CHtml::dropDownList('dialerListId', null, CHtml::listData(CallLists::model()->findAll(array('condition'=>'preset_ma='.CallLists::CUSTOM_RECRUITS, 'order'=>'name ASC')), 'id','name'), $htmlOptions=array('empty'=>'')); ?>
					<?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button dialer-button','style'=>'font-size: 10px; padding: 8px 10px;'))?>
					<?php echo CHtml::hiddenField('dialerProcess', null, $htmlOptions=array('value'=>'0'))?>
				</span>
			</div>
			<? if(Yii::app()->user->checkAccess('emails') && (strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') !== false || strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') !== false || strpos($_SERVER['SERVER_NAME'], 'christineleeteam') !== false)):?>
				<hr class="g12" />
				<div id="start-email-blast-button-container" class="g12">
					<div class="g3">
						<label class="g3"></label>
						<span class="g9">
							<?php echo CHtml::button('START EMAIL BLAST', array('class'=>'button g12','id'=>'start-email-blast-button', 'style'=>'width: 100%;')); ?>
						</span>
					</div>
				</div>
				<div id="start-email-blast-row" class="g12" style="display: none;">
					<div class="g3">
						<label class="g3">Email Template:</label>
						<span class="g9"><?=$form->dropDownList(
								$emailBlast,
								'email_template_id',
								CHtml::listData(
									EmailTemplates::model()->findAllByAttributes(
										array(
											'component_type_id'=>ComponentTypes::RECRUITS)
									)
									, 'id',
									'dropDownDisplay'
								),
								array(
									'data-placeholder' => 'Email Template',
									'style' => 'width:100%;','empty'=>'')
							);?></span>
						<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#EmailBlasts_email_template_id, select#draftIdToSend','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'allow_single_deselect' => true, 'width'=>'100%'))); ?>
					</div>
					<div class="g3">
						<label class="g3">Subject:</label>
						<span class="g9"><?=$form->textField($emailBlast,'email_subject', array('placeholder' => 'Subject'));?></span>
					</div>
					<div class="g3">
						<label class="g3">Description:</label>
						<span class="g9"><?=$form->textField($emailBlast,'description', array('placeholder' => 'Description'));?></span>
					</div>
					<div class="g3 submit" style="text-align:center">
						<label class="g3"></label>
                <span class="g9">
                    <?php echo CHtml::button('PREVIEW EMAIL & RECIPIENTS', array('class'=>'button g12','id'=>'email-blast-preview-button', 'style'=>'width: 100%;')); ?>
                </span>
					</div>
				</div>
				<div id="send-emails-row" style="display: none;">
					<div class="g3">
						<label class="g3">From Name:</label>
						<span class="g9"><?=$form->textField($emailBlast,'sender_name', array('placeholder' => 'Sender Name'));?></span>
					</div>
					<div class="g3">
						<label class="g3">From Email:</label>
                        <?
                        $x = Emails::model()->findAll(array('condition'=>'t.contact_id='.Yii::app()->user->id));
                        ?>
						<span class="g9"><?=$form->dropDownList($emailBlast,'sender_email_id', CHtml::listData(Emails::model()->findAll(array('condition'=>'t.contact_id='.Yii::app()->user->id)), 'id', 'email'), array('empty' => 'Select From Email','class'=>'g12'));?></span>
					</div>
					<div class="g3">
						<label class="g3">Send Datetime:</label>
                <span class="g9">
<!--                    --><?php //$this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					//                            'model'=>$emailBlast,
					//                            'attribute'=>'send_datetime',
					//                            'mode'=>'datetime',
					//                            'options'=>array(
					//                            ),
					//                            'htmlOptions'=>array(
					//                                'class'=>'g12',
					//                            ),
					//                        ));
					//
					//                    ?>
					<br>
                    *Note: It will send immediately for now.
                </span>
					</div>
					<div class="g3"></div>
					<div class="g3 submit" style="text-align:center">
						<label class="g3"></label>
                <span class="g9">
                	<?php echo CHtml::button('SEND EMAILS', array('class'=>'button g12','id'=>'send-emails-button', 'style'=>'width: 100%;')); ?>
            	</span>
					</div>
				</div>

                <? if($stmSmsNumber && Yii::app()->user->checkAccess('emails') && (strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') !== false && in_array(Yii::app()->user->id, array(1, 3256, 3145)) )):?>
				<!-- jamesk -->
				<hr class="g12" />
				<div id="start-sms-blast-button-container" class="g12">
					<div class="g3">
						<label class="g3"></label>
						<span class="g9">
							<?php echo CHtml::button('START SMS BLAST', array('class'=>'button g12','id'=>'start-sms-blast-button', 'style'=>'width: 100%;')); ?>
						</span>
					</div>
				</div>
				<div id="start-sms-blast-row" class="g12" style="display: none;">
					<div class="g3">
						<label class="g3">Template:</label>
						<span class="g9"><?=$form->dropDownList($smsBlast, 'sms_template_id', CHtml::listData(SmsTemplates::model()->findAllByAttributes(array('component_type_id'=>ComponentTypes::RECRUITS)),'id', 'dropDownDisplay'), array('data-placeholder' => 'Sms Template', 'style' => 'width:100%;','empty'=>''));?></span>
						<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#SmsBlasts_sms_template_id, select#draftIdToSend','options'=>array('enable_split_word_search'=>true,'search_contains'=>true,'allow_single_deselect' => true, 'width'=>'100%'))); ?>
					</div>
					<div class="g3">
						<label class="g3">Description:</label>
						<span class="g9"><?=$form->textField($smsBlast,'description', array('placeholder' => 'Description'));?></span>
					</div>
					<?php echo $form->hiddenField($smsBlast, 'id'); ?>

					<div class="g3 submit" style="text-align:center">
						<label class="g3"></label>
                        <span class="g9">
                            <?php
                                // @todo
                                echo CHtml::button('PREVIEW SMS & RECIPIENTS', array('class'=>'button g12','id'=>'sms-blast-preview-button', 'style'=>'width: 100%;'));
                            ?>
                        </span>
                    </div>
				</div>
				<div id="send-sms-row" style="display: none;">
					<div class="g3">
						<label class="g3">From:</label>
						<span class="g9">
                            <?=$form->dropDownList($smsBlast, 'sender_contact_id', array(Yii::app()->user->id => Yii::app()->format->formatPhone($stmSmsNumber->phone)), array('style' => 'width:100%;'));?>
						</span>
					</div>
					<div class="g3">
						<label class="g3">Send Datetime:</label>
						<span class="g9">
							<br>
							*Note: It will send immediately for now.
						</span>
					</div>
					<div class="g3">
						<label class="g3"></label>
						<span class="g9">
							<?php echo CHtml::button('SCHEDULE MESSAGES', array('class'=>'button g12','id'=>'send-sms-button', 'style'=>'width: 100%;')); ?>
						</span>
					</div>
				</div>
                <?endif;?>

			<?endif;?>

			<? if(Yii::app()->user->checkAccess('slybroadcast')):?>
				<hr style="padding: 0; min-height: 5px;" class="g12">
				<div class="g12">
					<label class="g1">Slybroadcast:</label>
            <span class="g10">

                <?php echo CHtml::textField('broadcastCallerId', null, $htmlOptions=['placeholder' => 'Caller ID', 'style' => 'width: 250px;']);?>

				<div style='display: inline-block;'>
					Audio File<br>
					<? echo CHtml::dropDownList('broadcastAudioFile', null, CHtml::listData(VoicemailBroadcastMessages::model()->findAll(), 'id', 'name'), $htmlOptions = array(
						//'class' => 'g8',
						'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
					));?>

				</div>

				<?php $this->widget('admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
						'name' => 'broadcastDeliveryDate',
						'defaultTime' => '12:00 am',
						'options' => array(
							'showAnim' => 'fold',
						),
						'htmlOptions' => array(
							'style' => 'width:185px;',
							'placeholder' => 'Set on Date & Time'
						),
					)
				); ?>

				<div style='display: inline-block;'>
					<label>Mobile Only:</label>
					<?php echo CHtml::checkbox('broadcastMobileOnlyCheckbox',false ,$htmlOptions=array(
						'value'=>0,
						'style'=>'font-size: 16px; position: relative; top: 10px;',
					)); ?>
					<?php echo CHtml::hiddenField('broadcastMobileOnly', null); ?>
				</div>

				<?php echo CHtml::hiddenField('broadcastProcess', null, $htmlOptions=array('value'=>'0'))?>
				<?php echo CHtml::button('Go', $htmlOptions=array('class'=>'gray button broadcast-button','style'=>'font-size: 10px; padding: 8px 10px;'))?>

				<a id="add-call-list-button" class="text-button" href="/admin/contacts/voicemailBroadcasts" style="display: inline-block;"><em class="icon i_stm_add"></em>Add Audio File</a>
            </span>
				</div>
			<? endif;?>
		<?endif;?>
	</div>
    <div id="show-more" class="g100 p-tc" style="margin: 20px 0 0 0; font-weight: bold;background: #e3e3e3; padding: 8px 0;">
        <a href="javascript:void(0)" class="advanced-search-button">
            <span class="text">Show Advanced Search Options</span>
            <i id="advanced-search-icon" class="fa fa-angle-down" aria-hidden="true"></i>
        </a>
    </div>
<?php $this->endWidget(); ?>