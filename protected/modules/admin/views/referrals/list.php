<?php
	$this->breadcrumbs = array(
		'List' => '',
	);

    $module = Yii::app()->controller->module->name;
	Yii::app()->clientScript->registerScript('search', <<<JS
	$('#listview-search form').submit(function() {
		$.fn.yiiGridView.update("referrals-grid", {
			data: $(this).serialize(),
            complete: function(jqXHR, status) {
                if (status=='success'){
                    var html = $.parseHTML(jqXHR.responseText);
                    var referralSummary = $('#referral-summary-container', $(html)).html();
                    $('#referral-summary-container').html(referralSummary);
                }
            }
		});
		return false;
	});

	$( '.delete-referral-button' ).live( "click", function() {
		if(confirm('Are you sure you want to delete this referral?')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var id = $(this).data('id');
			$.post('/$module/referrals/delete/'+id, function(data) {
				$("div.loading-container.loading").remove();
				if(data=='') {
					Message.create("success","Referral deleted successfully.");
					$.fn.yiiGridView.update("referrals-grid", { data: $(this).serialize() });
				} else
					Message.create("error","Error: Referral did not delete.");
			},"json");
		}
	});
JS
);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name;?>/referrals/add" class="button gray icon i_stm_add">Add Referral</a>
</div>
	<h1>Referrals</h1>

<div id="referral-summary-container" style="width:800px;margin-left: auto;margin-right: auto; padding:20px;">
	<table class="p-f0" style="font-size: 16px;">
		<tr>
			<th></th>
			<th style="width: 15%;">Quantity</th>
			<th style="width: 20%;">Volume</th>
			<th style="width: 22%;">Potential Income</th>
			<th style="width: 22%;">Closed Income</th>
		</tr>
		<tr>
			<th>Inbound</th>
			<td><?php echo $data['inboundQuantity']; ?></td>
			<td><?php echo ($data['inboundVolume'])? Yii::app()->format->formatDollars($data['inboundVolume']) : '-'; ?></td>
			<td><?php echo ($data['inboundPotentialIncome'])? Yii::app()->format->formatDollars($data['inboundPotentialIncome']) : '-'; ?></td>
			<td><?php echo ($data['inboundClosedIncome'])? Yii::app()->format->formatDollars($data['inboundClosedIncome']) : '-'; ?></td>
		</tr>
		<tr>
			<th>Outbound</th>
			<td><?php echo $data['outboundQuantity']; ?></td>
			<td><?php echo ($data['outboundVolume'])?Yii::app()->format->formatDollars($data['outboundVolume']): '-'; ?></td>
			<td><?php echo ($data['outboundPotentialIncome'])? Yii::app()->format->formatDollars($data['outboundPotentialIncome']) : '-'; ?></td>
			<td><?php echo ($data['outboundClosedIncome'])? Yii::app()->format->formatDollars($data['outboundClosedIncome']) : '-'; ?></td>
		</tr>
	</table>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBox', array(
			'model' => $model,
		)
	); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'referrals-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Date / Direction',
                'value' => 'Yii::app()->format->formatDate($data->referral_date)."<br />".$data->getModelAttribute("getReferralDirectionTypes",$data->referral_direction_ma)',
                'htmlOptions' => array('style' => 'width:100px'),
            ),
			array(
				'type' => 'raw',
				'name' => 'Type / Status',
				'value' => 'ucwords($data->transactionType->name)."<br />".$data->transactionStatus->name',
                'htmlOptions' => array('style' => 'width:100px'),
			),
            array(
                'type' => 'raw',
                'name' => 'Client Info / Source',
                'value' => '"<strong><u>".$data->contact->fullName."</u></strong><br />".Yii::app()->format->formatPrintEmails($data->contact->emails)."<br />".Yii::app()->format->formatPrintPhones($data->contact->phones)."<br /><i>Source: ".$data->source->name."</i>"',
            ),
			array(
				'type' => 'raw',
				'name' => 'Referral Agent',
				'value' => '$data->outsideReferralAgent->fullName."<br />".Yii::app()->format->formatPrintEmails($data->outsideReferralAgent->emails)."<br />".Yii::app()->format->formatPrintPhones($data->outsideReferralAgent->phones)',
			),
			array(
				'type' => 'raw',
				'name' => 'Assigned to',
				'value' => '$data->assignedTo->fullName',
			),
            array(
                'type' => 'raw',
                'name' => 'Price / Referral Fee',
                'value' => 'Yii::app()->format->formatDollars($data->price)."<br />(".Yii::app()->format->formatPercentages($data->referral_percent).")"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Potential Income',
                'value' => 'Yii::app()->format->formatDollars($data->potentialIncome)',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Notes',
                'value' => '$data->notes',
                'htmlOptions' => array('style' => 'width:180px'),
            ),
//			array(
//				'type' => 'raw',
//				'name' => '',
//				'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/referrals/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\" target=\"_blank\">Edit</a></div>"',
//				'htmlOptions' => array('style' => 'width:70px'),
//			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->name."/referrals/".$data->id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a></div>"
            ',
				'htmlOptions' => array('style' => 'width:80px'),
			),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '
                "<div><a data-id=\"".$data->id."\" class=\"button gray icon i_stm_delete grey-button delete-referral-button \">Delete</a></div>"
            ',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
		),
	)
);