<?php
	$this->breadcrumbs = array(
		'Add' => '',
	);
?>
<h1>Referrals List</h1>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('../contacts/_listSearchBox', array('model' => $Contact)); ?>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'contact-grid',
		'dataProvider' => $Contact->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			'first_name',
			'last_name',
			array(
				'type' => 'raw',
				'name' => 'Email',
				'value' => 'Yii::app()->format->formatCommaDelimited($data->emails,"email")',
			),
			array(
				'type' => 'raw',
				'name' => 'Phone',
				'value' => 'Yii::app()->format->formatPrintPhones($data->phones)',
			),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '
                "<div><a href=\"/".Yii::app()->controller->module->name."/referrals/add/".$data->id."\" class=\"button gray icon i_stm_add grey-button\">Add Referral</a></div>"
            ',
				'htmlOptions' => array('style' => 'width:135px'),
			),
		),
	)
);