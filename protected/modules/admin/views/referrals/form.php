<?php
	Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'ajax_chosen.js');

    $moduleName = Yii::app()->controller->module->id;
	$js = <<<JS
		$(document).ready(function () {
			$("select#Referrals_outside_referral_agent_id").ajaxChosen({
				type: 'POST',
				url: '/$moduleName/contacts/autocomplete',
				dataType: 'json'
			}, function (data) {
				var results = [];

				$.each(data, function (i, val) {
					results.push({ value: val.value, text: val.text+' ('+val.email+')' });
				});

				return results;
			});
		});
JS;
	Yii::app()->clientScript->registerScript('company-contact-script', $js);

	$this->widget('admin_module.extensions.moneymask.MMask', array(
				 'element' => '#Referrals_price, #Referrals_referral_paid, #Referrals_commission_gross, #Referrals_commission_net',
				 'currency' => 'PHP',
				 'config' => array(
					 'precision' => 2,
					 'symbol' => '$',
					 'showSymbol' => true,
					 'symbolStay' => true,
				 )
			 ));

	$this->widget('admin_module.extensions.moneymask.MMask', array(
				 'element' => '#Referrals_percent',
				 'currency' => 'PHP',
				 'config' => array(
					 'precision' => 2,
					 'symbol' => '',
					 'showSymbol' => false,
					 'symbolStay' => false,
				 )
				 ));

	if($this->action->id == 'edit') {
		$this->breadcrumbs = array(
			$model->contact->fullName => '/'.Yii::app()->controller->module->id.'/referrals/'.$model->id,
			'Edit' => '',
		);
	} else {
		$this->breadcrumbs = array(
			'Add' => '',
		);
	}
?>

<h1>Add New Referral</h1>

<?php if($Contact): ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' =>' add-referral-form',
	'enableAjaxValidation' => false,
));
	$this->beginStmPortletContent(array(
	'handleTitle'   => 'Referral Detail',
	'handleIconCss' => 'i_wizard'
	));
?>
<div id="referral-container" >
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<div class="g6">
			<table class="">
				<tr>
					<th><?php echo $form->labelEx($model, 'status_ma'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'status_ma', $model->getStatusTypes(), $htmlOptions=array('empty'=>'','class'=>'g6'));
						?>
						<?php echo $form->error($model,'status_ma'); ?>
					</td>
				</tr>
				<tr>
					<th>Client Name:</th>
					<td>
						<?php echo CHtml::tag('span', $htmlOptions = array('class' => 'contact-name','style'=>'font-size:18px;'), $Contact->fullName); ?>
						<?php echo $form->hiddenField($model, 'contact_id'); ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'transaction_type_id'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'transaction_type_id', $model->getTransactionTypes(),
												$htmlOptions=array('class' => 'g4', 'placeholder' => 'Select One','empty'=>'')); ?>
						<?php echo $form->error($model, 'transaction_type_id'); ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'source_id'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'source_id', CMap::mergeArray(array(''=>''),Sources::optGroupList($model->source_id)), $htmlOptions=array('style' => 'width:325px;',
																														'data-placeholder' => 'Select a Source',
							)); ?>
						<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Referrals_source_id',
																							 'options'=>array('allow_single_deselect'=>true),
																				   			)); ?>
						<?php echo $form->error($model,'source_id'); ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'assigned_to_id'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'assigned_to_id', CMap::mergeArray(array(''=>''),CHtml::listData(Contacts::model()->byActiveAdmins($together = false, $contactId = $model->assignedTo->id)->orderByName()->findAll(), 'id', 'fullName')), $htmlOptions=array('style' => 'width:325px;',
																														'data-placeholder' => 'Select Assignment',
							)); ?>
						<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#Referrals_assigned_to',
																							 'options'=>array('allow_single_deselect'=>true),
																					   )); ?>
						<?php echo $form->error($model,'assigned_to_id'); ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'price'); ?>:</th>
					<td>
						<?php echo $form->textField($model, 'price', $htmlOptions=array('class'=>'g6','placeholder'=>'Price')); ?>
						<?php echo $form->error($model,'price'); ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'referral_percent'); ?>:</th>
					<td>
						<?php echo $form->dropDownList($model, 'referral_percent', StmFormHelper::getPercentageList(array('max'=>100, 'min'=>0,'sort'=>'asc')), $htmlOptions=array( 'class'=>'g6')); ?>
						<?php echo $form->error($model,'referral_percent'); ?>
					</td>
				</tr>
				<tr>
					<th>Referral Amount:</th>
					<td>
						<?php echo $form->textField($model, 'referral_paid', $htmlOptions=array('class'=>'g6','placeholder'=>'Referral Paid')); ?>
						<?php echo $form->error($model,'referral_paid'); ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="g6">
			<table class="">
				<tr>
					<th><?php echo $form->labelEx($model, 'outside_referral_agent_id'); ?>:</th>
					<td>
						<?php
						$outsideReferralAgentArray = array(''=>'');
						if($model->outside_referral_agent_id) {
							$outsideReferralAgentArray = CMap::mergeArray($outsideReferralAgentArray, array($model->outside_referral_agent_id=>$model->outsideReferralAgent->fullName));
						}
						echo $form->dropDownList($model, 'outside_referral_agent_id', $outsideReferralAgentArray, $htmlOptions = array(
								'data-placeholder' =>'Type Name Here...',
								'class'=>'g12',
							));
						echo $form->error($model, 'outside_referral_agent_id');

						$this->widget('admin_module.extensions.EChosen.EChosen', array(
																				 'target' => 'select#Referrals_outside_referral_agent_id',
																				 'options'=>array(
																					 'allow_single_deselect'=>true,
																				 ),
																				 ));
						?>
                        <span class="label">(Note: Agent or Contact. Must be existing Contact)</span>
					</td>
				</tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'referral_direction_ma'); ?>:</th>
                    <td>
                        <?php echo $form->dropDownList($model, 'referral_direction_ma', $model->getReferralDirectionTypes(), $htmlOptions=array('empty'=>'','class'=>'g6')); ?>
                        <?php echo $form->error($model,'referral_direction_ma'); ?>
                    </td>
                </tr>
				<tr>
					<th><?php echo $form->labelEx($model, 'referral_date'); ?>:</th>
					<td>
						<?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
																			  'model' => $model,
																			  'attribute' => 'referral_date',
																			  'options' => array(
																				  'showAnim' => 'fold',
																			  ),
																			  'htmlOptions' => array(
																				  'class' => 'g6',
																			  	  'placeholder'=>'Referral Date'
																			  ),
																			  ));
						?>
						<?php echo $form->error($model,'referral_date'); ?>
					</td>
				</tr>
				<tr>
					<th>Closing Date:</th>
					<td>
						<?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
																			  'model' => $model,
																			  'attribute' => 'closed_date',
																			  'options' => array(
																				  'showAnim' => 'fold',
																			  ),
																			  'htmlOptions' => array(
																				  'class' => 'g6',
																				  'placeholder'=>'Closing Date'
																			  ),
																			  ));
						?>
						<?php echo $form->error($model,'closed_date'); ?>
					</td>
				</tr>
				<tr>
					<th>Referral Signed:</th>
					<td>
						<?php echo $form->dropDownList($model, 'referral_agreement_signed', StmFormHelper::getYesNoList(), $htmlOptions=array('class'=>'g6')); ?>
						<?php echo $form->error($model,'referral_agreement_signed'); ?>
					</td>
				</tr>
				<tr>
					<th>Commission Gross:</th>
					<td>
						<?php echo $form->textField($model, 'commission_gross', $htmlOptions=array('class'=>'g6','placeholder'=>'Commission Gross')); ?>
						<?php echo $form->error($model,'commission_gross'); ?>
					</td>
				</tr>
<!--				<tr>-->
<!--					<th>Commission Net:</th>-->
<!--					<td>-->
<!--						--><?php //echo $form->textField($model, 'commission_net', $htmlOptions=array('class'=>'g6','placeholder'=>'Commission Net')); ?>
<!--						--><?php //echo $form->error($model,'commission_net'); ?>
<!--					</td>-->
<!--				</tr>-->
			</table>
		</div>
		<table class="">
			<tr>
				<th class='narrow'>
					<?php echo $form->labelEx($model, 'notes'); ?>:
				</th>
				<td>
					<?php echo $form->textArea($model, 'notes', $htmlOptions = array(
							'class' => 'g11',
							'rows' => '8'
						)
					); ?>
					<?php echo $form->error($model, 'notes'); ?>
				</td>
			</tr>
		</table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
	<?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Add New' : 'Save') . ' Referral', $htmlOptions = array(
			'class' => 'submit wide',
			'type' => 'submit',
		)); ?>
</div>
<?php $this->endWidget(); ?>

<?php else: ?>
<br />
<br />
<h3>Invalid Contact. Please go back and try again.</h3>

<?php endif; ?>