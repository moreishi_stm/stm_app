<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="g3">
		<label class="g4">Status:</label>
			<span class="g8">
				<?php echo $form->dropDownList($model, 'status_ma', $model->getStatusTypes(), $htmlOptions=array('empty'=>' ')); ?>
			</span>
	</div>
	<div class="g2">
		<label class="g5">Direction:</label>
		<span class="g7">
    		<?php echo $form->dropDownList($model, 'referral_direction_ma', $model->getReferralDirectionTypes(), $htmlOptions=array('empty'=>' ')); ?>
		</span>
	</div>
	<div class="g2">
		<label class="g7">Transaction Type:</label>
			<span class="g5">
				<?php echo $form->dropDownList($model, 'transaction_type_id', $model->getTransactionTypes(), $htmlOptions=array('empty'=>' ')); ?>
			</span>
	</div>
	<div class="g3">
		<label class="g5">Assigned to:</label>
		<span class="g7">
    		<?php echo $form->dropDownList($model, 'assigned_to_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->assigned_to_id)->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions=array('empty'=>' ')); ?>
		</span>
	</div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <div class="g4">
        <label class="g3">ReferralDate:</label>
        <span class="g9">
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'fromDate',
                    // name of post parameter
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'mm/dd/yy',
                        'numberOfMonths' => 2,
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:90px;font-size: 12px;',
                        'class' => 'g5',
                    ),
                )
            );
            ?>
            <label class="label p-fl" style="padding-left:5px;padding-right:5px;">to</label>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'toDate',
                    // name of post parameter
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'mm/dd/yy',
                        'numberOfMonths' => 2,
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:90px;font-size: 12px;',
                        'class' => 'g5',
                    ),
                )
            );
            ?>
        </span>
    </div>
<?php $this->endWidget(); ?>
