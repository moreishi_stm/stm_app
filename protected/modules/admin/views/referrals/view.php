<?php
//	$this->breadcrumbs = array(
//		$model->contact->fullName,
//	);
//
//	$this->title = $model->contact->fullName;
//
//?>
<!--<style type="text/css">-->
<!--	#recruit-portlet div.disc-data {-->
<!--		display: inline-block;-->
<!--	}-->
<!---->
<!--	#recruit-portlet .disc-data span {-->
<!--		width: 35px;-->
<!--		font-weight: normal;-->
<!--		display: inline-block;-->
<!--	}-->
<!---->
<!--	#recruit-portlet .disc-data i {-->
<!--		font-weight: normal;-->
<!--		display: inline-block;-->
<!--		float: left;-->
<!--	}-->
<!--</style>-->

<?php
$this->breadcrumbs = array(
	$model->contact->fullName => '',
);
?>

<div id="top-data" class="g100 p-0">
	<div class="g30 p-0 p-fl">
		<?php
			$this->widget('admin_widgets.ContactPortlet.ContactPortlet', array(
					'model' => $model->contact,
					'view' => 'mini',
                    'originComponentTypeId' => $model->componentType->id,
                    'originComponentId' => $model->id,
					'handleTitle' => 'Contact Info',
					'handleButtons' => array(
						array(
							'label' => 'Edit',
							'iconCssClass' => 'i_stm_edit',
							'htmlOptions' => array('id' => 'edit-contact-button'),
							'name' => 'edit-contact',
							'type' => 'link',
							'link' => '/'.Yii::app()->controller->module->id.'/contacts/edit/' . $model->contact->id,
						)
					),
				)
			);
		?>
	</div>
	<div class="g70 p-0 p-fr">
		<?php
			$this->beginStmPortletContent(array(
					'handleTitle' => 'Referral Details',
					'handleIconCss' => 'i_wizard',
					'id' => 'recruits',
					'handleButtons' => array(
						array(
							'label' => 'Edit',
							'iconCssClass' => 'i_stm_edit',
							'htmlOptions' => array('id' => 'edit-contact-button'),
							'name' => 'edit-contact',
							'type' => 'link',
							'link' => '/'.Yii::app()->controller->module->id.'/referrals/edit/' . $model->id,
						)
					),

				)
			);
		?>
		<div>
			<div class="g12 p-mb5 rounded-text-box odd-static">
				<div class="g6">
					<table id="recruit-portlet" class="container">
						<tbody>
						<tr>
							<th>Status:</th>
							<td>
								<?php echo $model->transactionStatus->name; ?>
							</td>
						</tr>
						<tr>
							<th>Name:</th>
							<td><?php echo $model->contact->fullName; ?></td>
						</tr>
						<tr>
							<th>Source:</th>
							<td>
								<?php echo $model->source->name; ?>
							</td>
						</tr>
						<tr>
							<th>Assigned to:</th>
							<td>
								<?php echo $model->assignedTo->fullName; ?>
							</td>
						</tr>
						<tr>
							<th>Price:</th>
							<td>
								<?php echo Yii::app()->format->formatDollars($model->price); ?>
							</td>
						</tr>
						<tr>
							<th>Referral Fee:</th>
							<td>
								<?php echo Yii::app()->format->formatPercentages($model->referral_percent); ?>
							</td>
						</tr>
						<tr>
							<th>Referral Paid:</th>
							<td>
								<?php echo Yii::app()->format->formatDollars($model->referral_paid); ?>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="g6">
					<table class="container">
						<tbody>
						<tr>
							<th>Referral Agent:</th>
							<td><?php echo $model->outsideReferralAgent->fullName; ?></td>
						</tr>
						<tr>
							<th>Referral Date:</th>
							<td>
								<?php echo Yii::app()->format->formatDate($model->referral_date); ?>
							</td>
						</tr>
						<tr>
							<th>Closed Date:</th>
							<td>
								<?php echo Yii::app()->format->formatDate($model->closed_date); ?>
							</td>
						</tr>
						<tr>
							<th>Referral Direction:</th>
							<td>
								<?php echo $model->getModelAttribute("getReferralDirectionTypes",$model->referral_direction_ma); ?>
							</td>
						</tr>
						<tr>
							<th>Signed:</th>
							<td>
								<?php echo StmFormHelper::getYesNoName($model->referral_agreement_signed); ?>
							</td>
						</tr>
						<tr>
							<th>Commission Gross:</th>
							<td>
								<?php echo Yii::app()->format->formatDollars($model->commission_gross); ?>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="g12">
					<table class="container">
						<tbody>
						<tr class="p-p10">
							<th class="narrow">Notes:</th>
							<td><?php echo nl2br($model->notes); ?></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php $this->endStmPortletContent(); ?>
	</div>
</div>

<?php
$this->widget('admin_widgets.ActivityDetailsPortlet.ActivityDetailsPortlet', array(
			 'model'         => $model,
			 'componentType' => $model->componentType,
			 ));

?>


<div class="g12 p-mh0 p-mv5">
	<?php $this->widget('admin_widgets.ActivityLogPortlet.ActivityLogPortlet', array(
																		 'parentModel' => $model,
																		 ));
	?>
</div>