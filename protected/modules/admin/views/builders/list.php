<?
$this->breadcrumbs=array(
    'List'
);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/builders/add" class="button gray icon i_stm_add">Add New Builder</a>
    <a href="/<?php echo Yii::app()->controller->module->id; ?>/builders/modelHomes" class="button gray icon i_stm_search">Model Homes</a>
</div>

<h1>Builders List</h1>
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
			   'id' => 'builders-grid',
			   'dataProvider' => $model->search(),
			   'itemsCssClass' => 'datatables',
			   'columns' => array(
                   array(
                       'type' => 'raw',
                       'name' => 'Builder',
                       'value' => '"<strong>".$data->name."</strong><br>".$data->email."<br>".Yii::app()->format->formatPhone($data->phone)."<br>".$data->website',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => 'Description',
                       'value' => 'nl2br($data->description)',
                   ),
                   array(
                       'type' => 'raw',
                       'name' => 'Communities / Floor Plans / Models',
                       'value' => '"Communities: ".count($data->communities)."<br>Floor Plans: ".count($data->floorPlans)."<br>Models: ".count($data->modelHomes)',
                   ),
                   array(
					   'type' => 'raw',
					   'name' => '',
					   'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\" target=\"_blank\">Edit</a></div>"',
					   'htmlOptions' => array('style' => 'width:80px'),
				   ),
                   array(
                       'type' => 'raw',
                       'name' => '',
                       'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/floorPlans/".$data->id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">Floor Plans</a></div>"',
                       'htmlOptions' => array('style' => 'width:120px'),
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '',
                       'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/communities/".$data->id."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">Communities</a></div>"',
                       'htmlOptions' => array('style' => 'width:120px'),
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '',
                       'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/addFloorplan/".$data->id."\" class=\"button gray icon i_stm_add grey-button\" target=\"_blank\">Add Floor Plan</a></div>"',
                       'htmlOptions' => array('style' => 'width:140px'),
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '',
                       'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/addModelHome/".$data->id."\" class=\"button gray icon i_stm_add grey-button\" target=\"_blank\">Add Model</a></div>"',
                       'htmlOptions' => array('style' => 'width:110px'),
                   ),
                   array(
                       'type' => 'raw',
                       'name' => '',
                       'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/addCommunity/".$data->id."\" class=\"button gray icon i_stm_add grey-button\" target=\"_blank\">Add Community</a></div>"',
                       'htmlOptions' => array('style' => 'width:140px'),
                   ),
               ),
			   )
);