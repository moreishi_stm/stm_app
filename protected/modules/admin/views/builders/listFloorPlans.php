<?
$this->breadcrumbs=array(
    'Floor Plans'
);
?>
<div id="listview-actions">
    <?php if($builder): ?>
    <a href="/<?php echo Yii::app()->controller->module->id; ?>/builders/addFloorPlan/<?php echo $builder->id; ?>" class="button gray icon i_stm_add">Add Floor Plan</a>
    <?php endif; ?>
</div>


<h1><?php echo $builderName; ?> Floor Plan</h1>
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
			   'id' => 'floor-plan-grid',
			   'dataProvider' => $model->search(),
			   'itemsCssClass' => 'datatables',
			   'columns' => array(
				   'name',
                   'builder.name',
                   'bedrooms',
                   'baths',
                   'sq_feet',
                   array(
                       'type' => 'raw',
                       'name' => 'Communities',
                       'value' => 'Yii::app()->format->formatCommaDelimited($data->featuredAreas, "name")',
                   ),
				   array(
					   'type' => 'raw',
					   'name' => '',
					   'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/editFloorPlan/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
					   'htmlOptions' => array('style' => 'width:80px'),
				   ),
			   ),
			   )
);