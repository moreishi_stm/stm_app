<?
$this->breadcrumbs=array(
    ($this->action->id=='edit')? 'Edit' : 'Add' => ''
);
?>
<h1><?php echo  ($this->action->id=='edit')? 'Edit' : 'Add '; ?> Community for <?php echo $builder->name; ?></h1>
<br />

<?php $form = $this->beginWidget('CActiveForm', array(
        'id' =>' builder-community-form',
        'enableAjaxValidation' => false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'   => ($this->action->id=='edit')? 'Edit' : 'Add'.' New Builder Community',
        'handleIconCss' => 'i_wizard'
    ));
?>
    <div id="builder-community-container" >
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <div class="g3"></div>
            <div class="g6">
                <table class="container">
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'builder_id'); ?>:</th>
                        <td>
                            <?php echo $builder->name; ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'featured_area_id'); ?>:</th>
                        <td>
                            <?php echo $form->dropDownList($model, 'featured_area_id', CHtml::listData(FeaturedAreas::model()->findAll(array('order'=>'name ASC')), 'id', 'name'), $htmlOptions=array('class'=>'g6','empty'=>'','data-placeholder'=>'Select a Community'));
                            $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#BuilderCommunityLu_featured_area_id','options'=>array('allow_single_deselect'=>true)));
                            ?><br>
                            <a href="/admin/featuredAreas/add/<?php echo Yii::app()->user->accountId;?>" target="_blank" class="text-button" style="margin-left:0; display: inline-block;"><em class="icon i_stm_add"></em><span>Add New Featured Area</span></a>
                            <div style="font-weight: bold; display: inline-block; width: 360px; vertical-align: middle;">NOTE: After adding a new Featured Area, return to this screen to complete the add community process.</div>
                            <?php echo $form->error($model,'featured_area_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'price_min_max'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'price_min', $htmlOptions=array('class'=>'g3','placeholder'=>'Price Min')); ?>
                            <span class="label p-p5" style="float: left;">to</span>
                            <?php echo $form->textField($model, 'price_max', $htmlOptions=array('class'=>'g3','placeholder'=>'Price Max')); ?>
                            <?php echo $form->error($model,'price_min'); ?><?php echo $form->error($model,'price_max'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'phone'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'phone', $htmlOptions=array('class'=>'g6','placeholder'=>'Phone')); ?>
                            <?php echo $form->error($model,'phone'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'address'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'address', $htmlOptions=array('class'=>'g8','placeholder'=>'Address')); ?>
                            <?php echo $form->error($model,'address'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'city'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'city', $htmlOptions=array('class'=>'g4','placeholder'=>'City')); ?>
                            <?php echo $form->dropDownList($model, 'state_id', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions=array('class'=>'g2','empty'=>'State')); ?>
                            <?php echo $form->textField($model, 'zip', $htmlOptions=array('class'=>'g2','placeholder'=>'Zip')); ?>
                            <?php echo $form->error($model,'city'); ?><?php echo $form->error($model,'state_id'); ?><?php echo $form->error($model,'zip'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'website'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'website', $htmlOptions=array('class'=>'g8','placeholder'=>'http://www.builder.com')); ?>
                            <?php echo $form->error($model,'website'); ?>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="container">
                <tr>
                    <th class='narrow'>
                        <?php echo $form->labelEx($model, 'description'); ?>:
                    </th>
                    <td>
                        <?php echo $form->textArea($model, 'description', $htmlOptions = array(
                                'class' => 'g11',
                                'rows' => '8'
                            )
                        ); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Add New' : 'Save') . ' Community', $htmlOptions = array(
            'class' => 'submit wide',
            'type' => 'submit',
        )); ?>
</div>
<?php $this->endWidget(); ?>

