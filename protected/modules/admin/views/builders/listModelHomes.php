<?
$this->breadcrumbs=array(
    'Model Homes'
);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->id; ?>/builders/addModelHome" class="button gray icon i_stm_add">Add Model Home</a>
</div>

<h1>Model Homes List</h1>
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
			   'id' => 'model-homes-grid',
			   'dataProvider' => $model->search(),
			   'itemsCssClass' => 'datatables',
			   'columns' => array(
//				   'name',
//                   'builder.name',
//                   'bedrooms',
//                   'baths',
//                   'sq_feet',
//				   array(
//					   'type' => 'raw',
//					   'name' => '',
//					   'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/editFloorPlan/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
//					   'htmlOptions' => array('style' => 'width:80px'),
//				   ),
			   ),
			   )
);