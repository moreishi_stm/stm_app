<?
$this->breadcrumbs=array(
    ($this->action->id=='edit')? 'Edit' : 'Add' => ''
);
?>
<h1><?php echo  ($this->action->id=='edit')? 'Edit' : 'Add New'; ?> Model Home for <?php echo $builder->name; ?></h1>
<br />

<?php $form = $this->beginWidget('CActiveForm', array(
        'id' =>' model-home-form',
        'enableAjaxValidation' => false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'   => ($this->action->id=='edit')? 'Edit' : 'Add'.' New Model Home',
        'handleIconCss' => 'i_wizard'
    ));
?>
    <div id="referral-container" >
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <div class="g3"></div>
            <div class="g6">
                <table class="container">
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'builder_id'); ?>:</th>
                        <td>
                            <?php echo $builder->name; ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'floor_plan_id'); ?>:</th>
                        <td>
                            <?php echo $form->dropDownList($model, 'floor_plan_id', CHtml::listData(FloorPlans::model()->findAll(array('condition'=>'builder_id='.$builder->id)), 'id', 'name'), $htmlOptions=array('class'=>'g6')); ?>
                            <?php echo $form->error($model,'floor_plan_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'address'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'address', $htmlOptions=array('class'=>'g6','placeholder'=>'Address')); ?>
                            <?php echo $form->error($model,'address'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'city'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'city', $htmlOptions=array('class'=>'g6','placeholder'=>'City')); ?>
                            <?php echo $form->error($model,'city'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'state_id'); ?>:</th>
                        <td>
                            <?php echo $form->dropDownList($model, 'state_id', CHtml::listData(AddressStates::model()->findAll(), 'id', 'name'), $htmlOptions=array('class'=>'g6')); ?>
                            <?php echo $form->error($model,'state_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'zip'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'zip', $htmlOptions=array('class'=>'g6','placeholder'=>'Zip')); ?>
                            <?php echo $form->error($model,'zip'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'sq_feet'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'sq_feet', $htmlOptions=array('class'=>'g6','placeholder'=>'Sq Feet')); ?>
                            <?php echo $form->error($model,'sq_feet'); ?>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="container">
                <tr>
                    <th class='narrow'>
                        <?php echo $form->labelEx($model, 'description'); ?>:
                    </th>
                    <td>
                        <?php echo $form->textArea($model, 'description', $htmlOptions = array(
                                'class' => 'g11',
                                'rows' => '8'
                            )
                        ); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Add New' : 'Save') . ' Model Home', $htmlOptions = array(
            'class' => 'submit wide',
            'type' => 'submit',
        )); ?>
</div>
<?php $this->endWidget(); ?>

