<?
$this->breadcrumbs=array(
    ($this->action->id=='editFloorPlan')? 'Edit' : 'Add' => ''
);
?>
<h1><?php echo  ($this->action->id=='editFloorPlan')? 'Edit' : 'Add New';?> Floor Plan for <?php echo $builder->name; ?></h1>
<br />

<?php $form = $this->beginWidget('CActiveForm', array(
        'id' =>' floor-plan-form',
        'enableAjaxValidation' => false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'   => (($this->action->id=='editFloorPlan')? 'Edit' : 'Add New').' Floor Plan',
        'handleIconCss' => 'i_wizard'
    ));
?>
    <div id="referral-container" >
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <div class="g3"></div>
            <div class="g6">
                <table class="container">
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'name'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'name', $htmlOptions=array('class'=>'','placeholder'=>'Floor Plan Name')); ?>
                            <?php echo $form->error($model,'name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'bedrooms'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'bedrooms', $htmlOptions=array('class'=>'','placeholder'=>'Bedrooms')); ?>
                            <?php echo $form->error($model,'bedrooms'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'baths'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'baths', $htmlOptions=array('class'=>'','placeholder'=>'Baths')); ?>
                            <?php echo $form->error($model,'baths'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'sq_feet'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'sq_feet', $htmlOptions=array('class'=>'','placeholder'=>'Sq. Feet')); ?>
                            <?php echo $form->error($model,'sq_feet'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'communities'); ?>:</th>
                        <td>
                            <?php echo $form->dropDownList($model, 'communitiesCollection', CHtml::listData(BuilderCommunityLu::model()->findAllByAttributes(array('builder_id'=>$builder->id)),'featured_area_id','featuredArea.name'),$htmlOptions=array('class'=>'g12','data-placeholder'=>'Select Communities','empty'=>'','multiple'=>'multiple'));
                                $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#FloorPlans_communitiesCollection','options'=>array('allow_single_deselect'=>true)));
                            ?>
                            <?php echo $form->error($model,'communitiesCollection');?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'youtube_code'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'youtube_code', $htmlOptions=array('class'=>'','placeholder'=>'Youtube Code')); ?>
                            <?php echo $form->error($model,'youtube_code'); ?>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="container">
                <tr>
                    <th class='narrow'>
                        <?php echo $form->labelEx($model, 'description'); ?>:
                    </th>
                    <td>
                        <?php echo $form->textArea($model, 'description', $htmlOptions = array(
                                'class' => 'g11',
                                'rows' => '8'
                            )
                        ); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Add New' : 'Save') . ' Floor Plan', $htmlOptions = array(
            'class' => 'submit wide',
            'type' => 'submit',
        )); ?>
</div>
<?php $this->endWidget(); ?>

