<?
$this->breadcrumbs=array(
    'Communities'
);
?>
    <div id="listview-actions">
        <?php if($builder):?>
            <a href="/<?php echo Yii::app()->controller->module->id; ?>/builders/addCommunity/<?php echo $builder->id;?>" class="button gray icon i_stm_add">Add New Community</a>
        <?php endif;?>
    </div>

<h1><?php echo ($builder)? $builder->name.' ' : ''; ?>Communities List</h1>
<br />
<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'builder-communities-grid',
        'dataProvider' => $model->search(),
        'itemsCssClass' => 'datatables',
        'columns' => array(
            'builder.name',
            'featuredArea.name',
            array(
                'type' => 'raw',
                'name' => 'Address',
                'value' => '$data->address."<br>".$data->city.", ".AddressStates::getShortNameById($data->state_id)." ".$data->zip',
            ),
            'phone:phone',
            'website',
            'price_min:dollars',
            'price_max:dollars',
            array(
                'type' => 'raw',
                'name' => 'Description',
                'value' => 'nl2br($data->description)',
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/builders/editCommunity/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
        ),
    )
);