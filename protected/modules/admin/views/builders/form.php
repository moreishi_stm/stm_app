<?
$this->breadcrumbs=array(
    ($this->action->id=='edit')? 'Edit' : 'Add' => ''
);
?>
<h1>Add New Builders</h1>
<br />

<?php $form = $this->beginWidget('CActiveForm', array(
        'id' =>' add-builder-form',
        'enableAjaxValidation' => false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'   => 'Add New Builder',
        'handleIconCss' => 'i_wizard'
    ));
?>
    <div id="referral-container" >
        <div class="g12 p-mb5 rounded-text-box odd-static">
<!--            <div class="g6">-->
<!--                <table class="container">-->
<!--                    <tr>-->
<!--                        <th>--><?php //echo $form->labelEx($model, 'date'); ?><!--:</th>-->
<!--                        <td>-->
<!--                            --><?//	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                    'model' => $model,
//                                    'attribute' => 'date',
//                                    'options' => array(
//                                        'showAnim' => 'fold',
//                                    ),
//                                    'htmlOptions' => array(
//                                        'class' => 'g6',
//                                        'placeholder'=>'Expense Date'
//                                    ),
//                                ));
//                            ?>
<!--                            --><?php //echo $form->error($model,'date'); ?>
<!--                        </td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <th>--><?php //echo $form->labelEx($model, 'amount'); ?><!--</th>-->
<!--                        <td>-->
<!--                            --><?php //echo $form->textField($model, 'amount', $htmlOptions=array('class'=>'g6','placeholder'=>'Amount')); ?>
<!--                            --><?php //echo $form->error($model,'amount'); ?>
<!--                        </td>-->
<!--                    </tr>-->
<!--                </table>-->
<!--            </div>-->
            <div class="g3"></div>
            <div class="g6">
                <table class="container">
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'level'); ?>:</th>
                        <td>
                            <?php echo $form->dropDownList($model, 'level', array(Builders::LEVEL_LOCAL=>'Local', Builders::LEVEL_REGIONAL=>'Regional', Builders::LEVEL_NATIONAL=>'National'), $htmlOptions=array('class'=>'g4','empty'=>'')); ?>
                            <?php echo $form->error($model,'level'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'name'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'name', $htmlOptions=array('class'=>'','placeholder'=>'Builder Name')); ?>
                            <?php echo $form->error($model,'name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'phone'); ?>:</th>
                        <td>
                            <?php $this->widget('StmMaskedTextField', array(
                                    'model' => $model,
                                    'attribute' => 'phone',
                                    'mask' => '(999) 999-9999',
                                    'htmlOptions' => array('class'=>'g4', 'placeholder'=>'Phone'),
                                )); ?>
                            <?php echo $form->error($model,'phone'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'email'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'email', $htmlOptions=array('class'=>'','placeholder'=>'Email')); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'website'); ?>:</th>
                        <td>
                            <?php echo $form->textField($model, 'website', $htmlOptions=array('class'=>'','placeholder'=>'Website URL')); ?>
                            <?php echo $form->error($model,'website'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="narrow"><?php echo $form->labelEx($model, 'build_on_your_lot'); ?>:</th>
                        <td>
                            <?php echo $form->dropDownList($model, 'build_on_your_lot', StmFormHelper::getBooleanList(), $htmlOptions=array()); ?>
                            <?php echo $form->error($model,'build_on_your_lot'); ?>
                        </td>
                    </tr>

<!--                    <tr>-->
<!--                        <th class="narrow">--><?php //echo $form->labelEx($model, 'source_id'); ?><!--:</th>-->
<!--                        <td>-->
<!--                            --><?php //echo $form->dropDownList($model, 'source_id', CMap::mergeArray(array(''=>''),Sources::optGroupList()), $htmlOptions=array('style' => 'width:325px;',
//                                                                                                                                                            'data-placeholder' => 'Select a Source',
//                                )); ?>
<!--                            --><?php //$this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#AccountingTransactions_source_id',
//                                                                                                 'options'=>array('allow_single_deselect'=>true),
//                                )); ?>
<!--                            --><?php //echo $form->error($model,'source_id'); ?>
<!--                        </td>-->
<!--                    </tr>-->
                </table>
            </div>
            <table class="container">
                <tr>
                    <th class='narrow'>
                        <?php echo $form->labelEx($model, 'description'); ?>:
                    </th>
                    <td>
                        <?php echo $form->textArea($model, 'description', $htmlOptions = array(
                                'class' => 'g11',
                                'rows' => '8'
                            )
                        ); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <?php echo CHtml::htmlButton((($model->isNewRecord) ? 'Add New' : 'Save') . ' Builder', $htmlOptions = array(
            'class' => 'submit wide',
            'type' => 'submit',
        )); ?>
</div>
<?php $this->endWidget(); ?>

