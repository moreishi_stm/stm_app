<!-- JavaScript things -->
<?
$yuiScript = 'http://yui.yahooapis.com/3.17.2/build/yui/yui-min.js';
//if(strpos($_SERVER['SCRIPT_FILENAME'], '/Users/christinelee') !== false) {
//    $yuiScript = 'http://cdn.seizethemarket.local/assets/js/yui/3.17.2/yui-min.js';
//}
?>
<script src="<?=$yuiScript?>"></script>
<script type="text/javascript">

// Use YUI things
YUI().use('node', 'panel', 'datatable', 'datasource-io', 'datasource-jsonschema', 'datatable-datasource', 'datatype-number'/*, 'gallery-datatable-footerview'*/, 'io', 'io-form', 'json', 'handlebars','node-event-simulate', 'cookie', function(Y) {

    // Setup global ajax failure handler for YUI IO
    Y.on('io:failure', function(id, o) {

        // If we had a 403, send the user back to the login page
        if(o.status == 403) {
            alert("Sorry, your session has expired. Please login again to continue.");
            window.location.href ="/admin";
        }

    }, Y, 'Transaction Failed');

    // The selected table type
    var tableType;

    // Helper function to handle the meta things that come through
    var handleMeta = function(meta){
console.log('meta:');
console.log(meta);
        // If we have this, populate the div with details
        //@todo: need to add scenario for if a call is hung up AND not started the next dial yet. This is the time for note taking
        if(meta.active && meta.active.length != 0) {
            // Only do this if the phone ID has changed
            if(currentPhoneId != meta.active.phone_id) {
                Y.one('#container-details').setHTML(Y.Handlebars.render(Y.one('#template-details-postcall').getHTML(), meta.active));
                currentPhoneId = meta.active.phone_id;
                if(defaultVoicemailSelectedIndex != -1) {
                    Y.one('#voicemail').set('selectedIndex', defaultVoicemailSelectedIndex);
                }
            }
        }
        else {
            currentPhoneId = null;
            // only has the call-answer pop-up with in debug mode
            if(<?=$debugShowAnsweredBox?>) {
                Y.one('#container-details').setHTML(Y.Handlebars.render(Y.one('#template-details-postcall').getHTML(), {"phone":"1234567890"}));
                if(defaultVoicemailSelectedIndex != -1) {
                    Y.one('#voicemail').set('selectedIndex', defaultVoicemailSelectedIndex);
                }
            }
            // Y.one('#container-details').setHTML('');
        }

        // validate session ID from the array/object data
        var isValidSessionId = false;

        if(meta.session_id && meta.session_id[<?=Yii::app()->user->id?>] != undefined) {
            isValidSessionId = true;
        }

        if(isValidSessionId && meta.event_type != 'hangupAgent') { //meta.in_progress &&
            callSessionId = meta.session_id[<?=Yii::app()->user->id?>];
            Y.one('#preset').hide();
            Y.one('#call-list-label').setHTML(Y.one('#preset option:checked').get('text'));
            Y.one('#call-list-label').show();
            Y.one('#btn-load-list').hide();

            // remove loading gif if exists
            if(Y.one('div.loading-container.loading')) { //@todo: how to handle for start session event type?? - CLee 12/6/15
                setTimeout(function(){ $("div.loading-container.loading").remove(); }, 200);    //Y.one("div.loading-container.loading").remove();   //@todo: this is jquery needs to be YUI
            }
        }
        // Call session has ended. Display start button if the calls session is not in progress. Inform user that session has ended. @todo: supply summary data.
        else {

            Y.one('#form-call-now').show();
            Y.one('#after-confirm').show();
            if(meta.event_type == 'hangupAgent' || (callSessionId != null && callSessionId != undefined && callSessionId != 'undefined')) {
                Message.messagePosition = 'top-center';
                Message.create('notice','Call session has ended. You can reload a list and start another session.');
                Y.one('#preset').show();
                //Y.one('#call-list-label').setHTML('');
                Y.one('#call-list-label').hide();
                Y.one('#btn-load-list').show();
                Y.one('#btn-start').show();
            }

            callSessionId = null;

            //
            // Show load button
            //
        }

        // remove loading gif if exists and event_type is not adminStartCall which requires longer loading gif time handled elsewhere
        if(Y.one('div.loading-container.loading') && meta.event_type != 'adminStartCall' && meta.event_type != 'answerConfirmStart') {

            //$("div.loading-container.loading").remove();                                                                                    //@todo: this is jquery needs to be YUI
            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1000);
        }
    };

    // Callable methods the server has access to
    var DialerClientFunctions = {

        /**
         * Has Action
         *
         * Used to determine if this object has a given action directly in it, will not return true for inherited functions
         * @param string action Action name to check against
         */
        hasAction: function(action) {
            return (this.hasOwnProperty(action) && action != 'hasAction');
        },

        /**
         * System Ready
         *
         * Called once the server has sorted out this websocket connection and is ready for requests
         * @param params Object
         */
        systemReady: function(params) {
            console.log('System Ready...');
        },

        /**
         * Update Data
         *
         * Updates data in data table, will clear any existing data
         * @param params Object
         */
        updateData: function(params) {

            console.log((new Date).toLocaleTimeString() + ' - Update data called!'); console.log(params);

            var filterString = '';
            if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
                filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
            }
            if(Y.one("#preset option:checked").getData("clid") != undefined) {
                filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
            }
            if(params != undefined && params.eventType != undefined && params.eventType != '') {
                filterString += '&eventType=' + params.eventType
            }

            // Update the datatable
            table.datasource.load({
                request: 'presetId=' + encodeURIComponent(presetId) + '&callsession=' + encodeURIComponent(callSessionId) + filterString
            });
        },

        /**
         * Update Data 2
         *
         * Updates data in data table, will clear any existing data
         * @param params Object
         */
        updateData2: function(params) {

            console.log('updateData2 called');

            console.log(params);

            // Update datatable data from results!?
            table.set('data', params.results);

            // Update meta data
            handleMeta(params.meta);
        }
    };

    // Manage scope
    var ws, reconnectPanel, reconnectAttempts = 0;

    // Helper function to connect to websocket server
    var initializeWebsocket = function() {

        // Create new Web Socket connection to server
        ws = new WebSocket('ws://' + Y.one('#nodeUrl').get('value') + '/dialer3?session=' + Y.Cookie.get('PHPSESSID'));

        // Do this when the connection is opened
        ws.onopen = function(e) {

            console.log('WSConnection opened');

            // If we have a reconnect panel open, destroy it
            if(reconnectPanel) {

                // Remove the reconnect panel
                reconnectPanel.destroy();

                // Reload the datatable it's been initialiezed
                if(table) {
                    DialerClientFunctions.updateData();
                }
            }

        };

        // Do this when the connection is closed
        ws.onclose = function(e) {

            console.log('WSConnection closed');

            // If we don't already have a panel
            if(!reconnectPanel && !<?=(YII_DEBUG) ? 1 : 0?>) {

                // New informational panel for reconnect
                reconnectPanel = new Y.Panel({
                    headerContent: '',
                    bodyContent: "<h2 style='margin-top: 30px; margin-bottom: 30px;'>Connecting...</h2><p style='text-align: center; font-size: 14px;'>You've been disconnected from the dialer.<br>Please hold while we reconnect you...</p>",
                    width: 500,
                    height: 200,
                    zIndex: 10000,
                    centered: true,
                    modal: true,
                    visible: true,
                    render: Y.one('#yui-modal-container')
                });
            }

            // do not attempt if in debug mode
            if(!<?=(YII_DEBUG) ? 1 : 0?>) {

                // Wait 3 seconds and attempt reconnect
                setTimeout(function() {

                    // Check if we have reached the maximum number of reconnect attempts
                    if(reconnectAttempts < 5) {

                        // Initialize the websocket
                        initializeWebsocket();
                    }
                    else {
                        reconnectPanel.destroy();
                        alert('Unable to reconnect to server, please check your internet connection.');
                    }

                    // Increment counter
                    reconnectAttempts++;

                }, 3000);
            }
        }

        // Do this when an error occurs
        ws.onerror = function(e) {
            console.log('A web socket error has occurred!');
            console.log(e);
        };

        // Do this when a message is received
        ws.onmessage = function(e) {

            // Attempt to parse JSON data
            try {
                var request = Y.JSON.parse(e.data);
            }
            catch(e) {
                console.log('Invalid JSON returned! Terminating connection!');
                console.log(e);
                ws.close();
            }

            // Make sure that our action exists before we try to run it
            if(!DialerClientFunctions.hasAction([request.action])) {
                console.log('Invalid action requested (' + request.action + ')! Terminating connection!');
                ws.close();
                return;
            }

            // Run action
            DialerClientFunctions[request.action](request.params ? request.params : {});
        };

        // Generic send command funtion to be used with Web Socket server
        ws.socketCommand = function(action, params) {
            ws.send(Y.JSON.stringify({
                action: action,
                params: params
            }));
        };
    }

    // Run initial connect
    initializeWebsocket();

    // The selected call list ID
    var callListId;

    // The selected call list ID
    var presetId;

    // The currently running call session ID
    var callSessionId;

    // The current phone ID
    var currentPhoneId;

    // default voicemail ID
    var defaultVoicemailSelectedIndex;

    // Custom formatter for phone
    var formatDate = function(o) {
        var date = new Date(o.value);
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
    }

    // Custom formatter for phone
    var formatPhone = function(o) {
        return o.value.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }

    Y.Handlebars.registerHelper("formatPhone", function(phone) {
        if(phone != undefined) {
            phone = phone.replace(/[^0-9]/g, '');
            phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        }
        else {
            console.log('Phone is undefined in registerHelper - formatPhone');
        }
        return phone;
    });

    // Custom formatter for status
    var formatStatus = function(o) {

        // Handle different kinds of statuses
        var label = '';
        switch(o.value) {
            case 'Queued':
                label = 'default';
                break;
            case 'Complete':
                label = 'info';
                break;
            case 'Answered':
                label = 'success';
                break;
            case 'Boomerang':
            case 'Skipped':
            case 'No Answer':
                label = 'danger';
                break;
            case 'Ringing':
                label = 'warning';
                break;
            default:
                return o.value;
                break;
        }

        // Return formatted label
        return '<span class="label label-' + label + '">' + o.value + '</span>'
    }

    // Custom formatter for options column
    var formatContactInfo = function(o) {
        return "<a href='/<?=$module?>/" + o.data.component_type_name + "/" + o.data.component_id + "' style='text-decoration: underline; font-weight: bold;' target='_blank'>"+ o.data.contactInfo  + "</a><br>" + o.data.phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }

    // Custom formatter for options column
    var formatTaskButtons = function(o) {
        return "<button type='button' class='complete-task-button' data='" + o.data.task_id + "' ctid='"+ o.data.component_type_id +"'>Complete</button>&nbsp;"+
            "<button type='button' class='edit-task-button' data='" + o.data.task_id + "' ctid='"+ o.data.component_type_id +"'>Edit</button>&nbsp;"+
            "<button type='button' class='delete-task-button' data='" + o.data.task_id + "' ctid='"+ o.data.component_type_id +"'>Delete</button>&nbsp;"+
            "<button type='button' class='add-task-button' data='" + o.data.component_id + "' ctid='"+ o.data.component_type_id +"'>Add</button>&nbsp;";
    }

    // Custom formatter for options column
    var formatContactTaskButtons = function(o) {
        return "<button type='button' class='add-task-button' data='" + o.data.component_id + "' ctid='"+ o.data.component_type_id +"'>Add</button>";
    }

    // Custom formatter for options column
    var formatCustomDeleteQueueButtons = function(o) {
        return "<button type='button' class='delete-number-button' data-phone-id='" + o.data.call_list_phone_id + "'>Remove Phone #</button>";
    }

    // Custom formatter for options column
    var formatLogButtons = function(o) {
        return "<button type='button' class='log-activity add-activity-log-button' data='" + o.data.component_id + "' ctid='"+ o.data.component_type_id+"'>Log Activity</button>";
    }

    // Custom formatter for options column
    var formatQueue = function(o) {

        var label = '';
        var className = ''
        switch(o.data.status) {
            case 'Queued':
                label = 'Skip';
                className = 'skip-queue';
                break;
            case 'Boomerang':
                label = 'Skip';
                className = 'skip-queue';
                break;
            case 'No Answer':
                label = 'Add to Queue';
                className = 'add-to-queue';
                break;
            case 'Skipped':
                label = 'Add to Queue';
                className = 'add-to-queue';
                break;
            case 'Ringing':
                label = 'Hangup All & Next';
                className = 'hangup-queue';
                break;
            case 'Answered':
                label = 'Hangup & Call Next';
                className = 'hangup-queue';
                break;
            case 'Complete':
                label = 'Add to Queue';
                className = 'add-to-queue';
                break;
            default:
                return;
                break;
        }
        return '<button class="' + className + '" data-id="' + o.data.call_list_phone_id + '" type="button">' + label + '</span>'
    }

    // Configurations for our various datatable configurations
    var configs = {
        resultFields: {
            task: [
                'call_list_phone_id',
                'last_called',
                'phone',
                'contactInfo',
                'status',
                'fullName',
                'description',
                'component_id',
                'component_type_id',
                'component_type_name',
                'component_name',
                'due_date',
                'task_id',
                'activity_log_id',
                'last_spoke_to_date'
            ],
            time: [
                'call_list_phone_id',
                'last_login',
                'last_called',
                'dialer_count_30_days',
                'dialer_count_90_days',
                'phone',
                'contactInfo',
                'status',
                'source',
                'fullName',
                'description',
                'component_id',
                'component_type_id',
                'component_type_name',
                'component_name',
                'added',
                'last_activity_date',
//                    'activity_count',
                'activity_log_id',
                'last_spoke_to_date'
            ],
            custom: [
                'call_list_phone_id',
                'last_login',
                'last_called',
                'dialer_count_30_days',
                'dialer_count_90_days',
                'phone',
                'contactInfo',
                'status',
                'source',
                'fullName',
                'description',
                'component_id',
                'component_type_id',
                'component_type_name',
                'component_name',
                'added',
                'last_activity_date',
                'activity_log_id',
                'last_spoke_to_date'
            ]
        },
        columns: {
            task: [
//                {key: 'phone', label: 'Phone', formatter: formatPhone, width: '120px'},
//                {key: 'contactInfo', label: 'Contact Info', allowHTML: true, width: '180px'},
                {key: 'contactInfo', label: 'Contact Info', formatter: formatContactInfo, allowHTML: true, width: '180px'},
                {key: 'description', label: 'Description'},
                {key: 'due_date', label: 'Due Date', width: '80px'},
                {key: 'last_called', label: 'Last Call<br><span style="font-size: 10px;">For this Task</span>', width: '80px'},
                {key: 'status', label: 'Status', formatter: formatStatus, allowHTML: true, width: '90px'},
                {key: 'queue', label: 'Queue', formatter: formatQueue, allowHTML: true, width: '145px'},
                {key: 'taskButtons', label: 'Tasks', formatter: formatTaskButtons, allowHTML: true, width: '300px'},
                {key: 'logButtons', label: 'Activity Logs', formatter: formatLogButtons, allowHTML: true, width: '120px'}
            ],
            time: [
                {key: 'contactInfo', label: 'Contact Info<br><span style="font-size: 10px;">Click Name for Details</span>', formatter: formatContactInfo, allowHTML: true},
                {key: 'last_login', label: 'Last Login', width: '80px'},
                {key: 'dialer_count_30_days', label: '# Dialer Calls 30 Days', width: '80px'},
                {key: 'dialer_count_90_days', label: '# Dialer Calls 90 Days', width: '80px'},
                {key: 'last_called', label: 'Last Dialer Call', width: '80px'},
                {key: 'last_spoke_to_date', label: 'Last Spoke to', width: '80px'},
//                {key: 'activity_count', label: '# Total Touches', width: '80px'},
//                {key: 'last_activity_date', label: 'Last Activity', formatter: formatDate, width: '80px'},
//                {key: 'added', label: 'Submit Date', width: '80px'},
                {key: 'status', label: 'Status', formatter: formatStatus, allowHTML: true, width: '90px'},
                {key: 'queue', label: 'Queue', formatter: formatQueue, allowHTML: true, width: '145px'},
                {key: 'taskButtons', label: 'Tasks', formatter: formatContactTaskButtons, allowHTML: true, width: '80px'},
                {key: 'logButtons', label: 'Activity Logs', formatter: formatLogButtons, allowHTML: true, width: '120px'}
            ],
            custom: [
                {key: 'contactInfo', label: 'Contact Info<br><span style="font-size: 10px;">Click Name for Details</span>', formatter: formatContactInfo, allowHTML: true},
                {key: 'last_login', label: 'Last Login', width: '80px'},
                {key: 'dialer_count_30_days', label: '# Dialer Calls 30 Days', width: '80px'},
//                {key: 'dialer_count_90_days', label: '# Dialer Calls 90 Days', width: '80px'},
//                {key: 'last_called', label: 'Last Dialer Call', width: '80px'},
//                {key: 'last_spoke_to_date', label: 'Last Spoke to', width: '80px'},
                {key: 'status', label: 'Status', formatter: formatStatus, allowHTML: true, width: '90px'},
                {key: 'queue', label: 'Queue', formatter: formatQueue, allowHTML: true, width: '145px'},
                {key: 'taskButtons', label: 'Tasks', formatter: formatContactTaskButtons, allowHTML: true, width: '80px'},
                {key: 'logButtons', label: 'Activity Logs', formatter: formatLogButtons, allowHTML: true, width: '120px'},
                {key: 'taskButtons', label: 'Remove from Queue', formatter: formatCustomDeleteQueueButtons, allowHTML: true, width: '150px'}
            ]
        }
    };


    // Handle scope for datatable and datasource references
    var table, datasource;

    // Function to create datatable
    var createDataTable = function(tableType) {

        // Destroy datasource if it already exists
        if(datasource) {
            datasource.destroy();
        }

        // Destroy datatable if it already exists
        if(table) {
            table.destroy();
        }

        // Create datasource
        datasource = new Y.DataSource.IO({
            source: '/<?=$module?>/dialer3/load'
        });

        // Bind event to update statistics when we receive data
        datasource.after('response', function(e) {
            // Use the helper function
            handleMeta(e.response.meta);

        });

        // Use POST instead of GET
        datasource.set('ioConfig', {method: 'POST'});

        // Plug JSON support in to our datasource
        datasource.plug(Y.Plugin.DataSourceJSONSchema, {
            schema: {
                metaFields: {
                    event_type: 'meta.event_type',
                    active: 'meta.active',
                    session_id: 'meta.session_id'
                },
                resultListLocator: 'results',
                resultFields: configs.resultFields[tableType]
            }
        });

        // Create a new data table
        table = new Y.DataTable({
            columns: configs.columns[tableType],
            sortable: false
//                scrollable: 'y',
//                height: '500px'

//                footerView:   Y.FooterView,
//                footerConfig: {
//                    fixed:   true,
//                    heading: {
//                        colspan:    3,
//                        content:    "{row_count} Records of ??",
//                        className:  "align-left"
//                    },
//                    columns: [
//                        { key:'NumClients', content:"{avg} avg",  className:"clientsClass", formatter:fmtComma2 },
//                        { key:'SalesTTM',   content:"{sum}", className:"salesClass" }
//                    ]
//                }
        });

        // Plug data source in to data table
        table.plug(Y.Plugin.DataTableDataSource, {
            datasource: datasource
        });

        // Render the datatable
        table.render('#dt');
    };


    // IO Fail Helper Function
    var ioFail = function(id, o, note) {

        // Make sure note is an empty string if it's undefined
        note = note || '';

        // in case loading gif is present
        $("div.loading-container.loading").remove();                                                                                    //@todo: this is jquery needs to be YUI

        if(o.responseText !== undefined){

            // prevents infinite loop of io ajax call failing and calling itself
            if(o.responseURL.indexOf("/dialer3/io") > -1) {
                console.log('IO Reporting failed.');
                return;
            }

            // send ajax call to email errors
            Y.io('/<?=$module?>/dialer3/io/', {
                method: 'POST',
                data: {
                    id: id,
                    status: o.status,
                    response: o.response,
                    message: o.statusText,
                    node: note,
                    url: o.responseURL,
                    data: JSON.stringify(o)
                },
                on: {
                    success: function(id, o) {

                        // Attempt to parse out JSON
                        try {
                            var response = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            console.log('Error parsing response from server!');
                        }

                        // Handle different response types
                        switch(response.status) {
                            case 'success':
                                console.log('Email logged IO failure response.');
                                break;

                            default:
                                break;
                        }
                    }
                }
            });
        }
    };

    // Subscribe to "io.failure".
    Y.on('io:failure', function(id, o) {

        // Call helper function
        ioFail(id, o, 'A generic failure has occurred, this came from the global event handler;');
    },Y);

    // Bind dialer max call count filter drop down changes, update the days dropdown if empty
    Y.one('.io-error').on('click', function() {
        // send ajax call to email errors
        Y.io('/<?=$module?>/dialer3/io/', {
            method: 'POST',
            data: {
                id: null,
                status: "500",
                response: "The system is unable to find the requested action 'ioFailure'.",
                message: "Not found",
                url: 'http://www.christineleeteam.local/dialer3/something'
            },
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        console.log('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            //console.log('Successfully Email logged IO failure response.');
                            break;

                        default:
                            break;
                    }
                }
            }
        });
    });

    // Bind click event for resume button
    Y.one('#btn-load-list').on('click', function(e) {

        // clear call answer box in case there's any content present
        Y.one('#container-details').setHTML('');

        // Add loading gif
        if(!Y.one('div.loading-container.loading')) {
            $("body").prepend('<div class="loading-container loading"><em></em></div>');                //@todo: this is jquery needs to be YUI
        }

        // Set call list ID
        // callListId = Y.one('#session').get('value'); //@todo: not using right now, replaced with presetId

        // Set call list preset ID
        presetId = Y.one('#preset').get('value');

        // Stop if we don't have a call list ID
        if(!presetId) { //!callListId == "undefined"
            Message.messagePosition = 'top-center';
            Message.create('warning','Please select a Call List.');
            $("div.loading-container.loading").remove();                                                //@todo: this is jquery needs to be YUI
            return;
        }

        // Create datatable with the current type we want
        createDataTable(Y.one("#preset option:checked").getData("tableType"));     // This is where you drop in the table configuration name, had default in there

        var filterString = '';
        if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
            filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
        }

        if(Y.one("#preset option:checked").getData("clid") != undefined) {
            filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
        }

        // Reload datatable
        table.datasource.load({
            request: 'presetId=' + encodeURIComponent(presetId) +'&eventType=adminLoadButton&initialize=1' + filterString
        });

        Y.one('#after-confirm').show();
        Y.one('#form-call-now').show();

        if(presetId == <?php echo CallLists::ALL_NURTURING_SELLERS;?> || presetId == <?php echo CallLists::ALL_NURTURING_BUYERS;?>) {
//            if(Y.one('#maxDialerCallCount').get('value') == '') {
            Y.one('#maxDialerCallCount').set('value',<?=Dialer3Controller::FILTER_DEFAULT_MAX_CALL_COUNT?>);
//            }

//            if(Y.one('#maxDialerCallDays').get('value') == '') {
            Y.one('#maxDialerCallDays').set('value',<?=Dialer3Controller::FILTER_DEFAULT_MAX_CALL_DAYS?>);
//            }

            $('#call-filter-row').hide();
        }
        else if(presetId == <?php echo CallLists::CUSTOM_CONTACTS;?> || presetId == <?php echo CallLists::CUSTOM_BUYERS;?> || presetId == <?php echo CallLists::CUSTOM_SELLERS;?> || presetId == <?php echo CallLists::CUSTOM_RECRUITS;?>) {
            $('#call-filter-row').hide();
            Y.one('#maxDialerCallDays').set('value', '7');
            Y.one('#maxDialerCallCount').set('value', '30');
        }
        else {
            $('#call-filter-row').hide();
            Y.one('[name="maxDialerCallCount"]').set('value', '');
            Y.one('[name="maxDialerCallDays"]').set('value', '');
        }

        // position screen to top of call data
        window.location.hash='viewTop';                                                                 // @todo: NICOLE  - this is NOT working - HELP!!!!!!!!!!!!***********************

        // remove loading gif
        //setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);                    //@todo: this is jquery needs to be YUI

    });

    // Bind start button event
    Y.one('#form-call-now').on('submit', function(e) {

        // Prevent default
        e.preventDefault();
        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                    //@todo: this is jquery needs to be YUI
        var errorFlag = false;
        var errorMessage;

        // reset phone id value, not resetting will cause active call details to not show if you have to redial the same number
        currentPhoneId = null;

        callSessionId = null;

        // position screen to top of call data
        window.location.hash='viewTop';

        // Stop if we don't have a call list ID
        if(!presetId) { //!callListId == "undefined"
            errorMessage = 'Please select a Call List and confirm.';
            errorFlag = true;
        }

        // Stop if we don't have a phone number
        if(!Y.one('#from').get('value') || Y.one('#from').get('value').replace(/\D/g,'').length < 10) {                                 //@todo: this is jquery needs to be YUI
            errorMessage = 'Please enter your valid 10 digit phone number to call.';
            errorFlag = true;
        }

        // Stop if we don't have a caller ID #
        if(!Y.one('#callerIdNumber').get('value') || Y.one('#callerIdNumber').get('value').replace(/\D/g,'').length < 10) {                                 //@todo: this is jquery needs to be YUI
            errorMessage = 'Please enter a valid 10 digit Caller ID Number.';
            errorFlag = true;
        }

        // Stop if we don't have a caller ID name
        if(!Y.one('#callerIdName').get('value')) {                                 //@todo: this is jquery needs to be YUI
            errorMessage = 'Please a valid Caller ID Name.';
            errorFlag = true;
        }

        // Stop if max count filter has value, but days filter empty, display notice to make selection
        if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value') == '') {                                 //@todo: this is jquery needs to be YUI
            errorMessage = 'Please select filter for number of Days/Months.';
            errorFlag = true;
        }

        if(errorFlag) {
            Message.messagePosition = 'top-center';
            Message.create('notice', errorMessage);
            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1000);                                //@todo: this is jquery needs to be YUI
            return;
        }

        // hide local call and start call button so they don't press it again during a session.
        Y.one('#btn-load-list').hide();
        Y.one('#btn-start').hide();

        Y.one('#preset').hide();
        Y.one('#call-list-label').setHTML(Y.one('#preset option:checked').get('text'));
        Y.one('#call-list-label').show();

        // save the number in a cookie
        document.cookie="dialerNumber="+Y.one('#from').get('value').replace(/\D/g,'');                                                  //@todo: this is jquery needs to be YUI
        document.cookie="callerIdNumber="+Y.one('#callerIdNumber').get('value').replace(/\D/g,'');                                                  //@todo: this is jquery needs to be YUI
        document.cookie="callerIdName="+Y.one('#callerIdName').get('value').replace(/\D/g,'');                                                  //@todo: this is jquery needs to be YUI

        presetId = Y.one('#preset').get('value');
        defaultVoicemailSelectedIndex = Y.one('#default_voicemail').get('selectedIndex');
        console.log('vmindex:' + defaultVoicemailSelectedIndex);
        // Refresh the data table
        var filterString = '';
        if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
            filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
        }

        if(Y.one("#preset option:checked").getData("clid") != undefined) {
            filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
            callListId = Y.one("#preset option:checked").getData("clid")
        }

        if(callListId == undefined) {
            callListId = '';
        }

        // Update the datatable
        table.datasource.load({
            request: 'presetId=' + encodeURIComponent(presetId) + '&eventType=adminStartCall&callsession=' + encodeURIComponent(callSessionId) + '&initialize=1' + filterString
        });

        // Make the call
        Y.io('/<?=$module?>/dialer3/start', {
            method: 'POST',
            data: {
                preset_id: presetId,
                call_list_id: callListId,
                phone_number: Y.one('#from').get('value'),
                num_to_call:  Y.one('#num_to_call').get('value'),
                callerIdName:  Y.one('#callerIdName').get('value'),
                callerIdNumber:  Y.one('#callerIdNumber').get('value'),
                maxDialerCallCount:  Y.one('#maxDialerCallCount').get('value'),
                maxDialerCallDays:  Y.one('#maxDialerCallDays').get('value'),
            },
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                        Y.one('#preset').show();
                        Y.one('#call-list-label').setHTML('');
                        Y.one('#call-list-label').hide();
                        $("div.loading-container.loading").remove();
                        return;
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':

                            // Hide start button
                            Y.one('#form-call-now').hide();
                            Y.one('#after-confirm').hide();

                            Y.one('#container-details').setHTML('');
                            Message.messagePosition = 'top-center';
                            Message.create('success','Calling your phone now. Answer and press 1 to confirm session.');

                            // Set call session ID
                            callSessionId = response.call_session_id;

                            // Notify Node that we started a session
                            ws.socketCommand('startSession', {
                                call_list_id: response.call_list_id,
                                call_session_id: response.call_session_id
                            });

                            //
                            // Hide load button
                            //

                            break;

                        case 'sessionExists':
                            Message.messagePosition = 'top-center';
                            Message.create('error','A session is already in progress. Please end that session and try again.');
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1200);                                //@todo: this is jquery needs to be YUI
                            Y.one('#btn-load-list').show();
                            Y.one('#btn-start').show();

                            Y.one('#preset').show();
                            Y.one('#call-list-label').setHTML('');
                            Y.one('#call-list-label').hide();

                            break;

                        case 'error':
                            Message.messagePosition = 'top-center';
                            Message.create('error','There was an error starting the call session. Please try again.');
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1200);                                //@todo: this is jquery needs to be YUI
                            Y.one('#btn-load-list').show();
                            Y.one('#btn-start').show();

                            Y.one('#preset').show();
                            Y.one('#call-list-label').setHTML('');
                            Y.one('#call-list-label').hide();

                            break;

                        default:
                            Message.messagePosition = 'top-center';
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 1000);                                   //@todo: this is jquery needs to be YUI
                            Message.create('error','An unknown error has occurred!');
                            Y.one('#btn-load-list').show();
                            Y.one('#btn-start').show();

                            Y.one('#preset').show();
                            Y.one('#call-list-label').setHTML('');
                            Y.one('#call-list-label').hide();

                            //
                            // Show load button
                            //

                            break;
                    }
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Start action has failed!');
                }
            }
        });
    });

// Bind click even for close notes area button
Y.one('#container-details').delegate('click', function() {

    // check to see if there's any data in the notes section as it may be a mistake to click to close button
    Y.one('[name="ActivityLog[is_spoke_to]"]').set('value', 1);
    Y.one('#container-details').submit();

}, '.mark-spoke-to-button');

// Handle form submit
    Y.one('#container-details').delegate('submit', function(e) {

        // Prevent default
        e.preventDefault();

        if(e.currentTarget.one('[name="ActivityLog[is_spoke_to]"]').get('value') === '') {
            alert('Please notate whether or not you have spoke to the client');
            return;
        }

        // Make sure we selected these items
        if(e.currentTarget.one('[name="ActivityLog[lead_gen_typ_ma]"]').get('value') === '') {
            alert('Please select a lead gen type to continue');
            return;
        }

        if(e.currentTarget.one('[name="ActivityLog[is_spoke_to]"]').get('value') === '') {
            alert('Please enter a valid note');
            return;
        }

        if(e.currentTarget.one('[name="ActivityLog[note]"]').get('value').length < 6) {
            alert('Your Log Note is too short. Please be more descriptive.');
            return;
        }

        // Create new call list
        Y.io('/<?=$module?>/activityLog/add', {
            method: 'POST',
            form: {
                id: e.currentTarget
            },
            on: {
                success: function(id, o) {

                    // Hide form and show success message
                    e.currentTarget.hide();
                    e.currentTarget.ancestor().one('.activity-success-message').show();
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Activity Log has failed!');
                }
            }
        });
    }, '.activity-form');

    // Bind click even for close notes area button
    Y.one('#container-details').delegate('click', function() {

        // check to see if there's any data in the notes section as it may be a mistake to click to close button
        if(Y.one('[name="ActivityLog[note]"]').get('value')) {
            if(!confirm('You have information in the activity log notes. Are you sure want to close this area?')) {
                return false;
            }
        }
        Y.one('#container-details').setHTML('');

    }, 'button.close-notes-area');

    // Bind dialer max call count filter drop down changes, update the days dropdown if empty
    Y.one('#maxDialerCallCount').on('change', function() {

        if(Y.one('#maxDialerCallCount').get('value') && !Y.one('#maxDialerCallDays').get('value')) {
            Y.one('#maxDialerCallDays').set('selectedIndex', 3);
        }

    });

    // Bind add another activity log button
    Y.one('#container-details').delegate('click', function() {

        // Clear data
        Y.one('[name="ActivityLog[note]"]').set('value', '');
        Y.one('[name="ActivityLog[lead_gen_typ_ma]"]').set('value', '');
        Y.one('[name="ActivityLog[is_spoke_to]"]').set('value', '');

        // Swap containers
        Y.one('.activity-form').show();
        Y.one('.activity-success-message').hide();

    }, 'button.activity-log-add-another');

    // Bind add another activity log button
    Y.one('#listview-actions').delegate('click', function() {

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI

        if(table == undefined) {
            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 2000);
            return;
        }
        // Refresh the data table
        var filterString = '';
        if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
            filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
        }
        if(Y.one("#preset option:checked").getData("clid") != undefined) {
            filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
        }

        // Update the datatable
        table.datasource.load({
            request: 'presetId=' + encodeURIComponent(presetId) + '&eventType=adminLoadDebugButton&callsession=' + encodeURIComponent(callSessionId) + filterString
        });

        setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);

    }, 'button.load-list');

    // Bind add another activity log button
    Y.one('#listview-actions').delegate('click', function() {

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI
        console.log('answerCheck');
        Y.io('/<?=$module?>/dialer3/answerCheck', {
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                        Message.create('success','Successfully sent message to support.');
                        //Message.create('notice','Please email an screenshot immediately to support to match this check and include what List you were on.');
                        if(response.meta.active != null) {
                            Y.one('#container-details').setHTML(Y.Handlebars.render(Y.one('#template-details-postcall').getHTML(), response.meta.active));
                            currentPhoneId = response.meta.active.phone_id;
                        }

                        // Refresh the data table
                        var filterString = '';
//                        if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
//                            filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
//                        }
//                        if(Y.one("#preset option:checked").getData("clid") != undefined) {
//                            filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
//                        }

                        // Update the datatable
//                        table.datasource.load({
//                            request: 'presetId=' + encodeURIComponent(presetId) + '&callsession=' + encodeURIComponent(callSessionId) + filterString
//                        });
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Answer check has failed!');
                }
            }
        });

        setTimeout(function(){ $("div.loading-container.loading").remove(); }, 300);

    }, 'button#answer-check');

    //remove from custom call list
    Y.one('#container-details').delegate('click', function(e) {

        if(!confirm('Confirm removing from selected Call List.')) {
            return;
        }

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI

        Y.io('/<?=$module?>/dialer3/removeFromCallList', {
            method: 'POST',
            data: {
                callListId: e.currentTarget.getData('id'),
                phoneId:  e.currentTarget.getData('pid'),
                componentTypeId:  e.currentTarget.getData('ctid'),
                componentId:  e.currentTarget.getData('cid')
            },
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);

                        if(response.status == 'success') {
                            Message.create('success','Successfully removed from Call List.');
                            Y.one('#call-list-tag-' + response.callListId ).hide();
                        }
                        else if(response.status == 'error') {
                            Message.create('error','Error removing from Call List.');
                        }
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Remove from call list has failed!');
                }
            }
        });

        setTimeout(function(){ $("div.loading-container.loading").remove(); }, 300);

    }, '.remove-from-call-list');

    // Bind click even for voicemail button. This will update the activity log as answered to voicemail.
    Y.one('#container-details').delegate('click', function(e) {

        // prevent double clicking
        e.currentTarget.setAttribute('disabled','disabled');

        // this prevents someone from accidentally clicking on voicemail and eliminating a legitimate call.
        // @todo: Need to re-do flow later so maybe it asks for the confirmation FIRST before showing other stuff. That way it will prevent overriding a legitimate call.

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI
        // Log as voicemail, hangup  and update status
        Y.io('/<?=$module?>/dialer3/gotVoicemail/' + e.currentTarget.getAttribute('data-id'), {
            method: 'POST',
            on: {
                success: function(id, o) {

                    Y.one('#vm-mark-no-answer').removeAttribute('disabled');

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully logged voicemail.');
                            // Y.one('#container-details').setHTML('');
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);

                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','Voicemail log did not save. An unknown error has occurred!');
                            break;
                    }

                    // re-enables button that was disabled to prevent double clicking
                    // Y.one('button#vm-mark-no-answer').setAttribute('disabled',false); //removing for now. prolly comes back re-enabled from handlebar template
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Got Voicemail has failed!');
                }
            }
        });
    },'button#vm-mark-no-answer');

    Y.one('#container-details').delegate('change', function(e) {
        if(e.currentTarget.get('value')) {
            e.currentTarget.removeClass('error');
        }
    },'select#voicemail');

    // Bind click even for leaving voicemail
    Y.one('#container-details').delegate('click', function(e) {
        // prevent double clicking
        e.currentTarget.setAttribute('disabled','disabled');

        if(!Y.one('#voicemail').get('value')) {
            Message.messagePosition = 'top-center';
            Message.create('notice','Please select a Voicemail.');
            Y.one('#voicemail').addClass('error');
            e.currentTarget.removeAttribute('disabled');
            return;
        }

        if(!callSessionId) {
            Message.messagePosition = 'top-center';
            Message.create('notice','This dial session has ended. Cannot leave voicemail.');
            Y.one('#voicemail').removeClass('error');
            return;
        }

        // this prevents someone from accidentally clicking on voicemail and eliminating a legitimate call.
        // @todo: Need to re-do flow later so maybe it asks for the confirmation FIRST before showing other stuff. That way it will prevent overriding a legitimate call.

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI
        // Log as voicemail, hangup  and update status
        Y.io('/<?=$module?>/dialer3/leaveVoicemail/id/' + e.currentTarget.getAttribute('data-id') + '/voicemailId/' + Y.one('#voicemail').get('value'), {
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully logged voicemail.');
                            // Y.one('#container-details').setHTML('');
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);
                            break;

                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','Voicemail log did not save. An unknown error has occurred!');
                            break;
                    }
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Leave voicemail has failed!');
                }
            }
        });
        e.currentTarget.removeAttribute('disabled');

    },'button#leave-voicemail-button');

    // Bind click even for hangup and next button.
    Y.one('#container-details').delegate('click', function(e) {

        // prevent double clicking
        e.currentTarget.setAttribute('disabled','disabled');

        // this prevents someone from accidentally clicking on hangup and eliminating a legitimate call.

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI
console.log(e.currentTarget.getAttribute('data-id'));
        // hangup call and dial next
        Y.io('/<?=$module?>/dialer3/hangupSpecificCall/id/' + e.currentTarget.getAttribute('data-id'), {
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully hung up call. Dialing next number.');
                            //Y.one('#container-details').setHTML('');
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);

                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }

                    // re-enables button that was disabled to prevent double clicking
                    Y.one('button.hang-up-call-next').setAttribute('disabled',false);
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Hangup Specific Call has failed!');
                }
            }
        });

    },'button.hang-up-call-next');

    // Bind click even for hangup and next button.
    Y.one('#container-details').delegate('click', function(e) {

        // prevent double clicking
        e.currentTarget.setAttribute('disabled','disabled');

        // this prevents someone from accidentally clicking on hangup and eliminating a legitimate call.
        if(confirm("Confirm deleting this as bad number.")) {

            $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI

            // hangup call and dial next
            Y.io('/<?=$module?>/contacts/phoneStatus', {
                method: 'POST',
                data: {
                    "Phones[id]": e.currentTarget.getData('pid'),
                    actionType : 'deleteBadNumber',
                    phoneOriginComponentTypeId:  e.currentTarget.getData('ctid'),
                    phoneOriginComponentId:  e.currentTarget.getData('cid')
                },
                on: {
                    success: function(id, o) {

                        // hangup call and dial next
                        Y.io('/<?=$module?>/dialer3/deleteBadNumber/' + Y.one('button.delete-bad-number').getAttribute('data-id'), {
                            method: 'POST',
                            on: {
                                success: function(id, o) {

                                    // Attempt to parse out JSON
                                    try {
                                        var response = Y.JSON.parse(o.responseText);
                                    }
                                    catch(e) {
                                        alert('Error parsing response from server!');
                                    }

                                    // Handle different response types
                                    switch(response.status) {
                                        case 'success':
                                            Message.messagePosition = 'top-center';
                                            Message.create('success','Successfully Deleted Bad Number permanently from Contact and logged activity. Dialing next number.');
                                            // Y.one('#container-details').setHTML('');
                                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);

                                            break;
                                        default:
                                            Message.messagePosition = 'top-center';
                                            Message.create('error','An unknown error has occurred!');
                                            break;
                                    }

                                    // re-enables button that was disabled to prevent double clicking
                                    if(document.getElementById('delete-bad-number')) {
                                        Y.one('button.delete-bad-number').setAttribute('disabled',false);
                                    }
                                }
                            }
                        });
                    },
                    failure: function(id, o) {

                        // Call helper function
                        ioFail(id, o, 'Phone status has failed!');
                    }
                }
            });
        }
    },'button.delete-bad-number');

    Y.one('#container-details').delegate('change', function(e) {
        if(e.currentTarget.get('value')) {
            e.currentTarget.removeClass('error');
        }
    },'select#do-not-call-days');

    // Bind click even for adding to do not call list
    Y.one('#container-details').delegate('click', function(e) {

        if(Y.one('#do-not-call-days').get('value') == '') {
            Message.messagePosition = 'top-center';
            Message.create('notice','Please select a Do Not Call Days.');
            Y.one('#do-not-call-days').addClass('error');
            e.currentTarget.removeAttribute('disabled');
            return;
        }

        // prevent double clicking
        e.currentTarget.setAttribute('disabled','disabled');

        // this prevents someone from accidentally clicking on hangup and eliminating a legitimate call.
        if(confirm("Confirm adding "+ this.getAttribute('data-phone') +" to do not call list.")) {

            $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                            //@todo: this is jquery needs to be YUI
            // hangup call and dial next
            Y.io('/<?=$module?>/dialer3/addDoNotCall/id/' + this.getAttribute('data-id') + '/phoneNumber/' + this.getAttribute('data-phone') + '/ctid/' + this.getAttribute('data-ctid') + '/cid/' + this.getAttribute('data-cid') + '/days/' + Y.one('#do-not-call-days').get('value'), {
                method: 'POST',
                on: {
                    success: function(id, o) {

                        // Attempt to parse out JSON
                        try {
                            var response = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Error parsing response from server!');
                        }

                        // Handle different response types
                        switch(response.status) {
                            case 'success':
                                Message.messagePosition = 'top-center';
                                Message.create('success','Successfully added to Do Not Call List.');
                                setTimeout(function(){ $("div.loading-container.loading").remove(); }, 500);

                                break;
                            default:
                                Message.messagePosition = 'top-center';
                                Message.create('error','An unknown error has occurred!');
                                break;
                        }

                        // re-enables button that was disabled to prevent double clicking
                        Y.one('button.add-to-do-not-call').setAttribute('disabled',false);
                    },
                    failure: function(id, o) {

                        // Call helper function
                        ioFail(id, o, 'Add do not call has failed!');
                    }
                }
            });
        }
    },'button.add-to-do-not-call');

    // Bind click even for add to queue button. This will add the phone number on call list to the Queue to be dialed
    Y.one('#dt').delegate('click', function() {
        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                                    //@todo: this is jquery needs to be YUI
        // Add this call list phone back into the queue
        Y.io('/<?=$module?>/dialer3/addToQueue/' + this.getAttribute('data-id'), {
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':

                            // Refresh the data table
                            var filterString = '';
                            if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
                                filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
                            }
                            if(Y.one("#preset option:checked").getData("clid") != undefined) {
                                filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
                            }

                            // Update the datatable
                            table.datasource.load({
                                request: 'presetId=' + encodeURIComponent(presetId) + '&eventType=adminAddToQueue&callsession=' + encodeURIComponent(callSessionId) + filterString
                            });
                            $("div.loading-container.loading").remove();                                                                                    //@todo: this is jquery needs to be YUI


                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully added to Queue.');
                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Add to Queue has failed!');
                }
            }
        });

    },'button.add-to-queue');

    // Bind click even for skip queue button. This will skip the phone number on call list to the Queue to be dialed
    Y.one('#dt').delegate('click', function() {

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                                            //@todo: this is jquery needs to be YUI
        // Add this call list phone back into the queue
        Y.io('/<?=$module?>/dialer3/skipQueue/' + this.getAttribute('data-id'), {
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            // Refresh the data table
                            var filterString = '';
                            if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
                                filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
                            }
                            if(Y.one("#preset option:checked").getData("clid") != undefined) {
                                filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
                            }

                            // Update the datatable
                            table.datasource.load({
                                request: 'presetId=' + encodeURIComponent(presetId) + '&eventType=adminSkipQueue&callsession=' + encodeURIComponent(callSessionId) + filterString
                            });
                            setTimeout(function(){ $("div.loading-container.loading").remove(); }, 2000);                                             //@todo: this is jquery needs to be YUI

                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully skipped Queue.');
                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Skip Queue has failed!');
                }
            }
        });
        //setTimeout(function(){ $("div.loading-container.loading").remove(); }, 2000);

    },'button.skip-queue');

    // Bind click even for pause queue button. This will hangup and the phone number on call list and pause the session
    Y.one('#dt').delegate('click', function() {

        $("body").prepend('<div class="loading-container loading"><em></em></div>');                                                                        //@todo: this is jquery needs to be YUI
        // Add this call list phone back into the queue
//        var callerIdNumber = Y.one('#callerIdNumber').get('value').replace(/\D/g, '');        // Commenting this line out becasue the variable being set is NEVER used...
        Y.io('/<?=$module?>/dialer3/hangupCallListPhone/id/' + this.getAttribute('data-id'), { //hangupSpecificCall ... need to retrieve call_id
            method: 'POST',
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully Ended Call.');

                            // Refresh the data table
                            var filterString = '';
                            if(Y.one('#maxDialerCallCount').get('value') && Y.one('#maxDialerCallDays').get('value')) {
                                filterString = '&maxDialerCallCount=' + Y.one('#maxDialerCallCount').get('value') +'&maxDialerCallDays=' + Y.one('#maxDialerCallDays').get('value');
                            }
                            if(Y.one("#preset option:checked").getData("clid") != undefined) {
                                filterString += '&callListId=' + Y.one("#preset option:checked").getData("clid");
                            }

                            // Update the datatable
                            table.datasource.load({
                                request: 'presetId=' + encodeURIComponent(presetId) + '&eventType=adminHangupQueue&callsession=' + encodeURIComponent(callSessionId) + filterString
                            });

                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }

                    setTimeout(function(){ $("div.loading-container.loading").remove(); }, 2000);                                             //@todo: this is jquery needs to be YUI
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Hangup Call List Phone has failed!');
                }
            }
        });

    },'button.hangup-queue');

    // Add delete button for custom lists
    var panel;
    Y.one('#dt').delegate('click', function(e) {

        // Create a new panel
        panel = new Y.Panel({
            headerContent: '',
            bodyContent: "<h2 style='margin-top: 35px; margin-bottom: 20px;'>Confirm Delete</h2><div style='text-align: center;' data-phone-id='" + e.currentTarget.getData('phone-id') + "'><button type='button' class='confirm-delete-number' data-type='one'>This Number Only</button><br><br><button type='button' class='confirm-delete-number' data-type='all'>All Numbers For Contact</button><a href='javascript:void(0)' class='cancel-delete-number' style='margin-top: 15px; font-size: 15px; font-weight: bold; display: block;'>[ Cancel ]</a></div>",
            width: 350,
            height: 250,
            zIndex: 10000,
            centered: true,
            modal: true,
            visible: true,
            render: Y.one('#yui-modal-container'),
            hideOn: [
                {
                    // When we don't specify a `node`,
                    // it defaults to the `boundingBox` of this Panel instance.
                    eventName: 'clickoutside'
                },
                {
                    // Listen to click events on the `node` that was specified.
                    node     : Y.one('.cancel-delete-number'),
                    eventName: 'click'
                }
            ]
        });

    }, '.delete-number-button');

    // Add this number only delete
    Y.one('#yui-modal-container').delegate('click', function(e) {

        // Destroy the panel
        panel.destroy();

        // Add loading dialog
        $("body").prepend('<div class="loading-container loading"><em></em></div>');

        // Make AJAX call for deletion
        Y.io('/<?=$module?>/dialer3/deletecalllistphone', {
            method: 'POST',
            data: {
                call_list_phone_id: e.currentTarget.ancestor('div').getData('phone-id'),
                type: e.currentTarget.getData('type')
            },
            on: {
                success: function(id, o) {

                    // Attempt to parse out JSON
                    try {
                        var response = Y.JSON.parse(o.responseText);
                    }
                    catch(e) {
                        alert('Error parsing response from server!');
                    }

                    // Handle different response types
                    switch(response.status) {
                        case 'success':
                            Message.messagePosition = 'top-center';
                            Message.create('success','Successfully Deleted Number.');
                            break;
                        default:
                            Message.messagePosition = 'top-center';
                            Message.create('error','An unknown error has occurred!');
                            break;
                    }
                },
                complete: function() {
                    setTimeout(function(){ $("div.loading-container.loading").remove(); }, 2000);                                             //@todo: this is jquery needs to be YUI
                    DialerClientFunctions.updateData();
                },
                failure: function(id, o) {

                    // Call helper function
                    ioFail(id, o, 'Delete Call List Phone has failed!');
                }
            }

        });

    }, '.confirm-delete-number');
});
</script>