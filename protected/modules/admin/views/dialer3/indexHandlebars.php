<!-- Templates -->
<script id="template-details-incall" type="text/x-handlebars-template">
    <hr>
    <h1>{{phone}}</h1>
    <div class='live-activity-buttons'>
        <button type='button' data-type='live-answer'>Live Answer</button>
        <button type='button' data-type='no-answer'>No Answer</button>
        <button type='button' data-type='busy'>Busy [???]</button>
        <button type='button' data-type='remove-bad-number'>Remove Bad Number</button>
        <button type='button' data-type='fax'>Fax Line</button>
    </div>
    <hr>
</script>

<script id="template-details-postcall" type="text/x-handlebars-template">
    <hr>
    <div style='display: table; width: 100%;'>
        <div style='display: table-row'>
            <div style='display: table-cell; width: 36%; padding-left: 2%; padding-right: 2%;'>
                <h3>Name</h3>
                <a href='/<?=$module?>/{{component_name}}/{{component_id}}' target='_blank'>{{first_name}} {{spouse_first_name}} {{last_name}}</a><br>
                {{formatPhone phone}} &nbsp; <button id="delete-bad-number" class="button gray icon i_stm_delete grey-button delete-bad-number" data-id='{{call_id}}' data-pid="{{phone_id}}" data-ctid="{{component_type_id}}" data-cid="{{component_id}}">Delete Bad Number</button>
                <br>
                <br>
                <a href='/<?=$module?>/{{component_name}}/{{component_id}}' class="answer-buttons button" style="width: 87%;" target='_blank'>View {{component_label}} Details</a>
                <br>
                {{#if address}}
                <br>
                <h3>
                    Address
                    <a target="_blank" href="http://www.zillow.com/homes/{{address}},-{{city}},-{{state}},-{{zipcode}}_rb">
                        <img style="margin-left: 20px; top: 5px; position: relative; box-shadow: 0 0 3px #a3a3a3; -webkit-box-shadow: 0 0 3px #a3a3a3; -moz-box-shadow: 0 0 3px #a3a3a3;" width="76" height="26" src="http://cdn.seizethemarket.com/assets/images/zillow_mini_logo_2.png" alt="Zillow">
                    </a>
                    <a target="_blank" href="http://maps.google.com/maps?geocode=&amp;q={{address}} {{city}}, {{state}} {{zipcode}}">
                        <img style=" top: 5px; position: relative;  margin-left: 8px; box-shadow: 0 0 3px #a3a3a3; -webkit-box-shadow: 0 0 3px #a3a3a3; -moz-box-shadow: 0 0 3px #a3a3a3;" width="76" height="26" src="http://cdn.seizethemarket.com/assets/images/google_maps_mini_logo.png" alt="Google Maps">
                    </a>
                </h3>
                {{address}}<br>
                {{city}}, {{state}} {{zipcode}}<br>
                {{/if}}
                {{#if source}}
                <strong>Source:</strong>{{source}}<br>
                {{/if}}
                {{#if description}}
                <br>
                <h3>Task Description</h3>
                <i>{{description}}</i><br>
                {{/if}}
                <br>
                <hr/>
                <div class='task-buttons'>
                    <h3>Add New Task (defaults to Phone)</h3>
                    <button type='button' class='add-task-button phone-call-button' data-days='0' data='{{component_id}}' ctid='{{component_type_id}}'>Today</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='1' data='{{component_id}}' ctid='{{component_type_id}}'>Tomorrow</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='5' data='{{component_id}}' ctid='{{component_type_id}}'>5 Days</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='7' data='{{component_id}}' ctid='{{component_type_id}}'>1 Week</button>
                    <button type='button' class='add-task-button phone-call-button' data-days='30' data='{{component_id}}' ctid='{{component_type_id}}'>1 Month</button>
                    <button type='button' class='add-task-button phone-call-button' data='{{component_id}}' ctid='{{component_type_id}}'>Custom Date</button>
                    <br/>
                    <br/>
                    <a href='/<?=$module?>/{{component_name}}/edit/{{component_id}}' class="answer-buttons button" style="width: 87%;" target='_blank'>Set Appointment</a>
                    <br/>
                </div>
                <br/>
                <hr/>
                <h3>
                    Call Lists (Custom)
                    <button id="add-to-dialer-list-button" class="button gray icon i_stm_add grey-button add-to-dialer-list-button" data-id="{{call_id}}" data-pid="{{phone_id}}" data-ctid="{{component_type_id}}" data-cid="{{component_id}}">Add to List</button>
                </h3>
                <div id="call-list-tag-container">
                    {{#each call_list_ids}}
                    <div id="call-list-tag-{{id}}" class="call-list-tag"><span class="label">{{name}}</span><span class="remove-from-call-list" data-id="{{id}}" data-pid="{{phone_id}}" data-ctid="{{component_type_id}}" data-cid="{{component_id}}">X</span></div>
                    {{/each}}
                </div>
            </div>
            <div style='display: table-cell; width: 50%;'>
                <div style='margin-top: 20px;'>
                    <div id="answered-call-directions"><h2>Congrats, you have an Answered call.</h2><br>Log notes in the activity log below and Save.</div>
                    <h3>Activity Log Notes</h3>
                    <form method='POST' class='activity-form'>
                        <textarea rows='4' style='width: 100%; max-width: 607px;' name='ActivityLog[note]' placeholder="Add Notes & Save"></textarea><br>
                        <select multiple='multiple' name='ActivityLog[is_spoke_to]' style='width: 100%; max-width: 200px; height: 100%; border: 4px #00A500 solid;'>
                            <option selected value=''>- Select Spoke To -</option>
                            <option value='1'>Yes</option>
                            <option value='0'>No</option>
                        </select>
                        <select multiple='multiple' name='ActivityLog[lead_gen_typ_ma]' style='width: 100%; max-width: 200px; height: 100%;'>
                            <option selected value=''>- Select Lead Gen Type -</option>
                            <option value='1'>New</option>
                            <option value='2'>Followup</option>
                            <option value='0'>None</option>
                        </select>
                        <select multiple='multiple' name='ActivityLog[asked_for_referral]' style='width: 100%; max-width: 200px; height: 100%'>
                            <option selected value=''>- Select Asked for Referral -</option>
                            <option value='1'>Yes</option>
                            <option value='0'>No</option>
                        </select>
                        <br>
                        <input type='hidden' name='ActivityLog[component_id]' value='{{component_id}}'>
                        <input type='hidden' name='ActivityLog[component_type_id]' value='{{component_type_id}}'>
                        <input type='hidden' name='ActivityLog[activity_date]' value='{{activity_date}}'>
                        <input type='hidden' name='ActivityLog[scenario]' value='dialer'>
                        <input type='hidden' name='ActivityLog[task_type_id]' value='42'>
                        <input type='hidden' name='ActivityLog[task_id]' value='{{task_id}}'>
                        <input type='hidden' name='ActivityLog[activity_date]' value='<?=date('Y-m-d H:i:s')?>'>
                        <input type='hidden' name='is_dialer' value='true'>
                        <input type='hidden' name='referrerUrl' value='<?=$_SERVER['REQUEST_URI']?>'>
                        <input type='hidden' name='call_id' value='{{call_id}}'>
                        {{#if task_id}}
                        <button type='button' class="answer-buttons tall two-third complete-task-button" data="{{task_id}}" ctid="{{component_type_id}}">Complete Task</button>
                        {{/if}}
                        <button type='submit' class="answer-buttons tall two-third mark-spoke-to-button">Spoke to & Save Log</button>
                        <button type='submit' class="answer-buttons tall third log-activity-button">Add Activity Log</button><br/><br/>

                        <button type='button' id="vm-mark-no-answer" class="answer-buttons third" data-id='{{activity_log_id}}'>Ignore Voicemail</button>
                        <? if(!empty($voicemailList)):?>
                        <button type='button' id="leave-voicemail-button" class="answer-buttons third" data-id='{{call_id}}'>Leave Voicemail</button>
                        <select id="voicemail">
                            <option value="">-- Select One --</option>
                            <?php
                                foreach($voicemailList as $voicemail) {
                                    echo '<option value="'.$voicemail['id'].'">'.$voicemail['title'].'</option>';
                                }
                            ?>
                        </select><br>
                        <? endif; ?>

                        <button type='button' class="answer-buttons third hang-up-call-next" data-id='{{call_id}}'>Hang-up</button>
                        <button type='button' class="answer-buttons third add-to-do-not-call" data-id='{{call_list_phone_id}}' data-phone='{{phone}}' data-ctid='{{component_type_id}}' data-cid='{{component_id}}'>Do Not Call For:</button>
                        <select id="do-not-call-days">
                            <option value="">-- Select One --</option>
                            <option value="7">Do Not Call for 1 week</option>
                            <option value="14">Do Not Call for 2 week</option>
                            <option value="21">Do Not Call for 3 week</option>
                            <option value="28">Do Not Call for 4 week</option>
                            <option value="42">Do Not Call for 6 week</option>
                            <option value="56">Do Not Call for 8 week</option>
                            <option value="70">Do Not Call for 10 week</option>
                            <option value="90">Do Not Call for 3 months</option>
                            <option value="180">Do Not Call for 6 months</option>
                        </select><br>
                        <br>
                        <br>
                    </form>
                    <div class='activity-success-message' style="text-align: center; margin: 30px 0px; display: none;">
                        <div>- Activity Log Entry Saved! -</div>
                        <button type='button' class='activity-log-add-another'>Add Another</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type='button' class="answer-buttons close-notes-area" style="width: 90%; margin-left: 5%;">Close this Activity Log Box</button>
    <hr>
</script>