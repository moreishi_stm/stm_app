<?php $module = Yii::app()->controller->module->id;

// flag for showing answered call
$debugShowAnsweredBox = (YII_DEBUG)? 0 : 0;
$this->renderPartial('indexCss');

//@todo: 12/22/15 - CLee - checks for lead pool contact record exists in order to use the list
?>

<div class="yui-skin-sam yui3-skin-sam" id="yui-modal-container"></div>

<div id="listview-actions">
    <a href="/<?php echo $this->module->name;?>/dialer3/sessions" class="button gray icon i_stm_search">Call Sessions</a>
    <a href="/<?php echo $this->module->name;?>/dialer3/callFinder" class="button gray icon i_stm_search" target="_blank">Call Finder</a>
    <a href="/<?php echo $this->module->name;?>/dialer3/viewVoicemailGreetings" class="button gray icon i_stm_search">Voicemail Greetings</a>
    <?if(Yii::app()->user->checkAccess('dialerimport')):?>
    <a href="/<?php echo $this->module->name;?>/dialer3/recordings" class="button gray icon i_stm_search" target="_blank">Call Recordings</a>
    <a href="/<?php echo $this->module->name;?>/dialer3/callLists" class="button gray icon i_stm_edit">Call Lists</a>
    <a href="/<?php echo $this->module->name;?>/dialerimport" class="button gray icon i_stm_add">Dialer Import</a>
    <?endif;?>
    <button type="button" id="answer-check" style="display: inline;">Answer Check</button>
    <button class="button load-list" <?=(strpos($_SERVER['SERVER_NAME'], 'www.nothing.local') !== false) ? '' : 'style="display:none;"';?>>Refresh</button>
    <button class="button io-error" <?=(strpos($_SERVER['SERVER_NAME'], 'www.christineleeteam.local') !== false) ? '' : 'style="display:none;"';?>>IO Error Test</button>
</div>

<h1>Dialer</h1>
<br/>
<?php if($hasActiveSession): ?>
    <h3>Active Call Session already exists. Please hang up that session to continue and refresh this page.</h3>
<?php else: ?>
    <form style="display: table; width: 100%; margin-bottom: 20px;">
        <div style="text-align: center;">
            <span style="font-size: 14px;">Select a Super Easy Call List:</span>
            <span id="call-list-label" style="display: none; margin-left: 10px; font-size: 20px; font-weight: bold;"></span>
            <!-- Used to be #session-->
            <select id="preset" style="width: 300px; max-width: 350px; display: inline-block;">
                <option value=""></option>
                <?php if(strpos($_SERVER['SERVER_NAME'], 'www.seizethemarket.') !== false):?>

                    <optgroup label="Phone Tasks">
                        <option value="<?php echo CallLists::MY_CONTACTS_TASKS;?>" data-tableType="task">My Contact Phone Tasks</option>
                    </optgroup>
                    <optgroup label="Lead Pool Phone Tasks (Shared List)">
                        <option value="<?php echo CallLists::LEAD_POOL_CONTACTS_TASKS;?>" data-tableType="task">Lead Pool Contacts Phone Tasks</option>
                    </optgroup>

                    <?if(!$hideSharedLists && $customContactLists && in_array(CallLists::CUSTOM_CONTACTS, $listFilter)):?>
                        <optgroup label="Custom Contact Lists (Shared List)">
                            <? foreach($customContactLists as $customContactList) { ?>
                                <option value="<?php echo CallLists::CUSTOM_CONTACTS;?>" data-tableType="custom" data-clid="<?=$customContactList['call_list_id']?>"><?=$customContactList['name']?> (<?=$customContactList['count']?>)</option>
                            <? } ?>
                        </optgroup>
                    <? endif;?>

                <?php else: ?>

                    <?if(!empty(array_intersect(array(CallLists::MY_SELLERS_TASKS, CallLists::MY_BUYERS_TASKS, CallLists::MY_CONTACTS_TASKS, CallLists::MY_RECRUITS_TASKS), $listFilter))):?>
                        <optgroup label="Phone Tasks">
                            <?if(in_array(CallLists::MY_SELLERS_TASKS, $listFilter)):?>
                                <option value="<?php echo CallLists::MY_SELLERS_TASKS;?>" data-tableType="task">My Seller Phone Tasks</option>
                            <? endif;?>

                            <?if(in_array(CallLists::MY_BUYERS_TASKS, $listFilter)):?>
                                <option value="<?php echo CallLists::MY_BUYERS_TASKS;?>" data-tableType="task">My Buyer Phone Tasks</option>
                            <? endif;?>

                            <?if(in_array(CallLists::MY_CONTACTS_TASKS, $listFilter)):?>
                                <option value="<?php echo CallLists::MY_CONTACTS_TASKS;?>" data-tableType="task">My Contact Phone Tasks</option>
                            <? endif;?>

                            <?if(in_array(CallLists::MY_RECRUITS_TASKS, $listFilter)):?>
                                <option value="<?php echo CallLists::MY_RECRUITS_TASKS;?>" data-tableType="task">My Recruiting Phone Tasks</option>
                            <? endif;?>
                        </optgroup>
                    <?php endif; ?>

                    <?php if(Yii::app()->user->settings->lead_pool_contact_id && !$hideSharedLists && !empty(array_intersect(array(CallLists::LEAD_POOL_SELLERS_TASKS, CallLists::LEAD_POOL_BUYERS_TASKS), $listFilter))):?>
                        <optgroup label="Lead Pool Phone Tasks (Shared List)">
                            <?if(in_array(CallLists::LEAD_POOL_SELLERS_TASKS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_SELLERS_TASKS;?>" data-tableType="task">Lead Pool Seller Phone Tasks</option>
                            <? endif;?>

                            <?if(in_array(CallLists::LEAD_POOL_BUYERS_TASKS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_BUYERS_TASKS;?>" data-tableType="task">Lead Pool Buyer Phone Tasks</option>
                            <? endif;?>
                        </optgroup>
                    <?php endif; ?>

                    <?if(!$hideSharedLists && !empty(array_intersect( array(CallLists::ALL_NEW_SELLER_PROSPECTS, CallLists::ALL_NURTURING_SELLERS), $listFilter))):?>
                        <optgroup label="Seller Lead Pool (Shared List)">
                            <?if(in_array(CallLists::ALL_NEW_SELLER_PROSPECTS, $listFilter)):?>
                                <option value="<?php echo CallLists::ALL_NEW_SELLER_PROSPECTS;?>" data-tableType="time">New Seller Leads</option>
                            <? endif;?>

                            <?/*if(in_array(CallLists::LEAD_POOL_A_SELLERS, $listFilter)):?>
                            <option value="<?php echo CallLists::LEAD_POOL_A_SELLERS;?>" data-tableType="time">Hot "A" Seller Leads</option>
                        <? endif;?>

                        <?if(in_array(CallLists::LEAD_POOL_B_SELLERS, $listFilter)):?>
                            <option value="<?php echo CallLists::LEAD_POOL_B_SELLERS;?>" data-tableType="time">"B" Seller Leads</option>
                        <? endif;?>

                        <?if(in_array(CallLists::LEAD_POOL_C_SELLERS, $listFilter)):?>
                            <option value="<?php echo CallLists::LEAD_POOL_C_SELLERS;?>" data-tableType="time">"C" Seller Leads</option>
                        <? endif;*/?>

                            <?if(in_array(CallLists::ALL_D_SELLER_PROSPECTS, $listFilter)):?>
                                <option value="<?php echo CallLists::ALL_D_SELLER_PROSPECTS;?>" data-tableType="time">"D" Spoke to Seller Leads</option>
                            <? endif;?>

                            <?if(in_array(CallLists::ALL_NURTURING_SELLERS, $listFilter)):?>
                                <option value="<?php echo CallLists::ALL_NURTURING_SELLERS;?>" data-tableType="time">"E" Not Spoke to Seller Leads</option>
                            <? endif;?>
                        </optgroup>
                    <? endif;?>

                    <?if(!$hideSharedLists && !empty(array_intersect(array(CallLists::ALL_NEW_BUYER_PROSPECTS, CallLists::ALL_NURTURING_BUYERS), $listFilter))):?>
                        <optgroup label="Buyer Lead Pool (Shared List)">
                            <?if(in_array(CallLists::ALL_NEW_BUYER_PROSPECTS, $listFilter)):?>
                                <option value="<?php echo CallLists::ALL_NEW_BUYER_PROSPECTS;?>" data-tableType="time">New Buyer Leads</option>
                            <? endif;?>

                            <?/*if(in_array(CallLists::LEAD_POOL_A_BUYERS, $listFilter)):?>
                            <option value="<?php echo CallLists::LEAD_POOL_A_BUYERS;?>" data-tableType="time">Hot "A" Buyer Leads</option>
                        <? endif;?>

                        <?if(in_array(CallLists::LEAD_POOL_B_BUYERS, $listFilter)):?>
                            <option value="<?php echo CallLists::LEAD_POOL_B_BUYERS;?>" data-tableType="time">"B" Buyer Leads</option>
                        <? endif;?>

                        <?if(in_array(CallLists::LEAD_POOL_C_BUYERS, $listFilter)):?>
                            <option value="<?php echo CallLists::LEAD_POOL_C_BUYERS;?>" data-tableType="time">"C" Buyer Leads</option>
                        <? endif;*/?>

                            <?if(in_array(CallLists::ALL_D_BUYER_PROSPECTS, $listFilter)):?>
                                <option value="<?php echo CallLists::ALL_D_BUYER_PROSPECTS;?>" data-tableType="time">"D" Spoke to Buyer Leads</option>
                            <? endif;?>

                            <?if(in_array(CallLists::ALL_NURTURING_BUYERS, $listFilter)):?>
                                <option value="<?php echo CallLists::ALL_NURTURING_BUYERS;?>" data-tableType="time">"E" Not Spoke to Buyer Leads</option>
                            <? endif;?>
                        </optgroup>
                    <? endif;?>

                    <?if(!$hideSharedLists && $customSellerLists && in_array(CallLists::CUSTOM_SELLERS, $listFilter)):?>
                        <optgroup label="Custom Seller Lists (Shared List)">
                            <? foreach($customSellerLists as $customSellerList) { ?>
                                <option value="<?php echo CallLists::CUSTOM_SELLERS;?>" data-tableType="custom" data-clid="<?=$customSellerList['call_list_id']?>"><?=$customSellerList['name']?> (<?=$customSellerList['count']?>)</option>
                            <? } ?>
                        </optgroup>
                    <? endif;?>

                    <?if(!$hideSharedLists && $customBuyerLists && in_array(CallLists::CUSTOM_BUYERS, $listFilter)):?>
                        <optgroup label="Custom Buyer Lists (Shared List)">
                            <? foreach($customBuyerLists as $customBuyerList) { ?>
                                <option value="<?php echo CallLists::CUSTOM_BUYERS;?>" data-tableType="custom" data-clid="<?=$customBuyerList['call_list_id']?>"><?=$customBuyerList['name']?> (<?=$customBuyerList['count']?>)</option>
                            <? } ?>
                        </optgroup>
                    <? endif;?>

                    <?if(!$hideSharedLists && $customRecruitLists && in_array(CallLists::CUSTOM_RECRUITS, $listFilter)):?>
                        <optgroup label="Custom Recruit Lists (Shared List)">
                            <? foreach($customRecruitLists as $customRecruitList) { ?>
                                <option value="<?php echo CallLists::CUSTOM_RECRUITS;?>" data-tableType="custom" data-clid="<?=$customRecruitList['call_list_id']?>"><?=$customRecruitList['name']?> (<?=$customRecruitList['count']?>)</option>
                            <? } ?>
                        </optgroup>
                    <? endif;?>

                    <?if(!$hideSharedLists && $customContactLists && in_array(CallLists::CUSTOM_CONTACTS, $listFilter)):?>
                        <optgroup label="Custom Contact Lists (Shared List)">
                            <? foreach($customContactLists as $customContactList) { ?>
                                <option value="<?php echo CallLists::CUSTOM_CONTACTS;?>" data-tableType="custom" data-clid="<?=$customContactList['call_list_id']?>"><?=$customContactList['name']?> (<?=$customContactList['count']?>)</option>
                            <? } ?>
                        </optgroup>
                    <? endif;?>
                <?php endif; ?>



                <!--                --><?//foreach($callLists as $callList) : ?>
                <!--                <option value="--><?//=$callList->id?><!--">--><?//=$callList->name?><!-- | Total: --><?//=$callList->getDetails()->total?><!-- | Called: --><?//=$callList->getDetails()->num_called?><!-- | Created By: --><?//=$callList->contact->first_name?><!-- --><?//=$callList->contact->last_name?><!-- | Created On: --><?//=$callList->added?><!--</option>-->
                <!--                --><?//endforeach;?>
            </select>
            <button type="button" id="btn-load-list" style="display: inline;">Load Call List</button>
        </div>
        <div id="after-confirm" style="display: none;">Enter your phone # and click the "Start Calling" button. <br>The phone # entered will be called to start the session.</div>
        <div id="dialer-disconnect" style="display: none;">Dialer connection closed unexpectedly. <br>Refresh the page to reconnect. <br>If problem persists, please contact support.</div>

        <!--    <div style="display: table-row;">-->
        <!--        <div style="display: table-cell;"></div>-->
        <!--        <div style="display: table-cell;">&nbsp;</div>-->
        <!--        <div style="display: table-cell;"><label for="name" style="display: block;">List Name</label></div>-->
        <!--    </div>-->
        <!--    <div>-->
        <!--        <div style="display: table-cell; padding: 0px 10px;">-->
        <!--            - Or --->
        <!--        </div>-->
        <!--        <div style="display: table-cell;">-->
        <!--            <input type="text" id="name" style="width: 50%">-->
        <!--            <select id="component-type-id" style="max-width: 150px; width: 50%%;">-->
        <!--                <option value="">- Select a Type -</option>-->
        <!--                <option value="--><?//=ComponentTypes::RECRUITS?><!--">Recruits</option>-->
        <!--                <option value="--><?//=ComponentTypes::CONTACTS?><!--">Contacts</option>-->
        <!--                <option value="--><?//=ComponentTypes::BUYERS?><!--">Buyers</option>-->
        <!--                <option value="--><?//=ComponentTypes::SELLERS?><!--">Sellers</option>-->
        <!--                <option value="--><?//=ComponentTypes::CLOSINGS?><!--">Closings</option>-->
        <!--                <option value="--><?//=ComponentTypes::PROJECTS?><!--">Projects</option>-->
        <!--            </select>-->
        <!--            <button type="button" id="btn-new">Create New</button>-->
        <!--        </div>-->
        <!--    </div>-->
    </form>
<?php endif; ?>

<form method='POST' id='form-call-now' style="display: none;">
    <div id="call-filter-row" style="margin-bottom: 25px;">
        <label style="font-size: 14px; display: inline-block;">FILTERS:</label>
        <select id='maxDialerCallCount' name='maxDialerCallCount'>
            <option value=""></option>
            <option value="0">No dialer calls from this list</option>
            <option value="5">Less than 5 dialer calls from this list</option>
            <option value="10">Less than 10 dialer calls from this list</option>
            <option value="15">Less than 15 dialer calls from this list</option>
            <option value="20">Less than 20 dialer calls from this list</option>
            <option value="25">Less than 25 dialer calls from this list</option>
            <option value="30">Less than 30 dialer calls from this list</option>
        </select>
        <select id='maxDialerCallDays' name='maxDialerCallDays'>
            <option value=""></option>
            <option value="7">in the last 7 Days</option>
            <option value="14">in the last 14 Days</option>
            <option value="30">in the last 30 Days</option>
            <option value="45">in the last 45 Days</option>
            <option value="60">in the last 2 Months</option>
            <option value="90">in the last 3 Months</option>
            <option value="120">in the last 4 Months</option>
            <option value="150">in the last 5 Months</option>
            <option value="180">in the last 6 Months</option>
            <option value="210">in the last 7 Months</option>
            <option value="240">in the last 8 Months</option>
            <option value="270">in the last 9 Months</option>
            <option value="300">in the last 10 Months</option>
            <option value="330">in the last 11 Months</option>
            <option value="365">in the last 12 Months</option>
        </select>
        <label style="font-size: 14px; display: block;">(Automatically ordered by the most engaged leads first.)</label>
    </div>
    <div>
        <label for='from' style="font-size: 14px; display: inline-block;">Your Phone:</label>
        <?php $this->widget('StmMaskedTextField', array(
            'name' => 'from',
            'mask' => '(999) 999-9999',
            'id' => 'from',
            'value' => $_COOKIE['dialerNumber'],
        )); ?>
        <label for='num_to_call' style="font-size: 14px; display: inline-block;"># Simultaneous Lines to Dial:</label>
        <select id='num_to_call' name='num_to_call'>
            <option value="1">1 line</option>
            <option value="2">2 lines</option>
            <option value="3">3 lines</option>
            <option value="4">4 lines</option>
            <option value="5">5 lines</option>
            <option selected value="6">6 lines</option>
        </select>
        <button type='submit' id='btn-start'>Start Calling</button>
    </div>
    <div style="text-align: center;">
        <label style="font-size: 14px; display: inline-block;">Caller ID:</label>
        <?php
        echo CHtml::textField('callerIdName', $callerIdName, $htmlOptions=array('style'=>'width: 200px; display: inline-block;','placeholder'=>'Caller ID Name'));
        $this->widget('StmMaskedTextField', array(
            'name' => 'callerId',
            'mask' => '(999) 999-9999',
            'id' => 'callerIdNumber',
            'value' => $callerIdNumber,
            'htmlOptions' => $htmlOptions=array('style'=>'width: 200px; display: inline-block; margin-right: 10px;','placeholder'=>'(999) 999-9999')
        )); ?>
        <label for='default_voicemail' style="font-size: 14px; display: inline-block;">Default Voicemail:</label>
        <select id='default_voicemail' name='default_voicemail'>
            <option value="">- No Voicemail -</option>
            <? foreach($voicemailList as $voicemail) { ?>
                <option value="<?=$voicemail['id']?>"><?=$voicemail['title']?></option>
            <? } ?>
        </select>
    </div>
</form>

<a name="viewTop"></a>
<div id='container-details' style="margin: 30px 0px; font-size: 18px;"></div>
<div id="dt"></div>

<? Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
        'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
        'title' => 'Add New Activity Log',
        'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        'parentModel' => new Contacts
    ));
}

Yii::import('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget');
if (!class_exists('DialerListDialogWidget', false)) {

    Yii::app()->clientScript->registerScript('dialerAddToListJs', <<<JS
        function postAddToDialerList(data) {
            var callListTag = '<div id="call-list-tag-'+ data.added.call_list_id +'" class="call-list-tag"><span class="label">'+ data.added.name +'</span><span class="remove-from-call-list" data-id="'+ data.added.call_list_id +'" data-pid="' + data.added.phoneId + '" data-ctid="'+ data.added.componentTypeId +'" data-cid="'+ data.added.componentId +'">X</span></div>';
            $('#call-list-tag-container').append(callListTag);
        }
JS
);

    $this->widget('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget', array(
            'id' => DialerListDialogWidget::DIALOG_ID,
            'title' => 'Add to Dialer',
            'triggerElement' => '.add-to-dialer-list-button',
            'postAddFunction' => 'postAddToDialerList',
        )
    );
}

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
    'id' => TaskDialogWidget::TASK_DIALOG_ID,
    'title' => 'Create New Task',
    'triggerElement' => '.add-task-button, .edit-task-button, .delete-task-button',
    'parentModel' => new Contacts
)); ?>

<input type="hidden" id="nodeUrl" value="<?=$nodeUrl?>">
<? $this->renderPartial('indexHandlebars', array('module'=>$module, 'voicemailList' => $voicemailList)); ?>
<? $this->renderPartial('indexJs', array('module'=>$module, 'debugShowAnsweredBox' => $debugShowAnsweredBox)); ?>
