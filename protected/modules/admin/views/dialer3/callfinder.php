<?php
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('callSearch', <<<JS
    $('form#call-search').submit(function() {
        $.fn.yiiGridView.update('calls-grid', { data: $(this).serialize() });
        return false;
    });
JS
);

$this->breadcrumbs = array(
    'Call Finder' => '',
);
?>

<h1>Call Finder</h1>
<form id='call-search'>
    <div class="g3"></div>
    <span style="font-size: 14px; display: inline-block;" class="g2 p-tr">Phone to Search:</span>
    <?php $this->widget('StmMaskedTextField', array(
            'model' => $model,
            'attribute' => 'dial_b_leg_to',
            'mask' => '(999) 999-9999',
            'htmlOptions' => array('class'=>'g2'),
        )); ?>
    <input type='submit' value='Search' />
</form>

<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'calls-grid',
        'template'=>'{summary}{items}{summary}{pager}',
        'dataProvider' => $model->search(),
        'enableSorting'=>true,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Contact',
                'value' => '$data->skipSoftDeleteCheck()->callListPhone->phone->contact->fullName',
            ),
//            'first_name',
//            'last_name',
//            //			'primaryEmail',
            array(
                'type' => 'raw',
                'name' => 'List Name',
                'value' => '$data->callSession->callList->name',
            ),
            array(
                'type' => 'raw',
                'name' => 'Call Length',
                'value' => '(!empty($data->dial_b_leg_duration) ? ($data->dial_b_leg_duration < 60) ? $data->dial_b_leg_duration." seconds" :round($data->dial_b_leg_duration/60)." minutes" : "")',
            ),
            array(
                'type' => 'raw',
                'name' => 'Called by',
                'value' => '$data->callSession->contact->fullName',
            ),
            array(
                'type' => 'raw',
                'name' => 'Call Time',
                'value' => 'Yii::app()->format->formatDateTime($data->added)',
            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Source',
//                'value' => '$data->source->name."<br />Added: ".Yii::app()->format->formatDate($data->added)',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '
//				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
//			',
//                'htmlOptions' => array('style' => 'width:80px'),
//            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '
				"<div><a href=\"/".Yii::app()->controller->module->name."/contacts/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Contact</a></div>"',
                'htmlOptions' => array('style' => 'width:140px'),
            ),
        ),
    )
);
