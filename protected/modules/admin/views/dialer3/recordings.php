<?php
$domainSuffix = (YII_DEBUG) ? 'local': 'com';
$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";
Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);

$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('callRecordings', <<<JS

    $(document).on('click', '.listen-recording', function(e) {

        var recordingSource = $(this).data('source');
        $('#recording-controls').attr('data-src', recordingSource);
        $('#recording-source').attr('src', recordingSource);
        $('#recording-embed').attr('src', recordingSource);
        $("#recording-controls").trigger('load');

        $.fancybox({
            'type': 'inline',
            'content' : $('#recording-parent-container').html(),
            'width': 600,
            'height': 100,
            'autoSize':false,
            'scrolling':'no',
            'autoDimensions':false,
            'padding': 40
        });
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update("calls-grid", {
            data: $(this).serialize()
        });

        return false;
    });
JS
);
//$this->widget(
//    'admin_module.extensions.fancybox.EFancyBox', array(
//        'target' => 'a.listen-recording', 'config' => array(
//            'width' => '600', 'height' => '100', 'scrolling' => 'no', 'autoDimensions' => false, 'padding' => '40',
//        ),
//    )
//);

$this->breadcrumbs = array(
    'Call Recordings' => '',
);
?>

<h1>Call Recordings</h1>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'call-recordings-list-search',
        )); ?>
    <div class="g2">
        <label class="g3">Caller:</label>
        <span class="g9">
            <?php echo CHtml::dropDownList('contactId', null, CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions=array('empty'=>'Select One')); ?>
        </span>
    </div>
    <div class="g3">
        <label class="g6">List Name:</label>
        <span class="g5">
            <?php echo CHtml::dropDownList('callListId', null, CHtml::listData(CallLists::model()->findAll(), 'id', 'name'), $htmlOptions=array('empty'=>'Select One')); ?>
        </span>
    </div>
    <div class="g3">
        <label class="g6">Phone #:</label>
        <span class="g6">
            <?php $this->widget('StmMaskedTextField', array(
                    'name' => 'phone',
                    'mask' => '(999) 999-9999',
                    'htmlOptions' => array(
                        'placeholder' => '(999) 123-1234',
                    )
                )
            ); ?>
        </span>
    </div>
    <div class="g3">
        <label class="g4">Order by:</label>
        <span class="g8">
            <?php echo CHtml::dropDownList('order', null, array('recording_duration DESC'=>'Call Length (Longest first)','recording_duration ASC'=>'Call Length (Shortest first)','added DESC'=>'Call Time (Newest first)','added ASC'=>'Call Time (Oldest first)'), $htmlOptions=array('empty'=>'Select One')); ?>
        </span>
    </div>
    <div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div>

<!--<form id='call-search'>-->
<!--    <div class="g3"></div>-->
<!--    <span style="font-size: 14px; display: inline-block;" class="g2 p-tr">Phone to Search:</span>-->
<!--    --><?php //$this->widget('StmMaskedTextField', array(
//            'model' => $model,
//            'attribute' => 'dial_b_leg_to',
//            'mask' => '(999) 999-9999',
//            'htmlOptions' => array('class'=>'g2'),
//        )); ?>
<!--    <input type='submit' value='Search' />-->
<!--</form>-->

<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'calls-grid',
        'template'=>'{pager}{summary}{items}{summary}{pager}',
        'dataProvider' => $model->searchRecordings(),
        'enableSorting'=>true,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Contact',
                'value' => '$data->skipSoftDeleteCheck()->callSession->contact->fullName',
            ),
            array(
                'type' => 'raw',
                'name' => 'List Name',
                'value' => '$data->callSession->callList->name',
            ),
            array(
                'type' => 'raw',
                'name' => 'Call Length',
                'value' => '(!empty($data->recording_duration) ? $data->recording_duration." seconds" : "-") ', //(!empty($data->dial_b_leg_duration) ? ($data->dial_b_leg_duration < 60) ? $data->dial_b_leg_duration." seconds" :round($data->dial_b_leg_duration/60)." minutes" : "")
            ),
            array(
                'type' => 'raw',
                'name' => 'Call Time',
                'value' => 'Yii::app()->format->formatDateTime($data->added)',
            ),
            //            array(
            //                'type' => 'raw',
            //                'name' => 'Called Name',
            //                'value' => 'ComponentTypes::model()->getComponentModel($data->activityLog->component_type_id, $data->activityLog->component_id)->contact->fullName',
            //            ),
            array(
                'type' => 'raw',
                'name' => 'Phone #',
                'value' => 'Yii::app()->format->formatPhone(substr($data->dial_b_leg_to,1))',
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"#recording-container\" data-source=\"".$data->record_url."\" class=\"button gray icon i_stm_search grey-button listen-recording\">Listen to Recording</a></div>"',
                'htmlOptions' => array('style' => 'width:200px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '
				"<div><a href=\"/'.$module.'/dialer3/recordingDownload/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">Download</a></div>"',
                'htmlOptions' => array('style' => 'width:100px'),
            ),
        ),
    )
);
?>

<div id="recording-parent-container" style="display: none">
    <div id="recording-container" style="width: 600px;">
        <h2 style="margin-bottom: 20px;">Listen to Call Audio</h2>
        <audio controls title="Audio File" id="recording-controls" data-src="" style="width: 100%;">
            <source src="" type="audio/mpeg" id="recording-source">
            <embed height="50" width="600" src="" id="recording-embed">
        </audio>
    </div>
</div>
