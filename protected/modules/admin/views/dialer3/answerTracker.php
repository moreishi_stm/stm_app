<?php
$this->breadcrumbs=array(
'Answer Tracker'=>''
);

Yii::app()->clientScript->registerScript('search', <<<JS
    $('#listview-search form').submit(function() {
        if($('#datetime').val()=='') {
            alert('Please enter Date/time.');
            return false;
        }

        $.fn.yiiGridView.update('answered-call-grid', { data: $(this).serialize() });
        return false;
    });

JS
);
?>
    <div id="listview-actions">
        <a href="/admin/dialer3" class="button gray icon i_stm_search">Dialer</a>
    </div>

    <div id="content-header">
        <h1>Dialer Answered Call Tracker</h1>
        <!--<h1 id="header-date"><?=$fromDate.' - '.$toDate?></h1>-->
    </div>

    <div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
        <? $form = $this->beginWidget('CActiveForm', array(
                'action' => Yii::app()->createUrl($this->route),
            )
        ); ?>

        <div class="g4">
            <label class="g3">Caller:</label>
            <span class="g9"><?php echo CHtml::dropDownList('contactId', null, CHtml::listData(Contacts::model()->byActiveAdmins()->findAll(), 'id', 'fullName'), array(
                        'class' => 'chzn-select g12',
                        'data-placeholder' => 'Select Contact'
                    )
                ); ?>
            </span>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#contactId')); ?>
        </div>
        <div class="g3">
            <label class="g3">Date/time:</label>
            <span class="g9"><?php
            $this->widget(
                'admin_module.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                    'attribute' => 'datetime',
                    'name' => 'datetime',
                    'mode' => 'datetime',
                    'options'        => array(
                    ), 'htmlOptions' => array(
                        'class' => 'g12',
                    ),
                )
            );
            ?></span>
        </div>
        <div class="g3">
            <label class="g3">Timeframe:</label>
            <span class="g9"><?php echo CHtml::dropDownList('timeframe', null, array(0=>'None', 1=>'1 hour', 2=>'2 hours', 3=>'3 hours', 6=>'6 hours', 12=>'12 hours'), array(
                        'class' => 'chzn-select g12',
                        'data-placeholder' => 'Select Contact'
                    )
                ); ?>
            </span>
            <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#contactId')); ?>
        </div>

        <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('Search', array('class' => 'button')); ?></div>

        <?php $this->endWidget(); ?>
    </div>

<?php //$this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'session-grid','isForm'=>true, 'updateNonGridElement'=>'#header-date')); ?>
<? $this->widget('admin_module.components.StmGridView', array(
        'id' => 'answered-call-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'columns' => array(
//            array(
//                'type' => 'raw',
//                'name' => 'Name',
//                'value' => '$data[fullName]',
//                'footer' => '<span style="font-weight:bold;font-size:18px;float:right;">Total:</span>',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Time',
//                'value' => 'round(($data[total_time]/3600), 1)." hours"',
//                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.(($totalSum < 3600) ? ($totalSum/60)." mins" : (round(($totalSum/3060), 1)." hours")).'</span>',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Dials',
//                'value' => '$data[dials]',//." (".$data[dials].")"',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Spoke to',
//                'value' => '$data[spokeTo]',//." (".$data[spokeTo].")"',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Seller Appt Set',
//                'value' => '$data[sellerAppts]',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Buyer Appt Set',
//                'value' => '$data[buyerAppts]',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"/admin/dialer3/sessionDetails/".$data[contact_id]."\" class=\"button gray icon i_stm_search grey-button\">Details</a></div>"',
//                'htmlOptions' => array('style' => 'width:120px'),
//            ),
        ),
    )
);