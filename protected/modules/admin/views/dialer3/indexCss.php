<style type="text/css">
    h1 {
        margin: 50px 0px;
    }
    #form-call-now {
        margin-top: 30px;
        text-align: center;
    }
    #form-call-now label {
        display: block;
    }
    #from {
        max-width: 150px;
    }

    #after-confirm {
        max-width: 500px;
        font-size: 15px;
        font-weight: bold;
        background-color: #00a500;
        color: white;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-top: 30px;
    }

    #answered-call-directions {
        max-width: 100%;
        font-size: 15px;
        font-weight: bold;
        background-color: #00a500;
        color: white;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-bottom: 30px;
        text-align: center;
    }
    #answered-call-directions h2{
        color: white;
    }

    #call-list-label {
        font-size: 15px;
        font-weight: bold;
        background-color: #00a500;
        color: white;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }

    #dialer-disconnect {
        max-width: 500px;
        font-size: 15px;
        font-weight: bold;
        background-color: #0066ff;
        color: white;
        text-align: center;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-top: 30px;
    }

    /* Details container */
    #container-details h3 {
        text-align: left;
    }

    #container-details .live-activity-buttons {
        text-align: center;
    }

    #container-details .answer-buttons {
        font-size: 13px;
        width: 60%;
        color: #333;
    }
    #container-details .answer-buttons.third {
        width: 200px;
    }
    #container-details .answer-buttons.two-third {
        width: 408px;
    }
    #container-details .answer-buttons.tall {
        height: 60px;
    }
    #container-details .call-list-tag {
        background: #CCC;
        display: inline-block;
        font-size: 14px;
    }

    #container-details .call-list-tag .label {
        padding: 6px;
        color: #555;
        font-weight: normal;
    }

    #container-details .call-list-tag .remove-from-call-list{
        padding: 6px;
        font-weight: bold;
    }

    #container-details .call-list-tag .remove-from-call-list:hover {
        color: white;
        background: #c40000;
        cursor: pointer;
    }

    #dt thead tr th div{
        font-weight: bold;
        font-size: 14px;
    }

    /* Label styling for status badges and more */
    .label {
        display: inline-block;
        padding: 5px;
        font-weight: bold;
        color: white;
        border-radius: 3px;
    }
    .label.label-default {
        background-color: #777;
    }
    .label.label-primary {
        background-color: #428bca;
    }
    .label.label-success {
        background-color: #5cb85c;
    }
    .label.label-info {
        background-color: #5bc0de;
    }
    .label.label-warning {
        background-color: #f0ad4e;
    }
    .label.label-danger {
        background-color: #d9534f;
    }
    select optgroup{
        /*background:#000;*/
        color:black;
        /*font-style:normal;*/
        font-weight:bold;
    }
    select option{
        border: 0;
        padding-left: 20px;
    }

    .yui3-panel-content > .yui3-widget-hd {
        display: none !important;
    }
</style>