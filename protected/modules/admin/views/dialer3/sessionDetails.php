<?php
$this->breadcrumbs=array(
    'Session Details'=>''
);

Yii::app()->clientScript->registerScript('sessionDetails3Script', <<<JS
//    $('#dialer-session-detail').submit(function(){
//
//    });
JS
);
?>
    <div id="listview-actions">
        <a href="/admin/dialer3" class="button gray icon i_stm_search">Dialer</a>
    </div>

    <div id="content-header">
        <h1><?=$fullName?> </h1>
        <h2>Dialer Session Details</h2>
    </div>
<?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>'',
        'id'=>'dialer-session-detail',
    )); ?>
<?php $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'session-grid','returnSubmit'=>false, 'formSelector' => '#dialer-session-detail','updateNonGridElement'=>'#header-date','container'=> array('tag'=>'div','htmlOptions'=>array('class'=>'p-fr')))); ?>
    <div class="p-fr">
        <?=CHtml::dropDownList('componentTypeId', null, array(ComponentTypes::SELLERS => 'Sellers', ComponentTypes::BUYERS => 'Buyers', ComponentTypes::CONTACTS=>'Contacts', ComponentTypes::RECRUITS=>'Recruits'), $htmlOptions=array('empty' => 'Select Component Type', 'style'=>'font-size:13px;'))?>
    </div>
<?php $this->endWidget(); ?>
<? $this->widget('admin_module.components.StmGridView', array(
        'id' => 'session-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Call List',
                'value' => '$data[name]',
            ),
            array(
                'type' => 'raw',
                'name' => 'Number Lines',
                'value' => '$data[number_lines]',
            ),
            array(
                'type' => 'raw',
                'name' => 'Day of Week',
                'value' => '$data[day_name]',
            ),
            array(
                'type' => 'raw',
                'name' => 'Start',
                'value' => 'Yii::app()->format->formatDateTime($data[start])',
            ),
            array(
                'type' => 'raw',
                'name' => 'End',
                'value' => 'Yii::app()->format->formatDateTime($data[end])',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:right;">Total:</span>',
            ),
            //            array(
            //                'type' => 'raw',
            //                'name' => 'Duration (s)',
            //                'value' => '$data[total_time]',
            //                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.$totalSum.' (s)</span>',
            //            ),
            array(
                'type' => 'raw',
                'name' => 'Duration (min)',
                'value' => 'round($data[total_time]/60)',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.round($totalSum/60).' min</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'Duration (hours)',
                'value' => 'round($data[total_time]/3600,2)',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.round($totalSum/3600, 1).' hrs</span>',
            ),
            //            array(
            //                'type' => 'raw',
            //                'name' => 'Duration',
            //                'value' => 'StmFormatter::formatSeconds($data[total_time])',
            ////                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.(($totalSum < 360) ? ($totalSum/60)." mins" : (round(($totalSum/360), 1)." hours")).'</span>',
            //            ),
        ),
    )
);