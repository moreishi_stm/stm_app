<?php
$defaultHtmlOptions = array(
	'class' => 'phoneRow g12 p-p0',
);
if (isset($containerHtmlOptions)) {
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
	$containerHtmlOptions = $defaultHtmlOptions;
}

// Determine which model to use
$inputHtmlOptions = array('placeholder' => '(999) 123-1234', 'class' => 'phoneField');
if (isset($model)) { // for actual phone records
	$index = "[$i]";
	$PhoneModel = $model;
	$phoneFieldId = 'Phones_' . $i . '_phone';
} else { // for the phone template base
	$PhoneModel = new Phones;
	$index = '[0]';
	$inputHtmlOptions = CMap::mergeArray($inputHtmlOptions, array(
		'disabled' => 'disabled',
	));
	$phoneFieldId = 'Phones_0_phone';
}

echo CHtml::openTag('div', $containerHtmlOptions);
?>
<div class="g5">
	<?php $this->widget('StmMaskedTextField', array(
		'model' => $PhoneModel,
		'attribute' => $index . 'phone',
		'mask' => '(999) 999-9999',
		'id' => $phoneFieldId,
		'htmlOptions' => $inputHtmlOptions,
	)); ?>
	<?php echo $form->hiddenField($PhoneModel, $index . 'id', $inputHtmlOptions); ?>
	<?php echo $form->hiddenField($PhoneModel, $index . 'is_primary', $inputHtmlOptions); ?>
	<?php echo $form->hiddenField($PhoneModel, $index . 'remove', CMap::mergeArray(array('class' => 'remove'), $inputHtmlOptions)); ?>
	<?php echo $form->error($PhoneModel, $index . 'phone'); ?>
</div>
<div class="g6 p-pl8">
	<div class="g7" style="margin-top: -6px">
		<?php
		$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star_4', 'style' => 'margin-right: 3px;'), null) . 'Make Primary';
		$primaryButtonCssClass = 'make-primary-phone';

		if ($PhoneModel->is_primary) {
			$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star', 'style' => 'margin-right: 3px;'), null) . 'Remove Primary';
			$primaryButtonCssClass = 'remove-primary-phone';
		}
		echo CHtml::htmlButton($primaryButtonText, $htmlOptions = array(
			'class' => "text icon-only $primaryButtonCssClass",
		))
		?>
	</div>
	<div class="g5" style="margin-top: -6px">
		<button type="button" class="text remove-phone"><em class="icon i_stm_delete"></em>Remove</button>
	</div>
</div>
<?php echo CHtml::closetag('div'); ?>
