<?
		$js = <<<JS
		$(document).ready(function () {
			$(".remove-company-contact-button").click(function() {
			});
		});
JS;
		Yii::app()->clientScript->registerScript('company-contact-remove', $js);
?>

<div id="top-data" class="form g100 p-0">
	<div class="g100 p-0 p-fr">
		<?php
			$this->widget('admin_widgets.CompanyDetailsPortlet.CompanyDetailsPortlet', array(
				'model' => $model,
			));
		?>
	</div>
</div>

<div class="p-fr">
	<a href="javascript:void(0);" id="add-company-contact-button" class="button gray icon i_stm_add">Add Company Contact</a>
</div>

<div id="top-data" class="g100 p-0">
	<?php
	$this->beginStmPortletContent(array(
		'handleTitle'=>'Company Contacts',
		'handleIconCss'=>'i_user'
	));

	$this->widget('admin_module.components.StmGridView', array(
		'id' => 'company-contacts-grid',
		'dataProvider'  => new CArrayDataProvider($model->getAllContacts()),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			array(
				'name'  => 'First Name',
				'value' => '$data->first_name',
			),
			array(
				'name'  => 'Last Name',
				'value' => '$data->last_name',
			),
			array(
				'type'  => 'raw',
				'name'  => 'Phone',
				'value' => 'Yii::app()->format->formatPhone($data->primaryPhone)',
			),
			array(
				'type'  => 'raw',
				'name'  => 'Email',
				'value' => '$data->primaryEmail',
			),
			array(
				'type'=>'raw',
				'name'=>'',
				'value'=>'
					"<div><a href=\"/".Yii::app()->controller->module->id."/contacts/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit Contact</a></div>"
				',
				'htmlOptions'=>array('style'=>'width:125px'),
			),
			array(
				'class'       => 'CButtonColumn',
				'deleteButtonImageUrl'=>false,
				'deleteButtonUrl'=>'Yii::app()->createUrl("/".Yii::app()->controller->module->id."/companies/removeContact", array("CompanyContactLu[company_id]"=>'.$model->id.', "CompanyContactLu[contact_id]"=>$data->id))',
				'template'    => '{delete}',
				'htmlOptions' => array(
					'style' => 'width:90px'
				 ),
				'buttons' => array(
					'delete' => array(
						'label' => 'Remove',
						'raw'   => 'true',
						'options' => array(
							'class' => 'button gray icon i_stm_delete grey-button',
						),
					),
				),
			),
		)
	));

	$this->endStmPortletContent();
	?>
</div>
<?php
$this->widget('admin_widgets.ActivityDetailsPortlet.ActivityDetailsPortlet', array(
        'model'         => $model,
        'componentType' => ComponentTypes::model()->findByPk(ComponentTypes::COMPANIES),
    ));
?>
<div id="" class="g12 p-mh0 p-mv5">
    <?php
    $this->widget('admin_widgets.ActivityLogPortlet.ActivityLogPortlet', array(
            'parentModel' => $model,
        ));
    ?>
</div>
<?
$this->widget('admin_widgets.DialogWidget.CompanyContactDialogWidget.CompanyContactDialogWidget', array(
	'id' => 'company-contact-dialog',
	'title' => 'Add Company Contact',
	'triggerElement' => '#add-company-contact-button',
	'companyId' => $model->id,
));