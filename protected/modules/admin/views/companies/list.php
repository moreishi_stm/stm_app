<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->id.'/companies'),
);
$module=Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('search', <<<JS
    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('company-grid', {
            data: $(this).serialize()
        });
        return false;
    });

    $( '.delete-company-button' ).live( "click", function() {
        if(confirm('Are you sure you want to delete this Company?')) {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            var id = $(this).data('id');
            $.post('/$module/companies/delete/'+id, function(data) {
                $("div.loading-container.loading").remove();
                if(data=='') {
                    Message.create("success","Company deleted successfully.");
                    $.fn.yiiGridView.update("company-grid", { data: $(this).serialize() });
                } else
                    Message.create("error","Error: Action Plan did not delete.");
            },"json");
        }
        return false;
    });
JS
);
?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/companies/add" class="button gray icon i_stm_add">Add New Company</a>
</div>
<div id="content-header">
	<h1>Company List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $this->renderPartial('_listSearchBox',array('model'=>$model,)); ?>
</div>


<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'company-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Status',
            'value'=>'StmFormHelper::getStatusBooleanName($data->status_ma)',
        ),
        array(
            'type'=>'raw',
            'name'=>'Name',
            'value'=>'"<strong>".$data->name."</strong>".(($data->addresses)? "<br>".Yii::app()->format->formatPrintAddresses($data->addresses) : "")',
        ),
        array(
            'type'=>'raw',
            'name'=>'Phones',
            'value'=>'(($data->phones)? "<br>".Yii::app()->format->formatPrintPhones($data->phones) : "")',
        ),
		array(
			'type'=>'raw',
			'name'=>'Company Types',
			'value'=>'Yii::app()->format->formatCommaDelimited($data->types, "name")',
		),
        array(
            'type'=>'raw',
            'name'=>'Website',
            'value'=>'"<a href=\"http://".str_replace("http://","",$data->website)."\" target=\"_blank\">".$data->website."</a>"',
        ),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->id."/companies/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:80px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"javascript:void(0)\" data-id=\"".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_delete grey-button delete-company-button\">Delete</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:90px'),
        ),
	),
));