<?php
$defaultHtmlOptions = array(
	'class' => 'addressRow g12 p-p0',
);
if (isset($containerHtmlOptions)) {
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
} else {
	$containerHtmlOptions = $defaultHtmlOptions;
}

// Determine which model to use
if (isset($model)) { // for actual email records
	$AddressModel = $model;
	$index = "[$i]";
} else { // for the email template base
	$AddressModel = new Addresses;
	$index = '[0]';
}

echo CHtml::openTag('div', $containerHtmlOptions);
?>
<div class="g12">
	<?php echo $form->textField($AddressModel, $index . 'address', $htmlOptions = array(
		'placeholder' => 'Address',
		'style' => 'width: 93%'
	)); ?>
	<?php echo $form->error($AddressModel, $index . 'address'); ?>
</div>
<div class="g4">
	<?php echo $form->textField($AddressModel, $index . 'city', $htmlOptions = array(
		'placeholder' => 'City',
	)); ?>
	<?php echo $form->error($AddressModel, $index . 'city'); ?>
</div>
<div class="g4 p-ml10 p-tc">
	<?php echo $form->dropDownList($AddressModel, $index . 'state_id', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name asc')), 'id', 'short_name'), $htmlOptions = array(
		'empty' => 'State',
		'style' => 'max-width:99%',
	)); ?>
	<?php echo $form->error($AddressModel, $index . 'state_id'); ?>
</div>
<div class="g3 p-ml0">
	<?php echo $form->textField($AddressModel, $index . 'zip', $htmlOptions = array(
		'placeholder' => 'Zip',
	)); ?>
	<?php echo $form->error($AddressModel, $index . 'zip'); ?>
	<?php echo $form->hiddenField($AddressModel, $index . 'isPrimary'); ?>
</div>
<div class="g12 p-pl8">
	<div class="g4" style="margin-top: -6px">
		<?php
		$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star_4'), null) . 'Make Primary';
		$primaryButtonCssClass = 'make-primary-address';

		if ($AddressModel->isPrimary) {
			$primaryButtonText = CHtml::tag('em', $htmlOptions = array('class' => 'icon icon-only i_stm_star'), null) . 'Remove Primary';
			$primaryButtonCssClass = 'remove-primary-address';
		}
		echo CHtml::htmlButton($primaryButtonText, $htmlOptions = array(
			'class' => "text icon-only $primaryButtonCssClass",
		))
		?>
	</div>
	<div class="g4" style="margin-top: -6px">
		<button type="button" class="text remove-address"><em class="icon i_stm_delete"></em>Remove</button>
	</div>
</div>
<?php echo CHtml::closetag('div'); ?>
