<div id="company-header">
	<h1 class="name"><?php echo $this->pageTitle;?></h1>
	<?php
		if ($this->subTitle)
			echo CHtml::tag('h5', $htmlOptions=array(), $this->subTitle);
	?>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>