<?php
	$css = <<<CSS
	#company-details-right th {
		width: 20%;
	}
CSS;
	Yii::app()->clientScript->registerCss('companyAdd', $css);
?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'company-form',
			'enableAjaxValidation' => false,
		)
	); ?>
	<div id="top-data" class="row">
		<div class="g100 p-p0 p-m0">
			<? $this->beginStmPortletContent(array(
					'handleTitle' => 'Company Information',
					'handleIconCss' => 'i_user'
				)
			); ?>
			<div id="company-typtes-container">
				<div class="g12 p-mb5 rounded-text-box odd-static">
					<div id="company-details-left" class="g6">
						<table class="container">
							<tr>
								<th class="narrow">
									<?php echo $form->labelEx($model, 'status_ma') ?>
								</th>
								<td>
									<?php echo $form->dropDownList($model, 'status_ma', StmFormHelper::getStatusBooleanList(), $htmlOptions = array(
											'placeholder' => 'Status',
											'class' => 'g3'
										)
									); ?>
									<?php echo $form->error($model, 'name'); ?>
								</td>
							</tr>
                            <tr>
                                <th class="narrow">
                                    <?php echo $form->labelEx($model, 'name') ?>
                                </th>
                                <td>
                                    <?php echo $form->textField($model, 'name', $htmlOptions = array(
                                            'placeholder' => 'Company Name',
                                            'class' => 'g10'
                                        )
                                    ); ?>
                                    <?php echo $form->error($model, 'name'); ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="narrow">
                                    <?php echo $form->labelEx($model, 'website') ?>
                                </th>
                                <td>
                                    <?php echo $form->textField($model, 'website', $htmlOptions = array(
                                            'placeholder' => 'Website',
                                            'class' => 'g10'
                                        )
                                    ); ?>
                                    <?php echo $form->error($model, 'website'); ?>
                                </td>
                            </tr>
<!--							<tr>-->
<!--									<th class="narrow">--><?php //echo $form->labelEx($model, 'primary_contact_id');?><!--:</th>-->
<!--									<td>-->
<!--										--><?php //echo $form->dropDownList($model, 'primary_contact_id', CHtml::listData(Contacts::model()->byActiveAdmins(false, $model->primary_contact_id)->findAll(), 'id', 'fullName'), $htmlOptions = array(
//											'empty' => StmFormHelper::EMPTY_DROPDOWN_VALUE,
//										)); ?>
<!--										--><?php //echo $form->error($model, 'primary_contact_id'); ?>
<!--									</td>-->
<!--							</tr>-->
							<tr>
								<th class="top-align narrow">Address:</th>
								<td id="address-table-cell">
									<?php echo $this->renderPartial('../contacts/_addlAddressTemplate', array(
											'form' => $form,
											'containerHtmlOptions' => array(
												'style' => 'display: none',
												'id' => 'addlAddressTemplate'
											)
										)
									); ?>
									<div id="addlAddressRows" class="g12 p-p0" style="min-height: 0;">
										<?php
											if ($model->companyAddresses) {
												foreach ($model->companyAddresses as $i => $CompanyAddress) {
													echo $this->renderPartial('../contacts/_addlAddressTemplate', array(
															'form' => $form,
															'model' => $CompanyAddress,
															'i' => $i,
														)
													);
												}
											}
										?>
									</div>
									<button type="button" id="add-address" class="text"><em class="icon i_stm_add"></em>Add Address</button>
								</td>
							</tr>
							<tr>
								<th class="top-align narrow">Phone:</th>
								<td id="phone-table-cell">
									<?php echo $this->renderPartial('../contacts/_addlPhoneTemplate', array(
											'form' => $form,
											'containerHtmlOptions' => array(
												'style' => 'display: none',
												'id' => 'addlPhoneTemplate'
											)
										)
									); ?>
									<div id="addlPhoneRows" class="g12 p-p0" style="min-height: 0;">
										<?php
											if ($model->companyPhones) {
												foreach ($model->companyPhones as $i => $CompanyPhone) {
													echo $this->renderPartial('../contacts/_addlPhoneTemplate', array(
															'form' => $form,
															'model' => $CompanyPhone,
															'i' => $i,
														)
													);
												}
											}
										?>
									</div>
									<button type="button" id="add-phone" class="text"><em class="icon i_stm_add"></em>Add Phone Number</button>
								</td>
							</tr>
						</table>
					</div>
					<div id="company-details-right" class="g6">
						<table class="container">
							<tr>
								<th class="p-tl" colspan="2"><?php echo $form->labelEx($model, 'companyTypesToAdd', $htmlOptions = array('class' => 'p-fl')); ?> (select at least one company type)</th>
							</tr>
							<tr>
								<td colspan="2">
									<?php $this->widget('admin_widgets.StmMultiSelectWidget', array(
											'model' => $model,
											'attribute' => 'companyTypesToAdd',
											'listData' => CHtml::listData(CompanyTypes::model()->findAll(), 'id', 'name'),
										)
									); ?>
									<?php echo $form->error($model, 'companyTypesToAdd'); ?>
								</td>
							</tr>
						</table>
					</div>
                    <div class="g12">
                        <table class="container">
                            <tr>
                                <th class="narrow">Notes:</th>
                                <td>
                                    <?php echo $form->textArea($model, 'notes', $htmlOptions = array(
                                            'placeholder' => 'Notes',
                                            'class' => 'g10',
                                            'rows'=>6,
                                        )
                                    ); ?>
                                    <?php echo $form->error($model, 'notes'); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<!-- ====== END SECTIONS =============================================================================================== -->
	<div id="submit-button-wrapper">
		<button type="submit" class="submit wide"><?php echo ($model->isNewRecord) ? 'Submit' : 'Save'; ?> Company</button>
	</div>
	<?php $this->endWidget(); ?>
</div><!-- form-->

