<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'companies-list-search',
)); ?>
    <div class="g2">
        <label class="g6">Status:</label>
        <span class="g6"><?php echo $form->dropDownList($model,'status_ma', StmFormHelper::getStatusBooleanList(true));?></span>
    </div>
	<div class="g4">
		<label class="g2">Name:</label>
		<span class="g10"><?php echo $form->textField($model,'name');?></span>
	</div>
	<div class="g4">
		<label class="g2">Types:</label>
			<span class="g9"><?php echo $form->dropDownList($model->companyTypeLu,'idCollection',CHtml::listData(CompanyTypes::model()->findAll(array('order'=>'name ASC')), 'id', 'name'),array('class'=>'chzn-select','multiple'=>'multiple','style'=>'width:100%;','data-placeholder'=>'Company Types')); ?>
			</span>
		<?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#CompanyTypeLu_idCollection')); ?>
	</div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
