<?
$this->breadcrumbs=array(
    'List'=>''
);?>
    <div id="listview-actions">
        <a href="/admin/voicemail/addGreeting" class="button gray icon i_stm_add">Add New Greeting</a>
        <a href="/admin/phone/numbers" class="button gray icon i_stm_search">View Phone Numbers</a>
    </div>
    <div id="content-header">
        <h1>Voicemail Greetings</h1>
        <h3>List of Voicemail Greetings you have recorded.</h3>
    </div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'ivr-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Title',
            'value'=>'$data->title',
            'htmlOptions'=>array('style'=>'width:25%;'),
        ),
        'contact.fullName',
        array(
            'type'=>'raw',
            'name'=>'Length',
            'value'=>'(($data->recording_url) ? $data->recording_duration." seconds" : "- No Recording -")',
            'htmlOptions'=>array('style'=>'width:15%;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Added',
            'value'=>'Yii::app()->format->formatDateTime($data->added)',
            'htmlOptions'=>array('style'=>'width:15%;'),
        ),
//        array(
//            'type'=>'raw',
//            'name'=>'',
//            'value'=>'
//				"<a href=\"/admin/ivr/addExtension/".$data->id."\" class=\"button gray icon i_stm_add grey-button\">Add Extension</a>"
//			',
//            'htmlOptions'=>array('style'=>'width:110px;'),
//        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<a href=\"/admin/voicemail/editGreeting/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a>"
			',
            'htmlOptions'=>array('style'=>'width:80px;'),
        ),
    ),
));
?>