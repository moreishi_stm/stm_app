<?
require_once(Yii::getPathOfAlias('stm_app.modules.admin.components.StmAws.S3')."/FileManagement.php");
$fileExists = (file_exists(\StmAws\S3\FileManagement::getBaseUrlByType(\StmAws\S3\FileManagement::SITE_FILES_BUCKET_TYPE).'/'.$clientId.'/ivr/'.$model->id.'.mp3')) ? '1' : '0';
Yii::app()->clientScript->registerScript('ivrRecordScript', <<<JS
    $('#record-button').click(function(){
        if($fileExists) {
            if(!confirm('Are you sure you want to replace your current recording?')) {
                return false;
            }
        }
    });
JS
);

$this->breadcrumbs=array(
    $this->action->id == 'edit' ? 'Edit' : 'Add'=>''
);
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'greeting-form',
        'enableAjaxValidation' => false
    )
);
?>
    <h1><?=$this->action->id == 'editGreeting' ? 'Edit' : 'Add'?> Greeting</h1><br>
    <h3>INSTRUCTIONS: Enter the title of your greeting (Ex. John Smith Voicemail Greeting, Website Phone # Greeting, etc.) and click Save. <br>Then a Record button will appear and you can click that to record a new voicemail greeting. </h3>
<?
$this->beginStmPortletContent(array(
        'handleTitle' => 'Greeting Details',
        'handleIconCss' => 'i_wizard'
    )
);

?>
    <div id="action-plans-container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="">
                <tr>
                    <th class=""><?php echo $form->labelEx($model, 'title'); ?>:</th>
                    <td>
                        <?php echo $form->textField($model, 'title', $htmlOptions = array(
                                'placeholder' => 'Title',
                                'class' => 'g7',
                            )
                        ); ?>
                        <?php echo $form->error($model, 'title'); ?>
                    </td>
                </tr>
                <?if($this->action->id == 'editGreeting'):?>
                    <tr>
                        <th class="">
                            <label>Recording:</label>
                        </th>
                        <td>
                            <?if($model->recording_url):?>
                            <audio controls="controls" style="display: block;" class="g7">
                                <source src="<?=$model->recording_url?>">
<!--                                <source src="--><?//=\StmAws\S3\FileManagement::getBaseUrlByType(\StmAws\S3\FileManagement::SITE_FILES_BUCKET_TYPE)?><!--/--><?//=$clientId?><!--/greeting/--><?//=$model->id?><!--.mp3">-->
                                Error, your web browser does not support the required audio playback functionality. Please upgrade your browser to preview the recording.
                            </audio>
                            <?endif;?>
                            <a id="record-button" href="/admin/ivr/record/<?=$model->id?>?type=greeting" class="btn">Record New</a>
                        </td>
                    </tr>
                <?endif;?>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper">
        <button type="submit" class="submit wide">Save</button>
    </div>
<?php $this->endWidget(); ?>