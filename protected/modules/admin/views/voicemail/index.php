<?
$module = Yii::app()->controller->module->name;
$js = <<<JS
        $( '.delete-voicemail-button' ).live( "click", function() {
            if(confirm('Are you sure you want to delete this Voicemail?')) {
                $("body").prepend("<div class='loading-container loading'><em></em></div>");
                var id = $(this).data('id');
                $.post('/$module/voicemail/delete/'+id, function(data) {
                    $("div.loading-container.loading").remove();
                    if(data.status =='success') {
                        Message.create("success","Voicemail deleted successfully.");
                        $.fn.yiiGridView.update("ivr-grid", { data: $(this).serialize() });
                    } else {
                        Message.create("error","Error: Voicemail did not delete.");
                    }
                },"json");
            }
        });
JS;

Yii::app()->clientScript->registerScript('voicemailListScript', $js);
$this->breadcrumbs=array(
    'List'=>''
);?>
    <div id="listview-actions">
        <a href="/<?php echo Yii::app()->controller->module->name;?>/phone/callHistory/<?=$telephonyPhone->id?>" class="button gray icon i_stm_search">View Call History</a>
        <a href="/<?php echo Yii::app()->controller->module->name;?>/voicemail/greetings" class="button gray icon i_stm_search">Manage Voicemail Greetings</a>
    </div>
    <div id="content-header">
        <h1>Voicemails<?if($telephonyPhone) : ?> for <?=Yii::app()->format->formatPhone($telephonyPhone->phone)?><?endif;?></h1>
    </div>
<?php $this->widget('admin_module.components.StmGridView', array(
    'id'=>'ivr-grid',
    'dataProvider'=>$provider,
    'itemsCssClass'=>'datatables',
    'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Date',
            'value'=>'Yii::app()->format->formatDate($data->added,"m/d/Y")',
            'htmlOptions'=>array('style'=>'width:120px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Time',
            'value'=>'Yii::app()->format->formatDate($data->added,"g:i a")',
            'htmlOptions'=>array('style'=>'width:120px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'From Number',
            'value'=>'Yii::app()->format->formatPhone($data->callHuntGroupSession[0]->from_number)',
            'htmlOptions'=>array('style'=>'width:150px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Message Length (sec)',
            'value'=>'$data->recording_duration." seconds"',
            'htmlOptions'=>array('style'=>'width:150px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Audio File',
            'value'=>'"<audio controls=\"controls\" style=\"display: block; width: 100%;\" preload=\"none\"><source src=\"" . $data->record_url . "\">Error, your web browser does not support the required audio playback functionality. Please upgrade your browser to preview the recording.</audio>"',
            //'htmlOptions'=>array('style'=>'width:400px;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button delete-voicemail-button \" data-id=\"".$data->id."\">Delete</a></div>"',
            'htmlOptions'=>array('style'=>'width:90px'),
        ),
    ),
));
?>