<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 * @var CActiveDataProvider $contactSearchProvider
 */
$this->widget(
    'StmGridView', array(
        'id'             => 'contact-search-grid',
        'dataProvider'   => $contactSearchProvider,
        'selectableRows' => 2,
        'htmlOptions'    => array(
            'style' => 'clear: both',
        ),
        'columns'        => array(
            array(
                'class'               => 'CCheckBoxColumn',
                'id'                  => 'cid',
                'checkBoxHtmlOptions' => array(
                    'name' => 'contactIds[]',
                ),
                'value'               => '$data->id',
                'checked'             => 'false',
            ),
            'fullName:properCase',
            'primaryPhone:phone',
            'primaryEmail',
            'added:dateTime',
        ),
    )
);
