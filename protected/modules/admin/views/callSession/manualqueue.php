<?php
/**
 * @var CallSessionController $this
 * @var CActiveDataProvider   $contactSearchProvider
 */
$this->beginStmPortletContent(
    array(
        'handleTitle'          => $this->getPageTitle(),
        'handleIconCss'        => 'i_phone',
        'containerHtmlOptions' => array('id' => 'add-manual-queue'),
    )
);
$this->renderPartial(
    '_manualqueue_search', array(
        'model' => $contactSearchModel,
    )
);
?>
    <hr/>
<?php
$form = $this->beginWidget(
    'CActiveForm', array(
        'id'     => 'call-list-form',
        'action' => Yii::app()->createUrl($this->route),
    )
);
echo CHtml::linkButton(
    'Create Call List', $htmlOptions = array(
        'class' => 'btn',
        'style' => 'float: right; margin: 0 10px 10px 0;',

    )
);
$this->renderPartial(
    '_contactSearchGrid', array(
        'contactSearchProvider' => $contactSearchProvider,
    )
);
$this->endStmPortletContent();
$this->endWidget();
