<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
$this->widget('admin_module.components.widgets.PlivoWidget.PlivoWidget');
Yii::app()->getClientScript()->registerCss('sessionViewCss', <<<SESSION_VIEW_CSS
    #call-session-grid table.datatables tbody tr.selected {
        background: #deefff;
    }
    #call-session-grid table.datatables tbody tr.connected {
         background: #6bba70;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzZiYmE3MCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM2YmJhNzAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
        background: -moz-linear-gradient(top,  #6bba70 0%, #6bba70 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#6bba70), color-stop(100%,#6bba70));
        background: -webkit-linear-gradient(top,  #6bba70 0%,#6bba70 100%);
        background: -o-linear-gradient(top,  #6bba70 0%,#6bba70 100%);
        background: -ms-linear-gradient(top,  #6bba70 0%,#6bba70 100%);
        background: linear-gradient(to bottom,  #6bba70 0%,#6bba70 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6bba70', endColorstr='#6bba70',GradientType=0 );
    }
SESSION_VIEW_CSS
);

Yii::app()->getClientScript()->registerScript(
    'sessionViewJs', <<<SESSION_VIEW_JS
    $(function() {

        function formatPhoneNumberForDialer(phoneNumber) {
            return '1'+phoneNumber.replace(/[\(,\),\-,\s]/g, '');
        }

        $('a#start-dialing').click(function() {

            Message.create('success', 'Starting Dial Session');
            $('#call-session-grid table.datatables tbody tr:lt(3)').each(function() {
                $(this).addClass('selected');
                var phoneNumber = $(this).find('td:eq(1)').text();
                phoneNumber = formatPhoneNumberForDialer(phoneNumber);
                console.log('Adding Phone Number To Dial Session: '+phoneNumber);
                //Plivo.conn.call('19048619745');
            });

            return false;
        });

        $('a#stop-dialing').click(function() {

        });
    });

SESSION_VIEW_JS
);
$this->beginStmPortletContent(
    array(
        'handleTitle'          => $this->getPageTitle(),
        'handleIconCss'        => 'i_phone',
        'containerHtmlOptions' => array('id' => 'session-view'),
    )
);

echo CHtml::link(
    'Start Dialing', '#', $htmlOptions = array(
        'id'    => 'start-dialing',
        'class' => 'button',
        'style' => 'float: right; margin: 10px 10px 10px 0;',
    )
);

$this->widget(
    'StmGridView', array(
        'id'           => 'call-session-grid',
        'dataProvider' => $callSessionQueueProvider,
        'htmlOptions'  => array(
            'style' => 'clear: both;',
        ),
        'columns'      => array(
            'contact.fullName',
            'phone.phone:phone',
            'spoke_to:boolean',
//            array(
//                'class'    => 'CButtonColumn',
//                'template' => '{call}',
//                'buttons'  => array(
//                    'call' => array(
//                        'label'   => 'Call',
//                        'options' => array('class' => 'button'),
//                    )
//                ),
//            ),
        ),
    )
);

$this->endStmPortletContent();
