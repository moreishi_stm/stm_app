<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */
Yii::app()->getClientScript()->registerCss(
    'callSessionWidgetCss', <<<CSS
    #set-session-name {
        width: 50%;
        float: none !important;
        display: block !important;
        margin: 0 auto !important;
        text-align: center;
    }

    #set-session-name form div.row {
        clear: both;
        text-align: center;
        margin: 10px 0 10px 0;
    }

    #set-session-name form div.row input {
        width: 180px;
    }

CSS
);

$this->beginStmPortletContent(
    array(
        'handleTitle'          => $this->getPageTitle(),
        'handleIconCss'        => 'i_pencil',
        'containerHtmlOptions' => array('id' => 'set-session-name'),
    )
);

/** @var CActiveForm $form */
$form = $this->beginWidget(
    'CActiveForm', array(
        'id' => 'call-list-name-form',
    )
);
?>
    <div class="row g12">
        <?php echo $form->textField($callSession, 'name', $htmlOptions = array()); ?>
        <?php echo $form->error($callSession, 'name'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::linkButton('Save Session Name', $htmlOptions = array('class' => 'btn')); ?>
    </div>
<?php
$this->endWidget();
$this->endStmPortletContent();
