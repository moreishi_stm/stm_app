<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 * @var CallListController $this
 */
Yii::app()->getClientScript()->registerCss(
    'callSessionWidgetCss', <<<CSS
    #call-session-widget {
        width: 50%;
        float: none !important;
        display: block !important;
        margin: 0 auto !important;
    }

    #call-session-widget div#call-session-buttons {
        text-align: center;
    }

    #call-session-widget div#call-session-buttons a.call-session-button {
        width: 200px;
        float: none;
        margin-top: 10px;
        margin-bottom: 10px;
    }
CSS
);

$this->beginStmPortletContent(
    array(
        'handleTitle'          => $this->getPageTitle(),
        'handleIconCss'        => 'i_phone',
        'containerHtmlOptions' => array('id' => 'call-session-widget'),
    )
);
?>
    <div id="call-session-buttons" class="center">
        <?php
        $callSessionLinkHtmlOptions = array(
            'class' => 'btn call-session-button',
        );
        ?>
        <div class="row">
            <?php echo CHtml::link('Start from My Tasks', array('/admin/callSession/add/task'), $callSessionLinkHtmlOptions); ?>
        </div>
        <div class="row">
            <?php echo CHtml::link('Start from My Buckets', array('/admin/callSession/add/bucket'), $callSessionLinkHtmlOptions); ?>
        </div>
        <div class="row">
            <?php echo CHtml::link('Start from a Manual Queue', array('/admin/callSession/add/manualqueue'), $callSessionLinkHtmlOptions); ?>
        </div>
    </div>
<?php
$this->endStmPortletContent();
