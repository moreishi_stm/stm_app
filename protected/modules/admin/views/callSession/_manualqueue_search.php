<?php
/**
 * @author Chris Willard <chris@seizethemarket.com>
 * @since  1.0
 */

Yii::app()->clientScript->registerScript(
    'search', "
    $('form#contact-search-form').submit(function() {
        $.fn.yiiGridView.update('contact-search-grid', { data: $(this).serialize() });
        return false;
    });
  "
);
?>
<div class="g12">
    <?php
    $form = $this->beginWidget(
        'CActiveForm', array(
            'id' => 'contact-search-form',
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>
    <div class="g4">
        <label class="g3">Name:</label>
    <span class="g4"><?php echo $form->textField(
            $model->contact, 'first_name', $htmlOptions = array('placeholder' => 'First Name')
        ); ?></span>
    <span class="g4 p-ml10"><?php echo $form->textField(
            $model->contact, 'last_name', $htmlOptions = array('placeholder' => 'Last Name')
        ); ?></span>
    </div>
    <div class="g3">
        <label class="g3">Email:</label>
    <span class="g8"><?php echo $form->textField(
            $model->emails, 'email', $htmlOptions = array('placeholder' => 'Email')
        ); ?></span>
    </div>
    <div class="g3">
        <label class="g3">Phone:</label>
        <span class="g8">
          <?php $this->widget(
                'StmMaskedTextField', array(
                    'model'       => $model->phones,
                    'attribute'   => 'phone',
                    'mask'        => '(999) 999-9999',
                    //Makes the full 10 digit phone number optional (enables partial search)
                    'htmlOptions' => array(
                        'placeholder' => '(999) 123-1234',
                        'class'       => 'phoneField'
                    )
                )
            ); ?>
        </span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton(
            'SEARCH', array('class' => 'button')
        ); ?></div>
    <div class="g4">
        <label class="g3">Types:</label>
		<span class="g8"><?php echo $form->dropDownList(
                $model->contactTypeLu, 'idCollection', CHtml::listData(ContactTypes::model()->findAll(), 'id', 'name'),
                array(
                    'empty'            => 'All',
                    'class'            => 'chzn-select g100',
                    'multiple'         => 'multiple',
                    'data-placeholder' => 'Contact Types'
                )
            ); ?>
		</span>
        <?php $this->widget(
            'admin_module.extensions.EChosen.EChosen', array('target' => 'select#ContactTypeLu_idCollection')
        ); ?>
    </div>
    <div class="g3">
        <label class="g3">Sources:</label>
		<span class="g8"><?php echo $form->dropDownList(
                $model->source, 'idCollection', CHtml::listData(Sources::model()->findAll(), 'id', 'name'), array(
                    'empty'            => 'All',
                    'class'            => 'chzn-select g12',
                    'multiple'         => 'multiple',
                    'data-placeholder' => 'Sources'
                )
            ); ?>
		</span>
        <?php $this->widget(
            'admin_module.extensions.EChosen.EChosen', array('target' => 'select#Sources_idCollection')
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
