<?php
$action = ($this->action->id == 'add') ? 'Add' : 'Edit';
$this->breadcrumbs=array(
    $action=>'',
);
?>

<div id="header-links">
    <h1 class="name"><?php echo ($model->isNewRecord) ? 'Add New Audio' : $model->name;?></h1>
</div>
<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'audio-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
	$this->beginStmPortletContent(array(
            'handleTitle'=>'Audio Files',
            'handleIconCss'=>'i_strategy'
        ));
	?>
<div id="container">
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <table class="container">
            <tr>
                <th class="narrow"><?php echo $form->labelEx($model, 'name'); ?>:</th>
                <td colspan="3">
                    <?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'File Name', 'class'=>'g9', ));?>
                    <?php echo $form->error($model,'name'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $form->labelEx($model, 'tagCollection'); ?>:</th>
                <td>
                    <?php echo $form->dropDownList($model, 'tagCollection', CHtml::listData(OperationManualTags::model()->findAll(), 'id', 'name'), $htmlOptions=array('class'=>'g9','multiple'=>'multiple','data-placeholder'=>'Select Tags'));
                    $this->widget('admin_module.extensions.EChosen.EChosen', array(
                            'target' => 'select#AudioFiles_tagCollection',
                            'options'=>array(
                            ),
                        ));
                    ?>
                    <?php echo $form->error($model,'tagCollection'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $form->labelEx($model, 'description'); ?>:</th>
                <td colspan="3">
                    <?php echo $form->textArea($model,'description', $htmlOptions=array('placeholder'=>'Description / Keywords', 'class'=>'g9', 'rows'=>4));?>
                    <?php echo $form->error($model,'description'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow"><?php echo $form->labelEx($model, 'file_name'); ?>:</th>
                <td colspan="3">
                    <?php echo $form->fileField($model,'file_name', $htmlOptions=array('placeholder'=>'File', 'class'=>'g9'));?>
                    <?php echo $form->error($model,'file_name'); ?>
                    <span class="label g12">Allowed file types: mp3, wav</span>
                </td>
            </tr>
        </table>

    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Audio File</button></div>
<?php $this->endWidget(); ?>