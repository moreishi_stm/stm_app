<?
$this->breadcrumbs=array(
    'View'=>''
);
?>
    <h1>Listen to Audio</h1>
<?
$this->beginStmPortletContent(array(
        'handleTitle' => 'Audio',
        'handleIconCss' => 'i_wizard'
    )
);

?>
    <div id="container">
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th class="narrow">Name:</th>
                    <td>
                        <?=$model->name ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow">Description:</th>
                    <td>
                        <?=$model->description ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow">
                        <label>Audio:</label>
                    </th>
                    <td>
                        <audio controls="controls" style="display: block;" class="g7">
                            <source src="<?=$audioUrl?>">
                            Error, your web browser does not support the required audio playback functionality. Please upgrade your browser to preview the recording.
                        </audio>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>