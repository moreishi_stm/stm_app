<?php
//$domainSuffix = (YII_DEBUG) ? 'local': 'com';
//$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";
//Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
//Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);

$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('audioRecordings', <<<JS

//    $(document).on('click', '.listen-recording', function(e) {

        // ajax and get the temp url for this audio and then embed it
//        var recordingSource = $(this).data('source');
//        $('#recording-controls').attr('data-src', recordingSource);
//        $('#recording-source').attr('src', recordingSource);
//        $('#recording-embed').attr('src', recordingSource);
//        $("#recording-controls").trigger('load');
//
//        $.fancybox({
//            'type': 'inline',
//            'content' : $('#recording-parent-container').html(),
//            'width': 600,
//            'height': 100,
//            'autoSize':false,
//            'scrolling':'no',
//            'autoDimensions':false,
//            'padding': 40
//        });
//    });

//    $('#listview-search form').submit(function() {
//        $.fn.yiiGridView.update("calls-grid", {
//            data: $(this).serialize()
//        });
//
//        return false;
//    });
JS
);
$this->breadcrumbs = array(
    'List' => '',
);
$module = Yii::app()->controller->module->id;
Yii::app()->clientScript->registerScript('search', <<<JS

    $("#listview-search form").submit(function() {
        $.fn.yiiGridView.update("audio-grid", { data: $(this).serialize() });
        return false;
    });

//    $(".delete-operations-manual-button").live('click',function() {
//        if(confirm('Are you sure you want to delete this Operations Manual?')) {
//
//			$("body").prepend("<div class='loading-container loading'><em></em></div>");
//			var id = $(this).data('id');
//			$.post('/$module/operationManuals/delete/'+id, function(data) {
//
//				$("div.loading-container.loading").remove();
//
//				if(data.status=='success') {
//					Message.create("success","Operations Manual deleted successfully.");
//                    $.fn.yiiGridView.update("operation-manuals-grid", {
//                        data: $('#listview-search form').serialize()
//                    });
//                } else {
//					Message.create("error","Error: Operations Manual did not delete. " + data.errorMessage);
//				}
//			},"json");
//        }
//        return false;
//    });
JS
);
?>
<div id="listview-actions">
	<a href="/<?php echo $this->module->name;?>/audio/add" class="button gray icon i_stm_add">Add Audio File</a>
<!--	<a href="/--><?php //echo $this->module->name;?><!--/operationManuals/tags" class="button gray icon i_stm_document_add">Tags</a>-->
<!--	<a href="/--><?php //echo $this->module->name;?><!--/operationManuals/permissions" class="button gray icon i_stm_key">Permissions</a>-->
</div>
<div id="content-header">
	<h1>Audio Files List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
	<?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'audio-list-search',
        )); ?>
    <div class="g1">
<!--        <label class="g6">Status:</label>-->
<!--            <span class="g6">-->
<!--                --><?php //echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive'), $htmlOptions=array('empty'=>'Select One')); ?>
<!--            </span>-->
    </div>
    <div class="g5">
        <label class="g3">Name/Description:</label>
        <span class="g9"><?php echo $form->textField($model,'name');?></span>
    </div>
    <div class="g3">
        <label class="g2">Tags:</label>
		<span class="g10"><?php echo $form->dropDownList($model, 'tagCollection',
                CHtml::listData(OperationManualTags::model()->findAll(), 'id', 'name'), array(
//													'empty' => 'All',
                    'class' => 'chzn-select g12',
                    'multiple' => 'multiple',
                    'data-placeholder' => 'Tags'
                )); ?>
		</span>
        <?php $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => 'select#AudioFiles_tagCollection')); ?>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'audio-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			'name',
            'description',
            array(
                'type' => 'raw',
                'name' => 'Tags',
                'value' => 'Yii::app()->format->formatCommaDelimited($data->tags,"name")',
            ),
            array(
                'type'  => 'raw',
                'name'  => 'File Size',
                'value' => 'Yii::app()->format->formatFileSize($data->file_size)',
                'htmlOptions'=>array('style'=>'width:90px'),
            ),
			array(
				'type' => 'raw',
				'name' => '',
				'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/audio/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
				'htmlOptions' => array('style' => 'width:70px'),
			),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/audio/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">Listen to Audio</a></div>"',
                'htmlOptions' => array('style' => 'width:150px'),
            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"#recording-container\" data-id=\"".$data->id."\" class=\"button gray icon i_stm_search grey-button listen-recording\">Listen to Audio</a></div>"',
//                'htmlOptions' => array('style' => 'width:150px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'visible' => Yii::app()->user->checkAccess('owner'),
//                'value' => '
//                "<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button delete-operations-manual-button\" data-id=\"".$data->id."\">Delete</a></div>"
//            ',
//                'htmlOptions' => array('style' => 'width:70px'),
//            ),
		),
	)
);
?>
<div id="recording-parent-container" style="display: none">
    <div id="recording-container" style="width: 600px;">
        <h2 style="margin-bottom: 20px;">Listen to Audio</h2>
        <audio controls title="Audio File" id="recording-controls" data-src="" style="width: 100%;">
            <source src="" type="audio/mpeg" id="recording-source">
            <embed height="50" width="600" src="" id="recording-embed">
        </audio>
    </div>
</div>
