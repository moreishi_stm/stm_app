<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->name.'/contacts'),
);

?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name;?>/contactTypes/add" class="button gray icon i_stm_add">Add New Contact Types</a>
</div>
<div id="content-header">
	<h1>Contact Tags List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBox',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'contact-types-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'name',
        array(
            'type'=>'raw',
            'name'=>'Follow-up Days',
            'value'=>'$data->follow_up_days',
            'htmlOptions'=>array('style'=>'width:150px'),
        ),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/contactTypes/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:100px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/contactTypes/delete/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_delete grey-button\">Delete</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:100px'),
        ),
	),
));
