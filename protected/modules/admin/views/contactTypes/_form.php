<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-type-form',
	'enableAjaxValidation'=>false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'=>'Contact Tags',
		'handleIconCss'=>'i_strategy'
	));
	?>
	<div id="action-plans-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
	        <table class="container">
	        	<tr>
	        		<th class="narrow">Name:</th>
	        		<td colspan="3">
			    		<?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Contact Type', 'class'=>'g9', ));?>
        				<?php echo $form->error($model,'name'); ?>
	            	</td>
	        	</tr>
                <tr>
                    <th class="narrow">Subcategory of:</th>
                    <td colspan="3">
                        <?php echo $form->dropDownList($model,'parent_id', CHtml::listdata(ContactTypes::model()->exclude($model->id)->orderByName()->findAll(),'id','name'),$htmlOptions=array('empty'=>'', 'class'=>'g9', ));?>
                        <?php echo $form->error($model,'parent_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th class="narrow">Follow-up Days:</th>
                    <td colspan="3">
                        <?php echo $form->textField($model,'follow_up_days', $htmlOptions=array('placeholder'=>'Follow-up Days', 'class'=>'g2', ));?>
                        <?php echo $form->error($model,'follow_up_days'); ?>
                    </td>
                </tr>
	        </table>

		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Contact Tag</button></div>
<?php $this->endWidget(); ?>