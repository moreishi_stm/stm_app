<?php
$action = ($this->action->id == 'add') ? 'Add' : 'Edit';
$this->breadcrumbs=array(
    $action=>'',
);

?>
<div id="featured-areas-header">
	<h1 class="name">Add New Contact Tag</h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>