<?
$this->breadcrumbs=array(
    'Market Trends'
);
?>
<div id="listview-actions">
	<a href="/admin/marketTrends/add" class="button gray icon i_stm_add">Add Market Trend</a>
</div>
<div id="content-header">
	<h1>Market Trends</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php
	$this->renderPartial('_listSearchBox',array(
		'model'=>$model,
	));
?>
</div><!-- search-form -->
<?php
$this->widget('admin_module.components.StmGridView', array(
        'id'=>'contact-grid',
        'dataProvider'=>$model->search(),
        'itemsCssClass'=>'datatables',
        'columns'=>array(
            'name',
            'description',
            array(
                'class'=>'CButtonColumn',
                'template'=>'{view}',
                'buttons'=>array(
                    'view'=>array(
                        'imageUrl'=>$this->module->imageAssetsUrl.'/search.png',
                                'url' =>'Yii::app()->controller->createUrl("/admin/marketTrends/data/".$data->id)',
                    ),
                ),
            ),
        ),
));
?>