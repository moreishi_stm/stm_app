<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' =>' market-trends-form',
	'enableAjaxValidation' => false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'   => 'Market Trends',
		'handleIconCss' => 'i_graph'
	));
	?>
	<div id="action-plans-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
	        <table class="container">
	        	<tr>
	        		<th class="narrow">Name:</th>
	        		<td>
			    		<?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Market Trend Name', 'class'=>'g9', ));?>
        				<?php echo $form->error($model,'name'); ?>
	            	</td>
	        	</tr>
	        	<tr>
	        		<th class="narrow">Description:</th>
	        		<td>
			    		<?php echo $form->textField($model,'description', $htmlOptions=array('placeholder'=>'Description', 'class'=>'g9', ));?>
        				<?php echo $form->error($model,'description'); ?>
	            	</td>
	        	</tr>
	        </table>
			<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Market Trend</button></div>
		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
<?php $this->endWidget(); ?>
