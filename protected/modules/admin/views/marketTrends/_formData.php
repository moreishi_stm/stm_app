<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'market-data-form',
	'enableAjaxValidation' => false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'   => 'Market Trend Data',
		'handleIconCss' => 'i_graph'
	));
	?>
	<div>
		<div class="g12 p-mb5 rounded-text-box odd-static">
	        <table class="container">
	        	<tr>
	        		<th>Data:</th>
    				<td id="data-table-cell">
    					<?php echo $this->renderPartial('_addlMarketDataTemplate', array(
    						'form'=>$form,
    						'containerHtmlOptions'=>array(
    							'style'=>'display: none',
    							'id'=>'addlMarketDataTemplate'
    						)
    					)); ?>
        				<div id="addlMarketDataRows" class="g12 p-p0" style="min-height: 0;">
        				<?
        					if ($model) {
        						foreach( $model as $i => $Data )
        							echo $this->renderPartial('_addlMarketDataTemplate', array(
	            						'form'=>$form,
	            						'model'=>$Data,
	            						'i'=>$i,
	            					));
    						}
        				?>
        				</div>
						<button type="button" id="add-market-data" class="text"><em class="icon i_stm_add"></em>Add Market Data</button>
					</td>
	        	</tr>
	        </table>
		<div id="submit-button-wrapper"><button type="submit" class="submit wide">Update Market Data</button></div>
		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
<?php $this->endWidget(); ?>
