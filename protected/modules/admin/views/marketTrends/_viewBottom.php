<div class="p-clr p-p10" style="margin-bottom: -17px; position: relative; padding-top:30px;">
	<h2>Market Data</h2>
	<div id="listview-actions" class="inline">
		<a href="/admin/marketTrends/data/<?php echo $id?>" class="button gray icon i_stm_edit">Edit Market Data</a>
	</div>
</div>

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'action-plan-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		// array(
		// 	'type'=>'raw',
	 //        'name'=>'Task Type',
	 //        'value'=>'$data->taskType->name',
		// ),
		// 'description',
	 //    array(
		// 	'type'=>'raw',
	 //        'name'=>'Due',
	 //        'value'=>'$data->due_days." days "',
		// ),
	 //    array(
		// 	'type'=>'raw',
	 //        'name'=>'Timing',
	 //        'value'=>'$data->getModelAttribute("getDueDaysTypes",$data->due_days_type_ma)',
		// ),
	 //    array(
		// 	'type'=>'raw',
	 //        'name'=>'Reference Date',
	 //        'value'=>'$data->getModelAttribute("getDueReferences", $data->due_reference_ma)',
		// ),
	 //    array(
		// 	'type'=>'raw',
	 //        'name'=>'Assigned to',
	 //        'value'=>'$data->getModelAttribute("getAssignTos", $data->assign_to_type_ma)',
		// ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'buttons'=>array(
			    'view'=>array(
	            	'imageUrl'=>$this->module->imageAssetsUrl.'/search.png',
					'url' =>'Yii::app()->controller->createUrl("/admin/marketTrends/data/".$data->id)',
			    ),
            ),
		),
	),
)); ?>
