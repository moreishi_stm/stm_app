<?php
$defaultHtmlOptions = array(
	'class'=>'marketDataRow g12 p-p0',
);
if ( isset($containerHtmlOptions) )
	$containerHtmlOptions = CMap::mergeArray($defaultHtmlOptions, $containerHtmlOptions);
else
	$containerHtmlOptions = $defaultHtmlOptions;

// Determine which model to use
$inputHtmlOptions = array('placeholder'=>'Data');

if ( isset($model) ) {
    // for actual market data records
	$index = "[$i]";
	$MarketDataModel = $model;
} else {
    // for the email template base
	$MarketDataModel = new MarketTrendData;
	$index = '[0]';
	$inputHtmlOptions = CMap::mergeArray($inputHtmlOptions, array(
		'disabled'=>'disabled',
	));
}

echo CHtml::openTag('div', $containerHtmlOptions);
?>
<div class="g6">
	<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=>$MarketDataModel,
		    'attribute'=>$index.'date',
		    'options'=>array(
		        'showAnim'=>'fold',
		    ),
		    'htmlOptions'=>array(
		        'placeholder'=>'Date',
		        'class'=>'g5 p-mr10',
		    ),
		));
	?>
	<?//$form->textField($MarketDataModel,$index.'date', array('placeholder'=>'Date', 'class'=>'g5 p-mr10')); ?>
	<?php echo $form->textField($MarketDataModel,$index.'value', array('placeholder'=>'Market Data', 'class'=>'g6')); ?>
	<?php echo $form->error($MarketDataModel,$index.'date'); ?>
	<?php echo $form->error($MarketDataModel,$index.'value'); ?>
	<?php echo $form->hiddenField($MarketDataModel,$index.'id', $inputHtmlOptions); ?>
	<?php echo $form->hiddenField($MarketDataModel,$index.'remove', CMap::mergeArray(array('class'=>'remove'), $inputHtmlOptions)); ?>
</div>
<div class="g5 p-pl8">
	<div class="g5" style="margin-top: -6px">
		<button type="button" class="text remove-market-data"><em class="icon i_stm_delete"></em>Remove</button>
	</div>
</div>
<?php echo CHtml::closetag('div'); ?>
