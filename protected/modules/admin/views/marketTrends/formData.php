<?
$this->breadcrumbs = array(
    $marketTrendname=>'/admin/marketTrends/1',
	'Data' => '',
);
?>
	<div id="listview-actions">
		<a href="/admin/marketTrends/edit/<?php echo Yii::app()->request->getQuery('id')?>" class="button gray icon i_stm_edit">Edit Market</a>
	</div>
	<div>
		<h1><?php echo $marketTrendname?> Market</h1>
	</div>
	<br />

<?php echo $this->renderPartial('_formData', array('model'=>$model, 'marketTrendname'=>$marketTrendname)); ?>
