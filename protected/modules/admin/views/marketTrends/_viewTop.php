<?
$this->beginStmPortletContent(array(
	'handleTitle'=>'Market Trend',
	'handleIconCss'=>'i_wizard',
	'handleButtons'    => array(
		array(
			'label'        =>'Edit',
			'iconCssClass' =>'i_stm_edit',
			'htmlOptions'  =>array('id'=>'edit-market-trend-button'),
			'name'         =>'edit-market-trend',
			'type'         =>'link',
			'link'		   =>'/admin/marketTrends/edit/'.$model->id,
		)
	),
));
?>
	<div id="container">
		<div class="g12 p-mb5 rounded-text-box odd-static">
			<h2 class="p-pv10"><?php echo $model->name?></h2>

		</div>
	</div>
<?php $this->endStmPortletContent(); ?>
