<?
$this->breadcrumbs = array(
	ucfirst($this->action->id) => '',
);
?>
<div id="market-trends-header">
	<h1 class="name">Add Market Trend</h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
