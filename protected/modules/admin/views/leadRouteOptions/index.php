<style type="text/css">
    #options-table tr:hover {
        background-color: rgba(0, 0, 0, 0.05);
        cursor: pointer;
    }

    #option-values li {
        list-style-type: none;
    }
    #options-table .active {
        background-color: #add8e6;
    }
</style>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>

<h1>Lead Route Rules</h1>

<!-- Wrapper for Angular App -->
<div id="app-container" ng-app="leadApp">

    <div ng-controller="RulesController">

        <div style="float: left; width: 50%; padding-right: 10px; box-sizing: border-box;">
            <h2>Available Options</h2>
            <table id="options-table">
                <thead>
                <tr>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="leadRouteOption in leadRouteOptions">
                        <td ng-click="updateOptionsContainer(leadRouteOption, $event)">{{leadRouteOption.name}}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="float: left; width: 50%; padding-left: 10px; box-sizing: border-box;">
            <h2>Option Configuration</h2>
            <div id="template-content" ng-include src="currentTemplate"></div>
        </div>

        <input type="hidden" id="lead_route_id" value="<?=$lead_route_id?>">
    </div>

    <script type="text/ng-template" id="template-single-option">
        <div>
            <label for="inpt-single-option">Value</label>
            <input type="text" id="inpt-single-option" placeholder="Enter a Value" ng-repeat="optionValue in optionValues" value="{{optionValue.value}}" ng-model="optionValue.value" style="max-width: 450px">
            <button type="button" ng-click="deleteOne($index)">Delete</button>
        </div>
        <button type="button" ng-click="save()">Save</button>
    </script>

    <script type="text/ng-template" id="template-multi-option">
        <div>
            <label for="inpt-single-option">Values</label>
            <ul id="option-values">
                <li ng-repeat="optionValue in optionValues">
                    <input type="text" placeholder="Enter a Value" value="{{optionValue.value}}" ng-model="optionValue.value" style="max-width: 450px;">
                    <button type="button" ng-click="deleteOne($index)">Delete</button>
                    <!--
                    <button type="button" ng-click="deleteOne($index)" ng-if="$index != 0">Delete</button>
                    -->
                </li>
                <button type="button" ng-click="addAnother()">Add Another</button>
            </ul>
        </div>
        <button type="button" ng-click="save()">Save</button>
    </script>
</div>

<script type="text/javascript">
    (function() {

        // Initialize Angular JS
        var leadApp = angular.module('leadApp', []);

        // Rules controller, this is basically the only controller in this angular app
        leadApp.controller('RulesController', function ($scope, $http) {

            // Storage for things we want to delete
            var deletes = [];

            // The currently chosen lead route option
            var currentLeadRouteOption;

            // Load up options on controller load
            $http.get('/admin/leadRouteOptions/loadOptionValues/' + document.getElementById('lead_route_id').value).success(function(data) {

                // Set lead route options
                $scope.leadRouteOptions = data.leadRouteOptions;
            });

            // Function to handle opening options container
            $scope.updateOptionsContainer = function(model, event) {

                // Get all rows and currently selected rows
                var allRows = document.getElementById('options-table').getElementsByTagName('td');
                var currentRow = event.target;

                // Remove highlighted class from all rows
                for(var i = 0; i < allRows.length; i++) {
                    allRows[i].className = 'ng-binding';
                }

                // Add highlighted class to currently selected row
                currentRow.className = 'ng-binding active';

                // Set the current lead route option
                currentLeadRouteOption = model;

                // Clear out and declare deletes
                deletes = [];

                // Retrieve values for currently selected option
                $http.get('/admin/leadRouteOptions/loadValuesForOption?leadRouteId=' + document.getElementById('lead_route_id').value + '&optionId=' + model.id).success(function(data) {

                    // Set option values
                    $scope.optionValues = data.results;

                    // Add one if we don't already have one
                    if(!$scope.optionValues.length) {
                        $scope.addAnother();
                    }

                    // Render template
                    $scope.currentTemplate = model.multiple_values === "1" ?  "template-multi-option" : "template-single-option";
                });

            };

            // Adds another multi-option value
            $scope.addAnother = function() {
                $scope.optionValues.push({
                    lead_route_option_id: currentLeadRouteOption.lead_route_option_id,
                    lead_route_id: currentLeadRouteOption.lead_route_id,
                    value: ''
                });
            };

            // Removes a single option value from a multi option value option
            $scope.deleteOne = function(index) {

                // Pull the item out of the list and store the ID in the items to delete
                var item = $scope.optionValues.splice(index, 1)[0];
                deletes.push(item.id);
            };

            // Save functionality
            $scope.save = function() {

                // AJAX call to save
                $http.post('/admin/leadRouteOptions/save?leadRouteId=' + document.getElementById('lead_route_id').value + '&optionId=' + currentLeadRouteOption.id, {
                    option: currentLeadRouteOption,
                    optionValues: $scope.optionValues,
                    deletes: deletes
                }).success(function(data) {
                    alert('Save has been completed successfully!');
                });
            }
        });

    })();
</script>