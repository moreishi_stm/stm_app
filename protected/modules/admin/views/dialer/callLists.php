<?php
$this->breadcrumbs=array(
'Call Lists'=>''
);

$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/jquery.fileDownload.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScript('dialerCallListScript', <<<JS

        $(".calllist-delete-button").live('click', function(){
            if(confirm('Are you sure you want to delete this list?')) {
                $("body").prepend("<div class='loading-container loading'><em></em></div>");
                var id = $(this).data('id');
                $.post('/$module/dialer/deleteCallLists/'+id, function(data) {
                    $("div.loading-container.loading").remove();
                    if(data.status=='success') {
                        Message.create("success","Call List Deleted successfully.");
                        $.fn.yiiGridView.update("call-list-grid", { data: $(this).serialize() });
                    } else {
                        Message.create("error","Error: Call List did not Delete.");
                    }
                },"json");
            }
        });

        $(".calllist-export-button").click(function(e) {
            e.preventDefault();
            if($(this).attr("data-maxed") == "true") {
                alert('Please submit a request to support to export this list due to the size.');
                return false;
            }

            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            $.fileDownload($(this).prop('href'), {
                preparingMessageHtml: "",
                failMessageHtml: "",
                failCallback: function(url) {
                    $("div.loading-container.loading").remove();
                    Message.create("Error","Error trying to download file. Please try again later.");
                },
                successCallback: function(url) {
                    setTimeout(function() {
                        $("div.loading-container.loading").remove();
                        Message.create("success","File download completed.");
                    }, 500);
                }
            });
            return false;
        });
JS
);
?>
<?if(Yii::app()->user->checkAccess('dialerimport') || Yii::app()->user->checkAccess('dialerListRequeue') || Yii::app()->user->checkAccess('owner')):?>
    <div id="listview-actions">
        <a href="/<?php echo $this->module->name;?>/<?php echo $this->id;?>/addCallLists" class="button gray icon i_stm_add">Add Call List</a>
    </div>
<?endif;?>

    <div id="content-header">
        <h1>Dialer Custom Call Lists</h1>
    </div>
<? $this->widget('admin_module.components.StmGridView', array(
        'id' => 'call-list-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Name',
                'value' => '$data["name"]',
                'htmlOptions' => array('style' => 'min-width:200px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Type',
                'value' => 'CallLists::getComponentNameByPreset($data["preset_ma"], true)',
            ),
            array(
                'type' => 'raw',
                'name' => '# Queued',
                'value' => '$data["queuedCount"]',
                'htmlOptions' => array('style' => 'max-width:120px'),
                'visible' => (Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('dialerListRequeue') ) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => '# Total',
                'value' => '$data["totalCount"]',
                'htmlOptions' => array('style' => 'max-width:120px'),
                'visible' => (Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('dialerListRequeue') ) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => 'Re-Queue Timeframe',
                'value' => '$data["requeue_timeframe"]',
                'htmlOptions' => array('style' => 'max-width:100px'),
                'visible' => (Yii::app()->user->checkAccess('owner')) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => '# Max Calls for List Order',
                'value' => '($data["filter_max_call_count"]) ? $data["filter_max_call_count"]." calls" : "- Missing, please update. -"',
                'htmlOptions' => array('style' => 'width:200px'),
                'visible' => (Yii::app()->user->checkAccess('owner')) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => '# Days for List Order',
                'value' => '($data["filter_max_call_days"]) ? $data["filter_max_call_days"]." days" : "- Missing, please update. -"',
                'htmlOptions' => array('style' => 'max-width:200px'),
                'visible' => (Yii::app()->user->checkAccess('owner')) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => 'Last Requeue',
                'value' => '($log = CallListRequeueLog::getLastRequeueData($data["id"])) ? Yii::app()->format->formatDateTime($log->start_datetime)."<br>".$log->description.(($log->from_date) ? "<br>".Yii::app()->format->formatDate($log->from_date, "n/j/y") : "").(($log->to_date) ? " - ".Yii::app()->format->formatDate($log->to_date, "n/j/y") : "")."<br>By: ".$log->addedBy->fullName : ""',
                'htmlOptions' => array('style' => 'max-width:250px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->id."/dialer/editCallLists/".$data["id"]."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
                'visible' => (Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('dialerimport')) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->id."/dialer/callListDetails/".$data["id"]."\" class=\"button gray icon i_stm_search grey-button\">View</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
                'visible' => (Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('dialerimport')) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_search grey-button calllist-requeue-button\" data-id=\"".$data["id"]."\" data-name=\"".$data->name."\" data-ctid=\"".$data["component_type_id"]."\">Re-Queue</a></div>"',
                'htmlOptions' => array('style' => 'width:120px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button calllist-delete-button\" data-id=\"".$data["id"]."\" data-name=\"".$data->name."\">Delete</a></div>"',
                'htmlOptions' => array('style' => 'width:90px'),
                'visible' => (Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('dialerimport')) ? true : false,
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/admin/dialer/callListExport/$data[id]\" class=\"button gray icon i_stm_add grey-button calllist-export-button\" data-id=\"".$data["id"]."\" data-name=\"".$data->name."\" data-maxed=\"".(($data["totalCount"] > 5000)? "true" : "false")."\">Export</a></div>"',
                'htmlOptions' => array('style' => 'width:90px'),
                'visible' => (Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('dialerimport')) ? true : false,
            ),
        ),
    )
);

Yii::import('admin_widgets.DialogWidget.CallListRequeueDialogWidget.CallListRequeueDialogWidget');
if (!class_exists('CallListRequeueDialogWidget', false)) {
    $this->widget('admin_widgets.DialogWidget.CallListRequeueDialogWidget.CallListRequeueDialogWidget', array(
            'id' => CallListRequeueDialogWidget::DIALOG_ID,
            'title' => 'Call List Re-Queue',
            'triggerElement' => '.calllist-requeue-button',
        )
    );
}