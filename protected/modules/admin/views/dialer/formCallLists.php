<?php
$this->breadcrumbs=array(
'Call Lists'=>''
);
?>
    <div id="listview-actions">
<!--        <a href="/admin/dialer" class="button gray icon i_stm_search">Dialer</a>-->
    </div>

    <div id="content-header">
        <h1><?=$model->name?></h1>
        <h2>Dialer Custom Call Lists</h2>
    </div>

<?
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'lockboxes-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=> 'Dialer Call List',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-container" >
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <div class="g1"></div>
        <div class="g8">
            <table class="container">
                <? if($this->action->id == 'addCallLists'):?>
                <tr>
                    <th><?php echo $form->labelEx($model, 'preset_ma');?></th>
                    <td><?php echo $form->dropDownList($model, 'preset_ma', array(CallLists::CUSTOM_SELLERS => 'Seller', CallLists::CUSTOM_BUYERS =>'Buyer', CallLists::CUSTOM_CONTACTS => 'Contact', CallLists::CUSTOM_RECRUITS => 'Recruit'), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'preset_ma'); ?>
                    </td>
                </tr>
                <? endif; ?>
                <tr>
                    <th><?php echo $form->labelEx($model, 'name');?></th>
                    <td><?php echo $form->textField($model, 'name', $htmlOptions=array('placeholder'=>'Call List Name')); ?>
                        <?php echo $form->error($model,'name'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'requeue_timeframe');?></th>
                    <td><?php echo $form->dropDownList($model, 'requeue_timeframe', $model->getRequeueList(), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'requeue_timeframe'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Filter:</th>
                    <td><span class="label">Exclude people you have called more than <?php echo $form->dropDownList($model, 'filter_max_call_count', StmFormHelper::getDropDownList(array('start'=> 1,'end'=> 100)), $htmlOptions=array('empty'=>'Select One')); ?> times in the last <?php echo $form->dropDownList($model, 'filter_max_call_days', array(30 => '30 days', 60 => '60 days', 90 => '90 days', 120 => '120 days', 150 => '150 days', 180 => '180 days', 360 => '360 days'), $htmlOptions=array()); ?> days</span>
                        <?php echo $form->error($model,'filter_max_call_count'); ?> <?php echo $form->error($model,'filter_max_call_days'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="g3"></div>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Call List</button></div>
<?php $this->endWidget(); ?>
