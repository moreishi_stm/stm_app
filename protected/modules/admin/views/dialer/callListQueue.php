<?php
$this->breadcrumbs=array(
'Call Lists'=>''
);
?>
    <div id="listview-actions">
<!--        <a href="/admin/dialer" class="button gray icon i_stm_search">Dialer</a>-->
    </div>

    <div id="content-header">
        <h1><?=$model->name?> Call List Queue</h1>
    </div>
<? $this->widget('admin_module.components.StmGridView', array(
        'id' => 'session-grid',
        'dataProvider' => $dataProvider,
        'template'=>'{pager}{summary}{items}{summary}{pager}',
        'itemsCssClass' => 'datatables',
        'columns' => array(
            'id',
            'phone.contact.fullName',
            'phone.phone:phone',
            'status',
            'call_count',
            'last_called:dateTime',
            'added:dateTime',
            'updated:dateTime',
//            array(
//                'type' => 'raw',
//                'name' => 'Name',
//                'value' => '$data->name',
//            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Re-Queue Timeframe',
//                'value' => '$data->requeue_timeframe',
//                'htmlOptions' => array('style' => 'width:200px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"/".Yii::app()->controller->module->id."/dialer/editCallLists/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
//                'htmlOptions' => array('style' => 'width:120px'),
//            ),
        ),
    )
);