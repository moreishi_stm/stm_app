<?php
$module = Yii::app()->controller->module->id;
$controller = Yii::app()->controller->id;
Yii::app()->clientScript->registerScript('vm-greetings-script', <<<JS
	$('#btn-refresh').live('click',function() {
        $.fn.yiiGridView.update("vm-grid", {
        });

		return false;
	});

	$('.play-vm-button').live('click',function() {

        $(this).fancybox({'width':600,'height':100,'scrolling':'no','autoDimensions':false,'padding':40});
		var recordingSource = $(this).data('source');
        $('#recording-controls').attr('data-src', recordingSource);
        $('#recording-source').attr('src', recordingSource);
        $('#recording-embed').attr('src', recordingSource);
        $('#recording-controls').trigger('load');
		return false;
	});

	$('.delete-vm-button').live('click',function() {

	    if(confirm('Confirm delete of voicemail.')) {
			$("body").prepend("<div class='loading-container loading'><em></em></div>");
			var id = $(this).data('id');
			$.post('/$module/$controller/deleteVoicemailGreeting/'+id, function(data) {

				$("div.loading-container.loading").remove();

				if(data.status=='success') {
					Message.create("success","Dialer Voicemail deleted successfully.");
                    $.fn.yiiGridView.update("vm-grid", {
//                        data: $('#listview-search form').serialize()
                    });
                } else {
					Message.create("error","Error: Dialer Voicemail did not delete. " + data.errorMessage);
				}
			},"json");
	    }
		return false;
	});
JS
);

$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
        'target' => 'a.play-vm-button', 'config' => array(
            'width' => '600', 'height' => '100', 'scrolling' => 'no', 'autoDimensions' => false, 'padding' => '40',
        ),
    )
);

$this->breadcrumbs = array(
    'Voicemail Greetings' => '',
);
?>
<style>
	#form-call-now {
        margin-top: 30px;
        text-align: center;
    }
    #form-call-now label {
        display: block;
    }
    #from {
        max-width: 150px;
    }
	.label {
        display: inline-block;
        padding: 5px;
        font-weight: bold;
        color: white;
        border-radius: 3px;
    }
    .label.label-default {
        background-color: #777;
    }
    .label.label-primary {
        background-color: #428bca;
    }
    .label.label-success {
        background-color: #5cb85c;
    }
    .label.label-info {
        background-color: #5bc0de;
    }
    .label.label-warning {
        background-color: #f0ad4e;
    }
    .label.label-danger {
        background-color: #d9534f;
    }
	#number_to_call {
        max-width: 150px;
    }
</style>
<h1>Voicemail Greetings</h1>
<h3>Enter the voicemail title and your phone number to call to start the<br>recording. Click on the Start button. Refresh after recording is complete.</h3>
<form method='POST' id='form-call-now'>
    <div>
		<label for='title' style="font-size: 14px; display: inline-block;">Title:</label>
		<?php echo CHtml::textField('title', $callerIdName, $htmlOptions=array('style'=>'width: 200px; display: inline-block;','placeholder'=>'Title'));?>
        <label for='number_to_call' style="font-size: 14px; display: inline-block;">Your Phone:</label>
        <?php $this->widget('StmMaskedTextField', array(
                'name' => 'number_to_call',
                'mask' => '(999) 999-9999',
                'id' => 'number_to_call',
                'value' => $_COOKIE['number_to_call'],
            )); ?>
        <button type='submit' id='btn-start'>Start Calling</button>
        <button type="button" id='btn-refresh'>Refresh</button>
    </div>
</form>

<?php
$this->widget('admin_module.components.StmGridView', array(
	'id' => 'vm-grid',
	'template' => '{items}',
	'dataProvider' => $DataProvider,
	'itemsCssClass' => 'datatables',
	'columns' => array(
		array(
			'type' => 'raw',
			'name' => 'Title',
			'value' => '$data["title"]',
		),
        array(
            'type'=>'raw',
            'name'=>'Length',
            'value'=>'(($data->recording_url) ? $data->recording_duration." seconds" : "- No Recording -")',
            'htmlOptions'=>array('style'=>'width:15%;'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Added',
            'value'=>'Yii::app()->format->formatDateTime($data->added)',
            'htmlOptions'=>array('style'=>'width:15%;'),
        ),
        array(
			'type' => 'raw',
			'name' => 'Actions',
			'value' => '"<a href=\"#recording-container\" class=\"btn widget play-vm-button\" data-source=\"".$data->recording_url."\">Play</a> <a class=\"btn widget delete-vm-button\" data-id=\"".$data->id."\">Delete</a>"',
			'htmlOptions' => array('style' => 'width:140px;'),
		),
	),
));
?>
<div style="display: none">
    <div id="recording-container">
        <h2 style="margin-bottom: 20px;">Listen to Voicemail Greeting</h2>
        <audio controls title="Audio File" id="recording-controls" data-src="" style="width: 100%;">
            <source src="" type="audio/mpeg" id="recording-source">
            <embed height="50" width="650" src="" id="recording-embed">
        </audio>
    </div>
</div>
<script src="<?php echo 'http://yui.yahooapis.com/3.17.2/build/yui/yui-min.js'; ?>"></script>
<script>
	YUI().use('node', 'datatable', 'datasource-io', 'datasource-jsonschema', 'datatable-datasource', 'datatype-number', 'io', 'io-form', 'json', 'handlebars','node-event-simulate', function(Y) {

		Y.one('#form-call-now').on('submit', function(e) {
			e.preventDefault();

			$("body").prepend('<div class="loading-container loading"><em></em></div>');

			var num_to_call = Y.one('#number_to_call').get('value');
			if(num_to_call.length < 10) {
				$("div.loading-container.loading").remove();
				Message.messagePosition = 'top-center';
				Message.create('error','Please enter a 10 digit phone number.');
				return false;
			}

			var title = Y.one('#title').get('value');
			if(title.length < 3) {
				$("div.loading-container.loading").remove();
				Message.messagePosition = 'top-center';
				Message.create('error','Please enter a title.');
				return false;
			}

            Y.io('/<?=$module?>/<?=$controller?>/recordvoicemailgreeting', {
                method: 'POST',
                data: {
                    'number_to_call': num_to_call,
                    'title':  title
                },
                on: {
                    success: function(id, o) {

                        // Attempt to parse out JSON
                        try {
                            var response = Y.JSON.parse(o.responseText);
                        }
                        catch(e) {
                            alert('Error parsing response from server!');
                        }

                        // Handle different response types
                        switch(response.status) {
                            case 'success':

                                // Hide start button

                                Message.messagePosition = 'top-center';
                                Message.create('success','We are calling your phone now. Please answer to continue.');
                                setTimeout(function(){ $("div.loading-container.loading").remove(); }, 10000);

                                break;

                            default:
                                Message.messagePosition = 'top-center';
                                setTimeout(function(){ $("div.loading-container.loading").remove(); }, 0);
                                Message.create('error','An unknown error has occurred!');
                            break;
                        }
                    }
                }
            });
		});
	});
</script>