<?php
$this->breadcrumbs=array(
'Start: Step 1'=>''
);
$module = Yii::app()->controller->module->id;
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
$newSellerList = CallLists::ALL_NEW_SELLER_PROSPECTS;
$newBuyerList = CallLists::ALL_NEW_BUYER_PROSPECTS;
$myContactListId = CallLists::MY_CONTACTS_TASKS;
$leadPoolContactListId = CallLists::LEAD_POOL_CONTACTS_TASKS;
Yii::app()->clientScript->registerScript('dialerInitialJs', <<<JS

    $('.call-list-description-button').click(function(){
        $('.call-list-description-container').toggle('normal');
    });

    $('.close-call-list-description-container').click(function(){
        $('.call-list-description-container').hide('normal');
    });

    $('#preset').change(function(){

        var presetId = $("option:selected", this).attr('data-preset');
        var tableType = $("option:selected", this).attr('data-tabletype');
        var blankTemplate = Handlebars.compile($('#blank-filters').html());
        var template = Handlebars.compile($('#' + tableType + '-filters').html());
        var html = template();

        // hide all the call list settings
//        $('.call-session-options-row').addClass('hidden');

        // show list settings based on type
        if(tableType == 'task') {

            $('#num_to_call').val(1);
        } else if(tableType == 'time') {

            if(presetId == '$newSellerList' || presetId == '$newBuyerList') {
                $('#num_to_call').val(1);
            }
//            $('.call-session-options-row.time').removeClass('hidden');

        } else if(tableType == 'custom') {

//            $('.call-session-options-row.custom').removeClass('hidden');
        }

        // populate form and show the general call settings container
        $('.call-session-options-row-secondary').replaceWith('');
        $('#call-session-options-row').replaceWith(html);
        if(presetId == '$myContactListId' || presetId == '$leadPoolContactListId') {
            $('#filters_task_order option[value="status_asc"]').replaceWith('');
        }
        $('.call-session-settings').show('normal');
    });

    $('#filters_requeue_now').live('change',function(){
        if($(this).val() == '1') {
            $('.call-session-options-row-secondary').show('normal');
        }
        else {
            $('.call-session-options-row-secondary').hide('normal');
        }
    });

    // if call list type is task, ensure that it is only 1 line at a time
//    $('#num_to_call').change(function(){
//        if($("#preset option:selected").attr('data-tabletype') == 'task' && $(this).val() != '1') {
//            Message.create('notice', 'Task lists must be 1 line.');
//            $(this).val(1);
//        }
//    });

    $('form#call-session-form').submit(function(){

        $(this).attr('action', "/$module/$controller/indexSort/" + $('#preset').val());
    });

    $('#queue-list-button').click(function(){

        if($('#preset').val() == '' && $('#preset option:selected').attr('data-tabletype') != 'task' && $('#preset option:selected').attr('data-tabletype') != 'time') {
            alert('Please select a Call List.');
            return false;
        }

        // make sure Step 2 items are entered
        if($('#callerInitialNumber').val() == '') {
            alert('Please enter a number in Your Phone to call field.');
            return false;
        }

        if($('#callerInitialNumber').val().replace(/\D/g,'').length < 10) {
            alert('Please enter a valid number in Your Phone to call field.');
            return false;
        }

        if($('#callerIdName').val() == '') {
            alert('Please enter a Caller ID Name.');
            return false;
        }

        if($('#callerIdNumber').val() == '') {
            alert('Please enter a Caller ID Number.');
            return false;
        }

        $("body").prepend("<div class='loading-container loading'><em></em></div>");

        var filters; //$('form').serialize()
        var callListId;

        //if task list type and doesn't have callListId
        if(!$('#preset').val() && ($('#preset option:selected').attr('data-tabletype') == 'task' || $('#preset option:selected').attr('data-tabletype') == 'time')) {
            callListId = 'task';

            $.post('/$module/dialer/initializeCallList/'+ $('#preset option:selected').attr('data-preset'), { data : $('form').serialize() }, function(data) {

                if(data.status=='success') {
                    // do stuff here
                    $('#preset option:selected').attr('value', data.callListId);

                    initialQueueList(data.callListId);
                } else {
                    Message.create("error","Error: Call List did not get initialized. Try again or contact customer support.");
                    $("div.loading-container.loading").remove();
                    return false;
                }

            },"json");
        }
        else {
            initialQueueList($('#preset').val());
        }


        return false;
    });

    function initialQueueList(presetId)
    {
        // call the list-queue function to queue up the tasks
        $.post('/$module/dialer/initialQueueList/'+ presetId, { data : $('form').serialize() }, function(data) {

            if(data.status=='success') {
                Message.create("success","Call List queued successfully.");

                $('form#call-session-form').submit();
            } else {
                Message.create("error","Error: Call List did not queue. Try again or contact customer support.");
                $("div.loading-container.loading").remove();
            }
        },"json");
    }
JS
);

Yii::app()->clientScript->registerCss('dialerInitialCss', <<<CSS
    table.instructions {
        width: 50%;
        margin: 20px 25% 40px 25%;
    }
    .instructions th {
        width: 80px;
    }
    .instructions th, .instructions td {
        vertical-align: top;
        border-color: transparent;
        font-size: 18px;
    }
    h1 {
        margin-top: 10px;
    }
    select {
        font-size: 14px;
    }
    select optgroup {
        color: black;
    }
    select option {
        padding-left: 35px;
    }
    select#preset {
        max-width: 450px;
    }
    #call-list-container h1{
        max-width: 450px;
    }
    .call-list-description-container {
        font-size: 14px;
        background: #fcffc9;
        padding: 40px 60px;
        margin-top: 40px;
    }
    .call-list-description-container h1 {
        margin-top: 0;
    }
    .call-list-description-container hr {
        margin: 30px;
    }

    .call-session-settings table {
        width: 100%;
        margin: 16px 0 0 0;
    }
    .call-session-settings th {
        width: 250px;
        text-align: right;
        font-weight: normal;
    }
    .call-session-settings th, .call-session-settings td {
        vertical-align: top;
        border-color: transparent;
        font-size: 18px;
    }
    .call-session-settings input, .call-session-settings select {
        font-size: 17px;
        max-width: 400px;
        height: 35px;
        padding: 4px 2px;
    }
    #callerIdName, #callerIdNumber {
        height: 25px;
        max-width: 193px;
    }
    #callerInitialNumber {
        height: 25px;
    }
    .call-session-settings .step3 {
        text-align: left !important;
    }
    .call-session-settings .label {
        font-size: 12px;
    }
    #start-call-session-button, #queue-list-button {
        width: 99%;
        max-width: 358px;
        font-size: 15px !important;
    }
    .call-list-description-button {
        font-weight: bold;
        background: #fcffc9;
        padding: 10px;
        font-size: 15px;
    }
CSS
);
?>
    <div id="content-header p-clr">

        <?
        //@todo: remove after dialer update ignoring this column
        if(!$userContactStatus):?>
            <div style="min-height: 600px;">
                <h1>
                    Your Contact Status is "Trash". Please change it to "Active" and try again.<br>
                </h1>
                <h2>
                    <a href="/admin/contacts/edit/<?=Yii::app()->user->id?>" style="font-size: 20px;">Click here to go to your Contact.</a>
                </h2>
            </div>

        <? else:?>

        <h1>Welcome! You have been upgraded to our New Dialer!</h1>
        <div style="font-size: 14px; margin-bottom: 20px; width: 80%; margin-left: auto; margin-right: auto;">Follow the steps below. Your workflow is exactly the same once you start the call. We have made improvements behind the scenes for logic and performance. Please report any issues to support@seizethemarket.com and include screenshots and descriptions of your issues. We are on high alert to service any needs from this new roll-out. We appreciate your patience as we continue to improve your tools and systems. Have a great day!<br><br>
            <strong>New Feature:</strong> All Phone Task Lists can be ordered before you start your session so you can call your newest or oldest tasks first. We will have more options for the different lists rollingo it in the days/weeks to come. Enjoy!
        </div>
        <h1>Dialer Start-up Wizard</h1>
        <div class="p-tc" style="font-size: 15px; margin-top: 20px;">
            <a class="call-list-description-button" href="javascript:void(0);">[ Description of Call Lists - Click Here ]</a>
        </div>
        <div class="call-list-description-container" style="display: none; width: 80%; margin-left: auto; margin-right: auto;">
            <div class="p-tr">
                <a class="close-call-list-description-container">[ Close ]</a>
            </div>
            <h1>Description of Call Lists</h1>
            <hr/>
            <h2>Lead Pool Phone Task Lists</h2>
            <div>Description: These are phone tasks assigned to the lead pool contact. This is a shared list so multiple agents can call from this list simultaneously. They are separate by "types" (ex. sellers, buyers, recruits, contacts) so you can have similar conversations together in a session. Select one of these and the next screen will enable you to custom order them or select specific tasks to call first.
            </div>
            <hr/>

            <h2>Super Easy Automated Call Lists</h2>
            <div>Description: These are lists that are "Done for You" and automatically generated by lead STATUS and the length of TIME since the last conversation and call attempt. <br>When you update the lead status, it automatically adds/removes them from these lists. Make sure you update the status diligently to keep the integrity of these lists.
            </div>

            <hr/>
            <h2>New Leads</h2>
            <div>Description: Any new lead that comes in will get automatically populated into this list. Once you call the lead, it will get removed from this list and it will automatically <br>re-appear after the # hours in the settings (ex. 3 hours). As agents all you have to do is come back to this list periodically to see if there are any New Leads to call.
            </div>
            <hr/>
            <h2>D & E Status Leads</h2>
            <div>Description: D lead is a long term Spoke to and E is Not Spoke to lead. This list automatically pulls the long term leads and <br>queues up anyone you have not reach out to in more than 30 days. This ensures that no one is slipping through the cracks.
            </div>
            <hr/>
            <h2>Custom Call Lists</h2>
            <div>Description: These are lists that you created from the data you imported. These are not automatically re-queued. You must manually re-queue them when you are ready.<br>You can choose to dial completely through the list and start where you last stopped without requeueing or requeue the list in parts or in it's entirety depending on your needs.
            </div>
            <hr/>
            <div class="p-tc">
                <a class="close-call-list-description-container">[ Click to Close ]</a>
            </div>
        </div>
    </div>
<div id="dialer-wizard-containder" class="g12 p-clr" style="margin-top: 10px;">
    <div id="call-list-container" class="g5" style="margin-bottom: 40px;">
        <h1 class="g12 p-fr">Step 1: Select a Call List</h1>
        <select id="preset" class="g12 p-fr" style="height: 100%;" size=20>
            <?php if(strpos($_SERVER['SERVER_NAME'], 'www.seizethemarket.') !== false):?>

                <optgroup label="Phone Tasks">
                    <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::MY_CONTACTS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::MY_CONTACTS_TASKS?>">My Contact Phone Tasks</option>

                </optgroup>
                <optgroup label="Lead Pool Phone Tasks (Shared List)">
                    <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::LEAD_POOL_CONTACTS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::LEAD_POOL_CONTACTS_TASKS?>">Lead Pool Contacts Phone Tasks</option>
                </optgroup>

                <?if(!$hideSharedLists && $customContactLists && in_array(CallLists::CUSTOM_CONTACTS, $listFilter)):?>
                    <optgroup label="Custom Contact Lists (Shared List)">
                        <? foreach($customContactLists as $customContactList) { ?>
                            <option value="<?=$customContactList['call_list_id']?>" data-tableType="custom" data-preset="<?=CallLists::CUSTOM_CONTACTS?>"><?=$customContactList['name']?> (<?=$customContactList['count']?>)</option>
                        <? } ?>
                    </optgroup>
                <? endif;?>

            <?php else: ?>

                <?if(!empty(array_intersect(array(CallLists::MY_SELLERS_TASKS, CallLists::MY_BUYERS_TASKS, CallLists::MY_CONTACTS_TASKS, CallLists::MY_RECRUITS_TASKS), $listFilter))):?>
                    <optgroup label="Phone Tasks">
                        <?if(in_array(CallLists::MY_SELLERS_TASKS, $listFilter) && Yii::app()->user->checkAccess('transactions')):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::MY_SELLERS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::MY_SELLERS_TASKS?>">My Seller Phone Tasks</option>
                        <? endif;?>

                        <?if(in_array(CallLists::MY_BUYERS_TASKS, $listFilter) && Yii::app()->user->checkAccess('transactions')):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::MY_BUYERS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::MY_BUYERS_TASKS?>">My Buyer Phone Tasks</option>
                        <? endif;?>

                        <?if(in_array(CallLists::MY_CONTACTS_TASKS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::MY_CONTACTS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::MY_CONTACTS_TASKS?>">My Contact Phone Tasks</option>
                        <? endif;?>

                        <?if(in_array(CallLists::MY_RECRUITS_TASKS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::MY_RECRUITS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::MY_RECRUITS_TASKS?>">My Recruiting Phone Tasks</option>
                        <? endif;?>
                    </optgroup>
                <?php endif; ?>

                <?php if(Yii::app()->user->checkAccess('transactions') && Yii::app()->user->settings->lead_pool_contact_id && !$hideSharedLists && !empty(array_intersect(array(CallLists::LEAD_POOL_SELLERS_TASKS, CallLists::LEAD_POOL_BUYERS_TASKS), $listFilter))):?>
                    <optgroup label="Lead Pool Phone Tasks (Shared List)">
                        <?if(in_array(CallLists::LEAD_POOL_SELLERS_TASKS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::LEAD_POOL_SELLERS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::LEAD_POOL_SELLERS_TASKS?>">Lead Pool Seller Phone Tasks</option>
                        <? endif;?>

                        <?if(in_array(CallLists::LEAD_POOL_BUYERS_TASKS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::LEAD_POOL_BUYERS_TASKS, array())) ? $cl->id : '';?>" data-tableType="task" data-preset="<?=CallLists::LEAD_POOL_BUYERS_TASKS?>">Lead Pool Buyer Phone Tasks</option>
                        <? endif;?>
                    </optgroup>
                <?php endif; ?>

                <?if(Yii::app()->user->checkAccess('transactions') && !$hideSharedLists && !empty(array_intersect( array(CallLists::ALL_NEW_SELLER_PROSPECTS, CallLists::ALL_NURTURING_SELLERS), $listFilter))):?>
                    <optgroup label="Seller Lead Pool (Shared List)">
                        <?if(in_array(CallLists::ALL_NEW_SELLER_PROSPECTS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::ALL_NEW_SELLER_PROSPECTS, array())) ? $cl->id : '';?>" data-tableType="time" data-preset="<?=CallLists::ALL_NEW_SELLER_PROSPECTS?>">New Seller Leads</option>
                        <? endif;?>

                        <?/*if(in_array(CallLists::LEAD_POOL_A_SELLERS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_A_SELLERS;?>" data-tableType="time">Hot "A" Seller Leads</option>
                            <? endif;?>

                            <?if(in_array(CallLists::LEAD_POOL_B_SELLERS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_B_SELLERS;?>" data-tableType="time">"B" Seller Leads</option>
                            <? endif;?>

                            <?if(in_array(CallLists::LEAD_POOL_C_SELLERS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_C_SELLERS;?>" data-tableType="time">"C" Seller Leads</option>
                            <? endif;*/?>

                        <?if(in_array(CallLists::ALL_D_SELLER_PROSPECTS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::ALL_D_SELLER_PROSPECTS, array())) ? $cl->id : '';?>" data-tableType="time" data-preset="<?=CallLists::ALL_D_SELLER_PROSPECTS?>">"E" Not Yet Interested Seller Leads</option>
                        <? endif;?>

                        <?if(in_array(CallLists::ALL_NURTURING_SELLERS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::ALL_NURTURING_SELLERS, array())) ? $cl->id : '';?>" data-tableType="time" data-preset="<?=CallLists::ALL_NURTURING_SELLERS?>">"F" Not Spoke to Seller Leads</option>
                        <? endif;?>
                    </optgroup>
                <? endif;?>

                <?if(Yii::app()->user->checkAccess('transactions') && !$hideSharedLists && !empty(array_intersect(array(CallLists::ALL_NEW_BUYER_PROSPECTS, CallLists::ALL_NURTURING_BUYERS), $listFilter))):?>
                    <optgroup label="Buyer Lead Pool (Shared List)">
                        <?if(in_array(CallLists::ALL_NEW_BUYER_PROSPECTS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::ALL_NEW_BUYER_PROSPECTS, array())) ? $cl->id : '';?>" data-tableType="time" data-preset="<?=CallLists::ALL_NEW_BUYER_PROSPECTS?>">New Buyer Leads</option>
                        <? endif;?>

                        <?/*if(in_array(CallLists::LEAD_POOL_A_BUYERS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_A_BUYERS;?>" data-tableType="time">Hot "A" Buyer Leads</option>
                            <? endif;?>

                            <?if(in_array(CallLists::LEAD_POOL_B_BUYERS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_B_BUYERS;?>" data-tableType="time">"B" Buyer Leads</option>
                            <? endif;?>

                            <?if(in_array(CallLists::LEAD_POOL_C_BUYERS, $listFilter)):?>
                                <option value="<?php echo CallLists::LEAD_POOL_C_BUYERS;?>" data-tableType="time">"C" Buyer Leads</option>
                            <? endif;*/?>

                        <?if(in_array(CallLists::ALL_D_BUYER_PROSPECTS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::ALL_D_BUYER_PROSPECTS, array())) ? $cl->id : '';?>" data-tableType="time" data-preset="<?=CallLists::ALL_D_BUYER_PROSPECTS?>">"E" Not Yet Interested Buyer Leads</option>
                        <? endif;?>

                        <?if(in_array(CallLists::ALL_NURTURING_BUYERS, $listFilter)):?>
                            <option value="<?php echo ($cl = CallLists::getCallListByPresetId(CallLists::ALL_NURTURING_BUYERS, array())) ? $cl->id : '';?>" data-tableType="time" data-preset="<?=CallLists::ALL_NURTURING_BUYERS?>">"F" Not Spoke to Buyer Leads</option>
                        <? endif;?>
                    </optgroup>
                <? endif;?>

                <?if(!$hideSharedLists && $customSellerLists && in_array(CallLists::CUSTOM_SELLERS, $listFilter)):?>
                    <optgroup label="Custom Seller Lists (Shared List)">
                        <? foreach($customSellerLists as $customSellerList) { ?>
                            <option value="<?php echo $customSellerList['call_list_id'];?>" data-tableType="custom" data-preset="<?=CallLists::CUSTOM_SELLERS?>"><?=$customSellerList['name']?> (<?=$customSellerList['count']?>)</option>
                        <? } ?>
                    </optgroup>
                <? endif;?>

                <?if(!$hideSharedLists && $customBuyerLists && in_array(CallLists::CUSTOM_BUYERS, $listFilter)):?>
                    <optgroup label="Custom Buyer Lists (Shared List)">
                        <? foreach($customBuyerLists as $customBuyerList) { ?>
                            <option value="<?php echo $customBuyerList['call_list_id'];?>" data-tableType="custom" data-preset="<?=CallLists::CUSTOM_BUYERS?>"><?=$customBuyerList['name']?> (<?=$customBuyerList['count']?>)</option>
                        <? } ?>
                    </optgroup>
                <? endif;?>

                <?if(!$hideSharedLists && $customRecruitLists && in_array(CallLists::CUSTOM_RECRUITS, $listFilter)):?>
                    <optgroup label="Custom Recruit Lists (Shared List)">
                        <? foreach($customRecruitLists as $customRecruitList) { ?>
                            <option value="<?php echo $customRecruitList['call_list_id'];;?>" data-tableType="custom" data-preset="<?=CallLists::CUSTOM_RECRUITS?>"><?=$customRecruitList['name']?> (<?=$customRecruitList['count']?>)</option>
                        <? } ?>
                    </optgroup>
                <? endif;?>

                <?if(!$hideSharedLists && $customContactLists && in_array(CallLists::CUSTOM_CONTACTS, $listFilter)):?>
                    <optgroup label="Custom Contact Lists (Shared List)">
                        <? foreach($customContactLists as $customContactList) { ?>
                            <option value="<?php echo $customContactList['call_list_id'];?>" data-tableType="custom" data-preset="<?=CallLists::CUSTOM_CONTACTS?>"><?=$customContactList['name']?> (<?=$customContactList['count']?>)</option>
                        <? } ?>
                    </optgroup>
                <? endif;?>
            <?php endif; ?>
        </select>
    </div>
    <div class="g7 call-session-settings" style="display: none;">
        <form id="call-session-form" method="post">
            <table class="instructions">
                <tr><th></th>
                    <td style="padding-top: 0;"><h1 style="margin-top: 0; text-align: left;">Step 2: Enter your settings below.</h1>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Your Phone to Call:</th>
                    <td>
                        <?php $this->widget('StmMaskedTextField', array(
                                'name' => 'callerInitialNumber',
                                'mask' => '(999) 999-9999',
                                'id' => 'callerInitialNumber',
                                'value' => $_COOKIE['callerInitialNumber'],
                            )); ?><div class="label">This number will be called to start the session.</div>
                    </td>
                </tr>
                <tr>
                    <th>Caller ID:</th>
                    <td>
                        <? echo CHtml::textField('callerIdName', $_COOKIE['callerIdName'], $htmlOptions=array('class'=>'g6','style'=> 'display: inline-block;','placeholder'=>'Caller ID Name'));
                        $this->widget('StmMaskedTextField', array(
                                'name' => 'callerIdNumber',
                                'mask' => '(999) 999-9999',
                                'id' => 'callerIdNumber',
                                'value' => $_COOKIE['callerIdNumber'],
                                'htmlOptions' => $htmlOptions=array('class'=>'g6','style'=>' display: inline-block;','placeholder'=>'(999) 999-9999')
                            )); ?>
                    </td>
                </tr>
                <tr>
                    <th># Lines to Dial:</th>
                    <td>
                        <select id='num_to_call' name='num_to_call' class='g12'>
                            <option value="1">1 line</option>
                            <option value="2">2 lines</option>
                            <option value="3">3 lines</option>
                            <option value="4">4 lines</option>
                            <option value="5">5 lines</option>
                            <option selected value="6">6 lines</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>Voicemail:</th>
                    <td>
                        <select id='default_voicemail' name='default_voicemail' class='g12'>
                            <option value="">- No Voicemail -</option>
                            <? foreach($voicemailList as $voicemail) { ?>
                                <option value="<?=$voicemail['id']?>"><?=$voicemail['title']?></option>
                            <? } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <hr/>
                    </th>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <h1 class="step3">Step 3: Queue Your Call List</h1>
                        <div class="p-tl label">Below are the options to arrange your list in a specific way.</div>
                        <div class="p-tl label">For Shared Lists, this is affect anyone else using this list.</div>
                    </td>
                </tr>
                <tr id="call-session-options-row"></tr>
                <tr id="queue-list-row">
                    <th></th>
                    <td><a href="javascript:void(0)" id="queue-list-button" class="button gray icon i_stm_play_mini grey-button">NEXT</a></td>
                </tr>
                <tr id="start-session-row" class="hidden">
                    <th></th>
                    <td><a href="javascript:void(0)" id="start-call-session-button" class="button gray icon i_stm_play_mini grey-button">Start Call Session</a></td>
                </tr>
            </table>
        </form>
    </div>
</div>

<!-- Handlebars templates -->
<script id="blank-filters" type="text/x-handlebars-template">
    <tr id="call-session-options-row"></tr>
</script>

<script id="task-filters" type="text/x-handlebars-template">
    <tr id="call-session-options-row">
        <th>
            Re-queue Now:
        </th>
        <td>
            <select id="filters_requeue_now" name="filters[requeue_now]" class="g12">
                <option value="0">No, start where I left off</option>
                <option value="1">Yes, re-queue the list</option>
            </select>
            <div class="stm-tool-tip question">
                <span>When you requeue the list it will pull down any new phone tasks automatically.</span>
            </div>
        </td>
    </tr>
    <tr class="call-session-options-row-secondary" style="display: none;">
        <th>
            Order by:
        </th>
        <td>
            <select id="filters_task_order" name="filters[task_order]" class="g12">
                <option value="due_date_desc">Newest First after Priority Tasks</option>
                <option value="due_date_asc">Oldest First after Priority Tasks</option>
                <option value="status_asc">Hottest Status First after Priority Tasks</option>
            </select>

            <div class="g12 label">NOTE: Calls already made today will not re-queue until tomorrow.</div>
        </td>
    </tr>
</script>

<script id="time-filters" type="text/x-handlebars-template">
    <tr id="call-session-options-row">
        <th>
            Re-queue Now:
        </th>
        <td>
            <select id="filters_requeue_now" name="filters[requeue_now]" class="g12">
                <option value="0">No, start where I left off</option>
                <option value="1">Yes, re-queue the list</option>
            </select>
        </td>
    </tr>
</script>

<script id="custom-filters" type="text/x-handlebars-template">
    <tr id="call-session-options-row">
        <th>
            Custom List Filters:
        </th>
        <td>
            The new Re-queue feature is on it's way!<br>Meanwhile, Please use the Spot Requeue.<br>
            <a href="/<?=$module?>/<?=$controller?>/callLists">[ Click Here ]</a>
        </td>
    </tr>
</script>
<? endif;?>