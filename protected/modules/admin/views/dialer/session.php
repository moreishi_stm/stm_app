<?php
$this->breadcrumbs=array(
'Session'=>''
);
    Yii::app()->clientScript->registerCss('dialerSessionCss', <<<CSS
        #active-sessions-grid table {
            float: none;
        }
CSS
);
//Yii::app()->clientScript->registerScript('search', <<<JS
//JS
//);
$module = Yii::app()->controller->module->id;
$assignmentTypes = array('buyer_agent','listing_agent','listing_manager','isa','isa_buyer','isa_seller','closing_manager','showing_partner');
?>
    <div id="listview-actions">
        <a href="/admin/dialer" class="button gray icon i_stm_search">Dialer</a>
    </div>
    <div id="content-header" style="margin-bottom: 30px;">
        <h1>Active Dialer Sessions</h1>
        <? $this->widget('admin_module.components.StmGridView', array(
                'id' => 'active-sessions-grid',
                'dataProvider' => $activeSessionsDataProvider,
                'itemsCssClass' => 'datatables',
                'columns' => array(
                    array(
                        'type' => 'raw',
                        'name' => 'Name',
                        'value' => '$data->contact->fullName',
                    ),
                    array(
                        'type' => 'raw',
                        'name' => 'Call List',
                        'value' => '$data->callList->name',
                    ),
                    array(
                        'type' => 'raw',
                        'name' => 'Start Time',
                        'value' => 'Yii::app()->format->formatDateTime($data->start)',
                    ),
                    array(
                        'type' => 'raw',
                        'name' => '# Lines',
                        'value' => '$data->number_lines',
                    ),
                    array(
                        'type' => 'raw',
                        'name' => '',
                        'value' => '"<div><a href=\"/'.$module.'/dialer/sessionDetails/".$data->contact_id."\" class=\"button gray icon i_stm_search grey-button\">Sessions</a></div>"',
                        'htmlOptions' => array('style' => 'width:100px'),
                    ),
                ),
            )
        );?>
    </div>

    <div id="content-header">
        <h1>Dialer Session Totals</h1>
        <h1 id="header-date"><?=$fromDate.' - '.$toDate?></h1>
    </div>
    <?php

    $date_preset = array(
        'today',
        'up_to_today',
        'last_1_week',
        'last_2_weeks',
        'month_to_date',
        'this_month',
        'last_month',
        'last_30_days',
        'last_12_months',
        'this_year',
    );

    $this->widget('admin_module.components.widgets.DateRanger.DateRanger', array('gridName'=>'session-grid','isForm'=>true, 'updateNonGridElement'=>'#header-date', 'container'=> array('tag'=>'div','htmlOptions'=>array('class'=>'p-fr')))); ?>
    <div class="p-fr">
        <?=CHtml::dropDownList('componentTypeId', null, array(ComponentTypes::SELLERS => 'Sellers', ComponentTypes::BUYERS => 'Buyers', ComponentTypes::CONTACTS=>'Contacts', ComponentTypes::RECRUITS=>'Recruits'), $htmlOptions=array('empty' => 'Select Component Type', 'style'=>'font-size:13px;'))?>
    </div>
    <div class="p-fr">
        <?=CHtml::dropDownList('assignmentTypes', null, CHtml::listData(AssignmentTypes::model()->byNames($assignmentTypes)->findAll(), 'id', 'display_name'), $htmlOptions=array('empty' => 'Select Role Type', 'style'=>'font-size:13px;'))?>
    </div>
    <div class="clear"></div>
<? $this->widget('admin_module.components.StmGridView', array(
        'id' => 'session-grid',
        'dataProvider' => $dataProvider,
        'enableSorting'=>true,
        'filterSelector' => '#assignmentTypes, #componentTypeId',
        //        'filterPosition'=> 'header',
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'header'=> 'Name',
                'name' => 'fullName',
                'value' => '$data[fullName]',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:right;">Total:</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'hours',
                'value' => 'round(($data[total_time]/3600), 1)." hours"',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.(($totalSumHours < 3600) ? ($totalSumHours/60)." mins" : (round(($totalSumHours/3600), 1)." hours")).'</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'Minutes',
                'value' => 'round($data[total_time]/60)',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.($totalSumHours/60).'</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'dials',
                'value' => '$data[dials]',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.$totalSumDials.'</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'spoke_to',
                'value' => '$data[spokeTo]',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.$totalSumSpokeTo.'</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'Spoke to per Hour',
                'value' => '(($data[total_time] > 0 && round(($data[total_time]/3600), 5) > 0)? round($data[spokeTo] / round(($data[total_time]/3600), 5)) : "")',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.(($totalSumHours > 0 && round(($totalSumHours/3600), 1) > 0)? round($totalSumSpokeTo / round(($totalSumHours/3600), 1)) : "").'</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'sellerAppts',
                'value' => '$data[sellerAppts]',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.$totalSellerAppts.'</span>',
            ),
            array(
                'type' => 'raw',
                'name' => 'buyerAppts',
                'value' => '$data[buyerAppts]',
                'footer' => '<span style="font-weight:bold;font-size:18px;float:left;">'.$totalBuyerAppts.'</span>',
            ),
            array(
                'name' => 'Role',
                'value' => '$data[assignmentType]',
                //'filter' => '',//CHtml::listData(AssignmentTypes::model()->byNames($assignmentTypes)->findAll(), 'id', 'display_name') //
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/'.$module.'/dialer/sessionDetails/".$data[contact_id]."\" class=\"button gray icon i_stm_search grey-button\">Sessions</a></div>"',
                'htmlOptions' => array('style' => 'width:100px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/'.$module.'/tracking/activities/".$data[contact_id]."\" class=\"button gray icon i_stm_search grey-button\">Activity Logs</a></div>"',
                'htmlOptions' => array('style' => 'width:180px'),
            ),
        ),
    )
);