<style type="text/css">
    h1 {
        margin: 50px 0px;
    }
    hr {
        margin: 20px 0;
    }
    #form-call-now {
        text-align: center;
    }
    #form-call-now label {
        display: block;
    }
    #from {
        max-width: 150px;
    }

    #after-confirm {
        max-width: 500px;
        font-size: 15px;
        font-weight: bold;
        background-color: #00a500;
        color: white;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-top: 30px;
    }

    #answered-call-directions {
        max-width: 100%;
        font-size: 15px;
        font-weight: bold;
        background-color: #00a500;
        color: white;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-bottom: 30px;
        text-align: center;
    }
    #answered-call-directions h2{
        color: white;
    }

    /*#call-list-label {*/
        /*font-size: 15px;*/
        /*font-weight: bold;*/
        /*background-color: #00a500;*/
        /*color: white;*/
        /*display: inline-block;*/
        /*margin-left: auto;*/
        /*margin-right: auto;*/
        /*padding: 10px;*/
    /*}*/

    #dialer-disconnect {
        max-width: 500px;
        font-size: 15px;
        font-weight: bold;
        background-color: #0066ff;
        color: white;
        text-align: center;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-top: 30px;
    }
    #container-details {
        max-width: 1200px;
        margin-left: auto;
        margin-right: auto;
        position: relative;
    }
    #container-details .name{
        font-size: 20px;
        font-weight: bold;
        text-decoration: underline;
    }
    /* Details container */
    #container-details h3 {
        text-align: left;
    }
    #container-details #dialer-left-container {
        width: 45%;
        float: left;
        margin-left: 1.5%;
        margin-right: 1%;
    }
    #container-details #dialer-right-container {
        width: 50%;
        float: left;
        margin-left: 1%;
        margin-right: 1.5%;
    }
    #container-details #lead-gen-spoke-to-container {
        font-size: 14px;
    }
    #container-details #lead-gen-spoke-to-container label {
        display: inline-block;
        font-size: 14px;
        text-align: right;
        margin-right: 10px;
        width: 45%;
    }
    #container-details #lead-gen-spoke-to-container span {
        font-size: 14px;
    }
    #container-details #lead-gen-spoke-to-container .row {
        margin: 10px 0;
    }
    #container-details #do-not-call-days-container {
        position: absolute;
        bottom: -39px;
        width: 400px;
    }
    #container-details #do-not-call-days-container button {
        font-size: 12px;
    }
    #container-details .live-activity-buttons {
        text-align: center;
    }
    #container-details .answer-buttons {
        font-size: 13px;
        width: 60%;
        color: #333;
    }
    #container-details .answer-buttons.third {
        width: 200px;
    }
    #container-details .answer-buttons.two-third {
        width: 408px;
    }
    #container-details .answer-buttons.full {
        width: 100%;
        max-width: 607px;
    }
    #container-details .answer-buttons.tall {
        height: 60px;
    }
    #container-details .call-list-tag {
        background: #DEDEDE;
        display: inline-block;
        font-size: 14px;
        width: 48%;
        margin-bottom: 2px;
    }

    #container-details .call-list-tag .label {
        padding: 6px;
        color: #555 !important;
        font-size: 12px;
        font-weight: normal;
        width: 80%;
        overflow-x: hidden;
        white-space: nowrap;
    }

    #container-details .call-list-tag .remove-from-call-list{
        padding: 6px;
        font-weight: bold;
        float: right;
    }

    #container-details .call-list-tag .remove-from-call-list:hover {
        color: white;
        background: #c40000;
        cursor: pointer;
    }
    #container-details .action-boxes-container {
        width: 100%;
        float: left;
        margin-bottom: 25px;
    }
    #container-details .action-boxes li {
        float: left;
        list-style: none;
        margin-left: 0;
        margin-bottom: 6px;
        position: relative;

    }
    #container-details .action-boxes li:not(last-child) {
        margin-right: 4px;
    }
    #container-details .action-boxes li a,
    #container-details .action-boxes li a:visited {
        padding: 12px 0;
        border: 1px solid #AAA;
        color: #3f3f3f;
        display: inline-block;
        font-size: 12px;
        height: 50px;
        width: 75px;
        text-align: center;
        background: #efefef;
    }
    #container-details .action-boxes li a:hover{
        text-decoration: none;
        color: white;
        background: #337ab7;
        border-color: #275d8c;
    }
    #container-details .log-activity-button, #container-details .complete-task-button {
        padding: 10px;
        font-size: 14px;
    }
    #container-details #close-activity-log-button {
        top: 4px;
        right: 0;
        position: absolute;
    }
    #container-details #task-description {
        font-size: 14px;
    }
    #container-details textarea {
        font-size: 14px;
    }
    #container-details .dialer-activity-log-container {
        margin-top: 10px;
        margin-bottom: 40px;
    }
    #container-details .dialer-activity-log-container table tbody, #container-details .dialer-activity-log-container table thead
    {
        display: block;
    }

    #container-details .dialer-activity-log-container table tbody
    {
        overflow-y: auto;
        max-height: 300px;
    }
    #container-details .dialer-activity-log-container table tr.activity-log-header{
        width: 100%;
        display: block;
        background: #dddddd;
    }
    #container-details .dialer-activity-log-container table th{
        background: #dddddd;
        border: 0;
    }
    #container-details .dialer-activity-log-container table tbody tr:nth-child(odd) {
        background: #f5f5f5;
    }
    #container-details .dialer-activity-log-container table td {
        border-left: 0;
        border-right: 0;
        border-color: white #dadada white #dadada
    }
    #container-details .dialer-activity-log-container table .activity-date {
        width: 80px;
    }
    #container-details .dialer-activity-log-container table .activity-note {
        min-width: 200px;
        width: 85%;
    }
    #dt thead tr th div{
        font-weight: bold;
        font-size: 14px;
    }

    /* Label styling for status badges and more */
    .label {
        display: inline-block;
        padding: 5px;
        font-weight: bold;
        color: white;
        border-radius: 3px;
    }
    .label.label-default {
        background-color: #777;
    }
    .label.label-primary {
        background-color: #428bca;
    }
    .label.label-success {
        background-color: #5cb85c;
    }
    .label.label-info {
        background-color: #5bc0de;
    }
    .label.label-warning {
        background-color: #f0ad4e;
    }
    .label.label-danger {
        background-color: #d9534f;
    }
    select optgroup{
        /*background:#000;*/
        color:black;
        /*font-style:normal;*/
        font-weight:bold;
    }
    select option{
        border: 0;
        padding-left: 20px;
    }

    .yui3-panel-content > .yui3-widget-hd {
        display: none !important;
    }
</style>