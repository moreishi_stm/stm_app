<?php $module = Yii::app()->controller->module->id;
// flag for showing answered call
$debugShowAnsweredBox = (YII_DEBUG)? 0 : 0;
$this->renderPartial('indexCss');
?>
<div class="yui-skin-sam yui3-skin-sam" id="yui-modal-container"></div>

<div id="listview-actions" style="position: relative; text-align: right;">
    <a href="/<?php echo $this->module->name;?>/<?php echo $this->id;?>/sessions" class="button gray icon i_stm_search">Call Sessions</a>
    <a href="/<?php echo $this->module->name;?>/<?php echo $this->id;?>/callFinder" class="button gray icon i_stm_search" target="_blank">Call Finder</a>
    <a href="/<?php echo $this->module->name;?>/<?php echo $this->id;?>/viewVoicemailGreetings" class="button gray icon i_stm_search">Voicemail Greetings</a>
    <?if(Yii::app()->user->checkAccess('dialerimport') || Yii::app()->user->checkAccess('dialerListRequeue')):?>
        <a href="/<?php echo $this->module->name;?>/<?php echo $this->id;?>/callLists" class="button gray icon i_stm_edit">Call Lists</a>
    <?endif;?>

    <?if(Yii::app()->user->checkAccess('dialerimport')):?>
    <a href="/<?php echo $this->module->name;?>/<?php echo $this->id;?>/recordings" class="button gray icon i_stm_search" target="_blank">Call Recordings</a>
    <a href="/<?php echo $this->module->name;?>/<?php echo $this->id;?>/donotcalllist" class="button gray icon i_stm_search">Do Not Call List</a>
    <a href="/<?php echo $this->module->name;?>/dialerimport" class="button gray icon i_stm_add">Dialer Import</a>
    <?endif;?>
    <button type="button" id="answer-check" style="display: inline;">Answer Check</button>
    <button class="button load-list" <?=(strpos($_SERVER['SERVER_NAME'], 'www.nothing.local') !== false) ? '' : 'style="display:none;"';?>>Refresh</button>
    <button class="button io-error" <?=(strpos($_SERVER['SERVER_NAME'], 'www.christineleeteam.local') !== false) ? '' : 'style="display:none;"';?>>IO Error Test</button>
</div>

<h1>Dialer List: <?=$callList->name?></h1>
<?=CHtml::hiddenField('from', $from)?>
<?=CHtml::hiddenField('callerIdName', $callIdName)?>
<?=CHtml::hiddenField('callerIdNumber', $callerIdNumber)?>
<?=CHtml::hiddenField('num_to_call', $num_to_call)?>
<?=CHtml::hiddenField('callListId', $callList->id)?>
<?=CHtml::hiddenField('preset', $callList->preset_ma)?>
<?=CHtml::hiddenField('tableType', $callList->type->table_type)?>
<?=CHtml::hiddenField('default_voicemail', $voicemailId)?>
<?=CHtml::hiddenField('nodeUrl', $nodeUrl)?>

<?php if($hasActiveSession): ?>
    <h3>Active Call Session already exists. Please hang up that session to continue and refresh this page.</h3>
<?php else: ?>
<?php endif; ?>

<form method='POST' id='form-call-now'>
    <div class="p-clr" style="margin: 30px;"></div>
    <div id="call-button-container">
        <button type='submit' id='btn-start' style="font-size: 14px;"><i style="font-size: 18px; position: relative; top: 2px;" class="fa fa-phone"></i> Start Call Session</button>
        <a id="btn-load-list" class="action-button fa fa-eye blue dialer-tip" data-tooltip="Preview Call List"></a>
        <a href="/<?=Yii::app()->controller->module->id?>/<?=Yii::app()->controller->id?>" class="action-button fa fa-refresh green dialer-tip" data-tooltip="Start Over"></a>
    </div>
</form>

<a name="viewTop"></a>
<div id='container-details'></div>
<div id="dt"></div>

<? Yii::import('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog');
if (!class_exists('ActivityLogDialog', false)) {
    $this->widget('admin_widgets.DialogWidget.ActivityLogDialog.ActivityLogDialog', array(
        'id' => ActivityLogDialog::ACTIVITY_LOG_DIALOG_ID,
        'title' => 'Add New Activity Log',
        'triggerElement' => ActivityLogDialog::ACTIVITY_LOG_TRIGGERS,
        'parentModel' => new Contacts
    ));
}

Yii::import('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget');
if (!class_exists('DialerListDialogWidget', false)) {

    Yii::app()->clientScript->registerScript('dialerAddToListJs', <<<JS
        function postAddToDialerList(data) {
            var callListTag = '<div id="call-list-tag-'+ data.added.call_list_id +'" class="call-list-tag"><span class="label">'+ data.added.name +'</span><span class="remove-from-call-list" data-id="'+ data.added.call_list_id +'" data-pid="' + data.added.phoneId + '" data-ctid="'+ data.added.componentTypeId +'" data-cid="'+ data.added.componentId +'">X</span></div>';
            $('#call-list-tag-container').append(callListTag);
        }
JS
);

    $this->widget('admin_widgets.DialogWidget.DialerListDialogWidget.DialerListDialogWidget', array(
            'id' => DialerListDialogWidget::DIALOG_ID,
            'title' => 'Add to Dialer',
            'triggerElement' => '.add-to-dialer-list-button',
            'postAddFunction' => 'postAddToDialerList',
        )
    );
}

Yii::import('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget');
$this->widget('admin_widgets.DialogWidget.TaskDialogWidget.TaskDialogWidget', array(
    'id' => TaskDialogWidget::TASK_DIALOG_ID,
    'title' => 'Create New Task',
    'triggerElement' => '.add-task-button, .edit-task-button, .delete-task-button',
    'parentModel' => new Contacts
));

Yii::import('admin_widgets.DialogWidget.EmailDialogWidget.EmailDialogWidget');
$this->widget('admin_widgets.DialogWidget.EmailDialogWidget.EmailDialogWidget', array(
        'id' => 'email-dialog',
        'title' => 'Compose Email',
        'triggerElement' => '#send-email-button',
        'contactId' => null,
        'componentTypeId' => $callList->type->component_type_id,
    )
);

if($hasSmsProvidedNumber && Yii::app()->user->checkAccess('sms')
    && in_array($callList->type->component_type_id, array(ComponentTypes::BUYERS, ComponentTypes::SELLERS, ComponentTypes::CONTACTS, ComponentTypes::RECRUITS, ComponentTypes::CLOSINGS))) {
    $this->widget('admin_widgets.DialogWidget.SmsDialogWidget.SmsDialogWidget', array(
            'id' => 'sms-dialog',
            'title' => 'Compose SMS',
            'triggerElement' => '#send-sms-button',
//                'contactId' => (isset($parentModel->contact)) ? $parentModel->contact->id : $parentModel->id,
            'componentTypeId'  => $callList->type->component_type_id,
//                'componentId' => $parentModel->id
        )
    );
}

// Action Plans
$this->widget('admin_widgets.DialogWidget.ActionPlanDialogWidget.ActionPlanDialogWidget', array(
        'id' => 'action-plan-dialog',
        'parentModel' => null,
        'title' => 'Add Action Plan',
        'componentTypeId'  => $callList->type->component_type_id,
        'triggerElement' => '#add-action-plan-button',
    )
);
?>

<? $this->renderPartial('indexSortHandlebars', array('module'=>$module, 'voicemailId' => $voicemailId, 'hasSmsProvidedNumber' => $hasSmsProvidedNumber)); ?>
<? $this->renderPartial('indexSortJs', array('module'=>$module, 'debugShowAnsweredBox' => $debugShowAnsweredBox)); ?>
