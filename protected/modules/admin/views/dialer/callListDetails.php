<?php
$this->breadcrumbs=array(
'Call List Details'=>''
);

$module = Yii::app()->controller->module->name;
//Yii::app()->clientScript->registerScript('dialerCallListDetailsScript', <<<JS
//
//        $(".calllist-delete-button").live('click', function(){
//            if(confirm('Are you sure you want to delete this list?')) {
//                $("body").prepend("<div class='loading-container loading'><em></em></div>");
//                var id = $(this).data('id');
//                $.post('/$module/dialer/deleteCallLists/'+id, function(data) {
//                    $("div.loading-container.loading").remove();
//                    if(data.status=='success') {
//                        Message.create("success","Call List Deleted successfully.");
//                        $.fn.yiiGridView.update("call-list-grid", { data: $(this).serialize() });
//                    } else {
//                        Message.create("error","Error: Call List did not Delete.");
//                    }
//                },"json");
//            }
//        });
//
//        $(".calllist-export-button").click(function() {
//            if($(this).attr("data-maxed") == "true") {
//                alert('Please submit a request to support to export this list due to the size.');
//                return false;
//            }
//
//            $("body").prepend("<div class='loading-container loading'><em></em></div>");
//             $.fileDownload($(this).prop('href'), {
//                preparingMessageHtml: "We are preparing your report, please wait...",
//                failMessageHtml: "There was a problem generating your report, please try again."
//            });
//            return false;
//        });
//JS
//);
?>
    <div id="content-header">
        <h1><?=$calLList->name?></h1>
        <h2>Dialer Custom Call List Details</h2>
    </div>
<? $this->widget('admin_module.components.StmGridView', array(
        'id' => 'call-list-details-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'columns' => array(
//            array(
//                'type' => 'raw',
//                'name' => 'Seller ID',
//                'value' => '$data["sellerId"]',
//                'htmlOptions' => array('style' => 'width:80px'),
//            ),
            array(
                'type' => 'raw',
                'name' => 'Phone',
                'value' => 'Yii::app()->format->formatPhone($data["phone"])',
                'htmlOptions' => array('style' => 'width:120px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Name',
                'value' => '$data["first_name"]." ".$data["last_name"]',
            ),
            array(
                'type' => 'raw',
                'name' => 'Source',
                'value' => '$data["source"]',
                'htmlOptions' => array('style' => 'width:120px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Contact ID',
                'value' => '$data["contactId"]',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'ID',
                'value' => '$data["call_list_phone_id"]',
                'htmlOptions' => array('style' => 'width:10px'),
            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Type',
//                'value' => '"<div><a href=\"/".Yii::app()->controller->module->id."/sellers/".$data["sellerId"]."\" class=\"button gray icon i_stm_search grey-button\">View Seller</a></div>"',
//                'htmlOptions' => array('style' => 'width:120px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Queued',
//                'value' => '$data["queuedCount"]',
//                'htmlOptions' => array('style' => 'width:120px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Total',
//                'value' => '$data["totalCount"]',
//                'htmlOptions' => array('style' => 'width:120px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => 'Re-Queue Timeframe',
//                'value' => '$data["requeue_timeframe"]',
//                'htmlOptions' => array('style' => 'width:200px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Max Calls for List Order',
//                'value' => '($data["filter_max_call_count"]) ? $data["filter_max_call_count"]." calls" : "- Missing, please update. -"',
//                'htmlOptions' => array('style' => 'width:200px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '# Days for List Order',
//                'value' => '($data["filter_max_call_days"]) ? $data["filter_max_call_days"]." days" : "- Missing, please update. -"',
//                'htmlOptions' => array('style' => 'width:200px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"/".Yii::app()->controller->module->id."/dialer/editCallLists/".$data["id"]."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
//                'htmlOptions' => array('style' => 'width:80px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_search grey-button calllist-requeue-button\" data-id=\"".$data["id"]."\" data-name=\"".$data->name."\">Re-Queue</a></div>"',
//                'htmlOptions' => array('style' => 'width:120px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"javascript:void(0)\" class=\"button gray icon i_stm_delete grey-button calllist-delete-button\" data-id=\"".$data["id"]."\" data-name=\"".$data->name."\">Delete</a></div>"',
//                'htmlOptions' => array('style' => 'width:90px'),
//            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"/admin/dialer/callListExport/$data[id]\" class=\"button gray icon i_stm_add grey-button calllist-export-button\" data-id=\"".$data["id"]."\" data-name=\"".$data->name."\" data-maxed=\"".(($data["totalCount"] > 5000)? "true" : "false")."\">Export</a></div>"',
//                'htmlOptions' => array('style' => 'width:90px'),
//            ),
        ),
    )
);