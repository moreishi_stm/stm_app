<!-- Templates -->
<script id="template-details-incall" type="text/x-handlebars-template">
    <hr>
    <h1>{{phone}}</h1>
    <div class='live-activity-buttons'>
        <button type='button' data-type='live-answer'>Live Answer</button>
        <button type='button' data-type='no-answer'>No Answer</button>
        <button type='button' data-type='busy'>Busy [???]</button>
        <button type='button' data-type='remove-bad-number'>Remove Bad Number</button>
        <button type='button' data-type='fax'>Fax Line</button>
    </div>
    <hr>
</script>

<script id="template-details-postcall" type="text/x-handlebars-template">
    <hr>
    <a id="close-activity-log-button" class="action-button fa fa-times black close-notes-area"></a>
    <div id="dialer-left-container" class="g5">

        <style type="text/css">
            .action-boxes-container * {
                box-sizing: content-box !important;
            }
        </style>

        <div class="action-boxes-container">
            <ul class="action-boxes">
                <li><a href='/<?=$module?>/{{component_name}}/edit/{{component_id}}' target='_blank'>Set<br>Appointment</a></li>
                <li><a class="add-task-button phone-call-button" data='{{component_id}}' ctid='{{component_type_id}}'>Add<br>Follow-up<br>Task</a></li>
                <li><a id='send-email-button' href='javascript:void(0)' cntid='{{contact_id}}' cid='{{component_id}}' ctid='{{component_type_id}}'>Send<br>Email</a></li>
                <?if($hasSmsProvidedNumber):?><li><a id='send-sms-button' href='javascript:void(0)' cntid='{{contact_id}}' cid='{{component_id}}' ctid='{{component_type_id}}'>Send<br>SMS</a></li> <? endif;?>
                <li><a id='add-action-plan-button' href='javascript:void(0)' cid='{{component_id}}' ctid='{{component_type_id}}'>Apply<br>Action Plan</a></li>
            </ul>
        </div>
        {{#if activity_logs}}
        <h3>Activity Logs</h3>
        <div class="dialer-activity-log-container p-fl g12">
            <table>
                <thead>
                    <tr class="activity-log-header">
                        <th class="activity-date">Date</th>
                        <th class="activity-note">Note</th>
                    </tr>
                </thead>
                <tbody>
                {{#each activity_logs}}
                    <tr class="activity-log-row">
                        <td class="activity-date">{{date}}</td>
                        <td class="activity-note">{{note}}</td>
                    </tr>
                {{/each}}
                </tbody>
            </table>
        </div>
        {{/if}}

        <hr/>
        <h3>
            Dialer Call Lists
            <a id="add-to-dialer-list-button" class="dialer-tip action-button green fa fa-plus-circle add-to-dialer-list-button" data-tooltip='Add to Dialer List' data-id="{{call_id}}" data-pid="{{phone_id}}" data-ctid="{{component_type_id}}" data-cid="{{component_id}}"></a>
        </h3>
        <div id="call-list-tag-container">
            {{#each call_list_ids}}
            <div id="call-list-tag-{{id}}" class="call-list-tag"><span class="label">{{name}}</span><span class="remove-from-call-list" data-id="{{id}}" data-pid="{{phone_id}}" data-ctid="{{component_type_id}}" data-cid="{{component_id}}">X</span></div>
            {{/each}}
        </div>
    </div>
    <div id="dialer-right-container" class="g6">
        <div>
            <div class="action-boxes-container">
                <ul class="action-boxes">
                    <? /* <li><a>Spoke to</a></li>*/?>
                    <li><a id="vm-mark-no-answer" data-id='{{activity_log_id}}'>Hang up Ignore Voicemail</a></li>

                    <? if(!empty($voicemailId)):?>
                    <li><a id="leave-voicemail-button" data-id='{{call_id}}'>Leave Voicemail</a></li>
                    <? endif; ?>
                    <li><a id="add-to-do-not-call-button" data-id='{{call_list_phone_id}}' data-phone='{{phone}}' data-ctid='{{component_type_id}}' data-cid='{{component_id}}'>Add to<br>Do Not Call List</a>

                        <div id="do-not-call-days-container" style="display:none;">
                            <select id="do-not-call-days" class="error">
                                <option value="">-- Select Duration --</option>
                                <option value="7">Do Not Call for 1 week</option>
                                <option value="14">Do Not Call for 2 week</option>
                                <option value="21">Do Not Call for 3 week</option>
                                <option value="28">Do Not Call for 4 week</option>
                                <option value="42">Do Not Call for 6 week</option>
                                <option value="56">Do Not Call for 8 week</option>
                                <option value="70">Do Not Call for 10 week</option>
                                <option value="90">Do Not Call for 3 months</option>
                                <option value="180">Do Not Call for 6 months</option>
                                <option value="99999">Do Not Call Permanently</option>
                            </select>
                            <button class="add-to-do-not-call" data-id='{{call_list_phone_id}}' data-phone='{{phone}}' data-ctid='{{component_type_id}}' data-cid='{{component_id}}'>Submit</button>
                        </div>
                    </li>
                    <li><a class="delete-bad-number-button" data-id='{{call_id}}' data-pid="{{phone_id}}" data-ctid="{{component_type_id}}" data-cid="{{component_id}}">Delete Bad Number</a></li>
                    <li><a class="hang-up-call-next" data-id='{{call_list_phone_id}}'><br>Hang up</a></li>
                    <li><a id='dial-next-button' href='javascript:void(0)' data-id='{{session_id}}' data-clpid='{{call_list_phone_id}}' style='display:none;'>Dial<br>Next</a></li>
                </ul>
            </div>

            <a class="name" href='/<?=$module?>/{{component_name}}/{{component_id}}' target='_blank'>{{first_name}} {{spouse_first_name}} {{last_name}}</a> <a class="dialer-tip action-button red fa fa-search" href='/<?=$module?>/{{component_name}}/{{component_id}}' data-tooltip='View {{component_label}} Details' target='_blank'></a><br>
            <div class="g6">
            {{formatPhone phone}} <a class="dialer-tip action-button red fa fa-trash delete-bad-number-button" data-tooltip='Delete Bad Number' data-id='{{call_id}}' data-pid="{{phone_id}}" data-ctid="{{component_type_id}}" data-cid="{{component_id}}"></a>
            <br>
            </div>
            {{#if address}}
            <div class="g6">
            <br>
                {{address}}<br>
                {{city}}, {{state}} {{zipcode}}<br>
                <a target="_blank" href="http://www.zillow.com/homes/{{address}},-{{city}},-{{state}},-{{zipcode}}_rb"><img style="top: 5px; position: relative;" width="76" height="26" src="http://cdn.seizethemarket.com/assets/images/zillow_mini_logo_2.png" alt="Zillow"></a>
                <a target="_blank" href="http://maps.google.com/maps?geocode=&amp;q={{address}} {{city}}, {{state}} {{zipcode}}"><img style=" top: 5px; position: relative;  margin-left: 8px;" width="76" height="26" src="http://cdn.seizethemarket.com/assets/images/google_maps_mini_logo.png" alt="Google Maps"></a>
            </div>
            {{/if}}

            {{#if source}}
                <strong>Source:</strong>{{source}}<br>
            {{/if}}
            <hr/>

            {{#if description}}
                <div id="task-container" class="g12">
                    <h2 class="p-tl">Task Description</h2>
                    <div id="task-description" class="g6">{{description}}</div>
                    <div class="g6 p-fr p-tr">
                        <button type='button' class="complete-task-button p-fr" data="{{task_id}}" ctid="{{component_type_id}}">Log Call & Complete Task</button>
                    </div>
                    <hr class="g12">
                </div>
            {{else}}
                <form method='POST' class='activity-form'>
                    <h3>Activity Log Notes<span class="required">*</span></h3>
                    <textarea class="g12" rows='4' name='ActivityLog[note]' placeholder="Add notes here..."></textarea><br>
                    <div id="lead-gen-spoke-to-container" class="g12">
                        <div class="row">
                            <label><span class="required">*</span>Spoke to:</label>
                            <span>
                                <input id="" type="radio" name="ActivityLog[is_spoke_to]" class="spoke-to-button" value="1"> Yes
                                <input id="" type="radio" name="ActivityLog[is_spoke_to]" class="spoke-to-button" value="0"> No
                            </span>
                        </div>
                        <div class="row">
                            <label>Asked for Referral:</label>
                            <span>
                                <input id="" type="radio" name="ActivityLog[asked_for_referral]" class="asked-for-referral-button" value="1"> Yes
                                <input id="" type="radio" name="ActivityLog[asked_for_referral]" class="asked-for-referral-button" value="0"> No
                            </span>
                            <input id="" type="hidden" name="ActivityLog[lead_gen_type_ma]" value="3">
                        </div>
                    </div>
                    <br>
                    <input type='hidden' name='ActivityLog[component_id]' value='{{component_id}}'>
                    <input type='hidden' name='ActivityLog[component_type_id]' value='{{component_type_id}}'>
                    <input type='hidden' name='ActivityLog[activity_date]' value='{{activity_date}}'>
                    <input type='hidden' name='ActivityLog[scenario]' value='dialer'>
                    <input type='hidden' name='ActivityLog[task_type_id]' value='42'>
                    <input type='hidden' name='ActivityLog[task_id]' value='{{task_id}}'>
                    <input type='hidden' name='ActivityLog[activity_date]' value='<?=date('Y-m-d H:i:s')?>'>
                    <input type='hidden' name='is_dialer' value='true'>
                    <input type='hidden' name='referrerUrl' value='<?=$_SERVER['REQUEST_URI']?>'>
                    <input type='hidden' name='call_id' value='{{call_id}}'>
                    <button type='submit' class="log-activity-button g12">Add Activity Log</button><br/><br/>
                </form>
            {{/if}}

            <div class='activity-success-message' style="text-align: center; margin: 30px 0px; display: none;">
                <div style="margin-bottom: 20px;">- Activity Log Entry Saved! -</div>
                <button type='button' class='activity-log-add-another'><i class="fa fa-plus-circle"></i> Add Another Activity Log</button>
            </div>
        </div>
    </div>
    <hr>
</script>