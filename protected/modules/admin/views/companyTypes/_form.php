<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'company-type-form',
	'enableAjaxValidation'=>false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'=>'Company Types',
		'handleIconCss'=>'i_strategy'
	));
	?>
	<div id="action-plans-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
	        <table class="container">
	        	<tr>
	        		<th class="narrow">Name:</th>
	        		<td colspan="3">
                        <?php if($model->id > CompanyTypes::CORE_ID_MAX || Yii::app()->controller->action->id == 'add'): ?>
                            <?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Company Type', 'class'=>'g9', ));?>
                            <?php echo $form->error($model,'name'); ?>
                        <?php else: ?>
                            <?php echo $model->name;
                            echo '<br><br><span class="label">NOTE: This is a core field and cannot be changed.</span>';
                            ?>
                        <?php endif; ?>
	            	</td>
	        	</tr>
	        </table>

		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Company Type</button></div>
<?php $this->endWidget(); ?>