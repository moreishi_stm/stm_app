<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->id.'/contacts'),
);

?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->id; ?>/companyTypes/add" class="button gray icon i_stm_add">Add New Company Types</a>
</div>
<div id="content-header">
	<h1>Company Types List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBox',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'company-types-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'id',
		'name',
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				($data->id > CompanyTypes::CORE_ID_MAX)?"<div><a href=\"/".Yii::app()->controller->module->id."/companyTypes/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_search grey-button\">View Details</a></div>": ""
			',
			'htmlOptions'=>array('style'=>'width:120px; height: 32px;'),
		),
	),
));
