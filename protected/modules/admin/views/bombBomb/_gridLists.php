<?php $this->widget('admin_module.components.StmGridView', array(
		   'id' => 'bombBomb-grid',
		   'dataProvider' => $DataProvider,
		   'itemsCssClass' => 'datatables',
		   'columns' => array(
			   'name',
			   'ContactCount',
			   array(
				   'type' => 'raw',
				   'name' => '',
				   'value' => '"<div><a href=\"#\" class=\"button gray icon i_stm_search grey-button bombBombGridUpdate\" data-type=\"listContacts\" data-id=\"".$data[id]."\">View Contacts</a></div>"',
				   'htmlOptions' => array('style' => 'width:140px'),
			   ),
		   ),
		   )
);