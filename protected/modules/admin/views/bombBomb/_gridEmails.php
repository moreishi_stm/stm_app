<?php $this->widget('admin_module.components.StmGridView', array(
		   'id' => 'bombBomb-grid',
		   'dataProvider' => $DataProvider,
           'extraParams'   =>array('existingBbIds'=>$existingBbIds),
		   'itemsCssClass' => 'datatables',
		   'columns' => array(
			   array(
				   'type' => 'raw',
				   'name' => '',
				   'value' => '"<div class=\"email_thumbnail\" style=\"background-image: url(".$data[thumbUrl].")\"></div>"',
				   'htmlOptions' => array('style' => 'width:160px'),
			   ),
			   'name',
			   'description',
			   'subject',
               array(
                   'type' => 'raw',
                   'name' => '',
                   'value' => 'Yii::app()->controller->action->printButton($data, $this->grid->extraParams["existingBbIds"])',
                   'htmlOptions' => array('style' => 'width:80px'),
               ),
		   ),
		   )
);