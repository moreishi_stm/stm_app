<?php
Yii::app()->clientScript->registerScript('search', "

	$('.bombBombGridUpdate').live('click',function() {
		$.fn.yiiGridView.update('bombBomb-grid', { data: {'type':$(this).data('type'), 'id':$(this).data('id')} });
		if($(this).data('type')=='lists') {
			$('#create-list-container').removeClass('hidden');
		} else {
			$('#create-list-container').addClass('hidden');
		}

		if($(this).data('type')=='listContacts') {
			var listId = $(this).data('id');
			var refreshButton = $('#refresh-list-button');
			refreshButton.removeClass('hidden');
			refreshButton.attr('data-id', listId);
		} else {
			$('#refresh-list-button').addClass('hidden');
		}

		if($(this).data('type')=='videos') {
			$('#add-video-button').removeClass('hidden');
		} else {
			$('#add-video-button').addClass('hidden');
		}
	});

	$('#create-list-form').submit(function(){
		$.fn.yiiGridView.update('bombBomb-grid', { data: {'type':'createList', 'id':$(this).data('id'),'name':$('#createList_name').val()} });
		return false;
	});

	$('#add-video-button').click(function(){
		alert('Coming Soon...');
	});
");
$this->breadcrumbs=array(
	'Home Page',
);
?>
<style type="text/css">
	ul.bombBomb{
		width: 900px;
		margin-left: auto;
		margin-right: auto;
	}
	ul.bombBomb li {
		list-style: none;
	}
	ul.bombBomb li a{
		width: 200px;
		display: inline-block;
		color: white;
		background-color: #f46304;
		margin: 10px;
		padding: 20px 0;
		float: left;
		text-align: center;
		vertical-align: middle;
		font-size: 20px;
		font-weight: bold;
		-webkit-border-radius: 12px;
		-moz-border-radius: 12px;
		border-radius: 12px;
		-webkit-box-shadow: #999 1px 2px 4px;
		-moz-box-shadow: #999 1px 2px 4px;
		box-shadow: #999 1px 2px 4px;	}
	ul.bombBomb li a:hover{
		color: black;
		background-color: #FFA366;
	}
	ul.bombBomb.functions{
		width: 100%;;
	}
	ul.bombBomb.functions li a{
		float: right;
		width: 100px;
		font-size: 12px;
		font-weight: bold;
		padding: 12px 0;
		margin: 0 10px;
	}

	#bombBomb-grid .email_thumbnail,
	#bombBomb-grid .video_thumbnail {
		width: 160px;
		height: 140px;
		float: left;
		border-top-left-radius: 4px;
		border-bottom-left-radius: 4px;
		background-size: 150%;
		background-position: center;
	}
	#bombBomb-grid .video_thumbnail {
		width: 250px;
	}
</style>
<?php
if($view) {
    $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'bombBomb-list-form',
        )); ?>
    <h1>BombBomb</h1>
    <br />
    <br />
    <h3>Select one of the following options:</h3>
    <ul class="bombBomb">
        <li><a href="javascript:void(0);" class="bombBombGridUpdate" data-type="emails">Emails</a></li>
        <li><a href="javascript:void(0);" class="bombBombGridUpdate" data-type="lists">Lists</a></li>
        <li><a href="javascript:void(0);" class="bombBombGridUpdate" data-type="drips">Drips</a></li>
        <li><a href="javascript:void(0);" class="bombBombGridUpdate" data-type="videos">Videos</a></li>
    </ul>
    <?php $this->endWidget(); ?>

    <div class="clear"></div>
    <div id="function-containers">
        <ul class="bombBomb functions">
            <li><a href="#" id="refresh-list-button" class="bombBombGridUpdate hidden" data-type="listContacts" data-id="">Refresh</a></li>
            <li id="create-list-container" class="hidden">
                <div class="g2"></div>
                <div id="listview-search" class="g8 p-mh0 p-pv10 grey-gradient-box">
                    <?php $listForm=$this->beginWidget('CActiveForm', array(
                            'action'=>Yii::app()->createUrl($this->route),
                            'method'=>'get',
                            'id'=>'create-list-form',
                        )); ?>
                    <div class="g1"></div>
                    <label class="g2">List Name:</label>
                    <div class="g6"><?php echo CHtml::textField('createList[name]',null,$htmlOptions=array()); ?></div>
                    <div class="g3 submit" style="text-align:center"><?php echo CHtml::submitButton('Create List', array('class' => 'button','id'=>'create-list-button','data-type'=>'createList')); ?></div>
                    <?php $this->endWidget(); ?>
                </div>
            </li>
            <li><a href="#" id="add-video-button" class="hidden">Add Video</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <?php echo $this->renderPartial('_grid'.$view, array('DataProvider'=>$DataProvider,'existingBbIds' => $existingBbIds));
}
// api key is missing
else {
    echo '<br><br><br></be><h1>Please go to your Account Settings and enter your BombBomb API Key.</h1>';
}

