<?php $this->widget('admin_module.components.StmGridView', array(
		   'id' => 'bombBomb-grid',
		   'dataProvider' => $DataProvider,
		   'itemsCssClass' => 'datatables',
		   'columns' => array(
			   array(
				   'type' => 'raw',
				   'name' => 'Status',
				   'value'=>'($data[active])?"Active":"Inactive"',
			   ),
			   array(
				   'type' => 'raw',
				   'name' => 'Name',
				   'value'=>'$data[name]',
			   ),
			   array(
				   'type' => 'raw',
				   'name' => '# Drips',
				   'value'=>'$data[drip_drop_count]',
			   ),
			   array(
				   'type' => 'raw',
				   'name' => '# Contacts',
				   'value'=>'"Total Contacts :".$data[drip_contact_count]."<br>Active Contacts:".$data[drip_contact_active_count]."<br>Inactive Contacts:".$data[drip_contact_inactive_count]',
			   ),
			   array(
				   'type' => 'raw',
				   'name' => 'Drip Length',
				   'value'=>'$data[longest_delay_units_calculated]." ".$data[longest_delay_units_display]',
			   ),
		   ),
		   )
);