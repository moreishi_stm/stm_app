<?php $this->widget('admin_module.components.StmGridView', array(
		   'id' => 'bombBomb-grid',
		   'dataProvider' => $DataProvider,
		   'itemsCssClass' => 'datatables',
		   'columns' => array(
			   array(
				   'type' => 'raw',
				   'name' => '',
				   'value' => '"<div class=\"video_thumbnail\" style=\"background-image: url(".$data[thumbUrl].")\"></div>"',
				   'htmlOptions' => array('style' => 'width:160px'),
			   ),
			   array(
				   'type' => 'raw',
				   'name' => 'Status',
				   'value'=>'($data[status])?"Active":"Inactive"',
			   ),
			   'name',
			   'description',
			   array(
				   'type' => 'raw',
				   'name' => 'Video',
				   'value'=>'"<a href=\"".$data[vidUrl]."\" target=\"_blank\">Click to Watch Video</a>"',
			   ),
//			   array(
//				   'type' => 'raw',
//				   'name' => 'Last Name',
//				   'value'=>'$data[last_name]',
//			   ),
//			   array(
//				   'type' => 'raw',
//				   'name' => 'Phone',
//				   'value'=>'$data[phone]',
//			   ),
		   ),
		   )
);