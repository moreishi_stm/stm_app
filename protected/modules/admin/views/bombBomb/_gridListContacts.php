<?php $this->widget('admin_module.components.StmGridView', array(
		   'id' => 'bombBomb-grid',
		   'dataProvider' => $DataProvider,
		   'itemsCssClass' => 'datatables',
		   'columns' => array(
			   array(
				   'type' => 'raw',
				   'name' => 'Email',
				   'value'=>'$data[email]',
			   ),
			   array(
				   'type' => 'raw',
				   'name' => 'First Name',
				   'value'=>'$data[first_name]',
			   ),
			   array(
				   'type' => 'raw',
				   'name' => 'Last Name',
				   'value'=>'$data[last_name]',
			   ),
			   array(
				   'type' => 'raw',
				   'name' => 'Phone',
				   'value'=>'$data[phone]',
			   ),
		   ),
		   )
);