<?php $this->widget('admin_module.extensions.moneymask.MMask',array(
    'element'=>'#Goals_average_sales_price, #Goals_income_per_closing, #Goals_income_per_year, #Goals_fees',
    'currency'=>'PHP',
    'config'=>array(
    	'precision'=>0,
    	'symbol'=>'$',
    	'showSymbol'=>true,
    )
));

$this->widget('admin_module.extensions.moneymask.MMask',array(
    'element'=>'#Goals_contacts_per_appointment, #Goals_lead_gen_minutes_per_day, #Goals_contacts_per_day',
    'currency'=>'PHP',
    'config'=>array(
    	'precision'=>0,
    	'thousands'=>'',
    )
));

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'goals-form',
	'enableAjaxValidation'=>false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'=>$mode.' Goals',
		'handleIconCss'=>'i_wizard'
	));
	?>
	<div id="goals-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
			<div id="goals-left" class="g6">
		        <table class="container">
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'status_ma');?></th>
		        		<td><?php echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive'), $htmlOptions=array('empty'=>'Select One')); ?>
		        			<?php echo $form->error($model,'status_ma'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'name');?></th>
		        		<td><?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Goal Name', 'class'=>'g9', ));?>
		        			<?php echo $form->error($model,'name'); ?>
		        		</td>
		        	</tr>
		        </table>
	    	</div>
			<div id="goals-right" class="g6">
		        <table class="container">
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'year');?></th>
		        		<td><?php $startYear = ($model->year> 2000)? $model->year : date('Y');
                            $yearData = StmFormHelper::getDropDownList($opt=array('start'=>$startYear,'end'=>date('Y')+1,'increment'=>1));
                            echo $form->dropDownList($model, 'year', $yearData,
		        									$htmlOptions=array('empty'=>'Select One','class'=>'g4'));
		        			?>
		        			<?php echo $form->error($model,'year'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th>Date Range</th>
		        		<td>
                            <div class="g5">
                                <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        'model'=>$model,
                                        'attribute'=>'start_date',
                                        'options'=>array(
                                            'showAnim'=>'fold',
                                        ),
                                        'htmlOptions'=>array(
                                            'placeholder'=>'Start Date',
                                            'class'=>'g100',
                                            'readonly'=>'readonly',
                                        ),
                                    ));
                                ?>
                                <?php echo $form->error($model,'start_date',$htmlOptions=array('style'=>'display:inline-block;')); ?>
                            </div>
							<span class="label p-p9"> to </span>
                            <div class="g5">
                                <?	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        'model'=>$model,
                                        'attribute'=>'end_date',
                                        'options'=>array(
                                            'showAnim'=>'fold',
                                        ),
                                        'htmlOptions'=>array(
                                            'placeholder'=>'End Date',
                                            'class'=>'g100',
                                            'readonly'=>'readonly',
                                        ),
                                    ));
                                ?>
                                <?php echo $form->error($model,'end_date',$htmlOptions=array('style'=>'display:inline-block; margin-left:5px;')); ?>
                            </div>
		        		</td>
		        	</tr>
		        </table>
	    	</div>

	    	<hr />

	        <table class="container">
	        	<tr>
	        		<th class="narrow"><?php echo $form->labelEx($model, 'income_per_year');?></th>
	        		<td>
                        <?php echo $form->textField($model,'income_per_year', $htmlOptions=array('placeholder'=>'Annual Income', 'class'=>'g3 income-goal', ));?>
                        <div class="g3"></div>
                        <div class="g4" style="display: block; background: #f5f63a; padding:6px; text-align: center; font-size: 16px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;">Note: To Update Calculations<br />Click Submit Below.</div>
	        			<?php echo $form->error($model,'income_per_year'); ?>
	        		</td>
	        	</tr>
                <tr>
                    <th class="narrow"><?php echo $form->labelEx($model, 'fees');?></th>
                    <td>
                        <?php echo $form->textField($model,'fees', $htmlOptions=array('placeholder'=>'Fees', 'class'=>'g3', ));?>
                        <div class="g3"></div>
                        <div class="g4" style="display: block; background: #f5f63a; padding:6px; text-align: center; font-size: 16px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;">Note: To Update Calculations<br />Click Submit Below.</div>
                        <?php echo $form->error($model,'fees'); ?>
                    </td>
                </tr>
	        </table>
			<div id="goals-left" class="g6">
		        <table class="container">
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'average_sales_price');?></th>
		        		<td><?php echo $form->textField($model,'average_sales_price', $htmlOptions=array('placeholder'=>'Average Sales Price', 'class'=>'g4', ));?>
		        			<?php echo $form->error($model,'average_sales_price'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'incomePerClosing');?></th>
		        		<td>
		        			<label class="goals-metric p-mr5"><?php echo '$'.number_format($model->incomePerClosing);?></label>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'average_commission_percent');?></th>
		        		<td><?php echo $form->dropDownList($model, 'average_commission_percent', StmFormHelper::getPercentageList(array('max'=>10,'sort'=>'asc')), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?>
		        			<?php echo $form->error($model,'average_commission_percent'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'commission_split');?></th>
		        		<td>
                            <?php echo $form->dropDownList($model, 'commission_split', StmFormHelper::getPercentageList(array('sort'=>'asc')), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?>
		        			<?php echo $form->error($model,'commission_split'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th>Closings per Year</th>
		        		<td><label class="goals-metric p-mr5"><?php echo $model->annualClosings;?></label>
		        		</td>
		        	</tr>

		        </table>
	    	</div>
			<div id="goals-right" class="g6">
		        <table class="container">
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'contracts_closing_conversion');?></th>
		        		<td><?php echo $form->dropDownList($model, 'contracts_closing_conversion', StmFormHelper::getPercentageList(), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?><span class="label p-p9">=</span><label class="goals-metric p-mr5"><?php echo $model->annualContracts;?></label><span class="label p-pt8">Contracts per Year</span>
		        			<?php echo $form->error($model,'contracts_closing_conversion'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'agreements_contract_conversion');?></th>
		        		<td><?php echo $form->dropDownList($model, 'agreements_contract_conversion', StmFormHelper::getPercentageList(), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?><span class="label p-p9">=</span><label class="goals-metric p-mr5"><?php echo $model->annualAgreements;?></label><span class="label p-pt8">Agreements per Year</span>
		        			<?php echo $form->error($model,'agreements_contract_conversion'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'appointments_agreement_conversion');?></th>
		        		<td><?php echo $form->dropDownList($model, 'appointments_agreement_conversion', StmFormHelper::getPercentageList(), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?><span class="label p-p9">=</span><label class="goals-metric p-mr5"><?php echo $model->annualAppointments;?></label><span class="label p-pt8">Appts Met per Year</span>
		        			<?php echo $form->error($model,'appointments_agreement_conversion'); ?>
		        		</td>
		        	</tr>
                    <tr>
                        <th><?php echo $form->labelEx($model, 'appointments_set_met_conversion');?></th>
                        <td><?php echo $form->dropDownList($model, 'appointments_set_met_conversion', StmFormHelper::getPercentageList(), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?><span class="label p-p9">=</span><label class="goals-metric p-mr5"><?php echo ($model->appointments_set_met_conversion > 0)? round($model->annualAppointments/$model->appointments_set_met_conversion) : '-';?></label><span class="label p-pt8">Appts Set per Year</span>
                            <?php echo $form->error($model,'appointments_set_met_conversion'); ?>
                        </td>
                    </tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'contacts_per_appointment');?></th>
		        		<td>
                            <?php echo $form->dropDownList($model, 'contacts_per_appointment', StmFormHelper::getDropDownList(array('start'=>1,'end'=>100,'increment'=>1)), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?><span class="label p-p9">=</span><label class="goals-metric p-mr5"><?php echo $model->annualContacts;?></label><span class="label p-pt8">Contacts per Year</span>
		        			<?php echo $form->error($model,'contacts_per_appointment'); ?>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><label>Contacts per Month</label></th>
		        		<td><label class="goals-metric p-mr5"><?php echo ceil($model->annualContacts/10.5);?></label><span class="label p-p9">=</span><label class="goals-metric p-mr5"><?php echo $model->weeklyContacts;?></label><span class="label p-p9">Contacts per Week</span>
		        		</td>
		        	</tr>
                    <tr>
                        <th><label>Weeks Without Lead Gen</label></th>
                        <td>
                            <?php echo $form->dropDownList($model, 'weeks_time_off', StmFormHelper::getDropDownList(array('start'=>1,'end'=>52,'increment'=>1)), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?>
                            <span class="label"><u>9 weeks suggested</u><br>
                                3 weeks: Vacation<br>
                                2 weeks: Christmas / Thanksgiving<br>
                                2 weeks: Education / Training<br>
                                1 weeks: Other Holidays<br>
                                1 weeks: Sick/Personal/Misc<br>
                            </span>
                        </td>
                    </tr>
		        	<tr>
		        		<th><label>Contacts per Day</label></th>
		        		<td>
                            <label class="goals-metric p-mr5"><?php echo ceil($model->contacts_per_day);?></label><span class="label p-p9">@ 4 lead gen days per week</span>
		        		</td>
		        	</tr>
		        	<tr>
		        		<th><?php echo $form->labelEx($model, 'lead_gen_minutes_per_day');?></th>
		        		<td>
                            <?php echo $form->dropDownList($model, 'lead_gen_minutes_per_day', StmFormHelper::getDropDownList(array('start'=>1,'end'=>480,'increment'=>1)), $htmlOptions=array('empty'=>'Select One', 'class'=>'g3',)); ?>
                            <span class="label p-p9">=</span><label class="goals-metric p-mr5"><?php echo $model->monthlyLeadGen;?></label><span class="label p-pt8">Hours per Month @ 4 lead gen days per week</span>
		        			<?php echo $form->error($model,'lead_gen_minutes_per_day'); ?>
		        		</td>
		        	</tr>
		        	<?php echo $form->hiddenField($model,'contact_id');?>
		        </table>
	    	</div>
		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit to Calculate Goal</button></div>
<?php $this->endWidget(); ?>