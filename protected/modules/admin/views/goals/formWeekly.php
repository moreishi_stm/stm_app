<?
$this->breadcrumbs=array(
	'Weekly Goal'=>''
);

$css = <<<CSS
    optgroup {
        margin-left: 10px;
    }
    optgroup option{
        margin-left: 20px;
        border: 0;
    }

    #goal-units .alert {
        font-size: 14px;
        font-weight: bold;
        color: #D20000;
    }
    tr.conversion th, tr.conversion td{
        background-image: none;
        background: #d0e6ff;
    }
    #goals-container select.zero {
        background: #CCC;
    }
CSS;
Yii::app()->clientScript->registerCss('formWeeklyGoals', $css);

$js = <<<JS
    $('select').change(function(){

        if($(this).hasClass('oneThingType') || confirm('Do you want to update all following weeks with this value?')) {
            var className = "GoalsWeekly_collection_";
            var classBaseNameLength = className.length;
            var id = $(this).attr('id');
            var name = id.slice(classBaseNameLength);
            var index = name.slice(0,name.search("_"));
            var nameSuffix = name.slice(name.search("_"));

            // if oneThingType dropdown, update all from beginning
            if($(this).hasClass('oneThingType')) {
                index = 0;
            }

            while(index <= 52) {
                var currElementName = "#"+className+index+nameSuffix;
                if($(currElementName).val() != "0" && $(currElementName).val() != "0.0") {
                    $(currElementName).val($(this).val()).hide().show('slow');

                    if($(this).val() == "0.0" || $(this).val() == "0") {
                        $(currElementName).addClass('zero');
                    }
                    else {
                        $(currElementName).removeClass('zero');
                    }
                }

                index++;
            }
            Message.create('success','Successfully updated following fields. Zeros were not updated.');
        }

        if($(this).hasClass('one-thing-quantity') && ($(this).val() == "0" || $(this).val() == "0.0")) {
            if(confirm('Do you want to ZERO out all the goals for this week?')) {
                var weekNum = $(this).data('weeknum');
                $('#GoalsWeekly_collection_' + weekNum + '_working_days').val(0).addClass('zero');
                $('#GoalsWeekly_collection_' + weekNum + '_lead_gen_hours').val(0).addClass('zero');
                $('#GoalsWeekly_collection_' + weekNum + '_contacts').val(0).addClass('zero');
                $('#GoalsWeekly_collection_' + weekNum + '_appointments_set').val('0.0').addClass('zero');
                $('#GoalsWeekly_collection_' + weekNum + '_appointments_met').val('0.0').addClass('zero');
                $('#GoalsWeekly_collection_' + weekNum + '_signed').val('0.0').addClass('zero');
                $('#GoalsWeekly_collection_' + weekNum + '_contracts').val('0.0').addClass('zero');
                $('#GoalsWeekly_collection_' + weekNum + '_closings').val('0.0').addClass('zero');
            }
        }

        if($(this).val() == "0" || $(this).val() == "0.0") {
            $(this).addClass('zero');
        }
        else {
            $(this).removeClass('zero');
        }
    });
JS;

Yii::app()->clientScript->registerScript('formWeeklyGoals', $js);

$goalUnitAlertMessage = 'Weekly goal is lower than Annual goal';
$conversionAlertMessage = 'Actual Conversion is lower than Annual goal';

?>
<div id="listview-actions">
<!--    <a href="/admin/goals/calculator" class="button gray icon i_stm_edit">Calculator</a>-->
<!--	<a href="/admin/goals/add" class="button gray icon i_stm_add">Add New Goal</a>-->
<!--    <a href="/admin/goals/oneThing" class="button gray icon i_stm_search">One Thing</a>-->
    <a href="/admin/goals/edit/<?=$goal->id?>" class="button gray icon i_stm_edit grey-button">Edit Goal</a>
</div>
<div id="content-header">
	<h1>Weekly Goal</h1>
    <h1><?=$goal->contact->fullName?></h1>
    <table id="goal-units" style="width: 70%; margin-left: 15%; margin-right: 15%; margin-top: 20px; margin-bottom: 20px; clear: both;">
        <tr>
            <th></th>
            <th>per Annual Goal</th>
            <th>per Weekly Totals</th>
            <th>per Working Week @ <?=$leadGenWeeks?></th>
            <th>Difference</th>
        </tr>
        <tr>
            <th>Closings</th>
            <td><?=$goalClosings?></td>
            <td><?=$weeklySumClosings?></td>
            <td><?=(($goalClosings) ? number_format($goalClosings/$leadGenWeeks, 1) : '')?></td>
            <td class="alert"><?=($goalClosings > $weeklySumClosings)? $goalUnitAlertMessage.' by '.($goalClosings - $weeklySumClosings).'.' : ''?></td>
        </tr>
<!--        <tr class="conversion">-->
<!--            <th>Contracts-Close %</th>-->
<!--            <td>--><?//=$goalContractCloseConversion.'%'?><!--</td>-->
<!--            <td>--><?//=$weeklyContractCloseConversion.'% (Actual)'?><!--</td>-->
<!--            <td class="alert">--><?//=($goalContractCloseConversion > $weeklyContractCloseConversion)? $conversionAlertMessage.' by '.($goalContractCloseConversion - $weeklyContractCloseConversion).'%.' : ''?><!--</td>-->
<!--        </tr>-->
        <tr>
            <th>Contracts</th>
            <td><?=$goalContracts?></td>
            <td><?=$weeklySumContracts?></td>
            <td><?=(($goalContracts) ? number_format($goalContracts/$leadGenWeeks, 1) : '')?></td>
            <td class="alert"><?=($goalContracts > $weeklySumContracts)? $goalUnitAlertMessage.' by '.($goalContracts - $weeklySumContracts).'.' : ''?></td>
        </tr>
<!--        <tr class="conversion">-->
<!--            <th>Signed-Contract %</th>-->
<!--            <td>--><?//=$goalSignedContractConversion.'%'?><!--</td>-->
<!--            <td>--><?//=$weeklySignedContractConversion.'% (Actual)'?><!--</td>-->
<!--            <td class="alert">--><?//=($goalSignedContractConversion > $weeklySignedContractConversion)? $conversionAlertMessage.' by '.($goalSignedContractConversion - $weeklySignedContractConversion).'%.' : ''?><!--</td>-->
<!--        </tr>-->
        <tr>
            <th>Signed</th>
            <td><?=$goalAgreements?></td>
            <td><?=$weeklySumAgreements?></td>
            <td><?=(($goalAgreements) ? number_format($goalAgreements/$leadGenWeeks, 1) : '')?></td>
            <td class="alert"><?=($goalAgreements > $weeklySumAgreements)? $goalUnitAlertMessage.' by '.($goalAgreements - $weeklySumAgreements).'.' : ''?></td>
        </tr>
<!--        <tr class="conversion">-->
<!--            <th>Met-Signed %</th>-->
<!--            <td>--><?//=$goalMetSignedConversion.'%'?><!--</td>-->
<!--            <td>--><?//=$weeklyMetSignedConversion.'% (Actual)'?><!--</td>-->
<!--            <td class="alert">--><?//=($goalMetSignedConversion > $weeklyMetSignedConversion)? $conversionAlertMessage.' by '.($goalMetSignedConversion - $weeklyMetSignedConversion).'%.' : ''?><!--</td>-->
<!--        </tr>-->
        <tr>
            <th>Appts Met</th>
            <td><?=$goalMetAppointments?></td>
            <td><?=$weeklySumMetAppointments?></td>
            <td><?=(($goalMetAppointments) ? number_format($goalMetAppointments/$leadGenWeeks, 1) : '')?></td>
            <td class="alert"><?=($goalMetAppointments > $weeklySumMetAppointments)? $goalUnitAlertMessage.' by '.($goalMetAppointments - $weeklySumMetAppointments).'.' : ''?></td>
        </tr>
<!--        <tr class="conversion">-->
<!--            <th>Set-Met %</th>-->
<!--            <td>--><?//=$goalSetMetConversion.'%'?><!--</td>-->
<!--            <td>--><?//=$weeklySetMetConversion.'% (Actual)'?><!--</td>-->
<!--            <td class="alert">--><?//=($goalSetMetConversion > $weeklySetMetConversion)? $conversionAlertMessage.' by '.($goalSetMetConversion - $weeklySetMetConversion).'%.' : ''?><!--</td>-->
<!--        </tr>-->
        <tr>
            <th>Appts Set</th>
            <td><?=$goalSetAppointments?></td>
            <td><?=$weeklySumSetAppointments?></td>
            <td><?=(($goalSetAppointments) ? number_format($goalSetAppointments/$leadGenWeeks, 1) : '')?></td>
            <td class="alert"><?=($goalSetAppointments > $weeklySumSetAppointments)? $goalUnitAlertMessage.' by '.($goalSetAppointments - $weeklySumSetAppointments).'.' : ''?></td>
        </tr>
        <tr>
            <th>Contacts</th>
            <td><?=$goalContacts?></td>
            <td><?=$weeklySumContacts?></td>
            <td><?=(($goalContacts) ? number_format($goalContacts/$leadGenWeeks, 1) : '')?></td>
            <td class="alert"><?=($goalContacts > $weeklySumContacts)? $goalUnitAlertMessage.' by '.($goalContacts - $weeklySumContacts).'.' : ''?></td>
        </tr>
        <tr>
            <th>Lead Gen</th>
            <td><?=$goalLeadGen?></td>
            <td><?=$weeklySumLeadGen?></td>
            <td><?=(($goalLeadGen) ? number_format($goalLeadGen/$leadGenWeeks, 1) : '')?></td>
            <td class="alert"><?=($goalLeadGen > $weeklySumLeadGen)? $goalUnitAlertMessage.' by '.($goalLeadGen - $weeklySumLeadGen).'.' : ''?></td>
        </tr>
    </table>
    <br/>
</div>

<?php
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'one-thing-form',
        'enableAjaxValidation'=>false,
    ));

    $this->beginStmPortletContent(array(
        'handleTitle'=>$mode.' Weekly Goal',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-container" >
    <table class="container">
        <tr>
            <th>Week # / Dates</th>
            <th>My One Thing per Week</th>
            <th>Working Days</th>
            <th>Lead Gen Hours</th>
            <th>Contacts</th>
            <th>Appointment Set</th>
            <th>Appointment Met</th>
            <th>Signed</th>
            <th>Contract</th>
            <th>Closing</th>
        </tr>

        <?php foreach($model->collection as $i => $weeklyGoal) { ?>
        <tr>
            <td><?php $dates = $model->weekNumberDates($model->collection[$i]->week_number, $goal->year);
                    echo $model->collection[$i]->week_number.') '.$dates[0].' - '.$dates[1];
                ?>
                <?php echo $form->hiddenField($model->collection[$i], "id",  $htmlOptions=array('value'=>$model->collection[$i]['id'], 'name'=>$model->getFieldCollectionName('id', $i))); ?>
                <?php echo $form->hiddenField($model->collection[$i], "week_number",  $htmlOptions=array('value'=>$model->collection[$i]['week_number'], 'name'=>$model->getFieldCollectionName('week_number', $i))); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "one_thing_quantity", StmFormHelper::getDropDownList(array('start'=>0,'end'=>500, 'increment'=>.1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => 'one-thing-quantity'.Yii::app()->controller->zeroClass($model->collection[$i], 'one_thing_quantity'), 'data-weeknum'=>$i, 'name'=>$model->getFieldCollectionName('one_thing_quantity', $i))); ?>
                <?php echo $form->error($model->collection[$i], "one_thing_quantity"); ?>
                <?php echo $form->dropDownList($model->collection[$i], "one_thing_type", $model->getOneThingTypes(), $htmlOptions=array('empty'=>'Select One', 'style'=>'', 'class'=>'oneThingType', 'name'=>$model->getFieldCollectionName('one_thing_type', $i))); ?>
                <?php echo $form->error($model->collection[$i], "one_thing_type"); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], 'working_days', StmFormHelper::getDropDownList(array('start'=>0,'end'=>7, 'increment'=>1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'working_days'), 'name'=>$model->getFieldCollectionName('working_days', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'working_days'); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "lead_gen_hours", StmFormHelper::getDropDownList(array('start'=>0,'end'=>60, 'increment'=>1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'lead_gen_hours'), 'name'=>$model->getFieldCollectionName('lead_gen_hours', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'lead_gen_hours'); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "contacts", StmFormHelper::getDropDownList(array('start'=>0,'end'=>300, 'increment'=>1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'contacts'), 'name'=>$model->getFieldCollectionName('contacts', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'contacts'); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "appointments_set", StmFormHelper::getDropDownList(array('start'=>0,'end'=>30, 'increment'=>.1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'appointments_set'), 'name'=>$model->getFieldCollectionName('appointments_set', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'appointments_set'); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "appointments_met", StmFormHelper::getDropDownList(array('start'=>0,'end'=>20, 'increment'=>.1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'appointments_met'), 'name'=>$model->getFieldCollectionName('appointments_met', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'appointments_met'); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "signed", StmFormHelper::getDropDownList(array('start'=>0,'end'=>20, 'increment'=>.5,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'signed'), 'name'=>$model->getFieldCollectionName('signed', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'signed'); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "contracts", StmFormHelper::getDropDownList(array('start'=>0,'end'=>20, 'increment'=>.1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'contracts'), 'name'=>$model->getFieldCollectionName('contracts', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'contracts'); ?>
            </td>
            <td><?php echo $form->dropDownList($model->collection[$i], "closings", StmFormHelper::getDropDownList(array('start'=>0,'end'=>20, 'increment'=>.1,'label'=>'','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One', 'class' => Yii::app()->controller->zeroClass($model->collection[$i], 'closings'), 'name'=>$model->getFieldCollectionName('closings', $i))); ?>
                <?php echo $form->error($model->collection[$i], 'closings'); ?>
            </td>
        </tr>
        <? } ?>
    </table>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Weekly Goal</button></div>
<?php $this->endWidget(); ?>