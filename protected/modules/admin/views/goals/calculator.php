<?
$css = <<<CSS
    #income_goal{
        font-size: 50px;
    }
    #goal-comment {
        color: #12C000;
        margin: 20px 0;
        font-size: 30px;
    }
    .error-container {
        display: none;
        width: 98.65%;
        margin: 8px 0px 10px 6px;
        font-size: 35px;
        text-align: center;
        font-weight: bold;
        color: #B20000;
        background: none repeat scroll 0% 0% #f8c2c5;
        border-radius: 4px;
        padding: 2px 5px;
        border: 1px solid #d66767;
        min-width: 35px;
        min-height: 31px;
    }
    .goal-results {
        float: left;
        font-size: 35px;
        font-weight: bold;
        color: #12C000;
        background: none repeat scroll 0% 0% #EFEFEF;
        border-radius: 4px;
        padding: 2px 5px;
        border: 1px solid #AAA;
        min-width: 120px;
        min-height: 50px;
    }
    .assumptions .container tr, .results .container tr {
        height: 70px;
    }

    .assumptions .container tr th{
        width: 35%;
        padding-top: 30px;
    }

    .assumptions .container tr td select{
        width: 25%;
        float: left;
    }

    .results .container tr th{
        width: 75%;
        padding-top: 30px;
    }

CSS;
Yii::app()->clientScript->registerCss('goalCalculatorStyle', $css);

$this->widget('admin_module.extensions.moneymask.MMask',array(
        'element'=>'#income_goal, #fees, #average_sales_price',
        'currency'=>'PHP',
        'config'=>array(
            'precision'=>0,
            'symbol'=>'$',
            'showSymbol'=>true,
            'symbolStay' => true,
        )
    ));

$js = <<<JS
    $('#income_goal, #contract_to_close, #signed_to_contract, #appts_met_to_signed, #appts_set_to_met, #contacts_per_appointment, #contacts_per_hour, #planned_weeks_off').change(function(){
        if($('#calculated').val() == '1') {
            calculateIncome();
        }
    });

    $('form').submit(function(){
        calculateIncome();
        return false;
    });

    function calculateIncome() {
        var calculated = $('#calculated');
        var incomeGoal = $('#income_goal').val().replace(/[^0-9]/g,'');
        var fees = $('#fees').val().replace(/[^0-9]/g,'');
        var averageSalesPrice = $('#average_sales_price').val().replace(/[^0-9]/g, '');

        if(incomeGoal =="") {
            errorMessage('Enter an Income Goal.');
            return false;
        }
        else {
            if(incomeGoal == "" || incomeGoal == '0') {
                errorMessage('Enter a numeric value for Income Goal.');
                return false;
            }
        }

        if(fees =="") {
            $('#fees').val('$0');
            fees = '0';
        }

        var grossIncomeGoal = parseFloat(incomeGoal) + parseFloat(fees);

        if(averageSalesPrice =="" || averageSalesPrice == '0') {
            errorMessage('Enter an Average Sales Price.');
            return false;
        }

        var avgSalesCommission = $('#commission').val();
        var avgNetCommission = averageSalesPrice * avgSalesCommission * $('#split').val();
        var closingsGoal = Math.ceil(grossIncomeGoal / avgNetCommission);
        $('span.closings').html(closingsGoal);

        var contractsWritten = Math.ceil(closingsGoal / $('#contract_to_close').val());
        $('span.contracts').html(contractsWritten);

        var signed = Math.ceil(contractsWritten / $('#signed_to_contract').val());
        $('span.signed-agreements').html(signed);

        var met = Math.ceil(signed / $('#appts_met_to_signed').val());
        $('span.appointments-met').html(met);

        var setAppointments = Math.ceil(met / $('#appts_set_to_met').val());
        $('span.appointments-set').html(setAppointments);

        var contacts = Math.ceil(setAppointments * $('#contacts_per_appointment').val());
        $('span.contacts').html(contacts);

        var leadgenHours = Math.ceil(contacts / parseFloat($('#contacts_per_hour').val()));
        $('span.leadgen-hours').html(leadgenHours);

        var daysWorked = (52-parseFloat($('#planned_weeks_off').val())) * 4; // assume 4 lead gen days per work week
        $('span.working-days').html(daysWorked);

        var leadGenHoursPerDay = Math.ceil(leadgenHours / daysWorked);
        $('span.leadgen-hours-per-day').html(leadGenHoursPerDay);

        var contactsPerDay = Math.ceil(contacts / daysWorked);
        $('span.contacts-per-day').html(contactsPerDay);

        var appointmentsPerDay = Math.ceil( setAppointments / daysWorked );
        $('span.appointments-per-day').html(appointmentsPerDay);

        var grossIncomeGoalFormatted = grossIncomeGoal.toString().substring(0,3) + ',' + grossIncomeGoal.toString().substring(3,6);



        $('.error-container').hide('normal').val('');
        if(calculated.val() !== '1') {
            $('.goal-results').hide().show('normal');
            $('#goal-comment').html('To Net '+ $('#income_goal').val() +', Gross $'+ grossIncomeGoalFormatted +' by doing the following...').hide().show('normal');
        }
        else {
                $('#goal-comment').html('To Net '+ $('#income_goal').val() +', Gross $'+ grossIncomeGoalFormatted +' by doing the following...');
        }
        calculated.val(1);
        Message.create('success','Goals have been successfully calculated. See Key Indicators below...');
    }

    function errorMessage(message) {
        $('.error-container').html(message).show('normal');
    }
JS;
Yii::app()->clientScript->registerScript('goalcalculatorScript', $js);


$this->breadcrumbs=array(
	'Income Calculator'=>''
);
?>
    <div id="listview-actions">
        <a href="/admin/goals" class="button gray icon i_stm_search">View Goals</a>
    </div>

<?
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'goals-form',
        'enableAjaxValidation'=>false,
    ));

$this->beginStmPortletContent(array(
        'handleTitle'=>'Income Calculator',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-calculator-container" >
    <div class="error-container">
    </div>
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <table class="container">
            <tr>
                <th style="padding-top: 20px;">Annual Net Income Goal:</th>
                <td><?php echo Chtml::textField('income_goal', 100000, $htmlOptions=array('placeholder'=>'Income Goal','class'=>'g5')); ?></td>
            </tr>
        </table>

        <hr/>
        <h3>Conversion Assumptions</h3>
        <div class="g6">
            <table class="container">
                <tr>
                    <th>Misc Fees / Cost of Sale:</th>
                    <td><?php echo Chtml::textField('fees', '0', $htmlOptions=array('placeholder'=>'Misc Fees / Cost of Sale','class'=>'g6')); ?></td>
                </tr>

                <tr>
                    <th>Average Sales Price:</th>
                    <td><?php echo Chtml::textField('average_sales_price', 200000, $htmlOptions=array('placeholder'=>'Avg Sales Price $200,000', 'class'=>'g6')); ?></td>
                </tr>
            </table>
        </div>
        <div class="g6">
            <table class="container">
                <tr>
                    <th>Average Sales Commission:</th>
                    <td><?php echo Chtml::dropDownList('commission', .03, StmFormHelper::getPercentageList(array('max'=>10,'sort'=>'asc')), $htmlOptions=array('placeholder'=>'Average Commission','class'=>'g2')); ?></td>
                </tr>
                <tr>
                    <th>Average Split:</th>
                    <td><?php echo Chtml::dropDownList('split', "0.50", StmFormHelper::getPercentageList(array('sort'=>'asc')), $htmlOptions=array('placeholder'=>'Average Split','class'=>'g2')); ?></td>
                </tr>
            </table>
        </div>

        <hr/>
        <h3>Goal Key Performance Indicators</h3>
        <h2 id="goal-comment">&nbsp;</h2>

        <div class="g6 results">
            <table class="container">
                <tr>
                    <th>Closings:</th>
                    <td><span class="goal-results closings"></span></td>
                </tr>
                <tr>
                    <th>Contracts Written:</th>
                    <td><span class="goal-results contracts"></span></td>
                </tr>
                <tr>
                    <th>Signed Agreements:</th>
                    <td><span class="goal-results signed-agreements"></span></td>
                </tr>
                <tr>
                    <th>Appointments Met:</th>
                    <td><span class="goal-results appointments-met"></span></td>
                </tr>
                <tr>
                    <th>Appointments Set:</th>
                    <td><span class="goal-results appointments-set"></span></td>
                </tr>
                <tr>
                    <th>Contacts:</th>
                    <td><span class="goal-results contacts"></span></td>
                </tr>
                <tr>
                    <th>Lead Gen Hours:</th>
                    <td><span class="goal-results leadgen-hours"></span></td>
                </tr>
                <tr>
                    <th>Lead Gen Hours per Working Day:</th>
                    <td><span class="goal-results leadgen-hours-per-day"></span></td>
                </tr>
                <tr>
                    <th>Contacts per Working Day:</th>
                    <td><span class="goal-results contacts-per-day"></span></td>
                </tr>
                <tr>
                    <th>Appointments per Working Day:</th>
                    <td><span class="goal-results appointments-per-day"></span></td>
                </tr>
            </table>
        </div>

        <div class="g6 assumptions">
            <table class="container">
                <tr>
                    <th>Closings @ Contract to Close:</th>
                    <td><?php echo Chtml::dropDownList('contract_to_close', "0.90", StmFormHelper::getPercentageList(array('sort'=>'asc')), $htmlOptions=array('placeholder'=>'Contract to Close','class'=>'')); ?></td>
                </tr>
                <tr>
                    <th>Contracts @ Agreement Signed to Contract:</th>
                    <td><?php echo Chtml::dropDownList('signed_to_contract', "0.80", StmFormHelper::getPercentageList(array('sort'=>'asc')), $htmlOptions=array('placeholder'=>'Signed to Contract','class'=>'')); ?></td>
                </tr>
                <tr>
                    <th>Signed # Appointment Met to Signed:</th>
                    <td><?php echo Chtml::dropDownList('appts_met_to_signed', "0.70", StmFormHelper::getPercentageList(array('sort'=>'asc')), $htmlOptions=array('placeholder'=>'Appts Set to Signedt','class'=>'')); ?></td>
                </tr>
                <tr>
                    <th>Met @ Appointment Set to Met:</th>
                    <td><?php echo Chtml::dropDownList('appts_set_to_met', "0.50", StmFormHelper::getPercentageList(array('sort'=>'asc')), $htmlOptions=array('placeholder'=>'Appts Met to Set','class'=>'')); ?></td>
                </tr>
                <tr>
                    <th>Appts @ Contacts per Appointment Set:</th>
                    <td><?php echo Chtml::dropDownList('contacts_per_appointment', "20", StmFormHelper::getDropDownList(array('start'=>1,'end'=>100,'increment'=>1)), $htmlOptions=array('placeholder'=>'Contacts per Appt','class'=>'')); ?>
                        <div class="label p-clr p-fl">(Top Converter = 3, Average = 8, Beginner = 25)</div>
                    </td>
                </tr>
                <tr>
                    <th>Contacts per Hour:</th>
                    <td><?php echo Chtml::dropDownList('contacts_per_hour', "5", StmFormHelper::getDropDownList(array('start'=>1,'end'=>50,'increment'=>1)), $htmlOptions=array('placeholder'=>'Contacts per Hour','class'=>'')); ?>
                        <div class="label p-clr p-fl">(Contact = A real estate conversation generating future business)</div>
                    </td>
                </tr>
                <tr>
                    <th>Planned Weeks Off:</th>
                    <td><?php echo Chtml::dropDownList('planned_weeks_off', "9", StmFormHelper::getDropDownList(array('start'=>1,'end'=>52,'increment'=>1)), $htmlOptions=array('placeholder'=>'Contract to Close','class'=>'')); ?> <div class="label p-clr p-fl"><u>9 weeks Recommended</u>
                            <br>3 weeks: Vacation
                            <br>2 weeks: Christmas/Thanksgiving
                            <br>2 weeks: Education / Training
                            <br>1 weeks: Other Holidays
                            <br>1 week: Sick/Personal/Misc
                        </div></td>
                </tr>
                <tr>
                    <th>Working Days:</th>
                    <td><span class="goal-results working-days"></span></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<? echo CHtml::hiddenField('calculated', '0')?>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper"><button type="submit" class="submit wide">Calculate Now</button></div>
<?php $this->endWidget(); ?>