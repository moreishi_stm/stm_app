<?
// Files needed for Sales graph
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');
if($thisYearGoalExists && $thisYearWeeklyGoalExists):

    //turn all kpi data into variables so it can be accessed by the chart js block below
    foreach($kpiData as $kpiType => $kpiDetails) {

        foreach($kpiDetails as $kpiUnit => $value) {
            $varName = 'kpi'.$kpiType.$kpiUnit; //ex. var name = $kpiclosingsytdPercent
            $$varName = $value;
        }
    }

    if($yearlyOneThingPerformanceData) {

    }

$js = <<<JS
//        $('.chart').wl_Chart({
//            flot: {
//                lines:{
////                    fill:true,
//                    fillColor: {
//                        colors: [ { opacity: .2 }, { opacity: .7 } ]
//                    }
//                },
//                // colors: ["#555555","#18CC18","#FFAB4A"], //blue 528EFF
//                yaxis: {
//                    tickDecimals: 0,
//                    max: 120,
//                    tickFormatter: function (val, axis) {
//                        return val + "%";
//                    }
//                },
////                tooltip : true,
////                tooltipOpts : {
////    //                content : "Your sales for <b>%x</b> was <span>$%y</span>",
////                    content : "%y",
////                    defaultTheme : false
////                },
//                legend: {position: "nw", noColumns: 5}
//            },
//            xlabels:$xLabels, // ['1','2','3']
//            data:[
//                {
//                    label:'% Year-to-Date: $oneThingType',
//                    lines: {fill: true},
//                    color: "#18CC18",
//                    data: $yearlyOneThingPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
//                },
//                {
//                    //this 2nd row Goal values add on top of the Actual, so need to represent the difference between the two
//                    label:'Goal Line',
//                    lines: {fill: false, lineWidth: 1.5},
//                    color: "#555555",
//                    points: { radius: 0 },
//                    data: $goalData //[[0,1],[1,1],[2,1],[3,0],[4,1],[5,1],[6,0],[7,0],[8,1],[9,1],[10,0],[11,2]]
//                },
//    //            {
//    //                label:'% Daily Performance',
//    //                lines: {fill: false, zero:true},
//    //                color: "#FFEE00",//"#528EFF", //FFAB4A
//    //                data: $dailyPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
//    //            },
//                {
//                    label:'% Monthly Appts Set',
//                    lines: {fill: false, zero:true},
//                    color: "#FFEE00",//"#528EFF", //FFAB4A
//                    data: $monthlyPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
//                },
//    //            {
//    //                label:'% Weekly Performance',
//    //                lines: {fill: false},
//    //                color: "#528EFF",
//    //                data: $weeklyPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
//    //            },
//                {
//                    label:'% One Thing: $oneThingType',
//    //                lines: {fill: false, zero:true},
//                    lines: {fill: true},
//                    color: "#528EFF", //CD52FF
//                    data: $monthlyOneThingPerformanceData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]
//                }
//            ]
//        });
//
//    	$(window).resize(function() {
//    	    var newWidth = $('#graph-container').width() - 5;
//    		$('.chart').width(newWidth);
//    	});


       $('.chart2').wl_Chart({
            flot: {
                bars:{
                    align: "center",
                    lineWidth: 0,
                    fillColor: {
                        colors: [ { opacity: .7 }, { opacity: .7 } ]
                    }
                },
                yaxis: {
                    tickDecimals: 0,
                    max: 120,
                    axisLabel: '%',
                    tickFormatter: function (val, axis) {
                        return val + "%";
                    }
                },
                xaxis: {
//                    axisLabelPadding: 5,
                    tickLength: 0,
//                    axisLabelFontSizePixels: 25,
//                    axisLabelUseCanvas: true,
                    ticks:[
                        [0,'Closings<br>$kpiclosingsytdPercent% ($kpiclosingsytdActual of $kpiclosingsytdGoal)'],
                        [1,'Contracts<br>$kpicontractsytdPercent% ($kpicontractsytdActual of $kpicontractsytdGoal)'],
                        [2,'Signed<br>$kpisignedytdPercent% ($kpisignedytdActual of $kpisignedytdGoal)'],
                        [3,'Met Appts<br>$kpimetytdPercent% ($kpimetytdActual of $kpimetytdGoal)'],
                        [4,'Set Appts<br>$kpisetytdPercent% ($kpisetytdActual of $kpisetytdGoal)'],
                        [5,'Contacts<br>$kpicontactsytdPercent% ($kpicontactsytdActual of $kpicontactsytdGoal)'],
                        [6,'Lead Gen<br>$kpileadgenytdPercent% ($kpileadgenytdActual of $kpileadgenytdGoal)']
                    ]
                },
                legend: {position: "nw", noColumns: 7}
            },
            //colors: ["#FF0000","#FFFF00","#00FF00"],
            type: 'bars',
                    barWidth: 0.8,
            // xlabels: [""],//$xLabels, // ['1','2','3']
            data:[
                {
                    color: "#528EFF",//"#528EFF", //FFAB4A
                    data: [
                        [0,$kpiclosingsytdPercent],
                        [1,$kpicontractsytdPercent],
                        [2,$kpisignedytdPercent],
                        [3,$kpimetytdPercent],
                        [4,$kpisetytdPercent],
                        [5,$kpicontactsytdPercent],
                        [6,$kpileadgenytdPercent]
                    ]
                }
//                {
//                    label:'Closings $kpiclosingsytdPercent% ($kpiclosingsytdActual of $kpiclosingsytdGoal)',
//                    data: [[0,$kpiclosingsytdPercent]]
//                },
//                {
//                    label:'Signed $kpisignedytdPercent% ($kpisignedytdActual of $kpisignedytdGoal)',
//                    data: [[0,$kpisignedytdPercent]]
//                },
//                {
//                    label:'Met Appts $kpimetytdPercent% ($kpimetytdActual of $kpimetytdGoal)',
//                    data: [[0,$kpimetytdPercent]]
//                },
//                {
//                    label:'Set Appts $kpisetytdPercent% ($kpisetytdActual of $kpisetytdGoal)',
//                    data: [[0,$kpisetytdPercent]]
//                },
//                {
//                    label:'Spoke to $kpicontactsytdPercent% ($kpicontactsytdActual of $kpicontactsytdGoal)',
//                    data: [[0,$kpicontactsytdPercent]]
//                },
//                {
//                    label:'Lead Gen Hours $kpileadgenytdPercent% ($kpileadgenytdActual of $kpileadgenytdGoal)',
//                    data: [[0,$kpileadgenytdPercent]]
//                }
            ]
        });

JS;
// example in _sales.php
Yii::app()->clientScript->registerScript('oneThingGraph', $js);
endif;

Yii::app()->clientScript->registerCss('listOneThingCss', <<<CSS
    #goal-units .alert {
        font-size: 14px;
        font-weight: bold;
        color: #D20000;
    }
    tr.conversion th, tr.conversion td{
        background-image: none;
        background: #d0e6ff;
    }
CSS
);

$this->breadcrumbs=array(
	'One Thing'=>'/admin/goals/oneThing'
);

Yii::app()->clientScript->registerScript('search', <<<JS
    $('#listview-search form').change(function() {
        $('body').prepend('<div class=\"loading-container loading\"><em></em></div>');
        window.location = '/admin/goals/oneThing/' + $('#GoalsOneThing_contact_id').val();
    });

    $('#listview-search form').submit(function() {
        $('body').prepend('<div class=\"loading-container loading\"><em></em></div>');
        window.location = '/admin/goals/oneThing/' + $('#GoalsOneThing_contact_id').val();
    });
JS
);
?>
<div id="listview-actions">
    <a href="/admin/goals/addOneThing" class="button gray icon i_stm_add">Add One Thing</a>
    <?if($goal->hasWeeklyGoals):?>
        <a href="/admin/goals/editWeekly/<?=$goal->id?>" class="button gray icon i_stm_edit">Edit Weekly One Thing</a>
    <? else: ?>
    <a href="/admin/goals/addWeekly/<?=$goal->id?>" class="button gray icon i_stm_add">Add Weekly One Thing</a>
    <? endif;?>
    <a href="/admin/goals/calculator" class="button gray icon i_stm_edit">Calculator</a>
    <a href="/admin/goals<?=(Yii::app()->user->id == $model->contact_id)? '' : '/list/'.$model->contact_id;?>" class="button gray icon i_stm_search">Goals</a>
</div>
<div id="content-header">
	<h1>Goals List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'one-thing-list-search',
        )); ?>
    <div class="g4"></div>
    <div class="g3">
        <label class="g2">Agent:</label>
        <span class="g10"><?php
            echo $form->dropDownList($model,'contact_id', CMap::mergeArray(array(''=>'Everyone'), CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName')), $htmlOptions=array('class'=>'g12'));
            $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => '#GoalsOneThing_contact_id')); ?></span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div>
<?if($goal->hasWeeklyGoals):?>
    <h1>One Thing: <span style="color: #528EFF; text-decoration: underline; font-size: 30px; font-weight: bold;"><?=$oneThingQuantity.' '.$oneThingUnit?> per Week</span></h1>
    <br/>
    <h2>Month-to-Date: <?=$mtdGoalResults?> of <?=$mtdGoalNumber?> = <?=$mtdGoalPercent?>%</h2>
    <h2>Year-to-Date: <?=$ytdGoalResults?> of <?=$ytdGoalNumber?> = <?=$ytdGoalPercent?>%</h2>
    <br/>
    <br/>
    <h1>Projected Income: <span style="color: #18CC18; text-decoration: underline; font-size: 30px; font-weight: bold;">$<?=number_format($goal->income_per_year * $ytdGoalPercent / 100) ?></span> @ <?=$ytdGoalPercent?>% YTD</h1>
    <br/>
<? endif; ?>

<?php if(!empty($oneThingDailyMissingDaysString)): ?>
    <h1 style="line-height: 1.2; color: #D20000;">
        One Thing entries for the following days are missing.<br>
        <?=$oneThingDailyMissingDaysString?><br/><br/>
        Please add the records to continue.<br>
    </h1>
<? endif; ?>
<?php if($thisYearGoalExists && $thisYearWeeklyGoalExists): //&& empty($oneThingDailyMissingDaysString) ?>
    <div id="graph-container" style="padding: 10px; clear: both; width: 100%;">
        <h1><?=date('Y')?> Key Performance Indicators</h1>
        <table id="oneThing-graph2" class="chart2"   data-height="400" data-tooltip="false" width="75%"></table>
        <h2><br></h2>
<!--        <table id="oneThing-graph" class="chart"   data-height="300" data-stack="false" data-tooltip="false" width="75%"></table>-->
    </div>
<?php else: ?>
    <h1 style="line-height: 1.2; color: #D20000;">
        Goal data is missing for this year and/or week.<br>
        Please enter those records and return to this page to see your graph.<br>
        <a href="/admin/goals<?php echo (Yii::app()->user->id !== $model->contact_id && Yii::app()->user->checkAccess('owner'))? '/list/'.$model->contact_id : '';?>">Click here to view your Goal(s).</a>
    </h1>
<?php endif; ?>
<? $goalUnitAlertMessage = 'One Thing Weekly is lower than Annual goal';
    $conversionAlertMessage = 'Actual Conversion is lower than Annual goal';
?>
    <table id="goal-units" style="width: 70%; margin-left: 15%; margin-right: 15%; margin-top: 20px; margin-bottom: 20px; clear: both;">
        <tr>
            <th></th>
            <th>per Annual Goal</th>
            <th>per Weekly Goal</th>
            <th>Difference</th>
        </tr>
        <tr>
            <th>Closings</th>
            <td><?=$goalClosings?></td>
            <td><?=$weeklySumClosings?></td>
            <td class="alert"><?=($goalClosings > $weeklySumClosings)? $goalUnitAlertMessage.' by '.($goalClosings - $weeklySumClosings).'.' : ''?></td>
        </tr>
        <tr class="conversion">
            <th>Contracts-Close %</th>
            <td><?=$goalContractCloseConversion.'%'?></td>
            <td><?=$actualContractCloseConversion.'% (Actual)'?></td>
            <td class="alert"><?=($goalContractCloseConversion > $actualContractCloseConversion)? $conversionAlertMessage.' by '.($goalContractCloseConversion - $actualContractCloseConversion).'%.' : ''?></td>
        </tr>
        <tr>
            <th>Contracts</th>
            <td><?=$goalContracts?></td>
            <td><?=$weeklySumContracts?></td>
            <td class="alert"><?=($goalContracts > $weeklySumContracts)? $goalUnitAlertMessage.' by '.($goalContracts - $weeklySumContracts).'.' : ''?></td>
        </tr>
        <tr class="conversion">
            <th>Signed-Contract %</th>
            <td><?=$goalSignedContractConversion.'%'?></td>
            <td><?=$actualSignedContractConversion.'% (Actual)'?></td>
            <td class="alert"><?=($goalSignedContractConversion > $actualSignedContractConversion)? $conversionAlertMessage.' by '.($goalSignedContractConversion - $actualSignedContractConversion).'%.' : ''?></td>
        </tr>
        <tr>
            <th>Signed</th>
            <td><?=$goalAgreements?></td>
            <td><?=$weeklySumAgreements?></td>
            <td class="alert"><?=($goalAgreements > $weeklySumAgreements)? $goalUnitAlertMessage.' by '.($goalAgreements - $weeklySumAgreements).'.' : ''?></td>
        </tr>
        <tr class="conversion">
            <th>Met-Signed %</th>
            <td><?=$goalMetSignedConversion.'%'?></td>
            <td><?=$actualMetSignedConversion.'% (Actual)'?></td>
            <td class="alert"><?=($goalMetSignedConversion > $actualMetSignedConversion)? $conversionAlertMessage.' by '.($goalMetSignedConversion - $actualMetSignedConversion).'%.' : ''?></td>
        </tr>
        <tr>
            <th>Appts Met</th>
            <td><?=$goalMetAppointments?></td>
            <td><?=$weeklySumMetAppointments?></td>
            <td class="alert"><?=($goalMetAppointments > $weeklySumMetAppointments)? $goalUnitAlertMessage.' by '.($goalMetAppointments - $weeklySumMetAppointments).'.' : ''?></td>
        </tr>
        <tr class="conversion">
            <th>Set-Met %</th>
            <td><?=$goalSetMetConversion.'%'?></td>
            <td><?=$actualSetMetConversion.'% (Actual)'?></td>
            <td class="alert"><?=($goalSetMetConversion > $actualSetMetConversion)? $conversionAlertMessage.' by '.($goalSetMetConversion - $actualSetMetConversion).'%.' : ''?></td>
        </tr>
        <tr>
            <th>Appts Set</th>
            <td><?=$goalSetAppointments?></td>
            <td><?=$weeklySumSetAppointments?></td>
            <td class="alert"><?=($goalSetAppointments > $weeklySumSetAppointments)? $goalUnitAlertMessage.' by '.($goalSetAppointments - $weeklySumSetAppointments).'.' : ''?></td>
        </tr>
        <tr class="conversion">
            <th>Contacts-Appt</th>
            <td><?=$goalContactsApptConversion.''?></td>
            <td><?=$actualContactsApptConversion.' (Actual)'?></td>
            <td class="alert"><?=($goalContactsApptConversion < $actualContactsApptConversion)? $conversionAlertMessage.' by '.($actualContactsApptConversion - $goalContactsApptConversion).'.' : ''?></td>
        </tr>
        <tr>
            <th>Contacts</th>
            <td><?=$goalContacts?></td>
            <td><?=$weeklySumContacts?></td>
            <td class="alert"><?=($goalContacts > $weeklySumContacts)? $goalUnitAlertMessage.' by '.($goalContacts - $weeklySumContacts).'.' : ''?></td>
        </tr>
        <tr>
            <th>Lead Gen</th>
            <td><?=$goalLeadGen?></td>
            <td><?=$weeklySumLeadGen?></td>
            <td class="alert"><?=($goalLeadGen > $weeklySumLeadGen)? $goalUnitAlertMessage.' by '.($goalLeadGen - $weeklySumLeadGen).'.' : ''?></td>
        </tr>
    </table>

<?php
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'one-thing-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
        array(
            'type'=>'raw',
            'name'=>'Date',
            'value'=>'Yii::app()->format->formatDate($data->date)',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Name',
            'value'=>'$data->contact->fullName',
            'htmlOptions'=>array('style'=>'width:180px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Today\'s Goals Accomplished %',
            'value'=>'$data->percent_daily_goal_accomplished."%"',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Week\'s Goals Accomplished %',
            'value'=>'$data->percent_weekly_goal_accomplished."%"',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'Goal Description',
            'value'=>'$data->goal_description',
        ),
        array(
            'type'=>'raw',
            'name'=>'Result Description',
            'value'=>'$data->results_description',
        ),
        array(
            'type'=>'raw',
            'name'=>'Challenges',
            'value'=>'$data->challenges',
        ),
        array(
            'type'=>'raw',
            'name'=>'Tomorrow Must Do',
            'value'=>'$data->tomorrow_must_do',
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'"<div><a href=\"/admin/goals/editOneThing/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
            'htmlOptions'=>array('style'=>'width:120px'),
        ),
	),
));
?>