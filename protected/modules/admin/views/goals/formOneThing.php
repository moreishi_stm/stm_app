<?
$this->breadcrumbs=array(
	'One Thing'=>''
);
?>
<div id="listview-actions">
    <a href="/admin/goals/calculator" class="button gray icon i_stm_edit">Calculator</a>
	<a href="/admin/goals/add" class="button gray icon i_stm_add">Add New Goal</a>
    <a href="/admin/goals/oneThing" class="button gray icon i_stm_search">One Thing</a>
</div>
<div id="content-header">
	<h1>One Thing</h1>
</div>

<?php
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'one-thing-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=>$mode.' One Thing',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-container" >
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <div class="g3"></div>
        <div id="goals-left" class="g6">
            <table class="container">
                <tr>
                    <th><?php echo $form->labelEx($model, 'date');?></th>
                    <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
                                'attribute'=>'date',
                                'options'=>array(
                                    'showAnim'=>'fold',
                                ),
                                'htmlOptions'=>array(
                                    'placeholder'=>'Date',
                                    'class'=>'g6',
                                    'readonly'=>'readonly',
                                ),
                            )); ?>
                        <?php echo $form->error($model,'date'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'goal_description');?></th>
                    <td><?php echo $form->textField($model, 'goal_description', $htmlOptions=array('empty'=>'Select One', 'placeholder'=>'My One Thing')); ?>
                        <?php echo $form->error($model,'goal_description'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'percent_daily_goal_accomplished');?></th>
                    <td><?php echo $form->dropDownList($model, 'percent_daily_goal_accomplished', StmFormHelper::getDropDownList(array('start'=>0,'end'=>500,'increment'=>1,'label'=>'%','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'percent_daily_goal_accomplished'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'percent_weekly_goal_accomplished');?></th>
                    <td><?php echo $form->dropDownList($model, 'percent_weekly_goal_accomplished', StmFormHelper::getDropDownList(array('start'=>0,'end'=>500,'increment'=>1,'label'=>'%','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'percent_weekly_goal_accomplished'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'percent_monthly_goal_accomplished');?></th>
                    <td><?php echo $form->dropDownList($model, 'percent_monthly_goal_accomplished', StmFormHelper::getDropDownList(array('start'=>0,'end'=>500,'increment'=>1,'label'=>'%','placeholder'=>'')), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'percent_monthly_goal_accomplished'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'results_description');?></th>
                    <td><?php echo $form->textArea($model, 'results_description', $htmlOptions=array('rows'=>'12')); ?>
                        <?php echo $form->error($model,'results_description'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'challenges');?></th>
                    <td><?php echo $form->textArea($model, 'challenges', $htmlOptions=array('rows'=>'3')); ?>
                        <?php echo $form->error($model,'challenges'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'tomorrow_must_do');?></th>
                    <td><?php echo $form->textArea($model, 'tomorrow_must_do', $htmlOptions=array('empty'=>'Select One', 'rows'=>'3')); ?>
                        <?php echo $form->error($model,'tomorrow_must_do'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="g3"></div>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit One Thing</button></div>
<?php $this->endWidget(); ?>
