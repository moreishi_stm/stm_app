<?
$this->breadcrumbs=array(
	'List'=>''
);
    if(Yii::app()->user->checkAccess('owner')):
    Yii::app()->clientScript->registerScript('search', <<<JS
        $('#listview-search form').change(function() {
            $('#listview-search form').submit();
        });

        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("goals-grid", {
                data: $(this).serialize()
            });
            return false;
        });
JS
    );
endif;
?>
<div id="listview-actions">
    <a href="/admin/goals/calculator" class="button gray icon i_stm_edit">Calculator</a>
	<a href="/admin/goals/add" class="button gray icon i_stm_add">Add New Goal</a>
    <a href="/admin/goals/oneThing" class="button gray icon i_stm_search">View One Thing</a>
    <a href="/admin/goals/addOneThing" class="button gray icon i_stm_add">Add One Thing</a>
</div>
<div id="content-header">
	<h1>Goals List</h1>
</div>

<?php if(Yii::app()->user->checkAccess('owner')): ?>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'goals-list-search',
        )); ?>
    <div class="g4"></div>
    <div class="g3">
        <label class="g2">Agent:</label>
    <span class="g10"><?php
        echo $form->dropDownList($model,'contact_id', CHtml::listData(Contacts::model()->byActiveAdmins()->orderByName()->findAll(), 'id', 'fullName'), $htmlOptions=array('class'=>'g12'));
        $this->widget('admin_module.extensions.EChosen.EChosen', array('target' => '#Goals_contact_id')); ?></span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div>
<?endif;?>
<?php
$this->widget('admin_module.components.StmGridView', array(
	'id'=>'goals-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		array(
			'type'=>'raw',
			'name'=>'Status',
			'value'=>'StmFormHelper::getStatusBooleanName($data->status_ma)',
			),
        'contact.fullName',
		'name',
		'year',
		array(
			'type'=>'raw',
			'name'=>'Avg Sales Price',
			'value'=>'Yii::app()->format->formatDollars($data->average_sales_price)',
			),
		array(
			'type'=>'raw',
			'name'=>'Annual Closings',
			'value'=>'$data->annualClosings',
			),
		array(
			'type'=>'raw',
			'name'=>'Annual Income',
			'value'=>'Yii::app()->format->formatDollars($data->income_per_year)',
			),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/admin/goals/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:80px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'($data->hasWeeklyGoals)? "<div><a href=\"/admin/goals/editWeekly/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit Weekly Goals</a></div>" :
				"<div><a href=\"/admin/goals/addWeekly/".$data->id."\" class=\"button gray icon i_stm_add grey-button\">Add Weekly Goals</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:160px'),
        ),
	),
));
?>