<?php
$css = <<<CSS
    td.clear-formatting{
        background-color: white;
        font-family: Arial, Helvetica, sans-serif;
        padding: 30px;
        color: black;
        font-size: 13px;
    }
    tr:hover td.clear-formatting strong {
        color: inherit !important;
    }
    td.clear-formatting p, td.clear-formatting ul {
    line-height: inherit;
}
    td.clear-formatting ul {
        margin-top: 1em !important;
        margin-bottom: 1em !important;
    }

	p {
		margin-bottom: 18px;
	}
    span {
        font-family: inherit !important;
    }
    .email-view h1, .email-view h2, .email-view h3, .email-view h4, .email-view h5, .email-view h6{
        text-align: left;
    }
    .email-view table {
        background-image: none;
        float: none;
    }
    .email-view td {
        text-align: inherit;
        border-color: transparent;
    }
    .email-view tr:hover td strong {
        color: inherit !important;
    }
    .email-view p {
        margin-bottom: 1em;
        font-size: 100%;
        font-style: inherit;
        margin-top: 1em;
    }
    .email-view{
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		font-size: 14px;
		font-style: normal;
		font-variant: normal;
		font-weight: 400;
		line-height: 20px;
	}

	.email-view h1 {
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		font-size: 24px;
		font-style: normal;
		font-variant: normal;
		font-weight: 500;
		line-height: 26.4px;
		float:left;
		text-align: left !important;
	}
	.email-view h3 {
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		font-size: 14px;
		font-style: normal;
		font-variant: normal;
		font-weight: 500;
		line-height: 15.4px;
		float:left;
	}
	.email-view p {
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		font-size: 14px;
		font-style: normal;
		font-variant: normal;
		font-weight: 400;
		line-height: 20px;
	}
	.email-view blockquote {
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		font-size: 21px;
		font-style: normal;
		font-variant: normal;
		font-weight: 400;
		line-height: 30px;
		float:left;
	}
	.email-view img{
		border: none;
		margin:0;
	}
	.email-view a{
		color: #0000EE;
		text-decoration: underline;
	}
	.email-view a:hover{
		text-decoration: none;
	}
CSS;
Yii::app()->clientScript->registerCss('editEmailTemplate', $css);

$this->breadcrumbs=array(
	$model->description=>'',
);
$this->beginStmPortletContent(array(
	'handleTitle'=>'Email Template :: '.$model->description,
	'handleIconCss'=>'i_wizard',
	'handleButtons'    => array(
		array(
			'label'        =>'Edit',
			'iconCssClass' =>'i_stm_edit',
			'htmlOptions'  =>array('id'=>'edit-contact-button'),
			'name'         =>'edit-contact',
			'type'         =>'link',
			'link'		   =>'/'.Yii::app()->controller->module->name.'/emailTemplates/edit/'.$model->id,
		)
	),
));
?>
<div id="email-templates-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container p-mt10">
			<tr>
				<th>Status:</th>
				<td><?php echo StmFormHelper::getStatusBooleanName($model->status_ma)?><label class="label" style="padding-left: 100px;">Type:</label><?php echo $model->componentType->display_name?></td>
			</tr>
			<tr>
				<th>Description:</th>
				<td><?php echo $model->description;?></td>
			</tr>
		</table>
	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<table style="width: 900px; float: none; clear: both; margin: 0 auto;">
    <tr>
        <th colspan="2"><h2>Email Message</h2></th>
    </tr>
    <tr>
        <th class="">Subject:</th>
        <td><h2 style="text-align: left;"><?php echo $model->subject;?></h2></td>
    </tr>
    <tr>
        <th>Message:</th>
        <td class="email-view clear-formatting"><?php echo $model->body;?></td>
    </tr>
    <tr>
        <th>Attachments:</th>
        <td>
            <ul>
                <?foreach($model->attachments as $attachment) : ?>
                <li><?=$attachment->name?></li>
                <?endforeach?>
            </ul>
        </td>
    </tr>
</table>
<style type="text/css">

</style>