<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->name.'/emailTemplates'),
);

?>
	<div id="listview-actions">
		<a href="/<?php echo Yii::app()->controller->module->name;?>/emailTemplates/add" class="button gray icon i_stm_add">Add New Email Template</a>
		<!-- <a href="#" class="btnGray button i_stm_search icon">Search Contacts</a> -->
	</div>
	<div id="content-header">
		<h1>Email Template List</h1>
	</div>

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php
    $module = Yii::app()->controller->module->name;
    Yii::app()->clientScript->registerScript('searchEmailTemplates', <<<JS
    $('form .reset-button').click(function() {

        var componentTypeIdElement = '#EmailTemplates_component_type_id option[value="' + $('#EmailTemplates_component_type_id').val() + '"]';
        $(componentTypeIdElement).removeAttr('selected');
        $('#EmailTemplates_searchContent').val('');
        $('#EmailTemplates_body').val('');
        $('form#email-templates-list-search').submit();
    });

    $('#listview-search form select').change(function() {
        $(this).submit();
    });

    $('#listview-search form').submit(function() {
        $.fn.yiiGridView.update('email-templates-grid', { data: $(this).serialize() });
        return false;
    });

    $( '.delete-email-template-button' ).live( "click", function() {
        if(confirm('Are you sure you want to delete this Email Template? This will set the status to Inactive and you can recover it by using the Status filter.')) {
            $("body").prepend("<div class='loading-container loading'><em></em></div>");
            var id = $(this).data('id');
            $.post('/$module/emailTemplates/delete/'+id, function(data) {
                $("div.loading-container.loading").remove(); console.log(data);
                if(data.status=='success') {
                    Message.create("success","Email Template deleted successfully.");
                    $.fn.yiiGridView.update("email-templates-grid", { data: $(this).serialize() });
                } else {
                    Message.create("error","Error: Email Template did not delete.");
                }
            },"json");
        }
    });
JS
        );

        $form=$this->beginWidget('CActiveForm', array(
                'action'=>Yii::app()->createUrl($this->route),
                'method'=>'get',
                'id'=>'email-templates-list-search',
            )); ?>
        <div class="g2">
            <label class="g5">Status:</label>
		<span class="g7">
    		<?php echo $form->dropDownList($model, 'status_ma', array(1=>'Active', 0=>'Inactive'), $htmlOptions=array('empty'=>'Select One')); ?>
		</span>
        </div>
        <div class="g2">
            <label class="g5">Category:</label>
		<span class="g7">
    		<?php
            $criteria = new CDbCriteria();
            if(!Yii::app()->user->checkAccess('transactions')) {
                $criteria->compare('id',ComponentTypes::RECRUITS);
            }
            echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll($criteria), 'id', 'display_name'), $htmlOptions=array('empty'=>'Select One'));
            ?>
		</span>
        </div>
        <div class="g3">
            <label class="g3">Content:</label>
            <span class="g9"><?php echo $form->textField($model,'searchContent');?></span>
        </div>
        <div class="g3">
            <label class="g3">Body:</label>
            <span class="g9"><?php echo $form->textField($model,'body');?></span>
        </div>
        <div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
        <div class="g1 submit" style="text-align:center"><?php echo CHtml::resetButton('RESET', array('class'=>'button reset-button','style'=>'font-size: 11px; height: 30px;')); ?></div>
        <?php $this->endWidget(); ?>

	</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'email-templates-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'id',
		array(
			'type'  => 'raw',
			'name'  => 'Status',
			'value' => 'StmFormHelper::getStatusBooleanName($data->status_ma)',
		),
		'componentType.display_name',
		'subject',
        'description',
        array(
            'type'=>'raw',
            'name'=>'Include Signature',
            'value' => 'StmFormHelper::getYesNoName($data->include_signature)',
            'htmlOptions'=>array('style'=>'width:80px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/emailTemplates/edit/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:80px'),
        ),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a data-id=\"".$data->id."\" class=\"button gray icon i_stm_delete grey-button delete-email-template-button\">Delete</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:90px'),
        ),
	),
));
