<?php
	$form = $this->beginWidget('CActiveForm', array(
			'id' => ' contact-form',
			'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
		)
	);
	$this->beginStmPortletContent(array(
			'handleTitle' => 'Email Template Details',
			'handleIconCss' => 'i_wizard'
		)
	);

    $attachFileField = "<input placeholder='Attachment' class='g7 emailAttachmentInput' type='file' name='EmailTemplates[attachments][]' id='EmailTemplate_attachments' />";
?>
<div id="action-plans-container">
	<div class="g12 p-mb5 rounded-text-box odd-static">
		<table class="container">
			<tr>
				<th class="narrow">Status:</th>
				<td>
					<?php echo $form->dropDownList($model, 'status_ma', array(
							1 => 'Active',
							0 => 'Inactive'
						), $htmlOptions = array('class' => 'g2')
					); ?>
					<label class="label p-fl p-pr4" style="padding-left:100px;">Type:</label>
					<?php echo $form->dropDownList($model, 'component_type_id', CHtml::listData(ComponentTypes::model()->actionPlan()->findAll(), 'id', 'display_name'), $htmlOptions = array(
							'empty' => 'Select One',
							'class' => 'g2'
						)
					); ?>
					<?php echo $form->error($model, 'status_ma'); ?>
				</td>
			</tr>
			<tr>
				<th class="narrow">Description:</th>
				<td>
					<?php echo $form->textArea($model, 'description', $htmlOptions = array(
							'placeholder' => 'Description',
							'class' => 'g7',
                            'rows' => '4',
						)
					); ?>
					<?php echo $form->error($model, 'component_type_id'); ?><?php echo $form->error($model, 'description'); ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr/>
					<h3>Edit the Email Message Below:</h3>
				</td>

			</tr>
			<tr>
				<th class="narrow">Subject:</th>
				<td>
					<?php echo $form->textField($model, 'subject', $htmlOptions = array(
							'placeholder' => 'Subject',
							'class' => 'g7',
						)
					); ?>
					<?php echo $form->error($model, 'subject'); ?>
				</td>
			</tr>
            <tr>
                <th class="narrow">Signature:</th>
                <td>
                    <?php echo $form->dropDownList($model, 'include_signature', array(
                            1 => 'Yes, Include Signature',
                            0 => 'No, Do NOT Include Signature'
                        ), $htmlOptions = array('class' => 'g2')
                    ); ?>
                    <?php echo $form->error($model, 'include_signature'); ?>
                </td>
            </tr>
			<tr>
				<th class="narrow">Body:</th>
				<td style="width: 800px;">
                    <div style="font-weight: bold;">
                        To have the first name of the contact appear in the email, type in {{first_name}}.<br />
                        Example: Hello {{first_name}}, how are you doing?<br /><br />
                        To have the appointment date appear in the email, type in {{appointment_date}}<br />
                        Example: You have an appointment scheduled for {{appointment_date}}<br /><br />

                        To have the seller address appear in the email, the following fields are available: {{seller_address}}, {{seller_city}}, {{seller_state}} and {{seller_zip}}<br /><br />

                        To reference your primary website domain (mywebsite.com) in text or in a link: {{website}}.<br /><br />

                        To have the recruit information appear in the email, the following fields are available: {{recruit_units}}, {{recruit_volume}}, {{recruit_volume_change}} and {{recruit_volume_change_percent}}<br /><br />

                        WARNING: Do NOT copy from a Microsoft document as it adds invalid hidden tags.<br /><br />
                    </div>
                    <?php echo $form->error($model, 'body'); ?>
					<?php
						/*Yii::app()->controller->widget('admin_module.extensions.tinymce.ETinyMce', array(
								'model' => $model,
								'attribute' => 'body',
								'useSwitch' => false,
								'htmlOptions' => array(
									'style' => 'width: 800px',
								),
								'options' => array(
									'theme' => 'advanced',
									'theme_advanced_toolbar_location' => 'top',
									'theme_advanced_toolbar_align' => 'left',
									'theme_advanced_resizing' => 'false',
									'theme_advanced_buttons1' => 'bold,italic,underline,strikethrough,separator,forecolor,backcolor,separator,fontselect,fontsizeselect,separator,justifyleft,justifycenter,justifyright,justifyfull,separator',
									'theme_advanced_buttons2' => 'bullist,numlist,separator,outdent,indent,blockquote,separator,undo,redo,link,unlink,separator,link,unlink,anchor,image,cleanup,help,removeformat,code',
									'theme_advanced_buttons3' => '',
									'height' => '800',
                                    'width' => '800px',
									'apply_source_formatting' => false,
									'cleanup_on_startup' => false,
									'font_size_style_values' => '10px,12px,13px,14px,16px,18px,20px',
									'force_br_newlines' => true,
									'force_p_newlines' => false,
									'forced_root_block' => '',
									'convert_newlines_to_brs' => true,
									'plugins' => 'spellchecker',
								)
							)
						);*/
					Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);

$scope = (YII_DEBUG) ? "local" : "com";
$s3BaseUrl = (YII_DEBUG) ? "dev.sites.seizethemarket.com" : "sites.seizethemarket.com";

$s3Url = urlencode(base64_encode('s3://'.$s3BaseUrl.'/site-files/'.Yii::app()->user->clientId."/emailTemplates/".$model->id));

$js = <<<JS
	tinymce.PluginManager.load('moxiemanager', 'http://www.seizethemarket.{$scope}/imagemanager/plugin.js?v=4');
	tinymce.init({
		selector: "#EmailTemplates_body",
		convert_urls: false,
		relative_urls: false,
		height : 800,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste moxiemanager, spellchecker"
		],
		image_advtab: true,

        cleanup_on_startup: false,
        trim_span_elements: false,
        verify_html : false,

		extended_valid_elements : "span[*|top|position],a[*],a*,div[*],span*,div[*]",
		cleanup: false,
		toolbar: "fontselect fontsizeselect spellchecker | forecolor forecolorpicker backcolor bold italic underline | cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image insertfile undo redo | preview | code",
		autosave_ask_before_unload: false,
		setup : function(ed) {
		    ed.on('init', function() { this.getDoc().body.style.fontSize = '13px'; });
		    ed.on('keyUp', function() { tinyMCE.triggerSave(); });
		},
		spellchecker_rpc_url: "http://www.seizethemarket.{$scope}/js/tinymce/plugins/spellchecker/spellchecker.php",
		//moxiemanager_apiPageUrl: "http://www.seizethemarket.{$scope}/imagemanager/api.php,
		//moxiemanager_baseUrl: "http://www.seizethemarket.{$scope}/imagemanager"
		init_instance_callback: function () {
			moxman.Env.baseUrl = "http://www.seizethemarket.{$scope}/imagemanager";
			moxman.Env.apiPageName = "api.php?v=1&s3={$s3Url}";
			//moxman.path = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
			//moxman.rootpath = 's3://{$s3BaseUrl}/site-files/{$s3Url}';
		},
		//moxiemanager_rootpath: 's3://{$s3BaseUrl}/site-files/{$s3Url}',
		//moxiemanager_path: '/site-files/{$s3Url}',
	});
JS;

		//http://www.seizethemarket.local/imagemanager/api.php?
		//s3=czM6Ly9kZXYuc2l0ZXMuc2VpemV0aGVtYXJrZXQuY29tL3NpdGUtZmlsZXMvMS9jbXNQYWdlcy8%3D
		//&action=streamfile&path=%2FcmsPages
		//&name=one-story-craftsman-style-home-plans-l-d28baafe08f326b5.jpg
		//&loaded=0
		//&total=105057
		//&id=null
		//&csrf=41d9afb847906a6d5bafccc453a2c6a461204ab064876d1e1a0c198a554db7d2
		//&resolution=default

				Yii::app()->clientScript->registerScript('tinyMceNew', $js); ?>
				<textarea id="EmailTemplates_body" name="EmailTemplates[body]">
					<?=$model->body;?>
				</textarea>
				</td>
                <?php echo $form->hiddenField($model, 'bombbomb_id'); ?>
			</tr>
            <tr>
                <th class="narrow">Attachments:</th>
                <td>
                    <?if($model->id && count($model->attachments)) : ?>
                    <ul id="attachment-list">
                        <?foreach($model->attachments as $attachment) : ?>
                            <li><?=$attachment->name?> <button type="button" class="btn-delete-attachment" data-attachment-id="<?=$attachment->id?>">Delete</button></li>
                        <?endforeach?>
                    </ul>
                    <?endif;?>
                    <div id="attachment-container">
                        <?php echo $attachFileField; ?>
                        <?php echo $form->error($model, 'attachment'); ?>
                        <button type="button" id="add-attachment">Add Attachment</button>
                    </div>
                </td>
            </tr>
		</table>

	</div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
	<button type="submit" class="submit wide">Submit Email Template</button>
</div>
<?php $this->endWidget();

$js = <<<JS
    // Bind event for adding extra attachment
    $('#add-attachment').click(function() {

        var ele = jQuery('#EmailTemplate_attachments').clone();
        ele.attr('id', null);

        jQuery(ele).insertBefore('#add-attachment');
    });

    // Do this only if we have pre-existing attachments
    if($('#attachment-list')) {

        // Bind attachment deletion event
        $('#attachment-list').delegate('.btn-delete-attachment', 'click', function() {

            // Manage scope
            var ele = $(this);

            // Make AJAX call for deletion
            jQuery.post('/admin/emailTemplates/deleteAttachment', {
                attachment_id: ele.data('attachment-id')
            }, function(data) {
                ele.closest('li').remove();
            });

        });
    }
JS;
Yii::app()->clientScript->registerScript('emailTemplatesJs', $js);
