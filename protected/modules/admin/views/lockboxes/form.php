<?
$this->breadcrumbs=array(
    ucwords(Yii::app()->controller->action->id)
);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->id; ?>/lockboxes" class="button gray icon i_stm_search">View All Lockboxes</a>
</div>
<div id="content-header">
	<h1>Lockboxes</h1>
</div>

<?php
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'lockboxes-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=>$mode.' Lockboxes',
        'handleIconCss'=>'i_wizard'
    ));
?>
<div id="goals-container" >
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <div class="g1"></div>
        <div class="g8">
            <table class="container">
                <tr>
                    <th><?php echo $form->labelEx($model, 'status');?></th>
                    <td><?php echo $form->dropDownList($model, 'status', $model->getStatusTypes(), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'status'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'type');?></th>
                    <td><?php echo $form->dropDownList($model, 'type', $model->getLockboxTypes(), $htmlOptions=array('empty'=>'Select One')); ?>
                        <?php echo $form->error($model,'type'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'serial_number');?></th>
                    <td><?php echo $form->textField($model, 'serial_number', $htmlOptions=array('placeholder'=>'Lockbox Serial #')); ?>
                        <?php echo $form->error($model,'serial_number'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $form->labelEx($model, 'notes');?></th>
                    <td><?php echo $form->textArea($model, 'notes', $htmlOptions=array('placeholder'=>'Notes','rows'=>6)); ?>
                        <?php echo $form->error($model,'notes'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="g3"></div>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Lockbox</button></div>
<?php $this->endWidget(); ?>
