<?
$this->breadcrumbs=array(
    'List'
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').change(function() {
    $('#listview-search form').submit();
});

$('#listview-search form').submit(function() {
	$.fn.yiiGridView.update('lockboxes-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->id; ?>/lockboxes/add" class="button gray icon i_stm_add">Add Lockbox</a>
</div>
<div id="content-header">
	<h1>Lockboxes</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'lockboxes-list-search',
        )); ?>
    <div class="g3">
        <label class="g3">Status:</label>
        <span class="g9"><?php echo $form->dropDownList($model,'status', $model->getStatusTypes(), $htmlOptions=array('empty'=>'')); ?></span>
    </div>
    <div class="g3">
        <label class="g6">Lockbox Serial#:</label>
        <span class="g6"><?php echo $form->textField($model,'serial_number', $htmlOptions=array()); ?></span>
    </div>
    <div class="g2">
        <label class="g4">Type:</label>
        <span class="g8"><?php echo $form->dropDownList($model,'type', $model->getLockboxTypes(), $htmlOptions=array('empty'=>'')); ?></span>
    </div>
    <div class="g3">
        <label class="g2">Notes:</label>
        <span class="g9"><?php echo $form->textField($model,'notes', $htmlOptions=array()); ?></span>
    </div>
    <div class="g1 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div>

<br />
<?php $this->widget('admin_module.components.StmGridView', array(
			   'id' => 'lockboxes-grid',
			   'dataProvider' => $model->search(),
			   'itemsCssClass' => 'datatables',
			   'columns' => array(
                   array(
                       'type'=>'raw',
                       'name'=>'Lockbox Serial#',
                       'value'=>'$data->serial_number',
                       'htmlOptions' => array('style' => 'width:120px'),
                   ),
                   array(
                       'type'=>'raw',
                       'name'=>'Status',
                       'value'=>'$data->status',
                       'htmlOptions' => array('style' => 'width:180px'),
                   ),
                   array(
                       'type'=>'raw',
                       'name'=>'Type',
                       'value'=>'$data->type',
                       'htmlOptions' => array('style' => 'width:120px'),
                   ),
                   array(
                       'type'=>'raw',
                       'name'=>'Listing Info',
                       'value'=>'"Coming Soon... :)"',
                   ),
                   array(
                       'type'=>'raw',
                       'name'=>'Notes',
                       'value'=>'$data->notes',
                       'htmlOptions' => array('style' => 'width:400px'),
                   ),
                   array(
                       'type'=>'raw',
                       'name'=>'Last Updated',
                       'value'=>'$data->updated',
                       'htmlOptions' => array('style' => 'width:120px'),
                   ),
				   array(
					   'type' => 'raw',
					   'name' => '',
					   'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/lockboxes/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
					   'htmlOptions' => array('style' => 'width:80px'),
				   ),
			   ),
			   )
);