<?php
$this->breadcrumbs=array(
	'List'=>array('/'.Yii::app()->controller->module->name.'/recruits'),
);

?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name;?>/recruitTypes/add" class="button gray icon i_stm_add">Add New Recruit Tag</a>
</div>
<div id="content-header">
	<h1>Recruit Tags List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?
    Yii::app()->clientScript->registerScript('recruit-type-search', "
	$('#listview-search form').submit(function() {
	    $.fn.yiiGridView.update('recruit-types-grid', {
	        data: $(this).serialize()
	    });
	    return false;
	});
");

    $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'recruit-type-search',
        )); ?>
    <div class="g3">
    </div>
    <div class="g4">
        <label class="g2">Name:</label>
        <span class="g10"><?php echo $form->textField($model,'name');?></span>
    </div>
    <div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'recruit-types-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'name',
//        array(
//            'type'=>'raw',
//            'name'=>'Follow-up Days',
//            'value'=>'$data->follow_up_days',
//            'htmlOptions'=>array('style'=>'width:150px'),
//        ),
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/recruitTypes/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:100px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/recruitTypes/delete/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_delete grey-button\">Delete</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:100px'),
        ),
	),
));
