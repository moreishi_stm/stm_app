<?php
$action = ($this->action->id == 'add') ? 'Add' : 'Edit';
$this->breadcrumbs=array(
    $action=>'',
);

?>
    <div id="featured-areas-header">
        <h1 class="name">Add New Contact Type</h1>
    </div>

<?php
$form=$this->beginWidget('CActiveForm', array(
        'id'=>'recruit-type-form',
        'enableAjaxValidation'=>false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'=>'Recruit Types',
        'handleIconCss'=>'i_strategy'
    ));
?>
    <div id="action-plans-container" >
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table class="container">
                <tr>
                    <th>Name:</th>
                    <td colspan="3">
                        <?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Recruit Type', 'class'=>'g6', ));?>
                        <?php echo $form->error($model,'name'); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php $this->endStmPortletContent(); ?>
    <div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Recruit Type</button></div>
<?php $this->endWidget(); ?>