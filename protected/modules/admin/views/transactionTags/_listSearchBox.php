<?
Yii::app()->clientScript->registerScript('search', "
	$('#listview-search form').submit(function() {
	    $.fn.yiiGridView.update('transaction-tags-grid', {
	        data: $(this).serialize()
	    });
	    return false;
	});
");

 $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'transaction-tags-search',
)); ?>
	<div class="g3">
	</div>
	<div class="g4">
		<label class="g2">Name:</label>
		<span class="g10"><?php echo $form->textField($model,'name');?></span>
	</div>
	<div class="g2 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
<?php $this->endWidget(); ?>
