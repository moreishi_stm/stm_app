<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'transaction-tags-form',
	'enableAjaxValidation'=>false,
));
	$this->beginStmPortletContent(array(
		'handleTitle'=>'Transaction Tags',
		'handleIconCss'=>'i_strategy'
	));
	?>
	<div id="transaction-tags-container" >
		<div class="g12 p-mb5 rounded-text-box odd-static">
	        <table class="container">
	        	<tr>
	        		<th class="narrow">Tag Name:</th>
	        		<td colspan="3">
			    		<?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Transaction Tags', 'class'=>'g9', ));?>
        				<?php echo $form->error($model,'name'); ?>
	            	</td>
	        	</tr>
	        </table>

		</div>
	</div>
	<?php $this->endStmPortletContent(); ?>
	<div id="submit-button-wrapper"><button type="submit" class="submit wide">Submit Transaction Tag</button></div>
<?php $this->endWidget(); ?>