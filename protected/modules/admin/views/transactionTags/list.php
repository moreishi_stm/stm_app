<?php
$this->breadcrumbs=array(
	'List'=>'',
);

?>
<div id="listview-actions">
	<a href="/<?php echo Yii::app()->controller->module->name;?>/transactionTags/add" class="button gray icon i_stm_add">Add Transaction Tags</a>
</div>
<div id="content-header">
	<h1>Transaction Tags List</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php $this->renderPartial('_listSearchBox',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
	'id'=>'transaction-tags-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'datatables',
	'columns'=>array(
		'name',
		array(
			'type'=>'raw',
			'name'=>'',
			'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/transactionTags/edit/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"
			',
			'htmlOptions'=>array('style'=>'width:100px'),
		),
        array(
            'type'=>'raw',
            'name'=>'',
            'value'=>'
				"<div><a href=\"/".Yii::app()->controller->module->name."/transactionTags/delete/".$data->id."\" target=\"_blank\" class=\"button gray icon i_stm_delete grey-button\">Delete</a></div>"
			',
            'htmlOptions'=>array('style'=>'width:100px'),
        ),
	),
));
