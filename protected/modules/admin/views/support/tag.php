<? $this->breadcrumbs = array($tag->name=>'');
Yii::app()->clientScript->registerCss('supportTagCss', <<<CSS
    .tutorial-video {
        font-size: 22px;
    }
    a.tutorial-video {
        display: block;
        padding: 15px;
    }
    .video-row {
        background: #F3F3F3;
        padding: 0;
        margin-left: 32%;
    }
    .video-row:nth-child(odd) {
        background: white;
    }
    .video-row:hover {
        background: #d1eaff;
    }
    .play-button {
        background: url(http://cdn.seizethemarket.com/assets/images/play_button.png) 0 -213px !important;
        float: left;
        width: 56px;
        height: 31px;
    }
CSS
);
?>
<div id="content-header">
    <h1><?=$this->title?></h1>
    <h3>Play each video and learn its content. Click on the bottom middle right button to view full screen. </h3>
</div>
<br />
<br />
<?$step = 0; foreach($tag->supportVideos as $video) : ?>
    <?$step++;?>
    <div class="g5 p-tl p-p0 video-row p-clr">
        <a href="/admin/support/video/<?=$video['id']?>" class="tutorial-video"><div class="play-button"></div>Step <?=$step?>: <?=$video['title']?></a>
    </div>
<?endforeach;?>