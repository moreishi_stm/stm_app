<style type="text/css">
    <?php echo $this->getBaseCssScript(); ?>
</style>
<?php
$this->breadcrumbs = array(
    'Agent Quick Start Training' => '',
);
?>
	<div id="content-header">
		<h1>Agent Quick Start Training</h1>
	</div>
    <br />
    <br />
<div id="support-dashboard" class="">
    <ul class="">
        <li><a href="/admin/support/tag/7"><span>Agent Quick Start Training Level 1</span></a></li>

        <?php if(Yii::app()->user->checkAccess('supportAgentLevel2')): ?>
            <li><a href="/admin/support/tag/8"><span>Agent Quick Start Training Level 2</span></a></li>
        <?php endif; ?>
<!--        --><?php //if(Yii::app()->user->checkAccess('supportAgentLevel3')): ?>
<!--            <li><a href="#"><span>Agent Quick Start Training Level 3</span></a></li>-->
<!--        --><?php //endif; ?>
<!--        --><?php //if(Yii::app()->user->checkAccess('supportAgentLevel4')): ?>
<!--            <li><a href="#"><span>Agent Quick Start Training Level 4</span></a></li>-->
<!--        --><?php //endif; ?>
    </ul>
</div>