<style type="text/css">
    <?php echo $this->getBaseCssScript(); ?>
</style>
<?php
$this->breadcrumbs = array(
    'List' => '',
);

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
    $.fn.yiiGridView.update('support-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
	<div id="content-header">
		<h1>Seize the Market Video Training & Support Center</h1>
	</div>
    <br />
    <br />
<div style="margin-bottom: 50px;">
    <h2>How to submit a support ticket:</h2> <br>
        <h3>Email the following information to<br><span style="text-decoration: underline; font-weight: bold; font-size: 16px; color: cornflowerblue;">Support@SeizetheMarket.com</span>:</h3>
        <div style="display: inline-block; margin-left: 38%; float:none;">
            <ol style="font-size: 15px;">
                <li>Description of question</li>
                <li>URL link of the page</li>
                <li>How to recreate your scenario</li>
                <li>Screenshot of the whole screen you are referring to</li>
            </ol>
        </div>
    <h3>You can also call us at (904) 280-3800.</h3>
</div>
<div id="support-dashboard">
    <ul>
        <li><a href="/admin/support/agent"><span>Agent<br>Training</span></a></li>
        <li><a href="/admin/support/comprehensive"><span>Comprehensive <br>Systems<br>Training</span></a></li>
        <li><a href="/admin/support/lender"><span>Lender<br>Training</span></a></li>
        <li><a href="/admin/support/teamleader"><span>Team Leader<br>Training</span></a></li>
        <li><a href="/admin/support/tag/13"><span>Dialer<br>Training</span></a></li>
    </ul>
</div>
<h1 style="clear: both;">Search Support Topics by typing in a Search Term below:</h1>
<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">

    <?php $form = $this->beginWidget('CActiveForm', array(
            'method' => 'get',
            'id' => 'support-search',
        )
    ); ?>
    <div class="g12">
        <div class="g2"></div>
        <div class="g6">
            <label class="g4">Search Support:</label>
            <span class="g8"><?php echo $form->textField($model, 'title',$htmlOptions=array('placeholder'=>'Type Search Term here...')); ?></span>
        </div>
        <div class="g2 submit" style="text-align:center">
            <?php echo CHtml::submitButton('SEARCH', array('class' => 'button')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?
    $this->widget('admin_module.components.StmGridView', array(
            'id' => 'support-grid',
            'dataProvider' => $model->search(),
            'itemsCssClass' => 'datatables',
            'columns' => array(
                array(
                    'type' => 'raw',
                    'name' => 'Title',
                    'value' => '$data->title',
//                    'htmlOptions'=>array('style'=>'width:40%'),
                ),
//                array(
//                    'type' => 'raw',
//                    'name' => 'Description',
//                    'value' => '$data->description',
//                ),
                array(
                    'type'=>'raw',
                    'name'=>'',
                    'value'=>'"<div><a href=\"/admin/support/video/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View Details</a></div>"',
                    'htmlOptions'=>array('style'=>'width:160px'),
                ),
            ),
        )
    );
?>