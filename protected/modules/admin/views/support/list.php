<?php
Yii::app()->clientScript->registerScript(
    'supportScript', <<<JS
//    $('.video-container').click(function() {
//        next.a href="#video-box"
//        alert($(this).attr('data-vid'));
//        $('#video-box').html();
//        return false;
//    });

    $('.tutorial-video').click(function() {
        var htmlContent = '<iframe src="https://seizethemarket.viewscreencasts.com/embed/'+$(this).data('vid')+'" width="800" height="487" frameborder="0"></iframe>';
        $('#video-box').html(htmlContent);
    });
JS
);

$this->widget(
    'admin_module.extensions.fancybox.EFancyBox', array(
                                                  'target' => 'a.tutorial-video', 'config' => array(
            'width' => '800', 'height' => 'auto', 'scrolling' => 'no', 'autoDimensions' => false, 'padding' => '40',
        ),
                                                  )
);

$this->breadcrumbs = array(
    'List' => '',
);
//?>
<div id="content-header">
    <h1><?php echo $this->title; ?></h1>
    <h3>Play each video and learn its content. Click on the bottom middle right button to view full screen. </h3>
</div>
<br />
<br />
<!--	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">-->
<!--		--><?php //$this->renderPartial('_listSearchBox', array(
//				'model' => $model,
//			)
//		); ?>
<!--	</div><!-- search-form -->

<?php

foreach($tutorials as $key =>$tutorial) {
    $step = $key+1;
    echo '<div class="g12 video-container">';
    echo '<div class="g5 p-tc">';
    echo '<a href="#video-box" class="tutorial-video thumb" data-vid="'.$tutorial['videoCode'].'"><img src="https://viewscreencasts.blob.core.windows.net/screencast-images/'.$tutorial['videoCode'].'_thumb.jpg" width="300"></a>';
    echo '</div>';
    echo '<div class="g5">';
    echo '<h2 class="p-tl"  style="margin-top: 60px;"><a href="#video-box" class="tutorial-video" data-vid="'.$tutorial['videoCode'].'">Step '.$step.': '.$tutorial['title'].'</a></h2>';
    echo '</div>';
    echo '</div>';
}

?>
<div style="display: none">
    <div id="video-box">
    </div>
</div>