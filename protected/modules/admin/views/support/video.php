<? $this->breadcrumbs = array($video->title=>'') ?>
<h1><?=$video->title?></h1>
<div class="p-tc" style="margin-top: 40px;">
    <iframe src="http://www.seizethemarket.<?=(YII_DEBUG)? 'local': 'com'?>/supportVideos/<?=$video->vimeo_code;?>" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="1030" height="590" frameborder="0" style="margin-right: auto; margin-left: auto;"></iframe>
</div>


<? if(!empty($video->description)) : ?>
<h2 style="margin-top: 40px;">Description</h2>
<p style="max-width: 1200px; margin: 0 auto 50px auto; font-size: 1.7em;">
    <?=$video->description?>
</p>
<?endif;?>