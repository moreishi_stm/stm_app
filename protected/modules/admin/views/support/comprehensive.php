<style type="text/css">
    <?php echo $this->getBaseCssScript(); ?>
    #support-dashboard ul li{
        /*margin-left: 8%;*/
    }
    #support-dashboard ul li a span{
        top: 35px;
    }</style>
<?php
$this->breadcrumbs = array(
    'Comprehensive Systems Training' => '',
);
?>
	<div id="content-header">
		<h1>Comprehensive Systems Training</h1>
	</div>
    <br />
    <br />
<div id="support-dashboard" class="">
    <ul class="">
        <li><a href="/admin/support/tag/3"><span>Comprehensive Systems Training Level 1</span></a></li>

        <?php if(Yii::app()->user->checkAccess('supportLevel2')): ?>
            <li><a href="/admin/support/tag/4"><span>Comprehensive Systems Training Level 2</span></a></li>
        <?php endif; ?>
        <?php if(Yii::app()->user->checkAccess('supportLevel3')): ?>
            <li><a href="/admin/support/tag/5"><span>Comprehensive Systems Training Level 3</span></a></li>
        <?php endif; ?>
        <?php if(Yii::app()->user->checkAccess('supportLevel4')): ?>
            <li><a href="/admin/support/tag/6"><span>Comprehensive Systems Training Level 4</span></a></li>
        <?php endif; ?>
        <?php if(Yii::app()->user->checkAccess('supportLevel5')): ?>
            <li><a href="#"><span>Comprehensive Systems Training Level 5</span></a></li>
        <?php endif; ?>
    </ul>
</div>