<?php

$form = $this->beginWidget('CActiveForm', array(
        'id' =>'stm-form',
        'enableAjaxValidation' => false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'   => 'Flash Card Topic',
        'handleIconCss' => 'i_wizard'
    ));
?>
<div>
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <table class="container">
            <tr>
                <th class="narrow">Name:</th>
                <td colspan="3">
                    <?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Topic Name', 'class'=>'g9', ));?>
                    <?php echo $form->error($model,'name'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">Description:</th>
                <td colspan="3">
                    <?php echo $form->textArea($model,'description', $htmlOptions=array('placeholder'=>'Description', 'class'=>'g9','rows'=>4 ));?>
                    <?php echo $form->error($model,'description'); ?>
                </td>
            </tr>
        </table>

    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <?php echo CHtml::htmlButton('Submit Flash Card Topic', $htmlOptions = array(
            'class' => 'submit wide',
            'type' => 'submit',
        )); ?>
</div>
<?php $this->endWidget(); ?>
