<?php
	$this->breadcrumbs = array(
		'Sort Order' => '',
	);
    $module = Yii::app()->controller->module->name;

    Yii::app()->clientScript->registerScript('flashCardTopicsScript', <<<JS
//        $("#ActionPlans_component_type_id, #ActionPlans_status_ma").change(function(){
//            $('#listview-search form').submit();
//        });
//
        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("stm-grid", {
                data: $(this).serialize()
            });
            return false;
        });
//
//        $( '.delete-action-plan-button' ).live( "click", function() {
//            if(confirm('Are you sure you want to delete this Action Plan?')) {
//                $("body").prepend("<div class='loading-container loading'><em></em></div>");
//                var id = $(this).data('id');
//                $.post('/$module/actionPlans/delete/'+id, function(data) {
//                    $("div.loading-container.loading").remove();
//                    if(data=='') {
//                        Message.create("success","Action Plan Item deleted successfully.");
//                        $.fn.yiiGridView.update("action-plan-grid", { data: $(this).serialize() });
//                    } else {
//                        Message.create("error","Error: Action Plan did not delete.");
//                    }
//                },"json");
//            }
//        });
JS
);
?>
<style>
    #sortable {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    #sortable li {
        margin: 0 3px 3px 3px;
        padding: 0.4em;
        padding-left: 1.5em;
        font-size: 1.4em;
        height: 18px;
        border: 1px solid;
        border: 1px solid #888;
        background: #efefef;
    }
    #sortable li a {
        display: block;
        width: 100%;
        position: relative;
    }
    #sortable li span {
        position: absolute;
        right: 0;
    }
    #sortable li a:hover span {
        text-decoration: underline;
    }

</style>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/flashcards/addCards/<?=$_GET['id']?>" class="button gray icon i_stm_add">Add Flash Card</a>
    <a href="/<?php echo Yii::app()->controller->module->name;?>/flashcards" class="button gray icon i_stm_search">View All Topics</a>
    <a href="/<?php echo Yii::app()->controller->module->name;?>/flashcards/study/<?=$_GET['id']?>" class="button gray icon i_stm_search">Start Study</a>
</div>

<h1 style="margin-top: 20px;"><?=$model->name?></h1>
<h3>Instructions: Drag to Reorder the Flash Card</h3>

<div style="width: 50%; margin-left: 25%; margin-top: 50px;">
<?
$this->widget('zii.widgets.jui.CJuiSortable',array(
        'id' => 'sortable',
        'items'=>$listData,
        // additional javascript options for the JUI Sortable plugin
        'options'=>array(
            'delay'=>'150',
            'cursor' => 'move',
            'update' => new CJavaScriptExpression("function(){
            $('body').prepend('<div class=\"loading-container loading\"><em></em></div>');
            $.post('/admin/flashcards/orderTopicCards/{$_GET['id']}', {'items': $(this).sortable('toArray')}, function(data){
                setTimeout(function(){ $('div.loading-container.loading').remove()}, 500);
            });
        }"),
        ),
    ));?>
</div>