<?php
	$this->breadcrumbs = array(
		'Topics' => '',
	);
    $module = Yii::app()->controller->module->name;

    Yii::app()->clientScript->registerScript('flashCardTopicsScript', <<<JS
    $('.show-hide-button').click(function(){
        $(this).next().toggle('normal');
    });

    $('.next-flashcard').click(function(){
        var currCard = $('div.active');
        if(currCard.next().length==0) {
            alert('You reached the end of the flash cards');
            return false;
        }
        else {
            currCard.removeClass('active').addClass('hidden');
            currCard.next().removeClass('hidden').addClass('active');
        }
    });

    $('.prev-flashcard').click(function(){
        var currCard = $('div.active');
        if(currCard.prev().length==0) {
            alert('You reached the beginning of the flash cards');
            return false;
        }
        else {
            currCard.removeClass('active').addClass('hidden');
            currCard.prev().removeClass('hidden').addClass('active');
        }
    });

JS
);
?>
<style>
    h1  {
        font-size: 50px;
        line-height: 1 !important;
    }
    #flash-card-container {
        margin-top: 20px;
        border: 3px solid rgb(255, 247, 79);
        padding: 40px;
        min-height: 200px;
        background: #f9ff9d;
        margin-left: 12.5%;
        width: 75%;
    }
    #flash-card-button-container {
        margin-left: 12.5%;
        width: 75%;
        height: 50px;
    }
    .prev-flashcard, .next-flashcard {
        padding: 10px;
        background: #EFEFEF;
        border: 2px solid #DDD;
        margin-left: 40px;
        font-size: 20px;
        cursor: pointer;
    }
    .prev-flashcard {
        float: left;
    }
    .next-flashcard{
        float: right;
    }
    .prev-flashcard:hover, .next-flashcard:hover {
        background: #b3d4ff;
        border: 2px solid #8db0e3;
    }
    #flash-card-container .card-name {
        font-weight: bold;
        margin-bottom: 30px;
        text-decoration: underline;
    }
    #flash-card-container .front-content, #flash-card-container .back-content, #flash-card-container .card-name, #flash-card-container p{
        font-size: 30px;
        line-height: 1;
    }
    #flash-card-container p {
        margin-bottom: 32px;
    }
    #flash-card-container .back-content.hide {
        display: none;
    }
    .show-hide-button {
        cursor: pointer;
        background: #DDD;
        display: inline-block;
        font-size: 15px;
        margin: 20px 0;
        padding: 10px;
        margin-left: -10px;
    }
    .show-hide-button:hover {
        background: #efefef;
        text-decoration: underline;
    }
</style>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/flashcards" class="button gray icon i_stm_search">All Flash Card Topic</a>
</div>

<h3>Study Flash Cards</h3>
<h1><?=$topic->name;?></h1>
<hr/>
<h2>Memorize => Internalize => Improvise</h2><br>
<h3>1) MEMORIZE: Stick to the Script. Repeat out loud 10 times or until you have it Memorized. Do NOT change anything.</h3>
<h3>2) INTERNALIZE: You can repeat it out loud on demand without hesitation.</h3>
<h3>3) IMPROVISE: Once you have mastered the script and understand the key pieces, then you can Improvise.</h3>
<hr/>
<div id="flash-card-button-container">
    <div class="next-flashcard">
        Next
    </div>
    <div class="prev-flashcard">
        Previous
    </div>
</div>

<div id="flash-card-container">
<?foreach($flashCards as $i => $flashCards): ?>
<div class="<?=($i) ? 'hidden': 'active';?>">
    <div class="card-name">
        <?=$flashCards->name?>
    </div>
    <div class="front-content">
        <?=$flashCards->front_content?>
    </div>
    <div class="show-hide-button">
        Click to Show Answer
    </div>
    <div class="back-content hide">
        <?=$flashCards->back_content?>
    </div>
</div>

<?endforeach;?>
</div>