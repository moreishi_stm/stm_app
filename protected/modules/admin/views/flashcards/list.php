<?php
	$this->breadcrumbs = array(
		'Topics' => '',
	);
    $module = Yii::app()->controller->module->name;

    Yii::app()->clientScript->registerScript('flashCardTopicsScript', <<<JS
//        $("#ActionPlans_component_type_id, #ActionPlans_status_ma").change(function(){
//            $('#listview-search form').submit();
//        });
//
        $('#listview-search form').submit(function() {
            $.fn.yiiGridView.update("stm-grid", {
                data: $(this).serialize()
            });
            return false;
        });
//
//        $( '.delete-action-plan-button' ).live( "click", function() {
//            if(confirm('Are you sure you want to delete this Action Plan?')) {
//                $("body").prepend("<div class='loading-container loading'><em></em></div>");
//                var id = $(this).data('id');
//                $.post('/$module/actionPlans/delete/'+id, function(data) {
//                    $("div.loading-container.loading").remove();
//                    if(data=='') {
//                        Message.create("success","Action Plan Item deleted successfully.");
//                        $.fn.yiiGridView.update("action-plan-grid", { data: $(this).serialize() });
//                    } else {
//                        Message.create("error","Error: Action Plan did not delete.");
//                    }
//                },"json");
//            }
//        });
JS
);
?>
<div id="listview-actions">
    <a href="/<?php echo Yii::app()->controller->module->name;?>/<?php echo Yii::app()->controller->id;?>/addTopics" class="button gray icon i_stm_add">Add Flash Card Topic</a>
	<a href="/<?php echo Yii::app()->controller->module->name;?>/<?php echo Yii::app()->controller->id;?>/addCards" class="button gray icon i_stm_add">Add Flash Card</a>
    <a href="/<?php echo Yii::app()->controller->module->name;?>/<?php echo Yii::app()->controller->id;?>/cards" class="button gray icon i_stm_search">View All Cards</a>
</div>

<h1>Flash Card Topics</h1>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'list-search',
        )); ?>
    <div class="g3"></div>
    <div class="g5">
        <label class="g2">Name:</label>
        <span class="g10"><?php echo $form->textField($model,'name');?></span>
    </div>
    <div class="g3 submit" style="text-align:center"><?php echo CHtml::submitButton('SEARCH', array('class'=>'button')); ?></div>
    <?php $this->endWidget(); ?>
</div><!-- search-form -->

<?php $this->widget('admin_module.components.StmGridView', array(
		'id' => 'stm-grid',
		'dataProvider' => $model->search(),
		'itemsCssClass' => 'datatables',
		'columns' => array(
			'name',
            'description',
            array(
                'type' => 'raw',
                'name' => 'Card Count',
                'value' => 'count($data->flashCards)',
                'htmlOptions' => array('style' => 'width:100px'),
            ),
            array(
				'type' => 'raw',
				'name' => '',
				'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/".Yii::app()->controller->id."/editTopics/".$data->id."\" class=\"button gray icon i_stm_edit grey-button\">Edit</a></div>"',
				'htmlOptions' => array('style' => 'width:80px'),
			),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/".Yii::app()->controller->id."/addCards/".$data->id."\" class=\"button gray icon i_stm_add grey-button\">Add Flashcard</a></div>"',
                'htmlOptions' => array('style' => 'width:150px'),
            ),
//            array(
//                'type' => 'raw',
//                'name' => '',
//                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/flashcards/cards/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View Topic Cards</a></div>"',
//                'htmlOptions' => array('style' => 'width:150px'),
//            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/".Yii::app()->controller->id."/orderTopicCards/".$data->id."\" class=\"button gray icon i_stm_log grey-button\">View Topic Cards</a></div>"',
                'htmlOptions' => array('style' => 'width:150px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/".Yii::app()->controller->module->name."/".Yii::app()->controller->id."/study/".$data->id."\" class=\"button gray icon i_stm_question grey-button\">Study Now</a></div>"',
                'htmlOptions' => array('style' => 'width:120px'),
            ),
		),
	)
);
?>
