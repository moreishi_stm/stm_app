<?php
$this->breadcrumbs = array(
    'Card' => '',
);

Yii::app()->clientScript->registerScriptFile('http://cdn.seizethemarket.com/assets/js/tinymce/tinymce.min.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScript('flashCardScript', <<<JS
	tinymce.init({
		selector: "#FlashCards_front_content, #FlashCards_back_content",
		convert_urls: false,
		relative_urls: false,
		height : 500,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor textcolor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | styleselect | fontselect fontsizeselect forecolor backcolor bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		autosave_ask_before_unload: false,
		setup : function(ed) {
		    ed.on('init', function() { this.getDoc().body.style.fontSize = '36px'; });
//		    ed.on('keyUp', function() { tinyMCE.triggerSave(); });
		}
	});

    $('select#FlashCards_topicCollection').change(function(){
        if($('select#FlashCards_topicCollection').val()==null) {
	        $('select#FlashCards_topicCollection').val(null).trigger('liszt:updated');
        }
    });
JS
);
?>
<h1>Flash Card</h1>

<?

$form = $this->beginWidget('CActiveForm', array(
        'id' =>'stm-form',
        'enableAjaxValidation' => false,
    ));
$this->beginStmPortletContent(array(
        'handleTitle'   => 'Flash Cards',
        'handleIconCss' => 'i_wizard'
    ));
?>
<div>
    <div class="g12 p-mb5 rounded-text-box odd-static">
        <table class="container">
            <tr>
                <th class="narrow">Name:</th>
                <td colspan="3">
                    <?php echo $form->textField($model,'name', $htmlOptions=array('placeholder'=>'Flash Card Name', 'class'=>'g9', ));?>
                    <?php echo $form->error($model,'name'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">Topics:</th>
                <td colspan="3">
                    <?php echo $form->dropDownList($model, 'topicCollection', CHtml::listData(FlashCardTopics::model()->findAll(), 'id', 'name'), $htmlOptions=array('class'=>'g9','multiple'=>'multiple','data-placeholder'=>'Select Topics'));
                    $this->widget('admin_module.extensions.EChosen.EChosen', array(
                            'target' => 'select#FlashCards_topicCollection',
                            'options'=>array(
                            ),
                        ));
                    ?>
                    <?php echo $form->error($model,'topicCollection'); ?>
                </td>
            </tr>
            <tr>
                <th class="narrow">Front Content:</th>
                <td colspan="3">
                    <?php echo $form->error($model,'front_content'); ?>
                    <?php echo $form->textField($model,'front_content', $htmlOptions=array('class'=>'g9', ));?>
                </td>
            </tr
            <tr>
                <th class="narrow">Back Content:</th>
                <td colspan="3">
                    <?php echo $form->error($model,'back_content'); ?>
                    <?php echo $form->textField($model,'back_content', $htmlOptions=array('class'=>'g9', ));?>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $this->endStmPortletContent(); ?>
<div id="submit-button-wrapper">
    <?php echo CHtml::htmlButton('Submit Flash Card', $htmlOptions = array(
            'class' => 'submit wide',
            'type' => 'submit',
        )); ?>
</div>
<?php $this->endWidget(); ?>
