<?
$this->breadcrumbs=array(
	'Google'=>'',
);
//$css = <<<CSS
//CSS;
//Yii::app()->clientScript->registerCss('integrationsSettingCss', $css);

?>
<h1>Google Calendar List</h1>
<?php
//$x=1;
// list of calendars
//foreach($data as $item) {
//    echo '<a href="/admin/integrations/googleCalendar/'.$item->id.'">'.$item->summary.' (id: '.$item->id.')</a><br>';//.' Description: '.$item->description.'<br />'; // need to have owner //Google_Service_Calendar_CalendarListEntry
//}

// list of events
foreach($data as $item) {

    $timezone = new DateTimeZone(Yii::app()->user->account->timezone);

    $startTime = ($item->start->dateTime)? $item->start->dateTime : $item->start->date;
    $startTimeFormatString = ($item->start->dateTime)? 'm/d/Y g:iA' : 'm/d/Y';
    $startDateTime = new DateTime($item->start->dateTime);
    $startDateTime->setTimezone($timezone);



    $endTime = ($item->end->dateTime)? $item->end->dateTime : $item->end->date;
    $endTimeFormatString = ($item->end->dateTime)? 'm/d/Y g:iA' : 'm/d/Y';
    $endDateTime = new DateTime($item->start->dateTime);
    $endDateTime->setTimezone($timezone);

    $recurring = ($item->getRecurringEventId())? ' (Recurring)' : '';
    echo $item->summary.' Start: '.$startDateTime->format($startTimeFormatString).' | End: '.$endDateTime->format($endTimeFormatString).$recurring.'<br/>';
}
?>
