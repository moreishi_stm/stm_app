<?
$this->breadcrumbs=array(
	'Integrations'=>'',
);
$css = <<<CSS
    .row {
        margin-bottom: 30px;
    }
CSS;
Yii::app()->clientScript->registerCss('integrationsSettingCss', $css);

?>
<h1>Third-Party Integrations</h1>
<!--<div id="listview-actions">-->
<!--	<a href="/--><?php //echo Yii::app()->controller->module->id; ?><!--/settings" class="button gray icon i_stm_edit">Edit User Settings</a>-->
<!--</div>-->
<div id="integrations-container" style="width: 70%; margin-left: 15%;">
<!--    <div class="g12 row">-->
<!--        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/docusign.png" alt="" style="width: 250px;"/></div>-->
<!--        <div class="g6">-->
<!---->
<!--        </div>-->
<!--        <div class="g3"><a href="javascript:void(0)" class="button gray icon i_stm_edit">Edit Setting</a></div>-->
<!--    </div>-->
<!---->
<!--    <div class="g12 row">-->
<!--        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/dotloop.png" alt="" style="width: 250px;"/></div>-->
<!--        <div class="g6">-->
<!---->
<!--        </div>-->
<!--        <div class="g3"><a href="" class="button gray icon i_stm_edit">Edit Setting</a></div>-->
<!--    </div>-->
<!---->
<!--    <div class="g12 row">-->
<!--        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/mailchimp.png" alt="" style="width: 250px;"/></div>-->
<!--        <div class="g6">-->
<!---->
<!--        </div>-->
<!--        <div class="g3"><a href="/admin/oauth2/authorize/mailchimp" class="button gray icon i_stm_edit" target="_blank">Edit Setting</a></div>-->
<!--    </div>-->
<!---->
<!--    <div class="g12 row">-->
<!--        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/constantcontact.png" alt="" style="width: 250px;"/></div>-->
<!--        <div class="g6">-->
<!---->
<!--        </div>-->
<!--        <div class="g3"><a href="/admin/oauth2/authorize/constantcontact" class="button gray icon i_stm_edit" target="_blank">Edit Setting</a></div>-->
<!--    </div>-->
<!---->
<!--    <div class="g12 row">-->
<!--        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/dropbox.png" alt="" style="width: 250px;"/></div>-->
<!--        <div class="g6">-->
<!---->
<!--        </div>-->
<!--        <div class="g3"><a href="/admin/oauth2/authorize/dropbox" class="button gray icon i_stm_edit" target="_blank">Edit Setting</a></div>-->
<!--    </div>-->

    <?php
    $isGoogleConnected = (ApiKeys::model()->findByAttributes(array('contact_id'=>Yii::app()->user->id, 'type'=>'google')))? true : false;
    ?>
    <div class="g12 row">
        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/googleapps.png" alt="" style="width: 250px;"/></div>
        <div class="g2"></div>
        <div class="g4">
            <?php
            if(!$isGoogleConnected) {?>
                <a href="/admin/oauth2/authorize/google" class="button gray icon i_stm_add" target="_blank" style="margin-top: 20px;">Integrate Now</a>
            <? }
            else {
                echo '<h2 style="margin-top: 20px;">Already Connected!</h2>';
            }
            ?>
        </div>
        <div class="g3">
            <?php
            if($isGoogleConnected) {?>
                <a href="/admin/oauth2/authorize/google" class="button gray icon i_stm_add" target="_blank" style="margin-top: 20px;">Re-do Integration</a>
            <? }
            ?>
        </div>
    </div>
    <div class="g12 row">
        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/realtordotcom.png" alt="" style="width: 250px;"/></div>
        <div class="g2"></div>
        <div class="g7">
            <span>INSTRUCTIONS: Update the settings in your Realtor.com account so lead emails get sent to the following address. Note the "TO" field must have the following address.<br><br></span>
            <strong style="color: blue; text-decoration: underline;">realtordotcom.<?=$uuid?>@leads.seizethemarket.net</strong>
        </div>
    </div>

    <div class="g12 row">
        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/trulia.png" alt="" style="width: 250px;"/></div>
        <div class="g2"></div>
        <div class="g7">
            <span>INSTRUCTIONS: Update the settings in your Trulia account so lead emails get sent to the following address. Note the "TO" field must have the following address.<br><br></span>
            <strong style="color: blue; text-decoration: underline;">trulia.<?=$uuid?>@leads.seizethemarket.net</strong>
        </div>
    </div>

<!--    <div class="g12 row">-->
<!--        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/bombbomb.png" alt="" style="width: 250px;"/></div>-->
<!--        <div class="g6"></div>-->
<!--        <div class="g3"></div>-->
<!--    </div>-->

    <div class="g12 row">
        <div class="g3"><img src="http://cdn.seizethemarket.com/assets/images/vendors/zillow.png" alt="" style="width: 250px;"/></div>
        <div class="g2"></div>
        <div class="g7">
            <span>INSTRUCTIONS: Update the CRM settings in your Zillow account with the email address below. <br><br><a href="/admin/support/video/72" style="font-weight: bold; text-decoration: underline;">Click here to watch the Zillow Integration video tutorial.</a><br><br>Once you've connect, please contact us to confirm that Zillow lead data is being sent properly.<br><br>
            </span>
            Partner ID: <strong style="color: blue; text-decoration: underline;"><?=$uuid?></strong>
        </div>
    </div>

    <div class="g12 row">
        <div class="g3"><h1>Boomtown</h1></div>
        <div class="g2"></div>
        <div class="g7">
            <span>INSTRUCTIONS: Update the settings in your Boomtown account so lead emails get sent to the following address. Note the "TO" field must have the following address.<br><br></span>
            <strong style="color: blue; text-decoration: underline;">boomtown.<?=$uuid?>@leads.seizethemarket.net</strong>
        </div>
    </div>

    <div class="g12 row">
        <div class="g3"><h1>Abelson DISC</h1></div>
        <div class="g2"></div>
        <div class="g7">
            <span>INSTRUCTIONS: Update the settings in your Abelson DISC account so lead emails get sent to the following address. Note the "TO" field must have the following address.<br><br></span>
            <strong style="color: blue; text-decoration: underline;">abelsondisc.<?=$uuid?>@leads.seizethemarket.net</strong>
        </div>
    </div>

    <div class="g12 row">
        <div class="g3"><h1>Commissions Inc</h1></div>
        <div class="g2"></div>
        <div class="g7">
            <span>INSTRUCTIONS: Update the settings in your Commissions Inc account so lead emails get sent to the following address. Note the "TO" field must have the following address.<br><br></span>
            <strong style="color: blue; text-decoration: underline;">cinc.<?=$uuid?>@leads.seizethemarket.net</strong>
        </div>
    </div>

    <div class="g12 row">
        <div class="g3"><h1>Tiger Lead</h1></div>
        <div class="g2"></div>
        <div class="g7">
            <span>INSTRUCTIONS: Update the settings in your Tiger Lead account so lead emails get sent to the following address. Note the "TO" field must have the following address.<br><br></span>
            <strong style="color: blue; text-decoration: underline;">tigerlead.<?=$uuid?>@leads.seizethemarket.net</strong>
        </div>
    </div>
</div>