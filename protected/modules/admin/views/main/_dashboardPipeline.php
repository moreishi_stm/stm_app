<?php
$this->beginStmPortletContent(array(
	'handleTitle' => 'My Pipeline',
	'handleIconCss' => 'i_user'
));
?>
	<div class="dashboard-tab-widget">
		<?php
		Yii::import('stm_app.modules.admin.controllers.actions.transactions.PipelineAction');

		$controllerName = strtolower($transactionName);

		// Gather the data needed for the pipeline portlet view
		// $NewLeadsProvider = new CActiveDataProvider('Lead');
		// $newLeadsContent = $this->renderPartial('pipeline/_newLeads', array(
		// 	'dataProvider' => $NewLeadsProvider,
		// ), true);

		$NewProvider  = PipelineAction::getProviderByCategory('new', $transactionName);
		$newLeadCount = ($NewProvider->totalItemCount)? '(' . $NewProvider->totalItemCount . ')' : '';
		$HotAProvider = PipelineAction::getProviderByCategory('a', $transactionName);
		$BProvider    = PipelineAction::getProviderByCategory('b', $transactionName);
		$CProvider    = PipelineAction::getProviderByCategory('c', $transactionName);

		$this->widget('zii.widgets.jui.CJuiTabs', array(
			'id' => 'dashboard-pipeline',
			'tabs' => array(
				// 'New Leads (' . $NewLeadsProvider->totalItemCount . ')' => array(
				// 	'content' => $newLeadsContent,
				// 	'id' => 'new-leads-tab',
				// ),
				'New Leads '.$newLeadCount => array(
					'ajax' => $this->createUrl('/admin/'.$controllerName.'/pipeline/new'),
					'id' => 'new-leads-tab',
				),
				'Hot "A" Leads (' . $HotAProvider->totalItemCount . ')' => array(
					'ajax' => $this->createUrl('/admin/'.$controllerName.'/pipeline/a'),
					'id' => 'hot-a-tab',
				),
				'"B" Leads (' . $BProvider->totalItemCount . ')' => array(
					'ajax' => $this->createUrl('/admin/'.$controllerName.'/pipeline/b'),
					'id' => 'b-tab',
				),
				'"C" Leads (' . $CProvider->totalItemCount . ')' => array(
					'ajax' => $this->createUrl('/admin/'.$controllerName.'/pipeline/c'),
					'id' => 'c-tab',
				),
				// '"Pending" ('.$PendingProvider->totalItemCount.')'=>array(
				// 	'content'=>$pendingContent,
				// 	'id'=>'pending-tab',
				// ),
			),
		));
		?>
	</div>
<?php
$this->endStmPortletContent();
