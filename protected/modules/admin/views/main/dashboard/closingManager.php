<?
$this->breadcrumbs = array(Yii::app()->user->contact->first_name.'\'s Dashboard' => '');

Yii::app()->clientScript->registerScript('search', "
$('#listview-search form').submit(function() {
    $.fn.yiiGridView.update('closing-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<div id="content-header">
	<h1>My Tasks</h1>
</div>
<?php echo $this->renderPartial('../tasks/_listClosingTasks', array('tasksData'=>$tasksData));?>

<hr />

<div id="content-header">
	<h1>Team Closings</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php
	$this->renderPartial('../closings/_listSearchBox',array(
		'model'=>$model,
	));
?>
</div><!-- search-form -->


<?php echo $this->renderPartial('../closings/_list', array('model'=>$model));?>
<?
Yii::app()->clientScript->registerScript('onTaskDialogClose', "
$('form#task-dialog-form').submit(function() {
	setTimeout(function() {
	  $.fn.yiiGridView.update('closing-tasks-grid');
	}, 150);

	return false;
});
");
?>