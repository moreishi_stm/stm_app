<?php
// Files needed for Sales graph
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');

$js = <<<JS
    	$(window).resize(function() {
    		$('.chart').width($('#salesGraph-tab').width - 5);
    	})
JS;
Yii::app()->clientScript->registerScript('salesGraphScript', $js);

?>
<div id="dashboard">
	<h1 class="p-mt10"><?php echo Yii::app()->user->firstName; ?>'s Dashboard</h1>

	<p class="subtitle">Take Charge of Your Business</p>
	<hr style="margin:5px">

	<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
		<?
			$form = $this->beginWidget(
				'CActiveForm',
				array(
					'enableAjaxValidation' => false,
					'action'=>'/admin/buyers',
					'htmlOptions' => array('enctype' => 'multipart/form-data'),
				)
			);
			$this->renderPartial('../transactions/_listSearchBox',array('model'=>$model));
			$this->endWidget();
		?>
	</div>

	<!-- ========================================================================================================== -->
	<div class="g100">
		<div class="g6">
			<?php echo $this->renderPartial('_dashboardAppointments', array());?>
		</div>
		<div class="g6">
			<?php echo $this->renderPartial('_dashboardGoals', array());?>
		</div>
		<div class="clear"></div>
		<div class="g6">
			<?php echo $this->renderPartial('_dashboardPipeline', array('transactionName'=>'Buyers'));?>
		</div>
		<div class="g6">
			<?php echo $this->renderPartial('_dashboardTracking', array());?>
		</div>
	</div>
	<!-- ========================================================================================================== -->
</div>

<?php
// Timeblock Dialog Widget
$this->widget('admin_widgets.DialogWidget.TimeblockDialogWidget.TimeblockDialogWidget', array(
    'id' => 'timeblocking',
    'title' => 'Add Timeblock',
    'triggerElement' => '#add-timeblock, a.edit-timeblock',
));