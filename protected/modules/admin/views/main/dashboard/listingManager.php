<?php
$this->breadcrumbs = array(
	Yii::app()->user->contact->first_name.'\'s Dashboard' => array('/admin\/'.$model->transactionType
));

Yii::app()->clientScript->registerScript('search', "
$('form#transaction-list-search').submit(function() {
	$.fn.yiiGridView.update('contact-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="content-header">
	<h1>My Tasks</h1>
</div>
<?php echo $this->renderPartial('../tasks/_listListingTasks', array('model'=>$model, 'tasksData'=>$tasksData));?>

<hr />

<div id="content-header">
	<h1>Team Listings</h1>
</div>

<div id="listview-search" class="g100 p-mh0 p-pv10 grey-gradient-box">
<?php
	$this->renderPartial('../transactions/_listSearchBox',array(
		'model'=>$model,
	));
?>
</div><!-- search-form -->

<?php echo $this->renderPartial('../transactions/_listListings', array('model'=>$model));?>