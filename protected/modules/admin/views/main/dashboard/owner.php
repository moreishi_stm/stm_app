<?php
Yii::app()->clientScript->registerScript('onTaskDialogClose', "
$('form#task-dialog-form').submit(function() {
	setTimeout(function() {
	  $.fn.yiiGridView.update('task-grid');
	}, 150);

	return false;
});
");
?>
<style type="text/css">
    div#owner-dashboard {
        margin-bottom: 20px;
    }
    #owner-dashboard ul{
        list-style-type: none;
        font-size: 20px;
    }
    #owner-dashboard ul li{
        float: left;
        width: 23%;
        margin-bottom: 20px;;
    }
    #owner-dashboard ul li a{
        display: block;
        height: 200px;
        text-align: center;
        border: 3px #84a1dd solid;
        padding: 20px;
        background: #d6e5f8;
        max-width: 200px;
        -webkit-border-radius: 200px;
        -moz-border-radius: 200px;
        border-radius: 200px;
        line-height: 1;
    }
    #owner-dashboard ul li a:hover{
        border-color: #C20000;
        background: #fbecef;
    }
    #owner-dashboard ul li a span{
        top: 80px;
        position: relative;
    }
</style>
<div id="content-header">
    <h1 class="p-mt10"><?php echo Yii::app()->user->firstName; ?>'s Dashboard</h1>
</div>
<hr style="margin:10px">

<div id="owner-dashboard" class="g12">
    <ul class="g12">
        <li><a href="/admin/tracking/conversion"><span>Conversion Report</span></a></li>
        <li><a href="/admin/tracking/closings"><span>Closings Report</span></a></li>
        <li><a href="/admin/tracking/sources"><span>Source of Business Report</span></a></li>
        <li><a href="/admin/tracking/listings"><span>Listings Report</span></a></li>
        <li><a href="/admin/tracking/activities"><span>Activity Report</span></a></li>
        <li><a href="/admin/tracking/leads"><span>Leads Report</span></a></li>
        <li><a href="/admin/tracking/tasks"><span>Activity Insights Report</span></a></li>
        <li><a href="/admin/projects"><span>Projects</a></li>
    </ul>
</div>


<?php echo $this->renderPartial('../tasks/_list', array(
                                                  'dataProvider' => $tasksData,
                                                  'overdueCount' => $overdueCount,
                                                  )
); ?>
