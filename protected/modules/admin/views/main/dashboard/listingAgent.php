<?php
	// Files needed for Sales graph
	Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'flot.js');
	Yii::app()->clientScript->registerScriptFile($this->module->jsAssetsUrl . DS . 'wl_Chart.js');

	$js = <<<JS
    	$(window).resize(function() {
    		$('.chart').width($('#salesGraph-tab').width - 5);
    	})
JS;
	Yii::app()->clientScript->registerScript('salesGraphScript', $js);

	Yii::app()->clientScript->registerScript('search', "
$('form#transaction-list-search').submit(function() {
	$.fn.yiiGridView.update('task-grid', {
		data: $(this).serialize()
	});
	return false;
});
"
	);

	Yii::app()->clientScript->registerScript('onTaskDialogClose', "
$('form#task-dialog-form').submit(function() {
	setTimeout(function() {
	  $.fn.yiiGridView.update('task-grid');
	}, 150);

	return false;
});
"
	);
?>
	<div id="dashboard">
		<h1 class="p-mt10"><?php echo Yii::app()->user->firstName; ?>'s Dashboard</h1>

		<p class="subtitle">Take Charge of Your Business</p>
		<hr style="margin:5px">
		<?php echo $this->renderPartial('../tasks/_list', array(
				'dataProvider' => $tasksData,
				'overdueCount' => $overdueCount,
			)
		); ?>
		<hr style="margin:5px">
		<!-- ========================================================================================================== -->
		<div class="g100">
			<div class="g8">
				<?php echo $this->renderPartial('_dashboardTimeblocking', array()); ?>
			</div>
			<div class="g4">
				<?php echo $this->renderPartial('_dashboardGoals', array()); ?>
			</div>
		</div>
		<!-- ========================================================================================================== -->
	</div>

<?php
	// Timeblock Dialog Widget
	$this->widget('admin_widgets.DialogWidget.TimeblockDialogWidget.TimeblockDialogWidget', array(
			'id' => 'timeblocking',
			'title' => 'Add Timeblock',
			'triggerElement' => '#add-timeblock',
		)
	);