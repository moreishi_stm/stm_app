<style>
    body {
        background: #FDEEEE;
        margin: 0;
        padding: 20px;
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: #666666;
    }
    .error_page {
        width: 600px;
        padding: 50px;
        margin: auto;
    }
</style>
<?php
$this->pageTitle=Yii::app()->name . ' - Error';
?>
<div class="error_page">
    <img src="http://cdn.seizethemarket.com/assets/images/sad_face.gif">
    <h1>We're sorry...</h1>
    <?php echo (YII_DEBUG)? "<h2>Error ".$error['code']."</h2><h3>Error Message: ".CHtml::encode($error['message'])."</h3>" : '';?>

    <?php if ($error['code'] == '404'): ?>
        <p>The page you are looking for cannot be found.</p>
    <?php else: ?>
    	<p>We had an issue loading the page you requested.</p>
    <?php endif; ?>

    <br>
    <p><a href="javascript: window.history.go(-1)">Return to the Previous Page</a></p>
</div>