<div class="button-container">
	<button id="view-all-timeblocks" class="text" type="button" onclick="location.href='<?php echo $this->createUrl('/admin/tracking/timeblocking'); ?>'"><em class="icon i_stm_search"></em>View All Timeblocks
	</button>
	<button id="add-timeblock" class="text" type="button"><em class="icon i_stm_add"></em>Add Timeblock</button>
</div>
<div class="ui-button-panel">
	<?php
		$this->widget('StmGridView', array(
				'id' => 'timeblocking-grid',
				'dataProvider' => $dataProvider,
				'enableSorting' => true,
				'enablePagination' => true,
				'summaryCssClass' => 'summary hidden',
				'summaryText' => '{count}',
				'columns' => array(
					array(
						'name' => 'date',
						'type' => 'date',
					),
					array(
						'name' => 'start_time',
						'type' => 'time',
					),
					array(
						'name' => 'end_time',
						'type' => 'time',
					),
					array(
						'type' => 'raw',
						'name' => 'Hours',
						'value' => 'round($data->hours, 1)',
					),
					array(
						'class' => 'CLinkColumn',
						'label' => 'Edit',
						'urlExpression' => 'Yii::app()->createUrl("/admin/tracking/viewTimeblock", array(
                    "id" => $data->id,
                ))',
						'htmlOptions' => array(
							'style' => 'text-align: center',
						),
						'linkHtmlOptions' => array(
							'class' => 'edit-timeblock button gray icon i_stm_edit',
						),
					),
				),
			)
		);
	?>
</div>