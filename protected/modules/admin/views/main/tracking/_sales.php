<?
//@todo: this needs to be moved to the external action, putting it here while developing

$stackedChart = true;
$closingCount = Closings::getYTDclosingCountByMonth(Yii::app()->user->id);
foreach($closingCount as $key=>$value) {
	$closingData .= ($closingData)? ',['.$key.','.$value.']' : '['.$key.','.$value.']';
}
$closingData = '['.$closingData.']';



$annualClosings = Goals::model()->findByContactId(Yii::app()->user->id)->annualClosings;
foreach(MarketTrends::model()->trends as $key=>$value) {
	$value = round($value*$annualClosings);
	if($stackedChart) {
		if($closingCount[$key] == $value) {
			$value = 0;
		} else{
			$value = $value - $closingCount[$key];
		}
	}
    $goalData .= ($goalData)? ',' : '';
	$goalData .= '['.$key.','.$value.']';
}
$goalData = '['.$goalData.']';

$js = <<<JS
        $('.chart').wl_Chart({
            flot: {
                bars:{
                    fill:true,
                    fillColor: {
                        colors: [ { opacity: .7 }, { opacity: .7 } ]
                    }
                },
                colors: ["#18CC18","#DE4949"],
                yaxis: { tickDecimals: 0}
            },
            xlabels:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            data:[{
                label:'Actual',
                data: $closingData //[[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0]]//[[0,1],[1,2],[2,2],[3,3],[4,3],[5,3],[6,4],[7,4],[8,3],[9,3],[10,3],[11,0]]
            },{
                //this 2nd row Goal values add on top of the Actual, so need to represent the difference between the two
                label:'Goal',
                data:$goalData //[[0,1],[1,1],[2,1],[3,0],[4,1],[5,1],[6,0],[7,0],[8,1],[9,1],[10,0],[11,2]]
            }]
        });
JS;


Yii::app()->clientScript->registerScript('_salesViewGraphJs', $js);
?>
<!--data-type="bars"-->
<!--data-stack="--><?php //echo ($stackedChart)? 'true':'false'; ?><!--"-->
<div style="padding: 10px;">
    <table id="sales-graph" class="chart"  data-fill="true"  data-height="218" data-stack="true" data-tooltip="false" data-fill="true" width="75%"></table>
</div>