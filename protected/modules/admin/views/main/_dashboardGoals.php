<?
	$this->beginStmPortletContent(array(
			'handleTitle' => 'Monthly Goals',
			'handleIconCss' => 'i_strategy'
		)
	);
	$model = Goals::model()->findbyPk(Yii::app()->user->settings->primary_goal_id);
?>
<div class="dashboard-monthly-goals">
	<?
		if (isset($model->monthlyGoalProvider)) {
			$this->widget('zii.widgets.grid.CGridView', array(
					'id' => 'monthly-goals-grid',
					'dataProvider' => $model->monthlyGoalProvider,
					'enableSorting' => false,
					'enablePagination' => false,
					'summaryText' => '',
					'htmlOptions' => array('class' => 'datatables'),
					'columns' => array(
						array(
							'name' => '',
							'value' => '$data["type"].":"',
							'type' => 'raw',
							'htmlOptions' => array('class' => 'text-right strong'),
						),
						array(
							'name' => 'Actual',
							'type' => 'raw',
							'value' => '$data["actual"]',
						),
						array(
							'name' => 'Due',
							'type' => 'raw',
							'value' => '$data["due"]',
						),
						array(
							'name' => 'Goal',
							'type' => 'raw',
							'value' => '$data["goal"]',
						),
						array(
							'name' => 'Track',
							'type' => 'raw',
							'value' => '$data["track"]',
						),
					),
				)
			);
		} else {
			echo '<div><h3>You do not have Goals setup.<br /><a href="/admin/goals" style="color:#D20000; font-weight:bold;">Click here</a> to setup Goals now.</h3></div>';
		}
	?>
</div>
<?php $this->endStmPortletContent(); ?>
