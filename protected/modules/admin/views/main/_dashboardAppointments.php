<?
	$css = <<<CSS
.ui-button-panel {
	height: 168px;
}
CSS;
	Yii::app()->clientScript->registerCss('timeblocking', $css);

	//Need to add this function to global JS file...
	$js = <<<JS
$.fn.updateRecordCount = function() {
  return this.each(function() {
	 var type = $(this).attr('class');
	 if ($("table tr",this).length != 0 || type == 'datatables') {
		     var newCount = $('.summary',this).text();
		     var tabID = $('.summary',this).parent().parent().parent().attr('id');
		     $('a[href^=#'+tabID+']').text($('a[href^=#'+tabID+']').text().replace(/[\\d.]+/g, newCount));
	 }
  });
};
JS;

	Yii::app()->clientScript->registerScript('tabCountUpdater', $js);

	$this->beginStmPortletContent(array(
			'handleTitle' => 'Appointments this Month',
			'handleIconCss' => 'i_tick'
		)
	); ?>
<div class="dashboard-tab-widget">
	<?
		$countSigned = Appointments::model()->byCurrentUser()->signed()->met()->setForCurrentMonth()->count();
		$AppointmentsMetProvider = new CActiveDataProvider(Appointments::model()->forCurrentUser()->setForCurrentMonth()->setForDateAsc());  //byOrForCurrentUser()
		$appointmentsMetContent = $this->renderPartial('../tracking/_appointmentsMetMini', array(
				'DataProvider' => $AppointmentsMetProvider,
			), true
		);

		$AppointmentsUpcomingProvider = new CActiveDataProvider(Appointments::model()->forCurrentUser()->setOnCurrentMonth()->notMet()->setForDateAsc());  //byOrForCurrentUser()
		$appointmentsUpcomingContent = $this->renderPartial('../tracking/_appointmentsSetMini', array(
																						   'DataProvider' => $AppointmentsUpcomingProvider,
																						   ), true
		);

		$AppointmentsSetProvider = new CActiveDataProvider(Appointments::model()->forCurrentUser()->setOnCurrentMonth()->setForDateAsc());
		$appointmentsSetContent = $this->renderPartial('../tracking/_appointmentsSetMini', array(
																					 'DataProvider' => $AppointmentsSetProvider,
																					 ), true
		);

		$this->widget('zii.widgets.jui.CJuiTabs', array(
				'tabs' => array(
					'Signed /Met Appts ('.$countSigned.'/' . $AppointmentsMetProvider->totalItemCount . ')' => array(
						'content' => $appointmentsMetContent,
						'id' => 'dashboard-appointments-signed-tab',
					),
					'Upcoming Appts (' . $AppointmentsUpcomingProvider->totalItemCount . ')' => array(
						'content' => $appointmentsUpcomingContent,
						'id' => 'dashboard-appointments-upcoming-tab',
					),
					'New Set Appt (' . $AppointmentsSetProvider->totalItemCount . ')' => array(
						'content' => $appointmentsSetContent,
						'id' => 'dashboard-appointments-set-tab',
					),
				),
				'id' => 'dashboard-timeblockings',
				'options' => 'js:{\'select\':function(event,ui) {
				var $tabs = ui.tab.toString();
  				$($tabs.substr($tabs.lastIndexOf(\'#\'),$tabs.length)).updateRecordCount();
			}}',
			)
		);
	?>
</div>
<? $this->endStmPortletContent(); ?>
