<style>
    body {

    <?=(Yii::app()->user->checkAccess('owner') || Yii::app()->user->checkAccess('agent') || Yii::app()->user->checkAccess('lender'))? '' : 'background: #FDEEEE;'?>
        margin: 0;
        background: #FDEEEE;
        padding: 20px;
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: #666666;
    }
    .error_page {
        width: 600px;
        padding: 50px;
        margin: auto;
    }
</style>
<?php
$this->title='Access Denied';
?>
<div class="error_page">
    <img src="http://cdn.seizethemarket.com/assets/images/sad_face.gif">
    <h1>We're sorry...</h1>
    <h2>It looks like you don't have access to this section yet</h2>
    <p><?php echo (Yii::app()->user->checkAccess('owner'))? "Please contact your Seize the Market representative for access to this area." : "Please contact your supervisor for access to this area.";?></p>
    <p><a href="javascript: window.history.go(-1)">Return to the Previous Page</a></p>
</div>