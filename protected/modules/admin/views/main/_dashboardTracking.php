<? $this->beginStmPortletContent(array(
	'handleTitle'=>'Tracking',
	'handleIconCss'=>'i_graph'
)); ?>
	<div class="dashboard-tab-widget">
		<?
			$TimeblockingProvider = new CActiveDataProvider(Timeblockings::model()->byCurrentUser()->byCurrentMonth()->desc());
			$timeblockingContent = $this->renderPartial('tasks/_timeblocking', array(
																			   'dataProvider' => $TimeblockingProvider,
																			   ), true
			);

			$this->widget('zii.widgets.jui.CJuiTabs', array(
				'tabs'=>array(
			        'Annual Sales '.$documentsCount=>array('content'=>$this->renderPartial('tracking/_sales', array(), true),
			        	'id'=>'salesGraph-tab',
			        ),
					'Timeblocks (' . $TimeblockingProvider->totalItemCount . ')' => array(
						'content' => $timeblockingContent,
						'id' => 'timeblocking-tab',
					),
			        // 'My Goals '.$documentsCount=>array('ajax'=>'/admin/goals/dashboard',
			        // 	'id'=>'goals-tab',
			        // ),
			        // 'Closed (18)'.$documentsCount=>array('ajax'=>'/admin/closings/closed/'.Yii::app()->user->id,
			        // 	'id'=>'closed-tab',
			        // ),
			        // 'Conversion '.$documentsCount=>array('ajax'=>'/admin/tracking/conversion/'.Yii::app()->user->id,
			        // 	'id'=>'conversion-tab',
			        // ),
			    ),
				'id'=>'dashboard-tracking',
			));
		?>
	</div>
<?php $this->endStmPortletContent(); ?>
