<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 1.0
 */
$this->beginStmPortletContent(array(
		'handleTitle'=>'Timeblocking',
		'handleIconCss'=>'i_graph'
	));
?>
<div class="dashboard-tab-widget">
	<?
	$TimeblockingProvider = new CActiveDataProvider(Timeblockings::model()->byCurrentUser()->byCurrentMonth()->desc());
	$timeblockingContent = $this->renderPartial('tasks/_timeblocking', array(
			'dataProvider' => $TimeblockingProvider,
		), true
	);

	$this->widget('zii.widgets.jui.CJuiTabs', array(
			'tabs'=>array(
				'Timeblocks (' . $TimeblockingProvider->totalItemCount . ')' => array(
					'content' => $timeblockingContent,
					'id' => 'timeblocking-tab',
				),
			),
			'id'=>'dashboard-tracking',
		));
	?>
</div>
<?php $this->endStmPortletContent(); ?>
