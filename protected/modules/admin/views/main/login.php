<style type="text/css">
    body {
        background-color: transparent;
    }
</style>
<section id="login">
	<header class="login">
		<div id="logo">
			<?php echo CHtml::image('http://cdn.seizethemarket.com/assets/images/logo_admin.png', 'Seize the Market', $htmlOptions=array(
				'class'=>'logo',
                'style' => 'max-width: 100%;',
			)); ?>
		</div>
	</header>
	<section id="content" class="g12">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'login-form',
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); ?>
        <div class="g12 p-ml10">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email', $htmlOptions=array('class'=>'g11')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
        <div class="g12 p-ml10">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password', $htmlOptions=array('class'=>'g11')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
        <div class="g12 p-ml10">
            <?php echo $form->checkBox($model, 'rememberMe', $htmlOptions=array(
                'id'=>'remember',
            )); ?>
            <?php echo $form->labelEx($model, 'rememberMe'); ?>
        </div>
        <div class="g12 p-tr p-ml10 p-pb10">
            <button class="g11 default submit p-fl">Login</button>
        </div>
        <div class="g11 p-fr p-tr p-pt10 p-pb10 p-mr10" style="margin-top: 10px;">
            <a href="/forgotPassword">Forgot Password</a>
        </div>
        <?php $this->endWidget(); ?>
    </section>
    <footer class="g12">
        <p class="c">&copy; <?php echo date("Y")?> Seize The Market, Inc.</p>
    </footer>
</section>