<?php
$this->breadcrumbs = array(
    'News',
);
?>
<h1>News & Announcements</h1>
<h3>Below are the list of new features and announcements for Seize the Market.</h3>
<h3>P.S. Who do you know that we should talk to that is looking for an all-in-one real estate system? Email us a Info@SeizetheMarket.com</h3>
<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'news-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'datatables',
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'Date',
                'value' => 'Yii::app()->format->formatDate($data["date"])',
                'htmlOptions' => array('style' => 'width:100px; height: 50px;'),
            ),
            array(
                'type' => 'raw',
                'name' => '# New Updates',
                'value' => '$data["updateCount"]',
                'htmlOptions' => array('style' => 'width:100px; height: 50px; text-align: center;'),
            ),
            array(
                'type' => 'raw',
                'name' => 'Description',
                'value' => '$data["description"]',
            ),
        ),
    )
);