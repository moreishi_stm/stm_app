<?php
/**
 * Index Action for Index Controller
 *
 */
class IndexAction extends CAction {

    public function run()
    {
        // Render view
        $this->controller->render('index');
    }
}