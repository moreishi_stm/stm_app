<?php
/**
 * Index controller for admin module
 */
class IndexController extends AdminController
{

    public function actions()
    {
        return array(
            'index'        =>'stm_app.modules.admin2.controllers.actions.index.IndexAction'
        );
    }
}