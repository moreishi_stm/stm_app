<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <title>Admin 2</title>

        <link rel="stylesheet" href="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300">

        <link rel="stylesheet" href="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/css/ace.min.css" id="main-ace-style">

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/css/ace-part2.min.css">
        <![endif]-->

        <link rel="stylesheet" href="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/css/ace-skins.min.css">
        <link rel="stylesheet" href="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/css/ace-rtl.min.css">

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/css/ace-ie.min.css" />
        <![endif]-->

        <style type="text/css">
            * {
                font-family: arial;
            }
        </style>
    </head>
    <body class="skin-2">

        <!-- Top Navigation -->
        <div id="navbar" class="navbar navbar-default">
            <div class="navbar-container" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" style="width: 90px;">
                    <span class="sr-only">Toggle sidebar</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span style="color: white; margin-left: 24px; position: relative; top: -7px; font-size: 14px;">Menu</span>
                </button>
                <div class="navbar-header pull-left">
                    <a href="#" class="navbar-brand">
                        <small>
                            <i class="fa fa-home"></i>
                            Seize The Market
                        </small>
                    </a>
                </div>

                <form class="navbar-form navbar-left form-search" role="search">
                    <div class="form-group">
                        <input type="text" placeholder="Search" style="width: 250px;">
                    </div>

                    <button type="button" class="btn btn-xs btn-info2">
                        <i class="ace-icon fa fa-search icon-only bigger-110"></i>
                    </button>
                </form>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                Hi Christine
                                <i class="fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user"></i>
                                        Profile
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-cogs"></i>
                                        Settings
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-sign-out"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="sidebar responsive" id="sidebar">
            <ul class="nav nav-list">
                <li class="active">
                    <a href="#">
                        <i class="menu-icon fa fa-dashboard"></i>
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-shopping-cart"></i>
                        <span class="menu-text">Buyers</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-dollar"></i>
                        <span class="menu-text">Sellers</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-tag"></i>
                        <span class="menu-text">Closings</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-group"></i>
                        <span class="menu-text">Contacts</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-file-o"></i>
                        <span class="menu-text">Documents</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-line-chart"></i>
                        <span class="menu-text">Reports</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-wrench"></i>
                        <span class="menu-text">Tools</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-cogs"></i>
                        <span class="menu-text">Settings</span>
                    </a>
                </li>
            </ul>
            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>
        </div>

        <!-- Begin Main Content -->
        <div class="main-content">
            <div class="page-content">
                <div class="breadcrumbs" id="breadcrumbs">

                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">Home</a>
                        </li>
                        <li class="active">Dashboard</li>
                    </ul><!-- /.breadcrumb -->

                </div>

                <div class="page-header">
                    <h1>
                        Some User's Dashboard
                        <small>
                            <i class="fa fa-angle-double-right"></i> Take Charge of Your Business
                        </small>
                    </h1>
                </div>
                <?=$content?>
            </div>
        </div>

        <!-- JavaScript Includes -->
        <script src="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/js/ace-extra.min.js"></script>

        <!--[if lte IE 8]>
            <script src="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/js/html5shiv.min.js"></script>
            <script src="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/js/respond.min.js"></script>
        <![endif]-->

        <!--[if !IE]> -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <!-- <![endif]-->

        <!--[if IE]>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <![endif]-->

        <!--[if !IE]> -->
        <script type="text/javascript">
            window.jQuery || document.write("<script src='http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/js/jquery.min.js'>"+"<"+"/script>");
        </script>
        <!-- <![endif]-->

        <!--[if IE]>
        <script type="text/javascript">
            window.jQuery || document.write("<script src='http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/js/jquery1x.min.js'>"+"<"+"/script>");
        </script>
        <![endif]-->

        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <script src="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/js/ace-elements.min.js"></script>
        <script src="http://cdn.seizethemarket.com/assets/lib/admin-theme/assets/js/ace.min.js"></script>
    </body>
</html>