<style type="text/css">
    #dashboard-search>.row {
        margin-bottom: 10px;
    }
</style>
<div class="container">
    <form role="form" id="dashboard-search">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="first_name">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" name="last_name" id="last_name">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="user@example.com">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" id="phone" placeholder="(999) 123-1234">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="">- Select a Status -</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="assigned_to">Assigned To</label>
                <select name="assigned_to" id="assigned_to" class="form-control">
                    <option value="">- Select an Person -</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="has_phone">Has Phone</label>
                <select name="has_phone" id="has_phone" class="form-control">
                    <option value=""></option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="last_follow_up">Last Follow-Up Days</label>
                <select name="last_follow_up" id="last_follow_up" class="form-control">
                    <option value=""></option>
                    <option value="7">7+</option>
                    <option value="14">14+</option>
                    <option value="30">30+</option>
                    <option value="45">45+</option>
                    <option value="60">60+</option>
                    <option value="90">90+</option>
                    <option value="120">120+</option>
                    <option value="150">150+</option>
                    <option value="180">180+</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label for="sort">Sort</label>
                <select name="sort" id="sort" class="form-control">
                    <option value=""></option>
                    <option value="">Added: Newest first</option>
                    <option value="">Added: Oldest first</option>
                    <option value="">Last Login: Newest first</option>
                    <option value="">Last Login: Oldest first</option>
                    <option value="">Last Activity: Newest first</option>
                    <option value="">Last Activity: Oldest first</option>
                </select>
            </div>
            <div class="col-md-1">
                <label for="date_start">Start</label>
                <input type="text" name="date_start" class="form-control">
            </div>
            <div class="col-md-1">
                <label for="date_start">End</label>
                <input type="text" name="date_end" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="no_phone_call">No Phone Call</label>
                <select name="no_phone_call" id="no_phone_call" class="form-control">
                    <option value=""></option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="no_activity">No Activity</label>
                <select name="no_activity" id="no_activity" class="form-control">
                    <option value=""></option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="no_saved_search">No Saved Search</label>
                <select name="no_saved_search" id="no_saved_search" class="form-control">
                    <option value=""></option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-5 col-md-2 text-center">
                <button type="submit" name="search" class="btn btn-success">
                    <i class="fa fa-search"></i>
                    Search
                </button>
            </div>
        </div>
    </form>

    <div class="row" style="margin-top: 50px;">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <i class="fa fa-check"></i> Appointments This Month
                </div>
                <div class="panel-body">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active">
                            <a href="#tab-appts-signed-met" role="tab" data-toggle="tab">Signed / Met (0/0)</a>
                        </li>
                        <li>
                            <a href="#tab-appts-upcoming" role="tab" data-toggle="tab">Upcoming (0)</a>
                        </li>
                        <li>
                            <a href="#tab-appts-new-set" role="tab" data-toggle="tab">New Set (0)</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-appts-signed-met">Signed / Met</div>
                        <div class="tab-pane" id="tab-appts-upcoming">Upcoming</div>
                        <div class="tab-pane" id="tab-appts-new-set">New Set</div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <i class="fa fa-dollar"></i> Monthly Goals
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th class="text-center">Actual</th>
                                <th class="text-center">Due</th>
                                <th class="text-center">Goal</th>
                                <th class="text-center">Track</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-right"><b>Lead Gen:</b></td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center"><i class="fa fa-times"></i></td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>Dials:</b></td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center"><i class="fa fa-times"></i></td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>Contacts:</b></td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center"><i class="fa fa-times"></i></td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>Appts:</b></td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center"><i class="fa fa-times"></i></td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>Signed:</b></td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center"><i class="fa fa-times"></i></td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>Contracts:</b></td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center"><i class="fa fa-times"></i></td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>Closings:</b></td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center">0</td>
                                <td class="text-center"><i class="fa fa-check"></i></td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>Income:</b></td>
                                <td class="text-center">&nbsp;</td>
                                <td class="text-center">$1,337</td>
                                <td class="text-center">$42</td>
                                <td class="text-center"><i class="fa fa-times"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 25px;">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <i class="fa fa-user"></i> My Pipeline
                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <i class="fa fa-line-chart"></i> Tracking
                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>

</div>