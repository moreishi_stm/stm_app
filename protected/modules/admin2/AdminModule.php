<?php
Yii::setPathOfAlias('stm_app_root', SITE_ROOT . DS . 'stm_app');

class AdminModule extends CWebModule
{

//    public $layout = 'default';

    /**
     * Init
     *
     * Bootstraps everything
     * @return mixed (See parent init for details)
     */
    public function init()
    {
        // Set layout path and default path
        $this->layoutPath =  Yii::getPathOfAlias('admin2_module') . DS . 'views' . DS . 'layouts';
        $this->layout = 'default';

        // Import things we need that are module specific
        $this->setImport(
            array(
                'admin2_module.models.*',
                'admin2_module.components.*'
            )
        );

        // Set error handler?
        $this->setComponent('errorHandler', array(
            'errorAction' => '/admin2/error/index'
        ));
//
//        // Define error handler for this module
//        Yii::app()->setComponents(
//            array(
//                'errorHandler' => array(
//                    'errorAction' => '/admin/main/error',
//                ),
//            )
//        );

        // Chain parent init
        return parent::init();
    }

}
