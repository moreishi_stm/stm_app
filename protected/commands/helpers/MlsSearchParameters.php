<?php

	/**
	 * A uniform class representation for us to use to encapsulate the various parameters
	 * by which a remote MLS listing service may permit us to search by.
	 *
	 * @author Will Madison (will@willmadison.com)
	 */
	class MlsSearchParameters {

		private $mlsBoard;
		private $rawCriterion;
		private $propertyType;

		function __construct() {
		}

		public function setMlsBoard(MlsBoards $mlsBoard) {
			$this->mlsBoard = $mlsBoard;

			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getMlsBoard() {
			return $this->mlsBoard;
		}


		public function setPropertyType(MlsPropertyTypes $propertyType) {
			$this->propertyType = $propertyType;

			return $this;
		}

		public function getPropertyType() {
			return $this->propertyType;
		}

		public function setRawCriterion($rawCriterion) {
			$this->rawCriterion = $rawCriterion;

			return $this;
		}

		public function getRawCriterion() {
			return $this->rawCriterion;
		}

		function __toString() {
			return 'MlsSearchParameters{rawCriterion = ' . $this->rawCriterion . ', mlsBoard=' . print_r($this->mlsBoard->attributes, true) . ', propertyType=' . print_r($this->propertyType->attributes, true);
		}
	}
