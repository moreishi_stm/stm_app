<?php

	/**
	 * Object representation of the various
	 *
	 * @author Will Madison (will@willmadison.com)
	 */
	class MlsCollectionType {

		//Current known Collection Types
		const PROPERTIES = 'properties';

		const AGENTS = 'agents';

		const OFFICES = 'offices';

		public static $KNOWN_TYPES = array(
			self::PROPERTIES,
			self::AGENTS,
			self::OFFICES
		);

		/**
		 * @var string the type of collection type this instance is (i.e. this should be one of the known types above).
		 */
		private $type;

		/**
		 * @var string The board name under which collections of this particular type are stored within STM.
		 */
		private $boardName;

		/**
		 * @var string The collection type name used in the remote system (i.e. remote Rets or FTP server).
		 */
		private $remoteName;


		function __construct($type = 'Unknown', $boardName = 'Unknown', $remoteName = 'Unknown') {
			$this->type = $type;
			$this->boardName = $boardName;
			$this->remoteName = $remoteName;
		}

		public function setType($type) {
			$this->type = $type;
		}

		public function getType() {
			return $this->type;
		}

		public function setBoardName($localName) {
			$this->boardName = $localName;
		}

		public function getBoardName() {
			return $this->boardName;
		}

		public function setRemoteName($remoteName) {
			$this->remoteName = $remoteName;
		}

		public function getRemoteName() {
			return $this->remoteName;
		}

		public function __toString() {
			return 'MlsCollectionType{type = ' . $this->type . ' ,boardName = ' . $this->boardName . ' ,remoteName = ' . $this->remoteName . '}';
		}
	}
