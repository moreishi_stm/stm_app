<?php
	/**
	 * Simple container class for housing the parameters that the sync command was given in such a way
	 * as can be passed through to any auxiliary classes that may also be interested in them. As additional
	 * options are added to our commands, we should be prudent to ensure they are additionally reflected here.
	 *
	 * @author Will Madison (will@willmadison.com)
	 */
	class MlsSyncConfig {

		/**
		 * @var AbstractMlsSync a reference to the sync process which is responsible for creation of this configuration.
		 */
		private $mlsSyncProcess;

		/**
		 * @var boolean a flag which determines whether or not the sync process should perform a full dump (i.e. drop and creation of tables);
		 */
		private $doFullDump;

		/**
		 * @var mixed the listing id which we are to restrict our sync to.
		 */
		private $listingId;

		/**
		 * @var integer the number of days (relative to today) that we'd like to begin the time window limiting the scope of our sync.
		 */
		private $startDateOffset;

		/**
		 * @var integer the number of days (relative to today) that we'd like to end our time window limiting the scope of our sync.
		 */
		private $endDateOffset;

		/**
		 * @var integer the number of records we're limiting this sync run to.
		 */
		private $recordLimit;

		/**
		 * @var integer the property type identifier which we are to limit ourselves to.
		 */
		private $propertyTypeId;

        /**
         * @var boolean
         */
        private $useLocalDataOnly;


		function __construct() {
		}

		public function setMlsSyncProcess(AbstractMlsSync $mlsSyncProcess) {
			$this->mlsSyncProcess = $mlsSyncProcess;

			return $this;
		}

		/**
		 * @return AbstractMlsSync the sync process responsible for the creation of this configuration instance.
		 */
		public function getMlsSyncProcess() {
			return $this->mlsSyncProcess;
		}

		public function setDoFullDump($doFullDump) {
			$this->doFullDump = $doFullDump;

			return $this;
		}

		public function getDoFullDump() {
			return $this->doFullDump;
		}

		public function setStartDateOffset($beginDate) {
			$this->startDateOffset = $beginDate;

			return $this;
		}

		public function getStartDateOffset() {
			return $this->startDateOffset;
		}

		public function setEndDateOffset($endDate) {
			$this->endDateOffset = $endDate;

			return $this;
		}

		public function getEndDateOffset() {
			return $this->endDateOffset;
		}

		public function setRecordLimit($recordLimit) {
			$this->recordLimit = $recordLimit;

			return $this;
		}

		public function getRecordLimit() {
			return $this->recordLimit;
		}

		public function isTimeBoundSync() {
			return !empty($this->startDateOffset) && !empty($this->endDateOffset);
		}

		public function isSingleListingSync() {
			return !empty($this->listingId);
		}

		public function hasRecordLimit() {
			return $this->recordLimit > 0;
		}

		public function setListingId($listingId) {
			$this->listingId = $listingId;

			return $this;
		}

		public function getListingId() {
			return $this->listingId;
		}

		public function setPropertyTypeId($propertyTypeId) {
			$this->propertyTypeId = $propertyTypeId;

			return $this;
		}

		public function getPropertyTypeId() {
			return $this->propertyTypeId;
		}

        public function setUseLocalDataOnly($useLocalDataOnly) {
            $this->useLocalDataOnly = $useLocalDataOnly;

            return $this;
        }

        public function getUseLocalDataOnly() {
            return $this->useLocalDataOnly;
        }

		function __toString() {
			return 'MlsSyncConfig{' . 'doFullDump = ' . $this->doFullDump . ',listingId = ' . $this->listingId . ',startDateOffset = ' . $this->startDateOffset . ',endDateOffset = ' . $this->endDateOffset
			. ',propertyTypeId = ' . $this->propertyTypeId . ',recordLimit = ' . $this->recordLimit . '}';
		}
	}
