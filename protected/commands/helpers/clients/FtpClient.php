<?php

/**
 * Feed client for a remote resource whose listings are to be retrieved via
 * FTP, typically in a CSV format.
 *
 * Some notes on how we may accomplish this: http://goo.gl/so54m
 *
 * @author Will Madison (will@willmadison.com)
 */
interface FtpClient extends MlsFeedClient {

}
