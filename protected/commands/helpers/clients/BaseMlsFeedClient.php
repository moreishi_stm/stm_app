<?php
/**
 * Abstract base implementation for methods that can possibly be shared by all MlsFeedClients.
 *
 * @author Will Madison (will@willmadison.com)
 */ 
abstract class BaseMlsFeedClient implements MlsFeedClient {

	/**
	 *  Immediately logs the given message to the console rather than buffering the messages in
	 *  memory.
	 *
	 * @param string $message the message to be logged
	 * @param string $level level of the message (ex: 'trace', 'error', 'warning', etc.)
	 * @param string $scope the "scope" of the message (typically the class from which it originated).
	 */
	protected function log($message, $level = 'info', $scope = __CLASS__) {
		$now = new DateTime();

		echo $now->format('Y-m-d H:i:s') . " - [$level] $scope : $message\n";
	}
}
