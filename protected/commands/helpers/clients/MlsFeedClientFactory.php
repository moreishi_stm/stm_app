<?php
/**
 * Simple Factory class for returning concrete instances of an MlsFeedClient.
 *
 * @author Will Madison (will@willmadison.com)
 */ 
class MlsFeedClientFactory {

	//TODO: Ideally I would like some type safety on these flags, something like a Java enum equivalent
	const PH_RETS_IMPLEMENTATION = 1;
	const DEFAULT_FTP_IMPLEMENTATION = 2;

	/**
	 * Factory method for retrieving new MlsFeed Clients, typically for synchronization purposes.
	 *
	 * @param int $implementation the identifier for the specific client implementation desired.
	 * @return MlsFeedClient a new Feed client given the request implementation is supported, null otherwise.
	 */
	public static function newInstance($implementation = self::PH_RETS_IMPLEMENTATION) {
		$client = null;

		switch ($implementation) {
			case self::PH_RETS_IMPLEMENTATION:
				$client = new PhRetsFeedClient();
				break;
			case self::DEFAULT_FTP_IMPLEMENTATION:
				break;
		}

		return $client;
	}

}
