<?php


/**
 * PhRets specific MlsFeedClient implementation.
 *
 * Developer's Note:
 *        Right now for this particular client there are a lot of places where the assumption is that
 *        the collection type with which we're dealing is a property strictly. This is because as of
 *        this writing the only viable usage example makes the same assumption. In the interest of time
 *        we'll proceed with that same assumption until we have a good working example and we'll make it
 *        more robust afterwards.
 *
 * @author Will Madison (will@willmadison.com)
 */
class PhRetsFeedClient extends BaseMlsFeedClient implements RetsClient
{

    /**
     * @var phRETS an instance of the phRETS client developed by Troy Davidson
     * @see http://goo.gl/dWKqW
     */
    private $client;

    /**
     * @var bool flag which determines whether or not this client is currently connected to the remote resource.
     */
    private $connected = false;


    function __construct()
    {
        $this->client = new phRETS();
        $this->client->SetParam('compression_enabled', true); //for Photos, enables gzip compression
    }

    public function remoteLookupByPk(MlsCollectionType $type, MlsSearchParameters $parameters, $key, $identifier)
    {
        $recordsFound = array();
        $record = null;
        $criteria = '(' . $key . '=' . $identifier . ')';

        $PropertyTypeBoardLus = $this->getMlsPropertyTypeBoardLu($parameters);

        foreach ($PropertyTypeBoardLus as $PropertyTypeBoardLu) {
            if (!empty($PropertyTypeBoardLu)) {
                $records = $this->client->Search($type->getRemoteName(), $PropertyTypeBoardLu->feed_table_name, $criteria);
                $recordsFound = array_merge($recordsFound, $records);
            }
        }

        if (!empty($recordsFound)) {
            $record = current($recordsFound);
        }

        return $record;
    }

    public function nativeLookupByPk(MlsCollectionType $type, $identifier)
    {
        $record = null;
        $stmMlsModel = null;

        switch ($type->getType()) {
            case MlsCollectionType::AGENTS:
                $stmMlsModel = MlsAgents::model($type->getBoardName());
                break;
            case MlsCollectionType::PROPERTIES:
                $stmMlsModel = MlsProperties::model($type->getBoardName());
                break;
            case MlsCollectionType::OFFICES:
                $stmMlsModel = MlsOffices::model($type->getBoardName());
                break;
        }

        if (!empty($stmMlsModel)) {
            $record = $stmMlsModel->findByPk($identifier);
        }

        return $record;
    }

    public function retrieveActiveRecords(MlsCollectionType $type, MlsSearchParameters $parameters, $priceField, $listingIdField)
    {
        $activeMlsRecords = array();

        $PropertyTypeBoardLus = $this->getMlsPropertyTypeBoardLu($parameters);

        if (!empty($PropertyTypeBoardLus)) {
            foreach ($PropertyTypeBoardLus as $PropertyTypeBoardLu) {
                $activeListings = $this->client->Search($type->getRemoteName(), $PropertyTypeBoardLu->feed_table_name, '(' . $priceField . '=000000-99000000)', array('Select' => $listingIdField));

                foreach ($activeListings as $listing) {
                    $activeMlsRecords[] = new ActiveMlsRecord($listing[$listingIdField]);
                }
            }
        }

        return $activeMlsRecords;
    }

    public function search(MlsCollectionType $type, MlsSearchParameters $parameters, $options = array())
    {
        $recordsFound = array();

        $criteria = $parameters->getRawCriterion();
        $resourceName = $type->getRemoteName();

        $PropertyTypeBoardLus = $this->getMlsPropertyTypeBoardLu($parameters);

        if (!empty($PropertyTypeBoardLus)) {
            foreach ($PropertyTypeBoardLus as $PropertyTypeBoardLu) {
                $className = $PropertyTypeBoardLu->feed_table_name;
                $records = $this->client->Search($resourceName, $className, $criteria, $options);
                $recordsFound = array_merge($recordsFound, $records);
            }
        }

        return $recordsFound;
    }

    public function getObject(MlsCollectionType $type, $objectType, $identifier, $photoNumber='*', $location=0)
    {
        return $this->client->GetObject($type->getRemoteName(), $objectType, $identifier, $photoNumber, $location);
    }

    public function connect(MlsFeedCredentials $credentials)
    {
        $this->connected = $this->client->Connect($credentials->getResourceUri(), $credentials->getUsername(), $credentials->getPassword());

        if ($this->isConnected()) {
            $this->log('MLS Feed Connection Successfully Established!');
        } else {
            $this->log('Unable to establish MLS Feed Connection!! Proceed with Caution!!! Details: ' . print_r($this->client->Error(), true), CLogger::LEVEL_WARNING);
        }
    }

    public function disconnect()
    {
        $this->client->Disconnect();
        $this->connected = false;
    }

    public function isConnected()
    {
        return $this->connected;
    }

    public function numResults()
    {
        return $this->client->NumRows();
    }

    public function getRemoteResources()
    {
        return $this->client->GetMetadataResources();
    }

    public function getRemoteClassesByResource($desiredResource = null)
    {

        $classes = array();
        $resources = $this->getRemoteResources();

        foreach ($resources as $resource) {
            $resourceId = $resource['ResourceID'];
            if (!is_null($desiredResource) && $resourceId != $desiredResource) {
                continue;
            }
            $classes[$resourceId] = $this->client->GetMetadataClasses($resourceId);
        }

        return $classes;
    }

    public function getRemoteObjectsByResource($resource = null)
    {
        //TODO: Update to honor the resource parameter. For now it returns all always.

        $classes = array();
        $resources = $this->getRemoteResources();

        foreach ($resources as $resource) {
            $resourceId = $resource['ResourceID'];
            $classes[$resourceId] = $this->client->GetMetadataClasses($resourceId);
        }

        return $classes;
    }

    public function getRemoteFieldsByResource($resource = null, $classType = null, $fieldName = null)
    {
        //TODO: Update to honor the fieldName parameter. For now it returns all always.
        return $this->client->GetMetadataTable($resource, $classType);
    }

    /**
     * @param MlsSearchParameters $parameters the parameters to extract a board lookup instance from.
     *
     * @return array a collection of MlsPropertyTypeBoardLu entries.
     */
    private function getMlsPropertyTypeBoardLu(MlsSearchParameters $parameters)
    {
        $PropertyTypeBoardLus = null;

        $mlsBoard = $parameters->getMlsBoard();
        $propertyType = $parameters->getPropertyType();

        $PropertyTypeBoardLus = MlsPropertyTypeBoardLu::model()->byBoard($mlsBoard->id)->byPropertyType($propertyType->id)->active()->findAll();

        if (empty($PropertyTypeBoardLus)) {
            $PropertyTypeBoardLus = array();
        }

        return $PropertyTypeBoardLus;
    }
}
