<?php

/**
 * Rets Specific MlsFeedClient interface.
 *
 * @author Will Madison (will@willmadison.com)
 */
interface RetsClient extends MlsFeedClient {
	public function getRemoteResources();
	public function getRemoteClassesByResource($resource = null);
	public function getRemoteObjectsByResource($resource = null);
	public function getRemoteFieldsByResource($resource = null, $classType = null, $fieldName = null);
}
