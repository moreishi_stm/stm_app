<?php
/**
 * Container for housing credentials for connecting to either a remote RETS server or
 * a remote server housing a file for and MLS feed to be obtained (typically via SFTP).
 *
 * @author Will Madison (will@willmadison.com)
 */ 
class MlsFeedCredentials {

	private $resourceUri;
	private	$username;
	private $password;
	private $options;

	function __construct($resourceUri = '', $username = '', $password = '', $options = '') {
		$this->resourceUri = $resourceUri;
		$this->username = $username;
		$this->password = $password;
		$this->options = $options;
	}

	public function setResourceUri($resourceUri) {
		$this->resourceUri = $resourceUri;
		return $this;
	}

	public function getResourceUri() {
		return $this->resourceUri;
	}

	public function setUsername($username) {
		$this->username = $username;
		return $this;
	}

	public function getUsername() {
		return $this->username;
	}

	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setOptions($options) {
		$this->options = $options;
		return $this;
	}

	public function getOptions() {
		return $this->options;
	}
}
