<?php

	/**
	 * Interface for abstract representation of an <code>MlsFeedClient</code>. All MlsFeed clients should
	 * adhere to the "contract" as specified here.
	 *
	 * @author Will Madison (will@willmadison.com)
	 */
	interface MlsFeedClient {

		public function remoteLookupByPk(MlsCollectionType $type, MlsSearchParameters $parameters, $key, $identifier);

		public function nativeLookupByPk(MlsCollectionType $type, $identifier);

		public function retrieveActiveRecords(MlsCollectionType $type, MlsSearchParameters $parameters, $priceField, $listingIdField);

		public function search(MlsCollectionType $type, MlsSearchParameters $parameters, $options = array());

		public function getObject(MlsCollectionType $type, $objectType, $identifier);

		public function connect(MlsFeedCredentials $credentials);

		public function disconnect();

		public function isConnected();

		public function numResults();
	}
