<?php
/**
 * A simple little container such that we can have a uniform
 * interface when processing inactive records from an MLS Feed.
 *
 * @author Will Madison (will@willmadison.com)
 */

class ActiveMlsRecord {

	public $id;

	function __construct($id) {
		$this->id = $id;
	}
}