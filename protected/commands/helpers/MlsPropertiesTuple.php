<?php
/**
 * Simple container class for housing both a primary MlsProperties model instance
 * and it's corresponding MlsPropertiesSecondary instance. Used primarily during our
 * MlsSync processes.
 *
 * @author Will Madison (will@willmadison.com)
 */ 
class MlsPropertiesTuple {

	private $coreMlsPropertiesInstance;
	private $secondaryMlsPropertiesInstance;
	private $rawFeedEntry;


	function __construct() {
	}

	public function setCoreMlsPropertiesInstance($coreMlsPropertiesInstance) {
		$this->coreMlsPropertiesInstance = $coreMlsPropertiesInstance;
		return $this;
	}

	public function getCoreMlsPropertiesInstance() {
		return $this->coreMlsPropertiesInstance;
	}

	public function setSecondaryMlsPropertiesInstance($secondaryMlsPropertiesInstance) {
		$this->secondaryMlsPropertiesInstance = $secondaryMlsPropertiesInstance;
		return $this;
	}

	public function getSecondaryMlsPropertiesInstance() {
		return $this->secondaryMlsPropertiesInstance;
	}

	public function setRawFeedEntry($rawFeedEntry) {
		$this->rawFeedEntry = $rawFeedEntry;
		return $this;
	}

	public function getRawFeedEntry() {
		return $this->rawFeedEntry;
	}



}
