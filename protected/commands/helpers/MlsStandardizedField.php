<?php
	/**
	 * A simple class container used for housing standardized Mls Field Data.
	 *
	 * @author Will Madison (will@willmadison.com)
	 */
	class MlsStandardizedField {

		private $columnName;
		private $standardizedValue;

		function __construct($columnName = '', $standardizedValue = '') {
			$this->columnName = $columnName;
			$this->standardizedValue = $standardizedValue;
		}

		public function setColumnName($columnName) {
			$this->columnName = $columnName;
		}

		public function getColumnName() {
			return $this->columnName;
		}

		public function setStandardizedValue($standardizedValue) {
			$this->standardizedValue = $standardizedValue;
		}

		public function getStandardizedValue() {
			return $this->standardizedValue;
		}

		function __toString() {
			return 'MlsStandardizedField{columnName=' . $this->columnName . ', standardizedValue=' . $this->standardizedValue . '}';
		}
	}
