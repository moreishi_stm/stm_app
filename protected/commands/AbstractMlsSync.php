<?php

ini_set('memory_limit', '-1');
Yii::import('stm_app.commands.helpers.*');

/**
 * Abstract representation of an MLS Feed Sync process (regardless of data source). All
 * concrete implementations should therefore extend this class and implement the requisite
 * functions.
 *
 * Performance statistics on this script can be found here: http://goo.gl/vvlQ5
 *
 * @author Will Madison (will@willmadison.com)
 */
abstract class AbstractMlsSync extends StmConsoleCommand
{

    const FORWARD_SLASH = '/';

    const COMMAND_OUTPUT_BEGIN_HEADER = "\n\n****************************************** Command START *******************************************";

    const COMMAND_OUTPUT_END_HEADER = "\n\n****************************************** Command END *******************************************";

    const OFFICE_FEATURE_GROUP_ID = 9;

    protected $startTime;

    protected $boardId;

    protected $accountId;

    protected $mlsBoard;

    /**
     * @var MlsFeedLog model utilized for persistence of statistics regarding this feed update run to the database.
     */
    protected $mlsFeedLog;

    /**
     * @var MlsPropertyTypes the type of property for which this sync is being run.
     */
    protected $PropertyType;

    /**
     * @var array collection of core/primary mls feed fields.
     */
    protected $coreFields = array();

    /**
     * @var array collection of secondary mls feed fields.
     */
    protected $secondaryFields = array();

    /**
     * @var array local cache of the remote system name to local system name mappings.
     */
    protected $fieldMaps = array();

    protected $reverseFieldMap = array();

    /** @var array collection of office fields */
    private $_officeFields = array();

    public function init()
    {

        parent::init();
        $this->startTime = microtime(true);
        $this->initialize();
        $this->log(self::COMMAND_OUTPUT_BEGIN_HEADER);

        $this->mlsFeedLog                       = new MlsFeedLog();
        $dateTime                               = new DateTime();
        $this->mlsFeedLog->run_date             = $dateTime->format('Y-m-d H:i:s');
        $this->mlsFeedLog->num_records_updated  = 0;
        $this->mlsFeedLog->num_active_records   = 0;
        $this->mlsFeedLog->num_inactive_records = 0;
        $this->mlsFeedLog->final_record_count   = 0;
    }

    /**
     * Helper function to allow all concrete implementations of this class to initialize/bootstrap themselves.
     *
     * @return void
     */
    protected abstract function initialize();

    public function actionFullSync(
        $boardId = null, $accountId = null, $clientId = null, $type = MlsCollectionType::PROPERTIES,
        $propertyType = MlsPropertyTypes::RESIDENTIAL, $startDateOffset = null, $endDateOffset = null, $limit = -1,
        $listingId = null, $doFullDump = false
    ) {

        $this->boardId   = $boardId;
        $this->accountId = $accountId;
        $this->clientId  = $clientId;

        $this->getMlsBoard();

        if (empty($this->mlsBoard)) {
            throw new Exception('Invalid combination of boardId, accountId, and clientId. Couldn\'t locate a corresponding MlsBoards instance.');
        }

        $collectionType = $this->getMlsCollectionType($type);
        $feedClient     = $this->getFeedClient();

        $this->PropertyType               = new MlsPropertyTypes();
        $this->PropertyType->table_suffix = $propertyType;
        $PropertyTypes                    = $this->PropertyType->search()->getData();

        $propertyTypeId = 1; //Residential by default...
        if (!empty($PropertyTypes)) {
            $this->PropertyType = current($PropertyTypes);
            $propertyTypeId     = $this->PropertyType->id;
        } else {
            $this->PropertyType = MlsPropertyTypes::model()->findByPk($propertyTypeId);
        }

        $this->mlsFeedLog->mls_property_type_id = $propertyTypeId;

        $syncConfigurations = new MlsSyncConfig();
        $syncConfigurations->setDoFullDump($doFullDump)->setRecordLimit($limit)->setStartDateOffset($startDateOffset)
            ->setEndDateOffset($endDateOffset)->setPropertyTypeId($propertyTypeId)->setListingId(
                $listingId
            )->setMlsSyncProcess($this);

        switch ($type) {
        case MlsCollectionType::OFFICES:
        case MlsCollectionType::AGENTS:
        case MlsCollectionType::PROPERTIES:
            $this->synchronize($collectionType, $feedClient, $syncConfigurations);
            //$this->actionProcessInactives($type, $propertyTypeId, $feedClient);
            $this->calculateFinalCounts($collectionType);
            break;
        default:
            $this->log('Unrecognized MlsCollection Type Provided: ' . $type, CLogger::LEVEL_WARNING);
            break;
        }

        $tableName = $this->getMlsBoard()->getPrefixedName() . MlsProperties::getTableSuffix();
        if (Yii::app()->stm_mls->createCommand("SELECT count(1) FROM {$tableName} WHERE status IS NULL")->queryScalar() !== false) {
            $this->actionFullSync($boardId, $accountId, $clientId, $type, $propertyType, $startDateOffset, $endDateOffset, $limit);
        }

        $this->finalize();
    }

    /**
     * Returns the board for which this MLS Feed Sync process is run.
     *
     * @return MlsBoards an instance of the MLS Boards model which represents the board for
     *                   which this sync process is run.
     */
    protected function getMlsBoard()
    {

        if (empty($this->mlsBoard)) {
            $this->mlsBoard = MlsBoards::findBoardFor($this->accountId, $this->clientId, $this->boardId);
        }

        return $this->mlsBoard;
    }

    /**
     * @param $syncType string an identifier of the current sync type we're performing. @see MlsCollectionType::KNOWN_TYPES
     *
     * @return MlsCollectionType the type of collection corresponding to the provided sync type.
     */
    protected abstract function getMlsCollectionType($syncType);

    /**
     * @return MlsFeedClient the client which the concrete implementation uses to
     *         communicate with the remote MLS feed source.
     */
    protected abstract function getFeedClient();

    private function synchronize(
        MlsCollectionType $collectionType, MlsFeedClient $feedClient, MlsSyncConfig $syncConfigurations
    ) {

        $this->log(
            'Beginning synchronization on the following collection type: ' . $collectionType . ', Sync Configurations: '
                . $syncConfigurations
        );

        if ($syncConfigurations->getDoFullDump()) {
            if ($this->confirm('Are You Sure you\'d like to dump tables?')) {
                //$this->actionCreateTables($this->getMlsBoard()->short_name);
            }
        }

        $this->log('Building search criteria and additional options...');
        $searchCriteria = $this->buildSearchCriterionFrom($syncConfigurations, $collectionType);

        $propertyType = $searchCriteria->getPropertyType();
        if (empty($propertyType) && !empty($this->PropertyType)) {
            $searchCriteria->setPropertyType($this->PropertyType);
        }

        $mlsBoard = $searchCriteria->getMlsBoard();
        if (empty($mlsBoard)) {
            $searchCriteria->setMlsBoard($this->getMlsBoard());
        }

        /** @var MlsSearchParameters $options */
        $options = $this->buildAdditionalOptionsFrom($syncConfigurations, $collectionType);

        if ($syncConfigurations->getDoFullDump()) {
            $options['Select'] = $this->getListingIdField();
        }

        $this->log('Search Parameters: ' . $searchCriteria . ', Additional Options: ' . print_r($options, true));

        $this->mlsFeedLog->mls_board_id        = $this->getMlsBoard()->id;
        $this->mlsFeedLog->mls_collection_type = $collectionType->getType();
        $this->mlsFeedLog->run_details         = strval($syncConfigurations);

        if (!$feedClient->isConnected()) {
            $feedClient->connect($this->getCredentials());
        }

        $this->log('Invoking Feed Client Lookup.....');
        $searchResults = $feedClient->search($collectionType, $searchCriteria, $options);
        $this->log('Feed Client Lookup Complete!');

        switch ($collectionType->getType()) {
            case MlsCollectionType::OFFICES:
                $this->syncOffices($searchResults);
            break;
            case MlsCollectionType::PROPERTIES:
                $this->syncProperties($searchResults, $syncConfigurations);
            break;
            case MlsCollectionType::AGENTS:
                $this->syncAgents($searchResults);
            break;
        }
    }

    public function actionProcessInactives($type = MlsCollectionType::PROPERTIES, $propertyTypeId, $feedClient = null)
    {

        if ($type != MlsCollectionType::PROPERTIES) {
            $this->log(
                'actionProcessInactives() is only valid in the context of a Mls Properties Collection type.',
                CLogger::LEVEL_ERROR
            );

            return;
        }

        $collectionType = $this->getMlsCollectionType($type);

        if (empty($feedClient)) {
            $feedClient = $this->getFeedClient();
        }

        if (!$feedClient->isConnected()) {
            $feedClient->connect($this->getCredentials());
        }

        if (empty($this->PropertyType)) {
            $this->PropertyType               = new MlsPropertyTypes();
            $this->PropertyType->table_suffix = $type;
        }

        $this->PropertyType = MlsPropertyTypes::model()->findByPk($propertyTypeId);

        $this->mlsFeedLog->mls_property_type_id = $propertyTypeId;

        $syncConfigurations = new MlsSyncConfig();
        $syncConfigurations->setPropertyTypeId($propertyTypeId)->setMlsSyncProcess($this);

        $searchCriteria = $this->buildSearchCriterionFrom($syncConfigurations, $collectionType);

        $propertyType = $searchCriteria->getPropertyType();

        if (!empty($this->PropertyType)) {
            $searchCriteria->setPropertyType($this->PropertyType);
        }

        $mlsBoard = $searchCriteria->getMlsBoard();

        if (empty($mlsBoard)) {
            $searchCriteria->setMlsBoard($this->getMlsBoard());
        }

        $ActiveRecords = $feedClient->retrieveActiveRecords(
            $collectionType, $searchCriteria, $this->getListingPriceField(), $this->getListingIdField()
        );
        $tableName     = $collectionType->getBoardName() . MlsProperties::getTableSuffix();

        $activeRecordIds = array();

        foreach ($ActiveRecords as $record) {
            $activeRecordIds[] = $record->id;
        }

        $activeListingIds = join(',', $activeRecordIds);

        $inactivationQuery
            = <<<QUERY
UPDATE `$tableName`
SET `status` = 0,
    `property_last_updated` = NOW()
WHERE `listing_id` NOT IN ($activeListingIds) AND mls_property_type_id = $propertyTypeId
QUERY;

        if (!empty($activeListingIds)) {
            $this->mlsFeedLog->num_inactive_records = Yii::app()->stm_mls->createCommand($inactivationQuery)->execute();
        } else {
            $this->log('processInactives(): No active listings found....');
        }

        if ($feedClient->isConnected()) {
            $feedClient->disconnect();
        }
    }

    private function calculateFinalCounts(MlsCollectionType $collectionType)
    {

        $CollectionModel = null;

        switch ($collectionType->getType()) {
        case MlsCollectionType::AGENTS:
            $CollectionModel = MlsAgents::model($collectionType->getBoardName());
            break;
        case MlsCollectionType::PROPERTIES:
            $CollectionModel = MlsProperties::model($collectionType->getBoardName());
            break;
        case MlsCollectionType::OFFICES:
            $CollectionModel = MlsOffices::model($collectionType->getBoardName());
            break;
        }

        if (!empty($CollectionModel)) {
            $this->mlsFeedLog->final_record_count = $CollectionModel->count();
        }
    }

    /**
     *  Immediately logs the given message to the console rather than buffering the messages in
     *  memory.
     *
     * @param string $message the message to be logged
     * @param string $level   level of the message (ex: 'trace', 'error', 'warning', etc.)
     * @param string $scope   the "scope" of the message (typically the class from which it originated).
     */
    protected function log($message, $level = 'info', $scope = __CLASS__)
    {

        $now = new DateTime();

        echo $now->format('Y-m-d H:i:s') . " - [$level] $scope : $message\n";
    }

    private function finalize()
    {

        $now = microtime(true);

        $MEGABYTES_PER_BYTE = pow((1 / 1024), 2);

        $this->mlsFeedLog->runtime_in_seconds = round($now - $this->startTime, 2);

        $this->log('Script Runtime: ' . $this->mlsFeedLog->runtime_in_seconds . ' seconds');

        $memoryUsageMegaBytes = memory_get_peak_usage() * $MEGABYTES_PER_BYTE;

        $this->mlsFeedLog->peak_memory_usage_mb = round($memoryUsageMegaBytes, 2);

        $this->log('Memory Usage: ' . $this->mlsFeedLog->peak_memory_usage_mb . ' megabytes');

        $dateTime                   = new DateTime();
        $this->mlsFeedLog->end_time = $dateTime->format('Y-m-d H:i:s');

        if ($this->mlsFeedLog->save()) {
            $this->log(
                'Successfully saved MlsFeedLog Record: ' . print_r($this->mlsFeedLog->attributes, true),
                CLogger::LEVEL_INFO
            );
        } else {
            $this->log(
                'Failed to save MlsFeedLog Record due to errors: ' . print_r($this->mlsFeedLog->getErrors(), true),
                CLogger::LEVEL_WARNING
            );
        }

        $this->log(self::COMMAND_OUTPUT_END_HEADER);
    }

    /**
     * @return MlsFeedCredentials returns the requisite credentials for accessing a remote MLS feed source.
     */
    public abstract function getCredentials();

    function actionCreateTables($boardName, $dropTable = true)
    {

        $MlsBoard = MlsBoards::model()->find('short_name=:short_name', array('short_name' => strtoupper($boardName)));

        if (!empty($MlsBoard->short_name)) {
            $this->createMlsTables($MlsBoard, $boardName, $dropTable);
        } else {
            $this->log('Cannot create tables for a board that does not exist!', CLogger::LEVEL_ERROR);
        }
    }

    /**
     * @param MlsSyncConfig     $configurations the configurations on which we're to base our search criterion on.
     * @param MlsCollectionType $collectionType the type of collection we're building search criterion for.
     *
     * @return MlsSearchParameters the parameters by which we are to search based on the given configurations.
     */
    protected abstract function buildSearchCriterionFrom(
        MlsSyncConfig $configurations, MlsCollectionType $collectionType
    );

    /**
     * @param MlsSyncConfig     $configurations the configurations on which we're to base our additional options on.
     * @param MlsCollectionType $collectionType the type of collection we're building options for.
     *
     * @return array any additional options that may be garnered from the given sync configurations.
     */
    protected abstract function buildAdditionalOptionsFrom(
        MlsSyncConfig $configurations, MlsCollectionType $collectionType
    );

    private function syncOffices($feedSearchResults = array())
    {

        $mlsOffices = $this->convertToMlsOffices($feedSearchResults);

        $mlsCollectionType                        = $this->getMlsCollectionType(MlsCollectionType::OFFICES);
        $this->mlsFeedLog->beginning_record_count = MlsOffices::model($mlsCollectionType->getBoardName())->count();

        foreach ($mlsOffices as $office) {
            $isNewRecord = $office->isNewRecord;

            $officeSaved = false;

            //Necessary because there may be duplicates in our collection of offices.
            try {
                $officeSaved = $office->save();
            } catch (Exception $e) {
                $this->log(
                    "Encountered Exception Saving MlsOffice instance! :" . $e->getMessage(), CLogger::LEVEL_WARNING
                );
            }

            if (!$officeSaved) {
                $this->log("Failed to save MlsOffice instance!", CLogger::LEVEL_WARNING);
                $this->log('Office: ' . print_r($office->attributes, true));
                $this->log('Office Errors: ' . print_r($office->getErrors(), true));
            } else {
                $this->log('Office Successfully Synchronized!');

                $this->mlsFeedLog->num_active_records++;

                if (!$isNewRecord) {
                    $this->mlsFeedLog->num_records_updated++;
                }
            }
        }
    }

    private function syncProperties($feedSearchResults = array(), MlsSyncConfig $syncConfigurations)
    {

        $propertyUpdateSqlTemplate = ":action :tableName SET :values :where";

        $numberOfListings = count($feedSearchResults);
        $boardPrefix = $this->getMlsBoard()->getPrefixedName();
        $propertyTableName = $boardPrefix . MlsProperties::getTableSuffix();
        $secondaryPropertyTableName = $boardPrefix . MlsPropertiesSecondary::getTableSuffix();
        $propertyTypeId = $syncConfigurations->getPropertyTypeId();

        if ($syncConfigurations->getDoFullDump()) {
            foreach ($feedSearchResults as $feedSearchIdx => $feedData) {
                $listingId = $feedData[$this->getListingIdField()];
                $this->log("Seeding listing_id: $listingId [{$feedSearchIdx}/{$numberOfListings}]");
                Yii::app()->stm_mls->createCommand("INSERT INTO $propertyTableName (listing_id, mls_property_type_id) VALUES('$listingId', $propertyTypeId)")->execute();
                Yii::app()->stm_mls->createCommand("INSERT INTO $secondaryPropertyTableName (listing_id) VALUES('$listingId')")->execute();
            }

            $this->log('Properties, Offices, and Agents Successfully Seeded!');
        } else {
            $this->log('Converting the search results into MlsProperties/MlsPropertiesSecondary Models....');
            $mlsPropertiesTuples = $this->convertToMlsPropertiesTuples(
                $feedSearchResults, $syncConfigurations->getPropertyTypeId()
            );
            $this->log('Conversion complete!');
            $feedSearchResults = null;

            $mlsCollectionType                        = $this->getMlsCollectionType(MlsCollectionType::PROPERTIES);
            $this->mlsFeedLog->beginning_record_count = MlsProperties::model($mlsCollectionType->getBoardName())->count();

            $this->log('Beginning to save the derived models...');

            $mlsOfficeIdsProcessed = array();
            foreach ($mlsPropertiesTuples as $mlsPropertyTupleIdx => $mlsPropertyTuple) {
                $secondaryPropertiesSaved = true;

                $coreProperties      = $mlsPropertyTuple->getCoreMlsPropertiesInstance();
                $secondaryProperties = $mlsPropertyTuple->getSecondaryMlsPropertiesInstance();
                $mlsPropertyTuple = null;

                $coreProperties->mls_property_type_id = $syncConfigurations->getPropertyTypeId();

                /*
                if ($coreProperties->getOffice()) {
                    if (!in_array($coreProperties->getOffice()->office_id, $mlsOfficeIdsProcessed)) {
                        if (!MlsOffices::model($this->getMlsBoard()->getPrefixedName())->exists('office_id = :office_id', array(':office_id' => $coreProperties->getOffice()->office_id))) {
                            $coreProperties->getOffice()->save();
                            array_push($mlsOfficeIdsProcessed, $coreProperties->getOffice()->office_id);
                        }
                    }
                }
                */


//                if (empty($coreProperties->status_change_date)) {
//                    $coreProperties->status_change_date = new CDbExpression('NOW()');
//                }

                $isNewRecord = $coreProperties->isNewRecord;

                /** @var CDbConnection $mlsDbConnection */
                $mlsDbConnection = Yii::app()->stm_mls;

                $corePropertyValues = "";
                foreach ($coreProperties->getAttributes() as $attributeName => $attributeValue) {
                    $attributeValue = addslashes($attributeValue);
                    $corePropertyValues .= "{$attributeName} = '{$attributeValue}',";
                }
                $corePropertyValues = substr($corePropertyValues, 0, -1);

                $corePropertyUpdateSql = strtr($propertyUpdateSqlTemplate, array(
                    ':action' => (($coreProperties->getIsNewRecord()) ? 'INSERT INTO' : 'UPDATE'),
                    ':tableName' => $propertyTableName,
                    ':values' => $corePropertyValues,
                    ':where' => (($coreProperties->getIsNewRecord()) ? '' : "WHERE listing_id = '{$coreProperties->listing_id}'"),
                ));
                $mlsDbConnection->createCommand($corePropertyUpdateSql)->execute();
                $corePropertiesSaved = true;

                if (!empty($secondaryProperties)) {
                    $secondaryProperties->listing_id = $coreProperties->listing_id;

                    $secondaryPropertyValues = "";
                    foreach ($secondaryProperties->getAttributes() as $attributeName => $attributeValue) {
                        $attributeValue = addslashes($attributeValue);
                        $secondaryPropertyValues .= "{$attributeName} = '{$attributeValue}',";
                    }
                    $secondaryPropertyValues = substr($secondaryPropertyValues, 0, -1);

                    $secondaryPropertyUpdateSql = strtr($propertyUpdateSqlTemplate, array(
                        ':action' => (($secondaryProperties->getIsNewRecord()) ? 'INSERT INTO' : 'UPDATE'),
                        ':tableName' => $secondaryPropertyTableName,
                        ':values' => $secondaryPropertyValues,
                        ':where' => (($secondaryProperties->getIsNewRecord()) ? '' : "WHERE listing_id = '{$secondaryProperties->listing_id}'"),
                    ));
                    $mlsDbConnection->createCommand($secondaryPropertyUpdateSql)->execute();

                    $secondaryPropertiesSaved = true;
                }

                $saveComplete = $corePropertiesSaved && $secondaryPropertiesSaved;

                if (!$saveComplete) {
                    $coreSaved      = $corePropertiesSaved ? 'Yes' : 'No';
                    $secondarySaved = $secondaryPropertiesSaved ? 'Yes' : 'No';
                    $this->log(
                        "MlsProperties incompletely saved: Core Property Saved?: $coreSaved,
				 Secondary Properties Saved?: $secondarySaved ", CLogger::LEVEL_WARNING
                    );
                    $this->log('Core Property: ' . print_r($coreProperties->attributes, true));
                    $this->log('Core Property (Errors): ' . print_r($coreProperties->getErrors(), true));
                    $this->log('Secondary Property: ' . print_r($secondaryProperties->attributes, true));
                    $this->log('Secondary Property (Errors): ' . print_r($secondaryProperties->getErrors(), true));
                    $this->log('Raw Feed Entry: ' . print_r($mlsPropertyTuple->getRawFeedEntry(), true));
                } else {
                    $this->log("Property Successfully Synchronized! [{$mlsPropertyTupleIdx}/{$numberOfListings}]");

                    $this->mlsFeedLog->num_active_records++;

                    if (!$isNewRecord) {
                        $this->mlsFeedLog->num_records_updated++;
                    }
                }
            }
        }
    }

    private function syncAgents($feedSearchResults = array())
    {

        $mlsAgents = $this->convertToMlsAgents($feedSearchResults);

        $mlsCollectionType                        = $this->getMlsCollectionType(MlsCollectionType::AGENTS);
        $this->mlsFeedLog->beginning_record_count = MlsAgents::model($mlsCollectionType->getBoardName())->count();

        foreach ($mlsAgents as $agent) {
            $isNewRecord = $agent->isNewRecord;

            $agentSaved = false;

            //Necessary because there may be duplicates in our collection of agents.
            try {
                $agentSaved = $agent->save();
            } catch (Exception $e) {
                $this->log(
                    "Encountered Exception Saving MlsAgent instance! :" . $e->getMessage(), CLogger::LEVEL_WARNING
                );
            }

            if (!$agentSaved) {
                $this->log("Failed to save MlsAgent instance!", CLogger::LEVEL_WARNING);
                $this->log('Agent: ' . print_r($agent->attributes, true));
                $this->log('Agent Errors: ' . print_r($agent->getErrors(), true));
            } else {
                $this->log('Agent Successfully Synchronized!');

                $this->mlsFeedLog->num_active_records++;

                if (!$isNewRecord) {
                    $this->mlsFeedLog->num_records_updated++;
                }
            }
        }
    }

    protected abstract function getListingPriceField();

    protected abstract function getListingIdField();

    /**
     * @param $MlsBoard  MlsBoards an instance of the MlsBoards model corresponding to the board name we've been given
     * @param $boardName string the name of the board for which we are preparing to create tables for.
     * @param $dropTable boolean whether or not tables should be dropped prior to creation.
     *
     * @return void
     */
    private function createMlsTables($MlsBoard, $boardName, $dropTable)
    {

        $this->createPrimaryPropertyTables($MlsBoard, $boardName, $dropTable);
        $this->createSecondaryPropertyTables($MlsBoard, $boardName, $dropTable);
        $this->createAgentTables($MlsBoard, $boardName, $dropTable);
        $this->createOfficeTables($MlsBoard, $boardName, $dropTable);
    }

    /**
     * @param $listings mixed the result from a given MlsFeed search query.
     *
     * @return array a collection of MlsOffices models derived from the search results given,
     *               empty if the result set is empty;
     */
    protected function convertToMlsOffices($listings)
    {

        $mlsOffices = array();

        $fieldMaps = $this->getFieldMaps();
        $reverseLookup = $this->getReverseFieldMaps();

        $MlsBoard = $this->getMlsBoard();

        foreach ($listings as $listing) {
            $office = null;
            $officeIdColumn = $this->getOfficeIdField();

            $office = MlsOffices::model($MlsBoard->getPrefixedName())->findByPk($listing[$officeIdColumn]);

            if (!$office) {
                $office = new MlsOffices($MlsBoard->getPrefixedName());
            }

            foreach ($fieldMaps as $remoteFeedSystemName => $featureColumnName) {
                if ($listing[$officeIdColumn]) {
                    try {
                        $office->{$featureColumnName} = $listing[$remoteFeedSystemName];
                    } catch (Exception $e) {
                        $this->log(
                            "Encountered the following exception attempting to set the '$featureColumnName' property on a MlsOffices object: "
                                . $e->getMessage(), CLogger::LEVEL_WARNING, __CLASS__
                        );
                    }
                }
            }

            if ($office) {
                $mlsOffices[] = $office;
            }

        }

        return $mlsOffices;
    }

    /**
     * @param $listings mixed the result from a given MlsFeed search query.
     *
     * @param $mlsPropertyTypeId
     *
     * @return array a collection of MlsPropertiesTuples derived from the search results given,
     *               empty if the result set is empty;
     */
    protected function convertToMlsPropertiesTuples($listings, $mlsPropertyTypeId)
    {

        $numListings = count($listings);

        $numListingsProcessed = 0;

        //Every 10 conversions we'll let the end user know where we are with our processing...
        $heartBeat = 500;

        $mlsPropertyTuples = array();

        $unmappedFields = array();

        $MlsBoard = $this->getMlsBoard();

        foreach ($listings as $listingColIdx => $listing) {
            $listingId = $listing[$this->getListingIdField()];
            $listingOfficeId = $listing[$this->getOfficeIdField()];

            $coreMlsProperty = MlsProperties::model($MlsBoard->getPrefixedName())->findByPk(
                array(
                    'listing_id'           => $listingId,
                    'mls_property_type_id' => $mlsPropertyTypeId,
                )
            );

            if (empty($coreMlsProperty)) {
                $coreMlsProperty = new MlsProperties($MlsBoard->getPrefixedName());
            }

            $mlsOffice = MlsOffices::model($MlsBoard->getPrefixedName())->findByPk($listingOfficeId);
            if (empty($mlsOffice)) {
                $mlsOffice = new MlsOffices($MlsBoard->getPrefixedName());
            }

            $secondaryMlsProperty = MlsPropertiesSecondary::model($MlsBoard->getPrefixedName())->findByPk($listingId);

            if (empty($secondaryMlsProperty)) {
                $secondaryMlsProperty = new MlsPropertiesSecondary($MlsBoard->getPrefixedName());
            }

            foreach ($listing as $feedSystemField => $value) {
                // Setup a new office instance incase this property has an associated office

                if ($value != null && $value != '') {
                    $stmColumnName = $this->getColumnNameFromFieldMap($feedSystemField);

                    if (empty($stmColumnName)) {
                        //$this->log("Unmapped field: ($feedSystemField) detected for listing: " . $listingId, CLogger::LEVEL_WARNING, __CLASS__);
                        $unmappedFields[] = $feedSystemField;
                    } else if ($this->isOfficeField($feedSystemField)) {
                        $mlsOffice->{$stmColumnName} = $value;
                    } else {
                        if ($this->isStoredField($feedSystemField)) {
                            $standardizedFields = $this->standardizeField($feedSystemField, $value);

                            foreach ($standardizedFields as $standardizedField) {
                                $propertiesInstance     = ($this->isCoreField($standardizedField->getColumnName()))
                                    ? $coreMlsProperty : $secondaryMlsProperty;
                                $standardizedColumnName = $standardizedField->getColumnName();

                                if (!empty($standardizedColumnName)) {
                                    try {
                                        $propertiesInstance->{$standardizedColumnName}
                                            = $standardizedField->getStandardizedValue();
                                    } catch (Exception $e) {
                                        $this->log(
                                            "Encountered the following exception attempting to set the '$standardizedColumnName' property on a "
                                                . get_class($propertiesInstance) . ' object: ' . $e->getMessage(),
                                            CLogger::LEVEL_WARNING, __CLASS__
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $coreMlsProperty->setOffice($mlsOffice);
            $coreMlsProperty->listing_office_id = $listingOfficeId;

            $doHeartbeat = (++$numListingsProcessed % $heartBeat == 0);

            if ($doHeartbeat) {
                $percentageComplete = round($numListingsProcessed / $numListings * 100, 2);

                $this->log(
                    "{$percentageComplete}% Complete. $numListingsProcessed out of $numListings converted.",
                    CLogger::LEVEL_INFO, __CLASS__
                );
            }

            $mlsPropertyTuple    = new MlsPropertiesTuple();
            $mlsPropertyTuples[] = $mlsPropertyTuple->setCoreMlsPropertiesInstance($coreMlsProperty)
                ->setSecondaryMlsPropertiesInstance($secondaryMlsProperty)->setRawFeedEntry($listing);


            unset($listings[$listingColIdx]);
        }

        $unmappedFields = array_unique($unmappedFields);

        if (!empty($unmappedFields)) {
            $this->log(
                'Unmapped Fields found during this conversion: ' . print_r($unmappedFields, true), CLogger::LEVEL_INFO,
                __CLASS__
            );
        }

        return $mlsPropertyTuples;
    }

    /**
     * @param $listings mixed the result from a given MlsFeed search query.
     *
     * @return array a collection of MlsAgents models derived from the search results given,
     *               empty if the result set is empty;
     */
    protected function convertToMlsAgents($listings)
    {

        $mlsAgents = array();

        $reverseLookup = $this->getReverseFieldMaps();

        $MlsBoard = $this->getMlsBoard();

        foreach ($listings as $listing) {
            $agent = null;

            foreach ($this->getAgentIdFeedSystemColumns() as $agentIdColumn) {
                if ($listing[$agentIdColumn]) {
                    $agent = MlsAgents::model($MlsBoard->getPrefixedName())->findByPk($listing[$agentIdColumn]);

                    if (!$agent) {
                        $agent = new MlsAgents($MlsBoard->getPrefixedName());
                    }

                    foreach ($listingAgentFields as $featureColumnName => $agentTableColumn) {
                        $remoteFeedSystemName = $reverseLookup[$featureColumnName];
                        try {
                            $agent->{$agentTableColumn} = $listing[$remoteFeedSystemName];
                        } catch (Exception $e) {
                            $this->log(
                                "Encountered the following exception attempting to set the '$agentTableColumn' property on a MlsAgents object: "
                                    . $e->getMessage(), CLogger::LEVEL_WARNING, __CLASS__
                            );
                        }
                    }
                }
            }

            if ($agent) {
                $mlsAgents[] = $agent;
            }
        }

        return $mlsAgents;
    }

    private function createPrimaryPropertyTables($MlsBoard, $boardName, $dropTable)
    {

        $tableNameParts = array(
            $MlsBoard->state->short_name,
            $boardName,
            'properties'
        );

        $templateName = 'mls_properties_template.sql';

        $creationSuccessful = $this->createTable($tableNameParts, $templateName, $dropTable);

        if ($creationSuccessful) {
            $this->log("Success! Created primary property table for $boardName");
        }
    }

    private function createSecondaryPropertyTables($MlsBoard, $boardName, $dropTable)
    {

        $tableNameParts = array(
            $MlsBoard->state->short_name,
            $boardName,
            'properties',
            'secondary'
        );

        $templateName = "mls_properties_secondary_template_$boardName.sql";

        $creationSuccessful = $this->createTable($tableNameParts, $templateName, $dropTable);

        if ($creationSuccessful) {
            $this->log("Success! Created secondary property table for $boardName");
        }
    }

    private function createAgentTables($MlsBoard, $boardName, $dropTable)
    {

        $tableNameParts = array(
            $MlsBoard->state->short_name,
            $boardName,
            'agents'
        );

        $templateName = 'mls_agent_template.sql';

        $creationSuccessful = $this->createTable($tableNameParts, $templateName, $dropTable);

        if ($creationSuccessful) {
            $this->log("Success! Created Agents table for $boardName");
        }
    }

    private function createOfficeTables($MlsBoard, $boardName, $dropTable)
    {

        $tableNameParts = array(
            $MlsBoard->state->short_name,
            $boardName,
            'offices'
        );

        $templateName = 'mls_office_template.sql';

        $creationSuccessful = $this->createTable($tableNameParts, $templateName, $dropTable);

        if ($creationSuccessful) {
            $this->log("Success! Created Offices table for $boardName");
        }
    }

    public function getReverseFieldMaps()
    {

        if (empty($this->reverseFieldMap)) {
            //TODO: Inquire with Chris and/or Christine with about whether or not Field maps will ever contain duplicate values keyed by different feed system field names.
            $this->reverseFieldMap = array_flip($this->getFieldMaps());
        }

        return $this->reverseFieldMap;
    }


    /**
     * @return array contains collection of office fields
     */
    private function getOfficeFields() {

        if (empty($this->_officeFields)) {
            $officeFields = array();
            $officeFieldMaps = MlsFieldMaps::model()->with('feature')->findAll(array(
                'condition' => 'feature.mls_feature_group_id = :mls_feature_group_id',
                'params' => array(':mls_feature_group_id' => self::OFFICE_FEATURE_GROUP_ID),
            ));

            if (!empty($officeFieldMaps)) {
                /** @var MlsFieldMaps $officeFieldMap */
                foreach ($officeFieldMaps as $officeFieldMap) {
                    array_push($officeFields, $officeFieldMap->feature->column_name);
                }
            }

            $this->_officeFields = $officeFields;
        }

        return $this->_officeFields;
    }


    protected function getColumnNameFromFieldMap($systemField)
    {

        $fieldMaps = $this->getFieldMaps();

        return $fieldMaps[$systemField];
    }

    /**
     *
     * @param $feedSystemName string the field name as referred to in the MLS Feed.
     *
     * @return boolean true if the given field is to be stored
     */
    public function isStoredField($feedSystemName)
    {

        $mlsColumnName = $this->getColumnNameFromFieldMap($feedSystemName);
        return $this->isCoreField($mlsColumnName) || $this->isSecondaryField($mlsColumnName);
    }

    /**
     * Helper function which allows concrete implementations, which are generally more familiar with the details of their
     * particular feeds, to standardize the data stored within a given field for better representation with in the STM
     * application.
     *
     * @param $systemField string the field name as referred to within the remote MLS feed source.
     * @param $value       mixed the value of the given field.
     *
     * @return MlsStandardizedField the "standardized" field value.
     */
    public abstract function standardizeField($systemField, $value);

    /**
     * @param $mlsColumnName
     *
     * @return boolean true if the given field is a core field, false otherwise.
     */
    public function isCoreField($mlsColumnName)
    {

        $coreFields = $this->getCoreFields();

        return in_array($mlsColumnName, $coreFields);
    }

    /**
     * @param $tableNameParts array the constituent parts of the desired table's name (will be joined by an underscore).
     * @param $templateName   string the name of the sql template file which contains this table's DDL.
     * @param $dropTable      boolean whether or not the desired table should be dropped before recreation.
     *
     * @return bool true if the creation was successful, false otherwise.
     */
    private function createTable($tableNameParts, $templateName, $dropTable = true)
    {

        $tableName = strtolower(join('_', $tableNameParts));

        if ($dropTable) {
            $this->deleteTable($tableName);
        }

        $sqlTemplateBaseDir = $this->getSQLTemplateBaseDirectory();
        $lastCharacter      = substr($sqlTemplateBaseDir, -1);

        if ($lastCharacter != self::FORWARD_SLASH) {
            $sqlTemplateBaseDir .= self::FORWARD_SLASH;
        }

        $templateFileName = realpath($sqlTemplateBaseDir . $templateName);

        $this->log('Template File Name: ' . $templateFileName);

        $creationSuccessful = true;

        if (file_exists($templateFileName)) {
            $rawSQLTemplate = file_get_contents($templateFileName);
            $rawSQLTemplate = str_replace('{table_name}', $tableName, $rawSQLTemplate);

            try {
                Yii::app()->stm_mls->createCommand($rawSQLTemplate)->execute();
            } catch (Exception $e) {
                $this->log(
                    'Encountered exception creating the following table: ' . $tableName . '. Details: ' . print_r(
                        $e, true
                    ), CLogger::LEVEL_ERROR
                );
                $creationSuccessful = false;
            }
        } else {
            $this->log('Unable to locate the following SQL Template File: ' . $templateFileName, CLogger::LEVEL_ERROR);
            $creationSuccessful = false;
        }

        return $creationSuccessful;
    }

    /**
     * @return array a mapping from feed system field to STM database column name
     */
    public function getFieldMaps()
    {

        if (empty($this->fieldMaps)) {
            $fieldMaps = MlsFieldMaps::model()->findAll(
                array(
                    'condition' => "mls_board_id = :boardId AND mls_property_type_id = :propertyTypeId",
                    'params'    => array(
                        ":boardId"        => $this->mlsBoard->id,
                        ":propertyTypeId" => $this->PropertyType->id
                    )
                )
            );

            foreach ($fieldMaps as $fieldMap) {
                $this->fieldMaps[$fieldMap->feed_system_field] = $fieldMap->feature->column_name;
            }
        }

        return $this->fieldMaps;
    }

    /**
     * @return array a collection of, typically strings, denoting the feed id fields relevant to Agents.
     */
    public function getAgentIdFields()
    {

        if (empty($this->allAgentIdListingFields)) {
            $this->allAgentIdListingFields = array_merge(
                $this->getPrimaryAgentIdListingFields(), $this->getColistingAgentIdListingFields()
            );
        }

        return $this->allAgentIdListingFields;
    }

    /**
     * @param $mlsColumnName
     *
     * @return boolean true if the given field is a secondary field, false otherwise.
     */
    public function isSecondaryField($mlsColumnName)
    {

        $secondaryFields = $this->getSecondaryFields();

        return in_array($mlsColumnName, $secondaryFields);
    }

    public function getCoreFields()
    {

        if (empty($this->coreFields)) {
            $coreFields = MlsFieldMaps::model()->with('feature')->findAll(array(
                'condition' => 'feature.mls_feature_group_id = :core_feature_group_id AND mls_property_type_id = :mls_property_type_id',
                'params' => array(':core_feature_group_id' => MlsFeatureGroups::CORE_FEATURE_ID, ':mls_property_type_id' => $this->PropertyType->id),
                'group' => 'mls_feature_id',
            ));
            foreach ($coreFields as $coreField) {
                $this->coreFields[] = $coreField->feature->column_name;
            }
        }

        return $this->coreFields;
    }

    private function deleteTable($tableName)
    {

        $this->log('Attempting to delete the following table: ' . $tableName);

        $deletionQuery
            = <<<QUERY
DROP TABLE IF EXISTS `$tableName`;
QUERY;

        $deletionSuccessful = true;

        try {
            Yii::app()->stm_mls->createCommand($deletionQuery)->execute();
        } catch (Exception $e) {
            $deletionSuccessful = false;
            $this->log('Encountered Exception attempting to delete ' . $tableName . '. Details: ' . print_r($e, true));
        }

        if ($deletionSuccessful) {
            $this->log($tableName . ' deletion successful!');
        }
    }

    /**
     * @return string the base directory of the sql templates used by this process to create the relevant tables.
     */
    public abstract function getSQLTemplateBaseDirectory();

    public function getSecondaryFields()
    {

        if (empty($this->secondaryFields)) {
            $Criteria       = new CDbCriteria;
            $Criteria->with = 'feature';
            $Criteria->condition
                            = 't.mls_board_id = :mls_board_id AND
			                        feature.mls_feature_group_id >= :mls_feature_group_id_min AND
			                        feature.mls_feature_group_id <= :mls_feature_group_id_max'; //non-core fields

            $Criteria->params = array(
                ':mls_board_id'             => $this->mlsBoard->id,
                ':mls_feature_group_id_min' => 3,
                ':mls_feature_group_id_max' => 6
            );

            foreach (MlsFieldMaps::model()->findAll($Criteria) as $key => $fieldMap) {
                $this->secondaryFields[] = $this->getColumnNameFromFieldMap($fieldMap->feed_system_field);
            }
        }

        return $this->secondaryFields;
    }

    public function actionRemoteLookupByPk($type = MlsCollectionType::PROPERTIES, $identifier)
    {

        $collectionType = $this->getMlsCollectionType($type);
        $feedClient     = $this->getFeedClient();

        if (!$feedClient->isConnected()) {
            $feedClient->connect($this->getCredentials());
        }

        if (empty($this->PropertyType)) {
            $this->PropertyType               = new MlsPropertyTypes();
            $this->PropertyType->table_suffix = $type;
        }

        $PropertyTypes = $this->PropertyType->search()->getData();

        $propertyTypeId = 1; //Residential by default...

        if (!empty($PropertyTypes)) {
            $PropertyType   = current($PropertyTypes);
            $propertyTypeId = $PropertyType->id;
        } else {
            $this->PropertyType = MlsPropertyTypes::model()->findByPk($propertyTypeId);
        }

        $this->mlsFeedLog->mls_property_type_id = $propertyTypeId;

        $syncConfigurations = new MlsSyncConfig();
        $syncConfigurations->setPropertyTypeId($propertyTypeId)->setListingId($identifier)->setMlsSyncProcess($this);

        $searchCriteria = $this->buildSearchCriterionFrom($syncConfigurations, $collectionType);

        $propertyType = $searchCriteria->getPropertyType();

        if (empty($propertyType) && !empty($this->PropertyType)) {
            $searchCriteria->setPropertyType($this->PropertyType);
        }

        $mlsBoard = $searchCriteria->getMlsBoard();

        if (empty($mlsBoard)) {
            $searchCriteria->setMlsBoard($this->getMlsBoard());
        }

        $MlsListing = $feedClient->remoteLookupByPk(
            $collectionType, $searchCriteria, $this->getKeyByCollectionType($type), $identifier
        );

        $returnString = true;
        $this->log(print_r($MlsListing, $returnString));

        if ($feedClient->isConnected()) {
            $feedClient->disconnect();
        }

        $this->finalize();
    }

    protected abstract function getKeyByCollectionType(MlsCollectionType $type);

    public function actionNativeLookupByPk($type = MlsCollectionType::PROPERTIES, $identifier)
    {

        $collectionType = $this->getMlsCollectionType($type);
        $feedClient     = $this->getFeedClient();

        if (!$feedClient->isConnected()) {
            $feedClient->connect($this->getCredentials());
        }

        $MlsListing = $feedClient->nativeLookupByPk($collectionType, $identifier);

        $returnString = true;
        $this->log(print_r($MlsListing, $returnString));

        if ($feedClient->isConnected()) {
            $feedClient->disconnect();
        }

        $this->finalize();
    }

    /**
     * @param $feedSystemName string the name of the field we're inquiring about in the remote system.
     *
     * @return bool true if the given field is an agent field, false otherwise.
     */
    public function isAgentField($feedSystemName)
    {

        return array_key_exists($this->getColumnNameFromFieldMap($feedSystemName), $this->getAgentFields());
    }

    /**
     * @return array a collection of, typically strings, denoting the feed fields relevant to Agents.
     */
    public function getAgentFields()
    {


        return array();
    }

    /**
     * @param $feedSystemName string the name of the field we're inquiring about in the remote system.
     *
     * @return bool true if the given field is an agent field, false otherwise.
     */
    public function isOfficeField($feedSystemName)
    {

        return in_array($this->getColumnNameFromFieldMap($feedSystemName), $this->getOfficeFields());
    }

    protected abstract function getImageDirectory();
}