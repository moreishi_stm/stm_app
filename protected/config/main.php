<?php
define('DS', DIRECTORY_SEPARATOR);
define('SITE_ROOT', dirname(dirname(dirname(dirname(__FILE__)))));
define('SITE_ROOT_CONFIG', SITE_ROOT.DS.'stm_app'.DS.'protected'.DS.'config');
define('YII_FRAMEWORK_PATH', SITE_ROOT.DS.'stm_frameworks'.DS.'yii'.DS.YII_VERSION);
define('PRODUCTION', 'production');
define('DEVELOPMENT', 'development');

$isConsole = (defined('STDIN'))? true : false;
define('YII_IS_CONSOLE', $isConsole);

//// Used to display a alternate view if the page errors when maintenance is occurring
define('YII_MAINTENANCE', false);

//check to see if YII_IS_CONSOLE_PRODUCTION, based on certain settings existing or not indicates whether it's a Yii Console Production. FYI, crontab console command also have different server variable settings. Fun =)
$isConsoleProduction = (YII_IS_CONSOLE &&
                        // xdebug definitely indicates development setting
                        !isset($_SERVER['XDEBUG_CONFIG']) &&

                        // this setting indicates console command run NOT via cron
                        (
                            isset($_SERVER['SUDO_USER']) &&
                            isset($_SERVER['PWD']) &&
                            (!($_SERVER['SUDO_USER']=='ubuntu' && $_SERVER['SUDO_USER']=='root')) ||
                            (!empty($_SERVER['PWD']) && strpos($_SERVER['PWD'], 'christinelee') !== false)

                        ) ||
                        // this setting indicates console command run via cron
                        (
                            !isset($_SERVER['SUDO_USER']) &&
                            isset($_SERVER['PWD']) && $_SERVER['PWD']== '/root' &&
                            isset($_SERVER['LOGNAME']) && $_SERVER['LOGNAME']=='root'
                        )
                    )
                    ? true : false;

define( 'YII_IS_CONSOLE_PRODUCTION', $isConsoleProduction);

// other ways to check for is Debug as YII_CONSOLE does not recognize $_SERVER['APPLICATION_ENVIRONMENT']
if (isset($_SERVER['APPLICATION_ENVIRONMENT']) || (YII_IS_CONSOLE && !YII_IS_CONSOLE_PRODUCTION))
{
    defined( 'YII_TRACE_LEVEL' ) or define( 'YII_TRACE_LEVEL', 3 );
    define('ENVIRONMENT', DEVELOPMENT);
    defined( 'YII_DEBUG' ) or define( 'YII_DEBUG', true );
    error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE & ~E_DEPRECATED);
} else {
    define('ENVIRONMENT', PRODUCTION);
    if(YII_IS_CONSOLE_PRODUCTION) {
        error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
        ini_set('max_execution_time', 3600);
    } else {
        error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE & ~E_DEPRECATED);
    }
}

//normal web app mode
if(YII_IS_CONSOLE===false):
    require_once(YII_FRAMEWORK_PATH .DS.'yiilite.php');
    require_once(SITE_ROOT.DS.'stm_app'.DS.'protected'.DS.'modules'.DS.'admin'.DS.'StmWebApplication.php'); // path to StmWebApp

    Yii::setPathOfAlias('stm_app', SITE_ROOT.DS.'stm_app'.DS.'protected');
    Yii::setPathOfAlias('admin_module', Yii::getPathOfAlias('stm_app').DS.'modules'.DS.'admin');
    Yii::setPathOfAlias('admin2_module', Yii::getPathOfAlias('stm_app').DS.'modules'.DS.'admin2');
    Yii::setPathOfAlias('hq_module', SITE_ROOT.DS.'stm_domains'.DS.'seizethemarket.com'.DS.'protected'.DS.'modules'.DS.'hq');
    Yii::setPathOfAlias('front_module', Yii::getPathOfAlias('stm_app').DS.'modules'.DS.'front');
    Yii::setPathOfAlias('admin_widgets', Yii::getPathOfAlias('admin_module.components.widgets'));
    Yii::setPathOfAlias('front_widgets', Yii::getPathOfAlias('front_module.components.widgets'));
    Yii::setPathOfAlias('admin_exts', Yii::getPathOfAlias('admin_module.extensions'));
    Yii::setPathOfAlias('seizethemarket', SITE_ROOT.DS.'stm_domains'.DS.'seizethemarket.com');
    define('API_SUFFIX', 'local');

    $config = getInitialStmConfig();
    $config = mergeClientConfig($config);


//console command mode
else:
    require_once (YII_FRAMEWORK_PATH.DS.'yii.php');

    Yii::setPathOfAlias('stm_app', SITE_ROOT.DS.'stm_app'.DS.'protected');
    Yii::setPathOfAlias('admin_module', Yii::getPathOfAlias('stm_app').DS.'modules'.DS.'admin');
    Yii::setPathOfAlias('admin_widgets', Yii::getPathOfAlias('admin_module.components.widgets'));
    Yii::setPathOfAlias('hq_module', SITE_ROOT.DS.'stm_domains'.DS.'seizethemarket.com'.DS.'protected'.DS.'modules'.DS.'hq');
    Yii::setPathOfAlias('yii_framework', YII_FRAMEWORK_PATH);
    Yii::setPathOfAlias('seizethemarket', SITE_ROOT.DS.'stm_domains'.DS.'seizethemarket.com');

    $config = getInitialStmConfig();
    $config = mergeClientConfig($config);
    unset($config['theme']);

//    $config['components'] = CMap::mergeArray(
//        $config['components'], array(
//            'import'     => array(
//                'application.components.*',
//            ),
//        )
//    );

    $yiic = YII_FRAMEWORK_PATH.'/yiic.php';
endif;
/****************************************************************************/

/**
 * getInitialStmConfig - setup the config array starting from admin module main config file, then merge client db settings based on environment
 * @return array|mixed
 */
function getInitialStmConfig () {

    (YII_DEBUG) ? require_once(dirname(__FILE__) .DS.'development.php') : require_once(dirname(__FILE__) .DS.'production.php');
    // Get parent configurations
    //$parentConfig = require_once(SITE_ROOT.DS.'stm_app'.DS.'protected'.DS.'modules'.DS.'admin'.DS.'config'.DS.'main.php'); //Yii::getPathOfAlias('admin_module') not defined yet

    /**
     *
     * Moved admin module config file into here - modify later for smoother merge of data, just doing cut/paste for now
     *
     */

    // Require composer autoloader
    require_once Yii::getPathOfAlias('admin_module') . DS . 'vendor' . DS . 'autoload.php';

    $httpHost = '';
    if(!YII_IS_CONSOLE) {

        // Used to dynamically assign the host to the cdbsession
        $httpHost  = $_SERVER['HTTP_HOST'];
        $hostParts = explode('.', $httpHost);
        $httpHost  = "{$hostParts[$domainNameIdx = 1]}.{$hostParts[$tldIdx = 2]}";
    }

    // This is the main Web application configuration. Any writable
    // CWebApplication properties can be configured here.
    $parentConfig = array(
        'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
        'name'       => 'Seize The Market',
        'timeZone' => STM_CLIENT_TIME_ZONE,
        'theme'=>'default',

        // preloading 'log' component
        'preload'    => array('log'),

        // autoloading model and component classes
        'import'     => array(
            'hq_module.models.*',
            'application.models.*',
            'application.components.*',
            'application.helpers.*',
            'stm_app.modules.admin.extensions.yii-mail.*',
            'stm_app.modules.admin.components.widgets.StmWidget'
        ),

        'modules'    => array(
            // uncomment the following to enable the Gii tool
            'gii'   => array(
                'class'     => 'system.gii.GiiModule',
                'password'  => 'dev',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters' => array(),
            ),
            'api'   => array(
                'class' => 'stm_app.modules.api.ApiModule',
            ),
            'admin' => array(
                'class' => 'stm_app.modules.admin.AdminModule',
            ),
            'admin2' => array(
                'class' => 'stm_app.modules.admin2.AdminModule',
            ),
            'front' => array(
                'class' => 'stm_app.modules.front.FrontModule'
            )
        ),

        // application components
        'components' => array(
            'widgetFactory'     => array(
                'widgets' => array(
                    'CJuiDateTimePicker' => array(
                        'options' => array(
                            'controlType' => 'select',
                            'ampm'        => true,
                        )
                    ),

                    // Set some defaults for the CJuiTabs including a loading message
                    'CJuiTabs'           => array(
                        'options' => array(
                            'beforeLoad' => new CJavaScriptExpression('
			        		function (event, ui) {
			        			if ($(ui.panel).is(":empty")) {
				        			$(ui.panel).html("<p><strong>Loading, please wait...</strong></p>");
			        			}
							}
		        		'),
                        ),
                    ),
                ),
            ),

            'assetManager' => array(),
            'clientScript' => array('coreScriptPosition'=>CClientScript::POS_END,'defaultScriptFilePosition'=>CClientScript::POS_END),

            'apcCache' => array(
                'class' => 'system.caching.CApcCache',
            ),

            //        'cache' => array(
            //            'class' => 'system.caching.CMemCache',
            //            'servers' => array(
            //                array(
            //                    'host' => 'localhost',
            //                    'port' => 11211,
            //                    'weight' => 60
            //                ),
            //            )
            //        ),
            'fileCache' => array(
                'class' => 'system.caching.CFileCache',
            ),

            'registry'          => array(
                'class' => 'admin_module.components.Registry',
            ),

            //        'sms'               => array(
            //            'class' => 'admin_module.components.StmTwilio.StmTwilio',
            //            //use twilio extension temporarily
            //            'sid'   => 'AC6a1103fae1ae51af43593b3832eef244',
            //            //AC35d8934c5baa6c3efbd2a385f66129e4 (real), AC6a1103fae1ae51af43593b3832eef244 (test)
            //            'token' => 'af85442d998ee38ca29a5c3aaf0ed664',
            //            //c2b8598778253c1073833c95561007bc (real), af85442d998ee38ca29a5c3aaf0ed664 (test)
            //        ),
            'stmFunctions'          => array(
                'class' => 'admin_module.components.StmFunctions',
            ),
            'bombbomb'          => array(
                'class' => 'admin_module.components.StmBombBomb',
            ),
            'plivo'             => array(
                'class'         => 'admin_module.components.StmPlivo.StmPlivo',
                'auth_id'       => 'MANGFLOWNJMJU5NWY2OD',
                'auth_token'    => 'Nzc4NjU2M2UyY2ZmNmZkYTdiMzE4NWVkYjM1OGI4',
                'primary_phone' => '19048531990'
            ),
            'twilio'            => array(
                'class'         => 'admin_module.components.StmTwilio.StmTwilio',
            ),
            // ready for use, uncomment to use
            //			'geoip' => array(
            //				'class' => 'admin_exts.geoip.CGeoIP',
            //				'filename' => dirname(dirname(dirname(dirname(__FILE__)))).'/data/GeoLiteCity.dat',
            //				// Choose MEMORY_CACHE or STANDARD mode
            //				'mode' => 'STANDARD',
            //			),

            'session'           => array(
                'class'                  => 'admin.components.StmSession',
                'autoCreateSessionTable' => false,
                'connectionID'           => 'db',
                'timeout'                => 60 * 60 * 8,
                // set for 8 hours
                'cookieParams'           => array(
                    'lifetime' => 0,
                    'path'     => '/',
                    'domain'   => '.' . $httpHost,
                ),
            ),

            'passwordGenerator' => array(
                'class' => 'admin_module.components.StmPasswordGenerator',
            ),
            'imap'              => array(
                'class'            => 'admin_module.components.StmImap',
                'connectionString' => '{mail.seizethemarket.net:993/imap/ssl/novalidate-cert/norsh}INBOX',
                'userName'         => 'contactresponses@seizethemarket.net',
                'password'         => 'bruTr7Ph',
            ),
            //        'imap'              => array(
            //            'class'            => 'admin_module.components.StmImap',
            //            'connectionString' => '{mail.seizethemarket.com:993/imap/ssl/novalidate-cert/norsh}INBOX',
            //            'userName'         => 'web@seizethemarket.com',
            //            'password'         => 'stmapp',
            //        ),
            //        'image'=>array(
            //            'class'=>'application.extensions.image.CImageComponent',
            //            // GD or ImageMagick
            //            'driver'=>'ImageMagick',
            //            // ImageMagick setup path
            ////            'params'=>array('directory'=>'/opt/local/bin'),
            //            'params'=>array('directory'=>'/Applications/MAMP/bin/ImageMagick/ImageMagick-6.6.7'),
            //        ),
            'trafficTracker'    => array(
                'class' => 'admin_module.components.StmTrafficTracker',
            ),

            'userAgent'         => array(
                'class' => 'admin_module.components.StmUserAgentParser',
            ),

            'user'              => array(
                // enable cookie-based authentication
                'allowAutoLogin' => true,
                'class'          => 'admin_module.components.StmWebUser',
            ),

            'authManager'       => array(
                'class'           => 'CDbAuthManager',
                'connectionID'    => 'db',
                'assignmentTable' => 'auth_assignment',
                'itemTable'       => 'auth_item',
                'itemChildTable'  => 'auth_item_child',
            ),

            'curl'              => array(
                'class'   => 'admin_module.extensions.Curl',
                'options' => array(
                    'setOptions' => array(
                        CURLOPT_SSL_VERIFYPEER => false,
                    ),
                ),
            ),
            'dropbox'       => array(
                'class' => 'admin_module.extensions.dropbox.YiiDropbox',
                'appKey' => '',
                'appSecret' => '',
                'root' => 'dropbox' //or 'sandbox'
            ),
            'phpThumb'          => array(
                'class' => 'admin_module.extensions.EPhpThumb.EPhpThumb',
            ),

            'request'           => array(
                'class' => 'admin_module.components.StmHttpRequest',
            ),
            'mail'              => array(
                'class'            => 'admin_module.components.StmMail',
                'transportType'    => 'smtp',
                'viewPath'         => 'admin_module.views.emails',
                'bccRecipients'    => array(
                    //                'jljeff@fdn.com',
                    //                'sandman0035@hotmail.com',
                    //                'jstoopswsp@yahoo.com',
                    //                'jeffsandman0035@aol.com'
                ),
                'transportOptions' => array(
                    'host'      =>  'mail.seizethemarket.net',
                    'username'  =>  'donotreply@seizethemarket.net',
                    'password'  =>  'bj2K8AcD',
                    'port'      =>  587,
                    //                'host'     => 'mail.seizethemarket.com',
                    //                'username' => 'web@seizethemarket.com',
                    //                'password' => 'stmapp',
                    //                'port'     => '587',
                ),
            ),
            'mailSes'              => array(
                'class'            => 'admin_module.extensions.yii-mail.YiiMail',
                'transportType'    => 'smtp',
                'viewPath'         => 'admin_module.views.emails',
                'transportOptions' => array(
                    'host'     => 'email-smtp.us-east-1.amazonaws.com',
                    'username' => 'AKIAJKCAXELHHWFQDVTA',
                    'password' => 'Ap9pkvq+wphphQ2cazcjlenYBa+R9EMvRc/LqvIwZL84',
                    'port'     => '587',
                ),
            ),

            'phpseclib' => array(
                'class' => 'admin_exts.phpseclib.PhpSecLib',
            ),

            // uncomment the following to enable URLs in path-format
            'urlManager'        => array(
                'urlFormat'      => 'path',
                'showScriptName' => false,
                'rules'          => array(
                    //------------------------Admin Module site url rules-----------------------------------------------------
                    'admin'                                                                         => 'admin/main/index',
                    'admin/logout'                                                                  => 'admin/main/logout',
                    'admin/dashboard'                                                               => 'admin/main/dashboard',
                    'admin/news'                                                                    => 'admin/main/news',

                    'admin2'                                                                        =>  'admin2/index/index',
                    'admin2/<controller:\w+>/<action:\w+>'                                          =>  'admin2/<controller>/<action>',

                    // Rest Api Rules
                    array(
                        'api/resource/list',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>',
                        'verb'    => 'GET'
                    ),
                    array(
                        'api/resource/create',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>',
                        'verb'    => 'POST'
                    ),
                    array(
                        'api/resource/view',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>',
                        'verb'    => 'GET'
                    ),
                    array(
                        'api/resource/update',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>',
                        'verb'    => 'PUT'
                    ),
                    array(
                        'api/resource/delete',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>',
                        'verb'    => 'DELETE'
                    ),
                    array(
                        'api/resource/list',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>/<association:\w+>',
                        'verb'    => 'GET'
                    ),
                    array(
                        'api/resource/create',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>/<association:\w+>',
                        'verb'    => 'POST'
                    ),
                    array(
                        'api/resource/view',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>/<association:\w+>/<associationId:\d+>',
                        'verb'    => 'GET'
                    ),
                    array(
                        'api/resource/update',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>/<association:\w+>/<associationId:\d+>',
                        'verb'    => 'PUT'
                    ),
                    array(
                        'api/resource/delete',
                        'pattern' => 'api/v<version:\d+>/<resource:\w+>/<resourceId:\d+>/<association:\w+>/<associationId:\d+>',
                        'verb'    => 'DELETE'
                    ),

                    'admin/permissions/assign/<name:\w+>/<action:\w+>'                                                                    => 'admin/permissions/assign',
                    'admin/permissions/<action:(edit|delete)>/<name:\w+>'                                                                 => 'admin/permissions/<action>',
                    'admin/permissions/<action:\w+>/<type:\w+>'                                                                           => 'admin/permissions/<action>',

                    'admin/closings/companyContacts/<type:\w+>'                                                                           => 'admin/closings/companyContacts',
                    'admin/contacts/add/<componentName:(sellers|buyers|recruits)>'                                                        => 'admin/contacts/add',

                    'admin/cms/<pageType:(addListingStoryboard|addBuyerStoryboard)>/<transactionId:\d+>'                                  => 'admin/cms/<pageType>',
                    'admin/cms/<pageType:(addPage|addBlog|addHouseValues|addLandingPages)>/<domainId:\d+>/'                                                              => 'admin/cms/<pageType>',
                    'admin/cms/<pageType:(addPage)>/<domainId:\d+>/template/<template:\w+>/status/<status:\w+>'                           => 'admin/cms/<pageType>',
                    'admin/cms/<pageType:(editPage|editBlog|editListingStoryboard|editHouseValues|editLandingPages)>/<id:\d+>/template/<template:\w+>/status/<status:\w+>' => 'admin/cms/<pageType>',
                    'admin/cms/addComment/<cmsContentId:\d+>'                                                                             => 'admin/cms/addComment',
                    'admin/listingStoryboards'                                                                                            => 'admin/cms/listListingStoryboard',
                    'admin/buyerStoryboards'                                                                                              => 'admin/cms/listBuyerStoryboard',
                    'admin/houseValues'                                                                                                   => 'admin/cms/listHouseValues',

                    //                'admin/dialerimport/files'                                                                                            => 'admin/import/files',
                    //                'admin/dialerimport/reconcile'                                                                                        => 'admin/import/reconcile',
                    //                'admin/dialerimport/getColissionData/<id:\d+>'                                                                        => 'admin/import/getColissionData',

                    'admin/<transactionType:(sellers|buyers|listings)>'                                                                   => 'admin/transactions/index',
                    'admin/<transactionType:(sellers|buyers|listings)>/add/<contactId:\d+>/*'                                             => 'admin/transactions/add',
                    //					'admin/<transactionType:(sellers|buyers|listings)>/add/<contactId:\d+>/source/<sourceId:\d+>' => 'admin/transactions/add',
                    'admin/<transactionType:(sellers|buyers|listings)>/<id:\d+>'                                                          => 'admin/transactions/view',
                    'admin/<transactionType:(sellers|buyers|listings)>/<action:\w+>'                                                      => 'admin/transactions/<action>',
                    'admin/<transactionType:(sellers|buyers|listings)>/<action:\w+>/<id:\d+>'                                             => 'admin/transactions/<action>',
                    'admin/<transactionType:(sellers|buyers|listings)>/<action:\w+>/<category:\w+>'                                       => 'admin/transactions/<action>',
                    'admin/<transactionType:(sellers|buyers|listings)>/list/*'                                                                 => 'admin/transactions/index',

                    // call session controller pretty url
                    '/admin/callSession/add/<type:(task|bucket|manualqueue)>'                                                             => 'admin/callSession/<type>',

                    'admin/recruits/add/<contactId:\d+>/*'                                                                                => 'admin/recruits/add',

                    // Pass the contact id through to the email templates view action for parsing
                    'admin/emailTemplates/<id:\d+>/contact/<contactId:\d+>'                                                               => 'admin/emailTemplates/view',
                    'admin/smsTemplates/<id:\d+>/contact/<contactId:\d+>'                                                                 => 'admin/smsTemplates/view',

                    'admin/actionPlans/<id:\d+>'                                                                                          => 'admin/actionPlans/view',

                    'admin/craigslist/post/<propertyTypeName:\w+>/<listingId:\w+>'                                                        => 'admin/craigslist/post',
                    'admin/imageManager/<action:\w+>' => 'admin/imageManager/<action>',
                    'admin/<controller:\w+>'                                                                                              => 'admin/<controller>/index',
                    'admin/<controller:\w+>/<id:\d+>'                                                                                     => 'admin/<controller>/view',
                    'admin/<controller:\w+>/<action:\w+>/<id:\d+>'                                                                        => 'admin/<controller>/<action>',
                    'admin/<controller:\w+>/<action:\w+>/<category:(a|b|c)>'                                                              => 'admin/<controller>/<action>',
                    'admin/<controller:\w+>/<action:\w+>/<id:\w+>'                                                                        => 'admin/<controller>/<action>',
                    'admin/<controller:\w+>/<action:\w+>'                                                                                 => 'admin/<controller>/<action>',

                    //-------------------------Gii url rules------------------------------------------------------------------
                    'gii'                                                                                                                 => (YII_DEBUG) ? 'gii' : 'front/site/index',
                    'gii/<controller:\w+>'                                                                                                => (YII_DEBUG) ? 'gii/<controller>' : 'front/site/index',

                    //--------------------------Front Module site url rules---------------------------------------------------
                    '/'                                                                                                                   => 'front/site/index',
                    'login'                                                                                                               => 'front/site/login',
                    'logout'                                                                                                              => 'front/site/logout',
                    'optout/<id:\d+>/*'                                                                                                   => 'front/site/optout',
                    'optout/confirm/<id:\d+>/*'                                                                                           => 'front/site/optout-confirm',
                    'click2*'                                                                                                             => 'front/site/click2',
                    'click3*'                                                                                                             => 'front/site/click3',
                    'click/<encodedLinkId:[\w\*\-]+>/<encodedContactId:[\w\*\-]+>/<emailMessageId:\d+>'                                   => 'front/site/click',

                    'install/<authCode:\w+>'                                                                                              => 'front/site/install',

                    'blog'                                                                                                                => 'front/site/blog',
                    'buyer'                                                                                                               => 'front/site/buyer',
                    'seller'                                                                                                              => 'front/site/seller',
                    'topics'                                                                                                              => 'front/site/blog',
                    'topics/<tagUrl:[\w\-]+>'                                                                                             => 'front/site/blogTag',
                    'forgotPassword'                                                                                                      => 'front/site/forgotPassword',
                    'forgotpassword'                                                                                                      => 'front/site/forgotPassword',
                    'value/*'                                                                                                             => 'front/site/values',
                    'values/*'                                                                                                            => 'front/site/values',
                    'free/*'                                                                                                              => 'front/site/values',
                    'ourlistings'                                                                                                         => 'front/site/ourlistings',
                    'propertytaxcalculator'                                                                                               => 'front/site/propertytaxcalculator',
                    'marketUpdates/<id:\d+>'                                                                                              => 'front/site/marketUpdates',
                    'marketsnapshot'                                                                                                      => 'front/site/marketsnapshot',
                    'marketsnapshot/<id:\w+>'                                                                                             => 'front/site/marketsnapshot',
                    'mortgagecalculator'                                                                                                  => 'front/site/mortgagecalculator',
                    'mortgagecalculator/<id:\w+>'                                                                                         => 'front/site/mortgagecalculator',
                    'houseValues/<url:[\w\-]+>/*'                                                                                         => 'front/site/houseValues',
                    'l/<url:[\w\-\.]+>/*'                                                                                                 => 'front/site/landingPages',
                    'reverseProspects'                                                                                                    => 'front/site/reverseProspects',
                    'search'                                                                                                              => 'front/site/search/id/full',
                    'searchAutocompleteNeighborhood'                                                                                      => 'front/site/searchAutocompleteNeighborhood',
                    'search/<id:\w+>'                                                                                                     => 'front/site/search',
                    'privacy'                                                                                                             => 'front/site/privacy',

                    'homes*'                                                                                                              => '/front/site/homes',

                    //'homes/schoolsearch/<school:\w+>'                                                                                     => 'front/site/homes',
                    'home/<state:\w+>/<city:[\w\-\.]+>/<address:[\w\-\.]+>/<id:[\w\-]+>'                                                  => 'front/site/home',
                    'home/<state:\w+>/<zip:\w+>/<city:[\w\-\.]+>/<address:[\w\-\.]+>/<id:[\w\-]+>'                                        => 'front/site/home',
                    'home/<state:\w+>/<zip:\w+>/<city:[\w\-\.]+>/<address:[\w\-\.]+>/<id:[\w\-]+>/<mlsBoardId:\d+>'                       => 'front/site/home',

                    'home-ajax/<id:\d+>' => 'front/site/home-ajax',

                    'map/<id:[\w\-]+>'                                                                                                    => 'front/site/map',
                    'area/<area:[\w\-]+>/*'                                                                                               => 'front/site/area',
                    'videoEmail'                                                                                                          => 'front/site/videoEmail',
                    'storyboard/<title:[\w\-]+>/<id:\d+>'                                                                                 => 'front/site/storyboard',
                    'storyboard'                                                                                                          => 'front/site/storyboardList',
                    'printFlyer/<id:[\w\-]+>'                                                                                             => 'front/site/printFlyer',

                    'mortgage'                                                                                                            => 'front/site/mortgage',
                    'mortgages'                                                                                                           => 'front/site/mortgage',
                    'team'                                                                                                                => 'front/site/team',
                    'team/<id:\d+>/<name:[\w\-]+>'                                                                                        => 'front/site/team',
                    'contact'                                                                                                             => 'front/site/contact',
                    'referanagent'                                                                                                        => 'front/site/referanagent',

                    'myAccount/<email:[^@]+@\w+\.\w+>'                                                                                    => 'front/myAccount/index',
                    'myAccount/'                                                                                                          => 'front/myAccount/index',
                    'myAccount/<action:\w+>'                                                                                              => 'front/myAccount/<action>',
                    'myAccount/storyboards/<id:\d+>'                                                                                      => 'front/myAccount/storyboardsEdit',
                    'myAccount/<action:\w+>/<id:\d+>'                                                                                     => 'front/myAccount/<action>',

                    'plivo/<action:\w+>/*'                                                                                                =>  'front/plivo/<action>',
                    'plivo2/<action:\w+>/*'                                                                                               =>  'front/plivo/<action>',
                    'plivocall/<action:\w+>/*'                                                                                            =>  'front/plivocall/<action>',
                    'phone/<action:\w+>/*'                                                                                                =>  'front/phone/<action>',
                    'plivoleadroute/<action:\w+>/*'                                                                                       =>  'front/plivoleadroute/<action>',
                    'plivoclicktocall/<action:\w+>/*'                                                                                     =>  'front/plivoclicktocall/<action>',
                    //'plivotest/<action:\w+>/*'                                                                                              =>  'front/plivotest/<action>',
                    //'testincoming/<action:\w+>/*'                                                                                              =>  'front/testincoming/<action>',
                    'plivo3/<action:\w+>/*'                                                                                               =>  'front/plivo3/<action>',
                    'plivoivr/<action:\w+>/*'                                                                                             =>  'front/plivoivr/<action>',
                    'plivoivrhuntgroup/<action:\w+>/*'                                                                                    =>  'front/plivoivrhuntgroup/<action>',
                    'plivoivrrecord/<action:\w+>/*'                                                                                       =>  'front/plivoivrrecord/<action>',
                    'plivovoicemail/<action:\w+>/*'                                                                                       =>  'front/plivovoicemail/<action>',
                    'twiliophone/<action:\w+>/*'                                                                                          =>  'front/twiliophone/<action>',
                    'nodeapi/<action:\w+>/*'                                                                                              =>  'front/nodeapi/<action>',

                    'searchSuggestions'																									  => 'front/site/searchSuggestions',

                    'mapsearch/search/*'                                                                                                  => 'front/mapsearch/search',
                    'mapsearch*'                                                                                                          => 'front/mapsearch/index',

                    '<pageUrl>'                                                                                                           => 'front/site/page',
                ),
            ),

            // uncomment the following to use a MySQL database
            'db'                => array(
                'connectionString'      => 'mysql:host=localhost;dbname=',
                'class'                 => 'CDbConnection',
                'emulatePrepare'        => true,
                'username'              => 'root',
                'password'              => STM_DB_DEV_PW,
                'charset'               => 'utf8',
                'schemaCachingDuration' => 3600,
                'enableProfiling'       => false,
            ),

            'stm_mls'                   => array(
                'connectionString'      => 'mysql:host=localhost;dbname=stm_mls',
                'class'                 => 'CDbConnection',
                'emulatePrepare'        => true,
                'username'              => 'root',
                'password'              => STM_DB_DEV_PW,
                'charset'               => 'utf8',
                'schemaCachingDuration' => 3600,
                'enableProfiling'       => false,
            ),

            'stm_hq'                    => array(
                'connectionString'      => 'mysql:host=localhost;dbname=stm_hq',
                'class'                 => 'CDbConnection',
                'emulatePrepare'        => true,
                'username'              => 'root',
                'password'              => STM_DB_DEV_PW,
                'charset'               => 'utf8',
                'schemaCachingDuration' => 3600,
                'enableProfiling'       => false,
            ),

            'stm_public_records'                    => array(
                'connectionString'      => 'mysql:host=localhost;dbname=stm_public_records',
                'class'                 => 'CDbConnection',
                'emulatePrepare'        => true,
                'username'              => 'root',
                'password'              => STM_DB_DEV_PW,
                'charset'               => 'utf8',
                'schemaCachingDuration' => 3600,
                'enableProfiling'       => false,
            ),

            'stm_admin'                    => array(
                'connectionString'      => 'mysql:host=localhost;dbname=stm_admin',
                'class'                 => 'CDbConnection',
                'emulatePrepare'        => true,
                'username'              => 'root',
                'password'              => STM_DB_DEV_PW,
                'charset'               => 'utf8',
                'schemaCachingDuration' => 3600,
                'enableProfiling'       => false,
            ),

            // This should be used as a fallback
            'errorHandler'      => array(
                // use 'site/error' action to display errors
                'errorAction' => '/site/error',
            ),

            'log'               => array(
                'class'  => 'CLogRouter',
                'routes' => array(
                    //                array(
                    //                    'class'   => 'CFileLogRoute',
                    //                    'logFile' => 'php_errors.log',
                    //                    'except'  => 'exception.CHttpException.404',
                    //                    'levels'  => 'error',
                    //                ),
                    array(
                        'class'    => 'admin_module.components.StmEmailLogRoute',
                        'emails'   => array('debug@seizethemarket.com'),
                        'except'   => 'exception.CHttpException.404',
                        'sentFrom' => 'STM Error <automated@seizethemarket.com>',
                        'subject'  => 'An Error Has Occurred',
                        'levels'   => 'error',
                    ),
                    array(
                        'class'      => 'CFileLogRoute',
                        'logFile'    => '404_errors.log',
                        'levels'     => 'error',
                        'categories' => 'exception.CHttpException.404',
                    ),
                    array(
                        'class'      => 'CFileLogRoute',
                        'logFile'    => 'leadgen.errors.log',
                        'levels'     => 'error, trace, debug, info, warning',
                        'categories' => 'leadgen',
                    ),
                    array(
                        'class'      => 'CFileLogRoute',
                        'logFile'    => 'abstractform.errors.log',
                        'levels'     => 'error, trace, debug, info, warning',
                        'categories' => 'abstractform',
                    ),
                    array(
                        'class'      => 'CFileLogRoute',
                        'logFile'    => 'spam.errors.log',
                        'levels'     => 'error, trace, debug, info, warning',
                        'categories' => 'spam',
                    ),
                    array(
                        'class'      => 'CFileLogRoute',
                        'logFile'    => 'optedout.log',
                        'levels'     => 'warning',
                        'categories' => 'optout',
                    ),
                    //                					array(
                    //                						'class' => 'CWebLogRoute',
                    //                						'levels'=>'trace,info,error,warning',
                    //
                    //                					),
                ),
            ),

            'format'            => array(
                'class' => 'admin_module.components.StmFormatter',
            ),
        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'     => array(
            // this is used in contact page
            'adminEmail' => 'admin@seizethemarket.com',
            'layoutDirectory'=>'standard1',
            'isCliEnv' => (php_sapi_name() == 'cli'),
            //            'defaultPageSize'=>20,
            //            'pageSizeOptions'=>array(10=>10,20=>20,50=>50,100=>100),
        ),
    );














    // Extend the parent configuration, must supply the basePath so the application will use the domain's folder as the base folder
    $config = CMap::mergeArray($parentConfig, array(
            'basePath'=>CLIENT_REPO . DS . 'protected',
            'name'=>STM_CLIENT_NAME,
        ));

    /********************************************************************************************************/
    if(ENVIRONMENT == 'development'):

        $config['components'] = CMap::mergeArray(
            $config['components'],
            array(
                'timeZone' => STM_CLIENT_TIME_ZONE,
                'db'      => array(
                    'connectionString' => 'mysql:host=localhost;dbname='.STM_CLIENT_DB,
                    'username'         => 'root',
                    'password'         => STM_DB_DEV_PW,
                    'initSQLs'         => array(
                        'SET time_zone = "'.STM_CLIENT_TIME_ZONE.'"',
                    ),
                ),
            )
        );
    endif;
    /********************************************************************************************************/

    if(ENVIRONMENT == 'production' || YII_IS_CONSOLE_PRODUCTION==true):
        $config['components'] = CMap::mergeArray(
            $config['components'], array(
                'timeZone' => STM_CLIENT_TIME_ZONE,
                'db' => array(
                    'connectionString' => 'mysql:host='.STM_DB_HOST.';dbname='.STM_CLIENT_DB,
                    'username'              => STM_DB_USERNAME,
                    'password'              => STM_DB_PW,
                    'initSQLs'         => array(
                        'SET time_zone = "'.STM_CLIENT_TIME_ZONE.'"',
                    ),
                ),
                'stm_mls' => array(
                    'connectionString' => 'mysql:host='.STM_DB_HOST.';dbname=stm_mls',
                    'username'              => STM_DB_USERNAME,
                    'password'              => STM_DB_PW,
                    'initSQLs'         => array(
                        'SET time_zone = "'.STM_CLIENT_TIME_ZONE.'"',
                    ),
                ),
                'stm_hq' => array(
                    'connectionString' => 'mysql:host='.STM_DB_HOST.';dbname=stm_hq',
                    'username'              => STM_DB_USERNAME,
                    'password'              => STM_DB_PW,
                    'initSQLs'         => array(
                        'SET time_zone = "'.STM_CLIENT_TIME_ZONE.'"',
                    ),
                ),
                'stm_admin' => array(
                    'connectionString' => 'mysql:host='.STM_DB_HOST.';dbname=stm_admin',
                    'username'              => STM_DB_USERNAME,
                    'password'              => STM_DB_PW,
                    'initSQLs'         => array(
                        'SET time_zone = "'.STM_CLIENT_TIME_ZONE.'"',
                    ),
                ),
                'stm_public_records' => array(
                    'connectionString' => 'mysql:host='.STM_DB_HOST.';dbname=stm_public_records',
                    'username'              => STM_DB_USERNAME,
                    'password'              => STM_DB_PW,
                    'initSQLs'         => array(
                        'SET time_zone = "'.STM_CLIENT_TIME_ZONE.'"',
                    ),
                ),
                'request' => array(
                    'hostInfo' => STM_CLIENT_HOST_INFO,
                ),
            )
        );
    endif;

    $config['components'] = CMap::mergeArray(
        $config['components'], array(
            'themeManager' => array(
                'class' => 'admin_module.components.ThemeManager.ThemeManager',
                'themeClass' => 'admin_module.components.StmTheme.StmTheme',
                '_basePath' => array(
                    Yii::getPathOfAlias('stm_app.themes')
                )
            )
        )
    );

	/********************************************************************************************************/
    return $config;
}