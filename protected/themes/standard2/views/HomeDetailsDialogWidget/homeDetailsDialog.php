<style>
    /*need to refresh assets for all clients then remove this line*/
    .home-details-dialog .dialog-form-container table, .home-details-dialog .dialog-form-container table th, .home-details-dialog .dialog-form-container table td{
        background: none;
        border: none;
    }
    /*need to refresh assets for all clients then remove this line*/
    .home-details-dialog .home-details-dialog div.property-image{
        width: 280px;
    }
</style>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id'      => $this->id,
	'options' => array(
		'title'       => $this->title,
		'autoOpen'    => false,
		'dialogClass' => $this->dialogClass,
		'modal'       => true,
		'closeOnEscape' => true,
        'width'         => 530,
	),
));

?>
	<div class="property-container">
		<div class="property-image">
			<img src="<?php echo $this->photoUrl; ?>" width="280">
		</div>
		<div class="property-info">
			<h3>
				<?php echo $this->property->formatMlsAddress($this->property, array('dialog' => 1));?><br />
			</h3>
			<div class="price"><?php echo Yii::app()->format->formatDollars($this->property['price'])?></div>
			<div class="details">
				<?php echo $this->property->bedrooms; ?> Beds / <?php echo $this->property->baths_full; ?> Bath<br>
				<?php echo $this->property->sq_feet; ?> Sq. Feet<br>
			</div>
		</div>
	</div>

	<?php echo $this->render($this->formView, array('model' => $model, 'FormFields' => $FormFields)); ?>

<?php $this->endWidget('zii.widgets.jui.CJuiDialog');