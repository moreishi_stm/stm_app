<?php
$makeAnOfferForm = $this->beginWidget('CActiveForm', array(
	'id' => $this->type.'-dialog-form',
    'action' => array('/front/forms/makeAnOffer/formId/' . $this->formId),
    'enableAjaxValidation'   => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
    	'validateOnChange' => false,
    	'validateOnSubmit' => true,
        'inputContainer' => 'span',
		'afterValidate' => 'js:function(form, data, hasErrors) {
		        homeDetailsAfterValidate();

				if (!hasErrors) {
					// Action was successful
					$("#'.$this->id.'").dialog("close");
					Message.create("success", "Your Offer Request has been submitted!");
					return false;
				}
			}',
    ),
));

$model->setScenario(Forms::FORM_MAKE_AN_OFFER_HOME_DETAILS);
?>
<style>
	.dialog-form-container.form-horizontal,
	.dialog-form-container.form-horizontal .form-group {
		margin-left: 0;
		margin-right: 0;
	}
</style>
<div class="clearfix"></div>
<div class="dialog-form-container form-horizontal" style="margin-top: 10px;">
	<div class="form-group">
		<?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('price')->id .']', array('class' => 'col-sm-2 control-label'))?>
		<div class="col-lg-8 col-md-8 col-sm-8">
			<span>
				<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('price')->id .']', $htmlOptions=array('placeholder'=>'Offer Price', 'class'=>'form-control', ));?>
			</span>
		</div>
		<?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('price')->id .']'); ?>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Name <span class="required">*</span></label>
		<div class="col-lg-8 col-md-8 col-sm-8">
			<span>
				<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'form-control', ));?>
			</span>
			<span>
				<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'form-control', ));?>
			</span>
		</div>
		<?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('first_name')->id .']');?>
		<?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('last_name')->id .']');?>
	</div>
	<div class="form-group">
		<?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('email')->id .']', array('class' => 'col-sm-2 control-label'))?>
		
		<div class="col-lg-8 col-md-8 col-sm-8">
			<span>
				<?php echo $makeAnOfferForm->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'form-control', ));?>
			</span>
		</div>
		<?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
	</div>
	<div class="form-group">
		<?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('phone')->id .']', array('class' => 'col-sm-2 control-label'))?>
		<div class="col-lg-8 col-md-8 col-sm-8">
			<span>
			<?php $this->widget('StmMaskedTextField', array(
				'model' => $model,
				'attribute' => 'data['. $FormFields->getField('phone')->id .']',
				'mask' => '(999) 999-9999',
				'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'form-control'),
			)); ?>
			</span>
		</div>
		<?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('phone')->id .']'); ?>
	</div>
	<div class="form-group">
		<?php echo $makeAnOfferForm->labelEx($model, 'data['. $FormFields->getField('comments')->id .']', array('class' => 'col-sm-2 control-label'))?>
		<div class="col-lg-8 col-md-8 col-sm-8">
			<span>
			<?php echo $makeAnOfferForm->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('placeholder'=>'Comments', 'class'=>'form-control', ));?>
			</span>
		</div>
		<?php echo $makeAnOfferForm->error($model, 'data['. $FormFields->getField('comments')->id .']'); ?>
	</div>
   <div class="form-group">
	   <div class="col-lg-12 col-md-12 col-sm-12">
			<input class="btn btn-primary" type="submit" value="Submit Now" />
		</div>
	</div>
	<div class="home-details-dialog-loading"></div>
</div>
<?php $this->endWidget(); //end CActiveForm ?>
