<?php
if(!$this->formId){
	$this->formId = 3;
}
$sendToFriendForm = $this->beginWidget('CActiveForm', array(
	'id' => $this->type.'-dialog-form',
    'action' => array('/front/forms/sendToFriend/formId/' . $this->formId),
    'enableAjaxValidation'   => false,
    'enableClientValidation' => false,
    'clientOptions' => array(
    	'validateOnChange' => false,
    	'validateOnSubmit' => true,
        'inputContainer' => 'span',
		'afterValidate' => 'js:function(form, data, hasErrors) {
		        homeDetailsAfterValidate();

                if (!hasErrors) {
					// Action was successful
					$("#'.$this->id.'").dialog("close");
					Message.create("success", "This Property has been sent to your Friend!");
					return false;
                }
			}',
    )
));
$model->setScenario(Forms::FORM_SHARE_FRIEND);
?>
<div class="dialog-form-container">
    <table>
    	<tr>
            <th>From Name <span class="required">*</span>:</th>
    		<td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                <span>
                </span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('first_name')->id .']');?>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('last_name')->id .']');?>
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $sendToFriendForm->labelEx($model, 'data['. $FormFields->getField('email')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'col-lg-10 col-xs-10', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('email')->id .']');?>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" style="text-align:center;"><hr /></td>
        </tr>
        <tr>
            <th>To Name <span class="required">*</span>:</th>
            <td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('to_first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                </span>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('to_last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('to_first_name')->id .']');?>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('to_last_name')->id .']');?>
            </td>
        </tr>
        <tr>
            <th><?php echo $sendToFriendForm->labelEx($model, 'data['. $FormFields->getField('to_email')->id .']')?>:</th>
            <td>
                <span>
    	    		<?php echo $sendToFriendForm->textField($model, 'data['. $FormFields->getField('to_email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'col-lg-10 col-xs-10', ));?>
                </span>
                <?php echo $sendToFriendForm->error($model, 'data['. $FormFields->getField('to_email')->id .']'); ?>
            </td>
        </tr>
    	<tr>
            <th><?php echo $sendToFriendForm->labelEx($model, 'data['. $FormFields->getField('comments')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $sendToFriendForm->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('placeholder'=>'Comments', 'class'=>'col-lg-10 col-xs-10', ));?>
                </span>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" class="submit-button-row">
                <div class="home-details-dialog-loading"></div>
                <input class="btn btn-primary ajaxSubmit" type="submit" value="Submit Now">
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); //end CActiveForm ?>