<?php
$propertyTaxForm = $this->beginWidget('CActiveForm', array(
	'id' => $this->type.'-dialog-form',
    'action' => array('/front/forms/propertyTax/formId/' . $this->formId),
    'enableAjaxValidation'   => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
    	'validateOnChange' => false,
    	'validateOnSubmit' => true,
        'inputContainer' => 'span',
		'afterValidate' => 'js:function(form, data, hasErrors) {
		        homeDetailsAfterValidate();

				if (!hasErrors) {
					// Action was successful
					$("#'.$this->id.'").dialog("close");
					Message.create("success", "Your Property Tax inquiry has been submitted!");
					return false;
				}
			}',
    ),
));

$model->setScenario(Forms::FORM_PROPERTY_TAX);
?>

<div class="dialog-form-container">
    <table>
    	<tr>
    		<th>Name <span class="required">*</span>:</th>
    		<td>
                <span>
    	    		<?php echo $propertyTaxForm->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                </span>
                <span>
    	    		<?php echo $propertyTaxForm->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $propertyTaxForm->error($model, 'data['. $FormFields->getField('first_name')->id .']');?>
                <?php echo $propertyTaxForm->error($model, 'data['. $FormFields->getField('last_name')->id .']');?>
        	</td>
    	</tr>
    	<tr>
    		<th><?php echo $propertyTaxForm->labelEx($model, 'data['. $FormFields->getField('email')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $propertyTaxForm->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'g10', ));?>
                </span>
                <?php echo $propertyTaxForm->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $propertyTaxForm->labelEx($model, 'data['. $FormFields->getField('phone')->id .']')?>:</th>
    		<td>
                <span>
                    <?php $this->widget('StmMaskedTextField', array(
                        'model' => $model,
                        'attribute' => 'data['. $FormFields->getField('phone')->id .']',
                        'mask' => '(999) 999-9999',
                        'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'g5'),
                    )); ?>
                </span>
                <?php echo $propertyTaxForm->error($model, 'data['. $FormFields->getField('phone')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
            <th><?php echo $propertyTaxForm->labelEx($model, 'data['. $FormFields->getField('comments')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $propertyTaxForm->textArea($model, 'data['. $FormFields->getField('comments')->id .']', $htmlOptions=array('placeholder'=>'Comments', 'class'=>'g10', ));?>
                </span>
                <?php echo $propertyTaxForm->error($model, 'data['. $FormFields->getField('comments')->id .']'); ?>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" class="submit-button-row">
                <div class="home-details-dialog-loading"></div>
                <input class="btn btn-primary" type="submit" value="Submit Now">
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); //end CActiveForm ?>
