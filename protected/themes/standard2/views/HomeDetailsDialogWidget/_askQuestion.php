<?php
$askQuestionForm = $this->beginWidget('CActiveForm', array(
	'id'     => $this->type.'-dialog-form',
    'action' => array('/front/forms/askQuestion/formId/' . $this->formId),
    'enableAjaxValidation'   => false,
    'enableClientValidation' => false,
    'clientOptions' => array(
    	'validateOnChange' => false,
    	'validateOnSubmit' => false,
        'inputContainer' => 'span'
    ),
));

$model->setScenario(Forms::FORM_ASK_A_QUESTION);
?>
<div class="dialog-form-container">
    <table>
    	<tr>
    		<th>Name <span class="required">*</span>:</th>
    		<td>
                <span>
    	    		<?php echo $askQuestionForm->textField($model, 'data['. $FormFields->getField('first_name')->id .']', $htmlOptions=array('placeholder'=>'First Name', 'class'=>'g5', ));?>
                </span>
                <span>
    	    		<?php echo $askQuestionForm->textField($model, 'data['. $FormFields->getField('last_name')->id .']', $htmlOptions=array('placeholder'=>'Last Name', 'class'=>'g5', ));?>
                </span>
                <?php echo $askQuestionForm->error($model, 'data['. $FormFields->getField('first_name')->id .']'); ?>
                <?php echo $askQuestionForm->error($model, 'data['. $FormFields->getField('last_name')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
    		<th><?php echo $askQuestionForm->labelEx($model, 'data['. $FormFields->getField('email')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $askQuestionForm->textField($model, 'data['. $FormFields->getField('email')->id .']', $htmlOptions=array('placeholder'=>'Email', 'class'=>'col-lg-10 col-xs-10', ));?>
                </span>
                <?php echo $askQuestionForm->error($model, 'data['. $FormFields->getField('email')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
    		<th>Phone<span class="required">*</span>:</th>
    		<td>
                <span>
                    <?php $this->widget('StmMaskedTextField', array(
                        'model' => $model,
                        'attribute' => 'data['. $FormFields->getField('phone')->id .']',
                        'mask' => '(999) 999-9999',
                        'htmlOptions' => $htmlOptions=array('placeholder'=>'Phone', 'class'=>'col-lg-10 col-xs-10'),
                    )); ?>
                </span>
                <?php echo $askQuestionForm->error($model, 'data['. $FormFields->getField('phone')->id .']'); ?>
        	</td>
    	</tr>
    	<tr>
    		<th><?php echo $askQuestionForm->labelEx($model, 'data['. $FormFields->getField('question')->id .']')?>:</th>
    		<td>
                <span>
    	    		<?php echo $askQuestionForm->textArea($model, 'data['. $FormFields->getField('question')->id .']', $htmlOptions=array('placeholder'=>'Question', 'class'=>'col-lg-10 col-xs-10', ));?>
                </span>
                <?php echo $askQuestionForm->error($model, 'data['. $FormFields->getField('question')->id .']'); ?>
        	</td>
    	</tr>
        <tr>
            <td colspan="2" class="submit-button-row">
                <div class="home-details-dialog-loading"></div>
                <input class="btn btn-primary ajaxSubmit" type="submit" value="Submit Now">
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); //end CActiveForm ?>