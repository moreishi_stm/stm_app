<style>
    #popular-topics-list-widget {
        width: 244px;
        font-family: arial, helvetica, sans-serif;
        padding: 12px 12px
    }
    #popular-topics-list-widget h3 {
        color: #444;
        text-align: left;
        font-size: 24px;
        font-weight: normal;
        margin-bottom: 6px;
    }
    #popular-topics-list-widget div {
        padding: 2px 0px;
    }
    #popular-topics-list-widget a {
        color: #2D78CC;
        font-size: 16px;
    }
    #popular-topics-list-widget a:hover {
        color: #D20000;
    }
    #popular-topics-list-widget ul li {
        list-style-image: url('http://www.<?php echo Yii::app()->user->getPrimaryDomain->name; ?>/images/bulletTriangle.png');
        font-size: 12px;
        margin-left: 18px;
    }
</style>
<?php if($cmsTags): ?>
<div id="popular-topics-list-widget" class="popular-topics-list-widget-container">
    <h3>Popular Topics <img src="http://cdn.seizethemarket.com/assets/images/star.png" alt="Popular Topics"/></h3>
    <ul>
        <?php
        foreach($cmsTags as $tag) {
            echo '<li>';
            echo CHtml::link($tag->name,'/topics/'.$tag->url);
            echo '</li>';
        }
    ?>
    </ul>
</div>
<?php endif; ?>