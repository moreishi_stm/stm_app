<style type="text/css">
	.video-thumbnail-container{
		position: relative;
		margin-top: 8px;
		border: 3px solid transparent;
		/*width: 97%;*/
		padding: 4px;
		-webkit-border-radius: 6px;
		-moz-border-radius: 6px;
		border-radius: 6px;
		text-align: center;
	}
	.video-thumbnail-container img {
		min-width: 200px;
		width: 100%;
		margin-left: auto;
		margin-right: auto;
		display: block;
		padding-top: 10px;
	}
	.video-thumbnail-container:hover {
		border: 3px solid #C20000;
		background: #FFEBEB;
	}
	.video-thumbnail-container a h5 {
		clear: both;
		font-size: 15px;
		cursor: pointer;
		color: #444;
		text-decoration: none;
	}
    .video-thumbnail-container a .play-button {
        background-image: url(http://cdn.seizethemarket.com/assets/images/play_button.png);
        position: absolute;
        top: 40%;
        left: 50%;
		transform: translate(-50%, -50%);
        width: 152px;
        height: 41px;
        background-position: 0px 44px;
    }
	
	/*@media screen and (max-width: 1000px) {
		#video-widget .video-thumbnail-container {
			width: 23%;
			min-width: 152px;
			border: none;
			padding: 0;
			margin: 2px;
		}
		#video-widget .video-thumbnail-container:last-of-type,
		#video-widget .video-thumbnail-container:nth-of-type(4){
			margin-right: 0px;
		}
		
		#video-widget .video-thumbnail-container a .play-button {
			left: 0;
		}
		#video-widget .video-thumbnail-container img {
			width: 100%;
			float: none;
		}
	}
	
	@media screen and (max-width: 660px) {
		#video-widget .video-thumbnail-container {
			width: 98%;
			min-width: 152px;
			border: none;
			padding: 0;
			margin: 0 2px 0 0;
		}
		
		#houseValues-box {
			width: 100%;
			height: auto;
			padding: 0;
		}
		
		#houseValues-container #houseValues-box div.row {
			padding: 10px;
		}
		
		#houseValues-box h1 {
			display: block;
			margin: 0 auto;
		}
	}*/
</style>
<div id="video-widget">
    <div class="video-thumbnail-container col-lg-12 col-md-4 col-sm-6 col-xs-12">
        <a href="/cpo">
            <em class="play-button"></em>
            <img class="video-thumbnail" src="http://i1.ytimg.com/vi/7HRhL22FZ8g/mqdefault.jpg">
            <h5>CPO Home Selling Strategy in the News</h5>
        </a>
    </div>
    <div class="video-thumbnail-container col-lg-12 col-md-4 col-sm-6 col-xs-12">
        <a href="/listing-storyboard-in-the-news">
            <em class="play-button"></em>
            <img class="video-thumbnail" src="http://i1.ytimg.com/vi/URDT17QaqKg/mqdefault.jpg">
            <h5>Listing Storyboard is a MUST for Home Sellers</h5>
        </a>
    </div>
    <div class="video-thumbnail-container col-lg-12 col-md-4 col-sm-6 col-xs-12">
        <a href="/customer-review-eric">
            <em class="play-button"></em>
            <img class="video-thumbnail" src="http://i1.ytimg.com/vi/VWemgoz51dI/mqdefault.jpg">
            <h5>Looking for Something Different...</h5>
        </a>
    </div>
    <div class="video-thumbnail-container col-lg-12 col-md-4 col-sm-6 col-xs-12">
        <a href="/hiring">
            <em class="play-button"></em>
            <img class="video-thumbnail" src="http://i1.ytimg.com/vi/fyhwHov0hv0/mqdefault.jpg">
            <h5>Real Estate Career Opportunity</h5>
        </a>
    </div>
    <div class="video-thumbnail-container col-lg-12 col-md-4 col-sm-6 col-xs-12">
		<a href="/hot-staging-tips-help-your-home-sell-itself">
            <em class="play-button"></em>
			<img class="video-thumbnail" src="http://i1.ytimg.com/vi/VhjR3lkyaLU/mqdefault.jpg">
			<h5>New Construction Homes Mythbuster</h5>
		</a>
	</div>
    <div class="video-thumbnail-container col-lg-12 col-md-4 col-sm-6 col-xs-12">
        <a href="/rewardsforheroes">
            <em class="play-button"></em>
            <img class="video-thumbnail" src="http://i1.ytimg.com/vi/XfS4L5HsTPc/mqdefault.jpg">
            <h5>Rewards for Heroes</h5>
        </a>
    </div>
</div>