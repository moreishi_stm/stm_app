<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-12 unstyled noMarginLeft">
	<li class="no-border"><p class="promo-1"><strong>Search by Price</strong></p></li>

    <li><a href="/homes/price_min/100000/price_max/200000"> $100,000's - $200,000's </a></li>
    <li><a href="/homes/price_min/200000/price_max/300000"> $200,000's - $300,000's </a></li>
    <li><a href="/homes/price_min/300000/price_max/400000"> $300,000's - $400,000's </a></li>
    <li><a href="/homes/price_min/400000/price_max/500000"> $400,000's - $500,000's </a></li>
    <li><a href="/homes/price_min/500000/price_max/600000"> $500,000's - $600,000's </a></li>
    <li><a href="/homes/price_min/500000/price_max/700000"> $600,000's - $700,000's </a></li>
</ul>

<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-12 unstyled noMarginLeft">
    <li class="no-border"><p class="promo-1"><strong>Luxury Homes</strong></p></li>

    <li><a href="/homes/price_min/700000/price_max/800000"> $700,000's - $800,000's </a></li>
    <li><a href="/homes/price_min/800000/price_max/900000"> $800,000's - $900,000's </a></li>
    <li><a href="/homes/price_min/1000000/price_max/2000000"> $1 - $2 Million </a></li>
    <li><a href="/homes/price_min/2000000/price_max/3000000"> $2 - $3 Million </a></li>
    <li><a href="/homes/price_min/4000000/price_max/5000000"> $4 - $5 Million </a></li>
    <li><a href="/homes/price_min/5000000/price_max/6000000"> $5 - $6 Million </a></li>
</ul>



<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-12 unstyled noMarginLeft">
    <li class="no-border"><p class="promo-1"><strong>Search by Property Type</strong></p></li>

    <li><a href="/homes"> All Properties for Sale </a></li>
    <li><a href="/homes/property_types/1"> Single Family Homes </a></li>
    <li><a href="/homes/property_types/2"> Condos </a></li>
    <li><a href="/homes/property_types/3"> Vacant Land </a></li>
</ul>