<?
Yii::app()->clientScript->registerCss('footerHouseValuesStandard2', <<<CSS
    #footer-house-values-container {
        height: 450px;
        overflow: hidden;
        background: black;
        background: url(http://cdn.seizethemarket.com/assets/images/neighborhood-aerial-3.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    #footer-house-values-container h2 {
        color: white;
        font-size: 30px;
    }
    #footer-house-values-container h3 {
        color: white;
        font-size: 25px;
        font-style: italic;
    }
    #house-values-form-container {
        padding: 40px;
        background-color: rgba(77, 163, 255, 0.9);
        margin-top: 60px;
        min-height: 250px;
    }
    #house-values-form-container input, #house-values-form-container select{
        font-size: 20px;
        height: 50px;
    }
    #house-values-form-container div:not(.error) input, #house-values-form-container div:not(.error) select{
        background: white;
    }
    #house-values-form-container #submit-button {
        background: #24D829;
        width: 100%;
    }
    #house-values-form-container .errorMessage {
        color: white;
    }
CSS
);?>

<div id="footer-house-values-container">
    <!--            <img src="http://cdn.seizethemarket.local/assets/images/neighborhood-aerial-2.jpg" style="width: 100%; opacity: .8; position: absolute; " alt=""/>-->
    <div class="container main-container" style="">
        <div id="house-values-form-container" class="col-lg-6 col-md-8 col-md-offset-2 col-lg-offset-3 col-xs-12" style="">
            <h2>What's My House Worth?</h2>
            <h3>FREE <?=date('F Y')?> House Values Report</h3>
                <?
                $formId           = Forms::FORM_HOUSE_VALUES;
                $formPart = 1;
                $formName = $formId.'_Part'.$formPart;
                $SubmissionValues = new FormSubmissionValues($formId);
                $SubmissionValues->formPart = $formPart;
                $FormFields       = new FormFields;
                $formAction       = '/front/forms/houseValues/formId/'.$formId;
                $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'houseValues-form',
                        'action' => array($formAction),
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnChange' => false,
                            'validateOnSubmit' => true,
                            'beforeValidate' => 'js:function(form, attribute) {
                                    $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                    return true;
                                }',
                            'afterValidate' => 'js:function(form, data, hasErrors) {
                                    if (!hasErrors && data.status =="successPart1") {
                                        window.location = "http://' . $_SERVER["SERVER_NAME"] . '/values/address/" + $("#houseValues-form #FormSubmissionValues_data_14").val() + "/city/" + $("#houseValues-form #FormSubmissionValues_data_16").val() + "/state/" + $("#houseValues-form #FormSubmissionValues_data_17").val() + "/zip/" + $("#houseValues-form #FormSubmissionValues_data_18").val() + "/status/2";
                                    } else if (!hasErrors) {
                                        window.location = "http://' . $_SERVER["SERVER_NAME"] . '/values/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/success";
                                    } else {
                                        $("div.loading-container.loading").remove();
                                    }
                                    return false;
                                }',
                        ),
                    )); ?>
			<div id="ValueReportFormGeoLocation_container" class="col-xs-12 no-padding">
				<input class="col-xs-12" placeholder="Type your address" name="ValueReportFormGeoLocation" id="ValueReportFormGeoLocation" type="text" autocomplete="off">
				<div class="errorMessage" id="ValueReportFormGeoLocation_error" style="display:none"></div>
				<div class="results"></div>
			</div>
                        <div class="col-xs-12 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('address').']', $htmlOptions=array('class'=>'col-xs-12','placeholder'=>'Address'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('address') . ']'); ?>
                        </div>
                        <div class="col-xs-6 col-s-12 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('city').']', $htmlOptions=array('class'=>'col-xs-12','data-prompt-position'=>'bottomLeft:0,6', 'placeholder'=>'City'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('city') . ']'); ?>
                        </div>
                        <div class="col-xs-3">
                            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getFieldIdByName('state').']', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name ASC')), 'short_name', 'short_name'),$htmlOptions=array('empty'=>'State','class'=>'col-xs-12'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('state') . ']'); ?>
                        </div>
                        <div class="col-xs-3 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('zip').']', $htmlOptions=array('class'=>'col-xs-12', 'placeholder'=>'Zip', 'maxlength'=>5));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('zip') . ']'); ?>
                            <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
                        </div>
                        <div class="col-xs-12 col-s-12 no-padding">
                            <input id="submit-button" type="submit" class="submit btn btn-lg btn-primary" value="Submit">
                        </div>
                <? $this->endWidget();
                ?>

        </div>
    </div>
</div>
<?php
$js = <<<JS
var ValueReportFormGeoLocation_results = [];
var ValueReportFormGeoLocation_selected = false;

function stmSetLocation(id){
	var selected = ValueReportFormGeoLocation_results[ id ];
	var street_number;
	var route;
	var city;
	var state;
	var zip;

	ValueReportFormGeoLocation_selected = id;
	$("#houseValues-form #ValueReportFormGeoLocation").val(selected.formatted_address);
	$("#houseValues-form #ValueReportFormGeoLocation_container .results").hide();
	$.each(selected.address_components,function( key, section){
		if(section.types[0] == "street_number"){
			street_number = section.long_name;
		} else if (section.types[0] == "route"){
			route = section.short_name;
		} else if (section.types[0] == "locality"){
			city = section.long_name;
		} else if (section.types[0] == "administrative_area_level_1"){
			state = section.short_name;
		} else if (section.types[0] == "postal_code"){
			zip = section.long_name;
		}
	});
	$("#houseValues-form #FormSubmissionValues_data_14").val(street_number+" "+route);
	$("#houseValues-form #FormSubmissionValues_data_16").val(city);
	 $.when( $("#houseValues-form  #FormSubmissionValues_data_17 option").removeAttr("selected") ).done(function() {
                $("#houseValues-form  #FormSubmissionValues_data_17 option:contains(" + state + ")").attr("selected", "selected");
            });
	$("#houseValues-form #FormSubmissionValues_data_18").val(zip);
}


$("#ValueReportFormGeoLocation_container .results").mouseleave(function(){
    $(this).hide();
});

$("#houseValues-form #ValueReportFormGeoLocation").on("input",function (e) {
	if (e.which == 40){ return; }
	$("#houseValues-form #ValueReportFormGeoLocation_container .results").show();
	ValueReportFormGeoLocation_selected = false;
	ValueReportFormGeoLocation_results = [];
	$("#houseValues-form #ValueReportFormGeoLocation_container .results").html("No Results");
	var string = $.trim($(this).val());
	if(string && (string.length > 6) ){
		$("#houseValues-form #ValueReportFormGeoLocation_container .results").html("Searching....");
		$.get("http://maps.googleapis.com/maps/api/geocode/json?components=country:US",
			{
				"format": "json",
				"address": string,
				"sensor" : true,
			},
			function(result){
				if(result.status && (result.status == "OK") ){
					$("#houseValues-form #ValueReportFormGeoLocation_container .results").html(" ");
					var country;
					var result_count = 0;
					$.each( result.results, function( index, element ){
						if(result_count == 0){
							$("#houseValues-form #ValueReportFormGeoLocation_container .results").html("No Results");
						}
						if(element.formatted_address){
							country = "";
							$.each(element.address_components,function( key, section){
								if(section.types[0] == "country"){
									country = section.short_name;
								}
							});

							if(result_count == 0){ $("#houseValues-form #ValueReportFormGeoLocation_container .results").html("<ul></ul>"); }
							result_count++;
							$("#houseValues-form #ValueReportFormGeoLocation_container .results ul").append("<li onclick='stmSetLocation("+index+")'><a>"+element.formatted_address+"</a></li>");
							ValueReportFormGeoLocation_results[index] = element;



						}
					});
				}

			}
		);
	}
});
$( "#houseValues-form #ValueReportFormGeoLocation" ).on( "keydown", function( event ) {
	if(event.which == 40 || event.which == 38 || event.which == 13){
		event.preventDefault();
		if(event.which == 13 && !ValueReportFormGeoLocation_selected ){
			return $( "#houseValues-form #ValueReportFormGeoLocation_container .results ul li.selected" ).click();
		} else if (ValueReportFormGeoLocation_selected && event.which == 13 ) {
			$( this ).closest("form").submit();
		}
		if($( "#houseValues-form #ValueReportFormGeoLocation_container .results ul" ).html()){
			if(! $( "#ValueReportFormGeoLocation_container .results .selected" ).html() ){
				$( "#ValueReportFormGeoLocation_container .results ul li" ).first().addClass( "selected" );
			} else {
				var found = false;
				var used = false;
				var elements;
				if(event.which == 38){
					elements = $("#ValueReportFormGeoLocation_container .results ul li").get().reverse();
				} else {
					elements = $( "#ValueReportFormGeoLocation_container .results ul li" );
				}
				$.each( elements , function( index ) {
					if(found && !used){
						$( this ).addClass("selected");
						used = true;
						return;
					}
					if (!found && !used && $( this ).hasClass("selected")){
						$( this ).removeClass("selected");
						found = true;
					}
				});
			}
		}

	}

});

JS;
Yii::app()->clientScript->registerScript("FooterValueReportFormGeoLocation_js", $js,CClientScript::POS_END);
$css = <<<CSS
	#ValueReportFormGeoLocation_container{
		position: relative;
	}
	#ValueReportFormGeoLocation_container .results{
		position: absolute;
		top: 52px;
		left:0;
		z-index:100;
		background-color: #fff;
		padding:8px;
		display:none;
	}
	#ValueReportFormGeoLocation_container .results li a{
		z-index:999;
	}
	#ValueReportFormGeoLocation_container .results li{
		padding:8px;
	}
	#ValueReportFormGeoLocation_container .results li.selected,
	#ValueReportFormGeoLocation_container .results li:hover{
		background:#eee;

	}
	#houseValues-form #FormSubmissionValues_data_14,
	#houseValues-form #FormSubmissionValues_data_16,
	#houseValues-form #FormSubmissionValues_data_17,
	#houseValues-form #FormSubmissionValues_data_18{
		display:none;
	}
CSS;

Yii::app()->getClientScript()->registerCss('FooterValueReportFormGeoLocation_css',$css);
?>