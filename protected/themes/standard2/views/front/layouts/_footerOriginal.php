<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <h3> Search by Price </h3>
    <ul>
        <li><a href="/homes/price_min/100000/price_max/200000"> $100,000's - $200,000's </a></li>
        <li><a href="/homes/price_min/200000/price_max/300000"> $200,000's - $300,000's </a></li>
        <li><a href="/homes/price_min/300000/price_max/400000"> $300,000's - $400,000's </a></li>
        <li><a href="/homes/price_min/400000/price_max/500000"> $400,000's - $500,000's </a></li>
        <li><a href="/homes/price_min/500000/price_max/600000"> $500,000's - $600,000's </a></li>
    </ul>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
    <h3> Luxury Homes </h3>
	<ul>
        <li><a href="/homes/price_min/500000/price_max/700000"> $600,000's - $700,000's </a></li>
        <li><a href="/homes/price_min/700000/price_max/900000"> $700,000's - $900,000's </a></li>
        <li><a href="/homes/price_min/1000000/price_max/2000000"> $1 - $2 Million </a></li>
        <li><a href="/homes/price_min/2000000/price_max/3000000"> $2 - $3 Million </a></li>
        <li><a href="/homes/price_min/4000000/price_max/6000000"> $4 - $6 Million </a></li>
	</ul>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
	<h3> Property Search </h3>
	<ul>
		<li><a href="/homes"> All Properties for Sale </a></li>
		<li><a href="/homes/property_type/1"> Single Family Homes </a></li>
		<li><a href="/homes/property_type/2"> Condos </a></li>
		<li><a href="/homes/property_type/3"> Vacant Land </a></li>
	</ul>
</div>

<div style="clear:both" class="hide visible-xs"></div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
	<h3> Resources </h3>
	<ul>
		<li><a href="/values"> FREE House Values Report </a></li>
        <li><a href="/myAccount"> My Account </a></li>
        <li><a href="/login"> Login </a></li>
	</ul>
</div>