
<ul class="nav navbar-nav pull-right">
    <li><a href="/homes"> SEARCH </a></li>

    <?/*<!-- change width of megamenu = use class > megamenu-fullwidth, megamenu-60width, megamenu-40width -->*/?>
    <li class="dropdown megamenu-fullwidth">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#"> BUY <b class="caret"> </b></a>
        <ul class="dropdown-menu">
            <li class="megamenu-content">

                <!-- megamenu-content -->
                <?php $this->renderPartial('/layouts/_menuBuy'); ?>
                <?php /* images
                <ul class="col-lg-3  col-sm-3 col-md-3 col-xs-6">
                    <li class="no-margin productPopItem "><a href="product-details.html"> <img
                                class="img-responsive" src="/images/tempImages/images/site/g4.jpg" alt="img"> </a> <a
                            class="text-center productInfo alpha90" href="product-details.html"> Eodem modo
                            typi <br>
                            <span> $60 </span> </a></li>
                </ul>
                <ul class="col-lg-3  col-sm-3 col-md-3 col-xs-6">
                    <li class="no-margin productPopItem relative"><a href="product-details.html"> <img
                                class="img-responsive" src="/images/tempImages/images/site/g5.jpg" alt="img"> </a> <a
                            class="text-center productInfo alpha90" href="product-details.html"> Eodem modo
                            typi <br>
                            <span> $60 </span> </a></li>
                </ul>*/ ?>
            </li>
        </ul>
    </li>

    <li class="dropdown megamenu-40width">
        <a data-toggle="dropdown" class="dropdown-toggle" href="/values"> SELL <b class="caret"> </b> </a>
        <ul class="dropdown-menu">
            <li class="megamenu-content ">
                <?php $this->renderPartial('/layouts/_menuSell'); ?>

                <?php /*
                 * Images<ul class="col-lg-3  col-sm-3 col-md-3  col-xs-4">
                    <li><a class="newProductMenuBlock" href="product-details.html"> <img
                                class="img-responsive" src="/images/tempImages/images/site/promo1.jpg" alt="product"> <span
                                class="ProductMenuCaption"> <i class="fa fa-caret-right"> </i> JEANS </span>
                        </a></li>
                </ul>
                <ul class="col-lg-3  col-sm-3 col-md-3 col-xs-4">
                    <li><a class="newProductMenuBlock" href="product-details.html"> <img
                                class="img-responsive" src="/images/tempImages/images/site/promo2.jpg" alt="product"> <span
                                class="ProductMenuCaption"> <i
                                    class="fa fa-caret-right"> </i> PARTY DRESS </span> </a></li>
                </ul>
                <ul class="col-lg-3  col-sm-3 col-md-3 col-xs-4">
                    <li><a class="newProductMenuBlock" href="product-details.html"> <img
                                class="img-responsive" src="/images/tempImages/images/site/promo3.jpg" alt="product"> <span
                                class="ProductMenuCaption"> <i class="fa fa-caret-right"> </i> SHOES </span>
                        </a></li>
                </ul>*/ ?>
            </li>
        </ul>
    </li>

    <li class="dropdown megamenu-fullwidth">
        <a data-toggle="dropdown" class="dropdown-toggle" href="/homes"> NEIGHBORHOODS <b class="caret"> </b></a>
        <ul class="dropdown-menu">
            <li class="megamenu-content">
                <!-- megamenu-content -->
                <?php $this->renderPartial('/layouts/_menuNeighborhoods'); ?>
            </li>
        </ul>
    </li>

    <li><a href="/values"> HOME VALUES </a></li>
    <? /*
    <li class="dropdown megamenu-fullwidth">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#"> MORTGAGES</a>
    </li> */ ?>

</ul>