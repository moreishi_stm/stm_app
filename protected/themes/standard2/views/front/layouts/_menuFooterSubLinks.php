<?php if (isset($links) && !empty($links)): ?>
	<?php foreach ($links as $child): ?>
		<li><a href="<?=$child['url']; ?>"><?=$child['name'];?></a></li>
		<?php if(isset($menuLinkItems[$child['id']]) && !empty($menuLinkItems[$child['id']])): ?>
			<?php $this->renderPartial('/layouts/_menuFooterSubLinks', array(
				'links' => $menuLinkItems[$child['id']],
				'menuLinkItems' => $this->menus[$this->menu_positions['bottom_nav']])
			); ?>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif;
