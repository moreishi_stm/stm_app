<?php
if($setting = SettingAccountValues::model()->findByAttributes(['setting_id'=> Settings::SETTING_TRACKING_SCRIPTS, 'account_id'=>Yii::app()->user->accountId])) {
    $trackingScripts = $setting->value;
};

if(!YII_DEBUG && $trackingScripts):
    echo $trackingScripts;
endif;

// check to see if the google code setting exists
if($setting = SettingAccountValues::model()->findByAttributes(['setting_id'=> Settings::GOOGLE_ANALYTICS_CODE_ID, 'account_id'=>Yii::app()->user->accountId])) {
    $googleCode = $setting->value;
};

if(!YII_DEBUG && $googleCode):

    Yii::app()->clientScript->registerScript('googleAnalytic', <<<JS
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '$googleCode', 'auto');
    ga('send', 'pageview');
JS
, CClientScript::POS_END);

endif;
