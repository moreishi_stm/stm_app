<?php
$script = <<<CSS
    .my-account {
        padding-right: 0;
    }
    .nav .circle{
        width: 24px;
		height: 24px;
		display: inline-block;
		border-radius: 50%;
		font-size: 12px;
		color: #fff;
		text-align: center;
		background: #666;
		position: relative;
        top: -4px;
        margin-left: 3px;
	}
    .nav .circle span{
        position: relative;
    }
	.no-padding .navbar-collapse{
	    /*margin:10px;*/
	}
	.megamenu-content .promo-1{
	    color:#fff !important;
    }
	#login-widget{
		position: relative;
	}
	#login-widget .user-dropdown{
	    display: none;
		position: absolute;
		top:20px;
		right:0;
		z-index:899;
		min-width: 180px;
		background: #fff;
		border: 1px solid #ddd;
		/*border-radius: 4px;*/
		-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
		box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	}
	#login-widget:hover .user-dropdown{
	    display: block;
	}
	#login-widget .user-dropdown li{
		text-align: left;
		font-size: 14px;
		margin:8px;
	}
	#login-widget .user-dropdown li a{
		color:#666;
	}
CSS;
Yii::app()->clientScript->registerCss('login-user-nav-css',$script);
$SavedHomesCount = 0;
if(!Yii::app()->user->isGuest && Yii::app()->user->id):

    $criteria = new \CDbCriteria();
    $criteria->order = "added DESC";
    $criteria->condition = "contact_id = :contactID AND is_deleted=0";
    $criteria->params = array(':contactID' => Yii::app()->user->id);
    $SavedHomesCount = SavedHomes::model()->count($criteria);
endif;
?>

		<!-- Fixed navbar start -->
		<div class="navbar navbar-tshop navbar-fixed-top megamenu" role="navigation">
			<div class="navbar-top dark">
				<div class="">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
                            <div class="pull-left">
                                <div class="userMenu">
                                    <ul>
                                        <li class="phone-number">
                                            <a href="callto:+<?=str_replace('/[^0-9]/','',Yii::app()->getUser()->getSettings()->office_phone);?>">
                                                <span> <i class="glyphicon glyphicon-earphone"></i></span>
                                                <span class="" style="margin-left:5px"> <?=Yii::app()->getUser()->getSettings()->office_phone;?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
							<!--- this part will be hidden for mobile version -->
							<div class="nav navbar-nav navbar-left hidden-xs">
                                <? if(strpos($_SERVER['SERVER_NAME'], 'myrealtydfw') !== false && strpos($_SERVER['SERVER_NAME'], 'themcelroyteam') !== false):?>
								<div class="search-box static-search pull-left" id="typeahead-container">

									<?php
									$propertyTypeIds = MlsPropertyTypeBoardLu::getDefaultPropertyTypeIds(Yii::app()->user->board->id);

									$addHtml = "";
									if(!empty($propertyTypeIds)) {
										$addHtml = "/property_types/".implode(",",$propertyTypeIds)."/";
									}
									?>
									<form id="search-form" method="GET" action="/homes<?=$addHtml;?>" role="search" class="navbar-form">
										<div class="input-group">
											<i class="fa fa-2x fa-crosshairs nearMe" style="
											position: absolute;
											top: 50%;
											left: 0;
											transform: translate(0, -50%);
											color: #888888;
											z-index: 10;
											margin-left: 5px;
											cursor: pointer;" stmtemplate-title="Search Near Me" data-input="#main_q"></i>
											<?php /*navigator.geolocation.getCurrentPosition(showPosition)
											 *
											 * function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude +
    "<br>Longitude: " + position.coords.longitude;
}
											 *  */ ?>
											<input type="text" style="padding:6px 6px 6px 30px;" name="keywords" id="main_q" placeholder="Search an Area, Address, City, Zip, County, Schools, etc . . ."
												class="form-control"<?php if(isset($this->mainSearchValue) && !empty($this->mainSearchValue)): ?> value="<?=$this->mainSearchValue;?>"<?php endif; ?>>

											<div class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
											</div>
										</div>
									</form>
									<!-- /input-group -->

								</div>
								<!--/.search-box -->
                                <? endif;?>
							</div>
							<!--/.navbar-nav hidden-xs-->

						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <? if(Yii::app()->user->client->type !== 'Brokerage' /*(strpos($_SERVER['SERVER_NAME'], 'kwadvantage2') === false && strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') === false && strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') === false && strpos($_SERVER['SERVER_NAME'], 'kwclassicrealty') === false && strpos($_SERVER['SERVER_NAME'], 'kwrealtyboise') === false && strpos($_SERVER['SERVER_NAME'], 'kwsarasotasiestakey') === false && strpos($_SERVER['SERVER_NAME'], 'growwithkwrc') === false && strpos($_SERVER['SERVER_NAME'], 'demotl.') === false)*/):?>
                                <div class="pull-right">
                                    <ul class="userMenu">
                                        <?php $this->widget('front_module.components.widgets.LoginWidget.LoginWidget'); ?>
                                </div>
                            <? endif;?>
						</div>
					</div>
				</div>
			</div>
			<!--/.navbar-top-->

			<?php /* <div class="navbar-top" style="height: auto;">
			  <div class="container">
			  <div class="row">
			  <div class="col-lg-8 col-sm-6 col-xs-6 col-md-6">
			  <div class="pull-left" style="margin: 10px 0 10px 0;">
			  <a href="/" stmtemplate-title="<?php echo ($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?>"> <img src="images/logo.png" alt="<?php echo ($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?>" style="height: 37px;" class="img-responsive"> </a>
			  </div>

			  <div class="nav navbar-nav navbar-left hidden-xs">

			  <div class="search-box static-search pull-left">

			  <form id="search-form" method="GET" action="#" role="search" class="navbar-form">
			  <div class="input-group">
			  <input type="text" style="padding:6px 6px; width: 400px;" name="q" placeholder="Search an Area, Address, City, Zip, County or Schools" class="form-control">

			  <div class="input-group-btn">
			  <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
			  </button>
			  </div>
			  </div>
			  </form>
			  <!-- /input-group -->

			  </div>
			  <!--/.search-box -->
			  </div>
			  <!--/.navbar-nav hidden-xs-->
			  </div>
			  <div class="col-lg-3 col-sm-6 col-xs-6 col-md-6 no-margin no-padding">
			  <div class="pull-right">
			  <ul class="userMenu">
			  <li class="phone-number"> <a href="callto:+9042803000"  style="line-height: 50px;"><span><i class="glyphicon glyphicon-phone-alt "></i></span><span class="hidden-xs" style="margin-left:5px">(904) 280-3000</span></a></li>
			  <li class="login-register"><a href="#" data-toggle="modal" data-target="#ModalLogin" style="line-height: 50px;"><span class="hidden-xs">Login / Register</span> </a></li>
			  </ul>
			  </div>
			  </div>
			  </div>
			  </div>
			  </div> */ ?>


			<div class="">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only"> Toggle navigation </span>
                        <span class="icon-bar"> </span>
                        <span class="icon-bar"> </span>
                        <span class="icon-bar"> </span>
                        <span class="menu"> </span>
					<?php /*<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-cart"><i
							class="fa fa-shopping-cart colorWhite"> </i> <span class="cartRespons colorWhite"> My Homes (10) </span>*/ ?>
					</button>
                    <a class="navbar-brand" href="/"> <img src="/images/logo.png"> </a>
				</div>

                <div class="navbar-collapse collapse">
                    <?php if (!isset($this->useDBMenu[$this->menu_positions['top_nav']]) || !$this->useDBMenu[$this->menu_positions['top_nav']]): ?>
                        <?php $this->renderPartial('/layouts/_topNavOriginal'); ?>
                    <?php else: ?>
                        <ul class="nav navbar-nav">
<!--                            <li class="home"><a href="/"><i class="glyphicon glyphicon-home"> </i></a></li>-->

                            <?php foreach($this->menus[$this->menu_positions['top_nav']][0] as $link): ?>
                                <?php if(isset($this->menus[$this->menu_positions['top_nav']][$link['id']])  && !empty($this->menus[$this->menu_positions['top_nav']][$link['id']])):

                                    // get the number of columns and adjust widget according to it
                                    switch(count($this->menus[$this->menu_positions['top_nav']][$link['id']])) {
                                        case 1:
                                            $menuWidth = '33width';
                                            break;
                                        case 2:
                                            $menuWidth = '80width';
                                            break;
                                        // 3 or more columns get full width
                                        default:
                                            $menuWidth = 'fullwidth';
                                            break;

                                    }
                                    ?>
                                    <li class="dropdown megamenu-<?=$menuWidth?>">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?=$link['name']; ?> <i class="caret"> </i></a>
                                        <?php $this->renderPartial('/layouts/_menuSubLinks', array(
                                                'links' => $this->menus[$this->menu_positions['top_nav']][$link['id']],
                                                'menuLinkItems' => $this->menus[$this->menu_positions['top_nav']])
                                        ); ?>
                                    </li>
                                <?php else: ?>
                                    <li><a href="<?=((strpos($link['url'], 'http://') === false && substr($link['url'], 0, 1) !== '/') ? "/" : "") . $link['url']; ?>"><?=$link['name']; ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
<!--                            <li class="my-account"><a href="/myAccount">Favorites<i class="circle"><span class="saved-count">--><?php //echo $SavedHomesCount; ?><!--</span></i></a></li>-->
                        </ul>
                    <?php endif; ?>
                    <?php
                    ?>
                    <?/*<div class="nav navbar-nav navbar-right">
                        <div class="dropdown cartMenu" >
                            <a href="/myAccount" class="dropdown-toggle">
                                <span class="cartRespons">My Favorites</span>
                                <i class="circle">
                                    <span><?php echo ($total['totalSaved']) ? $total['totalSaved'] : 0; ?></span>
                                </i>
                            </a>
                        </div>
                    </div>
                    */?>
                </div>
                <!--/.nav-collapse -->



                <?/*
				<!-- this part for mobile -->
				<div class="search-box static-search hidden-lg hidden-md hidden-sm navbar-formbox navbar-right">
					<div style="clear:both"></div>
					<form id="search-form" method="GET" action="#" role="search" class="navbar-form">
						<div class="input-group">
							<input type="text" style="padding:6px 6px;" name="q" placeholder="Search..."
								   class="form-control">

							<div class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
								</button>
							</div>
						</div>
					</form>

					<div class="input-group hide">
						<button class="btn btn-nobg" type="button"><i class="fa fa-search"> </i></button>
					</div>
					<!-- /input-group -->

				</div>
				<!--/.mobile search input -->
            */?>
			</div>
			<!--/.container -->

			<!-- /w100 -->

			<div class="menuWrap">
				<div class="container">
					<!-- this part is duplicate from cartMenu  keep it for mobile -->
					<?php /*<div class="navbar-cart  collapse">
						<div class="cartMenu  col-lg-4 col-xs-12 col-md-4 ">

							<div class="w100 miniCartTable scroll-pane">
								<table>
									<tbody>
										<tr class="miniCartProduct">
											<td style="20%" class="miniCartProductThumb">
												<div><a href="product-details.html"> <img src="/images/tempImages/images/product/3.jpg" alt="img"> </a>
												</div>
											</td>
											<td style="40%">
												<div class="miniCartDescription">
													<h4><a href="product-details.html"> TSHOP T shirt Black </a></h4>
													<span class="size"> 12 x 1.5 L </span>

													<div class="price"><span> $8.80 </span></div>
												</div>
											</td>
											<td style="10%" class="miniCartQuantity"><a> X 1 </a></td>
											<td style="15%" class="miniCartSubtotal"><span> $8.80 </span></td>
											<td style="5%" class="delete"><a> x </a></td>
										</tr>
										<tr class="miniCartProduct">
											<td style="20%" class="miniCartProductThumb">
												<div><a href="product-details.html"> <img src="/images/tempImages/images/product/2.jpg" alt="img"> </a>
												</div>
											</td>
											<td style="40%">
												<div class="miniCartDescription">
													<h4><a href="product-details.html"> TSHOP T shirt Black </a></h4>
													<span class="size"> 12 x 1.5 L </span>

													<div class="price"><span> $8.80 </span></div>
												</div>
											</td>
											<td style="10%" class="miniCartQuantity"><a> X 1 </a></td>
											<td style="15%" class="miniCartSubtotal"><span> $8.80 </span></td>
											<td style="5%" class="delete"><a> x </a></td>
										</tr>
										<tr class="miniCartProduct">
											<td style="20%" class="miniCartProductThumb">
												<div><a href="product-details.html"> <img src="/images/tempImages/images/product/5.jpg" alt="img"> </a>
												</div>
											</td>
											<td style="40%">
												<div class="miniCartDescription">
													<h4><a href="product-details.html"> TSHOP T shirt Black </a></h4>
													<span class="size"> 12 x 1.5 L </span>

													<div class="price"><span> $8.80 </span></div>
												</div>
											</td>
											<td style="10%" class="miniCartQuantity"><a> X 1 </a></td>
											<td style="15%" class="miniCartSubtotal"><span> $8.80 </span></td>
											<td style="5%" class="delete"><a> x </a></td>
										</tr>
										<tr class="miniCartProduct">
											<td style="20%" class="miniCartProductThumb">
												<div><a href="product-details.html"> <img src="/images/tempImages/images/product/3.jpg" alt="img"> </a>
												</div>
											</td>
											<td style="40%">
												<div class="miniCartDescription">
													<h4><a href="product-details.html"> TSHOP T shirt Black </a></h4>
													<span class="size"> 12 x 1.5 L </span>

													<div class="price"><span> $8.80 </span></div>
												</div>
											</td>
											<td style="10%" class="miniCartQuantity"><a> X 1 </a></td>
											<td style="15%" class="miniCartSubtotal"><span> $8.80 </span></td>
											<td style="5%" class="delete"><a> x </a></td>
										</tr>
										<tr class="miniCartProduct">
											<td style="20%" class="miniCartProductThumb">
												<div><a href="product-details.html"> <img src="/images/tempImages/images/product/3.jpg" alt="img"> </a>
												</div>
											</td>
											<td style="40%">
												<div class="miniCartDescription">
													<h4><a href="product-details.html"> TSHOP T shirt Black </a></h4>
													<span class="size"> 12 x 1.5 L </span>

													<div class="price"><span> $8.80 </span></div>
												</div>
											</td>
											<td style="10%" class="miniCartQuantity"><a> X 1 </a></td>
											<td style="15%" class="miniCartSubtotal"><span> $8.80 </span></td>
											<td style="5%" class="delete"><a> x </a></td>
										</tr>
										<tr class="miniCartProduct">
											<td style="20%" class="miniCartProductThumb">
												<div><a href="product-details.html"> <img src="/images/tempImages/images/product/4.jpg" alt="img"> </a>
												</div>
											</td>
											<td style="40%">
												<div class="miniCartDescription">
													<h4><a href="product-details.html"> TSHOP T shirt Black </a></h4>
													<span class="size"> 12 x 1.5 L </span>

													<div class="price"><span> $8.80 </span></div>
												</div>
											</td>
											<td style="10%" class="miniCartQuantity"><a> X 1 </a></td>
											<td style="15%" class="miniCartSubtotal"><span> $8.80 </span></td>
											<td style="5%" class="delete"><a> x </a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--/.miniCartTable-->

							<div class="miniCartFooter  miniCartFooterInMobile text-right">
								<h3 class="text-right subtotal"> Subtotal: $210 </h3>
								<a class="btn btn-sm btn-danger">
									<i class="fa fa-shopping-cart"> </i> VIEW CART </a> <a class="btn btn-sm btn-primary">
									CHECKOUT </a>
							</div>
							<!--/.miniCartFooter-->

						</div>
						<!--/.cartMenu-->
					</div>*/ ?>
					<!--/.navbar-cart-->
				</div>
				<!--/.container -->

				<div class="search-full text-right">
					<a class="pull-right search-close"> <i class=" fa fa-times-circle"> </i> </a>

					<div class="searchInputBox pull-right">
						<input type="search" data-searchurl="search?=" name="q" placeholder="start typing and hit enter to search" class="search-input">
						<button class="btn-nobg search-btn" type="submit"><i class="fa fa-search"> </i></button>
					</div>
				</div>
				<!--/.search-full ||  search form here-->
			</div>
			<!--/.w100-full-->

		</div>
		<!-- /.Fixed navbar  -->