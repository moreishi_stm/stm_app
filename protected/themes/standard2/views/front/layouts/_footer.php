<?php if(!isset($this->useDBMenu[$this->menu_positions['bottom_nav']]) || !$this->useDBMenu[$this->menu_positions['bottom_nav']]): ?>
	<?php $this->renderPartial('/layouts/_footerOriginal'); ?>
<?php else:
	// count footer links
    //$this->menus[$this->menu_positions['bottom_nav']]; //@todo: this has both attributes and array of links - is this best structure, hard to get count of links, going to assume 5 based on theme for now.
	$count = 5; //count($links); //$links is not passed in, hard coding 5 for now.

	$bootstrapNumLg = 2;
	$bootstrapNumMd = 2;
	$bootstrapNumSm = 6;

	switch($count) {
		case 2:
			$bootstrapNumLg = $bootstrapNumMd = $bootstrapNumSm = 6;
		break;
		case 3:
			$bootstrapNumLg = $bootstrapNumMd = 4;
		break;
		case 4:
		case 5:
			//$bootstrapNumLg = $bootstrapNumMd = 3;
		break;
		case 6:
			//$bootstrapNumLg = $bootstrapNumMd = 2;
		break;
		default:
			$bootstrapNumLg = 2;
		break;
	}
	?>
	<?php foreach($this->menus[$this->menu_positions['bottom_nav']][0] as $link): ?>
	<div class="col-lg-<?=$bootstrapNumLg; ?> col-md-<?=$bootstrapNumMd; ?> col-sm-<?=$bootstrapNumSm;?> col-xs-12">
		<?php if(isset($this->menus[$this->menu_positions['bottom_nav']][$link['id']])  && !empty($this->menus[$this->menu_positions['bottom_nav']][$link['id']])): ?>
			<h3><?=$link['name'];?></h3>
			<ul>
				<?php $this->renderPartial('/layouts/_menuFooterSubLinks', array(
					'links' => $this->menus[$this->menu_positions['bottom_nav']][$link['id']],
					'menuLinkItems' => $this->menus[$this->menu_positions['bottom_nav']])
				); ?>
			</ul>
		<?php else: ?>
				<h3><?=$link['name'];?></h3>
		<?php endif; ?>
	</div>
	<?php endforeach; ?>
<?php endif;?>