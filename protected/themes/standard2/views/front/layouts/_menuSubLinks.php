<?php if (isset($links) && !empty($links)):
	$count = count($links);

	$bootstrapNumLg = 12;
	$bootstrapNumMd = 12;
	$bootstrapNumSm = 12;

	switch($count) {
		case 2:
			$bootstrapNumLg = $bootstrapNumMd = $bootstrapNumSm = 6;
		break;
		case 3:
			$bootstrapNumLg = $bootstrapNumMd = 4;
		break;
		case 4:
		case 5:
			$bootstrapNumLg = $bootstrapNumMd = 3;
		break;
		case 6:
			$bootstrapNumLg = $bootstrapNumMd = 2;
		break;
		default:
			$bootstrapNumLg = 12;
		break;
	} ?>
<ul class="dropdown-menu">
	<li class="megamenu-content">
	<?php foreach ($links as $child): ?>
	<ul class="col-lg-<?=$bootstrapNumLg;?> col-md-<?=$bootstrapNumMd;?> col-sm-<?=$bootstrapNumSm;?> col-xs-12 unstyled noMarginLeft">
		<li class="no-border"><p class="promo-1"><strong><?=$child['name']; ?></strong></p></li>
		<?php if(isset($menuLinkItems[$child['id']]) && !empty($menuLinkItems[$child['id']])): ?>
			<?php foreach ($menuLinkItems[$child['id']] as $sub_child): ?>
				<li><a href="<?=((strpos($sub_child['url'], 'http://') === false && substr($sub_child['url'], 0, 1) !== '/') ? "/" : "") . $sub_child['url']; ?>"><?=$sub_child['name'];?></a></li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>
	<?php endforeach; ?>
	</li>
</ul>
<?php endif; ?>