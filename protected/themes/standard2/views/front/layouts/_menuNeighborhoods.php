<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-12 unstyled noMarginLeft">
	<li class="no-border"><p class="promo-1"><strong>Homes by Price</strong></p></li>
	<li><a href="/homes/price_min/100000/price_max/200000">$100,000 - $200,000</a></li>
    <li><a href="/homes/price_min/100000/price_max/200000">$200,000 - $300,000</a></li>
    <li><a href="/homes/price_min/100000/price_max/200000">$300,000 - $400,000</a></li>
    <li><a href="/homes/price_min/100000/price_max/200000">$400,000 - $500,000</a></li>
    <li><a href="/homes/price_min/100000/price_max/200000">$500,000 - $600,000</a></li>
</ul>
<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-12 unstyled noMarginLeft">
	<li class="no-border"><p class="promo-1"><strong>Luxury Homes</strong></p></li>
    <li><a href="/homes/price_min/700000/price_max/800000"> $700,000's - $800,000's </a></li>
    <li><a href="/homes/price_min/800000/price_max/900000"> $800,000's - $900,000's </a></li>
    <li><a href="/homes/price_min/1000000/price_max/2000000"> $1 - $2 Million </a></li>
    <li><a href="/homes/price_min/2000000/price_max/3000000"> $2 - $3 Million </a></li>
    <li><a href="/homes/price_min/4000000/price_max/5000000"> Over $4 Million </a></li>
</ul>
<ul class="col-lg-4 col-md-4 col-sm-4 col-xs-12 unstyled noMarginLeft">
	<li class="no-border"><p class="promo-1"><strong>Property Type Search</strong></p></li>
    <li><a href="/homes"> All Properties for Sale </a></li>
    <li><a href="/homes/property_type/1"> Single Family Homes </a></li>
    <li><a href="/homes/property_type/2"> Condos </a></li>
    <li><a href="/homes/property_type/3"> Vacant Land </a></li>
</ul>