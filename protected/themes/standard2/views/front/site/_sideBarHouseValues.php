<?php
Yii::app()->clientScript->registerCss('houseValuesSideBarWidgetCss', <<<CSS
    #house-values-widget-container {
        border: 1px solid #CCC;
        position: relative;
        padding: 20px 20px 25px 20px;
        overflow-x: hidden;
        background-color: #fafafa;
        margin-top: 20px;
        margin-bottom: 25px;
        float:left;
        width: 100%;
    }
    #house-values-widget-container .title{
        padding-top: 10px;
        font-family: 'Arial';
    }
    #house-values-widget-container .date{
        font-size: 40px;
        font-family: 'Arial Black';
        line-height: 1.2;
        text-align: right;
        margin-top: -2px;
        margin-bottom: 10px;
    }
    #house-values-widget-container .year{
        font-size: 50px;
    }
    #house-values-widget-container img{
        position: absolute;
        left: 7px;
        top: 2px;
        width: 128px;
    }
    #house-values-widget-container h2{
        font-size: 23px;
        line-height: 1;
        color: black;
        text-align: right;
    }
    @media (min-width: 1300px) {
        #house-values-widget-container .date{
        font-size: 40px;
        }
        #house-values-widget-container .year{
            font-size: 50px;
        }
    }
    @media (max-width: 1300px) {
        #house-values-widget-container .date{
            font-size: 30px;
        }
        #house-values-widget-container .year{
            font-size: 35px;
        }
        #house-values-widget-container img {
            width: 124px;
        }
    }
    @media (max-width: 1200px) {
        #house-values-widget-container .title{
            padding-top: 10px;
        }
        #house-values-widget-container .date{
            font-size: 30px;
        }
        #house-values-widget-container .year{
            font-size: 45px;
        }
        #house-values-widget-container img {
            width: 175px;
            left: 28px;
            top: 10px;
        }
        #house-values-widget-container h2{
            font-size: 17px;
        }
    }
    @media (max-width: 992px) {
        #house-values-widget-container{
        }
        #house-values-widget-container .title{
            padding-top: 20px;
        }
        #house-values-widget-container .date{
            font-size: 35;
            text-align: left;
        }
        #house-values-widget-container .year{
            font-size: 35px;
            text-align: left;
        }
        #house-values-widget-container img {
            width: 50%;
        }
        #house-values-widget-container h2{
            font-size: 16px;
            text-align: left;
        }
    }
    @media (max-width: 768px) {
        #house-values-widget-container{
            width: 70%;
            margin-left: 15%;
        }
    }
    @media (max-width: 500px) {
        #house-values-widget-container{
            /*width: 100%;*/
            /*margin-left: 0;*/
        }
    }

    #house-values-widget-container .row{
        margin: 10px 0 0 0;
    }
    #house-values-widget-container .row a{
        color: blue;
        display: block;
        font-weight: bold;
        padding: 4px 6px;
        margin-left: -4px;
    }
    #house-values-widget-container .row a:hover{
        background-color: yellow;
    }
CSS
);
?>
<div id="house-values-widget-container">
    <div class="title">
        <div class="date"><div class="year"><?php echo date('Y'); ?></div><?php echo date('F'); ?></div>
        <h2>House Values Report</h2>
    </div>
    <?/*<img src="http://cdn.seizethemarket.com/assets/images/house_values_2.png" border="0">*/?>
    <div class="row">
        Find out what your property is
        worth in today’s market.
        <a href="/values">FREE House Values Report</a>
    </div>
    <div class="row">
        The most local and accurate
        market information.
    </div>
    <div class="row">
        <a href="/values">FREE <?=date('F Y')?> Real Estate Market Report</a>
    </div>
</div>
<div class="clearfix"></div>