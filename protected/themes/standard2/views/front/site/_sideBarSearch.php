<?php
$actionName = Yii::app()->controller->action->id;
if($actionName == 'area') {
    $actionName .= '/'.$_GET['area'];
}
$selectedText = 'selected="selected"';
Yii::app()->clientScript->registerScript('searchSideBar', <<<JS

    // override theme js for transforming checkboxes
    $.fn.ionCheckRadio = function (method) {}

    $('#home-listview-search form').change(function(){
        $("body").prepend("<div class='loading-container loading'><em></em></div>");
        $(this).submit();
    });

    $('#home-listview-search form').on('submit', function(event) {
        event.preventDefault();
//
//        var fields = $( this ).serializeArray();
//
//        var url = "/$actionName";
//
//        var mainQ = $("#main_q").val();
//
//        if(!mainQ) {
//        } else {
//            url += "/keywords/"+mainQ ;
//        }
//
//        var propertyTypesFieldValues = "";
//
//        $.each(fields, function(i, field) {
//            if(field.value) {
//                // except for propertyTypes / multi-checkbox
//                if(field.name == 'FeaturedAreas[property_types]') {
//
//                    if(propertyTypesFieldValues == "") {
//                        var propertyTypesFields = $('input[name="FeaturedAreas[property_types]"]').serializeArray();
//
//                         $.each(propertyTypesFields, function(j, propertyTypeField) {
//
//                            if(propertyTypeField.value) {
//                                propertyTypesFieldValues = (propertyTypesFieldValues != "") ? propertyTypesFieldValues + "," : "";
//                                propertyTypesFieldValues = propertyTypesFieldValues + propertyTypeField.value;
//                            }
//                         });
//
//                         if(propertyTypesFieldValues != "" && url.indexOf("property_types") == -1) {
//                             url += "/property_types/" + propertyTypesFieldValues;
//                         }
//                    }
//                }
//                else {
//                    var id = $('[name="'+field.name+'"]').attr('id');
//                    url += "/"+id+"/"+field.value;
//                }
//
//
//            }
//        });
//        window.location = url;
        window.location = "/$actionName" + buildSearchUrlVars($('#home-listview-search form'));
        return false;
    });

    function buildSearchUrlVars(form)
    {
        var fields = $( form ).serializeArray();

        var url = '';

        var mainQ = $("#main_q").val();

        if(!mainQ) {
        } else {
            url += "/keywords/"+mainQ ;
        }

        var propertyTypesFieldValues = "";

        $.each(fields, function(i, field) {
            if(field.value) {
                // except for propertyTypes / multi-checkbox
                if(field.name == 'FeaturedAreas[property_types]') {

                    if(propertyTypesFieldValues == "") {
                        var propertyTypesFields = $('input[name="FeaturedAreas[property_types]"]').serializeArray();

                         $.each(propertyTypesFields, function(j, propertyTypeField) {

                            if(propertyTypeField.value) {
                                propertyTypesFieldValues = (propertyTypesFieldValues != "") ? propertyTypesFieldValues + "," : "";
                                propertyTypesFieldValues = propertyTypesFieldValues + propertyTypeField.value;
                            }
                         });

                         if(propertyTypesFieldValues != "" && url.indexOf("property_types") == -1) {
                             url += "/property_types/" + propertyTypesFieldValues;
                         }
                    }
                }
                else {
                    var id = $('[name="'+field.name+'"]').attr('id');
                    url += "/"+id+"/"+field.value;
                }


            }
        });

        return url;
    }
JS
);

Yii::app()->clientScript->registerCss('searchSideBarCss', <<<CSS

    .btn.home-list-search-button {
        width: 100%;
    }
    .btn.home-list-search-button:hover {
        background-color: #25ad29;
    }
    #home-listview-search .form-group select:hover, #home-listview-search .form-group input:hover {
        background-color: #efefef;
        border: solid 2px #888;
    }
    #home-listview-search .form-control:focus {
        background-color: transparent;
        border: 1px solid #ccc;
    }
    @media (max-width: 768px) {
        #home-listview-search .panel-title {
            margin-top: 16px;
        }
    }
    input[type="checkbox"] {
        display: inline-block;
        height: 23px;
        width: 23px;
        overflow: hidden;
        margin-top: -4px;
        vertical-align: middle;
        outline: 0px none;
        background: #FFF none repeat scroll 0% 0%;
        border-radius: 2px;
        border: 1px solid #C8C8C8;
        box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.15) inset;
    }
    #collapseTypes label {
        font-weight: normal;
        position: relative;
        top: 5px;
        font-size: 14px;
    }

    .panel-default > .panel-heading {
        border-top: 0 !important;
    }
    .panel-body h5 {
        font-weight: bold;
        padding-bottom: 0;
        padding-top: 12px;
        font-size: 12px;
        text-transform: uppercase;
        color: #666;
        margin-bottom: -1px;
    }
    .activeFilter {
        background: #E6F2FF !important;
        border: 2px solid #337ABF !important;
    }
CSS
);

$form = $this->beginWidget('CActiveForm', array(
	'method' => 'get',
	'id' => 'home-list-search',
	'action' => $action
));

$priceList = StmFormHelper::getPriceList(); ?>

<div class="panel-group" id="accordionNo">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><a data-toggle="collapse" href="#collapseSearchCriteria" class="collapseWill pressed">Search Criteria <span class="pull-left"> <i class="fa fa-caret-right"></i></span></a>
                <a href="/<?=(Yii::app()->controller->action->id == 'homes') ? 'homes' : 'area/'.$_GET['area'];?>"><span class="pull-right clearFilter  label-danger"> Clear </span></a>
            </h4>
        </div>
        <div id="collapseSearchCriteria" style="clear:both;">
            <div class="panel-body ">
                <div class="form-group">
                    <h5>Property Type</h5>
                    <?php
                    $types = CHtml::listData(MlsPropertyTypeBoardLu::model()->displayOrder()->findAll(array('condition' =>'status_ma=1 AND mls_board_id='.Yii::app()->user->boardId)),'mls_property_type_id','displayName');

                    if(isset($_GET['property_types'])) {
                        $propertyTypeIds = explode(',', urldecode($_GET['property_types']));
                    }

                    if(!isset($_GET['property_types']) && empty($_GET['keywords'])) {
                        $propertyTypeIds = MlsPropertyTypeBoardLu::getDefaultPropertyTypeIds(Yii::app()->user->board->id);
                    }

                    foreach ($types as $value => $label):
                        $checked = "";
                        if (!empty($propertyTypeIds) && in_array($value, $propertyTypeIds)) {
                            $checked = 'checked="checked"';
                        } ?>
                        <div><input name="FeaturedAreas[property_types]" id="property_types_<?= $value ?>"
                                    value="<?= $value ?>" <?= $checked ?> type="checkbox"> <label
                                for="property_types_<?= $value ?>"><?= $label; ?></label></div>
                        <?php endforeach; ?>
                </div>
            </div>
            <div class="panel-body">
                <div role="form">
                    <div class="form-group">
                        <select class="form-control <?=($_GET['price_min']) ? "activeFilter" : "" ?>" name="FeaturedAreas[price_min]" id="price_min" data-placeholder="Price Min">
                            <option value="" <?php if(!isset($_GET['price_min']) || empty($_GET['price_min'])):?>selected="selected"<?php endif; ?>>Min Price</option>
                            <?php foreach($priceList as $value => $label):
                                $selected = "";
                                if(isset($_GET['price_min']) && intval($value) ==  intval($_GET['price_min'])) {
                                    $selected = 'selected="selected"';
                                }
                                ?>
                                <option value="<?=$value; ?>"<?=$selected;?>><?=$label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control <?=($_GET['price_max']) ? "activeFilter" : "" ?>" name="FeaturedAreas[price_max]" id="price_max">
                            <option value="" <?php if(!isset($_GET['price_max']) || empty($_GET['price_max'])):?>selected="selected"<?php endif; ?>>Max Price</option>
                            <?php foreach($priceList as $value => $label):
                                $selected = "";
                                if(isset($_GET['price_max']) && intval($value) ==  intval($_GET['price_max'])) {
                                    $selected = 'selected="selected"';
                                }
                                ?>
                                <option value="<?=$value; ?>"<?=$selected;?>><?=$label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control <?=($_GET['bedrooms']) ? "activeFilter" : "" ?>" name="FeaturedAreas[bedrooms]" id="bedrooms">
                            <option value="" <?php if(!isset($_GET['bedrooms']) || empty($_GET['bedrooms'])):?>selected="selected"<?php endif; ?>>Bedrooms</option>
                            <?php
                            $bedRooms = StmFormHelper::getBedroomList();
                            foreach($bedRooms as $value => $label):
                                $selected = "";
                                if(isset($_GET['bedrooms']) && intval($value) ==  intval($_GET['bedrooms'])) {
                                    $selected = 'selected="selected"';
                                }
                                ?>
                                <option value="<?=$value; ?>"<?=$selected;?>><?=$label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control <?=($_GET['baths_total']) ? "activeFilter" : "" ?>" name="FeaturedAreas[baths_total]" id="baths_total">
                            <option value="" <?php if(!isset($_GET['baths_total']) || empty($_GET['baths_total'])):?>selected="selected"<?php endif; ?>>Baths</option>
                            <?php
                            $baths = StmFormHelper::getBathList();
                            foreach($baths as $value => $label):
                                $selected = "";
                                if(isset($_GET['baths_total']) && intval($value) ==  intval($_GET['baths_total'])) {
                                    $selected = 'selected="selected"';
                                }?>
                                <option value="<?=$value; ?>"<?=$selected;?>><?=$label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control <?=($_GET['sq_feet']) ? "activeFilter" : "" ?>" name="FeaturedAreas[sq_feet]" id="sq_feet">
                            <option value="" <?php if(!isset($_GET['sq_feet']) || empty($_GET['sq_feet'])):?>selected="selected"<?php endif; ?>>Sq. Feet</option>
                            <?php
                            if(Yii::app()->user->board->is_sq_ft_range) {
                                $mlsProperties = new MlsProperties(Yii::app()->getUser()->getBoard()->getPrefixedName());
                                $sqFt = $mlsProperties->listSqFtRange();
                            }
                            else {
                                $sqFt = StmFormHelper::getSqFeetList(true, '+ Sq.Feet');
                            }
                            foreach($sqFt as $value => $label):
                                $selected = "";
                                if(isset($_GET['sq_feet']) && intval($value) ==  intval($_GET['sq_feet'])) {
                                    $selected = 'selected="selected"';
                                }?>
                                <option value="<?=$value; ?>"<?=$selected;?>><?=$label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <? $yearBuilt = (isset($_GET['year_built']))? $_GET['year_built'] : null; ?>
                        <select class="form-control <?=($_GET['year_built']) ? "activeFilter" : "" ?>" name="FeaturedAreas[year_built]" id="year_built">
                            <option value="" <?php if(!isset($_GET['year_built']) || empty($_GET['year_built'])):?>selected="selected"<?php endif; ?>>Year Built</option>
                            <?php
                            $thisYear = date('Y');
                            for($i=$thisYear; $i>($thisYear-50); $i--){ // $sqFt as $value => $label
                                $selected = "";
                                if(isset($_GET['year_built']) && intval($i) ==  intval($_GET['year_built'])) {
                                    $selected = 'selected="selected"';
                                }?>
                                <option value="<?=$i; ?>"<?=$selected;?>><?=$i; ?>+ Year Built</option>
                            <?php } ?>
                        </select>
                    </div>

                    <?if(in_array('car_garage', $searchableFields)):?>
                        <div class="form-group">
                            <select class="form-control <?=($_GET['car_garage']) ? "activeFilter" : "" ?>" name="FeaturedAreas[car_garage]" id="car_garage">
                                <option value="" <?php if(!isset($_GET['car_garage']) || (empty($_GET['car_garage']) && $_GET['car_garage'] != '0')):?>selected="selected"<?php endif; ?>># Car Garage</option>
                                <option value="1" <?=($_GET['car_garage'] == '1') ? 'selected' : ''?>>1+ Car Garage</option>
                                <option value="2" <?=($_GET['car_garage'] == '2') ? 'selected' : ''?>>2+ Car Garage</option>
                                <option value="3" <?=($_GET['car_garage'] == '3') ? 'selected' : ''?>>3+ Car Garage</option>
                                <option value="4" <?=($_GET['car_garage'] == '4') ? 'selected' : ''?>>4+ Car Garage</option>
                                <option value="5" <?=($_GET['car_garage'] == '5') ? 'selected' : ''?>>5+ Car Garage</option>
                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('pool_yn', $searchableFields)):?>
                        <div class="form-group">
                            <select class="form-control <?=($_GET['pool'] || $_GET['pool'] == 0) ? "activeFilter" : "" ?>" name="FeaturedAreas[pool]" id="pool">
                                <option value="" <?php if(!isset($_GET['pool']) || (empty($_GET['pool']) && $_GET['pool'] != '0')):?>selected="selected"<?php endif; ?>>Pool</option>
                                <option value="1" <?=($_GET['pool'] == '1') ? 'selected' : ''?>>Pool - Yes</option>
                                <option value="0" <?=($_GET['pool'] == '0') ? 'selected' : ''?>>Pool - No</option>
                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('stories', $searchableFields)):?>
                        <div class="form-group">
                            <? $stories = (isset($_GET['stories']))? $_GET['stories'] : null; ?>
                            <select class="form-control <?=($_GET['stories']) ? "activeFilter" : "" ?>" name="FeaturedAreas[stories]" id="stories">
                                <option value="" <?php if(!isset($_GET['stories']) || (empty($_GET['stories']) && $_GET['stories'] != '0')):?>selected="selected"<?php endif; ?>># Stories</option>
                                <option value="1" <?=($_GET['stories'] == '1') ? 'selected' : ''?>>1 Story</option>
                                <option value="2" <?=($_GET['stories'] == '2') ? 'selected' : ''?>>2 Stories</option>
                                <option value="3" <?=($_GET['stories'] == '3') ? 'selected' : ''?>>3 Stories</option>
                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('new_construction_yn', $searchableFields)):?>
                        <div class="form-group">
                            <? $newConstructionYn = (isset($_GET['new_construction_yn']))? $_GET['new_construction_yn'] : null; ?>
                            <select class="form-control <?=($_GET['new_construction_yn']) ? "activeFilter" : "" ?>" name="FeaturedAreas[new_construction_yn]" id="new_construction_yn">
                                <option value="" <?php if(!isset($_GET['new_construction_yn']) || (empty($_GET['new_construction_yn']) && $_GET['new_construction_yn'] != '0')):?>selected="selected"<?php endif; ?>>New Construction</option>
                                <option value="1" <?=($_GET['new_construction_yn'] == '1') ? 'selected' : ''?>>New Construction Only</option>
                                <option value="0" <?=($_GET['new_construction_yn'] == '0') ? 'selected' : ''?>>Exclude New Construction</option>
                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('waterfront_yn', $searchableFields)):?>
                        <div class="form-group">
                            <? $waterfrontYn = (isset($_GET['waterfront_yn']))? $_GET['waterfront_yn'] : null; ?>
                            <select class="form-control <?=($_GET['waterfront_yn']) ? "activeFilter" : "" ?>" name="FeaturedAreas[waterfront_yn]" id="waterfront_yn">
                                <option value="" <?php if(!isset($_GET['waterfront_yn']) || (empty($_GET['waterfront_yn']) && $_GET['waterfront_yn'] != '0')):?>selected="selected"<?php endif; ?>>Waterfront</option>
                                <option value="1" <?=($_GET['waterfront_yn'] == '1') ? 'selected' : ''?>>Waterfront Only</option>
                                <option value="0" <?=($_GET['waterfront_yn'] == '0') ? 'selected' : ''?>>Exclude Waterfront</option>
                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('foreclosure', $searchableFields)):?>
                        <div class="form-group">
                            <select class="form-control <?=($_GET['foreclosure']) ? "activeFilter" : "" ?>" name="FeaturedAreas[foreclosure]" id="foreclosure">
                                <option value="" <?php if(!isset($_GET['foreclosure']) || empty($_GET['foreclosure'])):?>selected="selected"<?php endif; ?>>Foreclosure</option>
                                <option value="1" <?=($_GET['foreclosure'] == '1') ? $selectedText : "";?>>Foreclosures Only</option>
                                <option value="0"<?=($_GET['foreclosure'] == '0') ? $selectedText : "";?>>Exclude Foreclosures</option>

                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('short_sale', $searchableFields)):?>
                        <div class="form-group">
                            <select class="form-control <?=($_GET['short_sale']) ? "activeFilter" : "" ?>" name="FeaturedAreas[short_sale]" id="short_sale">
                                <option value="" <?php if(!isset($_GET['short_sale']) || empty($_GET['short_sale'])):?>selected="selected"<?php endif; ?>>Short Sale</option>
                                <option value="1" <?=($_GET['short_sale'] == '1') ? $selectedText : "";?>>Short Sales Only</option>
                                <option value="0"<?=($_GET['short_sale'] == '0') ? $selectedText : "";?>>Exclude Short Sales</option>

                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('fifty_five_plus_community_yn', $searchableFields)):?>
                        <div class="form-group">
                            <select class="form-control <?=($_GET['fifty_five_plus_community_yn']) ? "activeFilter" : "" ?>" name="FeaturedAreas[fifty_five_plus_community_yn]" id="fifty_five_plus_community_yn">
                                <option value="" <?php if(!isset($_GET['fifty_five_plus_community_yn']) || empty($_GET['fifty_five_plus_community_yn'])):?>selected="selected"<?php endif; ?>>55+ Community</option>
                                <option value="1" <?=($_GET['fifty_five_plus_community_yn'] == '1') ? $selectedText : "";?>>55+ Communities Only</option>
                                <option value="0"<?=($_GET['fifty_five_plus_community_yn'] == '0') ? $selectedText : "";?>>Excluded 55+ Communities</option>

                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('membership_required_yn', $searchableFields)):?>
                        <div class="form-group">
                            <select class="form-control <?=($_GET['membership_required_yn']) ? "activeFilter" : "" ?>" name="FeaturedAreas[membership_required_yn]" id="membership_required_yn">
                                <option value="" <?php if(!isset($_GET['membership_required_yn']) || empty($_GET['membership_required_yn'])):?>selected="selected"<?php endif; ?>>Membership Required</option>
                                <option value="1" <?=($_GET['membership_required_yn'] == '1') ? $selectedText : "";?>>Membership Required</option>
                                <option value="0"<?=($_GET['membership_required_yn'] == '0') ? $selectedText : "";?>>Membership Not Required</option>

                            </select>
                        </div>
                    <? endif; ?>

                    <?if(in_array('acreage', $searchableFields)):?>
                        <div class="form-group">
                            <select class="form-control <?=($_GET['acreage_min']) ? "activeFilter" : "" ?>" name="FeaturedAreas[acreage_min]" id="acreage_min">
                                <option value="" <?php if(!isset($_GET['acreage_min']) || empty($_GET['acreage_min'])):?>selected="selected"<?php endif; ?>>Acreage Min</option>
                                <?
                                $acreageDropDownList = StmFormHelper::getAcreageList();
                                foreach($acreageDropDownList as $key => $value) {
                                    echo "<option value='{$key}' ".(($_GET['acreage_min'] == $key) ? 'selected="selected"': "").">{$value} Acres Min</option>";
                                }
                                ?>

                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control <?=($_GET['acreage_max']) ? "activeFilter" : "" ?>" name="FeaturedAreas[acreage_max]" id="acreage_max">
                                <option value="" <?php if(!isset($_GET['acreage_max']) || empty($_GET['acreage_max'])):?>selected="selected"<?php endif; ?>>Acreage Max</option>
                                <?
                                $acreageDropDownList = StmFormHelper::getAcreageList();
                                foreach($acreageDropDownList as $key => $value) {
                                    echo "<option value='{$key}' ".(($_GET['acreage_max'] == $key) ? 'selected="selected"': "").">{$value} Acres Max</option>";
                                }
                                ?>

                            </select>
                        </div>
                    <? endif;?>

                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <h5>Keywords</h5>
                    <? $keywords = (isset($_GET['keywords']))? $_GET['keywords'] : null; ?>
                    <input class="form-control <?=($_GET['keywords']) ? "activeFilter" : "" ?>" name="FeaturedAreas[keywords]" id="keywords" placeholder="Keywords" value="<?=$keywords?>">
                </div>

                <div class="form-group">
                    <h5>MLS #</h5>
                    <? $listingId = (isset($_GET['listing_id']))? $_GET['listing_id'] : null; ?>
                    <input class="form-control <?=($_GET['listing_id']) ? "activeFilter" : "" ?>" name="FeaturedAreas[listing_id]" id="listing_id" placeholder="MLS #" value="<?=$listingId?>">
                </div>

                <div class="form-group">
                    <h5>Neighborhood</h5>
                    <? $neighborhood = (isset($_GET['neighborhood']))? $_GET['neighborhood'] : null; ?>
                    <input class="form-control <?=($_GET['neighborhood']) ? "activeFilter" : "" ?>" name="FeaturedAreas[neighborhood]" id="neighborhood" placeholder="Neighborhood / Community" value="<?=$neighborhood?>">
                </div>

                <?if(in_array('school_district', $searchableFields)):?>
                    <div class="form-group">
                        <h5>School District</h5>
                        <? $schoolDistrict = (isset($_GET['school_district']))? $_GET['school_district'] : null; ?>
                        <select class="form-control <?=($_GET['school_district']) ? "activeFilter" : "" ?>" name="FeaturedAreas[school_district]" id="school_district">
                            <option value="" <?php if(!isset($_GET['school_district']) || empty($_GET['school_district'])):?>selected="selected"<?php endif; ?>>School District</option>
                            <?
                            $schoolDistrictList = MlsPropertiesSecondary::model(Yii::app()->user->getBoard()->getPrefixedName())->getDistinctList('school_district');
                            foreach($schoolDistrictList as $key => $value) {
                                echo "<option value='{$value}' ".(($_GET['school_district'] == $value) ? 'selected="selected"': "").">{$value}</option>";
                            }
                            ?>
                        </select>
                    </div>
                <? endif; ?>

                <div class="form-group">
                    <h5>Schools</h5>
                    <? $school = (isset($_GET['school']))? $_GET['school'] : null; ?>
                    <input class="form-control <?=($_GET['school']) ? "activeFilter" : "" ?>" name="FeaturedAreas[school]" id="school" placeholder="Schools" value="<?=$school?>">
                </div>

                <div class="form-group">
                    <h5>Street Name</h5>
                    <? $streetName = (isset($_GET['street_name']))? $_GET['street_name'] : null; ?>
                    <input class="form-control <?=($_GET['street_name']) ? "activeFilter" : "" ?>" name="FeaturedAreas[street_name]" id="street_name" placeholder="Street Name" value="<?=$streetName?>">
                </div>

                <div class="form-group">
                    <h5>City</h5>
                    <? $city = (isset($_GET['city']))? $_GET['city'] : null; ?>
                    <input class="form-control <?=($_GET['city']) ? "activeFilter" : "" ?>" name="FeaturedAreas[city]" id="city" placeholder="City" value="<?=$city?>">
                </div>

                <div class="form-group">
                    <h5>Zip</h5>
                    <? $zip = (isset($_GET['zip']))? $_GET['zip'] : null; ?>
                    <input class="form-control <?=($_GET['zip']) ? "activeFilter" : "" ?>" name="FeaturedAreas[zip]" id="zip" placeholder="Zip Code" value="<?=$zip?>">
                </div>

                <div class="form-group">
                    <h5>County</h5>
                    <? $county = (isset($_GET['county']))? $_GET['county'] : null; ?>
                    <input class="form-control <?=($_GET['county']) ? "activeFilter" : "" ?>" name="FeaturedAreas[county]" id="county" placeholder="County" value="<?=$county?>">
                </div>

            </div>

        </div>
    </div>
    <!--/Property Types Multiple Checkbox panels end-->

<?/*	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title"><a data-toggle="collapse" href="#collapseType" class="collapseWill">
					Property Type <span class="pull-left"> <i class="fa fa-caret-right"></i></span> </a></h4>
		</div>
		<div id="collapseType" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="form-group">
					<select class="form-control" name="FeaturedAreas[property_type]" id="property_type">
						<option value="" <?php if(!isset($_GET['property_type']) || empty($_GET['property_type'])):?>selected="selected"<?php endif; ?>>Property Types</option>
					<?php
					$types = CHtml::listData(MlsPropertyTypeBoardLu::model()->findAllByAttributes(array('status_ma'=>1,'mls_board_id'=>Yii::app()->user->boardId)),'mls_property_type_id','displayName');
					foreach($types as $value => $label):
						$selected = "";
						if(isset($_GET['property_type']) && $value == urldecode($_GET['property_type'])) {
							$selected = 'selected="selected"';
						}?>
					<option value="<?=$value; ?>"<?=$selected;?>><?=$label; ?></option>
					<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
	</div>
	<!--/Property Types panels end-->
*/?>

    <?php /*<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title"><a data-toggle="collapse" href="#collapseMore" class="collapseWill">
					More Options <span class="pull-left"> <i class="fa fa-caret-right"></i></span> </a></h4>
		</div>
		<div id="collapseMore" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="block-element">
					<div class="g4 p-fr">
						<a href="/values" class="text-button" style="display: inline-block; font-size: 12px !important; text-decoration: none;"><em class="icon i_stm_dollar"></em>Free House Values Report</a>
					</div>
					<div class="g3 p-fr">
						<a href="/search" class="text-button" style="display: inline-block; font-size: 12px !important; text-decoration: none;"><em class="icon i_stm_search"></em>Advanced Search</a>
					</div>
					<button id="more-search-options" class="btn btn-primary">
						<em class="icon i_stm_add"></em>More Search Options
					</button>
				</div>
			</div>
		</div>
	</div>
	<!--/More Options panels end-->*/ ?>
	<input type="submit" name="startSearch" value="Search" class="btn btn-lg btn-primary home-list-search-button" style="margin-top: 10px;" />
</div>

<?php
$this->endWidget();

//@todo: temp solution, fix with widget full featured out
if(!in_array($_SERVER['SERVER_NAME'], array('seizethemarket.com', 'www.seizethemarket.com'))) {
    $this->renderPartial('_sideBarHouseValues');
}

/**
 * Sidebar Widget Goes Here
 */
Yii::import('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget');
$this->widget('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget', array(
        'areas' => [
            ['name'=>'Top Communities', 'tag'=>'Top Communities Sidebar'],
            ['name'=>'Top Areas','tag'=>'Top Areas Sidebar']
        ]
    ));
Yii::import('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
$this->widget('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
