<div id="area">
    <?php $this->renderPartial('_homeListHeader'); ?>
    <?php echo $areaMessage; ?>

	<div id="home-list-view">
		<div class="main-container">
			<?php $this->renderPartial('_homeList', array(
				'DataProvider' => $DataProvider,
				'model' => $model,
				'area' => $area,
                'images' => $images,
                'mainImage' => $mainImage,
                'isFeaturedArea' => true,
			));?>
		</div>
	</div>
    <?php $this->renderPartial('_homeListFooter'); ?>
	<div id="description-marker" class="p-clr p-p10"></div>
    <div id="mls-disclosure">
        <?php $this->renderPartial('_mlsDisclosure'); ?>
    </div>
</div>