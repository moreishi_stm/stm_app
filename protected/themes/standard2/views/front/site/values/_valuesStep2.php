<?
$address = $SubmissionValues->data[$FormFields->getFieldIdByName('address')];
$cityStZip = $SubmissionValues->data[$FormFields->getFieldIdByName('city')].', '.$SubmissionValues->data[$FormFields->getFieldIdByName('state')] . ' ' . $SubmissionValues->data[$FormFields->getFieldIdByName('zip')];
Yii::app()->clientScript->registerScript('valuesStep2Js', <<<JS

    //*********************************************************************************
    // Google Maps Subject
    //*********************************************************************************
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'address': '{$address}, {$cityStZip}'
    }, function (results) {
//        map.setCenter(results[0].geometry.location);
        var infowindowSubject = new google.maps.InfoWindow();
        var mapSubject = new google.maps.Map(document.getElementById('map-subject'), {
            zoom: 30,
            disableDefaultUI: true,
            scrollwheel: false,
            zoomControl: true,
            center: results[0].geometry.location,
            mapTypeId: google.maps.MapTypeId.SATELLITE
        });
        var markerSubject = new google.maps.Marker({
            // Supply map and position params as usual.
            position: results[0].geometry.location,
            icon: 'http://cdn.seizethemarket.com/assets/images/map-house-marker-green.png',
            map: mapSubject,
            title: '{$address}'

        });
        infowindowSubject.setContent('<div class="subjectAddress">{$address}<br>{$cityStZip}</div>');
        infowindowSubject.open(mapSubject, markerSubject);

    });
JS
, CClientScript::POS_END);


Yii::app()->clientScript->registerCss('valuesStep2Css', <<<CSS
#map-subject {
    /*height: 400px;*/
    /*margin-bottom: 20px;*/
}
#map-subject .gm-style-iw .subjectAddress{
    font-size: 20px;
    font-weight: bold;
    line-height: 35px;
    padding: 10px;
}
CSS
);
?>
<script src="http://maps.google.com/maps/api/js" type="text/javascript"></script>

<div class="form-container-2 fadedBg col-lg-10 col-xs-12 col-lg-offset-1">
    <div class="col-lg-12 status-bar-container text-center">
        <div>50% Complete</div>
        <img src="http://cdn.seizethemarket.<?=((YII_DEBUG)? 'local': 'com')?>/assets/images/status_bar_gray_50.gif" class="col-lg-12 status-bar-image" alt=""/>
        <h1 class="col-lg-12">Property Found at :<br>
            <?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('city')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('state')] . ' ' . $SubmissionValues->data[$FormFields->getFieldIdByName('zip')]; ?>
        </h1>
    </div>
    <div class="col-sm-6 col-xs-12">
        <h2>
            Where should we send your evaluation?
        </h2>
        <div class="col-xs-12 no-padding">
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('email') . ']', $htmlOptions = array('class' => 'col-xs-12',  'placeholder' => 'Enter your Email')); ?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('email') . ']'); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <div class="col-xs-6 no-padding">
                <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('first_name') . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'First Name')); ?>
                <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('first_name') . ']'); ?>
            </div>
            <div class="col-xs-6 no-padding">
                <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('last_name') . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Last Name')); ?>
                <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('last_name') . ']'); ?>
            </div>
            <?php
            $this->widget('StmMaskedTextField', array(
                    'model' => $SubmissionValues,
                    'attribute' => 'data[' . $FormFields->getFieldIdByName('phone') . ']',
                    'mask' => '(999) 999-9999',
                    'htmlOptions' => $htmlOptions = array('placeholder' => 'Enter your Phone Number', 'class' => 'col-xs-12'),
                ));
            ?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('phone') . ']'); ?>
            <?php echo $form->hiddenField($SubmissionValues, 'data[' . $FormFields->getField('form_submission_id')->id . ']'); ?>

            <?php
            $addressFields = array('address', 'city', 'state', 'zip');
            foreach ($addressFields as $addressField) {
                echo $form->hiddenField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName($addressField) . ']', $htmlOptions = array('value' => $$addressField));
            }
            ?>
            <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition') . ']', array('Excellent' => 'Excellent', 'Average' => 'Average', 'Below Average' => 'Below Average', 'Poor' => 'Poor'), $htmlOptions = array('class'=>'col-xs-12 margin-bottom-row','placeholder' => 'Condition', 'empty' => 'Select Condition')); ?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition') . ']'); ?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']', StmFormHelper::getBedroomList(' Bedrooms'), $htmlOptions = array('class'=>'col-xs-12 margin-bottom-row', 'empty' => 'Select Beds')); ?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']'); ?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']', StmFormHelper::getHouseValuesBathList(), $htmlOptions = array('class'=>'col-xs-12 margin-bottom-row', 'empty' => 'Select Baths')); ?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']'); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Approx Sq. Feet')); ?>
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']'); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <input id="submit-button" type="submit" class="submit btn btn-lg btn-primary" value="Send Me My Value">
        </div>
    </div>
    <div id="map-subject" class="col-sm-6 col-xs-12 no-padding map">
    </div>
</div>