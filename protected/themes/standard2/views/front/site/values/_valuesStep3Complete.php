<div class="form-container-2 fadedBg col-lg-10 col-xs-12 col-lg-offset-1" style="margin-top:2.5%;">
    <h1 class="col-lg-12">Congratulations!</h1>
    <h1 class="col-lg-12">Your <?php echo date('F Y');?> House Values Report <br/>will be sent to your Inbox!<br /><br />For immediate assistance call <?php echo Yii::app()->user->settings->office_phone; ?>.</h1>
    <div class="col-xs-12 no-padding">
        <iframe style="height: 350px;" class="map success col-xs-12" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('city')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('state')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('zip')]; ?>&amp;t=h&amp;z=19&amp;output=embed"></iframe>
    </div>
</div>