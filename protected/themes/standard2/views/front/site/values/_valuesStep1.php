<div class="form-container fadedBg col-lg-6 col-md-8 col-md-offset-2 col-lg-offset-3 col-xs-12">
    <h1 class="col-xs-12">What's My House Worth?</h1>
    <h2 class="col-xs-12">FREE <?=date("F Y")?> House Values Report</h2>
	<div id="ValueReportFormGeoLocation_container" class="col-xs-12 no-padding">
		<input class="col-xs-12" placeholder="Type your address" name="ValueReportFormGeoLocation" id="ValueReportFormGeoLocation" type="text" autocomplete="off">
		<div class="errorMessage" id="ValueReportFormGeoLocation_error" style="display:none"></div>
		<div class="results"></div>
	</div>
    <div class="col-xs-12 no-padding">
        <?php echo $form->textField($SubmissionValues, "data[".$FormFields->getFieldIdByName("address")."]", $htmlOptions=array("class"=>"col-xs-12","placeholder"=>"Address"));?>
        <?php echo $form->error($SubmissionValues, "data[" . $FormFields->getFieldIdByName("address") . "]"); ?>
    </div>
    <div class="col-xs-6 col-s-12 no-padding">
        <?php echo $form->textField($SubmissionValues, "data[".$FormFields->getFieldIdByName("city")."]", $htmlOptions=array("class"=>"col-xs-12","data-prompt-position"=>"bottomLeft:0,6", "placeholder"=>"City"));?>
        <?php echo $form->error($SubmissionValues, "data[" . $FormFields->getFieldIdByName("city") . "]"); ?>
    </div>
    <div class="col-xs-3">
        <?php echo $form->dropDownList($SubmissionValues, "data[".$FormFields->getFieldIdByName("state")."]", CHtml::listData(AddressStates::model()->findAll(array("order"=>"short_name ASC")), "short_name", "short_name"),$htmlOptions=array("empty"=>"State","class"=>"col-xs-12"));?>
        <?php echo $form->error($SubmissionValues, "data[" . $FormFields->getFieldIdByName("state") . "]"); ?>
    </div>
    <div class="col-xs-3 no-padding">
        <?php echo $form->textField($SubmissionValues, "data[".$FormFields->getFieldIdByName("zip")."]", $htmlOptions=array("class"=>"col-xs-12", "placeholder"=>"Zip", "maxlength"=>5));?>
        <?php echo $form->error($SubmissionValues, "data[" . $FormFields->getFieldIdByName("zip") . "]"); ?>
        <?php echo $form->hiddenField($SubmissionValues, "formPart"); ?>
    </div>
    <div class="col-xs-12 col-s-12 no-padding">
        <input id="submit-button" type="submit" class="submit btn btn-lg btn-primary" value="Submit">
    </div>
</div>
<?php
$css = <<<CSS
	#ValueReportFormGeoLocation_container{
		position: relative;
	}
	#ValueReportFormGeoLocation_container .results{
		position: absolute;
		top: 52px;
		left:0;
		z-index:100;
		background-color: #fff;
		padding:8px;
		display:none;
	}
	#ValueReportFormGeoLocation_container .results li a{
		z-index:999;
	}
	#ValueReportFormGeoLocation_container .results li{
		padding:8px;
	}
	#ValueReportFormGeoLocation_container .results li.selected,
	#ValueReportFormGeoLocation_container .results li:hover{
		background:#eee;

	}
	#FormSubmissionValues_data_14,
	#FormSubmissionValues_data_16,
	#FormSubmissionValues_data_17,
	#FormSubmissionValues_data_18{
		display:none;
	}
CSS;

Yii::app()->getClientScript()->registerCss('ValueReportFormGeoLocation_css',$css);

$js = <<<JS
var ValueReportFormGeoLocation_results = [];
var ValueReportFormGeoLocation_selected = false;
function stmSetLocation(id){
	var selected = ValueReportFormGeoLocation_results[ id ];
	var street_number;
	var route;
	var city;
	var state;
	var zip;

	ValueReportFormGeoLocation_selected = id;

	$("#ValueReportFormGeoLocation").val(selected.formatted_address);
	$("#ValueReportFormGeoLocation_container .results").hide();
	$.each(selected.address_components,function( key, section){
		if(section.types[0] == "street_number"){
			street_number = section.long_name;
		} else if (section.types[0] == "route"){
			route = section.short_name;
		} else if (section.types[0] == "locality"){
			city = section.long_name;
		} else if (section.types[0] == "administrative_area_level_1"){
			state = section.short_name;
		} else if (section.types[0] == "postal_code"){
			zip = section.long_name;
		}
	});
	$("#FormSubmissionValues_data_14").val(street_number+" "+route);
	$("#FormSubmissionValues_data_16").val(city);
	 $.when( $("#houseValues-form  #FormSubmissionValues_data_17 option").removeAttr("selected") ).done(function() {
		$("#houseValues-form  #FormSubmissionValues_data_17 option:contains(" + state + ")").attr("selected", "selected");
	});
	$("#FormSubmissionValues_data_18").val(zip);
}


$("#ValueReportFormGeoLocation_container .results").mouseleave(function(){
    $(this).hide();
});

$("#ValueReportFormGeoLocation").on("input",function (e) {
	if (e.which == 40){ return; }
	$("#ValueReportFormGeoLocation_container .results").show();
	ValueReportFormGeoLocation_selected = false;
	ValueReportFormGeoLocation_results = [];
	$("#ValueReportFormGeoLocation_container .results").html("No Results");
	var string = $.trim($(this).val());
	if(string && (string.length > 6) ){
		$("#ValueReportFormGeoLocation_container .results").html("Searching....");
		$.get("http://maps.googleapis.com/maps/api/geocode/json?components=country:US",
			{
				"format": "json",
				"address": string,
				"sensor" : true
			},
			function(result){
				if(result.status && (result.status == "OK") ){
					$("#ValueReportFormGeoLocation_container .results").html(" ");
					var country;
					var result_count = 0;
					$.each( result.results, function( index, element ){
						if(result_count == 0){
							$("#ValueReportFormGeoLocation_container .results").html("No Results");
						}
						if(element.formatted_address){
							country = "";
							$.each(element.address_components,function( key, section){
								if(section.types[0] == "country"){ country = section.short_name; }
							});
							if(result_count == 0){ $("#ValueReportFormGeoLocation_container .results").html("<ul></ul>"); }
							result_count++; console.log(index);
							$("#ValueReportFormGeoLocation_container .results ul").append("<li onclick='stmSetLocation("+index+");' ><a>"+element.formatted_address+"</a></li>");
							ValueReportFormGeoLocation_results[index] = element;
						}
					});
				}

			}
		);
	}
});
$( "#ValueReportFormGeoLocation" ).on( "keydown", function( event ) {
	if(event.which == 40 || event.which == 38 || event.which == 13){
		event.preventDefault();
		if(event.which == 13 && !ValueReportFormGeoLocation_selected ){
			return $( "#ValueReportFormGeoLocation_container .results ul li.selected " ).click();
		} else if (ValueReportFormGeoLocation_selected && event.which == 13 ) {
			$( this ).closest("form").submit();
		}
		if($( "#ValueReportFormGeoLocation_container .results ul" ).html()){
			if(! $( "#ValueReportFormGeoLocation_container .results .selected" ).html() ){
				$( "#ValueReportFormGeoLocation_container .results ul li" ).first().addClass( "selected" );
			} else {
				var found = false;
				var used = false;
				var elements;
				if(event.which == 38){
					elements = $("#ValueReportFormGeoLocation_container .results ul li").get().reverse();
				} else {
					elements = $( "#ValueReportFormGeoLocation_container .results ul li" );
				}
				$.each( elements , function( index ) {
					if(found && !used){
						$( this ).addClass("selected");
						used = true;
						return;
					}
					if (!found && !used && $( this ).hasClass("selected")){
						$( this ).removeClass("selected");
						found = true;
					}
				});
			}
		}

	}

});

JS;
Yii::app()->clientScript->registerScript("ValueReportFormGeoLocation_js", $js,CClientScript::POS_END);
?>