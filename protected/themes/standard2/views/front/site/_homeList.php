<?php
$contactId = Yii::app()->getUser()->id;
$listingId = Yii::app()->getRequest()->getQuery('id');

$module = Yii::app()->controller->module->name;
Yii::app()->clientScript->registerCss('homeListCssScript', <<<CSS
    #home-list .categoryFooter {
        border-top: none;
    }
    #home-list .clearfix { clear: both; }
    #home-list .categoryFooter {

    }
    .productFilter p:first-child {
        font-size: 20px;
         margin-top: 10px;
    }

@media (max-width: 992px) {
    .panel-heading, .panel-group .panel {
        width: 100% !important;
    }

}
CSS
);
$jsSavedHomesList = <<<JS
//	$('.btn-quickview').click(function (event) {
//		var listing_id = $(this).data('listing-id');
//
//		$("body").prepend('<div class="loading-container loading"><em></em></div>');
//		$.ajax({
//			url: '/home-ajax/'+listing_id,
//			type: "POST",
//			data: {'listing_id': listing_id},
//			cache: false,
//			success: function(data) {
//				$('.loading-container.loading').remove();
//				if(!data) {
//					Message.create("error", "Error: There has been an error. Please try again.");
//					event.preventDefault(); //STOP default action
//					return false;
//				}
//
//				$('#product-details-modal').modal('show');
//			},
//			error: function(event) {
//				$('.loading-container.loading').remove();
//				Message.create("error", "Error: There has been an error. Please try again.");
//			}
//		});
//	});
    $('.map-view ').click(function(){
        // grab url and append it to /mapsearch/
        //var url = window.location.href;
        // grab all the variables from sidesearchbar and pass to mapsearch
        window.location = '/mapsearch' + buildSearchUrlVars($('#home-listview-search form'));
//        window.location.href = '/mapsearch';
    });

JS;

if(isset($_GET['view']) && !empty($_GET['view'])) {
    if($_GET['view'] == 'list') {
        $jsSavedHomesList .= '$(".list-view").trigger("click")';
    } else {
        $jsSavedHomesList .= '$(".grid-view").trigger("click");';
    }
};

Yii::app()->getClientScript()->registerScript('savedHomesListJs', $jsSavedHomesList);
 ?>

<div id="home-list-view">
	<?php /*<div id="home-listview-search" class="g99 p-mh0 p-pv10 grey-gradient-box">
		<?php $this->renderPartial('_listSearchBox', array(
			'model' => $model
		)); ?>
	</div>*/ ?>

	<div class="">
        <br/>
		<!--left column-->

		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 no-padding">
			<div id="home-listview-search">
				<?php $this->renderPartial('_sideBarSearch', array(
				    'model' => $model,
				    'action' => $action,
                    'searchableFields' => MlsFieldMaps::getSearchableColumns(Yii::app()->user->board->id)
			   )); ?>
			</div>
		</div>

		<!--right column-->
		<div class="home-list-right-container col-lg-10 col-md-9 col-sm-9 col-xs-12">

            <?php
            if($isFeaturedArea) {

                if(count($images) > 1) {
                    $this->renderPartial('_slider', array('model' => $model, 'images' => $images));
                }

                if(!empty($mainImage) && count($images) == 1): ?>
                    <div style="text-align: center;">
                        <img src="<?=$mainImage;?>" class="img-responsive" style="margin: 0 auto;" />
                    </div>
                <?php endif ;?>

            <h1><?php echo Yii::app()->format->properCase($model->name) . ' Homes for Sale'; ?></h1>

            <? if($model->description):?>
            <div id="description">
<!--                <h2>--><?php //echo $model->title; ?><!--</h2>-->

                <p><?php echo nl2br($model->description); ?></p>
            </div>
            <br/>
            <?endif;?>
            <div class="re-stats-container clearfix">
                <h2>Real Estate Statistics</h2>
                <div class="re-stats-col col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Total For Sale:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6"><?=$model->count_homes?></span>
                    </div>
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Average Price:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6"><?php echo Yii::app()->format->formatDollars($model->avg_price);?></span>
                    </div>
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Lowest Price:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6"><?php echo Yii::app()->format->formatDollars($model->min_price);?></span>
                    </div>
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Highest Price:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6"><?php echo Yii::app()->format->formatDollars($model->max_price);?></span>
                    </div>
                </div>
                <div class="re-stats-col col-lg-6 col-md-6 col-sm-6  col-xs-12 no-padding">
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Average Size:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6"><?php echo $model->avg_sf?> SF</span>
                    </div>
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Average Beds:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6"><?php echo $model->avg_bedrooms?></span>
                    </div>
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Average Baths:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6"><?php echo $model->avg_baths?></span>
                    </div>
                    <div class="re-stats-row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="re-stats-label col-lg-6 col-md-6 col-sm-6 col-xs-6">Average $/SF:</span>
                        <span class="data col-lg-6 col-md-6 col-sm-6 col-xs-6">$<?=$model->avg_price_sf?> per SF</span>
                    </div>
                </div>
            </div>

            <div class="re-easy-search-container clearfix">
                <h2>Quick Searches for <?=$model->name?></h2>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6"><a href="/area/<?=$model->url?>">View All Properties</a></div>
                <?
                $max = ($model->max_price < 1000000) ? ceil($model->max_price / 100000)-1 : ceil(($model->max_price > 2000000 ? 3000000 : $model->max_price) / 1000000)-1 +9;

                for($i=0; $i<$max; $i++) {

                    switch($i) {
                        case (0):
                            $priceRange = 'Under $100,000';
                            $priceMin = 0;
                            $priceMax = 100000;
                            break;

                        case (10):
                            $priceRange = 'Above $2,000,000';
                            $priceMin = 2000000;
                            $priceMax = 999000000;
                            break;

                        case ($i > 8):
                            $millionBracket = $i-9;
                            $priceRange = Yii::app()->format->formatDollars(($millionBracket+1)*1000000). ' - ' .Yii::app()->format->formatDollars(($millionBracket+2)*1000000);
                            $priceMin = ($millionBracket+1)*1000000;
                            $priceMax = ($millionBracket+2)*1000000;
                            break;

                        default:
                            $priceRange = Yii::app()->format->formatDollars(($i)*100000). ' - ' .Yii::app()->format->formatDollars(($i+1)*100000);
                            $priceMin = ($i)*100000;
                            $priceMax = ($i+1)*100000;
                            break;
                    }
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6"><a href="/area/<?=$model->url?>/price_min/<?=$priceMin?>/price_max/<?=$priceMax?>"><?=$priceRange?></a></div>
                <? }

                    // this is a filler for if there is 2 across, adding 1 more makes sense. Any more than 1 filler column does not respond well
                    if($i%3 == 1) { ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6"></div>
                    <?}
                elseif($i%3 == 0) { ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6"></div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 hide-small"></div>
                <? } ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
            </div>

            <?php } ?>

            <?php /* <div class="w100 clearfix category-top">
			  <h2> TSHIRT </h2>

			  <div class="categoryImage"><img src="images/site/subcategory.jpg" class="img-responsive" alt="img">
			  </div>
			  </div>
			  <!--/.category-top--> */ ?>

			<div class="w100 productFilter clearfix">
				<p class="pull-left"> <strong><?=$DataProvider->getTotalItemCount(); ?></strong> Properties Found</p>

				<div class="pull-right ">
					<?php /*<div class="change-order pull-right">
						<select class="form-control" name="orderby">
							<option selected="selected">Default sorting</option>
							<option value="popularity">Sort by popularity</option>
							<option value="rating">Sort by average rating</option>
							<option value="date">Sort by newness</option>
							<option value="price">Sort by price: low to high</option>
							<option value="price-desc">Sort by price: high to low</option>
						</select>
					</div>*/ ?>
					<div class="change-view pull-right">
						<a href="#" stmtemplate-title="Grid" class="grid-view"> <i class="fa fa-th-large"></i> </a>
						<a href="#" stmtemplate-title="List" class="list-view "><i class="fa fa-th-list"></i></a>
                        <?
                        //@todo: temp limitation until map feature fleshed out more
                        // MLS Board: 5=fl_mfr
                        if(Yii::app()->controller->action->id != 'area'/* && !in_array(Yii::app()->user->board->id, array(5))*/):?>
                        <a href="javascript:void(0)" title="Map" class="map-view "><i class="fa fa-map-marker"></i></a>
                        <?endif;?>
                    </div>
				</div>
			</div>
			<!--/.productFilter-->

			<?php /* <?php
			  $this->widget('stm_app.widgets.ContainerWidget.ContainerWidget', array(
			  'id' => 'AreasWidgetContainer',
			  'title' => '',
			  'containerClass' => 'row categoryProduct xsResponse clearfix',
			  'widgets' => array(
			  'stm_app.widgets.RowWidget.RowWidget' => array(
			  'id' => 'AreaHomes',
			  'useCustomRowClass' => true,
			  'customRowClass' => 'item col-sm-4 col-lg-4 col-md-4 col-xs-6',
			  'additionalRowCssClasses' => array('item'),
			  'displayWidget' => 'stm_app.widgets.AreaHomeWidget.AreaHomeWidget',
			  'displayWidgetOptions' => array(),
			  'DataProvider' => $DataProvider,
			  'noResultsMessage' => "There are currently no results."
			  )
			  )
			  )
			  );
			  ?> */ ?>


			<?php
            $sortByHtml =
                "<select id='sort'>
                    <option value='highest_price'>Sort by: Highest Price</option>
                    <option value='lowest_price'>Sort by: Lowest Price</option>
                    <option value='newest'>Sort by: Newest</option>
                    <option value='oldest'>Sort by: Oldest</option>
                </select>";

			$this->widget('front_module.components.StmListView', array(
				'dataProvider' => $DataProvider,
				'template' => '{pager}{summary}{items}{pager}{summary}',
				'id' => 'home-list',
                'viewData' => array('mlsDisclosureThumbnail' => $mlsDisclosureThumbnail),
				'ajaxUpdate' => false,
				'itemView' => '_homeListRow',
				'itemsCssClass' => 'xsResponse clearfix',
				'afterAjaxUpdate' => 'js:function(id,data) {
	$(\'#home-list\').replaceWith($(\'#home-list\', \'<div>\'+data+\'</div>\'));
	$(\'div.loading-container.loading\').remove();
}',
				'pagerCssClass' => 'w100 categoryFooter',
				'summaryText' => "<p>Showing {start}-{end} of {count}</p>", //$sortByHtml
				'summaryCssClass' => 'pull-right pull-right col-sm-4 col-xs-12 no-padding text-right text-left-xs',
				'pager' => array(
					'class' => 'CLinkPager',
					'header' => '<div class="pagination pull-left no-margin-top">',
					'footer' => '</div>',
					'firstPageCssClass' => '',
					'selectedPageCssClass' => 'active',
					'nextPageLabel' => "»",
					'prevPageLabel' => "«",
					'lastPageCssClass' => "hidden",
					'firstPageCssClass' => "hidden",
					'htmlOptions' => array('class' => 'pagination no-margin-top')
				)
			));

			/* <div class="w100 categoryFooter">
			  <div class="pagination pull-left no-margin-top">
			  <ul class="pagination no-margin-top">
			  <li><a href="#">«</a></li>
			  <li class="active"><a href="#">1</a></li>
			  <li><a href="#">2</a></li>
			  <li><a href="#">3</a></li>
			  <li><a href="#">4</a></li>
			  <li><a href="#">5</a></li>
			  <li><a href="#">»</a></li>
			  </ul>
			  </div>
			  <div class="pull-right pull-right  col-sm-4 col-xs-12 no-padding text-right text-left-xs">
			  <p>Showing 1–450 of 12 results</p>
			  </div>
			  </div> */
			?>
		</div>
		<!--/.categoryFooter-->
	</div>
	<!--/right column end-->
</div>

<?php
Yii::import('stm_app.widgets.DialogWidget.DialogWidget');
$this->widget('stm_app.widgets.DialogWidget.DialogWidget',
	array(
	'id' => 'product-details-modal',
	'title' => ''
));
