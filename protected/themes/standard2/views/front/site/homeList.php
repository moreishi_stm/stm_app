<?php
$this->renderPartial('_homeList', array(
	'DataProvider'=>$DataProvider,
	'model'=>$model,
	'action' => $action,
    'mlsDisclosureThumbnail' => Yii::app()->user->board->website_disclosure_thumbnail
));
?>
<div id="mls-disclosure" style="color: #666; font-size: 11px; line-height: 1.3;">
    <?php $this->renderPartial('_mlsDisclosure'); ?>
</div>