<div id="login">
	<div id="login-container">
        <h1><em class="login-message"></em>Login to Your Account</h1>
		<?php if($model->resubmit) {
			echo '<h3 class="text-center" style="margin-top:20px;color:#C20000;">Your already have an account. Please login below.</h3>';
		}
		echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>