<?
Yii::app()->clientScript->registerCss('teamProfileView', <<<CSS
	#profile {
		margin-top: 20px;
	}
	#profile em.icon{
		position: relative;
		top: 12px;
		left: 2px;
		margin-top: -11px;
		margin-left: -4px;
		height: 24px;
		width: 24px;
		display: inline-block;
		background-repeat: no-repeat !important;
		position: relative;
		top: 10px;
		margin-right: 10px;
		margin-top: -14px;
		height: 32px;
		width: 41px;
		float:left;
	}
	.i_stm_books {
		background: url(http://cdn.seizethemarket.com/assets/images/profile_sprite.png) 0 0 !important;
	}
	.i_stm_restaurants {
		background: url(http://cdn.seizethemarket.com/assets/images/profile_sprite.png) 0 -42px !important;
	}
	.i_stm_movies {
		background: url(http://cdn.seizethemarket.com/assets/images/profile_sprite.png) 0 -85px !important;
	}
	.i_stm_hobbies {
		background: url(http://cdn.seizethemarket.com/assets/images/profile_sprite.png) 0 -120px !important;
	}
	.i_stm_words {
		background: url(http://cdn.seizethemarket.com/assets/images/profile_sprite.png) 0 -161px !important;
	}
	.i_stm_quotes {
		background: url(http://cdn.seizethemarket.com/assets/images/profile_sprite.png) 0 -247px !important;
	}
	.i_stm_smile {
		background: url(http://cdn.seizethemarket.com/assets/images/profile_sprite.png) 0 -207px !important;
	}
	#profile div.top-container{
		padding: 10px;
		-webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
		-moz-box-shadow: 0 0 10px rgba(0,0,0,0.15);
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
		background: whiteSmoke;
		border: 1px solid #DDD;
		-webkit-border-radius: 6px;
		-moz-border-radius: 6px;
		border-radius: 6px;
		margin: 0 auto;
		float: none;
	}
	#profile #profile-photo{
		border: 2px #CCC solid;
		max-width: 100%;
	}
	#profile h3{
		color:#C20000;
		font-size: 30px;
		font-weight: bold;
	}
	#profile h4{
		font-size: 20px;
		font-weight: bold;
	}
	#profile p.bio {
		font-size: 16px;
	}
	#profile textarea{
		width: 175px;
	}
	#profile div button#send-message{
		clear: both;
		width: 180px;
	}
	#profile ul{
		list-style: none;
		clear: both;
	}
	#profile ul.profile-details li{
	    font-size: 16px;
		padding: 35px 0 20px 5px;
		border-top: 1px solid #DDD;
		width: 100%;
	}
	#profile ul li div.profile-item-container{
		display: inline-block;
		width: 80%;
		min-height: 32px;
	}
	#profile ul li label{
		margin-right: 6px;
	}
CSS
);
?>
<div class="main-container no-padding no-margin">
    <div id="profile" class="col-lg-9 col-md-9 col-sm-12">
        <?php /** Add social icons here */ ?>
        <div class="col-md-3">
            <?php
            if ($file = $SettingContactValues->data[$model->getField('profile_photo')->id]) {
                $image = Yii::app()->user->profileImageBaseUrl . DS . Yii::app()->request->getQuery('id') . DS . $file;
            } else {
                $image = 'http://cdn.seizethemarket.com/assets/images/profile_placeholder_1.jpg';
            }
            ?>
            <img id="profile-photo" src="<?php echo $image; ?>">
        </div>
        <div class="col-md-9">
            <h3 class="name"><?php echo $Contact->fullName; ?></h3>
            <h4 class="title"><?php echo $SettingContactValues->data[$model->getField('title')->id]; ?></h4>
            <h4 class="title"><?php echo $SettingContactValues->data[$model->getField('subtitle')->id]; ?></h4>
            <h4>My Bio:</h4>
            <p class="bio"><?php echo $SettingContactValues->data[$model->getField('bio')->id]; ?></p>
        </div>


        <div class="p-pt10">
            <h4>Fun Facts:</h4>
            <ul class="profile-details"><?php if ($books = $ContactAttributeValues->data[$ContactAttributes->getField('books')->id]) { ?>
                    <li><em class="icon i_stm_books"></em><div class="profile-item-container"><label>Favorite Books:</label><?php echo $books; ?></div></li>
                    <?php
                }
                if ($restaurants = $ContactAttributeValues->data[$ContactAttributes->getField('restaurants')->id]) {
                    ?>
                    <li><em class="icon i_stm_restaurants"></em><div class="profile-item-container"><label>Favorite Restaurants:</label><?php echo $restaurants; ?>
                    </li>
                    <?php
                }
                if ($movies = $ContactAttributeValues->data[$ContactAttributes->getField('movies')->id]) {
                    ?>
                    <li><em class="icon i_stm_movies"></em><div class="profile-item-container"><label>Favorite Movies:</label><?php echo $movies; ?></div></li>
                    <?php
                }
                if ($hobbies = $ContactAttributeValues->data[$ContactAttributes->getField('hobbies')->id]) {
                    ?>
                    <li><em class="icon i_stm_hobbies"></em><div class="profile-item-container"><label>Hobbies:</label><?php echo $hobbies; ?></div></li>
                    <?php
                }
                if ($words = $SettingContactValues->data[$model->getField('words_describe_self')->id]) {
                    ?>
                    <li><em class="icon i_stm_words"></em><div class="profile-item-container"><label>Words to Describes Self:</label><?php echo $words; ?></div></li>
                    <?php
                }
                if ($quotes = $SettingContactValues->data[$model->getField('favorite_quotes')->id]) {
                    ?>
                    <li><em class="icon i_stm_quotes"></em><div class="profile-item-container"><label>Favorite Quotes:</label><?php echo $quotes; ?></div></li>
                    <?php
                }
                if ($moments = $SettingContactValues->data[$model->getField('memorable_moments')->id]) {
                    ?>
                    <li><em class="icon i_stm_smile"></em><div class="profile-item-container"><label>Most Memorable Real Estate
                                Moments:</label><?php echo $moments; ?></div></li>
                <?php } ?>
            </ul>
        </div>
	</div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        <?
        $this->renderPartial('_sideBarHouseValues');

        /**
         * Sidebar Widget Goes Here
         */
        Yii::import('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget');
        $this->widget('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget', array(
                'areas' => [
                    ['name'=>'Top Communities', 'tag'=>'Top Communities Sidebar'],
                    ['name'=>'Top Areas','tag'=>'Top Areas Sidebar']
                ]
            ));
        Yii::import('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
        $this->widget('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
        ?>
    </div>
</div>
