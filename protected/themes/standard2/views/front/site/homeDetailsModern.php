<?php
function _stmPropertyFeatureCount($set){
	$count = 0;
	if(is_array($set) && !empty($set)){
		foreach ($set as $label => $data){
			if(strstr($data,",")){
				$count = $count + count(explode(",",$data));
			}
			$count++; // need the label either way
		}
	}
	return $count;
}
$domainSuffix = (YII_DEBUG) ? 'local' : 'com';
$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";

$contactId = Yii::app()->user->id;
$listingId = Yii::app()->request->getQuery('id');

$addRemoveClass = ($isFavorite) ? 'remove-favorites' : 'add-favorites';
$addRemoveButtonClass = ($isFavorite) ? 'fa fa-remove' : 'fa fa-star';
$addRemoveText = ($isFavorite) ? 'Saved Favorite' : 'Save to Favorite';

$photoContent['thumbnails'] = json_decode($photoContent['thumbnails']);
$mlsDisclosureThumbnail = Yii::app()->user->board->website_disclosure_thumbnail;
$curentSharePage = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
$formattedAddress = $model->formatMlsAddress($model);
Yii::app()->controller->fullWidth = true;
Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile($this->module->cdnUrl . 'assets/css/idangerous.swiper.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile("http://maps.google.com/maps/api/js?key=AIzaSyA5On2atWpapg7fDL7r7QDJ3mb_QDtL0YE",CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile("https://fonts.googleapis.com/icon?family=Material+Icons");
?>
<div id="HomeDetailsImageSlider">
<?
if(!empty($photoContent['thumbnails'])) {
	?>
		<?php foreach ($photoContent['thumbnails'] as $i => $photoUrl):
			if (empty($similarProperty['photoUrl'])) {
				$similarProperty['photoUrl'] = "http://cdn.seizethemarket.com/assets/images/default-placeholder.png";
			}
			?>
			<div class="item">
				<div class="product">
					<?php if ($i == 0): ?>
						<a href="<?= $photoContent['fullSizeUrl'][$i]; ?>" class="product-image fancybox" rel="HomeDetailsImageSlider">
							<img src="<?= $photoUrl; ?>" class="product-image" alt="img">
						</a>
					<?php else: ?>
						<a href="<?= $photoContent['fullSizeUrl'][$i]; ?>" class="product-image fancybox" rel="HomeDetailsImageSlider">
							<img src="<?= $photoUrl; ?>" class="img-responsive" alt="img">
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
	<?php
} #end if !empty
?>
	</div>

	<div class="transitionfx" id="home-detail" >
		<!-- right column -->
		<div id="property-details-container" >
			<div class="container">
				<?php
				if($isFavorite){
					$favContent = "<span><i class=\"material-icons\">favorite</i></span>Saved Favorite";
				} else {
					$favContent = "<span><i class=\"material-icons\">favorite_border</i></span>Save to Favorites";
				}
				?>
				<div class="ctabuttons">
					<ul>

						<li>
							<a
								title="Add to favorites"
								class="cta-button-tip add-fav<?=($isFavorite) ? " active" : "";?>"
								data-listingid="<?=$listingId?>" data-mbid="<?=$mlsBoardId?>"
								data-contact-id="<?=$contactId; ?>"
								data-tooltip="Add to Favorites"
								id="add-favorites-button"
							>
								<?php echo $favContent;?>
							</a>
						</li>
						<li>
							<a id="ask-question-button" class="cta-button-tip" data-tooltip="Ask a Question">
								<span><i class="fa fa-comment-o"></i></span>
								Ask a Question
							</a>
						</li>
						<?/*		<li>
			<a href="#map-tab" id="map-fav-link" title="View Map" class="cta-button-tip" data-tooltip="View the Map">
				<i class="fa fa-map-marker"></i>
				View Map
			</a>
		</li>
						<li>
							<a id="send-to-friend-button-fav" href="#share-fav-link" id="share-fav-link" title="Share" class="cta-button-tip" data-tooltip="Share this">
								<i class="fa fa-envelope-o"></i>
								Email to Friend
							</a>
						</li>
*/?>
						<li>
							<a id="print-flyer-button" class="cta-button-tip" data-tooltip="Print Flyer">
								<span><i class="fa fa-print"></i></span>
								Print
							</a>
						</li>
						<li>
							<a href="#showing-request-form" class="submit btn btn-primary request-showing-button cta-button-tip scrollTo"  data-tooltip="Request a Showing">
								Request Showing
							</a>
						</li>
					</ul>
				</div>

				<div class="details-description col-sm-7 no-padding">
						<div class="spec location col-lg-12" style="margin-bottom: 8px;">
							<ul>
								<li><?php echo ($model['common_subdivision']) ?  Yii::app()->format->formatProperCase($model['common_subdivision']) : ''; ?></li>
								<li class="cityStateZip"><?php echo $model->getCityStZip();?></li>
							</ul>
						</div>
						<div class="spec address  col-lg-12" style="margin-bottom: 12px">
							<?php echo $model->getStreetAddress();?>
						</div>
<!--					</div>-->
                </div>
            </div>
            <div class="details-description-border container" style="">
                <div class="info col-sm-7 no-padding no-margin">
                    <?php
                    if($model['bedrooms']){ ?>
                        <span class="spec">
                            <?php echo $model['bedrooms'] ?> BD
                        </span>
                    <?php }
                    ?>
                    <?php
                    if($model['baths_total']){ ?>
                        <span class="spec">
                            <?php echo $model['baths_total'] ?> BA
                        </span>
                    <?php }
                    ?>
                    <?php
                    if($model['sq_feet']){ ?>
                        <span class="spec">
                            <?php echo ((Yii::app()->user->getBoard()->is_sq_ft_range) ? $model['sq_feet'] : Yii::app()->format->formatNumber($model['sq_feet'])) ?> SF
                        </span>
                    <?php }
                    ?>
                    <span class="spec">
                        <?php echo (strpos($_SERVER['SERVER_NAME'],'findhomesinpdx') !== false)? $model['contingent'] : Yii::app()->format->formatDollars($model['price']); ?>
                    </span>
                    <span class="spec">
                        <a href="javascript:void(0)" class="mortgageCalcToggle" onclick="calculatorToggle();" title="Mortgage Calculator">
                            <i class="fa fa-calculator"></i>
                        </a>
                    </span>
                </div>
            </div>
		</div>
		<div class="clearfix"></div>
		<div id="mortgage-calculator-tab" class="row" style="display:none;"></div>
		<div class="container">
			<?php
			$modelSections = array(
				array(
					"label" => "Exterior Features",
					"data" => $exteriorFeatures
				),
				array(
					"label" => "Interior Features",
					"data" => $interiorFeatures
				),
				array(
					"label" => "Property Features",
					"data" => $propertyFeatures
				)
			)
			?>
			<div class="features clearfix">
				<?php
				if(!empty($modelSections)){
					$sectionIdCount =0;
					foreach ($modelSections as $section){

						$item_count = _stmPropertyFeatureCount($section["data"]);
						if($item_count > 2){
							$item_count = ceil( ($item_count / 2) );
						}
						if(!empty($item_count)){
							$sectionIdCount++;
							?>
							<div id="featureSection<?php echo $sectionIdCount;?>" class="border-bottom clearfix feature-section">
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h3><?php echo $section["label"];?></h3>
									<?php
									if($item_count > 5){?>
										<a class="show-more">Show More <i class="fa  fa-angle-down"></i></a>
									<?php }
									?>

								</div>

								<div class="col-sm-3 col-md-3 col-lg-3">
									<?php
									if (is_array($section["data"]) && !empty($section["data"])) {
										$item_count = _stmPropertyFeatureCount($section["data"]);
										if($item_count > 2){
											$item_count = ceil( ($item_count / 2) );
										}
										$current = 1;
										$colCount = 1;
										$col2Used = false;
										ksort($section["data"]);
										foreach($section["data"] as $label=>$value) {
											if(!$col2Used && $current >= $item_count){
												echo "</div><div class=\"col-sm-3 col-md-3 col-lg-3\">";
												$col2Used = true;
												$colCount = 1;
											}
											if($colCount > 5){$hiddenStyle = "hidden-data";}else{$hiddenStyle = "";}
											if(strstr($value,",")){
												echo "<li class='{$hiddenStyle}'><label>{$label}</label><ul class='{$hiddenStyle}'>";
												$current++; //for label row
												$colCount++;

												$subs = explode(",",$value);
												foreach ($subs as $sub){
													if($colCount > 5){$hiddenStyle = "hidden-data";}else{$hiddenStyle = "";}
													echo "<li class='{$hiddenStyle}'>{$sub}</li>";
													$current++;
													$colCount++;
												}
												echo "</ul></li>";

											}else{
												if(strtolower($label) == 'virtual tour') {
													echo "<li class='{$hiddenStyle}'><label>{$label}:</label>&nbsp;<a href='{$value}' target='_blank'>{$value}</a></li>";
												} else {
													echo "<li class='{$hiddenStyle}'><label>{$label}:</label>&nbsp;{$value}</li>";
												}
												$current++;
												$colCount++;
											}
										}
									}
									?>
								</div>
							</div>
							<div style="clear:both;"></div>
						<?php }
					}
				}
				?>
			</div>

			<div class="features clearfix">
				<div id="featureSectionDescription" class="border-bottom clearfix feature-section">

					<div class="col-sm-3 col-md-3 col-lg-3">
						<h3>Property Description</h3>
						<a class="show-more">Show More <i class="fa fa-angle-down"></i> </a>
					</div>
					<div class="col-sm-8 col-md-8 col-lg-8 content short">
						<?php echo $model->public_remarks; ?>
					</div>
				</div>
				<?php
				if(isset($schoolFeatures) && !empty($schoolFeatures)){ ?>
					<div id="featureSectionSchool" class="border-bottom clearfix feature-section">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<h3>School Information</h3>
						</div>
						<div class="col-sm-8 col-md-8 col-lg-8">
							<ul class='feature_list'>
								<?php
								if (isset($schoolFeatures)) {
									ksort($schoolFeatures);
									foreach($schoolFeatures as $label=>$value) {
										echo '<li><label>',$label,'</label>',  $value,'</li>';
									}
								} else {
									echo 'No data found...';
								}
								?>
							</ul>
						</div>
					</div>
				<?php }
				?>
			</div>
            <div style="clear:both;"></div>
            <div class="listing-office">
                Courtesty of <?=ucwords(strtolower($model->getListingOfficeName()))?>
            </div>

			<?php
			$googleDataSubject = StmFunctions::getGoogleAddressData($model->getStreetAddress(), $model->city, $model->state, $model->zip); // get zip only
			if(isset($googleDataSubject['results'][0]['geometry']['location']['lat'])) {
				$subjectMarkerCenter = ($googleDataSubject['results'][0]['geometry']['location']['lat']).','.$googleDataSubject['results'][0]['geometry']['location']['lng']; //['lat']+.0001
				$subjectMapCenter = ($googleDataSubject['results'][0]['geometry']['location']['lat']).','.$googleDataSubject['results'][0]['geometry']['location']['lng']; //['lat']+.00015
				$subjectCenter = $googleDataSubject['results'][0]['geometry']['location']['lat'].','.$googleDataSubject['results'][0]['geometry']['location']['lng'];
			}
			$streetAddress = $model->getStreetAddress();
			$cityStZip = $model->getCityStZip();
			$geoAddress = $streetAddress.", ".$cityStZip;
			$bedrooms = $model->bedrooms;
			$baths = $model->baths_total;
			$sf = $model->sq_feet;

			$specs = array();
			if($bedrooms){
				$specs[] = $bedrooms." BD";
			}
			if($baths){
				$specs[] = $baths." BA";
			}
			if($sf){
				$specs[] = $sf." SF";
			}
			if(!empty($specs)){
				$specs = "<div class=\"subjectSpecs\">".implode("&nbsp; | &nbsp;",$specs)."</div>";
			}else{
				$specs = "";
			}
			?>
			<div id="map-tab" class="dual-map map" style="min-height:240px;">
				<i class="fa fa-close"></i>
			</div>
			<div id="map-subject-pano" class="dual-map map" style="min-height:240px;"></div>
		</div>


		<div id="showing-request-form" class="form-container" style="margin:20px auto;">
			<div id="form" class="form-inline">

				<?php
				$formId = Forms::FORM_SHOWING_REQUEST;
				$SubmissionValues = new FormSubmissionValues($formId);
				$FormFields = new FormFields;

				if (!Yii::app()->user->isGuest) {

					$SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;
					$SubmissionValues->data[$FormFields->getField('last_name')->id] = Yii::app()->user->lastName;
					$SubmissionValues->data[$FormFields->getField('email')->id] = Yii::app()->user->contact->getPrimaryEmail();
					$SubmissionValues->data[$FormFields->getField('phone')->id] = Yii::app()->user->contact->getPrimaryPhone();
				}

				$form = $this->beginWidget('CActiveForm', array(
					'id' => 'video-form',
					'action' => array('/front/forms/showing/formId/' . $formId),
					'enableAjaxValidation' => true,
					'enableClientValidation' => false,
					'clientOptions' => array(
						'validateOnChange' => false,
						'validateOnSubmit' => true,
						'beforeValidate' => 'js:function(form) {
									$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
									return true;
								}',
						'afterValidate' => 'js:function(form, data, hasErrors) {
								$("div.loading-container.loading").remove();
								if (!hasErrors) {
									Message.create("success", "Your Showing Request was sent successfully!");
									$("div#form").addClass("hidden");
									$("div#thank-you-message").hide().removeClass("hidden").show();
								}

								return false;
							}',
					)
				));
				?>

				<div class="container main-container">
					<h2>Request a Showing</h2>
					<div class="form-group col-sm-6 col-xs-12 no-padding">
						<?php
						echo $form->textArea($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']', $htmlOptions = array(
							'placeholder' => "When would you like to see this home?", 'class' => 'col-xs-12', 'rows'=>4
						));
						?>
						<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']'); ?>
						<div class="form-group col-xs-12 no-padding">
							<div class="col-sm-6 col-xs-6 no-padding">
								<?php
								echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array(
									'placeholder' => 'First Name', 'class' => 'col-xs-12',
								));
								?>
								<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
							</div>
							<div class="col-sm-6 col-xs-6 no-padding">
								<?php
								echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array(
									'placeholder' => 'Last Name', 'class' => 'col-xs-12',
								));
								?>
								<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
							</div>
						</div>

						<div class="form-group col-xs-12 no-padding">
							<div class="col-sm-6 col-xs-6 no-padding">
								<?php
								echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array(
									'placeholder' => 'E-mail', 'class' => 'col-xs-12',
								));
								?>
								<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
							</div>
							<div class="col-sm-6 col-xs-6 no-padding">
								<?php
								$this->widget('StmMaskedTextField', array(
									'model' => $SubmissionValues,
									'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
									'mask' => '(999) 999-9999',
									'id' => 'FormSubmissionValues_data_4',
									'htmlOptions' => array('class' => 'col-xs-12','placeholder'=>'Phone'),
								));
								?>
								<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
								<?php echo CHtml::hiddenField('url', $_SERVER['REQUEST_URI']); ?>
							</div>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::SOURCE_DESCRIPTION_ID.']', $htmlOptions=array('value'=>'Showing Request'));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$model->listing_id));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$model->getPhotoUrl(1)));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('home_url') .']', $htmlOptions=array('value'=>$model->getUrl()));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$model->price));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$model->bedrooms));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$model->baths_total));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$model->sq_feet));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->streetAddress)))));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->city)))));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->state)))));?>
							<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$model->zip));?>
						</div>
						<div class="col-xs-12 no-padding">
							<button id="submit-button" type="submit" class="submit btn btn-primary request_showing_form_button">Send Request</button>
						</div>
					</div>

					<div class="form-group col-sm-6 col-xs-12 form-contact-container">
						<div class="col-xs-12">
							<h3>
								<?
								if($leadRoute = LeadRoutes::model()->findByAttributes(array('component_type_id'=>ComponentTypes::BUYERS))) {
									$agents = $leadRoute->getActingAgents();
									if($settingValue = SettingContactValues::model()->findByAttributes(array('setting_id'=>Settings::SETTING_PROFILE_PHOTO_ID, 'contact_id'=>$agents[0]->id))) {
										$profileUrl = Yii::app()->user->getProfileImageBaseUrl().'/'.$agents[0]->id.'/'.trim($settingValue->value);
									}
								}
								?>
								<div class="profile-photo-container">
									<?
									if($profileUrl):
										?>
										<img class="profile" src="<?=$profileUrl?>"/><br>
										<?
									endif;?>
								</div>
								<?
								$agentName = $agents[0]->fullName;
								?>
								<?=$agentName?>
							</h3>
						</div>
						<div class="col-xs-12">
							<label>Phone:</label>
							<span><?=Yii::app()->user->settings->office_phone?></span>
						</div>
						<div class="col-xs-12 office-name">
							<span><?=Yii::app()->user->settings->office_name?></span>
						</div>
					</div>
				</div>

				<?php $this->endWidget(); ?>

			</div>

			<div id="thank-you-message" class="hidden container">
				<img src="<?=$model->getPhotoUrl(1)?>" alt=""/>
				<h3>Thank You!</h3>
				We received your Showing Request. <br /><br />

				Feel free to call <strong><?php echo Yii::app()->user->settings->office_phone; ?></strong> for immediate assistance. <br /><br />

				Look forward to seeing you soon!<br><br>

				<h3 class="agent-name"><?=$agentName?></h3>
			</div>
		</div>

		<?php
		if(count($similarProperties) > 0){ ?>
			<div class="row recommended col-lg-12">
				<div class="container main-container">
					<h1> Similar Properties (<?= count($similarProperties); ?>) </h1>
					<div id="SimilarProductSlider">
						<?php foreach ($similarProperties as $similarProperty):
							if(empty($similarProperty['photoUrl'])){
								$similarProperty['photoUrl'] = "http://cdn.seizethemarket.com/assets/images/default-placeholder.png";
							}
							?>
							<div class="item">
								<div class="product">
									<a class="product-image" href="<?= $similarProperty['propertyUrl']; ?>">
										<img src="<?= $similarProperty['photoUrl']; ?>">
										<div class="description">
											<div class="price">
												<span><?= Yii::app()->format->formatDollars($similarProperty['price']); ?></span>
											</div>
											<div class="price">
										<span>
											<?=($similarProperty['bedrooms']) ? $similarProperty['bedrooms']." Bed" : ""; ?>
											<?=($similarProperty['baths_total']) ? " | ".$similarProperty['baths_total']." Bath" : ""; ?>
										</span>
											</div>
										</div>
                                    </a>
								</div>
                                <div style="color: #555; font-size: 12px;"><?
                                    $mlsDisclosurelistingOfficeName = $similarProperty['listingOfficeName'];
									if(!empty($mlsDisclosureThumbnail)) {
										eval('echo ' . $mlsDisclosureThumbnail);
									}?>
                                </div>
							</div>
						<?php endforeach; ?>
						<?php /*</div>*/ ?>
					</div>
				</div>
				<!--/.recommended-->
			</div>
			<?php
		}
		?>
	</div>
	<div style="clear:both"></div>
<?php
if($isFavorite){
	$favContent = "<span><i class=\"material-icons\">favorite</i></span>Favorite";;
} else {
	$favContent = "<span><i class=\"material-icons\">favorite_border</i></span>Add to Favorites";
}

$dialogWidget = 'front_widgets.DialogWidget.HomeDetailsDialogWidget.HomeDetailsDialogWidget';
$isGuest = Yii::app()->user->isGuest;
$theme = Yii::app()->theme->name;
$propertyDetails = $model->getHomeDetails($model->listing_id);
$photoUrls = explode(',', $propertyDetails['photoUrls']);
$photoUrl = $photoUrls[0];

//@todo: temp quick fix for non-photo url
if(!Yii::app()->user->board->has_photo_url) {
	$photoUrl = $model->getPhotoUrl(1);
}
if ($viewsMaxReached) {

} else {
	// Send to Friend Dialog Widget

	$this->widget($dialogWidget, array(
		'id'            => 'send-to-friend-dialog-fav',
		'type'          => 'send-to-friend',
		'property'      => $model,
		'photoUrl'      => $photoUrl,
		'isGuest'       => $isGuest,
		'title'         => 'Send to a Friend',
		'triggerElement'=> '#send-to-friend-button-fav',
		'formView'      => '_sendToFriend',
        'theme' => $theme
	));


	// Ask Question Dialog Widget
	$this->widget($dialogWidget, array(
		'id'             => 'ask-question-dialog',
		'type'           => 'ask-question',
		'property'       => $model,
		'photoUrl'       => $photoUrl,
		'isGuest'        => $isGuest,
		'title'          => 'Ask a Question',
		'triggerElement' => '#ask-question-button',
		'formView'       => '_askQuestion',
		'theme' => $theme
	));
}

$script = <<<CSS
.banner {
    margin-top: 0;
}
#home-detail {
    margin-top: 10px;
}
#home-detail .spec {
    font-size: 16px;
    color: #000;
}
#home-detail .spec.address{
    padding-left: 12px;
    padding-right:12px;
    font-size: 36px;
    min-height: 50px;
    line-height: 40px;
    margin:12px 0;
    clear: both;
}

#home-detail .feature_list {
    list-style: none;
}
#home-detail .feature_list li{
    float:left;
    width:210px;
    margin-bottom: 8px;
    font-size: 13px;
}
#home-detail .feature_list li:not(:first-of-type){
    margin-left: 32px;
}
#home-detail .feature_list label{
    display: block;
    border-bottom:none;
    font-weight: bold;
    line-height: 18px;
}
#home-detail .sidebar{
    float: right;
    width: 205px;
}
#home-detail .sidebar .side-container{
    width: 205px;
    position: relative;
    float: right;
    margin-top: 15px;
    margin-right: -20px;
    background: #fafafa;
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    padding: 10px 10px 10px 10px;
}
#home-detail .sidebar .request-container{
    background-color: #FFF8E6;
    border: 1px solid #FFB62E;
}
.listing-office {
    font-size: 14px;
    color: #777;
    text-align: right;
    margin-top: 20px;
}

@media (max-width: 992px) {
    .row.cabuttons a.btn {
        width: 100%;
        margin: 2px 0;
    }
}

.productThumb{
    max-height: 200px;
    min-height: 75px;
    overflow: hidden;
    overflow-y: auto;
    margin-bottom: 10px;
}
.product-tab.w100 {
    padding-top: 10px;
    margin-top: 10px;
    border-top: 1px solid #dddddd;
}
/*.product-share {
    margin-top: 0;
    margin-bottom: 10px;
}*/
#SimilarProductSlider {
    position: relative;
}
#SimilarProductSlider .product{
    min-height: 220px;
    max-width: 220px;
    background: #F3F3F3;
    color: #333;
}
#SimilarProductSlider .product a:hover {
    text-decoration: none;
    color: #333;
}
#SimilarProductSlider .description{
    padding-top: 15px;
}
#SimilarProductSlider .item .price {
    text-align: center;
    margin-left: 0;
}
#SimilarProductSlider .product-image img {
    height: 145px;
}
@media (max-width: 768px) {
    .headerOffset {
        padding-top: 145px;
    }
    .row.cabuttons a.btn {
        width: 100%;
        margin: 5px 0;
    }
}
.home-details-dialog h3{
    font-size: 18px;
}
.home-details-dialog .property-container{
    float: left;
}
.home-details-dialog div.property-image{
    width: 280px;
    vertical-align: top;
    float: left;
}
.home-details-dialog div.property-info{
    width: 245px;
    float: left;
    padding-top: 20px;
    padding-left: 20px;
}
.home-details-dialog .price{
    font-size: 16px;
    color: #3C0;
    font-weight: bold;
    margin: 5px 0;
    float: left;
}
.home-details-dialog .details{
    clear: both;
    font-size: 13px;
    font-weight: normal;
}
.home-details-dialog .dialog-form-container{
    clear: both;
    width: 100%;
    padding-top: 15px;
}
.home-details-dialog .dialog-form-container table{
    width: 100%;
}
.home-details-dialog .dialog-form-container table th{
    width: 150px;
    text-align: right;
    padding-right: 5px;
}
.home-details-dialog hr{
    margin-left: auto;
    margin-right: auto;
    width: 90%;
}

.home-details-dialog .submit-button-row{
    padding: 23px 15px 15px 15px;
    text-align: center;
    position: relative;
}
.home-details-dialog .home-details-dialog-submit-button{
    text-transform: uppercase;
    font-size: 11px;
    padding-left: 18px;
    padding-right: 18px;
    cursor: pointer;
    width: 200px;
}
.home-details-dialog .home-details-dialog-loading{
    width: 32px;
    height: 32px;
    text-align: center;
    position: absolute;
    margin-left: -15px;
    top: -6px;
    left: 50%;
}
.home-details-dialog .home-details-dialog-loading.loading{
    background: url('http://cdn.seizethemarket.com/assets/images/loading.gif') no-repeat;
    opacity: 1 !important;
    position: relative;
}
.home-details-dialog .dialog-form-container table, .home-details-dialog .dialog-form-container table th, .home-details-dialog .dialog-form-container table td{
    background: none;
    border: none;
}
.home-details-dialog h3{
    font-size: 18px;
}
.home-details-dialog .property-container{
    float: left;
}
.home-details-dialog div.property-image{
    width: 280px;
    vertical-align: top;
    float: left;
}
.home-details-dialog div.property-info{
    width: 245px;
    float: left;
    padding-top: 20px;
    padding-left: 20px;
}
.home-details-dialog .price{
    font-size: 16px;
    color: #3C0;
    font-weight: bold;
    margin: 5px 0;
    float: left;
}
.home-details-dialog .details{
    clear: both;
    font-size: 13px;
    font-weight: normal;
}
.home-details-dialog .dialog-form-container{
    clear: both;
    width: 100%;
    /*padding-top: 15px;*/
}
.home-details-dialog .dialog-form-container table{
    width: 100%;
}
.home-details-dialog .dialog-form-container table th{
    width: 150px;
    text-align: right;
    padding-right: 5px;
}
.home-details-dialog hr{
    margin-left: auto;
    margin-right: auto;
    width: 90%;
}

.home-details-dialog .submit-button-row{
    padding: 23px 15px 15px 15px;
    text-align: center;
    position: relative;
}
.home-details-dialog .home-details-dialog-submit-button{
    text-transform: uppercase;
    font-size: 11px;
    padding-left: 18px;
    padding-right: 18px;
    cursor: pointer;
    width: 200px;
}
.home-details-dialog .home-details-dialog-loading{
    width: 32px;
    height: 32px;
    text-align: center;
    position: absolute;
    margin-left: -15px;
    top: -6px;
    left: 50%;
}
.home-details-dialog .home-details-dialog-loading.loading{
    background: url('http://cdn.seizethemarket.com/assets/images/loading.gif') no-repeat;
    opacity: 1 !important;
    position: relative;
}
.home-details-dialog .dialog-form-container table, .home-details-dialog .dialog-form-container table th, .home-details-dialog .dialog-form-container table td{
    background: none;
    border: none;
}
.recommended {
    margin-bottom: 10px;
    padding-top: 60px;
    padding-bottom: 60px;
    border-top: 0;
}
#SimilarProductSlider .item {
    padding: 0px;
    float: left;
}
.socialIcon {
    margin-left: 10px;
    float: left;
    padding-top: 10px;
}
.nextControl {
    right: 40px;
}
.prevControl, .nextControl,
.prevControl i, .nextControl i {
    cursor: pointer;
}

.recommended {
    margin-bottom: 10px;
}
#SimilarProductSlider .item {
    padding: 0px;
    float: left;
}
.socialIcon {
    margin-left: 10px;
    float: left;
    padding-top: 10px;
}
.nextControl {
    right: 40px;
}
.prevControl, .nextControl,
.prevControl i, .nextControl i {
    cursor: pointer;
}

.form-container {
    padding: 60px 30px;
    margin: 20px 0;
    background: #EFEFEF;
}
.form-container h2 {
    font-size: 40px;
}
.form-container .submit {
    width: 260px;
    font-size: 30px;
    float:right;
    border-radius: 0;
}
.form-container .submit:hover, .request-showing-button:hover {
    background-color: #25ad29;
}
.form-contact-container, #thank-you-message {
    font-size: 20px;
}
.form-contact-container h3, #thank-you-message h3{
    font-size: 30px;
}
#thank-you-message img {
    height: 250px;
    float: left;
    margin-right: 30px;
}
.form-contact-container .office-name {
    margin-top: 10px;
}
.form-contact-container .agent-name {
    margin-top: 30px;
}
.form-contact-container .profile-photo-container {
    min-height: 20px;
}
.form-contact-container .profile {
    max-height: 150px;
    margin-bottom: 10px;
}

.thumbnail-container {
    overflow-x: scroll;
}

.ui-widget form input[type="submit"] {
    width: 100%;
    padding: 14px;
    font-size: 14px;
}
.add-fav{
    top:4px;
    right:10px;
    position: relative;
    margin-left: 8px;
}

/**
 * modern
 */
#property-details-container {
    border-bottom: 2px solid;
 }
.details-description {
    margin-top: 25px;
}
#property-details-container .details-description-border {
    padding-bottom: 6px;
}
.hidden-data{
    display:none;
}
.spec{
    position: relative;
}
#home-detail .info .spec {
    padding-left: 12px;
    padding-right:12px;
    padding-bottom: 6px;
}

.info{
    padding:8px;
}
.info .spec:not(:first-child)::before {
    content: "";
    height: 12px;
    width: 2px;
    position: absolute;
    bottom: -2px;
    left: 0;
    background-color: #000;
}
#mortgage-calculator-tab{
    background: #000;
    position: relative;
}
#mortgage-calculator-tab h1 a{
    color: #fff;
}

#home-detail .features .row{
    margin-bottom: 24px;
    padding-bottom: 12px;
    border-bottom: 1px solid;
}
.map{
    width:100%;
    margin:60px auto;
    height: 400px;
}
.dual-map{
    float:left;
    width:50%;
}
.spec ul{
    padding:0;
    width:100%;
}
.spec ul li{
    display: list-item;
    float:left;
    font-size: 16px;
}
.spec ul li:not(:first-of-type):before{
    font-family: FontAwesome;
    content: '\f04d';
    font-size: 4px;
    top: -2px;
    position: relative;
    margin-right: 8px;
}
.spec.location ul li.cityStateZip{
    list-style-type: disc;
    margin-left: 20px;
}
.border-bottom{
    border-bottom: 1px solid #ccc;
    margin-top: 24px;
    padding-bottom: 16px;
}
#home-detail .spec.location{
    font-size: 14px;
}
.request-showing-button{
    background-color: #000;
}
@media (max-width: 992px) {
    .form-contact-container{
        padding-left: 0;
        padding-right: 0;
    }
    .form-container .submit,
    .request-showing-button{
        float:none;
    }
    .form-container h2{
        font-size: 24px;
    }
    .listing-office{
        text-align: left;
    }

    #home-detail .details-description .spec:first-of-type, #home-detail .spec.address{
        padding-left: 0;
    }

    #home-detail .details-description .spec{
        font-size: 30px;
        font-weight: 400;
    }
    #home-detail .details-description .spec .location{
        margin-bottom: 12px !important;
    }
    #home-detail .address{

        font-weight: 900;
    }
    #home-detail .info .spec{
        /*font-size: 14px;*/
        float: left;
        text-align: left;
        /*padding-right: 6px;*/
        /*padding-left: 6px;*/
    }
    #home-detail .info .spec:first-child {
        paddingl-left: 0;
    }
    #property-details-container .details-description-border {
        padding-bottom: 0;
    }
    .info .spec:not(:first-child)::before {
        height:8px;
        bottom: 0;
        left: 0;
    }
    #home-detail .info{
        margin-bottom: 4px;
        text-align: center;
    }
    #HomeDetailsImageSlider .owl-item{
        height:325px !important;
    }
    #HomeDetailsImageSlider .product-image img{
        height:325px !important;
    }

    .clearfix:before,
    .clearfix:after {
        content: "";
        display: table;
    }
    .clearfix:after {
        clear: both;
    }
    .clearfix {
        zoom: 1; /* For IE 6/7 (trigger hasLayout) */
    }
    .dual-map{
        float:none;
        width:90%;
    }
    #home-detail .feature_list li:not(:first-of-type) {
        margin-left: 0;
    }
    .spec ul li{
        font-size: 14px;
    }
    .submit{
        width:98%;
        margin:8px 0;
    }
}
@media (max-width: 450px) {
    #home-detail .info .spec{
        font-size: 14px;
        float: left;
        text-align: left;
        padding-right: 6px;
        padding-left: 6px;
    }
}
#featureSectionDescription div.short {
    overflow: hidden;
    max-height: 64px;
}
#video-form .request_showing_form_button{
    width:100%;
}
.feature-section{
    padding-top:60px;
    padding-bottom:60px;
}
.feature-section h3{
    font-size:24px;
    padding-bottom: 24px;
}
.feature-section li{
    list-style: none;
}
.feature-section li ul{
    margin-bottom: 8px;
}
.feature-section li ul li{
    margin-left: 8px;;
}
.feature-section li ul li::before{
    font-family: FontAwesome;
    content: "\f105 \00a0";
}
.feature-section a.show-more{
    font-size: 16px;
    width:124px;
}
.feature-section a.show-more i{
    margin-left: 8px;
}
.feature-section .show-more

#SimilarProductSlider{
    position: relative;
}
#SimilarProductSlider .owl-buttons{
    position: relative;
}
#SimilarProductSlider .owl-buttons .owl-prev {
    position:absolute;
    top:-244px;
    left:6px;
    font-size: 72px;
    padding: 12px;
    border-radius:0;
}
#SimilarProductSlider .owl-buttons .owl-next {
    position:absolute;
    top:-244px;
    right:16px;
    font-size: 72px;
    padding: 12px;
    border-radius:0;

}
#home-detail .row{
    margin: 0;
}
#HomeDetailsImageSlider {
    position: relative;
    margin-top: 100px;
}

#HomeDetailsImageSlider .product {
    background: #F3F3F3;
    color: #333;
    margin: 0;
}

#HomeDetailsImageSlider .product a:hover {
    text-decoration: none;
    color: #333;
}
#HomeDetailsImageSlider .owl-item {
    height:400px;
}

#HomeDetailsImageSlider .product-image img {
    width:600px;
    margin: 2px;
    max-height: 400px;
    height: 400px;
}
#HomeDetailsImageSlider .item {
    padding: 0;
    float: left;
    margin: 0;
}
#HomeDetailsImageSlider .owl-buttons {
    position: relative;
    border-radius:0;
}
#HomeDetailsImageSlider .owl-buttons .owl-prev, #HomeDetailsImageSlider .owl-buttons .owl-next {
    position:absolute;
    top:-272px;
    font-size: 72px;
    padding: 12px;
    border-radius:0;
    margin: 0;
}
#HomeDetailsImageSlider .owl-buttons .owl-prev {
    left:0;
}
#HomeDetailsImageSlider .owl-buttons .owl-next {
    right:0;
}
.owl-theme .owl-controls .owl-buttons div{
    background: #000 !important;
    opacity: .5;
}
.owl-theme .owl-controls.clickable .owl-buttons div:hover {
    opacity: .7 !important;
}
.owl-theme .owl-controls .owl-page span{
    background-color:#666;
}
.owl-theme .owl-controls.clickable .owl-buttons div:hover{
    opacity: .7;
}
.owl-theme .owl-controls .owl-page span{
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0;
}
@media (max-width: 992px) {
    .owl-pagination{
        display: none;
    }
    .owl-buttons .owl-next,
    .owl-buttons .owl-prev{
        top: -182px !important;
        font-size: 50px !important;
        margin: 0 !important;
        padding: 3px 8px !important;
    }
    .hidden-data{
        display:none;
    }
}
@media (max-width: 768px) {
    #home-detail .details-description .spec{
        font-size: 25px;
    }
    #HomeDetailsImageSlider .owl-item{
        height:257px !important;
    }
    #HomeDetailsImageSlider .product-image img{
        height:257px !important;
    }
    .owl-buttons .owl-next, .owl-buttons .owl-prev {
        top: -172px !important;
    }
}
/*@media (max-width: 767px) {*/
    /*.details-description {*/
        /*width: 50%;*/
    /*}*/
/*}*/
@media (max-width: 768px) {
    .ctabuttons a{
        display: none;
    }
    .ctabuttons{
        border-top: 1px solid rgba(0,0,0,0.1);
        display:block;
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        background: #ffffff;
        z-index: 999;
    }
    .ctabuttons ul li:last-of-type {
        text-align: center;
    }
    #HomeDetailsImageSlider .owl-item {
        height: 240px !important;
    }
    .details-description {
        margin-top: 15px;
    }
}
.ctabuttons {
    display: inline-block;
    float: right;
}
.ctabuttons a:hover {
    text-decoration: none;
    color: #C20000;
}
.ctabuttons li a{
    font-size: 16px;
    color: #777;
}
.ctabuttons li a.add-fav{
    position: relative;
    top: 0;
}
.ctabuttons li .add-fav i{
    font-size: 20px;
}
.ctabuttons li .add-fav:not(.active) i{
    color: #777;
    text-shadow: none;
    position: relative;
}
.ctabuttons i {
    margin:8px 8px 8px 4px;
    font-size: 16px;
}
.ctabuttons ul li .submit{
    min-width: 248px;
    padding: 16px 32px;
    font-size: 18px;
    border-color: #000;
    background-color: #000;
    color: #fff;
    display: inline-block;
    border: 1px solid #000;
    border-radius: 0;
    margin-top: 12px;;
}
.ctabuttons a:hover i,
.ctabuttons li a.active i,
.ctabuttons li a.add-fav:hover i {
    color: #C20000;
    border-color: transparent;
}
.ctabuttons a.add-fav i {
    top: 3px;
    position: relative;
}
.add-fav.active i{
    font-size: 19px;
}
.add-fav:not(.active) i {
    text-shadow: none;
    color:#777;
}
.map-full-screen{
    left:0;
    right:0;
    z-index:500;
    margin: 12px auto;
}
.map i{
    color: rgb(255, 255, 255);
    font-size: 3em;
    margin: 4px;
    float: right;
    cursor: pointer;
    z-index:20;
    text-shadow: #000 1px 1px 0;
}
.map i:hover{
    color:#C20000;
}
#map-subject-pano{
    border-left: 1px solid #ffffff;
}
.fancybox-opened .fancybox-skin {
    border-radius: 0;
}
CSS;

Yii::app()->clientScript->registerCss('homeDetailsCss',$script);

if($viewsMaxReached){
	$maxedViews = 1;
}else{
	$maxedViews = 0;
}

$script = <<<JS
var CLIENTMAXVIEWSREACHED = {$maxedViews};
$("#HomeDetailsImageSlider").owlCarousel({
    navigation: true,
    autoWidth:true,
    margin: 2,
    scrollPerPage: true,
    itemsCustom : [
        [0, 1],
        [450, 1],
        [600, 2],
        [700, 2],
        [1000, 2],
        [1200, 3],
        [1400, 3],
        [1600, 3]
    ]

});
$( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
$( ".owl-next").html('<i class="fa fa-angle-right"></i>');

$("#HomeDetailsImageSlider .fancybox").fancybox({
    beforeShow: function () {
        if(screen.width < 640) {console.log(screen.width); console.log(this.width);
            var sizePercent = (screen.width / this.width);  console.log(sizePercent);
            this.width = this.width * sizePercent;  console.log(this.width);
            this.height = this.height * sizePercent;  console.log(this.height);
            $.fancybox.update();
        }
    },
    type        : 'image',
    minWidth    : (screen.width > 640) ? 640 : null,
    padding    : (screen.width > 640) ? 10 : 1,
    prevEffect  : 'none',
    nextEffect  : 'none',
    openEffect	: 'none',
    closeEffect	: 'none'
});

$(document).ready(function(){

    var panorama = new google.maps.StreetViewPanorama(document.getElementById("map-subject-pano"));
    var directionsService = new google.maps.DirectionsService();
    var sv = new google.maps.StreetViewService();
    var geocoder = new google.maps.Geocoder();
    var myLatLng;

    geocoder.geocode( { 'address': "{$streetAddress}, {$cityStZip}"}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            myLatLng = results[0].geometry.location;
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });

            // create the regular map view
            var infowindowSubject = new google.maps.InfoWindow();
            var mapSubject = new google.maps.Map(document.getElementById('map-tab'), {
                zoom: 40,
                disableDefaultUI: true,
                scrollwheel: false,
                zoomControl: true,
                //center: new google.maps.LatLng({$subjectMapCenter}),
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.SATELLITE
            });
            var markerSubject = new google.maps.Marker({
                // Supply map and position params as usual.
                //position: new google.maps.LatLng({$subjectMarkerCenter}),
                position: results[0].geometry.location,
                icon: 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png',
                map: mapSubject,
                title: '{$streetAddress}'
            });
            infowindowSubject.setContent('<div class="subjectAddress"><strong>{$streetAddress}</strong><br>{$cityStZip}</div>{$specs}');
            infowindowSubject.open(mapSubject, markerSubject);

            // create the panoramic map view
            // find a Streetview location on the road
            var request = {
                origin: "{$streetAddress}, {$cityStZip}",
                destination: "{$streetAddress}, {$cityStZip}",
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, directionsCallback);

        } else {
            $('#map-tab, #map-subject-pano').hide();
        }
    });

//    var panorama = new google.maps.StreetViewPanorama(document.getElementById("map-subject-pano"));
//    var sv = new google.maps.StreetViewService();
    //sv.getPanoramaByLocation(myLatLng, 50, processSVData);
    //var myLatLng = new google.maps.LatLng({$subjectCenter});

//----------------------------------------------
//    var directionsService = new google.maps.DirectionsService();
//    var geocoder = new google.maps.Geocoder();
//    var address = '{$geoAddress}';
//    var myLatLng;
//
//    geocoder.geocode({
//        'address': address
//    }, function(results, status) {
//        if (status == google.maps.GeocoderStatus.OK) {
//            myLatLng = results[0].geometry.location;
//
//            // find a Streetview location on the road
//            var request = {
//                origin: address,
//                destination: address,
//                travelMode: google.maps.DirectionsTravelMode.DRIVING
//            };
//            directionsService.route(request, directionsCallback);
//        } else {
//            alert("Geocode was not successful for the following reason: " + status);
//        }
//    });

    function directionsCallback(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            var latlng = response.routes[0].legs[0].start_location;
            sv.getPanoramaByLocation(latlng, 50, processSVData);
        } else {
            alert("Directions service not successful for the following reason:" + status);
        }
    }
//----------------------------------------------

    // function for street view data
    function processSVData(data, status)
    {
        if (status == google.maps.StreetViewStatus.OK) {
            var markerPano = new google.maps.Marker({
                position: data.location.latLng,
                draggable: true,
                map: map,
                title: '{$streetAddress}'
            });

            panorama.setPano(data.location.pano);

            var heading = google.maps.geometry.spherical.computeHeading(data.location.latLng, myLatLng);
            panorama.setPov({
                heading: heading,
                pitch: 0,
                zoom: 1
            });
            panorama.setVisible(true);
            google.maps.event.addListener(markerPano, 'click', function() {
                var markerPanoID = data.location.pano;
                // Set the Pano to use the passed panoID
                panorama.setPano(markerPanoID);
                panorama.setPov({
                    heading: 270,
                    pitch: 0,
                    zoom: 1
                });
                panorama.setVisible(true);
            });
            panorama.setOptions({disableDefaultUI: true, zoom: 2});
            $('#map-subject-pano').append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\"map-subject-pano\")'></i>");
            $('#map-tab').append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\"map-tab\")'></i>");
        }
        else {
            $('#map-tab').removeClass('dual-map');
            $('#map-subject-pano').hide();
            var r = google.maps.event.trigger(mapSubject, "resize");
            mapSubject.setCenter(new google.maps.LatLng({$subjectMapCenter}));
            $('#map-tab').append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\"map-tab\")'></i>");
            return;
        }
    }
});
var map = new google.maps.Map(document.getElementById('map-tab'), {
    zoom: 13,
    scrollwheel: false,
    disableDefaultUI: true,
    center: new google.maps.LatLng($center),
    mapTypeId: google.maps.MapTypeId.SATELLITE
});

function stmMapFullScreen(id){
    $('#'+id+" i").remove();
    if( $("#"+id).hasClass("map-full-screen") ){
        $("#"+id).removeClass("map-full-screen");
        $('#'+id).css({ "position": "relative","height": " ", "width": " ", "top":" " });
        $('#'+id).attr("style","min-height: 240px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;");
        google.maps.event.trigger(document.getElementById(id), "resize");
        $('#'+id).append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\""+id+"\")'></i>");
        return;

    }
    $("#"+id).addClass("map-full-screen");


    var tm = parseInt($(".navbar-fixed-top:first").innerHeight());
    var h = parseInt( ( $(window).height() - tm ) - 50);
    var w = parseInt($(window).width() - 50);

    $('#'+id).css({ "position": "fixed","height": h, "width": w, "top":tm });
    $('#'+id+" i").removeClass("fa-expand");
    $('#'+id+" i").addClass("fa-close");
    google.maps.event.trigger(document.getElementById(id), "resize");
    $('#'+id).append("<i class='fa fa-close' title='Shrink Map' onclick='stmMapFullScreen(\""+id+"\")'></i>");

}
$('.cta-button-tip').tipsy({ gravity: 's', title: 'data-tooltip',offset : 100});
$('#print-flyer-button').on('click', printFlyer);
function printFlyer() {
    window.open('/printFlyer/{$listingId}', 'Home Details Flyer', 'width=785,height=700,scrollbars=no');
}

//http://stackoverflow.com/questions/21978098/item-alignment-within-an-image-carousel
$("#SimilarProductSlider").owlCarousel({
    navigation: true,
    scrollPerPage: true,
    navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>' ]
});

$('#SimilarProductSlider .item .product').responsiveEqualHeightGrid();

var mySwiper = new Swiper('.swiper-container', {
    pagination: '.box-pagination',
    keyboardControl: true,
    paginationClickable: true,
    slidesPerView: 'auto',
    autoResize: true,
    resizeReInit: true
});

var calcLoaded = false;
var calcOpen = false;

function calculatorUpdateFields(){
    var downPayment = $('#down-payment').val();
    downPayment = downPayment.replace('%','');
    var percent = downPayment;
    percent = (1-(percent /100));

    var loanAmount = $('#price').val();
    loanAmount = loanAmount.replace('$','');
    loanAmount = loanAmount.replace(/,/g,'');
    loanAmount = loanAmount * percent;
    var interest = $('#interest').val();
    interest = interest.replace('%','');
    interest = interest /100/12;
    var months = $('#years').val();

    var payment = Math.round((loanAmount*interest)/(1-Math.pow(1+interest,(-1*months)))*100)/100;

    $('#monthly-payment').html('$'+payment.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

    var totalMortgage = (payment * months);

    $("#mortgage-amount").html("$"+totalMortgage.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

    var price = $("#price").val();
    price = price.replace('$','');
    price = price.replace(/,/g,'');
    downPayment = (parseFloat(downPayment) / 100);
    downPayment = (parseFloat(price) * parseFloat(downPayment));

    $("#down-payment-amount").html("$"+downPayment.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
}
function calculatorToggle(){
    if(calcOpen == true) {
        stmHomeDetailsScrollTo("#home-detail");
        $("#mortgage-calculator-tab").hide();
        calcOpen = false;
        return ;
    }
    $("#mortgage-calculator-tab").show();
    calcOpen = true;
    stmHomeDetailsScrollTo("#mortgage-calculator-tab");
    if(calcLoaded) {
        return;
    }
    $.get( "/mortgagecalculator/$model->price", function( data ) {
        $("#mortgage-calculator-tab").html( data );
        calcLoaded = true;
        calculatorUpdateFields();
        $('#mortgage-calcualtor-fields input, #mortgage-calcualtor-fields select').on('change',function() {
            calculatorUpdateFields();
        });
    });
}
$("#mortgageCalcToggle").on("click",function(e){
    e.preventDefault();
    return calculatorToggle();
});
$("#map-fav-link").on("click",function(e){
    e.preventDefault();
    return stmHomeDetailsScrollTo("#map-tab");
});
$(".feature-section .show-more").on("click",function(){
    var element = "#"+$(this).closest(".feature-section").attr("id");
    if ( element == "#featureSectionDescription") {
        $(element+" .content").toggleClass( "short" );
        if( $(element+" .content").hasClass("short") ){
            $(this).html("Show More <i class=\"fa  fa-angle-down\"></i>");
        } else {
            $(this).html("Show Less <i class=\"fa  fa-angle-up\"></i>");
        }
        return;
    }
    element = element +" .hidden-data";
    $(element).toggle();
    if( $(element).is(":visible") ){
        $(this).html("Show Less   <i class=\"fa  fa-angle-up\"></i>");
    } else {
        $(this).html("Show More <i class=\"fa  fa-angle-down\"></i>");
    }
});

$(".scrollTo").on("click",function(e){
    var t = $(this).attr("href");
    if(t){
        e.preventDefault();
        stmHomeDetailsScrollTo(t);
    }

})


function stmHomeDetailsScrollTo(id){
    var tm = parseInt($(".navbar-fixed-top:first").innerHeight());
    $("html,body").animate(
        { scrollTop: parseInt( $(id).offset().top - tm - 48 )}
    );
}

$( "form .ajaxSubmit" ).on("click", function( event ) {
    event.preventDefault();
    $("body").prepend("<div class='loading-container loading'><em></em></div>");
    var element_id = $(this).closest('form').attr('id');
    $("#"+element_id+" input, #"+element_id+" textarea").removeClass("error");
    $("#"+element_id+" input").removeClass("error");
    $.post(
        $("#"+element_id).attr("action"),
        $( "#"+element_id ).serializeArray(),
        function(data) {
            if(!data || data == ""){
                // @todo need to add some kind of confirmation for the user jamesk may 6 2016
                location.reload();
            } else {
                $(".loading-container").removeClass("loading");
                $.each(data, function( index, value ) {
                    $("#"+element_id+" #"+index).addClass("error");
                });
            }
        }
    );
});

JS;
	Yii::app()->clientScript->registerScript('homeDetailsJs',$script,CClientScript::POS_END);

?>