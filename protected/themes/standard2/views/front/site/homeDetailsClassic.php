<?php
Yii::app()->clientScript->registerCssFile($this->module->cdnUrl . 'assets/css/idangerous.swiper.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
//Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.'.((YII_DEBUG) ? 'com': 'local').'/assets/js/owlcarousel/2.0.0/owl.carousel.css');
$contactId = Yii::app()->user->id;
$listingId = Yii::app()->request->getQuery('id');

$addRemoveClass = ($isFavorite) ? 'remove-favorites' : 'add-favorites';
$addRemoveButtonClass = ($isFavorite) ? 'fa fa-remove' : 'fa fa-star';
$addRemoveText = ($isFavorite) ? 'Remove Favorite' : 'Add Favorite';

$photoContent['thumbnails'] = json_decode($photoContent['thumbnails']);

$curentSharePage = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
$formattedAddress = $model->formatMlsAddress($model);
Yii::app()->controller->fullWidth = true;

//echo "<pre>".print_r($photoContent,1)."<pre>";
//echo $this->renderPartial(
//	'homeDetails/_topSlider', array(
//	'photoContent' => $photoContent,
//));
?>
	<div class="container headerOffset">
		<div class="row transitionfx" id="home-detail">

			<!-- left column -->
			<div class="col-lg-6 col-md-6 col-sm-6">
				<!-- product Image and Zoom -->
				<div class="main-image sp-wrap col-lg-12 no-padding">
					<div class="banner">
						<div class="full-container">
							<div class="slider-content">
                            <span id="main-image-prevControl" class="prevControl sliderControl">
								<i class="fa fa-angle-left fa-3x "></i>
							</span>
                            <span id="main-image-nextControl" class="nextControl sliderControl">
								<i class="fa fa-angle-right fa-3x "></i>
							</span>
								<div class="sp-large"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ left column end -->
			<!-- right column -->
			<div id="property-details-container" class="col-lg-6 col-md-6 col-sm-6">
				<div class="ctabuttons">
					<a data-placement="right" class="add-fav<?=($isFavorite) ? " active" : "";?>" data-listingid="<?=$listingId?>" data-mbid="<?=$mlsBoardId?>" data-contact-id="<?=$contactId; ?>">
						<i class="glyphicon glyphicon-heart"></i>
					</a>
					<a id="print-flyer-button" class="cta-button-tip" data-tooltip="Print Flyer"><i class="fa fa-print"></i></a>
					<a id="ask-question-button" class="cta-button-tip" data-tooltip="Ask a Question"><i class="fa fa-comment"></i>Ask a Question</a>
					<!--                <a id="send-to-friend-button" class="cta-button-tip" data-tooltip="Send to Friend"><i class="fa fa-envelope-o"></i> Send to Friend</a>-->
				</div>


				<div class="price">
                <span class="price-sales">
                    <?php echo (strpos($_SERVER['SERVER_NAME'],'findhomesinpdx') !== false)? $model['contingent'] : Yii::app()->format->formatDollars($model['price']); ?>
                </span>
				</div>
				<h1 class="address"><?php echo $model->formatMlsAddress($model, $opt = array('lineBreak' => true)) ?></span></h1>


				<?php echo ($model['common_subdivision']) ? 'Neighborhood: ' . Yii::app()->format->formatProperCase($model['common_subdivision']) . '<br /><br />' : ''; ?>


				<div class="details-description clearfix">
					<div class="pull-left">
                <span class="spec">
                    <?php echo ((Yii::app()->user->getBoard()->is_sq_ft_range) ? $model['sq_feet'] : Yii::app()->format->formatNumber($model['sq_feet'])) ?>
                </span> Sq. Feet
						<span class="spec"><?php echo $model['bedrooms'] ?></span> Beds &nbsp; <span class="spec"><?php echo $model['baths_full'] ?></span> Baths &nbsp;
						<?php if ($model['baths_half']) {
							echo '<span class="spec">' . $model['baths_half'] . '</span> Half Bath';
						} ?>
					</div>

					<div class="col-lg-12 no-padding" style="margin-top: 40px;">
						<a href="#showing-request-form" class="submit btn btn-primary request-showing-button">Request Showing</a>
					</div>
				</div>
			</div>

			<div class="thumbnail-container col-lg-12 col-xs-12">
				<div class="productThumb clearfix" style="width: <?=(count($photoContent['thumbnails'] ) * 74)?>px; height: 74px;">
					<div class="sp-thumbs sp-tb-active">
						<?php
						$large_star_suffix = ($isFavorite) ? '' : '_empty';
						$addRemoveClass = ($isFavorite) ? 'remove-favorites' : 'add-favorites';

						foreach($photoContent['thumbnails'] as $i => $photoUrl):?>
							<?php if($i == 0):?>
								<a href="<?=$photoContent['fullSizeUrl'][$i];?>" class="sp-current"><img src="<?= $photoUrl; ?>" class="img-responsive" alt="img"></a>
							<?php else: ?>
								<a href="<?=$photoContent['fullSizeUrl'][$i];?>"><img src="<?= $photoUrl; ?>" class="img-responsive" alt="img"></a>
							<?php endif; ?>
						<?php endforeach; ?>


					</div>
				</div>
				<!--/.productThumb-->
			</div>

			<!--/ right column end -->

			<div class="w100 clearfix">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<?php if (!empty($model->public_remarks)): ?>
						<h3>Property Description</h3>
						<hr style="margin: 0 0 5px 0;">
						<div class="justify">
							<?php echo $model->public_remarks; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>

			<div class="product-tab w100 clearfix">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#features" data-toggle="tab">Features</a></li>
						<li><a href="#schools" data-toggle="tab">Schools</a></li>
						<li><a href="#mortgage-calculator-tab" data-toggle="tab" id="calc-tab-link">Mortgage Calculator</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content" id="home-property-widget">
						<div class="tab-pane active" id="features">
							<div class="features-container col-lg-4 col-md-4 col-sm-4">
								<h3>Exterior Features</h3>
								<ul class='feature_list'>
									<?php
									if (isset($exteriorFeatures)) {
										ksort($exteriorFeatures);
										foreach($exteriorFeatures as $label=>$value) {
											echo '<li><label>',$label,'</label>',  str_replace(',','<br />',$value),'</li>';
										}
									}
									?>
								</ul>
							</div>

							<div class="features-container col-lg-4 col-md-4 col-sm-4">
								<h3>Interior Features</h3>
								<ul class='feature_list'>
									<?php
									if (isset($interiorFeatures)) {
										ksort($interiorFeatures);
										foreach($interiorFeatures as $label=>$value) {
											echo '<li><label>',$label,'</label>', str_replace(',','<br />',$value),'</li>';
										}
									}
									?>
								</ul>
							</div>

							<div class="features-container col-lg-4 col-md-4 col-sm-4">
								<h3>Property Features</h3>
								<ul class='feature_list'>
									<?php
									if (isset($propertyFeatures)) {
										ksort($propertyFeatures);
										foreach($propertyFeatures as $label=>$value) {
											echo '<li><label>',$label,'</label>', str_replace(',','<br />',$value),'</li>';
										}
									}
									?>
								</ul>
							</div>
							<div style="clear:both;"></div>
						</div>
						<div class="tab-pane" id="schools">
							<div>
								<h3>School Information</h3>
								<ul class='feature_list'>
									<?php
									if (isset($schoolFeatures)) {
										ksort($schoolFeatures);
										foreach($schoolFeatures as $label=>$value) {
											echo '<li><label>',$label,'</label>',  $value,'</li>';
										}
									} else {
										echo 'No data found...';
									}
									?>
								</ul>
							</div>
							<div style="clear:both;"></div>
						</div>
						<div class="tab-pane" id="mortgage-calculator-tab"></div>
					</div>

					<!-- /.tab content -->
				</div>
			</div>
			<hr/>
			<div class="listing-office">
				Courtesty of <?=ucwords(strtolower($model->getListingOfficeName()))?>
			</div>
		</div>
		<a name="showing-request-form"></a>
		<div class="form-container col-lg-12 pull-left">
			<div id="form" class="form-inline">
				<?php
				$formId = Forms::FORM_SHOWING_REQUEST;
				$SubmissionValues = new FormSubmissionValues($formId);
				$FormFields = new FormFields;

				if (!Yii::app()->user->isGuest) {

					$SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;
					$SubmissionValues->data[$FormFields->getField('last_name')->id] = Yii::app()->user->lastName;
					$SubmissionValues->data[$FormFields->getField('email')->id] = Yii::app()->user->contact->getPrimaryEmail();
					$SubmissionValues->data[$FormFields->getField('phone')->id] = Yii::app()->user->contact->getPrimaryPhone();
				}

				$form = $this->beginWidget('CActiveForm', array(
					'id' => 'video-form',
					'action' => array('/front/forms/showing/formId/' . $formId),
					'enableAjaxValidation' => true,
					'enableClientValidation' => false,
					'clientOptions' => array(
						'validateOnChange' => false,
						'validateOnSubmit' => true,
						'beforeValidate' => 'js:function(form) {
									$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
									return true;
								}',
						'afterValidate' => 'js:function(form, data, hasErrors) {
								$("div.loading-container.loading").remove();
								if (!hasErrors) {
									Message.create("success", "Your Showing Request was sent successfully!");
									$("div#form").addClass("hidden");
									$("div#thank-you-message").hide().removeClass("hidden").show();
								}

								return false;
							}',
					)
				));
				?>

				<h2>Request a Showing</h2>
				<div class="form-group col-sm-6 col-xs-12 no-padding">
					<?php
					echo $form->textArea($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']', $htmlOptions = array(
						'placeholder' => "When would you like to see this home?", 'class' => 'col-xs-12', 'rows'=>4
					));
					?>
					<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']'); ?>
					<div class="form-group col-xs-12 no-padding">
						<div class="col-sm-6 col-xs-6 no-padding">
							<?php
							echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array(
								'placeholder' => 'First Name', 'class' => 'col-xs-12',
							));
							?>
							<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
						</div>
						<div class="col-sm-6 col-xs-6 no-padding">
							<?php
							echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array(
								'placeholder' => 'Last Name', 'class' => 'col-xs-12',
							));
							?>
							<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
						</div>
					</div>

					<div class="form-group col-xs-12 no-padding">
						<div class="col-sm-6 col-xs-6 no-padding">
							<?php
							echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array(
								'placeholder' => 'E-mail', 'class' => 'col-xs-12',
							));
							?>
							<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
						</div>
						<div class="col-sm-6 col-xs-6 no-padding">
							<?php
							$this->widget('StmMaskedTextField', array(
								'model' => $SubmissionValues,
								'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
								'mask' => '(999) 999-9999',
								'id' => 'FormSubmissionValues_data_4',
								'htmlOptions' => array('class' => 'col-xs-12','placeholder'=>'Phone'),
							));
							?>
							<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
							<?php echo CHtml::hiddenField('url', $_SERVER['REQUEST_URI']); ?>
						</div>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::SOURCE_DESCRIPTION_ID.']', $htmlOptions=array('value'=>'Showing Request'));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$model->listing_id));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$model->getPhotoUrl(1)));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('home_url') .']', $htmlOptions=array('value'=>$model->getUrl()));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$model->price));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$model->bedrooms));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$model->baths_total));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$model->sq_feet));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->streetAddress)))));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->city)))));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->state)))));?>
						<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$model->zip));?>
					</div>
					<div class="col-xs-12 no-padding">
						<button id="submit-button" type="submit" class="submit btn btn-primary">Send Request</button>
					</div>
				</div>
				<div class="form-group col-sm-6 col-xs-12 form-contact-container">
					<div class="col-xs-12">
						<h3>
							<?
							if($leadRoute = LeadRoutes::model()->findByAttributes(array('component_type_id'=>ComponentTypes::BUYERS))) {
								$agents = $leadRoute->getActingAgents();
								if($settingValue = SettingContactValues::model()->findByAttributes(array('setting_id'=>Settings::SETTING_PROFILE_PHOTO_ID, 'contact_id'=>$agents[0]->id))) {
									$profileUrl = Yii::app()->user->getProfileImageBaseUrl().'/'.$agents[0]->id.'/'.trim($settingValue->value);
								}
							}
							?>
							<div class="profile-photo-container">
								<?
								if($profileUrl):
									?>
									<img class="profile" src="<?=$profileUrl?>"/><br>
									<?
								endif;?>
							</div>
							<?
							$agentName = $agents[0]->fullName;
							?>
							<?=$agentName?>
						</h3>
					</div>
					<div class="col-xs-12">
						<label>Phone:</label>
						<span><?=Yii::app()->user->settings->office_phone?></span>
					</div>
					<div class="col-xs-12 office-name">
						<span><?=Yii::app()->user->settings->office_name?></span>
					</div>
				</div>
				<?php $this->endWidget(); ?>

			</div>

			<div id="thank-you-message" class="hidden">
				<img src="<?=$model->getPhotoUrl(1)?>" alt=""/>
				<h3>Thank You!</h3>
				We received your Showing Request. <br /><br />

				Feel free to call <strong><?php echo Yii::app()->user->settings->office_phone; ?></strong> for immediate assistance. <br /><br />

				Look forward to seeing you soon!<br><br>

				<h3 class="agent-name"><?=$agentName?></h3>
			</div>
		</div>
		<div class="row recommended col-lg-12" id="map-tab">

		</div>

		<div class="row recommended col-lg-12">

			<h1> Similar Properties (<?= count($similarProperties); ?>) </h1>

			<div id="SimilarProductSlider"> <?php /*style="height: 270px; overflow-x: scroll;"*/ ?>
				<?php /*<div style="width: <?=(count($similarProperties) * 250)?>px;">*/ ?>
				<?php foreach ($similarProperties as $similarProperty):
					if(empty($similarProperty['photoUrl'])){
						$similarProperty['photoUrl'] = "http://cdn.seizethemarket.com/assets/images/default-placeholder.png";
					}
					?>
					<div class="item">
						<div class="product">
							<a class="product-image" href="<?= $similarProperty['propertyUrl']; ?>">
								<img src="<?= $similarProperty['photoUrl']; ?>">
								<div class="description">
									<div class="price">
										<span><?= Yii::app()->format->formatDollars($similarProperty['price']); ?></span>
									</div>
									<div class="price">
                                    <span>
                                        <?=($similarProperty['bedrooms']) ? $similarProperty['bedrooms']." Bed" : ""; ?>
										<?=($similarProperty['baths_total']) ? " | ".$similarProperty['baths_total']." Bath" : ""; ?>
                                    </span>
									</div>
								</div>
							</a>
						</div>
					</div>
				<?php endforeach; ?>
				<?php /*</div>*/ ?>
			</div>
			<!--/.recommended-->
		</div>
	</div>
	<div style="clear:both"></div>


	<div id="mls-disclosure" style="color: #666; font-size: 11px; line-height: 1.3;">
		<?php $this->renderPartial('_mlsDisclosure'); ?>
	</div>

<?php
$dialogWidget = 'front_widgets.DialogWidget.HomeDetailsDialogWidget.HomeDetailsDialogWidget';
$isGuest = Yii::app()->user->isGuest;
$theme = Yii::app()->theme->name;
//@todo: temporary speed boost - get with Chris for better implementation
//$tablePrefix = Yii::app()->user->board->prefixedName;
//$listingPhotoTableName = $tablePrefix . MlsPropertiesPhoto::getTableSuffix();
//$query = "select url from ".$listingPhotoTableName." where listing_id='{$model->listing_id}' ORDER BY photo_number ASC limit 1";
//$photoUrl = Yii::app()->stm_mls->createCommand("$query")->queryScalar();

$propertyDetails = $model->getHomeDetails($model->listing_id);
$photoUrls = explode(',', $propertyDetails['photoUrls']);
$photoUrl = $photoUrls[0];

//@todo: temp quick fix for non-photo url
if(!Yii::app()->user->board->has_photo_url) {
	$photoUrl = $model->getPhotoUrl(1);
}

if ($viewsMaxReached) {
	//$cssAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('front_widgets.DialogWidget.HomeDetailsDialogWidget.assets.css'), false, -1, FrontModule::REFRESH_CSS_ASSETS);
	//Yii::app()->clientScript->registerCssFile($cssAssetsUrl.DS.'homeDetails.css');
	/** $this->widget('front_widgets.DialogWidget.RegisterDialogWidget.RegisterDialogWidget', array(
	'id'       => 'register-dialog',
	'property' => $model,
	'photoUrl' => $photoUrl,
	'title'    => 'Get Access Now!', // View Photos, Videos & Maps!
	));
	 */
} else {
	// Make an Offer Dialog Widget
//    $this->widget($dialogWidget, array(
//            'id'            => 'make-an-offer-dialog',
//            'type'          => 'make-an-offer',
//            'property'      => $model,
//            'photoUrl'      => $photoUrl,
//            'isGuest'       => $isGuest,
//            'title'         => 'Make an Offer',
//            'triggerElement'=> '#make-an-offer-button',
//            'formView'      => '_makeAnOffer',
//			'theme' => $theme
//        ));

	// Send to Friend Dialog Widget
//    $this->widget($dialogWidget, array(
//        'id'            => 'send-to-friend-dialog',
//        'type'          => 'send-to-friend',
//        'property'      => $model,
//        'photoUrl'      => $photoUrl,
//        'isGuest'       => $isGuest,
//        'title'         => 'Send to a Friend',
//        'triggerElement'=> '#send-to-friend-button',
//        'formView'      => '_sendToFriend',
//		'theme' => $theme
//    ));

	// Showing Request Dialog Widget
//    $this->widget($dialogWidget, array(
//        'id'             => 'showing-request-dialog',
//        'type'           => 'showing-request',
//        'property'       => $model,
//        'photoUrl'       => $photoUrl,
//        'isGuest'       => $isGuest,
//        'title'          => 'Showing Request',
//        'triggerElement' => '#showing-request-button',
//        'formView'       => '_showing',
//		'theme' => $theme
//    ));

	// Ask Question Dialog Widget
	$this->widget($dialogWidget, array(
		'id'             => 'ask-question-dialog',
		'type'           => 'ask-question',
		'property'       => $model,
		'photoUrl'       => $photoUrl,
		'isGuest'        => $isGuest,
		'title'          => 'Ask a Question',
		'triggerElement' => '#ask-question-button',
		'formView'       => '_askQuestion',
		'theme' => $theme
	));

	// Property Tax Dialog Widget
//    $this->widget($dialogWidget, array(
//        'id'             => 'property-tax-dialog',
//        'type'           => 'property-tax',
//        'property'       => $model,
//        'photoUrl'       => $photoUrl,
//        'isGuest'        => $isGuest,
//        'title'          => 'Property Tax Info Request',
//        'triggerElement' => '#property-tax-button',
//        'formView'       => '_propertyTax',
//		'theme' => $theme
//    ));
}

Yii::app()->clientScript->registerCss("homeDetailsCss",<<<CSS
.row.cabuttons a.btn {
    margin: 0;
}
.banner {
    margin-top: 0;
}
#home-detail {
    margin-top: 10px;
}
#home-detail .spec {
    font-size: 19px;
    font-weight: bold;
    color: #333;
    padding-left: 10px;
}
#home-detail #home-property-widget h3 {
    padding: 25px 0 0 35px;
}
#home-detail .feature_list label {
    display: block;
    border-bottom: 1px solid #ccc;
    font-weight: bold;
    line-height: 18px;
}
#home-detail .feature_list {
    list-style: none;
    margin-top:20px;
}
#home-detail .feature_list li{
    float:left;
    margin-left:35px;
    width:210px;
    margin-bottom: 20px;
    font-size: 13px;
}
#home-detail .feature_list label{
    display: block;
    border-bottom:1px solid #ccc;
    font-weight: bold;
    line-height: 18px;
}
#home-detail .sidebar{
    float: right;
    width: 205px;
}
#home-detail .sidebar .side-container{
    width: 205px;
    position: relative;
    float: right;
    margin-top: 15px;
    margin-right: -20px;
    background: #fafafa;
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    padding: 10px 10px 10px 10px;
}
#home-detail .sidebar .request-container{
    background-color: #FFF8E6;
    border: 1px solid #FFB62E;
}
.listing-office {
    font-size: 13px;
    color: #777;
    text-align: right;
}
.sp-wrap {
    /*margin: 0 0 15px 0;*/
    font-size: 12px;
}
.sp-large {
    max-width: 100%;
    /*border: 1px solid #DDDDDD;*/
}
.sp-large a img {
    max-width: 100%;
    width: 100%;
    min-width: 600px;
    cursor: default;
    border: 1px solid #DDDDDD;
}
.sp-thumbs img {
    height: 50px !important;
}

@media (max-width: 992px) {
    .sp-large {
        /*max-width:90%;*/
        border: none;
    }
    .sp-large a img {
        /*max-width: 100%;*/
        width: 100%;
        min-width: 100px;
    }
    .row.cabuttons a.btn {
        width: 100%;
        margin: 2px 0;
    }
}

.productThumb{
    max-height: 200px;
    min-height: 75px;
    overflow: hidden;
    overflow-y: auto;
    margin-bottom: 10px;
}
.product-tab.w100 {
    padding-top: 10px;
    margin-top: 10px;
    border-top: 1px solid #dddddd;
}
/*.product-share {
    margin-top: 0;
    margin-bottom: 10px;
}*/
#SimilarProductSlider {
    position: relative;
}
#SimilarProductSlider .product{
    min-height: 220px;
    max-width: 220px;
    background: #F3F3F3;
    color: #333;
}
#SimilarProductSlider .product a:hover {
    text-decoration: none;
    color: #333;
}
#SimilarProductSlider .description{
    padding-top: 15px;
}
#SimilarProductSlider .item .price {
    text-align: center;
    margin-left: 0px;
}

#SimilarProductSlider .product-image img {
    height: 145px;
}
#home-detail .details-description .spec:first-of-type {
    padding-left: 0;
}

#home-detail #mortgage-calculator-tab.ui-tabs-panel{
    padding: 35px 0 !important;
}

#home-detail .mortgage-calculator-tab-container .i_mortgage_calculator{
    height: 78px;
    width: 69px;
    display: inline-block;
    background-repeat: no-repeat;
    background-image: url('http://cdn.seizethemarket.com/assets/images/calculator_2.png');
    float: left;
    margin-left: -81px;
}

#home-property-widget #features {
    max-height: 300px;
    overflow: auto;
}
@media (max-width: 768px) {
    .headerOffset {
        padding-top: 145px;
    }
    .sp-large {
        border: none;
    }
    .row.cabuttons a.btn {
        width: 100%;
        margin: 5px 0;
    }
}

.home-details-dialog h3{
    font-size: 18px;
}
.home-details-dialog .property-container{
    float: left;
}
.home-details-dialog div.property-image{
    width: 280px;
    vertical-align: top;
    float: left;
}
.home-details-dialog div.property-info{
    width: 245px;
    float: left;
    padding-top: 20px;
    padding-left: 20px;
}
.home-details-dialog .price{
    font-size: 16px;
    color: #3C0;
    font-weight: bold;
    margin: 5px 0;
    float: left;
}
.home-details-dialog .details{
    clear: both;
    font-size: 13px;
    font-weight: normal;
}
.home-details-dialog .dialog-form-container{
    clear: both;
    width: 100%;
    /*padding-top: 15px;*/
}
.home-details-dialog .dialog-form-container table{
    width: 100%;
}
.home-details-dialog .dialog-form-container table th{
    width: 150px;
    text-align: right;
    padding-right: 5px;
}
.home-details-dialog hr{
    margin-left: auto;
    margin-right: auto;
    width: 90%;
}
.form-container .submit, .request-showing-button {
    width: 100%;
    font-size: 30px !important;
}
.home-details-dialog .submit-button-row{
    padding: 23px 15px 15px 15px;
    text-align: center;
    position: relative;
}
.home-details-dialog .home-details-dialog-submit-button{
    text-transform: uppercase;
    font-size: 11px;
    padding-left: 18px;
    padding-right: 18px;
    cursor: pointer;
    width: 200px;
}
.home-details-dialog .home-details-dialog-loading{
    width: 32px;
    height: 32px;
    text-align: center;
    position: absolute;
    margin-left: -15px;
    top: -6px;
    left: 50%;
}
.home-details-dialog .home-details-dialog-loading.loading{
    background: url('http://cdn.seizethemarket.com/assets/images/loading.gif') no-repeat;
    opacity: 1 !important;
    position: relative;
}
.home-details-dialog .dialog-form-container table, .home-details-dialog .dialog-form-container table th, .home-details-dialog .dialog-form-container table td{
    background: none;
    border: none;
}
.ctabuttons {
    margin: 0 0 20px 0;
    display: inline-block;
    float: right;
    width: 100%;
}
.ctabuttons a {
    float: right;
}
.ctabuttons a.btn i {
    margin-right: 8px;
}

.ctabuttons a:hover {
    text-decoration: none;
}
.ctabuttons i.fa {
    padding: 10px;
    font-size: 17px;
    color: #666;
}

.recommended {
    margin-bottom: 10px;
}
#SimilarProductSlider .item {
    padding: 0px;
    float: left;
}
.socialIcon {
    margin-left: 10px;
    float: left;
    padding-top: 10px;
}
.nextControl {
    right: 40px;
}
.prevControl, .nextControl,
.prevControl i, .nextControl i {
    cursor: pointer;
}

.form-container {
    padding: 60px 30px;
    margin: 20px 0;
    background: #EFEFEF;
}
.form-container h2 {
    font-size: 40px;
}
.form-container .submit, .request-showing-button {
    width: 100%;
    font-size: 30px;
}
.form-container .submit:hover, .request-showing-button:hover {
    background-color: #25ad29;
}
.form-contact-container, #thank-you-message {
    font-size: 20px;
}
.form-contact-container h3, #thank-you-message h3{
    font-size: 30px;
}
#thank-you-message img {
    height: 250px;
    float: left;
    margin-right: 30px;
}
.form-contact-container .office-name {
    margin-top: 10px;
}
.form-contact-container .agent-name {
    margin-top: 30px;
}
.form-contact-container .profile-photo-container {
    min-height: 20px;
}
.form-contact-container .profile {
    max-height: 150px;
    margin-bottom: 10px;
}

.thumbnail-container {
    overflow-x: scroll;
}
#property-details-container .price, #property-details-container .address {
    margin-bottom: 25px;
    font-size: 30px;
    line-height: 35px;
}
.ui-widget form input[type="submit"] {
    width: 100%;
    padding: 14px;
    font-size: 14px;
}
.add-fav{
    top:4px !important;
    right:10px !important;
    position: relative !important;
    margin-left: 8px !important;
}
.add-fav.active {
    color: #C20000;
    border-color: transparent;
}
.add-fav:not(.active) i {
    text-shadow: none;
    color: #888;
}

CSS
);

Yii::app()->clientScript->registerScript('homeDetailsJs', <<<JS
//$(".sp-wrap").append('<div class="sp-large"></div>');
//$(".productThumb").append('<div class="sp-thumbs sp-tb-active"></div>');
//$(".sp-wrap a").appendTo(".sp-thumbs");
$(".sp-thumbs a:first").addClass("sp-current").clone().removeClass("sp-current").appendTo(".sp-large");
$(".sp-wrap").css("display", "inline-block");
var slideTiming = 300;
var maxWidth = $(".sp-large img").width();
$(".sp-thumbs").on("click", function (e) {
    e.preventDefault()
});
$(".sp-tb-active a").on("click", function (e) {
    $(".sp-current").removeClass();
    $(".sp-thumbs").removeClass("sp-tb-active");
    $(".sp-thumbs").find('a.sp-current').removeClass("sp-current");
    $(".sp-zoom").remove();
    var t = $(".sp-large").height();
    $(".sp-large").css({height: t + "px"}); //overflow: "hidden",
    $(".sp-large a").remove();
    $(this).addClass("sp-current").clone().hide().removeClass("sp-current").appendTo(".sp-large").fadeIn(slideTiming, function () {
        var e = $(".sp-large img").height();
        $(".sp-large").height(t).animate({height: e}, "fast", function () {
            $(".sp-large").css("height", "auto")
        });
        $(".sp-thumbs").addClass("sp-tb-active")
    });
    e.preventDefault()
});
$(".sp-large a").on("click", function (e) {
    /*var t = $(this).attr("href");
     $(".sp-large").append('<div class="sp-zoom"><img src="' + t + '"/></div>');
     $(".sp-zoom").fadeIn();
     $(".sp-large").css({left: 0, top: 0});

     var largeImage = $(this).find("img").attr('src');
     $(".zoomImage1").attr('src', largeImage);
     $(".zoomImg").attr('src', largeImage);
     $(".gall-item").attr('href', largeImage);*/

    e.preventDefault()
});
$(document).ready(function () {
    $(".sp-large").mousemove(function (e) {
        var t = $(".sp-large").width();
        var n = $(".sp-large").height();
        var r = $(".sp-zoom").width();
        var i = $(".sp-zoom").height();
        var s = $(this).parent().offset();
        var o = e.pageX - s.left;
        var u = e.pageY - s.top;
        var a = Math.floor(o * (t - r) / t);
        var f = Math.floor(u * (n - i) / n);
        $(".sp-zoom").css({left: a, top: f})
    }).mouseout(function () {
    });
});
$(".sp-zoom").on("click", function (e) {
    $(this).fadeOut(function () {
        $(this).remove()
    })
});

$('.cta-button-tip').tipsy({live: true, gravity: 's', title: 'data-tooltip'});

$('.add-favorites').on('click', addFavorites);
$('.remove-favorites').on('click', removeFavorites);

function removeFavorites() {
    $.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: '$listingId', action: 'remove' }, function(data) {
        //results....
    });
    $('#add-favorites-button').html($('#add-favorites-button').html().replace('Remove Favorite','Add Favorite'));
    $('#add-favorites-button').html($('#add-favorites-button').html().replace('fa fa-remove','fa fa-star'));
    $('#add-favorites-button').removeClass('remove-favorites').addClass('add-favorites');
    $('.saved-count').html($('.saved-count').html()-1);
//		$('#favorite-container').html($('#favorite-container').html().replace('fa fa-2x fa-star remove-favorite','fa fa-2x fa-star-o favorite'));
//		$('#favorite-container').removeClass('remove-favorites').addClass('add-favorites');

    $('.add-favorites').unbind('click');
    $('.add-favorites').on('click', addFavorites);
}

function addFavorites() {
    $.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: '$listingId', action: 'add' }, function(data) {
        //results....
    });
    $('#add-favorites-button').html($('#add-favorites-button').html().replace('Add Favorite', 'Remove Favorite'));
    $('#add-favorites-button').html($('#add-favorites-button').html().replace('fa fa-star','fa fa-remove'));
    $('#add-favorites-button').removeClass('add-favorites').addClass('remove-favorites');
    $('.saved-count').html($('.saved-count').html()*1+1);
//		$('#favorite-container').html($('#favorite-container').html().replace('fa fa-2x fa-star-o favorite','fa fa-2x fa-star remove-favorite'));
//		$('#favorite-container').removeClass('add-favorites').addClass('remove-favorites');

    $('.remove-favorites').unbind('click');
    $('.remove-favorites').on('click', removeFavorites);
}

$('#print-flyer-button').on('click', printFlyer);

function printFlyer() {
    window.open('/printFlyer/$listingId', 'Home Details Flyer', 'width=785,height=700,scrollbars=no');
}

//http://stackoverflow.com/questions/21978098/item-alignment-within-an-image-carousel
$("#SimilarProductSlider").owlCarousel({
    navigation: true,
    navigationText : ["Prev", "Next"]
});

$('#SimilarProductSlider .item .product').responsiveEqualHeightGrid();



var mySwiper = new Swiper('.swiper-container', {
    pagination: '.box-pagination',
    keyboardControl: true,
    paginationClickable: true,
    slidesPerView: 'auto',
    autoResize: true,
    resizeReInit: true
});

/*$('.prevControl').on('click', function (e) {
 e.preventDefault();
 mySwiper.swipePrev();
 });
 $('.nextControl').on('click', function (e) {
 e.preventDefault();
 mySwiper.swipeNext();
 });*/

$(document).ready(function(){
    $.get( "/map/$model->listing_id", function( data ) {
        $("#map-tab").html( data );
        mapLoaded = true;
    });
});

var calcLoaded = false;
$("#calc-tab-link").on('click', function (e) {
    if(calcLoaded) {
        return;
    }

    $.get( "/mortgagecalculator/$model->price", function( data ) {
        $("#mortgage-calculator-tab").html( data );
        calcLoaded = true;
        $('#mortgage-calculator-button').on('click',function() {
            if($('#mortgage-calcualtor-fields').css('display') == 'none') {
                $('#mortgage-calcualtor-fields').show('slow');
            } else {
                var percent = $('#down-payment').val();
                percent = percent.replace('%','');
                percent = (1-(percent /100));

                var loanAmount = $('#price').val();
                loanAmount = loanAmount.replace('$','');
                loanAmount = loanAmount.replace(/,/g,'');
                loanAmount = loanAmount * percent;
                var interest = $('#interest').val();
                interest = interest.replace('%','');
                interest = interest /100/12;
                var months = $('#years').val();
                var payment = Math.round((loanAmount*interest)/(1-Math.pow(1+interest,(-1*months)))*100)/100;
                $('#monthly-payment').html('$'+payment.toFixed(2));
            }
        });
    });
});

$("#main-image-prevControl").click(function(e) {
    e.preventDefault();

    var current = $(".sp-current");

    $(".sp-current").removeClass();
    $(".sp-thumbs").removeClass("sp-tb-active");
    $(".sp-thumbs").find('a.sp-current').removeClass("sp-current");
    $(".sp-zoom").remove();
    var t = $(".sp-large").height();
    $(".sp-large").css({height: t + "px"}); //overflow: "hidden",
    $(".sp-large a").remove();

    var elm = $(current).prev();
    if(!elm.length) {
        elm = $('.sp-thumbs a').last();
    }

    $(elm).addClass("sp-current").clone().hide().removeClass("sp-current").appendTo(".sp-large").fadeIn(slideTiming, function () {
        var e = $(".sp-large img").height();
        $(".sp-large").height(t).animate({height: e}, "fast", function () {
            $(".sp-large").css("height", "auto")
        });
        $(".sp-thumbs").addClass("sp-tb-active")
    });
});
$("#main-image-nextControl").click(function(e) {
    e.preventDefault();

    var current = $(".sp-current");

    $(".sp-current").removeClass();
    $(".sp-thumbs").removeClass("sp-tb-active");
    $(".sp-thumbs").find('a.sp-current').removeClass("sp-current");
    $(".sp-zoom").remove();
    var t = $(".sp-large").height();
    $(".sp-large").css({height: t + "px"}); //overflow: "hidden",
    $(".sp-large a").remove();

    var elm = $(current).next();
    if(!elm.length) {
        elm = $('.sp-thumbs a').first();
    }

    $(elm).addClass("sp-current").clone().hide().removeClass("sp-current").appendTo(".sp-large").fadeIn(slideTiming, function () {
        var e = $(".sp-large img").height();
        $(".sp-large").height(t).animate({height: e}, "fast", function () {
            $(".sp-large").css("height", "auto")
        });
        $(".sp-thumbs").addClass("sp-tb-active")
    });
});
JS
, CClientScript::POS_END);
?>