<?
Yii::app()->clientScript->registerCss('houseValuesStandard2', <<<CSS

    #house-values-container .overlay {
        opacity: .2;
        background-color: black;
        position: absolute;
        height: 800px;
        width: 100%;
        top: 0;
        left: 0;
    }
    #house-values-container .container {
        top: 15%;
        height: 800px;
    }
    #houseValues-form h1, #houseValues-form h2 {
        color: white;
    }
    #houseValues-form .status-bar-container h1 {
        margin-top: 20px;
    }
    #house-values-container .fadedBg {
        z-index: 2;
        background-color: rgba(77, 163, 255, 0.9); /*rgba(255,255,255,0.75);*/
        padding: 0;
        min-height: 200px;
        max-width: none !important;
    }
    #house-values-container .form-container, #house-values-container .form-container-2 {
        padding: 30px;
        margin-top: 10%;
    }
    #house-values-container .form-container-2 {
        margin-top: 0;
    }
    #house-values-container input, #house-values-container select {
        font-size: 20px;
        height: 50px;
        background: white;
    }
    #house-values-container div.error input, #house-values-container div.error select {
        background: #FFB8B8;
    }
    #house-values-container #submit-button {
        background: #24D829;
        width: 100%;
    }
    @media (max-width: 360px) {
        #house-values-container #submit-button {
            margin-bottom: 20px;
        }
        #house-values-container h1 {
            font-size: 20px;
            line-height: 25px;
            padding-bottom: 0;
        }
    }
    #house-values-container #submit-button:hover {
        background: #10EC16;
    }
    #house-values-container .errorMessage {
        color: white;
    }
    #house-values-container .map {
        height: 440px;
         /*width: 100%*/
    }
    #house-values-container .margin-bottom-row {
        margin-bottom: 10px;
    }
    #house-values-container .status-bar-container {
        font-weight: bold;
        font-size: 15px;
        margin-bottom: 20px;
    }
    #house-values-container .status-bar-image{
        position: absolute;
        z-index: -1;
        top: -2px;
        left: 0;
        height: 25px;
        width: 100%;
    }
    .footer {
        margin-top: 0;
    }
CSS
);
?>
<div id="house-values-container">

    <div class="container main-container headerOffset">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'houseValues-form',
            'action' => array($formAction),
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnChange' => false,
                'validateOnSubmit' => true,
                'beforeValidate' => 'js:function(form, attribute) {
                    $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                    if(!$("#FormSubmissionValues_data_18").val()){
                		if(ValueReportFormGeoLocation_results[ 0 ]){
                			stmSetLocation(0);
                		}
                	}
                    return true;
                }',
                'afterValidate' => 'js:function(form, data, hasErrors) {
                    if (!hasErrors && data.status =="successPart1") {
                        window.location = "http://' . $_SERVER["SERVER_NAME"] . '/values/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/2/fsid/" + data.form_submission_id;
                    } else if (!hasErrors) {
                            window.location = "http://'.$_SERVER["SERVER_NAME"].'/houseValues/'.$houseValuesActionId.'/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/success/fsid/'.$_GET["fsid"].'";
                    } else {
                        $("div.loading-container.loading").remove();
                    }
                    return false;
                }',
            ),
        ));

            $this->renderPartial('values/' . $view, array(
                'form' => $form,
                'SubmissionValues' => $SubmissionValues,
                'FormFields' => $FormFields,
            ));

        $this->endWidget();
        ?>
    </div>
    <div class="" style="overflow: hidden; height: 800px; position: absolute; top: 0;">
        <img alt="img" src="http://cdn.seizethemarket.<?=((YII_DEBUG)? 'local': 'com')?>/assets/images/neighborhood-aerial-3.jpg" class="img-responsive" style="width: 100%; min-height: 800px;">
    </div>
    <div class="overlay"></div>
</div>
