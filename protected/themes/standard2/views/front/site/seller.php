<?php
Yii::app()->getClientScript()->registerCss('sellerPage', <<<CSS
#seller-form h1 {
    padding-left: 29px;
}
#thank-you-message h3 {
    font-size: 30px;
}
#thank-you-message h4 {
    font-size: 25px;
    line-height: 34px;
}
#seller-form .btn {
    padding: 20px;
    margin-bottom: 30px;
}
#thank-you-message {
    background-color: #a0ffa0;
    text-align: center;
    padding: 20px 10px;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    -moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
    -moz-box-shadow:    0px 2px 2px 0px #ccc;
    -webkit-box-shadow: 0px 2px 2px 0px #ccc;
    box-shadow:         0px 2px 2px 0px #ccc;
    margin-bottom: 30px;
}
#seller-form-image {
    margin-top: 80px;
    width: 100%;
    max-width: 400px;
}
CSS
);
?>
<div id="thank-you-message" class="hidden col-lg-12 col-xs-12">
    <h3>Thank You!</h3>
    <h4>
        Your information has been submitted successfully.<br>
        Feel free to call us at <?=Yii::app()->user->settings->office_phone?> for immediate assistance.<br><br>
        We look forward to helping you!
    </h4>
</div>
<div class="col-lg-8 col-xs-12">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'seller-form',
            'action'                 => array('/front/forms/submit/formId/'.$formId),
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnChange' => false,
                'validateOnSubmit' => true,
                'beforeValidate' => 'js:function(form, attribute) {
                                        $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                        return true;
                                    }',
                'afterValidate' => 'js:function(form, data, hasErrors) {
                                        if (!hasErrors) {
                                            Message.create("success", "Successfully submitted!");
                                            $("#thank-you-message").removeClass("hidden").hide().show("normal");
                                            $(".row").hide("normal");
                                        }

                                        $("div.loading-container.loading").remove();
                                        return false;
                                    }',
            ),
        ));
    ?>
        <h1 class="col-lg-12">Sell Your House</h1>
        <div class="col-lg-12 col-xs-12">
            <label class="col-lg-12 col-xs-12">Name: <span class="required">*</span></label>
            <div class="col-lg-6 col-xs-12">
                <?php echo $form->textField($model, 'data[' . $FormFields->getFieldIdByName('first_name') . ']',$htmlOptions = array('placeholder' => 'First Name', 'class' => 'col-lg-12 col-xs-12',)); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('first_name') . ']'); ?>
            </div>
            <div class="col-lg-6 col-xs-12">
                <?php echo $form->textField($model, 'data[' . $FormFields->getFieldIdByName('last_name') . ']',$htmlOptions = array('placeholder' => 'Last Name', 'class' => 'col-lg-12 col-xs-12',)); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('last_name') . ']'); ?>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12">
            <label class="col-lg-12 col-xs-12">Email: <span class="required">*</span></label>
            <div class="col-lg-12 col-xs-12">
                <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('email') . ']', $htmlOptions=array('placeholder' => 'Email', 'class'=>'col-lg-12 col-xs-12')); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('email') . ']'); ?>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12">
            <label class="col-lg-12 col-xs-12">Address: <span class="required">*</span></label>
            <div class="col-lg-12 col-xs-12">
                <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('address') . ']', $htmlOptions=array('placeholder' => 'Address', 'class'=>'ol-lg-12 col-xs-12')); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('address') . ']'); ?>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12">
            <label class="col-lg-12 col-xs-12">City: <span class="required">*</span></label>
            <div class="col-lg-6 col-xs-12">
                <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('city') . ']', $htmlOptions=array('placeholder' => 'City', 'class'=>'col-lg-12 col-xs-12')); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('city') . ']'); ?>
            </div>
            <div class="col-lg-2 col-xs-6">
                <?php echo $form->dropDownList($model,'data[' . $FormFields->getFieldIdByName('state') . ']', CHtml::listData(AddressStates::model()->findAll(array('order'=>'short_name ASC')), 'id', 'short_name'), $htmlOptions=array('empty' => 'Select State', 'class'=>'col-lg-12 col-xs-12')); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('state') . ']'); ?>
            </div>
            <div class="col-lg-4 col-xs-6">
                <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('zip') . ']', $htmlOptions=array('placeholder' => 'Zip', 'class'=>'col-lg-12 col-xs-12')); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('zip') . ']'); ?>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12" style="clear: both;">
            <label class="col-lg-12 col-xs-12">Phone: <span class="required">*</span></label>
            <div class="col-lg-12 col-xs-12">
                <?php $this->widget('StmMaskedTextField', array(
                        'model' => $model,
                        'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                        'mask' => '(999) 999-9999',
                        'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                        'htmlOptions' => array('class' => 'col-lg-12 col-xs-12', 'placeholder' => 'Phone'),
                    )); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('phone') . ']'); ?>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12">
            <label class="col-lg-12 col-xs-12">Timeframe:</label>
            <div class="col-lg-12 col-xs-12">
                <?php echo $form->textField($model,'data[' . $FormFields->getFieldIdByName('timeframe') . ']', $htmlOptions=array('placeholder' => 'Time frame', 'class'=>'col-lg-12 col-xs-12')); ?>
                <?php echo $form->error($model,'timeframe'); ?>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12" style="clear: both;">
            <label class="col-lg-4 col-xs-12">Comments:</label>
            <div class="col-lg-12 col-xs-12">
                <?php echo $form->textArea($model,'data[' . $FormFields->getFieldIdByName('comments') . ']', $htmlOptions=array('placeholder' => 'Comments', 'class'=>'col-lg-12 col-xs-12', 'rows'=>6)); ?>
                <?php echo $form->error($model,'data[' . $FormFields->getFieldIdByName('comments') . ']'); ?>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12">
            <div class="col-lg-12 col-xs-12">
                <button id="contact-agent-button" class="btn btn-primary col-lg-12 col-xs-12" type="submit">Submit</button>
            </div>
        </div>
    <? $this->endWidget(); ?>
</div>
<div class="col-lg-4 col-xs-12">
    <img id="seller-form-image" src="http://cdn.seizethemarket.com/assets/images/house_in_hand.png" alt=""/>
</div>