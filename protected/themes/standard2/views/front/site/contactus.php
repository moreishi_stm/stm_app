<style>
    textarea {
        height: auto;;
    }
    h1 {
        margin-top: 30px;
    }
    .form-container, .message-container, .success-message {
        margin-top: 30px;
        margin-bottom: 30px;
    }
    .success-message {
        background: #d1ffd1;
    }
    .message-container h2 {
        text-align: center;
        margin-bottom: 20px;
        width: 100%;
    }
    .contact-message {
        padding: 20px 40px 40px;
        font-size: 15px;
    }
    #logo-container {
        margin-bottom: 30px;
    }
</style>
<h1>Contact Us</h1>
<?php
$formId = Forms::FORM_CONTACT_US;
$SubmissionValues = new FormSubmissionValues($formId);
$FormFields = new FormFields;
?>
    <div class="form-container col-lg-6 col-xs-12 no-padding">

        <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contact-us-form',
                'action' => array('/front/forms/submit/formId/' . $formId),
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'beforeValidate' => 'js:function(form, attribute) {
                            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                            return true;
                        }',
                    'afterValidate' => 'js:function(form, data, hasErrors) {
                            if (!hasErrors) {
                                $(".form-container, .message-container").hide();
                                $(".success-message").show("normal");
                            }
                            $("div.loading-container.loading").remove();
                            return false;
                        }',
                ),
            ));
        ?>
        <?php $SubmissionValues->data[FormFields::SOURCE_DESCRIPTION_ID] = 'Contact Us Form'; ?>
        <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_DESCRIPTION_ID.']'); ?>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'First Name'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Last Name'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Email'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
            <?php $this->widget('StmMaskedTextField', array(
                    'model' => $SubmissionValues,
                    'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                    'mask' => '(999) 999-9999',
                    'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                    'htmlOptions' => array('class' => 'col-xs-12', 'placeholder' => 'Phone'),
                )); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('multi_component_type')->id . ']'); ?>
            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getField('multi_component_type')->id . ']', $FormFields->multiComponentList, $htmlOptions = array('class' => 'col-xs-12', 'empty' => 'How can we help you?'));?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('comments')->id . ']'); ?>
            <?php echo $form->textArea($SubmissionValues, 'data[' . $FormFields->getField('comments')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'rows'=>6, 'placeholder' => 'Comments'));?>
        </div>

        <div class="col-xs-12 no-padding">
            <?php if (!empty($fields['submit-button'])) { ?>
                <button id="submit-button" type="submit" class="stmcms submit col-xs-12 btn btn-lg btn-primary btn-success" stmtemplate-title="Submit Button Text" ></button>
            <?php } else { ?>
                <button id="submit-button" type="submit" class="submit col-xs-12 btn btn-lg btn-primary btn-success" stmtemplate-title="Submit Button Text" >SUBMIT NOW</button>
            <?php } ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="success-message col-lg-6 col-xs-12" style="display: none;">
        <h1 style="margin-bottom: 20px;">Success!</h1>
        <h2 style="margin-bottom: 20px;">Your information has been successfully submitted.</h2>
        <h2 style="margin-bottom: 20px;">If you need immediate assistance,<br>Call <?php echo Yii::app()->user->settings->office_phone; ?>.</h2>
        <h2 style="margin-bottom: 20px;">Thank you and have a great day!</h2>
    </div>
    <div class="col-lg-6 col-xs-12 message-container">
        <? if(!Yii::app()->controller->fullWidth && !$isHome && (Yii::app()->user->client->type !== 'Brokerage')):?>
        <h2 class="col-lg-12 col-xs-12">We look forward to helping you!</h2>
        <div class="contact-message col-lg-12 col-xs-12">
            Buying or selling a home is an impactful event in your life. We are dedicated to providing you with world class service and expert advice that will take the stress out of the home selling or buying process.
        </div>
        <h2 class="col-lg-12 col-xs-12">Feel free to call us at <?php echo Yii::app()->user->settings->office_phone; ?>.</h2>
        <h2 class="col-lg-12 col-xs-12">Thank you for the opportunity to <br>help you or your loved ones.</h2>
        <?else:?>
            <div id="logo-container" class="text-center"><img src="/images/logo.png" alt=""/></div>
            <h2 class="col-lg-12 col-xs-12">We look forward to helping you build a great career. Let us know how we can help you or anyone you know.</h2>
            <h2 class="col-lg-12 col-xs-12">Feel free to call us at <?php echo Yii::app()->user->settings->office_phone; ?>.</h2>
        <?endif;?>
    </div>