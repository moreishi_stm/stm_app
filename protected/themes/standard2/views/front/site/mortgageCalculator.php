<style type="text/css">
	.mortgage-calculator-tab-container {
		margin: 18px;
		position: relative;
		float: left;
		width: 640px;
	}
	.mortgage-calculator-tab-container label {
		width: 280px;
		display: inline-block;
		text-align: right;
		font-size: 15px;
	}
	.mortgage-calculator-tab-container input, .mortgage-calculator-tab-container select{
		font-size: 25px;
		font-weight: bold;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		border: solid 1px #CCC;
		padding: 5px 10px;
	}
	.mortgage-calculator-tab-container input, .mortgage-calculator-tab-container select{
		color: #333;
	}
	.mortgage-calculator-tab-container select{
		border-color: #AAA;
		height: 43px;
		width: 27.5%;
	}
	.mortgage-calculator-tab-container h3 {
		padding-left: 0 !important;
		font-size: 25px;
		height: 45px;
		display: inline-block;
	}
	.mortgage-calculator-tab-container div.monthly-payment-row{
		margin-top: 10px;
	}
	.mortgage-calculator-tab-container #monthly-payment{
		border: 1px solid #62D164;
		background-color: #CDF7CE;
		font-size: 24px;
		font-weight: bold;
		padding: 10px 10px;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		width: 24.5%;
		display: inline-block;
	}
	.mortgage-calculator-tab-container .i_mortgage_calculator{
		height: 78px;
		width: 69px;
		display: inline-block;
		background-repeat: no-repeat;
		background-image: url(http://cdn.seizethemarket.com/assets/images/calculator_2.png);
		float: right;
		position: relative;
		top:20px;
	}
	.mortgage-calculator-tab-container #mortgage-calcualtor-fields div.row{
		padding: 5px 0;
	}
	.mortgage-calculator-tab-container #mortgage-calculator-button{
		font-size: 14px;
		margin-top: 15px;
		padding: 12px 30px;
	}

</style>
<?php
	$this->renderPartial('homeDetails/_mortgageCalculator', array('mortgageRates'=>$mortgageRates,'price'=>$price));
?>