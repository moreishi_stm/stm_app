<div id="login">
    <div id="forgot-password-container">
        <h1 class="green"></em>Forgot Password</h1>
        <h4 class="text-center">Enter your email address to retreive your password.</h4>
        <?php echo $this->renderPartial('_forgotPasswordForm', array('model'=>$model)); ?>
    </div>
</div>
