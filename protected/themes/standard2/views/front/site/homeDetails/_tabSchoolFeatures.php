<div>
    <h3>School Information</h3>
    <ul class='feature_list'>
        <?php
        if (isset($schoolFeatures)) {
            ksort($schoolFeatures);
            foreach($schoolFeatures as $label=>$value) {
                echo '<li><label>',$label,'</label>',  $value,'</li>';
            }
        } else {
            echo 'No data found...';
        }
        ?>
    </ul>
</div>
<div style="clear:both;"></div>