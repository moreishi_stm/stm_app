<?php
$interest = ($mortgageRates['response']['today']['thirtyYearFixed'])?$mortgageRates['response']['today']['thirtyYearFixed']: 3.75;
$loanAmount = $price * .8;
$years = 30;
$payment = mortgagePayment($interest, $years, $loanAmount);
function mortgagePayment($interest, $years, $loanAmount) {
	$interest = $interest/100/12;
	$months = $years*12;
	return floor(($loanAmount*$interest/(1-pow(1+$interest,(-1*$months))))*100)/100;
}
?>
<style>
	.mortgage-calculator-tab-container{
		margin: 90px auto;
		background: rgb(255, 255, 255) none repeat scroll 0% 0%;
		width:90%;
	}
	#home-detail .mortgage-calculator-tab-container {
		background-color: #eee;
	}
	#mortgage-calc-container{
		position: relative;
	}
	#home-detail #home-property-widget h3 {
		padding: 0;
	}
	#mortgage-calcualtor-fields{
		width:50%;
		background-color: #fff;
		padding: 24px;
	}
	#mortgage-calcualtor-results{
		width:40%;
		font-weight:700;
		margin-top:4em;
		margin-left: 12px;
	}
	#mortgage-calcualtor-results .row{
		margin-bottom:32px;
	}
	#mortgage-calculator-tab .exit{
		position: absolute;
		top:8px;
		right:12px;
		z-index: 999;
	}
	#mortgage-calculator-tab .exit a:hover{
		color:#C20000;
		text-decoration: none;
	}
	#mortgage-calcualtor-results .monthlyPayment{
		border-top: 1px solid #000;
		padding-top: 12px;
	}

	@media (max-width: 992px) {

		#mortgage-calcualtor-fields,
		#mortgage-calcualtor-results{
			width:100%;
		}
		#mortgage-calcualtor-fields h3{
			font-size:24px;
		}
		#mortgage-calcualtor-results{
			margin-top: 8px;
			margin-left: 0;
			padding:24px;
		}
		#mortgage-calcualtor-results .pull-left,
		#mortgage-calcualtor-results .pull-right{

		}
		#mortgage-calcualtor-results .monthlyPayment{
			border-top: 1px solid #000;
			padding-top: 12px;
		}
		.mortgageAmount .pull-right{
			margin-bottom: 12px;
		}
		#mortgage-calcualtor-fields .form-group{
			padding-bottom: 15px;
			border-bottom: 1px solid;
			padding-top: 15px;
		}

		#mortgage-calcualtor-fields .form-group .form-control,
		#mortgage-calcualtor-results .pull-right{
			width:48%;
			float:right;
		}
	}
</style>
<h1 class="exit">
	<a href="javascript:void(0)" onclick="calculatorToggle();">
		<i class="fa fa-remove"></i>
	</a>
</h1>
<div id="mortgage-calc-container" class="container main-container clearfix">
	<div class="mortgage-calculator-tab-container clearfix">
		<div id="mortgage-calcualtor-fields" class="pull-left form">
			<div class="form-group title">
				<h3>Mortgage Calculator</h3>
			</div>
			<div style="padding-top:8px;" class="form-group price">
				<label>Price:</label>
				<input type="text" id="price" class="form-control" size="15" placeholder="Price" value="<?php echo Yii::app()->format->formatDollars($price)?>">
			</div>
			<div class="form-group interest">
				<label>Interest:</label>
				<input type="text" id="interest" class="form-control" size="5" placeholder="Interest" value="<?php echo $interest?>%">
			</div>
			<div class="form-group down-payment">
				<label>Down Pmt:</label>
				<input type="text" id="down-payment" class="form-control" size="5" placeholder="Down Payment" value="20%">
			</div>
			<div class="form-group years">
				<label>Years:</label>
				<select id="years" class="form-control"><option value="360">30 years</option><option value="240">20 years</option><option value="180">15 years</option></select>
			</div>
		</div>
		<div id="mortgage-calcualtor-results" class="pull-left clearfix">
			<div class="row mortgageAmount">
				<div class="col-lg-6 pull-left">Mortgage Amount</div>
				<div class="col-lg-3 pull-right"><span id="mortgage-amount">$0</span></div>
			</div>
			<div class="row paymentAmount">
				<div class="col-lg-6 pull-left">Down Payment Amount</div>
				<div class="col-lg-3 pull-right"><span id="down-payment-amount">$0</span></div>
			</div>
			<hr>
			<div class="row monthlyPayment">
				<div class="col-lg-6 pull-left">Total Monthly Payment</div>
				<div class="col-lg-3 pull-right"><span id="monthly-payment">$0</span></div>
			</div>
		</div>
	</div>
</div>
