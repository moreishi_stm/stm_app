<div class="features-container col-lg-4 col-md-4 col-sm-4">
    <h3>Exterior Features</h3>
    <ul class='feature_list'>
        <?php
        if (isset($exteriorFeatures)) {
            ksort($exteriorFeatures);
            foreach($exteriorFeatures as $label=>$value) {
                echo '<li><label>',$label,'</label>',  str_replace(',','<br />',$value),'</li>';
            }
        }
        ?>
    </ul>
</div>

<div class="features-container col-lg-4 col-md-4 col-sm-4">
    <h3>Interior Features</h3>
    <ul class='feature_list'>
        <?php
        if (isset($interiorFeatures)) {
            ksort($interiorFeatures);
            foreach($interiorFeatures as $label=>$value) {
                echo '<li><label>',$label,'</label>', str_replace(',','<br />',$value),'</li>';
            }
        }
        ?>
    </ul>
</div>

<div class="features-container col-lg-4 col-md-4 col-sm-4">
    <h3>Property Features</h3>
    <ul class='feature_list'>
        <?php
        if (isset($propertyFeatures)) {
            ksort($propertyFeatures);
            foreach($propertyFeatures as $label=>$value) {
                echo '<li><label>',$label,'</label>', str_replace(',','<br />',$value),'</li>';
            }
        }
        ?>
    </ul>
</div>
<div style="clear:both;"></div>