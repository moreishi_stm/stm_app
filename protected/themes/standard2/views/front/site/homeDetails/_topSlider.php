<?php
if(!empty($photoContent['thumbnails'])) {
	$domainSuffix = (YII_DEBUG) ? 'local' : 'com';
	$cdnBaseUrl = "http://cdn.seizethemarket.$domainSuffix/assets/fancybox/fancybox-2.1.5/source";
	Yii::app()->clientScript->registerCssFile($cdnBaseUrl . '/jquery.fancybox.css?v=2.1.5');
	Yii::app()->clientScript->registerScriptFile($cdnBaseUrl . '/jquery.fancybox.pack.js?v=2.1.5', CClientScript::POS_END);
	$js = <<<JS
$("#HomeDetailsImageSlider").owlCarousel({
        navigation: true,
        autoWidth:true,
		itemsCustom : [
			[0, 1],
			[450, 1],
			[600, 2],
			[700, 2],
			[1000, 2],
			[1200, 3],
			[1400, 3],
			[1600, 3]
		]

    });
    $( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
 	$( ".owl-next").html('<i class="fa fa-angle-right"></i>');
	$("#HomeDetailsImageSlider .fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});
JS;

	Yii::app()->clientScript->registerScript('HomeDetailsImageSliderJs', $js, CClientScript::POS_END);
	/**
	 * added css below to avoid !important because the owl css is loaded after the register css
	 */
	?>
	<style type="text/css">
		#home-detail .row{
			margin: 0;
		}
		#HomeDetailsImageSlider {
			position: relative;
			margin-top: 100px;
		}

		#HomeDetailsImageSlider .product {
			background: #F3F3F3;
			color: #333;
			margin: 0;
		}

		#HomeDetailsImageSlider .product a:hover {
			text-decoration: none;
			color: #333;
		}
		#HomeDetailsImageSlider .owl-item {
			height:400px;
		}

		#HomeDetailsImageSlider .product-image img {
			width: 600px;
			margin: 2px;
			max-height: 400px;
			height: 400px;
		}
		#HomeDetailsImageSlider .item {
			padding: 0;
			float: left;
			margin: 0;
		}
		#HomeDetailsImageSlider .owl-buttons {
			position: relative;
			border-radius:0;
		}
		#HomeDetailsImageSlider .owl-buttons .owl-prev {
			position:absolute;
			top:-272px;
			left:0px;
			font-size: 72px !important;
			padding: 12px;
			border-radius:0;
		}
		#HomeDetailsImageSlider .owl-buttons .owl-next {
			position:absolute;
			top:-272px;
			right:0px;
			font-size: 72px !important;
			padding: 12px;
			border-radius:0;

		}
		.owl-theme .owl-controls .owl-buttons div{
			background-color: #000;
			opacity: .5;
		}
		.owl-theme .owl-controls .owl-page span{
			background-color:#666;
		}
		.owl-theme .owl-controls.clickable .owl-buttons div:hover{
			opacity: .7;
		}
		.owl-theme .owl-controls .owl-page span{
			-webkit-border-radius: 0;
			-moz-border-radius: 0;
			border-radius: 0;
		}
		@media (max-width: 992px) {
			.owl-pagination{
				display: none;
			}
			.owl-buttons .owl-next,
			.owl-buttons .owl-prev{
				top:-172px ;
				font-size:50px !important;
			}
			.hidden-data{
				display:none;
			}
		}


	</style>
	<div id="HomeDetailsImageSlider">
		<?php foreach ($photoContent['thumbnails'] as $i => $photoUrl):
			if (empty($similarProperty['photoUrl'])) {
				$similarProperty['photoUrl'] = "http://cdn.seizethemarket.com/assets/images/default-placeholder.png";
			}
			?>
			<div class="item">
				<div class="product">
					<?php if ($i == 0): ?>
						<a
							href="<?= $photoContent['fullSizeUrl'][$i]; ?>"
							class="product-image fancybox"
							rel="HomeDetailsImageSlider"
						>
							<img src="<?= $photoUrl; ?>" class="product-image"
								 alt="img">
						</a>
					<?php else: ?>
						<a
							href="<?= $photoContent['fullSizeUrl'][$i]; ?>"
							class="product-image fancybox"
							rel="HomeDetailsImageSlider"
						>
							<img src="<?= $photoUrl; ?>" class="img-responsive"
								 alt="img">
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<?php
} #end if !empty
?>