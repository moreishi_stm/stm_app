<?php
function _stmPropertyFeatureCount($set){
	$count = 0;
	if(is_array($set) && !empty($set)){
		foreach ($set as $label => $data){
			if(strstr($data,",")){
				$count = $count + count(explode(",",$data));
			}
			$count++; // need the label either way
		}
	}
	return $count;
}
?>
<div class="features clearfix">
	<?php
	if(!empty($model)){
		$sectionIdCount =0;
		foreach ($model as $section){

			$item_count = _stmPropertyFeatureCount($section["data"]);
			if($item_count > 2){
				$item_count = ceil( ($item_count / 2) );
			}
			if(!empty($item_count)){
				$sectionIdCount++;
				?>
				<div id="featureSection<?php echo $sectionIdCount;?>" class="border-bottom clearfix feature-section">
					<div class="col-sm-3 col-md-3 col-lg-3">
						<h3><?php echo $section["label"];?></h3>
						<?php
						if($item_count > 5){?>
							<a class="show-more">Show More <i class="fa  fa-angle-down"></i></a>
						<?php }
						?>

					</div>
					<div class="col-sm-3 col-md-3 col-lg-3">
						<?php
						if (is_array($section["data"]) && !empty($section["data"])) {
							$item_count = _stmPropertyFeatureCount($section["data"]);
							if($item_count > 2){
								$item_count = ceil( ($item_count / 2) );
							}
							$current = 1;
							$colCount = 1;
							$col2Used = false;
							ksort($section["data"]);
							foreach($section["data"] as $label=>$value) {
								if(!$col2Used && $current >= $item_count){
									echo "</div><div class=\"col-sm-3 col-md-3 col-lg-3\">";
									$col2Used = true;
									$colCount = 1;
								}
								if($colCount > 5){$hiddenStyle = "hidden-data";}else{$hiddenStyle = "";}
								if(strstr($value,",")){
									echo "<li class='{$hiddenStyle}'><label>{$label}</label><ul class='{$hiddenStyle}'>";
									$current++; //for label row
									$colCount++;

									$subs = explode(",",$value);
									foreach ($subs as $sub){
										if($colCount > 5){$hiddenStyle = "hidden-data";}else{$hiddenStyle = "";}
										echo "<li class='{$hiddenStyle}'>{$sub}</li>";
										$current++;
										$colCount++;
									}
									echo "</ul></li>";

								}else{
									echo "<li class='{$hiddenStyle}'><label>{$label}:</label>&nbsp;{$value}</li>";
									$current++;
									$colCount++;
								}
							}
						}
						?>
					</div>
				</div>
				<div style="clear:both;"></div>
			<?php }
		}
	}
	?>
</div>

