<div id="form" class="form-inline">
	<?php
	$formId = Forms::FORM_SHOWING_REQUEST;
	$SubmissionValues = new FormSubmissionValues($formId);
	$FormFields = new FormFields;

	if (!Yii::app()->user->isGuest) {

		$SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;
		$SubmissionValues->data[$FormFields->getField('last_name')->id] = Yii::app()->user->lastName;
		$SubmissionValues->data[$FormFields->getField('email')->id] = Yii::app()->user->contact->getPrimaryEmail();
		$SubmissionValues->data[$FormFields->getField('phone')->id] = Yii::app()->user->contact->getPrimaryPhone();
	}

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'video-form',
		'action' => array('/front/forms/showing/formId/' . $formId),
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
		'clientOptions' => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
			'beforeValidate' => 'js:function(form) {
									$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
									return true;
								}',
			'afterValidate' => 'js:function(form, data, hasErrors) {
								$("div.loading-container.loading").remove();
								if (!hasErrors) {
									Message.create("success", "Your Showing Request was sent successfully!");
									$("div#form").addClass("hidden");
									$("div#thank-you-message").hide().removeClass("hidden").show();
								}

								return false;
							}',
		)
	));
	?>
	<div class="container main-container">
		<h2>Request a Showing</h2>
		<div class="form-group col-sm-6 col-xs-12 no-padding">
			<?php
			echo $form->textArea($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']', $htmlOptions = array(
				'placeholder' => "When would you like to see this home?", 'class' => 'col-xs-12', 'rows'=>4
			));
			?>
			<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']'); ?>
			<div class="form-group col-xs-12 no-padding">
				<div class="col-sm-6 col-xs-6 no-padding">
					<?php
					echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array(
						'placeholder' => 'First Name', 'class' => 'col-xs-12',
					));
					?>
					<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
				</div>
				<div class="col-sm-6 col-xs-6 no-padding">
					<?php
					echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array(
						'placeholder' => 'Last Name', 'class' => 'col-xs-12',
					));
					?>
					<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
				</div>
			</div>

			<div class="form-group col-xs-12 no-padding">
				<div class="col-sm-6 col-xs-6 no-padding">
					<?php
					echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array(
						'placeholder' => 'E-mail', 'class' => 'col-xs-12',
					));
					?>
					<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
				</div>
				<div class="col-sm-6 col-xs-6 no-padding">
					<?php
					$this->widget('StmMaskedTextField', array(
						'model' => $SubmissionValues,
						'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
						'mask' => '(999) 999-9999',
						'id' => 'FormSubmissionValues_data_4',
						'htmlOptions' => array('class' => 'col-xs-12','placeholder'=>'Phone'),
					));
					?>
					<?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
					<?php echo CHtml::hiddenField('url', $_SERVER['REQUEST_URI']); ?>
				</div>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::SOURCE_DESCRIPTION_ID.']', $htmlOptions=array('value'=>'Showing Request'));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$model->listing_id));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$model->getPhotoUrl(1)));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('home_url') .']', $htmlOptions=array('value'=>$model->getUrl()));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$model->price));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$model->bedrooms));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$model->baths_total));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$model->sq_feet));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->streetAddress)))));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->city)))));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($model->state)))));?>
				<?php echo $form->hiddenField($SubmissionValues, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$model->zip));?>
			</div>
			<div class="col-xs-12 no-padding">
				<button id="submit-button" type="submit" class="submit btn btn-primary request_showing_form_button">Send Request</button>
			</div>
		</div>
		<div class="form-group col-sm-6 col-xs-12 form-contact-container">
			<div class="col-xs-12">
				<h3>
					<?
					if($leadRoute = LeadRoutes::model()->findByAttributes(array('component_type_id'=>ComponentTypes::BUYERS))) {
						$agents = $leadRoute->getActingAgents();
						if($settingValue = SettingContactValues::model()->findByAttributes(array('setting_id'=>Settings::SETTING_PROFILE_PHOTO_ID, 'contact_id'=>$agents[0]->id))) {
							$profileUrl = Yii::app()->user->getProfileImageBaseUrl().'/'.$agents[0]->id.'/'.trim($settingValue->value);
						}
					}
					?>
					<div class="profile-photo-container">
						<?
						if($profileUrl):
							?>
							<img class="profile" src="<?=$profileUrl?>"/><br>
							<?
						endif;?>
					</div>
					<?
					$agentName = $agents[0]->fullName;
					?>
					<?=$agentName?>
				</h3>
			</div>
			<div class="col-xs-12">
				<label>Phone:</label>
				<span><?=Yii::app()->user->settings->office_phone?></span>
			</div>
			<div class="col-xs-12 office-name">
				<span><?=Yii::app()->user->settings->office_name?></span>
			</div>
		</div>
	</div>

	<?php $this->endWidget(); ?>

</div>

<div id="thank-you-message" class="hidden">
	<img src="<?=$model->getPhotoUrl(1)?>" alt=""/>
	<h3>Thank You!</h3>
	We received your Showing Request. <br /><br />

	Feel free to call <strong><?php echo Yii::app()->user->settings->office_phone; ?></strong> for immediate assistance. <br /><br />

	Look forward to seeing you soon!<br><br>

	<h3 class="agent-name"><?=$agentName?></h3>
</div>