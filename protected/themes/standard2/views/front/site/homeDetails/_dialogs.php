<?php
$dialogWidget = 'front_widgets.DialogWidget.HomeDetailsDialogWidget.HomeDetailsDialogWidget';
$isGuest = Yii::app()->user->isGuest;
$theme = Yii::app()->theme->name;
$propertyDetails = $model->getHomeDetails($model->listing_id);
$photoUrls = explode(',', $propertyDetails['photoUrls']);
$photoUrl = $photoUrls[0];

//@todo: temp quick fix for non-photo url
if(!Yii::app()->user->board->has_photo_url) {
    $photoUrl = $model->getPhotoUrl(1);
}
if ($viewsMaxReached) {

} else {
	// Send to Friend Dialog Widget

	$this->widget($dialogWidget, array(
		'id'            => 'send-to-friend-dialog-fav',
		'type'          => 'send-to-friend',
		'property'      => $model,
		'photoUrl'      => $photoUrl,
		'isGuest'       => $isGuest,
		'title'         => 'Send to a Friend',
		'triggerElement'=> '#send-to-friend-button-fav',
		'formView'      => '_sendToFriend'
	));


    // Ask Question Dialog Widget
    $this->widget($dialogWidget, array(
        'id'             => 'ask-question-dialog',
        'type'           => 'ask-question',
        'property'       => $model,
        'photoUrl'       => $photoUrl,
        'isGuest'        => $isGuest,
        'title'          => 'Ask a Question',
        'triggerElement' => '#ask-question-button',
        'formView'       => '_askQuestion',
		'theme' => $theme
    ));

    // Property Tax Dialog Widget
//    $this->widget($dialogWidget, array(
//        'id'             => 'property-tax-dialog',
//        'type'           => 'property-tax',
//        'property'       => $model,
//        'photoUrl'       => $photoUrl,
//        'isGuest'        => $isGuest,
//        'title'          => 'Property Tax Info Request',
//        'triggerElement' => '#property-tax-button',
//        'formView'       => '_propertyTax',
//		'theme' => $theme
//    ));
}
?>
<?php
$js = <<<JS
$( "form .ajaxSubmit" ).on("click", function( event ) {
		event.preventDefault();
		$(".loading-container").addClass("loading");
		var element_id = $(this).closest('form').attr('id');
		$("#"+element_id+" input, #"+element_id+" textarea").removeClass("error");
		$("#"+element_id+" input").removeClass("error");
		$.post(
			$("#"+element_id).attr("action"),
			$( "#"+element_id ).serializeArray(),
			function(data) {
				if(!data || data == ""){
					// @todo need to add some kind of confirmation for the user jamesk may 6 2016
					location.reload();
				} else {
					$(".loading-container").removeClass("loading");
					$.each(data, function( index, value ) {
						$("#"+element_id+" #"+index).addClass("error");
					});
				}
			}
		);
	});
JS;
Yii::app()->clientScript->registerScript('HOMEDETAILSAUTOFORMSUBMITJS', $js, CClientScript::POS_END);
?>
