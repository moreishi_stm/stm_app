<?php

/**
 * @param required $MlsProperties
 */
$googleDataSubject = StmFunctions::getGoogleAddressData($MlsProperties->getStreetAddress(), $MlsProperties->city, $MlsProperties->state, $MlsProperties->zip); // get zip only
if(isset($googleDataSubject['results'][0]['geometry']['location']['lat'])) {
	$subjectMarkerCenter = ($googleDataSubject['results'][0]['geometry']['location']['lat']).','.$googleDataSubject['results'][0]['geometry']['location']['lng']; //['lat']+.0001
	$subjectMapCenter = ($googleDataSubject['results'][0]['geometry']['location']['lat']).','.$googleDataSubject['results'][0]['geometry']['location']['lng']; //['lat']+.00015
	$subjectCenter = $googleDataSubject['results'][0]['geometry']['location']['lat'].','.$googleDataSubject['results'][0]['geometry']['location']['lng'];
}
$streetAddress = $MlsProperties->getStreetAddress();
$cityStZip = $MlsProperties->getCityStZip();
$geoAddress = $streetAddress.", ".$cityStZip;
$bedrooms = $MlsProperties->bedrooms;
$baths = $MlsProperties->baths_total;
$sf = $MlsProperties->sq_feet;

$specs = array();
if($bedrooms){
	$specs[] = $bedrooms." BD";
}
if($baths){
	$specs[] = $baths." BA";
}
if($sf){
	$specs[] = $sf." SF";
}
if(!empty($specs)){
	$specs = "<div class=\"subjectSpecs\">".implode("&nbsp; | &nbsp;",$specs)."</div>";
}else{
	$specs = "";
}


// {$bedrooms} BD &nbsp;|&nbsp; {$baths} BA &nbsp;|&nbsp; {$sf} SF</div>

$js = <<<JS
	$(document).ready(function(){
		var infowindowSubject = new google.maps.InfoWindow();
		var mapSubject = new google.maps.Map(document.getElementById('map-tab'), {
			zoom: 40,
			disableDefaultUI: true,
			scrollwheel: false,
			zoomControl: true,
			center: new google.maps.LatLng({$subjectMapCenter}),
			mapTypeId: google.maps.MapTypeId.SATELLITE
		});
		var markerSubject = new google.maps.Marker({
			// Supply map and position params as usual.
			position: new google.maps.LatLng($subjectMarkerCenter),
			icon: 'http://cdn.seizethemarket.com/assets/images/map-house-marker.png',
			map: mapSubject,
			title: '{$streetAddress}'
	
		});
	infowindowSubject.setContent('<div class="subjectAddress"><strong>{$streetAddress}</strong><br>{$cityStZip}</div>{$specs}');
	infowindowSubject.open(mapSubject, markerSubject);
	    var panorama = new google.maps.StreetViewPanorama(document.getElementById("map-subject-pano"));
    var sv = new google.maps.StreetViewService();
//    sv.getPanoramaByLocation(myLatLng, 50, processSVData);
    //var myLatLng = new google.maps.LatLng({$subjectCenter});

//----------------------------------------------
    var directionsService = new google.maps.DirectionsService();
    var geocoder = new google.maps.Geocoder();
    var address = '{$geoAddress}';
    var myLatLng;

    geocoder.geocode({
        'address': address
    }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
        myLatLng = results[0].geometry.location; 

        // find a Streetview location on the road
        var request = {
            origin: address,
            destination: address,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, directionsCallback);
    } else {
        alert("Geocode was not successful for the following reason: " + status);
    }
    });

    function directionsCallback(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            var latlng = response.routes[0].legs[0].start_location;
            sv.getPanoramaByLocation(latlng, 50, processSVData);
        } else {
            alert("Directions service not successfull for the following reason:" + status);
        }
    }
//----------------------------------------------

    // function for street view data
    function processSVData(data, status)
    {
        if (status == google.maps.StreetViewStatus.OK) {
            var markerPano = new google.maps.Marker({
                position: data.location.latLng,
                draggable: true,
                map: map,
                title: '{$streetAddress}'
            });

            panorama.setPano(data.location.pano);

            var heading = google.maps.geometry.spherical.computeHeading(data.location.latLng, myLatLng);
            panorama.setPov({
                heading: heading,
                pitch: 0,
                zoom: 1
            });
            panorama.setVisible(true);
            google.maps.event.addListener(markerPano, 'click', function() {
            var markerPanoID = data.location.pano;
            // Set the Pano to use the passed panoID
            panorama.setPano(markerPanoID);
            panorama.setPov({
                heading: 270,
                pitch: 0,
                zoom: 1
            });
            panorama.setVisible(true);
            });
            panorama.setOptions({disableDefaultUI: true, zoom: 2});
            $('#map-subject-pano').append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\"map-subject-pano\")'></i>");
            $('#map-tab').append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\"map-tab\")'></i>");
        }
        else {
            $('#map-tab').removeClass('dual-map');
            $('#map-subject-pano').hide();
            var r = google.maps.event.trigger(mapSubject, "resize");
            mapSubject.setCenter(new google.maps.LatLng({$subjectMapCenter}));
            $('#map-tab').append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\"map-tab\")'></i>");
            return;
        }
    }
	});
	var map = new google.maps.Map(document.getElementById('map-tab'), {
        zoom: 13,
        scrollwheel: false,
        disableDefaultUI: true,
        center: new google.maps.LatLng($center),
        mapTypeId: google.maps.MapTypeId.SATELLITE
    });
    
    function stmMapFullScreen(id){
    	$('#'+id+" i").remove();
    	if( $("#"+id).hasClass("map-full-screen") ){
    		$("#"+id).removeClass("map-full-screen");
    		$('#'+id).css({ "position": "relative","height": " ", "width": " ", "top":" " });
    		$('#'+id).attr("style","min-height: 240px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;");
    		google.maps.event.trigger(document.getElementById(id), "resize");
    		$('#'+id).append("<i class='fa fa-expand' title='Expand Map' onclick='stmMapFullScreen(\""+id+"\")'></i>");
    		return;
    			
    	}
    	$("#"+id).addClass("map-full-screen");
    	console.log($(window).width());
    	console.log($(window).height());
    	
    	var tm = parseInt($(".navbar-fixed-top:first").innerHeight());
    	var h = parseInt( ( $(window).height() - tm ) - 50);
    	var w = parseInt($(window).width() - 50);
    	console.log(tm);
    	$('#'+id).css({ "position": "fixed","height": h, "width": w, "top":tm });
    	$('#'+id+" i").removeClass("fa-expand");
    	$('#'+id+" i").addClass("fa-close");
    	google.maps.event.trigger(document.getElementById(id), "resize");
    	$('#'+id).append("<i class='fa fa-close' title='Shrink Map' onclick='stmMapFullScreen(\""+id+"\")'></i>");
    	
    }
    
JS;

Yii::app()->clientScript->registerScript('googleMapScript',$js,CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile("http://maps.google.com/maps/api/js?key=AIzaSyA5On2atWpapg7fDL7r7QDJ3mb_QDtL0YE",CClientScript::POS_END);

?>
<style>
	.map-full-screen{
		left:0;
		right:0;
		z-index:500;
		margin: 12px auto;
	}
	.map i{
		color: rgb(255, 255, 255);
		font-size: 3em;
		margin: 4px;
		float: right;
		cursor: pointer;
		z-index:20;
		text-shadow: #000 1px 1px 0;
	}
	.map i:hover{
		color:#C20000;
	}
	#map-subject-pano{
		border-left: 1px solid #ffffff;
	}
</style>
<div id="map-tab" class="dual-map map" style="min-height:240px">
	<i class="fa fa-close"></i>
</div>
<div id="map-subject-pano" class="dual-map map" style="min-height:240px;"></div>