<?php
/**
 * @author RapidMod.com
 * @author 813.330.0522
 * @param $isFavorite;
 * @param $mlsBoardId;
 * @param $contactId;
 * @param $listingId
 */
Yii::app()->clientScript->registerCssFile("https://fonts.googleapis.com/icon?family=Material+Icons");

?>
<style type="text/css">
	.ctabuttons {
		display: inline-block;
		float: right;
	}

	.ctabuttons ul li i span {
		width:24px;
	}

	.ctabuttons a:hover {
		text-decoration: none;
		color: #C20000;
	}
	.ctabuttons li a{
		font-size: 16px;
		color: #777;
        position: relative;
	}
	.ctabuttons i {
		margin:8px;
		font-size: 16px;
	}
	.ctabuttons ul li .submit{
		min-width: 248px;
		padding: 16px 32px;
		font-size: 18px;
		border-color: #000;
		background-color: #000;
		color: #fff;
		display: inline-block;
		border: 1px solid #000;
		border-radius: 0;
		margin-top: 12px;;
	}
	.add-fav.active,
	.ctabuttons .active i,
	.ctabuttons a:hover i{
		color: #C20000;
		border-color: transparent;
	}
	.add-fav.active i{
		font-size: 19px;
		margin-top: 0px;
		margin-bottom: 0px;
		padding-bottom: 0px;
	}

	.add-fav:not(.active) i {
		text-shadow: none;
		color:#777;
	}
</style>
<?php
	if($isFavorite){
		$favContent = "<span><i class=\"material-icons\">favorite</i></span>Saved";
	} else {
		$favContent = "<span><i class=\"material-icons\">favorite_border</i></span>Save to Favorites";
	}
?>
<div class="ctabuttons">
	<ul>

		<li>
			<a
				title="Add to favorites"
				class="cta-button-tip add-fav<?=($isFavorite) ? " active" : "";?>"
				data-listingid="<?=$listingId?>" data-mbid="<?=$mlsBoardId?>"
				data-contact-id="<?=$contactId; ?>"
				data-tooltip="Add to Favorites"
				id="add-favorites-button"
			>
				<?php echo $favContent;?>
			</a>
		</li>
		<li>
			<a id="ask-question-button" class="cta-button-tip" data-tooltip="Ask a Question">
				<span><i class="fa fa-comment-o"></i></span>
				Ask a Question
			</a>
		</li>
<?/*		<li>
			<a href="#map-tab" id="map-fav-link" title="View Map" class="cta-button-tip" data-tooltip="View the Map">
				<i class="fa fa-map-marker"></i>
				View Map
			</a>
		</li>
*/?>
		<li>
			<a id="send-to-friend-button-fav" href="#share-fav-link" id="share-fav-link" title="Share" class="cta-button-tip" data-tooltip="Share this">
				<i class="fa fa-envelope-o"></i>
				Email to Friend
			</a>
		</li>
		<li>
			<a id="print-flyer-button" class="cta-button-tip" data-tooltip="Print Flyer">
				<span><i class="fa fa-print"></i></span>
				Print
			</a>
		</li>
		<li>
			<a href="#showing-request-form" class="submit btn btn-primary request-showing-button cta-button-tip scrollTo"  data-tooltip="Request a Showing">
				Request Showing
			</a>
		</li>
	</ul>
</div>
