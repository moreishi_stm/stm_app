<!-- Main component call to action -->
    <?php

    //@todo: put this conditional in manually for clients not ready for this yet.
    // check to see if feature area tag "Display Homepage" exists  @todo: this is a temp solution
    if($displayTag = FeaturedAreaTypes::model()->findByAttributes(array('name'=>'Display Homepage'))) {
        $displayHomePage = FeaturedAreaTypeLu::model()->findAllByAttributes(array('featured_area_type_id' => $displayTag->id));
    }

    if($displayHomePage):?>
		<div class="container main-container">

			<!--This is pieced from index 5-->
			<?php
			$featuredAreasCriteria = new CDbCriteria;
            $featuredAreasCriteria->with = array('typeLu');
			$featuredAreasCriteria->condition = "typeLu.featured_area_type_id=".$displayTag->id;
			$featuredAreasCriteria->order = "name ASC";

			$featuredAreasModel = new FeaturedAreas();
			$data = $featuredAreasModel->findAll($featuredAreasCriteria);

			$areaMainimageCriteria = new CDbCriteria;
			foreach($data as $area) {
				$areaMainimageCriteria->condition = "featured_area_id = :featured_area_id AND is_primary = 1";
				$areaMainimageCriteria->params = array(
					":featured_area_id" => $area->id
				);
				$mainAreaImage = FeaturedAreaImages::model()->find($areaMainimageCriteria);

				if(!empty($mainAreaImage)) {
					$area->image = $mainAreaImage;
				}
			}

			$featuredAreasDataProvider = new CArrayDataProvider($data, array('pagination' => array('pageSize' => count($data))));

			if($featuredAreasDataProvider->getItemCount() > 0) {

				$TopAreasWidgetMobileClickHandler = "TopAreas-Clicker";

				Yii::import('stm_app.widgets.ContainerWidget.ContainerWidget');
				$this->widget('stm_app.widgets.ContainerWidget.ContainerWidget', array(
					'id' => 'TopAreasContainer',
					'title' => 'TOP AREAS & NEIGHBORHOODS',
					'wrapper' => '<div class="featuredImageLook3">%s</div>',
					'widgets' => array(
						'stm_app.widgets.RowWidget.RowWidget' => array(
							'id' => 'TopAreasRow',
                            'perRow' => 3,
							'displayWidget' => 'stm_app.widgets.TopAreasWidget.TopAreasWidget',
							'displayWidgetOptions' => array(
								'view_file' => "TopAreas",
								'baseUrl' => '/area/',
								'jsHandler' => $TopAreasWidgetMobileClickHandler
							),
							'DataProvider' => $featuredAreasDataProvider,
						)
					)
				));

Yii::app()->getClientScript()->registerScript('TopAreasWidgetMobileClickHandler', <<<JS
    $(".{$TopAreasWidgetMobileClickHandler}").click(function(e) {
        if($(this).data('href')) {
            e.preventDefault();
            e.stopPropagation();
        
            $("body").prepend('<div class="loading-container loading"><em></em></div>');
            window.location.href=$(this).data('href');
            
            return true;
        } 
        
        if($(this).attr('data-href')) {
            e.preventDefault();
            e.stopPropagation();
            
            $("body").prepend('<div class="loading-container loading"><em></em></div>');
            window.location.href=$(this).data('href');
            
            return true;
        }
        
        return true;
    });
JS
);
			}
			?>

			<!--/.featuredPostContainer-->
			<!--/.This is pieced from index 5-->
			<div class="row featuredPostContainer style2">
			</div>
			<!--/.featuredPostContainer-->
		</div>
		<!-- /main container -->
    <?php
    endif;

    if(/*strpos($_SERVER['SERVER_NAME'], 'kwadvantage2') === false && strpos($_SERVER['SERVER_NAME'], 'kellerwilliamsadvantage2') === false && strpos($_SERVER['SERVER_NAME'], 'kwlegacypikesville') === false  && strpos($_SERVER['SERVER_NAME'], 'kwclassicrealty') === false && strpos($_SERVER['SERVER_NAME'], 'kwsarasotasiestakey') === false && strpos($_SERVER['SERVER_NAME'], 'kwrealtyboise') === false && strpos($_SERVER['SERVER_NAME'], 'growwithkwrc') === false && strpos($_SERVER['SERVER_NAME'], 'demotl.') === false*/ Yii::app()->user->client->type != 'Brokerage'):

		/* Yii::import('stm_app.widgets.FeaturedCategoriesWidget.FeaturedCategoriesWidget');
		  $this->widget('stm_app.widgets.FeaturedCategoriesWidget.FeaturedCategoriesWidget', array(
		  'id' => 'FeaturedCategories',
		  'title' => 'Featured Categories',
		  )
		  ); */
		?>
		<!--/.sectionCategory-->


		<?php
		/* <div class="container main-container globalPaddingTop">

		  Yii::import('stm_app.widgets.SellerProgramWidget.SellerProgramWidget');
		  $this->widget('stm_app.widgets.SellerProgramWidget.SellerProgramWidget', array(
		  'id' => 'SellerProgram',
		  'title' => 'Seller Program',
		  )
		  );

		  Yii::import('stm_app.widgets.BuyerProgramWidget.BuyerProgramWidget');
		  $this->widget('stm_app.widgets.BuyerProgramWidget.BuyerProgramWidget', array(
		  'id' => 'BuyerProgram',
		  'title' => 'Buyer Program',
		  )
		  );

		  Yii::import('stm_app.widgets.CustomerReviewsWidget.CustomerReviewsWidget');
		  $this->widget('stm_app.widgets.CustomerReviewsWidget.CustomerReviewsWidget', array(
		  'id' => 'CustomerReviews',
		  'title' => 'Customer Reviews',
		  )
		  );

		  </div> */
		?>

		<?php
		/* Yii::import('stm_app.widgets.ParralaxWidget.ParralaxWidget');
		  $this->widget('stm_app.widgets.ParralaxWidget.ParralaxWidget', array(
		  'id' => 'bookNow',
		  'title' => '',
		  'view_file' => 'default'
		  )
		  ); */
		?>

		<div class="container main-container">
			<?php
			$boardName = Yii::app()->getUser()->getBoard()->getPrefixedName();
			$property = new MlsProperties($boardName, 'search');
			$property->status = 'Active';
			$property->listing_agent_id = Yii::app()->user->getAccountSettings()['agent_mls_num'];
			$featuredHomesDataProvider = $property->searchFull();
			$featuredHomesDataProvider->criteria->limit = 20;

            //@todo: this will be setting based once we get that setup - temp solution using if statement below
            if(strpos($_SERVER['SERVER_NAME'], 'salemhomesearchpro')!==false ){
                $featuredHomesDataProvider->criteria->order = 'price DESC, status_change_date DESC';
            }
            else {
                $featuredHomesDataProvider->criteria->order = 'status_change_date DESC, price DESC';
            }
			$featuredHomesDataProvider->criteria->offset = 0;
			$featuredHomesDataProvider->pagination = false;

            if($featuredHomesDataProvider->itemCount) {

                $this->widget('stm_app.widgets.ContainerWidget.ContainerWidget', array(
                'id' => 'TopAreasContainer',
                'title' => 'Featured Homes For Sale',
                'wrapper' => '
                    <div class="container">
                        <div class="row xsResponse">%s</div>
                    </div>
                    <!-- /.rows -->

                    <div class="row">
                        <div class="load-more-block text-center">
                            <a class="btn btn-primary btn-success btn-lg" href="/homes">
                                <i class="fa fa-plus-sign">+</i> Search All Homes on the Market - Click Here</a>
                        </div>
                    </div>',
                'containerClass' => 'morePost row featuredPostContainer style2 globalPaddingTop',
                'widgets' => array(
                    'stm_app.widgets.RowWidget.RowWidget' =>
                    array(
                        'id' => 'FeaturedHomes',
                        'perRow' => 2,
                        'additionalRowCssClasses' => array('item'),
                        'displayWidget' => 'stm_app.widgets.FeaturedHomesForSaleWidget.FeaturedHomesForSaleWidget',
                        'displayWidgetOptions' => array(),
                        'DataProvider' => $featuredHomesDataProvider,
                        'noResultsMessage' => "There are currently no results."
                    )
                ))); ?>
        <? } ?>
		</div>
		<!--main-container-->

		<?php
    endif;
		/* Yii::import('stm_app.widgets.ParralaxWidget.ParralaxWidget');
		  $this->widget('stm_app.widgets.ParralaxWidget.ParralaxWidget',
		  array(
		  'id' => 'whatsYourHomeWorth',
		  'title' => '',
		  'view_file' => 'whatsYourHomeWorth'
		  )
		  );

		Yii::import('stm_app.widgets.DialogWidget.DialogWidget');
		$this->widget('stm_app.widgets.DialogWidget.DialogWidget', array(
			'id' => 'HomeDetailsModal',
			'title' => ''
		));*/
