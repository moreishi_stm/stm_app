<?
Yii::app()->clientScript->registerCss('teamListRow', <<<CSS
    #profile #team-list div.row{
        padding: 35px 25px 25px 20px;
        border-bottom: 1px solid #F1F1F1;
        float: left;
    }
    #profile #team-list div.row:nth-child(odd) {
        background-color: #F1F1F1;
    }
    #profile #team-list div.row:hover {
        background-color: #DEE4FB;
    }
    #profile #team-list div.row:first-child {
        border-top: 1px solid #F1F1F1;
    }
    #profile h1 {
        margin-top: 20px;
    }
    #profile h3{
        color:#C20000;
        font-size: 22px;
        font-weight: bold;
    }
    #profile h4{
        font-size: 15px;
    }
    #profile #profile-photo{
        border: 2px #CCC solid;
    }
    #profile .row .bio{
        font-size: 13px;
        margin-bottom: 10px
    }
    #profile .row a.see-more{
        padding: 8px;
        font-size: 13px !important;
        padding-left: 0;
        margin-left: 0;
        color: black;
        display: inline-block;
        text-decoration: none;
        font-weight: bold;
    }
    #profile .row a.see-more:hover{
        text-decoration: underline;
    }
    #profile .row a.see-more em{
        margin-left: -4px;
        background-repeat: no-repeat !important;
        position: relative;
        top: 10px;
        margin-right: 4px;
        margin-top: -14px;
        background-image: url(http://cdn.seizethemarket.com/assets/images/search.png) !important;
        background-position: 8px 1px;
        display: inline-block;
        width: 24px;
        height: 24px;
    }
CSS
);
?>
<div id="profile" class="main-container no-padding no-margin">
    <div class="col-lg-9 col-md-9 col-sm-12 no-padding">
        <h1>Meet Our Team</h1>
        <?php
            $this->widget( 'front_module.components.StmListView', array(
                'dataProvider'         => $DataProvider,
                'id'                   => 'team-list',
                'template'			   => '{items}{pager}',
                'itemView'             => '_teamListRow',
                'itemsCssClass'        =>'datatables',
            ));
        ?>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
    <?
    $this->renderPartial('_sideBarHouseValues');

    /**
     * Sidebar Widget Goes Here
     */
    Yii::import('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget');
    $this->widget('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget', array(
            'areas' => [
                ['name'=>'Top Communities', 'tag'=>'Top Communities Sidebar'],
                ['name'=>'Top Areas','tag'=>'Top Areas Sidebar']
            ]
        ));
    Yii::import('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
    $this->widget('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
    ?>
    </div>
</div>