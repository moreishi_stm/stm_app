<? $SettingContactValues = new SettingContactValues;?>
<div class="row no-margin col-xs-12">
    <div class="col-md-3 col-xs-4">
        <a class="team-photo" href="/team/<?php echo $data->id?>/<?php echo str_replace(' ','-',strtolower($data->first_name)).'-'.str_replace(' ','-', strtolower($data->last_name));?>"><img id="profile-photo" class="team-list-photo" src="<?php echo Yii::app()->user->profileImageBaseUrl?>/<?php echo $data->id?>/<?php echo $SettingContactValues->getValue('profile_photo', $data->id)->value;?>" width="152" height="200"></a>
    </div>
    <div class="col-md-9 col-xs-12">
        <h3 class="name"><?php echo $data->fullName; ?></h3>
        <h4 class="title"><?php echo $SettingContactValues->getValue('title',$data->id)->value; ?></h4>
        <div class="bio">
            <?php
            $strMax = 395;
            $bio = $SettingContactValues->getValue('bio',$data->id)->value;
            if(!empty($bio)){
                if(strlen($bio) > $strMax) {
                    echo substr($bio, 0, strpos($bio, ' ', $strMax));
                    if(strlen($bio) > $strMax) echo '...';
                } else
                    echo $bio;
            }
            ?>
        </div>
        <?php $teamUrl = str_replace(' ','-',strtolower(trim($data->first_name))).'-'.str_replace(' ','-', strtolower(trim($data->last_name)));?>
        <a class="see-more text-button" href="/team/<?php echo $data->id?>/<?php echo $teamUrl; ?>"><em class="icon i_stm_search"></em>View <?=$data->first_name?>'s Full Profile</a>
    </div>
</div>