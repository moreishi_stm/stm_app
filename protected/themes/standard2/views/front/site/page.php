<?
    if (strpos(Yii::app()->controller->layout, 'column2') !== false) {
        $colSize = 9;
    }
    else {
        $colSize = 12;
        $hideCmsTags = true;
    }
?>
<div class="row transitionfx">
	<div class="col-lg-12 col-md-12 col-sm-12 page-layout no-padding">
        <?
        //@todo: to remove after bootstrap refactor, this is so it would add container class for listing storyboard
        $cmsContentContainerClass = (Yii::app()->params['layoutDirectory'] == 'storyboard') ? 'container': 'col-lg-'.$colSize.' no-padding';
        ?>
		<div id="cms-content-container" class="<?=$cmsContentContainerClass?>">
			<?php
				ob_start();
				eval(' ?>'.$pageContent.'<?php ');
				ob_end_flush();
			?>
            <?php if(!$hideCmsTags && !empty($cmsTags)): ?>
                <div class="col-lg-12" style="padding: 16px; border: 1px solid #CCC; font-size: 20px; margin: 30px 0; -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, 0.2); -moz-box-shadow: 0 0 8px rgba(0,0,0,0.2); box-shadow: 0 0 8px rgba(0, 0, 0, 0.2); background: #f8f8f8;
">
                Similar Topics:
                    <?php $allTags = '';
                    foreach($cmsTags as $cmsTag) {
                        $allTags .= ($allTags)? ', ' : '';
                        $allTags .= CHtml::link($cmsTag->name, '/topics/'.$cmsTag->url, $htmlOptions=array('style'=>'color: #2D78CC;'));
                    }
                    echo $allTags;
                    ?>
                </div>
            <?php endif; ?>

            <?php if($allow_comments) {
                $this->widget('front_module.components.widgets.CmsCommentWidget.CmsCommentWidget', array('commentPillView' => 'commentPill', 'commentEntryView' => '_commentEntry'));
            }
            ?>

        </div>

        <?if($colSize == 9):?>
        <div id="cms-sidebar-container" class="col-lg-3">
            <?
            $this->renderPartial('_sideBarHouseValues');

            Yii::import('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget');
            $this->widget('stm_app.widgets.FeaturedAreaSidebarWidget.FeaturedAreaSidebarWidget', array(
                    'areas' => [
                        ['name'=>'Top Communities', 'tag'=>'Top Communities Sidebar'],
                        ['name'=>'Top Areas','tag'=>'Top Areas Sidebar']
                    ]
                ));

            Yii::import('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
            $this->widget('stm_app.widgets.HomesByPriceSidebarWidget.HomesByPriceSidebarWidget');
            ?>
        </div>
    <? endif; ?>
    </div>
	<?php /*<div class="col-lg-3 col-md-12 col-sm-12">
		<?php $this->widget('front_module.components.widgets.WidgetFactoryWidget.WidgetFactoryWidget'); ?>
	</div>*/ ?>
</div>