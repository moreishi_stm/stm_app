<?php
$this->widget('admin_module.extensions.EChosen.EChosen', array(
	'target' => 'select#MlsProperties_county, select#MlsProperties_city, select#MlsProperties_neighborhood, select#MlsProperties_zip, select#MlsPropertiesSecondary_school, select#MlsPropertiesSecondary_masterbr_room_level',
	'options' => array('allow_single_deselect' => true)));

$js = <<<JS
        $(document).ready(function () {
            $("#MlsProperties_neighborhood").ajaxChosen({
                type: 'POST',
                url: '/searchAutocompleteNeighborhood',
                dataType: 'json'
            }, function (data) {
                return data;
            });
        });
JS;

Yii::app()->clientScript->registerScript('search-autocomplete-script', $js);

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'search-form'
));
?>
<style type="text/css">
	.chzn-container.chzn-container-single a {
		height: 28px;
		/*padding: 7px;*/
	}
	.chzn-container.chzn-container-single {
		font-size: 16px;
	}
	.chzn-container-single .chzn-single div b{
		top: 8px;
		position: relative;
	}
	.search-choice-close {
		top: 14px !important;
	}
	.form-group {
		max-width: 75%;
	}
	.form-group .form-control {
		margin: 5px 0;
	}
</style>
<div>
	<h1 style="width:540px;">Full Search</h1>
	<img src="http://cdn.seizethemarket.com/assets/images/search_magnifying_glass.png" width="170px;" style="right:10px; top:17px; position: absolute;">
	<hr/>
	<div>
		<div class="form-group">
			<?php
			echo $form->dropDownList($property, 'price_min', StmFormHelper::getPriceList(), $htmlOptions = array(
				'empty' => 'Price Min',
				'class' => 'form-control'
					)
			);
			?>
			<?php
			echo $form->dropDownList($property, 'price_max', StmFormHelper::getPriceList(), $htmlOptions = array(
				'empty' => 'Price Max',
				'class' => 'form-control'
					)
			);
			?>
			<?php
			echo $form->dropDownList($property, 'bedrooms', StmFormHelper::getBedroomList(), $htmlOptions = array(
				'empty' => 'Bedrooms',
				'class' => 'form-control'
					)
			);
			?>
			<?php
			echo $form->dropDownList($property, 'baths_total', StmFormHelper::getBathList(), $htmlOptions = array(
				'empty' => 'Baths',
				'class' => 'form-control'
					)
			);
			?>
		</div>
		<div class="form-group">
			<?php
			echo $form->dropDownList($property, 'sq_feet', StmFormHelper::getSqFeetList(), $htmlOptions = array(
				'empty' => 'Sq. Feet',
				'class' => 'form-control'
					)
			);
			?>
			<?php
			echo $form->dropDownList($property, 'year_built', StmFormHelper::getYrBuiltList(), $htmlOptions = array(
				'empty' => 'Yr Built',
				'class' => 'form-control'
					)
			);
			?>
			<?php
			$property->mls_property_type_id = ($property->mls_property_type_id) ? $property->mls_property_type_id : 1;
			$mlsPropertyTypeListData = CHtml::listData(MlsPropertyTypeBoardLu::model()->with('propertyType')->findAllByAttributes(array(
								'mls_board_id' => Yii::app()->getUser()->getBoard()->id,
								'status_ma' => MlsPropertyTypeBoardLu::ACTIVE,
							)), 'mls_property_type_id', 'displayName');
			echo $form->dropDownList($property, 'property_type', $mlsPropertyTypeListData, $htmlOptions = array(
				'empty' => 'Property Type',
				'class' => 'form-control'
					)
			);
			?>
		</div>


		<div class="form-group">
			Keywords:<br/>
			<?php echo $form->textField($property, 'keywords', $htmlOptions = array('class' => 'form-control', 'placeholder' => 'Keyword, MLS #, Neighborhood, City, Street Name, Zip')); ?>
		</div>
		<div class="form-group">
			Neighborhood:<br/>
			<?php
			echo $form->dropDownList($property, 'neighborhood', array(), $htmlOptions = array(
				'data-placeholder' => 'Type a Neighborhood...',
				'empty' => '',
				'class' => 'form-control',
			));
			?>
		</div>
		<div class="form-group">
			County:<br/>
			<?php
			echo $form->dropDownList($property, 'county', (array) MlsPropertyFormHelper::getInstance()->getCountyOptions(), $htmlOptions = array(
				'data-placeholder' => 'Select or Type a County...',
				'empty' => '',
				'class' => 'form-control',
			));
			?>
		</div>
		<div class="form-group">
			School:<br />
			<?php
			echo $form->dropDownList($secondaryProperty, 'school', (array) MlsPropertyFormHelper::getInstance()->getSchoolOptions(), $htmlOptions = array(
				'data-placeholder' => 'Select or Type a School...',
				'empty' => '',
				'class' => 'form-control',
			));
			?>
		</div>
	</div>
	<div class="form-group">
		Street #:<br/>
		<?php echo $form->textField($property, 'street_number', $htmlOptions = array('class' => 'form-control')); ?>
		Street Name:<br/>
		<?php echo $form->textField($property, 'street_name', $htmlOptions = array('class' => 'form-control')); ?>
		(Rd, St, Blvd)<br/>
		<?php echo $form->textField($property, 'street_suffix', $htmlOptions = array('class' => 'form-control')); ?>
		City:<br/>
		<?php
		echo $form->dropDownList($property, 'city', (array) MlsPropertyFormHelper::getInstance()->getCityOptions(), $htmlOptions = array(
			'data-placeholder' => 'Type a City...',
			'empty' => '',
			'class' => 'form-control',
		));
		?>
		Zip:<br/>
		<?php
		echo $form->dropDownList($property, 'zip', (array) MlsPropertyFormHelper::getInstance()->getZipOptions(), $htmlOptions = array(
			'data-placeholder' => 'Select or Type a Zip Code...',
			'empty' => '',
			'class' => 'form-control',
		));
		?>
	</div>

	<?php //TOTAL hack but temp fix for now as we will re-do front site soon + need some time to think of better scalable design
	if (strpos($_SERVER['SERVER_NAME'], 'myashevillerealestate.com') !== false):
		?>

		<div class="form-group">
			Master Bedroom Floor Level:<br/>
			<?php
			echo $form->dropDownList($secondaryProperty, 'masterbr_room_level', array(1 => 'Master Bedroom on 1st Floor', 2 => 'Master Bedroom on 2nd Floor', 3 => 'Master Bedroom on 3rd Floor'), $htmlOptions = array(
				'data-placeholder' => 'Select a Floor Level',
				'empty' => '',
				'class' => 'form-control',
			));
			?>
		</div>

<?php endif; ?>

	<div class="form-group" style="text-align:center;">
		<input name="LoginForm[email]" id="LoginForm_email" type="submit" value="Search Now" class="btn btn-primary">
	</div>
</div>
<?php
$this->endWidget();
