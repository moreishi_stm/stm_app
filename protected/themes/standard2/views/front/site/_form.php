<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>false,
	'action'=>'/login',
));
?>
<hr />
<div class="row">
	<?php echo $form->labelEx($model,'email'); ?>
	<?php echo $form->textField($model,'email'); ?>
	<?php echo $form->error($model,'email'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model,'password'); ?>
	<?php echo $form->passwordField($model,'password'); ?>
	<?php echo $form->error($model,'password'); ?>
</div>
<div class="row submit">
	<button id="login-form-button" class="btn btn-primary" type="submit">Login</button>
    <?php echo CHtml::link('Forgot Password','/forgotPassword',$htmlOptions=array('id'=>'forgot-password','class'=>'link','style'=>'')); ?>
</div>
<? $this->endWidget(); ?>