<?php
Yii::app()->clientScript->registerCssFile($this->module->cdnUrl . 'assets/css/idangerous.swiper.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->controller->module->getCdnAssetUrl().'/css/jquery.tipsy.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->module->getCdnAssetUrl().'/js/stm_TipsyFollow.js', CClientScript::POS_END);
//Yii::app()->clientScript->registerCssFile('http://cdn.seizethemarket.'.((YII_DEBUG) ? 'com': 'local').'/assets/js/owlcarousel/2.0.0/owl.carousel.css');
$contactId = Yii::app()->user->id;
$listingId = Yii::app()->request->getQuery('id');

$addRemoveClass = ($isFavorite) ? 'remove-favorites' : 'add-favorites';
$addRemoveButtonClass = ($isFavorite) ? 'fa fa-remove' : 'fa fa-star';
$addRemoveText = ($isFavorite) ? 'Saved' : 'Save to Favorite';

$photoContent['thumbnails'] = json_decode($photoContent['thumbnails']);

$curentSharePage = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
$formattedAddress = $model->formatMlsAddress($model);
Yii::app()->controller->fullWidth = true;
?>
	<style>

		.banner {
			margin-top: 0;
		}
		#home-detail {
			margin-top: 10px;
		}
		#home-detail .spec {
			font-size: 16px;
			color: #000;
			padding-left: 12px;
			padding-right:12px;
		}

		#home-detail .spec.address{
			font-size: 36px;
			min-height: 50px;
			line-height: 40px;
			margin:12px 0;
		}

		#home-detail .feature_list {
			list-style: none;
		}
		#home-detail .feature_list li{
			float:left;
			width:210px;
			margin-bottom: 8px;
			font-size: 13px;
		}
		#home-detail .feature_list li:not(:first-of-type){
			margin-left: 32px;
		}
		#home-detail .feature_list label{
			display: block;
			border-bottom:none;
			font-weight: bold;
			line-height: 18px;
		}
		#home-detail .sidebar{
			float: right;
			width: 205px;
		}
		#home-detail .sidebar .side-container{
			width: 205px;
			position: relative;
			float: right;
			margin-top: 15px;
			margin-right: -20px;
			background: #fafafa;
			border: 1px solid #e5e5e5;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			padding: 10px 10px 10px 10px;
		}
		#home-detail .sidebar .request-container{
			background-color: #FFF8E6;
			border: 1px solid #FFB62E;
		}
		.listing-office {
			font-size: 13px;
			color: #777;
			text-align: right;
		}


		@media (max-width: 992px) {
			.row.cabuttons a.btn {
				width: 100%;
				margin: 2px 0;
			}
		}

		.productThumb{
			max-height: 200px;
			min-height: 75px;
			overflow: hidden;
			overflow-y: auto;
			margin-bottom: 10px;
		}
		.product-tab.w100 {
			padding-top: 10px;
			margin-top: 10px;
			border-top: 1px solid #dddddd;
		}
		/*.product-share {
			margin-top: 0;
			margin-bottom: 10px;
		}*/
		#SimilarProductSlider {
			position: relative;
		}
		#SimilarProductSlider .product{
			min-height: 220px;
			max-width: 220px;
			background: #F3F3F3;
			color: #333;
		}
		#SimilarProductSlider .product a:hover {
			text-decoration: none;
			color: #333;
		}
		#SimilarProductSlider .description{
			padding-top: 15px;
		}
		#SimilarProductSlider .item .price {
			text-align: center;
			margin-left: 0px;
		}

		#SimilarProductSlider .product-image img {
			height: 145px;
		}
		#home-detail .details-description .spec:first-of-type {

		}


		@media (max-width: 768px) {
			.headerOffset {
				padding-top: 145px;
			}
			.row.cabuttons a.btn {
				width: 100%;
				margin: 5px 0;
			}
		}

		.home-details-dialog h3{
			font-size: 18px;
		}
		.home-details-dialog .property-container{
			float: left;
		}
		.home-details-dialog div.property-image{
			width: 280px;
			vertical-align: top;
			float: left;
		}
		.home-details-dialog div.property-info{
			width: 245px;
			float: left;
			padding-top: 20px;
			padding-left: 20px;
		}
		.home-details-dialog .price{
			font-size: 16px;
			color: #3C0;
			font-weight: bold;
			margin: 5px 0;
			float: left;
		}
		.home-details-dialog .details{
			clear: both;
			font-size: 13px;
			font-weight: normal;
		}
		.home-details-dialog .dialog-form-container{
			clear: both;
			width: 100%;
			padding-top: 15px;
		}
		.home-details-dialog .dialog-form-container table{
			width: 100%;
		}
		.home-details-dialog .dialog-form-container table th{
			width: 150px;
			text-align: right;
			padding-right: 5px;
		}
		.home-details-dialog hr{
			margin-left: auto;
			margin-right: auto;
			width: 90%;
		}

		.home-details-dialog .submit-button-row{
			padding: 23px 15px 15px 15px;
			text-align: center;
			position: relative;
		}
		.home-details-dialog .home-details-dialog-submit-button{
			text-transform: uppercase;
			font-size: 11px;
			padding-left: 18px;
			padding-right: 18px;
			cursor: pointer;
			width: 200px;
		}
		.home-details-dialog .home-details-dialog-loading{
			width: 32px;
			height: 32px;
			text-align: center;
			position: absolute;
			margin-left: -15px;
			top: -6px;
			left: 50%;
		}
		.home-details-dialog .home-details-dialog-loading.loading{
			background: url('http://cdn.seizethemarket.com/assets/images/loading.gif') no-repeat;
			opacity: 1 !important;
			position: relative;
		}
		.home-details-dialog .dialog-form-container table, .home-details-dialog .dialog-form-container table th, .home-details-dialog .dialog-form-container table td{
			background: none;
			border: none;
		}

		.home-details-dialog h3{
			font-size: 18px;
		}
		.home-details-dialog .property-container{
			float: left;
		}
		.home-details-dialog div.property-image{
			width: 280px;
			vertical-align: top;
			float: left;
		}
		.home-details-dialog div.property-info{
			width: 245px;
			float: left;
			padding-top: 20px;
			padding-left: 20px;
		}
		.home-details-dialog .price{
			font-size: 16px;
			color: #3C0;
			font-weight: bold;
			margin: 5px 0;
			float: left;
		}
		.home-details-dialog .details{
			clear: both;
			font-size: 13px;
			font-weight: normal;
		}
		.home-details-dialog .dialog-form-container{
			clear: both;
			width: 100%;
			/*padding-top: 15px;*/
		}
		.home-details-dialog .dialog-form-container table{
			width: 100%;
		}
		.home-details-dialog .dialog-form-container table th{
			width: 150px;
			text-align: right;
			padding-right: 5px;
		}
		.home-details-dialog hr{
			margin-left: auto;
			margin-right: auto;
			width: 90%;
		}

		.home-details-dialog .submit-button-row{
			padding: 23px 15px 15px 15px;
			text-align: center;
			position: relative;
		}
		.home-details-dialog .home-details-dialog-submit-button{
			text-transform: uppercase;
			font-size: 11px;
			padding-left: 18px;
			padding-right: 18px;
			cursor: pointer;
			width: 200px;
		}
		.home-details-dialog .home-details-dialog-loading{
			width: 32px;
			height: 32px;
			text-align: center;
			position: absolute;
			margin-left: -15px;
			top: -6px;
			left: 50%;
		}
		.home-details-dialog .home-details-dialog-loading.loading{
			background: url('http://cdn.seizethemarket.com/assets/images/loading.gif') no-repeat;
			opacity: 1 !important;
			position: relative;
		}
		.home-details-dialog .dialog-form-container table, .home-details-dialog .dialog-form-container table th, .home-details-dialog .dialog-form-container table td{
			background: none;
			border: none;
		}


		.recommended {
			margin-bottom: 10px;
			padding-top: 60px;
			padding-bottom: 60px;
			border-top: 0;
		}
		#SimilarProductSlider .item {
			padding: 0px;
			float: left;
		}
		.socialIcon {
			margin-left: 10px;
			float: left;
			padding-top: 10px;
		}
		.nextControl {
			right: 40px;
		}
		.prevControl, .nextControl,
		.prevControl i, .nextControl i {
			cursor: pointer;
		}



		.recommended {
			margin-bottom: 10px;
		}
		#SimilarProductSlider .item {
			padding: 0px;
			float: left;
		}
		.socialIcon {
			margin-left: 10px;
			float: left;
			padding-top: 10px;
		}
		.nextControl {
			right: 40px;
		}
		.prevControl, .nextControl,
		.prevControl i, .nextControl i {
			cursor: pointer;
		}

		.form-container {
			padding: 60px 30px;
			margin: 20px 0;
			background: #EFEFEF;
		}
		.form-container h2 {
			font-size: 40px;
		}
		.form-container .submit {
			width: 260px;
			font-size: 30px;
			float:right;
			border-radius: 0;
		}
		.form-container .submit:hover, .request-showing-button:hover {
			background-color: #25ad29;
		}
		.form-contact-container, #thank-you-message {
			font-size: 20px;
		}
		.form-contact-container h3, #thank-you-message h3{
			font-size: 30px;
		}
		#thank-you-message img {
			height: 250px;
			float: left;
			margin-right: 30px;
		}
		.form-contact-container .office-name {
			margin-top: 10px;
		}
		.form-contact-container .agent-name {
			margin-top: 30px;
		}
		.form-contact-container .profile-photo-container {
			min-height: 20px;
		}
		.form-contact-container .profile {
			max-height: 150px;
			margin-bottom: 10px;
		}

		.thumbnail-container {
			overflow-x: scroll;
		}

		.ui-widget form input[type="submit"] {
			width: 100%;
			padding: 14px;
			font-size: 14px;
		}
		.add-fav{
			top:4px;
			right:10px;
			position: relative;
			margin-left: 8px;
		}

		/**
		 * jamesk
		 */
		.details-description .info{
			padding:8px;
		}
		.hidden-data{
			display:none;
		}
		.spec{
			position: relative;
		}
		.info .spec:not(:first-child)::before {
			content: "";
			height: 12px;
			width: 2px;
			position: absolute;
			bottom: -11px;
			left: 0;
			background-color: #000;
		}
		#mortgage-calculator-tab{
			background: #000;
			position: relative;
		}
		#mortgage-calculator-tab h1 a{
			color: #fff;
		}

		#home-detail .features .row{
			margin-bottom: 24px;
			padding-bottom: 12px;
			border-bottom: 1px solid;
		}

		.dual-map{
			float:left;
			width:50%;
		}
		.map{
			width:100%;
			margin:60px auto;
			height: 400px;
		}
		.spec ul{
			padding:0;
			width:100%;
		}
		.spec ul li{
			display: list-item;
			float:left;
			font-size: 16px;
		}
		.spec ul li:not(:first-of-type):before{
			font-family: FontAwesome;
			content: '\f04d';
			font-size: 4px;
			top: -2px;
			position: relative;
			margin-right: 8px;
		}
		.spec ul li:not(:first-of-type) {
			margin-left: 8px;
		}
		.spec ul li:last-of-type{

		}
		#property-details-container .details-description-border{
			border-bottom: 2px solid;
		}
		.border-bottom{
			border-bottom: 1px solid #ccc;
			margin-top: 24px;
			padding-bottom: 16px;
		}
		#home-detail .spec.location{
			font-size: 14px;
		}
		.request-showing-button{
			background-color: #000;
		}
		@media (max-width: 992px) {
			.form-contact-container{
				padding-left: 0;
				padding-right: 0;
			}
			#home-detail .row{
				margin: 4px auto !important;
			}

			.form-container .submit,
			.request-showing-button{
				float:none;
			}
			.form-container h2{
				font-size: 24px;
			}
			.listing-office{
				text-align: left;
			}

			#home-detail .details-description .spec:first-of-type{
				padding-left: 0;
			}

			#home-detail .details-description .spec{
				text-align: left;
				font-size: 20px;
				font-weight: 400;
			}
			#home-detail .details-description .spec .location{
				margin-bottom: 12px !important;
			}
			#home-detail .address{

				font-weight: 900;
			}
			#home-detail .details-description .info .spec{
				font-size: 14px;
				float: left;
				text-align: left;
				padding-right: 6px;
				padding-left: 6px;
				padding-bottom:6px;
			}
			#home-detail .details-description .info .spec:first-child {
				paddingl-left: 0;
			}
			.info .spec:not(:first-child)::before {
				height:8px;
				bottom: 0;
				left: 0;
			}
			#home-detail .details-description .info{
				margin-bottom: 4px;
				text-align: center;
			}
			#HomeDetailsImageSlider .owl-item{
				height:240px !important;
			}
			#HomeDetailsImageSlider .product-image img{
				height:240px !important;
				max-height:240px !important;
			}

			.clearfix:before,
			.clearfix:after {
				content: "";
				display: table;
			}
			.clearfix:after {
				clear: both;
			}
			.clearfix {
				zoom: 1; /* For IE 6/7 (trigger hasLayout) */
			}
			.dual-map{
				float:none;
				width:90%;
			}
			#home-detail .feature_list li:not(:first-of-type) {
				margin-left: 0;
			}

			.ctabuttons a{
				display: none;
			}
			.ctabuttons{

				background: white;
				border-top: 1px solid rgba(0,0,0,0.1);
				display:block;
				position: fixed;
				bottom: 0px;
				left: 0px;
				right: 0px;
				background: #ffffff;
				z-index: 999;
			}
			.ctabuttons ul li:last-of-type {
				text-align: center;
			}
			.spec ul li{
				font-size: 14px;
			}
			.submit{
				width:98%;
				margin:8px 0;
			}
		}
		#featureSectionDescription div.short {
			overflow: hidden;
			max-height: 64px;
		}
		#video-form .request_showing_form_button{
			width:100%;
		}
		.feature-section{
			padding-top:60px;
			padding-bottom:60px;
		}
		.feature-section h3{
			font-size:24px;
			padding-bottom: 24px;
		}
		.feature-section li{
			list-style: none;
		}
		.feature-section li ul{
			margin-bottom: 8px;
		}
		.feature-section li ul li{
			margin-left: 8px;;
		}
		.feature-section li ul li::before{
			font-family: FontAwesome;
			content: "\f105 \00a0";
		}
		.feature-section a.show-more{
			font-size: 16px;
			width:124px;
		}
		.feature-section a.show-more i{
			margin-left: 8px;
		}
		.feature-section .show-more

		#SimilarProductSlider{
			position: relative;
		}
		#SimilarProductSlider .owl-buttons{
			position: relative;
		}
		#SimilarProductSlider .owl-buttons .owl-prev {
			position:absolute;
			top:-244px;
			left:6px;
			font-size: 72px;
			padding: 12px;
			border-radius:0;
		}
		#SimilarProductSlider .owl-buttons .owl-next {
			position:absolute;
			top:-244px;
			right:16px;
			font-size: 72px;
			padding: 12px;
			border-radius:0;

		}
	</style>
<?php

echo $this->renderPartial(
	'homeDetails/_topSlider', array(
	'photoContent' => $photoContent,
));
?>

	<div class="transitionfx" id="home-detail" >
		<!-- right column -->
		<div id="property-details-container" >
			<div class="container">
				<?php echo $this->renderPartial('homeDetails/_actionButtons', array(
					'mlsBoardId' => $mlsBoardId,
					'isFavorite' => $isFavorite,
					'contactId' => $contactId,
					'listingId' => $listingId
				));
				?>
				<div class="details-description col-lg-6 clearfix">
					<div class="clearfix">
						<div class="spec location" style="margin-bottom: 8px;">
							<ul>
								<li><?php echo ($model['common_subdivision']) ?  Yii::app()->format->formatProperCase($model['common_subdivision']) : ''; ?></li>
								<li><?php echo $model->getCityStZip();?></li>
							</ul>
						</div>
						<div class="clearfix" style="min-width: 100%;">&nbsp;</div>
						<div class="spec address col-lg-6 no-padding clearfix" style="margin-bottom: 12px">
							<?php echo $model->getStreetAddress();?>
						</div>
					</div>
				</div>

			</div>
			<div class="details-description details-description-border clearfix">
				<div class="clearfix"></div>
				<div class="container">
					<div class="info">
						<?php
						if($model['bedrooms']){ ?>
							<span class="spec">
								<?php echo $model['bedrooms'] ?> BD
							</span>
						<?php }
						?>

						<?php
						if($model['baths_total']){ ?>
							<span class="spec">
						<?php echo $model['baths_total'] ?> BA
					</span>
						<?php }
						?>

						<?php
						if($model['sq_feet']){ ?>
							<span class="spec">
								<span>
									<?php echo ((Yii::app()->user->getBoard()->is_sq_ft_range) ? $model['sq_feet'] : Yii::app()->format->formatNumber($model['sq_feet'])) ?>
									SF
								</span>
							</span>
						<?php }
						?>
						<span class="spec">
						<?php echo (strpos($_SERVER['SERVER_NAME'],'findhomesinpdx') !== false)? $model['contingent'] : Yii::app()->format->formatDollars($model['price']); ?>
					</span>
					<span class="spec">
						<a href="javascript:void(0)" class="mortgageCalcToggle" onclick="calculatorToggle();" title="Mortgage Calculator">
							<i class="fa fa-calculator"></i>
						</a>
					</span>
					</div>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div id="mortgage-calculator-tab" class="row" style="display:none;"></div>
		<div class="container">
			<?php
			$this->renderPartial(
				'homeDetails/_propertyDetails', array(
					"model" => array(
						array(
							"label" => "Exterior Features",
							"data" => $exteriorFeatures
						),
						array(
							"label" => "Interior Features",
							"data" => $interiorFeatures
						),
						array(
							"label" => "Property Features",
							"data" => $propertyFeatures
						)
					)

				)
			);
			?>

			<div class="features clearfix">
				<div id="featureSectionDescription" class="border-bottom clearfix feature-section">

					<div class="col-sm-3 col-md-3 col-lg-3">
						<h3>Property Description</h3>
						<a class="show-more">Show More <i class="fa fa-angle-down"></i> </a>
					</div>
					<div class="col-sm-8 col-md-8 col-lg-8 content short">
						<?php echo $model->public_remarks; ?>
					</div>
				</div>
				<?php
				if(isset($schoolFeatures) && !empty($schoolFeatures)){ ?>
					<div id="featureSectionSchool" class="border-bottom clearfix feature-section">
						<div class="col-sm-3 col-md-3 col-lg-3">
							<h3>School Information</h3>
						</div>
						<div class="col-sm-8 col-md-8 col-lg-8">
							<ul class='feature_list'>
								<?php
								if (isset($schoolFeatures)) {
									ksort($schoolFeatures);
									foreach($schoolFeatures as $label=>$value) {
										echo '<li><label>',$label,'</label>',  $value,'</li>';
									}
								} else {
									echo 'No data found...';
								}
								?>
							</ul>
						</div>
					</div>
				<?php }
				?>
			</div>
			<div style="clear:both;"></div>
			<?php $this->renderPartial('homeDetails/_googleMap',array("MlsProperties" => $model)); ?>
			<div class="listing-office">
				Courtesty of <?=ucwords(strtolower($model->getListingOfficeName()))?>
			</div>
		</div>


		<div id="showing-request-form" class="form-container" style="margin:20px auto;">
			<?php $this->renderPartial("homeDetails/_requestShowingForm",array("model"=>$model)); ?>
		</div>

		<?php
		if(count($similarProperties) > 0){ ?>
			<div class="row recommended col-lg-12">
				<div class="container main-container">
					<h1> Similar Properties (<?= count($similarProperties); ?>) </h1>
					<div id="SimilarProductSlider">
						<?php foreach ($similarProperties as $similarProperty):
							if(empty($similarProperty['photoUrl'])){
								$similarProperty['photoUrl'] = "http://cdn.seizethemarket.com/assets/images/default-placeholder.png";
							}
							?>
							<div class="item">
								<div class="product">
									<a class="product-image" href="<?= $similarProperty['propertyUrl']; ?>">
										<img src="<?= $similarProperty['photoUrl']; ?>">
										<div class="description">
											<div class="price">
												<span><?= Yii::app()->format->formatDollars($similarProperty['price']); ?></span>
											</div>
											<div class="price">
										<span>
											<?=($similarProperty['bedrooms']) ? $similarProperty['bedrooms']." Bed" : ""; ?>
											<?=($similarProperty['baths_total']) ? " | ".$similarProperty['baths_total']." Bath" : ""; ?>
										</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
						<?php /*</div>*/ ?>
					</div>
				</div>
				<!--/.recommended-->
			</div>
			<?php
		}
		?>

	</div>
	<div style="clear:both"></div>
<?php
if($isFavorite){
	$favContent = "<span><i class=\"material-icons\">favorite</i></span>Favorite";;
} else {
	$favContent = "<span><i class=\"material-icons\">favorite_border</i></span>Add to Favorites";
}
?>
<?php
Yii::app()->clientScript->registerScript('homeDetailsJs', <<<JS
	$('.cta-button-tip').tipsy({ gravity: 's', title: 'data-tooltip',offset : 100});
    $('#print-flyer-button').on('click', printFlyer);
	function printFlyer() {
		window.open('/printFlyer/{$listingId}', 'Home Details Flyer', 'width=785,height=700,scrollbars=no');
	}

    //http://stackoverflow.com/questions/21978098/item-alignment-within-an-image-carousel
	$("#SimilarProductSlider").owlCarousel({
        navigation: true,
        navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>' ]
    });

	$('#SimilarProductSlider .item .product').responsiveEqualHeightGrid();

	var mySwiper = new Swiper('.swiper-container', {
        pagination: '.box-pagination',
        keyboardControl: true,
        paginationClickable: true,
        slidesPerView: 'auto',
        autoResize: true,
        resizeReInit: true
    });

	var calcLoaded = false;
	var calcOpen = false;

	function calculatorUpdateFields(){
		var downPayment = $('#down-payment').val();
		downPayment = downPayment.replace('%','');
		var percent = downPayment;
		percent = (1-(percent /100));

		var loanAmount = $('#price').val();
		loanAmount = loanAmount.replace('$','');
		loanAmount = loanAmount.replace(/,/g,'');
		loanAmount = loanAmount * percent;
		var interest = $('#interest').val();
		interest = interest.replace('%','');
		interest = interest /100/12;
		var months = $('#years').val();

		var payment = Math.round((loanAmount*interest)/(1-Math.pow(1+interest,(-1*months)))*100)/100;

		$('#monthly-payment').html('$'+payment.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

		var totalMortgage = (payment * months);

		$("#mortgage-amount").html("$"+totalMortgage.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

		var price = $("#price").val();
		price = price.replace('$','');
		price = price.replace(/,/g,'');
		downPayment = (parseFloat(downPayment) / 100);
		downPayment = (parseFloat(price) * parseFloat(downPayment));

		$("#down-payment-amount").html("$"+downPayment.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
	}
	function calculatorToggle(){
		if(calcOpen == true) {
			stmHomeDetailsScrollTo("#home-detail");
			$("#mortgage-calculator-tab").hide();
			calcOpen = false;
			return ;
		}
		$("#mortgage-calculator-tab").show();
		calcOpen = true;
		stmHomeDetailsScrollTo("#mortgage-calculator-tab");
		if(calcLoaded) {
			return;
		}
		$.get( "/mortgagecalculator/$model->price", function( data ) {
			$("#mortgage-calculator-tab").html( data );
			calcLoaded = true;
			calculatorUpdateFields();
			$('#mortgage-calcualtor-fields input, #mortgage-calcualtor-fields select').on('change',function() {
				calculatorUpdateFields();
			});
		});
	}
	$("#mortgageCalcToggle").on("click",function(e){
		e.preventDefault();
		return calculatorToggle();
	});

	$("#map-fav-link").on("click",function(e){
		e.preventDefault();
		return stmHomeDetailsScrollTo("#map-tab");
	});
	$(".feature-section .show-more").on("click",function(){
		var element = "#"+$(this).closest(".feature-section").attr("id");
		if ( element == "#featureSectionDescription") {
			$(element+" .content").toggleClass( "short" );
			if( $(element+" .content").hasClass("short") ){
			console.log("is short");
				$(this).html("Show More <i class=\"fa  fa-angle-down\"></i>");
			} else {
				$(this).html("Show Less <i class=\"fa  fa-angle-up\"></i>");
			}
			return;
		}
		element = element +" .hidden-data";
		$(element).toggle();
		if( $(element).is(":visible") ){
			$(this).html("Show Less   <i class=\"fa  fa-angle-up\"></i>");
		} else {
			$(this).html("Show More <i class=\"fa  fa-angle-down\"></i>");
		}
	});

	$(".scrollTo").on("click",function(e){
		var t = $(this).attr("href");
		if(t){
			e.preventDefault();
			stmHomeDetailsScrollTo(t);
		}

	})




	function stmHomeDetailsScrollTo(id){
		var tm = parseInt($(".navbar-fixed-top:first").innerHeight());
		$("html,body").animate(
			{ scrollTop: parseInt( $(id).offset().top - tm - 48 )}
		);
	}


JS
	, CClientScript::POS_END);
?>
	<div id="mls-disclosure" style="color: #666; font-size: 11px; line-height: 1.3;">
		<?php $this->renderPartial('_mlsDisclosure'); ?>
	</div>

<?php
echo $this->renderPartial('homeDetails/_dialogs', array('model' => $model, 'viewsMaxReached' => $viewsMaxReached));
