<style>
    textarea {
        height: auto;;
    }
    h1 {
        margin-top: 30px;
    }
    .about-agent {
        margin-top: 35px;
        padding-left: 0;
    }
    .form-container, .message-container, .success-message-container {
        margin-top: 30px;
        margin-bottom: 30px;
    }
    .success-message-container {
        background: #d1ffd1;
        padding-left: 40px;
    }
    .message-container h2 {
        text-align: center;
        margin-bottom: 20px;
        width: 100%;
    }
    .contact-message {
        padding: 20px 40px 40px;
        font-size: 15px;
    }
</style>
    <h1>Refer an Agent</h1>
        <?
        $SubmissionValues = new FormSubmissionValues(Forms::RECRUIT_REFERRAL);
        $FormFields = new FormFields;
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contact-us-form',
            'action' => array('/front/forms/recruiting/formId/'.Forms::RECRUIT_REFERRAL),
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnChange' => false,
                'validateOnSubmit' => true,
                'beforeValidate' => 'js:function(form, attribute) {
                        $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                        return true;
                    }',
                'afterValidate' => 'js:function(form, data, hasErrors) {
                    if (!hasErrors) {

                        $(".form-container").hide();
                        $(".success-message-container").show("normal");
                    }

                    $("div.loading-container.loading").remove();
                    return false;
                }',
            ),
        ));
        ?>

    <div class="form-container col-lg-6 col-xs-12 no-padding">

        <h2>Your Information</h2>

        <?php $SubmissionValues->data[FormFields::SOURCE_DESCRIPTION_ID] = 'Refer an Agent Form'; ?>
        <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_DESCRIPTION_ID.']'); ?>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('referrer_first_name')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('referrer_first_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Your First Name'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('referrer_last_name')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('referrer_last_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Your Last Name'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('referrer_email')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('referrer_email')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Your Email'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('referrer_phone')->id . ']'); ?>
            <?php $this->widget('StmMaskedTextField', array(
                    'model' => $SubmissionValues,
                    'attribute' => 'data[' . $FormFields->getField('referrer_phone')->id . ']',
                    'mask' => '(999) 999-9999',
                    'id' => 'FormSubmissionValues_data_'.FormFields::REFERRER_PHONE_ID,
                    'htmlOptions' => array('class' => 'col-xs-12', 'placeholder' => 'Your Phone'),
                )); ?>
        </div>

        <h2 class="col-sm-12 col-xs-12 about-agent">About the Agent</h2>

        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Agent First Name'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Agent Last Name'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Agent Email'));?>
        </div>
        <div class="col-sm-6 col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
            <?php $this->widget('StmMaskedTextField', array(
                    'model' => $SubmissionValues,
                    'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                    'mask' => '(999) 999-9999',
                    'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                    'htmlOptions' => array('class' => 'col-xs-12', 'placeholder' => 'Agent Phone'),
                )); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('is_licensed').']'); ?>
            <?php echo $form->dropDownList($SubmissionValues,  'data['.$FormFields->getFieldIdByName('is_licensed').']', array(1=>'Yes, is licensed', 0=>'No, is not licensed'), $htmlOptions=array('empty'=> 'Select License Status', 'class' => 'col-xs-12'));?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('relationship').']'); ?>
            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getFieldIdByName('relationship').']', array('Co-op'=>'Co-op','Networking'=>'Networking','Personal Sphere'=>'Personal Sphere','Family'=>'Family','Other'=>'Other'), $htmlOptions=array('empty'=> 'How do you know them?', 'class' => 'col-xs-12')); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('is_expecting_call').']'); ?>
            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getFieldIdByName('is_expecting_call').']', array(1=>'Yes, expecting a Call',0=>'No, not expecting a call'), $htmlOptions=array('empty'=> 'Expecting a Call?', 'class' => 'col-xs-12')); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getFieldIdByName('current_brokerage').']'); ?>
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getFieldIdByName('current_brokerage').']', $htmlOptions=array('placeholder'=> 'Current Brokerage', 'class' => 'col-xs-12')); ?>
        </div>
        <div class="col-xs-12 no-padding">
            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('comments')->id . ']'); ?>
            <?php echo $form->textArea($SubmissionValues, 'data[' . $FormFields->getField('comments')->id . ']', $htmlOptions = array('class' => 'col-xs-12', 'rows'=>4, 'placeholder' => 'What should we know about your referral? (Ex. What are their challenges?)'));?>
        </div>
        <div class="col-xs-12 no-padding">
            <button id="submit-button" type="submit" class="submit col-xs-12 btn btn-lg btn-primary btn-success" stmtemplate-title="Submit Button Text" style="">SUBMIT NOW</button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <div class="success-message-container col-lg-6 col-xs-12" style="display: none;">
        <h1 style="margin-bottom: 20px;">Thank You!</h1>
        <h2 style="margin-bottom: 20px;">Your referral was sent successfully.</h2>
        <h2 style="margin-bottom: 20px;">If you need immediate assistance,<br>Call <?php echo Yii::app()->user->settings->office_phone; ?>.</h2>
        <h2 style="margin-bottom: 20px;">Thank you and have a great day!</h2>
    </div>

    <div class="col-lg-6 col-xs-12 message-container text-center">
        <? if(Yii::app()->user->clientId != 44):?>
        <img class="" src="http://cdn.seizethemarket.com/assets/images/referrals-red-marker.jpg" alt=""/>
        <h2 class="col-lg-12 col-xs-12">Thank you for your Referral!</h2>
        <? else: ?>
        <h2 class="col-lg-12 col-xs-12" style="padding: 60px; text-align: left;">
            Thank you for your commitment to transforming lives through real estate.
            <br><br>When you introduce non-KW associates to our great company and our great models, system and tools, you not only provide them with an opportunity to reach their goals, you're also helping yourself and your family reach your wealth-building goals.
            <br>
            <br>Thank you!
        </h2>
        <? endif; ?>
        <h2 class="col-lg-12 col-xs-12">Feel free to call <?php echo Yii::app()->user->settings->office_phone; ?> for immediate assistance.</h2>
    </div>