<div class="col-xs-12" style="padding-top: 30px;">
    <h2>Activities & Communication</h2>

<?php
	Yii::app()->clientScript->registerCssFile($this->module->cdnUrl . 'assets/css/footable-0.1.css');
    $this->widget('admin_module.components.StmGridView', array(
            'id' => 'activity-log-grid',
            'template' => '{items}{pager}',
            'enablePagination' => true,
            'dataProvider' => $DataProvider,
            'itemsCssClass' => 'footable tablet breakpoint footable-loaded',
            'columns' => array(
                array(
                    'type' => 'raw',
                    'name' => 'Date',
                    'value' => '"<div style=\"display:inline-block\">".Yii::app()->format->formatDate($data->activity_date, "n/j/y")." ".Yii::app()->format->formatTime($data->activity_date).
                  "<br>"."<input type=\"hidden\" class=\"log_id\" value=\"".$data->id."\"></div>"
        ',
                    'htmlOptions' => array(
                        'style' => 'width:150px;'
                    ),
                ),
                'note',
                array(
                    'type' => 'raw',
                    'name' => 'From',
                    'value' => '$data->addedBy->fullName',
                    'htmlOptions' => array(
                        'style' => 'width:150px;'
                    ),
                ),
            ),
        )
    );
?>
</div>
