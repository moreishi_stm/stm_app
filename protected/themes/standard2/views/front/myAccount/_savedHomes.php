<?php
$mls_board_id = Yii::app()->user->boardId;
$jsSavedHomesList = <<<JS
	$('.add-favorites-button').live('click', addFavorites);
	$('.remove-favorites-button').live('click', removeFavorites);
	function removeFavorites() {
		var listingId = $(this).attr('data-id');
		console.log(listingId);
		$.post('/myAccount/apiSavedHomes', {mls_board_id:'$mls_board_id' , listing_id: listingId, action: 'remove' }, function(data) {
			$(this).removeClass('remove-favorites-button').addClass('add-favorites-button');
			$('.saved-count').html($('.saved-count').html()-1);
			$("#home-list .id"+listingId).hide();
			$.fn.yiiListView.update("home-list" , {
			complete: function(jqXHR, status) {
					$('.fav-home-conatiner > .panel-body').responsiveEqualHeightGrid();
				}
			})
		});

	}
	function addFavorites() {
		var listingId = $(this).attr('data');
		$.post('/myAccount/apiSavedHomes', { contact_id: '$contactId', listing_id: listingId, action: 'add' }, function(data) {
			$(this).removeClass('add-favorites-button').addClass('remove-favorites-button');
			$('.saved-count').html($('.saved-count').html()*1+1);
		});

	}
JS;


Yii::app()->clientScript->registerScript('savedHomesListJs', $jsSavedHomesList); ?>
	<style>
		@media (min-width: 1300px) {
			#home-list .home-photo-container .home-photo .img-responsive {
				min-width: 360px;
				min-height: 240px;
			}
		}
		.home-photo-container {
			margin-bottom: 4px;
		}
        .remove-favorites-button {
            color: #666;
            background-color: #EFEFEF;
        }
        .remove-favorites-button:hover {
            color: white;
            background-color: #C20000;
        }
	</style>
	<h2>My Favorites</h2>
<?php
$DataProvider = $model->search();

if ($DataProvider->totalItemCount) {
	$js = <<<JS
	$('.fav-home-conatiner > .panel-body').responsiveEqualHeightGrid();
JS;
	Yii::app()->clientScript->registerScript('responsiveEqualHeightGridSavedHomesListJs', $js);

	$this->widget( 'front_module.components.StmListView', array(
		'dataProvider'    =>$DataProvider,
		'template'        =>'{items}{pager}{summary}',
		'id'              =>'home-list',
		'itemView'        => '_homeListRow',
		'itemsCssClass' => 'row categoryProduct xsResponse clearfix',
		'summaryCssClass' => 'pull-right pull-right col-sm-4 col-xs-12 no-padding text-right text-left-xs',
		'pager' => array(
			'class' => 'CLinkPager',
			'header' => '<div class="pagination pull-left no-margin-top">',
			'footer' => '</div>',
			'firstPageCssClass' => '',
			'selectedPageCssClass' => 'active',
			'nextPageLabel' => "»",
			'prevPageLabel' => "«",
			'lastPageCssClass' => "hidden",
			'firstPageCssClass' => "hidden",
			'htmlOptions' => array('class' => 'pagination no-margin-top')
		)
	));
} else {
	$this->renderPartial('_noResults');
}