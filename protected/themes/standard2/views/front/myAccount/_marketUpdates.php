<h1>Market Updates</h1>
<hr />
<?php
$this->widget('admin_module.components.StmGridView', array(
			   'id' => 'market-update-grid',
			   'dataProvider' => $model->search(),
			   'itemsCssClass' => 'datatables',
			   'columns' => array(
				   'name',
				   array(
					   'type' => 'raw',
					   'name' => 'Type',
					   'value' => '"<strong><u>Market Criteria:</u></strong>".$data->printSearchCriteria()',
				   ),
				   array(
					   'type' => 'raw',
					   'name' => 'Search Results',
					   'value' => '"<strong>".$data->getCountPropertyResults()." homes found</strong>"',
					   'htmlOptions' => array(
						   'style' => 'width:150px'
					   ),
				   ),
				   array(
					   'name' => '',
					   'type' => 'raw',
					   'value' => '"<a href=\"/myAccount/marketUpdatesResults/".$data->id."\" class=\"button gray icon i_stm_search grey-button\">View Properties</a>"',
					   'htmlOptions' => array(
						   'style' => 'width:150px'
					   ),
				   ),
			   ),
		   )
);