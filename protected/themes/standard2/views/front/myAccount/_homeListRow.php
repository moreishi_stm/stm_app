<?php
if(get_class($data) != 'MlsProperties') {
    $data = $data->property;
}

if(!empty($data)): ?>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 id<?php echo $data->listing_id;?>">
	<div class="panel panel-default fav-home-conatiner">
		<div class="panel-heading">
			<h3 class="panel-title"><strong> <?php echo Yii::app()->format->formatProperCase($data->common_subdivision) ?></strong></h3>
		</div>
		<div class="panel-body">
			<div class="home-photo-container">
				<a class="home-photo" href="<?php echo $data->getUrl($relative == true); ?>"><img src="<?php echo (!empty($data->firstPhotoUrl)) ? $data->firstPhotoUrl : $data->getPhotoUrl($photoNumber = 1); ?>" class="img-responsive" /></a>
			</div>
			<ul>
				<li>
					<span class="price"><?php echo Yii::app()->format->formatDollars($data->price) ?></span>
				</li>
				<?php /* <li><span class="address-name"> <strong>Tanim Ahmed</strong></span></li>
				  <li><span class="address-company"> TanimDesign &amp; Development </span></li> */ ?>
				<li><span class="address-name address-line1"> <?php echo Yii::app()->format->formatProperCase($data->common_subdivision) ?> </span></li>
				<li><span class="address-name address-line2"> <?php echo Yii::app()->format->formatProperCase($data->city) ?>, <?php echo $data->getStateShortName($lowercase = false) . ' ' . substr($data->zip, 0, 5) ?> </span></li>

				<?php if (!empty($data->bedrooms) && ! empty($data->baths_total)) : ?>
				<li>
					<?php echo $data->bedrooms ?> Beds / <?php echo $data->baths_total ?> Baths
                    <?php if (!empty($data->sq_feet)) : ?>
                        <?php echo ' / '.Yii::app()->format->formatNumber($data->sq_feet) ?> SF
                        ($<?php echo $data->pricePerSf ?>/SF)
                    <?php endif; ?>
				</li>
				<?php endif; ?>
                <?php if ($data->year_built): ?>
                    <li>
                        <?=$data->year_built; ?> Yr Built
                    </li>
                <?php endif; ?>
                <?php if ($data->pool_yn): ?>
				<li>
					<span class="water">Pool</span>
				</li>
				<?php endif; ?>
				<li><a href="<?php echo $data->getUrl($relative = true); ?>" class="photos"><?php echo $data->photo_count ?> Photos</a></li>
<!--				<li>--><?php //echo Yii::app()->format->formatDays($data->status_change_date) ?><!--</li>-->
				<li> MLS#: <?php echo $data->listing_id ?></li>
			</ul>
		</div>
		<div class="panel-footer panel-footer-address">
			<a href="<?php echo $data->getUrl($relative = true); ?>" class="btn btn-sm btn-success">
				<i class="fa fa-eye"> </i> View Details
			</a>
			<a class="btn btn-sm btn-danger remove-favorites-button" data-id="<?php echo $data->listing_id;?>">
				<i class="fa fa-remove"></i>
			</a>
		</div>
	</div>
</div>
<?php endif;
