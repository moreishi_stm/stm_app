<div id="noresults">
	<h2>You currently have no Saved Homes</h2>
	<h3 class="search-price">Search by Price:</h3>
	<div class="search" src=""></div>
	<ul>
		<li>
            <?php echo CHtml::link('$200,000 - $300,000', array('/front/site/homes', 'price_min' => 200000, 'price_max' => 300000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$300,000 - $400,000', array('/front/site/homes', 'price_min' => 300000, 'price_max' => 400000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$400,000 - $500,000', array('/front/site/homes', 'price_min' => 400000, 'price_max' => 500000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$500,000 - $600,000', array('/front/site/homes', 'price_min' => 500000, 'price_max' => 600000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$600,000 - $700,000', array('/front/site/homes', 'price_min' => 600000, 'price_max' => 700000)); ?>
        </li>
	</ul>
	<ul>
        <li>
            <?php echo CHtml::link('$700,000 - $800,000', array('/front/site/homes', 'price_min' => 700000, 'price_max' => 800000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$800,000 - $900,000', array('/front/site/homes', 'price_min' => 800000, 'price_max' => 900000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$1 - 2 Million', array('/front/site/homes', 'price_min' => 1000000, 'price_max' => 2000000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$2 - 3 Million', array('/front/site/homes', 'price_min' => 20000000, 'price_max' => 30000000)); ?>
        </li>
        <li>
            <?php echo CHtml::link('$4+ Million', array('/front/site/homes', 'price_min' => 4000000)); ?>
        </li>
	</ul>
</div>