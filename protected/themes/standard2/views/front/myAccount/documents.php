<h2 style="margin-bottom: 0; padding-bottom:0;padding-top: 10px;">Documents</h2>
<?php $this->widget('admin_module.components.StmGridView', array(
        'id' => 'client-docs-grid',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'footable tablet breakpoint footable-loaded',
        'columns' => array(
            array(
                'type'=>'raw',
                'name' => 'Description',
                'value'=>'$data[description]',
            ),
            array(
                'type'=>'raw',
                'name'=>'Filesize',
                'value'=>'StmFormatter::formatFileSize($data[file_size])',
                'htmlOptions'=>array('style'=>'width:120px'),
            ),
            array(
                'type' => 'raw',
                'name' => '',
                'value' => '"<div><a href=\"/myAccount/documentView/".$data[id]."\" class=\"button gray icon i_stm_search grey-button\" target=\"_blank\">View</a></div>"',
                'htmlOptions' => array('style' => 'width:80px'),
            ),
        ),
    )
);