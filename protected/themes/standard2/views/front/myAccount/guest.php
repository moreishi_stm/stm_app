<div id="login">
    <?php if ($model->email): ?>
        <h2 style="text-align: center; margin-top: 20px; margin-bottom: -20px;">You're Almost There!...</h2>
    <?php endif; ?>
	<div id="login-container">
        <h1><em class="login-message"></em>Login to Your Account</h1>
        <?php echo $this->renderPartial('../site/_form', array('model'=>$model)); ?>
	</div>
</div>
<!--<h2 style="text-align: center; font-size: 35px; margin-bottom: 20px;">You may be Surprised at What Your Home is Worth!</h2>-->
<!---->
<!--<div id="houseValues-container">-->
<!--    --><?php //echo $this->renderPartial('../site/_formHouseValues', array('model'=>$model)); ?>
<!--</div>-->
