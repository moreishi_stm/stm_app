<style type="text/css">
    h1 {
        margin-top: 30px;
    }
    table#cms-editor {
        background-image: none;
    }
    table#cms-editor input[type="text"] {
        font-size: 16px;
    }
    .rounded-text-box {
        border: 1px solid #ccc;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .rounded-text-box table.container td {
        border: 0;
    }
    #CmsTemplateField_video-main_src {
        width: 90%;
    }
    .stm-image {
        max-width: 400px;
    }
    .mce-tinymce.mce-container {
        width: 90%;
    }
    #cms-editor tr:nth-child(odd) {
        background: #efefef;
    }
    #cms-editor tr:nth-child(even) {
        background: #fafafa;
    }
    #cms-editor td{
        padding: 20px;
    }
    .cms-template-field-title h4 {
        font-weight: bold;
        font-size: 30px;
        line-height: 1;
    }
    .btn.btn-success {
        padding: 20px;
        font-size: 30px !important;
    }
</style>
<h1>Edit My <?php echo ($model->type_ma == CmsContents::TYPE_LISTING_SB)? 'Listing': 'Buyer'; ?> Storyboard</h1>
<hr />
<div id="<?php echo ($model->type_ma == CmsContents::TYPE_LISTING_SB)? 'green': 'yellow'; ?>" style="margin-bottom: 20px;">
    <?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'page-content-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
    ));
    ?>
        <div class="g12 p-mb5 rounded-text-box odd-static">
            <table id="cms-editor">
                <?php echo $form->hiddenField($model, 'id'); ?>
                <?php
                    foreach ($Template->fields as $Field) {
                        // This prevents us from trying to render a field template that may not exist
                        // due to the absence of a 'type'
                        if (!$Field->type)
                            continue;

                        $title = CHtml::tag('h4', $htmlOptions=array(), $Field->title);
                        $fieldEditor = $this->renderPartial("../../../admin/views/cms/field_templates/{$Field->type}", array('model'=>$Field), $return=true);

                        echo CHtml::openTag('tr');
                        echo CHtml::tag('td', $htmlOptions = array('class'=>'cms-template-field-title'), $title);
                        echo CHtml::tag('td', $htmlOptions = array(), $fieldEditor);
                        echo CHtml::closeTag('tr');
                    }
                ?>
            </table>
        </div>
        <div id="submit-button-wrapper" style="margin-top: 20px;">
            <button type="submit" class="btn btn-success" style="width: 100%; font-size: 20px;">Update Storyboard</button>
        </div>
    <?php $this->endWidget();?>
</div>
