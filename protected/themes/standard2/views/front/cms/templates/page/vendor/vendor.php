<style>
    #cms-content-container {
        margin: 40px 0;
    }
</style>
<div id="logo">
	<img stmtemplate-title="Logo" src="photo_file.png" class="stmcms img-responsive" id="logo-photo"/>
</div>
<div id="title-container">
	<h1 id="vendor-category" class="stmcms" stmtemplate-title="Vendor Category">A/C, Roofer, Handyman, etc.</h1>
	<hr />
	<h2 class="stmcms" id="vendor-name" stmtemplate-title="Vendor Name">A/C, Roofer, Handyman, etc.</h2>
	<div class="stmcms" id="phone" stmtemplate-title="Phone">(999) 999-9999</div>
	<div id="website"><a href="http://www.website.com" stmtemplate-title="Website">www.website.com</a></div>
</div>
<div class="stmcms" id="bio" stmtemplate-title="Vendor Info &amp; Bio">
	<p>Insert a little bio about your vendor.</p>
</div>

<div id="services-container">
	<h3 class="stmcms" id="services-title" stmtemplate-title="Services Title">Description of Services</h3>
	<div class="stmcms" id="services" stmtemplate-title="Service Description">
		<p>Insert body paragraph text.</p>
	</div>
</div>