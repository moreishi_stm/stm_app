<style type="text/css">
    #cms-content-container {
        margin: 40px 0;
    }
    #standard-tmp div#main-photo{
        text-align: center;
    }
    #standard-tmp div#content{
        text-align: left;
    }
    audio#audio-1{
        width: 100%;
    }
</style>

<h1 class="stmcms" id="header" stmtemplate-title="Page Header">Add Your Header</h1>

<?php if (($fields['photo-1'] != 'photo_file.png') && (!empty($fields['photo-1']) && strpos($fields['photo-1'], 'photo_file.png') == false)): ?>
<div id="photo-1-container" style="text-align: center;">
	<img stmtemplate-title="Main Photo" src="photo_file.png" class="stmcms img-responsive" id="photo-1" />
</div>
<?php endif; ?>

<?php if (!empty($fields['audio-1'])): ?>
<div id="audio-1-container">
	<audio controls stmtemplate-title="Audio File" class="stmcms" id="audio-1" data-src="">
		<source src="<?php echo $fields['audio-1']; ?>" type="audio/mpeg">
		<embed height="50" width="650" src="<?php echo $fields['audio-1']; ?>">
	</audio>
</div>
<?php endif; ?>

<div class="stmcms" id="content" stmtemplate-title="Content">
    <p>Insert body paragraph text.</p>
</div>