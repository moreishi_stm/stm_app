<style>
    #cms-content-container {
        margin: 40px 0;
    }
</style>
<?php if (($fields['photo-1'] != 'photo_file.png') && (!empty($fields['photo-1']) && strpos($fields['photo-1'], 'photo_file.png') == false)): ?>
<div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center; margin-bottom: 20px;">
	<img stmtemplate-title="Main Photo" src="photo_file.png" class="stmcms img-responsive" id="photo-1" />
</div>
<?php endif; ?>
<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 40px;">
    <h1 class="stmcms" id="header" stmtemplate-title="Page Header" style="font-weight: bold;">Add Your Header</h1>
</div>
<div class="stmcms col-lg-12 col-md-12 col-sm-12" id="content" stmtemplate-title="Content">
    <p>Insert body paragraph text.</p>
</div>
