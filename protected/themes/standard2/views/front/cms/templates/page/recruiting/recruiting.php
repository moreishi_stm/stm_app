<style>
    #cms-content-container {
        margin: 40px 0;
    }
    #video-tmp #main-video {
		min-height: 415px;
        border: 20px solid black;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
	}
    #video-tmp #video-container {
        margin: 0 0 60px 0;
        max-width: 800px;
    }
    #cms-topics-container {
        margin-top: 20px;
    }
    #video-tmp h1#content-title {
        margin-top: 40px;
    }
    #video-tmp h1 {
        font-weight: bold;
        font-size: 40px;
    }
    #video-tmp #content {
        margin-bottom: 20px;
    }
    #video-tmp #main-video-tv-base {
        background: url('http://cdn.seizethemarket.com/assets/images/tv_base.png');
        width: 347px;
        height: 60px;
        margin-left: auto;
        margin-right: auto;
        top: -7px;
        position: relative;
    }
    #video-tmp .ask-question {
        font-size: 35px;
        text-align: left;
        color: #555;
        font-weight: normal;
    }
    #video-tmp h3 {
		font-weight: bold;
		font-size: 26px;
		line-height: 1.2;
	}
    #video-tmp h4 {
        font-weight: bold;
        font-size: 22px;
    }
	.form-container {
        padding: 60px 30px;
		margin: 20px 0;
        background: #EFEFEF;
	}
    .form-container .submit{
        width: 100%;
        font-size: 30px;
        background-color: #25ad29;
    }
    .form-container .submit:hover{
        background-color: #28CA2D;
    }

	.video-thumbnail-container{
		position: relative;
		margin-top: 8px;
		border: 3px solid transparent;
		/*width: 97%;*/
		padding: 4px;
		-webkit-border-radius: 6px;
		-moz-border-radius: 6px;
		border-radius: 6px;
		text-align: center;
	}
	.video-thumbnail-container img {
		min-width: 150px;
		width: 100%;
		margin-left: auto;
		margin-right: auto;
		display: block;
		padding-top: 10px;
	}
	.video-thumbnail-container:hover {
		border: 3px solid #C20000;
		background: #FFF4F4;
	}
	.video-thumbnail-container a h5 {
		clear: both;
		font-size: 15px;
		cursor: pointer;
		color: #444;
		text-decoration: none;
	}
    .video-thumbnail-container:hover a h5 {
        color: #C20000;
    }
    .video-thumbnail-container a .play-button {
        background-image: url(http://cdn.seizethemarket.com/assets/images/play_button.png);
        position: absolute;
        top: 40%;
        left: 50%;
		transform: translate(-50%, -50%);
        width: 152px;
        height: 41px;
        background-position: 0px 44px;
    }
    #video-tmp #suggested-videos-container {
        padding: 50px 0 10px 0;
        margin: 20px 0 0 0;
        border-top: 1px solid #AAA;
    }
    #video-tmp .cta-button {
        font-size: 25px;
        padding: 10px 35px;
    }
    #video-tmp .cta-container {
        margin-bottom: 30px;
    }
    #video-tmp .anchor{
        display: inline-block;
        position: relative;
        top: -90px;
    }
    #video-tmp ol {
        list-style: decimal;
        padding-left: 40px;
        margin: 14px 0;
    }
    #video-tmp ul {
        list-style: inherit;
    }
    body {
        line-height: inherit;
    }
</style>
<div id="video-tmp" class="col-lg-12">
	<div class="top-container">
        <h5 id="source-description" class="stmcms" stmtemplate-title="Source Description" style="display: none;"></h5>
        <?php if(!empty($fields['main-video'])): ?>
            <div id="video-container">
                <iframe width="100%" id="main-video" height="" class="stmcms" placeholder="Enter a video url" src="" data-type="youtube" />
            </div>
        <?php elseif (($fields['photo-1'] != 'photo_file.png') && (!empty($fields['photo-1']) && strpos($fields['photo-1'], 'photo_file.png') == false)): ?>
            <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center; margin-bottom: 20px;">
                <img stmtemplate-title="Main Photo" src="photo_file.png" class="stmcms img-responsive" id="photo-1" />
            </div>
        <?php endif; ?>

        <?php if(!empty($fields['content-title'])): ?>
        <h1 id="content-title" class="stmcms col-lg-12" stmtemplate-title="Content Title" placeholder="Add Content Title"></h1>
        <?php endif; ?>

        <p id="content" class="stmcms col-lg-12" stmtemplate-title="Content">
        </p>

        <?php if (!empty($fields['video-link-1']) || !empty($fields['video-link-2']) || !empty($fields['video-link-3']) || !empty($fields['video-link-4'])): ?>

            <div id="suggested-videos-container" class="col-lg-12">
            <h2>Related Popular Videos</h2>
            <div class="col-lg-12 no-padding">
                <?php if (!empty($fields['video-link-1'])): ?>
                <div class="video-thumbnail-container col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#" id="video-link-1" class="stmcms" stmtemplate-title="Video #1 Link">
                        <em class="play-button"></em>
                        <iframe id="video-1" width="220" height="" class="stmcms video-thumbnail" stmtemplate-title="Suggested Video #1" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
                        <h5 class="stmcms" stmtemplate-title="Suggested Video Title #1" id="video-1-title" placeholder="Video Title"></h5>
                    </a>
                </div>
                <?php endif; ?>
                <?php if (!empty($fields['video-link-2'])): ?>
                <div class="video-thumbnail-container col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#" id="video-link-2" class="stmcms" stmtemplate-title="Video #2 Link">
                        <em class="play-button"></em>
                        <iframe  id="video-2" width="220" height="" class="stmcms video-thumbnail" stmtemplate-title="Suggested Video #2" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
                        <h5 class="stmcms" stmtemplate-title="Suggested Video Title #2" id="video-2-title" placeholder="Video Title"></h5>
                    </a>
                </div>
                <?php endif; ?>
                <?php if (!empty($fields['video-link-3'])): ?>
                <div class="video-thumbnail-container col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#" id="video-link-3" class="stmcms" stmtemplate-title="Video #3 Link">
                        <em class="play-button"></em>
                        <iframe  id="video-3" width="220" height="" class="stmcms video-thumbnail" stmtemplate-title="Suggested Video #3" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
                        <h5 class="stmcms" stmtemplate-title="Suggested Video Title #3" id="video-3-title" placeholder="Video Title"></h5>
                    </a>
                </div>
                <?php endif; ?>
                <?php if (!empty($fields['video-link-4'])): ?>
                <div class="video-thumbnail-container col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#" id="video-link-4" class="stmcms" stmtemplate-title="Video #4 Link">
                        <em class="play-button"></em>
                        <iframe  id="video-4" width="220" height="" class="stmcms video-thumbnail" stmtemplate-title="Suggested Video #4" placeholder="Enter a video url" src="" data-type="youtube" data-thumbnail="true" />
                        <h5 class="stmcms" stmtemplate-title="Suggested Video Title #4" id="video-4-title" placeholder="Video Title"></h5>
                    </a>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>

        <a name="free-advice" class="anchor"></a>
		<div class="form-container col-lg-12 pull-left">
            <h5 id="form-position-name" class="stmcms" stmtemplate-title="Form Position" style="display: none;"></h5>
            <span id="form-description" class="stmcms form-description hide" stmtemplate-title="Form Description"></span>

            <div id="form" class="form-inline">
				<?php
				$formId = Forms::FORM_CAREERS;
				$SubmissionValues = new FormSubmissionValues($formId);
                $SubmissionValues->data[FormFields::SOURCE_DESCRIPTION_ID] = $fields['source-description'];
				$FormFields = new FormFields;
				if (isset($_GET['err'])) {
					Yii::app()->user->setFlash('error', $_GET['err']);
				}

				if (!Yii::app()->user->isGuest) {

					$SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;
					$SubmissionValues->data[$FormFields->getField('last_name')->id] = Yii::app()->user->lastName;
					$SubmissionValues->data[$FormFields->getField('email')->id] = Yii::app()->user->contact->getPrimaryEmail();
					$SubmissionValues->data[$FormFields->getField('phone')->id] = Yii::app()->user->contact->getPrimaryPhone();
				}
                if(!empty(trim($fields['form-position-name']))) {
                    $SubmissionValues->data[$FormFields->getField('position')->id] = (trim($fields['form-position-name']));
                }

				$form = $this->beginWidget('CActiveForm', array(
					'id' => 'video-form',
					'action' => array('/front/forms/videoForm/formId/' . $formId),
					'enableAjaxValidation' => true,
					'enableClientValidation' => false,
					'clientOptions' => array(
						'validateOnChange' => false,
						'validateOnSubmit' => true,
						'beforeValidate' => 'js:function(form) {
									$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
									return true;
								}',
						'afterValidate' => 'js:function(form, data, hasErrors) {
								$("div.loading-container.loading").remove();
								if (!hasErrors) {
									Message.create("success", "Your request was sent successfully!");
									$("div#form").addClass("hidden");
									$("div#thank-you-message").hide().removeClass("hidden").show();
								}

								return false;
							}',
					)
				));
				?>
                <h3 id="form-title" class="stmcms ask-question" stmtemplate-title="Form Title">I'm Interested in this Opportunity...</h3>
                <div class="form-group col-sm-6 col-xs-12 no-padding">
                        <?php
                        echo $form->textArea($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']', $htmlOptions = array(
                                'placeholder' => "Type your question, the opportunity you are interested in and a little bit about yourself.", 'class' => 'col-xs-12', 'rows'=>6
                            ));
                        ?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('question')->id . ']'); ?>
                </div>
                <div class="form-group col-sm-6 col-xs-12 no-padding">
                    <div class="col-sm-12 col-xs-12 no-padding">
                        <?php
                        echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('position')->id . ']', $htmlOptions = array(
                                'placeholder' => 'Position of Interest', 'class' => 'col-xs-12',
                            ));
                        ?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('position')->id . ']'); ?>
                    </div>
                </div>
				<div class="form-group col-sm-6 col-xs-12 no-padding">
					<div class="col-sm-6 col-xs-6 no-padding">
						<?php
						echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array(
							'placeholder' => 'First Name', 'class' => 'col-xs-12',
						));
						?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
					</div>
					<div class="col-sm-6 col-xs-6 no-padding">
						<?php
						echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array(
							'placeholder' => 'Last Name', 'class' => 'col-xs-12',
						));
						?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
					</div>
				</div>
				<div class="form-group col-sm-6 col-xs-12 no-padding">
					<div class="col-sm-6 col-xs-6 no-padding">
						<?php
						echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array(
							'placeholder' => 'E-mail', 'class' => 'col-xs-12',
						));
						?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
					</div>
					<div class="col-sm-6 col-xs-6 no-padding">
						<?php
						$this->widget('StmMaskedTextField', array(
							'model' => $SubmissionValues,
							'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
							'mask' => '(999) 999-9999',
							'id' => 'FormSubmissionValues_data_4',
							'htmlOptions' => array('class' => 'col-xs-12','placeholder'=>'Phone'),
						));
						?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
                        <?php echo CHtml::hiddenField('url', $_SERVER['REQUEST_URI']); ?>
                    </div>
                </div>

				<div class="col-xs-12 no-padding">
                    <?php if(!empty($fields['submit-button'])): ?>
                        <button id="submit-button" type="submit" class="stmcms submit btn btn-primary" stmtemplate-title="Submit Button Text" >Request More Information</button>
                    <?else:?>
                        <button id="submit-button" type="submit" class="submit btn btn-primary" stmtemplate-title="Submit Button Text" >Request More Information</button>
                    <?endif;?>
				</div>
				<?php $this->endWidget(); ?>
			</div>
			<div id="thank-you-message" class="hidden">
                <h3>Thank You!</h3>
                <h4>We received your request. For immediate assistance call <strong><?php echo Yii::app()->user->settings->office_phone; ?></strong> for personal assistance. </h4>
                <h4>Have a great day! =)</h4>
			</div>
		</div>
	</div>
</div>