<?php
    $formId = Forms::FORM_HOUSE_VALUES;
    if(isset($_GET['address']) && isset($_GET['city']) && isset($_GET['state']) && isset($_GET['zip'])) {
        if($_GET['status']==2) {
            $formPart = '2_details';
        } elseif($_GET['status']=='success') {
            $formPart = 'success';
        }
        $formName = $formId.'_Part'.$formPart;
    } else {
        $formPart = 1;
        $formName = $formId.'_Part'.$formPart;
    }
    $SubmissionValues = new FormSubmissionValues($formName);
    $SubmissionValues->formPart = $formPart;

    if(isset($_GET['address']) && isset($_GET['city']) && isset($_GET['state']) && isset($_GET['zip'])  && ($_GET['status']=='success' || $_GET['status']==2)) {
        $SubmissionValues->data = array(FormFields::ADDRESS_ID=>$_GET['address'], FormFields::CITY_ID=>$_GET['city'], FormFields::STATE_ID=>$_GET['state'], FormFields::ZIP_ID=>$_GET['zip'],
                                        FormFields::BEDROOMS_ID=>$_GET['bedrooms'], FormFields::BATHS_ID=>$_GET['baths'], FormFields::SQ_FEET_ID=>$_GET['sq_feet'], FormFields::CONDITION_ID=>$_GET['condition']);
    }
    $fields = $Template->loadedFields;
    $SubmissionValues->data[FormFields::SOURCE_DESCRIPTION_ID] = $fields['source-description'];
?>

<style type="text/css">
    .loading-container.loading {
        display: block;
        position: fixed;
        background-color: #FFFFFF;
        background: white;
        background-image: none;
        opacity: .9;
        filter: Alpha(Opacity=90);
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000 !important;
    }

    .loading-container.loading em {
        display: block;
        background: url(http://cdn.seizethemarket.com/assets/images/loading3.gif) no-repeat;
        opacity: 1 !important;
        position: relative;
        top: 38%;
        margin-left: auto;
        margin-right: auto;
        height: 256px;
        width: 256px;
    }

    .video-container{
        padding: 60px 0 10px 0;
        border-bottom: 1px solid #DDD;
        /*background: #BDD7FF;*/

        background: -moz-linear-gradient(top,  rgba(76,76,76,0.65) 0%, rgba(0,0,0,0.1) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(76,76,76,0.65)), color-stop(100%,rgba(0,0,0,0.1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a64c4c4c', endColorstr='#1a000000',GradientType=0 ); /* IE6-9 */

        /*background: -moz-linear-gradient(top,  rgba(0,0,0,0.65) 0%, rgba(0,0,0,0) 100%); *//* FF3.6+ */
        /*background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.65)), color-stop(100%,rgba(0,0,0,0))); *//* Chrome,Safari4+ */
        /*background: -webkit-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); *//* Chrome10+,Safari5.1+ */
        /*background: -o-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); *//* Opera 11.10+ */
        /*background: -ms-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); *//* IE10+ */
        /*background: linear-gradient(to bottom,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); *//* W3C */
        /*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a6000000', endColorstr='#00000000',GradientType=0 ); *//* IE6-9 */


        /*background: -moz-linear-gradient(top,  rgba(30,87,153,0.7) 0%, rgba(41,137,216,0.7) 50%, rgba(125,185,232,0.7) 100%); *//* FF3.6+ */
        /*background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(30,87,153,0.7)), color-stop(50%,rgba(41,137,216,0.7)), color-stop(100%,rgba(125,185,232,0.7))); *//* Chrome,Safari4+ */
        /*background: -webkit-linear-gradient(top,  rgba(30,87,153,0.7) 0%,rgba(41,137,216,0.7) 50%,rgba(125,185,232,0.7) 100%); *//* Chrome10+,Safari5.1+ */
        /*background: -o-linear-gradient(top,  rgba(30,87,153,0.7) 0%,rgba(41,137,216,0.7) 50%,rgba(125,185,232,0.7) 100%); *//* Opera 11.10+ */
        /*background: -ms-linear-gradient(top,  rgba(30,87,153,0.7) 0%,rgba(41,137,216,0.7) 50%,rgba(125,185,232,0.7) 100%); *//* IE10+ */
        /*background: linear-gradient(to bottom,  rgba(30,87,153,0.7) 0%,rgba(41,137,216,0.7) 50%,rgba(125,185,232,0.7) 100%); *//* W3C */
        /*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b31e5799', endColorstr='#b37db9e8',GradientType=0 ); *//* IE6-9 */
    }
    .video-container iframe, #photo-main {
        border: 15px solid black;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    #photo-main {
        position: relative;
        top: -6px;
    }

    .tv-base{
        background: url('http://cdn.seizethemarket.com/assets/images/tv_base.png');
        width: 347px;
        height: 60px;
        margin-left: auto;
        margin-right: auto;
        top: -7px;
        position: relative;
        clear: left;
    }

    .form-container {
        /*background: #F5F5F5;*/
        padding: 50px 0;
    }
    .form-container input, .form-container select{
        font-size: 20px;
        padding: 6px !important;
        height: 45px;
        background: white;
    }
    select {
        margin-bottom: 10px;
    }
    div.error select {
        background: none repeat scroll 0% 0% #FFB8B8;
        border: 1px solid #FF5757;
    }
    .btn-success {
        background: #2FCC2E;
    }
    div.errorMessage {
        clear: both;
        color: #FF5757;
        font-weight: bold;
        font-size: 11px;
    }
    input.error, select.error, textarea.error, div.error input, div.error textarea, span.error input, span.error select, span.error textarea {
        color: #000;
        background: #FFB8B8;
        border: 1px solid #FF5757;
    }

    .bonus-area {
        background: #D2E6FF;
        border-top: 1px solid #C9E2F9;
        /*background: #E4E3E3;*/
        padding: 40px 0;
    }
    .bonus-area h3 {
        font-size: 24px;
        margin-bottom: 8px;
        line-height: 1.2;
        font-weight: bold;
    }
    .bonus-area .tagline {
        color: #D20000;
        font-weight: bold;
        font-size: 14px;
        margin-top: 15px;
        bottom: 0;
    }

    .footer {
        font-size: 12px;
        color: #888;
        left: 0;
        text-align: center;
        clear: both;
        background-color: #F7F7F7;
        padding: 14px;
        /*top: 200px;*/
    }




    #house-values-container .overlay {
        opacity: .2;
        background-color: black;
        position: absolute;
        height: 900px;
        width: 100%;
        top: 0;
        left: 0;
    }
    #house-values-container .container {
        top: 15%;
        height: 900px;
    }
    .form-container-2 h1 {
        color: white;
    }
    .form-container-2 {
        padding: 30px;
        margin-top: 5%;
        min-height: 900px;
        margin-bottom: 200px;
    }
    .form-container-2 .errorMessage {
        color: white;
        top: -13px;
        position: relative;
    }
    #submit-button {
        background: #24D829;
        width: 100%;
    }
    #submit-button:hover {
        background: #10EC16;
    }
    .overlay {
        opacity: .2;
        background-color: black;
        position: absolute;
        height: 900px;
        width: 100%;
        top: 0;
        left: 0;
    }
    .map {
        height: 440px;
        width: 100%
    }
    .fadedBg {
        z-index: 2;
        background-color: rgba(77, 163, 255, 0.9); /*rgba(255,255,255,0.75);*/
        min-height: 200px;
        max-width: none !important;
    }
    .status-bar-container {
        font-weight: bold;
        font-size: 15px;
        margin-bottom: 20px;
    }
    .status-bar-image{
        position: absolute;
        z-index: -1;
        top: -2px;
        left: 0;
        height: 25px;
    }
</style>

<div id="stmcms-template">
    <? if($SubmissionValues->formPart == 1): ?>

    <div class="video-container">
        <div class="text-center container">
            <div class="col-lg-12">
                <?php if(!empty($fields['main-video'])): ?>
                        <iframe width="750" height="420" id="main-video" class="stmcms" placeholder="Enter a video url" src="" data-type="youtube" />
                <?php elseif(!empty($fields['photo-main'])): ?>
                        <img id="photo-main" stmtemplate-title="Main Photo (size: 750 x 420. Displays if no video)" src="http://cdn.seizethemarket.com/assets/default-placeholder.png" class="stmcms"/>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <? elseif(in_array($SubmissionValues->formPart, array(2, 'success'))): ?>

    <div id="house-values-container">
        <div class="" style="overflow: hidden; height: 900px; position: absolute; top: 0;">
            <img alt="img" src="http://cdn.seizethemarket.<?=((YII_DEBUG)? 'local': 'com')?>/assets/images/neighborhood-aerial-2.jpg" class="img-responsive" style="width: 100%; min-height: 800px;">
        </div>
        <div class="overlay"></div>
    </div>

    <? endif; ?>

    <div class="form-container">
        <div class="container">
            <?php
            $successForwardUrl = $fields['forward-url'];
            $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'houseValues-form',
                    'action' => array('/front/forms/houseValues/formId/' . $formId),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'clientOptions' => array(
                        'validateOnChange' => false,
                        'validateOnSubmit' => true,
                        'beforeValidate' => 'js:function(form, attribute) {
                            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                            return true;
                        }',
                        'afterValidate' => 'js:function(form, data, hasErrors) {
                            if (!hasErrors && data.status =="successPart1") {
                                window.location = "http://'.$_SERVER["SERVER_NAME"].'/l/'.$cmsUrl.'/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/2";
                            } else if (!hasErrors) {
                                window.location = "http://'.$_SERVER["SERVER_NAME"].'/l/'.$cmsUrl.'/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/success";
                            } else {
                                $("div.loading-container.loading").remove();
                            }
                            return false;
                        }',
                    ),
                ));
            ?>
            <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_DESCRIPTION_ID.']'); ?>

            <?php
            /**
             * Form fields display by default and hides upon successful submission
             */
            ?>
            <div style="<?php if ($SubmissionValues->formPart !== 1 && $SubmissionValues->formPart !== 2)  { echo 'display:none;'; } ?>" >
                <div  style="<?php if ($SubmissionValues->formPart !== 1) { echo 'display: none;'; } ?>" >
                    <div class="col-lg-6">
                        <h5 id="source-description" class="stmcms" stmtemplate-title="Source Description" style="display: none;"></h5>
                        <h1 id="main-title" class="stmcms" stmtemplate-title="Main Title"></h1>
                        <h2 id="header-subtitle" class="stmcms inline" stmtemplate-title="Subtitle 1"></h2>
                        <h3 id="subtitle-2" class="stmcms" stmtemplate-title="Subtitle 2"></h3>
                        <h4 id="subtitle-3" class="stmcms" stmtemplate-title="Subtitle 3"></h4>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-xs-12 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'Address'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'City'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
                        </div>
                        <div class="col-xs-3">
                            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('state') . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array('empty' => 'State', 'class' => 'col-xs-12 no-padding'));?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
                        </div>
                        <div class="col-xs-3 no-padding">
                            <?php $this->widget('StmMaskedTextField', array(
                                    'stmMaskedTextFieldClass'=>'houseValuesZip',
                                    'model' => $SubmissionValues,
                                    'attribute' => 'data[' . $FormFields->getFieldIdByName('zip') . ']',
                                    'mask' => '99999',
                                    'id' => 'FormSubmissionValues_data_'.FormFields::ZIP_ID,
                                    'htmlOptions' => array('class' => 'col-xs-12 no-padding', 'placeholder' => 'Zip'),
                                )); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
                            <?php if (!empty($fields['submit-button'])) { ?>
                                <button id="submit-button" type="submit" class="stmcms submit col-xs-12 btn btn-lg btn-primary btn-success" stmtemplate-title="Submit Button Text" ></button>
                            <?php } else { ?>
                                <button id="submit-button" type="submit" class="submit col-xs-12 btn" stmtemplate-title="Submit Button Text" >Submit Now</button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($SubmissionValues->formPart == '2_details'): ?>

                <div class="form-container-2 fadedBg col-lg-10 col-xs-12 col-lg-offset-1">
                    <div class="col-lg-12 status-bar-container text-center">
                        <div>50% Complete</div>
                        <img src="http://cdn.seizethemarket.<?=((YII_DEBUG)? 'local': 'com')?>/assets/images/status_bar_gray_50.gif" class="col-lg-12 status-bar-image" alt=""/>
                    </div>
                    <div class="col-xs-6">
                        <h1 class="col-lg-12">Verify Property Information</h1>
                        <h2 class="col-lg-12">
                            <?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('city')] . ', ' . AddressStates::getShortNameById($SubmissionValues->data[$FormFields->getFieldIdByName('state')]) . ' ' . $SubmissionValues->data[$FormFields->getFieldIdByName('zip')]; ?>
                        </h2>
                        <div class="col-xs-12 no-padding">
                            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition') . ']', array('Excellent' => 'Excellent', 'Average' => 'Average', 'Below Average' => 'Below Average', 'Poor' => 'Poor'), $htmlOptions = array('class'=>'col-xs-12 margin-bottom-row','placeholder' => 'Condition', 'empty' => 'Select Condition')); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition') . ']'); ?>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']', StmFormHelper::getBedroomList(' Bedrooms'), $htmlOptions = array('class'=>'col-xs-12 margin-bottom-row', 'empty' => 'Select Beds')); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']'); ?>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']', StmFormHelper::getHouseValuesBathList(), $htmlOptions = array('class'=>'col-xs-12 margin-bottom-row', 'empty' => 'Select Baths')); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']'); ?>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Approx Sq. Feet')); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']'); ?>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('first_name') . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'First Name')); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('first_name') . ']'); ?>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('last_name') . ']', $htmlOptions = array('class' => 'col-xs-12', 'placeholder' => 'Last Name')); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('last_name') . ']'); ?>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('email') . ']', $htmlOptions = array('class' => 'col-xs-12',  'placeholder' => 'Email')); ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('email') . ']'); ?>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <?php
                            $this->widget('StmMaskedTextField', array(
                                    'model' => $SubmissionValues,
                                    'attribute' => 'data[' . $FormFields->getFieldIdByName('phone') . ']',
                                    'mask' => '(999) 999-9999',
                                    'htmlOptions' => $htmlOptions = array('placeholder' => 'Phone', 'class' => 'col-xs-12'),
                                ));
                            ?>
                            <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('phone') . ']'); ?>
                            <?php
                            $addressFields = array('address', 'city', 'state', 'zip');
                            foreach ($addressFields as $addressField) {
                                echo $form->hiddenField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName($addressField) . ']', $htmlOptions = array('value' => $$addressField));
                            }
                            ?>
                            <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <input id="submit-button" type="submit" class="submit btn btn-lg btn-primary" value="Get Your Free Report Now">
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <iframe class="map col-xs-12" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;iwloc=&amp;geocode=&amp;q=<?php echo $SubmissionValues->data[$FormFields->getFieldIdByName('address')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('city')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('state')] . ', ' . $SubmissionValues->data[$FormFields->getFieldIdByName('zip')]; ?>&amp;t=h&amp;z=19&amp;output=embed"></iframe>
                    </div>
                </div>

            <?php endif; ?>
            <?php
            /**
             * Success message upon submission
             */
            if($SubmissionValues->formPart == 'success'): ?>
                <div class="form-container-2 fadedBg col-lg-10 col-xs-12 col-lg-offset-1">
                    <div class="col-xs-12 col-md-offset-2">
                        <h1 style="margin-bottom: 20px;">Success!</h1>
                        <h1 style="margin-bottom: 20px;">Your request has been successfully submitted.</h1>
                        <h1 style="margin-bottom: 20px;">If you need immediate assistance,<br>Call <?php echo Yii::app()->user->settings->office_phone; ?>.</h1>
                        <h1 style="margin-bottom: 20px;">Thank you and have a great day!</h1>
                        <?php if(!empty($fields['forward-url'])): ?>
                            <div class="forwardMessage">You will be forwarded in 8 seconds...</div>
                            <a id="forward-url" href="" class="stmcms inline" stmtemplate-title="Forward Url After Submit">Click Here to View All Homes on the Market!</a>
                            <?php (YII_DEBUG) ? '' : Yii::app()->clientScript->registerScript('completeForwarder', 'setTimeout(function(){ window.location = "' . $fields['forward-url'] . '";}, 10000)'); ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>

    <?php
    /**
     * Benefits info boxes, doesn't display after submission when success message displays
     */
    if($SubmissionValues->formPart == 1): ?>
    <div class="bonus-area">
        <div class="container">
            <ul>
                <li class="col-lg-3">
                    <h3 id="bonus-title-1" class="stmcms" stmtemplate-title="Bonus Title #1:"></h3>
                    <span id="bonus-content-1" class="stmcms" stmtemplate-title="Bonus Content #1"></span>

                    <h5 id="bonus-tagline-1" class="stmcms tagline" stmtemplate-title="Bonus Tag Line #1">- FREE Inside...</h5>
                </li>
                <li class="col-lg-3 col-lg-offset-1">
                    <h3 id="bonus-title-2" class="stmcms" stmtemplate-title="Bonus Title #2"></h3>
                    <span id="bonus-content-2" class="stmcms" stmtemplate-title="Bonus Title #2"></span>

                    <h5 id="bonus-tagline-2" class="stmcms tagline" stmtemplate-title="Bonus Tag Line #2">- FREE Inside...</h5>
                </li>
                <li class="col-lg-3 col-lg-offset-1">
                    <h3 id="bonus-title-3" class="stmcms" stmtemplate-title="Bonus Title #3"></h3>
                    <span id="bonus-content-3" class="stmcms" stmtemplate-title="Bonus Title #3"></span>

                    <h5 id="bonus-tagline-3" class="stmcms tagline" stmtemplate-title="Bonus Tag Line #3">- FREE Inside...</h5>
                </li>
            </ul>
        </div>
    </div>
    <h1 id="tracking-pixel" class="stmcms" style="display: hidden;" stmtemplate-title="Tracking Pixel">
    </h1>
    <?php endif; ?>
    <div class="footer col-12-lg" style="<? /* Margin is funky here cuz of the absolute position of backgorund */ echo ($SubmissionValues->formPart == 'success') ? 'margin-top: 150px;' : '' ?>">
        <div class="credits">© Copyright <?php echo date('Y');?>. Not intended to solicit currently listed properties.  <?php echo Yii::app()->user->accountSettings['office_name']; ?>.</div>
    </div>
</div>