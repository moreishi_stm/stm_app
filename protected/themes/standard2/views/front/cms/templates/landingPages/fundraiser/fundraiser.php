<?php
$formId = Forms::FORM_FUNDRAISER;
$SubmissionValues = new FormSubmissionValues($formId);
if(isset($_GET['fname']) && isset($_GET['lname']) && isset($_GET['email']) && isset($_GET['phone']) && isset($_GET['status']) && ($_GET['status']=='success')) {
    $SubmissionValues->formPart = 'success';
} else {
    $SubmissionValues->formPart = 1;
}

$fields = $Template->loadedFields;
$SubmissionValues->data[FormFields::SOURCE_ID] = $fields['source-id'];
$SubmissionValues->data[FormFields::SOURCE_DESCRIPTION_ID] = $fields['source-description'];

$js = "
    $('#FormSubmissionValues_data_41').change('', function(){
        if($(this).val() == '3' || $(this).val() == 'seller_and_buyer') {
            $('.address-field').show('normal');
        }
        else {
            $('.address-field').hide('normal');
        }
    });
";
/*Yii::app()->clientScript->registerScript('fundraiserForm', $js);*/
?>
<style type="text/css">
    .loading-container.loading {
        display: block;
        position: fixed;
        background-color: #FFFFFF;
        background: white;
        background-image: none;
        opacity: .9;
        filter: Alpha(Opacity=90);
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000 !important;
    }

    .loading-container.loading em {
        display: block;
        background: url(http://cdn.seizethemarket.com/assets/images/loading3.gif) no-repeat;
        opacity: 1 !important;
        position: relative;
        top: 38%;
        margin-left: auto;
        margin-right: auto;
        height: 256px;
        width: 256px;
    }

    .video-container{
        padding: 60px 0 10px 0;
        border-bottom: 1px solid #DDD;
        background: -moz-linear-gradient(top,  rgba(76,76,76,0.65) 0%, rgba(0,0,0,0.1) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(76,76,76,0.65)), color-stop(100%,rgba(0,0,0,0.1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(76,76,76,0.65) 0%,rgba(0,0,0,0.1) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a64c4c4c', endColorstr='#1a000000',GradientType=0 ); /* IE6-9 */
    }
    .video-container iframe {
        border: 15px solid black;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }

    .tv-base{
        background: url('http://cdn.seizethemarket.com/assets/images/tv_base.png');
        width: 347px;
        height: 60px;
        margin-left: auto;
        margin-right: auto;
        top: -6px;
        position: relative;
        clear: left;
    }
    #photo-main {
        max-width: 100%;
    }
    .form-container {
        padding: 50px 0;
    }
    .form-container input, .form-container select{
        font-size: 20px;
        padding: 6px !important;
        height: 45px;
    }
    div.error select {
        background: none repeat scroll 0% 0% #FFB8B8;
        border: 1px solid #FF5757;
    }
    .btn-success {
        background: #2FCC2E;
    }
    div.errorMessage {
        clear: both;
        color: #FF5757;
        font-weight: bold;
        font-size: 11px;
    }
    input.error, select.error, textarea.error, div.error input, div.error textarea, span.error input, span.error select, span.error textarea {
        color: #000;
        background: #FFB8B8;
        border: 1px solid #FF5757;
    }
    .bonus-area {
        background: #D2E6FF;
        border-top: 1px solid #C9E2F9;
        /*background: #E4E3E3;*/
        padding: 40px 0;
    }
    .bonus-area h3 {
        font-size: 24px;
        margin-bottom: 8px;
        line-height: 1.2;
        font-weight: bold;
    }
    .bonus-area .tagline {
        color: #D20000;
        font-weight: bold;
        font-size: 14px;
        margin-top: 15px;
        bottom: 0;
    }

    .footer {
        font-size: 12px;
        color: #888;
        left: 0;
        text-align: center;
        clear: both;
        background-color: #F7F7F7;
        padding: 14px;
    }
</style>

<div id="stmcms-template">
    <div class="video-container">
        <div class="text-center container">
            <?php if(!empty($fields['main-video'])) { ?>
                <div class="col-lg-12">
                    <iframe width="750" height="420" id="main-video" class="stmcms" placeholder="Enter a video url" src="" data-type="youtube" />
                </div>
                <div class="tv-base"></div>
            <?php } else { ?>
                <?php if(!empty($fields['photo-main'])): ?>
                    <img id="photo-main" stmtemplate-title="Main Photo (size: 750 x 420. Displays if no video)" src="http://cdn.seizethemarket.com/assets/default-placeholder.png" class="stmcms"/>
                <?php endif; ?>
            <?php } ?>
        </div>
    </div>

    <div class="form-container">
        <div class="container">
            <?php
            $successForwardUrl = $fields['forward-url'];
            $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'video-landing-page-form',
                    'action' => array('/front/forms/recruiting/formId/' . $formId),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'clientOptions' => array(
                        'validateOnChange' => false,
                        'validateOnSubmit' => true,
                        'beforeValidate' => 'js:function(form, attribute) {
                            $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                            return true;
                        }',
                        'afterValidate' => 'js:function(form, data, hasErrors) {
                            if (!hasErrors) {
                                window.location = "http://'.$_SERVER["SERVER_NAME"].'/l/'.$cmsUrl.'/fname/" + $("#FormSubmissionValues_data_1").val() + "/lname/" + $("#FormSubmissionValues_data_2").val() + "/email/" + $("#FormSubmissionValues_data_3").val() + "/phone/" + $("#FormSubmissionValues_data_4").val() + "/status/success";
                            } else {
                                $("div.loading-container.loading").remove();
                            }
                            return false;
                        }',
                    ),
                ));
            ?>
            <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_ID.']'); ?>
            <?php echo $form->hiddenField($SubmissionValues, 'data['.FormFields::SOURCE_DESCRIPTION_ID.']'); ?>
            <?php
            /**
             * Form fields dispaly by default and hides upon successful submission
             */
            if($SubmissionValues->formPart == 1): ?>
                <div class="col-lg-6">
                    <select id="source-id" class="stmcms" stmtemplate-title="Source" data-id="<?=$fields['source-id']?>" style="display: none;"></select>
                    <h5 id="source-description" class="stmcms" stmtemplate-title="Source Description" style="display: none;"></h5>
                    <h1 id="main-title" class="stmcms" stmtemplate-title="Main Title"></h1>
                    <h2 id="header-subtitle" class="stmcms inline" stmtemplate-title="Subtitle 1"></h2>
                    <h3 id="subtitle-2" class="stmcms" stmtemplate-title="Subtitle 2"></h3>
                    <h4 id="subtitle-3" class="stmcms" stmtemplate-title="Subtitle 3"></h4>
                </div>
                <div class="col-lg-6">
                    <div class="col-xs-6 no-padding">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'First Name'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'Last Name'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'Email'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <?php $this->widget('StmMaskedTextField', array(
                                'stmMaskedTextFieldClass'=>'houseValuesPhone',
                                'model' => $SubmissionValues,
                                'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                                'mask' => '(999) 999-9999',
                                'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                                'htmlOptions' => array('class' => 'col-xs-12 no-padding', 'placeholder' => 'Phone'),
                            )); ?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getField('multi_component_type')->id . ']', $FormFields->multiComponentList, $htmlOptions = array('class' => 'col-xs-12 no-padding', 'empty' => 'Select your next real estate activity...'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('multi_component_type')->id . ']'); ?>
                    </div>
                    <div class="col-xs-12 no-padding address-field" style="display: block;">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'Address'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
                    </div>
                    <div class="col-xs-6 no-padding address-field" style="display: block;">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'City'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
                    </div>
                    <div class="col-xs-3 no-padding address-field" style="display: block;">
                        <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('state') . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array('empty' => 'State', 'class' => 'col-xs-12 no-padding'));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
                    </div>
                    <div class="col-xs-3 no-padding address-field" style="display: block;">
                        <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']', $htmlOptions = array('class' => 'col-xs-12 no-padding', 'placeholder' => 'Zip','maxlength'=>5));?>
                        <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
                    </div>


                    <div class="col-xs-12 no-padding">
                        <?php if (!empty($fields['submit-button'])) { ?>
                            <button id="submit-button" type="submit" class="stmcms submit col-xs-12 btn btn-lg btn-primary btn-success" stmtemplate-title="Submit Button Text" ></button>
                        <?php } else { ?>
                            <button id="submit-button" type="submit" class="submit col-xs-12 btn btn-lg btn-primary btn-success" stmtemplate-title="Submit Button Text" >Submit Now</button>
                        <?php } ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            /**
             * Success message upon submission
             */
            if($SubmissionValues->formPart == 'success'): ?>
                <div class="col-xs-12 col-md-offset-2">
                    <h1 style="margin-bottom: 20px;">Success!</h1>
                    <h2 style="margin-bottom: 20px;">Your information has been successfully submitted.</h2>
                    <h2 style="margin-bottom: 20px;">If you need immediate assistance,<br>Call <?php echo Yii::app()->user->settings->office_phone; ?>.</h2>
                    <h2 style="margin-bottom: 20px;">Thank you and have a great day!</h2>
                    <?php if(!empty($fields['forward-url'])): ?>
                        <div class="forwardMessage">You will be forwarded in 8 seconds...</div>
                        <a id="forward-url" href="" class="stmcms inline" stmtemplate-title="Forward Url After Submit">Click Here to View All Homes on the Market!</a>
                        <?php (YII_DEBUG) ? '' : Yii::app()->clientScript->registerScript('completeForwarder', 'setTimeout(function(){ window.location = "' . $fields['forward-url'] . '";}, 10000)'); ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>

    <?php
    /**
     * Benefits info boxes, doesn't display after submission when success message displays
     */
    if($SubmissionValues->formPart != 'success'): ?>
        <div class="bonus-area">
            <div class="container">
                <ul>
                    <li class="col-lg-3">
                        <h3 id="bonus-title-1" class="stmcms" stmtemplate-title="Bonus Title #1:"></h3>
                        <span id="bonus-content-1" class="stmcms" stmtemplate-title="Bonus Content #1"></span>

                        <h5 id="bonus-tagline-1" class="stmcms tagline" stmtemplate-title="Bonus Tag Line #1">- FREE Inside...</h5>
                    </li>
                    <li class="col-lg-3 col-lg-offset-1">
                        <h3 id="bonus-title-2" class="stmcms" stmtemplate-title="Bonus Title #2"></h3>
                        <span id="bonus-content-2" class="stmcms" stmtemplate-title="Bonus Title #2"></span>

                        <h5 id="bonus-tagline-2" class="stmcms tagline" stmtemplate-title="Bonus Tag Line #2">- FREE Inside...</h5>
                    </li>
                    <li class="col-lg-3 col-lg-offset-1">
                        <h3 id="bonus-title-3" class="stmcms" stmtemplate-title="Bonus Title #3"></h3>
                        <span id="bonus-content-3" class="stmcms" stmtemplate-title="Bonus Title #3"></span>

                        <h5 id="bonus-tagline-3" class="stmcms tagline" stmtemplate-title="Bonus Tag Line #3">- FREE Inside...</h5>
                    </li>
                </ul>
            </div>
        </div>
    <?php endif; ?>
    <div class="footer col-12-lg">
        <div class="credits">© Copyright <?php echo date('Y');?>. Not intended to solicit currently listed properties.  <?php echo Yii::app()->user->accountSettings['office_name']; ?>.</div>
    </div>
</div>