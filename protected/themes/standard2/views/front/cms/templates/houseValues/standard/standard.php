<style type="text/css">
    body, div, #houseValues-template, h1, h2, h3, h4 { margin:0; padding: 0;}

    .loading-container.loading {
        display: block;
        position: fixed;
        background-color: #FFFFFF;
        background: white;
        background-image: none;
        opacity: .9;
        filter: Alpha(Opacity=90);
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000 !important;
    }

    .loading-container.loading em {
        display: block;
        background: url(http://cdn.seizethemarket.com/assets/images/loading3.gif) no-repeat;
        opacity: 1 !important;
        position: relative;
        top: 38%;
        margin-left: auto;
        margin-right: auto;
        height: 256px;
        width: 256px;
    }
    div.inline { display: inline-block; vertical-align: top;}
    #subtitle-2 {
        margin-top: 15px;
    }
    #subtitle-3 {
        font-size: 18px;
        font-weight: normal;
        margin-bottom: 20px;
    }
    .top-area {
        background-color: #999;
        background-repeat: repeat-x;
        background-size: 100% 900px;
        background-image: -moz-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #444444), color-stop(100%, #999999));
        background-image: -webkit-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: -o-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: -ms-linear-gradient(top, #444444 0%, #999999 100%);
        background-image: linear-gradient(to bottom, #444444 0%, #999999 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#444444', endColorstr='#999999', GradientType=0);
        height: 600px;
    }
    .top-box {
        position: relative;
        top: 50px;
        margin-left: auto;
        margin-right: auto;
        width: 900px;
        height: 450px;
        background: rgba(255, 255, 255, 0.95);
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-box-shadow: 4px 4px 4px #666;
        -webkit-box-shadow: 4px 4px 4px #666;
        box-shadow: 4px 4px 4px #666;
    }
    .top-photo-container {
        width: 410px;
        height: 450px;
        display: inline-block;
        /*background-image: url("http://cdn.seizethemarket.com/assets/images/cmsTemplates/houseValues/standard/house-values.png");*/
        position: relative;
        top: 0;
        left: 0;
    }
    .top-photo-container img {
        width: 410px;
        height: 450px;
        border-top-left-radius: 5px;
        -moz-border-radius-topleft: 5px;
        -webkit-border-top-right-radius: 5px;
        border-bottom-left-radius: 5px;
        -moz-border-radius-bottomleft: 5px;
        -webkit-border-bottom-left-radius: 5px;
    }
    .header-container {
        display: inline-block;
        vertical-align: top;
        padding: 4px 0 0 20px;
    }
    .form-container {
        display: inline-block;
        margin-top: 20px;
        width: 425px;
    }
    .form-container input, .form-container select {
        font-size: 25px;
    }
    .form-container select {
        height: 45px;
    }
    .form-container form .address {
        /*width: 460px;*/
    }

    .form-container form .city {
        width: 180px;
    }

    .form-container form .state {
        width:110px;
    }

    .form-container form .zip {
        width: 110px;
    }
    .form-container form .firstName, .form-container form .lastName, .form-container form .email, .form-container form .phone {
        width: 195px;
    }
    .form-box .center {
        text-align: center;
    }
    #house-values-submit-button {
        font-size: 18px !important;
        width: 100%;
        height: 45px;
    }
    div.arrow {
        background: url('http://cdn.seizethemarket.com/assets/images/cmsTemplates/houseValues/standard/arrow.png') repeat scroll 0% 0% transparent;
        height: 131px;
        width: 234px;
        position: relative;
        top: 11px;
    }
    div.error select {
        background: none repeat scroll 0% 0% #FFB8B8;
        border: 1px solid #FF5757;
    }
    .row.submit {
        margin-top: 5px;
    }
    a#forward-url {
        color: #1b83e6;
        font-size: 20px;
        font-weight: bold;
    }
    a#forward-url:hover {
        color: #D20000;
    }
    .bonus-area {
        width: 900px;
        height: 250px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 20px;
    }
    .bonus-area ul {
        padding: 0;
        min-height: 250px;
    }
    .bonus-area ul li {
        display: inline-block;
        width: 260px;
        height: 235px;
        padding-right: 50px;
        float: left;
        list-style: none;
    }
    .bonus-area ul li:last-child {
        padding-right: 0;
    }
    .bonus-area h3 {
        font-size: 24px;
        margin-bottom: 8px;
        line-height: 1.2;
    }
    .bonus-area .tagline {
        color: #D20000;
        font-weight: bold;
        font-size: 14px;
        margin-top: 15px;
        bottom: 0;
    }
    .bonus-area .forwardLink {
        font-size: 34px;
        color: black;
        position: relative;
        top: 100px;
        display: block;
        text-align: center;
    }
    .bonus-area .forwardMessage {
        font-size: 12px;
        color: #888;
        top: 120px;
        position: relative;
        text-align: center;
    }
    .footer {
        font-size: 12px;
        color: #888;
        margin-top: 25px;
        left: 0;
        text-align: center;
        clear: both;
        background-color: #e4e4df;
        padding: 14px;
    }
</style>

<div id="houseValues-template">
    <div class="top-area">
        <div class="top-box">
            <div class="top-photo-container">
                <?php if(!isset($fields['photo-main']) || empty($fields['photo-main']) || ($fields['photo-main']==1)) { ?>
                    <img id="photo-main" src="http://cdn.seizethemarket.com/assets/images/cmsTemplates/houseValues/standard/house-values.png"/>
                <?php } else { ?>
                        <img id="photo-main" stmtemplate-title="Main Photo (size: 410 x 450)" src="http://cdn.seizethemarket.com/assets/images/cmsTemplates/houseValues/standard/house-values.png" class="stmcms"/>
                <?php } ?>
            </div>
            <div class="header-container">
                <div class="form-container">
                    <?php
                        $successForwardUrl = $fields['forward-url'];
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'houseValues-form',
                            'action' => array('/front/forms/houseValues/formId/' . $formId),
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                            'clientOptions' => array(
                                'validateOnChange' => false,
                                'validateOnSubmit' => true,
                                'beforeValidate' => 'js:function(form, attribute) {
                                    $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                                    return true;
                                }',
                                'afterValidate' => 'js:function(form, data, hasErrors) {
                                    if (!hasErrors && data.status =="successPart1_details") {
                                        window.location = "http://'.$_SERVER["SERVER_NAME"].'/houseValues/'.$houseValuesActionId.'/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/bedrooms/" + $("#FormSubmissionValues_data_26").val() + "/baths/" + $("#FormSubmissionValues_data_27").val() + "/sq_feet/" + $("#FormSubmissionValues_data_28").val() + "/condition/" + $("#FormSubmissionValues_data_29").val() + "/status/2/fsid/" + data.form_submission_id;
                                    } else if (!hasErrors) {
                                        window.location = "http://'.$_SERVER["SERVER_NAME"].'/houseValues/'.$houseValuesActionId.'/address/" + $("#FormSubmissionValues_data_14").val() + "/city/" + $("#FormSubmissionValues_data_16").val() + "/state/" + $("#FormSubmissionValues_data_17").val() + "/zip/" + $("#FormSubmissionValues_data_18").val() + "/status/success/fsid/" + data.form_submission_id;
                                    } else {
                                        $("div.loading-container.loading").remove();
                                    }
                                    return false;
                                }',
                            ),
                        ));
                    ?>
                        <?php echo ($SubmissionValues->formPart == '1_details')? '<div>':'<div style="display: none;">'; ?>
                            <h1>FREE <?php echo date('F Y');?> Report</h1>
                            <h2 id="header-subtitle" class="stmcms inline" stmtemplate-title="Report Subtitle"></h2>
                            <h3 id="subtitle-2">Find Out What Your Home is Worth!</h3>
                            <h4 id="subtitle-3">Free Report Sent to your Inbox!</h4>
                            <div class="row">
                                <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']', $htmlOptions = array('class' => 'address', 'placeholder' => 'Address'));?>
                                <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('address')->id . ']'); ?>
                            </div>
                            <div class="row">
                                <div class="inline">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']', $htmlOptions = array('class' => 'city', 'placeholder' => 'City', 'value' => $opt['city']));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('city')->id . ']'); ?>
                                </div>
                                <div class="inline">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']', CHtml::listData(AddressStates::model()->findAll(), 'id', 'short_name'), $htmlOptions = array('empty' => 'State', 'class' => 'state'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('state')->id . ']'); ?>
                                </div>
                                <div class="inline">
                                    <?php $this->widget('StmMaskedTextField', array(
                                            'stmMaskedTextFieldClass'=>'houseValuesZip',
                                            'model' => $SubmissionValues,
                                            'attribute' => 'data[' . $FormFields->getField('zip')->id . ']',
                                            'mask' => '99999',
                                            'id' => 'FormSubmissionValues_data_'.FormFields::ZIP_ID,
                                            'htmlOptions' => array('class' => 'zip', 'placeholder' => 'Zip'),
                                        )); ?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('zip')->id . ']'); ?>
                                </div>
                            </div>
                            <div class="row g100">
                                <div class="inline g6 p-p0">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition'). ']', array('Excellent'=>'Excellent','Average'=>'Average','Below Average'=>'Below Average','Poor'=>'Poor'), $htmlOptions = array('class' => 'g95', 'placeholder' => 'Baths', 'empty'=>'Select Condition'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('condition') . ']'); ?>
                                </div>
                                <div class="inline g6 p-p0">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']', $htmlOptions = array('class' => 'g95', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'Sq. Feet'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('sq_feet') . ']'); ?>
                                </div>
                            </div>
                            <div class="row g100 p-p0">
                                <div class="inline g6 p-p0">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']', StmFormHelper::getBedroomList(' Bedrooms'), $htmlOptions = array('class' => 'g95', 'data-prompt-position' => 'bottomLeft:0,6', 'placeholder' => 'Bedrooms','empty'=>'Select Bedrooms'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('bedrooms') . ']'); ?>
                                </div>
                                <div class="inline g6 p-p0">
                                    <?php echo $form->dropDownList($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']', StmFormHelper::getHouseValuesBathList(), $htmlOptions = array('class' => 'g100', 'placeholder' => 'Baths', 'empty'=>'Select Baths'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getFieldIdByName('baths') . ']'); ?>
                                </div>
                            </div>
                        </div>
                        <?php if($SubmissionValues->formPart == 2): ?>
                            <h1>FREE House Values Report</h1>
                            <h3 style="margin-top: 20px;">Confirm Delivery to:</h3>
                            <div class="row" style="margin-top: 20px;">
                                <div class="inline">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']', $htmlOptions = array('class' => 'firstName', 'placeholder' => 'First Name'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('first_name')->id . ']'); ?>
                                </div>
                                <div class="inline">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']', $htmlOptions = array('class' => 'lastName', 'placeholder' => 'Last Name'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('last_name')->id . ']'); ?>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="inline">
                                    <?php echo $form->textField($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']', $htmlOptions = array('class' => 'firstName', 'placeholder' => 'Email'));?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('email')->id . ']'); ?>
                                </div>
                                <div class="inline">
                                    <?php $this->widget('StmMaskedTextField', array(
                                            'stmMaskedTextFieldClass'=>'houseValuesPhone',
                                            'model' => $SubmissionValues,
                                            'attribute' => 'data[' . $FormFields->getField('phone')->id . ']',
                                            'mask' => '(999) 999-9999',
                                            'id' => 'FormSubmissionValues_data_'.FormFields::PHONE_ID,
                                            'htmlOptions' => array('class' => 'lastName', 'placeholder' => 'Phone'),
                                        )); ?>
                                    <?php echo $form->error($SubmissionValues, 'data[' . $FormFields->getField('phone')->id . ']'); ?>

                                    <?php echo $form->hiddenField($SubmissionValues, 'data[' . $FormFields->getField('form_submission_id')->id . ']'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($SubmissionValues->formPart == 'success'): ?>
                            <h1>Congratulations!</h1>
                            <br/>
                            <h2>Your FREE House Values Report will be delivered in your inbox shortly.</h2>
                            <br/>
                            <h2>If you need immediate assistance,<br>call <?php echo Yii::app()->user->settings->office_phone; ?>.</h2>
                            <br/>
                            <h2>Thank you and have a great day!</h2>
                            <br/>
                            <br/>
                            <div class="forwardMessage">You will be forwarded in 8 seconds...</div>
                        <?php endif; ?>
                        </div>
                        <div class="row center submit">
                            <?php if($SubmissionValues->formPart !== 'success') { ?>
                                <?php echo $form->hiddenField($SubmissionValues, 'formPart'); ?>
                                <?php echo CHtml::submitButton('Get House Values Now', array('class' => 'button', 'id' => 'house-values-submit-button')); ?>
                                <div class="arrow"></div>
                            <?php } elseif($SubmissionValues->formPart == 'success' && !empty($fields['forward-url'])) { ?>
                                <a id="forward-url" href="" class="stmcms inline" stmtemplate-title="Forward Url After Submit">Click here to view current homes on the market!</a>
                                <?php Yii::app()->clientScript->registerScript('completeForwarder', 'setTimeout(function(){ window.location = "' . $fields['forward-url'] . '";}, 10000)');
                            } ?>

                        </div>
                    </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<div class="bonus-area">
    <ul>
        <li>
            <h3>FREE Top Home Staging Tips</h3>
            <span>Ever wonder what the difference is between the homes that sit and the homes that SELL?! Find out what you need to do to stand out in the market to get your home SOLD!</span>

            <div class="tagline">- FREE Inside...</div>
        </li>
        <li>
            <h3>Homes that Sit vs. Homes that Sell</h3>
            <span>Find out how to make your home sell itself! Create the perfect first impression by staging to have buyers can fall in love with your home!</span>

            <div class="tagline">- FREE Inside...</div>
        </li>
        <li>
            <h3>FREE House Values Report</h3>
            <span>Find out what your home is worth in today’s market. Get the most up-to-date, accurate, local real estate market information. You'll be surprised by what's happening in your local market!</span>

            <div class="tagline">- FREE Inside...</div>
        </li>
    </ul>
</div>

<div class="footer">
    <div class="credits">© Copyright <?php echo date('Y');?>. Not intended to solicit currently listed properties.  <?php echo Yii::app()->user->settings->office_name; ?>.</div>
</div>