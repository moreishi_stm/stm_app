<style>
    #cms-content-container {
        margin: 40px 0;
    }
</style>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<h1><?php echo $this->pageTitle; ?></h1>
		<p id="blog-content" class="stmcms">Enter your post content here</p>
	</div>
</div>

<?php $this->widget('front_module.components.widgets.CmsCommentWidget.CmsCommentWidget', array('commentPillView' => 'commentPill', 'commentEntryView' => '_commentEntry')); ?>
