<?php //$layout = 'column1';
?>
<style type="text/css">
    #buyerStandard-template .label {
        font-weight: bold;
        font-size: 15px;
        display: inline;
    }
    #buyerStandard-template .buyer-response {
        padding: 10px 0;
    }
    #buyerStandard-template .inline, #lifestyleStandard-template .inline p {
        display: inline;
    }
	#buyerStandard-template #thank-you-message {
		background-color: #a0ffa0;
		text-align: justify;
		-webkit-border-radius: 6px;
		-moz-border-radius: 6px;
		border-radius: 6px;
		padding: 20px 10px;
		-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
		-moz-box-shadow   : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);
		box-shadow        : inset 0 1px 0 rgba(255, 255, 255, 0.6), inset 0 2px 5px rgba(255, 255, 255, 0.5), inset 0 -2px 5px rgba(0, 0, 0, 0.1);

		-moz-box-shadow:    0px 2px 2px 0px #ccc;
		-webkit-box-shadow: 0px 2px 2px 0px #ccc;
		box-shadow:         0px 2px 2px 0px #ccc;
	}
    #buyerStandard-template #video-container iframe.stmcms {
        margin-top: 30px;
        border: 13px solid black;
        border-radius: 8px;
    }
    #buyerStandard-template #video-container #tv-base{
        background: url('http://cdn.seizethemarket.com/assets/images/tv_base.png');
        width: 347px;
        height: 60px;
        margin-left: auto;
        margin-right: auto;
        top: -7px;
        position: relative;
    }</style>
<div id="buyerStandard-template">
	<div class="g12">
        <div style="text-align: center;">
            <div style="margin-left: auto; margin-right: auto;">
                <div class="g3" style="padding-top: 12px;">
                    <img src="http://cdn.seizethemarket.com/assets/images/buyer_storyboard.png" alt="Listing Storyboard &trade;" stmtemplate-title="Buyer Storyboard &trade;" width="228px" height="70px">
                </div>
                <div class="g9">
                    <h1 style="line-height: 30px; font-size: 25px; text-align: right;">
                        <span style="color:#08C000;">Preferred Pre-Qualified: <?php echo ($prequal = $Transaction->getFieldValue('prequal_date'))? 'Yes': 'No'; ?></span>
                    </h1>
                    <h3 style="text-align: right; font-size: 20px;">
<!--                        <span style="color:#08C000;">--><?php //echo ($price = $Transaction->getFieldValue('price_min'))? Yii::app()->format->formatDollars($price): 'Call for Pricing'; ?><!--</span>-->
                        <?php echo ($beds = $Transaction->getFieldValue('bedrooms'))? (($prequal)?'&nbsp;|&nbsp; ':' ').$beds.' Beds': ''; ?>
                        <?php echo ($baths = $Transaction->getFieldValue('baths'))? '&nbsp;|&nbsp; '.$baths.' Baths': ''; ?>
                        <?php
                            $sf = $Transaction->getFieldValue('sq_feet');
                            $sf = str_replace(array(' ',','),'',$sf);
                            if(is_numeric($sf)) {
                                $sf = number_format($sf);
                            }
                            echo ($sf)? '&nbsp;|&nbsp; '.$sf.' Sq.Feet': '';
                        ?>
                    </h3>
                </div>
            </div>

            <?php if (!empty($fields['video-main']['src'])): ?>
                <div id="video-container">
                    <iframe id="video-main" width="700" height="390" allowfullscreen="true" frameborder="0" class="stmcms video-thumbnail" stmtemplate-title="Video" placeholder="Enter a video url" src="" data-type="youtube"></iframe>
                    <div id="tv-base">
                    </div>
                </div>
            <?php endif; ?>
		</div>
	</div>
	<div class="p-clr"></div>
	<h3 style="font-family: Georgia, 'Times New Roman'; font-size: 30px; font-weight: normal; margin-top: 20px;">A Note from the Buyer...
        <div class="p-fr" style="position: relative; top:-5px;">
            <a href="http://www.facebook.com/sharer/sharer.php?u=http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
                <img src="http://cdn.seizethemarket.com/assets/images/facebook_share.png" alt="Facebook Share Listing Storyboard &trade;" stmtemplate-title="Facebook Share Listing Storyboard &trade;" >
            </a>
        </div>
    </h3>
	<hr style="clear:both;">
    <?php if (!empty($fields['photo-1']) && ($fields['photo-1'] !== 'photo_file.png')): ?>
		<div class="p-fr p-ml10">
			<img id="photo-1" stmtemplate-title="Photo #1 on Right" src="photo_file.png" class="stmcms" id="main" width="300px"/>
		</div>
	<?php endif;

    if (!empty($fields['dream-home'])): ?>
		<div class="buyer-response">
			<span class="label">My Dream Home:</span>
			<div id="dream-home" class="stmcms inline" stmtemplate-title="My Dream Home"></div>
		</div>
	<?php endif;

    if (!empty($fields['motivation'])):
	?>
	<div class="buyer-response">
		<span class="label">Reason for Buying:</span>
		<div id="motivation" class="stmcms inline" stmtemplate-title="Reason for Buying"></div>
	</div>
	<?php endif;

    if (!empty($fields['photo-2']) && ($fields['photo-2'] !== 'photo_file.png')): ?>
		<div class="p-fl p-mr10">
			<img id="photo-2" stmtemplate-title="Photo #2 on Left" src="photo_file.png" class="stmcms" width="300px"/>
		</div>
	<?php endif;

    if (!empty($fields['lifestyle'])):
	?>
	<div class="buyer-response">
		<span class="label">Desired Lifestyle:</span>
		<div id="lifestyle" class="stmcms inline" stmtemplate-title="Desired Lifestyle"></div>
	</div>
	<?php endif;

    if (!empty($fields['restaurant'])):
        ?>
        <div class="buyer-response">
            <span class="label">Favorite Restaurant:</span>
            <div id="restaurant" class="stmcms inline" stmtemplate-title="Favorite Restaurants"></div>
        </div>
    <?php endif;

    if (!empty($fields['hobbies'])):
	?>
	<div class="buyer-response">
		<span class="label">Hobbies:</span>
		<div id="hobbies" class="stmcms inline" stmtemplate-title="Hobbies"></div>
	</div>
	<?php endif;

    if (!empty($fields['personal-note'])):
	?>
	<div class="buyer-response">
		<span class="label">Personal Note:</span>
		<div id="personal-note" class="stmcms inline" stmtemplate-title="Personal Note"></div>
	</div>
	<?php endif;

    if (!empty($fields['prequal'])):
	?>
	<div class="buyer-response">
		<span class="label">Financial Qualification:</span>
		<div id="prequal" class="stmcms inline" stmtemplate-title="Financial Qualification: (pre-qualed or cash)"></div>
	</div>
	<?php endif;

	?>
	<div class="g12" style="background-color:#FCF4E1; text-align: center; border-top: 2px solid black; border-bottom: 2px solid black; margin: 20px 0 20px 0;padding: 15px;">
        <div class="g5"><img style="width: 240px;" src="http://cdn.seizethemarket.com/assets/images/house_in_hand.png" alt="Suggest Property to Buyer"/></div>
		<div id="form" class="g5">
		<?php
			$formId           = Forms::FORM_BUYER_STORYBOARD_OFFER_ME;
			$SubmissionValues = new FormSubmissionValues($formId);
			$FormFields       = new FormFields;
			if(isset($_GET['err'])) {
				Yii::app()->user->setFlash('error', $_GET['err']);
			}

			if (!Yii::app()->user->isGuest) {

				$SubmissionValues->data[$FormFields->getField('first_name')->id] = Yii::app()->user->firstName;
				$SubmissionValues->data[$FormFields->getField('last_name')->id]  = Yii::app()->user->lastName;
				$SubmissionValues->data[$FormFields->getField('email')->id]      = Yii::app()->user->contact->getPrimaryEmail();
				$SubmissionValues->data[$FormFields->getField('phone')->id]      = Yii::app()->user->contact->getPrimaryPhone();
			}

			$form = $this->beginWidget('CActiveForm', array(
								'id'                     => 'sneak-preview-form',
								'action'                 => array('/front/forms/videoForm/formId/' . $formId),
								'enableAjaxValidation'   => true,
								'enableClientValidation' => false,
								'clientOptions'          => array(
									'validateOnChange'   => false,
									'validateOnSubmit'   => true,
									'beforeValidate'     => 'js:function(form) {
											if($("#name-container").hasClass("hidden")) {

											    if($("#FormSubmissionValues_data_14").val() == "" ) {
											        alert("Please provide an Address.");
											    } else if($("#FormSubmissionValues_data_14").val().length < 5) {
											        alert("Please provide a valid Address.");
											    } else if($("#FormSubmissionValues_data_16").val() == "") {
											        alert("Please provide a City.");
											    } else if($("#FormSubmissionValues_data_17").val() == "") {
											        alert("Please select a State.");
											    } else if($("#FormSubmissionValues_data_18").val() == "") {
											        alert("Please provide a Zip Code.");
											    } else if($("#FormSubmissionValues_data_18").val().length < 5) {
											        alert("Please provide a valid Zip Code.");
											    } else {
    												$("div#name-container").hide().removeClass("hidden").show("normal");
											    }
												return false;
											} else {
												$("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
												return true;
											}
										}',
									'afterValidate'    => 'js:function(form, data, hasErrors) {
										$("div.loading-container.loading").remove();
										if (!hasErrors && (typeof data.error == "undefined")) {
											$("div#form").addClass("hidden");
											$("div#thank-you-message").hide().removeClass("hidden").show("normal");
										} else if (typeof data.error != "undefined") {
											Message.create("error",data.error);
										}
										return false;
									}')
							  	));
		?>
			<span class="g12 p-tl" style="font-size:18px; font-style: italic; font-weight: bold;">Suggest Your Property to the Buyer...</span>
<!--			<span class="p-tl" style="font-size: 18px; display:block;">--><?php //echo $Address->address;?><!--</span>-->
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('address')->id.']', $htmlOptions=array(
                    'placeholder'=>'Address','class'=>'g10 p-clr',
                ));?>
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('address')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('city')->id.']', $htmlOptions=array(
                    'placeholder'=>'City','class'=>'g5 p-clr',
                ));?>
            <?php echo $form->dropDownList($SubmissionValues, 'data['.$FormFields->getField('state')->id.']', CHtml::listData(AddressStates::model()->findAll(), 'short_name', 'short_name'),$htmlOptions=array('empty'=>'State','class'=>'state g3'));?>
            <?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']', $htmlOptions=array(
                    'placeholder'=>'Zip','class'=>'g2',
                ));?>
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('city')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('state')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>
            <?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('zip')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>

			<div id="name-container" class="hidden p-clr">
				<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']', $htmlOptions=array(
						'placeholder'=>'First Name','class'=>'g5 p-clr',
					));?>
				<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']', $htmlOptions=array(
						'placeholder'=>'Last Name','class'=>'g5',
					));?>
				<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('first_name')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>
				<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('last_name')->id.']',$htmlOptions=array('class'=>'errorMessage p-fl', 'style'=>'clear:none;')); ?>
				<?php echo $form->textField($SubmissionValues, 'data['.$FormFields->getField('email')->id.']', $htmlOptions=array(
						'placeholder'=>'Email','class'=>'g5 p-clr',
					));?>
				<?php $this->widget('StmMaskedTextField', array(
														  'model' => $SubmissionValues,
														  'attribute' => 'data['.$FormFields->getField('phone')->id.']',
														  'mask' => '(999) 999-9999',
														  'id' => 'FormSubmissionValues_data_4',
														  'htmlOptions' => array('class'=>'g5','placeholder' => 'Phone'),
														  )); ?>
				<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('email')->id.']',$htmlOptions=array('class'=>'errorMessage g5', 'style'=>'clear:none;')); ?>
				<?php echo $form->error($SubmissionValues, 'data['.$FormFields->getField('phone')->id.']',$htmlOptions=array('class'=>'errorMessage g5','style'=>'clear:none;')); ?>
				<?php echo $form->error($SubmissionValues, 'id', $htmlOptions=array('class'=>'hidden','style'=>'clear:none;')); ?>
			</div>

			<input class="button g12" type="submit" value="Suggest Property Now" style="font-size: 16px; padding: 12px; width: 86%;"/>
			<div class="g12 p-tl" style="font-size: 17px;">or call <?php echo Yii::app()->user->settings->office_phone;?> for immediate service.</div>
			<?php $this->endWidget(); ?>
		</div>
		<div id="thank-you-message" class="hidden g5">
			<p>
			<h3>Thank You!</h3> We received your property suggestion! <br /><br /> We would like to know more about the property. Please call <strong><?php echo Yii::app()->user->settings->office_phone;?></strong> for immediate assistance. <br /><br />We look forward to helping you. Have a great day! =)<br /><br /><a href="/area/all">Click here to continue your Home Search.</a>
			</p>
		</div>
	</div>
</div>
<div>
    <a href="http://www.facebook.com/sharer/sharer.php?u=http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
        <img src="http://cdn.seizethemarket.com/assets/images/facebook_share.png" alt="Facebook Share Listing Storyboard &trade;" stmtemplate-title="Facebook Share Listing Storyboard &trade;" >
    </a>
</div>
<?php
$this->widget('front_module.components.widgets.CmsCommentWidget.CmsCommentWidget', array('commentPillView' => 'commentPill', 'commentEntryView' => '_commentEntry'));
?>
