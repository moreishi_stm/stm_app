<?php
if($this->property->listing_id){
	$userCanClose = false;
} else {
	$userCanClose = true;
}

$isYiiDebug = (YII_DEBUG) ? 1 : 0;
$hasGoogleTrackingCode = (Yii::app()->user->settings->google_tracking_code) ? 1 : 0;
$bodyOverflow = ($userCanClose ? "auto" : "hidden");
$css = <<<CSS
    body {
        overflow: {$bodyOverflow};
    }
    .home-details-register-dialog .ui-dialog-titlebar {
        display: none;
    }
    .home-details-register-dialog .submit-button-row{
        padding: 23px 15px 15px 15px;
        text-align: center;
        position: relative;
    }
    .home-details-register-dialog #register-dialog-submit-button, #house-value-button{
        text-transform: uppercase;
        width: 100%;
        color: white;
        font-size: 18px;
        font-weight: 600;
    }
    .home-details-register-dialog #register-dialog-submit-button.btn-primary:hover {
        background-color: #25AD29;
    }
    #house-value-button{
        background-color: #1aa3fe;
    }
    #house-value-button:hover{
        background-color: #25c3fe;
    }
    .home-details-register-dialog #register-dialog-loading{
        width: 32px;
        height: 32px;
        text-align: center;
        position: absolute;
        margin-left: -15px;
        top: -6px;
        left: 50%;
    }
    .home-details-register-dialog #register-dialog-loading.loading{
        background: url(http://cdn.seizethemarket.com/assets/images/loading.gif) no-repeat;
        opacity: 1 !important;
        position: relative;
    }
    #register-dialog th {
        width: 80px;
    }
    .home-details-register-dialog {
        position: fixed;
    }
    .dialog-form-container {
        overflow-y: auto;
    }
    .dialog-form-container {
        margin-left: auto;
        margin-right: auto;
    }
    .dialog-form-container .top_image {
        width: 220px;
        float: left;
    }
    .dialog-form-container .top_description {
        width: 50% !important;
        padding-left: 8px;
        float: left;
    }
    .dialog-form-container .be_surprised {
        text-align: center;
    }
    .dialog-form-container .or-container {
    margin: 12px 0;
    width: 100%;
    font-weight: bold;
    font-size: 15px;
    text-align: center;
    }
    .dialog-form-container .model_footer {
        margin-top: 20px;
        position: relative;
    }
    .dialog-form-container .model_footer h3{
        position: absolute;
        font-size: 15px;
        margin-left: 172px;
        font-weight: bold;
        padding-bottom: 0;
    }
    .house-values-row {
        text-align: center;
        font-size: 15px;
    }

    @media (min-width: 768px) {

    }
    @media (max-width: 530px) {
        .home-details-dialog.home-details-register-dialog {
            width: 100% !important;
        }
    }
    @media (max-width: 480px) {
        body {
            overflow: auto;
        }
        .home-details-register-dialog {
            position: absolute;
        }
        .home-details-register-dialog #register-dialog-submit-button {
            width: 100%;
            margin: 0 auto;
            float: none;
        }
        .dialog-form-container .top_image {
            width: 50%;
            margin: 0 auto;
        }
        .dialog-form-container .top_image img{
            width: 100%;
        }
        .dialog-form-container .top_description {
            width: 50% !important;
            /*width: 270px; */
            /*padding-left: 8px;*/
            /*float: left;*/
        }
        .dialog-form-container table .be_surprised {
            text-align: left;
            font-size: 14px;
        }
        .dialog-form-container .model_footer {
            display: none;
        }
    }
    .no-padding-left {
        padding-left: 0 !important;
    }
    #login-form-dialog,
    #forgot-password-form-dialog{
    	display:none;
    	padding:32px;
	}
	#register-dialog-form table,
	#login-form-dialog table,
	#login-form-dialog table input,
	#forgot-password-form-dialog table,
	#forgot-password-form-dialog table input{
		width:100%;
	}
	#login-form-dialog table button,
	#forgot-password-form-dialog table button{
		    width: 100%;
			margin: 12px auto;
			text-align: center;
			padding: 8px;
			font-size: 2em;
	}
	#register-form-messages{
		text-align:center;
		padding-top:8px;
	}
	 #register-form-messages .error{
	 	color:#C20000;
	 }
	 #register-dialog{
	 	overflow:hidden;
	 }
	 #close-register-form-button{
	 	position: absolute;
		right: 12px;
		font-size: 24px;
	 }
	 #close-register-form-button:hover{
	 	cursor:pointer;
	 	color: #C20000;
	 }
	 a.register-dialog-toggle, a.register-dialog-toggle.link {
	    color: black !important;
	 }
	 a.register-dialog-toggle:hover, a.register-dialog-toggle:hover.link {
	    color: #C20000 !important;
	 }
CSS;
Yii::app()->clientScript->registerCss('registerDialogCss', $css);
$js = <<<JS
	$(document).ready(function(){
		var registerDialogListingId = $("#property-details-container .ctabuttons .add-fav").data("listingid");
		if ( registerDialogListingId ){
			if(CLIENTMAXVIEWSREACHED ==  1){
				var url = $("form#register-dialog-form").attr("action");
				$.post("/login",{
						"addListingID" : registerDialogListingId
					},
					function(data) {
						if(data.header != undefined) {
							$("#register-dialog-form .topContent").html(data.header);
							$("#register-dialog-form .hiddenFormFields").html(data.formFieldHtml);
						}
					}
				);
				$("#register-dialog").dialog({"autoOpen":true,"closeOnEscape":false});
				$("body").css("overflow","hidden");
				$(".ui-dialog").css("width:100%");
			}
		} else if( !$("#close-register-form-button").html() ){
			$("#register-dialog").prepend("<span id='close-register-form-button' onclick='$(\"#register-dialog\").dialog(\"close\");'><i class='fa fa-close'></i>");
		}
		return;

	});
	$(".register-dialog-open").on("click",function(e){
		e.preventDefault();
		$('#register-dialog').dialog("open");
		$(this).removeClass("register-dialog-open");
		$(this).addClass("register-dialog-close");
		return toggleRegisterDialogPanel( $( this ).data("target") );
	});
	$(".register-dialog-close").on("click",function(e){
		e.preventDefault();
		$(this).removeClass("register-dialog-close");
		$(this).addClass("register-dialog-open");
		return $('#register-dialog').dialog("close");
	});
	function toggleRegisterDialogPanel(element_id){
		$("#register-dialog form").hide();
		$('#register-form-messages').html('');
		return $( "#" + element_id ).show();
	}

	$("a.register-dialog-toggle").on("click",function(e){
		e.preventDefault();
		return toggleRegisterDialogPanel( $( this ).data("target") );
	});

	$( "form .ajxSubmitButton" ).on("click", function( event ) {
		event.preventDefault();
		$(".loading-container").addClass("loading");
		var element_id = $(this).closest('form').attr('id');
		$("#"+element_id+" input").removeClass("error");
		$("#register-form-messages").html(" ");
		var params = $( "#"+element_id ).serializeArray();
		params.isAjax = 1;
		$.post(
			$("#"+element_id).attr("action"),
			params
			,
			function(data) {
				if(data.status == 200){
					if(data.redirect){
						window.location = data.redirect;
						return;
					}
					else {
						$(".loading-container").removeClass("loading");
					}
					if(data.hasError){
						$.each(data.errors, function( index, value ) {
							$("#register-form-messages").append("<span class='error'>"+value+"</span>");
						});
					}
					if(data.hasMessage){
						$.each(data.messages, function( index, value ) {
							$("#register-form-messages").append(value);

						});
					}
					if(element_id == "forgot-password-form-dialog"){
						if(data.passWordReset == "success"){
							$(".ui-dialog form").hide();
							$( "#login-form-dialog"  ).show();
						}
					}
				} else {
					$(".loading-container").removeClass("loading");
					if(element_id == "register-dialog-form"){
						if(data.email && !data.resubmit){
							location.reload();
							return;
						}
						else if(data.email && data.resubmit){
							window.location = "/login?email="+data.email+"&resubmit=1";
						}
					} else if (element_id == "login-form-dialog"){
						if(data.status == 404){
							return $("#"+element_id+" input").addClass("error");
						}
					}

					$.each(data, function( index, value ) {
						$("#"+element_id+" #"+index).addClass("error");
					});

				}
			}
		);
	});	
JS;

Yii::app()->clientScript->registerScript("registerloginscript", $js);
$this->beginWidget(
	'zii.widgets.jui.CJuiDialog', array(
		'id'      => $this->id,
		'options' => array(
			'title'         => $this->title,
			'autoOpen'      => ($userCanClose ? false : true),
			'closeOnEscape' => ($userCanClose ? true : false),
			'dialogClass'   => $this->dialogClass,
			'modal'         => true,
			'width'         => 530,
			'draggable'     => false,
			'resizable'     => false,
			'beforeClose'   => 'js:function() {
                if (!' . $isYiiDebug . ' && ' . $hasGoogleTrackingCode . ' && 0) {
                    _gaq.push(["_trackEvent", "Required Register Form", "Click", "Required Register Click Close"]);
                    _gaq.push(["b._trackEvent", "Required Register Form", "Click", "Required Register Click Close"]);
                }
		}',
		),
	)
);
//echo Yii::app()->getController()->getAction()->controller->action->id."<br>";

//echo "<pre>".print_r(Yii::app()->getController()->getAction()->controller->attributes,1)."</pre>";

?>
	<div id="register-form-messages"></div>
<?php
$form = $this->beginWidget(
	'CActiveForm', array(
		'id'                     => 'register-dialog-form',
		'action'                 => array('/front/forms/register/formId/' . $this->formId),
		'enableAjaxValidation'   => true,
		'enableClientValidation' => false,
		'clientOptions'          => array(
			'validateOnChange' => false,
			'validateOnSubmit' => true,
			'inputContainer'   => 'span',
			'beforeValidate'   => 'js:function() {
				$("#register-dialog-loading").addClass("loading");
				$(".loading-container").addClass("loading");

				return true;
            }',
			'afterValidate'    => 'js:function(form, data, hasErrors) {
				$("#register-dialog-loading").removeClass("loading");
				$(".loading-container").removeClass("loading");

                // If marked as spam then just refresh the current page
				if (data.isSpam) {
                    location.reload();
                    return;
				}
                if (!hasErrors) {
                    // Action was successful
					$(".loading-container").addClass("loading");

					// Google Goals tracking event: categories, actions, label
			        if (!' . $isYiiDebug . ' && ' . $hasGoogleTrackingCode . ' && 0) {
						_gaq.push(["_trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Success"]);
						_gaq.push(["b._trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Success"]);
					}
					if(data.resubmit==true) {
						window.location = "/login?email="+data.email+"&resubmit=1";
					} else {
						Message.create("success", "Congratulations! Full Access has been granted!");
						location.reload();
					}

                    return false;
                } else {
					$(".loading-container").removeClass("loading");
					$("#register-dialog-submit-button").prop("disabled", false);
					Message.create("error", "Please fill out the required fields and submit.");

					// Google Goals tracking event: categories, actions, label
			        if (!' . $isYiiDebug . ' && ' . $hasGoogleTrackingCode . ' && 0) {
						_gaq.push(["_trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Error"]);
						_gaq.push(["b._trackEvent", "Required Register Form", "Submit", "Required Register Form Submit Error"]);
					}
                }
            }',
		),
	)
);
?>
	<div class="dialog-form-container">
		<span class="topContent">
		<?php
		if($this->property->listing_id){
			?>
			<div style="margin-bottom: 10px; clear: both;">
				<div class="top_image">
					<img src="<?php echo $this->photoUrl; ?>" width="220px">
				</div>
				<div class="top_description">
					<div style="text-align:center;color: #00c300; font-weight: bold; font-size: 18px;margin-top: 20px;"> <?php if(!empty($this->property->photo_count)) echo ($this->property->photo_count)?$this->property->photo_count.' Photos Available!':' '; ?></div>
					<div style="text-align:center;font-weight: bold; font-size: 14px; margin-top: 4px;">View All Photos, Maps, Details</div>
					<div style="text-align:center; margin-top: 15px;"> All Properties current as of<br />Today <strong><?php echo date('F j, Y'); ?></strong>.</div>
				</div>
				<div class="clearfix"></div>
			</div>
		<?php } else { ?>
			<h3 style="padding: 32px 0px 16px 32px;">Register</h3>
			<hr />
		<?php }?>
</span>
		<table>
			<tr>
				<th>Name:</th>
				<td>
	    			<span class="col-sm-6 col-xs-12 no-padding">
			    		<?php echo $form->textField(
							$model, 'data[' . FormFields::getFieldIdByName('first_name') . ']',
							$htmlOptions = array('placeholder' => 'First Name', 'class' => 'col-xs-12',)
						); ?>
	    			</span>
	    			<span class="col-sm-6 col-xs-12 no-padding-left">
			    		<?php echo $form->textField(
							$model, 'data[' . FormFields::getFieldIdByName('last_name') . ']',
							$htmlOptions = array('placeholder' => 'Last Name', 'class' => 'col-xs-12',)
						); ?>
	    			</span>
					<?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('first_name') . ']'); ?>
					<?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('last_name') . ']'); ?>
				</td>
			</tr>
			<tr>
				<th>Email:</th>
				<td>
	    			<span class="col-sm-6 col-xs-12 no-padding">
			    		<?php echo $form->textField(
							$model, 'data[' . FormFields::getFieldIdByName('email') . ']',
							$htmlOptions = array('placeholder' => 'Email', 'class' => 'col-xs-12',)
						); ?>
						<?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('email') . ']', $htmlOptions=array('style'=>'display: inline-block;')); ?>
	    			</span>
	    			<span class="col-sm-6 col-xs-12 no-padding-left">
                        <?php $this->widget(
							'StmMaskedTextField', array(
								'model'       => $model,
								'attribute'   => 'data[' . FormFields::getFieldIdByName('phone') . ']',
								'mask'        => '(999) 999-9999',
								'htmlOptions' => $htmlOptions = array('placeholder' => 'Phone', 'class' => 'col-xs-12'),
							)
						); ?>
						<?php echo $form->error($model, 'data[' . FormFields::getFieldIdByName('phone') . ']'); ?>
	    			</span>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="submit-button-row">
					<input id="register-dialog-submit-button" type="submit" value="Get FREE Access Now" class="btn btn-success btn-primary btn-lg ajxSubmitButton">
				</td>
			</tr>
			<tr>
				<td colspan="2" class="house-values-row">
					<a href="/values">FREE <?=date('F Y')?> House Values Report</a>
				</td>
			</tr>

		</table>

		<div class="model_footer">
			<!--            <a href="/values">-->
			<!--                <img src="http://cdn.seizethemarket.--><?//=(YII_DEBUG) ? 'local': 'com'?><!--/assets/images/house_values_tag_2.jpg" alt="" width="100%;"/>-->
			<!--            </a>-->
			<div style="text-align:center;padding-bottom: 10px; font-size: 13px;">
				<a class="register-dialog-toggle" href="javascript:void(0);" data-target="login-form-dialog">Already a member? Login Here</a>
			</div>
		</div>
	</div>
	<div id="register-dialog-loading"></div>
	<span class="hiddenFormFields">
<?php
if($this->property->listing_id){
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('listing_id') .']', $htmlOptions=array('value'=>$this->property->listing_id));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('photo_url') .']', $htmlOptions=array('value'=>$this->photoUrl));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('home_url') .']', $htmlOptions=array('value'=>$this->property->getUrl()));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('price') .']', $htmlOptions=array('value'=>$this->property->price));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('bedrooms') .']', $htmlOptions=array('value'=>$this->property->bedrooms));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('baths') .']', $htmlOptions=array('value'=>$this->property->baths_total));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('sq_feet') .']', $htmlOptions=array('value'=>$this->property->sq_feet));

	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('address') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->streetAddress)))));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('city') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->city)))));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('state') .']', $htmlOptions=array('value'=>trim(ucwords(strtolower($this->property->state)))));
	echo $form->hiddenField($model, 'data['. FormFields::getFieldIdByName('zip') .']', $htmlOptions=array('value'=>$this->property->zip));
}
?>
	</span>
<?php

$this->endWidget();

$this->render('loginDialog',array("model"=>$model));
$this->render('forgotDialog',array("model"=>$model));

?>
	<div class="loading-container"><em></em></div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>