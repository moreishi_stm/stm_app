<?php
$model = new Emails('forgotPassword');
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'forgot-password-form-dialog',
	'enableAjaxValidation' => false,
	'action' => array('/forgotPassword'),
));
?>
	<h3>Reset Password</h3>
	<hr />
	<table>
		<tbody>
		<tr>
			<th><?php echo $form->labelEx($model,'email'); ?></th>
			<td>
				<?php echo $form->textField($model,'email'); ?>
				<?php echo $form->error($model,'email'); ?>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button id="forgotpassword-form-button" class="btn btn-primary ajxSubmitButton" type="submit">Recover Password</button>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center;">
				<a class="link child register-dialog-toggle" data-target="register-dialog-form">Not a Member? Register for FREE Here</a>
				&nbsp;
				<a class="link register-dialog-toggle" href="javascript:void(0);" data-target="login-form-dialog">Login Here</a>
			</td>
		</tr>
		</tbody>
	</table>
	<input type="hidden" name="isAjax" value="1">
<?php
$this->endWidget();