<?
$model = new LoginForm;
if (isset($_POST['LoginForm'])) {
	$model->attributes=$_POST['LoginForm'];
	$model->email = trim($model->email);
	$model->password = trim($model->password);
	// validate user input and redirect to the previous page if valid
	if ($model->validate() && $model->login()) {
		PageViewTrackerWidget::processLogin();
		RecentSavedHomesWidget::processLogin();
		Contacts::updateLastLogin();
		LoginLog::processLogin();
		// $defaultUrl = Yii::app()->createUrl('myAccount/index');
		$defaultUrl = '/myAccount';
		//$this->controller->redirect(Yii::app()->user->getReturnUrl($defaultUrl));
	}
} elseif (isset($_GET['email'])) {
	if(isset($_GET['resubmit']))
		$model->resubmit = $_GET['resubmit'];
	$email = str_replace(' ','',trim($_GET['email']));
	$model->email = $email;
}
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form-dialog',
	'enableAjaxValidation'=>false,
	'action'=>'/login',
));
?>
	<h3>Log In</h3>
	<hr />
	<table>
		<tbody>
		<tr>
			<th><?php echo $form->labelEx($model,'email'); ?></th>
			<td>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
			</td>
		</tr>
		<tr>
			<th><?php echo $form->labelEx($model,'password'); ?></th>
			<td>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
			</td>
		</tr>
		<tr>
			<td colspan="99">
		<button id="login-form-button" class="btn btn-primary ajxSubmitButton" type="submit">Login</button>
			</td>
		</tr>
		<tr>
			<td>

			</td>
			<td>
                <a class="link child register-dialog-toggle" data-target="register-dialog-form">Not a Member? Register for FREE Here</a> &nbsp;&nbsp;
				<a id="forgot-password" class="link register-dialog-toggle" data-target="forgot-password-form-dialog">Forgot Password?</a>
			</td>
		</tr>
		</tbody>
	</table>
	<input type="hidden" name="isAjax" value="1">

<? $this->endWidget(); ?>