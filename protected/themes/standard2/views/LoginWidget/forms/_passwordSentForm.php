<span id="close">X</span>
<h3>Password Sent</h3>
<hr />
<h4>Your password has been sent!</h4>
<p style="margin:15px 0px; clear:both; text-align:center;">For immediate support <br />Call <strong><?php echo Yii::app()->getUser()->getSettings()->office_phone; ?>.</strong></p>
<div class="row">
	<span id="return-to-login" class="link">Return to Login</span>
	<span id="new-user" class="link child">I'm a New User!</span>
</div>