<? $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>false,
	'action'=>array('/front/site/login'),
));
?>

<span id="close">X</span>
<h3>Log In</h3>
<hr />
<div class="row">
	<?php echo $form->labelEx($model,'email'); ?>
	<?php echo $form->textField($model,'email'); ?>
	<?php echo $form->error($model,'email'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model,'password'); ?>
	<?php echo $form->passwordField($model,'password'); ?>
	<?php echo $form->error($model,'password'); ?>
</div>
<div class="row">
	<button id="login-form-button" class="btn btn-primary" type="submit">Login</button>
</div>
<div class="row">
	<span id="forgot-password" class="link">Forgot Password?</span>
	<span id="new-user" class="link child">I'm a New User!</span>
</div>

<? $this->endWidget(); ?>