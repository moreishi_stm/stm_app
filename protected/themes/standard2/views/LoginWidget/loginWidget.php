<?php if (Yii::app()->user->isGuest): ?>
	<li class="login-register">
		<button id="login-button" data-target="login-form-dialog" class="hidden-xs register-dialog-open" style="border: none; background: none;">Log In / Register</button>
		<a  href="/login"><i class="glyphicon glyphicon-log-in hide visible-xs"></i></a>
	</li>
</ul>

<?php
	$this->widget('front_widgets.DialogWidget.RegisterDialogWidget.RegisterDialogWidget', array(
		'id'       => 'register-dialog',
		'property' => $model,
		'photoUrl' => $photoUrl,
		'title'    => 'Get Access Now!', // View Photos, Videos & Maps!
	));

else: ?>
<li><a href="/front/site/logout"><span><i class="glyphicon glyphicon-log-out hide visible-xs"></i></span></a></li>
</ul>
	<div id="login-widget" class="user hidden-xs">
        <div class="username cartMenu">
			<a href="" class=" user">
        		Hi <?php echo Yii::app()->user->firstName?>
                <?/*<span class="favorite-icon-container">
                    <i class="fa fa-heart"></i>
                    <span class="saved-count">1</span>
                </span>*/?>
                <i class="fa fa-caret-down"></i>
			</a>
		</div>
		<div class="user-dropdown">
			<ul>
        <li><a href="/myAccount">My Favorites</a></li>
        <li><a href="/myAccount/savedSearches/">Saved Searches</a></li>
        <li><a href="/myAccount/storyboards">My Storyboards</a></li>
        <li><a href="/myAccount/documents">My Documents</a></li>
<!--        <li><a href="/myAccount/subscriptions/">Email Preferences</a></li>-->
        <li><a href="/myAccount/profile/">Settings</a></li>
        <li><a href="/logout">Logout</a></li>
    </ul>
		</div>
	</div>

<?php endif;

