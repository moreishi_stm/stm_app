<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 1.0
 */
Yii::app()->getClientScript()->registerCss('cmsCommentEntryCss', <<<CSS
    .cms-comment-entry {
        margin: 5px;
        padding: 8px;
        clear: both;
        font-size: 15px;
        min-height: 75px;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -khtml-border-radius: 5px;
        -webkit-border-radius: 5px;
    }

    .child-cms-comment-entry {
        padding-left: 10px;
        border-left: 1px solid #CCC;
    }

    .cms-comment-entry div.header {
        color: #777;
        font-size: 12px;
    }

    div.cms-comment-entry div.profile_photo {
        width: 70px;
        float: left;
    }

    div.cms-comment-entry div.profile_photo img {
        width: 60px;
         border-radius: 30px;
        -moz-border-radius: 30px;
        -khtml-border-radius: 30px;
        -webkit-border-radius: 30px;
    }

    div.cms-comment-entry div.body {
        margin-left: 60px;
    }

    span.reply a {
        margin-left: 10px;
        font-weight: bold;
        color: #b88b2e;
        font-size: 11px;
        text-decoration: none;
    }

    span.reply a:hover {
        text-decoration: underline;
    }
CSS
);
?>
<div class="cms-comment-entry" data-id="<?php echo $cmsCommentEntry->id; ?>">
    <div class="profile_photo">
        <?php
            echo CHtml::image($cmsCommentEntry->contact->getProfilePhotoUrl(), $cmsCommentEntry->getName(), array(

            ));
        ?>
    </div>
    <div class="body">
        <div class="comment">
            <?php echo nl2br($cmsCommentEntry->comment); ?>
        </div>
        <div class="header">
            <?php echo $cmsCommentEntry->getName(); ?>
            | <?php echo Yii::app()->format->formatDateTime($cmsCommentEntry->added); ?>
            <span class="reply">
                <a href="#">[Reply]</a>
            </span>
        </div>
    </div>
    <?php
        $childComments = CmsComment::model()->findAllByAttributes(array(
            'parent_id' => $cmsCommentEntry->id,
        ));
        if ($childComments) {
            foreach ($childComments as $childComment) {
                echo CHtml::openTag('div', array(
                    'class' => 'child-cms-comment-entry cms-comment-entry',
                ));
                echo $this->render($this->commentEntryView, array(
                    'cmsCommentEntry' => $childComment,
                ));
                echo CHtml::closeTag('div');
            }
        }
    ?>
</div>
