<?php
/**
 * @author: Chris Willard <chriswillard.dev@gmail.com>
 * @since: 1.0
 */
Yii::app()->getClientScript()->registerCss('commentPillCss', <<<CSS
    #cms-comment-pill {
        padding: 0;
        clear: both;
        margin-top: 20px;
    }

    #cms-comment-pill div.cms-comment-entry:nth-child(even) {
        background-color: #EEE;
    }

    #cms-comment-pill div.cms-comment-entry:hover {
        background-color: #cde8ff;
    }

    #cms-comment-pill div#cms-add-comment {
        margin-top: 20px;
        background: #F7F7F7;
        border: 1px solid #CCC;
		padding: 10px;
    }

    /*#cms-comment-pill div#cms-add-comment div.row {
        width: 95%;
        margin: 5px;
        padding: 4px 10px;
    }*/

    /*#cms-comment-pill div#cms-add-comment div.row .clear {
        float: right;
        font-size: 11px;
    }*/

    /*#cms-comment-pill div#cms-add-comment div.row .half {
        width: 48%;
        display: inline-block;
    }*/
    #cms-comment-pill .button-row {
        margin-top: 20px;
    }
    #cms-comment-pill div#cms-add-comment div.row div.errorMessage {
        float: right;
    }

    #cms-comment-pill div#cms-add-comment div.row input[type="submit"] {
        margin-top: 10px;
        font-size: 11px;
    }

    #cms-comment-pill div#cms-add-comment div.row input[type="submit"]:hover {
        cursor: pointer;
    }

    #cms-comment-pill div#cms-add-comment textarea {
        font-family: Arial;
        font-size: 20px;
    }
CSS
);

Yii::app()->getClientScript()->registerScript('cmsCommentPillJs', <<<JS
    $('.clear-button').click(function() {
        var commentForm = $('#cms-add-comment');
        commentForm.find('textarea').val('');
        commentForm.find('input[type="hidden"]').val('');

        return false;
    });

    $('span.reply a').click(function() {
        var parent = $(this).parent().parent().parent('div.cms-comment-entry');

        var commentForm = $('#add-comment-form');
        commentForm.find('input[type="hidden"]').val(parent.attr('data-id'));

        // Scroll to the top of the comment form
        $('html, body').animate({
            scrollTop: $('#cms-add-comment').offset().top
        }, 750);

        return false;
    });
JS
);
?>
<div style="border-top: 1px solid #CCC; margin-top: 30px;"></div>
<div id="cms-comment-pill" class="col-lg-12 col-md-12 col-sm-12">
	<h3>Comments:</h3>
	<div class="col-lg-12 col-md-12 col-sm-12">
		<?php
			$comments = CmsComment::model()->findAllByAttributes(array(
				'parent_id' => null,
				'cms_content_id' => $cmsContentEntry->id,
			));
			if (!empty($comments)) {
				foreach ($comments as $comment) {
					$this->render($this->commentEntryView, array(
						'cmsCommentEntry' => $comment,
					));
				}
			}
		?>
	</div>
	<div class="clearfix"></div>
    <div id="cms-add-comment">
		<div class="form-group">
        <?php
        /** @var CActiveForm $form */
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'add-comment-form',
            'action' => CMap::mergeArray($this->commentFormAction, array('cmsContentId' => $cmsContentEntry->id)),
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'beforeValidate' => 'js:function(form) {
                    $("body").prepend("<div class=\"loading-container loading\"><em></em></div>");
                    form.find("input[type=\'submit\']").prop("disabled", true);

                    return true;
                }',
                'afterValidate' => 'js:function(form, data, hasError) {
                    if (!hasError) {
                        Message.create("success", "Successfully added comment!");
                        window.location.reload(false);
                    } else {
                        $("div.loading-container.loading").remove();
                        form.find("input[type=\'submit\']").prop("disabled", false);
                    }
                }',
            )
        ));

        $readOnly = (Yii::app()->user->getIsGuest()) ? '' : 'readonly';
        ?>
		</div>
        <div class="form-group">
			<div class="col-xs-6">
				<?php echo $form->labelEx($cmsCommentEntry, 'comment'); ?>
				<?php echo $form->error($cmsCommentEntry, 'comment'); ?>
				<?php echo $form->textArea($cmsCommentEntry, 'comment', array('class' => 'form-control','placeholder'=>'Comment','rows'=>4)); ?>
			</div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <?php echo $form->labelEx($cmsCommentEntry, 'email'); ?>
                <?php echo $form->error($cmsCommentEntry, 'email'); ?>
                <?php echo $form->textField($cmsCommentEntry, 'email', array(
                        'readonly' => $readOnly,
                        'placeholder'=>'Email',
						'class' => 'form-control'
                    )); ?>
            </div>
            <div class="col-xs-6">
                <?php echo $form->labelEx($cmsCommentEntry, 'name'); ?>
                <?php echo $form->error($cmsCommentEntry, 'name'); ?>
                <?php echo $form->textField($cmsCommentEntry, 'name', array(
                        'readonly' => $readOnly,
                        'placeholder'=>'First & Last Name',
						'class' => 'form-control'
                    )); ?>
            </div>
        </div>
		<div class="clearfix"></div>
        <div class="form-group">
			<div class="col-xs-12 button-row">
				<?php echo $form->hiddenField($cmsCommentEntry, 'parent_id'); ?>
				<?php echo CHtml::submitButton('Add Comment', array(
					'class' => 'btn btn-primary btn-lg btn-success',
				)); ?>
				<span class="btn btn-lg btn-link"><a class="clear-button" href="javascript:void(0)">Clear</a></span>
			</div>
        </div>
		<div class="clearfix"></div>
        <?php $this->endWidget(); ?>
    </div>
</div>
