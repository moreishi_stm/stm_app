<?php
$css = <<<CSS
    #house-values-widget-container {
        border: 1px solid #CCC;
        position: relative;
        padding: 15px;
        overflow-x: hidden;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        background-color: #F7F7FF;
        margin: 16px 0 8px 0 ;
        color: black;
    }
    #house-values-widget-container img{
        position: absolute;
        right: 3px;
        top: 11px;
        width: 100px;
    }
    #house-values-widget-container .title{
        font-size: 40px;
        padding-top: 34px;
        font-family: 'Arial';
    }
    #house-values-widget-container .year{
        font-size: 44px;
        font-family: 'Arial Black';
        line-height: 1.2;
    }
    #house-values-widget-container h2{
        font-size: 30px;
        line-height: 1;
        text-align: left;
        color: black;
    }
    #house-values-widget-container .item-text{
        margin-top: 10px;
        font-family: arial, helvetica, sans-serif;
        font-size: 14px;
    }
    #house-values-widget-container .item-text a{
        color: blue;
        display: block;
        font-weight: bold;
        padding: 4px 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        margin-left: 12px;
    }
    #house-values-widget-container .item-text a:hover{
        background-color: yellow;
    }
CSS;
Yii::app()->clientScript->registerCss('houseValuesWidgetCss', $css);
?>
<div id="house-values-widget-container">
    <div class="title">
        <div class="year"><?php echo date('Y'); ?></div>
        <h2>House Values Report</h2>
    </div>
    <img src="http://cdn.seizethemarket.com/assets/images/houseValuesWidget.png" border="0">
    <div class="item-text">
        Find out what your property is
        worth in today’s market.
        <a href="/values">FREE House Values Report</a>
    </div>
    <div class="item-text">
        Get the most local and accurate
        market information.
    </div>
    <div class="item-text">
        Ask about our FREE Real Estate
        Check-up!
        <a href="/values">CLICK HERE</a>
    </div>
</div>