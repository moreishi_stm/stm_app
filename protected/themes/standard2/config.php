<?php
return array(
    'widgetFactory'=>array(
        'enableSkin'=>true,
    ),
    'assetManager' => array(
        'class' => 'admin_module.components.AssetManager.StmAssetManager',
        'enableCdn' => true,
        'cdnUrl' => 'http://cdn.seizethemarket.com/assets/v1'
    ),
    'clientScript'=> array(
        'coreScriptPosition'=>CClientScript::POS_END,
        'defaultScriptFilePosition'=>CClientScript::POS_END,
        'packages'=> array(
            'jqueryMigrate' => array(
                'baseUrl'=>'http://code.jquery.com/',
                'js'=>array('jquery-migrate-1.2.1.min.js'),
            ),
            'jquery'=>array(
                'baseUrl'=>'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/',
                'js'=>array('jquery.min.js'),
                //'depends'=>array('jqueryMigrate'),
            ),
            'yiiactiveform'=>array(
                'js'=>array('jquery.yiiactiveform.js'),
                'depends'=>array('jquery'),
            ),
            'maskedinput'=>array(
                'js'=>NULL,
                'depends'=>NULL,
            ),
            'bbq' => array(
                'baseUrl'=>NULL,
                'js'=>NULL,
            ),
        )
    )
);