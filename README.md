Ok, I fixed this issue with a hack on a MoxieManager js variable. By doing:

moxman.Env.baseUrl = "PATH_TO_OTHER_SERVER/tinymce/plugins/moxiemanager";
After the tinyMCE initialize. The completed code is:

tinymce.init({
          ..... //options
          init_instance_callback: function () {
            moxman.Env.baseUrl = "PATH_TO_OTHER_SERVER/tinymce/plugins/moxiemanager";
          }
});
If you see a login popup when you open the Moxiemanager, take a look of your configuration file, if you have the authenticator key as SessionAuthenticator , you should share the session variables between both servers. Maybe you will need to make some changes on your Apache Config by adding some Headers such as :

Header set Access-Control-Allow-Headers: "Origin, X-Requested-With, Content-Type, Accept"
Header set Access-Control-Allow-Origin: "http://DOMAIN_OF_YOUR_SERVER"